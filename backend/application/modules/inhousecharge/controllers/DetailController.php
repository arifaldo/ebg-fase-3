<?php


require_once 'Zend/Controller/Action.php';


class inhousecharge_DetailController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$arraycharges = array_combine(array_values($this->_monthlytype['code']),array_values($this->_monthlytype['desc']));
		$custid = $this->_getParam('custid');
		$docErr = "*No changes allowed for this record while awaiting approval for previous change";
		$select = $this->_db->select()
				->from(array('A' => 'M_CUSTOMER'),array('*'));
		$select -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $this->_db->fetchRow($select);
		$this->view->result = $result;
		
		////Cash Management///////////////////////////////////////////////////
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		$selectnoprk2 -> where("A.ACCT_SOURCE IS NOT NULL");
		
		$selectprk = $selectprk->__toString();
		//$selectnoprk = $selectnoprk->__toString();
		$selectnoprk2 = $selectnoprk2->__toString();
		
		$unionquery = $this->_db->select()
								->union(array($selectprk));
								
		$selectunion = $this->_db->select()
							->from (($unionquery),array('*'));
		
		$chargeaccount = $this->_db->select()
								->union(array($selectnoprk2));
		$resultlist = $chargeaccount->query()->FetchAll();
							
		$chargestatus = $result["CUST_CHARGES_STATUS"];
		$monthlyfeestatus = $result["CUST_MONTHLYFEE_STATUS"];
		
		if($chargestatus == 0)
		{
			$this->view->chargestatus = 'Disabled';
			$chargestatuspdf = 'Disabled';
		}
		if($chargestatus == 1)
		{
			$this->view->chargestatus = 'Enabled';
			$chargestatuspdf = 'Enabled';
		}
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = 'Disabled';
			$monthlyfeestatuspdf = 'Disabled';
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = 'Enabled';
			$monthlyfeestatuspdf = 'Enabled';
		}
		
////////Realtime charges/////////////////////////////////////////////////////////////////////////////		
		
		$select3 = $this->_db->select()
							->from('TEMP_CHARGES_WITHIN');
		$select3 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select3 -> where("BUSINESS_TYPE = '1'");
		$cekrealtime = $select3->query()->FetchAll();
		//Zend_Debug::dump($cekrealtime);die;
		$select4 = $this->_db->select()
			        	->from(array('A' => 'M_CHARGES_WITHIN'),array('*'))
			        	->join(array('B' => $chargeaccount), 'A.ACCT_NO = B.ACCT_NO',array('*'));
		$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$select4 -> where("A.BUSINESS_TYPE = '1'");
		$result4 = $select4->query()->FetchAll();
		
		if($cekrealtime)
		{
			$this->view->disablerealtime = true;	
			$this->view->realtimeerror = $docErr;
		}
		
		$i = 0;
		foreach($result4 as $realtimelist)
		{
			$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
			if($i==0)
			{
				$templatedetail = 
				'<tr>
					<td class="'.$td_css.'">'.$realtimelist['ACCT_NO'].' / '.$realtimelist['ACCT_NAME'].' ('.$realtimelist['CCY_ID'].')</td>
					<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($realtimelist['AMOUNT']).'</td>
			  	</tr>';
			}
			else
			{
				$templatedetail = $templatedetail.
				'<tr>
					<td class="'.$td_css.'">'.$realtimelist['ACCT_NO'].' / '.$realtimelist['ACCT_NAME'].' ('.$realtimelist['CCY_ID'].')</td>
					<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($realtimelist['AMOUNT']).'</td>
			  	</tr>';
			}
			$i++;
		}
		
		if(isSet($templatedetail))
		{
			$templaterealtime =
			'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="600">
				<tr>
					<th valign="top">Account</th>
					<th valign="top">Charges per Transaction</th>
				</tr>'
				.$templatedetail.
			'</table>';
		
		$this->view->templaterealtime = $templaterealtime;
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////

////////Monthly Fee//////////////////////////////////////////////////////////////////////////////////

		if($result['CUST_MONTHLYFEE_TYPE'])
		{
			$monthlytype = $arraycharges[$result['CUST_MONTHLYFEE_TYPE']];
		}
		else
		{
			$monthlytype = "";
		}
		$this->view->monthlytype = $monthlytype;
		
		$select5 = $this->_db->select()
					->from(('TEMP_ADMFEE_MONTHLY'),array('*'));
		$select5 -> where("MONTHLYFEE_TYPE = '1' OR MONTHLYFEE_TYPE = '2'");	
		$select5 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		
		$select6 = $this->_db->select()
					->from('TEMP_CHARGES_MONTHLY');
		$select6 -> where("MONTHLYFEE_TYPE = '3' OR MONTHLYFEE_TYPE = '4'");	
		$select6 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
	
		$cekmonthly1 = $select5->query()->FetchAll();
		$cekmonthly2 = $select6->query()->FetchAll();
		
		//Zend_Debug::dump($cekmonthly1);die;
		
		if($cekmonthly1 || $cekmonthly2)
		{
			$this->view->disablemonthly = true;	
			$this->view->monthlyerror = $docErr;
		}
		
		if($result['CUST_MONTHLYFEE_TYPE'] != '0')
		{
			if($result['CUST_MONTHLYFEE_TYPE'] == '1' || $result['CUST_MONTHLYFEE_TYPE'] == '2')
			{ 
				$select7 = $this->_db->select()
			        	->from(array('A' => 'M_ADMFEE_MONTHLY'),array('*'))
			        	->joinleft(array('B' => $selectunion), 'A.ACCT_NO = B.ACCT_NO',array('*'));
			}
			
			if($result['CUST_MONTHLYFEE_TYPE'] == '3' || $result['CUST_MONTHLYFEE_TYPE'] == '4')
			{	 
				$select7 = $this->_db->select()
			        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'))
			        	->joinleft(array('B' => $selectunion), 'A.ACCT_NO = B.ACCT_NO',array('*'));	
			}
			
			$select7 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select7 -> where("MONTHLYFEE_TYPE LIKE ".$this->_db->quote($result['CUST_MONTHLYFEE_TYPE']));
			$result7 = $select7->query()->FetchAll();
			//die;
						
			if($result['CUST_MONTHLYFEE_TYPE'] == '3' || $result['CUST_MONTHLYFEE_TYPE'] == '4')
			{	 
				$select7 = $this->_db->select()
			        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'))
			        	->joinleft(array('B' => 'M_CUSTOMER_ACCT'), 'A.CUST_ID = B.CUST_ID AND A.ACCT_NO = B.ACCT_NO',array('*'));	
			}
			
			$select7 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select7 -> where("MONTHLYFEE_TYPE LIKE ".$this->_db->quote($result['CUST_MONTHLYFEE_TYPE']));	
			$result7 = $select7->query()->FetchAll();

			/*---------------------------Template Admin Fee Account-------------------------------*/
			if($result['CUST_MONTHLYFEE_TYPE'] == 1 && $result7)
			{
				$i = 0;
				foreach($result7 as $monthlylist)
				{
					$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
					
					$chargesacct = $this->_db->select()->distinct()
			        			->from(array('A' => $selectunion),array('*'))
			        			->where("A.ACCT_NO LIKE ".$this->_db->quote($monthlylist['CHARGES_ACCT_NO']));
			        $chargesacct = $this->_db->fetchRow($chargesacct);
					
			        if($chargesacct)
			        {
			        	$chargesdata = $chargesacct['ACCT_NO'].' / '.$chargesacct['ACCT_NAME'].' ('.$chargesacct['CCY_ID'].')';
			        }
			        else
			        {
			        	$chargesdata = '-';
			        }
			        
					if($i==0)
					{
						$templatemonthlydetail = 
						'<tr>
							<td class="'.$td_css.'">'.$monthlylist['ACCT_NO'].' / '.$monthlylist['ACCT_NAME'].' ('.$monthlylist['CCY_ID'].')</td>
							<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($monthlylist['AMOUNT']).'</td>
							<td class="'.$td_css.'">'.$chargesdata.'</td>
			  			</tr>';
					}
					else
					{
						$templatemonthlydetail = $templatemonthlydetail.
						'<tr>
							<td class="'.$td_css.'">'.$monthlylist['ACCT_NO'].' / '.$monthlylist['ACCT_NAME'].' ('.$monthlylist['CCY_ID'].')</td>
							<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($monthlylist['AMOUNT']).'</td>
							<td class="'.$td_css.'">'.$chargesdata.'</td>
			  			</tr>';
					}
					$i++;
				}
		
				$templatemonthly =
				'<table class="table-sm table-striped">
					<tr>
						<th valign="top">Account</th>
						<th valign="top">Charges per Month</th>
						<th valign="top">Account Charges</th>
					</tr>'
					.$templatemonthlydetail.
				'</table>';
		
				$this->view->templatemonthly = $templatemonthly;
			}
			/*-----------------------------------------------------------------------------------------*/
			
			/*---------------------------Template Admin Fee Company-----------------------------------*/
			if($result['CUST_MONTHLYFEE_TYPE'] == 2 && $result7)
			{
				
				foreach($result7 as $monthlylist)
				{
					$templatemonthlydetail = 
						'<tr>
							<td class="tbl-evencontent">'.$monthlylist['ACCT_NO'].' / '.$monthlylist['ACCT_NAME'].' ('.$monthlylist['CCY_ID'].')</td>
							<td class="tbl-evencontent">'.Application_Helper_General::displayMoney($monthlylist['AMOUNT']).'</td>
			  			</tr>';
				}
		
				$templatemonthly =
				'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="600">
					<tr>
						<th valign="top">Account</th>
						<th valign="top">Charge per Month</th>
					</tr>'
					.$templatemonthlydetail.
				'</table>';
		
				$this->view->templatemonthly = $templatemonthly;
			}
			/*-----------------------------------------------------------------------------------------*/

			/*---------------------------Template Monthly Fee Account-------------------------------*/
			if($result['CUST_MONTHLYFEE_TYPE'] == 3 && $result7)
			{
				$monthlyaccount = $this->_db->select()->distinct()
			        			->from(array('A' => 'M_CHARGES_MONTHLY'),array('A.ACCT_NO'))
			        			-> where("A.CUST_ID LIKE ".$this->_db->quote($custid))
								-> where("MONTHLYFEE_TYPE LIKE ".$this->_db->quote($result['CUST_MONTHLYFEE_TYPE']));
			        			
			    $resultmonthlyaccount = $this->_db->fetchAll($monthlyaccount);
			    
				$i = 1;
				
				foreach($resultmonthlyaccount as $monthlylist)
				{
					$acctno = $monthlylist['ACCT_NO'];
					$select77 = $this->_db->select()
			        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'))
			        	->joinleft(array('B' => $selectunion), 'A.ACCT_NO = B.ACCT_NO',array('*'));
			        $select77 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
					$select77 -> where("MONTHLYFEE_TYPE LIKE ".$this->_db->quote($result['CUST_MONTHLYFEE_TYPE']));		
					$select77 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
					$resultacct =  $this->_db->fetchAll($select77);
					$rowspan = count($resultacct);
					$y = 1;
					foreach($resultacct as $resultacctlist)
					{					
					
						$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
						
						$chargesacct = $this->_db->select()->distinct()
			        			->from(array('A' => $selectunion),array('*'))
			        			->where("A.ACCT_NO LIKE ".$this->_db->quote($resultacct[0]['CHARGES_ACCT_NO']));
			        	$chargesacct = $this->_db->fetchRow($chargesacct);
						//Zend_Debug::dump($result7);die;
						
					 	if($chargesacct)
			        	{
			        		$chargesdata = $chargesacct['ACCT_NO'].' / '.$chargesacct['ACCT_NAME'].' ('.$chargesacct['CCY_ID'].')';
			        	}
			       	 	else
			        	{
			        		$chargesdata = '-';
			        	}
			        	
						if($y == 1 && $i == 1)
						{
							$templatemonthlydetail = 
							'<tr>
								<td class="'.$td_css.'" rowspan = "'.$rowspan.'">'.$resultacctlist['ACCT_NO'].' / '.$resultacctlist['ACCT_NAME'].' ('.$resultacctlist['CCY_ID'].')</td>
								<td class="'.$td_css.'">'.$resultacctlist['TRA_FROM'].'</td>
								<td class="'.$td_css.'">'.$resultacctlist['TRA_TO'].'</td>
								<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($resultacctlist['AMOUNT']).'</td>
								<td class="'.$td_css.'" rowspan = "'.$rowspan.'">'.$chargesdata.'</td>
							</tr>';
						}
						
						if($y == 1 && $i != 1)
						{
							$templatemonthlydetail = $templatemonthlydetail.
							'<tr>
								<td class="'.$td_css.'" rowspan = "'.$rowspan.'">'.$resultacctlist['ACCT_NO'].' / '.$resultacctlist['ACCT_NAME'].' ('.$resultacctlist['CCY_ID'].')</td>
								<td class="'.$td_css.'">'.$resultacctlist['TRA_FROM'].'</td>
								<td class="'.$td_css.'">'.$resultacctlist['TRA_TO'].'</td>
								<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($resultacctlist['AMOUNT']).'</td>
								<td class="'.$td_css.'" rowspan = "'.$rowspan.'">'.$chargesdata.'</td>
							</tr>';
						}
						
						if($y !=1)
						{
							$templatemonthlydetail = $templatemonthlydetail.
							'<tr>
								<td class="'.$td_css.'">'.$resultacctlist['TRA_FROM'].'</td>
								<td class="'.$td_css.'">'.$resultacctlist['TRA_TO'].'</td>
								<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($resultacctlist['AMOUNT']).'</td>
							</tr>';
						}
						$y++;				
					}
					$i++;
				}
				//echo $i;die;
				$templatemonthly = 
						'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="80%">
  						 	<tr>
    							<th rowspan="2"><div align="center">Account</div></th>
    							<th colspan="2">Number of Transaction</th>
    							<th rowspan="2"><div align="center">Charges per Month</div></th>
    							<th rowspan="2"><div align="center">Account Charges</div></th>
  							</tr>
  							<tr>
    							<th>From</th>
   								<th>To</th>
  							</tr>'
  							.$templatemonthlydetail.
  						'</table>';
  							
  				$this->view->templatemonthly = $templatemonthly;
			}
			/*-----------------------------------------------------------------------------------------*/
		
			/*---------------------------Template Monthly Fee Company---------------------------------*/
			if($result['CUST_MONTHLYFEE_TYPE'] == 4 && $result7)
			{
				$i = 1;
				foreach($result7 as $monthlylist)
				{
					$acctno = $monthlylist['ACCT_NO'];
					$rowspan = count($resultacct);
	
					$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
						
					$chargesacct = $this->_db->select()->distinct()
			        		->from(array('A' => $chargeaccount),array('*'))
			        		->where("A.ACCT_NO LIKE ".$this->_db->quote($monthlylist['CHARGES_ACCT_NO']));
			        $chargesacct = $this->_db->fetchRow($chargesacct);
						
					if($i == 1)
					{
						$templatemonthlydetail = 
						'<tr>
							<td class="'.$td_css.'">'.$monthlylist['TRA_FROM'].'</td>
							<td class="'.$td_css.'">'.$monthlylist['TRA_TO'].'</td>
							<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($monthlylist['AMOUNT']).'</td>
						</tr>';
					}
					else
					{
						$templatemonthlydetail = $templatemonthlydetail.
						'<tr>
							<td class="'.$td_css.'">'.$monthlylist['TRA_FROM'].'</td>
							<td class="'.$td_css.'">'.$monthlylist['TRA_TO'].'</td>
							<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($monthlylist['AMOUNT']).'</td>
						</tr>';
					}
					$i++;
				}
				
				$templatemonthly =
						'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="50%">
  						 	<tr>
    							<th colspan="2">Number of Transaction</th>
    							<th rowspan="2"><div align="center">Charges per Month</div></th>
  							</tr>
  							<tr>
    							<th>From</th>
   								<th>To</th>
  							</tr>'
  							.$templatemonthlydetail.
  						'</table>
  						<br />
  						<b>Account Charges:</b> '.$chargesacct['ACCT_NO'].' / '.$chargesacct['ACCT_NAME'].' ('.$chargesacct['CCY_ID'].')';
  				
  				$this->view->templatemonthly = $templatemonthly;
			}
			/*-----------------------------------------------------------------------------------------*/
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////

/////Service Charges/////////////////////////////////////////////////////////////////////////////////
		$select11 = $this->_db->select()
					->from(('TEMP_CHARGES_OTHER'),array('*'));
		$select11 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select11 -> where("CHARGES_TYPE = '1' OR CHARGES_TYPE = '2'");
		$cekservice = $select11->query()->FetchAll();
		
		if($cekservice)
		{
			$this->view->disableservice = true;	
			$this->view->serviceerror = $docErr;
		}
		
		$select12 = $this->_db->select()->distinct()
			        ->from(array('A' => 'M_CHARGES_OTHER'),array('*'))
			        ->joinleft(array('B' => 'M_CUSTOMER_ACCT'), 'A.CUST_ID = B.CUST_ID',array('*'));
		$select12 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$select12 -> where("CHARGES_TYPE = '1'");
		$result12 = $this->_db->fetchRow($select12);
			
		$select13 = $this->_db->select()->distinct()
			        ->from(array('A' => 'M_CHARGES_OTHER'),array('*'))
			        ->joinleft(array('B' => 'M_CUSTOMER_ACCT'), 'A.CUST_ID = B.CUST_ID',array('*'));
		$select13 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$select13 -> where("CHARGES_TYPE = '2'");
		$result13 = $this->_db->fetchRow($select13);
		
		if($result12 && $result13)
		{
			$templateservice =
				   '<div class="table-responsive">
				   	<table class="table-sm table-striped">
					<tr>
						<th>Charges Type</th>
						<th>Currency</th>
						<th>Amount</th>
					</tr>
					<tr>
						<td class="tbl-evencontent">RTGS</td>
						<td class="tbl-evencontent">'.$result12["CHARGES_CCY"].'</td>
						<td class="tbl-evencontent">'.Application_Helper_General::displayMoney($result12["CHARGES_AMT"]).'</td>
					</tr>
					<tr>
						<td class="tbl-oddcontent">SKN</td>
						<td class="tbl-oddcontent">'.$result13["CHARGES_CCY"].'</td>
						<td class="tbl-oddcontent">'.Application_Helper_General::displayMoney($result13["CHARGES_AMT"]).'</td>
					</tr>
					</table>
					</div>';
			
			$this->view->templateservice = $templateservice;
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////
	
//////PDF///////////////////////////////////////////////////////////////////////////////////////////

		$pdf = $this->_getParam('pdf');
		if($pdf)
		{
			$template1 ='
			<div class="tabheader" id="tabheader">Company Charges and Administration Fee Detail</div>
			<br /><br />
			<h2>Detail Company Charges and Administration Fee</h2>
			<table border="0" cellspacing="0" cellpadding="0" class="tableform" width="600">
				<tr>
					<td class="tdform-even">&nbsp; Company Code</td>
					<td class="tdform-even">&nbsp;</td>
					<td class="tdform-odd">'.$result['CUST_ID'].'</td>
				</tr>
				<tr>
					<td class="tdform-even">&nbsp; Company Name</td>
					<td class="tdform-even">&nbsp;</td>
					<td class="tdform-odd">'.$result['CUST_NAME'].'</td>
				</tr>
				<tr>
					<td class="tdform-even">&nbsp; Current Charges Status</td>
					<td class="tdform-even">&nbsp;</td>
					<td class="tdform-odd">'.$chargestatuspdf.'</td>
				</tr>
				<tr>
					<td class="tdform-even">&nbsp; Current Administration Fee Status</td>
					<td class="tdform-even">&nbsp;</td>
					<td class="tdform-odd">'.$monthlyfeestatuspdf.'</td>
				</tr>
			</table>
			<br />
			<i>*Charges and Monthly Fee Status can be set from customer management menu</i>
			<br />
			<br />
			<br />';

			if($templatemonthly)
			{
				$template3 = '
				<h2>Monthly Fee</h2>
				'.'<b>'.$monthlytype.'</b><br/>'
				.$templatemonthly.'<br /><br /><br />';
			}
			else
			{
				$template3 = '
				<h2>Monthly Fee</h2>
				Monthly fee Charges not yet set<br /><br /><br />';
			}
			
			if($templaterealtime)
			{
				$template2 = '
				<h2>Inhouse Charges</h2>
				'.$templaterealtime.'<br /><br /><br />';
			}
			else
			{
				$template2 = '
				<h2>Inhouse Charges</h2>
				Realtime Charges not yet set<br /><br /><br />';
			}
			
			if($templateservice)
			{
				$template5 = '
				<h2>Service Charges</h2>
				'.$templateservice.'<br /><br /><br />';
			}
			else
			{
				$template5 = '
				<h2>Service Charges</h2>
				Service Charges scheme not yet set<br /><br /><br />';
			}
			
			$datapdf = "<tr><td>".$template1.$template3.$template2.$template5."</td></tr>";
			//Zend_Debug::dump($datapdf); die;
			$this->_helper->download->pdf(null,null,null,'Company Charges and Administration Fee',$datapdf);   
		}
		Application_Helper_General::writeLog('CHLS','View Company charges detail ('.$custid.')');
	}
}
