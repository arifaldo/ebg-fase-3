<?php
require_once 'Zend/Controller/Action.php';
require_once 'Service/Account.php';

class marginaldeposit_SuggestiondetailController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newpopup');

		$model = new marginaldeposit_Model_Marginaldeposit();

		$changes_id = $this->_getParam('changes_id');
		$changes_id = (Zend_Validate::is($changes_id, 'Digits')) ? $changes_id : 0;
		$this->view->changes_id = $changes_id;

		if ($changes_id) {
			$select = $this->_db->select()
				->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
				->where('CHANGES_ID = ' . $this->_db->quote($changes_id))
				->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
			$result = $this->_db->fetchOne($select);

			if (empty($result)) {
				$this->_redirect('/notification/invalid/index');
			} else {
				$select = $this->_db->select()
					->from(
						array('A' => 'TEMP_MARGINALDEPOSIT'),
						array('*')
					)
					->joinLeft(
						array('B' => 'TEMP_MARGINALDEPOSIT_DETAIL'),
						'B.CUST_ID = A.CUST_ID AND B.CHANGES_ID=A.CHANGES_ID',
						array('*')
					)
					->joinLeft(
						array('D' => 'M_PRODUCT_TYPE'),
						'D.PRODUCT_CODE = B.MD_ACCT_TYPE',
						array('PRODUCT_NAME')
					)
					->joinLeft(
						array('C' => 'M_CUSTOMER'),
						'C.CUST_ID = A.CUST_ID',
						array('CUST_ID', 'CUST_CIF', 'CUST_NAME', 'CUST_PHONE')
					)
					->joinLeft(
						array('G' => 'T_GLOBAL_CHANGES'),
						'G.CHANGES_ID = A.CHANGES_ID',
						array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
					)
					->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'B.MD_ACCT = MCA.ACCT_NO', 'ACCT_DESC')
					->where('A.CHANGES_ID = ?', $changes_id);
				$resultData = $this->_db->fetchAll($select);
				//echo '<pre>';print_r($resultData);
				//die('resultData');

				if ($resultData) {




					$cust_id   = $resultData[0]['CUST_ID'];
					$cust_name = $resultData[0]['CUST_NAME'];
					$custCIF   = $resultData[0]['CUST_CIF'];

					$this->view->cust_id        = $resultData[0]['CUST_ID'];
					$this->view->changes_id     = $resultData[0]['CHANGES_ID'];
					$this->view->changes_type   = $resultData[0]['CHANGES_TYPE'];
					$this->view->changes_status = $resultData[0]['CHANGES_STATUS'];
					$this->view->read_status    = $resultData[0]['READ_STATUS'];
					$this->view->flag           = $resultData[0]['FLAG'];

					$this->view->acctNo   = [];
					$this->view->acctName = [];
					$this->view->acctType = [];
					$this->view->acctCcy  = [];
					$this->view->balance  = [];

					$checkinq = true;
					// var_dump($resultData);die;
					foreach ($resultData as $row) {

						$acctNo  = $row['MD_ACCT'];
						$acctCcy = Application_Helper_General::getCurrNum($row['MD_ACCT_CCY']);


						$svcAccount = new Service_Account($acctNo, $ccy);
						$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
						//var_dump($result);die;
						if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
							//var_dump($result);
							//var_dump($result);
							//var_dump($row['GUARANTEE_AMOUNT']);die;
							/*if ($result['balance_active'] < $row['GUARANTEE_AMOUNT']) {
                                //var_dump($result);die;
                                $checkinq= false;
                                break;
                            } else {
                                // DATA TEMP INSURANCE BRANCH
                                array_push($this->view->acctNo, $row['MD_ACCT']);
                                array_push($this->view->acctName, $row['MD_ACCT_NAME']);
                                array_push($this->view->acctType, $row['MD_ACCT_TYPE']);
                                array_push($this->view->acctCcy, $row['MD_ACCT_CCY']);
                                array_push($this->view->balance, $row['GUARANTEE_AMOUNT']);
                            }*/

							array_push($this->view->acctNo, $row['MD_ACCT']);
							array_push($this->view->acctName, $row['MD_ACCT_NAME']);
							// array_push($this->view->acctType, empty($row['PRODUCT_NAME']) ? $row['MD_ACCT_TYPE'] : $row['PRODUCT_NAME']);
							array_push($this->view->acctType, empty($row['PRODUCT_NAME']) ? $row['ACCT_DESC'] : $row['PRODUCT_NAME']);
							array_push($this->view->acctCcy, $row['MD_ACCT_CCY']);
							array_push($this->view->balance, $row['GUARANTEE_AMOUNT']);
						} else {
							if ($result['response_code'] != '0000') {
								$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
								$result = $svcAccount->inquiryDeposito('AB', TRUE);
								//var_dump($result);die;
								if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
									array_push($this->view->acctNo, $row['MD_ACCT']);
									array_push($this->view->acctName, $row['MD_ACCT_NAME']);
									// array_push($this->view->acctType, empty($row['PRODUCT_NAME']) ? $row['MD_ACCT_TYPE'] : $row['PRODUCT_NAME']);
									array_push($this->view->acctType, empty($row['PRODUCT_NAME']) ? $row['ACCT_DESC'] : $row['PRODUCT_NAME']);
									array_push($this->view->acctCcy, $row['MD_ACCT_CCY']);
									array_push($this->view->balance, $row['GUARANTEE_AMOUNT']);
								} else {
									$checkinq = false;
									break;
								}
								//var_dump($result);die;
							} else {
								$checkinq = false;
								break;
							}
						}
					}
					//die;
					$submit_review         = $this->_getParam('submit_review');
					$submit_approve        = $this->_getParam('submit_approve');
					$submit_reject         = $this->_getParam('submit_reject');

					//var_dump($checkinq);die;
					if ($submit_review && $checkinq) {
						//$this->_db->beginTransaction();

						try {
							$data = [
								'FLAG'              => 2,
								'LAST_REVIEWED'     => new Zend_Db_Expr('now()'),
								'LAST_REVIEWEDBY'   => $this->_userIdLogin
							];
							$where = ['CHANGES_ID = ?' => $changes_id];
							$this->_db->update('TEMP_MARGINALDEPOSIT', $data, $where);

							//$this->_db->commit();

							Application_Helper_General::writeLog('RMDC', 'Review Insurance Marginal Deposit Changes for ' . $cust_name . ' (' . $cust_id . ')');

							$this->view->is_review = true;
						} catch (Exception $error) {
							//$this->_db->rollBack();
							echo '<pre>';
							print_r($error->getMessage());
							echo '</pre><br>';
							die;
						}
					} elseif ($submit_approve && $checkinq) {

						// start validasi
						$data = $this->_db->select()
							->from(
								array('A' => 'M_MARGINALDEPOSIT'),
								array('*')
							)
							->joinLeft(
								array('C' => 'M_CUSTOMER'),
								'C.CUST_ID = A.CUST_ID',
								array('CUST_ID', 'CUST_NAME')
							)
							->joinLeft(
								array('D' => 'M_CUST_LINEFACILITY'),
								'D.CUST_ID = A.CUST_ID',
								array('PLAFOND_LIMIT', 'MARGINAL_DEPOSIT', "PKS_STATUS_LF" => "D.STATUS")
							)
							->where("A.CUST_ID = ?", $cust_id)
							->query()->fetchAll();

						$select = $this->_db->select()
							->from(
								array('A' => 'TEMP_MARGINALDEPOSIT'),
								array('*')
							)
							->joinLeft(
								array('B' => 'TEMP_MARGINALDEPOSIT_DETAIL'),
								'B.CUST_ID = A.CUST_ID AND B.CHANGES_ID=A.CHANGES_ID',
								array('*')
							)
							->joinLeft(
								array('C' => 'M_CUSTOMER'),
								'C.CUST_ID = A.CUST_ID',
								array('CUST_ID', 'CUST_CIF', 'CUST_NAME', 'CUST_PHONE')
							)
							->joinLeft(
								array('D' => 'M_CUSTOMER_ACCT'),
								'B.MD_ACCT = D.ACCT_NO AND D.CUST_ID = B.CUST_ID',
								array('ACCT_TYPE')
							)
							->joinLeft(
								array('G' => 'T_GLOBAL_CHANGES'),
								'G.CHANGES_ID = A.CHANGES_ID',
								array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
							)
							->where('A.CHANGES_ID = ?', $changes_id);
						$resultData = $this->_db->fetchAll($select);

						$filter = true;
						$filterParam['fCustID'] 	= $cust_id;
						$data = $model->getData($filterParam, $filter);

						foreach ($data as $row) {
							$COUNTING_DEADLINE_TOPUP += $row['COUNTING_DEADLINE_TOPUP'];
						}

						foreach ($resultData as $row) {
							$select = $this->_db->select()
								->from(
									array('M_MARGINALDEPOSIT_DETAIL'),
									array('MD_ACCT', 'GUARANTEE_AMOUNT')
								)
								->where('MD_ACCT_TYPE = ?', 'ESCROW')
								->where('CUST_ID = ?', $cust_id);
							$escrowData = $this->_db->fetchRow($select);
							if ($escrowData) {
								$escrowAmount = $escrowData['GUARANTEE_AMOUNT'];
							}
							$totalescrowAmount = $escrowAmount + $row['GUARANTEE_AMOUNT'];

							if ($totalescrowAmount < $data[0]['MARGINAL_DEPOSIT']) {
								//$errMsgFlag = true;
								//$errMsgMarginalDeposit[$row['MD_ACCT']] = "Kurang dari marginal deposit";

								$dataUpdate2  = ['LAST_CHECK_MINUS' => new Zend_Db_Expr('now()'), 'COUNTING_DEADLINE_TOPUP' => $COUNTING_DEADLINE_TOPUP + 1];
								$whereUpdate2 = ['CUST_ID = ?' => $cust_id];
								//var_dump($dataUpdate2);die;
								//$this->_db->beginTransaction();
								try {
									$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
								} catch (Exception $error) {
									$this->_db->rollBack();
									//echo '<pre>';
									//print_r($error->getMessage());
									//echo '</pre><br>';
									die;
								}

								$select = $this->_db->select()
									->from(
										array('M_CUST_LINEFACILITY'),
										array('GRACE_PERIOD', 'GRACE_PERIOD')
									)
									->where('CUST_ID = ?', $cust_id);
								$lfData = $this->_db->fetchRow($select);

								$select2 = $this->_db->select()
									->from(
										array('M_MARGINALDEPOSIT'),
										array('COUNTING_DEADLINE_TOPUP', 'COUNTING_DEADLINE_TOPUP')
									)
									->where('CUST_ID = ?', $cust_id);
								$mdData = $this->_db->fetchRow($select2);

								/*if($lfData){
									if ($mdData['COUNTING_DEADLINE_TOPUP'] > $lfData['GRACE_PERIOD']){
										$errMsgFlag = true;
										$errMsgMarginalDeposit[$row['MD_ACCT']] = "Terkena Freeze";
									}
								}*/
							}

							/*if($row['CG_COMPLETENESS'] != "1"){
								$errMsgFlag = true;
								$errMsgDokumen[$row['MD_ACCT']] = "Dokumen belum lengkap";
							}*/

							$selectCIF = $this->_db->select()
								->from('M_CUSTOMER')
								->where('CUST_ID = ?', $cust_id);
							$caDataCIF = $this->_db->fetchRow($selectCIF);

							$svcAccountCIF = new Service_Account($row['MD_ACCT'], $row['MD_ACCT_CCY']);
							$resultCIF = $svcAccount->inquiryAccontInfo('AB', FALSE);
							//var_dump($resultCIF);die;
							if ($caDataCIF['CUST_CIF'] != $resultCIF['cif'] && false) {
								$errMsgFlag = true;
								$errMsgAcctStatusCore[$row['MD_ACCT']] = "CIF Rekening tidak sama dengan CIF Perusahaan";
							}





							$select1 = $this->_db->select()
								->from('M_CUSTOMER_ACCT')
								->where('ACCT_NO = ?', $row['MD_ACCT'])
								->where('CUST_ID = ?', $cust_id);

							$caData = $this->_db->fetchRow($select1);
							//echo $caData['ACCT_STATUS'];
							//var_dump($caData);die;

							if (empty($caData) && $row['MD_ACCT_TYPE'] != 'ESCROW') {
								$errMsgFlag = true;
								$errMsgAcctStatusGiro[$row['MD_ACCT']] = "Rekening Nasabah tidak terdaftar diperusahaan";
							}

							if ($caData['ACCT_TYPE'] == 'D') {

								if ($caData['ACCT_STATUS'] != '1') {

									$errMsgFlag = true;
									$errMsgAcctStatusGiro[$row['MD_ACCT']] = "Rekening Nasabah pada sistem bukan status approved";
								}

								$svcAccountGiro = new Service_Account($row['MD_ACCT'], $row['MD_ACCT_CCY']);
								$resultGiro = $svcAccountGiro->inquiryAccountBalance('AB', TRUE);
								//var_dump($result);die;
								if ($resultGiro['response_code'] == '00' || $resultGiro['response_code'] == '0000') {
									if ($resultGiro['status'] != '1') {

										$errMsgFlag = true;
										$errMsgAcctStatusCore[$row['MD_ACCT']] = "Status rekening tidak aktif";
									}



									$select2 = $this->_db->select()
										->from('M_PRODUCT_TYPE')
										->where('PRODUCT_CODE in (?)', $resultGiro['account_type']);
									$paData = $this->_db->fetchRow($select2);

									if (empty($paData)) {
										$errMsgFlag = true;
										$errMsgProductType[$row['MD_ACCT']] = "tipe produk tidak terdaftar";
									}

									if ($resultGiro['balance_active'] < $row['GUARANTEE_AMOUNT']) {
										$errMsgFlag = true;
										$errMsgSaldo[$row['MD_ACCT']] = "Saldo rekening tidak cukup";
									}
								}
							}

							if ($caData['ACCT_TYPE'] == 'T') {

								if ($caData['ACCT_STATUS'] != '1') {

									$errMsgFlag = true;
									$errMsgAcctStatusDeposito[$row['MD_ACCT']] = "Rekening Nasabah pada sistem bukan status approved";
								}

								$svcAccountDeposito = new Service_Account($row['MD_ACCT'], $row['MD_ACCT_CCY']);
								$resultDeposito = $svcAccountDeposito->inquiryDeposito('AB', TRUE);
								//var_dump($result);die;
								if ($resultDeposito['response_code'] == '00' || $resultDeposito['response_code'] == '0000') {
									if ($resultDeposito['status'] != '1') {
										$errMsgFlag = true;
										$errMsgAcctStatusDepositoCore[$row['MD_ACCT']] = "Status rekening tidak aktif";
									}

									if ($resultDeposito['aro_status'] == 'N') {
										$errMsgFlag = true;
										$errMsgAcctARO[$row['MD_ACCT']] = "Type bukan ARO";
									}

									/*if($resultDeposito['varian_rate'] != '0'){
																$errMsgFlag = true;
																$errMsgAcctRate[$row['MD_ACCT']] = "Variant Rate bukan 0";
																
															}*/

									if ($resultDeposito['hold_description'] == 'Y') {
										$errMsgFlag = true;
										$errMsgAcctHold[$row['MD_ACCT']] = "status rekening hold";
									}



									$select2 = $this->_db->select()
										->from('M_PRODUCT_TYPE')
										->where('PRODUCT_CODE in (?)', $resultDeposito['account_type']);
									$paData = $this->_db->fetchRow($select2);

									if (empty($paData)) {
										$errMsgFlag = true;
										$errMsgProductTypeDeposito[$row['MD_ACCT']] = "tipe produk tidak terdaftar";
									}

									if ($resultDeposito['balance'] < $row['GUARANTEE_AMOUNT']) {
										$errMsgFlag = true;
										$errMsgSaldoDeposito[$row['MD_ACCT']] = "Saldo rekening tidak cukup";
									}
								}
							}
						}
						//var_dump($resultData);



						//$this->view->errMsgMarginalDeposit = $errMsgMarginalDeposit;
						//$this->view->errMsgDokumen = $errMsgDokumen;

						$this->view->errMsgAcctStatusGiro = $errMsgAcctStatusGiro;
						$this->view->errMsgAcctStatusCore = $errMsgAcctStatusCore;
						$this->view->errMsgProductType = $errMsgProductType;
						$this->view->errMsgSaldo = $errMsgSaldo;


						$this->view->errMsgAcctStatusDeposito = $errMsgAcctStatusDeposito;
						$this->view->errMsgAcctStatusDepositoCore = $errMsgAcctStatusDepositoCore;
						$this->view->errMsgAcctARO = $errMsgAcctARO;
						$this->view->errMsgAcctRate = $errMsgAcctRate;
						$this->view->errMsgAcctHold = $errMsgAcctHold;
						$this->view->errMsgProductTypeDeposito = $errMsgProductTypeDeposito;
						$this->view->errMsgSaldoDeposito = $errMsgSaldoDeposito;




						//var_dump($errMsgValidasi);
						//var_dump($errMsgDokumen);die;





						// end validasi


						if ($errMsgFlag == false) {
							// escrow manual
							if ($resultData[0]['MD_ACCT_TYPE'] == 'ESCROW') {

								$svcAccount = new Service_Account($resultData[0]['MD_ACCT'], $resultData[0]['MD_ACCT_CCY']);
								$result = $svcAccount->inquiryAccontInfo('AB', FALSE);

								$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
								$result2 = $svcAccountCIF->inquiryCIFAccount();
								$filterBy = $result['account_number']; // or Finance etc.
								$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
									return ($var['account_number'] == $filterBy);
								});

								$singleArr = array();
								foreach ($new as $key => $val) {
									$singleArr = $val;
								}

								if ($singleArr['type'] == 'T') {
									$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
									$result3 = $svcAccountDeposito->inquiryDeposito();
								} else {
									$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
									$result3 = $svcAccountBalance->inquiryAccountBalance();
								}

								//var_dump($result3);die;


								if ($result3['status'] == '1' || $result3['status'] == '4') {
									$data1 = [
										'LAST_SUGGESTED'   => $resultData[0]['LAST_SUGGESTED'],
										'LAST_SUGGESTEDBY' => $resultData[0]['LAST_SUGGESTEDBY'],
										'LAST_REVIEWED'    => $resultData[0]['LAST_REVIEWED'],
										'LAST_REVIEWEDBY'  => $resultData[0]['LAST_REVIEWEDBY'],
										'LAST_APPROVED'    => new Zend_Db_Expr('now()'),
										'LAST_APPROVEDBY'  => $this->_userIdLogin,
									];

									$where1 = ['CUST_ID = ?' => $cust_id];
									$this->_db->update('M_MARGINALDEPOSIT', $data1, $where1);

									$data2 = [
										'CUST_ID'           => $cust_id,
										'MD_ACCT'           => $resultData[0]['MD_ACCT'],
										'MD_ACCT_NAME'      => $resultData[0]['MD_ACCT_NAME'],
										'MD_ACCT_TYPE'      => $resultData[0]['MD_ACCT_TYPE'],
										'MD_ACCT_CCY'       => $resultData[0]['MD_ACCT_CCY'],
										'GUARANTEE_AMOUNT'  => $resultData[0]['GUARANTEE_AMOUNT'],
									];
									$this->_db->insert('M_MARGINALDEPOSIT_DETAIL', $data2);

									$where2 = [
										'CHANGES_ID = ?' => $changes_id,
										'MD_ACCT = ?' => $resultData[0]['MD_ACCT']
									];


									$this->_db->delete('TEMP_MARGINALDEPOSIT_DETAIL', $where2);

									$data3  = [
										'CHANGES_STATUS'    => 'AP',
										'LASTUPDATED'       => new Zend_Db_Expr('now()')
									];
									$whereglob = ['CHANGES_ID = ?' => $changes_id];
									$this->_db->update('T_GLOBAL_CHANGES', $data3, $whereglob);
									$wheredelete = ['CHANGES_ID = ?' => $changes_id];
									$this->_db->delete('TEMP_MARGINALDEPOSIT', $wheredelete);

									Application_Helper_General::writeLog('AMDC', 'Approve Insurance Marginal Deposit Changes for ' . $cust_name . ' (' . $cust_id . ')');

									$this->view->is_approve = true;
								} else {

									$errMsgFlag = true;
									$errMsgAcctStatusDepositoCore[$result3['account_number']] = "Status rekening tidak aktif";
								}
							} else {

								$select = $this->_db->select()
									->from(
										array('M_MARGINALDEPOSIT_DETAIL'),
										array('MD_ACCT', 'GUARANTEE_AMOUNT')
									)
									->where('MD_ACCT_TYPE = ?', 'ESCROW')
									->where('CUST_ID = ?', $cust_id);
								$escrowData = $this->_db->fetchRow($select);

								if ($escrowData) {
									$escrowAmount = $escrowData['GUARANTEE_AMOUNT'];
								} else {
									$escrowAmount = 0;

									$app      = Zend_Registry::get('config');
									$bankCode = $app['app']['bankcode'];

									$ccy = Application_Helper_General::getCurrNum('IDR');
									//echo 'here';
									$svcAccount = new Service_Account($custCIF, 'IDR', $bankCode);
									$resultSvc = $svcAccount->createAccount("003", "J1");

								}
								// $this->dd($resultSvc);
								//var_dump($escrowAmount);die;
								//$this->_db->beginTransaction();
								try {
									$data1 = [
										'LAST_SUGGESTED'   => $resultData[0]['LAST_SUGGESTED'],
										'LAST_SUGGESTEDBY' => $resultData[0]['LAST_SUGGESTEDBY'],
										'LAST_REVIEWED'    => $resultData[0]['LAST_REVIEWED'],
										'LAST_REVIEWEDBY'  => $resultData[0]['LAST_REVIEWEDBY'],
										'LAST_APPROVED'    => new Zend_Db_Expr('now()'),
										'LAST_APPROVEDBY'  => $this->_userIdLogin,
									];
									$where1 = ['CUST_ID = ?' => $cust_id];
									$this->_db->update('M_MARGINALDEPOSIT', $data1, $where1);
									//var_dump($where1);die;
									foreach ($resultData as $row) {
										if ($row['ACCT_TYPE'] == 'T' || $row['ACCT_TYPE'] == '30') {
											$data2 = [
												'CUST_ID'           => $cust_id,
												'MD_ACCT'           => $row['MD_ACCT'],
												'MD_ACCT_NAME'      => $row['MD_ACCT_NAME'],
												'MD_ACCT_TYPE'      => $row['MD_ACCT_TYPE'],
												'MD_ACCT_CCY'       => $row['MD_ACCT_CCY'],
												'GUARANTEE_AMOUNT'  => $row['GUARANTEE_AMOUNT'],
											];
											$this->_db->insert('M_MARGINALDEPOSIT_DETAIL', $data2);
										} else {
											$escrowAmount = $escrowAmount + $row['GUARANTEE_AMOUNT'];
											$GUARANTEE_AMOUNT = $row['GUARANTEE_AMOUNT'];
										}
									}

									//var_dump($escrowData);die;
									if ($escrowData) {
										$data3  = ['GUARANTEE_AMOUNT' => $escrowAmount];
										$where3 = ['MD_ACCT = ?' => $escrowData['MD_ACCT']];
										//var_dump($data3);
										//var_dump($where3);die;
										$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $data3, $where3);

										foreach ($resultData as $row) {

											if ($row['MD_ACCT_TYPE'] == 'T') {
												$svcAccount = new Service_Account($row['MD_ACCT'], 'IDR');
												$result = $svcAccount->inquiryDeposito('AB', TRUE);
												//var_dump($result);
												if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
													//var_dump($row['MD_ACCT']);die;
													$svcAccount = new Service_Account($row['MD_ACCT'], 'IDR', null, null, null, null, $this->_userIdLogin);

													$param = $result;
													$param['balance'] = $row['GUARANTEE_AMOUNT'];
													//$param['balance'] = 
													$resultSvc = $svcAccount->lockDeposito($param);
													//var_dump($resultSvc);
													$data3 = [
														'CUST_ID'           => $cust_id,
														'MD_ACCT'           => $resultSvc['account_number'],
														'MD_ACCT_NAME'      => $resultData[0]['MD_ACCT_NAME'],
														'MD_ACCT_TYPE'      => 'Deposito',
														'MD_ACCT_CCY'       => 'IDR',
														'GUARANTEE_AMOUNT'  => $result['balance'],
													];
													//$this->_db->insert('M_MARGINALDEPOSIT_DETAIL', $data3);

													$where = [
														'CHANGES_ID = ?' => $changes_id,
														'MD_ACCT = ?' => $row['MD_ACCT']
													];
													$this->_db->delete('TEMP_MARGINALDEPOSIT_DETAIL', $where);
												}
											} else {


												$param['SOURCE_ACCOUNT_NUMBER']           = $row['MD_ACCT']; // 1003005125 1003005098
												$param['BENEFICIARY_ACCOUNT_NUMBER']      = $escrowData['MD_ACCT']; // 2003500303 2003500320

												$param['PHONE_NUMBER_FROM']                 = $row['CUST_PHONE'];
												$param['AMOUNT']                     = $GUARANTEE_AMOUNT; //$escrowAmount ;
												$param['PHONE_NUMBER_TO']                     = $row['CUST_PHONE'];
												$param['RATE1']                 = 0; // 461001186
												$param['RATE2']                 = 0;
												$param['RATE3']         = 0;
												$param['RATE4']     = 0;

												$param['SOURCE_ACCOUNT_NAME']         = $row['MD_ACCT_NAME'];
												$param['BENEFICIARY_ACCOUNT_NAME']     = $resultData[0]['MD_ACCT_NAME'];
												$param['DESCRIPTION']                     = 'transfer to escrow';
												$param['DESCRIPTION_DETAIL']                     = 'by system';
												//var_dump($param);die;
												$data =  new Service_TransferWithin($param);
												$result = $data->sendTransfer();

												if ($result['RawResult']['response_code'] == '00' || $result['RawResult']['response_code'] == '0000' || $result['RawResult']['response_code'] == '0995') {

													//update last cek saldo
													$filter = true;
													$filterParam['fCustID'] 	= $cust_id;
													$data = $model->getData($filterParam, $filter);

													if ($escrowAmount >= $data[0]['MARGINAL_DEPOSIT']) {

														$updateLastCheckMinus  = ['LAST_CHECK' => new Zend_Db_Expr('now()'), 'COUNTING_DEADLINE_TOPUP' => null, 'LAST_CHECK_MINUS' => null];
														$whereUpdateLastCheckMinus = ['CUST_ID = ?' => $cust_id];
														$this->_db->update('M_MARGINALDEPOSIT', $updateLastCheckMinus, $whereUpdateLastCheckMinus);
													}
													//

													$select = $this->_db->select()
														->from(
															array('M_MARGINALDEPOSIT_DETAIL'),
															array('MD_ACCT', 'GUARANTEE_AMOUNT')
														)
														->where('MD_ACCT_TYPE = ?', 'ESCROW')
														->where('CUST_ID = ?', $cust_id);
													$escrowDatanew = $this->_db->fetchRow($select);

													if (!empty($escrowDatanew)) {
														//echo 'here';
														$total = $escrowAmount;
														$data3  = ['GUARANTEE_AMOUNT' => $total];
														$where3 = ['MD_ACCT = ?' => $escrowData['MD_ACCT']];
														$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $data3, $where3);
														$where = [
															'CHANGES_ID = ?' => $changes_id,
															'MD_ACCT = ?' => $row['MD_ACCT']
														];
														//var_dump($where);die;
														$this->_db->delete('TEMP_MARGINALDEPOSIT_DETAIL', $where);
													} else {
														//echo 'here1';
														$data3 = [
															'CUST_ID'           => $cust_id,
															'MD_ACCT'           => $resultSvc['account_number'],
															'MD_ACCT_NAME'      => $resultData[0]['MD_ACCT_NAME'],
															'MD_ACCT_TYPE'      => 'ESCROW',
															'MD_ACCT_CCY'       => 'IDR',
															'GUARANTEE_AMOUNT'  => $escrowAmount,
														];
														$this->_db->insert('M_MARGINALDEPOSIT_DETAIL', $data3);
													}
													//die('here');

												}
											}
										}
										$select = $this->_db->select()
											->from(
												array('TEMP_MARGINALDEPOSIT_DETAIL'),
												array('*')
											)
											->where('CHANGES_ID = ?', $changes_id);
										//->where('CUST_ID = ?', $cust_id);
										$checkdata = $this->_db->fetchRow($select);
										if (empty($checkdata)) {

											$data2  = [
												'CHANGES_STATUS'    => 'AP',
												'LASTUPDATED'       => new Zend_Db_Expr('now()')
											];
											$whereglob = ['CHANGES_ID = ?' => $changes_id];
											$this->_db->update('T_GLOBAL_CHANGES', $data2, $whereglob);
											$wheredelete = ['CHANGES_ID = ?' => $changes_id];
											$this->_db->delete('TEMP_MARGINALDEPOSIT', $wheredelete);
										}
									} else {
										//var_dump($resultSvc);die;
										if ($resultSvc['response_code'] == '00' || $resultSvc['response_code'] == '0000') {

											foreach ($resultData as $row) {
												if ($row['MD_ACCT_TYPE'] != 'T') {
													$param['SOURCE_ACCOUNT_NUMBER']           = $row['MD_ACCT']; // 1003005125 1003005098
													$param['BENEFICIARY_ACCOUNT_NUMBER']      = $resultSvc['account_number']; // 2003500303 2003500320

													$param['PHONE_NUMBER_FROM']                 = $row['CUST_PHONE'];
													$param['AMOUNT']                     = $escrowAmount;
													$param['PHONE_NUMBER_TO']                     = $row['CUST_PHONE'];
													$param['RATE1']                 = 0; // 461001186
													$param['RATE2']                 = 0;
													$param['RATE3']         = 0;
													$param['RATE4']     = 0;

													$param['SOURCE_ACCOUNT_NAME']         = $row['MD_ACCT_NAME'];
													$param['BENEFICIARY_ACCOUNT_NAME']     = $resultData[0]['MD_ACCT_NAME'];
													$param['DESCRIPTION']                     = 'transfer to escrow';
													$param['DESCRIPTION_DETAIL']                     = 'by system';
													//var_dump($param);
													$data =  new Service_TransferWithin($param);
													$result = $data->sendTransfer();
													//echo 'transfer';
													//var_dump($result);die;


													if ($result['RawResult']['response_code'] == '00' || $result['RawResult']['response_code'] == '0000' || $result['RawResult']['response_code'] == '0995') {
														//die('here');

														//update last cek saldo
														$filter = true;
														$filterParam['fCustID'] 	= $cust_id;
														$data = $model->getData($filterParam, $filter);

														if ($escrowAmount >= $data[0]['MARGINAL_DEPOSIT']) {

															$updateLastCheckMinus  = ['LAST_CHECK' => new Zend_Db_Expr('now()'), 'COUNTING_DEADLINE_TOPUP' => null, 'LAST_CHECK_MINUS' => null];
															$whereUpdateLastCheckMinus = ['CUST_ID = ?' => $cust_id];
															$this->_db->update('M_MARGINALDEPOSIT', $updateLastCheckMinus, $whereUpdateLastCheckMinus);
														}
														//


														$select = $this->_db->select()
															->from(
																array('M_MARGINALDEPOSIT_DETAIL'),
																array('MD_ACCT', 'GUARANTEE_AMOUNT')
															)
															->where('MD_ACCT_TYPE = ?', 'ESCROW')
															->where('CUST_ID = ?', $cust_id);
														$escrowDatanew = $this->_db->fetchRow($select);
														//var_dump($escrowDatanew);die;
														if (!empty($escrowDatanew)) {
															//echo 'here';
															$total = $escrowAmount;
															$data3  = ['GUARANTEE_AMOUNT' => $total];
															$where3 = ['MD_ACCT = ?' => $escrowData['MD_ACCT']];
															$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $data3, $where3);
														} else {
															//echo 'here1';
															$data3 = [
																'CUST_ID'           => $cust_id,
																'MD_ACCT'           => $resultSvc['account_number'],
																'MD_ACCT_NAME'      => $resultData[0]['MD_ACCT_NAME'],
																'MD_ACCT_TYPE'      => 'ESCROW',
																'MD_ACCT_CCY'       => 'IDR',
																'GUARANTEE_AMOUNT'  => $escrowAmount,
															];
															//var_dump($resultSvc);
															//var_dump($data3);die;
															$this->_db->insert('M_MARGINALDEPOSIT_DETAIL', $data3);
														}
														//die('here');
														$where = [
															'CHANGES_ID = ?' => $changes_id,
															'MD_ACCT = ?' => $row['MD_ACCT']
														];
														//$this->_db->delete('TEMP_MARGINALDEPOSIT_DETAIL', $where);
													}
												} else {
													$svcAccount = new Service_Account($row['MD_ACCT'], 'IDR');
													$result = $svcAccount->inquiryDeposito('AB', TRUE);
													//var_dump($result);die;
													if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
														// $svcAccount = new Service_Account($custCIF, 'IDR', null, null, null, null, $this->_userIdLogin);
														$svcAccount = new Service_Account($row['MD_ACCT'], 'IDR', null, null, null, null, $this->_userIdLogin);
														$param = $result;
														//$param['balance'] = 
														// $resultDepo = $svcAccount->lockDeposito($param);
														$svcAccount->lockDeposito($param);

														$where = [
															'CHANGES_ID = ?' => $changes_id,
															'MD_ACCT = ?' => $row['MD_ACCT']
														];
														//$this->_db->delete('TEMP_MARGINALDEPOSIT_DETAIL', $where);
													}
												}


												//var_dump($result);die;

												//die('h');


												//}
											}

											$select = $this->_db->select()
												->from(
													array('TEMP_MARGINALDEPOSIT_DETAIL'),
													array('*')
												)
												->where('CHANGES_ID = ?', $changes_id);
											//->where('CUST_ID = ?', $cust_id);
											$checkdata = $this->_db->fetchRow($select);
											if (!empty($checkdata)) {
												$data2  = [
													'CHANGES_STATUS'    => 'AP',
													'LASTUPDATED'       => new Zend_Db_Expr('now()')
												];
												$whereglob = ['CHANGES_ID = ?' => $changes_id];
												$this->_db->update('T_GLOBAL_CHANGES', $data2, $whereglob);
												$wheredelete = ['CHANGES_ID = ?' => $changes_id];
												$this->_db->delete('TEMP_MARGINALDEPOSIT', $wheredelete);
												$where = ['CHANGES_ID = ?' => $changes_id];
												$this->_db->delete('TEMP_MARGINALDEPOSIT_DETAIL', $where);
											}
										}
									}
									$SoapClient = new SGO_Soap_ClientUser();
									$request = array(
										'account_number' => $resultSvc['account_number'],
										'type' => 'ESCROW'
									);

									// $returnService = $SoapClient->callapi('lockdeposito',null,$request);
									//die;

									// $where4 = ['CUST_ID = ?' => $cust_id];
									// $this->_db->delete('TEMP_MARGINALDEPOSIT', $where4);
									// $this->_db->delete('TEMP_MARGINALDEPOSIT_DETAIL', $where4);

									// $data5  = [
									//     'CHANGES_STATUS'    => 'AP',
									//     'LASTUPDATED'       => new Zend_Db_Expr('now()')
									// ];
									// $where5 = ['CHANGES_ID = ?' => $changes_id];
									// $this->_db->update('T_GLOBAL_CHANGES', $data5, $where5);

									//$this->_db->commit();

									Application_Helper_General::writeLog('AMDC', 'Approve Insurance Marginal Deposit Changes for ' . $cust_name . ' (' . $cust_id . ')');

									$this->view->is_approve = true;
								} catch (Exception $error) {
									echo '<pre>';
									var_dump($error);
									die;
									$this->_db->rollBack();
									echo '<pre>';
									print_r($error->getMessage());
									echo '</pre><br>';
									die;
								}

								// $dataLF  = [
								// 	'STATUS'    => '1' // unfreeze
								// ];
								// $whereLF = ['CUST_ID = ?' => $cust_id];
								// $this->_db->update('M_CUST_LINEFACILITY', $dataLF, $whereLF);
							}
							//

							$getAllBg = $this->_db->select()
								->from('T_BANK_GUARANTEE')
								->where('BG_INSURANCE_CODE = ?', $cust_id)
								->query()->fetchAll();

							$validCgIns = true;


							foreach ($getAllBg as $key => $bg) {
								if ($bg['CGINS_STATUS'] == '0') {
									if (new DateTime(date('Y-m-d')) > new DateTime($bg['BG_CG_DEADLINE'])) {
										$validCgIns = false;
									}
								}
							}

							if ($validCgIns) {
								$getLfIns = $this->_db->select()
									->from('M_CUST_LINEFACILITY')
									->where('CUST_ID = ?', $cust_id)
									->query()->fetch();

								if ($getLfIns['FREEZE_MANUAL'] != '1') {
									$this->_db->update('M_CUST_LINEFACILITY', [
										'STATUS' => 1,
										'ENDED_REASON' => '',
									], [
										'CUST_ID = ?' => $getLfIns['CUST_ID']
									]);
								}
							}
						}
						/*else{
							$dataLF  = [
							'STATUS'    => '4', // freeze
							'ENDED_REASON'    => 'auto freeze akibat MD / kelengkapan dokumen' // freeze
							 ];
							$whereLF = ['CUST_ID = ?' => $cust_id];
							$this->_db->update('M_CUST_LINEFACILITY', $dataLF, $whereLF);
						}*/
					} elseif ($submit_reject) {
						$this->_db->beginTransaction();
						try {
							$where = ['CHANGES_ID = ?' => $changes_id];
							$this->_db->delete('TEMP_MARGINALDEPOSIT', $where);
							$this->_db->delete('TEMP_MARGINALDEPOSIT_DETAIL', $where);

							$data2  = [
								'CHANGES_STATUS'    => 'RJ',
								'LASTUPDATED'       => new Zend_Db_Expr('now()')
							];
							$this->_db->update('T_GLOBAL_CHANGES', $data2, $where);

							$this->_db->commit();

							$this->view->is_reject = true;
						} catch (Exception $error) {
							$this->_db->rollBack();
							echo '<pre>';
							print_r($error->getMessage());
							echo '</pre><br>';
							die;
						}
					} else {
						Application_Helper_General::writeLog('VMDC', 'View Insurance Marginal Deposit Changes for ' . $cust_name . ' (' . $cust_id . ')');
					}
				} else {
					$this->_redirect('/notification/invalid/index');
				}
			}
		} else {
			$this->_redirect('/popuperror/index/index');
		}
	}
}
