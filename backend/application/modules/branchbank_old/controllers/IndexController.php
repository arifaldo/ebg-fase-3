<?php

require_once 'Zend/Controller/Action.php';

class Branchbank_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$countrycode = $this->language->_('Country Code');
		$branchname = $this->language->_('Branch Name');
		$branchcode = $this->language->_('Branch Code');
		$branchaddress = $this->language->_('Branch Address');
		$cityname = $this->language->_('City Name');
		$religionname = $this->language->_('Region Name');
		$contact = $this->language->_('Contact');
		$pn = $this->language->_('PN Setempat');
		$head = ''; //$this->language->_('Headquarter');


		$fields = array(
			'branch_code'  => array(
				'field' => 'BRANCH_CODE',
				'label' => $branchcode,
				'sortable' => true
			),
			'branch_name'  => array(
				'field' => 'BRANCH_NAME',
				'label' => $branchname,
				'sortable' => true
			),
			'head'     => array(
				'field' => 'HEADQUARTER',
				'label' => $head,
				'sortable' => true
			),
			'branch_address'     => array(
				'field' => 'BANK_ADDRESS',
				'label' => $branchaddress,
				'sortable' => true
			),

			'local_pn'     => array(
				'field' => 'LOCAL_PN',
				'label' => $pn,
				'sortable' => true
			),
		);
		$headers = array(
			'branch_code'  => array(
				'field' => 'BRANCH_CODE',
				'label' => 'Branch Code'
			),
			'branch_name'  => array(
				'field' => 'BRANCH_NAME',
				'label' => 'Branch Name'
			),
			'branch_address'     => array(
				'field' => 'BANK_ADDRESS',
				'label' => 'Branch Address'
			),
			/*'branch_city'     => array(
				'field' => 'CITY_NAME',
				'label' => 'City Name'
			),
			'branch_province'     => array(
				'field' => 'REGION_NAME',
				'label' => 'Province'
			),*/
			'branch_contact'     => array(
				'field' => 'CONTACT',
				'label' => 'Contact'
			),
			'local_pn'     => array(
				'field' => 'LOCAL_PN',
				'label' => 'PN Setempat'
			),
			'branch_coverage'     => array(
				'field' => 'BRANCH_COVERAGE_NAME',
				'label' => 'BTS Coverage'
			),
			/*'headquarter'     => array(
				'field' => 'HEADQUARTER',
				'label' => 'Headquarter'
			),*/
		);
		$filterlist = array("BRANCH_CODE", "BRANCH_NAME", "branch_address", "contact");

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'BRANCH_NAME');
		$sortDir = $this->_getParam('sortdir', 'asc');
		$filter 		= $this->_getParam('filter');
		$filter_clear = $this->_getParam('filter_clear');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		//get filtering param

		$filterArr = array(
			'filter' 	  	 => array('StringTrim', 'StripTags'),
			'BRANCH_CODE'  => array('StringTrim', 'StripTags'),
			'BRANCH_NAME'  => array('StringTrim', 'StripTags'),
			'branch_address'  => array('StringTrim', 'StripTags'),
			'contact'  => array('StringTrim', 'StripTags'),
		);

		$dataParam = array("BRANCH_CODE", "BRANCH_NAME", "branch_address", "contact");

		$dataParamValue = array();
		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);

		foreach ($dataParam as $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				$order = 0;

				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
					$order++;
				}
			}
		}

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $dataParamValue);

		$getData = Application_Helper_Array::SimpleArray($fields, 'field');

		$select = $this->_db->select()
			->from(array('A' => 'M_BRANCH'), array(
				'ID',
				'BRANCH_CODE',
				'BRANCH_NAME',
				'BANK_ADDRESS',
				'CITY_NAME',
				'REGION_NAME',
				'CONTACT',
				'HEADQUARTER',
				"LOCAL_PN"
			))->order('HEADQUARTER DESC');

		$sqlBranchCoverage =   $this->_db->select()
			->from(array('B' => 'M_BRANCH_COVERAGE'), array('B.BRANCH_CODE'))
			->group(array('B.BRANCH_CODE'));
		$resultdata  = $this->_db->fetchAll($sqlBranchCoverage);
		$this->view->m_branch_coverage = $resultdata;


		$fcountry_name = $zf_filter->getEscaped('BRANCH_NAME');
		$fcountry_code = $zf_filter->getEscaped('BRANCH_CODE');
		$fregion_name = $zf_filter->getEscaped('REGION');
		$branch_address = $zf_filter->getEscaped('branch_address');
		$contact = $zf_filter->getEscaped('contact');
		$fcity_name = $zf_filter->getEscaped('CITY_NAME');


		if ($fcountry_code) $select->where('UPPER(BRANCH_CODE) LIKE ' . $this->_db->quote('%' . strtoupper($fcountry_code) . '%'));
		if ($fcountry_name) $select->where('UPPER(BRANCH_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fcountry_name) . '%'));
		if ($branch_address) $select->where('UPPER(BANK_ADDRESS) LIKE ' . $this->_db->quote('%' . strtoupper($branch_address) . '%'));
		if ($contact) $select->where('UPPER(CONTACT) LIKE ' . $this->_db->quote('%' . strtoupper($contact) . '%'));



		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf)
		if ($csv || $pdf || $this->_request->getParam('print')) {
			$header = Application_Helper_Array::simpleArray($headers, 'label');



			$this->view->branch_name = $fcountry_name;
			$this->view->city_name = $fregion_name;
			$this->view->region_name = $fcity_name;

			if ($fheadquarter == 'Y') {
				$this->view->yhead = 'selected';
			} else if ($fheadquarter == 'N') {
				$this->view->yhead = 'selected';
			} else {
				$this->view->nn = 'selected';
			}
		}


		//$this->view->success = true;
		$select->order($sortBy . ' ' . $sortDir);
		$this->paging($select);


		$this->view->fields = $fields;
		$this->view->filter = $filter;


		if (!empty($dataParamValue)) {
			$filterlistdata = array("Filter");
			foreach ($dataParamValue as $fil => $val) {
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
			}
		} else {
			$filterlistdata = array("Filter - None");
		}

		$this->view->data_filter = $filterlistdata;


		$tempExport = [];


		if ($csv || $pdf || $this->_request->getParam('print')) {
			$select = $this->_db->fetchall($select);

			foreach ($select as $key => $value) {

				unset($select[$key]['ID']);

				$subData = [];
				$subData['BRANCH_CODE'] = "'" . $value['BRANCH_CODE'];
				$subData['BRANCH_NAME'] = $value['BRANCH_NAME'];
				$subData['BANK_ADDRESS'] = str_replace("&", "dan", $value['BANK_ADDRESS']);
				$subData['BANK_ADDRESS'] = str_replace("amp;", "", $value['BANK_ADDRESS']);
				//$subData['CITY_NAME'] = $value['CITY_NAME'];
				//$subData['REGION_NAME'] = $value['REGION_NAME'];
				$subData['CONTACT'] = "'" . $value['CONTACT'];
				$subData['LOCAL_PN'] = $value['LOCAL_PN'];
				// GET BRANCH_COVERAGE_NAME
				$resultdataCoverage = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_BRANCH_COVERAGE'))
						->where("BRANCH_CODE=?", $value['BRANCH_CODE'])
				);
				$BRANCH_COVERAGE_NAME = "";

				foreach ($resultdataCoverage as $key => $value) {

					$BRANCH_COVERAGE_NAME .= $value['BRANCH_COVERAGE_NAME'] . ", ";
				}

				$branch_code_coverage  =  rtrim($BRANCH_COVERAGE_NAME, ", ");



				//


				$subData['BRANCH_COVERAGE_NAME'] = $branch_code_coverage;
				//$subData['HEADQUARTER'] = $value['HEADQUARTER'];

				$tempExport[] = $subData;
			}
		}
		//die;
		//         print_r($select);die;
		//--------konfigurasicsv dan pdf---------
		if ($csv) {
			$this->_helper->download->csv($header, $tempExport, null, 'Branch Bank List');
			Application_Helper_General::writeLog('COLS', 'Download CSV Branch Bank List');
		} else if ($pdf) {

			$this->_helper->download->pdf($header, $select, null, 'Branch Bank List');
			Application_Helper_General::writeLog('COLS', 'Download PDF Branch Bank List');
		} else if ($this->_request->getParam('print')) {

			unset($fields['action']);
			$filterlistdatax = $this->_request->getParam('data_filter');
			$this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Branch Bank List', 'data_header' => $headers, 'data_filter' => $filterlistdatax));
		}
		//-------END konfigurasicsv dan pdf------------

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}

			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
		Application_Helper_General::writeLog('VLBB', 'View Branch Bank List');
	}
}
