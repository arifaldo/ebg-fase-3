<?php
require_once 'Zend/Controller/Action.php';

class viewapi_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('layout_api');
	}
	
}
