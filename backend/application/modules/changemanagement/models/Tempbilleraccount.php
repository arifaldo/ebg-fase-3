<?php
/**
 * Tempcustomer model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempbilleraccount extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'CST';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) 
	{
		//query from T_GLOBAL_CHANGES
		$globalChanges = $this->dbObj->select()
						  			->from('T_GLOBAL_CHANGES')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
									
		//query from TEMP_PROVIDER_ACCT
		$biller = $this->dbObj->select()
						  			->from('TEMP_PROVIDER_ACCT')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($biller))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
	
	    //==========================================================================================================
		//insert to master table M_PROVIDER_ACCT
		$insertArr = array_diff_key($biller,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		$insertArr['ACCT_CREATED'] = new Zend_Db_Expr('GETDATE()');
		$insertArr['ACCT_CREATEDBY']   = $actor;
		$insertArr['ACCT_UPDATED'] = new Zend_Db_Expr('GETDATE()');
		$insertArr['ACCT_UPDATEDBY']   = $actor;
		
		$billerInsert = $this->dbObj->insert('M_PROVIDER_ACCT',$insertArr);
		if(!(boolean)$billerInsert) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}
				
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) // BILLER ACCOUNT no Editing Flaming( 15/7/2013 )
	{
		return true;
	}
	
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_PROVIDER_ACCT
		$biller = $this->dbObj->select()
						   ->from('TEMP_PROVIDER_ACCT')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($biller))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
        	
		$cek = $this->dbObj->fetchOne(
						$this->dbObj->select()
							->from(array('B' => 'M_PROVIDER_ACCT'),array('B.ACCT_NO'))
							->where("B.ACCT_NO = ?", $biller['ACCT_NO'])
					);
				
		if($cek)
		{
			//update master table M_PROVIDER_ACCT
			$updateArr = array(
												'ACCT_SUGGESTED' => $biller['ACCT_SUGGESTED'],
												'ACCT_SUGGESTEDBY' => $biller['ACCT_SUGGESTEDBY'],
												'ACCT_STATUS' => $biller['ACCT_STATUS'],
												'ACCT_UPDATED' => new Zend_Db_Expr('GETDATE()'),
												'ACCT_UPDATEDBY' => $actor
											);
			$whereArr = array(
							'ACCT_NO = ?'     => $biller['ACCT_NO'],
						);

			$query = $this->dbObj->update('M_PROVIDER_ACCT',$updateArr,$whereArr);
		}
		
		if(!(boolean)$query) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}

	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_PROVIDER_ACCT
		$biller = $this->dbObj->select()
						   ->from('TEMP_PROVIDER_ACCT')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($biller))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
        	
		$cek = $this->dbObj->fetchOne(
						$this->dbObj->select()
							->from(array('B' => 'M_PROVIDER_ACCT'),array('B.ACCT_NO'))
							->where("B.ACCT_NO = ?", $biller['ACCT_NO'])
					);
				
		if($cek)
		{
			//update master table M_PROVIDER_ACCT
			$updateArr = array(
												'ACCT_SUGGESTED' => $biller['ACCT_SUGGESTED'],
												'ACCT_SUGGESTEDBY' => $biller['ACCT_SUGGESTEDBY'],
												'ACCT_STATUS' => $biller['ACCT_STATUS'],
												'ACCT_UPDATED' => new Zend_Db_Expr('GETDATE()'),
												'ACCT_UPDATEDBY' => $actor
											);
			$whereArr = array(
							'ACCT_NO = ?'     => $biller['ACCT_NO'],
						);

			$query = $this->dbObj->update('M_PROVIDER_ACCT',$updateArr,$whereArr);
		}
		
		if(!(boolean)$query) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveDelete($actor = null) 
	{
		//query from TEMP_PROVIDER_ACCT
		$temp = $this->dbObj->select()
						   		  ->from('TEMP_PROVIDER_ACCT')
						  		  ->where('CHANGES_ID = ?',$this->_changeId)
						  	      ->query()
						  	      ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($temp))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
						  
		$updateArr = array('ACCT_STATUS' => 3,'ACCT_UPDATED' => new Zend_Db_Expr('GETDATE()'),
												'ACCT_UPDATEDBY' => $actor);
		
		//$updateArr['ACCT_UPDATED'] = new Zend_Db_Expr('GETDATE()');
		$whereArr = array(
		                  'ACCT_NO  = ?'     => (string)$temp['ACCT_NO']);
		
		$customerupdate = $this->dbObj->update('M_PROVIDER_ACCT',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}

		
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			    return false;
		
		return true;
	}
	
	
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() 
	{
		//delete from TEMP_PROVIDER_ACCT
		$acctdelete = $this->dbObj->delete('TEMP_PROVIDER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_PROVIDER_ACCT
		$acctdelete = $this->dbObj->delete('TEMP_PROVIDER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_PROVIDER_ACCT
		$customerdelete = $this->dbObj->delete('TEMP_PROVIDER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_PROVIDER_ACCT
		$customerdelete = $this->dbObj->delete('TEMP_PROVIDER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	public function deleteDelete() 
	{
		$customerdelete = $this->dbObj->delete('TEMP_PROVIDER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}

		
    public function approveDeactivate(){

	}

	public function approveActivate(){

	}
	
    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}
	
	
	
}