<?php

Class binsetup_Model_Binsetup extends Application_Main
{
    protected $_custData; 
    protected $_userData;  
    protected $_accData;
    protected $_limitData;

	// constructor
    public function initModel()
    { 
      	$this->_custData = array('CUST_ID'              => null,
					             'CUST_CIF'             => null,
					             'CUST_NAME'            => null,
					             'CUST_TYPE'            => null,
					             'CUST_ADDRESS'         => null,
					             'CUST_CITY'            => null,
      							 'CUST_ZIP'             => null,
					             'CUST_PROVINCE'        => null,
                                 'CUST_CONTACT'         => null,
					             'CUST_PHONE'           => null,
      							 'COUNTRY_CODE'         => null,
					             'CUST_EXT'             => null,
					             'CUST_FAX'             => null,
					             'CUST_EMAIL'           => null,
					             'CUST_WEBSITE'         => null,
      	                         'CUST_CHARGES_STATUS'  => null,
      	                         'CUST_MONTHLYFEE_STATUS'   => null,
      	                         'CUST_TOKEN_AUTH'   	=> null,
      	
					             // 'CUST_EMOBILE'         => null,
					             // 'CUST_ISEMOBILE'       => null,
                            );
                            
           $this->_userData = array('CUST_ID'        => null,
  	                         		'USER_ID'        => null,
		                     		'USER_NAME'      => null,
                             		'USER_EMAIL'     => null,
                             		'USER_PHONE'     => null,
                             		'USER_MOBILENO'  => null,
                             		'USER_DIVISION'  => null,
                             		'USER_STATUS'    => null,
  	                         		'FGROUP_ID'      => null,
  	                         		'USER_HASTOKEN'  => null
                            		);
                            		
         	$this->_accData = array('CUST_ID'        => null,
  	                         		'ACCT_NO'        => null,
		                     		'CCY_ID'         => null,
                             		'ACCT_NAME'      => null,
                             		'ACCT_GROUP'     => null,
                             		'ACCT_TYPE'      => null,
                             		'ACCT_LIMIT'     => null,
                             		'BANK_ID'        => null,
  	                         		'COUNTRY_CODE'   => null,
  	                         		'BANK_NAME'  	 => null,
  	                         		'BRANCH_CODE'  	 => null,
  	                         		'ACCT_DESC'  	 => null
                            		);
                            		
          $this->_limitData = array('CUST_ID'        	=> null,
  	                         		'DR_DAILY_LIMIT'   	=> null,
		                     		'CR_DAILY_LIMIT'    => null,
                             		'AMT_BLOCK_LIMIT'   => null,
                             		'CCY_ID'     		=> null
                            		);                  		
    }
	
    public function getAllCustomer()
    { 
       $select = $this->_db->select()
                           ->from(array('a' => 'M_CUSTOMER_BIN'),array('CUST_ID'))
                            ->order(array('CUST_ID ASC'))
                           ->query()->fetchAll();
	   return $select;
    }
    
    
    public function getCustomer($cust_id,$cust_bin)
    { 
//       $select = $this->_db->select()
//                           ->from(array('a'=> 'M_CUSTOMER_BIN'))
//                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
//                           ->query()->fetch();
                           
	   $select = $this->_db->select()
  	                         ->from(array('A' => 'M_CUSTOMER_BIN'),array('*'))
//							 ->joinLeft(array('B' => 'M_CUSTOMER'),'A.CUST_ID = B.CUST_ID AND A.CUST_ID = B.CUST_ID',array('*'));
							 ->joinLeft(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('CUST_NAME'))
                           	 ->where('UPPER(A.CUST_ID)='.$this->_db->quote((string)$cust_id))
                           	 ->where('UPPER(A.CUST_BIN)='.$this->_db->quote((string)$cust_bin))
                             // echo $select;die;
							 ->query()->fetch();
							 
	   return $select;
    }
    
    public function getCountry()
    { 
       $select = $this->_db->select()
                           ->from('M_COUNTRY')
                           ->query()->fetchAll();
	   return $select;
    }
    
	
	public function getCustomerAcct($cust_id)
    { 
        $select = $this->_db->select()
	 				         ->from(array('CA'=>'M_CUSTOMER_ACCT'),array('ACCT_NO','CCY_ID','ACCT_NAME','ACCT_DESC','ACCT_GROUP','ACCT_EMAIL','ORDER_NO','ACCT_STATUS'))
		  				     ->joinleft(array('g'=>'M_GROUPING'),'g.GROUP_ID=CA.GROUP_ID',array('GROUP_NAME'))
		  				     ->where('UPPER(CA.CUST_ID)='.$this->_db->quote((string)$cust_id))
		  				     //->where('ACCT_STATUS!=3')
		  				     ->order(array('ORDER_NO ASC'))
		  				     ->query()->fetchAll();
		  				     
		return $select;  				     
    }
    
    public function getUserAcct($cust_id)
    { 
       $select = $this->_db->select()
	 				         ->from(array('u'=>'M_USER'),array('USER_ID',
																				'USER_FULLNAME',
																				'USER_STATUS',
																				'USER_EMAIL', /*+ ADDED BY SUHANDI +*/
																				'USER_PHONE',/*+ ADDED BY SUHANDI +*/
																				'USER_ISLOCKED',
																				'USER_ISEMAIL',
																				'USER_CREATED',/*+ ADDED BY SUHANDI +*/
																				'USER_UPDATED',
																				'USER_UPDATEDBY',
																				'USER_SUGGESTED',
																				'USER_SUGGESTEDBY'))
		  				     //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
		  				     ->where('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
		  				     //->where('USER_STATUS!=3');
		  				     ->query()->fetchAll();
		  				    
       return $select;  				     
    }
    
    public function getUserLimit($cust_id)
    {
       $select = $this->_db->select()
  	                         ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','MAXLIMIT','MAKERLIMIT_STATUS'))
							 ->joinLeft(array('B' => 'M_USER'),'A.USER_LOGIN = B.USER_ID AND A.CUST_ID = B.CUST_ID',array('USER_FULLNAME'))
							 ->joinLeft(array('C' => 'M_CUSTOMER_ACCT'),'A.ACCT_NO = C.ACCT_NO',array('ACCT_NO','CCY_ID','ACCT_NAME')) // ADDED BY SUHANDI
							 ->where('A.MAXLIMIT > 0')	
	 				         ->where('A.MAKERLIMIT_STATUS <> 3')																													  
	 				         //->where('C.ACCT_STATUS <> 3')																													  
  	                         ->where('UPPER(B.CUST_ID)='.$this->_db->quote((string)$cust_id))
  	                         ->where('UPPER(A.CUST_ID)='.$this->_db->quote((string)$cust_id))
  	                         ->query()->fetchAll();
							// Zend_Debug::dump($select->__toString());die;
  	   return $select;  	                       
    }
    
    public function getUserDailyLimit($cust_id)
    {
       $select = $this->_db->select()
                             ->from(array('d'=>'M_DAILYLIMIT'))
		  				     ->join(array('u'=>'M_USER'),'d.USER_LOGIN=u.USER_ID and d.CUST_ID=u.CUST_ID',array('USER_FULLNAME'))
		  				     ->where('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
		  				     ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
		  				     //->where('DAILYLIMIT_STATUS!=3')
		  				     ->query()->fetchAll();

		  				     //Zend_Debug::dump($select);die;
       	return $select; 	  				    
    }
    
    public function getBenefBankAcc($cust_id)
    { 
       $select = $this->_db->select()
                           ->from('M_BENEFICIARY',array('BENEFICIARY_ACCOUNT','CURR_CODE','BENEFICIARY_NAME','BENEFICIARY_CREATED','BENEFICIARY_UPDATED'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->order(array('BENEFICIARY_ACCOUNT ASC'))
                           ->query()->fetchAll();
       return $select;
    }
    
    public function getBenefUser($cust_id)
    { 
       $select = $this->_db->select()
                           ->from(array('bu'=>'M_BENEFICIARY_USER'),array('USER_ID'))
                           ->join(array('b'=>'M_BENEFICIARY'),'bu.BENEFICIARY_ID=b.BENEFICIARY_ID',array('BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','CURR_CODE'))
                           ->join(array('u'=>'M_USER'),'bu.USER_ID=u.USER_ID',array('USER_FULLNAME'))
                           ->where('UPPER(bu.CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->order(array('b.BENEFICIARY_ACCOUNT ASC'))
                           ->query()->fetchAll();
       return $select;
    }
    
    
    
    public function getAppGroup($cust_id)
    {
       $select = $this->_db->select()
                           ->from('M_APP_GROUP_USER')
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->query()->fetchAll();
       return $select;
    }
    
    public function getAppBoundary($cust_id)
    {
       $select = $this->_db->select()
                           ->from(array('MAB'=>'M_APP_BOUNDARY'),array('*'))
						   ->joinleft(array('MABG'=>'M_APP_BOUNDARY_GROUP'),'MABG.BOUNDARY_ID=MAB.BOUNDARY_ID',array('*')) 	/*+ ADDED BY SUHANDI +*/	
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('BOUNDARY_ISUSED=1')
                           ->order('BOUNDARY_MIN ASC')
                           ->query()->fetchAll();
       return $select;
    }
    
    
    
    public function getCustomerActIn($cust_id,$cust_bin,$status=null)
    {
       /*$select = $this->_db->select()
                           ->from('M_CUSTOMER')
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('CUST_STATUS='.$status)
                           ->query()->fetch();*/
    
       $select = $this->_db->select()
                           ->from('M_CUSTOMER_BIN')
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('UPPER(CUST_BIN)='.$this->_db->quote((string)$cust_bin));
                           
       if($status != null) $select->where('CUST_BIN_STATUS='.$this->_db->quote($status));
     
       return $select->query()->fetch();                       
    } 
    
    public function getTempCustomerId($cust_id,$cust_bin,$cust_bin_tmp = TRUE)
    {
    	if($cust_bin_tmp == TRUE){
	       $select = $this->_db->select()
	                           ->from('TEMP_CUSTOMER_BIN',array('*'))
	                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
	                           ->where('UPPER(CUST_BIN)='.$this->_db->quote((string)$cust_bin))
	                           ->query()->fetch();
    	}
    	else{
    	   $select = $this->_db->select()
	                           ->from('TEMP_CUSTOMER_BIN',array('*'))
	                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
	                           ->where('UPPER(CUST_BIN_TMP)='.$this->_db->quote((string)$cust_bin))
	                           ->query()->fetch();
    	}
       return $select;                        
    }
    
    public function getMemberId($cust_id)
    { 
       $select = $this->_db->select()
      	                   ->from('M_MEMBER',array('MEMBER_ID'))
      	                   ->where('UPPER(MEMBER_CUST)='.$this->_db->quote((string)$cust_id))
      	                   ->where('UPPER(MEMBER_STATUS)='.$this->_db->quote($this->_masterStatus['code']['active']))
      	                   ->query()->fetchAll();
      return $select;                        
    }
    
    public function getMemberId2($cust_id)
    {
        $select = $this->_db->select()
      	                       ->from(array('a'=>'M_ANCHOR'),array())
      	                       ->join(array('s'=>'M_SCHEME'),'a.ANCHOR_ID=s.ANCHOR_ID',array())
      	                       ->join(array('c'=>'M_COMMUNITY'),'c.SCHEME_ID=s.SCHEME_ID',array())
      	                       ->join(array('m'=>'M_MEMBER'),'m.COMM_ID=c.COMM_ID',array('MEMBER_ID'))
      	                       ->where('UPPER(a.ANCHOR_CUST)='.$this->_db->quote((string)$cust_id))
      	                       ->where('UPPER(m.MEMBER_STATUS)='.$this->_db->quote($this->_masterStatus['code']['active']))
      	                       ->query()->fetchAll();
       return $select; 	                       
    }
    
    public function insertTempCustomer($change_id,$cust_data) 
    {
    
     $content = array('CHANGES_ID'              => $change_id,
					 'CUST_ID'                  => $cust_data['CUST_ID'],
					 'CUST_BIN'               	=> $cust_data['CUST_BIN'],
					 'CUST_BIN_TMP'             => $cust_data['CUST_BIN_TMP'],
                     'CUST_BIN_STATUS'          => $cust_data['CUST_BIN_STATUS'],

                     'BIN_CREATED'             => $cust_data['BIN_CREATED'],
                     'BIN_CREATEDBY'           => $cust_data['BIN_CREATEDBY'],
                     'BIN_UPDATED'             => $cust_data['BIN_UPDATED'],
                     'BIN_UPDATEDBY'           => $cust_data['BIN_UPDATEDBY'],
     
                     'BIN_SUGGESTED'    		=> $cust_data['BIN_SUGGESTED'],
                     'BIN_SUGGESTEDBY'  		=> $cust_data['BIN_SUGGESTEDBY'],


                     'BANK_NAME'        => $cust_data['BANK_NAME'],
                     'BRANCH_NAME'      => $cust_data['BRANCH_NAME'],
                     'ACCT_NO'        => $cust_data['ACCT_NO'],
                     'ACCT_NAME'      => $cust_data['ACCT_NAME'],
                     'ACCT_ALIAS_NAME'        => $cust_data['ACCT_ALIAS_NAME'],
                     'CCY'      => $cust_data['CCY'],
                     'APPROVED_DATE'        => $cust_data['APPROVED_DATE'],
                     'BIN_NAME'      => $cust_data['BIN_NAME'],
                     );

      
                     
        $this->_db->insert('TEMP_CUSTOMER_BIN',$content);   
     }
     
     
     protected function updateTempCustomer($changes_id,$cust_data)
     {
       
        $content = array(
					// 'CUST_ID'                  => $cust_data['CUST_ID'],
					 'CUST_BIN'                 => $cust_data['BIN'],
                     'CUST_BIN_STATUS'          => $cust_data['CUST_BIN_STATUS'],
                     );
     
         //Zend_Debug::dump($cust_data);          die;  
     
        $whereArr = array('CHANGES_ID = ?'=>$changes_id);
		$customerupdate = $this->_db->update('TEMP_CUSTOMER_BIN',$content,$whereArr);
		
		
     }
     
     
    protected function setUserData($prefix,$data,$change_id)
    {
  	   if(count($prefix))
  	   {
  	      $group = $this->getGroupUser();
  	      foreach($prefix as $a)
  	      {
   	      	$user_data = $this->_userData;
   	    	$user_data['CUST_ID'] = $data->cust_id;
   	    	$user_data['FGROUP_ID'] = $group;
   	    	$user_data['USER_STATUS'] = $this->_masterStatus['code']['active'];
   	    	$user_data['USER_HASTOKEN'] = $this->_masterhasStatus['code']['no'];
        	$id = 'user_id_'.$a; $user_data['USER_ID'] = $data->$id;
        	$name = 'user_name_'.$a; $user_data['USER_NAME'] = $data->$name;
        	$email = 'user_email_'.$a; $user_data['USER_EMAIL'] = $data->$email;
        	$phone = 'user_phone_'.$a; $user_data['USER_PHONE'] = $data->$phone;
        	$mobile = 'user_mobileno_'.$a; $user_data['USER_MOBILENO'] = $data->$mobile;
       	 	$division = 'user_division_'.$a; $user_data['USER_DIVISION'] = $data->$division;
        	$this->insertTempUser($change_id,$user_data);
  	      }	
  	   }
    }
    
    
    protected function getGroupUser()
    {
  	   $select = $this->_db->select()
  	                       ->from('M_FGROUP',array('FGROUP_ID'))
  	                       ->where('UPPER(CUST_ID)='.$this->_db->quote('BANK'))
  	                       ->where('UPPER(FGROUP_NAME)='.$this->_db->quote('SYSADMIN'));
       return $this->_db->fetchOne($select);
    }
    
    protected function insertTempUser($change_id,$user_data) 
    {
    	$content = array('CHANGES_ID'     => $change_id,
					 'CUST_ID'        => $user_data['CUST_ID'],
  	                 'USER_ID'        => $user_data['USER_ID'],
		             'USER_NAME'      => $user_data['USER_NAME'],
                     'USER_EMAIL'     => $user_data['USER_EMAIL'],
                     'USER_PHONE'     => $user_data['USER_PHONE'],
                     'USER_MOBILENO'  => $user_data['USER_MOBILENO'],
                     'USER_DIVISION'  => $user_data['USER_DIVISION'],
                     'USER_STATUS'    => $user_data['USER_STATUS'],
  	                 'FGROUP_ID'      => $user_data['FGROUP_ID'],
  	                 'USER_HASTOKEN'  => $user_data['USER_HASTOKEN']
					);
       if(isset($user_data['TOKEN_SERIALNO']))$content = array_merge($content,array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
	   $this->_db->insert('TEMP_USER',$content);
    }
    
    
	protected function insertTempAcc($countacc,$change_id,$acc_data) 
    {
    	if($countacc)
    	{
      		foreach($acc_data as $row)
      		{
				for($i=0;$i<5;$i++)
				{
					$content = array('CHANGES_ID'     => $change_id,
									 'CUST_ID'        => $row[$i]['CUST_ID'],
									 'ACCT_NO'        => $row[$i]['ACCT_NO'],
									 'CCY_ID'      	  => $row[$i]['CCY_ID'],
									 'ACCT_NAME'      => $row[$i]['ACCT_NAME'],
									 'ACCT_GROUP'     => $row[$i]['ACCT_GROUP'],
									 'ACCT_TYPE'      => $row[$i]['ACCT_TYPE'],
									 'ACCT_LIMIT'     => $row[$i]['ACCT_LIMIT'],
									 'BANK_ID'        => $row[$i]['BANK_ID'],
									 'COUNTRY_CODE'   => $row[$i]['COUNTRY_CODE'],
									 'BANK_NAME'      => $row[$i]['BANK_NAME'],
									 'BRANCH_CODE'    => $row[$i]['BRANCH_CODE'],
									 'ACCT_DESC'      => $row[$i]['ACCT_DESC']
									);
					$this->_db->insert('TEMP_CUSTOMER_ACCT',$content);
				}
      		}
    	}
    }
    
	protected function insertTempLimit($change_id,$limit_data) 
    {
		
		if(count($limit_data))
    	{
      		foreach($limit_data as $row)
      		{
				$content = array('CHANGES_ID'     	=> $change_id,
								 'CUST_ID'        	=> $row['CUST_ID'],
								 'DR_DAILY_LIMIT'   => $row['DR_DAILY_LIMIT'],
								 'CR_DAILY_LIMIT'   => $row['CR_DAILY_LIMIT'],
								 'AMT_BLOCK_LIMIT'  => $row['AMT_BLOCK_LIMIT'],
								 'CCY_ID'      	  	=> $row['CCY_ID']
							);
			   $this->_db->insert('TEMP_CUSTOMER_LIMIT',$content);
			}
		}
    }
    
    protected function getTempCustomer($changeId) 
    {
       $select  = $this->_db->fetchRow(
	  				         $this->_db->select()
	  				                   ->from(array('T' => 'TEMP_CUSTOMER_BIN'))
	  				                   ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
	  				                   ->where('T.CHANGES_ID = ?', $changeId)
	  			                     );
	  			                     
	  	return $select;
     }
     
  
}




