<?php
require_once 'Zend/Controller/Action.php';

class notification_PopsuccessController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	
	public function initController(){
			  $this->_helper->layout()->setLayout('popup');
	}
	

	public function indexAction()
	{
		$ns = new Zend_Session_Namespace('FVC');
	    $ns->backURL ='';
	    
	    $this->view->cust_id = strip_tags( trim($this->_getParam('cust_id')) );
	}
}
