<?php

require_once 'Zend/Controller/Action.php';

class Corporatedcard_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');  
	
	    $filters = array('regid' => array('StringTrim', 'StripTags'));
							 
		$validators =  array(
					    'regid' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'T_CUST_DEBIT', 'field' => 'REG_NUMBER')),
							       	  'messages' => array(
							   						  'Cannot be empty',
							   					      'Country code is not found',
							   						     ) 
							             )
					        );
		
		$selectbank = $this->_db->select()
							->from('M_BANK_TABLE', array('value' => 'BANK_NAME','BANK_CODE'));
					        //->from(array('A' => 'M_BANK_TABLE'));		
		
        $tempBank = $this->_db->fetchAll($selectbank);

        $this->view->bankarr = json_encode($tempBank);
			
		if(array_key_exists('regid',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
				    //$this->_db->beginTransaction();
				    
					$id  = $zf_filter_input->getEscaped('regid'); 
					$this->view->regid = $id;
					$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							//->where('A.CUST_ID = ?',$cust_id)
							->where('A.REG_NUMBER = ?',$id);
							
					$data = $this->_db->fetchRow($select);			
					
					$datainq =  new Service_Account($data['VA_NUMBER'],'000');
					$dataInquiry = $datainq->miniaccountInquiry('AB',FALSE); 
					$balance = 0;
					if($dataInquiry['ResponseCode'] == '00'){
						$balance = Application_Helper_General::displayMoneyplain($dataInquiry['Balance']);
						$insertData = array(
									'DATE'		=> new Zend_Db_Expr('now()'),
									'BALANCE' => $dataInquiry['Balance'],
									'ACCT_NO' => $data['VA_NUMBER'],
									'CUST_ID'	=> $data['CUST_ID']
								);
						$this->_db->insert('T_DEBIT_BALANCE', $insertData);
						$lastupdate =  date('d-M-Y H:i:s');
					}else{
						
						$select = $this->_db->select()
										->from(array('A' => 'T_DEBIT_BALANCE'),array('*'))
										->where('A.CUST_ID = ?',$data['CUST_ID']);
						$databalance = $this->_db->fetchRow($select);					
						$balance = Application_Helper_General::displayMoneyplain($databalance['BALANCE']);
						$lastupdate = $databalance['DATE'];
					}

					$this->view->balance = 'IDR '.$balance;
					$this->view->lastupdate = $lastupdate;
								
					if($this->_request->isPost())
					{
						$filters = array(
										'regid' => array('StringTrim', 'StripTags'),
										'bank_code' => array('StringTrim', 'StripTags'),
										'account' => array('StringTrim', 'StripTags'),
										'email' => array('StringTrim', 'StripTags')
						);
							 
						$validators =  array(
										'regid' => array(
													  'NotEmpty', 
													  array('Db_RecordExists', array('table' => 'T_CUST_DEBIT', 'field' => 'REG_NUMBER')),
													  'messages' => array(
																	  'Cannot be empty',
																	  'Reg Number is not found',
																		 ) 
														 ),
										'bank_code' => array(
													  'NotEmpty', 
													  'messages' => array(
																	  'Cannot be empty',
																		 ) 
														 ),
										'account' => array(
													  'NotEmpty', 
													  'messages' => array(
																	  'Cannot be empty',
																		 ) 
														 ),
										'email' => array(
														'NotEmpty',
														//new SGO_Validate_EmailAddress(),
														array('StringLength',array('min'=>1,'max'=>128)),
														'messages' => array(
														$this->language->_('Can not be empty'),
														$this->language->_('Email lenght cannot be more than 128'),
														//$this->language->_('Invalid email format'),
														)
													),		
											);
						
						$params = $this->_request->getParams();
						
						$zf_filter_input = new Zend_Filter_Input($filters, $validators,$params, $this->_optionsValidator);
			
						if($zf_filter_input->isValid()) 
						{
							
							$num_str = sprintf("%012d", mt_rand(1, 999999999999));
							$content = $data;
							$content['WITHDRAW_BANKCODE'] = $zf_filter_input->bank_code;
							$content['WITHDRAW_ACCOUNT'] = $zf_filter_input->account;
							$content['WITHDRAW_EMAIL'] = $zf_filter_input->email;
							$content['DEBIT_SUGGESTED'] = new Zend_Db_Expr('now()');
							$content['DEBIT_SUGGESTEDBY'] = $this->_userIdLogin;
							$content['DEBIT_STATUS'] = '2';
							
							unset($content['CUST_NAME']);			   
							try 
							{
								//echo '<pre>';
			 					//print_r($content);die;
								//-------- insert --------------
								$this->_db->beginTransaction(); 
								$change_id = $this->suggestionWaitingApproval('Corporate Debit Card',$info,$this->_changeType['code']['delete'],null,'T_CUST_DEBIT','TEMP_CUST_DEBIT',$data['CUST_ID'],$data['CUST_NAME'],$data['CUST_ID'],$data['CUST_NAME']);
								$content['CHANGES_ID'] = $change_id;
								$this->_db->insert('TEMP_CUST_DEBIT', $content);
			// 					die;
								$this->_db->commit();
								//$id = $this->_db->lastInsertId();
								Application_Helper_General::writeLog('COAD','Delete Corporate Debit Card. Corporate : ['.$data['CUST_NAME'].']');
								$this->view->success = true;
								$this->view->report_msg = array();
								 $this->setbackURL('/'.$this->_request->getModuleName().'/index');
								 $this->_redirect('/notification/submited/index');
							}
							catch(Exception $e) 
							{
								//rollback changes
								//var_dump($e);die;
								$this->_db->rollBack();
								
								foreach(array_keys($filters) as $field)
									$this->view->$field = $zf_filter_input->getEscaped($field);

								$errorMsg = 'exeption';
								$this->_helper->getHelper('FlashMessenger')->addMessage('F');
								$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
							}
							
							
						}else
						{
							//die;
							$this->view->error = true;
							foreach(array_keys($filters) as $field)
									$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);


							
							$error = $zf_filter_input->getMessages();
							//var_dump($error);die;
							//format error utk ditampilkan di view html 
							$errorArray = null;
							foreach($error as $keyRoot => $rowError)
							{
							   foreach($rowError as $errorString)
							   {
								  $errorArray[$keyRoot] = $errorString;
							   }
							}
							
							foreach($zf_filter_input->getMessages() as $key=>$err){
								$xxx = 'x'.$key;
								$this->view->$xxx = $this->displayError($err);
							}


							$this->view->succes = false;
							$this->view->report_msg = $errorArray;
						}
						
					}
					
//					$this->_db->delete('M_DOMESTIC_BANK_TABLE','BANK_ID = ?', $bankId);
					//$this->_db->delete('M_BRANCH','ID = '.$this->_db->quote($id));
				   	
					
					//$this->_db->commit();		
						 
					//Application_Helper_General::writeLog('COUD','Delete Branch Bank. Branch Id : ['.$id.']');
					//$this->_redirect('/notification/success/index');
					
					/*$msg_success = 'Record Deleted';
					
        	        $this->_helper->getHelper('FlashMessenger')->addMessage('S');
			        $this->_helper->getHelper('FlashMessenger')
						   ->addMessage(($msg_success !="")? $msg_success : null);
			   
			 
			 
			        $this->backendLog('A', $this->_moduleDB, $bankId  , null, null);
			        */
				    if($data['DEBIT_TYPE'] == '1'){
						$data['DEBIT_TYPE'] = 'Corporate';
					}else{
						$data['DEBIT_TYPE'] = 'Card Holder';
					}
					
					if($data['DEBIT_ATM'] == '1'){
						$data['DEBIT_ATM'] = 'On';
					}else{
						$data['DEBIT_ATM'] = 'Off';
					}
					
					if($data['DEBIT_EDC'] == '1'){
						$data['DEBIT_EDC'] = 'On';
					}else{
						$data['DEBIT_EDC'] = 'Off';
					}
					
					
					$this->view->data = $data;
						
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
					$this->_redirect('/'.$this->_request->getModuleName().'/index');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						Application_Helper_General::writeLog('COUD','Update Branch Bank list');
						
						$this->_redirect('/'.$this->_request->getModuleName().'/index');
						//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
						
					}					
				}	
			} 
			
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('COUD','Update Branch Bank list');
			
			//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
			$this->_redirect('/'.$this->_request->getModuleName().'/index');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
