<?php
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once ('General/Login.php');

class LoginController extends Application_Main
{
	
	public function initController(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	public function serviceAction()
    {
		$config = Zend_Registry::get('config');
		//firewall login
		if(!in_array($_SERVER['REMOTE_ADDR'], $config['coolpay1']['ip'])){
			echo 'FORBIDDEN Acces From '.$_SERVER['REMOTE_ADDR'];
			die;
		}
		
    	set_time_limit ( 0 );
		ini_set ( "default_socket_timeout", 500 );
		ini_set ( "soap.wsdl_cache_enabled", 0 );
    	$server = new SoapServer("./wsdl/login.wsdl", array('soap_version' => SOAP_1_2));
		$server->setClass('Login');
		$server->handle ();
    }
    
    
    public function sessiontransferAction(){
    	$custId = $this->_getParam('cust_id');
    	$userId = $this->_getParam('user_id');
    	$token = $this->_getParam('token');
    	$username = $userId;

    	if($this->validateToken($custId, $userId, $token)){
    				  $CustomerUser	= new CustomerUser(strtoupper($custId), strtoupper($username));
					  $listAccount 	= $CustomerUser->getAccounts();
					  $accountList 	= Application_Helper_Array::simpleArray($listAccount,'ACCT_NO');
				   	  $Settings = new Settings();
				   	  $allSetting = $Settings->getAllSetting();
				      $listPrivilege = $CustomerUser->getPrivileges();
    			      $privilege = Application_Helper_Array::simpleArray($listPrivilege,'FPRIVI_ID');
					   
                   	  $privilege[] = 'systemPrivilege';

                       // set keterangan login ke auth
		              $auth = Zend_Auth::getInstance();
					  $data = new stdClass();
					  //$data->listCommunityByCustLogin = $this->communityMenu(strtoupper($custId));
					  $data->listCommunityByCustLogin = $CustomerUser->getCommunities(3); 
					  
					  if(is_array($data->listCommunityByCustLogin)){
		    			foreach ($data->listCommunityByCustLogin as $key=>$comm)
						{
						   $data->listCommunityByCustLogin[$key]['BUSS_SCHEME_CODE'] = $this->_db->fetchOne('SELECT BUSS_SCHEME_CODE FROM M_SCHEME WHERE SCHEME_CODE  = ?',$comm['SCHEME_CODE']);
						}
    				  }
					  
					  
					  $data->isCommLinkage = $CustomerUser->isCommLinkage; 
					  $data->userIdLogin    = $username;
					  $data->userNameLogin  = $username;
					  $data->priviIdLogin   = $privilege;
					  $data->allSetting     = $allSetting;
					 // $data->userGroupLogin = $groupArr;
					  $data->custIdLogin    = strtoupper($custId);
					  $data->accountList    = $accountList;

					// ---------------------------------------- HASH -----------------------------------------------  //
						$hash = $this->hashKey($userId,$custId);
						$data->userHash     = $hash;

					  $auth->getStorage()->write($data);
                      // End set keterangan login ke auth
                      $browser = substr($_SERVER['HTTP_USER_AGENT'], 0,24);
					  $sessionHash = md5($_SERVER['REMOTE_ADDR'].$browser);

					  //update USER_ISLOGIN
					  $updateArr['USER_ISLOGIN'] = 1;
					  $updateArr['USER_LASTLOGIN'] = new Zend_Db_Expr('now()');
					  $updateArr['USER_FAILEDATTEMPT'] = 0;
					  $updateArr['USER_ISNEW'] = 0;
					  $updateArr['USER_RCHANGE'] = 0;
					  $updateArr['USER_HASH'] = $hash;
					  $updateArr['SESSION_HASH'] = $sessionHash;
	                  $whereArr = array('CUST_ID = ?' => (string)$custId);
	                  $whereArr = array('USER_ID = ?' => (string)$username);
		              $customerupdate = $this->_db->update('M_USER',$updateArr,$whereArr);

					  $this->_redirect('/home/index/index/refreshmenu/1');		
    	}else{
    				$config =  Zend_Registry::get('config');
					$this->_redirect($config['coolpay1']['url']);  
					  
    	}
    }
    
	protected function validateToken($custId,$userId,$token){

		//check login token & remove
		$userIdQuote = $this->_db->quote($userId);
		$custIdQuote = $this->_db->quote($custId);
		$tokenQuote = $this->_db->quote($token);
		$result = $this->_db->fetchRow("SELECT * FROM TEMP_SESSION_USER WHERE USER_ID = $userIdQuote AND CUST_ID = $custIdQuote AND TOKEN_WS = $tokenQuote AND IS_LOGIN  = 0");
		if(is_array($result)){
			$returnValue = true;
		}else{
			$returnValue = false;
		}
		$this->_db->update('TEMP_SESSION_USER',array('IS_LOGIN' => 1),array('USER_ID = ?' => $userId,'CUST_ID = ?' => $custId));
		return $returnValue;
	}
	
    private function communityMenu($cust_id)
	{
	   $member = $this->_db->select()
						   ->from(array('c' => 'M_COMMUNITY'),array('COMMUNITY_CODE','COMMUNITY_NAME'))
                           ->join(array('m' => 'M_MEMBER'),'m.COMMUNITY_CODE = c.COMMUNITY_CODE',array())
                           ->where('m.MEMBER_COMPANY='.$this->_db->quote((string)$cust_id))
                           ->query()->fetchAll();
                           
       $anchor = $this->_db->select()
						   ->from(array('c' => 'M_COMMUNITY'),array('COMMUNITY_CODE','COMMUNITY_NAME'))
                           ->join(array('a' => 'M_ANCHOR'),'c.ANCHOR_ID = a.ANCHOR_ID',array())
                           ->where('a.ANCHOR_CUST='.$this->_db->quote((string)$cust_id))
                           ->query()->fetchAll();
                           
      if(is_array($member) && count($member) > 0)      $comm_code_arr = $member;
      else if(is_array($anchor) && count($anchor) > 0) $comm_code_arr = $anchor;
      
	  /*/*  $comm_code_arr = $this->_db->select()
						   ->from(array('c' => 'M_COMMUNITY'),array('COMMUNITY_CODE','COMMUNITY_NAME'))
                           ->join(array('m' => 'M_MEMBER'),'m.COMMUNITY_CODE = c.COMMUNITY_CODE',array())
                           ->join(array('a' => 'M_ANCHOR'),'c.ANCHOR_ID = a.ANCHOR_ID',array())
                           ->where('m.MEMBER_COMPANY='.$this->_db->quote((string)$cust_id). ' OR a.ANCHOR_CUST='.$this->_db->quote((string)$cust_id))
                           ->query()->fetchAll();*/

      
       //Zend_Debug::dump($anchor);  echo "</br></br>";  Zend_Debug::dump($member);  die;
      
       $comm_code_arr = Application_Helper_Array::listArray($comm_code_arr,'COMMUNITY_CODE','COMMUNITY_NAME');                    
       
       /*foreach($comm_code_arr as $comm_id => $comm_name)
       {
           $result['COMMUNITY_CODE'] = $comm_id;
           $result['COMMUNITY_NAME'] = $comm_name;
       }*/
       
       
       return $result;
      
	}
	
	public function hashKey($userId,$custId) {
		do
		{
			$key = null;
			for($i=0;$i<3;$i++)
				$key .= rand(0,9);

			$browser = $_SERVER['HTTP_USER_AGENT'];
			$hash = md5($_SERVER['REMOTE_ADDR'].$browser.$key);
			$checkHash = null;
			// CHECK HASH 
			$checkHash = $this->_db->fetchOne(
																			$this->_db->SELECT()
																								->FROM('M_USER' ,array('USER_HASH'))
																								->WHERE('USER_ID =?',$userId)
																								->WHERE('CUST_ID =?',$custId)
																								->WHERE('USER_HASH =?',$hash)
																			);
		}while(!empty($checkHash));

		return $hash;
	}
	
}