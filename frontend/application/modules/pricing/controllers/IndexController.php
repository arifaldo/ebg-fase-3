<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
class pricing_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	// public function __construct()
	// {
	// 	$this->_db = Zend_Db_Table::getDefaultAdapter();
	// }


	public function indexAction()
	{
		// $custId = $this->_custIdLogin;
		$this->_helper->layout()->setLayout('newlayout');

		// if (!$this->view->hasPrivilege('VBOB')) { 
		// 	$this->_redirect('/authorizationacl/index/index');
		// }

	

		$app = Zend_Registry::get('config');
		// charges.type.code.api/
		$status = $app['charges']['type']['code']['api'];
		$desc = $app['charges']['type']['desc']['api'];
		// var_dump($status);die;
		$paramservice = array();
		foreach ($status as $key => $value) {
			$paramservice[$value] = $desc[$key];
		}

		$this->view->paramservice = $paramservice;

		// $bankArr = array(''=>'-- '.$this->language->_('Please Select').' --');
		
		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankArr,$cacheID);
        }
		
		asort($bankArr);
		
		$this->view->DEBIT_BANKarr  = $bankArr;

		$optMonth = '';
		$min1month = date("F Y", strtotime("first day of -1 month"));
		$min1monthval = date("Y-m", strtotime("first day of -1 month"));
		$min2month = date("F Y", strtotime("first day of -2 month"));
		$min2monthval = date("Y-m", strtotime("first day of -2 month"));
		$min3month = date("F Y", strtotime("first day of -3 month"));
		$min3monthval = date("Y-m", strtotime("first day of -3 month"));

		$currentMonthVal = date("Y-m");
		$currentMonth = date("F Y");

		$optMonth .= '<option value="' . $min3monthval . '">' . $min3month . '</option>';
		$optMonth .= '<option value="' . $min2monthval . '">' . $min2month . '</option>';
		$optMonth .= '<option value="' . $min1monthval . '">' . $min1month . '</option>';
		$optMonth .= '<option value="' . $currentMonthVal . '" selected>' . $currentMonth . ' (Ongoing)</option>';

		$this->view->optMonth = $optMonth;

		$test1 = date('Y-m-01');
		$test2 = date('Y-m-d');
		$this->view->from = $test1;
		$this->view->to = $test2;

		// if ($this->_request->isPost()) {
		// 	$month = $this->_getParam('month');

		// 	$hitlistbank = $this->_db->fetchAll(
		// 		$this->_db->select()
		// 			->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
		// 			->JOIN(array('B' => 'M_BANK_TABLE'), 'A.DIGI_BANK = B.BANK_CODE', array('B.BANK_NAME'))
		// 			->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
		// 			->where("A.DIGI_SERVICE != 0")
		// 			// ->where("A.DIGI_BANK != NULL")
		// 			// ->where("A.DIGI_SERVICE != NULL")
		// 			->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $month . "'")
		// 			->group('A.DIGI_BANK')
		// 	);

		// 	$hitlist = $this->_db->fetchAll(
		// 		$this->_db->select()
		// 			->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
		// 			->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
		// 			->where("A.DIGI_SERVICE != 0")
		// 			->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $month . "'")
		// 			->group('A.DIGI_BANK')
		// 			->group('A.DIGI_SERVICE')
		// 	);
		// } else {
			$whereCurDate = date('Y-m');

			$hitlistbank = $this->_db->select()
					->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
					->JOIN(
						array('B' => 'M_BANK_TABLE'),
						'A.DIGI_BANK = B.BANK_CODE',
						array('B.BANK_NAME')
					)
					->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
					->where("A.DIGI_SERVICE != 0")
					// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $whereCurDate . "'")
					->group('A.DIGI_BANK');
			
			$hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($test1)." and ".$this->_db->quote($test2));

			$hitlistbank = $this->_db->fetchAll($hitlistbank);

			$hitlist = $this->_db->select()
					->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
					->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
					->where("A.DIGI_SERVICE != 0")
					// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $whereCurDate . "'")
					->group('A.DIGI_BANK')
					->group('A.DIGI_SERVICE');
			$hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($test1)." and ".$this->_db->quote($test2));
			$hitlist = $this->_db->fetchAll($hitlist);
			
		// }
		// var_dump($hitlist);die;

		// echo $complist;	
		// echo "<pre>";
		// print_r($hitlist);die;

		//get data from globalvar.ini
		$serviceType = array_flip($this->_chargestype['code']['api']);
		$serviceDesc =  $this->_chargestype['desc']['api'];
		$serviceColumnName =  $this->_chargestype['dbcolumn']['api'];

		$this->view->serviceType = $serviceType;
		$this->view->serviceColumnName = $serviceColumnName;
		$this->view->serviceDesc = $serviceDesc;

		//get charges data from M_CHARGES_API
		$custIdList = "'" . $this->_custIdLogin . "','GLOBAL'";
		$chargesFee = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_CHARGES_API'), array('*'))
				->where("A.CUST_ID IN (" . $custIdList . ")")
		);

		$chargesFee = $chargesFee[0];

		$this->view->chargesFee = $chargesFee;

		if (!empty($chargesFee)) {
			foreach ($chargesFee as $key => $value) {
				$priceTable[$key] = $value;
			}
		}
		$this->view->priceTable = $priceTable;
	

		$total = 0;
		if (!empty($hitlist)) {
			foreach ($hitlist as $key => $value) {
				$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
				$totalsrv = (int) $value['TOTALHIT'] * $price;
				$hitlist[$key]['TOTALPRICE'] = $totalsrv;

				$total = $total + $totalsrv;
			}
			$this->view->hitlistbank = $hitlistbank;
			// print_r($hitlist);die();
			$this->view->servicelist = $hitlist;
			// echo "<pre>";
			// var_dump($hitlistbank);
			// var_dump($hitlist);
			$this->view->totalprice = $total;

			foreach ($bankArr as $key => $value) {
				foreach ($hitlist as $ht) {
					if ($key == $ht['DIGI_BANK']) {
						$price = $chargesFee[$serviceColumnName[$serviceType[$ht['DIGI_SERVICE']]]];
						$totalsrv = $price;
						// $hitlist[$key]['TOTALPRICE'] = $totalsrv;

						$dataTable[$ht['DIGI_BANK']][$ht['DIGI_SERVICE']] = array($ht['TOTALHIT'], $totalsrv);
					}
				}
			}

			$this->view->dataTable = $dataTable;
			
		}

		Application_Helper_General::writeLog('VBOB', 'View Billing Console');

		$pdf = $this->_getParam('pdf');

		if ($pdf) {
			$base64 = $this->_getParam('assetsChartBase64');

			$datefrom = $this->_getParam('startdate');
			$dateto = $this->_getParam('enddate');

			$hitlistbank = $this->_db->select()
					->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
					->JOIN(array('B' => 'M_BANK_TABLE'), 'A.DIGI_BANK = B.BANK_CODE', array('B.BANK_NAME'))
					->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
					->where("A.DIGI_SERVICE != 0")
					// ->where("A.DIGI_BANK != NULL")
					// ->where("A.DIGI_SERVICE != NULL")
					// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $month . "'")
					->group('A.DIGI_BANK');
			// );

			if(!empty($datefrom) && empty($dateto))
	            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') >= ".$this->_db->quote($datefrom));
	            
		   	if(empty($datefrom) && !empty($dateto))
		            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') <= ".$this->_db->quote($dateto));
		            
		    if(!empty($datefrom) && !empty($dateto))
		            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

		    $hitlistbank = $this->_db->fetchAll($hitlistbank);

			$hitlist = $this->_db->select()
					->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
					->JOIN(array('B' => 'M_BANK_TABLE'), 'A.DIGI_BANK = B.BANK_CODE', array('B.BANK_NAME'))
					->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
					->where("A.DIGI_SERVICE != 0")
					// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $month . "'")
					->group('A.DIGI_BANK')
					->group('A.DIGI_SERVICE')
					->order('B.BANK_NAME ASC');
			// );

			if(!empty($datefrom) && empty($dateto))
	            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') >= ".$this->_db->quote($datefrom));
	            
		   	if(empty($datefrom) && !empty($dateto))
		            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') <= ".$this->_db->quote($dateto));
		            
		    if(!empty($datefrom) && !empty($dateto))
		            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

			$hitlist = $this->_db->fetchAll($hitlist);

			$total = 0;
			if (!empty($hitlist)) {
				foreach ($hitlist as $key => $value) {
					$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
					$totalsrv = (int) $value['TOTALHIT'] * $price;
					$hitlist[$key]['TOTALPRICE'] = $totalsrv;

					$total = $total + $totalsrv;
				}
				$this->view->pdfHitlistbank = $hitlistbank;
				$this->view->pdfServicelist = $hitlist;
				$this->view->pdfTotalprice = $total;
			}

			// $formatedMonth = DateTime::createFromFormat('Y-m', $month);
			// $formMonth = $formatedMonth->format('F Y');

			if(!empty($datefrom) && empty($dateto)){
	            $formatedMonth = DateTime::createFromFormat('Y-m-d', $datefrom);
				$formMonth = $formatedMonth->format('d F Y');
			}
	            
		   	if(empty($datefrom) && !empty($dateto)){
		        $formatedMonth = DateTime::createFromFormat('Y-m-d', $dateto);
				$formMonth = $formatedMonth->format('d F Y');
		    }       
		    if(!empty($datefrom) && !empty($dateto)){
		        $formatedMonth1 = DateTime::createFromFormat('Y-m-d', $datefrom);
				$formMonth1 = $formatedMonth1->format('d F Y');

				$formatedMonth2 = DateTime::createFromFormat('Y-m-d', $dateto);
				$formMonth2 = $formatedMonth2->format('d F Y');

				$formMonth = $formMonth1.' - '.$formMonth2;
		    }
			// echo $formMonth;
			// die();

			// $curMonth = strtotime(date("Y-m"));

			// $datediff = strtotime($month) - $curMonth;
			// $difference = floor($datediff / (60 * 60 * 24));

			// $title = '';
			// if ($difference == 0) {
			// 	$title = 'Billing Console_' . $formMonth . ' (Ongoing)';
			// 	$this->view->formatedMonth = $formMonth . ' (Ongoing)';
			// } else {
				$title = 'Billing Console_' . $formMonth;
				$this->view->formatedMonth = $formMonth;
			// }

			Application_Helper_General::writeLog('BAIQ', 'Print PDF');
			// $HTMLchart = $this->view->render($this->view->controllername.'/chart.phtml');
			$HTMLtable = $this->view->render($this->view->controllername . '/pdf.phtml');
			$this->_helper->download->pdfWithChart(null, null, null, $title, $this->_custNameLogin, 'BILLING CONSOLE', $base64, '280px', $HTMLtable);
		}
	}

	public function changemonthAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// $value = $this->_getParam('value');

		$datefrom = $this->_getParam('startdate');
		$dateto = $this->_getParam('enddate');

		$hitlistbank = $this->_db->select()
				->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
				->JOIN(array('B' => 'M_BANK_TABLE'), 'A.DIGI_BANK = B.BANK_CODE', array('B.BANK_NAME'))
				->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
				->where("A.DIGI_SERVICE != 0")
				// ->where("A.DIGI_BANK != NULL")
				// ->where("A.DIGI_SERVICE != NULL")
				// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d')= '" . $value . "'")
				->group('A.DIGI_BANK');

		if(!empty($datefrom) && empty($dateto))
	            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

		$hitlistbank = $this->_db->fetchAll($hitlistbank);



		$hitlist = $this->_db->select()
				->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
				->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
				->where("A.DIGI_SERVICE != 0")
				// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $value . "'")
				->group('A.DIGI_BANK')
				->group('A.DIGI_SERVICE');
		// );

		if(!empty($datefrom) && empty($dateto))
	            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

		$hitlist = $this->_db->fetchAll($hitlist);

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'))
			->order('C.BANK_NAME ASC');


		$databank 					= $this->_db->fetchAll($selectbank);

		foreach ($databank as $key => $value) {
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		//get data from globalvar.ini
		$serviceType = array_flip($this->_chargestype['code']['api']);
		$serviceDesc =  $this->_chargestype['desc']['api'];
		$serviceColumnName =  $this->_chargestype['dbcolumn']['api'];

		$total = 0;
		$custIdList = "'" . $this->_custIdLogin . "','GLOBAL'";
		$chargesFee = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_CHARGES_API'), array('*'))
				->where("A.CUST_ID IN (" . $custIdList . ")")
		);

		$chargesFee = $chargesFee[0];
		if (!empty($chargesFee)) {
			foreach ($chargesFee as $key => $value) {
				$priceTable[$key] = $value;
			}
		}
		// echo "<pre>";
		// var_dump($hitlist);die;
		if (!empty($hitlist)) {
			foreach ($hitlist as $key => $value) {
				$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
				$totalsrv = (int) $value['TOTALHIT'] * $price;
				$hitlist[$key]['TOTALPRICE'] = $totalsrv;

				$total = $total + $totalsrv;
			}
		}

		$result['hitlistbank'] = $hitlistbank;
		$result['servicelist'] = $hitlist;
		$result['totalprice'] = 'Total: IDR ' . Application_Helper_General::displayMoney($total);

		if (!empty($hitlistbank)) {
			$datagraph = array();
			$totalhit = 0;
			foreach ($hitlistbank as $key => $value) {
				$totalhit += $value['TOTALHIT'];
			}
			foreach ($hitlistbank as $key => $value) {

				foreach ($bankArr as $test => $val) {
					if ($test == $value['DIGI_BANK']) {

					  $price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
					  $totalsrv = $value['TOTALHIT'] * $price;
					 
					}
				}

				// $datagraph[$key]['y'] = (int) $value['TOTALHIT'];
				$datagraph[$key]['y'] = number_format(((int) $value['TOTALHIT'] / (int) $totalhit) * 100, 2);
      			$datagraph[$key]['toolTipContent'] = 'IDR '.Application_Helper_General::displayMoney($totalsrv);
				// $datagraph[$key]['legendText'] = $value['BANK_NAME'] . ' : ' . number_format($value['TOTALHIT']) . ' (' . number_format(((int) $value['TOTALHIT'] / (int) $totalhit) * 100, 2) . '%)';
				$datagraph[$key]['legendText'] = $value['BANK_NAME'];
				$datagraph[$key]['name'] = $value['BANK_NAME'];
				$datagraph[$key]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			}
		}

		// $result['datapointstr'] = $str;

		$visitorsData['Service'][] = array(
			'click' => 'visitorsChartDrilldownHandler',
			'cursor' => 'pointer',
			'explodeOnClick' => false,
			'name' => "New vs Returning Visitors",
			'showInLegend' => true,
			'indexLabel' => "{y}%",
			'indexLabelPlacement' => "inside",
			'indexLabelFontColor' => 'white',
			'indexLabelFontSize' => 11,
			// 'indexLabelPlacement' => "outside",
			'type' => "doughnut",
			'dataPoints' => $datagraph
		);

		if (!empty($hitlistbank)) {


			$str = '';
			foreach ($hitlistbank as $key => $value) {

				$datagraphdetail = array();

				$strdata = array();
				foreach ($hitlist as $k => $val) {
					if ($value['DIGI_BANK'] == $val['DIGI_BANK']) {
						$datagraphdetail['y'] = (int) $val['TOTALHIT'];
						$service = array();
						$service[1] = 'Balance Inquiry';
						$service[2] = 'Register ';
						$service[NULL] = 'Transfer Fee';
						$datagraphdetail['name'] = $service[$val['DIGI_SERVICE']];
						$datagraphdetail['indexLabel'] = $service[$val['DIGI_SERVICE']] . ' : ' . $val['TOTALHIT'];
						$datagraphdetail['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

						array_push($strdata, $datagraphdetail);
						unset($datagraphdetail);
					}
				}

				if (!empty($hitlistbank[$key + 1])) {

					$visitorsData[$value['BANK_NAME']][] = array(
						'click' => 'visitorsChartDrilldownHandler',
						'cursor' => 'pointer',
						'explodeOnClick' => false,
						'color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
						'name' => $value['BANK_NAME'],
						'type' => 'doughnut',
						'showInLegend' => 'true',
						'indexLabel' => "{y}%",
						'indexLabelPlacement' => "inside",
						'indexLabelFontColor' => 'white',
						'indexLabelFontSize' => 11,
						'dataPoints' => $strdata,
					);
				} else {

					$visitorsData[$value['BANK_NAME']][] = array(
						'click' => 'visitorsChartDrilldownHandler',
						'cursor' => 'pointer',
						'explodeOnClick' => false,
						'color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
						'name' => $value['BANK_NAME'],
						'type' => 'doughnut',
						// 'indexLabelPlacement' => 'outside',
						'showInLegend' => 'true',
						'indexLabel' => "{y}%",
						'indexLabelPlacement' => "inside",
						'indexLabelFontColor' => 'white',
						'indexLabelFontSize' => 11,
						'dataPoints' => $strdata,
					);
				}
			}
		}

		$result['visitorsdata'] = json_encode($visitorsData);


		


		$i = 0;
		$tableData = '';

		$serviceDesc =  $this->_chargestype['desc']['api'];
		$serviceType = array_flip($this->_chargestype['code']['api']);
		// echo "<pre>";
		// var_dump($serviceDesc);die;

		foreach ($bankArr as $key => $value) {
			foreach ($hitlist as $ht) {
				if ($key == $ht['DIGI_BANK']) {
					$price = $chargesFee[$serviceColumnName[$serviceType[$ht['DIGI_SERVICE']]]];
					$totalsrv = $price;
					// $hitlist[$key]['TOTALPRICE'] = $totalsrv;

					$dataTable[$ht['DIGI_BANK']][$ht['DIGI_SERVICE']] = array($ht['TOTALHIT'], $totalsrv);
				}
			}
		}
		if (!empty($dataTable)) {
				//price
				$p_balance = $priceTable['BALANCE_INQUIRY'];
				$p_pb = $priceTable['INHOUSE_TRANSFER'];
				$p_online = $priceTable['ONLINE_TRANSFER'];
				$p_skn = $priceTable['SKN_TRANSFER'];
				$p_rtgs = $priceTable['RTGS_TRANSFER'];
				$p_interbenefinquiry = $priceTable['INTERBANK_BENEF_INQUIRY'];
				$p_statement = $priceTable['ACCOUNT_STATEMENT'];
				$p_inhousebenefinquiry = $priceTable['INHOUSE_BENEF_INQUIRY'];
			foreach ($dataTable as $key => $d_value) {
				$balance = 0;
				$pb = 0;
				$online = 0;
				$skn = 0;
				$rtgs = 0;
				$interbenefinquiry = 0;
				$inhousebenefinquiry = 0;
				$statement = 0;
				
				foreach ($d_value as $ht => $ht_value) {
					$serviceType[$ht] == 'balance' ? $balance = $ht_value[0] : '';
					$serviceType[$ht] == 'pb' ? $pb = $ht_value[0] : '';
					$serviceType[$ht] == 'online' ? $online = $ht_value[0] : '';
					$serviceType[$ht] == 'skn' ? $skn = $ht_value[0] : '';
					$serviceType[$ht] == 'rtgs' ? $rtgs = $ht_value[0] : '';
					$serviceType[$ht] == 'interbenefinquiry' ? $interbenefinquiry = $ht_value[0] : '';
					$serviceType[$ht] == 'statement' ? $statement = $ht_value[0] : '';
					$serviceType[$ht] == 'inhousebenefinquiry' ? $inhousebenefinquiry = $ht_value[0] : '';

					// $serviceType[$ht] == 'balance' ? $p_balance = $ht_value[1] : '';
					// $serviceType[$ht] == 'pb' ? $p_pb = $ht_value[1] : '';
					// $serviceType[$ht] == 'online' ? $p_online = $ht_value[1] : '';
					// $serviceType[$ht] == 'skn' ? $p_skn = $ht_value[1] : '';
					// $serviceType[$ht] == 'rtgs' ? $p_rtgs = $ht_value[1] : '';
					// $serviceType[$ht] == 'interbenefinquiry' ? $p_interbenefinquiry = $ht_value[1] : '';
					// $serviceType[$ht] == 'statement' ? $p_statement = $ht_value[1] : '';
					// $serviceType[$ht] == 'inhousebenefinquiry' ? $p_inhousebenefinquiry = $ht_value[1] : '';
				}
				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
				$tableData .= '<tr>  
										<td class="' . $td_css . '">' . $bankArr[$key] . '</td>
										<td class="' . $td_css . ' rightalign"><p data-toggle="tooltip" data-html="true" title="' . Application_Helper_General::displayMoney($p_balance * $balance) . '<br>' . Application_Helper_General::displayMoney($p_balance) . '/HIT">' . number_format($balance) . '</p></td>
										<td class="' . $td_css . ' rightalign"><p data-toggle="tooltip" data-html="true" title="' . Application_Helper_General::displayMoney($p_online * $online) . '<br>' . Application_Helper_General::displayMoney($p_online) . '/HIT">' . number_format($online) . '</p></td>
										<td class="' . $td_css . ' rightalign"><p data-toggle="tooltip" data-html="true" title="' . Application_Helper_General::displayMoney($p_skn * $skn) . '<br>' . Application_Helper_General::displayMoney($p_skn) . '/HIT">' . number_format($skn) . '</p></td>
										<td class="' . $td_css . ' rightalign"><p data-toggle="tooltip" data-html="true" title="' . Application_Helper_General::displayMoney($p_rtgs * $rtgs) . '<br>' . Application_Helper_General::displayMoney($p_rtgs) . '/HIT">' . number_format($rtgs) . '</p></td>
										<td class="' . $td_css . ' rightalign"><p data-toggle="tooltip" data-html="true" title="' . Application_Helper_General::displayMoney($p_inhousebenefinquiry * $inhousebenefinquiry) . '<br>' . Application_Helper_General::displayMoney($p_inhousebenefinquiry) . '/HIT">' . number_format($inhousebenefinquiry) . '</p></td>
										<td class="' . $td_css . ' rightalign"><p data-toggle="tooltip" data-html="true" title="' . Application_Helper_General::displayMoney($p_interbenefinquiry * $interbenefinquiry) . '<br>' . Application_Helper_General::displayMoney($p_interbenefinquiry) . '/HIT">' . number_format($interbenefinquiry) . '</p></td>
										<td class="' . $td_css . ' rightalign"><p data-toggle="tooltip" data-html="true" title="' . Application_Helper_General::displayMoney($p_statement * $statement) . '<br>' . Application_Helper_General::displayMoney($p_statement) . '/HIT">' . number_format($statement) . '</p></td>
							</tr>';
				$i++;
				// <td class="' . $td_css . '"><p data-toggle="tooltip" data-html="true" title="' . Application_Helper_General::displayMoney($p_interbenefinquiry * $interbenefinquiry) . '<br>' . Application_Helper_General::displayMoney($p_interbenefinquiry) . '/HIT">' . number_format($interbenefinquiry) . '</p></td>
			}
		}
		// if (!empty($hitlist)) {
		// 	foreach ($hitlist as $key => $value) {
		// 		$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
		// 		$tableData .= '<tr>  
		//                 <td class="' . $td_css . '">' . $bankArr[$value['DIGI_BANK']] . '</td>
		//                 <td class="' . $td_css . '">' . $serviceDesc[$serviceType[$value['DIGI_SERVICE']]] . '</td>
		//                 <td class="' . $td_css . '" align="right">' . number_format($value['TOTALHIT']) . '</td>
		//                 <td class="' . $td_css . '" style="max-width: 160px;" ><label class="col-md-3" style="text-align:left">IDR</label><label class="col-md-9" style="text-align:right">' . Application_Helper_General::displayMoney($value['TOTALPRICE']) . '</label></td>
		//               </tr>';
		// 		$i++;
		// 	}
		// } 
		else {
			$tableData .= '<tr><td colspan="8" align="center">---- No Data ----</td></tr>';
		}


		$result['tableData'] = $tableData;

		echo json_encode($result);
	}
}
