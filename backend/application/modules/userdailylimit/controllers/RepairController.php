<?php

require_once 'Zend/Controller/Action.php';

class userdailylimit_RepairController extends userdailylimit_Model_Userdailylimit{

  public function indexAction()
  {
  
    //gara" pakai pop up javascript display money & convert money tidak berfungsi di view, jd pop up harus dicomment
    $this->_helper->layout()->setLayout('popup');

  	$changes_id = $this->_getParam('changes_id');
  	$changes_id = (Zend_Validate::is($changes_id,'Digits'))? $changes_id : 0;
  	$customer_view = 1; $error_remark = null; $key_value = null;
  	
  	 //convert dailylimit agar bisa masuk ke database
     $dailyLimit = $this->_getParam('dailylimit');
     $dailyLimit = Application_Helper_General::convertDisplayMoney($dailyLimit);
     $this->_setParam('dailylimit',$dailyLimit); 
     //END convert dailylimit agar bisa masuk ke database
  	
  	 $this->view->userDailyLimit_msg = array();
  	
  	 
  	 
  	//jika change id ada isinya maka true
  	if($changes_id)
  	{ 
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID','CHANGES_TYPE'))
                             ->where('CHANGES_ID='.$this->_db->quote($changes_id));
                             /* ->where('UPPER(CHANGES_STATUS)='.$this->_db->quote(strtoupper($this->_changeStatus['code']['waitingApproval'])).' OR UPPER(CHANGES_STATUS)='.$this->_db->quote(strtoupper($this->_changeStatus['code']['repairRequested'])))
                             //->where('UPPER(MODULE)='.$this->_db->quote(strtoupper($this->_request->getModuleName())))
                             ->where('UPPER(CHANGES_TYPE)='.$this->_db->quote(strtoupper($this->_changeType['code']['new'])).' OR UPPER(CHANGES_TYPE)='.$this->_db->quote(strtoupper($this->_changeType['code']['edit'])))
                             ->where('UPPER(CREATED_BY)='.$this->_db->quote((string)strtoupper($this->_userIdLogin)))
                             ->where("CHANGES_FLAG='B'");*/
      $resultdata = $this->_db->fetchRow($select);
     
      //jika change id ada di database maka true
      if($resultdata['CHANGES_ID'])
      {
         $dailyLimitData = $this->getTempUserDailyLimit($changes_id);
      	 
         if($dailyLimitData['TEMP_ID'])
      	 {
      	   $change_type = strtoupper($resultdata['CHANGES_TYPE']);
      	   if($change_type == strtoupper($this->_changeType['code']['edit'])) $key_value = strtoupper($dailyLimitData['CUST_ID']);
      	   
      	   $cust_id = $dailyLimitData['CUST_ID'];
      	   $user_id = $dailyLimitData['USER_LOGIN'];
      	   $ccy_id  = $dailyLimitData['CCY_ID'];
      	   
      	 }
      	 else{ $changes_id = 0; }
      }
      else{ $changes_id = 0; } 
  	}
  	
  	
  	
  	//jika change id tidak ada isinya maka error
    if(!$changes_id)
    {
      $error_remark = $this->getErrorRemark('22',$this->language->_('Suggestion ID'));
      //insert log
  	  try 
  	  {
  	  	$this->_db->beginTransaction();
  	    if($this->_request->isPost())
  	    {
  	      if($this->_request->getPost('populate_cif')=='ya'){ $action = strtoupper($this->_actionID['popcif']); }
  	  	  else{ $action = strtoupper($this->_actionID['repair']); }
  	      $fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($_POST);
  	    }
  	    else
  	    { 
  	      $action = strtoupper($this->_actionID['view']); 
  	      $fulldesc = null; 
  	    }
      		$this->backendLog($action,strtoupper($this->_moduleID['cust']),null,$fulldesc,$error_remark);
      		$this->_db->commit();
      }
      catch(Exception $e)
      {
     		$this->_db->rollBack();
    		SGO_Helper_GeneralLog::technicalLog($e);
      }
	  
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL); 	
    }
  	

    if($change_type == $this->_changeType['code']['new']){
      $userData = $this->_db->select()
                              ->from('M_USER',array('USER_ID','USER_FULLNAME'))
                              ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                              //->where('USER_STATUS!=3')
                              ->query()->fetchAll();
      $this->view->userData = $userData; 
    }

    $getCcy = $this->getCcyList();
    $this->view->CCYData  = $getCcy;
    
  
  	if($this->_request->isPost())
  	{
        $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_LIMIT_USD','CUST_LIMIT_IDR'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
                             //->where('CUST_STATUS!=3');
        $result = $this->_db->fetchRow($select);
        $companyLim = array();
        $companyLim['IDR'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_IDR']);
        $companyLim['USD'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_USD']);

        $sumArr = array();
        foreach($getCcy as $val){
          $tempCCY = $val['CCY_ID'];
          $selectSum = $this->_db->select()
                                ->from('M_DAILYLIMIT', new Zend_Db_Expr('SUM(DAILYLIMIT)'))
                                ->where('UPPER(CUST_ID)= ?', $cust_id)
                                ->where('CCY_ID = ?', $tempCCY);
          $sumArr[$val['CCY_ID']] = $this->_db->fetchOne($selectSum);
        }
  	   
        $filters = array('user_login'     => array('StripTags','StringTrim','StringToUpper'),
                         'ccy_id'         => array('StripTags','StringTrim','StringToUpper'),
                         'dailylimit'     => array('StripTags','StringTrim'),
                         'cust_id'        => array('StripTags','StringTrim','StringToUpper'),
                        );

        if($change_type == $this->_changeType['code']['edit']){
          $validators =  array('cust_id'       => array(),
        
          				     'user_login'    => array(),

                               'ccy_id'        => array(),
  											                  
  						  
                               'dailylimit'    => array('NotEmpty',
  											          //'Digits',
  											          'Float',
  											          'messages' => array($this->language->_('Can not be empty'),
  												                       $this->language->_('Invalid Maximum Amount Format'))
  											          ),    
                              );
        }
        else{
          $ccy_id = $this->_getParam('ccy_id');
          $validators =  array('cust_id'     => array('NotEmpty',
                                                    'messages' => array($this->language->_('Can not be empty'))
                                                   ), 
    
                           'user_login'  => array('NotEmpty',
                                                  //array('Db_NoRecordExists',array('table'=>'M_DAILYLIMIT','field'=>'USER_LOGIN','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' and UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id.' and DAILYLIMIT_STATUS ="1"'))),
                                                  //array('Db_NoRecordExists',array('table'=>'TEMP_DAILYLIMIT','field'=>'USER_LOGIN','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' and UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id))),
                                                  'messages' => array($this->language->_('Can not be empty'),
                                                                      //$this->language->_('This User id and Currency combination already exists'),
                                                                      //$this->language->_('This User id and Currency combination already suggested'))
                                                                      )
                                                    ),
                           'ccy_id'        => array('NotEmpty',
                              'messages' => array($this->language->_('Can not be empty'))
                              ), 
                                        
                            'dailylimit'    => array('NotEmpty',
                              'Float',
                              array('GreaterThan',0),
                              array('StringLength',array('min'=>1,'max'=>16)), //batas = 13 digit,16
                              'messages' => array($this->language->_('Can not be empty'),
                                                $this->language->_('Invalid Maximum Amount Format'),
                                                  $this->language->_('Maximum Amount must be greater than zero'),
                                        $this->language->_('Too many significant digits.Maximum digit allowed : 13 digit(s)'),
                                                 )
                              ),            
                         );
        }
  	
       
  	    $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
      
      
    if($zf_filter_input->isValid())
	  {
	    $passvalid = false;
      if($change_type != $this->_changeType['code']['edit']){
        $checkMaster = $this->_db->select()
                              ->from(array('M_DAILYLIMIT'), array('USER_LOGIN'))
                              ->where('USER_LOGIN = ?', $zf_filter_input->user_login)
                              ->where('CUST_ID = ?', $cust_id)
                              ->where('CCY_ID = ?', $zf_filter_input->ccy_id)
                              ->where('DAILYLIMIT_STATUS = ?', '1');
        $uniqueMaster = $this->_db->fetchOne($checkMaster);


        $checkTemp = $this->_db->select()
                              ->from(array('TEMP_DAILYLIMIT'), array('USER_LOGIN'))
                              ->where('USER_LOGIN = ?', $zf_filter_input->user_login)
                              ->where('CUST_ID = ?', $cust_id)
                              ->where('CCY_ID = ?', $zf_filter_input->ccy_id);
        $uniqueTemp = $this->_db->fetchOne($checkMaster);

        if(empty($uniqueTemp) && empty($uniqueMaster))
          $passvalid = true;
      }
      else{
        $passvalid = true;
      }

      if($passvalid){
        $ccy = $zf_filter_input->ccy_id;
        $dailylim = $zf_filter_input->dailylimit;
        $dailylimLeft = $companyLim[$ccy] ;
        // - Application_Helper_General::convertDisplayMoney($sumArr[$ccy]);

        $errorLim = false;

        if($dailylim > $dailylimLeft){
          $errorLim = true;
          $error_remark = "Maximum Amount of Daily Limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);
        }
        else{
    	     $info = 'Cust ID = '.$zf_filter_input->cust_id.', User Id = '.$zf_filter_input->user_login.', Ccy Id = '.$zf_filter_input->ccy_id;
            
    	     $dailyLimitData = $this->_dailyLimitData;
            
    	     foreach($validators as $key=>$value)
    	     {
    	  	     if($zf_filter_input->$key) $dailyLimitData[strtoupper($key)] = $zf_filter_input->$key;
    	  	 }
    	  	
    	  	 
              //$user_data['USER_STATUS'] = 1;
              //$user_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
    		 // $user_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
    	  	
    	  	
    	  	 
    	  	try 
    	  	{
      		  $this->_db->beginTransaction();
      		  
      		  //update T_GLOBAL_CHANGES
      		  $this->updateGlobalChanges($changes_id,$info,null,null,null,$cust_id);
      		  
      		  //update TEMP_DAILYLIMIT
      		  $this->updateTempDailyLimit($changes_id,$dailyLimitData);
      	  	  
      		  //log CRUD
      		  Application_Helper_General::writeLog('DLCR','User Daily Limit has been Repaired, User Id : '.$zf_filter_input->user_login. ' CCY : '.$ccy_id);
    	  	  
            $this->_db->commit();
            
            $this->_redirect('/popup/successpopup');
    	  	}
      		catch(Exception $e)
      		{
      		  $this->_db->rollBack();
      		  $error_remark = $this->getErrorRemark('82');
      		  SGO_Helper_GeneralLog::technicalLog($e);
      		}
        }
      }
      else{
        if(!empty($uniqueMaster))
          $error_remark = $this->language->_('This User id and Currency combination already exists');
        else
          $error_remark = $this->language->_('This User id and Currency combination already suggested');
      }
		  
		
      if(isset($error_remark)){
          try 
          {
            $this->_db->beginTransaction();
            $fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($_POST);
            $this->backendLog(strtoupper($this->_actionID['repair']),strtoupper($this->_moduleID['cust']),$zf_filter_input->cust_id,$fulldesc,$error_remark);
            $this->_db->commit();
          }
          catch(Exception $e)
          {
            $this->_db->rollBack();
            SGO_Helper_GeneralLog::technicalLog($e);
          }

          $errorArray = array();
          if($errorLim == true){
            $errorArray['dailylimit'] = $error_remark;
          }
          else{
            if(!empty($uniqueMaster) || !empty($uniqueTemp))
              $errorArray['user_login'] = $error_remark;
            else
              $this->view->error_msg = $error_remark;
          }

      if(!empty($errorArray)){ $this->view->userDailyLimit_msg = $errorArray;}
          
          $this->view->ccy_id  = ($zf_filter_input->isValid('ccy_id'))?  $zf_filter_input->ccy_id  : $this->_getParam('ccy_id');
          $this->view->user_id = ($zf_filter_input->isValid('user_login'))? $zf_filter_input->user_login : $this->_getParam('user_login');
          $this->view->max_amt = ($zf_filter_input->isValid('dailylimit'))? $zf_filter_input->dailylimit : $this->_getParam('dailylimit');
      }
	    //insert log
        
	  }//END if($zf_filter_input->isValid())
	  else
	  { 
	    
	     $this->view->error = 1;
	  	
	  	 $this->view->max_amt = ($zf_filter_input->isValid('dailylimit'))? $zf_filter_input->max_amt : $this->_getParam('dailylimit');
        
       $error = $zf_filter_input->getMessages();
	
       //format error utk ditampilkan di view html 
       $errorArray = null;
       foreach($error as $keyRoot => $rowError)
       {
          foreach($rowError as $errorString)
          {
            $errorArray[$keyRoot] = $errorString;
          }
       }
       
       $this->view->userDailyLimit_msg = $errorArray;
         
         
    }
  }//END if($this->_request->isPost())
  else 
  {
  
      $this->view->max_amt = Application_Helper_General::displayMoney($dailyLimitData['DAILYLIMIT']);
      
  }
    
    
  $select = $this->_db->select()
	                       ->from('M_CUSTOMER',array('CUST_NAME'))
	                       ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
  $this->view->cust_name = $this->_db->fetchOne($select);
  
  $this->view->cust_id = $cust_id;
  $this->view->user_id = $user_id;
  $this->view->ccy_id  = $ccy_id;
  $this->view->changes_id = $changes_id;
  $this->view->changes_type = $change_type;
  $this->view->modulename = $this->_request->getModuleName();
    
    
    
    //insert log
	try {
	  $this->_db->beginTransaction();
	  
	  Application_Helper_General::writeLog('DLCR','Repair User Daily Limit');
	  
	  $this->_db->commit();
	}
  catch(Exception $e){
 	  $this->_db->rollBack();
	  SGO_Helper_GeneralLog::technicalLog($e);
	}

  }
}