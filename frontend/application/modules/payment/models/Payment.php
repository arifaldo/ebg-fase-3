<?php

Class payment_Model_Payment
{
    protected $_db;
   
    public function __construct()
    { 
		$this->_config 	= Zend_Registry::get('config');
		$this->_db 		= Zend_Db_Table::getDefaultAdapter();
    }
    
	function date()
	{
		return date("d M Y H:i:s");
	}
	
	function cekList($param)
	{
		$select	= $this->_db->select()
								->from	(
											array(	'TPL' => 'T_PAY_LIST'),
											array(	
													'PAY_LIST_ID'		=> 'TPL.PAY_LIST_ID',
													'PS_NUMBER'			=> 'TPL.PS_NUMBER',
													'USER_ID'			=> 'TPL.USER_ID',
													'CUST_ID'			=> 'TPL.CUST_ID',
													'USER_BANK_CODE' 	=> 'TPL.USER_BANK_CODE',
													'PROVIDER_ID' 		=> 'TPL.PROVIDER_ID',
													'PROVIDER_NAME' 	=> 'MSC.PROVIDER_NAME',
													'REF_NO' 			=> 'TPL.REF_NO',
												)
										)
								->joinLeft(
											array('MSC' => 'M_SERVICE_PROVIDER'),
											'MSC.PROVIDER_ID = TPL.PROVIDER_ID',null
										);
		if(isset($param['PROVIDER_ID']))
		{
			$select->where("TPL.PROVIDER_ID = ? ", $param['PROVIDER_ID']);
		}
		if(isset($param['USER_ID']))
		{
			$select->where("TPL.USER_ID = ? ", $param['USER_ID']);
		}
		if(isset($param['CUST_ID']))
		{
			$select->where("TPL.CUST_ID = ? ", $param['CUST_ID']);
		}
		if(isset($param['REF_NO']))
		{
			$select->where("TPL.REF_NO = ? ", $param['REF_NO']);
		}
		
		if(!isset($param['fetch']))
		{
			$param['fetch'] = 'fetchAll';
		}
		
		$result =  $this->_db->FetchAll($select);
		return $result;
	}
	
	function cekListVA($param)
	{
		// print_r($param);die;
		$select	= $this->_db->select()
								->from	(
											array(	'TPL' => 'T_PAY_LIST'),
											array(	
													'PAY_LIST_ID'		=> 'TPL.PAY_LIST_ID',
													'PS_NUMBER'			=> 'TPL.PS_NUMBER',
													'USER_ID'			=> 'TPL.USER_ID',
													'USER_BANK_CODE' 	=> 'TPL.USER_BANK_CODE',
													'PROVIDER_ID' 		=> 'TPL.PROVIDER_ID',													
													'REF_NO' 			=> 'TPL.REF_NO',
													'ACCT_NAME' 		=> 'TPL.ACCT_NAME',
												)
										);								
		
		if(!isset($param['fetch']))
		{
			$param['fetch'] = 'fetchAll';
		}
		if(isset($param['USER_ID']))
		{
			$select->where("TPL.USER_ID = ? ", $param['USER_ID']);
		}
		
		$result =  $this->_db->fetchAll($select);
		return $result;
	}

	function getProviderCode($providerId)
	{
		$select = $this->_db->select()
				->from(array('A' => 'M_SERVICE_PROVIDER'),array('PROVIDER_ID','PROVIDER_NAME','PROVIDER_CODE'));	
		$select->where("A.PROVIDER_CODE = ? ", $providerId);
		$select->where('A.PROVIDER_STATUS = ?',1);
		//$select->where('MU.ACCT_NO !="" ');
		$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		
		return $result;
		
	}
	
	function getProviderId($servicetype, $providertype)
	{
		//modify agung
		
		$select = $this->_db->select()
				->from(array('A' => 'M_SERVICE_PROVIDER'),array('PROVIDER_ID','PROVIDER_NAME','PROVIDER_CODE'));
		$select->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('A.PROVIDER_ID','A.PROVIDER_NAME'));		
		$select->where("A.SERVICE_TYPE = ? ", $servicetype);
		$select->where("A.PROVIDER_TYPE = ? ", $providertype);
		$select->where('A.PROVIDER_STATUS = ?',1);
		$select->where('A.PROVIDER_ID != ?', 1158);
		$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		
		return $result;
		
	}
	
	function getProviderName($providerId, $providerType)
	{
		$select	= $this->_db->select()
							->from	(
									array(	'MSC' => 'M_SERVICE_PROVIDER'),
									array(	
											'PROVIDER_ID'	=> 'MSC.PROVIDER_ID',
											'PROVIDER_NAME'	=> 'MSC.PROVIDER_NAME',
										))
							->where("MSC.PROVIDER_ID = ? ", $providerId)
							->where("MSC.PROVIDER_TYPE = ? ", $providerType);
		
		$result =  $this->_db->fetchRow($select);
		return $result;
	}
	
	
	function getProvider($param)
	{
		
		//modify agung
		$select	= $this->_db->select()
		->from	(
					array(	'MSC' => 'M_SERVICE_PROVIDER'),
					array(	
							'PROVIDER_ID'	=> 'MSC.PROVIDER_ID',
							'PROVIDER_CODE'	=> 'MSC.PROVIDER_CODE',
							'PROVIDER_NAME'	=> 'MSC.PROVIDER_NAME',
							'SERVICE_TYPE' 	=> 'MSC.SERVICE_TYPE',
							'PROVIDER_TYPE' => 'MSC.PROVIDER_TYPE',
							'PROVIDER_STATUS' => 'MSC.PROVIDER_STATUS',
						)
				)		
		->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = MSC.PROVIDER_ID', array('*'))			
		->where('MSC.PROVIDER_STATUS = ?',1)
		->where('MU.ACCT_NO !="" ');
				
								
		if(isset($param['SERVICE_TYPE']))
		{
			$select->where("MSC.SERVICE_TYPE = ? ", $this->_db->quote($param['SERVICE_TYPE']));
		}
		if(isset($param['PROVIDER_TYPE']))
		{
			$select->where("MSC.PROVIDER_TYPE = ? ", $this->_db->quote($param['PROVIDER_TYPE']));
		}
		if(isset($param['PROVIDER_ID']))
		{
			$select->where("MSC.PROVIDER_ID = ? ", $param['PROVIDER_ID']);
		}
		if(!isset($param['fetch']))
		{
			$param['fetch'] = 'fetchAll';
		}
		
		$result =  $this->_db->FetchAll($select);
		return $result;
	}
	
	public function getSgoProductCode($getProvid)
	{	
		$select = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE'));
		$select->where("A.PROVIDER_ID = ? ", $getProvid);
		$select->where("A.CATALOG_STATUS = ? ", 'A');
		//$select->where("A.NOMINAL > 0");
		//$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		//Zend_Debug::dump($result);
		return $result;
		
	}
	
	public function getSgoProductCodeNominal($getProvid)
	{	
		$select = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE'));
		$select->where("A.SGO_PRODUCT_CODE = ? ", $getProvid);
		$select->where("A.CATALOG_STATUS = ? ", 'A');
		//$select->where("A.NOMINAL > 0");
		//$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		//Zend_Debug::dump($result);
		return $result;
		
	}
	
	public function getNominal($getProvid, $getSgoProdCode)
	{	
		$select = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE'));
		$select->where("A.PROVIDER_ID = ? ", $getProvid);
		$select->where("A.SGO_PRODUCT_CODE = ? ", $getSgoProdCode);
		$select->where("A.CATALOG_STATUS = ? ", 'A');
		//$select->where("A.NOMINAL > 0");
		// echo $select;
		//$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		//Zend_Debug::dump($result);
		
		if(count($result) < 1){
			return FALSE;
		}
		else{
			return TRUE;	
		}
	}
	
	function getCharges($provId)
	{
		$chargesArr = array();
		$result = array();
		$chargesArr = $this->_db->SELECT()
													->FROM(array('M_CHARGES_PROVIDER'), array('CHARGES_CCY', 'CHARGES_AMT', 'CHARGES_AMT_MIN', 'CHARGES_AMT_MAX'))
													->WHERE('PROVIDER_ID = ?',$provId)
													->QUERY()->fetchAll();
		if($chargesArr){
			foreach($chargesArr as $arr)
			{
				$result[$arr['CHARGES_CCY']] = $arr;
			}
		}
		return $result;
	}
	
	function getCustomerData($param)
	{
		//Zend_Debug::dump($param);die;
		$select	= $this->_db->select()
								->from	(array(	'MCA' => 'M_CUSTOMER_ACCT'),
										 array(	'ACCT_NO'		  => 'MCA.ACCT_NO',
												'CCY_ID'		  => 'MCA.CCY_ID',
												'ACCT_NAME'	 	  => 'MCA.ACCT_NAME',
												'ACCT_ALIAS_NAME' => 'MCA.ACCT_ALIAS_NAME',
												'ACCT_TYPE' 	  => 'MCA.ACCT_TYPE',
										 		'ACCT_DESC' 	  => 'MCA.ACCT_DESC',
												'FREEZE_STATUS'	  => 'MCA.FREEZE_STATUS',
												'MAXLIMIT' 	  	  => 'MML.MAXLIMIT',
											   ))
								->joinLeft	(array('MML' => 'M_MAKERLIMIT'), 'MCA.ACCT_NO = MML.ACCT_NO', array());
		if(isset($param['CUST_ID']))
		{	
			$select->where("MCA.CUST_ID = ? ", $param['CUST_ID']);    	
		}
		if(isset($param['ACCT_NO']))
		{	
			$select->where("MCA.ACCT_NO = ? ", $param['ACCT_NO']);    	
		}
		
		
		
		$result =  $this->_db->fetchRow($select);
		return $result;
	}
	
	function getConvertHuruf($angka)
	{		
		$angka = str_replace("A","01",$angka);
		$angka = str_replace("B","02",$angka);
		$angka = str_replace("C","03",$angka);
		$angka = str_replace("D","04",$angka);
		$angka = str_replace("E","05",$angka);
		$angka = str_replace("F","06",$angka);
		$angka = str_replace("G","07",$angka);
		$angka = str_replace("H","08",$angka);
		$angka = str_replace("I","09",$angka);
		$angka = str_replace("J","10",$angka);
		$angka = str_replace("K","11",$angka);
		$angka = str_replace("L","12",$angka);
		$angka = str_replace("M","13",$angka);
		$angka = str_replace("N","14",$angka);
		$angka = str_replace("O","15",$angka);
		$angka = str_replace("P","16",$angka);
		$angka = str_replace("Q","17",$angka);
		$angka = str_replace("R","18",$angka);
		$angka = str_replace("S","19",$angka);
		$angka = str_replace("T","20",$angka);
		$angka = str_replace("U","21",$angka);
		$angka = str_replace("V","22",$angka);
		$angka = str_replace("W","23",$angka);
		$angka = str_replace("X","24",$angka);
		$angka = str_replace("Y","25",$angka);
		$angka = str_replace("Z","26",$angka);
		return $angka;
		
		$angkaasli = $angka; //Inputan
		getConvertHuruf($angkaasli); //Hasil HURUF menjadi ANGKA
	}

}