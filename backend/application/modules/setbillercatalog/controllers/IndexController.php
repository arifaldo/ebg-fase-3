<?php


require_once 'Zend/Controller/Action.php';

class setbillercatalog_IndexController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model 			 = new biller_Model_Biller();
		$billerCharges = new setbillercharges_Model_Setbillercharges();
		$providerArr 	 = $billerCharges->getServiceProvider();
    	$options = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));
    	//$options = array_combine(array_values($this->_masterStatus['code']),array_values($this->_masterStatus['desc']));
		unset($options[3]);
		array_unshift($options,'---'.$this->language->_('Any Value').'---');
		$this->view->options = $options;
		$this->view->var = $providerArr;	
		
		$fields = array	(
							'Biller Code'  			=> array	(
																	'field' => 'PROVIDER_NAME',
																	'label' => $this->language->_('Product Name'),
																	'sortable' => true
																),
		
						);

		$filterlist = array("PRODUCT_NAME");
		
		$this->view->filterlist = $filterlist;
		
						
		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'PRODUCT_NAME'  	=> array('StripTags','StringTrim','StringToUpper'),
	                      );
	                      
	    $validator = array('filter' 		=> array(),
	                       'PRODUCT_NAME'  	=> array(),
	      
	                      );
	    $dataParam = array("PRODUCT_NAME");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
			
		}             
		$fParam = array();	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    $filter = $zf_filter->getEscaped('filter');
		$billerName = $fParam['billerName'] = html_entity_decode($zf_filter->getEscaped('PRODUCT_NAME'));

		
		$page = (Zend_Validate::is($page,'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $fParam['sortBy'] = $sortBy;
		$this->view->sortDir     = $fParam['sortDir'] = $sortDir;
	
		if($filter = TRUE)
		{
			$this->view->fDateTo    = $dateto;
			$this->view->ufDateTo    = $udateto;
			$this->view->fDateFrom  = $datefrom;
			$this->view->ufDateFrom  = $udatefrom;
			 		    
			if($billerName)
			{
	       		$this->view->billerName = $billerName;
			}			
		}
		else
		{
			$fParam['billerName'] = '';
		}

		$data = $model->getBillerCatalog($fParam,$sortBy,$sortDir,$filter);
		
    	$this->paging($data);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	if($csv || $pdf)
    	{
    		$arr = $data;
    		
    		foreach($arr as $key=>$value)
			{
				unset($arr[$key]["PROVIDER_ID"]);
				$arr[$key]["ACCT_UPDATED"] = Application_Helper_General::convertDate($value["ACCT_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$arr[$key]["ACCT_SUGGESTED"] = Application_Helper_General::convertDate($value["ACCT_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
    		// Zend_Debug::dump($arr);die;
	    	if($csv)
			{
				Application_Helper_General::writeLog('VBIC','Download CSV Setup Biller Catalog');
				$this->_helper->download->csv(array_keys($fields),$arr,null,'Setup Biller Charges');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('VBIC','Download PDF Setup Biller Catalog');
				$this->_helper->download->pdf(array_keys($fields),$arr,null,'Setup Biller Charges');
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('VBIC','View Biller Catalog');
    	}

    	if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}
}