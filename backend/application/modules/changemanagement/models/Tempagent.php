<?php
/**
 * Tempagent model
 *
 * @author
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';

class Changemanagement_Model_Tempagent extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = '';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null){
		//user password = autogenerate
		//perlu insert ke T_REQ_PIN_MAILER
		//query from TEMP_USER

		$user = $this->dbObj->select()
			->from('TEMP_USER_LKP')
  			->where('CHANGES_ID = ?',$this->_changeId)
  			->query()
  			->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Agent)';
			return false;
        }

		//--------------------------------INSERT TO TABLE M_USER----------------------------------------------------------
		$insertArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$insertArr['USER_CREATED']     = new Zend_Db_Expr('GETDATE()');
		//$insertArr['USER_CREATEDBY']   = $actor;
		$insertArr['USER_UPDATED']     = new Zend_Db_Expr('GETDATE()');
		$insertArr['USER_UPDATEDBY']   = $actor;
		//$insertArr['USER_ISNEW']       = 1;
		$insertArr['USER_STATUS'] = 1;

		try{
			$userins = $this->dbObj->insert('M_USER_LKP',$insertArr);
		}catch(Exception $err){
			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($user,__FILE__.__LINE__,FALSE));
			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($insertArr,__FILE__.__LINE__,FALSE));
			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($err->getMessage(),__FILE__.__LINE__,FALSE));
			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($err,__FILE__.__LINE__,FALSE));
		}

		if(!(boolean)$userins){
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Agent)';
			return false;
		}
		//-------------------------------END INSERT TO TABLE M_USER-------------------------------------------------------

		$flag = "approved";
        $deleteChanges = $this->deleteNew($flag);
		if(!$deleteChanges) return false;


		$tempsms = $this->dbObj->select()
			->from('M_SETTING')
  			->where('SETTING_ID = "ftemplate_approveagent"')
  			->query()->fetchAll();

		$bankphone = $this->dbObj->select()
			->from('M_SETTING')
			->where('SETTING_ID = "master_bank_callcenter"')
			->query()->fetchAll();

  // 		$var = array('[[userAcct]]' => $user['USER_ACCT_NO'],'[[bankPhone]]' => $bankphone[0]['SETTING_VALUE']);
		// $newMessage = strtr($tempsms[0]['SETTING_VALUE'],$var);
		// $sms = new Service_SMS($user['USER_MOBILE_PHONE'],$newMessage);
		// $type = 'Activation'; //so receiver is INFOBIP
		// $sms->OutgoingSMS($type);

		return true;
	}

	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  	->from('TEMP_USER_LKP')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Agent)';
			return false;
        }

        //query from M_USER
		$m_user = $this->dbObj->select()
						  	  ->from('M_USER_LKP')
						  	  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($m_user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Master Agent)';
			return false;
        }

        $updateArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>'','USER_ID'=>'','USER_BRANCH_CODE'=>'','USER_STATUS'=>'','USER_CREATED'=>'','USER_CREATEDBY'=>'','USER_MOBILE_PHONE'=>'','USER_ACCT_NO'=>''));

		//update record
		$updateArr['USER_UPDATED']     = new Zend_Db_Expr('GETDATE()');
		$updateArr['USER_UPDATEDBY']   = $actor;

		$whereArr = array('USER_ID = ?'=>$user['USER_ID']);

		$userupdate = $this->dbObj->update('M_USER_LKP',$updateArr,$whereArr);
		 
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

        $deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges) return false;


		return true;
	}

	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER_LKP')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Agent)';
			return false;
        }

	    //query from M_USER
		$m_user = $this->dbObj->select()
						  ->from('M_USER_LKP')
						  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($m_user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Master Agent)';
			return false;
        }

		$updateArr = array('USER_STATUS' => $user['USER_STATUS']);
		$updateArr['USER_SUGGESTED'] = $user['USER_SUGGESTED'];
		$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
		$updateArr['USER_UPDATED']  = new Zend_Db_Expr('GETDATE()');
		$updateArr['USER_UPDATEDBY'] = $actor;

		$whereArr = array('USER_ID = ?'=>(string)$user['USER_ID']);
		$userupdate = $this->dbObj->update('M_USER_LKP',$updateArr,$whereArr);

		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Agent)';
			return false;
		}

		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER_LKP')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Agent)';
			return false;
        }

        //query from M_USER
		$m_user = $this->dbObj->select()
						  ->from('M_USER_LKP')
						  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($m_user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Master Agent)';
			return false;
        }


		$updateArr = array('USER_STATUS' => $user['USER_STATUS']);
		$updateArr['USER_SUGGESTED']   = $user['USER_SUGGESTED'];
		$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
		$updateArr['USER_UPDATED']     = new Zend_Db_Expr('GETDATE()');
		$updateArr['USER_UPDATEDBY']   = $actor;



		$whereArr = array('USER_ID = ?'=>(string)$user['USER_ID']);
		$userupdate = $this->dbObj->update('M_USER_LKP',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Agent)';
			return false;
		}

		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}

	public function approveDelete($actor = null)
	{
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER_LKP')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Agent)';
			return false;
	    }


        //query from M_USER
		$m_user = $this->dbObj->select()
						  	  ->from('M_USER_LKP')
						  	  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($m_user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Master Agent)';
			return false;
        }

		//if there is no suspect trans, user can be deleted
		// $suspect = $this->dbObj->select()
		// 						->from('T_TRANSACTION')
		// 						->where('USER_ID = ?',(string)$user['USER_ID'])
		// 						->where('TRA_STATUS = 1')
		// 						->query()
		// 						->fetchAll();

		//if(empty($suspect)){
			$updateArr['USER_STATUS']      = $user['USER_STATUS'];
			$updateArr['USER_SUGGESTED']   = $user['USER_SUGGESTED'];
			$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
			$updateArr['USER_UPDATED']     = new Zend_Db_Expr('GETDATE()');
			$updateArr['USER_UPDATEDBY']   = $actor;
			$updateArr['USER_MOBILE_PHONE']  = NULL;

			$whereArr = array('USER_ID = ?'=>(string)$user['USER_ID']);
			$userupdate = $this->dbObj->update('M_USER_LKP',$updateArr,$whereArr);

			$deleteChanges  = $this->deleteDelete();
			if(!$deleteChanges) return false;

			// $tempsms = $this->dbObj->select()
			// 	->from('M_SETTING')
	  // 			->where('SETTING_ID = "ftemplate_deleteagent"')
	  // 			->query()->fetchAll();

  	// 		$bankphone = $this->dbObj->select()
			// 	->from('M_SETTING')
			// 	->where('SETTING_ID = "master_bank_callcenter"')
			// 	->query()->fetchAll();


	  // 		$var = array('[[bank_phone]]' => $bankphone[0]['SETTING_VALUE']);
			// $newMessage = strtr($tempsms[0]['SETTING_VALUE'],$var);
			// $sms = new Service_SMS($m_user['USER_MOBILE_PHONE'],$newMessage);
			// $type = 'Activation'; //so receiver is INFOBIP
			// $sms->OutgoingSMS($type);
		  

			return true;
		// }
		// else{
		// 	$this->_errorMsg = "Gagal menghapus nasabah. Nasabah masih memiliki transaksi yang berstatus suspect.";
		// 	return false;
		// }
	}

    /**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew($flag = null) {
		$user = $this->dbObj->select()
							->from('TEMP_USER_LKP')
				  			->where('CHANGES_ID = ?',$this->_changeId)
				  			->query()
				  			->fetch(Zend_Db::FETCH_ASSOC);

		$mobilePhone = $user['USER_MOBILE_PHONE'];

		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER_LKP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$this->dbObj->delete('TEMP_FPRIVI_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		if(empty($flag)){ //if called from reject
			$tempsms = $this->dbObj->select()
				->from('M_SETTING')
	  			->where('SETTING_ID = "ftemplate_rejectagent"')
	  			->query()->fetchAll();

  			$bankphone = $this->dbObj->select()
				->from('M_SETTING')
				->where('SETTING_ID = "master_bank_callcenter"')
				->query()->fetchAll();


	  		$var = array('[[bank_phone]]' => $bankphone[0]['SETTING_VALUE']);
			$newMessage = strtr($tempsms[0]['SETTING_VALUE'],$var);
			$sms = new Service_SMS($mobilePhone,$newMessage);
			$type = 'Activation'; //so receiver is INFOBIP
			$sms->OutgoingSMS($type);
		}

		return true;
	}

	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		$userdelete = $this->dbObj->delete('TEMP_USER_LKP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$this->dbObj->delete('TEMP_FPRIVI_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}

/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {
		$userdelete = $this->dbObj->delete('TEMP_USER_LKP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}

	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER_LKP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}

	public function deleteDelete()
	{
		$userdelete = $this->dbObj->delete('TEMP_USER_LKP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}



    public function approveDeactivate(){

	}

	public function approveActivate(){

	}

    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}




}