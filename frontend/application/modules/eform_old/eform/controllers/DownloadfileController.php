<?php

class eform_DownloadfileController extends Application_Main
{

    public function indexAction()
    {

        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");

        if ($bg_number == "" && $download == "") {
            return $this->_redirect("/home/dashboard");
        }

        $attahmentDestination = UPLOAD_PATH . '/document/submit/';


        if ($download == "specialformat") {

            $getSpecialFormatFile = $this->_db->select()
                ->from(["A" => "TEMP_BANK_GUARANTEE"], ["SPECIAL_FORMAT_DOC"])
                ->where("BG_REG_NUMBER = '$bg_number'")
                ->query()->fetchAll();

            $getFileName = $getSpecialFormatFile[0]["SPECIAL_FORMAT_DOC"];

            return $this->_helper->download->file($getFileName, $attahmentDestination . $getFileName);
        }

        $checkOthersAttachment = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
            ->where("BG_REG_NUMBER = '$bg_number'")
            ->query()->fetchAll();

        if (count($checkOthersAttachment) == 0) {
            return $this->_redirect("/home/dashboard");
        }

        $getFileName = $checkOthersAttachment[$download]["BG_FILE"];

        return $this->_helper->download->file($getFileName, $attahmentDestination . $getFileName);
    }

    public function specialAction()
    {
        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");

        if ($bg_number == "" && $download == "") {
            return $this->_redirect("/home/dashboard");
        }

        $attahmentDestination = UPLOAD_PATH . '/document/submit/';


        $getSpecialFormatFile = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE"], ["SPECIAL_FORMAT_DOC"])
            ->where("BG_REG_NUMBER = '$bg_number'")
            ->query()->fetchAll();

        $getFileName = $getSpecialFormatFile[0]["SPECIAL_FORMAT_DOC"];

        return $this->_helper->download->file($getFileName, $attahmentDestination . $getFileName);
    }
}
