<?php

class virtualpayment_InstitutionsController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';
	
	
	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
	}
	
	public function indexAction (){
		
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
				'INST_ID'	=> array(
						'field'	=> 'INST_ID',
						'label'	=> $this->language->_('Institutions ID'),
						'sortable'	=> true
				),
				'INST_NAME'	=> array(
						'field' => 'INST_NAME',
						'label' => $this->language->_('Institutions Name'),
						'sortable'	=> true
				),
		
		);
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($fields, 'field');
		
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[0]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
//		unset($stringParam['module']);
//		unset($stringParam['controller']);
//		unset($stringParam['action']);
//		unset($stringParam['filter']);
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
				'INST_ID'			=> array(),
				'INST_NAME'			=> array(),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		
		$INST_ID = $this->_getParam('INST_ID');
		$INST_NAME = $this->_getParam('INST_NAME');
		$this->view->institutions_id = $INST_ID;
		$this->view->institutions_name = $INST_NAME;
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
		
			$select = $this->_db->select()
			->from(
					array('M_INSTITUSI'),
					array('INST_ID','INST_NAME')
			)
			->order($sortBy.' '.$sortDir)
			;
			//$select->where('INST_NAME like "%pajak%"');
			
			if (!empty($INST_ID)){
				$select->where('INST_ID like '.$this->_db->quote('%'.$INST_ID.'%'));
			}
			
			if (!empty($INST_NAME)){
				$select->where('INST_NAME like '.$this->_db->quote('%'.$INST_NAME.'%'));
			}
				
			// 			echo $select;
			$data = $this->_db->fetchAll($select);
//			Zend_Debug::dump($data);
		}
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
		$this->paging($data);
	}
	
}
