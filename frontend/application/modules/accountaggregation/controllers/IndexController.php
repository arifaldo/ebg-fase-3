<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
class accountaggregation_IndexController extends Application_Main 
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		
		// $custId = $this->_custIdLogin;
		$this->_helper->layout()->setLayout('newlayout');
		$model = new accountaggregation_Model_Accountaggregation();

		$fields1 = array(
					'acct'  => array('field' => 'ACCT_NO',
										   'label' => $this->language->_('Account No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'ccy'   => array('field'    => 'CCY_ID',
										  'label'    => $this->language->_('CCY'),
										  'sortable' => true),
					'available'   => array('field'    => 'AVAILABLE_BALANCE',
										  'label'    => $this->language->_('Available Balance'),
										  'sortable' => true),
		            'equivalen'   => array('field'    => 'OUTSTANDING',
                        		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                        		        'sortable' => true),
		);

		$fields2 = array(
					'depo_acct'  => array('field' => 'DEPOSIT_NO',
										   'label' => $this->language->_('Deposit No'),
										   'sortable' => true),
					'protype'   => array('field'    => 'PROD_TYPE',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'depo_ccy'   => array('field'    => 'CCY_ID',
										  'label'    => $this->language->_('CCY'),
										  'sortable' => true),
					'amount'   => array('field'    => 'AVAIL_BAL',
										  'label'    => $this->language->_('Available Balance'),
										  'sortable' => true),
		            'equivalen'   => array('field'    => 'EQUIVALEN',
		                                  'label'    => $this->language->_('Ekuivalent (IDR)'),
		                                  'sortable' => true),
		);

		$fields3 = array(
					'cardno'  => array('field' => '',
										   'label' => $this->language->_('Card No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'creditlimit'   => array('field'    => '',
										  'label'    => $this->language->_('Credit Limit'),
										  'sortable' => true),
					'closingbal'   => array('field'    => '',
										  'label'    => $this->language->_('Closing Balance'),
										  'sortable' => true),
		);



		$fields4 = array(
					'loan_acct'  => array('field' => 'ACCT_NO',
										   'label' => $this->language->_('Account No'),
										   'sortable' => true),
					'protype'   => array('field'    => 'PROD_TYPE',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
		            'loan_ccy'   => array('field'    => 'CCY_ID',
                            		        'label'    => $this->language->_('CCY'),
                            		        'sortable' => true),
					'plafond'   => array('field'    => 'PLAFOND',
										  'label'    => $this->language->_('Plafond'),
										  'sortable' => true),
					'outstanding'   => array('field'    => 'OUTSTANDING',
										  'label'    => $this->language->_('Outstanding'),
										  'sortable' => true),
		             'equivalen'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                            		        'sortable' => true),
		);

		$fields5 = array(
        		    'ccy'  => array('field' => 'ACCT_NO',
                            		        'label' => $this->language->_('CCY'),
                            		        'sortable' => true),
        		    'personalaccount'   => array('field'    => 'PROD_TYPE',
                            		        'label'    => $this->language->_('Personal Account'),
                            		        'sortable' => true),
        		    'depositaccount'   => array('field'    => 'PLAFOND',
                            		        'label'    => $this->language->_('Deposit Account'),
                            		        'sortable' => true),
        		    'loanaccount'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Loan Account'),
                            		        'sortable' => true),
		            'networth'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Net Worth'),
                            		        'sortable' => true),
		            'equivalen'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                            		        'sortable' => true),
		);

		$sortByParam  = $this->_getParam('sortby');
		$sortDirParam = $this->_getParam('sortdir');


		//check param for personal table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields1)))){
			$sortBy1  =  $fields1[$sortByParam]['field'];
			$sortDir1 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy1  = $fields1[key($fields1)]['field'];
			$sortDir1 = 'asc';
		}

		// $custId = $this->_custIdLogin;
		// var_dump($custId); die;
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts(array("ACCT_TYPE" => array('10','20')));	// show acc in IDR only
		//print_r($ACBENEF_CURRENCY);die;
		//die;
		if(!empty($AccArr)){
		$srcData = $this->_db->select()
		->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE'))
		->where('ACCT_NO = ?', $AccArr['0']['ACCT_NO'])
		->limit(1)
		;

		$acsrcData = $this->_db->fetchRow($srcData);
		// print_r($acsrcData);die;
		if(!empty($acsrcData)){
		    $accsrc = $acsrcData['ACCT_NO'];
		    $accsrctype = $acsrcData['ACCT_TYPE'];
		}
		}
		//print_r($AccArr);die('here');
		$svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
		$resultKursEx = $svcInquiry->rateInquiry();

		//rate inquiry for display
		$kurssell = '';
		$kurs = '';
		$book = '';
		if($resultKursEx['ResponseCode']=='00'){
		    $kursList = $resultKursEx['DataList'];
		    $kurssell = '';
		    $kurs = '';
// 		    print_r($resultKursEx['DataList']);die;
		    $this->view->rateKurs 		= $resultKursEx['DataList'];

		}else{

		    $this->view->rateKurs 		= array();

		}

		$sorting1 = $sortBy1.' '.$sortDir1;
		$temp = $this->_db->fetchAll(
					$this->_db->select()->distinct()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->group('A.ACCT_NO')
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
						 // echo $temp;d
		//$temp = $model->getUserAccount(array('userId'=>$this->_userIdLogin),$sorting1);
		//print_r($temp);die;
		if (count($temp) > 0)
		{
			$datapers = array();

			foreach ( $temp as $key=>$row ) {
			//	print_r($row);die;
				//$systemBalance = new SystemBalance($row['CUST_ID'],$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
				//$systemBalance->setFlag(false);
				//$systemBalance->checkBalance();
				$Account = new Account($row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']),null,$row['ACCT_TYPE'],$row['ACCT_CIF'],$this->_userIdLogin);
				$Account->setFlag(false);
//die;
				$Account->checkBalance();

				if($Account->getCoreProductPlan() != NULL){
					// 					print_r('here');

					$product = $model->getProduct($Account->getCoreProductPlan(),$row ['ACCT_TYPE']);
					$product_name = $product['0']['PRODUCT_NAME'];
				}else{
					// 					print_r('here1');
					$product_name = 'N/A';
				}
				// 				print_r($product);die;
				$datapers[$key]['ACCT_PRODUCT_NAME'] = $product_name;
				// 				$product = $model->getProduct($Account->getCoreProductPlan(),$row ['PRODUCT_CODE']);
				// 				$datapers[$key]['ACCT_PRODUCT_NAME'] = $product['0']['PRODUCT_NAME'];
				$datapers[$key]['ACCT_NO'] = $row['ACCT_NO'];
				$datapers[$key]['CCY_ID'] = $row['CCY_ID'];
				$datapers[$key]['PRODUCT_NAME'] = $row['PRODUCT_NAME'];
				$datapers[$key]['PRODUCT_CODE'] = $row['PRODUCT_CODE'];
				$datapers[$key]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
			}
		}
// 		$temp = $model->getUserAccount(array('userId'=>$this->_userIdLogin),$sorting1);

// 		$temp = $this->_db->fetchAll(
// 				$this->_db->select()
// 				->from(array('A' => 'M_CUSTOMER_ACCT'))
// 				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
// 				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
// 				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
// 				->where("A.ACCT_STATUS = 1")
// 				->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
// 				->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
// 				->where("C.MAXLIMIT > 0")
// 				->order('B.GROUP_NAME DESC')
// 				->order('A.ORDER_NO ASC')
// 		);

// 		$isGroupPersonal = false;
// 		if(count($temp))
// 		{
// 			foreach($temp as $row)
// 			{
// 				if($row['GROUP_ID'])
// 				{
// 					$isGroupPersonal = true;
// 				}
// 			}
// 		}
		$userId = $this->_userIdLogin;
		$model = new accountstatement_Model_Accountstatement();
		$userCIF = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_USER'))
				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
				->limit(1)
		);
		//die('here');
		$param['userId'] = $userId;
		$userAcctInfo = $model->getCustAccount($param);
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];
		//print_r($accountCcy);
		//print_r($userCIF['CUST_CIF']);
		//print_r($accountType);
		//print_r($accountNo);
		//die;
		if(!empty($userAcctInfo)){
		$Account = new Account($accountNo, $accountCcy, $accountType, $userCIF['CUST_CIF'], $userId);
		$Account->setFlag(false);
		$resLoan = $Account->checkBalanceLoan();
		$accountListDataLoan = $resLoan['AccountList'];

		$resDeposit = $Account->checkBalanceDeposit();
		//print_r($resDeposit);die;
		$accountListDataDeposit = $resDeposit['AccountList'];

// 		$tempDeposit = array();
// 		$tempDeposit[0]['DEPOSIT_NO'] = "20015001303311";
// 		$tempDeposit[0]['PROD_TYPE'] = "My Depo";
// 		$tempDeposit[0]['CCY_ID'] = "IDR";
// 		$tempDeposit[0]['AVAIL_BAL'] = "20,000,000.00";

// 		$tempDeposit[1]['DEPOSIT_NO'] = "20015001304344";
// 		$tempDeposit[1]['PROD_TYPE'] = "My Depo 2";
// 		$tempDeposit[1]['CCY_ID'] = "IDR";
// 		$tempDeposit[1]['AVAIL_BAL'] = "35,000,000.00";

		if(!empty($accountListDataDeposit)){
			$tempDeposit = $accountListDataDeposit;
// 			print_r($tempDeposit);die;
			if (count($tempDeposit) > 0)
			{
				$datadeposit = array();
				$tdep = array();
				$totaldepo = 0;
				foreach ( $tempDeposit as $key=>$row ) {
					if (isset($row ['accountNo'])){

						$productList = $model->getProduct($row ['planCode'],$row ['productPlan']);
						$product_name = $productList[0]['PRODUCT_NAME'] == NULL ? "N/A" : $productList[0]['PRODUCT_NAME'];

						$datadeposit[$key]['ACCT_PRODUCT_NAME'] = $product_name;
						$datadeposit[$key]['ACCT_NO'] = $row['accountNo'];
						$datadeposit[$key]['CCY_ID'] = $row['currency'];
						//					$datadeposit[$key]['PRODUCT_NAME'] = $row['PRODUCT_NAME'];
						$datadeposit[$key]['AVAILABLE_BALANCE'] = $row['amount'];
						$kurs = '';
						foreach ($resultKursEx['DataList'] as $val){
						    if($val['currency']==$row['currency']){
						        $kurs = $val['sell'];
						    }
						}
						$plain_amount = str_replace(',','',$row['amount']);
						if(!empty($kurs)){
						    $kurs = str_replace(',','',$kurs);
						    $equivalen = $plain_amount*$kurs;

						}else if($row['currency'] == 'IDR'){
						    $equivalen = str_replace(',','',$row['amount']);
						}else{
						    $equivalen = 'N/A';
						}


						$datadeposit[$key]['EQUIVALEN'] = $equivalen;

						$totaldepo += $equivalen;

						if($row['currency']=='IDR'){
						$tdep[$row['currency']] += $plain_amount;
						}else if($row['currency']=='USD'){
						$tdep[$row['currency']] += $plain_amount;
						}else if($row['currency']=='JPY'){
						$tdep[$row['currency']] += $plain_amount;
						}
					}
				}
			}

		}else{
			$dataDeposit = array();
		}
	}

		$this->view->totaldepo = $totaldepo;
		$this->view->tdep = $tdep;
//
		if(!empty($accountListDataLoan)){
			$tempLoan = $accountListDataLoan;
			if (count($tempLoan) > 0)
			{
				$dataloan = array();
				$tloan = array();
				$totalloan = 0;
// 				print_r($tempLoan);die;
				foreach ( $tempLoan as $key=>$row ) {
					if (isset($row ['accountNo'])){
						//print_r($row ['productPlan']);

						$productList = $model->getProduct($row ['planCode'],$row ['productPlan']);
						//print_r($productList[0]['PRODUCT_NAME']);die;
						if(!empty($productList[0]['PRODUCT_NAME'])){
						$productList[0]['PRODUCT_NAME'] = $productList[0]['PRODUCT_NAME'];
						}else{
						$productList[0]['PRODUCT_NAME'] = 'N/A';
						}
						$product_name = $productList[0]['PRODUCT_NAME'];
						//print_r($product_name);die;
						$dataloan[$key]['ACCT_NO'] 		= $row['accountNo'];
						$dataloan[$key]['CCY_ID'] 		= $row['currency'];
						$dataloan[$key]['PRODUCT_NAME'] = $product_name;
						$dataloan[$key]['PLAFOND'] 		= $row['plafond'];
						$dataloan[$key]['OUTSTANDING'] 	= $row['outstanding'];
						$plain_amount = str_replace(',','',$row['outstanding']);
						$kurs = '';
						foreach ($resultKursEx['DataList'] as $val){
						    if($val['currency']==$row['currency']){
						        $kurs = $val['sell'];
						    }
						}
// 						$plain_amount = str_replace(',','',$row['amount']);
						if(!empty($kurs)){
						     $kurs = str_replace(',','',$kurs);
						    $equivalen = $plain_amount*$kurs;
						    // $equivalen = Application_Helper_General::displayMoney($equivalen);
						}else if($row['currency'] == 'IDR'){
						    $equivalen = str_replace(',','',$row['outstanding']);
						}else{
						    $equivalen = 'N/A';
						}


						$dataloan[$key]['EQUIVALEN'] = $equivalen;
						$totalloan += $equivalen;
						if($row['currency']=='IDR'){
						    $tloan[$row['currency']] += $plain_amount;
						}else if($row['currency']=='USD'){
						    $tloan[$row['currency']] += $plain_amount;
						}else if($row['currency']=='JPY'){
						    $tloan[$row['currency']] += $plain_amount;
						}
					}
				}
			}
		}else{
			$dataloan = array();
			$tempLoan = array();
		}

		$this->view->totalloan = $totalloan;

		array_push($tloan,"");
		$this->view->tloan = $tloan;
// 		$tempLoan[0]['ACCT_NO'] = "100345721001";
// 		$tempLoan[0]['PROD_TYPE'] = "Pinjaman BJB";
// 		$tempLoan[0]['PLAFOND'] = "100,000,000.00";
// 		$tempLoan[0]['OUTSTANDING'] = "60,000,000.00";

// 		$tempLoan[1]['ACCT_NO'] = "1003457823004";
// 		$tempLoan[1]['PROD_TYPE'] = "Pinjaman BJB";
// 		$tempLoan[1]['PLAFOND'] = "150,000,000.00";
// 		$tempLoan[1]['OUTSTANDING'] = "45,000,000.00";


		//check param for deposit table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields2)))){
			$sortBy2  =  $fields2[$sortByParam]['field'];
			$sortDir2 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy2  = $fields2[key($fields2)]['field'];
			$sortDir2 = 'asc';
		}

		//any query + logic for deposit

		//check param for cc table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields3)))){
			$sortBy3  =  $fields3[$sortByParam]['field'];
			$sortDir3 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy3  = $fields3[key($fields3)]['field'];
			$sortDir3 = 'asc';
		}

		//any query + logic for cc

		//check param for loan table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields4)))){
			$sortBy4  =  $fields4[$sortByParam]['field'];
			$sortDir4 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy4  = $fields4[key($fields4)]['field'];
			$sortDir4 = 'asc';
		}

		//check param for loan table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields5)))){
		    $sortBy5  =  $fields5[$sortByParam]['field'];
		    $sortDir5 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
		    $sortBy5  = $fields5[key($fields5)]['field'];
		    $sortDir5 = 'asc';
		}

		//any query + logic for deposit


//die;

		$logDesc = 'Viewing';


		Application_Helper_General::writeLog('BAIQ',$logDesc);
// 		print_r($temp);die;
		$this->view->custId = $this->_custIdLogin;
		$this->view->isGroupPersonal = $isGroupPersonal;
		// $this->view->isGroupDeposit = $isGroupDeposit;
		// $this->view->isGroupLoan = $isGroupLoan;


		$this->view->userId = $this->_userIdLogin;
		$this->view->personal = $datapers;
		//$this->view->creditcard = $datacc;


// 		$this->view->deposit = $tempLoan;
		$this->view->deposit = $datadeposit;
		$this->view->loan = $dataloan;

		$this->view->fields1 = $fields1; //personal
		$this->view->fields2 = $fields2; //deposit
		$this->view->fields3 = $fields3; //credit card
		$this->view->fields4 = $fields4; //loan
		$this->view->fields5 = $fields5; //loan

		$this->view->sortBy1 = $sortBy1;
		$this->view->sortBy2 = $sortBy2;
		$this->view->sortBy3 = $sortBy3;
		$this->view->sortBy4 = $sortBy4;
		$this->view->sortBy5 = $sortBy5;
//die;
		$this->view->sortDir1 = $sortDir1;
		$this->view->sortDir2 = $sortDir2;
		$this->view->sortDir3 = $sortDir3;
		$this->view->sortDir4 = $sortDir4;
		$this->view->sortDir5 = $sortDir5;
	}


	public function newAction()
	{
		// $custId = $this->_custIdLogin;
		$this->_helper->layout()->setLayout('newlayout');
		$model = new accountaggregation_Model_Accountaggregation();

		$fields1 = array(
					'acct'  => array('field' => 'ACCT_NO',
										   'label' => $this->language->_('Account No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'ccy'   => array('field'    => 'CCY_ID',
										  'label'    => $this->language->_('CCY'),
										  'sortable' => true),
					'available'   => array('field'    => 'AVAILABLE_BALANCE',
										  'label'    => $this->language->_('Available Balance'),
										  'sortable' => true),
		            'equivalen'   => array('field'    => 'OUTSTANDING',
                        		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                        		        'sortable' => true),
		);

		$fields2 = array(
					'depo_acct'  => array('field' => 'DEPOSIT_NO',
										   'label' => $this->language->_('Deposit No'),
										   'sortable' => true),
					'protype'   => array('field'    => 'PROD_TYPE',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'depo_ccy'   => array('field'    => 'CCY_ID',
										  'label'    => $this->language->_('CCY'),
										  'sortable' => true),
					'amount'   => array('field'    => 'AVAIL_BAL',
										  'label'    => $this->language->_('Available Balance'),
										  'sortable' => true),
		            'equivalen'   => array('field'    => 'EQUIVALEN',
		                                  'label'    => $this->language->_('Ekuivalent (IDR)'),
		                                  'sortable' => true),
		);

		$fields3 = array(
					'cardno'  => array('field' => '',
										   'label' => $this->language->_('Card No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'creditlimit'   => array('field'    => '',
										  'label'    => $this->language->_('Credit Limit'),
										  'sortable' => true),
					'closingbal'   => array('field'    => '',
										  'label'    => $this->language->_('Closing Balance'),
										  'sortable' => true),
		);



		$fields4 = array(
					'loan_acct'  => array('field' => 'ACCT_NO',
										   'label' => $this->language->_('Account No'),
										   'sortable' => true),
					'protype'   => array('field'    => 'PROD_TYPE',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
		            'loan_ccy'   => array('field'    => 'CCY_ID',
                            		        'label'    => $this->language->_('CCY'),
                            		        'sortable' => true),
					'plafond'   => array('field'    => 'PLAFOND',
										  'label'    => $this->language->_('Plafond'),
										  'sortable' => true),
					'outstanding'   => array('field'    => 'OUTSTANDING',
										  'label'    => $this->language->_('Outstanding'),
										  'sortable' => true),
		             'equivalen'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                            		        'sortable' => true),
		);

		$fields5 = array(
        		    'ccy'  => array('field' => 'ACCT_NO',
                            		        'label' => $this->language->_('CCY'),
                            		        'sortable' => true),
        		    'personalaccount'   => array('field'    => 'PROD_TYPE',
                            		        'label'    => $this->language->_('Personal Account'),
                            		        'sortable' => true),
        		    'depositaccount'   => array('field'    => 'PLAFOND',
                            		        'label'    => $this->language->_('Deposit Account'),
                            		        'sortable' => true),
        		    'loanaccount'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Loan Account'),
                            		        'sortable' => true),
		            'networth'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Net Worth'),
                            		        'sortable' => true),
		            'equivalen'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                            		        'sortable' => true),
		);

		$sortByParam  = $this->_getParam('sortby');
		$sortDirParam = $this->_getParam('sortdir');


		//check param for personal table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields1)))){
			$sortBy1  =  $fields1[$sortByParam]['field'];
			$sortDir1 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy1  = $fields1[key($fields1)]['field'];
			$sortDir1 = 'asc';
		}

		// $custId = $this->_custIdLogin;
		// var_dump($custId); die;
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts(array("ACCT_TYPE" => array('10','20')));	// show acc in IDR only
		//print_r($ACBENEF_CURRENCY);die;
		//die;
		if(!empty($AccArr)){
		$srcData = $this->_db->select()
		->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE'))
		->where('ACCT_NO = ?', $AccArr['0']['ACCT_NO'])
		->limit(1)
		;

		$acsrcData = $this->_db->fetchRow($srcData);
		// print_r($acsrcData);die;
		if(!empty($acsrcData)){
		    $accsrc = $acsrcData['ACCT_NO'];
		    $accsrctype = $acsrcData['ACCT_TYPE'];
		}
		}
		//print_r($AccArr);die('here');
		$svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
		$resultKursEx = $svcInquiry->rateInquiry();

		//rate inquiry for display
		$kurssell = '';
		$kurs = '';
		$book = '';
		if($resultKursEx['ResponseCode']=='00'){
		    $kursList = $resultKursEx['DataList'];
		    $kurssell = '';
		    $kurs = '';
// 		    print_r($resultKursEx['DataList']);die;
		    $this->view->rateKurs 		= $resultKursEx['DataList'];

		}else{

		    $this->view->rateKurs 		= array();

		}

		$sorting1 = $sortBy1.' '.$sortDir1;
		$temp = $this->_db->fetchAll(
					$this->_db->select()->distinct()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->group('A.ACCT_NO')
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
						 // echo $temp;d
		//$temp = $model->getUserAccount(array('userId'=>$this->_userIdLogin),$sorting1);
		//print_r($temp);die;
		if (count($temp) > 0)
		{
			$datapers = array();

			foreach ( $temp as $key=>$row ) {
			//	print_r($row);die;
				//$systemBalance = new SystemBalance($row['CUST_ID'],$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
				//$systemBalance->setFlag(false);
				//$systemBalance->checkBalance();
				$Account = new Account($row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']),null,$row['ACCT_TYPE'],$row['ACCT_CIF'],$this->_userIdLogin);
				$Account->setFlag(false);
//die;
				$Account->checkBalance();

				if($Account->getCoreProductPlan() != NULL){
					// 					print_r('here');

					$product = $model->getProduct($Account->getCoreProductPlan(),$row ['ACCT_TYPE']);
					$product_name = $product['0']['PRODUCT_NAME'];
				}else{
					// 					print_r('here1');
					$product_name = 'N/A';
				}
				// 				print_r($product);die;
				$datapers[$key]['ACCT_PRODUCT_NAME'] = $product_name;
				// 				$product = $model->getProduct($Account->getCoreProductPlan(),$row ['PRODUCT_CODE']);
				// 				$datapers[$key]['ACCT_PRODUCT_NAME'] = $product['0']['PRODUCT_NAME'];
				$datapers[$key]['ACCT_NO'] = $row['ACCT_NO'];
				$datapers[$key]['CCY_ID'] = $row['CCY_ID'];
				$datapers[$key]['PRODUCT_NAME'] = $row['PRODUCT_NAME'];
				$datapers[$key]['PRODUCT_CODE'] = $row['PRODUCT_CODE'];
				$datapers[$key]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
			}
		}
// 		$temp = $model->getUserAccount(array('userId'=>$this->_userIdLogin),$sorting1);

// 		$temp = $this->_db->fetchAll(
// 				$this->_db->select()
// 				->from(array('A' => 'M_CUSTOMER_ACCT'))
// 				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
// 				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
// 				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
// 				->where("A.ACCT_STATUS = 1")
// 				->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
// 				->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
// 				->where("C.MAXLIMIT > 0")
// 				->order('B.GROUP_NAME DESC')
// 				->order('A.ORDER_NO ASC')
// 		);

// 		$isGroupPersonal = false;
// 		if(count($temp))
// 		{
// 			foreach($temp as $row)
// 			{
// 				if($row['GROUP_ID'])
// 				{
// 					$isGroupPersonal = true;
// 				}
// 			}
// 		}
		$userId = $this->_userIdLogin;
		$model = new accountstatement_Model_Accountstatement();
		$userCIF = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_USER'))
				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
				->limit(1)
		);
		//die('here');
		$param['userId'] = $userId;
		$userAcctInfo = $model->getCustAccount($param);
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];
		//print_r($accountCcy);
		//print_r($userCIF['CUST_CIF']);
		//print_r($accountType);
		//print_r($accountNo);
		//die;
		if(!empty($userAcctInfo)){
		$Account = new Account($accountNo, $accountCcy, $accountType, $userCIF['CUST_CIF'], $userId);
		$Account->setFlag(false);
		$resLoan = $Account->checkBalanceLoan();
		$accountListDataLoan = $resLoan['AccountList'];

		$resDeposit = $Account->checkBalanceDeposit();
		//print_r($resDeposit);die;
		$accountListDataDeposit = $resDeposit['AccountList'];

// 		$tempDeposit = array();
// 		$tempDeposit[0]['DEPOSIT_NO'] = "20015001303311";
// 		$tempDeposit[0]['PROD_TYPE'] = "My Depo";
// 		$tempDeposit[0]['CCY_ID'] = "IDR";
// 		$tempDeposit[0]['AVAIL_BAL'] = "20,000,000.00";

// 		$tempDeposit[1]['DEPOSIT_NO'] = "20015001304344";
// 		$tempDeposit[1]['PROD_TYPE'] = "My Depo 2";
// 		$tempDeposit[1]['CCY_ID'] = "IDR";
// 		$tempDeposit[1]['AVAIL_BAL'] = "35,000,000.00";

		if(!empty($accountListDataDeposit)){
			$tempDeposit = $accountListDataDeposit;
// 			print_r($tempDeposit);die;
			if (count($tempDeposit) > 0)
			{
				$datadeposit = array();
				$tdep = array();
				$totaldepo = 0;
				foreach ( $tempDeposit as $key=>$row ) {
					if (isset($row ['accountNo'])){

						$productList = $model->getProduct($row ['planCode'],$row ['productPlan']);
						$product_name = $productList[0]['PRODUCT_NAME'] == NULL ? "N/A" : $productList[0]['PRODUCT_NAME'];

						$datadeposit[$key]['ACCT_PRODUCT_NAME'] = $product_name;
						$datadeposit[$key]['ACCT_NO'] = $row['accountNo'];
						$datadeposit[$key]['CCY_ID'] = $row['currency'];
						//					$datadeposit[$key]['PRODUCT_NAME'] = $row['PRODUCT_NAME'];
						$datadeposit[$key]['AVAILABLE_BALANCE'] = $row['amount'];
						$kurs = '';
						foreach ($resultKursEx['DataList'] as $val){
						    if($val['currency']==$row['currency']){
						        $kurs = $val['sell'];
						    }
						}
						$plain_amount = str_replace(',','',$row['amount']);
						if(!empty($kurs)){
						    $kurs = str_replace(',','',$kurs);
						    $equivalen = $plain_amount*$kurs;

						}else if($row['currency'] == 'IDR'){
						    $equivalen = str_replace(',','',$row['amount']);
						}else{
						    $equivalen = 'N/A';
						}


						$datadeposit[$key]['EQUIVALEN'] = $equivalen;

						$totaldepo += $equivalen;

						if($row['currency']=='IDR'){
						$tdep[$row['currency']] += $plain_amount;
						}else if($row['currency']=='USD'){
						$tdep[$row['currency']] += $plain_amount;
						}else if($row['currency']=='JPY'){
						$tdep[$row['currency']] += $plain_amount;
						}
					}
				}
			}

		}else{
			$dataDeposit = array();
		}
	}

		$this->view->totaldepo = $totaldepo;
		$this->view->tdep = $tdep;
//
		if(!empty($accountListDataLoan)){
			$tempLoan = $accountListDataLoan;
			if (count($tempLoan) > 0)
			{
				$dataloan = array();
				$tloan = array();
				$totalloan = 0;
// 				print_r($tempLoan);die;
				foreach ( $tempLoan as $key=>$row ) {
					if (isset($row ['accountNo'])){
						//print_r($row ['productPlan']);

						$productList = $model->getProduct($row ['planCode'],$row ['productPlan']);
						//print_r($productList[0]['PRODUCT_NAME']);die;
						if(!empty($productList[0]['PRODUCT_NAME'])){
						$productList[0]['PRODUCT_NAME'] = $productList[0]['PRODUCT_NAME'];
						}else{
						$productList[0]['PRODUCT_NAME'] = 'N/A';
						}
						$product_name = $productList[0]['PRODUCT_NAME'];
						//print_r($product_name);die;
						$dataloan[$key]['ACCT_NO'] 		= $row['accountNo'];
						$dataloan[$key]['CCY_ID'] 		= $row['currency'];
						$dataloan[$key]['PRODUCT_NAME'] = $product_name;
						$dataloan[$key]['PLAFOND'] 		= $row['plafond'];
						$dataloan[$key]['OUTSTANDING'] 	= $row['outstanding'];
						$plain_amount = str_replace(',','',$row['outstanding']);
						$kurs = '';
						foreach ($resultKursEx['DataList'] as $val){
						    if($val['currency']==$row['currency']){
						        $kurs = $val['sell'];
						    }
						}
// 						$plain_amount = str_replace(',','',$row['amount']);
						if(!empty($kurs)){
						     $kurs = str_replace(',','',$kurs);
						    $equivalen = $plain_amount*$kurs;
						    // $equivalen = Application_Helper_General::displayMoney($equivalen);
						}else if($row['currency'] == 'IDR'){
						    $equivalen = str_replace(',','',$row['outstanding']);
						}else{
						    $equivalen = 'N/A';
						}


						$dataloan[$key]['EQUIVALEN'] = $equivalen;
						$totalloan += $equivalen;
						if($row['currency']=='IDR'){
						    $tloan[$row['currency']] += $plain_amount;
						}else if($row['currency']=='USD'){
						    $tloan[$row['currency']] += $plain_amount;
						}else if($row['currency']=='JPY'){
						    $tloan[$row['currency']] += $plain_amount;
						}
					}
				}
			}
		}else{
			$dataloan = array();
			$tempLoan = array();
		}

		$this->view->totalloan = $totalloan;

		array_push($tloan,"");
		$this->view->tloan = $tloan;
// 		$tempLoan[0]['ACCT_NO'] = "100345721001";
// 		$tempLoan[0]['PROD_TYPE'] = "Pinjaman BJB";
// 		$tempLoan[0]['PLAFOND'] = "100,000,000.00";
// 		$tempLoan[0]['OUTSTANDING'] = "60,000,000.00";

// 		$tempLoan[1]['ACCT_NO'] = "1003457823004";
// 		$tempLoan[1]['PROD_TYPE'] = "Pinjaman BJB";
// 		$tempLoan[1]['PLAFOND'] = "150,000,000.00";
// 		$tempLoan[1]['OUTSTANDING'] = "45,000,000.00";


		//check param for deposit table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields2)))){
			$sortBy2  =  $fields2[$sortByParam]['field'];
			$sortDir2 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy2  = $fields2[key($fields2)]['field'];
			$sortDir2 = 'asc';
		}

		//any query + logic for deposit

		//check param for cc table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields3)))){
			$sortBy3  =  $fields3[$sortByParam]['field'];
			$sortDir3 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy3  = $fields3[key($fields3)]['field'];
			$sortDir3 = 'asc';
		}

		//any query + logic for cc

		//check param for loan table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields4)))){
			$sortBy4  =  $fields4[$sortByParam]['field'];
			$sortDir4 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy4  = $fields4[key($fields4)]['field'];
			$sortDir4 = 'asc';
		}

		//check param for loan table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields5)))){
		    $sortBy5  =  $fields5[$sortByParam]['field'];
		    $sortDir5 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
		    $sortBy5  = $fields5[key($fields5)]['field'];
		    $sortDir5 = 'asc';
		}

		//any query + logic for deposit


//die;

		$logDesc = 'Viewing';


		Application_Helper_General::writeLog('BAIQ',$logDesc);
// 		print_r($temp);die;
		$this->view->custId = $this->_custIdLogin;
		$this->view->isGroupPersonal = $isGroupPersonal;
		// $this->view->isGroupDeposit = $isGroupDeposit;
		// $this->view->isGroupLoan = $isGroupLoan;


		$this->view->userId = $this->_userIdLogin;
		$this->view->personal = $datapers;
		//$this->view->creditcard = $datacc;


// 		$this->view->deposit = $tempLoan;
		$this->view->deposit = $datadeposit;
		$this->view->loan = $dataloan;

		$this->view->fields1 = $fields1; //personal
		$this->view->fields2 = $fields2; //deposit
		$this->view->fields3 = $fields3; //credit card
		$this->view->fields4 = $fields4; //loan
		$this->view->fields5 = $fields5; //loan

		$this->view->sortBy1 = $sortBy1;
		$this->view->sortBy2 = $sortBy2;
		$this->view->sortBy3 = $sortBy3;
		$this->view->sortBy4 = $sortBy4;
		$this->view->sortBy5 = $sortBy5;
//die;
		$this->view->sortDir1 = $sortDir1;
		$this->view->sortDir2 = $sortDir2;
		$this->view->sortDir3 = $sortDir3;
		$this->view->sortDir4 = $sortDir4;
		$this->view->sortDir5 = $sortDir5;
	}


}
