<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
class notionalpooling_SettingController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti
	protected $_payType;
	public function initController()
	{
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
		//$this->_payType = "'".$this->_beneftype["code"]["domestic"]."' , '".$this->_beneftype["code"]["online"]."'";
		//$this->_payType = "'".$this->_beneftype["code"]["remittance"]."'";
		
		
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		// $model = new predefinedbeneficiary_Model_Predefinedbeneficiary();

		$aliasname = $this->language->_('Alias Name');
    	$beneficiaryaccount = $this->language->_('Destination Name');
    	
    	$nrc = $this->language->_('NRC');
  		$phone = $this->language->_('Phone');
  		$ccy = $this->language->_('CCY');
  		$favorite = $this->language->_('Favorite');
  		$bankcode = $this->language->_('Bank Code');
  		$status = $this->language->_('Status');
  		$bankname = $this->language->_('Bank Name');
  		$delete = $this->language->_('Action');
  		$address = $this->language->_('Address');
  		
  		
  		
		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANKTABLE'));
		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$databank 					= $this->_db->fetchAll($selectbank);
		// print_r($databank);die;
		$bankArr = array();
		foreach ($databank as $key => $value) {
			// print_r($value);die;
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}
		// print_r($bankArr);die;
		$this->view->DEBIT_BANKarr  = $bankArr;
  		// print_r($this->_request->getParams());die;

		if($this->_getParam('error') == '1'){
			$sessionNamespace = new Zend_Session_Namespace('directdebit');
			$error = $sessionNamespace->error;
			// print_r($sessionNamespace);die;
			$this->view->error = $error;
		}

		if($this->_getParam('confirm') == '1'){
			$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
			$content = $sessionNamespace->content;
			$this->view->confirmPage = 1;
			$this->view->COMPANY_ID = $content['COMP_ID'];
			$this->view->COMPANY_NAME = $content['COMP_NAME'];
			$this->view->COMPANY_TYPE = $content['COMP_TYPE'];
			
			$bankdata = $this->_db->select()
					 ->from(array('C' => 'M_BANKTABLE'));
			$bankdata->where("BANK_CODE = ".$this->_db->quote($content['DEBIT_BANK']));
			$databank 					= $this->_db->fetchAll($bankdata);
			
			$this->view->DEBIT_BANK_NAME = $databank['0']['BANK_NAME'];
			$this->view->DEBIT_BANK = $content['DEBIT_BANK'];
			$this->view->DEBIT_ACCOUNT = $content['DEBIT_ACCT'];
			$this->view->ACCT_NAME = $content['ACCT_NAME'];
		}

		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $aliasname,
											   'sortable' => true),*/
						// 'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
						// 					   'label' => $beneficiaryaccount,
						// 					   'sortable' => true),
						// 'comp_id'  => array('field' => 'BENEFICIARY_NAME',
						// 					   'label' => $this->language->_('Company ID'),
						// 					   'sortable' => true),
				
						'debit_bank'  => array('field' => 'BENEFICIARY_ISAPPROVE_disp',
											   'label' => $this->language->_('Bank Name'),
											   'sortable' => true),
						
						'debit_acct'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
											  'label'    => $this->language->_('Bank Account'),
											  'sortable' => false),
						'account_name'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
											  'label'    => $this->language->_('Account Name'),
											  'sortable' => false),
						'alias_name'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
											  'label'    => $this->language->_('Alias Name'),
											  'sortable' => false),
						'prod_type'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
											  'label'    => $this->language->_('Prod Type'),
											  'sortable' => false),
						'debit_ccy'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => false),
						
				);

		$filterlist = array('COMP_ID','DEBIT_BANK','COMP_NAME','DEBIT_ACCT');
		
		$this->view->filterlist = $filterlist;

		$selectcomp = $this->_db->select()
                            ->from('M_CUSTOMER', array('value' => 'CUST_ID','CUST_NAME','CUST_TYPE'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);

        $this->view->custarr = json_encode($tempColumn);
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'COMP_ID'    	=> array('StripTags'),
	                       	'COMP_NAME'  		=> array('StripTags','StringTrim','StringToUpper'),
							'DEBIT_BANK'    => array('StripTags'),
							'DEBIT_ACCT' 		=> array('StripTags','StringTrim','StringToUpper'),
		);

		$dataParam = array('COMP_ID','DEBIT_BANK','COMP_NAME','DEBIT_ACCT');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
			}
		}

		$validators = array(
						'COMP_ID' 	=> array(),
						'COMP_NAME' 	=> array(),
						'DEBIT_BANK' 	=> array(),	
						'DEBIT_ACCT' 	=> array(),		
						);
		
		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue);

		$filter = $this->_getParam('filter');

		$alpha = $this->_getParam('alpha');

		$this->view->currentPage = $page;
		$this->view->sortBy = $param['sortBy'] = $sortBy;
		$this->view->sortDir = $param['sortDir'] = $sortDir;
		$this->view->alpha = $alpha;

		if($filter == TRUE)
		{
			$fcompid = $zf_filter->getEscaped('COMP_ID');
			$fcompname = $zf_filter->getEscaped('COMP_NAME');
			$fdebitbank = $zf_filter->getEscaped('DEBIT_BANK');
			$fdebitacct = $zf_filter->getEscaped('DEBIT_ACCT');

			if($fcompid) $param['fcompid'] = $fcompid;
	        if($fcompname) $param['fcompname'] = $fcompname;
	        if($fdebitbank) $param['fdebitbank'] = $fdebitbank;
	        if($fdebitacct) $param['fdebitacct'] = $fdebitacct;

			$this->view->custid = $fcompid;
			$this->view->custname = $fcompname;
			$this->view->debit_bank = $fdebitbank;
			$this->view->debit_acct = $fdebitacct;





			$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('benef_acct');
			$fName = $zf_filter->getEscaped('benef_name');
			$payType = $zf_filter->getEscaped('benef_type');

			if($fAlias) $param['fAlias'] = $fAlias;
	        if($fAcct) $param['fAcct'] = $fAcct;
	        if($fName) $param['fName'] = $fName;
	        if($payType) $param['payType'] = $payType;

			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->benef_type = $payType;
		}

		$param['user_id'] = $this->_userIdLogin;
		$param['cust_id'] = $this->_custIdLogin;
		$param['payType'] = '4';
		$param['beneLinkage'] = $this->view->hasPrivilege('BLBU');
		/*if(empty($payType))
			$param['payType'] = $this->_payType;*/
		if($alpha) $param['alpha'] = $alpha;

		$select = $this->_db->select()
					 ->from(array('C' => 'T_AGREGATION'),array('*','company'	=> new Zend_Db_Expr("CONCAT(COMP_NAME , ' (' , COMP_ID , ')  ' )")));

		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		// $databank 					= $this->_db->fetchAll($selectbank);
		if($fcompid)              $select->where('UPPER(COMP_ID)='.$this->_db->quote(strtoupper($fcompid)));
		if($fcompname)            $select->where('UPPER(COMP_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fcompname).'%'));
		if($fdebitbank)            $select->where('UPPER(DEBIT_BANK)='.$this->_db->quote(strtoupper($fdebitbank)));
		if($fdebitacct)            $select->where('UPPER(DEBIT_ACCT) LIKE '.$this->_db->quote('%'.strtoupper($fdebitacct).'%'));
		// $select   = $model->getBeneficiaries($param);
		// print_r($select);die;
		// echo $select;die;
		$this->paging($select);
		$this->view->payType = array(''=> ' -- Please Select -- ',$this->_beneftype["code"]["domestic"]=>$this->_beneftype["desc"]["domestic"],$this->_beneftype["code"]["online"]=>$this->_beneftype["desc"]["online"]);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	    Application_Helper_General::writeLog('BLBA','View Destination Account');

	    if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }


	}


}
