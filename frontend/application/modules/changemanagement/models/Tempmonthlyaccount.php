<?php
/**
 * Temprootcommunity model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempmonthlyaccount extends Changemanagement_Model_Tempchanges {
	protected $_moduleId = 'BGR';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_ROOT_COMM
		$select = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_CHARGES_MONTHLY')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
									 
        $custid = $select[0]['CUST_ID'];
	
        //$whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
       $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'MONTHLYFEE_TYPE =?'	=>	'3'); 

       $assign = $this->dbObj->delete('M_CHARGES_MONTHLY',$whereArr);
	   //die;
       if($select){
	       	//insert to master table M_BPRIVI_GROUP
	       	foreach ($select as $list) {
	       		$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>''));
	       		//$insertArr['UPDATED'] = new Zend_Db_Expr('now()');
				//$insertArr['UPDATEDBY'] = $actor;
				$assign = $this->dbObj->insert('M_CHARGES_MONTHLY',$insertArr);
	       	}
			
       }
       
       	$updatearr['CUST_MONTHLYFEE_TYPE'] = '3';
		$whereArr = array('CUST_ID = ?'=>$custid);
		$updateCust = $this->dbObj->UPDATE('M_CUSTOMER',$updatearr,$whereArr);
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate() {
		
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate() {
		
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit($actor = null) {

		//delete from TEMP_ROOT_COMM
		$assigndelete = $this->dbObj->delete('TEMP_CHARGES_MONTHLY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$rootcommunitydelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Backend Group)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteDelete() {
		//will never be called
	}
}