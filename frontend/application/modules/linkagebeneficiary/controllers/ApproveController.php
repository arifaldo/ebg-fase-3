<?php

require_once 'Zend/Controller/Action.php';


class linkagebeneficiary_ApproveController extends Application_Main {
	
	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$arr = null;
		$aprovalstatus = $this->_aprovalstatus;
		$aprovalstatuscode = $aprovalstatus['code'];
		$aprovalstatusdesc = $aprovalstatus['desc'];
		$this->view->aprovalstatuscode = array_flip($aprovalstatuscode);
		$this->view->aprovalstatusdesc = $aprovalstatusdesc;
		//$this->view->aprovalstatus = $aprovalstatus;
		
		$aprovalstatus = $this->_aprovalstatus;
		$aprovalstatus = array(''=>'-- '. $this->language->_('All') .' --');
		$aprovalstatus += Application_Helper_Array::globalvarArray($this->_aprovalstatus);
		foreach($aprovalstatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		
		unset($aprovalstatus[2]);
		unset($aprovalstatus[3]);
		unset($aprovalstatus[6]);
		
		unset($optpayStatusRaw[2]);
		unset($optpayStatusRaw[3]);
		unset($optpayStatusRaw[6]);
		
		
		// Zend_Debug::dump($optpayStatusRaw);die;
		//$this->view->aprovalstatus = $aprovalstatus;
		$this->view->aprovalstatus = $optpayStatusRaw;
		
		
		$usersArr = $this->_db->fetchAll(
								$this->_db->select()
									 ->from(array('M_USER'),array('USER_ID')) 
									 ->where("CUST_ID=?", $this->_custIdLogin)
									 ->order("USER_ID")
							);	
		
		$listUser = array(''=>'--'. $this->language->_('Select User') .'--');
		$listUser = array_merge($listUser,Application_Helper_Array::listArray($usersArr,'USER_ID','USER_ID'));
		$this->view->userArr = $listUser;
		
		$fields = array	(
							'User ID'  			=> array	(
																	'field' => 'USER_ID',
																	'label' => $this->language->_('User ID'),
																	'sortable' => true
																),
							'User Name'  					=> array	(
																		'field' => 'USER_FULLNAME',
																		'label' => $this->language->_('User Name'),
																		'sortable' => true
																	),
							'Suggest Date'  			=> array	(
																	'field' => 'SUGGEST_DATE',
																	'label' => $this->language->_('Suggested Date'),
																	'sortable' => true
																),
							'Suggested By'  					=> array	(
																		'field' => 'SUGGEST_USER',
																		'label' => $this->language->_('Suggested By'),
																		'sortable' => true
																	),
							'Suggest Status'  					=> array	(
																		'field' => 'SUGGEST_STATUS',
																		'label' => $this->language->_('Status'),
																		'sortable' => true
																	),
						);
		$this->view->fields = $fields;

		$filterlist = array("SUGGEST_DATE",'USER_ID','STATUS');
		
		$this->view->filterlist = $filterlist;
		
		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$filterArr = array(	
							'STATUS' 	  	=> array('StringTrim','StripTags'),
							'USER_ID' 	  	=> array('StringTrim','StripTags'),
							'SUGESTDATE' 	  	=> array('StringTrim','StripTags'),
							'SUGESTDATE_END' 	  	=> array('StringTrim','StripTags'),
							);
		
		
		$dataParam = array("STATUS",'SUGESTDATE','SUGESTDATE_END','USER_ID');
		$dataParamValue = array();
		//var_dump($this->_request->getParams());
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('whereco'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('whereco') as $key => $value) {
						if($dtParam==$value){
							if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
							
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['SUGESTDATE'] = $createarr[0];
					$dataParamValue['SUGESTDATE_END'] = $createarr[1];
			}

		$options = array('allowEmpty' => true);
		$validators = array(
							'STATUS' 	  	=> array(),
							'USER_ID' 	=> array(),
							'SUGESTDATE'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
							'SUGESTDATE_END'    => array(new Zend_Validate_Date($this->_dateDisplayFormat))
		);

		//var_dump($dataParamValue);

		$zf_filter 	= new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
		
		$filter 	= $zf_filter->getEscaped('filter');
		$filter_clear 	= $zf_filter->getEscaped('clearfilter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$select = $this->_db->select()->distinct()
							->from(	array('TB' => 'T_BENEFICIARY_USER'),
									array(
											'SUGGEST_ID'=>'TB.SUGGEST_ID',
											'USER_ID'=>'TE.USER_ID',
											'USER_FULLNAME'=>'MU.USER_FULLNAME',
											'SUGGEST_DATE'=>'TB.SUGGEST_DATE',
//											'SUGGEST_DATE'=>'CONVERT(VARCHAR,TB.SUGGEST_DATE,113)',
											'SUGGEST_USER'=>'TB.SUGGEST_USER',
											'SUGGEST_STATUS'=>'TB.SUGGEST_STATUS',))
							->joinLeft	(
											array('TE' => 'TEMP_BENEFICIARY_USER'),
											'TE.SUGGEST_ID = TB.SUGGEST_ID',
											array()
										)
							->joinLeft	(
											array('MU' => 'M_USER'),
											'MU.USER_ID = TE.USER_ID AND MU.CUST_ID = TB.CUST_ID',
											array()
										)
							->where('MU.CUST_ID   = ?' , $this->_custIdLogin);
								

		if($filter == null)
		{
			$select->where("TB.SUGGEST_STATUS NOT IN ('2','3','6')");
			$m = date("m")-1;
			$DATE_START = (date("d/0".$m."/Y"));
			$DATE_END = (date("d/m/Y"));
			$this->view->DATE_START = $DATE_START;
			$this->view->DATE_END = $DATE_END;
		}
							
		// if($filter == TRUE)
		// {
			$fstatus = $zf_filter->getEscaped('STATUS');
			//var_dump($fstatus);
			// if($fstatus!== null && $fstatus!== ''){$select->where('TB.SUGGEST_STATUS = '.$this->_db->quote($fstatus));}

			$fuser_id = $zf_filter->getEscaped('USER_ID');
			// if($fuser_id){$select->where('UPPER(TE.USER_ID) = '.$this->_db->quote(strtoupper($fuser_id)));}
				
			// die ($select);
			$DATE_START = $zf_filter->getEscaped('SUGESTDATE');
			$DATE_END = $zf_filter->getEscaped('SUGESTDATE_END');

			if($fstatus)
			{	
				$fstatusarr = explode(',', $fstatus);
				$select->where("TB.SUGGEST_STATUS  in (?)",$fstatusarr );	
			}

			if($fuser_id)
			{	
				$fuser_idarr = explode(',', $fuser_id);
				$select->where("UPPER(TE.USER_ID)  in (?)",$fuser_idarr );	
			}
			//var_dump($DATE_START);
			//var_dump($DATE_END);
			if ($DATE_START) {
				$FormatDate 	= new Zend_Date($DATE_START, $this->_dateDisplayFormat);
				$payDateFrom   	= $FormatDate->toString($this->_dateDBFormat);
				$select->where('DATE(TB.SUGGEST_DATE) >= ?', $payDateFrom);
			}

			if ($DATE_END) {
				$FormatDate 	= new Zend_Date($DATE_END, $this->_dateDisplayFormat);
				$payDateFrom   	= $FormatDate->toString($this->_dateDBFormat);
				$select->where('DATE(TB.SUGGEST_DATE) <= ?', $payDateFrom);
			}

			$this->view->DATE_START = $DATE_START;
			$this->view->DATE_END = $DATE_END;
			$this->view->status = $fstatus;
			$this->view->user_id = $fuser_id;
		// }
		
		if($filter_clear == TRUE){
			$DATE_START = '';
			$DATE_END = '';
			
			$this->view->DATE_START = '';
			$this->view->DATE_END = '';
		}
		
		// if($filter == TRUE || $filter == null)
		// {
		// 	if($DATE_START)
		// 	{
		// 		$FormatDate 	= new Zend_Date($DATE_START, $this->_dateDisplayFormat);
		// 		$from   	= $FormatDate->toString($this->_dateDBFormat);
		// 		if($from)
		// 		{
		// 			$select->where('DATE(TB.SUGGEST_DATE) >= '.$this->_db->quote($from));
					
		// 		}
		// 	}	
			
		// 	if($DATE_END)
		// 	{
		// 		$FormatDate 	= new Zend_Date($DATE_END, $this->_dateDisplayFormat);
		// 		$to   	= $FormatDate->toString($this->_dateDBFormat);
		// 		if($to)
		// 		{
		// 			$select->where('DATE(TB.SUGGEST_DATE) <= '.$this->_db->quote($to));
					
		// 		}
		// 	}	
			
		// }
		
		$select->order($sortBy.' '.$sortDir);
		// Zend_Debug::dump($select->__toString());die;
		//echo '<pre>';
		//echo $select;
		//die;
		$arr = $this->_db->fetchAll($select);
		$this->paging($arr);
		
		//new - begin
//		$data_val = array();
//		$i=0;
//		foreach ($arr as $keyy=>$valuee) {
//			//unset($valuee['SUGGEST_ID']);
//			
//			$arr[$keyy]['SUGGEST_STATUS'] = (array_key_exists($arr[$keyy]['SUGGEST_STATUS'],$this->view->aprovalstatuscode))? $this->view->aprovalstatusdesc[$this->view->aprovalstatuscode[$arr[$keyy]['SUGGEST_STATUS']]] : '&nbsp;';
//			
//			$valuee['SUGGEST_STATUS']= $this->language->_($arr[$keyy]['SUGGEST_STATUS']);
//			array_push($data_val, $valuee);
//			//$data_val[$i++] = $data;
//		}
		//new - end
//		Zend_Debug::dump($arr);
//		die;

		if($csv  || $pdf || $this->_request->getParam('print'))
		{
			
			//unset($arr['USER_ID']);
			foreach($arr as $key=>$value)
			{
				unset($arr[$key]['SUGGEST_ID']);
				//$arr[$key]['SUGGEST_STATUS'] = (array_key_exists($arr[$key]['SUGGEST_STATUS'],$this->view->aprovalstatuscode))? $this->view->aprovalstatusdesc[$this->view->aprovalstatuscode[$arr[$key]['SUGGEST_STATUS']]] : '&nbsp;';
				$arr[$key]['SUGGEST_STATUS'] = $this->language->_((array_key_exists($arr[$key]['SUGGEST_STATUS'],$this->view->aprovalstatuscode))? $this->view->aprovalstatusdesc[$this->view->aprovalstatuscode[$arr[$key]['SUGGEST_STATUS']]] : '&nbsp;');
				//$value['SUGGEST_STATUS']= $this->language->_($arr[$key]['SUGGEST_STATUS']);
			}
		}
		$header = Application_Helper_Array::simpleArray($fields, 'label');
		if($csv)
		{
			$this->_helper->download->csv($header,$arr,null,'Pairing Beneficiary - Approval');  
		}
		else if($pdf)
		{
			$this->_helper->download->pdf($header,$arr,null,'Pairing Beneficiary - Approval');
			
		}
		elseif($this->_request->getParam('print') == 1){
            $data = $arr;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Pairing Beneficiary - Approval', 'data_header' => $fields));
		}
		
		if (!empty($dataParamValue)) {
			 $this->view->createdStart = $dataParamValue['SUGESTDATE'];
			 $this->view->createdEnd = $dataParamValue['SUGESTDATE_END'];
		//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
		//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

			 unset($dataParamValue['SUGESTDATE_END']);
			// unset($dataParamValue['PS_EFDATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',',$value);
							if(!empty($duparr)){
								
								foreach($duparr as $ss => $vs){
									$wherecol[]	= $key;
									$whereval[] = $vs;
								}
							}else{
									$wherecol[]	= $key;
									$whereval[] = $value;
							}
			}
			//var_dump($wherecol);
			
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
				
		}
		
        // if(!empty($this->_request->getParam('wherecol'))){
		// 		$this->view->wherecol			= $this->_request->getParam('wherecol');
		// 	}

		// 	if(!empty($this->_request->getParam('whereopt'))){
		// 		$this->view->whereopt			= $this->_request->getParam('whereopt');
		// 	}

		// 	if(!empty($this->_request->getParam('whereval'))){
		// 		$this->view->whereval			= $this->_request->getParam('whereval');
		// 	}
		Application_Helper_General::writeLog('BABU',"Viewing Approve");
	}
}

