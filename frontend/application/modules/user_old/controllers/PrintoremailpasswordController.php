<?php
require_once 'Zend/Controller/Action.php';
class user_PrintoremailpasswordController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction(){
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$this->_helper->layout()->setLayout('newlayout');
		$cust_id = $this->_custIdLogin;
		$this->view->cust_id = $cust_id;
		$user_id = $this->_userIdLogin;
		$this->view->user_id = $user_id;
	
		$fields = array	(
			'User ID' => array	(
				'field' => 'USER_ID',
				'label' => $this->language->_('User ID'),
				'sortable' => true
			),
			'Fullname' => array	(
				'field' => 'USER_FULLNAME',
				'label' => $this->language->_('Fullname'),
				'sortable' => true
			),
		// 'Prefered Method'	=> array	(
			// 'field' => 'USER_ISEMAILPWD',
			// 'label' => 'Prefered Method',
			// 'sortable' => false
		// ),		
		// 'Emailed?'=> array	(
			// 'field' => 'USER_RPWD_ISEMAILED',
			// 'label' => 'Emailed?',
			// 'sortable' => false
		// ),
		// 'Printed?' => array	(
			// 'field' => 'USER_RPWD_ISPRINTED',
			// 'label' => 'Printed?',
			// 'sortable' => false
		// ),
			'Email'	=> array	(
				'field' => 'USER_EMAIL',
				'label' => 'Email',
				'sortable' => true
			),																
		);
		
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','User ID');
		$sortDir = $this->_getParam('sortdir','asc');
	
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
	
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
				array(array_keys($fields))
		))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	
		$sortDir = (Zend_Validate::is($sortDir,'InArray',
				array('haystack'=>array('asc','desc'))
		))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
	
		$select = $this->_db->select()
			->FROM (
				array('a' =>'M_USER'),
				array('a.USER_ID', 'a.USER_FULLNAME','a.USER_EMAIL','a.USER_RPWD_ISEMAILED','a.USER_RPWD_ISPRINTED','a.USER_ISEMAILPWD','a.USER_RPWD_ISBYBO' ))
			->WHERE('a.CUST_ID = ?',$this->_custIdLogin)
			->WHERE('a.USER_RCHANGE = 1 ')
		;
	    $select->order($sortBy.' '.$sortDir);
		$this->paging($select);
		$this->view->fields = $fields;
		
		Application_Helper_General::writeLog('UVEP','View Print Or Email Password');
	}
}
