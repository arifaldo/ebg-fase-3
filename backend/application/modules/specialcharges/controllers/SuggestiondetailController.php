<?php

require_once 'Zend/Controller/Action.php';

class Specialcharges_SuggestiondetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$changeid = $this->_getParam('changes_id'); 
  	
	$select = $this->_db->select()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
	$select -> where("A.CHANGES_ID = ?", $changeid);
	$select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
	$result = $select->query()->FetchAll();
	$this->view->result=$result;
	$custid = $result[0]['KEY_FIELD'];
	//var_dump($result);die;
	$this->view->changes_type =  $result['0']['CHANGES_TYPE'];
 	if(!$result)
	{
		$this->_redirect('/notification/invalid/index');
	}
	
	//Zend_Debug::dump($result);die;
	
	
		$pstypeArr = array(1 => 'RTGS',2 => 'SKN',8 => 'Domestic Online',0 => 'Inhouse');
		
		$this->view->pstype = $pstypeArr;
		
		/*
		$selectpbglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_WITHIN'),array('*'));
		//$selectpbglobal ->where("A.CUST_ID = 'GLOBAL'");
		$selectpbglobal ->where("A.CHANGES_ID = ? ",$changes_id);
		$pbglobal = $this->_db->fetchAll($selectpbglobal);
		 if(!empty($pbglobal)){
			 $pbamount = $pbglobal['0']['AMOUNT'];
		 }else{
			 $pbamount = 0;
		 }
		 //var_dump($pbamount);
		$this->view->xpbglobal = $pbamount;
		*/
		
		$selectpbspecial = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_WITHIN'),array('*'));
		//$selectpbspecial ->where("A.CUST_ID = 'SPECIAL'");
		$selectpbspecial ->where("A.CHANGES_ID = ? ",$changeid);
		$pbspecial = $this->_db->fetchAll($selectpbspecial);
		 if(!empty($pbspecial)){
			 $pbspecialamount = $pbspecial['0']['AMOUNT'];
		 }else{
			 $pbspecialamount = 0;
		 } 
		 //echo '<pre>';
		 //var_dump($result);
		 //var_dump($pbspecial); die;
		$this->view->xpbspecial = $pbspecialamount;
		$this->view->packagename = $pbspecial['0']['PACKAGE_NAME'];
		/*
		$selectdomglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
		$selectdomglobal ->where("A.CUST_ID = 'GLOBAL'");
		$selectdomglobal ->where("A.CHANGES_ID = ? ",$changes_id);
		$domglobal = $this->_db->fetchAll($selectdomglobal);
		 
		$this->view->xdomglobal = $domglobal;
		*/
		$selectdomspecial = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
		//$selectdomspecial ->where("A.CUST_ID = 'SPECIAL'");
		$selectdomspecial ->where("A.CHANGES_ID = ? ",$changeid);
		$domspecial = $this->_db->fetchAll($selectdomspecial);
		 
		$this->view->xdomspecial = $domspecial;
		   
		$custid = $pbspecial['0']['CUST_ID'];
		$this->view->custid = $custid;
		$selectpbglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_WITHIN'),array('*'));
		$selectpbglobal->where("A.CUST_ID = ?",$custid);
		$pbglobal = $this->_db->fetchAll($selectpbglobal);
		 if(!empty($pbglobal)){
			 $pbamount = $pbglobal['0']['AMOUNT'];
		 }else{  
			 $pbamount = 0;
		 }
		 //var_dump($pbamount);
		$this->view->pbglobal = $pbamount;
		$this->view->xpackagename = $pbglobal['0']['PACKAGE_NAME'];
		
		
		$selectpbspecial = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_WITHIN'),array('*'));
		$selectpbspecial ->where("A.CUST_ID = ? ",$custid);
		$pbspecial = $this->_db->fetchAll($selectpbspecial);
		 if(!empty($pbspecial)){
			 $pbspecialamount = $pbspecial['0']['AMOUNT'];
		 }else{
			 $pbspecialamount = 0;
		 }
		 //var_dump($pbamount); 
		$this->view->pbspecial = $pbspecialamount;
		
		
		
		$selectdomspecial = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_OTHER'),array('*'));
		$selectdomspecial ->where("A.CUST_ID = ? ",$custid);
		$domspecial = $this->_db->fetchAll($selectdomspecial);
		 
		$this->view->domspecial = $domspecial;


		$changes_id = strip_tags( trim($this->_getParam('changes_id')) );
		$this->view->changes_id = $changes_id;

		$pstypeArr = array(1 => 'RTGS',2 => 'SKN',8 => 'Domestic Online',0 => 'Inhouse');
		
		$this->view->pstype = $pstypeArr;
		
		


	//echo $select2; die;
    if(!$this->_request->isPost()){
    Application_Helper_General::writeLog('CHCL','View Domestic charges changes list page ('.$custid.')');
    }
  }
}