<?php


require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class monthlycompany_IndexController extends Application_Main
{
	public function indexAction() 
	{	
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $custid = $AESMYSQL->decrypt($this->_getParam('custid'), $password);
	    $custid = (Zend_Validate::is($custid,'Alnum') && Zend_Validate::is($custid,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $custid : null;
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$select -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $select->query()->FetchAll();
		$this->view->result = $result;
		
		$monthlyfeestatus = $result[0]["CUST_MONTHLYFEE_STATUS"];
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = $this->language->_('Disabled');
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = $this->language->_('Enabled');
		}
			
		$custname = $result[0]['CUST_NAME'];
		
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		//$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;			     
		
		$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'));
		$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$select4 -> where("MONTHLYFEE_TYPE = '4'");
		$result4 = $select4->query()->FetchAll();

		$i = 0;

			if( !empty($result4) )
			{
				foreach($result4 as $row2)
				{	
					$idamt = 'amount'.$i;
					$idfrom = 'from'.$i;
   					$idto = 'to'.$i;
					$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
					
					if($row2['TRA_FROM'] != 0 && $row2['TRA_TO'] != 0)
					{
						$this->view->$idfrom = $row2['TRA_FROM'];
						$this->view->$idto = $row2['TRA_TO'];
					}
					
					$this->view->$idamt = $amt;
					$i++;
				}
				$cekacct = 'cek'.'acct';
				$this->view->$cekacct = $row2['CHARGES_ACCT_NO'];
			}
		
		Application_Helper_General::writeLog('CHUD','Update company charges');
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
		$cek = $this->_db->select()
							->from('TEMP_ADMFEE_MONTHLY');
		$cek -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek = $cek->query()->FetchAll();
		
		$cek2 = $this->_db->select()
							->from('TEMP_CHARGES_MONTHLY');
		$cek2 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek2 = $cek2->query()->FetchAll();
		//Zend_Debug::dump($submit); die;
		if(!$cek && !$cek2)
		{
			if($submit)
			{
					$error = 0;
					$cekacct = 'acct';
					$cekisi = 0;
					$cekisi2 = 0;
					$arraycekisi = array();
					
					$cekcekacct = $this->_getParam($cekacct);
   					$cek_angka4 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   					$this->view->cekacct = $cekcekacct;
   					
					if($cekcekacct != null && $cekcekacct != "")
					{
						if($cek_angka4 == false)
						{
							$errid = 'erracct';
							$err = $this->language->_('Account value has to be numeric');
							$this->view->$errid = $err;
							$error++;
									//die;
						}
					}
						
					if(!$cekcekacct)
					{
						$errid = 'erracct';
						$err = $this->language->_('Charges Account must be choosen');
						$this->view->$errid = $err;
						$error++;
					}
					
					for($i=0;$i<10;$i++)
					{
						$idamt = 'amount'.$i;
						$idfrom = 'from'.$i;
   						$idto = 'to'.$i;
   						$err2 = 'err2'.$i;
   						//Zend_Debug::dump($idamt); die;
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						$temp_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
						$this->view->$idamt = $cekamt;
						
						$cekfrom = $this->_getParam($idfrom);
						$cek_angka2 = (Zend_Validate::is($cekfrom,'Digits')) ? true : false;
						$this->view->$idfrom = $cekfrom;
						
						$cekto = $this->_getParam($idto);
						$cek_angka3 = (Zend_Validate::is($cekto,'Digits')) ? true : false;
						$this->view->$idto = $cekto;
						
   						//Zend_Debug::dump($cekfrom); die;
   						
						if($cek_angka && $cek_angka2 && $cek_angka3)
						{
							if ($cekfrom >= $cekto)
							{
								$errid = 'err'.$idfrom;
								$err = $this->language->_('From Range has to be less than To Range');
								$this->view->$errid = $err;
								$error++;
							}
							
							if ($cekfrom == '0')
							{
								$errid = 'err'.$idfrom;
								$err = 'Range value can not start from 0';
								$this->view->$errid = $err;
								$error++;
								//die;
							}
							
							for($j=$i;$j>=0;$j--)
							{
								if($i != $j)
								{
									$fromerror = false;
									$toerror = false;
									$idfromcek = 'from'.$j;
   									$idtocek = 'to'.$j;
   									$fromcek = $this->_getParam($idfromcek);
   									$tocek = $this->_getParam($idtocek);
   									
   									if($fromcek && $tocek)
   									{
   										if($fromcek <= $cekfrom && $cekfrom <= $tocek)
   										{
   											$fromerror = true;
   											//echo $cekfrom;die;
   										}
   									
										if($fromcek < $cekto && $cekto < $tocek)
   										{
   											$toerror = true;
   										}
   									
   										if($fromerror == true || $toerror == true)
   										{
   											$errid = 'err'.$idto.$j;
											$err = 'Value overlap not allowed (overlap with : '.$fromcek.'-'.$tocek.')';
											$this->view->$errid = $err;
											$this->view->$err2 = true;
											$error++;
   										}
   									}	
								}
							}
						}
   						
						if($cek_angka2 && $cek_angka3)
							{
								if($cekamt == null || $cekamt == '0.00')
								{
									$errid = 'err'.$idamt;
									$err = 'Amount value has to be set for this range';
									$this->view->$errid = $err;
									$error++;
									//die;
								}
							}
						
						if($cek_angka == false && $cekamt != null)
							{
								$errid = 'err'.$idamt;
								$err = 'Amount value has to be numeric';
								$this->view->$errid = $err;
								$error++;
								//die;
							}
						
						if($cek_angka && $cekamt != '0.00')
							{
								if($cek_angka2 == false || $cek_angka3 == false)
								{
									$errid = 'err'.$idfrom;
									$err = 'From and To value has to be numeric';
									$this->view->$errid = $err;
									$error++;
									//die;
								}
								
								if($cekfrom == null || $cekto == null)
								{
									$errid = 'err'.$idfrom;
									$err = 'From and To Range should not be empty while the amount is filled';
									$this->view->$errid = $err;
									$error++;
									//die;
								}
							}
						if($cekfrom == null && $cekto == null && $amt == '0.00' && $cek_angka4)
							{
								$cekisi++;
							}
						$cekisi2++;					
						}
					
						if($cekisi == $cekisi2)
						{
							//$arraycekisi = array($acctno);
						}
					//Zend_Debug::dump($error); die;
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
				    //Zend_Debug::dump($cek_angka2); die;
					$info = 'Charges';
					$info2 = 'Set Charges Monthly per Company';
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_MONTHLY','TEMP_CHARGES_MONTHLY',$custid,$custname,$custid);
					
					$cekacct = 'acct';
						
					for($i=0;$i<10;$i++)
					{
						$idamt = 'amount'.$i;
						$idfrom = 'from'.$i;
   						$idto = 'to'.$i;
   						
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
						$cekfrom = $this->_getParam($idfrom);
						
						$cekto = $this->_getParam($idto);
						
						$cekcekacct = $this->_getParam($cekacct);
						
						if($cekamt && $cekfrom && $cekto && $cekcekacct)
						{
						
							$data= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $cekcekacct,
										'TRA_FROM' 			=> $cekfrom,
										'TRA_TO' 			=> $cekto,
										'AMOUNT' 			=> $amt,
										'MONTHLYFEE_TYPE' 	=> '4',
										'CHARGES_ACCT_NO'	=> $cekcekacct
									);
						$this->_db->insert('TEMP_CHARGES_MONTHLY',$data);										
						//echo 'aaa';die;
						}
					}
					
					if($arraycekisi)
					{
						foreach($arraycekisi as $array)
						{				
							$cekcekacct = $this->_getParam('acct');
								
							$data2= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $cekcekacct,
										'TRA_FROM' 			=> '0',
										'TRA_TO' 			=> '0',
										'AMOUNT' 			=> '0.00',
										'MONTHLYFEE_TYPE' 	=> '4',
										'CHARGES_ACCT_NO'	=> $cekcekacct
										);
							$this->_db->insert('TEMP_CHARGES_MONTHLY',$data2);				
						}
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/setcompanycharges/detail/index/custid/'.$this->_getParam('custid'));
						$this->_redirect('/notification/submited/index');
					}
				}
				
				catch(Exception $e)
				{
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}		
		}
		}
		if($cek || $cek2)
		{			
			$docErr = "*No changes allowed for this record while awaiting approval for previous change";	
			$this->view->error = $docErr;
			$this->view->changestatus = "disabled";
		}	
	}
}
