<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';
class reportdu_IndexController extends Application_Main
{
  

  public function indexAction()
  {

    $this->_helper->layout()->setLayout('newlayout');

    $filter_clear     = $this->_getParam('clearfilter');
    
    $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    $AccArr = $CustomerUser->getAccounts();
    $this->view->AccArr = $AccArr;
    
    $transstatus = $this->_transferstatus;
    $transstatuscode = array_flip($transstatus['code']);
    $statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
    
    $paymenttype = $this->_paymenttype;

    $arrPayType  = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
    $this->view->arrPayType = $arrPayType;
    $typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
    
    $tratypearr = array_combine(array_values($this->_transfertype['code']),array_values($this->_transfertype['desc']));

       


        $fields = array(
            'COMPANY'       => array('field' => 'COMPANY',
                                        'label' => $this->language->_('Company '),
                                        ),
            'DOC_ID'        => array('field' => 'DOC_ID',
                                        'label' => $this->language->_('Document ID '),
                                        ),
            // 'PS_NUMBER'       => array('field' => 'PS_NUMBER',
            //                             'label' => $this->language->_('Payment Ref# '),
            //                             ),
            'CCY'       => array('field' => 'CCY',
                                        'label' => $this->language->_('Currency '),
                                        ),
            'AMOUNT'        => array('field' => 'AMOUNT',
                                      'label' => $this->language->_('Amount '),
                                      ),
            'PAY_DATE'    => array('field' => 'PAY_DATE',
                                        'label' => $this->language->_('Payment Date '),
                                        ),
            'PAYMENT_TYPE'        => array('field' => 'PAYMENT_TYPE',
                                      'label' => $this->language->_('Payment Type '),
                                      ),
            'NOTES'         => array('field' => 'NOTES',
                                      'label' => $this->language->_('Notes '),
                                      ),
            'STATUS'        => array('field' => 'STATUS',
                                      'label' => $this->language->_('Status '),
                                      ),
            'DOWNLOAD'    => array('field' => 'DOWNLOAD',
                                        'label' => $this->language->_('Download '),
                                        ),
            
          );

          $this->view->fields = $fields;
          
      $filterlist = array('PS_NUMBER');

    $this->view->filterlist = $filterlist;  
        
            
        
    $filterArr = array( 'PS_NUMBER'  => array('StringTrim','StripTags'),
        );
        
    $dataParam = array('PS_NUMBER');
    $dataParamValue = array();
    foreach ($dataParam as $dtParam)
    {

      // print_r($dtParam);die;
      if(!empty($this->_request->getParam('wherecol'))){
        $dataval = $this->_request->getParam('whereval');
          foreach ($this->_request->getParam('wherecol') as $key => $value) {
            if($dtParam==$value){
              if(!empty($dataParamValue[$dtParam])){
                $dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
              }
              $dataParamValue[$dtParam] = $dataval[$key];
            }
          } 

      }

      // $dataPost = $this->_request->getPost($dtParam);
      // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
    }
      
      $select3 = $this->_db->select()
                  ->from(array('A' => 'T_UNDERLYING')); 
    $select3->joinleft(array('K' => 'T_TRANSACTION'), 'A.DOC_ID = K.DOC_ID', array('K.PS_NUMBER'));
    //echo $select3;die;
      
         $options = array('allowEmpty' => true);
      $validators = array(
              'PS_NUMBER'        => array(),

              );

      $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
      $number    = $zf_filter->getEscaped('PS_NUMBER');
       //var_dump($number);
      if($number)
      { 
        $select3->where("K.PS_NUMBER LIKE ".$this->_db->quote('%'.$number.'%')); 
      } 
                            
        

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;
        $AESMYSQL = new Crypt_AESMYSQL();
        $PS_NUMBER      = urldecode($this->_getParam('DOC_ID'));
        $FILE_ID = $AESMYSQL->decrypt($PS_NUMBER, $password);

    if($FILE_ID)
    {
      $select3->where('DOC_ID =?',$FILE_ID);
      $data = $this->_db->fetchRow($select3);
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';
      $this->_helper->download->file($data['DOC_NAME'],$attahmentDestination.$data['DOC_SYSNAME']);
    }

      $select3->GROUP('DOC_ID');
      
        $this->paging($select3);

    }
}