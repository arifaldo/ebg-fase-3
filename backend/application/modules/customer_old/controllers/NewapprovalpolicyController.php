<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/Settings.php';

class Customer_NewapprovalpolicyController extends customer_Model_Customer
{
  protected $_moduleDB = 'RTF'; //masih harus diganti

  public function indexAction()
  {

    // $arrTraType   = array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
    // print_r($arrTraType)

    // $this->view->
    $this->_helper->layout()->setLayout('newlayout');
     $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
     if(count($temp)>1){
          if($temp[0]=='F' || $temp[0]=='S'){
            if($temp[0]=='F')
              $this->view->error = 1;
            else
              $this->view->success = 1;
            $msg = ''; unset($temp[0]);
            foreach($temp as $value)
            {
              if(!is_array($value))
                $value = array($value);
              $msg .= $this->view->formErrors($value);
            }
            $this->view->report_msg = $msg;
        } 
      }

       
      $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
      $password = $sessionNamespace->token; 
      $this->view->token = $sessionNamespace->token;

    $this->view->enccust_id = $this->_getParam('cust_id');
      $AESMYSQL = new Crypt_AESMYSQL();
      $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
      $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

     /* $selectcust =  $this->_db->select()
                  ->from( 'M_CUSTOMER',array('CUST_ID','CUST_ID'));
                  //->where('PASSWORD = \''.$v_password_old.'\'');
        
        // echo $v_password;
      $cust = $this->_db->fetchAll($selectcust);  
      $arrcust = Application_Helper_Array::listArray($cust,'CUST_ID','CUST_ID');

      $this->view->arrcust = $arrcust;

      $selectcustname =  $this->_db->select()
                  ->from( 'M_CUSTOMER',array('CUST_ID','CUST_NAME'));
                  //->where('PASSWORD = \''.$v_password_old.'\'');*/
        
        // echo $v_password;
    /*  $custname = $this->_db->fetchAll($selectcustname);  
      $arrcustname = Application_Helper_Array::listArray($custname,'CUST_ID','CUST_NAME');

      $this->view->arrcustname = $arrcustname;*/

      $arrTraType     = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
      unset($arrTraType['18']); 
    unset($arrTraType['22']); 
        $arrTraType['18'] = 'Disbursement';
    $setting = new Settings();
    $system_type = $setting->getSetting('system_type');
        if($system_type == '2'){
      unset($arrTraType);
	 
      
    }
    
     $arrTraTypenew = array();
      $arrTraTypenew['50'] = 'Cash Collateral';
      $arrTraTypenew['51'] = 'Non Cash Collateral';
      $this->view->arrTraType = $arrTraTypenew;

      $selectccy =  $this->_db->select()
                  ->from( 'M_MINAMT_CCY',array('CCY_ID','CCY_ID'));
                  //->where('PASSWORD = \''.$v_password_old.'\'');
        
        // echo $v_password;
      $ccy = $this->_db->fetchAll($selectccy);  
      $arrccy = Application_Helper_Array::listArray($ccy,'CCY_ID','CCY_ID');

      $this->view->arrccy = $arrccy;




        $selectcust = $this->_db->select()
            ->from(array('MAB'=>'TEMP_APP_BOUNDARY'))
            // ->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
            ->where('MAB.CUST_ID = ?', (string)$cust_id);

        $resulttemp = $this->_db->fetchAll($selectcust);

        if(!empty($resulttemp)){
          $this->view->tempapp = true;
        }


       if($cust_id)
       {
          $resultdata = $this->getCustomer($cust_id);

          if($resultdata['CUST_ID'])
          {
                $this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
            $this->view->cust_name    = $resultdata['CUST_NAME'];
            $this->view->cust_cif     = $resultdata['CUST_CIF'];
            $this->view->cust_type    = $resultdata['CUST_TYPE'];
            $this->view->cust_workfield  = $resultdata['CUST_WORKFIELD'];
            $this->view->cust_address = $resultdata['CUST_ADDRESS'];
            $this->view->cust_city    = $resultdata['CUST_CITY'];
            $this->view->cust_zip     = $resultdata['CUST_ZIP'];
            $this->view->cust_province = $resultdata['CUST_PROVINCE'];
            $this->view->country_code  = $resultdata['COUNTRY_CODE'];
            $this->view->cust_contact  = $resultdata['CUST_CONTACT'];
            $this->view->cust_phone    = $resultdata['CUST_PHONE'];
            $this->view->cust_ext      = $resultdata['CUST_EXT'];
            $this->view->cust_fax      = $resultdata['CUST_FAX'];
            $this->view->cust_email    = $resultdata['CUST_EMAIL'];
            $this->view->cust_website  = $resultdata['CUST_WEBSITE'];
              $this->view->cust_status   = strtoupper($resultdata['CUST_STATUS']);

              $this->view->cust_charges_status  = $resultdata['CUST_CHARGES_STATUS'];
                $this->view->cust_admfee_status   = $resultdata['CUST_MONTHLYFEE_STATUS'];
                $this->view->cust_token_auth   = $resultdata['CUST_TOKEN_AUTH'];

              $this->view->cust_created     = $resultdata['CUST_CREATED'];
              $this->view->cust_createdby   = $resultdata['CUST_CREATEDBY'];
              $this->view->cust_suggested   = $resultdata['CUST_SUGGESTED'];
              $this->view->cust_suggestedby = $resultdata['CUST_SUGGESTEDBY'];
              $this->view->cust_updated     = $resultdata['CUST_UPDATED'];
              $this->view->cust_updatedby   = $resultdata['CUST_UPDATEDBY'];

              $this->view->cust_limit_idr     = Application_Helper_General::displayMoney($resultdata['CUST_LIMIT_IDR']);
              $this->view->cust_limit_usd   = Application_Helper_General::displayMoney($resultdata['CUST_LIMIT_USD']);



             //--------------------------------------------------Bank Account--------------------------------------------
              $this->view->bankAccount     = $this->getCustomerAcct($cust_id);
              $this->view->benefBankAcc    = $this->getBenefBankAcc($cust_id);
              $this->view->userAccount     = $this->getUserAcct($cust_id);
              $this->view->userLimit       = $this->getUserLimit($cust_id);
              $this->view->userDailyLimit  = $this->getUserDailyLimit($cust_id);
        // $this->view->userTempDailyLimit  = $this->getUserTempDailyLimit($cust_id);

              $getAppGroup = $this->getAppGroup($cust_id);
              $getAppGroupArray = null;
              foreach($getAppGroup as $row)
              {
                $getAppGroupArray[ $row['GROUP_USER_ID'] ][] = $row['USER_ID'];
              }
              $this->view->appGroup = $getAppGroupArray;


              $getAppBoundaryGroup = $this->getAppBoundary($cust_id);
              $boundaryGroup = null;
                foreach($getAppBoundaryGroup as $row)
              {
                  //example : N_CUSTCHRIS_01 & S_CUSTCHRIS
                $explodeGroup = explode('_',$row['GROUP_USER_ID']);

                if($explodeGroup[0] == 'N')      $group_desc = 'Group ' . (int)$explodeGroup[2];
                  else if($explodeGroup[0] == 'S') $group_desc = 'Special Group';


                  $boundaryGroup[$row['BOUNDARY_ID']]['GROUP_USER_ID'][] = $group_desc;
                  $boundaryGroup[$row['BOUNDARY_ID']]['BOUNDARY_MIN']    = $row['BOUNDARY_MIN'];
                  $boundaryGroup[$row['BOUNDARY_ID']]['BOUNDARY_MAX']    = $row['BOUNDARY_MAX'];
                  $boundaryGroup[$row['BOUNDARY_ID']]['CCY_BOUNDARY']    = $row['CCY_BOUNDARY'];
              }
              $this->view->appBoundary = $boundaryGroup;

              $this->view->benefUser   = $this->getBenefUser($cust_id);

             //----------------------------------------------- END Bank Account------------------------------------------



       $result = $this->getTempCustomerId($cust_id);

             if($result)  $temp = 0;
             else  $temp = 1;

             $this->view->cust_temp = $temp;
           }
           else $cust_id = null;
       }// End if cust_id == true


       // print_r($cust_id);die;
        // if(!$cust_id)
        // {
           // $error_remark = 'Invalid Cust ID';
        //insert log
$sessionNamespace   = new Zend_Session_Namespace('approvalgroup'.$cust_id);
        $content    = $sessionNamespace->content;
        // echo "<pre>";
        // print_r($content);die;
        $this->view->content = $content;
             $submit = $this->_getParam('submit');
      if($submit)
      {
        $data = $this->_request->getParams();
        
        // echo "<pre>";
        // print_r($data);die;
        // $sessionNamespace         = new Zend_Session_Namespace('approvalgroup');
        // $sessionNamespace->content    = $conten;
        $sessionNamespace   = new Zend_Session_Namespace('approvalgroup'.$cust_id);
        $content    = $sessionNamespace->content;
    //echo '<pre>';
//  echo '<pre>';
//  var_dump($data);
//    var_dump($content);die;
            $changeInfo= "Edit Boundary";
            $changeType = $this->_changeType['code']['edit'];
            $masterTable = 'M_APP_BOUNDARY,M_APP_BOUNDARY_GROUP';
            $tempTable =  'TEMP_APP_BOUNDARY,TEMP_APP_BOUNDARY_GROUP';
            $keyField = $cust_id;
            $keyValue = $cust_id;
            $custid = $cust_id;

            $displayTableName = 'Submission Approver Matrix';
          $changesId = $this->suggestionWaitingApproval($displayTableName, $changeInfo, $changeType,'',$masterTable,$tempTable,$keyField,$keyValue,$custid,null,'boundary');

            
            $listsuper = explode(',', $content['sg_user']);
            // print_r($data);
            // print_r($content);die;
            // print_r($listsuper);

            // foreach ($data['policy_group'] as $key => $value) {
            //     if($value==)
            // }

    //  try{
            foreach ($listsuper as $key => $value) {
                  $specialinsertid = 'S_'.$custid;
                  // $nogroup = sprintf("%02d", $key+1);

                    $insertgroup = array(
                      'CHANGES_ID'    => $changesId,
                      'GROUP_USER_ID'    => $specialinsertid,
                      'CUST_ID'    => $custid,
                      'USER_ID'    => $value,
                      'GROUP_NAME'    => $content['sg_name']
                    );  
                      $this->_db->insert('TEMP_APP_GROUP_USER',$insertgroup);


                     // $keywords = preg_split("/[\s,]+/", $data); 
                      // foreach ($data['policy_group'] as $no => $val) {
                      //   $keywords = preg_split("/[\s,()]+/", $val);
                      //   if (in_array('SG', $keywords))
                      //   {
                      //       $inserboundary = 
                      //   }

                      // }


            }

            // print_r($content);die;
            foreach ($content['g_id'] as $key => $value) {

                $listuser = explode(',', $content['g_user'][$key]);
                  $nogroup = sprintf("%02d", $key+1);
                  foreach ($listuser as $no => $val) {
                    // print_r($val);
                    $specialinsertid = 'N_'.$custid;
                    // $specialinsertid = $content['g_id'][$key].'N_'.$custid;
                    // print_r($content);


                    $insertgroup = array(
                      'CHANGES_ID'    => $changesId,
                      'GROUP_USER_ID'    => $specialinsertid.'_'.$nogroup,
                      'CUST_ID'    => $custid,
                      'USER_ID'    => $val,
                      'GROUP_NAME'    => $content['g_list'][$key]
                    );
                    // print_r($insertgroup);die;  
                      $this->_db->insert('TEMP_APP_GROUP_USER',$insertgroup);
                  }
                    
            }

           $resultdata = 
           $this->_db->fetchRow(
              $this->_db->select()
                 ->from(array('TEMP_APP_BOUNDARY'))
                 // ->where("BANK_ID=?", $bank_id)
                 ->ORDER('BOUNDARY_ID DESC ')
                               );
           // print_r($resultdata);die;

           $lastbound = $resultdata['BOUNDARY_ID']+1;

            $specialTrfType = array('19', '20', '23');
            // print_r($lastid);die;
              try{
     //    echo '<pre>';
      
                 //var_dump($data);die;
         //$data['policyindex'] = $data['policyindex'] + 1;
             if(!empty($data['policyindex'])){
         
         $index = 0;
                  // $row = $i+1;
                  // print_r($lastid);
                  // echo "<pre>";
                  // print_r($data);
                  // print_r($data[$amount_from][0]);
                  // print_r($i-1);
          
                $lastbound = $resultdata['BOUNDARY_ID']+$lastid;
                $amount_to = 'amount_to';
                $amount_from = 'amount_from';
                $policy_group = 'policy_group';
                $trfType = $data['policy_type'][0];
          //var_dump($data[$policy_group]);die;
                  foreach ($data[$policy_group] as $no => $value) {

                    $amount_from_val = Application_Helper_General::convertDisplayMoney($data[$amount_from][$no]);
                    $amount_to_val = Application_Helper_General::convertDisplayMoney($data[$amount_to][$no]);

                    if (in_array($trfType, $specialTrfType)) {
                      $amount_from_val = null;
                      $amount_to_val = null;
                    }

                    if($no == 0){
                      $no = 1; 
                    }
                    $lastbound = $lastbound + $no;

                     $insertboundary = array(
                        'CHANGES_ID'    => $changesId,
                        'BOUNDARY_ID'    => $lastbound,
                        'CCY_BOUNDARY'    => $data['ccy'][0],
                        'BOUNDARY_MIN'    =>  $amount_from_val,
                        'BOUNDARY_MAX'    => $amount_to_val,
                        'CUST_ID'    => $custid,
                        'BOUNDARY_ISUSED'    => '1',
                        'POLICY'           => $value,
                        'ROW_INDEX'   => $lastbound,
                        'TRANSFER_TYPE'   => $trfType
                      );
                    $this->_db->insert('TEMP_APP_BOUNDARY',$insertboundary);  
  
                     $keywords = preg_split("/[\s,()]+/", $data[$policy_group][$no]);
                          // print_r($keywords);
                      $specialinsertid = 'S_'.$custid;
                              $insertboundarygroup = array(
                                'CHANGES_ID'    => $changesId,
                                'BOUNDARY_ID'    => $lastbound,
                                'GROUP_USER_ID'   => $specialinsertid,
                                'ROW_INDEX'       => $lastbound
                              );
                              // print_r($insertboundarygroup);
                              $this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$insertboundarygroup);

                          // if (in_array('SG', $keywords))
                          // {
                            
                          // }

                           foreach ($content['g_id'] as $a => $b) {
                      
                        $keywords = preg_split("/[\s,()]+/", $data[$policy_group][$no]);
                        // print_r($b);
                          if (in_array($b, $keywords))
                          {
                            $nogroup = sprintf("%02d", $a+1);
                            $specialinsertid = 'N_'.$custid.'_'.$nogroup;

                              $insertboundarygroup = array(
                                'CHANGES_ID'    => $changesId,
                                'BOUNDARY_ID'    => $lastbound,
                                'GROUP_USER_ID'   => $specialinsertid,
                                'ROW_INDEX'       => $lastbound
                              );
                              // print_r($insertboundarygroup);
                              $this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$insertboundarygroup);
                          }

                    }

                  }
     
     
     
              for ($i=0; $i < $data['policyindex'] ; $i++) { 
                  $index = 0;
                  // $row = $i+1;
                  // print_r($lastid);
                  // echo "<pre>";
                  // print_r($data);
                  // print_r($data[$amount_from][0]);
                  // print_r($i-1);
          $newid = $i+1;
                $lastbound = $resultdata['BOUNDARY_ID']+$lastid+1;
                $amount_to = 'amount_to'.$i;
                $amount_from = 'amount_from'.$i;
                $policy_group = 'policy_group'.$i;
                $trfType = $data['policy_type'][$i+1];
          //var_dump($data[$policy_group]);die;
                  foreach ($data[$policy_group] as $no => $value) {

                    $amount_from_val = Application_Helper_General::convertDisplayMoney($data[$amount_from][$no]);
                    $amount_to_val = Application_Helper_General::convertDisplayMoney($data[$amount_to][$no]);

                    if (in_array($trfType, $specialTrfType)) {
                      $amount_from_val = null;
                      $amount_to_val = null;
                    }

                    if($no == 0){
                      $no = 1; 
                    }
                    $lastbound = $lastbound + $no;

                     $insertboundary = array(
                        'CHANGES_ID'    => $changesId,
                        'BOUNDARY_ID'    => $lastbound,
                        'CCY_BOUNDARY'    => $data['ccy'][$i+1],
                        'BOUNDARY_MIN'    =>  $amount_from_val,
                        'BOUNDARY_MAX'    => $amount_to_val,
                        'CUST_ID'    => $custid,
                        'BOUNDARY_ISUSED'    => '1',
                        'POLICY'           => $value,
                        'ROW_INDEX'   => $lastbound,
                        'TRANSFER_TYPE'   => $trfType
                      );
                    $this->_db->insert('TEMP_APP_BOUNDARY',$insertboundary);  
                    if(empty($data[$policy_group][$no])){
                      $no = 0;
                    }
                     $keywords = preg_split("/[\s,()]+/", $data[$policy_group][$no]);
                          // print_r($keywords);
                      $specialinsertid = 'S_'.$custid;
                              $insertboundarygroup = array(
                                'CHANGES_ID'    => $changesId,
                                'BOUNDARY_ID'    => $lastbound,
                                'GROUP_USER_ID'   => $specialinsertid,
                                'ROW_INDEX'       => $lastbound
                              );
                              // print_r($insertboundarygroup);
                              $this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$insertboundarygroup);

                          // if (in_array('SG', $keywords))
                          // {
                            
                          // }

                           foreach ($content['g_id'] as $a => $b) {
                       
                        $keywords = preg_split("/[\s,()]+/", $data[$policy_group][$no]);
                        // print_r($b);
                          if (in_array($b, $keywords))
                          {
                            $nogroup = sprintf("%02d", $a+1);
                            $specialinsertid = 'N_'.$custid.'_'.$nogroup;

                              $insertboundarygroup = array(
                                'CHANGES_ID'    => $changesId,
                                'BOUNDARY_ID'    => $lastbound,
                                'GROUP_USER_ID'   => $specialinsertid,
                                'ROW_INDEX'       => $lastbound
                              );
                              // print_r($insertboundarygroup);
                              $this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$insertboundarygroup);
                          }

                    }

                  }
                }
              }else if($data['policyindex'] == 0){
          $i = '';
        $lastbound = $resultdata['BOUNDARY_ID']+1;
                $amount_to = 'amount_to'.$i;
                $amount_from = 'amount_from'.$i;
                $policy_group = 'policy_group'.$i;
                $trfType = $data['policy_type']['0'];
        
                  foreach ($data[$policy_group] as $no => $value) {

                    $amount_from_val = Application_Helper_General::convertDisplayMoney($data[$amount_from][$no]);
                    $amount_to_val = Application_Helper_General::convertDisplayMoney($data[$amount_to][$no]);
  
                    if (in_array($trfType, $specialTrfType)) {
                      $amount_from_val = null;
                      $amount_to_val = null;
                    }

                    if($no == 0){
                      $no = 1; 
                    }
                    $lastbound = $lastbound + $no;

                     $insertboundary = array(
                        'CHANGES_ID'    => $changesId,
                        'BOUNDARY_ID'    => $lastbound,
                        'CCY_BOUNDARY'    => $data['ccy'][$no],
                        'BOUNDARY_MIN'    =>  $amount_from_val,
                        'BOUNDARY_MAX'    => $amount_to_val,
                        'CUST_ID'    => $custid,
                        'BOUNDARY_ISUSED'    => '1',
                        'POLICY'           => $value,
                        'ROW_INDEX'   => $lastbound,
                        'TRANSFER_TYPE'   => $trfType
                      );
          //  var_dump($insertboundary);die;
            try{
                    $this->_db->insert('TEMP_APP_BOUNDARY',$insertboundary);  
            }catch(Exception $e)
      {
      var_dump($e);die;
         //print_r($e);die('1');
         // $this->_db->rollBack();
      }
                     $keywords = preg_split("/[\s,()]+/", $data[$policy_group][$no]);
                          // print_r($keywords);
                      $specialinsertid = 'S_'.$custid;
                              $insertboundarygroup = array(
                                'CHANGES_ID'    => $changesId,
                                'BOUNDARY_ID'    => $lastbound,
                                'GROUP_USER_ID'   => $specialinsertid,
                                'ROW_INDEX'       => $lastbound
                              );
                             //  print_r($insertboundarygroup);die;
                              $this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$insertboundarygroup);

                          // if (in_array('SG', $keywords))
                          // {
                            
                          // }

                           foreach ($content['g_id'] as $a => $b) {
                      
                        $keywords = preg_split("/[\s,()]+/", $data[$policy_group][$no]);
                        // print_r($b);
                          if (in_array($b, $keywords))
                          {
                            $nogroup = sprintf("%02d", $a+1);
                            $specialinsertid = 'N_'.$custid.'_'.$nogroup;

                              $insertboundarygroup = array(
                                'CHANGES_ID'    => $changesId,
                                'BOUNDARY_ID'    => $lastbound,
                                'GROUP_USER_ID'   => $specialinsertid,
                                'ROW_INDEX'       => $lastbound
                              );
                              // print_r($insertboundarygroup);
                              $this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$insertboundarygroup);
                          }

                    }

                  }


        }
      //  die;
      }catch(Exception $e)
      {
      var_dump($e);die;
         //print_r($e);die;
         // $this->_db->rollBack();
      }
      // die;
            // $url = '/customer/index/index/cust_id/'.$data['cust_id'];
            // $this->_redirect($url);
          // die();
          Zend_Session::namespaceUnset('approvalgroup'.$cust_id);
          $this->setbackURL('/'.$this->_request->getModuleName());
            $this->_redirect('/notification/submited');
            // die;
            // $data = array(
            //           'CHANGES_ID'    => $changesId,
            //           'CCY_BOUNDARY'    => $insert['ccy'],
            //           'BOUNDARY_ID'   => $insert['bound'],
            //           'BOUNDARY_MIN'    => $insert['low'],
            //           'BOUNDARY_MAX'    => $insert['up'],
            //           'CUST_ID'       => $custid,
            //           'BOUNDARY_ISUSED' => $insert['active'],
            //           'ROW_INDEX'     => $insert['row'],
            //         );

            //   $this->_db->insert('TEMP_APP_BOUNDARY',$data);

        // print_r($data);die;
      try
      {
         $this->_db->beginTransaction();

         Application_Helper_General::writeLog('CCLS','View Detail Customer [Invalid Cust ID]');

         $this->_db->commit();
      }
      catch(Exception $e)
      {
      //var_dump($e);die;
         $this->_db->rollBack();
      }

      }
           // $this->_helper->getHelper('FlashMessenger')->addMessage('F');
           // $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
           // $this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));
         // }
      $paytypeall   = $optpaytypeAll = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);

      $paytypeall[19] = 'Cash Pooling Same Bank';
      $paytypeall[20] = 'Cash Pooling Same Bank';

      $this->view->paymenttype    = $paytypeall;



       $current = $this->_db->select()
                  ->from(array('A' => 'M_APP_BOUNDARY'),array('CCY_BOUNDARY','BOUNDARY_MIN','BOUNDARY_MAX','BOUNDARY_ID','CUST_ID','POLICY','TRANSFER_TYPE'));
      $current -> where("A.CUST_ID LIKE ".$this->_db->quote($cust_id));
      $current -> where("A.BOUNDARY_ISUSED LIKE '1'");
      $rcurrent = $current->query()->FetchAll();
      // print_r($rcurrent);die;
      $this->view->curlist = $rcurrent;

      $currentccy = $this->_db->select()
                  ->from(array('A' => 'M_APP_BOUNDARY'),array('CCY_BOUNDARY','TRANSFER_TYPE'));
      $currentccy -> where("A.CUST_ID LIKE ".$this->_db->quote($cust_id));
      $currentccy -> where("A.BOUNDARY_ISUSED LIKE '1'");
      $currentccy->group('A.CCY_BOUNDARY');
      $currentccy->group('A.TRANSFER_TYPE');
      $rcurrentccy = $currentccy->query()->FetchAll();

      // echo '<pre>';
      // print_r($rcurrentccy);die();
      $this->view->curlistccy = $rcurrentccy;

      $selectUsergroup = $this->_db->select()
                  ->from(array('M_APP_GROUP_USER'),array('*'))
                  ->where('CUST_ID = ?', $cust_id)
                  ->group('GROUP_USER_ID')
                  // echo $selectUser;die;
                  ->query()->fetchall();

      $selectUser = $this->_db->select()
                  ->from(array('M_APP_GROUP_USER'),array('*'))
                  ->where('CUST_ID = ?', $cust_id)
                  // echo $selectUser;die;
                  ->query()->fetchall();
        // print_r($selectUser);die;
        
        $userlist = '';
        foreach ($selectUsergroup as $key => $value) {
            foreach ($selectUser as $no => $val) {
              if($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']){
              if(empty($userlist))
                $userlist .= $val['USER_ID'];
              else
                $userlist .= ', '.$val['USER_ID'];
            }
                
            }
            $selectUsergroup[$key]['USER'] .= $userlist;
            $userlist = '';
            $spesial = 'S_'.$cust_id;
            if($value['GROUP_USER_ID']==$spesial){
                $selectUsergroup[$key]['GID'] .= 'SG';
            }else{
              $group = explode('_', $value['GROUP_USER_ID']);
            $alphabet = array(1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',22=>'V',23=>'W',24=>'X',25=>'Y',26=>'Z');
            
              // $cust = explode('_', $value['GROUP_USER_ID'])
              $selectUsergroup[$key]['GID'] .= $alphabet[(int)$group[2]];
            }
            
        }
        $this->view->selectUsergroup = $selectUsergroup;
        // echo '<pre>';
        // print_r($selectUsergroup);die;


        $this->view->cust_id = $cust_id;
        $this->view->status_type = $this->_masterglobalstatus;
        $this->view->modulename = $this->_request->getModuleName();




        //insert log
        try
        {
         $this->_db->beginTransaction();

         Application_Helper_General::writeLog('CCLS','View Customer Detail ['.$cust_id.']');

           $this->_db->commit();
      }
      catch(Exception $e)
      {
         $this->_db->rollBack();
      }
/*
     $cektrx = $this->_db->select()
              ->from('T_PSLIP');
        $cektrx -> where("CUST_ID = ? ",$cust_id);
        $cektrx -> where("PS_STATUS = ? ",'1');
        // echo $select2;die;
        $cektrx = $cektrx->query()->FetchAll(); 

         $select2 = $this->_db->select()
               ->from('TEMP_APP_BOUNDARY');
       $select2 -> where("CUST_ID LIKE ".$this->_db->quote($cust_id));
       // echo $select2;die;
       $cek = $select2->query()->FetchAll();  

      if(!empty($cektrx) || !empty($cek)){
        $this->view->error = true;
        $this->view->error_msg  = 'Not allowed to make changes. There is a transaction on payment workflow awaiting for approval.';
      } 
    */

      $selectdatachange = $this->_db->select()
            ->from('T_GLOBAL_CHANGES');
      $selectdatachange -> where("CUST_ID = ".$this->_db->quote($cust_id)." OR CUST_ID ='BANK'");
      $selectdatachange -> where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS ='RR'");
      // $selectdatachange -> where("CHANGES_STATUS = ? ",'WA');
      // $selectdatachange -> where("CHANGES_STATUS = ? ",'RR');
      $selectdatachange -> where("COMPANY_CODE = ? ",$cust_id);
      $selectdatachange -> where("DISPLAY_TABLENAME = ? ",'Submission Approver Matrix');

      // echo $selectdatachange;die();
       $selectdatachange1 = $selectdatachange->query()->FetchAll();
      
      if(!empty($selectdatachange1)){
        $this->view->error2 = true;
        $this->view->error_msg2  = 'Not allowed to make changes. There is suggestion awaiting for approval on data changes requests.';
      }

  }


  public function whereccyAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
        $selectccy =  $this->_db->select()
                  ->from( 'M_MINAMT_CCY',array('CCY_ID','CCY_ID'));

        $ccy = $this->_db->fetchAll($selectccy);  
        $arrccy = Application_Helper_Array::listArray($ccy,'CCY_ID','CCY_ID');
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($arrccy)){
        foreach($arrccy as $key => $row){
                $select = '';
            $optHtml.="<option value='".$row."' ".$select.">".$row."</option>";
        }
        }

        echo $optHtml;
    }


    public function wheretypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
        $arrTraType     = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
        unset($arrTraType['18']); 
        unset($arrTraType['22']); 
            $arrTraType['18'] = 'Disbursement';
        // print_r($arrTraType);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
    $setting = new Settings();
    $system_type = $setting->getSetting('system_type');
        if($system_type == '2'){
      unset($arrTraType);
     // $arrTraType['21'] = 'Money Movement';
    //  $arrTraType['19'] = 'Cash Pooling Same Bank';
   //   $arrTraType['23'] = 'Cash Pooling Other';
	 
    }
    $arrTraTypenew = array();
    $arrTraTypenew['50'] = 'Cash Collateral';
    $arrTraTypenew['51'] = 'Non Cash Collateral';
        if(!empty($arrTraTypenew)){
        foreach($arrTraTypenew as $key => $row){
                $select = '';
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }
        }

        echo $optHtml;
    }

}