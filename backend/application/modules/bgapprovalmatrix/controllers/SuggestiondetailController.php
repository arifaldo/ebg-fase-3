<?php

require_once 'Zend/Controller/Action.php';

class bgapprovalmatrix_SuggestiondetailController extends customer_Model_Customer
{
  protected $_moduleDB = 'CST';

  public function indexAction()
  {
    $changesid = strip_tags(trim($this->_getParam('changes_id')));

    if (!$this->view->hasPrivilege("VAMC")) {
      return $this->_redirect("/home/dashboard");
    }

    $submit = $this->_request->getParam("submit");
    if ($submit) {
      $select = $this->_db->fetchRow("SELECT CHANGES_STATUS, CREATED, CREATED_BY, KEY_VALUE, COMPANY_CODE, COMPANY_NAME FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = " . $this->_db->quote($changesid) . " AND (CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR')");

      if ($submit == "Approve") {
        $this->_db->update("T_GLOBAL_CHANGES", ["CHANGES_STATUS" => "AP"], ["CHANGES_ID = ?" => $changesid]);

        // M_APP_GROUP_BUSER ------------------------------------------

        $getNewDatas = $this->_db->select()
          ->from(["TEMP_APP_GROUP_BUSER"], ["*"])
          ->where("CHANGES_ID = ?", $changesid)
          ->query()->fetchAll();

        $this->_db->delete('M_APP_GROUP_BUSER', "1");

        // $checker_group_buser_id = [
        //   "S_" => "Special Group",
        //   "N__01" => "Kepala Cabang",
        //   "N__02" => "DBM",
        //   "N__03" => "BG Ops Approver 1",
        //   "N__04" => "BG Ops Approver 2",
        //   "N__05" => "BG Ops Approver 3"
        // ];

        foreach ($getNewDatas as $key => $getNewData) {
          if (isset($checker_group_buser_id[$getNewData["GROUP_BUSER_ID"]])) {
            $this->_db->beginTransaction();
            $this->_db->insert("M_APP_GROUP_BUSER", [
              "GROUP_BUSER_ID" => $getNewData["GROUP_BUSER_ID"],
              "BUSER_ID" => $getNewData["BUSER_ID"],
              "GROUP_NAME" => $getNewData["GROUP_NAME"]
            ]);
            $this->_db->commit();
            unset($checker_group_buser_id[$getNewData["GROUP_BUSER_ID"]]);
          } else {
            $this->_db->beginTransaction();
            $this->_db->insert("M_APP_GROUP_BUSER", [
              "GROUP_BUSER_ID" => $getNewData["GROUP_BUSER_ID"],
              "BUSER_ID" => $getNewData["BUSER_ID"],
              "GROUP_NAME" => $getNewData["GROUP_NAME"]
            ]);
            $this->_db->commit();
          }
        }

        // if (count($checker_group_buser_id) > 0) {
        //   foreach ($checker_group_buser_id as $key => $value) {
        //     $this->_db->beginTransaction();
        //     $this->_db->insert("M_APP_GROUP_BUSER", [
        //       "GROUP_BUSER_ID" => $key,
        //       "BUSER_ID" => "",
        //       "GROUP_NAME" => $value
        //     ]);
        //     $this->_db->commit();
        //   }
        // }

        $this->_db->delete('TEMP_APP_GROUP_BUSER', "1");

        // END M_APP_GROUP_BUSER

        // M_APP_BGBOUNDARY ------------------------------------------

        $getNewDatas = $this->_db->select()
          ->from(["TEMP_APP_BGBOUNDARY"], ["*"])
          ->where("CHANGES_ID = ?", $changesid)
          ->query()->fetchAll();

        $this->_db->delete('M_APP_BGBOUNDARY', "1");

        foreach ($getNewDatas as $key => $getNewData) {
          $this->_db->beginTransaction();
          $this->_db->insert("M_APP_BGBOUNDARY", [
            "CCY_BOUNDARY" => $getNewData["CCY_BOUNDARY"],
            "TRANSFER_TYPE" => $getNewData["TRANSFER_TYPE"],
            "BOUNDARY_MIN" => $getNewData["BOUNDARY_MIN"],
            "BOUNDARY_MAX" => $getNewData["BOUNDARY_MAX"],
            "CREATED" => new Zend_Db_Expr("now()"),
            "CREATEDBY" => $this->_userIdLogin,
            "LASTUPDATED" => new Zend_Db_Expr("now()"),
            "LASTUPDATEDBY" => $select["CREATED_BY"],
            "BOUNDARY_ISUSED" => $getNewData["BOUNDARY_ISUSED"],
            "ROW_INDEX" => $getNewData["ROW_INDEX"],
            "POLICY" => $getNewData["POLICY"],
          ]);
          $this->_db->commit();
        }

        $this->_db->delete('TEMP_APP_BGBOUNDARY', "1");

        Application_Helper_General::writeLog("AAMC", "Approve Bank BG Approval Matrix Changes");

        // END M_APP_BGBOUNDARY

        $this->view->refresh = 1;
      }
      if ($submit == "Reject") {
        $this->_db->update("T_GLOBAL_CHANGES", ["CHANGES_STATUS" => "RJ"], ["CHANGES_ID = ?" => $changesid]);
        $this->_db->delete('TEMP_APP_GROUP_BUSER', "1");
        $this->_db->delete('TEMP_APP_BGBOUNDARY', "1");

        $data = ["status" => "success"];
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data);
        die();
      }
      if ($submit == "Request Repair") {
        $this->_db->update("T_GLOBAL_CHANGES", ["CHANGES_STATUS" => "RR"], ["CHANGES_ID = ?" => $changesid]);

        $data = ["status" => "success"];
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data);
        die();
      }
      if ($submit == "Repair") {

        Application_Helper_General::writeLog("RAMC", "Repair Bank BG Approval Matrix Changes");

        return $this->_redirect("bgapprovalmatrix/repairapprovalgroup/index/changes_id/" . $changesid);
      }
    } else {
      $this->_helper->layout()->setLayout('popup');

      $params = $this->_request->getParams();
      $select = $this->_db->fetchRow("SELECT CHANGES_STATUS, CREATED, CREATED_BY, KEY_VALUE, COMPANY_CODE, COMPANY_NAME FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = " . $this->_db->quote($changesid) . " AND (CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR')");

      if ($select) {
        $this->view->changes_id = $changesid;
        $this->view->created = $select['CREATED'];
        $this->view->createdby = $select['CREATED_BY'];
        $this->view->cust_id = $cust_id = $select['COMPANY_CODE'];
        $this->view->cust_name = $select['COMPANY_NAME'];
        // foreach($options as $codestat=>$descstat)
        // {
        //   if($codestat == $select['CHANGES_STATUS'])
        //   {
        //     $this->view->status = $descstat;
        //   }
        // }

        //      DATA BARU       
        $selectnew = $this->_db->select()
          ->FROM(array('B' => 'TEMP_APP_GROUP_BUSER'))
          ->where('CHANGES_ID = ?', $changesid)
          ->query()->FetchAll();
        // print_r($selectnew);die;
        $newAppGroupArray = array();
        $listgroup = array();
        foreach ($selectnew as $row) {
          $newAppGroupArray[$row['GROUP_NAME']][] = $row['BUSER_ID'];
          $listgroup[$row['GROUP_NAME']] = $row['GROUP_BUSER_ID'];
        }
        $this->view->databarugroup = $listgroup;
        $this->view->databaru = $newAppGroupArray;
        // echo '<pre>';
        // print_r($newAppGroupArray);die;
        //      DATA LAMA     
        $selectold = $this->_db->select()
          ->from(array('A' => 'M_APP_GROUP_BUSER'))
          ->query()->FetchAll();


        $oldAppGroupArray = array();
        $oldgroup = array();
        foreach ($selectold as $row) {
          $oldAppGroupArray[$row['GROUP_NAME']][] = $row['BUSER_ID'];
          $oldgroup[$row['GROUP_NAME']] = $row['GROUP_BUSER_ID'];
        }
        $this->view->datalamagroup = $oldgroup;
        $this->view->datalama = $oldAppGroupArray;

        $arrTraTypenew = array();
        $arrTraTypenew['50'] = 'Cash Collateral';
        $arrTraTypenew['51'] = 'Non Cash Collateral';
        $this->view->paymenttype    = $arrTraTypenew;
        //    print_r($paytypeall);die;
        //matrix

        $suggested = $this->_db->select()
          ->from(array('A' => 'TEMP_APP_BGBOUNDARY'), array('CCY_BOUNDARY', 'BOUNDARY_MIN', 'BOUNDARY_MAX', 'ROW_INDEX', 'POLICY', 'TRANSFER_TYPE'));
        $suggested->where("A.CHANGES_ID LIKE " . $this->_db->quote($changesid));
        $suggested->where("A.BOUNDARY_ISUSED LIKE '1'");
        $rsuggested = $suggested->query()->FetchAll();

        $newSuggestArray = array();
        foreach ($rsuggested as $row) {
          $newSuggestArray[$row['TRANSFER_TYPE']][] = $row['BOUNDARY_MAX'];
        }
        $this->view->newSuggest = $newSuggestArray;
        // echo $suggested;die;
        // print_r($rsuggested);die;
        $this->view->suglist = $rsuggested;

        $suggested = $this->_db->select()
          ->from(array('A' => 'TEMP_APP_BGBOUNDARY'), array('CCY_BOUNDARY', 'TRANSFER_TYPE'));
        $suggested->where("A.CHANGES_ID LIKE " . $this->_db->quote($changesid));
        $suggested->where("A.BOUNDARY_ISUSED LIKE '1'");
        $suggested->group('A.CCY_BOUNDARY');
        $suggested->group('A.TRANSFER_TYPE');
        // echo $suggested;die;
        $rsuggested = $suggested->query()->FetchAll();

        // print_r($rsuggested);die;
        $this->view->suglistccy = $rsuggested;

        $current = $this->_db->select()
          ->from(array('A' => 'M_APP_BGBOUNDARY'), array('CCY_BOUNDARY', 'BOUNDARY_MIN', 'BOUNDARY_MAX', 'BOUNDARY_ID', 'POLICY', 'TRANSFER_TYPE'));

        $current->where("A.BOUNDARY_ISUSED LIKE '1'");
        $rcurrent = $current->query()->FetchAll();
        $oldSuggestArray = array();
        foreach ($rsuggested as $row) {
          $oldSuggestArray[$row['TRANSFER_TYPE']][] = $row['BOUNDARY_MAX'];
        }
        $this->view->oldSuggest = $oldSuggestArray;
        $this->view->curlist = $rcurrent;

        $currentccy = $this->_db->select()
          ->from(array('A' => 'M_APP_BGBOUNDARY'), array('CCY_BOUNDARY', 'TRANSFER_TYPE'));

        $currentccy->where("A.BOUNDARY_ISUSED LIKE '1'");
        $currentccy->group('A.CCY_BOUNDARY');
        $currentccy->group('A.TRANSFER_TYPE');
        $rcurrentccy = $currentccy->query()->FetchAll();
        $this->view->curlistccy = $rcurrentccy;

        // Zend_Debug::dump($oldAppGroupArray);die;
        $this->view->cek = 1;
      } else {
        $this->_redirect('/notification/invalid/index');
        $this->view->cek = 0;
      }
    }

    if (!$this->_request->isPost()) {
      Application_Helper_General::writeLog('AGCL', 'Viewing Update Approver Group');
    }
  }
}
