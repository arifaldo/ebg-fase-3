<?php
/**
 * Tempcustomer model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempbiller extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'CST';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) 
	{
		//query from T_GLOBAL_CHANGES
		$globalChanges = $this->dbObj->select()
						  			->from('T_GLOBAL_CHANGES')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
									
		//query from TEMP_SERVICE_PROVIDER
		$biller = $this->dbObj->select()
						  			->from('TEMP_SERVICE_PROVIDER')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($biller))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
	
	    //==========================================================================================================
		//insert to master table M_SERVICE_PROVIDER
		$insertArr = array_diff_key($biller,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		$insertArr['PROVIDER_CREATED'] = new Zend_Db_Expr('GETDATE()');
		$insertArr['PROVIDER_CREATEDBY']   = $actor;
		$insertArr['PROVIDER_UPDATED'] = new Zend_Db_Expr('GETDATE()');
		$insertArr['PROVIDER_UPDATEDBY']   = $actor;
		
		$billerInsert = $this->dbObj->insert('M_SERVICE_PROVIDER',$insertArr);
		if(!(boolean)$billerInsert) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}

		//insert SGO_PRODUCT_CODE to master table M_PROVIDER_CATALOG
		$select = $this->dbObj->select()
				->from(array('A' => 'M_SERVICE_PROVIDER'),array('PROVIDER_ID'));
		$select->where("A.PROVIDER_CODE = ? ", $insertArr['PROVIDER_CODE']);				
		$dataPCode = $this->dbObj->fetchRow($select);
		
		$providerId = $dataPCode['PROVIDER_ID'];
		
		$insertArrProvCat['PROVIDER_ID'] = $providerId;
		$insertArrProvCat['SGO_PRODUCT_CODE'] = 'SDIRPAY';
		$insertArrProvCat['PRODUCT_NAME'] = 'DIRECT PAYMENT';
		$insertArrProvCat['PRODUCT_ALIAS_NAME'] = 'DIRPAY';
		$insertArrProvCat['NOMINAL'] = '0';
		$insertArrProvCat['MARKUP_PRICE'] = '1';
		$insertArrProvCat['CATALOG_STATUS'] = 'A';
		$insertArrProvCat['PROVIDER_SUGGESTED'] = new Zend_Db_Expr('GETDATE()');
		$insertArrProvCat['PROVIDER_SUGGESTEDBY'] = $actor;
		$insertArrProvCat['PROVIDER_UPDATED'] = new Zend_Db_Expr('GETDATE()');
		$insertArrProvCat['PROVIDER_UPDATEDBY'] = $actor;
				
		$sgoProductCodeInsert = $this->dbObj->insert('M_PROVIDER_CATALOG',$insertArrProvCat);
		// end
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		//query from TEMP_SERVICE_PROVIDER
		$biller = $this->dbObj->select()
						   ->from('TEMP_SERVICE_PROVIDER')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($biller))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
        	
		$cek = $this->dbObj->fetchOne(
						$this->dbObj->select()
							->from(array('B' => 'M_SERVICE_PROVIDER'),array('B.PROVIDER_ID'))
							->where("B.PROVIDER_ID = ?", $biller['PROVIDER_ID'])
					);
				
		if($cek)
		{
			//update master table M_SERVICE_PROVIDER
			$updateArr = array(
												'PROVIDER_CODE' => $biller['PROVIDER_CODE'],
												'PROVIDER_NAME' => $biller['PROVIDER_NAME'],
												'PROVIDER_UPDATED' => new Zend_Db_Expr('GETDATE()'),
												'PROVIDER_UPDATEDBY' => $actor
											);
			$whereArr = array(
							'PROVIDER_ID = ?'     => $biller['PROVIDER_ID'],
						);

			$query = $this->dbObj->update('M_SERVICE_PROVIDER',$updateArr,$whereArr);
		}
		else
		{
			//insert master table M_SERVICE_PROVIDER
			$insertArr = array_diff_key($biller,array('TEMP_ID'=>'','CHANGES_ID'=>''));
			$insertArr['PROVIDER_SUGGESTED']   = $biller['PROVIDER_SUGGESTED'];
			$insertArr['PROVIDER_SUGGESTEDBY'] = $biller['PROVIDER_SUGGESTEDBY'];
			$insertArr['PROVIDER_CREATED']     = new Zend_Db_Expr('GETDATE()');
			$insertArr['PROVIDER_CREATEDBY']   = $actor;
			$insertArr['PROVIDER_UPDATED']     = new Zend_Db_Expr('GETDATE()');
			$insertArr['PROVIDER_UPDATEDBY']   = $actor;
			
			$query = $this->dbObj->insert('M_SERVICE_PROVIDER',$insertArr);
		}
		
		if(!(boolean)$query) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_SERVICE_PROVIDER
		$biller = $this->dbObj->select()
						   ->from('TEMP_SERVICE_PROVIDER')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($biller))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
        	
		$cek = $this->dbObj->fetchOne(
						$this->dbObj->select()
							->from(array('B' => 'M_SERVICE_PROVIDER'),array('B.PROVIDER_ID'))
							->where("B.PROVIDER_ID = ?", $biller['PROVIDER_ID'])
					);
				
		if($cek)
		{
			//update master table M_SERVICE_PROVIDER
			$updateArr = array(
												'PROVIDER_SUGGESTED' => $biller['PROVIDER_SUGGESTED'],
												'PROVIDER_SUGGESTEDBY' => $biller['PROVIDER_SUGGESTEDBY'],
												'PROVIDER_STATUS' => $biller['PROVIDER_STATUS'],
												'PROVIDER_UPDATED' => new Zend_Db_Expr('GETDATE()'),
												'PROVIDER_UPDATEDBY' => $actor
											);
			$whereArr = array(
							'PROVIDER_ID = ?'     => $biller['PROVIDER_ID'],
						);

			$query = $this->dbObj->update('M_SERVICE_PROVIDER',$updateArr,$whereArr);
		}
		
		if(!(boolean)$query) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}

	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_SERVICE_PROVIDER
		$biller = $this->dbObj->select()
						   ->from('TEMP_SERVICE_PROVIDER')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($biller))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
        	
		$cek = $this->dbObj->fetchOne(
						$this->dbObj->select()
							->from(array('B' => 'M_SERVICE_PROVIDER'),array('B.PROVIDER_ID'))
							->where("B.PROVIDER_ID = ?", $biller['PROVIDER_ID'])
					);
				
		if($cek)
		{
			//update master table M_SERVICE_PROVIDER
			$updateArr = array(
												'PROVIDER_SUGGESTED' => $biller['PROVIDER_SUGGESTED'],
												'PROVIDER_SUGGESTEDBY' => $biller['PROVIDER_SUGGESTEDBY'],
												'PROVIDER_STATUS' => $biller['PROVIDER_STATUS'],
												'PROVIDER_UPDATED' => new Zend_Db_Expr('GETDATE()'),
												'PROVIDER_UPDATEDBY' => $actor
											);
			$whereArr = array(
							'PROVIDER_ID = ?'     => $biller['PROVIDER_ID'],
						);

			$query = $this->dbObj->update('M_SERVICE_PROVIDER',$updateArr,$whereArr);
		}
		
		if(!(boolean)$query) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveDelete() 
	{
		//query from TEMP_SERVICE_PROVIDER
		$dailylimit = $this->dbObj->select()
						   		  ->from('TEMP_SERVICE_PROVIDER')
						  		  ->where('CHANGES_ID = ?',$this->_changeId)
						  	      ->query()
						  	      ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($dailylimit))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
        }
						  
		//$updateArr = array('DAILYLIMIT_STATUS' => 3);
		$updateArr = array('PROVIDER_STATUS' => 3);
		
		//$updateArr['PROVIDER_UPDATED'] = new Zend_Db_Expr('GETDATE()');
		$whereArr = array('PROVIDER_ID = ?'     => (string)$dailylimit['PROVIDER_ID']);
		
		$customerupdate = $this->dbObj->update('M_SERVICE_PROVIDER',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller)';
			return false;
		}

		
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			    return false;
		
		return true;
	}
	
	
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() 
	{
		//delete from TEMP_SERVICE_PROVIDER
		$acctdelete = $this->dbObj->delete('TEMP_SERVICE_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_SERVICE_PROVIDER
		$acctdelete = $this->dbObj->delete('TEMP_SERVICE_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_SERVICE_PROVIDER
		$customerdelete = $this->dbObj->delete('TEMP_SERVICE_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_SERVICE_PROVIDER
		$customerdelete = $this->dbObj->delete('TEMP_SERVICE_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	public function deleteDelete() 
	{
		$customerdelete = $this->dbObj->delete('TEMP_SERVICE_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}

		
    public function approveDeactivate(){

	}

	public function approveActivate(){

	}
	
    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}
	
	
	
}