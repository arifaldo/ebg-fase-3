<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class servicearea_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
		$this->_helper->layout()->setLayout('newpopup');
	}

    public function indexAction()
	{
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->view->modulename);

		$model = new servicearea_Model_Servicearea();


		if($this->_request->isPost() )
		{
					$filters = array('area_name'=> array('StringTrim','StripTags','StringToUpper'));

					$validators = array(
											'area_name' => array('NotEmpty',
														 //new Zend_Validate_Alnum(array('allowWhiteSpace' => true)),
														 // new Zend_Validate_StringLength(array('min'=>1,'max'=>15)),
														 array	(
																		'Db_NoRecordExists',
																		array	(
																					'table'=>'M_SERVICE_AREA',
																					'field'=>'AREA_NAME'
																				)
																	),
														 'messages' => array(
																			 $this->language->_('Can not be empty'),
																			 //'Format Area Layanan salah, tidak boleh menggunakan karakter khusus',
																			 // 'Maksimum panjang karakter adalah 15 karakter',
																			 'Service Area already exist',
																			 )
														),
										   );


			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				$content = array(
									'AREA_NAME'  => $zf_filter_input->area_name,
									// 'LAST_UPDATED' => new Zend_Db_Expr('GETDATE()'),
									'LAST_UPDATEDBY' => $this->_userIdLogin
								);

				try
				{

					//-------- insert --------------
					$this->_db->beginTransaction();
					// print_r($content);die;
					$model->insertData($content);
					//Application_Helper_General::writeLog('OBAD','Add Online Bank. Bank ID : ['.$zf_filter_input->bank_code.'], Bank Name : ['.$zf_filter_input->bank_name.']');
					$this->_db->commit();

					$tempdata = new Zend_Session_Namespace('tempdata');
					$tempdata->area_name = $zf_filter_input->area_name;

					//$this->view->success = true;
					//$this->view->error = false;

					$this->_redirect('/servicearea/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = 'exeption';
					$this->view->error_msg = $errorMsg;
				}
			}
			else
			{
				$this->fillParam($zf_filter_input);
				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
				foreach($error as $keyRoot => $rowError)
				{
				   foreach($rowError as $errorString)
				   {
						$errID = 'error_'.$keyRoot;
						$this->view->$errID = $errorString;
				   }
				}
				$this->view->error = true;
			}
		}

		//Application_Helper_General::writeLog('OBAD','Add Online Bank Table');
	}

    private function fillParam($zf_filter_input)
	{
		$this->view->area_name     = ($zf_filter_input->isValid('area_name')) ? $zf_filter_input->area_name : $this->_getParam('area_name');
	}
}