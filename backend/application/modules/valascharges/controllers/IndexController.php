<?php


require_once 'Zend/Controller/Action.php';


class valascharges_IndexController extends Application_Main
{
	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()->distinct()
					->from(array('M_CUSTOMER'),array('CUST_ID'))
					->where("CUST_STATUS != '3'")
					->order('CUST_ID ASC')
				 	-> query() ->fetchAll();
		//Zend_Debug::dump($status); die;
		$this->view->var=$select;

		/*$select3 = $this->_db->select()
				->from(array('B' => 'M_CHARGES_WITHIN'),array('*'));
		$coba = $this->_db->fetchAll($select3);
		Zend_Debug::dump($select3); die;*/

		$list = array(	''				=>	'--- '.$this->language->_('Any Value').' ---',
						'Enabled'		=>	$this->language->_('Enabled'),
						'Disabled' 		=> 	$this->language->_('Disabled'));
		$this->view->optionlist = $list;

		$ceklist = array(	'0'		=>	$this->language->_('Disabled'),
							'1' 	=> 	$this->language->_('Enabled'));

		$companyCode = $this->language->_('Company Code');
		$companyName = $this->language->_('Company');
		$company_name = $this->language->_('Company Name');
		$monthlyFee = $this->language->_('Monthly Fee');
		$suggestedDate = $this->language->_('Suggested Date');
		$suggester = $this->language->_('Suggester');

		$fields = array	(
							// 'Company Code'  			=> array	(
							// 										'field' => 'B.CUST_ID',
							// 										'label' => $companyCode,
							// 										'sortable' => true
							// 									),
							'Company'  			=> array	(
																	'field' => 'COMPANY',
																	'label' => $companyName,
																	'sortable' => true
																),
							'CompanyName'  			=> array	(
																	'field' => 'COMPANY_NAME',
																	'label' => $company_name,
																	'sortable' => true
																),
							'Charges'  					=> array	(
																	'field' => 'CUST_CHARGES_STATUS',
																	'label' => $this->language->_('Charges'),
																	'sortable' => true
																),
							'Monthly Fee'  		=> array	(
																	'field' => 'CUST_MONTHLYFEE_STATUS',
																	'label' => $monthlyFee,
																	'sortable' => true
																),
							'Suggest Date'  		=> array	(
																	'field' => 'CUST_SUGGESTED',
																	'label' => $suggestedDate,
																	'sortable' => true
																),
							'Suggestor'  		=> array	(
																	'field' => 'CUST_SUGGESTEDBY',
																	'label' => $suggester,
																	'sortable' => true
																),
						);

		$filterlist = array("CUST_ID","MONTHLY_FEE_STATUS","CUST_NAME","SUGGESTOR");
		$this->view->filterlist = $filterlist;

		$filterArr = array('filter' 			=> array('StripTags','StringTrim'),
	                       'custid'    			=> array('StripTags','StringTrim'),
	                       'custname'  			=> array('StripTags','StringTrim','StringToUpper'),
						   'suggestor'  		=> array('StripTags','StringTrim','StringToUpper'),
						   'chargeStatus'  		=> array('StripTags','StringTrim'),
						   'monthlyfeeStatus'  	=> array('StripTags','StringTrim'),
						   'fDateFrom'  		=> array('StripTags','StringTrim'),
						   'fDateTo'  			=> array('StripTags','StringTrim'),
	                      );

	    $validator = array('filter' 			=> array(),
	                       'custid'    			=> array(),
	                       'custname'  			=> array(),
						   'suggestor'  		=> array(),
						   'chargeStatus'  		=> array(),
						   'monthlyfeeStatus'  	=> array(),
						   'fDateFrom'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo'  			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );


	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $custid = html_entity_decode($zf_filter->getEscaped('custid'));
		$custname = html_entity_decode($zf_filter->getEscaped('custname'));
		$suggestor = html_entity_decode($zf_filter->getEscaped('suggestor'));
		$chargeStatus = html_entity_decode($zf_filter->getEscaped('chargeStatus'));
		$monthlyfeeStatus = html_entity_decode($zf_filter->getEscaped('monthlyfeeStatus'));
		$datefrom = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$dateto = html_entity_decode($zf_filter->getEscaped('fDateTo'));
		//Zend_Debug::dump($this->_masterglobalstatus); die;

		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');

		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		/*if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}*/

		$chargeStatusarr = "(CASE B.CUST_CHARGES_STATUS ";
  		foreach($ceklist as $key=>$val)
  		{
   			$chargeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$chargeStatusarr .= " END)";

  		$adminfeeStatusarr = "(CASE B.CUST_MONTHLYFEE_STATUS ";
  		foreach($ceklist as $key=>$val)
  		{
   			$adminfeeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$adminfeeStatusarr .= " END)";

		$select2 = $this->_db->select()
						->from(array('B' => 'M_CUSTOMER'),array('B.CUST_ID',
																'B.CUST_NAME',
																'CUST_CHARGES_STATUS' => $chargeStatusarr,
																'CUST_MONTHLYFEE_STATUS' => $adminfeeStatusarr,
																'B.CUST_SUGGESTED',
																'COMPANY'	=> new Zend_Db_Expr("CONCAT(B.CUST_NAME , ' (' , B.CUST_ID , ')  ' )"),
																'B.CUST_SUGGESTEDBY'))
						->where("CUST_STATUS != '3'");
		$arr = $this->_db->fetchAll($select2);

		//if($filter == null || $filter == 'Set Filter')
		if($filter == true)
		{
		 $this->view->fDateTo    = $dateto;
		 $this->view->fDateFrom  = $datefrom;
		//Zend_Debug::dump($custid); die;

		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);
	            }

	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
					//Zend_Debug::dump($dateto); die;
	            }

		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) >= ".$this->_db->quote($datefrom));

	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) <= ".$this->_db->quote($dateto));

	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}

		if($filter == true)
		{
			if($chargeStatus)
			{
				$this->view->chargeStatus = $chargeStatus;
				if($chargeStatus == "Enabled")
				{
					$cekchargeStatus = "1";
				}
				if($chargeStatus == "Disabled")
				{
					$cekchargeStatus = "0";
				}
				$select2->where("B.CUST_CHARGES_STATUS LIKE ".$this->_db->quote($cekchargeStatus));
			}

			if($monthlyfeeStatus)
			{
				$this->view->adminfeeStatus = $monthlyfeeStatus;
				if($monthlyfeeStatus == "Enabled")
				{
					$cekadminfeeStatus = "1";
				}
				if($monthlyfeeStatus == "Disabled")
				{
					$cekadminfeeStatus = "0";
				}
				$select2->where("CUST_MONTHLYFEE_STATUS LIKE ".$this->_db->quote($cekadminfeeStatus));
			}

		    if($custid)
		    {
	       		$this->view->cekcustid = $custid;
	       		$select2->where("B.CUST_ID LIKE ".$this->_db->quote($custid));
		    }

			if($custname)
			{
	       		$this->view->custname = $custname;
	       		$select2->where("B.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));
			}

			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
	       		$select2->where("B.CUST_SUGGESTEDBY LIKE ".$this->_db->quote('%'.$suggestor.'%'));
		    }
		}



		//$select2->order($sortBy.' '.$sortDir);
		$select2->order($sortBy.' '.$sortDir);





		$result2 = $select2->query()->FetchAll();

		$sqlQueryHoliday = "
			SELECT 
				CHARGE_ID,
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT ,
				CHARGE_APPROVEDBY,
				CHARGE_APPROVED,
				CHARGE_SUGGESTED,
				CHARGE_SUGGESTEDBY
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM M_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?) B
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
			//ORDER BY CHARGE_ID DESC";
		// $custid = 'GLOBAL';

		foreach ($result2 as $key => $val) {
			// print_r($result2);die;

					 $transfee = $this->_db->fetchAll($sqlQueryHoliday,array('3',$val['CUST_ID']));
					 // echo $sqlQueryHoliday;
					 // var_dump($transfee);die;
				 if(empty($transfee['0']['CHARGE_ID'])){
				 	// die;
				 	 $transfee = $this->_db->fetchAll($sqlQueryHoliday,array('3','GLOBAL'));
				 }

		       $fullfee = $this->_db->fetchAll($sqlQueryHoliday,array('4',$val['CUST_ID']));
		       if(empty($fullfee['0']['CHARGE_ID'])){
		       	 $fullfee = $this->_db->fetchAll($sqlQueryHoliday,array('4','GLOBAL'));
		       }
		       	 $prfee = $this->_db->fetchAll($sqlQueryHoliday,array('5',$val['CUST_ID']));

		        if(empty($prfee['0']['CHARGE_ID'])){
		        	 $prfee = $this->_db->fetchAll($sqlQueryHoliday,array('5','GLOBAL'));
		        }

		        if(!empty($prfee)){
		        foreach($prfee as $key=>$value)
				{

					// if(!empty($key)){
						$keyw = $val['CUST_ID'].'prccy'.$value['CCY_ID'];
						$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

						$keymin = $val['CUST_ID'].'prmin'.$value['CCY_ID'];
						// print_r($keyamt);
						// echo "<br>";
						if(empty($value['CHARGE_PROV_MIN_AMT'])){
							$value['CHARGE_PROV_MIN_AMT'] = '-';
						}
						$this->view->$keymin = $value['CHARGE_PROV_MIN_AMT'];

						$keymax = $val['CUST_ID'].'prmax'.$value['CCY_ID'];
						// print_r($keyamt);
						// echo "<br>";
						if(empty($value['CHARGE_PROV_MAX_AMT'])){
							$value['CHARGE_PROV_MAX_AMT'] = '-';
						}
						$this->view->$keymax = $value['CHARGE_PROV_MAX_AMT'];

						$keypct = $val['CUST_ID'].'prpct'.$value['CCY_ID'];
						// print_r($keyamt);prccyAUD
						// echo "<br>";
						if(empty($value['CHARGE_PCT'])){
							$value['CHARGE_PCT'] = '-';
						}
						$this->view->$keypct = $value['CHARGE_PCT'];


						$keyccy = $val['CUST_ID'].'prccy'.$value['CCY_ID'];
						// print_r($keyamt);prccyAUD
						// echo "<br>";
						if(empty($value['CHARGE_AMOUNT_CCY'])){
							$value['CHARGE_AMOUNT_CCY'] = '-';
						}
						$this->view->$keyccy = $value['CHARGE_AMOUNT_CCY'];
					// }
				}
				}
				if(!empty($fullfee)){
		       foreach($fullfee as $key=>$value)
				{
					// if(!empty($key)){
						$keyw = $val['CUST_ID'].'faccy'.$value['CCY_ID'];
						if(empty($value['CHARGE_AMOUNT_CCY'])){
							$value['CHARGE_AMOUNT_CCY'] = '-';
						}
						$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];
						if(empty($value['CCY_ID'])){
							$value['CCY_ID'] = '-';
						}
						$keyamt = $val['CUST_ID'].'faamt'.$value['CCY_ID'];
						// print_r($keyamt);
						// echo "<br>";
						if(empty($value['CHARGE_AMT'])){
							$value['CHARGE_AMT'] = '-';
						}
						// echo $value['CHARGE_AMT'];echo '<br>';
						$this->view->$keyamt = $value['CHARGE_AMT'];
					// }
				}
				}
		       // echo "<pre>";
		       // print_r($transfee);
				if($transfee){
		       foreach($transfee as $key=>$value)
				{
					// if(!empty($key)){
						$keyw = $val['CUST_ID'].'tfccy'.$value['CCY_ID'];
						if(empty($value['CHARGE_AMOUNT_CCY'])){
							$value['CHARGE_AMOUNT_CCY'] = '-';
						}
						$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

						$keyamt = $val['CUST_ID'].'tfamt'.$value['CCY_ID'];
						// print_r($keyamt);
						// echo "<br>";
						if(empty($value['CHARGE_AMT'])){
							$value['CHARGE_AMT'] = '-';
						}
						$this->view->$keyamt = $value['CHARGE_AMT'];

						$keysug = $val['CUST_ID'].'tfsug'.$value['CCY_ID'];

						if(empty($value['CHARGE_SUGGESTED'])){
							$value['CHARGE_SUGGESTED'] = '-';
						}
						$this->view->$keysug = $value['CHARGE_SUGGESTED'];

						$keysugby = $val['CUST_ID'].'tfsugby'.$value['CCY_ID'];

						if(empty($value['CHARGE_SUGGESTEDBY'])){
							$value['CHARGE_SUGGESTEDBY'] = '-';
						}
						$this->view->$keysugby = $value['CHARGE_SUGGESTEDBY'];

						

						$keyapp = $val['CUST_ID'].'tfapp'.$value['CCY_ID'];

						if(empty($value['CHARGE_APPROVED'])){
							$value['CHARGE_APPROVED'] = '-';
						}
						$this->view->$keyapp = $value['CHARGE_APPROVED'];


						$keyappby = $val['CUST_ID'].'tfappby'.$value['CCY_ID'];

						if(empty($value['CHARGE_APPROVEBY'])){
							$value['CHARGE_APPROVEDBY'] = '-';
						}
						$this->view->$keyappby = $value['CHARGE_APPROVEDBY'];


						$keycharacct = $val['CUST_ID'].'tfcharacc'.$value['CCY_ID'];

						if(empty($value['CHARGE_ACCT'])){
							$value['CHARGE_ACCT'] = '-';
						}
						$this->view->$keycharacct = $value['CHARGE_ACCT'];
					// }
				}
				}
		}

      


    	$this->paging($select2,30);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	$header = Application_Helper_Array::simpleArray($fields, "label");
    	$arr = $this->_db->fetchAll($select2,30);

    	if($this->_request->getParam('print') == 1){

			$this->_forward('printvalas', 'index', 'widget', array('data_content' => $arr));
		}

    	if($csv || $pdf)
    	{
    		
    		foreach($arr as $key=>$value)
			{
				$arr[$key]["CUST_SUGGESTED"] = Application_Helper_General::convertDate($value["CUST_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}

	    	if($csv)
			{
				Application_Helper_General::writeLog('CHLS','Download CSV Company Charges List');
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}

			if($pdf)
			{
				Application_Helper_General::writeLog('CHLS','Download PDF Company Charges List');
				$this->_helper->download->pdf($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('CHLS','View Company Charges List');
    	}
	}
}
