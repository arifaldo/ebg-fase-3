<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';


class trafficapicharges_NewController extends Application_Main
{
	// protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
	    $this->_helper->layout()->setLayout('newlayout');    

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	 
    	}

    	$this->setbackURL('/trafficapicharges');

    	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
	    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    	
    	$sessionNameConfrim = new Zend_Session_Namespace('apicharges');

		if (empty($cust_id)) {
			$cust_id = $sessionNameConfrim->cust_id;
		}
		else{
			Zend_Session::namespaceUnset('apicharges');
			$sessionNameConfrim = new Zend_Session_Namespace('apicharges');
			$sessionNameConfrim->cust_id = $cust_id;
		}

		$selectglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_API'),array('*'));
		$selectglobal ->where("A.CUST_ID = 'GLOBAL'");
		$arrglobal = $this->_db->fetchAll($selectglobal);

		$this->view->globalcharge = $arrglobal;

    	//-----------------------------select data charges from db---------------------------
    	$select = $this->_db->select()->distinct()
				->from(array('B' => 'M_CUSTOMER'),array('B.CUST_ID',
														'B.CUST_NAME',
														'B.CUST_SUGGESTED',
														'B.CUST_SUGGESTEDBY'))
				->joinLeft(array('C' => 'M_CHARGES_API'),'B.CUST_ID = C.CUST_ID',
						array(
								'coalesce(C.BALANCE_INQUIRY,"") AS BALANCE_INQUIRY', 
								'coalesce(C.ACCOUNT_STATEMENT,"") AS ACCOUNT_STATEMENT', 
								'coalesce(C.INHOUSE_TRANSFER,"") AS INHOUSE_TRANSFER',
								'coalesce(C.SKN_TRANSFER,"") AS SKN_TRANSFER',
								'coalesce(C.RTGS_TRANSFER,"") AS RTGS_TRANSFER',
								'coalesce(C.ONLINE_TRANSFER,"") AS ONLINE_TRANSFER',
								// 'coalesce(C.INHOUSE_BENEF_INQUIRY,"") AS INHOUSE_BENEF_INQUIRY',
								'coalesce(C.INTERBANK_BENEF_INQUIRY,"") AS INTERBANK_BENEF_INQUIRY',
								'coalesce(C.KURS_RATE_INQUIRY,"") AS KURS_RATE_INQUIRY',
								'coalesce(C.CHARGES_SUGGESTED,"") AS CHARGES_SUGGESTED',
								'coalesce(C.CHARGES_SUGGESTEDBY,"") AS CHARGES_SUGGESTEDBY',
								'coalesce(C.CHARGES_APPROVED,"") AS CHARGES_APPROVED',
								'coalesce(C.CHARGES_APPROVEDBY,"") AS CHARGES_APPROVEDBY'
						))
				->where("B.CUST_ID = ?", (string)$cust_id)
				->where("B.CUST_STATUS != '3'")
				->order('B.CUST_ID ASC');
						
		$arr = $this->_db->fetchAll($select);

        if (!empty($arr)) {
        	$cust_name = $arr[0]['CUST_NAME'];
        	$this->view->cust_name = $cust_name;
        	$this->view->balance_inquiry = $arr[0]['BALANCE_INQUIRY'];
        	$this->view->account_statement = $arr[0]['ACCOUNT_STATEMENT'];
        	$this->view->inhouse_transfer = $arr[0]['INHOUSE_TRANSFER'];
        	$this->view->skn_transfer = $arr[0]['SKN_TRANSFER'];
        	$this->view->rtgs_transfer = $arr[0]['RTGS_TRANSFER'];
        	$this->view->online_transfer = $arr[0]['ONLINE_TRANSFER'];
        	// $this->view->inhouse_benef_inquiry = $arr[0]['INHOUSE_BENEF_INQUIRY'];
        	$this->view->inter_benef_inquiry = $arr[0]['INTERBANK_BENEF_INQUIRY'];
        	$this->view->kurs_rate = $arr[0]['KURS_RATE_INQUIRY'];

        	if($arr[0]['BALANCE_INQUIRY'] == '')
	        {
	        	$cust_name = $arr[0]['CUST_NAME'];
	        	$this->view->cust_name = $cust_name;
	        	$this->view->balance_inquiry = $arrglobal[0]['BALANCE_INQUIRY'];
	        	$this->view->account_statement = $arrglobal[0]['ACCOUNT_STATEMENT'];
	        	$this->view->inhouse_transfer = $arrglobal[0]['INHOUSE_TRANSFER'];
	        	$this->view->skn_transfer = $arrglobal[0]['SKN_TRANSFER'];
	        	$this->view->rtgs_transfer = $arrglobal[0]['RTGS_TRANSFER'];
	        	$this->view->online_transfer = $arrglobal[0]['ONLINE_TRANSFER'];
	        	// $this->view->inhouse_benef_inquiry = $arrglobal[0]['INHOUSE_BENEF_INQUIRY'];
	        	$this->view->inter_benef_inquiry = $arrglobal[0]['INTERBANK_BENEF_INQUIRY'];
	        	$this->view->kurs_rate = $arrglobal[0]['KURS_RATE_INQUIRY'];
	        }
        }



        //---------------------------------------------------------------------------------------

		if($this->_request->isPost() )
		{

			$sessionNameConfrim = new Zend_Session_Namespace('apicharges');

			$cust_id = $sessionNameConfrim->cust_id;

			$filters = array(
			                'balance_inquiry' => array('StringTrim','StripTags'),
			                'account_statement' => array('StringTrim','StripTags'),
							'inhouse_transfer' => array('StringTrim','StripTags'),
            			    'skn_transfer' => array('StringTrim','StripTags'),
            			    'rtgs_transfer' => array('StringTrim','StripTags'),
            			    'online_transfer' => array('StringTrim','StripTags'),
            			    // 'inhouse_benef_inquiry' => array('StringTrim','StripTags'),
            			    'inter_benef_inquiry' => array('StringTrim','StripTags'),
            			    'kurs_rate' => array('StringTrim','StripTags'),
							);

			$validators = array('balance_inquiry' => array('NotEmpty',
													 'messages' => array($this->language->_('Can not be empty'))
													),
			                    'account_statement' => array('NotEmpty',
													 'messages' => array($this->language->_('Can not be empty'))
													),
								'inhouse_transfer'  => array('NotEmpty',
													 'messages' => array($this->language->_('Can not be empty'))
													),
                			    'skn_transfer'      => array('NotEmpty',
                                    			     'messages' => array($this->language->_('Can not be empty'))
                                    			    ),
                			    'rtgs_transfer'      => array('NotEmpty',
                                    			     'messages' => array($this->language->_('Can not be empty'))
                                    			    ),
                			    'online_transfer'      => array('NotEmpty',
                                    			     'messages' => array($this->language->_('Can not be empty'))
                                    			    ),
                			    // 'inhouse_benef_inquiry'      => array('NotEmpty',
                       //              			     'messages' => array($this->language->_('Can not be empty'))
                       //              			    ),
                			    'inter_benef_inquiry'      => array('NotEmpty',
                                    			     'messages' => array($this->language->_('Can not be empty'))
                                    			    ),
                			    'kurs_rate'      => array('NotEmpty',
                                    			     'messages' => array($this->language->_('Can not be empty'))
                                    			    ),
							   );
		
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{

				$change_id = $this->suggestionWaitingApproval('Charges API','Edit API Charges',$this->_changeType['code']['new'],null,'M_CHARGES_API','TEMP_CHARGES_API',$cust_id,$cust_name,$cust_id,$cust_name);

				$content = array(
					'CUST_ID' 	 		 => $cust_id,
					'BALANCE_INQUIRY' 	 => $zf_filter_input->balance_inquiry,
					'ACCOUNT_STATEMENT'  => $zf_filter_input->account_statement,
					'INHOUSE_TRANSFER' 	 => $zf_filter_input->inhouse_transfer,
				    'SKN_TRANSFER' 	 	 => $zf_filter_input->skn_transfer,
				    'RTGS_TRANSFER' 	 => $zf_filter_input->rtgs_transfer,
				    'ONLINE_TRANSFER' 	 => $zf_filter_input->online_transfer,
				    // 'INHOUSE_BENEF_INQUIRY' 	 => $zf_filter_input->inhouse_benef_inquiry,
				    'INTERBANK_BENEF_INQUIRY' 	 => $zf_filter_input->inter_benef_inquiry,
				    'KURS_RATE_INQUIRY'  => $zf_filter_input->kurs_rate,
				    'CHARGES_SUGGESTED'  => date('Y-m-d H:i:s'),
				    'CHARGES_SUGGESTEDBY'  => $this->_userIdLogin,
				    'CHANGES_ID'  => $change_id
		        );
						       
				try 
				{
					//-------- insert temporary table--------------
					$this->_db->beginTransaction();
					
					$this->_db->insert('TEMP_CHARGES_API', $content);

					$this->_db->commit();
					
					$this->view->success = true;
					$this->view->report_msg = array();
					
					Zend_Session::namespaceUnset('apicharges');
					$this->_redirect('/notification/submited/index');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);


				if($zf_filter_input->headquarter=='YES'){
						$this->view->yhead      = 'selected';	
					}else if($zf_filter_input->headquarter=='NO'){
						$this->view->nhead      = 'selected';	
					}else{
						$this->view->nn      = 'selected';	
					}
				$error = $zf_filter_input->getMessages();
				
				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
		
		// Application_Helper_General::writeLog('COAD','Add New Branch Bank');
	}

}