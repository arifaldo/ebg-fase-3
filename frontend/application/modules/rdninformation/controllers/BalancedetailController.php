<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
class rdninformation_BalancedetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		
		$model = new rdninformation_Model_Rdninformation();
		// die('here');
		$fields1 = array(
					'acct'  => array('field' => 'ACCT_NO',
											 'label' => $this->language->_('Account No'),
											 'sortable' => false),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => false),
					'ccy'   => array('field'    => 'CCY_ID',
											'label'    => $this->language->_('CCY'),
											'sortable' => false),
					'available'   => array('field'    => 'AVAILABLE_BALANCE',
											'label'    => $this->language->_('Available Balance'),
											'sortable' => false),
		);

		$fields2 = array(
					'depo_acct'  => array('field' => 'DEPOSIT_NO',
											 'label' => $this->language->_('Deposit No'),
											 'sortable' => false),
					'protype'   => array('field'    => 'PROD_TYPE',
											'label'    => $this->language->_('Product Type'),
											'sortable' => false),
					'depo_ccy'   => array('field'    => 'CCY_ID',
											'label'    => $this->language->_('CCY'),
											'sortable' => false),
					'amount'   => array('field'    => 'AVAIL_BAL',
											'label'    => $this->language->_('Available Balance'),
											'sortable' => false),
		);

		$fields3 = array(
					'cardno'  => array('field' => '',
											 'label' => $this->language->_('Card No'),
											 'sortable' => false),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => false),
					'creditlimit'   => array('field'    => '',
											'label'    => $this->language->_('Credit Limit'),
											'sortable' => false),
					'closingbal'   => array('field'    => '',
											'label'    => $this->language->_('Closing Balance'),
											'sortable' => false),
		);



		$fields4 = array(
					'loan_acct'  => array('field' => 'ACCT_NO',
											 'label' => $this->language->_('Account No'),
											 'sortable' => false),
					'protype'   => array('field'    => 'PROD_TYPE',
											'label'    => $this->language->_('Product Type'),
											'sortable' => false),
					'plafond'   => array('field'    => 'PLAFOND',
											'label'    => $this->language->_('Plafond'),
											'sortable' => false),
					'outstanding'   => array('field'    => 'OUTSTANDING',
											'label'    => $this->language->_('Outstanding'),
											'sortable' => false),
		);


		$fields5 = array(
								'ccy'  => array('field' => 'ACCT_NO',
																				'label' => $this->language->_('CCY'),
																				'sortable' => true),
								'personalaccount'   => array('field'    => 'PROD_TYPE',
																				'label'    => $this->language->_('Personal Account'),
																				'sortable' => true),
								'depositaccount'   => array('field'    => 'PLAFOND',
																				'label'    => $this->language->_('Deposit Account'),
																				'sortable' => true),
								'loanaccount'   => array('field'    => 'OUTSTANDING',
																				'label'    => $this->language->_('Loan Account'),
																				'sortable' => true),
								'networth'   => array('field'    => 'OUTSTANDING',
																				'label'    => $this->language->_('Net Worth'),
																				'sortable' => true),
							//  'equivalen'   => array('field'    => 'OUTSTANDING',
													//  		        'label'    => $this->language->_('Ekuivalent (IDR)'),
													//  		        'sortable' => true),
		);

		$sortByParam  = $this->_getParam('sortby');
		$sortDirParam = $this->_getParam('sortdir');


		//check param for personal table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields1)))){
			$sortBy1  =  $fields1[$sortByParam]['field'];
			$sortDir1 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy1  = $fields1[key($fields1)]['field'];
			$sortDir1 = 'asc';
		}

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();	// show acc in IDR only
		// print_r($AccArr);die;
		//die;
		if(!empty($AccArr)){
		$srcData = $this->_db->select()
		->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE'))
		->where('ACCT_NO = ?', $AccArr['0']['ACCT_NO'])
		->limit(1)
		;
		$acsrcData = $this->_db->fetchRow($srcData);
		
		if(!empty($acsrcData)){
				$accsrc = $acsrcData['ACCT_NO'];
				$accsrctype = $acsrcData['ACCT_TYPE'];
		}
		//print_r($AccArr);die('here');

		$svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
		$resultKursEx = $svcInquiry->rateInquiry();
		// }else{
		// 	$resultKursEx = array();
		// }
		//rate inquiry for display
		$kurssell = '';
		$kurs = '';
		$book = '';
		if($resultKursEx['ResponseCode']=='00'){
				$kursList = $resultKursEx['DataList'];
				$kurssell = '';
				$kurs = '';
// 		    print_r($resultKursEx['DataList']);die;
				$this->view->rateKurs 		= $resultKursEx['DataList'];

		}else{

				$this->view->rateKurs 		= array();

		}


		/*// Account Inquiry
		$sorting1 = $sortBy1.' '.$sortDir1;
		$temp = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->group('A.ACCT_NO')
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);

		//$temp = $model->getUserAccount(array('userId'=>$this->_userIdLogin),$sorting1);
		//print_r($temp);die;
		if (count($temp) > 0)
		{
			$datapers = array();

			foreach ( $temp as $key=>$row ) {

				$Account = new Account($row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']),null,$row['ACCT_TYPE'],$row['ACCT_CIF'],$this->_userIdLogin);
				$Account->setFlag(false);

				$Account->checkBalance();

				if($Account->getCoreProductPlan() != NULL){
					// 					print_r('here');

					$product = $model->getProduct($Account->getCoreProductPlan(),$row ['ACCT_TYPE']);
					$product_name = $product['0']['PRODUCT_NAME'];
				}else{
					// 					print_r('here1');
					$product_name = 'N/A';
				}
				// 				print_r($product);die;
				$datapers[$key]['ACCT_PRODUCT_NAME'] = $product_name;
				// 				$product = $model->getProduct($Account->getCoreProductPlan(),$row ['PRODUCT_CODE']);
				// 				$datapers[$key]['ACCT_PRODUCT_NAME'] = $product['0']['PRODUCT_NAME'];
				$datapers[$key]['ACCT_NO'] = $row['ACCT_NO'];
				$datapers[$key]['CCY_ID'] = $row['CCY_ID'];
				$datapers[$key]['PRODUCT_NAME'] = $row['PRODUCT_NAME'];
				$datapers[$key]['PRODUCT_CODE'] = $row['PRODUCT_CODE'];
				$datapers[$key]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
			}
		}*/
		// End Account Inquiry

		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

		$model = new rdninformation_Model_Rdninformation();
		$userCIF = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_USER'))
				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
				->limit(1)
		);
		//die('here');
		$param['userId'] = $userId;
		$param['custId'] = $custId;
		$userAcctInfo = $model->getCustAccount($param);
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];

		//cifaccouninquiry
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$core = array();
		$svcAccount = new Service_Account($userCIF['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
		// $result		= $svcAccount->cifaccountInquiry($core);
		// print_r($userCIF);
		$result		= $svcAccount->cifaccountInquiry($core);
		// var_dump($result);die;
		if($result['ResponseDesc'] != 'Success'){
			if($result['ResponseDesc'] != 'No Data'){
			$this->view->error_per = $result['ResponseDesc'];
			}
		}
//		Zend_Debug::dump($result);
//		Zend_Debug::dump($core->accountList);
//		echo count($core->accountList). " acct list"; die;

		if(count($core->accountList) > 0){

				$arrResult = array();
					$err = array();

					if(count($core->accountList) > 1){
// 			   		print_r($core->accountList);
						//array data lebih dari 1
						foreach ($core->accountList as $key => $val){
							if($val->status == '0'){
								$arrResult = array_merge($arrResult, array($val->accountNo => $val));
								array_push($err, $val->accountNo);
							}
// 					   	echo 'here';
						}
					}
					else{
						//array kurang dari 2
						foreach ($core as $data){
							if($data->status == '0'){
								$arrResult = array_merge($arrResult, array($data->accountNo => $data));
								array_push($err, $data->accountNo);
							}
						}
					}

				$final = array();
				$i = 0;
//
				foreach($arrResult as $key => $val){
					$final[$i++] = $val;
				}


				if(count($final) < 1){
					$this->view->data1 = '0';
				}
				else{
					$this->view->data = $final;
				}


//				Zend_Debug::dump($final, "final"); die;
					foreach ($final as $dataProductPlan){
//				   		if($dataProductPlan->status == '0'){

								if (isset($dataProductPlan->productPlan) && isset($dataProductPlan->planCode))
								{
								$select_product_type	= $this->_db->select()
											->from(array('M_PRODUCT_TYPE'),'PRODUCT_NAME')
				//							->where('PRODUCT_CODE = ?','10');
				//							->where("PRODUCT_CODE IN(?)", $dataPro)
											->where("PRODUCT_CODE = ?", $dataProductPlan->productPlan)
											->where("PRODUCT_PLAN = ?", $dataProductPlan->planCode)
											->where("ACCT_TYPE = 4");
				//							->where('PRODUCT_PLAN = ?','62');
								$userData_product_type = $this->_db->fetchCol($select_product_type);

								$dataProductPlan->planName = $userData_product_type[0];
								$dataProductPlanOri = (($dataProductPlan->planName == null)?'':$dataProductPlan);
								}else{
									$dataProductPlanOri = $dataProductPlan;
								}

							$dataUser[] = $dataProductPlanOri;

//				   		}
					}
					// echo "<pre>";
					// var_dump($dataUser);die;
//				echo count($dataUser); die;
//				Zend_Debug::dump($dataUser,"datauser"); die;
				if (count($dataUser) > 0)
				{
					$datapers = array();
					$i = 0;
					foreach ( $dataUser as $key=>$row ) {

						if($row->productType == '40'){

						if (!empty($row->accountNo)){
							$Account = new Account($row->accountNo,$row->ccy);
							$Account->setFlag(false);
							$Account->checkBalance();
	
							// var_dump($Account);die;
	
							if($row->planCode != NULL && $row->productType != NULL){
								$product = $model->getProduct($row->planCode,$row->productPlan);
								$temp = $model->getCustAccount(array('userId'=>$this->_userIdLogin));
	
								if (empty($product)){
									// echo "test";
								}else{
									$product_name = $product['0']['PRODUCT_NAME'];
									$acct_type = $product['0']['ACCT_TYPE'];
									$acct_name = $temp['0']['ACCT_NAME'];
									if($result['ResponseCode'] == '00'){
										$status = "Active";
									}
								}
							}else{
								$product_name = 'N/A';
							}
	
							$datapers[$i]['ACCT_TYPE'] = $acct_type;
							$datapers[$i]['ACCT_PRODUCT_NAME'] = 'RDN GIRO';
							$datapers[$i]['ACCT_NO'] = $row->accountNo;
							$datapers[$i]['ACCT_NAME'] = $this->_custNameLogin;
							$datapers[$i]['STATUS'] = 'Active';
							$datapers[$i]['CCY_ID'] = Application_Helper_General::getCurrCode($row->ccy);
							$datapers[$i]['PRODUCT_NAME'] = $row->PRODUCT_NAME;
							$datapers[$i]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
							$datapers[1]['ACCT_TYPE'] = $acct_type;
							$datapers[1]['ACCT_PRODUCT_NAME'] = 'RDN GIRO'; 
							$datapers[1]['ACCT_NO'] = '2001029000125';
							$datapers[1]['ACCT_NAME'] = $this->_custNameLogin;
							$datapers[1]['STATUS'] = 'Active';
							$datapers[1]['CCY_ID'] = Application_Helper_General::getCurrCode($row->ccy);
							$datapers[1]['PRODUCT_NAME'] = $row->PRODUCT_NAME;
							$datapers[1]['AVAILABLE_BALANCE'] = 160000000;
							$datapers[2]['ACCT_TYPE'] = $acct_type;
							$datapers[2]['ACCT_PRODUCT_NAME'] = 'RDN GIRO';
							$datapers[2]['ACCT_NO'] = '2001029000161';
							$datapers[2]['ACCT_NAME'] = $this->_custNameLogin;
							$datapers[2]['STATUS'] = 'Active';
							$datapers[2]['CCY_ID'] = Application_Helper_General::getCurrCode($row->ccy);
							$datapers[2]['PRODUCT_NAME'] = $row->PRODUCT_NAME;
							$datapers[2]['AVAILABLE_BALANCE'] = 50000000;
							$i++;
						}

						}

					}
				}

		}
		// var_dump($dataUser); die;


							

		$data = $datapers;
				// var_dump($data);die;
				$isGroup = false;
				if(count($data))
				{
					foreach($data as $row)
					{
						if($row['GROUP_ID'])
						{
							$isGroup = true;
						}
					}
				}

			$pdf = $this->_getParam('pdf');
			$csv = $this->_getParam('csv');
			$this->view->isGroup = $isGroup;
			$this->view->custId = $this->_custIdLogin;
			$this->view->resultdata = $data;
			$this->view->fields = $fields;



				if($pdf){
					//Zend_Debug::dump($this->view->render($this->view->controllername.'/pdf.phtml'));die;
					Application_Helper_General::writeLog('BAIQ','Print PDF');
					$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf.phtml')."</td></tr>";
					//Zend_Debug::dump($outputHTML);die;
					$this->_helper->download->pdf(null,null,null,'RDN Balance Detail',$outputHTML);
				}
				if($csv)
					{
						$arr = array();

						$header = array(
							$this->language->_('Account No'),
							$this->language->_('Account Name'),
							$this->language->_('CCY'),
							$this->language->_('Type'),
							'Status',
							$this->language->_('Available Balance')
						);

						if(!$isGroup)
						{
							$arr[0] = $header;
							$i = 1;
							$arr_value_tot_group =  array();
							$arr_display_tot_group =  array();
							foreach($data as $row)
							{

								$systemBalance = new SystemBalance($row['CUST_ID'],$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
								$systemBalance->setFlag(false);
								$systemBalance->checkBalance();


								if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
						          $arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
						        }
						        else {
						          $arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
						        }

								$accountStatusModified = $systemBalance->getCoreAccountStatusDesc();
								if ($row['ACCT_TYPE'] == 4) {
									$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);

									$arr[$i][] = $row['ACCT_NO'];
									$arr[$i][] = $systemBalance->getCoreAccountName();
									$arr[$i][] = $row['CCY_ID'];
									$arr[$i][] = $row ['ACCT_PRODUCT_NAME'];
									$arr[$i][] = $accountStatusModified;
									$arr[$i][] = $systemBalance->getEffectiveBalance();

									$i++;
								}

							}
							$display_tot_group = implode("; ",$arr_display_tot_group);
							$arr[$i][] = $this->language->_('TOTAL EFFECTIVE BALANCE').' : '.$display_tot_group;
						}
						else
						{
							$i = 0;
							$group = null;

							$resultdataGroup =  array();
							foreach ($data as $rowGroup){
								$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup;
							}
							$i = 0;
							$group = null;
							$arr_value_tot_group = array();
							foreach ( $resultdataGroup as $groupname=>$rowPerGroup )
							{
								$i++;
								$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
								$groupname = ($groupname) ? $groupname : 'OTHERS';
								$arr[$i][] = $groupname;
								$i++;
								$arr[$i][] = $header;

									if(count($rowPerGroup) > 0){
										$arr_value_tot_group =  array();
										$arr_display_tot_group =  array();
										foreach ($rowPerGroup as $row){
										$i++;
										$systemBalance = new SystemBalance($this->_custIdLogin,$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
										$systemBalance->setFlag(false);
										$systemBalance->checkBalance();
										$model = new accountstatement_Model_Accountstatement();
											if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
												$arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
											}else{
												$arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
											}
											$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);
										$accountStatusModified = $systemBalance->getCoreAccountStatusDesc();
			 							//print_r($row);die;
										if($systemBalance->getCorePlanCode() != NULL || $systemBalance->getCorePlanCode() != 'N/A'){
											 					//print_r('here');die;
											//$product = $model->getProduct($systemBalance->getCorePlanCode(),$row ['PRODUCT_CODE']);
											$product_name = $row['ACCT_DESC'];
										}else{
											// 					print_r('here1');
											$product_name = 'N/A';
										}

										$arr[$i][] = $row['ACCT_NO'];
										$arr[$i][] = $row['ACCT_ALIAS_NAME'];
										$arr[$i][] = $systemBalance->getCoreAccountName();

										$arr[$i][] = $row['CCY_ID'];
										$arr[$i][] = $product_name;
			// 							$arr[$i][] = $row ['ACCT_DESC'];
										$arr[$i][] = $accountStatusModified;
										$arr[$i][] = $systemBalance->getEffectiveBalance();

										}
									}
									$i++;
									$display_tot_group = implode("; ",$arr_display_tot_group);
									$arr[$i][] = $this->language->_('TOTAL EFFECTIVE BALANCE').' : '.$display_tot_group;



							}

						}
							$logDesc = 'Export to CSV';
							$this->_helper->download->csv(array(),$arr,null,'RDN Balance Detail');

					}
		$logDesc = 'Viewing';


		Application_Helper_General::writeLog('BAIQ',$logDesc);
		// echo '<pre>';
		// print_r ($datapers); die;
		$this->view->userId = $this->_userIdLogin;
		$this->view->custId = $this->_custIdLogin;
		$this->view->personal = $datapers;
		$this->view->creditcard = $datacc;
		$this->view->deposit = $datadeposit;
		$this->view->loan = $dataloan;
		$this->view->fields1 = $fields1; //personal
		$this->view->fields2 = $fields2; //deposit
		$this->view->fields3 = $fields3; //credit card
		$this->view->fields4 = $fields4; //loan
		$this->view->fields5 = $fields5; //loan

		$this->view->sortBy1 = $sortBy1;
		$this->view->sortBy2 = $sortBy2;
		$this->view->sortBy3 = $sortBy3;
		$this->view->sortBy4 = $sortBy4;
		$this->view->sortBy5 = $sortBy5;

		$this->view->sortDir1 = $sortDir1;
		$this->view->sortDir2 = $sortDir2;
		$this->view->sortDir3 = $sortDir3;
		$this->view->sortDir4 = $sortDir4;
		$this->view->sortDir5 = $sortDir5;
	}


}
}
