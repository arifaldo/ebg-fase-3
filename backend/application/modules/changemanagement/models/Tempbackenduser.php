<?php

/**
 * Tempuser model
 * 
 * @author 
 * @version
 */
require_once('Crypt/AESMYSQL.php');
require_once 'General/BankUser.php';

class Changemanagement_Model_Tempbackenduser extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = 'CUS';
	/**
	 * Approve Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	// const actual_link = $_SERVER['SERVER_NAME'];
	public function approveNew($actor = null)
	{

		//user password = autogenerate
		//perlu insert ke T_REQ_PIN_MAILER
		//query from TEMP_BUSER
		$user = $this->dbObj->select()
			->from('TEMP_BUSER')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($user)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$globaldata = $this->dbObj->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($globaldata)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$encrypt = new Crypt_AESMYSQL();
		$password = new BankUser($user['BUSER_ID']);
		$generate = $password->generateNewPassword();


		//--------------------------------INSERT TO TABLE M_BUSER----------------------------------------------------------
		$datepass = date('Y-m-d h:i:s', strtotime("+1 days"));
		$str = rand();
		$rand = md5($str);
		$insertArr = array_diff_key($user, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
		$insertArr['CREATED']     = new Zend_Db_Expr('now()');
		$insertArr['CREATEDBY']   = $actor;
		$insertArr['SUGGESTED']     = $globaldata['CREATED'];
		$insertArr['SUGGESTEDBY']   = $globaldata['CREATED_BY'];
		$insertArr['UPDATED']     = new Zend_Db_Expr('now()');
		$insertArr['UPDATEDBY']   = $actor;
		$insertArr['BUSER_ISLOCKED'] = 0;
		$insertArr['BUSER_ISNEW'] = 1;
		$insertArr['BUSER_RCHANGE'] = 1;
		$insertArr['BUSER_RRESET'] = 0;
		$insertArr['BUSER_RPASSWORD_ISEMAILED'] = 0;
		$insertArr['BUSER_RPASSWORD_ISPRINTED'] = 0;
		$insertArr['BUSER_ISEMAILPWD'] = 0;
		//$insertArr['BUSER_LASTACTIVITY'] = new Zend_Db_Expr('now()');
		$insertArr['BUSER_ISREQUIRE_CHANGEPWD'] = 0;
		$insertArr['BUSER_STATUS'] = 2;
		$insertArr['BUSER_CLEARTEXT_PASSWORD'] = $generate['CLEAR_TEXT_PASSWORD'];
		$insertArr['BUSER_PASSWORD'] = $generate['ENCRYPTED_PASSWORD'];
		$insertArr['BUSER_DATEPASS'] = $datepass;
		$insertArr['BUSER_CODE'] 	 = $rand;

		//Zend_Debug::dump($insertArr);die;
		// echo "<pre>";
		// var_dump($insertArr);die;

		try {
			$userins = $this->dbObj->insert('M_BUSER', $insertArr);
		} catch (\Throwable $th) {
		}
		$jsonusernew = json_encode($userins);
		// var_dump($userins);die;
		if (!(bool)$userins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		//-------------------------------END INSERT TO TABLE M_BUSER-------------------------------------------------------


		$subject = "Backend New User Information";
		$setting = new Settings();
		$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name');
		$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
		$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email');
		$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp');
		$templateEmailMasterBankWa	 	= $setting->getSetting('master_bank_wapp');
		$template 						= $setting->getSetting('bemailtemplate_newuser');
		$url_bo							= $setting->getSetting('url_bo');
		// 		$subject = "CMD - New User";

		$user_id = ($this->sslEnc($user['BUSER_ID']));

		$actual_link = $_SERVER['SERVER_NAME'];

		//$newPassword = $url_bo.'/pass/index?safetycheck=&code='.urlencode($rand).'&user_id='.urlencode($user_id);
		$newPassword = $url_bo . '/pass/index/activation?safetycheck=&code=' . urlencode($rand) . '&user_id=' . urlencode($user_id);

		$template = str_ireplace('[[buser_fullname]]', $user['BUSER_FULLNAME'], $template);
		$template = str_ireplace('[[buser_login]]', $user['BUSER_ID'], $template);
		$template = str_ireplace('[[buser_name]]', $user['BUSER_NAME'], $template);

		$template = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankEmail, $template);
		$template = str_ireplace('[[master_bank_telp]]', $templateEmailMasterBankTelp, $template);
		$template = str_ireplace('[[master_bank_wapp]]', $templateEmailMasterBankWa, $template);
		$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
		$date = date('Y-m-d h:i:s', strtotime("+1 days"));
		$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
		$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
		$template = str_ireplace('[[exp_date]]', $datenow, $template);
		$template = str_ireplace('[[confirm_link]]', $newPassword, $template);

		$email = $user['BUSER_EMAIL'];
		// print_r($template);die;
		// echo $template;die;
		if ($email != '') {
			$result = Application_Helper_Email::sendEmail($email, $subject, $template);
		}

		//$jsonusernew = json_encode($backendUsernew);

		/*$fullDesc = "Approve Changes Id : " . $this->_changeId . " Old Data : -  Suggest Data : " . $jsonusernew . "   Data Name : " . $insertArr['BUSER_ID'];
		Application_Helper_General::writeLog('ADCA', $fullDesc);
		*/
		$deleteChanges = $this->deleteNew();
		if (!$deleteChanges)
			return false;

		return true;
	}

	/**
	 * Approve Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveEdit($actor = null)
	{
		//query from TEMP_BUSER
		$user = $this->dbObj->select()
			->from('TEMP_BUSER')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);


		if (!count($user)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		$backendUserold = $user;


		//Validation, user can only be activated if this user's customer is active

		//query from M_BUSER
		$m_BUSER = $this->dbObj->select()
			->from('M_BUSER')
			->where('BUSER_ID = ?', $user['BUSER_ID'])
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		//Zend_Debug::dump($m_BUSER);die;
		if (!count($m_BUSER)) {

			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}


		$globaldata = $this->dbObj->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($globaldata)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}


		//update record
		$updateArr = array_diff_key($user, array(
			'TEMP_ID' => '', 'CHANGES_ID' => '', 'BUSER_ID' => '', 'BUSER_STATUS' => '', 'BUSER_PASSWORD' => '',
			'BUSER_ISLOGIN' => '', 'BUSER_ISLOCKED' => '', 'BUSER_FAILEDATTEMPT' => '', 'CREATED' => '', 'CREATEDBY' => ''
		));
		$updateArr['UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY']   = $actor;

		$whereArr = array('BUSER_ID = ?' => $user['BUSER_ID']);
		$updateArr['SUGGESTED']     = $globaldata['CREATED'];
		$updateArr['SUGGESTEDBY']   = $globaldata['CREATED_BY'];

		if ($m_BUSER["BUSER_ISNEW"] == 1) {
			$str = rand();
			$rand = md5($str);
			$updateArr['BUSER_CODE'] 	 = $rand;
		}

		$userupdate = $this->dbObj->update('M_BUSER', $updateArr, $whereArr);
		$backendUsernew = $this->dbObj->select()
			->from('M_BUSER')
			->where('BUSER_ID = ?', $user['BUSER_ID'])
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!(bool)$userupdate) {

			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$jsonusernew = json_encode($backendUsernew);
		$jsonuserold = json_encode($backendUserold);

		$fullDesc = "Approve Changes Id : " . $this->_changeId . " Old Data : " . $jsonuserold . " Suggest Data : " . $jsonusernew . "   Data Name : " . $insertArr['BUSER_ID'];
		//echo $fullDesc;die;
		Application_Helper_General::writeLog('ADCA', $fullDesc);

		// send email kalau masih baru --------------------------------------------------------

		if ($m_BUSER["BUSER_ISNEW"] == 1) {
			$user_id = ($this->sslEnc($user['BUSER_ID']));

			$subject = "Backend New User Information";
			$setting = new Settings();
			$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name');
			$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
			$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email');
			$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp');
			$templateEmailMasterBankWa	 	= $setting->getSetting('master_bank_wapp');
			$template 						= $setting->getSetting('bemailtemplate_newuser');
			$url_bo							= $setting->getSetting('url_bo');

			$newPassword = $url_bo . '/pass/index/activation?safetycheck=&code=' . urlencode($rand) . '&user_id=' . urlencode($user_id);

			$template = str_ireplace('[[buser_fullname]]', $user['BUSER_FULLNAME'], $template);
			$template = str_ireplace('[[buser_login]]', $user['BUSER_ID'], $template);
			$template = str_ireplace('[[buser_name]]', $user['BUSER_NAME'], $template);

			$template = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankEmail, $template);
			$template = str_ireplace('[[master_bank_telp]]', $templateEmailMasterBankTelp, $template);
			$template = str_ireplace('[[master_bank_wapp]]', $templateEmailMasterBankWa, $template);
			$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
			$date = date('Y-m-d h:i:s', strtotime("+1 days"));
			$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
			$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
			$template = str_ireplace('[[exp_date]]', $datenow, $template);
			$template = str_ireplace('[[confirm_link]]', $newPassword, $template);

			$email = $user['BUSER_EMAIL'];
			// print_r($template);die;
			// echo $template;die;
			Application_Helper_Email::sendEmail($email, $subject, $template);
		}

		// ------------------------------------------------------------------------------------------------------

		$deleteChanges  = $this->deleteEdit();
		if (!$deleteChanges) return false;

		return true;
	}

	/**
	 * Approve Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveActivate($actor = null)
	{
	}

	/**
	 * Approve Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveDeactivate($actor = null)
	{
	}

	public function approveSuspend($actor = NULL)
	{
		$backendUser = $this->dbObj->select()
			->from('TEMP_BUSER')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);

		if (!count($backendUser)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend User)';
			return false;
		}
		$backendUserold = $this->dbObj->select()
			->from('M_BUSER')
			->where('BUSER_ID = ?', $backendUser['BUSER_ID'])
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);

		$globaldata = $this->dbObj->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($globaldata)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$whereArr = array('BUSER_ID = ?' => $backendUser['BUSER_ID']);
		//update to master table M_BACKEND GROUP
		$backendUser['SUGGESTED']     = $globaldata['CREATED'];
		$backendUser['SUGGESTEDBY']   = $globaldata['CREATED_BY'];
		$updateArr = array_diff_key($backendUser, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BUSER', $updateArr, $whereArr);
		$backendUsernew = $this->dbObj->select()
			->from('M_BUSER')
			->where('BUSER_ID = ?', $backendUser['BUSER_ID'])
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);

		$subject = "Backend User Status";
		$setting = new Settings();
		$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name');
		$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
		$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email');
		$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp');
		$templateEmailMasterBankWa	 	= $setting->getSetting('master_bank_wapp');
		$template 						= $setting->getSetting('bemailtemplate_buserstatus');
		// 		$subject = "CMD - New User";

		$user_id = ($this->sslEnc($user['BUSER_ID']));

		// $actual_link = $_SERVER['SERVER_NAME'];

		// $newPassword = $actual_link.':2018/pass/index?safetycheck=&code='.urlencode($rand).'&user_id='.urlencode($user_id);

		$template = str_ireplace('[[buser_fullname]]', $backendUser['BUSER_NAME'], $template);
		$template = str_ireplace('[[buser_id]]', $backendUser['BUSER_ID'], $template);
		$template = str_ireplace('[[buser_status]]', 'Suspend', $template);
		// $template = str_ireplace('[[buser_name]]',$user['BUSER_NAME'],$template);

		$template = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankEmail, $template);
		$template = str_ireplace('[[master_bank_telp]]', $templateEmailMasterBankTelp, $template);
		$template = str_ireplace('[[master_bank_wapp]]', $templateEmailMasterBankWa, $template);
		$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
		$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
		// $template = str_ireplace('[[exp_date]]',Application_Helper_General::convertDate($date,'d-MMM-yyyy HH:mm:ss','yyyy-MM-dd HH:mm:ss'),$template);
		// $template = str_ireplace('[[confirm_link]]',$newPassword,$template);

		$email = $backendUser['BUSER_EMAIL'];
		// print_r($template);die;
		// echo $template;die;
		if ($email != '') {
			$result = Application_Helper_Email::sendEmail($email, $subject, $template);
		}

		$jsonusernew = json_encode($backendUsernew);
		$jsonuserold = json_encode($backendUserold);

		$fullDesc = "Approve Changes Id : " . $this->_changeId . " Old Data : " . $jsonuserold . " Suggest Data : " . $jsonusernew . "   Data Name : " . $insertArr['BUSER_ID'];
		Application_Helper_General::writeLog('ADCA', $fullDesc);
		$deleteChanges  = $this->deleteSuspend();
		if (!$deleteChanges)
			return false;

		return true;
	}

	public function approveUnsuspend($actor = NULL)
	{
		$backendUser = $this->dbObj->select()
			->from('TEMP_BUSER')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);

		if (!count($backendUser)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend User)';
			return false;
		}

		$backendUserold = $this->dbObj->select()
			->from('M_BUSER')
			->where('BUSER_ID = ?', $backendUser['BUSER_ID'])
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);

		$globaldata = $this->dbObj->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($globaldata)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$whereArr = array('BUSER_ID = ?' => $backendUser['BUSER_ID']);
		//update to master table M_BACKEND GROUP
		$backendUser['SUGGESTED']     = $globaldata['CREATED'];
		$backendUser['SUGGESTEDBY']   = $globaldata['CREATED_BY'];
		$updateArr = array_diff_key($backendUser, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BUSER', $updateArr, $whereArr);
		$backendUsernew = $this->dbObj->select()
			->from('M_BUSER')
			->where('BUSER_ID = ?', $backendUser['BUSER_ID'])
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		$subject = "Backend User Status";
		$setting = new Settings();
		$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name');
		$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
		$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email');
		$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp');
		$templateEmailMasterBankWa	 	= $setting->getSetting('master_bank_wapp');
		$template 						= $setting->getSetting('bemailtemplate_buserstatus');
		// 		$subject = "CMD - New User";

		$user_id = ($this->sslEnc($user['BUSER_ID']));

		// $actual_link = $_SERVER['SERVER_NAME'];

		// $newPassword = $actual_link.':2018/pass/index?safetycheck=&code='.urlencode($rand).'&user_id='.urlencode($user_id);

		$template = str_ireplace('[[buser_fullname]]', $backendUser['BUSER_NAME'], $template);
		$template = str_ireplace('[[buser_id]]', $backendUser['BUSER_ID'], $template);
		$template = str_ireplace('[[buser_status]]', 'Unsuspend', $template);
		// $template = str_ireplace('[[buser_name]]',$user['BUSER_NAME'],$template);

		$template = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankEmail, $template);
		$template = str_ireplace('[[master_bank_telp]]', $templateEmailMasterBankTelp, $template);
		$template = str_ireplace('[[master_bank_wapp]]', $templateEmailMasterBankWa, $template);
		$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
		$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
		// $template = str_ireplace('[[exp_date]]',Application_Helper_General::convertDate($date,'d-MMM-yyyy HH:mm:ss','yyyy-MM-dd HH:mm:ss'),$template);
		// $template = str_ireplace('[[confirm_link]]',$newPassword,$template);

		$email = $backendUser['BUSER_EMAIL'];
		// print_r($template);die;
		// echo $template;die;
		if ($email != '') {
			$result = Application_Helper_Email::sendEmail($email, $subject, $template);
		}
		$jsonusernew = json_encode($backendUsernew);
		$jsonuserold = json_encode($backendUserold);

		$fullDesc = "Approve Changes Id : " . $this->_changeId . " Old Data : " . $jsonuserold . " Suggest Data : " . $jsonusernew . "   Data Name : " . $insertArr['BUSER_ID'];
		Application_Helper_General::writeLog('ADCA', $fullDesc);

		$deleteChanges  = $this->deleteUnsuspend();
		if (!$deleteChanges)
			return false;

		return true;
	}

	public function approveDelete($actor = null)
	{
		//query from TEMP_BUSER
		$backendUser = $this->dbObj->select()
			->from('TEMP_BUSER')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);

		if (!count($backendUser)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$globaldata = $this->dbObj->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($globaldata)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		//Validation, user can only be activated if this user's customer is active
		$whereArr = array('BUSER_ID = ?' => $backendUser['BUSER_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendUser, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
		$updateArr['BUSER_ISLOGIN'] = '0';
		$updateArr['BUSER_ISLOCKED'] = '0';
		$updateArr['BUSER_FAILEDATTEMPT'] = '0';
		$updateArr['BUSER_LOCKREASON'] = '';
		$updateArr['SUGGESTED']     = $globaldata['CREATED'];
		$updateArr['SUGGESTEDBY']   = $globaldata['CREATED_BY'];
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BUSER', $updateArr, $whereArr);

		if (!(bool)$updateBackendGroup) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$subject = "Backend User Status";
		$setting = new Settings();
		$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name');
		$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
		$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email');
		$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp');
		$templateEmailMasterBankWa	 	= $setting->getSetting('master_bank_wapp');
		$template 						= $setting->getSetting('bemailtemplate_buserstatus');
		// 		$subject = "CMD - New User";

		// $user_id = ( $this->sslEnc ( $user['BUSER_ID'] ) );

		// $actual_link = $_SERVER['SERVER_NAME'];

		// $newPassword = $actual_link.':2018/pass/index?safetycheck=&code='.urlencode($rand).'&user_id='.urlencode($user_id);

		$template = str_ireplace('[[buser_fullname]]', $backendUser['BUSER_NAME'], $template);
		$template = str_ireplace('[[buser_id]]', $backendUser['BUSER_ID'], $template);
		$template = str_ireplace('[[buser_status]]', 'Delete', $template);
		// $template = str_ireplace('[[buser_name]]',$user['BUSER_NAME'],$template);

		$template = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankEmail, $template);
		$template = str_ireplace('[[master_bank_telp]]', $templateEmailMasterBankTelp, $template);
		$template = str_ireplace('[[master_bank_wapp]]', $templateEmailMasterBankWa, $template);
		$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
		$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
		// $template = str_ireplace('[[exp_date]]',Application_Helper_General::convertDate($date,'d-MMM-yyyy HH:mm:ss','yyyy-MM-dd HH:mm:ss'),$template);
		// $template = str_ireplace('[[confirm_link]]',$newPassword,$template);

		$email = $backendUser['BUSER_EMAIL'];
		// print_r($template);die;
		// echo $template;die;
		if ($email != '') {
			$result = Application_Helper_Email::sendEmail($email, $subject, $template);
		}

		$deleteChanges  = $this->deleteDelete();
		if (!$deleteChanges)
			return false;

		return true;
	}

	public function sslPrm()
	{
		return array("6a1f325be4c0492063e83a8cb2cb9ae7", "IV (optional)", "aes-128-cbc");
	}

	public function sslEnc($msg)
	{
		list($pass, $iv, $method) = $this->sslPrm();
		return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}

	/**
	 * Delete Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteNew()
	{

		//delete from TEMP_BUSER
		$userdelete = $this->dbObj->delete('TEMP_BUSER', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteEdit()
	{

		//delete from TEMP_BUSER
		$userdelete = $this->dbObj->delete('TEMP_BUSER', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteActivate()
	{
	}

	/**
	 * Delete Changes for changes type: Deactivate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteDeactivate()
	{
	}

	public function deleteSuspend()
	{
		$userdelete = $this->dbObj->delete('TEMP_BUSER', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		return true;
	}

	public function deleteUnsuspend()
	{
		$userdelete = $this->dbObj->delete('TEMP_BUSER', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		return true;
	}

	public function deleteDelete()
	{
		$userdelete = $this->dbObj->delete('TEMP_BUSER', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		return true;
	}
}
