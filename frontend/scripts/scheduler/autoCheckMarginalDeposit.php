<?php
require_once(realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Account.php';
require_once 'General/Settings.php';
require_once 'Service/Account.php';

$setting = new Settings();
$starttime = $setting->getSetting('start_running_balance');
$endtime = $setting->getSetting('cut_off_running_balance');
$begin = new DateTime($starttime);
$end = new DateTime($endtime);
$now = new DateTime();

$db = Zend_Db_Table::getDefaultAdapter();

$x = 0;

// FREEZE *******************************************************************************
// ambil data COUNTING_DEADLINE_TOPUP
$dataMD = $db->select()
	->from(
		array('M_MARGINALDEPOSIT'),
		array('*')
	)
	->query()->fetchAll();

foreach ($dataMD as $row) {
	$counter[$row['CUST_ID']] = $row['COUNTING_DEADLINE_TOPUP'];
	$firstCheckMinus[$row['CUST_ID']] = $row['LAST_CHECK_MINUS'];
}

// ambil jumlah marginal deposit
$cek_min_md = $db->select()
	->from("M_CUST_LINEFACILITY")
	->where('CUST_SEGMENT = ?', 4)
	->query()->fetchAll();

if ($cek_min_md) {
	foreach ($cek_min_md as $value) {
		$marginalDeposit[$value['CUST_ID']] = $value['MARGINAL_DEPOSIT'];

		$sqlCekRekeningJaminan = $db->select()
			->from(
				array('M_MARGINALDEPOSIT_DETAIL'),
				array('*')
			)->where("CUST_ID = ?", $value['CUST_ID']);
		$dataRekeningJaminan = $db->fetchAll($sqlCekRekeningJaminan);

		if (empty($dataRekeningJaminan)) {
			$dataUpdate  = [
				'LAST_CHECK' => new Zend_Db_Expr('now()'),
				'COUNTING_DEADLINE_TOPUP' => ($firstCheckMinus[$value['CUST_ID']] ? ($counter[$value['CUST_ID']] + 1) : 0),
				'LAST_CHECK_MINUS' => $firstCheckMinus[$value['CUST_ID']] ?: new Zend_Db_Expr('now()')
			];
			$whereUpdate = ['CUST_ID = ?' => $value['CUST_ID']];

			$db->update('M_MARGINALDEPOSIT', $dataUpdate, $whereUpdate);
			$x = 1;
		} else {
			// JIKA MD TIDAK KOSONG MAKA HITUNG TOTAL MD YANG ADA

			$totalAmount = 0;
			foreach ($dataRekeningJaminan as $row) {
				// ----------------------------------------------------------------------------------------------------
				$svcAccount = new Service_Account($row['MD_ACCT'], $row['MD_ACCT_CCY']);
				$result = $svcAccount->inquiryAccountBalance('AB', FALSE);

				if ($result['response_code'] == '00' || $result['response_code'] == '0000') {

					if (strtolower($result['account_type']) == 'd') {
						$db->update('M_MARGINALDEPOSIT_DETAIL', [
							'GUARANTEE_AMOUNT' => round($result['available_balance'], 2, PHP_ROUND_HALF_UP)
						], [
							'MD_ACCT = ?' => $row['MD_ACCT'],
							'CUST_ID = ?' => $value['CUST_ID']
						]);

						$totalAmount += round($result['available_balance'], 2, PHP_ROUND_HALF_UP);
					} else {
						$selectAllLockSaving = $svcAccount->inqLockSaving();

						$filterLockSaving = array_shift(array_filter($selectAllLockSaving['lock'], function ($item) use ($row) {
							return $item['hold_by_branch'] == $row['HOLD_BY_BRANCH'] && $item['sequence_number'] == $row['HOLD_SEQUENCE'];
						}));

						if ($filterLockSaving) {
							$db->update('M_MARGINALDEPOSIT_DETAIL', [
								'GUARANTEE_AMOUNT' => round(floatval($filterLockSaving['amount'] / 100), 2, PHP_ROUND_HALF_UP)
							], [
								'MD_ACCT = ?' => $row['MD_ACCT'],
								'CUST_ID = ?' => $value['CUST_ID'],
								'HOLD_SEQUENCE = ?' => $filterLockSaving['sequence_number']
							]);

							$totalAmount += round(floatval($filterLockSaving['amount'] / 100), 2, PHP_ROUND_HALF_UP);
						}
					}

					$x = 1;
				} else {
					// inquiry core untuk cek balance deposito
					$svcAccount = new Service_Account($row['MD_ACCT'], $row['MD_ACCT_CCY']);
					$result = $svcAccount->inquiryDeposito('AB', TRUE);
					if ($result['response_code'] == '00' || $result['response_code'] == '0000') {

						$inqLockDeposito = $svcAccount->inquiryLockDeposito();

						if (
							$inqLockDeposito['response_code'] == '0000' &&
							$inqLockDeposito['hold_by_branch'] == $row['HOLD_BY_BRANCH'] &&
							$inqLockDeposito['sequence_number'] == $row['HOLD_SEQUENCE']
						) {
							$db->update('M_MARGINALDEPOSIT_DETAIL', [
								'GUARANTEE_AMOUNT' => round(floatval($inqLockDeposito['amount'] / 100), 2, PHP_ROUND_HALF_UP)
							], [
								'MD_ACCT = ?' => $row['MD_ACCT'],
								'CUST_ID = ?' => $value['CUST_ID']
							]);

							$totalAmount += round(floatval($inqLockDeposito['amount'] / 100), 2, PHP_ROUND_HALF_UP);
						}

						$x = 1;
					}
				}
				// -----------------------------------------------------------------------------------------------
			}

			if ($totalAmount < $value['MARGINAL_DEPOSIT']) {
				$dataUpdate  = [
					'LAST_CHECK' => new Zend_Db_Expr('now()'),
					'COUNTING_DEADLINE_TOPUP' => ($firstCheckMinus[$value['CUST_ID']] ? ($counter[$value['CUST_ID']] + 1) : 0),
					'LAST_CHECK_MINUS' => $firstCheckMinus[$value['CUST_ID']] ?: new Zend_Db_Expr('now()')
				];
				$whereUpdate = ['CUST_ID = ?' => $value['CUST_ID']];

				$db->update('M_MARGINALDEPOSIT', $dataUpdate, $whereUpdate);
				$x = 1;
			}
			// UNFREZZE *******************************************************************************

			else {
				$dataUpdate  = [
					'LAST_CHECK' => new Zend_Db_Expr('now()'),
					'COUNTING_DEADLINE_TOPUP' => null,
					'LAST_CHECK_MINUS' => null,
				];
				$whereUpdate = ['CUST_ID = ?' => $value['CUST_ID']];

				$db->update('M_MARGINALDEPOSIT', $dataUpdate, $whereUpdate);

				$status = ($value['FREEZE_MANUAL'] != 1 && $value['MONITOR_KONTRA'] != 1) ? 1 : $value['STATUS'];
				$dataLFUpdate  = [
					'STATUS'    => $status,
					'MONITOR_MD' => null,
				];
				$whereLFUpdate = ['CUST_ID = ?' => $row['CUST_ID']];
				$db->update('M_CUST_LINEFACILITY', $dataLFUpdate, $whereLFUpdate);

				$x = 1;
			}

			// END UNFREEZE ***************************************************************************

			$whereUpdate = ['CUST_ID = ?' => $value['CUST_ID']];
			$db->update('M_MARGINALDEPOSIT', [
				'INQUIRY_TIME' => new Zend_Db_Expr('NOW()'),
			], $whereUpdate);
		}
	}
}

// ambil data terakhir COUNTING_DEADLINE_TOPUP
$dataMDLast = $db->select()
	->from(
		['M_MARGINALDEPOSIT']
	)->query()->fetchAll();

if ($dataMDLast) {
	foreach ($dataMDLast as $row) {

		$sqlCekFreezeManual = $db->select()
			->from(
				['M_CUST_LINEFACILITY']
			)->where("CUST_ID = ?", $row['CUST_ID']);

		$selectLineFacility = $db->fetchRow($sqlCekFreezeManual);

		if ($row['COUNTING_DEADLINE_TOPUP'] > $selectLineFacility['GRACE_PERIOD']) {

			$dataLFUpdate  = [
				'STATUS'    => '4', // freeze
				'MONITOR_MD' => 1,
			];
			$whereLFUpdate = ['CUST_ID = ?' => $row['CUST_ID']];
			$db->update('M_CUST_LINEFACILITY', $dataLFUpdate, $whereLFUpdate);
		}
	}
}

// END FREEZE *****************************************************************************

// ---------------------------------------------------------------------------------------------

// }
// simpan log cron
echo 'AFFECTED ROW(S): COUNTING_DEADLINE_TOPUP (' . $x . ')' . PHP_EOL;
Application_Helper_General::cronLog(basename(__FILE__), 'COUNTING_DEADLINE_TOPUP', '1');
