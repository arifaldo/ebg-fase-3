<?php

/**
 * Tempholiday model
 * 
 * @author 
 * @version 
 */

class Changemanagement_Model_Tempholiday extends Changemanagement_Model_Tempchanges {
	
	protected $_moduleId = 'HDY';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_USER
		$listHoliday = $this->dbObj->select()
						  	->from('TEMP_HOLIDAY')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!count($listHoliday)){
        	
			$deleteHolidayInYear = $this->dbObj->delete('M_HOLIDAY',$this->dbObj->quoteInto('HOLIDAY_YEAR = ?', date("Y")));
			$userupdate = true;
			return true;
        }
		
		if(is_array($listHoliday)){
			
			$deleteHolidayInYear = $this->dbObj->delete('M_HOLIDAY',$this->dbObj->quoteInto('HOLIDAY_YEAR = ?',$listHoliday[0]['HOLIDAY_YEAR']));
			
			foreach ($listHoliday as $row) {
					$insertArr = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));
					$userupdate = $this->dbObj->insert('M_HOLIDAY',$insertArr);
			}
		}				  	
		//update record
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(listHoliday)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) {
		//will never be called
	}
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_HOLIDAY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() {
	}
}