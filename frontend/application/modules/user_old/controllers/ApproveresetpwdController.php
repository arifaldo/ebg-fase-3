<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
class user_ApproveresetpwdController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$allowUpdate = true;
		$fields = array	(
			'User ID'  			=> array	(
				'field' => 'USER_ID',
				'label' => $this->language->_('User ID'),
				'sortable' => false
			),
			'Fullname'  			=> array	(
				'field' => 'USER_FULLNAME',
				'label' => $this->language->_('Fullname'),
				'sortable' => false
			),
			'Request By'  			=> array	(
				'field' => 'USER_RPWD_ISBYBO',
				'label' => $this->language->_('Requested By'),
				'sortable' => false
			),
		);	
		
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$currentUserId = $this->_userIdLogin;
		$select = $this->_db->select()
		->FROM (array('a' =>'M_USER'),
			array('a.USER_ID', 'a.USER_FULLNAME','a.USER_RPWD_ISBYBO','a.USER_RPWD_ISBYUSER' ))
		->WHERE('a.CUST_ID = ?',$this->_custIdLogin)
		->WHERE('a.USER_RRESET = 1 ')
		->WHERE('a.USER_ID != ?',$currentUserId)
		->ORDER('a.USER_ID ASC'  ,'a.USER_FULLNAME  DESC');								
		$result = $this->_db->fetchall($select);

		if($result)
		{
			$this->view->button=true;
		}

		if($this->_request->isPost())
		{
			$filter_reject = $this->_getParam('filter_reject');
			$filter_approve = $this->_getParam('filter_approve');

			if($filter_approve == "Menyetujui"){
				$filter_approve = TRUE;
			}

			if($filter_reject == "Menolak Permintaan"){
				$filter_reject = TRUE;
			}

			$user_id = $this->_getParam('user_id');
			if($filter_reject == TRUE || $filter_approve == TRUE)
			{			
				$temp=0;
				if(!array_key_exists('1',array_flip($user_id)))
				{				
					$this->view->error = 1;
					$msg = $this->language->_('Error: Please checked selection').".";
				}
				else
				{
					$allowUpdate = true;
					foreach($user_id as $keyUserId => $value)
					{
						if($value=='0') unset($user_id[$keyUserId]);
						$checkUSER_ISLOGIN = $this->_db->FETCHONE(
							$this->_db->SELECT()
							->FROM('M_USER' ,array('USER_ISLOGIN'))
							->WHERE('USER_ID = ?',$keyUserId)
							->WHERE('CUST_ID = ?',$this->_custIdLogin)
						);

						if( $checkUSER_ISLOGIN == 1  && $filter_approve==TRUE)  
						{
							$allowUpdate = false;
							$user = $keyUserId;
							break;
						}
					}

					if($filter_approve==TRUE)
					{
						if($allowUpdate)
						{
							foreach($user_id as $keyUserId => $value)
							{
								// echo $key." : fungsi approve</br>";
								$CustomerUser = new CustomerUser($this->_custIdLogin,$keyUserId);
								$CustomerUser->approveRequestResetPassword();
								$temp=1;
							}
						}
						else
						{
							$this->view->user = $user;
						}
					}
					else if($filter_reject==TRUE)
					{
						foreach($user_id as $keyUserId => $value)
						{
							// echo $key." : fungsi reject</br>";
							$CustomerUser = new CustomerUser($this->_custIdLogin,$keyUserId);
							$CustomerUser->rejectRequestResetPassword();
							$temp=1;						
						}
					}
				}

				if($temp==1)
				{
					if($filter_reject == TRUE){
						$filter = 'Reject';	
					}
					else{
						$filter = 'Approve';	
					}
					
					Application_Helper_General::writeLog('UAUR','Success '.$filter.' Approve Unlock and Reset Passwrod : Cust Id ( '.$this->_custIdLogin.' ) ,User Id ( '.$keyUserId.' )');
					$this->setbackURL('/user/Approveresetpwd');
					$this->_redirect('/notification/success/index');
				}			
				$this->view->report_msg = $msg;
			}
		}
		$this->view->allowUpdate = $allowUpdate;
		$this->paging($select);		
		$this->view->fields = $fields;
		//$this->view->filter = $filter;
		if(!$this->_request->isPost()){
			Application_Helper_General::writeLog('UAUR','User > Approve Reset Password');
		}
	}
}
