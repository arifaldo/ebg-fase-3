<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SourceAccount.php';
class accountgroup_RepairController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$suggestId = $this->_getParam('suggest_id');
		$suggestId = (Zend_Validate::is($suggestId,'Digits'))? $suggestId : null;
		
		$fields = array(
						'number'  => array('field' => 'ACCT_NO',
											   'label' => 'Account Number',
											   'sortable' => false),
						'name'  => array('field' => 'ACCT_NAME',
											   'label' => 'Account Name',
											   'sortable' => false),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => 'CCY',
											  'sortable' => false),
						/*'alias'  => array('field' => 'ACCT_ALIAS_NAME',
											   'label' => 'Alias Name',
											   'sortable' => false),*/
						'group'  => array('field' => 'GROUP_NAME',
											   'label' => 'Account Group',
											   'sortable' => false)
				);
				
		//get sortby, sortdir
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
		
		$groupArr = $this->_db->select()
								->from(array('M_GROUPING'), array('GROUP_ID', 'GROUP_NAME'))
								->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
								->order('GROUP_ID ASC')
								->query()->fetchAll();		
		$listGroup = array(''=>'');
		$listGroup += Application_Helper_Array::listArray($groupArr,'GROUP_ID','GROUP_NAME');
		$this->view->group = $listGroup;

		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$suggest = $this->_db->select()
					   ->from(array('A' => 'TEMP_CUSTOMER_ACCT_GROUP')) 
					   ->joinLeft(array('G' => 'M_GROUPING'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'))
					   ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   ->order($sortBy.' '.$sortDir)
					   ->query()->fetchAll();
		   
		$notes = $this->_db->fetchOne(
				  $this->_db->select()
					   ->from("T_CUST_ACCT_GROUP_CHANGES",array('SUGGEST_REASON')) 
					   ->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   );
		   
		$this->view->data = $suggest;
		$this->view->fields = $fields;
		$this->view->suggest_id = $suggestId;
		$this->view->notes = $notes;
		
		if($this->_request->isPost() )
		{
			$param = $this->getRequest()->getParams();
			$alias = $this->getRequest()->getParam('alias');
			$group = $this->getRequest()->getParam('group');
			$suggestId = $this->_getParam('suggest_id');
			$suggestId = (Zend_Validate::is($suggestId,'Digits'))? $suggestId : null;
			
			if($suggestId)
			{
				try 
				{
					$this->_db->beginTransaction();
					
					foreach($alias as $key => $value)
					{						
						$updateArr = array(
											'GROUP_ID'			=> $group[$key],
											'ACCT_ALIAS_NAME' 	=> $value
										);
						
						$where['ACCT_NO = ?'] = (string)$key;
						$this->_db->update('TEMP_CUSTOMER_ACCT_GROUP',$updateArr,$where);
					}
					
					$updateArr = array('SUGGEST_STATUS'=>5);				
					$where1['SUGGEST_ID = ?'] = $suggestId;
					$this->_db->update('T_CUST_ACCT_GROUP_CHANGES',$updateArr,$where1);
					
					$this->_db->commit();
					Application_Helper_General::writeLog('GRAC','Repairing Account Group');
					
					$this->setbackURL('/'.$this->_request->getModuleName().'/acctgroupchange/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					Application_Helper_General::writeLog('GRAC','Roll Back Repair Account Group');
					$errorMsg = 'Database Query Failed';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}				
			}
			else
			{
			   $error_remark = 'Database Query Failed';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			}
			
			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}	
			}
		}
		else
		{
			Application_Helper_General::writeLog('GRAC','Viewing Repair Account Group');
		}
	}
}
