<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';
require_once 'Service/TransferWithin.php';
require_once 'CMD/Payment.php';

$flag = realpath(dirname(__FILE__)).'/inprogressBulkMultiCredit'; //path to flag file

$res = 'Execute Multi Credit In Progress';
$x = 0;

echo 'Bulk Multi Credit Executor'.PHP_EOL;

// if (file_exists($flag)) {
// 	echo 'Queue inprogress at '.date('Y-m-d H:i:s').PHP_EOL;
// }else{
	try{
		$fp = fopen($flag, 'w');
		@fclose($fp);
		echo 'Create flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
	}catch(Exception $e){
		$stringFailed = 'Can\'t create flag at: '.date('Y-m-d H:i:s');
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		echo $stringFailed.PHP_EOL;

		die();
	}

	$db = Zend_Db_Table::getDefaultAdapter();
	$config = Zend_Registry::get('config');

	$listpayroll = array('11','25','31','32');

	$data = $db->select()
		->from	(
			array('P' => 'T_PSLIP'),
			array('*')
		)
		->where('P.PS_STATUS = 8')
		->where('P.SENDFILE_STATUS = 0')
		->where('DATE(P.PS_EFDATE) = DATE(now())')
		->where('P.PS_TYPE NOT IN (?)', $listpayroll) //do not execute payroll

		->order('P.PS_UPDATED ASC')
		->query()->fetchAll();
	;

	$uid = shell_exec('id -u benny');
	$gid = shell_exec('id -g benny');

	$uid = intval($uid);
	$gid = intval($gid);

	//set user id and group id for running process
	//NOTE: posix_setuid and posix_setgid only works on POSIX compliant OS (does not work on windows)
	if(false === posix_setgid($gid)) {
	    echo "Error. Unable to set gid to ".$gid."!".PHP_EOL; 
	    exit();
	}
	if(false === posix_setuid($uid)) {
	    echo "Error. Unable to set uid to ".$uid."!".PHP_EOL;
	    exit();
	}

	declare(ticks = 1);

	$openProcesses = 0; 
	$procs = array();
	$maxProcs = 10;

	pcntl_signal(SIGCHLD, "childFinished");



	$limit = 10;
	$childCounter = $i = 0;

	
	// Zend_Debug::dump($data);

	if($data){
		foreach($data as $pslip){
			$continue = true;

			while($continue)
			{
			$x = 0;
			$result = array();
			$start = microtime(true);
$dbObj = Zend_Db_Table::getDefaultAdapter();
				// $dbObj->query("SET ANSI_WARNINGS ON");
				// $dbObj->query("SET ANSI_PADDING ON");
				// $dbObj->query("SET ANSI_NULLS ON");
				// $dbObj->query("SET QUOTED_IDENTIFIER ON");
				// $dbObj->query("SET CONCAT_NULL_YIELDS_NULL ON");
			$trx = $dbObj->select()
				->from(array('TT' => 'T_TRANSACTION'), array('*'))
				->joinLeft(array('TP'=>'T_PSLIP'), 'TP.PS_NUMBER = TT.PS_NUMBER', array('CUST_ID','USER_ID' => 'PS_CREATEDBY'))
				->joinLeft(array('MB'=>'M_BENEFICIARY'), 'MB.BENEFICIARY_ID = TT.BENEFICIARY_ID', array('BENEFICIARY_ADDRESS'))
				->where ('TT.PS_NUMBER = ?', $pslip['PS_NUMBER'])
				->where('RIGHT(TT.TRANSACTION_ID, 1) = ?', '3')
				->limit($limit)
				->query()->fetchAll();
			// echo $trx;die;
$dbObj->closeConnection();
unset($dbObj);
			foreach ($trx as $key => $val){


				$pid = pcntl_fork();
				if($pid == -1) {
					//fork failed, no child is created
					$message = $childCounter.'Fork Failed : '.$functionCode;
					// saveLog($LOG_PATH, $message);
					echo $message;
					throw new Exception($message);
				} elseif($pid == 0) {
					$dbObj = Zend_Db_Table::getDefaultAdapter();
				$x ++;
				if (!(empty($val['SOURCE_ACCOUNT'])) && !(empty($val['BENEFICIARY_ACCOUNT']))){
					$paymentObj = new Payment($val['PS_NUMBER'], $val['CUST_ID'], $val['USER_ID']);

					if ($val['TRANSFER_TYPE'] == "0")
						$return = $paymentObj->sendTransferSingleWithin($pslip, $val);

					if ($val['TRANSFER_TYPE'] == "1" || $val['TRANSFER_TYPE'] == "2")
						$return = $paymentObj->sendTransferSingleDomestic($pslip, $val);

					$result = array_merge_recursive($result, array('TRX' => $return['TRX']));
					}
				}else {
				$procs[] = $pid; // add the PID to the list
				++$openProcesses;
				echo 'Current process(es) : '.$openProcesses.PHP_EOL;
				if ($openProcesses >= $maxProcs) {
					pcntl_wait($status);     
				}                           
			}




			}
		}

			if ($result && is_array($result) && count($result))
				$paymentObj->setPaymentCompleted($trx, $result);
		}
	}

	try{
		unlink($flag);
		echo 'Delete flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
	}catch(Exception $e){
		$stringFailed = 'Can\'t delete flag at: '.date('Y-m-d H:i:s');
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		echo $stringFailed.PHP_EOL;

		die();
	}



function childFinished($signo) {

    global $openProcesses, $procs;

    // Check each process to see if it's still running
    // If not, remove it and decrement the count
    foreach ($procs as $key => $pid) if (posix_getpgid($pid) === false) {
        unset($procs[$key]);
        $openProcesses--;
    }

}

function signalHandler($signal) {
    global $killed;
    
	$message = "Got signal ".$signal;
	saveLog($LOG_PATH, $message);
    switch($signal) {
       case SIGTERM:
            //just update flag, so loop will terminate
            $killed = true;
            return;
			break;
	   case SIGINT:
            //just update flag, so loop will terminate
            $killed = true;
            return;
			break;	
	   case SIGHUP:
			//do nothing
			return;
			break;
	   case SIGQUIT:
			//do nothing
			return;
			break;
	   //default:
    }
}
// }

echo 'AFFECTED ROW(S): '.$res.'('.$x.')'.PHP_EOL;
Application_Helper_General::cronLog(basename(__FILE__), $res, '1');

?>