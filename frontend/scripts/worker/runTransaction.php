<?php

use function PHPSTORM_META\type;

require_once(realpath(dirname(__FILE__) . '/zf-cli.php'));
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'General/Settings.php';

error_reporting(E_ALL ^ E_NOTICE);

$sms = array();

$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();

$updateTransaction = function ($transactionId, $resultService) {

    $db = Zend_Db_Table::getDefaultAdapter();

    switch ($resultService['response_code']) {
        case '0000':
            $traStatus = 1;
            break;
        case '9999':
            $traStatus = 3;
            break;
        default:
            $traStatus = 4;
            break;
    }

    $db->update('T_BG_TRANSACTION', [
        'TRA_STATUS' => $traStatus,
        'RESPONSE_CODE' => $resultService['response_code'],
        'RESPONSE_DESC' => $resultService['response_desc'],
        'BANK_RESPONSE' => join(' - ', [$resultService['response_code'], $resultService['response_desc']]),
        'UUID' => $resultService['uuid'],
        'TRA_TIME' => new Zend_Db_Expr('now()'),
        'TRA_REF' => $resultService['ref']
    ], [
        'TRANSACTION_ID = ?' => $transactionId
    ]);
};

$cekTimeoutPerTrx = function ($transactionId, $status) {

    $db = Zend_Db_Table::getDefaultAdapter();

    $transaction = $db->select()
        ->from('T_BG_TRANSACTION')
        ->where('TRANSACTION_ID = ?', $transactionId)
        ->query()->fetch();

    $transCodeMapping = [
        1 => '8776',
        5 => '8771',
        6 => '8772',
        7 => '8768',
        8 => '8769',
    ];

    $success = false;

    $svcCekStatus = new SGO_Soap_ClientUser();

    if ($transaction) {
        $trans = $transaction;

        if (in_array($trans['SERVICE'], [2, 3])) {
            if ($trans['SERVICE'] == 2) {
                $inqLockDeposito = new Service_Account($trans['SOURCE_ACCT'], '');
                $resultInqLockDeposito = $inqLockDeposito->inquiryLockDeposito();

                // if ($resultInqLockDeposito['response_code'] == '0404' && $status == '3') {
                //     $db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
                //         'TRANSACTION_ID = ?' => $transactionId
                //     ]);

                //     $success = true;
                // }

                if ($resultInqLockDeposito == '0000') {
                    $unlockDeposito = new Service_Account($transaction["SOURCE_ACCT"], null);
                    $unlockDeposito = $unlockDeposito->unlockDeposito('T', $transaction['HOLD_BY_BRANCH'], $transaction['HOLD_SEQUENCE']);

                    if ($unlockDeposito['response_code'] == '0000') $success = true;
                    elseif ($status == '3' && $unlockDeposito['response_code'] != '0000') {
                        $db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 4], [
                            'TRANSACTION_ID = ?' => $transactionId
                        ]);
                    }
                }
            } else {
                $inqLockSaving = new Service_Account($trans['SOURCE_ACCT'], '');
                $resultInqLockSaving = $inqLockSaving->inqLockSaving();

                if ($resultInqLockSaving['response_code'] == '0000') {
                    $cekSeqNumber = array_filter($resultInqLockSaving['lock'], function ($item) use ($trans) {
                        return $item['sequence_number'] == $trans['HOLD_SEQUENCE'];
                    });

                    if (empty($cekSeqNumber)) {
                        $db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
                            'TRANSACTION_ID = ?' => $transactionId
                        ]);

                        $success = true;
                    } else {
                        $unholdSavingService = new Service_Account($transaction['SOURCE_ACCT'], '');
                        $unholdSavingService = $unholdSavingService->unlockSaving([
                            'branch_code' => $transaction['HOLD_BY_BRANCH'],
                            'sequence_number' => $transaction['HOLD_SEQUENCE']
                        ]);

                        if ($unholdSavingService['response_code'] == '0000') $success = true;
                        elseif ($status == '3' && $unholdSavingService['response_code'] != '0000') {
                            $db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 4], [
                                'TRANSACTION_ID = ?' => $transactionId
                            ]);
                        }
                    }
                }
            }
        } else {

            if ($trans['SERVICE'] == 1) {
                $mappingCode = $transCodeMapping[$trans['SERVICE']];

                $checkGiroOrSaving = new Service_Account($trans['SOURCE_ACCT'], '');
                $resultSvc = $checkGiroOrSaving->inquiryAccountBalance();

                if ($resultSvc['response_code'] == '0000' && strtolower($resultSvc['account_type']) == 't') {
                    $mappingCode = 8776;
                }
            }

            $svcCekStatus->callapi("inqtransfer", null, [
                "ref_trace" => $trans['TRA_REF'],
                "trans_code" => $mappingCode
            ]);

            $result = $svcCekStatus->getResult();

            if ($result['response_code'] == '0000') {

                $db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
                    'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
                ]);

                $success = true;
            } else {
                if ($status == '3') {
                    $db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 4], [
                        'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
                    ]);
                }

                $success = false;
            }
        }
    }

    return $success;
};

$prepareForSendEmail = function ($databg, $emailTemplate) {

    $db = Zend_Db_Table::getDefaultAdapter();

    $data = $databg;

    $settings = new Settings();
    $allSetting = $settings->getAllSetting();

    $config = Zend_Registry::get('config');

    $bgcgType   = $config["bgcg"]["type"]["desc"];
    $bgcgCode   = $config["bgcg"]["type"]["code"];
    $arrbgcg     = array_combine($bgcgCode, $bgcgType);

    $bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
    $bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
    $arrbgclosingType     = array_combine($bgclosingCode, $bgclosingType);

    $bgclosingStatus   = $config["bgclosinghistory"]["status"]["desc"];
    $bgclosingstatusCode   = $config["bgclosinghistory"]["status"]["code"];
    $arrbgclosingStatus     = array_combine($bgclosingstatusCode, $bgclosingStatus);

    $getEscrowRelease = $db->select()
        ->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
        ->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
        ->where('ACCT_INDEX = ?', 1)
        ->query()->fetchAll();

    $benefAcctNumber = array_shift(array_filter($getEscrowRelease, function ($item) {
        return strtolower($item['PS_FIELDNAME']) == 'beneficiary account number';
    }))['PS_FIELDVALUE'];

    $benefAcctName = array_shift(array_filter($getEscrowRelease, function ($item) {
        return strtolower($item['PS_FIELDNAME']) == 'beneficiary account name';
    }))['PS_FIELDVALUE'];

    $getBankName = $db->select()
        ->from(['mdbt' => 'M_DOMESTIC_BANK_TABLE'], ['mdbt.BANK_NAME'])
        ->where('mdbt.SWIFT_CODE = ?', new Zend_Db_Expr('(SELECT PS_FIELDVALUE FROM TEMP_BANK_GUARANTEE_DETAIL_CLOSE tbgdc WHERE tbgdc.CLOSE_REF_NUMBER = ' . $db->quote($data['databg']['CLOSE_REF_NUMBER']) . ' AND LOWER(tbgdc.PS_FIELDNAME) = \'swift code\')'))
        ->query()->fetch();

    // T BANK GUARANTEE UNDERLYING
    $getAllUnderlying = $db->select()
        ->from('T_BANK_GUARANTEE_UNDERLYING')
        ->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER_SPESIAL'])
        ->query()->fetchAll();

    $underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

    $underlyingForEmail = '
		<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
			<br>
		<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>
		';

    $getBgCurrency = $db->select()
        ->from('T_BANK_GUARANTEE_DETAIL')
        ->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER_SPESIAL'])
        ->where('LOWER(PS_FIELDNAME) = ?', 'currency')
        ->query()->fetch();

    $getBgCurrency = $getBgCurrency ? $getBgCurrency['PS_FIELDVALUE'] : '-';

    // GET BENEF ACCOUNT
    $getBenefAccount = $db->select()
        ->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
        ->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
        ->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
        ->where('ACCT_INDEX = ?', 1)
        ->query()->fetch();

    $getBenefAccount = $getBenefAccount['PS_FIELDVALUE'];

    $checkTransaction = $db->select()
        ->from('T_BG_TRANSACTION')
        ->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
        ->where('BENEF_ACCT = ?', $getBenefAccount ?: '')
        ->query()->fetch();

    // GET SPLIT
    $getSplit = $db->select()
        ->from(['A' => 'T_BANK_GUARANTEE_SPLIT'])
        ->where('A.BG_NUMBER = ?', $data['databg']['BG_NUMBER'])
        ->query()->fetchAll();

    $filterWihtoutEscrow = array_filter($getSplit, function ($item) {
        return strtolower($item['ACCT_DESC']) != 'escrow';
    });

    $filterWihtoutEscrow = $filterWihtoutEscrow ?: [];

    $sumAll = array_sum(array_column($filterWihtoutEscrow, 'AMOUNT'));
    $sumAllNumberFormat = number_format($sumAll, 2);

    $dataEmail = [
        '[[close_ref_number]]' => $data['databg']['CLOSE_REF_NUMBER'],
        '[[bg_number]]' => $data['databg']['BG_NUMBER'],
        '[[bg_subject]]' => $data['databg']["BG_SUBJECT"],
        '[[usage_purpose]]' => ucwords(strtolower($data['databg']['USAGE_PURPOSE_DESC'])),
        '[[cust_name]]' => $data['databg']["CUST_NAME"],
        '[[cust_cp]]' => $data['databg']["CUST_CP"],
        '[[contract_name]]' => $underlyingForEmail,
        '[[bg_amount]]' => implode(' ', [$getBgCurrency, number_format($data['databg']['BG_AMOUNT'], 2)]),
        '[[transaction_id]]' => $checkTransaction['TRANSACTION_ID'],
        '[[transaction_amount]]' => implode(' ', [$checkTransaction['TRA_CCY'], number_format($checkTransaction['TRA_AMOUNT'], 2)]),
        '[[transaction_date]]' => date('d-m-Y', strtotime($checkTransaction['TRA_TIME'])),
        '[[tra_status]]' => 'Sukses',
        '[[recipient_name]]' => $data['databg']['RECIPIENT_NAME'],
        '[[recipient_cp]]' => $data['databg']['RECIPIENT_CP'],
        '[[counter_warranty_type]]' => $arrbgcg[$data['databg']['COUNTER_WARRANTY_TYPE']],
        '[[change_type]]' => $arrbgclosingType[$data['databg']['CHANGE_TYPE_CLOSE']],
        '[[suggestion_status]]' => $arrbgclosingStatus[15],
        '[[repair_notes]]' => $data['historydata']['BG_REASON'],
        '[[beneficiary_account_number]]' => $benefAcctNumber,
        '[[time_period_start]]' => date('d M Y', strtotime($data['databg']['TIME_PERIOD_START'])),
        '[[time_period_end]]' => date('d M Y', strtotime($data['databg']['TIME_PERIOD_END'])),
        '[[bank_name]]' => $getBankName['BANK_NAME'],
        '[[beneficiary_account_name]]' => $benefAcctName,
        '[[insurance_name]]' => $data['databg']['INS_NAME'] ?: '-',
        '[[insurance_branch_name]]' => $data['databg']['INS_BRANCH_NAME'],
        '[[ps_status]]' => 'Proses Selesai',
        '[[ps_done]]' => date('d-m-Y'),
        '[[title_counter_type]]' => $data['databg']['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
        '[[md_principal]]' => implode(' ', [$getBgCurrency, $sumAllNumberFormat]),
    ];

    $getEmailTemplate = $allSetting[$emailTemplate];
    $getEmailTemplate = strtr($getEmailTemplate, $dataEmail);

    return $getEmailTemplate;
};

$runTransaction = function ($transaction, $status) use ($updateTransaction, $cekTimeoutPerTrx) {

    $db = Zend_Db_Table::getDefaultAdapter();


    if (in_array($status, [4, 6])) {
        // MAPPING SERVICE
        switch ($transaction['SERVICE']) {
            case 1: // inhouse
                $transferData = [
                    "SOURCE_ACCOUNT_NUMBER" => $transaction['SOURCE_ACCT'],
                    "SOURCE_ACCOUNT_NAME" => $transaction['SOURCE_ACCT_NAME'],
                    "BENEFICIARY_ACCOUNT_NUMBER" => $transaction['BENEF_ACCT'],
                    "BENEFICIARY_ACCOUNT_NAME" => $transaction['BENEF_ACCT_NAME'],
                    "PHONE_NUMBER_FROM" => $transaction['SOURCE_PHONE'],
                    "AMOUNT" => floatval($transaction["TRA_AMOUNT"]),
                    "PHONE_NUMBER_TO" => $transaction['BENEF_PHONE'],
                    "RATE1" => "10000000",
                    "RATE2" => "10000000",
                    "RATE3" => "10000000",
                    "RATE4" => "10000000",
                    "DESCRIPTION" => $transaction['REMARKS1'],
                    "DESCRIPTION_DETAIL" => 'EBG',
                ];

                $transferInhouse =  new Service_TransferWithin($transferData);
                $result = $transferInhouse->sendTransfer();
                $resultTransferInhouse = $result["RawResult"];

                $finalResult = $resultTransferInhouse;

                $updateTransaction($transaction['TRANSACTION_ID'], $resultTransferInhouse);
                break;
            case 2: // unlock deposito
                $unlockDeposito = new Service_Account($transaction["SOURCE_ACCT"], null);
                $unlockDeposito = $unlockDeposito->unlockDeposito('T', $transaction['HOLD_BY_BRANCH'], $transaction['HOLD_SEQUENCE']);

                $finalResult = $unlockDeposito;

                $updateTransaction($transaction['TRANSACTION_ID'], $unlockDeposito);
                break;
            case 3: // unhold saving
                $unholdSavingService = new Service_Account($transaction['SOURCE_ACCT'], '');
                $unholdSavingService = $unholdSavingService->unlockSaving([
                    'branch_code' => $transaction['HOLD_BY_BRANCH'],
                    'sequence_number' => $transaction['HOLD_SEQUENCE']
                ]);

                $finalResult = $unholdSavingService;

                $updateTransaction($transaction['TRANSACTION_ID'], $unholdSavingService);
                break;
            case 5: // deposito disbursement
                $depositoDisrburseService =  new Service_Account($transaction['SOURCE_ACCT'], '');
                $resultDepoDisburse = $depositoDisrburseService->depositoDisbursement(
                    $transaction['SOURCE_ACCT_CCY'],
                    $transaction['SOURCE_ACCT'],
                    $transaction['SOURCE_ACCT_NAME'],
                    $transaction['SOURCE_ACCT_CCY'],
                    $transaction['BENEF_ACCT'],
                    $transaction['BENEF_ACCT_NAME'],
                    floatval($transaction['TRA_AMOUNT']),
                    '10000000',
                    '10000000',
                    '10000000',
                    '10000000',
                    $transaction['REMARKS1'],
                    'EBG'
                );

                $finalResult = $resultDepoDisburse;

                $updateTransaction($transaction['TRANSACTION_ID'], $resultDepoDisburse);
                break;
            case 6: // loan disbursement
                $loanDisburse =  new Service_Account('', '');
                $resultLoanDisburse = $loanDisburse->loanDisbursement(
                    $transaction['SOURCE_ACCT_CCY'],
                    $transaction['SOURCE_ACCT'],
                    $transaction['SOURCE_ACCT_NAME'],
                    $transaction['TRA_CCY'],
                    $transaction['BENEF_ACCT'],
                    $transaction['BENEF_ACCT_NAME'],
                    floatval($transaction['TRA_AMOUNT']),
                    "10000000",
                    "10000000",
                    "10000000",
                    "10000000"
                );

                $finalResult = $resultLoanDisburse;

                $updateTransaction($transaction['TRANSACTION_ID'], $resultLoanDisburse);
                break;
            case 7: // skn
                $getBgBranch = $db->select()
                    ->from(['A' => 'T_BANK_GUARANTEE'], ['A.BG_BRANCH'])
                    ->where('A.BG_NUMBER = ?', $transaction['BG_NUMBER'])
                    ->query()->fetch();

                $callServiceRunningNumber = new Service_Account('', '');
                $runningNumberResult = $callServiceRunningNumber->inquiryRunningNumber([
                    'type' => 'SKNGN'
                ]);
                $remittenceProduct = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['remittance_product'] : '00';
                $runningNumber = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['running_number'] : '00';

                $remittenceString = implode('', [$getBgBranch['BG_BRANCH'], '01', $remittenceProduct, str_pad($runningNumber, 7, '0', STR_PAD_LEFT)]);

                $bankCode = $db->select()
                    ->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
                    ->where('CLOSE_REF_NUMBER = ?', $transaction['CLOSE_REF_NUMBER'])
                    ->where('LOWER(PS_FIELDNAME) = ?', 'swift code')
                    ->query()->fetch();

                $bankCode = $bankCode['PS_FIELDVALUE'];

                $transferData = [
                    'transfer_type' => '1',
                    'remittance' => $remittenceString,
                    'source_account_number' => $transaction['SOURCE_ACCT'],
                    'source_account_name' => $transaction['SOURCE_ACCT_NAME'],
                    'source_address' => substr($transaction['SOURCE_ADDRESS'], 0, 40),
                    'beneficiary_bank_code' => $bankCode,
                    'beneficiary_account_number' => $transaction['BENEF_ACCT'],
                    'beneficiary_account_name' => $transaction['BENEF_ACCT_NAME'],
                    'beneficiary_address' => substr($transaction['BENEF_ADDRESS'], 0, 40),
                    'amount' => floatval($transaction['TRA_AMOUNT']),
                    'remark1' => $transaction['REMARKS1'],
                    'remark2' => 'EBG',
                    'remark3' => 'EBG',
                    'fee' => $transaction['CHARGE_AMOUNT'],
                    'account_fee' => $transaction['CHARGE_ACCT'],
                ];

                $transferOtherService =  new Service_Account('', '');
                $resultSkn = $transferOtherService->transferDomestic($transferData);

                $finalResult = $resultSkn;

                $updateTransaction($transaction['TRANSACTION_ID'], $resultSkn);
                break;
            case 8: // rtgs
                $getBgBranch = $db->select()
                    ->from(['A' => 'T_BANK_GUARANTEE'], ['A.BG_BRANCH'])
                    ->where('A.BG_NUMBER = ?', $transaction['BG_NUMBER'])
                    ->query()->fetch();

                $callServiceRunningNumber = new Service_Account('', '');
                $runningNumberResult = $callServiceRunningNumber->inquiryRunningNumber([
                    'type' => 'SKNGN'
                ]);
                $remittenceProduct = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['remittance_product'] : '00';
                $runningNumber = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['running_number'] : '00';

                $remittenceString = implode('', [$getBgBranch['BG_BRANCH'], '01', $remittenceProduct, str_pad($runningNumber, 7, '0', STR_PAD_LEFT)]);

                $bankCode = $db->select()
                    ->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
                    ->where('CLOSE_REF_NUMBER = ?', $transaction['CLOSE_REF_NUMBER'])
                    ->where('LOWER(PS_FIELDNAME) = ?', 'swift code')
                    ->query()->fetch();

                $bankCode = $bankCode['PS_FIELDVALUE'];

                $transferData = [
                    'transfer_type' => '2',
                    'remittance' => $remittenceString,
                    'source_account_number' => $transaction['SOURCE_ACCT'],
                    'source_account_name' => $transaction['SOURCE_ACCT_NAME'],
                    'source_address' => substr($transaction['SOURCE_ADDRESS'], 0, 40),
                    'beneficiary_bank_code' => $bankCode,
                    'beneficiary_account_number' => $transaction['BENEF_ACCT'],
                    'beneficiary_account_name' => $transaction['BENEF_ACCT_NAME'],
                    'beneficiary_address' => substr($transaction['BENEF_ADDRESS'], 0, 40),
                    'amount' => floatval($transaction['TRA_AMOUNT']),
                    'remark1' => $transaction['REMARKS1'],
                    'remark2' => 'EBG',
                    'remark3' => 'EBG',
                    'fee' => $transaction['CHARGE_AMOUNT'],
                    'account_fee' => $transaction['CHARGE_ACCT'],
                ];

                $transferOtherService =  new Service_Account('', '');
                $resultRtgs = $transferOtherService->transferDomestic($transferData);

                $finalResult = $resultRtgs;

                $updateTransaction($transaction['TRANSACTION_ID'], $resultRtgs);
                break;

            default:
                # code...
                break;
        }
    } else {
        $finalResult = ['response_code' => '9999'];
    }

    if ($finalResult['response_code'] == '9999') {

        $maxTry = 3;
        $counterTry = 1;
        while (true) {
            $checkTrxTimeout = $cekTimeoutPerTrx($transaction['TRANSACTION_ID'], $status);

            if ($checkTrxTimeout) $finalResult['response_code'] = '0000';

            // JIKA SUDAH DICOBA SEBANYAK 3 KALI
            if ($counterTry == $maxTry || $checkTrxTimeout) break;

            $counterTry++;
            sleep(2);
        }
    }

    return $finalResult;
};

$sendEmail = function ($emailAddress, $subjectEmail, $emailTemplate) {
    $setting = new Settings();
    $allSetting = $setting->getAllSetting();

    $data = [
        '[[master_bank_name]]' => $allSetting["master_bank_name"],
        '[[master_app_name]]' => $allSetting["master_bank_app_name"],
        '[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
        '[[master_bank_email]]' => $allSetting["master_bank_email"],
        '[[master_bank_telp]]' => $allSetting["master_bank_telp"],
    ];

    $getEmailTemplate = strtr($emailTemplate, $data);
    $emailClass = new Application_Helper_Email;
    $emailClass->sendEmail($emailAddress, $subjectEmail, $getEmailTemplate);
};

$callback = function ($sms) use ($runTransaction, $prepareForSendEmail, $sendEmail) {

    $request = json_decode($sms->body, true);

    $closeRefNumber = $request['close_ref_number'];
    $statusExe = $request['status'];

    $db = Zend_Db_Table::getDefaultAdapter();

    $Settings = new Settings();
    $templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
    $templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');

    $startExecute = date("Y-m-d H:i:s");

    $getAllTransaction = $db->select()
        ->from(['A' => 'T_BG_TRANSACTION'], ['*'])
        ->joinLeft(['B' => 'T_BG_PSLIP'], 'A.CLOSE_REF_NUMBER = B.CLOSE_REF_NUMBER', ['B.TYPE'])
        ->where('A.CLOSE_REF_NUMBER = ?', $closeRefNumber)
        ->order('TRANSACTION_ID ASC')
        ->query()->fetchAll();

    if ($getAllTransaction) {
        $failUnlockUnhold = [
            'TYPE_SERVICE' => 0,
            'STATUS' => false
        ];

        $saveReppk = [
            'account_number' => '',
            'account_name' => ''
        ];

        foreach ($getAllTransaction as $key => $transaction) {

            $bgdata = $db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array(
                    '*', 'CLOSE_REF_NUMBER_T' => 'A.CLOSE_REF_NUMBER',
                    'INS_NAME' => new Zend_Db_Expr('(SELECT CI.CUST_NAME FROM M_CUSTOMER CI WHERE CI.CUST_ID = A.BG_INSURANCE_CODE LIMIT 1)'),
                    'INS_BRANCH_NAME' => new Zend_Db_Expr('(SELECT INS_BRANCH_NAME FROM M_INS_BRANCH WHERE INS_BRANCH_CODE = (SELECT PS_FIELDVALUE FROM T_BANK_GUARANTEE_DETAIL tbgd WHERE tbgd.BG_REG_NUMBER = A.BG_REG_NUMBER AND LOWER(tbgd.PS_FIELDNAME) = \'insurance branch\' LIMIT 1) LIMIT 1)'),
                    'CUST_EMAIL_BG' => 'A.CUST_EMAIL',
                    'BG_REG_NUMBER_SPESIAL' => 'A.BG_REG_NUMBER',
                    'INS_BRANCH_EMAIL' => new Zend_Db_Expr('(SELECT INS_BRANCH_EMAIL FROM M_INS_BRANCH WHERE INS_BRANCH_CODE = (SELECT PS_FIELDVALUE FROM T_BANK_GUARANTEE_DETAIL tbgd WHERE tbgd.BG_REG_NUMBER = A.BG_REG_NUMBER AND LOWER(tbgd.PS_FIELDNAME) = \'insurance branch\' LIMIT 1) LIMIT 1)')
                ))
                ->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
                ->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME', 'C.BRANCH_EMAIL'))
                ->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLOSE_REF_NUMBER', 'A.CLAIM_FIRST_VERIFIED'))
                ->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
                ->joinleft(array('F' => 'T_BG_PSLIP'), 'F.BG_NUMBER = A.BG_NUMBER', array('PSLIP_STATUS' => 'F.TYPE', 'PSLIP_DONE' => 'F.PS_DONE'))
                ->where('A.BG_NUMBER = ?', $transaction['BG_NUMBER'])
                ->query()->fetch();

            if ($transaction['SERVICE'] == 4) {
                if ($transaction['SOURCE_ACCT'] == 'XXX' && $statusExe == '6') {

                    // $db->update('T_BG_TRANSACTION', ['TRA_STATUS' => '4'], [
                    //     'CLOSE_REF_NUMBER = ?' => $closeRefNumber,
                    //     'SERVICE NOT IN(?)' => [4]
                    // ]);

                    break;
                } elseif ($transaction['SOURCE_ACCT'] == 'XXX' && $transaction['TRA_STATUS'] == '4') {
                    // BUAT REPPK / REKENING PENAMPUNG
                    $clientUser = new SGO_Soap_ClientUser();
                    $requestopen = array();
                    $requestopen['cif'] = $bgdata["CUST_CIF"];
                    $requestopen['branch_code'] = $bgdata['BG_BRANCH'];
                    $requestopen['product_code'] = 'K2';
                    $requestopen['currency'] = 'IDR';

                    $clientUser->callapi('createaccount', null, $requestopen);
                    $reppkResult = $clientUser->getResult();

                    if ($reppkResult['response_code'] == '0000') {

                        $db->update('T_BG_TRANSACTION', [
                            'RESPONSE_CODE' => $reppkResult['response_code'],
                            'RESPONSE_DESC' => $reppkResult['response_desc'],
                            'TRA_STATUS' => 1
                        ], [
                            'TRANSACTION_ID = ?' => $transaction['TRANSACTION_ID']
                        ]);

                        $reppkDetailService = new Service_Account($reppkResult['account_number'], '');
                        $reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

                        // UPDATE DB YANG MENGGUNAKAN REPPK 
                        $db->update('T_BG_TRANSACTION', [
                            'SOURCE_ACCT' => $reppkResult['account_number'],
                            'SOURCE_ACCT_NAME' => $reppkDetailAcct['account_name']
                        ], [
                            'SOURCE_ACCT = ?' => 'XXX',
                            'CLOSE_REF_NUMBER = ?' => $transaction['CLOSE_REF_NUMBER']
                        ]);

                        $db->update('T_BG_TRANSACTION', [
                            'BENEF_ACCT' => $reppkResult['account_number'],
                            'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name']
                        ], [
                            'BENEF_ACCT = ?' => 'XXX',
                            'CLOSE_REF_NUMBER = ?' => $transaction['CLOSE_REF_NUMBER']
                        ]);

                        $saveReppk = [
                            'account_number' => $reppkResult['account_number'],
                            'account_name' => $reppkDetailAcct['account_name']
                        ];
                    } else {
                        $db->update('T_BG_TRANSACTION', [
                            'RESPONSE_CODE' => $reppkResult['response_code'],
                            'RESPONSE_DESC' => $reppkResult['response_desc']
                        ], [
                            'TRANSACTION_ID = ?' => $transaction['TRANSACTION_ID']
                        ]);
                        break;
                    }
                } else continue;
            }

            if ($transaction['SERVICE'] != 4) {
                if ($transaction['SOURCE_ACCT'] == 'XXX') {
                    $transaction['SOURCE_ACCT'] = $saveReppk['account_number'];
                    $transaction['SOURCE_ACCT_NAME'] = $saveReppk['account_name'];
                }

                if ($transaction['BENEF_ACCT'] == 'XXX') {
                    $transaction['BENEF_ACCT'] = $saveReppk['account_number'];
                    $transaction['BENEF_ACCT_NAME'] = $saveReppk['account_name'];
                }
            }

            if ($transaction['TRA_STATUS'] != $statusExe) {

                $transferObligeeAcct = end($getAllTransaction);
                if ($transaction['TYPE'] != 3) {
                    if ($transaction['TRA_STATUS'] != '6') continue;
                }
                if ($transaction['SOURCE_ACCT'] != $transferObligeeAcct['SOURCE_ACCT']) {
                    if ($transaction['TRA_STATUS'] != '6') continue;
                } else {
                    $failTransaction = $db->select()
                        ->from('T_BG_TRANSACTION')
                        ->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
                        ->where('TRA_STATUS NOT IN(?)', [1, 2, 5])
                        ->where('TRANSACTION_ID NOT IN(?)', $transferObligeeAcct['TRANSACTION_ID'])
                        ->query()->fetchAll();

                    if ($failTransaction) continue;
                }
            }

            $statusTransaction = [];

            if ($failUnlockUnhold['STATUS'] && $transaction['TYPE'] == 3) {
                // $db->update('T_BG_TRANSACTION', [
                //     'TRA_STATUS' => '5',
                //     'RESPONSE_CODE' => '1111',
                //     'RESPONSE_DESC' => ($failUnlockUnhold['TYPE_SERVICE'] == 2 ? 'Unlock Deposito' : 'Unhold Tabungan') . 'gagal',
                //     'TRA_TIME' => new Zend_Db_Expr('NOW()'),
                //     'UUID' => '-',
                //     'TRA_REF' => '-'
                // ], [
                //     'TRANSACTION_ID = ?' => $transaction['TRANSACTION_ID']
                // ]);

                $failUnlockUnhold['TYPE_SERVICE'] = 0;
                $failUnlockUnhold['STATUS'] = false;
            } else {
                $transferObligeeAcct = end($getAllTransaction);

                if ($transaction['TYPE'] == 3 && $transaction['SOURCE_ACCT'] == $transferObligeeAcct['SOURCE_ACCT']) {

                    // JIKA KLAIM OBLIGEE CEK APAKAH SEMUA TRANSAKSI SUDAH BERHASIL SEMUA, JIKA SUDAH LAKUKAN TRANSFER KE OBLIGEE
                    $failTransaction = $db->select()
                        ->from('T_BG_TRANSACTION')
                        ->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
                        ->where('TRA_STATUS NOT IN(?)', [1, 2, 5])
                        ->where('TRANSACTION_ID NOT IN(?)', [$transaction['TRANSACTION_ID']])
                        ->query()->fetchAll();

                    // if (!$failTransaction) $statusTransaction = $runTransaction($transaction, $statusExe);
                    if (!$failTransaction) $statusTransaction = $runTransaction($transaction, $transaction['TRA_STATUS']);
                } else {
                    // $statusTransaction = $runTransaction($transaction, $statusExe);
                    if ($transaction['TYPE'] == 3) {
                        $getBeforeTrx = $db->select()
                            ->from('T_BG_TRANSACTION')
                            ->where('TRANSACTION_ID = ?', $getAllTransaction[$key - 1]['TRANSACTION_ID'])
                            ->query()->fetch();

                        if (in_array($getBeforeTrx['SERVICE'], [2, 3])) {
                            if (in_array($getBeforeTrx['TRA_STATUS'], [1, 2])) $statusTransaction = $runTransaction($transaction, $transaction['TRA_STATUS']);
                        } else {
                            $statusTransaction = $runTransaction($transaction, $transaction['TRA_STATUS']);
                        }
                    } else {
                        $statusTransaction = $runTransaction($transaction, $transaction['TRA_STATUS']);
                    }
                }
            }

            // if ($statusTransaction['response_code'] == '0000' && $transaction['TYPE'] == 3 && in_array($transaction['SERVICE'], [2, 3]) && $statusExe == '4') {
            //     $runTransaction($getAllTransaction[$key + 1], $getAllTransaction[$key + 1]['TRA_STATUS']);
            // }

            if ($statusTransaction['response_code'] != '0000' && $transaction['TYPE'] == 3 && in_array($transaction['SERVICE'], [2, 3]) && $statusExe == '6') {
                $failUnlockUnhold['TYPE_SERVICE'] = $transaction['SERVICE'];
                $failUnlockUnhold['STATUS'] = true;
            }

            $getNewStatusTransaction = $db->select()
                ->from('T_BG_TRANSACTION')
                ->where('TRANSACTION_ID = ?', $transaction['TRANSACTION_ID'])
                ->query()->fetch();

            if ($getNewStatusTransaction) {
                $today = date("Ymd");
                $nametext = $closeRefNumber . $today . '.txt';
                $path_file = LIBRARY_PATH . '/data/logs/transaction/';

                $dirname = dirname($path_file . $nametext);

                if (!is_dir($dirname)) {
                    mkdir($dirname, 0755, true);
                }

                $fp = fopen($path_file . $nametext, 'a'); //opens file in append mode
                fwrite($fp, json_encode($getNewStatusTransaction));
                fwrite($fp, PHP_EOL);
                fclose($fp);

                shell_exec('chown nginx ' . $path_file);
                shell_exec('chown nginx ' . $path_file . $nametext);
            }
        }

        $failTransaction = $db->select()
            ->from('T_BG_TRANSACTION')
            ->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
            ->where('TRA_STATUS NOT IN(?)', [1, 2, 5])
            ->query()->fetchAll();

        if (!$failTransaction) {
            // SEMUA TRANSAKSI BERHASIL
            $db->update('T_BG_PSLIP', [
                'PS_STATUS' => '5',
                'PS_DONE' => new Zend_Db_Expr('now()')
            ], [
                'CLOSE_REF_NUMBER = ?' => $closeRefNumber
            ]);

            // CEK TOTAL TRANSAKSI
            $countTransaction = $db->select()
                ->from('T_BG_TRANSACTION', [])
                ->columns([
                    'TOTAL' => new Zend_Db_Expr('COUNT(*)')
                ])
                ->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
                ->query()->fetch();

            // KIRIM EMAIL JIKA TRANSAKSI LEBIH DARI SATU
            if ($countTransaction['TOTAL'] > 0) {

                $bgdata['BG_REG_NUMBER'] = $bgdata['BG_REG_NUMBER_SPESIAL'];
                $bgdata['CLOSE_REF_NUMBER'] = $bgdata['CLOSE_REF_NUMBER_T'];

                $data = [];
                $data['databg'] = $bgdata;

                if ($data['databg']['PSLIP_STATUS'] == 3) {

                    $reppkAcct = $db->select()
                        ->from('T_BG_TRANSACTION')
                        ->columns([
                            'SOURCE_ACCT'
                        ])
                        ->where('CLOSE_REF_NUMBER = ?', $transaction['CLOSE_REF_NUMBER'])
                        ->where('SERVICE = ?', 4)
                        ->query()->fetch();

                    $reppkAcct = $reppkAcct['SOURCE_ACCT'];

                    // CEK REKENING REPPK APAKAH MASIH ADA SISA
                    $reppkDetailService = new Service_Account($reppkAcct, '');
                    $reppkDetailAcct = $reppkDetailService->inquiryAccountBalance();
                    $reppkInfoAcct = $reppkDetailService->inquiryAccontInfo();

                    if ($reppkDetailAcct['available_balance'] > 0) {
                        $sisaDana = true;
                        // INSERT TO T BG TRANSACTION
                        $insertData = [
                            'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad(count($getAllTransaction) + 1, 2, '0', STR_PAD_LEFT),
                            'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
                            'BG_NUMBER' => $data['databg']['BG_NUMBER'],
                            'SERVICE' => 9, // MANUAL SETTLEMENT
                            'SOURCE_ACCT' => $reppkAcct,
                            'SOURCE_ACCT_CCY' => 'IDR',
                            'CHARGE_ACCT' => '',
                            'BENEF_SWIFT_CODE' => '',
                            'BENEF_ACCT' => '',
                            'BENEF_ACCT_NAME' => '',
                            'TRA_CCY' => 'IDR',
                            'TRA_AMOUNT' => $reppkDetailAcct['available_balance'],
                            'CHARGE_AMOUNT' => '',
                            "SOURCE_ACCT_NAME" => $reppkDetailAcct['account_name'],
                            'REMARKS1' => strval($data['databg']['CLOSE_REF_NUMBER'] . " Manual Settlement"),
                            'REMARKS2' => '',
                            'SOURCE_ADDRESS' => '',
                            'SOURCE_PHONE' => $reppkInfoAcct['phone_number'] ?: '000',
                            'BENEF_ADDRESS' => '',
                            'BENEF_PHONE' => '',
                            'TRA_TIME' => new Zend_Db_Expr('now()'),
                            'TRA_STATUS' => '5',
                            'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
                            'UPDATE_APPROVEDBY' => 'SYSTEM',
                            'UUID' => '',
                            'RESPONSE_CODE' => '',
                            'RESPONSE_DESC' => '',
                            'TRA_REF' => ''
                        ];

                        $db->insert('T_BG_TRANSACTION', $insertData);
                    }

                    $getNewStatusTransactionObligee = $db->select()
                        ->from('T_BG_TRANSACTION')
                        ->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
                        ->where('SOURCE_ACCT = ?', $reppkAcct)
                        ->where('SERVICE != ?', 4)
                        ->query()->fetch();

                    if ($getNewStatusTransactionObligee['TRA_STATUS'] == 1) {

                        $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_3B');
                        if ($data['databg']['RECIPIENT_EMAIL']) $sendEmail($data['databg']['RECIPIENT_EMAIL'], strval('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

                        // SEND EMAIL TO PRINCIPAL
                        $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_3A');
                        if ($data['databg']['CUST_EMAIL']) $sendEmail($data['databg']['CUST_EMAIL_BG'], strval('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

                        // SEND EMAIL TO KANTOR CABANG PENERBIT
                        $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_3C');
                        if ($data['databg']['BRANCH_EMAIL']) $sendEmail($data['databg']['BRANCH_EMAIL'], strval('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

                        if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 1) {
                            // FULL COVER
                            $settings = new Settings();
                            $getGroupFC = $settings->getSettingNew('email_group_cashcoll');
                            $getGroupFC = explode(',', $getGroupFC);
                            $getGroupFC = array_map('trim', $getGroupFC);
                            if ($getGroupFC) {
                                foreach ($getGroupFC as $emailGroup) {
                                    $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_3C');
                                    if ($emailGroup) $sendEmail($emailGroup, strval('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
                                }
                            }
                        }

                        if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 2) {
                            // GROUP LINEFACILITY
                            $settings = new Settings();
                            $getGroupFC = $settings->getSettingNew('email_group_linefacility');
                            $getGroupFC = explode(',', $getGroupFC);
                            $getGroupFC = array_map('trim', $getGroupFC);
                            if ($getGroupFC) {
                                foreach ($getGroupFC as $emailGroup) {
                                    $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_3C');
                                    if ($emailGroup) $sendEmail($emailGroup, strval('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
                                }
                            }
                        }

                        if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 3) {
                            // GROUP ASURANSI
                            $settings = new Settings();
                            $getGroupFC = $settings->getSettingNew('email_group_insurance');
                            $getGroupFC = explode(',', $getGroupFC);
                            $getGroupFC = array_map('trim', $getGroupFC);
                            if ($getGroupFC) {
                                foreach ($getGroupFC as $emailGroup) {
                                    $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_3C');
                                    if ($emailGroup) $sendEmail($emailGroup, strval('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
                                }
                            }

                            if ($data['databg']['INS_BRANCH_EMAIL']) {

                                $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_3D');
                                $sendEmail($data['databg']['INS_BRANCH_EMAIL'], strval('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
                            }
                        }
                    }
                } else {
                    // SEND EMAIL TO PRINCIPAL
                    $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_2A');
                    if ($data['databg']['CUST_EMAIL_BG']) $sendEmail($data['databg']['CUST_EMAIL_BG'], strval('Pemberitahuan Proses Penyelesaian BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

                    // SEND EMAIL TO KANTOR CABANG PENERBIT
                    $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_2C');
                    if ($data['databg']['BRANCH_EMAIL']) $sendEmail($data['databg']['BRANCH_EMAIL'], strval('Pemberitahuan Proses Penyelesaian BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

                    // SEND EMAIL TO GROUP CASH COL / NON CASH COL

                    if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 1) {
                        // FULL COVER
                        $getGroupFC = $Settings->getSettingNew('email_group_cashcoll');
                        $getGroupFC = explode(',', $getGroupFC);
                        $getGroupFC = array_map('trim', $getGroupFC);
                        if ($getGroupFC) {
                            foreach ($getGroupFC as $emailGroup) {
                                $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_2C');
                                if ($emailGroup) $sendEmail($emailGroup, strval('Pemberitahuan Proses Penyelesaian BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
                            }
                        }
                    }

                    if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 2) {
                        // LINE FACILITY
                        $getGroupFC = $Settings->getSettingNew('email_group_linefacility');
                        $getGroupFC = explode(',', $getGroupFC);
                        $getGroupFC = array_map('trim', $getGroupFC);
                        if ($getGroupFC) {
                            foreach ($getGroupFC as $emailGroup) {
                                $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_2C');
                                if ($emailGroup) $sendEmail($emailGroup, strval('Pemberitahuan Proses Penyelesaian BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
                            }
                        }
                    }

                    if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 3) {
                        // LINE FACILITY
                        $getGroupFC = $Settings->getSettingNew('email_group_insurance');
                        $getGroupFC = explode(',', $getGroupFC);
                        $getGroupFC = array_map('trim', $getGroupFC);
                        if ($getGroupFC) {
                            foreach ($getGroupFC as $emailGroup) {
                                $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_2C');
                                if ($emailGroup) $sendEmail($emailGroup, strval('Pemberitahuan Proses Penyelesaian BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
                            }
                        }
                    }

                    // SEND EMAIL TO KANTOR CABANG PENERBIT
                    $getInsBranch = $db->select()
                        ->from('T_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE'])
                        ->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
                        ->where('LOWER(PS_FIELDNAME) = ?', 'insurance branch')
                        ->query()->fetch();

                    $getInsBranchEmail = $db->select()
                        ->from('M_INS_BRANCH')
                        ->where('INS_BRANCH_CODE = ?', $getInsBranch['PS_FIELDVALUE'] ?: 'XXX')
                        ->query()->fetch();

                    $getEmailTemplate = $prepareForSendEmail($data, 'bemailtemplate_2B');
                    if ($getInsBranchEmail['INS_BRANCH_EMAIL'] && $bgdatasplit) $sendEmail($getInsBranchEmail['INS_BRANCH_EMAIL'], strval('Pemberitahuan Proses Penyelesaian BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
                }
            }
        } else {
            // ADA TRANSAKSI YANG GAGAL
            $db->update('T_BG_PSLIP', [
                'PS_STATUS' => '4'
            ], [
                'CLOSE_REF_NUMBER = ?' => $closeRefNumber
            ]);
        }
    } else {
        $db->update('T_BG_PSLIP', [
            'PS_STATUS' => '5',
            'PS_DONE' => new Zend_Db_Expr('now()')
        ], [ 
            'CLOSE_REF_NUMBER = ?' => $closeRefNumber
        ]);
    }
};

$queueConsumer = $queueService->getQueueByProfileName("RUN_TRANSACTION");

try {
    $queueConsumer->waitAndProcess($callback);
} catch (Exception $e) {
    var_dump($e->getMessage());
    $param = [];
    Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e->getMessage(), NULL, FALSE), $param);
    SGO_Helper_Supervisor::doExitByExitCodeName("RESTART");
}
