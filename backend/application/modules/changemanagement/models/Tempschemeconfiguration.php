<?php
/**
 * Tempuser model
 * 
 * @author Suhandi
 * @version
 */

class Changemanagement_Model_Tempschemeconfiguration extends Changemanagement_Model_Tempchanges 
{

	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	private function generateSchemeCode()
	{
		$setting = new Settings();
		$counter = $setting->getSetting("scheme_sequence");

		$countSequence = ($counter < 1) ? 1 : $counter+1;
			
		if ($countSequence > 999)	//MAX SEQUENCE = 999
		{	$countSequence = 1;		}	
			
		// update the counter + 1
		$setting->setSetting("scheme_sequence", $countSequence);
		
		$seqNumber = str_pad($countSequence, ((strlen($countSequence) == 1) ? 3 : ((3+strlen($countSequence)) - strlen($countSequence))), "0", STR_PAD_LEFT);	
		$currentDate = date("Ymd");
		//$checkDigit  = strtoupper(substr(MD5($currentDate.$seqNumber),-3));
		$checkDigit  = strtoupper(substr(preg_replace("/[^a-zA-Z0-9]/", "", uniqid()),-3));
		$schemeCode   = 'S'.$currentDate.$seqNumber.$checkDigit;
	
		return $schemeCode;
	}
	
	public function approveNew($actor = null){
		
		$scheme_Model_Scheme = new scheme_Model_Scheme();
		//query from TEMP_SCHEME
		$scheme = $scheme_Model_Scheme->getRawTempScheme($this->_changeId);
		
		if(!count($scheme)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(scheme)';
			return false;
        }
		
		//GET FROM TEMP TABLE
		$docSetting 								= $scheme_Model_Scheme->getRawTempDocSetting($this->_changeId);
		$disbWorkflow 						= $scheme_Model_Scheme->getRawTempDisbWorkflow($this->_changeId);
		$Installment								= $scheme_Model_Scheme->getTempInstallment($this->_changeId);		
		$docChecklist							= $scheme_Model_Scheme->getRawTempDocCheckList($this->_changeId);		
		$Charges									= $scheme_Model_Scheme->getRawTempCharges($this->_changeId);		
		
		//--------------------------------INSERT TO MASTER TABLE----------------------------------------------------------
		$created 			= array('CREATED' => $this->_changesInfo['CREATED'],
												  'CREATEDBY' => $this->_changesInfo['CREATED_BY']);
		$updated 			= array('UPDATED' => new Zend_Db_Expr('now()'),
												  'UPDATEDBY' => $actor);
		$suggested 		= array('SUGGESTED' =>  $this->_changesInfo['CREATED'],
												  'SUGGESTEDBY' => $this->_changesInfo['CREATED_BY']);
		$status 				= array('SCHEME_STATUS' =>  1);
		
		$scheme 				= array_diff_key($scheme,array('TEMP_ID'=>'','CHANGES_ID'=>''));	
		//GENERATE CODE
		$schemeCode 		= $this->generateSchemeCode();

			//INSERTING
			$scheme					  = array_merge($scheme,array('SCHEME_CODE'=>$schemeCode));
			$scheme					  = array_merge($scheme,$created);
			$scheme					  = array_merge($scheme,$updated);
			$scheme					  = array_merge($scheme,$suggested);
			$scheme					  = array_merge($scheme,$status);
			$schemeIns				  = $this->dbObj->insert('M_SCHEME',$scheme);

			if(!(boolean)$schemeIns) 
			{
				$this->_errorCode = '82';
				$this->_errorMsg = 'Query failed(scheme)';
				return false;
			}	
			foreach($docSetting as $row)
			{
				$docSettingIns 		= 0;
				$row 							= array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));		
				$row							= array_merge($row,array('CODE'=>$schemeCode));
				$docSettingIns 		= $this->dbObj->insert('M_DOC_SETTING',$row);
				if(!(boolean)$docSettingIns) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(docSetting)';
					return false;
				}	
			}
			
			foreach($disbWorkflow as $row)
			{
				$disbWorkflowIns		 = 0;
				$row							 = array_merge($row,array('CODE'=>$schemeCode));
				$row 					         = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$disbWorkflowIns 	 = $this->dbObj->insert('M_DISBURSEMENT_WORKFLOW',$row);
				if(!(boolean)$disbWorkflowIns) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(disbWorkflow)';
					return false;
				}	
			}
			
			if($Installment){
				$Installment 			= array_diff_key($Installment,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$Installment			= array_merge($Installment,array('CODE'=>$schemeCode));		

				$installmentInfoIns    = $this->dbObj->insert('M_INSTALLMENT',$Installment);
				if(!(boolean)$installmentInfoIns) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(installmentInfo)';
					return false;
				}
			}
			if($docChecklist)
			{
				foreach($docChecklist as $row)
				{					
					$docChecklistIns = 0;
					$row 								= array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));					
					$documentCheckList		= array(
																		'CODE'=>$schemeCode,
																		'TYPE '=> '1',
																		'DOC_ID'=>$row['DOC_ID'],																		
																		);																		
					$docChecklistIns		   		= $this->dbObj->insert('M_DOC_CHECKLIST',$documentCheckList);
					
					if(!(boolean)$docChecklistIns) 
					{
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query failed(disbInfo)';
						return false;
					}	
				}	
			}

			foreach($Charges as $row)
			{				
				$ChargestIns = 0;
				$row			 		= array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$row					= array_merge($row,array('CODE'=>$schemeCode));
				$ChargestIns	= $this->dbObj->insert('M_CHARGES_DETAIL',$row);
				if(!(boolean)$ChargestIns) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Chargest)';
					return false;
				}	
			}
		
		//-------------------------------END INSERT TO TABLE MASTER-------------------------------------------------------
		
		$deleteChanges = $this->deleteNew();
		if(!$deleteChanges)
			return false;
			
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {

		$scheme_Model_Scheme = new scheme_Model_Scheme();
		//query from TEMP_SCHEME
		$scheme = $scheme_Model_Scheme->getRawTempScheme($this->_changeId);

		$schemeCode = $scheme['SCHEME_CODE'];
		if(!count($scheme)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(scheme)';
			return false;
        }
		
		//GET FROM TEMP TABLE
		$docSetting 								= $scheme_Model_Scheme->getRawTempDocSetting($this->_changeId);
		$disbWorkflow 							= $scheme_Model_Scheme->getRawTempDisbWorkflow($this->_changeId);
		$Installment								= $scheme_Model_Scheme->getTempInstallment($this->_changeId);		
		$docChecklist							= $scheme_Model_Scheme->getRawTempDocCheckList($this->_changeId);		
		$Charges									= $scheme_Model_Scheme->getRawTempCharges($this->_changeId);		
		
		//-------------------------------- INSERT TO MASTER TABLE ----------------------------------------------------------
		$updated 			= array('UPDATED' => new Zend_Db_Expr('now()'),
												  'UPDATEDBY' => $actor);
		$suggested 		= array('SUGGESTED' =>  $this->_changesInfo['CREATED'],
												  'SUGGESTEDBY' => $this->_changesInfo['CREATED_BY']);
		$status 				= array('SCHEME_STATUS' =>  1);
		
		$scheme 				= array_diff_key($scheme,array('TEMP_ID'=>'','CHANGES_ID'=>''));	
			
			//INSERTING
			$whereScheme		  = "SCHEME_CODE =  '".$schemeCode."'";
			$scheme					  = array_merge($scheme,$updated);
			$scheme					  = array_merge($scheme,$suggested);
			$scheme					  = array_merge($scheme,$status);
			$schemeIns				  = $this->dbObj->update('M_SCHEME',$scheme,$whereScheme);

			if(!(boolean)$schemeIns) 
			{
				$this->_errorCode = '82';
				$this->_errorMsg = 'Query failed(scheme)';
				return false;
			}	
			
			// $select = $this->dbObj->fetchall($this->dbObj->select()->from('M_DOC_SETTING')->where('CODE'=>$schemeCode));
			$arrCode = array('CODE'=>$schemeCode);
			$whereCode		  = "CODE =  '".$schemeCode."'";
			$this->dbObj->delete('M_DOC_SETTING',$whereCode);
			foreach($docSetting as $row)
			{
				$docSettingIns 		= 0;
				$row 							= array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));		
				$row							= array_merge($row,$arrCode);				
				$docSettingIns 		= $this->dbObj->insert('M_DOC_SETTING',$row);
				if(!(boolean)$docSettingIns) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(docSetting)';
					return false;
				}	
			}
			
			$this->dbObj->delete('M_DISBURSEMENT_WORKFLOW',$whereCode);
			foreach($disbWorkflow as $row)
			{
				$disbWorkflowIns		 = 0;
				$row 					         = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$row							 = array_merge($row,$arrCode);
				$disbWorkflowIns 	 = $this->dbObj->insert('M_DISBURSEMENT_WORKFLOW',$row);
				if(!(boolean)$disbWorkflowIns) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(disbWorkflow)';
					return false;
				}	
			}
			
			$this->dbObj->delete('M_INSTALLMENT',$whereCode);
			if($Installment){
				$Installment 			= array_diff_key($Installment,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$Installment			= array_merge($Installment,array('CODE'=>$schemeCode));		

				$installmentInfoIns    = $this->dbObj->insert('M_INSTALLMENT',$Installment);
				if(!(boolean)$installmentInfoIns) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(installmentInfo)';
					return false;
				}
			}

			$this->dbObj->delete('M_DOC_CHECKLIST',$whereCode);
			if($docChecklist)
			{
				foreach($docChecklist as $row)
				{
					$docChecklistIns = 0;
					$documentCheckList		= array(
																		'CODE'=>$schemeCode,
																		'TYPE '=> '1',
																		'DOC_ID'=>$row['DOC_ID'],																		
																		);
					$docChecklistIns		   		= $this->dbObj->insert('M_DOC_CHECKLIST',$documentCheckList);
					if(!(boolean)$docChecklistIns) 
					{
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query failed(disbInfo)';
						return false;
					}	
				}	
			}

			$this->dbObj->delete('M_CHARGES_DETAIL',$whereCode);
			foreach($Charges as $row)
			{				
				$ChargestIns = 0;
				$row			 		= array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$row					= array_merge($row,$arrCode);
				$ChargestIns	= $this->dbObj->insert('M_CHARGES_DETAIL',$row);
				if(!(boolean)$ChargestIns) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Chargest)';
					return false;
				}	
			}
		
		//------------------------------- END INSERT TO TABLE MASTER -------------------------------------------------------
		
		$deleteChanges  = $this->deleteEdit();
			if(!$deleteChanges)return false;

		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) 
	{		
		// UPDATE STATUS TO DELETE//
		$statusDelete = null;
		$updateStatusDelete = array(
												'SCHEME_STATUS' => 3,
											  );
		$where		  		= "SCHEME_NAME =  '".$this->_changesInfo['KEY_VALUE']."'";
		$statusDelete	= $this->dbObj->update('M_SCHEME',$updateStatusDelete,$where);
		if(!$statusDelete) return false;
		return true;
	}
	
    /**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

		//delete from TEMP_SCHEME		
		$TEMP_SCHEME = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DOC_SETTING		
		$TEMP_DOC_SETTING = $this->dbObj->delete('TEMP_DOC_SETTING',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DISBURSEMENT_WORKFLOW		
		$TEMP_DISBURSEMENT_WORKFLOW = $this->dbObj->delete('TEMP_DISBURSEMENT_WORKFLOW',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DOC_CHECKLIST		
		$TEMP_DOC_CHECKLIST = $this->dbObj->delete('TEMP_DOC_CHECKLIST',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_INSTALLMENT		
		$TEMP_INSTALLMENT = $this->dbObj->delete('TEMP_INSTALLMENT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_CHARGES_DETAIL		
		$TEMP_CHARGES_DETAIL = $this->dbObj->delete('TEMP_CHARGES_DETAIL',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_SCHEME		
		$TEMP_SCHEME = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DOC_SETTING		
		$TEMP_DOC_SETTING = $this->dbObj->delete('TEMP_DOC_SETTING',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DISBURSEMENT_WORKFLOW		
		$TEMP_DISBURSEMENT_WORKFLOW = $this->dbObj->delete('TEMP_DISBURSEMENT_WORKFLOW',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DOC_CHECKLIST		
		$TEMP_DOC_CHECKLIST = $this->dbObj->delete('TEMP_DOC_CHECKLIST',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_INSTALLMENT		
		$TEMP_INSTALLMENT = $this->dbObj->delete('TEMP_INSTALLMENT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_CHARGES_DETAIL		
		$TEMP_CHARGES_DETAIL = $this->dbObj->delete('TEMP_CHARGES_DETAIL',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() 
	{
		//delete from TEMP_SCHEME		
		$TEMP_SCHEME = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DOC_SETTING		
		$TEMP_DOC_SETTING = $this->dbObj->delete('TEMP_DOC_SETTING',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DISBURSEMENT_WORKFLOW		
		$TEMP_DISBURSEMENT_WORKFLOW = $this->dbObj->delete('TEMP_DISBURSEMENT_WORKFLOW',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_DOC_CHECKLIST		
		$TEMP_DOC_CHECKLIST = $this->dbObj->delete('TEMP_DOC_CHECKLIST',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_INSTALLMENT		
		$TEMP_INSTALLMENT = $this->dbObj->delete('TEMP_INSTALLMENT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		//delete from TEMP_CHARGES_DETAIL		
		$TEMP_CHARGES_DETAIL = $this->dbObj->delete('TEMP_CHARGES_DETAIL',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
}