<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'Crypt/AESMYSQL.php';

class transactionreport_ReconciliationreportController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 

		$this->_helper->layout()->setLayout('newlayout');


		try {
			$conf 		= Zend_Registry::get('config');
			// 		$host 		= $conf['db2']['params']['host'];
			// 		$username	= $conf['db2']['params']['username'];
			// 		$pass		= $conf['db2']['params']['password'];
			// 		$db			= $conf['db2']['params']['dbname'];
			// 		$port		= $conf['db2']['params']['port'];

			//stag config
			$db = Zend_Db_Table::getDefaultAdapter();
			$config = Zend_Registry::get('config');

			//protected $_db;
			// $config = array(
			// 		'host'      => '192.168.88.6',
			// 		'username'  => 'root',
			// 		'password'  => '590dbpad4ng!',
			// 		'dbname'    => 'sgolivebankdev',
			// 		'port'  => '3306'
			// );
			$config = array(
							       'host'      => '192.168.86.23',
							       'username'  => 'ibsgolive',
							       'password'  => 'F9t%<HCj3L9tPZf?mv)]C#K',
							       'dbname'    => 'sgolivebankdev',
							       'port'  => '3306'
							     );

			$dbCon = new Zend_Db_Adapter_Pdo_Mysql($config);

			// 		$key = $conf['enc']['dec']['key'];
			// 		$iv = $conf['enc']['dec']['iv'];

			// 		$usernameval = Application_Helper_General::decrypt($key,$iv,$username);
			// 		$passval = Application_Helper_General::decrypt($key,$iv,$pass);

			// 		$config = array(
			// 				'host'      	=> $host,
			// 				'username'  	=> $usernameval,
			// 				'password'  	=> $passval,
			// 				'dbname'    	=> $db,
			// 				'port'  		=> $port
			// 		);

			// 		$dbCon = new Zend_Db_Adapter_Pdo_Mysql($config);
		}
		catch(Zend_Db_Exception $e) {
			echo $e->getMessage();
		}

		$arrPayType 	= Application_Helper_General::filterPaymentTypeRekon($this->_paymenttype, $this->_transfertype);
		$arrTraType 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		$arrTraStatus	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		//Zend_Debug::dump($arrPayType);


		unset($arrPayType["6,4"]);
		unset($arrPayType["7,5"]);
		unset($arrPayType['8']);
		unset($arrPayType['1']);
		unset($arrPayType['2']);
//
		unset ($arrTraStatus['2']);
		unset ($arrTraStatus['0']);

		// 6,4 THEN 'Multi Credit'
		// 7,5 THEN 'Multi Debet'
		$paytypeall		= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		foreach($paytypeall as $key => $value){

			if($key != 3 && $key != 8 && $key != 9 && $key != 10){

				if($key == 4
				// OR $key == 6
				) {
					$value = $paytypeall[6];
					$optpaytypeAll[$key] = $value;
				}
				else if($key == 5
				// OR $key == 7
				) {
					$value = $paytypeall[7];
					$optpaytypeAll[$key] = $value;
				}
				else $optpaytypeAll[$key] = $value;
			}

		}


		foreach($arrPayType as $key => $value){
			if($key != 3){
				$optpaytypeRaw[$key] = $value;
			}
		}

		foreach($arrTraType as $key => $value){
			if($key <= 2 ){
				$arrTraTypeRaw[$key] = $value;
			}
		}

		/*
			ChargedStatus :
				Not Charged if tra_status = '4' -> (failed)
				Charged if tra_status = '3' -> (success)
		*/

		$arrChargedStatus = array('0'=>'Not Charged','1'=>'Charged');

		$listId = $this->_db->select()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID','CUST_NAME'))
						->order('CUST_ID ASC')
						->query()->fetchAll();

       	$list = array(""=>'-- '.$this->language->_('Please Select'). " --");
		$list += Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');

		$this->view->arrPayType 		= $optpaytypeRaw;
		$this->view->arrTraType 		= $arrTraTypeRaw;
		$this->view->arrTraStatus 		= $arrTraStatus;
		$this->view->arrChargedStatus 	= $arrChargedStatus;
		$this->view->listCustId 		= $list;



		$fields = array	(
							'companycode'  		=> array(
															'field' => 'companycode',
															'label' => $this->language->_('Company'),
															'sortable' => true
														),
							/*'companyname'  		=> array(
															'field' => 'companyname',
															'label' => $this->language->_('Company Name'),
															'sortable' => true
														),*/
							'payref'  			=> array(
															'field' => 'payref',
															'label' => $this->language->_('Payment Ref#').' / <br>'.$this->language->_('Reference No'),
															'sortable' => true
														),
							/*'referenceno'  			=> array(
															'field' => 'referenceno',
															'label' => $this->language->_('Reference No'),
															'sortable' => true
														),*/
							'transferdate'  	=> array(
															'field' => 'transferdate',
															'label' => $this->language->_('Payment Date'),
															'sortable' => true
														),
							'paymenttype'  		=> array(
															'field' => 'paymenttype',
															'label' => $this->language->_('Payment Type'),
															'sortable' => true
														),
							'serviceCat'  		=> array(
																'field' => 'SERVICE_NAME',
																'label' => $this->language->_('Service Category'),
																'sortable' => true
														),
							'providerName'  		=> array(
															'field' => 'PROVIDER_NAME',//'PS_BILLER_ID',
															'label' => $this->language->_('Biller Name'),
															'sortable' => true
														),
							'orderId'  			=> array(
															'field' => 'BILLER_ORDER_ID',
															'label' => $this->language->_('Order Id'),
															'sortable' => true
														),
							'uuid'  	=> array(
															'field' => 'uuid',
															'label' => $this->language->_('UUID'),
															'sortable' => true
														),
							'traamount'  		=> array(
																'field' => 'traamount',
																'label' => $this->language->_('Amount'),
																'sortable' => true
														),
							/*'action'  		=> array(
																'field' => '',
																'label' => $this->language->_('Action'),
																'sortable' => false
														),*/
						);

		$filterlist = array('PS_TRFDATE','COMP_ID','PAYTYPE','SERV_CAT','COMP_NAME','PAY_REF','REF_NUM','BILLER_NAME');
		
		$this->view->filterlist = $filterlist;

		//validasi page, jika input page bukan angka
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');

		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;

		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;

		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		if($sortBy=='companycode'){
			$sortBy = 'referenceno';
		}

		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';

// 		print_r($sortBy);
// 		print_r($sortDir);
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$filterArr = array('COMP_ID'    	=> array('StripTags'),
	                       'COMP_NAME'  		=> array('StripTags','StringTrim','StringToUpper'),

						   'PAY_REF' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'TRANSACTIONID' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'REF_NUM' 		=> array('StripTags','StringTrim'),
						   'PS_TRFDATE' 		=> array('StripTags','StringTrim'),
						   'PS_TRFDATE_END' 		=> array('StripTags','StringTrim'),
						   'PAY_REF' 			=> array('StripTags','StringTrim'),
						   'REF_NUM' 			=> array('StripTags','StringTrim'),
						   'BILLER_NAME' 			=> array('StripTags','StringTrim','StringToUpper'),

						   'UPDATEDFROM' 		=> array('StripTags','StringTrim'),
						   'UPDATEDTO' 			=> array('StripTags','StringTrim'),

						   'CREATEDFROM' 		=> array('StripTags','StringTrim'),
						   'CREATEDTO' 			=> array('StripTags','StringTrim'),

						   'ACCTSRC' 			=> array('StripTags','StringTrim','StringToUpper'),
						   'BENEACCT' 			=> array('StripTags','StringTrim','StringToUpper'),
						   'SERV_CAT' 	=> array('StripTags'),
						   'PAYTYPE' 			=> array('StripTags'),
						   'TRANSFERTYPE' 		=> array('StripTags'),
						   'UUID' 				=> array('StripTags'),
						   'TRANSSTATUS' 		=> array('StripTags'),
						   'CHARGEDSTATUS' 		=> array('StripTags'),
	                      );

		// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','COMP_NAME','PAY_REF','SERV_CAT','PAYTYPE','REF_NUM','BILLER_NAME');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_TRFDATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('trfdate'))){
				$transferarr = $this->_request->getParam('trfdate');
					$dataParamValue['PS_TRFDATE'] = $transferarr[0];
					$dataParamValue['PS_TRFDATE_END'] = $transferarr[1];
			}

		// The default is set so all fields allow an empty string
		$options = array('allowEmpty' => true);
		$validators = array(

						'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($list)))),
						'COMP_NAME' 	=> array(),

						'PAY_REF' 	=> array(),
						'REF_NUM' 	=> array(),
						'TRANSACTIONID' => array(),
						'PAY_REF' 			=> array(),
						'REF_NUM' 			=> array(),
						'BILLER_NAME' 			=> array(),

						'PS_TRFDATE' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_TRFDATE_END'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),

						'UPDATEDFROM' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'UPDATEDTO'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),

						'CREATEDFROM' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'CREATEDTO'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),

						'ACCTSRC' 		=> array(),
						'BENEACCT' 		=> array(),
						'SERV_CAT'=> array(),

						'PAYTYPE' 		=> array(array('InArray', array('haystack' => array_keys($optpaytypeRaw)))),
						'TRANSFERTYPE' 	=> array(array('InArray', array('haystack' => array_keys($arrTraTypeRaw)))),
						'UUID' 			=> array(),
						'TRANSSTATUS' 	=> array(array('InArray', array('haystack' => array_keys($arrTraStatus)))),

						'CHARGEDSTATUS' => array(array('InArray', array('haystack' => array_keys($arrChargedStatus)))),

						);

		$zf_filter  		= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

		$billerCharges = new systemlog_Model_Transactionreport();
		$serviceTypeArr 	 = $billerCharges->getServiceType();
		$this->view->serviceTypeArr = $serviceTypeArr;



		foreach($arrPayType as $key => $value){
			if($key != 3){
				$optpaytypeRaw[$key] = $this->language->_($value);
			}
		}

		$this->view->arrPayType 	= $optpaytypeRaw;

		$fCOMPANYCODE 		= html_entity_decode($zf_filter->getEscaped('COMP_ID'));
		$fCOMPANYNAME 		= html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
		$fPAYMENTREF 		= html_entity_decode($zf_filter->getEscaped('PAY_REF'));
		$fREFERENCENO 		= html_entity_decode($zf_filter->getEscaped('REF_NUM'));

		$fTRANSACTIONID		= html_entity_decode($zf_filter->getEscaped('TRANSACTIONID'));

		$fpayref 		= html_entity_decode($zf_filter->getEscaped('PAY_REF'));
		$freferenceno 		= html_entity_decode($zf_filter->getEscaped('REF_NUM'));
		$fproviderName 		= html_entity_decode($zf_filter->getEscaped('BILLER_NAME'));

		$fTRANSFERFROM 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE'));
		$fTRANSFERTO 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE_END'));

		$fACCTSRC 			= html_entity_decode($zf_filter->getEscaped('ACCTSRC'));
		$fBENEACCT 			= html_entity_decode($zf_filter->getEscaped('BENEACCT'));

		$fPAYTYPE 			= html_entity_decode($zf_filter->getEscaped('PAYTYPE'));
		$fTRANSFERTYPE 		= html_entity_decode($zf_filter->getEscaped('TRANSFERTYPE'));
		$fUUID 		= html_entity_decode($zf_filter->getEscaped('UUID'));
		$fTRANSSTATUS 		= html_entity_decode($zf_filter->getEscaped('TRANSSTATUS'));
		$fCHARGEDSTATUS 	= html_entity_decode($zf_filter->getEscaped('CHARGEDSTATUS'));

		$fSERVICECATEGORY = html_entity_decode($zf_filter->getEscaped('SERV_CAT'));


		if($filter == NULL && $clearfilter != 1){
			$fTRANSFERFROM 	= date("d/m/Y");
			$fTRANSFERTO 	= date("d/m/Y");
		}
		else{

			if($filter != NULL){
				$fTRANSFERFROM 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE'));
				$fTRANSFERTO 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE_END'));
			}
			else if($clearfilter == 1){
				$fTRANSFERFROM 	= "";
				$fTRANSFERTO 	= "";
			}
		}

		/*
			CONCAT(sb_pslip.acctsrc,' (',sb_pslip.accsrc_ccy,')',' / ', sb_pslip.accsrc_bankname,' / ',sb_pslip.accsrc_alias) AS fullaccfrom,
			CONCAT(sb_transaction.tra_accttgt,' (', sb_transaction.tra_accttgtcurr,')',' / ', sb_transaction.tra_accttgtbankname) AS fullaccto,

			IF (tra_alreadycharged=1,'Y','N') AS tra_statuscharged,

		*/

		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($optpaytypeAll as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";

		$caseTraType = "(CASE T.TRANSFER_TYPE ";
  		foreach($arrTraTypeRaw as $key => $val)	{ $caseTraType .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraType .= " END)";

		$caseTraStatus = "(CASE T.TRA_STATUS ";
  		foreach($arrTraStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$select = $this->_db->select()
							->from(		array(	'T'=>'T_TRANSACTION'),array())
							->joinLeft(	array(	'P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',
										array(


												'companycode'	=>'C.CUST_ID',
												'companyname'	=>'C.CUST_NAME',
												'payref'		=>'P.PS_NUMBER',
												'referenceno' =>'T.TRX_ID',
												'transferdate'	=>'P.PS_EFDATE',
												'paymenttype'	=>$casePayType,
												'ccy'			=>'P.PS_CCY',




												'T.BILLER_ORDER_ID',
												'traamount'		=>'T.TRA_AMOUNT',
												'P.PS_BILLER_ID',

												'uuid'			=>'P.UUID',


												))
							->joinLeft(array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())

							->joinLeft (array('A' => 'M_SERVICE_PROVIDER'), 'A.PROVIDER_ID = P.PS_BILLER_ID', array('A.PROVIDER_ID','A.SERVICE_TYPE'))
							->joinLeft (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array('B.SERVICE_NAME','A.PROVIDER_NAME'))
							->where("UPPER(P.PS_TYPE) IN (16,17)")
							->where(" T.TRA_STATUS = 3");

		if($fpayref)	{ $select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fpayref.'%')); }
		if($freferenceno)	{ $select->where("UPPER(T.TRX_ID) LIKE ".$this->_db->quote('%'.$freferenceno.'%')); }



// 		if($fproviderName)	{ $select->where("UPPER(C.CUST_NAME) LIKE ".$this->_db->quote('%'.$fproviderName.'%')); }

		if($fCOMPANYCODE)	{ $select->where("UPPER(C.CUST_ID) = ".$this->_db->quote($fCOMPANYCODE)); }
		if($fCOMPANYNAME)	{ $select->where("UPPER(C.CUST_NAME) LIKE ".$this->_db->quote('%'.$fCOMPANYNAME.'%')); }
		if($fPAYMENTREF) 	{ $select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPAYMENTREF.'%')); }
		if($fTRANSACTIONID) { $select->where("UPPER(T.TRANSACTION_ID) LIKE ".$this->_db->quote('%'.$fTRANSACTIONID.'%')); }

		if($fTRANSFERFROM){
			$FormatDate 	= new Zend_Date($fTRANSFERFROM, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) >= ?', $transferfrom);
		}
		if($fTRANSFERTO){
			$FormatDate 	= new Zend_Date($fTRANSFERTO, $this->_dateDisplayFormat);
			$transferto  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) <= ?', $transferto);
		}

		if($fCREATEDFROM){
			$FormatDate 	= new Zend_Date($fCREATEDFROM, $this->_dateDisplayFormat);
			$createdfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_CREATED) >= ?', $createdfrom);
		}
		if($fCREATEDTO){
			$FormatDate 	= new Zend_Date($fCREATEDTO, $this->_dateDisplayFormat);
			$createdto  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_CREATED) <= ?', $createdto);
		}

		if($fUPDATEDFROM){
			$FormatDate 	= new Zend_Date($fUPDATEDFROM, $this->_dateDisplayFormat);
			$updatedfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_UPDATED) >= ?', $updatedfrom);
		}
		if($fUPDATEDTO){
			$FormatDate 	= new Zend_Date($fUPDATEDTO, $this->_dateDisplayFormat);
			$updatedto  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_UPDATED) <= ?', $updatedto);
		}

		if($fACCTSRC)	{ $select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fACCTSRC.'%')); }
		if($fBENEACCT)	{ $select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBENEACCT.'%')); }

		if($fPAYTYPE){
			$fPayType 	 	= explode(",", $fPAYTYPE);
			$select->where("P.PS_TYPE in (?) ", $fPayType);
		}

		if($fTRANSFERTYPE != null)
		{
			$select->where("T.TRANSFER_TYPE = ".$this->_db->quote($fTRANSFERTYPE));
		}
		else if($this->_getParam('TRANSFERTYPE') != null)
		{
			if($this->_getParam('TRANSFERTYPE') == 0)
			{
				$fTRANSFERTYPE = 0;
				$select->where("T.TRANSFER_TYPE = ".$this->_db->quote($fTRANSFERTYPE));
			}
		}

		if($fTRANSSTATUS){
			$select->where("T.TRA_STATUS = ".$this->_db->quote($fTRANSSTATUS));
		}
		if($fCHARGEDSTATUS!=''){
			$select->where("T.TRANSFER_FEE_STATUS = ".$this->_db->quote($fCHARGEDSTATUS));
		}

		if($fproviderName){
			$select->where("A.PROVIDER_NAME LIKE ".$this->_db->quote('%'.$fproviderName.'%'));
		}

		if($fSERVICECATEGORY)
		{
			$select->where("A.SERVICE_TYPE = (?)", $fSERVICECATEGORY);
		}

		$select->order($sortBy.' '.$sortDir);

		$dataSQL 	= $this->_db->fetchAll($select);

		$privi = $this->_priviId;

		foreach ($privi as $val){
			if($val == 'URRP'){
				$this->view->changestatus = false;
			}
		}

		if(!empty($dataSQL)){
		foreach($dataSQL as $key =>$dt)
			{
				$biller = (!empty($dt["PS_BILLER_ID"])?$dt["PS_BILLER_ID"]:'');

				$select2 = $this->_db->select()
				->FROM(array('A' => 'M_SERVICE_PROVIDER'),
						array('A.PROVIDER_ID','A.PROVIDER_NAME','A.SERVICE_TYPE'))
						->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array('B.SERVICE_NAME'));
				//						->WHERE('A.PROVIDER_ID = ? ', $provId);
				$select2->where('A.PROVIDER_ID = ? ', $biller);

				$resultTes = $this->_db->fetchAll($select2);
// 				$resultTes = $getPayment->getProvider($biller);
				unset($dataSQL[$key]['PS_BILLER_ID']);
				$dataSQL[$key]['PS_BILLER_ID'] = $resultTes[0]["SERVICE_NAME"];
				$dataSQL[$key]['PROVIDER_NAME'] = $resultTes[0]["PROVIDER_NAME"];




				$sql_d2 = 'select * FROM L_SERVICECALL WHERE UUID = "'.$dt["uuid"].'" ';
				$result_d2 = $dbCon->fetchAll($sql_d2);
// 				var_dump($result_d2);
				unset($dataSQL[$key]['uuid']);
				if(!empty($result_d2['0'])){
					$sql_sequence = 'select * FROM L_SERVICECALL WHERE  ORDER_NO = 2 AND SEQUENCE_ID = "'.$result_d2['0']['SEQUENCE_ID'].'" ';
					$result_sequence = $dbCon->fetchAll($sql_sequence);
					$dataSQL[$key]['uuid'] = $result_sequence["0"]["UUID"];
				}else{
					$dataSQL[$key]['uuid'] = '';
				}

				$totalnumbertx = $totalnumbertx + $dt["traamount"];
				$totalvaluetx  = $totalvaluetx + $dt["TRANSFER_FEE"];

			}
		}

// 		echo "<pre>";
// 				print_r($dataSQL);die;


		$dataCount 	= count($dataSQL); // utk validasi max row csv

		if($csv || $pdf || $this->_request->getParam('print')){
			$tempfields = $fields;
			unset($tempfields["action"]);
			$header  = Application_Helper_Array::simpleArray($tempfields, "label");

			$select->order($sortBy.' '.$sortDir);
// 			echo "<pre>";
// 			print_r($sortBy.' '.$sortDir);
			$dataSQL 	= $this->_db->fetchAll($select);
// 			print_r($dataSQL);die;
			if(is_array($dataSQL) && count($dataSQL) > 0){
				foreach($dataSQL as $key => $val){
// 					echo "<pre>";
// 					print_r($val);die;
//

					// 				$resultTes = $getPayment->getProvider($biller);
					$order_id = $dataSQL[$key]['BILLER_ORDER_ID'];
					$amount = $dataSQL[$key]['traamount'];


					unset($dataSQL[$key]["SERVICE_TYPE"]);
					unset($dataSQL[$key]["PS_BILLER_ID"]);
					unset($dataSQL[$key]["PROVIDER_ID"]);
					unset($dataSQL[$key]["traamount"]);
					unset($dataSQL[$key]['BILLER_ORDER_ID']);
					unset($dataSQL[$key]['PS_BILLER_ID']);

// 					$dataSQL[$key]['SERVICE_NAME'] = $resultTes[0]["SERVICE_NAME"];
// 					$dataSQL[$key]['PROVIDER_NAME'] = $resultTes[0]["PROVIDER_NAME"];




					unset($dataSQL[$key]['UUID_REAL']);
					unset($dataSQL[$key]['BILLER_ORDER_ID']);


					$dataSQL[$key]['BILLER_ORDER_ID'] = $order_id;
					$sql_d2 = 'select * FROM L_SERVICECALL WHERE UUID = "'.$val["uuid"].'" ';
					$result_d2 = $dbCon->fetchAll($sql_d2);
					unset($dataSQL[$key]['uuid']);
					if(!empty($result_d2['0'])){
						$sql_sequence = 'select * FROM L_SERVICECALL WHERE  ORDER_NO = 2 AND SEQUENCE_ID = "'.$result_d2['0']['SEQUENCE_ID'].'" ';
						$result_sequence = $dbCon->fetchAll($sql_sequence);
						$dataSQL[$key]['uuid'] = $result_sequence["0"]["UUID"];
					}else{
						$dataSQL[$key]['uuid'] = '';
					}

					$dataSQL[$key]['traamount'] = $amount;


					unset($dataSQL[$key]["benebankname"]);

					$key_index = $key;
				}


			}
			else  $dataSQL = array();


			if($csv)
			{
// 				echo "<pre>";
// 				print_r($dataSQL);
// 				die;
				$dataSQL[$key+1]['number']	= false;
				$dataSQL[$key_index+1][0]	= '';
				$dataSQL[$key_index+1][1]	= '';
				$dataSQL[$key_index+1][2]	= '';
				$dataSQL[$key_index+1][4]	= '';
				$dataSQL[$key_index+1][5]	= '';
				$dataSQL[$key_index+1][6]	= '';
				$dataSQL[$key_index+1][7]	= '';
				$dataSQL[$key_index+1][8]	= '';
				$dataSQL[$key_index+1][9]	= '';
				$dataSQL[$key_index+1][10]	= 'Total';

				$dataSQL[$key_index+1][11]	= $totalnumbertx;




				$this->_helper->download->csv($header,$dataSQL,null,'Reconciliation Report');
				Application_Helper_General::writeLog('RPTX','Download CSV Reconciliation report');
			}
			elseif($pdf)
			{
				$dataSQL[$key+1]['number']	= false;
				$dataSQL[$key_index+1][0]	= '';
				$dataSQL[$key_index+1][1]	= '';
				$dataSQL[$key_index+1][2]	= '';
				$dataSQL[$key_index+1][4]	= '';
				$dataSQL[$key_index+1][5]	= '';
				$dataSQL[$key_index+1][6]	= '';
				$dataSQL[$key_index+1][7]	= '';
				$dataSQL[$key_index+1][8]	= '';
				$dataSQL[$key_index+1][9]	= '';
				$dataSQL[$key_index+1][10]	= 'Total';

				$dataSQL[$key_index+1][11]	= $totalnumbertx;

// 				var_dump($dataSQL);die;
				$this->_helper->download->pdf($header,$dataSQL,null,'Reconciliation Report');
				Application_Helper_General::writeLog('RPTX','Download PDF Reconciliation report');
			}
			else if($this->_request->getParam('print') == 1){


				//$dataSQL[$key_index+1]['companycode']	= '';
				$dataSQL[$key_index+1][1]	= '';
				$dataSQL[$key_index+1][2]	= '';
				$dataSQL[$key_index+1][4]	= '';
				$dataSQL[$key_index+1][5]	= '';
				$dataSQL[$key_index+1][6]	= '';
				$dataSQL[$key_index+1][7]	= '';
				$dataSQL[$key_index+1][8]	= '';
				$dataSQL[$key_index+1][9]	= '';
				$dataSQL[$key_index+1][10]	= '';

				$dataSQL[$key_index+1][11]	= '';
				$dataSQL[$key_index+1]['uuid']	= 'Total';
				$dataSQL[$key_index+1]['traamount']	= $totalnumbertx;



				$this->_forward('print', 'index', 'widget', array('data_content' => $dataSQL, 'data_caption' => 'Reconciliation Report', 'data_header' => $tempfields));
			}
		}
		else
		{
			$this->paging($dataSQL);

			$stringParam = array(
									'COMP_ID'	=>$fCOMPANYCODE,
									'COMP_NAME'	=>$fCOMPANYNAME,
									'PAY_REF'	=>$fPAYMENTREF,
									'REF_NUM'	=>$fREFERENCENO,
									'TRANSACTIONID'	=>$fTRANSACTIONID,

									'PS_TRFDATE'	=>$fTRANSFERFROM,
									'PS_TRFDATE_END'	=>$fTRANSFERTO,

									'CREATEDFROM'	=>$fCREATEDFROM,
									'CREATEDTO'		=>$fCREATEDTO,

									'UPDATEDFROM'	=>$fUPDATEDFROM,
									'UPDATEDTO'		=>$fUPDATEDTO,

									'ACCTSRC'		=>$fACCTSRC,
									'BENEACCT'		=>$fBENEACCT,

									'PAYTYPE'		=>$fPAYTYPE,
									'TRANSFERTYPE'	=>$fTRANSFERTYPE,
									'UUID'			=>$fUUID,
									'TRANSSTATUS'	=>$fTRANSSTATUS,
									'CHARGEDSTATUS'	=>$fCHARGEDSTATUS,
									'clearfilter'	=> $clearfilter,
									'filter'		=> $filter,

								);


			$this->view->totalnumbertx 	= $totalnumbertx;
			$this->view->COMPANYCODE 	= $fCOMPANYCODE;
			$this->view->COMPANYNAME 	= $fCOMPANYNAME;
			$this->view->PAYMENTREF 	= $fPAYMENTREF;
			$this->view->REFERENCENO 	= $fREFERENCENO;
			$this->view->TRANSACTIONID 	= $fTRANSACTIONID;

			$this->view->TRANSFERFROM 	= $fTRANSFERFROM;
			$this->view->TRANSFERTO 	= $fTRANSFERTO;

			$this->view->CREATEDFROM 	= $fCREATEDFROM;
			$this->view->CREATEDTO 		= $fCREATEDTO;

			$this->view->UPDATEDFROM 	= $fUPDATEDFROM;
			$this->view->UPDATEDTO 		= $fUPDATEDTO;

			$this->view->ACCTSRC 		= $fACCTSRC;
			$this->view->BENEACCT 		= $fBENEACCT;

			$this->view->PAYTYPE 		= $fPAYTYPE;
			$this->view->TRANSFERTYPE 	= $fTRANSFERTYPE;
			$this->view->UUID 			= $fUUID;
			$this->view->TRANSSTATUS 	= $fTRANSSTATUS;
			$this->view->CHARGEDSTATUS 	= $fCHARGEDSTATUS;

			$this->view->fields = $fields;
			$this->view->filter = $filter;

			$this->view->arrPayType 		= $optpaytypeRaw;
			$this->view->arrTraType 		= $arrTraTypeRaw;
			$this->view->arrTraStatus 		= $arrTraStatus;
			$this->view->arrChargedStatus 	= $arrChargedStatus;
			$this->view->listCustId 		= $list;
			$this->view->dataCount 			= $dataCount;

			Application_Helper_General::writeLog('RPTX','View Reconciliation Report');

			if(!empty($dataParamValue)){
	    		$this->view->trfdateStart = $dataParamValue['PS_TRFDATE'];
	    		$this->view->trfdateEnd = $dataParamValue['PS_TRFDATE_END'];

	    	  	unset($dataParamValue['PS_TRFDATE_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
		        $this->view->wherecol     = $wherecol;
		        $this->view->whereval     = $whereval;
		     // print_r($whereval);die;
		    }

		}

	}

	public function editAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


		$paymentref = $this->language->_('Payment Ref');
		$beneficiaryaccount = $this->language->_('Beneficiary Account');
		$paymentref = $this->language->_('Payment Ref#');
		$referenceno = $this->language->_('Reference No');
		$traceNo = $this->language->_('Trace No');
		$createddate = $this->language->_('Created Date');
		$beneficiarytype = $this->language->_('Beneficiary Type');
		$updateddate = $this->language->_('Updated Date');
		$paymentdate = $this->language->_('Payment Date');
		$sourceaccount = $this->language->_('Source Account');
		$paymenttype = $this->language->_('Payment Type');
		$paymentstatus = $this->language->_('Payment Status');
		$amount = $this->language->_('Amount');
		$fee = $this->language->_('Fee');

		try {
			$conf 		= Zend_Registry::get('config');
			// 		$host 		= $conf['db2']['params']['host'];
			// 		$username	= $conf['db2']['params']['username'];
			// 		$pass		= $conf['db2']['params']['password'];
			// 		$db			= $conf['db2']['params']['dbname'];
			// 		$port		= $conf['db2']['params']['port'];

			//stag config
			$db = Zend_Db_Table::getDefaultAdapter();
			$config = Zend_Registry::get('config');

			//protected $_db;
		$config = array(
							       'host'      => '192.168.86.23',
							       'username'  => 'ibsgolive',
							       'password'  => 'F9t%<HCj3L9tPZf?mv)]C#K',
							       'dbname'    => 'sgolivebankdev',
							       'port'  => '3306'
							     );
			$dbCon = new Zend_Db_Adapter_Pdo_Mysql($config);

			// 		$key = $conf['enc']['dec']['key'];
			// 		$iv = $conf['enc']['dec']['iv'];

			// 		$usernameval = Application_Helper_General::decrypt($key,$iv,$username);
			// 		$passval = Application_Helper_General::decrypt($key,$iv,$pass);

			// 		$config = array(
			// 				'host'      	=> $host,
			// 				'username'  	=> $usernameval,
			// 				'password'  	=> $passval,
			// 				'dbname'    	=> $db,
			// 				'port'  		=> $port
			// 		);

			// 		$dbCon = new Zend_Db_Adapter_Pdo_Mysql($config);
		}
		catch(Zend_Db_Exception $e) {
			echo $e->getMessage();
		}


		$fields = array	(
				'payref'  			=> array(
						'field' => 'PS_NUMBER',
						'label' => $paymentref,
						'sortable' => true
				),
				'referenceno'  			=> array(
						'field' => 'TRX_ID',
						'label' => $referenceno,
						'sortable' => true
				),




				'transferdate'  	=> array(
						'field' => 'PS_EFDATE',
						'label' => $paymentdate,
						'sortable' => true
				),

				'paytype'  			=> array(
						'field' => 'PS_TYPE',
						'label' => $paymenttype,
						'sortable' => true
				),

				'serviceName'  		=> array(
						'field' => 'PS_BILLER_ID',
						'label' => $this->language->_('Service Category'),
						'sortable' => true
				),

				'providerName'  		=> array(
						'field' => 'PROVIDER_NAME',//'PS_BILLER_ID',
						'label' => $this->language->_('Biller Name'),
						'sortable' => true
				),
				'orderId'  			=> array(
						'field' => 'BILLER_ORDER_ID',
						'label' => $this->language->_('Order Id'),
						'sortable' => true
				),
				'uuid'  			=> array(
						'field' => 'UUID',
						'label' => $this->language->_('UUID'),
						'sortable' => true
				),
				'amount'  			=> array(
						'field' => 'TRA_AMOUNT',
						'label' => $amount,
						'sortable' => true
				),

		);


		//$getPayment = new transactionreport_Model_Transactionreport();

// 		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
// 		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
// 		if($sortBy=='U.USER_ID'){
// 			$sortBy = 'PS_NUMBER';
// 		}
// 		$sortDir = $this->_getParam('sortdir');
// 		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';

// 		$sorting = $sortBy.' '.$sortDir;
		//$filterSend = array();
		$sorting = 'P.PS_NUMBER DESC';
		//$filterSend['paymentreff'] = $this->_getParam('psnumber');

		$AESMYSQL = new Crypt_AESMYSQL();
		$fpayref = $AESMYSQL->decrypt($this->_getParam('psnumber'), $password);
		//$result = $getPayment -> getPaymentRecon($filterSend,$sorting);

		$paytypeall		= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		$paystatusarr = array_combine($this->_paymentstatus['code'],$this->_paymentstatus['desc']);
		//var_dump($paystatusarr);


		foreach($paytypeall as $key => $value){

			if($key != 3 && $key != 8 && $key != 9 && $key != 10){

				if($key == 4
				// OR $key == 6
				) {
					$value = $paytypeall[6];
					$optpaytypeAll[$key] = $value;
				}
				else if($key == 5
				// OR $key == 7
				) {
					$value = $paytypeall[7];
					$optpaytypeAll[$key] = $value;
				}
				else $optpaytypeAll[$key] = $value;
			}

		}

		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($optpaytypeAll as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";

  		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE 'N/A' END)";

		$select = $this->_db->select()
							//->distinct()
							->from(		array(	'T'=>'T_TRANSACTION'),array())
							->joinLeft(	array(	'P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',
										array(


												'companycode'	=>'C.CUST_ID',
												'companyname'	=>'C.CUST_NAME',
												'payref'		=>'P.PS_NUMBER',
												'referenceno' =>'T.TRX_ID',
												'transferdate'	=>'P.PS_EFDATE',
												'paymenttype'	=>$casePayType,





												'T.BILLER_ORDER_ID',
												'traamount'		=>'T.TRA_AMOUNT',
												'P.PS_BILLER_ID',

												'uuid'			=>'P.UUID',
												'PS_STATUS'		=>$casePayStatus,


												))
							->joinLeft(array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())

							->joinLeft (array('A' => 'M_SERVICE_PROVIDER'), 'A.PROVIDER_ID = P.PS_BILLER_ID', array('A.PROVIDER_ID','A.SERVICE_TYPE'))
							->joinLeft (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array('B.SERVICE_NAME','A.PROVIDER_NAME'))
							->where("UPPER(P.PS_TYPE) IN (16,17)");
							//->where(" T.TRA_STATUS = 3 oR T.TRA_STATUS = 4");

		$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fpayref.'%'));
		$select->order($sorting);
		//$select->limit(1);

		$result = $this->_db->fetchAll($select);

		$this->view->psnumber = $result[0]['payref'];


// 		print_r($result);die;

		$totalnumbertx = 0;
		$totalvaluetx = 0;
		// 		var_dump($result);die;
		foreach($result as $key =>$dt)
		{
			$sql_d2 = 'select * FROM L_SERVICECALL WHERE UUID = "'.$dt["UUID"].'" ';
			$result_d2 = $dbCon->fetchAll($sql_d2);

			if(!empty($result_d2['0'])){
				$sql_sequence = 'select * FROM L_SERVICECALL WHERE  ORDER_NO = 2 AND SEQUENCE_ID = "'.$result_d2['0']['SEQUENCE_ID'].'" ';
				$result_sequence = $dbCon->fetchAll($sql_sequence);
				$result[$key]['UUID_REAL'] = $result_sequence["0"]["UUID"];
			}else{
				$result[$key]['UUID_REAL'] = '';
			}

			$totalnumbertx = $totalnumbertx + $dt["TRA_AMOUNT"];
			//$totalvaluetx  = $totalvaluetx + $dt["TRANSFER_FEE"];

		}

		@$avgnumbertx 	= round($totalnumbertx / count($result),2);
		//@$avgvaluetx 	= round($totalvaluetx / count($result),2);


		//count totalhit and average:
		$totalnumbertx 	= Application_Helper_General::displayMoney($totalnumbertx);

		$this->view->totalnumbertx 	= $totalnumbertx;
		//$this->view->totalvaluetx 	= $totalvaluetx;

		$privi = $this->_priviId;
// 		print_r($privi);die;
		foreach ($privi as $val){
// 			print_r($val);die;
			if($val == 'URRP'){

					$this->view->changestatus = false;
			}
		}

		$submit = $this->_getParam('submit');
		if($submit)
		{

// 			print_r('here');die;
			$error = 0;
			if($error == 0)
			{
				$this->_db->beginTransaction();
				try
				{

					$paymentreff 	 = $this->_getParam('psnumber');
					$content  = array('TRA_STATUS' => '4');

					//$result = $getPayment ->updateRecon($paymentreff,$content);
					$whereArr  = array('PS_NUMBER = ?'=>$paymentreff);
    				$result = $this->_db->update('T_TRANSACTION',$content,$whereArr);

					Application_Helper_General::writeLog('URRP','Update Reconciliation Report');

					if($error == 0)
					{
						$this->_db->commit();
						$this->setbackURL('/transactionreport/reconciliationreport');
						$this->_redirect('/notification/success/index');
					}
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
		}

		$this->paging($result);
		$this->view->fields = $fields;

	}
}
