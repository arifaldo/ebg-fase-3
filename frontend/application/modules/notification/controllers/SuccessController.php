<?php
require_once 'Zend/Controller/Action.php';

class notification_SuccessController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ns = new Zend_Session_Namespace('FVC');
		
		if (isset($ns->backURL) && !(empty($ns->backURL))){
			$this->view->backURL = $ns->backURL;
		}
		$ns->backURL = '';
	}

	public function requestAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ns = new Zend_Session_Namespace('FVC');
		
		if (isset($ns->backURL) && !(empty($ns->backURL))){
			$this->view->backURL = $ns->backURL;
		}
		$ns->backURL = '';
	}

	public function comingsoonAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ns = new Zend_Session_Namespace('FVC');
		
		if (isset($ns->backURL) && !(empty($ns->backURL))){
			$this->view->backURL = $ns->backURL;
		}
		$ns->backURL = '';
	}
}
