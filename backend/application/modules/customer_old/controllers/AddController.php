<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once "Service/Account.php";
require_once 'General/Account.php';
require_once 'General/Settings.php';
require_once 'General/CustomerUser.php';

class customer_AddController extends customer_Model_Customer
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$this->view->cust_typeArr 			= $this->getCompanyType();
		$this->view->business_typeArr 		= $this->getBusinessEntity();
		$this->view->debitur_codeArr 		= $this->getDebitur();
		$this->view->collectibility_codeArr = $this->getCreditQuality();
		$this->view->cust_cityArr 			= $this->getCity();
		$this->view->country_codeArr 		= $this->getCountry();

		if ($this->_request->isPost()) {
			$params = $this->_request->getParams();

			$filters = array('*' => array('StringTrim', 'StripTags'));

			$validators = array(
				'cust_model'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Customer Type cannot be empty'))
				),
				'cust_id'    => array(
					'NotEmpty',
					'Alnum',
					array('StringLength', array('min' => 5, 'max' => 16)),
					array('Db_NoRecordExists', array('table' => 'M_CUSTOMER', 'field' => 'CUST_ID')),
					array('Db_NoRecordExists', array('table' => 'TEMP_CUSTOMER', 'field' => 'CUST_ID')),
					'messages' => array(
						$this->language->_('Customer Code cannot be empty'),
						$this->language->_('Customer Code invalid format'),
						$this->language->_('Customer Code length min') . ' 6 ' . $this->language->_('max') . '16',
						$this->language->_('Customer code already in use. Please use another'),
						$this->language->_('Customer code already suggested. Please use another')
					)
				),
				'debitur_code'  => array(
					'allowEmpty' => true
				),
				'cust_cif'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Company CIF cannot be empty'))
				),
				'cust_name'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Company Name cannot be empty'))
				),
				'cust_npwp'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Company NPWP cannot be empty'))
				),
				'cust_type'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Company Type cannot be empty'))
				),
				'business_type'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Business Type cannot be empty'))
				),
				'go_public'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Go Public cannot be empty'))
				),
				'grup_bumn'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Grup Bumn cannot be empty'))
				),
				'cust_address'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Address cannot be empty'))
				),
				'cust_village'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kelurahan cannot be empty'))
				),
				'cust_district'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kecamatan cannot be empty'))
				),
				'cust_city'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kabupaten/City cannot be empty'))
				),
				'cust_zip'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Zip cannot be empty'))
				),
				'country_code'    => array(
					'NotEmpty',
					'messages' => array($this->language->_('Country cannot be empty'))
				),
				'cust_email' => array(
					'NotEmpty',
					'messages' => array($this->language->_('Contact Email cannot be empty'))
				),
				'cust_website'  => array(
					'allowEmpty' => true
				),
				'req_id'	 => array(
					'allowEmpty' => true
				),
				'cust_same_user'  => array(
					'allowEmpty' => true
				),
				'cust_approver'  => array(
					'allowEmpty' => true
				),
			);

			if ($params['cust_model'] == 1) {
				$validators += array(
					'debitur_code'   => array(
						'NotEmpty',
						'messages' => array($this->language->_('Golongan Debitur Code cannot be empty'))
					),
					'cust_contact'	 => array('allowEmpty' => true),
					'cust_contact_phone'     => array(
						'allowEmpty' => true,
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
					'cust_phone'       => array(
						'allowEmpty' => true,
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
				);
			} else {
				$validators += array(
					'cust_phone' => array(
						'NotEmpty',
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Company Phone cannot be empty'),
							$this->language->_('Company Phone maximum characters allowed is') . ' 128'
						)
					),
				);
			}

			if ($params['cust_model'] == 2) {
				$validators += array(
					'cust_contact'	 => array(
						'NotEmpty',
						'messages' => array($this->language->_('Contact Person cannot be empty'))
					),
					'cust_contact_phone' => array(
						'NotEmpty',
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number cannot be empty'),
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
				);
			}

			if ($params['cust_model'] == 3) {
				$validators += array(
					'cust_contact'	 => array('allowEmpty' => true),
					'cust_contact_phone'     => array(
						'allowEmpty' => true,
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
				);
			}

			if ($params['cust_model'] != 3) {
				$validators += array(
					'collectibility_code'	 => array(
						'NotEmpty',
						'messages' => array($this->language->_('Collectibility Code cannot be empty'))
					),
				);
			}

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
			// $this->print($zf_filter_input);

			if ($zf_filter_input->isValid()) {
				// Validasi multiple email
				if ($zf_filter_input->cust_email) {
					$validate = new validate;
					$cek_multiple_email = $validate->isValidEmailMultiple($zf_filter_input->cust_email);
				} else {
					$cek_multiple_email = true;
				}

				foreach ($validators as $key => $value) {
					if ($zf_filter_input->$key) {
						$cust_data[strtoupper($key)] = $zf_filter_input->$key;
					}
				}

				$info = 'Customer ID = ' . $zf_filter_input->cust_id . ', Customer Name = ' . $zf_filter_input->cust_name;
				if ($this->_request->isPost()) {

					try {
						$this->_db->beginTransaction();

						if (is_null($cust_data['CUST_CHARGES_STATUS']) || $cust_data['CUST_CHARGES_STATUS'] == '') {
							$cust_data['CUST_CHARGES_STATUS'] = 0;
						}
						if (is_null($cust_data['CUST_MONTHLYFEE_STATUS']) || $cust_data['CUST_MONTHLYFEE_STATUS'] == '') {
							$cust_data['CUST_MONTHLYFEE_STATUS'] = 0;
						}
						if (is_null($cust_data['CUST_TOKEN_AUTH']) || $cust_data['CUST_TOKEN_AUTH'] == '') {
							$cust_data['CUST_TOKEN_AUTH'] = 0;
						}
						if (is_null($cust_data['CUST_SPECIAL_RATE']) || $cust_data['CUST_SPECIAL_RATE'] == '') {
							$cust_data['CUST_SPECIAL_RATE'] = 0;
						}
						if (is_null($cust_data['CUST_SPECIAL']) || $cust_data['CUST_SPECIAL'] == '') {
							$cust_data['CUST_SPECIAL'] = 'N';
						}
						if (is_null($cust_data['CUST_LIMIT_IDR']) || $cust_data['CUST_LIMIT_IDR'] == '') {
							$cust_data['CUST_LIMIT_IDR'] = 0;
						}
						if (is_null($cust_data['CUST_LIMIT_USD']) || $cust_data['CUST_LIMIT_USD'] == '') {
							$cust_data['CUST_LIMIT_USD'] = 0;
						}

						if ($cust_data['GRUP_BUMN'] == "Y") {
							$cust_data['GRUP_BUMN'] = 1;
						} else {
							$cust_data['GRUP_BUMN'] = 0;
						}

						$cust_data['CUST_STATUS']		= 1;
						$cust_data['CUST_CREATED']		= date('Y-m-d H:i:s');
						$cust_data['CUST_CREATEDBY']	= $this->_userIdLogin;
						$cust_data['CUST_UPDATED']		= null;
						$cust_data['CUST_UPDATEDBY']	= null;
						$cust_data['CUST_SUGGESTED']	= date('Y-m-d H:i:s');
						$cust_data['CUST_SUGGESTEDBY']	= $this->_userIdLogin;

						if ($cust_data['CUST_APP_TOKEN'] == 'on') {
							$cust_data['CUST_APP_TOKEN'] = '1';
						}
						if ($cust_data['CUST_RLS_TOKEN'] == 'on') {
							$cust_data['CUST_RLS_TOKEN'] = '1';
						}
						if ($zf_filter_input->cust_role_adm == 'on') {
							$cust_data['CUST_ADM_ROLE'] = '1';
						}
						if ($cust_data['CUST_FINANCE'] == '3') {
							$cust_data['CUST_LIMIT_IDR'] = 1;
							$cust_data['CUST_LIMIT_USD'] = 1;
						}

						if (!empty($zf_filter_input->same_release)) {
							$cust_data['CUST_SAME_USER'] = $zf_filter_input->same_release;
						}

						if (!empty($zf_filter_input->cust_cif)) {
							$cust_data['CUST_CIF'] = $zf_filter_input->cust_cif;
						}

						// insert ke T_GLOBAL_CHANGES
						$change_id = $this->suggestionWaitingApproval('Customer', $info, $this->_changeType['code']['new'], null, 'M_CUSTOMER', 'TEMP_CUSTOMER', $zf_filter_input->cust_id, $zf_filter_input->cust_name, $zf_filter_input->cust_id, $zf_filter_input->cust_name);
						// $this->print($change_id); 
						try {
							// Insert temp
							//$this->print($cust_data); die();
							$this->insertTempCustomer($change_id, $cust_data);

							foreach ($zf_filter_input->req_id as $key => $value) {
								$content = [
									'CHANGES_ID'		=> $change_id,
									'CUST_ID'			=> $cust_data['CUST_ID'],
									'ACCT_NO'			=> $value,
									'CCY_ID'			=> Application_Helper_General::getCurrCode($this->_getParam('ccy' . $value)),
									'ACCT_EMAIL'		=> null,
									'ACCT_STATUS'		=> 1,
									'ACCT_SUGGESTED'	=> date('Y-m-d H:i:s'),
									'ACCT_SUGGESTEDBY'	=> $this->_userIdLogin,
									'PLAFOND'			=> null,
									'ACCT_SOURCE'		=> null,
									// 'ACCT_NAME'			=> $this->_getParam('accountName' . $value),
									'ACCT_NAME'			=> $this->_getParam('cust_name'),
									'ACCT_TYPE'			=> $this->_getParam('productType' . $value),
									'ACCT_DESC'			=> $this->_getParam('acct_desc' . $value),
									// 'ACCT_ALIAS_NAME'	=> isset($acct_data['ACCT_ALIAS_NAME']) ? $acct_data['ACCT_ALIAS_NAME'] : null,
									'ACCT_ALIAS_NAME'  => $this->_getParam('accountName' . $value),
									'ORDER_NO'         => $key,
									// 'ACCT_NAME'			=> isset($acct_data['ACCT_NAME']) ? $acct_data['ACCT_NAME'] : null,
									'GROUP_ID'			=> null,
									'ACCT_RESIDENT'		=> null,
									'ACCT_CITIZENSHIP'	=> null,
									'ACCT_CATEGORY'		=> null,
									'ACCT_ID_TYPE'		=> null,
									'ACCT_ID_NUMBER'	=> null
								];
								// $this->print($content);
								$insert = $this->_db->insert('TEMP_CUSTOMER_ACCT', $content);
							}
						} catch (Exception $e) {
							$this->print($e);
							die('catch');
						}

						Application_Helper_General::writeLog('CCAD', 'Customer has been added, Cust ID : ' . $zf_filter_input->cust_id . ' Cust Name : ' . $zf_filter_input->cust_name . ' Change ID : ' . $change_id);

						$this->_db->commit();

						$set = new Settings();
						$template 						= $set->getSetting('bemailtemplate_greeting_eregis');
						$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
						$templateEmailMasterBankName 	= $set->getSetting('master_bank_name');
						$masterBankEmail 				= $set->getSetting('master_bank_email');
						$masterBankTlp 					= $set->getSetting('master_bank_telp');
						$masterBankWapp 				= $set->getSetting('master_bank_wapp');

						$template = str_ireplace('[[contact_name]]', $zf_filter_input->cust_contact, $template);
						$template = str_ireplace('[[company_name]]', $zf_filter_input->cust_name, $template);
						$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
						$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
						$template = str_ireplace('[[master_bank_email]]', $masterBankEmail, $template);
						$template = str_ireplace('[[master_bank_telp]]', $masterBankTlp, $template);
						$template = str_ireplace('[[master_bank_wapp]]', $masterBankWapp, $template);

						$subject = "Greeting Registration";

						// Application_Helper_Email::queueEmail($email,$subject,$template,__FILE__.__LINE__);
						Application_Helper_Email::sendEmail($zf_filter_input->cust_email, $subject, $template);
						$this->setbackURL('/customer');
						$this->_redirect('/notification/submited/index');
					} catch (Exception $e) {
						$this->_db->rollBack();
						$error_remark = 'exception';
					}
				}
				if (isset($error_remark)) {
					// 
				} else {
					$this->view->success = 1;
					$msg = 'Record added successfully';
					$key_value = $zf_filter_input->cust_id;
					$this->view->customer_msg = $msg;
					// Insert log
					try {
						$this->_db->beginTransaction();

						Application_Helper_General::writeLog('CCAD', ' Create Customer [' . $this->view->cust_id . ']');

						$this->_db->commit();
					} catch (Exception $e) {
						$this->_db->rollBack();
						Application_Log_GeneralLog::technicalLog($e);
					}
				}
			} else {
				$errors = $zf_filter_input->getMessages();
				$errDesc['cust_model']			= (isset($errors['cust_model'])) ? $errors['cust_model'] : null;
				$errDesc['cust_id']				= (isset($errors['cust_id'])) ? $errors['cust_id'] : null;
				$errDesc['cust_cif']			= (isset($errors['cust_cif'])) ? $errors['cust_cif'] : null;
				$errDesc['cust_name']			= (isset($errors['cust_name'])) ? $errors['cust_name'] : null;
				$errDesc['cust_npwp']			= (isset($errors['cust_npwp'])) ? $errors['cust_npwp'] : null;
				$errDesc['cust_type']			= (isset($errors['cust_type'])) ? $errors['cust_type'] : null;
				$errDesc['business_type']		= (isset($errors['business_type'])) ? $errors['business_type'] : null;
				$errDesc['go_public']			= (isset($errors['go_public'])) ? $errors['go_public'] : null;
				$errDesc['grup_bumn']			= (isset($errors['grup_bumn'])) ? $errors['grup_bumn'] : null;
				$errDesc['cust_address']		= (isset($errors['cust_address'])) ? $errors['cust_address'] : null;
				$errDesc['debitur_code']		= (isset($errors['debitur_code'])) ? $errors['debitur_code'] : null;
				$errDesc['collectibility_code']	= (isset($errors['collectibility_code'])) ? $errors['collectibility_code'] : null;
				$errDesc['cust_village']		= (isset($errors['cust_village'])) ? $errors['cust_village'] : null;
				$errDesc['cust_district']		= (isset($errors['cust_district'])) ? $errors['cust_district'] : null;
				$errDesc['cust_city']			= (isset($errors['cust_city'])) ? $errors['cust_city'] : null;
				$errDesc['cust_zip']			= (isset($errors['cust_zip'])) ? $errors['cust_zip'] : null;
				$errDesc['country_code']		= (isset($errors['country_code'])) ? $errors['country_code'] : null;
				$errDesc['cust_contact']		= (isset($errors['cust_contact'])) ? $errors['cust_contact'] : null;
				$errDesc['cust_contact_phone']	= (isset($errors['cust_contact_phone'])) ? $errors['cust_contact_phone'] : null;
				$errDesc['cust_email']			= (isset($errors['cust_email'])) ? $errors['cust_email'] : null;
				$errDesc['cust_phone']			= (isset($errors['cust_phone'])) ? $errors['cust_phone'] : null;
				$errDesc['cust_website']		= (isset($errors['cust_website'])) ? $errors['cust_website'] : null;

				$this->view->errDesc 			= $errDesc;
				$this->view->cust_model 		= $zf_filter_input->cust_model;
				$this->view->cust_id 			= $zf_filter_input->cust_id;
				$this->view->cust_cif 			= $zf_filter_input->cust_cif;
				$this->view->cust_name 			= $zf_filter_input->cust_name;
				$this->view->cust_npwp 			= $zf_filter_input->cust_npwp;
				$this->view->cust_type 			= $zf_filter_input->cust_type;
				$this->view->business_type 		= $zf_filter_input->business_type;
				$this->view->go_public 			= $zf_filter_input->go_public;
				$this->view->grup_bumn 			= $zf_filter_input->grup_bumn;
				$this->view->cust_address 		= $zf_filter_input->cust_address;
				$this->view->debitur_code 		= $zf_filter_input->debitur_code;
				$this->view->collectibility_code = $zf_filter_input->collectibility_code;
				$this->view->cust_village 		= $zf_filter_input->cust_village;
				$this->view->cust_district 		= $zf_filter_input->cust_district;
				$this->view->cust_city 			= $zf_filter_input->cust_city;
				$this->view->cust_zip 			= $zf_filter_input->cust_zip;
				$this->view->country_code 		= $zf_filter_input->country_code;
				$this->view->cust_contact 		= $zf_filter_input->cust_contact;
				$this->view->cust_contact_phone = $zf_filter_input->cust_contact_phone;
				$this->view->cust_email 		= $zf_filter_input->cust_email;
				$this->view->cust_phone 		= $zf_filter_input->cust_phone;
				$this->view->cust_website 		= $zf_filter_input->cust_website;
			}
		}
	}

	public function cifaccountAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$cust_cif   = $this->_getParam('cust_cif');
		$changes_id = $this->_getParam('changes_id');

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$core = array();
		//var_dump($cust_cif);

		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $cust_cif);
		$result = $svcAccountCIF->inquiryCIFAccount();


		$core =  $result['accounts'];
		//var_dump($result);
		//die;
		// DATA TEMP CUSTOMER ACCOUNT
		$select = $this->_db->select()
			->from('TEMP_CUSTOMER_ACCT')
			->where('CHANGES_ID = ?', $changes_id);
		$accountInfo = $this->_db->fetchAll($select);

		$accountInfoArr = [];
		if ($accountInfo) {
			foreach ($accountInfo as $row) {
				$accountInfoArr[] = $row['ACCT_NO'];
			}
		}


		if ($result['response_code'] == '0000') {
			$arrResult = array();
			$err = array();

			if (count($core) > 1) {
				foreach ($core as $key => $val) {
					//if($val->status == '0'){
					$arrResult = array_merge($arrResult, array($val['account_number'] => $val));
					array_push($err, $val['account_number']);
					//}
				}
			} else {
				foreach ($core as $data) {
					//if($data->status == '0'){
					$arrResult = array_merge($arrResult, array($data['account_number'] => $data));
					array_push($err, $data['account_number']);
					//}
				}
			}
			echo '<pre>';
			//var_dump($arrResult);die;





			$final = $arrResult;
			if (count($final) < 1) {
				$this->view->data1 = '0';
			} else {
				$this->view->data = $arrResult;
			}

			$dataProPlan = array('xxxxx');
			foreach ($final as $dataProductPlan) {

				// print_r($dataProductPlan);die;
				//if($dataProductPlan->status == '0'){
				if (isset($dataProductPlan['type'])) {

					// $test = new Service_Account($dataProductPlan['account_number'], '');

					// $info = $test->inquiryAccontInfo();
					// $girosaving = $test->inquiryAccountBalance();
					// $deposito = $test->inquiryDeposito();

					// if(!empty($info["account_type"])){
					// 	$dataProductPlan['type'] = $info["account_type"];
					// }elseif (!empty($girosaving["account_type"])) {
					// 	$dataProductPlan['type'] = $girosaving["account_type"];
					// }else{
					// 	$dataProductPlan['type'] = $deposito["account_type"];
					// }

					// print_r($info["account_type"]);
					// print_r('----------------');
					// print_r($girosaving["account_type"]);
					// print_r('----------------');
					// print_r($deposito["account_type"]);
					// die();

					// var_dump($info, $girosaving, $deposito);


					// print_r('res');
					// print_r($test);
					// die();


					// $select_product_type  = $this->_db->select()
					// ->from(array('M_PRODUCT_TYPE'), array('PRODUCT_NAME'))
					// ->where("PRODUCT_CODE = ?", $dataProductPlan['type']);

					// $userData_product_type = $this->_db->fetchAll($select_product_type);

					$dataUser[] = $dataProductPlan;


					// if(!empty($userData_product_type)){
					// 	$dataProductPlan['type_desc'] = $userData_product_type[0]['PRODUCT_NAME'];	
					// 	$dataUser[] = $dataProductPlan;
					// }

				}
			}
		}

		$optHtml = '<table cellpadding="3" cellspacing="0" border="0" class="table table-sm table-striped" id="myTable">
		<thead>
		<tr class="headercolor">
		<th width="3%" valign="top" class="tablehead fth-header"></th>
		<th  valign="top" class="tablehead fth-header">' . $this->language->_('Account Number') . '</th>
		<th  valign="top" class="tablehead fth-header">' . $this->language->_('Currency') . '</th>
		<th  valign="top" class="tablehead fth-header">' . $this->language->_('Type') . '</th>
		</tr>
		</thead>
		<tbody id="accountBody">';


		if (!empty($dataUser)) {
			foreach ($dataUser as $key => $row) {
				$i = $i + 1;
				$acct_source = 3;
				$acct_desc   = $row['type_desc'];

				if ($row['currency'] == '000') {
					$cekIDR = 'IDR';
				}

				if (in_array($row['account_number'], $accountInfoArr)) {
					$checked = 'checked';
				} else {
					$checked = '';
				}

				$button_checkbox = "<input type='checkbox' name='req_id[]' id='" . $row['account_number'] . "' value='" . $row['account_number'] . "'>";
				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
				if ($row['account_number'] != '') {
					$optHtml .= '<tr>
					<td class="' . $td_css . '"><input type="checkbox" name="req_id[]" id="' . $row['account_number'] . '" class="cekbok" value="' . $row['account_number'] . '" ' . $checked . '></td>
					<td class="' . $td_css . '"><input type="hidden" name="accountName' . $row['account_number'] . '" id="accountName' . $row['account_number'] . '" value="' . $row['account_number'] . '">' . $row['account_number'] . '</td>
					<td class="' . $td_css . '"><input type="hidden" name="ccy' . $row['account_number'] . '" id="ccy' . $row['account_number'] . '" value="' . $cekIDR . '">' . $row['currency'] . '</td>
					<td class="' . $td_css . '">' . $acct_desc . '<input type="hidden" name="acct_desc' . $row['account_number'] . '" id="acct_desc' . $row['account_number'] . '" value="' . $acct_desc . '"><input type="hidden" name="productType' . $row['account_number'] . '" value="' . $row['type'] . '"><input type="hidden" name="planCode' . $row['account_number'] . '" value="' . $row['type'] . '"><input type="hidden" name="ccy' . $row['account_number'] . '" value="' . $row['currency'] . '"><input type="hidden" name="productName' . $row['account_number'] . '" id="productName' . $row['account_number'] . '" value="' . $acct_desc . '">
					</td>
					</tr>';
				}
			}
		} else {
			$optHtml .= '<tr><td colspan="5" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}

		$optHtml .= '</tbody></table>';
		echo $optHtml;
	}

	public function checkcustidAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$cust_id   = $this->_getParam('cust_id');

		$select = $this->_db->select()
			->from('TEMP_CUSTOMER')
			->where('CUST_ID = ?', $cust_id);
		$tempCustomer = $this->_db->fetchAll($select);

		if (!$tempCustomer) {

			$select2 = $this->_db->select()
				->from('M_CUSTOMER')
				->where('CUST_ID = ?', $cust_id);
			$masterCustomer = $this->_db->fetchAll($select2);

			if (!$masterCustomer) {
				echo json_encode(["status" => "Y"]);
			} else {
				echo json_encode(["status" => "N", "msg" => $this->language->_('Kode customer telah di entri, silahkan gunakan kode lain')]);
			}
		} else {
			echo json_encode(["status" => "N", "msg" => $this->language->_('Kode customer telah di entri, silahkan gunakan kode lain')]);
		}
	}

	private function print($data)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre><br>";
	}

	private function dump($data)
	{
		echo "<pre>";
		var_dump($data);
		echo "</pre><br>";
	}
}
