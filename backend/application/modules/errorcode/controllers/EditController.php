<?php

require_once 'Zend/Controller/Action.php';

class Errorcode_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction() 
	{
	    //pengaturan url untuk button back
			$this->_helper->layout()->setLayout('newlayout');
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}


    	$this->view->report_msg = array();

		if(!$this->_request->isPost())
		{
			$countrycode = $this->_getParam('CODE');
			//$bank_id = (Zend_Validate::is($bank_id,'Digits'))? $bank_id : null;

			if($countrycode)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_ERRORCODE'))
									 ->where("CODE=?", $countrycode)
							                   );
			  if($resultdata)
			  {
			        $this->view->CODE        = $resultdata['CODE'];
					$this->view->DESC      = $resultdata['DESC'];
					$this->view->IS_RERUN      = $resultdata['IS_RERUN'];

			  }
			}
			else
			{
			   $error_remark = 'Code Code not found';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   //$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			   $this->_redirect('/'.$this->_request->getModuleName().'/index');
			}
		}
		else
		{
			$filters = array(
							'IS_RERUN'  => array('StringTrim','StripTags','StringToUpper'),
							 'CODE'       => array('StringTrim','StripTags','StringToUpper'),
			                 'DESC'     => array('StringTrim','StripTags','StringToUpper'),
							);

			$validators = array(
								'IS_RERUN'  => array(),
								'CODE'  => array(),
			                    'DESC' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>128)),
													 'messages' => array(
																         $this->language->_('Cannot be empty'),
																         $this->language->_('Data too long (max 128 chars)'),
			                                                             )
													)
							   );

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//Zend_Debug::Dump($this->_request->getParams());die;

			if($zf_filter_input->isValid())
			{
				$content = array(
								'DESC' 	 => $zf_filter_input->DESC,
								'IS_RERUN' 	 => $zf_filter_input->IS_RERUN,
						       );

				try
				{
				    //-----insert--------------
					$this->_db->beginTransaction();

					$whereArr  = array('CODE = ?'=>$zf_filter_input->CODE);
					$this->_db->update('M_ERRORCODE',$content,$whereArr);

					$this->_db->commit();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					Application_Helper_General::writeLog('COUD','Edit Code. Code : ['.$zf_filter_input->CODE.']');
					$this->view->success = true;
					$this->view->report_msg = array();

					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

				    foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);
					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
	}

}
