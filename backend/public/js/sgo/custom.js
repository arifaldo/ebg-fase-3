function openwindow(url,width,height)
	{
		var new_window;
		//calculate window position
		var xpos=(screen.width/2)-(width/2);
		var ypos=(screen.height/2)-(height/2);
		new_window = window.open(url, "New_Window",
	    "left="+xpos+", top="+ypos+", width="+width+"," +
	    "height="+height+", location=no, menubar=no, resizable=yes, " +
	    "scrollbars=yes, scrollable=yes, status=no, titlebar=no, toolbar=no");
		new_window.focus();
	}
		
	var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
	var serverdate=new Date(currenttime)
	
	function padlength(what){
	var output=(what.toString().length==1)? "0"+what : what
	return output
	}
	
	function displaytime(){
		serverdate.setSeconds(serverdate.getSeconds()+1)
		var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())
		document.getElementById("cdcontainer").innerHTML=timestring

		var hr = Number(serverdate.getHours());
		var iconsrc;
		var greeting;
		if(hr < 12){
			greeting = "Morning";
			iconsrc = "/assets/permatalayout/assets/newlayout/img/sunrise.png";
		}
		else if(hr >= 12 && hr <= 18){
			greeting = "Afternoon";
			iconsrc = "/assets/permatalayout/assets/newlayout/img/cloudy.png";
		}
		else{
			greeting = "Evening";
			iconsrc = "/assets/permatalayout/assets/newlayout/img/cloudy-night.png";
		}

		document.getElementById("greetings").innerHTML=greeting;
		document.getElementById("timeicon").innerHTML='<img src="'+iconsrc+'" width="30"/> ';
	}
	
	window.onload=function(){
		setInterval("displaytime()", 1000)
	}