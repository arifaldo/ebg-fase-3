<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/MultiPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';

class multidebet_ImportController extends Application_Main
{
	protected $_moduleDB = 'RTF';			
	
	protected $_destinationUploadDir = '';
	protected $_acctArr = array();
	
	public function initController(){
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 	       	
		
		$Customer = new Customer($this->_custIdLogin,$this->_userIdLogin);
		$AccArr = $Customer->getAccounts();
		$this->view->AccArr =  $AccArr;
		$this->_acctArr = array_unique(Application_Helper_Array::simpleArray($AccArr,'ACCT_NO'));
		
		$this->view->ccyArr = $this->getCcy();
	}

	public function indexAction()
	{
		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		
		if($this->_request->isPost() )
		{	
			$filter = new Application_Filtering();
			$confirm = false;

			$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 			= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACBENEF 			= $filter->filter($this->_request->getParam('ACBENEF'), "ACCOUNT_NO");
			$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
			$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
			$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");

			$accLen = 10;	// fixed length of Bank Account 
			$error_msg[0] = "";
			
			if (Zend_Validate::is($ACBENEF, 'NotEmpty') == false)  
				$error_msg[0] = "Beneficiary Account cannot be left blank. Please correct it.";
			elseif (Zend_Validate::is($ACBENEF, 'Digits') == false)  
				$error_msg[0] = "Beneficiary Account must be numeric.";	
			elseif (strlen($ACBENEF) != $accLen)  
				$error_msg[0] = "Beneficiary Account length must be ".$accLen." digits.";	
			elseif ($ACBENEF_ALIAS == "")	
				$error_msg[0] = "Alias Name cannot be left blank. Please correct it.";
			elseif (strlen($ACBENEF_ALIAS) > 35)	
				$error_msg[0] = "Maximum lengths of Alias Name is 35 characters. Please correct it.";
			else if ($ACBENEF_CCY == "")	
				$error_msg[0] = "Currency cannot be left blank. Please correct it.";
			else if(!$PS_EFDATE)
				$error_msg[0] = "Payment Date can not be left blank.";
			else
			{			
				$paramSettingID = array('range_futuredate', 'auto_release_payment');
				
				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only
				
				$adapter = new Zend_File_Transfer_Adapter_Http();
				
				$adapter->setDestination ( $this->_destinationUploadDir );
				
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					'Extension file must be *.csv'
				);
				
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'File exceeds maximum size'
				);
				
				$adapter->setValidators ( array (		
					$extensionValidator, 
					$sizeValidator,
				));
				
				if ($adapter->isValid ()) 
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
					
					$adapter->addFilter ( 'Rename',$newFileName  );
						
					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end 
						
						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}
									
						if($totalRecords)
						{
							$rowNum = 0;
							
							$paramPayment = array( "CATEGORY"      		=> "MULTI DEBET",
												   "FROM"       		=> "I",
												   "PS_NUMBER"     		=> "",
												   "PS_SUBJECT"    		=> $PS_SUBJECT,
												   "PS_EFDATE"     		=> $PS_EFDATE,
												   "_dateFormat"    	=> $this->_dateDisplayFormat,
												   "_dateDBFormat"    	=> $this->_dateDBFormat,
												   "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
												   "_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
												   "_createPB"     		=> $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
												   "_createDOM"    		=> false,        // cannot create DOM trx
												   "_createREM"    		=> false,        // cannot create REM trx
												  );
							
							$paramTrxArr = array();
							
							foreach ( $csvData as $idx => $row ) 
							{
								if(count($row)==4)
								{
									$rowNum++;
									$sourceAcct = trim($row[0]);
									$amount = trim($row[1]);
									$message = trim($row[2]);
									$addMessage = trim($row[3]);
									
									$filter = new Application_Filtering();
									
									$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
									$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
									$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
									// $ACCTSRC 			= $filter->filter($sourceAcct, "ACCOUNT_NO");
									$ACCTSRC 			= $sourceAcct;
									$TRANSFER_TYPE 		= 'PB';
										
									$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
										
									$filter->__destruct();
									unset($filter);
														 
									$paramTrxArr[$idx-1] = array(	"TRANSFER_TYPE" 	=> $TRANSFER_TYPE,
														"TRA_AMOUNT" 		=> $TRA_AMOUNT_num,	
														"TRA_MESSAGE" 		=> $TRA_MESSAGE,	
														"TRA_REFNO" 		=> $TRA_REFNO,
														"ACCTSRC" 			=> $ACCTSRC,	
														"ACBENEF" 			=> $ACBENEF,	
														"ACBENEF_CCY" 		=> $ACBENEF_CCY,	
														"ACBENEF_EMAIL" 	=> '',		
								
													// for Beneficiary data, except (bene CCY and email), must be passed by reference
														"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,	
														"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
													//	"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
													//	"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,	
													//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,	
													//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,	
															
													//	"ORG_DIR" 					=> $ORG_DIR,
													//	"BANK_CODE" 				=> $CLR_CODE,
													//	"BANK_NAME" 				=> $BANK_NAME,
													//	"BANK_BRANCH" 				=> $BANK_BRANCH,
													//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
													//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
													//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
													 );	
									
								}	
								else
								{				
									$error_msg[0] = 'Wrong File Format';
									break;
								}	
							}
							
							// kalo gak ada error
							if(!$error_msg[0])
							{
								$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr);
								
								$payment 		= $validate->getPaymentInfo();
			
								if($validate->isError() === false)	// payment data is valid
								{
									$confirm = true;
								
									$validate->__destruct();
									unset($validate);
								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
									
									$validate->__destruct();
									unset($validate);
									
									if($errorMsg)
									{
										$error_msg[0] = $errorMsg;
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else //kalo total record = 0
						{
							$error_msg[0] = 'Wrong File Format. There is no data on csv File.';
						}	
					}
				} 
				else 
				{
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = 'File cannot be left blank. Please correct it.';
						else
							$error_msg[0] = $val;
						break;
					}
				}

			}
			
			if($confirm)
			{
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				
				$sessionNamespace = new Zend_Session_Namespace('confirmImportDebet');
				$sessionNamespace->content = $content;
				$this->_redirect('/multidebet/import/confirm');
			}
			
			$this->view->error 		= true;
			$error_msg[0] = 'Error: '.$error_msg[0];
			$this->view->report_msg	= $this->displayError($error_msg);
			
			$this->view->PSSUBJECT = $PS_SUBJECT; 
			$this->view->ACBENEF = $ACBENEF;			
			$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;			
			$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;			
			$this->view->CURR_CODE = $ACBENEF_CCY;			
			$this->view->PSEFDATE = $PS_EFDATE;
		}
	}
	
	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmImportDebet');
		$data = $sessionNamespace->content;
		//Zend_Debug::dump($data);die;
		
		foreach($data['paramTrxArr'] as $key=>$row)
		{
			if (in_array($row['ACCTSRC'], $this->_acctArr))
			{
				$acctData = $this->_db->fetchRow(
							$this->_db->select()
									  ->from('M_CUSTOMER_ACCT',array('ACCT_NAME','ACCT_ALIAS_NAME'))
									  ->where('ACCT_NO =?', $row['ACCTSRC'])
							);
				$acctName = $acctData['ACCT_NAME'];
				if($acctData['ACCT_ALIAS_NAME']) $acctName .= ' / '.$acctData['ACCT_ALIAS_NAME'];
			}
			else
			{
				$acctName = 'INVALID ACCOUNT';
			}
			
			$data['paramTrxArr'][$key]['ACCT_NAME'] = $acctName;
			$data['paramTrxArr'][$key]['REMARK'] = isset($data['errorTrxMsg']['PB'][$data['paramTrxArr'][0]['ACBENEF_CCY']][$key]) ? $data['errorTrxMsg']['PB'][$data['paramTrxArr'][0]['ACBENEF_CCY']][$key] : '';
		}
		
		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}
		
		// Zend_Debug::dump($data['paramTrxArr']);die;
		
		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->ACBENEF = $data['paramTrxArr'][0]['ACBENEF'];
		$this->view->ACBENEF_CCY = $data['paramTrxArr'][0]['ACBENEF_CCY'];
		$this->view->ACBENEF_BANKNAME = $data['paramTrxArr'][0]['ACBENEF_BANKNAME'];
		$this->view->ACBENEF_ALIAS = $data['paramTrxArr'][0]['ACBENEF_ALIAS'];
		
		$this->view->totalFailed = $totalFailed;
		$this->view->totalRecord = $data['payment']['countTrxTOTAL'];	
		$this->view->trxArr = $data['paramTrxArr'];	
		$this->view->totalAmount = $data['payment']['sumTOTAL'];	
		
		
		
		if($this->_request->isPost() )
		{			
			if($this->_getParam('submit') == 'Cancel')
			{
				unset($_SESSION['confirmImportDebet']); 
				$this->_redirect('/multidebet/import');
			}
			
			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= $this->_paymenttype['code']['multidebet'];
			$param['PS_CCY']  = $data['paramTrxArr'][0]['ACBENEF_CCY'];
			
			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
						'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
						'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
						'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
						'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'],
						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'],
						// 'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
						// 'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
						// 'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
						// 'CLR_CODE' 					=> $row['BANK_CODE'],
						'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'],
						'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
						'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
						'TRA_REFNO' 				=> $row['TRA_REFNO'],
				);
			}
			
			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = 'IPMO';
			 // Zend_Debug::dump($param);die;
			
			$MultiPayment = new MultiPayment("", $this->_custIdLogin, $this->_userIdLogin);
			
			$result = $MultiPayment->createPayment($param);
			if($result)
			{
				unset($_SESSION['confirmImportDebet']); 
				$this->_redirect('/notification/success');
			}
			else
			{				
				$this->view->error 		= true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/multidebet/import');
			}
		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;		
	}
}
