<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'CMD/Validate/Validate.php';

class escrow_SetController extends Application_Main
{
	
	public function indexAction() 
	{
		$getCcy = $this->getCcy();
		$getCcy = Application_Helper_Array::listArray($getCcy,'CCY_NUM','CCY_ID');
		$this->view->CCYData = $getCcy;
		$errorArray = array();
		$confirmPage  = $this->_getParam('confirmPage',0);

		if($this->_request->isPost())
		{
			$acct_no = $this->_getParam('acct_no','');
			$filters = array(
										'acct_no'        => array('StripTags','StringTrim'),
										'ccy_id'         => array('StripTags','StringTrim','StringToUpper'),
									);

			$validators =  array(
											'acct_no'       => array('NotEmpty',
																	 'Digits',
																	 array('StringLength',array('min'=>11,'max'=>16)),
																	 array('Db_NoRecordExists',array('table'=>'M_MASTER','field'=>'M_KEY2','exclude'=>'M_STATUS!=3')),
																	 array('Db_NoRecordExists',array('table'=>'TEMP_MASTER','field'=>'M_KEY2')),
																	 'messages' => array('Can not be empty',
																						 'Account Number must be numeric',
																						 'Maximum length of Account Number is 16 digits',
																						 'Account Number ['.$acct_no.'] already registered',
																						 'Account Number ['.$acct_no.'] already suggested! Please Approve or Reject previous suggestion first!',
																						)
																	),
										   'ccy_id'         => array('NotEmpty',
																	 array('inArray',array_keys($getCcy)),
																	 array('Db_NoRecordExists',array('table'=>'M_MASTER','field'=>'M_KEY1','exclude'=>'M_STATUS!=3')),
																	 array('Db_NoRecordExists',array('table'=>'TEMP_MASTER','field'=>'M_KEY1')),
																	  'messages' => array(
																						 'Can not be empty',
																						 'Please Select From The List',
																						 'Escrow Account for this currency has been registered.',
																						 'Escrow Account for this currency already suggested! Please Approve or Reject previous suggestion first!',
																						)
																	),
												);

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				//integrate ke core
				//get number from ccy_id
				$account    = new Service_Account($zf_filter_input->acct_no,$zf_filter_input->ccy_id);
				$result     = $account->accountInquiry();
				// $result = array(
						// 'ResponseCode'	=> '00',
						// 'ResponseDesc'	=> 'success',
						// 'AccountName'	=> 'Susiana Huang',
						// 'Currency'		=> 'IDR',
						// 'Balance'		=> '999999999.00',
						// 'AccountStatus'	=> '01',
						// 'AccountType'	=> '01',
						// 'CIF'			=> '123456',
					// );
				if($result['ResponseCode'] != '00') //00 = success
				{
					if($result['errorMessage']) $errorArray['acct_no'] = $result['errorMessage'];
					else $errorArray['acct_no'] = 'Error connection with Host';
				}
				else
				{
					$AcctStatus	= $result['AccountStatus'];
					$AcctName		= $result['AccountName'];
					$acctType		= $result['AccountType'];
				}
			}
		  //----------END validasi tambahan-------------------
		  
			if( $zf_filter_input->isValid() &&  empty($errorArray) )
			{
				$params['M_KEY']	=	'ESCROW';
				$params['M_TEXT1']	=	'Rekening Penampungan';
				$params['M_STATUS']	=	1;
				$params['M_KEY1']	=	$ccy_id       = $getCcy[$zf_filter_input->ccy_id];
				$params['M_KEY2']	=	$AcctNo      = $zf_filter_input->acct_no;
				$params['M_KEY3']	=	$AcctName;
				$params['M_KEY4']	=	$acctType;
				$params['M_CREATEDBY']	=	$this->_userIdLogin;
				$params['M_CREATED']		=	new Zend_Db_Expr('now()');
				$params['M_SUGGESTEDBY']	=	$this->_userIdLogin;
				$params['M_SUGGESTED']		=	new Zend_Db_Expr('now()');
				$info = 'Add Escrow Account : '.$AcctNo.' , Currency : '.$ccy_id;
				if($confirmPage == 1)
				{
					$this->_db->beginTransaction();
					//jika berhasil ke layar confirm :
					try{
						$change_id = $this->suggestionWaitingApproval('Escrow Account',$info,$this->_changeType['code']['new'],null,'M_MASTER','TEMP_MASTER',$AcctNo,$AcctName);
						$params['CHANGES_ID']	=	$change_id;
						$model = new escrow_Model_Escrow();
						$insert = $model->insertTemp($params);
						Application_Helper_General::writeLog('ECST','Set Escrow Account '.$AcctNo.' for currency '.$ccy_id);
						$this->_db->commit();
						$this->setbackURL('/'.$this->view->modulename);
						$this->_redirect('/notification/submited/index');
					}
					catch(Exception $e)
					{
						$this->_db->rollBack();
					}
				}
				else
				{
					$confirmPage = 1;
					$this->view->acct_no	= $zf_filter_input->acct_no; 
					$this->view->ccy_id	= $zf_filter_input->ccy_id;
				}
			}
			else
			{
				$this->view->error = 1;
				$this->view->acct_no      = ($zf_filter_input->isValid('acct_no'))? $zf_filter_input->acct_no : $this->_getParam('acct_no'); 
				$this->view->ccy_id       = ($zf_filter_input->isValid('ccy_id'))? $zf_filter_input->ccy_id : $this->_getParam('ccy_id'); 

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html 
				if(is_array($error))
				{
					foreach($error as $keyRoot => $rowError)
					{
						foreach($rowError as $errorString)
						{
							$errorArray[$keyRoot] = $errorString;
						}
					}
				}
			}
		}//if($this->_request->isPost())
		$this->view->err_msg  = json_encode($errorArray);
		$this->view->confirmPage  = $confirmPage;
	}

}