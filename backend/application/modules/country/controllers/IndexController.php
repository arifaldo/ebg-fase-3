<?php

require_once 'Zend/Controller/Action.php';

class Country_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$countrycode = $this->language->_('Country Code');
		$countryname = $this->language->_('Country Name');



	    $fields = array(
						'country_code'  => array('field' => 'COUNTRY_CODE',
											      'label' => $countrycode,
											      'sortable' => true),
						'country_name'     => array('field' => 'COUNTRY_NAME',
											      'label' => $countryname,
											      'sortable' => true),
				      );

	    $filterlist = array("COUNTRY_NAME");

		$this->view->filterlist = $filterlist;


		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','COUNTRY_NAME');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'COUNTRY_NAME'  => array('StringTrim','StripTags'),
		);

		$dataParam = array("COUNTRY_NAME");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		//$filter_lg = $this->_getParam('filter');

		$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_COUNTRY'),$getData);

		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf)
		if($filter == true || $csv || $pdf || $this->_request->getParam('print'))
		{
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fcountry_name = $zf_filter->getEscaped('COUNTRY_NAME');

	        if($fcountry_name) $select->where('UPPER(COUNTRY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fcountry_name).'%'));

			$this->view->country_name = $fcountry_name;
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);

		$select = $this->_db->fetchall($select);

	  $this->view->countryData = $select;

		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {
				$this->_helper->download->csv($header,$select,null,'Country List');
				Application_Helper_General::writeLog('COLS','Download CSV Country List');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$select,null,'Country List');
				Application_Helper_General::writeLog('COLS','Download PDF Country List');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Country List', 'data_header' => $fields));
       	}
		else
		{
				Application_Helper_General::writeLog('COLS','View Country List');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }

	}

}
