<?php
require_once 'Zend/Controller/Action.php';

class fprivilagetemplate_DetailController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ftemplateid = $this->_getParam('ftemplateid');
		// $ftemplateid = $this->xmlentities($ftemplateid);


		$modelPrivil = new fprivilagetemplate_Model_Fprivilagetemplate();
		$templatedesc = $modelPrivil->getTemplate($ftemplateid);
		$fpriviltem = $modelPrivil->getFpriviltem($ftemplateid);

		$arrType = array(""=>"---".$this->language->_('Please Select')."---",'F'=>"Frontend", 'B'=>"Backend");
		$arrSecure = array(""=>"---".$this->language->_('Please Select')."---",0=>"No", 1=>"Yes");

		$this->view->isbybo = $arrType;
		$this->view->secure = $arrSecure;

		$this->view->ftemid = $ftemplateid;
		$this->view->templatedesc = $templatedesc['FTEMPLATE_DESC'];
		$this->view->tmplgroup = $templatedesc['FTEMPLATE_GROUP'];
		$this->view->tmplsecurity = $templatedesc['FTEMPLATE_SECURE'];
		// $this->view->templatedesc = $templatedesc;
		$this->view->data = $fpriviltem;
		$this->view->modulename = $this->_request->getModuleName();
		Application_Helper_General::writeLog('PTLS','View Detail Frontend Privilege Template');
	}


	function xmlentities($string) {
  		 return str_replace ( array ( '&', '"', "'", '<', '>'), array ( '&amp;' , '&#034', '&#039' , '&lt;' , '&gt;' ), $string );
	}
}
