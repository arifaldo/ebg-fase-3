<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class transactionreport_LldreportdetailController extends Application_Main
{
  public function indexAction()
  {
  	$this->_helper->layout()->setLayout('newlayout');
  	$pdf = $this->_getParam('pdf');

  	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


	$AESMYSQL = new Crypt_AESMYSQL();
	$psnumber = $AESMYSQL->decrypt($this->_getParam('psnumber'), $password);

  	$getPaymentDetail 	= new transactionreport_Model_Transactionreport();
//   	print_r($psnumber);die;
  	$detail = $getPaymentDetail->getPaymentDetail($psnumber);
  	$history = $getPaymentDetail->getHistory($psnumber);
  	$paymentref = $detail[0]['PS_NUMBER'];

	if (Zend_Registry::isRegistered('language')){
			$language = Zend_Registry::get('language');
		}
	$lldrelation = array();
		$lldrelation["A"] = $language->_('Affiliated');
		$lldrelation["N"] = $language->_('Not Affiliated');

		$lldidenty = array();
		$lldidenty["I"] = $language->_('Sender is Identical with Beneficiary');
		$lldidenty["N"] = $language->_('Sender is Not Identical with Beneficiary');

//  	if($detail[0]['TRA_STATUS'] == '4'){
//  		$paystatus = 'Completed with 1 Failed Transaction(s)';
//  	}
  					if($detail[0]['PS_STATUS'] == '9'){
						$psStatus = 'Exception';
					}
					elseif($detail[0]['PS_STATUS'] == '6'){
						$psStatus = 'Transfer Failed';
					}
					elseif($detail[0]['PS_STATUS'] == '4'){
						$psStatus = 'Rejected';
					}
					elseif($detail[0]['PS_STATUS'] == '7'){
						$psStatus = 'Pending Future Date';
					}
					elseif($detail[0]['PS_STATUS'] == '8'){
						$psStatus = 'In Progress';
					}
					elseif($detail[0]['PS_STATUS'] == '13'){
						$psStatus = 'Pending Process';
					}
					elseif($detail[0]['PS_STATUS'] == '14'){
						$psStatus = 'Cancel Future Date';
					}
				  	else{
				  		$paystatus = $detail[0]['PS_STATUS'];
				  	}


  	$source = $detail[0]['ACCT_SOURCE'];
  	$transmission = '';
  	$traceno = '';
  	$challenge = $detail[0]['PS_RELEASER_CHALLENGE'];
  	$created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
  	$updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
  	$efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
  	$amount = $detail[0]['PS_CCY'].' '.Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']);
  	$type = $detail[0]['PS_TYPE'];
  	$cektype = $detail[0]['CEK_PS_TYPE'];

  	$paymentref_lg = $this->language->_('Payment Ref');
   	$paymentstatus = $this->language->_('Payment Status');
   	$sourceacc = $this->language->_('Source Account');
   	$transmission_lg = $this->language->_('Transmission');
   	$traceno_lg = $this->language->_('Trace No.');
   	$challengecode_lg = $this->language->_('Challenge Code');
   	$createddate = $this->language->_('Created Date');
   	$updateddate = $this->language->_('Updated Date');
   	$totalpayment = $this->language->_('Total Payment');
   	$paymenttype = $this->language->_('Payment Type');
   	$paymentdate = $this->language->_('Payment Date');
   	$transactionref = $this->language->_('Transaction Ref');
   	$benerficiaryaccount = $this->language->_('Beneficiary Account');
   	$beneficiaryname = $this->language->_('Beneficiary Name');
   	$bank = $this->language->_('Bank');
   	$citizenship_lg = $this->language->_('Citizenship');
   	$currency = $this->language->_('Currency');
   	$amount_lg = $this->language->_('Amount');
   	$charge = $this->language->_('Charge');
   	$message = $this->language->_('Message');
   	$transfertype = $this->language->_('Transfer Type');
   	$status = $this->language->_('Status');
   	$response = $this->language->_('Response');
   	$userid = $this->language->_('Company ID');
   	$action = $this->language->_('Action');
   	$datetime = $this->language->_('Date / Time');
   	$reason = $this->language->_('Reason');
   	$datetime1 = $this->language->_('Date Time');
   	$lldcontent = $this->language->_('LLD Content');

   	$categorycentrabank = $this->language->_('Content');


	// TEMPLATE DETAIL

		$pslipdetail = $getPaymentDetail->getPslipDetailLld($psnumber);
//		echo '<pre>';
 //		print_r($pslipdetail);die;
		$htmldataDetailDetail = '';
		foreach($pslipdetail as $pslipdetaillist)
		{
		   	if($pslipdetaillist['PS_FIELDTYPE'] == 1)
		   	{
		   		$value = Application_Helper_General::displayMoney($pslipdetaillist['PS_FIELDVALUE']);
		   	}
			elseif($pslipdetaillist['PS_FIELDTYPE'] == 2)
		   	{
		   		$value = Application_Helper_General::convertDate($pslipdetaillist['PS_FIELDVALUE'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		   	}
		   	elseif($pslipdetaillist['PS_FIELDTYPE'] == 3)
		   	{
		   		$value = $pslipdetaillist['PS_FIELDVALUE'];
		   	}
		   	else
		   	{
		   		$value = '';
		   	}




		   	$htmldataDetailDetail .='
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_($pslipdetaillist['PS_FIELDNAME']).'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$this->language->_($value).'</td>
			</tr>';
		}


		$pdetail = $pslipdetail['0'];
		if(!empty($pdetail['ACCT_CITIZENSHIP']) && $pdetail['ACCT_CITIZENSHIP'] == 'W'){
		$sender_country = 'WNI';
		}else{
			$sender_country = 'WNA';
		}

		if($pdetail['ACCT_RESIDENT'] == 'R')
		{
			$scitizenship = 'Resident';
		}
		elseif($pdetail['ACCT_RESIDENT'] == 'NR')
		{
			$scitizenship = 'Non Resident';
		}
		else
		{
			$scitizenship = '';
		}
		if(!empty($pdetail['BENEFICIARY_CITIZENSHIP']) && $pdetail['BENEFICIARY_CITIZENSHIP'] == 'W'){
		$bene_country = 'WNI';
		}else{
			$bene_country = 'WNA';
		}
		if($pdetail['BENEFICIARY_RESIDENT'] == 'R')
		{
			$bcitizenship = 'Resident';
		}
		elseif($pdetail['BENEFICIARY_RESIDENT'] == 'NR')
		{
			$bcitizenship = 'Non Resident';
		}
		else
		{
			$bcitizenship = '';
		}

		if($pdetail['ACCT_CATEGORY'] == 1)
			$pdetail['ACCT_CATEGORY'] = 'Individual';
		elseif($pdetail['ACCT_CATEGORY'] == 2)
		$pdetail['ACCT_CATEGORY'] = 'Government';
		elseif($pdetail['ACCT_CATEGORY'] == 3)
		$pdetail['ACCT_CATEGORY'] = 'Bank';
		elseif($pdetail['ACCT_CATEGORY'] == 4)
		$pdetail['ACCT_CATEGORY'] = 'Financial Institution Non Bank';
		elseif($pdetail['ACCT_CATEGORY'] == 5)
		$pdetail['ACCT_CATEGORY'] = 'Company';
		elseif($pdetail['ACCT_CATEGORY'] == 6)
		$pdetail['ACCT_CATEGORY'] = 'Other';


		if($pdetail['BENEFICIARY_CATEGORY'] == 1)
			$pdetail['BENEFICIARY_CATEGORY'] = 'Individual';
		elseif($pdetail['BENEFICIARY_CATEGORY'] == 2)
		$pdetail['BENEFICIARY_CATEGORY'] = 'Government';
		elseif($pdetail['BENEFICIARY_CATEGORY'] == 3)
		$pdetail['BENEFICIARY_CATEGORY'] = 'Bank';
		elseif($pdetail['BENEFICIARY_CATEGORY'] == 4)
		$pdetail['BENEFICIARY_CATEGORY'] = 'Financial Institution Non Bank';
		elseif($pdetail['BENEFICIARY_CATEGORY'] == 5)
		$pdetail['BENEFICIARY_CATEGORY'] = 'Company';
		elseif($pdetail['BENEFICIARY_CATEGORY'] == 6)
		$pdetail['BENEFICIARY_CATEGORY'] = 'Other';
		// print_r($pdetail);
		$selectpurpose	= $this->_db->select()
		->from(	array('P' => 'M_TRANSACTION_PURPOSE'),array('*'))
		//
		->where("P.CODE = ? ", $pdetail['LLD_TRANSACTION_PURPOSE']);
		// echo $selectpurpose;die;
		if(!empty($pdetail['LLD_TRANSACTION_PURPOSE'])){
			$purpose = $this->_db->fetchRow($selectpurpose);
		}else{
			$purpose = array();
		}

		// print_r($purpose);die;
		$htmldataDetail =
		'<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td class="tbl-evencontent" style="width:200px !important;">&nbsp; '.$this->language->_('Company ID').'</td>
				<td class="tbl-evencontent" style="width:10px !important;">:</td>
				<td class="tbl-evencontent">'.$pdetail['CUST_ID'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Company').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['CUST_NAME'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Source Account').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['SOURCE_ACCOUNT'].' ('.$pdetail['SOURCE_ACCOUNT_CCY'].')</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Source Account Name').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['SOURCE_ACCOUNT_NAME'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Currency').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['PS_CCY'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Amount').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.Application_Helper_General::displayMoney($pdetail['PS_TOTAL_AMOUNT']).'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Sender Citizenship').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$scitizenship.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Sender Nationality').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$sender_country.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Sender Category').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['ACCT_CATEGORY'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Sender Identification Type').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['ACCT_ID_TYPE'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Sender Identification Number').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['ACCT_ID_NUMBER'].'</td>
			</tr>


			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Account').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['BENEFICIARY_ACCOUNT'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Name').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['BENEFICIARY_ACCOUNT_NAME'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Citizenship').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$bcitizenship.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Nationality').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$bene_country.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Category').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['BENEFICIARY_CATEGORY'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Identification Type').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['BENEFICIARY_ID_TYPE'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Identification Number').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$pdetail['BENEFICIARY_ID_NUMBER'].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Identity').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$lldidenty[$pdetail['LLD_IDENTITY']].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Transactor Relationship').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$lldrelation[$pdetail['LLD_TRANSACTOR_RELATIONSHIP']].'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Transactor Purpose').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$purpose['DESCRIPTION'].'</td>
			</tr>
		</table>';


	// TEMPLATE TRANSACTION
	if($cektype != 110 && $cektype != 120)
	{
		$i = 1;
		$htmldataTransactionDetail = '';
		foreach($detail as $transactionlist)
		{
		   	$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';

		   	if($transactionlist['BENEFICIARY_RESIDENT'] == 'R')
		   	{
		   		$citizenship = 'Resident';
		   	}
			elseif($transactionlist['BENEFICIARY_RESIDENT'] == 'NR')
		   	{
		   		$citizenship = 'Non Resident';
		   	}
		   	else
		   	{
		   		$citizenship = '';
		   	}
		   	// 9. Create LLD string
			$settings 			= new Application_Settings();
			$LLD_array 			= array();
			$LLD_DESC_arrayCat 	= array();
			$lldTypeArr  		= $settings->getLLDDOMType();

			if (!empty($transactionlist['BENEFICIARY_CATEGORY']))
			{
				$lldCategoryArr  	= $settings->getLLDDOMCategory();
				$LLD_array["CT"] 	= $transactionlist['BENEFICIARY_CATEGORY'];
				$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$transactionlist['BENEFICIARY_CATEGORY']];
			}

			if (!empty($transactionlist['BENEFICIARY_ID_TYPE']))
			{
				$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
				$LLD_array["CT"] 	= $transactionlist['BENEFICIARY_ID_TYPE'];
				$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$transactionlist['BENEFICIARY_ID_TYPE']];
			}

// 			$getBeneficiary = new cmdreport_Model_Cmdreport();
			$CITY_CODEGet = (!empty($transactionlist['BENEFICIARY_CITY_CODE'])?$transactionlist['BENEFICIARY_CITY_CODE']:'');
			$arr 					= $getPaymentDetail->getCityCode($CITY_CODEGet);
		   	if($type == 'Others'){

			$htmldataTransactionDetail .='
			<tr>
				<td class="'.$td_css.'">'.$transactionlist['TRANSACTION_ID'].'</td>
				<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_ACCOUNT'].'</td>';
				//<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_ACCOUNT_NAME'].' ('.$transactionlist['BENEFICIARY_ALIAS_NAME'].')</td>
			$htmldataTransactionDetail .='
				<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_ACCOUNT_NAME'].'</td>

				<td class="'.$td_css.'">'.$LLD_CATEGORY_POST.'</td>
				<td class="'.$td_css.'">'.$LLD_BENEIDENTIF_POST.'</td>
				<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_ID_NUMBER'].'</td>
				<td class="'.$td_css.'">'.$arr[0]['CITY_NAME'].'</td>

				<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_BANK_NAME'].'</td>
				<td class="'.$td_css.'">'.$citizenship.'</td>
				<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_ACCOUNT_CCY'].'</td>
				<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']).'</td>
				<td class="'.$td_css.'">'.$transactionlist['TRA_MESSAGE'].'</td>
				<td class="'.$td_css.'">'.$this->language->_($transactionlist['TRANSFER_TYPE']).'</td>
				<td class="'.$td_css.'">'.$this->language->_($transactionlist['TRANSFER_STATUS']).'</td>
				<td class="'.$td_css.'">'.$transactionlist['BANK_RESPONSE'].'</td>
			</tr>


			';




		   	}
		   	else{

			   	if($cektype == '16'){ //type purchase
			   		$BENEFICIARY_ACCOUNT = '-';
			   		$BENEFICIARY_ACCOUNT_NAME = '-';
			   	}
			   	elseif($cektype == '17'){ //payment
			   		$BENEFICIARY_ACCOUNT = '-';
			   		$BENEFICIARY_ACCOUNT_NAME = '-';
			   	}
			   	else{
			   		$BENEFICIARY_ACCOUNT = $transactionlist['BENEFICIARY_ACCOUNT'];
			   		//$BENEFICIARY_ACCOUNT_NAME = $transactionlist['BENEFICIARY_ACCOUNT_NAME'].' ('.$transactionlist['BENEFICIARY_ALIAS_NAME'].')';
			   		$BENEFICIARY_ACCOUNT_NAME = $transactionlist['BENEFICIARY_ACCOUNT_NAME'];
			   	}

			$htmldataTransactionDetail .='
			<tr>
				<td class="'.$td_css.'">'.$transactionlist['TRANSACTION_ID'].'</td>
				<td class="'.$td_css.'">'.$BENEFICIARY_ACCOUNT.'</td>
				<td class="'.$td_css.'">'.$BENEFICIARY_ACCOUNT_NAME.'</td>
				<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_BANK_NAME'].'</td>



				<td class="'.$td_css.'">'.$citizenship.'</td>
				<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_ACCOUNT_CCY'].'</td>
				<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']).'</td>
				<td class="'.$td_css.'">'.$transactionlist['TRA_MESSAGE'].'</td>
				<td class="'.$td_css.'">'.$this->language->_($transactionlist['TRANSFER_TYPE']).'</td>
				<td class="'.$td_css.'">'.$this->language->_($transactionlist['TRANSFER_STATUS']).'</td>
				<td class="'.$td_css.'">'.$transactionlist['BANK_RESPONSE'].'</td>
			</tr>
			';

		   	}

			$i++;
		}
		if(empty($transactionlist))
		{
			$htmldataTransactionDetail ='
			<tr>
				<td class="tbl-evencontent" colspan="5" align="center">--- No Data ---</td>
			</tr>';
		}

		if($type == 'Others'){
		$htmldataTransaction =
		'<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<th valign="top" class="tdform-sub">'.$transactionref.'#</th>
				<th valign="top" class="tdform-sub">'.$benerficiaryaccount.'</th>
				<th valign="top" class="tdform-sub">'.$beneficiaryname.'</th>

				<th valign="top" class="tdform-sub">'.$this->language->_('Beneficiary Category').'</th>
				<th valign="top" class="tdform-sub">'.$this->language->_('Beneficiary Identification Type').'</th>
				<th valign="top" class="tdform-sub">'.$this->language->_('Beneficiary Identification Number').'</th>
				<th valign="top" class="tdform-sub">'.$this->language->_('Beneficiary City').'</th>

				<th valign="top" class="tdform-sub">'.$bank.'</th>
				<th valign="top" class="tdform-sub">'.$citizenship_lg.'</th>
				<th valign="top" class="tdform-sub">'.$currency.'</th>
				<th valign="top" class="tdform-sub">'.$amount_lg.'</th>
				<th valign="top" class="tdform-sub">'.$message.'</th>
				<th valign="top" class="tdform-sub">'.$transfertype.'</th>
				<th valign="top" class="tdform-sub">'.$status.'</th>
				<th valign="top" class="tdform-sub">'.$response.'</th>
			</tr>'
			.$htmldataTransactionDetail.'
		</table>

		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<th valign="top" class="tdform-sub">'.$transactionref.'#</th>
				<th valign="top" class="tdform-sub">'.$lldcontent.'</th>

			</tr>

				<tr>
				<td class="'.$td_css.'">'.$transactionlist['TRANSACTION_ID'].'</td>
				<td class="'.$td_css.'">Category:Central Bank;Identity:Sender
				is Identical with Beneficiary;Transactor Relationship:Affiliated;
				Transaction Purpose:Goods / Service
				</td>
			</tr>
		</table>'

		;
		}
		else{
		$htmldataTransaction =
		'<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<th valign="top">'.$transactionref.'#</th>
				<th valign="top">'.$benerficiaryaccount.'</th>
				<th valign="top">'.$beneficiaryname.'</th>
				<th valign="top">'.$bank.'</th>
				<th valign="top">'.$citizenship_lg.'</th>
				<th valign="top">'.$currency.'</th>
				<th valign="top">'.$amount_lg.'</th>
				<th valign="top">'.$message.'</th>
				<th valign="top">'.$transfertype.'</th>
				<th valign="top">'.$status.'</th>
				<th valign="top">'.$response.'</th>

			</tr>'
			.$htmldataTransactionDetail.'
		</table>

		';

		}

	}


 	// TEMPLATE HISTORY
	{
		$i = 1;
		$htmldataHistoryDetail = '';
		foreach($history as $historylist)
		{
		   	$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';

			$htmldataHistoryDetail .='
			<tr>
				<td class="'.$td_css.'">'.$historylist['CUST_ID'].'</td>
				<td class="'.$td_css.'">'.$this->language->_($historylist['HISTORY_STATUS']).'</td>
				<td class="'.$td_css.'">'.Application_Helper_General::convertDate($historylist['DATE_TIME'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat).'</td>
				<td class="'.$td_css.'">'.$historylist['PS_REASON'].'</td>
			</tr>
			';
			$i++;
		}
		if(empty($history))
		{
			$htmldataHistoryDetail ='
			<tr>
				<td class="tbl-evencontent" colspan="5" align="center">--- No Data ---</td>
			</tr>';
		}

		$htmldataHistory =
		'<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<th valign="top">'.$userid.'</th>
				<th valign="top">'.$action.'</th>
				<th valign="top">'.$datetime.'</th>
				<th valign="top">'.$reason.'</th>
			</tr>'
			.$htmldataHistoryDetail.'
		</table>';
	}
	if(isSet($htmldataDetail))$this->view->templateDetail = $htmldataDetail;
	if(isSet($htmldataTransaction))$this->view->templateTransaction = $htmldataTransaction;
	if(isSet($htmldataHistory))$this->view->templateHistory = $htmldataHistory;

		if($pdf)
		{

			Application_Helper_General::writeLog('RPPY','Download PDF LLD Report Detail ('.$psnumber.')');

			if($cektype == 11 && $cektype == 12)
			{
// 				echo "here";
// 				print_r($htmldataDetail);die;
				$datapdf 	=
						$htmldataDetail."
						<br />
						<br />
						<h2>History</h2>
						".$htmldataHistory;
			}
			else
			{
// 				echo "here1";
// 				print_r($htmldataDetail);die;
// 				print_r($expression)
				$datapdf 	=
						$htmldataDetail;
			}

			$datapdf = "<tr><td>".$datapdf."</td></tr>";
			//print_r($datapdf);die;
			$this->_helper->download->pdf(null,null,null,'LLD Report Detail',$datapdf);
		}
		else if($this->_request->getParam('print') == 1){
			//die;
			//print_r($cektype);die;
			if($cektype == 11 && $cektype == 12)
			{
				$dataprint 	=
						$htmldataDetail."
						<br />
						<br />
						<h2>History</h2>
						".$htmldataHistory;
			}
			else
			{
				$dataprint 	=
						$htmldataDetail;
			}
			//print_r($dataprint);die;
			$dataprint = "<tr><td>".$dataprint."</td></tr>";

			$this->_forward('print', 'index', 'widget', array('data_content' => $dataprint, 'data_caption' => 'LLD Report Detail', 'data_triger' => 'paymentreportdetail'));
			Application_Helper_General::writeLog('RPPY','Download PDF LLD Report Detail ('.$psnumber.')');
		}
		else
		{
			Application_Helper_General::writeLog('RPPY','View LLD Report Detail ('.$psnumber.')');
		}
	}
}
