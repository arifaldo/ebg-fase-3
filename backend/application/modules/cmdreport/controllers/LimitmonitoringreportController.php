<?php
require_once 'Zend/Controller/Action.php';

class Cmdreport_limitmonitoringreportController extends cmdreport_Model_Report
{	
	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{	
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->customer_msg = $msg;
			}	
		}
		
		//$usdexchange	= $this->_getParam('USDEXCHANGE');
		//$usdexchangeM	= Application_Helper_General::displayMoney($usdexchange);
				
		$arrPrincipal 	= $this->getAllCustomer();
		$arrCommunity 	= $this->getAllCommunity();
		$arrLimit 		= $this->getLimitStatus();
				
		$this->view->arrPrincipal 	= $arrPrincipal;
		$this->view->arrCommunity 	= $arrCommunity;
		$this->view->arrLimit 		= $arrLimit;
		$this->view->USDEXCHANGE	= $fusdexchange;
		
		$fields = array('commname' 	=> array(
												'field' => 'commname',
												'label' => 'Community',
												'sortable' => true
											),
						'anchorname'=> array(	
												'field' => 'anchorname',
												'label' => 'Principal',
												'sortable' => true
											),						
						);
		
		$fieldselect = array	(	
							'commname' 				=> array(
															'field' => 'commname',
															'label' => 'Community',
															'sortable' => true
														),
							'anchorname'  			=> array(	
															'field' => 'anchorname',
															'label' => 'Principal',
															'sortable' => true
														),
							
							'limitcomm'  			=> array(
															'field' => 'limitcomm',
															'label' => 'Limit',
															'sortable' => false
														),
							'uticomm'  				=> array(
															'field' => 'uticomm',
															'label' => 'Utilization',
															'sortable' => false
														),
							'remainlimit'  			=> array(
															'field' => 'remainlimit',
															'label' => 'Remain Limit',
															'sortable' => false
														),
							'statuscomm'  			=> array(
															'field' => 'statuscomm',
															'label' => 'Status',
															'sortable' => false
														),						
							'limitcompany'  		=> array(
															'field' => 'limitcompany',
															'label' => 'Limit',
															'sortable' => false
														),
							'uticompany'  			=> array(
															'field' => 'uticompany',
															'label' => 'Utilization',
															'sortable' => false
														),
							'remainlimitcompany'  	=> array(
															'field' => 'remainlimitcompany',
															'label' => 'Remain Limit',
															'sortable' => false
														),
							'statuscompany'  			=> array(
															'field' => 'statuscompany',
															'label' => 'Status',
															'sortable' => false
														),			
						);
			
			//validasi page, jika input page bukan angka               
			$page 		= $this->_getParam('page');
			$csv 		= $this->_getParam('csv');
			$pdf 		= $this->_getParam('pdf');
			
			$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
			
			//validasi sort, jika input sort bukan ASC atau DESC
			$sortBy  = $this->_getParam('sortby');
			$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
			$sortDir = $this->_getParam('sortdir');
			$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
			
			$this->view->currentPage 	= $page;
			$this->view->sortBy 		= $sortBy;
			$this->view->sortDir 		= $sortDir;
			
			$filterArr = array(	'PRINCIPALCODE'		=>array('StripTags','StringTrim','StringToUpper'),
								'PRINCIPALNAME'		=>array('StripTags','StringTrim'),
								
								'COMMUNITYCODE'		=>array('StripTags','StringTrim','StringToUpper'),
								'COMMUNITYNAME'		=>array('StripTags','StringTrim'),
								
								'LIMITSTATUS'		=>array('StripTags','StringTrim'),
								);
								
			// if POST value not null, get post, else get param
			$dataParam = array("PRINCIPALCODE","PRINCIPALNAME","COMMUNITYCODE","COMMUNITYNAME","LIMITSTATUS","USDEXCHANGE");
			$dataParamValue = array();
			foreach ($dataParam as $dtParam)
			{
				$dataPost = $this->_request->getPost($dtParam);
				$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
			}	
			
			// The default is set so all fields allow an empty string		
			$options = array('allowEmpty' => true);
			$validators = array(
									'PRINCIPALCODE' 	=> array(array('InArray', array('haystack' => array_keys($arrPrincipal)))),	
									'PRINCIPALNAME' 	=> array(),	
									
									'COMMUNITYCODE' 	=> array(array('InArray', array('haystack' => array_keys($arrCommunity)))),
									'COMMUNITYNAME' 	=> array(),	
									
									'LIMITSTATUS'		=> array(array('InArray', array('haystack' => array_keys($arrLimit)))),
									'USDEXCHANGE'		=> array(),
									
								);
						
			$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
			$filter 	= $this->_getParam('filter');
			$csv 		= $this->_getParam('csv');
			$pdf 		= $this->_getParam('pdf');
						
			$fPRINCIPALCODE  	= $zf_filter->getEscaped('PRINCIPALCODE');	
			$fPRINCIPALNAME  	= $zf_filter->getEscaped('PRINCIPALNAME');	
			
			$fCOMMUNITYCODE 	= $zf_filter->getEscaped('COMMUNITYCODE');	
			$fCOMMUNITYNAME  	= $zf_filter->getEscaped('COMMUNITYNAME');	
						
			$fLIMITSTATUS  		= $zf_filter->getEscaped('LIMITSTATUS');
			$fusdexchange  		= $zf_filter->getEscaped('USDEXCHANGE');
			
			$select = $this->_db->select()
								
								->from		(array('C'=>'M_COMMUNITY'),
																array(	'commcode'=>'COMMUNITY_CODE',
																		'commname'=>'COMMUNITY_NAME',
																		'limitcomm'=>'COMM_LIMIT_AMT'))
								->joinLeft	(array('A'=>'M_ANCHOR'), 'A.ANCHOR_ID = C.ANCHOR_ID',
																array(	'anchorid'	=>'ANCHOR_ID',
																		'anchorcust'=>'ANCHOR_CUST',
																		'limitcompany'=>'LIMIT_CORP_AMT'))
								->joinLeft	(array('CS'=>'M_CUSTOMER'), 'A.ANCHOR_CUST = CS.CUST_ID',
																array(	'custid'=>'CUST_ID',
																		'anchorname'=>'CUST_NAME'))
								->where("	
											C.COMMUNITY_STATUS 	!= '3'
											AND A.ANCHOR_STATUS != '3'
											AND CS.CUST_STATUS 	!= '3'
									");
							
								
								//->where("CS.CUST_ID = 'ELEGANZ4'");
								
			
			//ANCHOR_ID,CS.CUST_NAME
			if($fPRINCIPALCODE)	{$select->where('UPPER(A.ANCHOR_CUST) ='.$this->_db->quote(strtoupper($fPRINCIPALCODE)));
				//echo "<pre>";
				//echo $select->__toString();
				//die();
			}
			if($fPRINCIPALNAME)	$select->where('UPPER(CS.CUST_NAME) LIKE '.$this->_db->quote(strtoupper('%'.$fPRINCIPALNAME.'%')));
			
			//COMMUNITY_CODE, COMMUNITY_NAME, COMMUNITY_STATUS
			if($fCOMMUNITYCODE)	$select->where('UPPER(C.COMMUNITY_CODE) = '.$this->_db->quote(strtoupper($fCOMMUNITYCODE)));
			if($fCOMMUNITYNAME)	$select->where('UPPER(C.COMMUNITY_NAME) LIKE '.$this->_db->quote(strtoupper('%'.$fCOMMUNITYNAME.'%')));
			
			if($fLIMITSTATUS)	{				
				/*
					$arrLimit[1] = 'Available';
					$arrLimit[2] = 'Exceed';
					$arrLimit[3] = 'Full';
				*/
				if($fLIMITSTATUS == 1) $limvar = 'Available';
				else if($fLIMITSTATUS == 2) $limvar = 'Exceed';
				else $limvar = 'Full';
				
			}
			//else{ $sortlimit = false; }
			
			
			$select->order($sortBy.' '.$sortDir);
			$dataSQL = $this->_db->fetchAll($select);
			
			//echo "<pre>";
			//echo $select->__toString();
			//echo "<br />Data sql: ";
			//print_r($dataSQL);
			//die();
			//Zend_Debug::Dump($dataSQL); die;
			
			/*
				25 May 2012 11:57:54:050AM
				Application_Helper_General::convertDate($row['LOG_DATE'],$this->displayDateTimeFormat,$this->defaultDateFormat)
			
			*/
			
			$data = array();
			if($fusdexchange){
				
				// echo $fusdexchange;
				// die;
				
				$xxx = str_replace('.','',$fusdexchange);
				
				if(!empty($dataSQL) && count($dataSQL) > 0){
					foreach ($dataSQL as $d => $dt)
					{						
						$cc 			= $dt['commcode'];
						$limitcomm 		= $dt['limitcomm'];
						$limitcompany 	= $dt['limitcompany'];
						$anchorid 		= $dt['anchorid'];
						$anchorcust 	= $dt['anchorcust'];
										
						foreach ($fieldselect as $key => $field)
						{				
							$value = $dt[$key];					
							if ($key == "anchorname"){ $value = $dt['custid'].' - '.$dt['anchorname']; }				
							else if ($key == "commname"){ $value = $dt['commcode'].' - '.$dt['commname']; }				
							else if ($key == "limitcompany" && !$csv){	$value = Application_Helper_General::displayMoney($value); }
							else if ($key == "limitcomm" && !$csv){	$value = Application_Helper_General::displayMoney($value); }
							
							else if ($key == "uticomm"){ 
								if($cc){
									//select from M_COMM_ACCT
									
									// $selectacc = $this->_db->select()
															// ->distinct()
															// ->from(	array('A'=>'M_COMM_ACCT'),
																	// array('accno'=>'ACCT_NO','acctccy'=>'ACCT_CCY'))
															// ->joinLeft(array('M'=>'M_MEMBER'),'A.CODE = M.MEMBER_CODE',array())
															// ->where("A.ACCT_CCY = 'IDR' OR A.ACCT_CCY = 'USD'")
															// ->where('A.ACCT_OWNER =? ','M')
															// ->where('A.ACCT_SOURCE =? ','1')
															// ->where('M.COMMUNITY_CODE =? ',$cc);
									
									$selectacc = $this->_db->select()
														->distinct()
														->from(array('CA'=>'M_COMM_ACCT'),array(
																								'acctno'	=>'ACCT_NO',
																								'acctccy'	=>'ACCT_CCY'
																								
																								))
														->where("
															CA.CODE IN(
																SELECT 	MEMBER_CODE AS memmbercode
																FROM	M_MEMBER AS MM
																WHERE	MM.MEMBER_CODE IN 
																			(	SELECT		M.MEMBER_CODE AS mcode
																				FROM		M_MEMBER AS M
																				LEFT JOIN	M_COMMUNITY AS C 	ON M.COMMUNITY_CODE = C.COMMUNITY_CODE
																				WHERE		C.COMMUNITY_CODE = '$cc'
																							AND C.COMMUNITY_STATUS != '3'
																							AND M.MEMBER_STATUS != '3'
																			)
																		
																		
															)
														
														")
														->where('CA.ACCT_OWNER =?','M')
														->where('CA.ACCT_SOURCE =?','1')
														->where("CA.ACCT_CCY = 'IDR' OR CA.ACCT_CCY = 'USD'");
									
									// Zend_Debug::Dump($selectacc); die;			
									$dataacc = $this->_db->fetchAll($selectacc);
									
									// echo "<pre>";
									// print_r($dataacc);
									
									// echo "<pre>";
									// echo $selectacc->__toString();
									// die();
									// echo "<br />";
									
									$tot = 0;
									if(is_array($dataacc) && count($dataacc) > 0){
										
										// echo 'Community code: '.$cc.'<br />';
										// echo "<pre>";
										// print_r($dataacc);
										// echo "<br />";
										// die;
										
										foreach($dataacc as $keyacc => $accfield){
											$accno = $accfield["acctno"];
											$acctccy = $accfield["acctccy"];
											
											$selectsum = $this->_db->select()
																	->from(array('LIST_ACCT_INFO'),array('sumplafond'=>'sum(PLAFOND)'))
																	->where('ACCOUNT_NO= ?',$accno)
																	->where('CCY= ?',$acctccy);
											$resultsum = $this->_db->fetchRow($selectsum);
											
											//echo $acctccy;
											
											if($acctccy == 'IDR'){
												$utilizationcomm = $resultsum["sumplafond"];
											}
											else {
												$utilizationcomm = ($resultsum["sumplafond"]) * $xxx;
												//echo $resultsum["sumplafond"];
											}
											$tot = $tot + $utilizationcomm;
											
											//echo "accno: $accno, sum: $rr";
											
										}
									} else $tot = 0;
									
								}
								else $tot = 0;
								
								if(!$csv) $value = Application_Helper_General::displayMoney($tot); 
								else $value = $tot;
								
							}
							else if ($key == "remainlimit"){
								$remain = $limitcomm - $tot;
							
								if(!$csv) $value = Application_Helper_General::displayMoney($remain);			
								else $value = $remain;
							}
							
							else if ($key == "statuscomm"){	
								if($remain > 0) $stat = 'Available';
								else if( $remain < 0) $stat = 'Exceed';
								else $stat = 'Full';
								
								$value = $stat; 
							}
							
							else if($key == "uticompany"){
								
								/*
									1. List community -> member -> sum plafont smua community.
									anchor punya 2 community, 
									
									2. Duplicate: 1 member join 2 community
									list member -> account -> list account info (plafond) sum.
									if(account member dipake di community lain) 
								
								*/
								
								//select from M_COMM_ACCT LIMIT COMPANY
								
								if($anchorcust){
								
									$selectA = $this->_db->select()
														->distinct()
														->from(array('CA'=>'M_COMM_ACCT'),array(
																								'acctno'=>'ACCT_NO',
																								'acctccy'=>'ACCT_CCY'
																								))
														->where("
															CA.CODE IN(
																SELECT 	MEMBER_CODE AS memmbercode
																FROM	M_MEMBER AS MM
																WHERE	MM.MEMBER_CODE IN 
																			(	SELECT		M.MEMBER_CODE AS mcode
																				FROM		M_MEMBER AS M
																				LEFT JOIN	M_COMMUNITY AS C 	ON M.COMMUNITY_CODE = C.COMMUNITY_CODE
																				LEFT JOIN	M_ANCHOR AS A 		ON C.ANCHOR_ID = A.ANCHOR_ID
																				WHERE		A.ANCHOR_CUST = '$anchorcust'
																							AND C.COMMUNITY_STATUS != '3'
																							AND M.MEMBER_STATUS != '3'
																							AND A.ANCHOR_STATUS != '3'
																			)
															)
														
														")
														->where('CA.ACCT_OWNER =?','M')
														->where('CA.ACCT_SOURCE =?','1')
														->where("CA.ACCT_CCY = 'IDR' OR CA.ACCT_CCY = 'USD'");
									//echo "<pre>";
									// echo $selectA->__toString();
									
									
									$rselectaccA = $this->_db->fetchAll($selectA);
									
									//print_r($rselectaccA);
									//die;
								//}
									// $selectaccA = $this->_db->select()
															// ->from(array('CA'=>'M_COMM_ACCT'),array('accno'=>'ACCT_NO','acctccy'=>'ACCT_CCY'))
															// ->joinLeft(array('C'=>'M_COMMUNITY'),'CA.CODE = C.COMMUNITY_CODE',array())
															// ->joinLeft(array('A'=>'M_ANCHOR'),'C.ANCHOR_ID = A.ANCHOR_ID',array())
															// ->where('CA.ACCT_OWNER =?','M')
															// ->where('CA.ACCT_SOURCE =?','1')
															// ->where("ACCT_CCY = 'IDR' OR ACCT_CCY = 'USD'")
															
															// ->where('C.ANCHOR_ID =?',$anchorid);
																
									
											
									//echo "<pre>";
									//echo $selectaccA->__toString();
									//die();
										
									$totcompany = 0;
									if(is_array($rselectaccA) && count($rselectaccA) > 0){
										
										// echo 'anchorcust: '.$anchorcust.'<br />';
										// echo "<pre>";
										// print_r($rselectaccA);
										// echo "<br />";
										
										foreach($rselectaccA as $keyacc => $accfieldA){
										
											$accnoA = $accfieldA["acctno"];
											$acctccyA = $accfieldA["acctccy"];
												
											$selectsumA = $this->_db->select()
																	->from(array('LIST_ACCT_INFO'),array('sumplafond'=>'sum(PLAFOND)'))
																	->where('ACCOUNT_NO =?',$accnoA);
											$resultsumA = $this->_db->fetchRow($selectsumA);
											
											if($acctccyA == 'IDR') $utilizationA = $resultsumA["sumplafond"];
											else $utilizationA = ($resultsumA["sumplafond"]) * $xxx;
											
											$totcompany = $totcompany + $utilizationA;
												//echo "accno: $accno, sum: $rr";
										}
										
									}
									else $totcompany = 0;
								
								}
								else $totcompany = 0;
								
								if(!$csv) $value = Application_Helper_General::displayMoney($totcompany); 		
								else $value = $totcompany;
								
							}
							
							else if ($key == "remainlimitcompany"){
								$remaincompany = $limitcompany - $totcompany;
								
								if(!$csv) $value = Application_Helper_General::displayMoney($remaincompany);	
								else $value = $remaincompany;
													
							}
							
							else if ($key == "statuscompany"){
								if($limitcompany > 0) $stat = 'Available';
								else if( $limitcompany < 0) $stat = 'Exceed';
								else $stat = 'Full';
								
								$value = $stat; 
							}
							$value = ($value == "" && !$csv)? "&nbsp;": $value;
							$data[$d][$key] = $value;					
							
						}
					
					}
				}
				
				$dataF = array();
				//echo "limvar: $limvar";
				if($fLIMITSTATUS){
					if(is_array($data)){
						foreach($data as $keyd => $vald){
							
							foreach($vald as $keyt => $valt){
								
								$statuscompany = $vald["statuscompany"];
								$statuscomm = $vald["statuscomm"];
								$value = $vald[$keyt];
								
								
								if($statuscompany == $limvar || $statuscomm == $limvar){
									$dataD[$keyd][$keyt] = $value;	
								}
													
								
							}
						
						}
					}
					if($dataD == NULL) $dataC = $dataF;
					else $dataC = $dataD;
				}
				else $dataC = $data;
			}
			else{
				
				// echo "<pre>";
				// print_r ($data);
				// die;
				$dataC = $data;
				$data = array();
			}
			
			// Zend_Debug::Dump($fusdexchange);
			// Zend_Debug::Dump($data);die;
			
			if(!$pdf && !$csv){
			
				$this->paging($data);
				$data = $this->view->paginator;
			
			}
			else{
				
				$header  = Application_Helper_Array::simpleArray($fieldselect, "label");
			}
			
			if($csv)
			{
				$this->_helper->download->csv($header,$dataC,null,'Limit Monitoring Report');  
				Application_Helper_General::writeLog('LIMR','Export CSV Limit Monitoring Report');
				
			}
			elseif($pdf)
			{
				$this->_helper->download->pdf($header,$dataC,null,'Limit Monitoring Report');  
				Application_Helper_General::writeLog('LIMR','Export PDF Limit Monitoring Report');
				
			}
			else
			{			
				
					
				$stringParam = array(
									 
									 'PRINCIPALCODE'		=> $fPRINCIPALCODE,
									 'PRINCIPALNAME'		=> $fPRINCIPALNAME,
									 
									 'COMMUNITYCODE'		=> $fCOMMUNITYCODE,
									 'COMMUNITYNAME'		=> $fCOMMUNITYNAME,
									 
									 'LIMITSTATUS'			=> $fLIMITSTATUS,
									 'USDEXCHANGE' 			=> $fusdexchange,
									 
									);
				
				$this->view->PRINCIPALCODE 			= $fPRINCIPALCODE;
				$this->view->PRINCIPALNAME 			= $fPRINCIPALNAME;
				
				$this->view->COMMUNITYCODE 			= $fCOMMUNITYCODE;
				$this->view->COMMUNITYNAME 			= $fCOMMUNITYNAME;
								
				$this->view->LIMITSTATUS 			= $fLIMITSTATUS;
				$this->view->USDEXCHANGE			= $fusdexchange;
				
				$this->view->paramstring 			= $stringParam; // parameter utk page
				$this->view->data					= $data;
				
				$this->view->fields 				= $fields;
				$this->view->filter 				= $filter;
				
				$this->view->sortBy 				= $sortBy;
				$this->view->sortDir 				= $sortDir;
				
				$this->view->arrPrincipal 			= $arrPrincipal;
				$this->view->arrCommunity 			= $arrCommunity;
				$this->view->arrLimit				= $arrLimit;
				
				Application_Helper_General::writeLog('LIMR','View Limit Monitoring Report');
				
			}			
			
	}
	
	public function getLimitStatus(){
	
		$arrLimit[1] = 'Available';
		$arrLimit[2] = 'Exceed';
		$arrLimit[3] = 'Full';
		
		return $arrLimit;
		
	}	
}
