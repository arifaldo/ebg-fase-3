<?php

require_once 'Zend/Controller/Action.php';

class systembalance_HistoryController extends Application_Main 
{
	public function initController()
	{       			
		$listYesNo = array('' => '--- Any Value ---',												
						   '0' => 'No',												
						   '1' => 'Yes'
						  );	
		
	/*	$listRange = array('EQ' => '=',												
						   'NE' => '!=',												
						   'LT' => '<',
						   'GT' => '>',
						   'LE' => '<=',
						   'GE' => '>=',
						  );	
		$this->view->listRange = $listRange;*/						  
		$this->view->listYesNo = $listYesNo;

	}
 
	public function indexAction() 
	{  
		$acct_no = $this->_getParam('acct_no');
		$acctInfo = $this->_db->fetchRow(
						$this->_db->select()
								  ->from(array('M_CUSTOMER_ACCT'),array('CUST_ID', 'ACCT_NO', 'ACCT_NAME', 'CCY_ID'))
								  ->where("ACCT_NO = ".$this->_db->quote($acct_no))
					);
		$this->view->acct_no = $acctInfo['ACCT_NO'];
		$this->view->acct_name = $acctInfo['ACCT_NAME'];
		$this->view->acct_ccy = $acctInfo['CCY_ID'];
		$this->view->acct_cust = $acctInfo['CUST_ID'];
		
		$fields = array('approve_date'   => array('field'    => 'APPROVE',
											  'label'    => 'Approve Date',
											  'sortable' => true),
					   'suggested_by'  => array('field' => 'SUGGESTEDBY',
											   'label' => 'Suggested By',
											   'sortable' => true),
					   'approved_by'   => array('field'    => 'APPROVEDBY',
											  'label'    => 'Approved By',
											  'sortable' => true),
					   'issys_bal'   => array('field'    => 'ISSYSTEMBALANCE',
											  'label'    => 'System Balance',
											  'sortable' => true),
					   'tolerance'   => array('field'    => 'PLAFOND',
											  'label'    => 'Tolerance',
											  'sortable' => true),
					   'plafond_date'   => array('field'    => 'ISPLAFONDDATE',
											  'label'    => 'Tolerance Date',
											  'sortable' => true),
					   'plafond_start'   => array('field'    => 'PLAFOND_START',
											  'label'    => 'Tolerance Start Date',
											  'sortable' => true),
					   'plafond_end'   => array('field'    => 'PLAFOND_END',
											  'label'    => 'Tolerance End Date',
											  'sortable' => true)
				);
				
		//get page, sortby, sortdir
		$page = $this->_getParam('page');
		$sortBy = $this->_getParam('sortby');
		$sortDir = $this->_getParam('sortdir');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
     
        //get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array('filter' 	 => array('StringTrim','StripTags'),
						   'suggested'     => array('StringTrim','StripTags','StringToUpper'),
						   'approved' 	 => array('StringTrim','StripTags','StringToUpper'),
						   'system_balance'      => array('StringTrim','StripTags','StringToUpper'),
						   'tolerance_date' 	 => array('StringTrim','StripTags','StringToUpper'),
						  );
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
			
		$filter = $zf_filter->getEscaped('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
	   
		$select = $this->_db->select()
							->from(array('T_SET_SYSTEMBALANCE'),
								   array('APPROVE', 'SUGGESTEDBY', 'APPROVEDBY','ISSYSTEMBALANCE','PLAFOND','ISPLAFONDDATE','PLAFOND_START','PLAFOND_END'))
							->where("ACCT_NO = ".$this->_db->quote($acct_no));

		$select->order($sortBy.' '.$sortDir);
		
		if( $filter == 'Set Filter' || ($filter == 'Set Filter' &&( $csv || $pdf ) ))
		{
			$suggested = html_entity_decode($zf_filter->getEscaped('suggested'));
			$approved = html_entity_decode($zf_filter->getEscaped('approved'));
			$system_balance = html_entity_decode($zf_filter->getEscaped('system_balance'));
			$tolerance_date = html_entity_decode($zf_filter->getEscaped('tolerance_date'));
			
			$appDateFrom = html_entity_decode($this->_getParam('appDateFrom'));
			$appDateTo = html_entity_decode($this->_getParam('appDateTo'));
			
			$startDateFrom = html_entity_decode($this->_getParam('startDateFrom'));
			$startDateTo = html_entity_decode($this->_getParam('startDateTo'));
			
			$endDateFrom = html_entity_decode($this->_getParam('endDateFrom'));
			$endDateTo = html_entity_decode($this->_getParam('endDateTo'));
			//Zend_Debug::dump($system_balance);die;
		   //where clauses
			if($suggested)
				$select->where("UPPER(SUGGESTEDBY) LIKE ".$this->_db->quote('%'.$suggested.'%'));
			if($approved)
				$select->where("UPPER(APPROVEDBY) LIKE ".$this->_db->quote('%'.$approved.'%'));
			if($system_balance!=null)
				$select->where("ISSYSTEMBALANCE = ".$this->_db->quote($system_balance));
			if($tolerance_date!=null)
				$select->where("ISPLAFONDDATE = ".$this->_db->quote($tolerance_date));
				
			if($appDateFrom)
			{
				$appDateFrom  = (Zend_Date::isDate($appDateFrom,$this->_dateDisplayFormat))?
												   new Zend_Date($appDateFrom,$this->_dateDisplayFormat):
												   false;
				$select->where("CONVERT(DATE, APPROVE)  >= ".$this->_db->quote($appDateFrom));	
				if($appDateFrom)	$this->view->appDateFrom   = $appDateFrom->toString($this->_dateDisplayFormat);
			}
			if($appDateTo)
			{
				$appDateTo  = (Zend_Date::isDate($appDateTo,$this->_dateDisplayFormat))?
												   new Zend_Date($appDateTo,$this->_dateDisplayFormat):
												   false;
				$select->where("CONVERT(DATE, APPROVE)  <= ".$this->_db->quote($appDateTo));
				if($appDateTo)	$this->view->appDateTo   = $appDateTo->toString($this->_dateDisplayFormat);
			}	
			if($startDateFrom)
			{
				$startDateFrom  = (Zend_Date::isDate($startDateFrom,$this->_dateDisplayFormat))?
												   new Zend_Date($startDateFrom,$this->_dateDisplayFormat):
												   false;
				$select->where("CONVERT(DATE, PLAFOND_START)  >= ".$this->_db->quote($startDateFrom));	
				if($startDateFrom)		$this->view->startDateFrom   = $startDateFrom->toString($this->_dateDisplayFormat);
			}

			if($startDateTo)
			{
				$startDateTo  = (Zend_Date::isDate($startDateTo,$this->_dateDisplayFormat))?
												   new Zend_Date($startDateTo,$this->_dateDisplayFormat):
												   false;
				$select->where("CONVERT(DATE, PLAFOND_START)  <= ".$this->_db->quote($startDateTo));	
				if($startDateTo)	$this->view->startDateTo   = $startDateTo->toString($this->_dateDisplayFormat);
			}	
			
			if($endDateFrom)
			{
				$endDateFrom  = (Zend_Date::isDate($endDateFrom,$this->_dateDisplayFormat))?
												   new Zend_Date($endDateFrom,$this->_dateDisplayFormat):
												   false;
				$select->where("CONVERT(DATE, PLAFOND_END)  >= ".$this->_db->quote($endDateFrom));	
				if($endDateFrom)	$this->view->endDateFrom   = $endDateFrom->toString($this->_dateDisplayFormat);
			}
			if($endDateTo)
			{
				$endDateTo  = (Zend_Date::isDate($endDateTo,$this->_dateDisplayFormat))?
												   new Zend_Date($endDateTo,$this->_dateDisplayFormat):
												   false;
				$select->where("CONVERT(DATE, PLAFOND_END)  <= ".$this->_db->quote($endDateTo));	
				if($endDateTo)	$this->view->endDateTo   = $endDateTo->toString($this->_dateDisplayFormat);
			}	
								
			$this->view->suggested   = $suggested;
			$this->view->approved   = $approved;
			$this->view->system_balance   = $system_balance;
			$this->view->tolerance_date   = $tolerance_date;			
		}
		
		if($csv || $pdf)
			{
				$arr = $this->_db->fetchAll($select);
				$header = array('Approve Date','Suggested By','Approved By','System Balance','Tolerance','Tolerance Date','Tolerance Start Date','Tolerance End Date');
				
				foreach ($arr as $key => $row)
				{
					$arr[$key]["APPROVE"]= isset($row['APPROVE']) ? Application_Helper_General::convertDate($row['APPROVE'],'dd MMM yyyy HH:mm:ss','yyyy-MM-dd HH:mm:ss') : "N/A";
					$arr[$key]["SUGGESTEDBY"]= isset($row['SUGGESTEDBY']) ? $row['SUGGESTEDBY'] : "N/A";
					$arr[$key]["APPROVEDBY"]= isset($row['APPROVEDBY']) ? $row['APPROVEDBY'] : "N/A";
					$arr[$key]["ISSYSTEMBALANCE"]= ($row['ISSYSTEMBALANCE']==1) ? "Yes" : "No";
					$arr[$key]["PLAFOND"]= isset($row['PLAFOND']) ? Application_Helper_General::displayMoney($row['PLAFOND']) : "N/A";
					$arr[$key]["ISPLAFONDDATE"]= ($row['ISPLAFONDDATE']==1) ? "Yes" : "No";
					$arr[$key]["PLAFOND_START"]= isset($row['PLAFOND_START']) ? Application_Helper_General::convertDate($row['PLAFOND_START'],'dd MMM yyyy','yyyy-MM-dd HH:mm:ss') : "N/A";
					$arr[$key]["PLAFOND_END"]= isset($row['PLAFOND_END']) ? Application_Helper_General::convertDate($row['PLAFOND_END'],'dd MMM yyyy','yyyy-MM-dd HH:mm:ss') : "N/A";
				}
				if($csv)
				{
					Application_Helper_General::writeLog('BLLS','Downloading CSV History System Balance List');
					$this->_helper->download->csv($header,$arr,null,'System Balance History List');
				}
				
				if($pdf)
				{
					Application_Helper_General::writeLog('BLLS','Downloading PDF History System Balance List');
					$this->_helper->download->pdf($header,$arr,null,'System Balance History List');
				}
			}
		if(!$this->_request->isPost())
		{
			Application_Helper_General::writeLog('BLLS','Viewing History System Balance List');
		}
		$this->paging($select);
		
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		
		Application_Helper_General::writeLog('BLLS','Viewing History System Balance List');
	}
}


