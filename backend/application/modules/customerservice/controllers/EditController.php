<?php

class customerservice_EditController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		
		if($this->_getParam('QUESTION_ID'))
		{
			$filters = 	array( 
									'QUESTION_ID'	=> 	array('StringTrim','StripTags'),
								);
			$validator = array	( 
									'QUESTION_ID' 	=> 	array	(	
																		array	(
																					'Db_RecordExists',array	(
																													'table'=>'CUSTOMER_SERVICES',
																													'field'=>'QUESTION_ID'
																												)
																				),
																		'messages' => array	(
																								$this->language->_('Error: Question Has not been Exist').".",
																							)
																	)
								);
			$param['QUESTION_ID']	= $this->_getParam('QUESTION_ID');
			
			$zf_filter_input = new Zend_Filter_Input($filters, $validator, $param, $this->_optionsValidator);
			
			if($zf_filter_input->isValid())
			{
				$QUESTION_ID = $this->_getParam('QUESTION_ID');
				$this->view->QUESTION_ID = $QUESTION_ID;
			}
			else
			{
				$this->_redirect('/home/index');
			}
		}
		else
		{
			$this->_redirect('/home/index');
		}
		
		if($this->_getParam('submit') == TRUE)
		{
			$filters = 	array( 
									'answer'	=> 	array('StringTrim','StripTags'),
								);
			$validator = array	( 
									'answer'	=> 	array	(
																'NotEmpty',
																'messages' => array	(
																						$this->language->_('Error: Answer has not fill yet').'.',
																					)
															),
								);
			
			$param['answer']	= $this->_getParam('answer');
			
			$zf_filter_input = new Zend_Filter_Input($filters, $validator, $param, $this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				try 
				{
					$params = array (
										"QUESTION_A" 		=> (isset($param['answer'])) ? $param['answer'] : null,
										'QUESTION_A_DATE' 	=> new Zend_Db_Expr('GETDATE()'),
										'BUSER_ID' 			=> $this->_userIdLogin,
									);
					$whereArr = array('QUESTION_ID = ?'=>$QUESTION_ID);
					//Zend_Debug::dump($params);
					Application_Helper_General::writeLog('ACSQ','Answer Customer Service Question : '. $this->_request->getParam('answer'));
					
					$userupdate = $this->_db->update("CUSTOMER_SERVICES",$params,$whereArr);
					$this->view->suksess = true;
				}
				catch(Exception $e)
				{
					$this->view->failErr = "Data Fail To Insert";
					$this->view->answer = $param['answer'];
				}
			}
			else
			{
				$errors 				= $zf_filter_input->getMessages();
				$this->view->answerErr 	= (isset($errors['answer']))? $errors['answer'] : null;
				$this->view->answer 	= $param['answer'];
			}
		}
		
		$arrquestioncategory 	= array_combine($this->_questioncategory["code"],$this->_questioncategory["desc"]);
		
		$questioncategoryArray 	= array( '' => '--- Please Select --- ');
		$questioncategoryArray += array_combine(array_values($this->_questioncategory['code']),array_values($this->_questioncategory['desc']));				
		$this->view->questioncategoryArray 		= $questioncategoryArray;
		
		$casequestioncategory = "(CASE CS.QUESTION_CATEGORY ";
  		foreach($arrquestioncategory as $key => $val)	{ $casequestioncategory .= " WHEN ".$key." THEN '".$val."'"; }
  		$casequestioncategory .= " END)";
		
		$select = $this->_db->select()
							->from(	array(	'CS'				=> 'CUSTOMER_SERVICES'),
									array(	'QUESTION_DATE'		=> 'QUESTION_DATE',	
											'CATEGORY'			=> $casequestioncategory,
											'QUESTION_Q'		=> 'QUESTION_Q',	
											'QUESTION_A_DATE'	=> 'QUESTION_A_DATE',	
											'QUESTION_A'		=> 'QUESTION_A',	
											'USER_ID'			=> 'USER_ID',		
											'QUESTION_ID'		=> 'QUESTION_ID',	
											)
									);
		$select->where('CS.QUESTION_ID = '.$this->_db->quote($this->_getParam('QUESTION_ID')));
		$arr = $this->_db->fetchRow($select);
		$this->view->arr = $arr;
	}
}
