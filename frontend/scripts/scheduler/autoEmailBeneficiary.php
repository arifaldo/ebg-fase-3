<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

$config    	= Zend_Registry::get('config');
$Settings = new Settings();
$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
$templateEmailMasterBankWapp = $Settings->getSetting('master_bank_wapp');
$db = Zend_Db_Table::getDefaultAdapter();
//$select = $db->select()->distinct()
//    ->from(array('A' => 'T_TRANSACTION'))
//	->joinleft(array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER')
//// 	->joinLeft(
////         array('E' => 'T_EMAIL_NOTIFICATION_TRX'), 
////         'A.TRANSACTION_ID = E.TRANSACTION_ID', 
////         array(
////             'BENEFICIARY_ACCOUNT',
////             'BENEFICIARY_ACCOUNT_CCY',
////     		'BENEFICIARY_ACCOUNT_NAME',
////     		'BENEFICIARY_ALIAS_NAME',
////     		'BENEFICIARY_EMAIL'
////         )
//// 	)
////     ->where('B.PS_STATUS = 5')
//	->where('A.TRA_STATUS = 3')
//// 	->where("E.PS_ISBENEEMAILED NOT LIKE '1' OR E.PS_ISBENEEMAILED IS NULL")
//// 	->query() ->fetchAll()
//;
//// 	Zend_Debug::dump($select);
//	die($select);	
		 ini_set('display_errors', 1);
		 ini_set('display_startup_errors', 1);
		 error_reporting(E_ALL);
			$setting = new Settings();
			$template						= $setting->getSetting('femailtemplate_benefnotification','');
			$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name','');
			$templateEmailMasterBankAppName 	= $setting->getSetting('master_bank_app_name','');
			$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp','');
			$templateEmailMasterBankWapp 	= $setting->getSetting('master_bank_wapp','');
			$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email','');
			$templateEmailMasterBankEmail1 	= $setting->getSetting('master_bank_email1','');
			$date 							= date("d M Y H:i:s");

			$config    		= Zend_Registry::get('config');
			$paymentType 	= $config["payment"]["type"];
			$transferType 	= $config["transfer"]["type"]["code"];
			$transferTypedesc 	= $config["transfer"]["type"]["desc"];
			$paymentType 	= $config["payment"]["type"]["desc"];
			$paymentTypeCode 	= $config["payment"]["type"]["code"];

		 $select = $db->select()->distinct()
		->from(array('A' => 'T_TRANSACTION'),array())
		->joinleft(array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER',array())
		->joinLeft(array('E' => 'T_EMAIL_NOTIFICATION_TRX'), 'A.TRANSACTION_ID = E.TRANSACTION_ID', array(	'A.BENEFICIARY_ACCOUNT',
																											'A.BENEFICIARY_ACCOUNT_CCY',
																											'A.BENEFICIARY_ACCOUNT_NAME',
																											'A.BENEFICIARY_ALIAS_NAME',
																											'A.BENEFICIARY_EMAIL',
																											'B.PS_NUMBER'))
		->where('B.PS_STATUS = 5')
		->where('A.BENEFICIARY_ACCOUNT IS NOT NULL')
		->where('A.BENEFICIARY_EMAIL != ""')
		// ->where('A.BENEFICIARY_ACCOUNT = ?',"20182100211")

		->where('A.TRA_STATUS = 3')
		->where("DATE(B.PS_EFDATE) = DATE(now())")
		->where("E.PS_ISBENEEMAILED NOT LIKE '1' OR E.PS_ISBENEEMAILED IS NULL")
		->query() ->fetchAll();
		//echo $select;die;
		 //Zend_Debug::dump($select);die;

if($select){

	
	foreach($select as $result)
	{
		$send = false;
		//select data from database where is uploaded today and not email yet and not deleted yet
		//$caseBeneBank = "CASE B.PS_TYPE WHEN '2' THEN A.BENEFICIARY_BANK_NAME ELSE '".$config['app']['bankname']."' END";
		//var_dump($result);
		$select2 = $db->select()//->distinct()
						->from(array('A' => 'T_TRANSACTION'),array('TRANSACTION_ID'))
						->joinleft(array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER',array('PS_NUMBER'))
						->joinLeft(array('E' => 'T_EMAIL_NOTIFICATION_TRX'), 'A.TRANSACTION_ID = E.TRANSACTION_ID', array(	'A.*',
																															'B.*',
																															//'benebank' => new Zend_Db_Expr($caseBeneBank),
																															'E.PS_ISBENEEMAILED', 
																															'E.PS_ISSOURCEEMAILED') )
						->where('B.PS_STATUS = 5')
						->where('A.TRA_STATUS = 3')
						->where('A.BENEFICIARY_ACCOUNT LIKE '.$db->quote($result['BENEFICIARY_ACCOUNT']))
						->where("E.PS_ISBENEEMAILED NOT LIKE '1' OR E.PS_ISBENEEMAILED IS NULL")
						->where("DATE(B.PS_EFDATE) = DATE(now())")
						->query() ->fetchAll();
						//echo $select2;die;
		$benename = $result['BENEFICIARY_ACCOUNT_NAME'];
		$beneccy = $result['BENEFICIARY_ACCOUNT_CCY'];
		$beneacc = $result['BENEFICIARY_ACCOUNT'];
		//Zend_Debug::dump($select2);die;
		try
		{
			// $db->beginTransaction();
			$content = '';
			$counter = 0;
			foreach($select2 as $result2)
			{

	

					if ($result2['PS_NUMBER'] == 'autotopup') {
						$pslip['PS_TYPE'] = $paymentTypeCode['autotopup'];
						$pslip['CUST_NAME'] = $result2['DATA']['CUST_NAME'];
						$result2['PS_NUMBER'] = empty($result2['TRANSACTION_ID']) ? '-' : $result2['TRANSACTION_ID'];
					}
					else{
						$select	= $db->select()
									->from(array('P'	 		=> 'T_PSLIP'),array('*'))
									->where('P.PS_NUMBER = ?',$result2['PS_NUMBER']);
						$pslipdata =  $db->fetchAll($select);
						$pslip = $pslipdata[0];
						
						$selectcust	= $db->select()
									->from(array('P'	 		=> 'M_CUSTOMER'),array('*'))
									->where('P.CUST_ID = ?',$pslip['CUST_ID']);
						$custdata =  $db->fetchAll($selectcust);
						$custd = $custdata[0];
						//var_dump($custd);die;
						$pslip['CUST_NAME'] = $custd['CUST_NAME'];
					}
					
					
					$totaldata = count($select2);
					$str = '';
					//echo '<pre>';
					
					$payType 	= Application_Helper_General::filterPaymentType($paymentType, $transferType);
					
					$paytypesarr = array_combine(array_values($paymentTypeCode),array_values($paymentType));
				//	$transferType 	= Application_Helper_General::filterPaymentTypeRekon($paymentType, $transferType);

						foreach($payType as $key => $value){
				// 			if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);

							 $optpaytypeRaw[$key] = $value;
						}
						$opttype = array();
						foreach($transferType as $key => $val){
							//var_dump($val);
							$opttype[$val] = $transferTypedesc[$key];
						}
					//var_dump($transferTypedesc);
					//var_dump($opttype);die;
					//$result2['TRA_MESSAGE'] = ($sendTransfer['ResponseCode'] == '0000')?'Success':'Failed';
					
					
						//$result2['TRA_MESSAGE'] = 'Success';
					
					if(!empty($result2['BENEF_ACCT_BANK_CODE'])){
					$select	= $db->select()
						->from(
							array('TA' => 'M_BANKTABLE'),
							array(
								'BANK_NAME' 			=> 'TA.BANK_NAME'
							)
						)
						
						->where("TA.BANK_CODE 	= ?" , $result2['BENEF_ACCT_BANK_CODE']);

					$beneBank = $db->fetchRow($select);
					$result2['BENEFICIARY_BANK_NAME'] = $beneBank['BANK_NAME'];
					}
					
					if(!empty($result2['SOURCE_ACCT_BANK_CODE'])){
					$select	= $db->select()
						->from(
							array('TA' => 'M_BANKTABLE'),
							array(
								'BANK_NAME' 			=> 'TA.BANK_NAME'
							)
						)
						
						->where("TA.BANK_CODE 	= ?" , $result2['SOURCE_ACCT_BANK_CODE']);

					$beneBank = $db->fetchRow($select);
					$result2['SOURCE_BANK_NAME'] = $beneBank['BANK_NAME'];
					}else{
							$config    	= Zend_Registry::get('config');

							$bankName    = $config['app']['bankname'];
							$result2['SOURCE_BANK_NAME'] = $bankName;
					}



	//	echo '<pre>';
	//var_dump($pslip);die;
	
		if($result2['BENEFICIARY_BANK_NAME'] == '' || $result2['BENEFICIARY_BANK_NAME'] == NULL){
			$config    	= Zend_Registry::get('config');

			$bankName    = $config['app']['bankname'];
			$result2['BENEFICIARY_BANK_NAME'] = $bankName;
		}
			if($totaldata == 1){
			
					
			
			
					$str .= '<table style="margin:15px 0;color:rgba(0,0,0,0.70);border-collapse:collapse" width="100%">';
				
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Tanggal Pembayaran</td>';
					$datenow = date('Y-m-d');
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.Application_Helper_General::convertDate($datenow, 'dd MMM yyyy').'</td>';
					$str.= '</tr>';
					$PAYMENT_DATE = Application_Helper_General::convertDate($datenow, 'dd MMM yyyy');
					if($pslip['PS_TYPE'] == '1'){
						
					$tratype = array('0' => 'PB','1' => 'RTGS','2' => 'SKN','7' => 'PB');
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jenis Transaksi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$tratype[$result2['TRANSFER_TYPE']].'</td>';
					$str.= '</tr>';
					$TRANSFER_TYPE = $tratype[$result2['TRANSFER_TYPE']];
					}else if($pslip['PS_TYPE'] == '2'){
						
					$tratype = array('0' => 'PB','1' => 'RTGS','2' => 'SKN','5' => 'ONLINE');
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jenis Transaksi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$tratype[$result2['TRANSFER_TYPE']].'</td>';
					$str.= '</tr>';
					$TRANSFER_TYPE = $tratype[$result2['TRANSFER_TYPE']];
					}else if($pslip['PS_TYPE'] == '16' || $pslip['PS_TYPE'] == '17'){

					}else{
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jenis Transaksi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$paytypesarr[$pslip['PS_TYPE']].'</td>';
					$str.= '</tr>';	
					$TRANSFER_TYPE = $paytypesarr[$pslip['PS_TYPE']];
					}
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nomor Referensi </td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['PS_NUMBER'].'</td>';
					$str.= '</tr>';
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nomor Transaksi </td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRANSACTION_ID'].'</td>';
					$str.= '</tr>';
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Subject </td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['PS_SUBJECT'].'</td>';
					$str.= '</tr>';
					
			if($pslip['PS_TYPE'] == '1' && $result2['TRANSFER_TYPE'] == '0'){
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].' / '.$result2['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					// $BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'];
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($result2['TRA_AMOUNT']).'</td>';
					$str.= '</tr>';
					$TOTAL = 'IDR '.number_format($result2['TRA_AMOUNT']);
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
					$str.= '</tr>';
			}else if($pslip['PS_TYPE'] == '1' && $result2['TRANSFER_TYPE'] == '7'){
					$str.= '<tr>'; 
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].' / '.$result2['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					// $BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'];
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah</td>';
					if($transactionData['0']['BENEFICIARY_ACCOUNT_CCY'] != 'IDR'){
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT_CCY'].' '.$result2['TRA_AMOUNT'].'</td>';
						$TOTAL = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.$result2['TRA_AMOUNT'];
					}else{
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($result2['TRA_AMOUNT']).'</td>';
						$TOTAL = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($result2['TRA_AMOUNT']);
					}
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
					$str.= '</tr>';
					
			}else if($pslip['PS_TYPE'] == '1' && $result2['TRANSFER_TYPE'] == '5'){		
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].' / '.$result2['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					// $BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'];
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($result2['TRA_AMOUNT']).'</td>';
					$str.= '</tr>';
					$TOTAL = 'IDR '.number_format($result2['TRA_AMOUNT']);
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
					$str.= '</tr>';
			}else if($pslip['PS_TYPE'] == '1' && $result2['TRANSFER_TYPE'] == '5'){		
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].' / '.$result2['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					// $BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'];
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($result2['TRA_AMOUNT']).'</td>';
					$str.= '</tr>';
					$TOTAL = 'IDR '.number_format($result2['TRA_AMOUNT']);
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
					$str.= '</tr>';
			}else if($pslip['PS_TYPE'] == '1' && $result2['TRANSFER_TYPE'] == '6'){
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">No. Virtual Account</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].'</td>';
					$str.= '</tr>';
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nama</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
					$total = $result2['TOTAL_CHARGES']+$result2['TRA_AMOUNT'];
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($total).'</td>';
					$str.= '</tr>';
					$TOTAL = 'IDR '.number_format($total);
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
					$str.= '</tr>';
					
			}else if($pslip['PS_TYPE'] == '16' || $pslip['PS_TYPE'] == '17'){
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].' / '.$result2['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					// $BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'];
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
					$TOTAL = 'IDR '.number_format($result2['TRA_AMOUNT']);
					$select	= $db->select()
							->from(array('P'	 		=> 'T_PSLIP_DETAIL'),array('*'))
							->where('P.PS_NUMBER = ?',$transactionData['0']['PS_NUMBER']);
					$pslipdetail =  $db->fetchAll($select);
					 
					if(!empty($pslipdetail)){
						foreach($pslipdetail as $val){

								$str.= '<tr>';
								$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">'.$val['PS_FIELDNAME'].'</td>';
								$ccy = '';
								if($val['PS_FIELDTYPE']=='2'){
									$ccy = 'IDR';
									$val['PS_FIELDVALUE'] = round($val['PS_FIELDVALUE'], 0);
								}
								$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$ccy.' '.$val['PS_FIELDVALUE'].'</td>';
								$str.= '</tr>';
						}
					}
					
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
					$str.= '</tr>';
					
			}

			//if autotopup
			else if($pslip['PS_TYPE'] == $paymentTypeCode['autotopup']){

					$autotopupData = $result2['DATA'];

					//if exceed warn level
					if($autotopupData['STATUS'] == '6' || $autotopupData['STATUS'] == '7'){

						$template	= $setting->getSetting('femailtemplate_topupthresholdnotification','');
					}
					else if($autotopupData['STATUS'] == '3'){
						
						$template	= $setting->getSetting('femailtemplate_topupfailednotification','');
					}
					else{

						if ($autotopupData['STATUS'] == 2) {
						//	$result2['TRA_MESSAGE'] = 'Success';
							$color = 'green';
						}
						else if ($autotopupData['STATUS'] == 4) {
							//$result2['TRA_MESSAGE'] = 'Failed';
							$color = 'red';
						}
						else if ($autotopupData['STATUS'] == 5) {
						//	$result2['TRA_MESSAGE'] = 'Suspect';
							$color = 'red';
						}

						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$autotopupData['SOURCE_ACCOUNT'].' / '.$autotopupData['ACCOUNT_NAME'].'</td>';
						$str.= '</tr>';
						$SOURCE_ACCOUNT = $autotopupData['SOURCE_ACCOUNT'];
						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$autotopupData['BANK_NAME'].'</td>';
						$str.= '</tr>';
						
						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Kode Maskapai</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$autotopupData['AIRLINE_CODE'].' - '.$autotopupData['AIRLINE_DATA']['AIRLINE_NAME'].'</td>';
						$str.= '</tr>';

						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Travel Agent ID</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$autotopupData['TRAVELAGENT_ID'].'</td>';
						$str.= '</tr>';

						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($autotopupData['TOPUP_AMOUNT']).'</td>';
						$str.= '</tr>';
						$TOTAL = 'IDR '.number_format($autotopupData['TOPUP_AMOUNT']);
						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold;color:'.$color.';" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
						$str.= '</tr>';
					}
			}else if($pslip['PS_TYPE'] == '3'){
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].' / '.$result2['SOURCE_ACCOUNT_NAME'].'</td>';
				$str.= '</tr>';
				$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
				$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					// $BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'];
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
				
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah</td>';
				if($chargeTrfCcy != 'IDR'){
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT_CCY'].' '.$result2['TRA_AMOUNT'].'</td>';
				$TOTAL = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.$result2['TRA_AMOUNT'];
				}else{
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($result2['TRA_AMOUNT']).'</td>';	
				$TOTAL = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($result2['TRA_AMOUNT']);
				}
				$str.= '</tr>';
				
									$chargeTrf = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 3)
												->where("CHARGE_CCY = ?", $result2['SOURCE_ACCOUNT_CCY'])
												->where("CUST_ID = ?", (string)$pslip['CUST_ID'])
											);

									if(empty($chargeTrf)){
										$chargeTrf = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 3)
												->where("CHARGE_CCY = ?", $result2['SOURCE_ACCOUNT_CCY'])
												->where("CUST_ID = ?", 'GLOBAL')
											);
									}

									//$chargeTrfAmt = $chargeTrf["CHARGE_AMT"];
									$chargeTrfCcy = $chargeTrf["CHARGE_AMOUNT_CCY"];
									$chargeFA = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 4)
												->where("CHARGE_CCY = ?", $result2['BENEFICIARY_ACCOUNT_CCY'])
												->where("CUST_ID = ?", (string)$pslip['CUST_ID'])
											);
									
									if(empty($chargeFA)){
										$chargeFA = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 4)
												->where("CHARGE_CCY = ?", $result2['BENEFICIARY_ACCOUNT_CCY'])
												->where("CUST_ID = ?", 'GLOBAL')
											);
									}
									
									
									$chargeFACcy = $chargeFA["CHARGE_AMOUNT_CCY"];
									
									$chargeProv = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 5)
												->where("CHARGE_CCY = ?", $result2['SOURCE_ACCOUNT_CCY'])
												->where("CUST_ID = ?", (string)$pslip['CUST_ID'])
											);

									if(empty($chargeProv)){
											$chargeProv = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 5)
												->where("CHARGE_CCY = ?", $result2['SOURCE_ACCOUNT_CCY'])
												->where("CUST_ID = ?", 'GLOBAL')
											);
										}

									$chargeProvMinAmt = $chargeProv["CHARGE_PROV_MIN_AMT"];
									$chargeProvMaxAmt = $chargeProv["CHARGE_PROV_MAX_AMT"];
									$chargeProvPct = $chargeProv["CHARGE_PCT"];
									$chargeProvCcy = $chargeFA["CHARGE_AMOUNT_CCY"];
				
				
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
				
				if($result2['BENEFICIARY_ACCOUNT_CCY'] == 'USD' && $result2['SOURCE_ACCOUNT_CCY'] == 'USD'){
					
						$total = $result2['EQUIVALENT_AMOUNT_USD'];
						//var_dump($total);die('here');
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT_CCY'].' '.$total.'</td>';
						$TOTAL = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($total);
				}else if($result2['BENEFICIARY_ACCOUNT_CCY'] != 'IDR'){
						$total = $result2['EQUIVALENT_AMOUNT_USD'];
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT_CCY'].' '.$total.' ( IDR '.number_format($result2['EQUIVALENT_AMOUNT_IDR']).' )</td>';
						$TOTAL = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.$total.' ( IDR '.number_format($result2['EQUIVALENT_AMOUNT_IDR']).' )';
				}else{
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($total).'</td>';
					$TOTAL = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($total);
				}
				
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
				$str.= '</tr>';
			}else if($pslip['PS_TYPE'] == '21'){
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
				if(empty($result2['SOURCE_ACCOUNT_NAME'])){
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].'</td>';
						$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
				}else{
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].' / '.$result2['SOURCE_ACCOUNT_NAME'].'</td>';
					$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
				}
				
				$str.= '</tr>';
				$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					// $BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'];
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($result2['TRA_AMOUNT']).'</td>';
				$str.= '</tr>';
				$TOTAL = 'IDR '.number_format($result2['TRA_AMOUNT']);
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
				$str.= '</tr>';
				//echo $str;die;
			}
			else{
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['SOURCE_ACCOUNT'].' / '.$result2['SOURCE_ACCOUNT_NAME'].'</td>';
				$str.= '</tr>';
				$SOURCE_ACCOUNT = $result2['SOURCE_ACCOUNT'];
				$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					// $BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'].' / '.$result2['BENEFICIARY_ACCOUNT_NAME'];
					$BENEFICIARY_ACCOUNT = $result2['BENEFICIARY_ACCOUNT'];
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($result2['TRA_AMOUNT']).'</td>';
				$str.= '</tr>';
				$TOTAL = 'IDR '.number_format($result2['TRA_AMOUNT']);
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Deskripsi</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$result2['TRA_MESSAGE'].'</td>';
				$str.= '</tr>';
			}
				
			$str.= '</table>';	
				
				
			}else{
				
								//	var_dump(count($select2));
		 			//Zend_Debug::dump($select2);
		 		//die($select2);			
				$sourcename = $result['SOURCE_ACCOUNT_NAME'];
				$sourcealname = $result['SOURCE_ACCOUNT_ALIAS_NAME'];
				$sourceccy = $result['SOURCE_ACCOUNT_CCY'];
				$sourceacc = $result['SOURCE_ACCOUNT'];
				
				$totaltrx = count($select2);
				$totals = 0;
				$totalf = 0;
				$totalp = 0;
				foreach($select2 as $key=>$val){
					if($val['TRA_STATUS'] == '3'){
						$totals++;
					}else if($val['TRA_STATUS'] == '4'){
						$totalf++;
					}else{
						$totalp++;
					}
				}
				
				$str .= '<table style="margin:15px 0;color:rgba(0,0,0,0.70);border-collapse:collapse" width="100%">';
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nomor Referensi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$select2['0']['PS_NUMBER'].'</td>';
					$str.= '</tr>';
						
					if($select2['0']['PS_TYPE'] == '4' || $select2['0']['PS_TYPE'] == '11'){
							$str.= '<tr>';
							$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Pengirim</td>';
							$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$select2['0']['SOURCE_ACCOUNT'].' - '.$select2['0']['SOURCE_ACCOUNT_NAME'].'</td>';
							$str.= '</tr>';
					}
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Total Transaksi	</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$totaltrx.' Transaksi</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($select2['0']['PS_TOTAL_AMOUNT']).'</td>';
					$str.= '</tr>';
				$str.= '</table>';
					
				
			/*	$str .= '<table style="margin:15px 0;color:rgba(0,0,0,0.70);border-collapse:collapse" width="100%">';
				$str .= '<tr>
						<th>No</th>
						<th>No Referensi</th>
						<th>No Transaksi</th>
						<th>Subjek</th>
						<th>Rekening Penerima</th>
						<th>Jumlah</th>
						<th>Deskripsi</th>
						<th>Tanggal Pembayaran Pembayaran</th>
						<th>Bank Tujuan</th>
						<th>Tipe Transfer</th>
					</tr>';
				$content = '';
				$counter = 0;
				foreach($select2 as $result2)
				{
					$beneficiary = $result2['BENEFICIARY_ACCOUNT'].' ('.$result2['BENEFICIARY_ACCOUNT_CCY'].') '.$result2['BENEFICIARY_ACCOUNT_NAME'].' / '.$result2['BENEFICIARY_ALIAS_NAME'];
					$amount = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.Application_Helper_General::displayMoney($result2['TRA_AMOUNT']);
					$content .= '
					<tr>
						<td><font>'.++$counter.'</font></td>
						<td><font>'.$result2['PS_NUMBER'].'</font></td>
						<td><font>'.$result2['TRANSACTION_ID'].'</font></td>
						<td><font>'.$result2['PS_SUBJECT'].'</font></td>
						<td><font>'.$beneficiary.'</font></td>
						<td><font>'.$amount.'</font></td>
						<td><font>'.$result2['TRA_MESSAGE'].'</font></td>
						<td><font>'.Application_Helper_General::convertDate($result2['PS_EFDATE'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
						<td><font>'.$result2['benebank'].'</font></td>
						<td><font>'.$result2['paymenttype'].'</font></td>
						';	
				}
				$str .= $content;
				*/
					
			}






				//echo '<pre>';
				//var_dump($result2);die;
				if($result2['PS_ISBENEEMAILED'] == null && $result2['PS_ISSOURCEEMAILED'] == null)
				{
					$send = true;
					$insertArr = 
						array(
							'TRANSACTION_ID' 		=> $result2['TRANSACTION_ID'], 
							'PS_NUMBER'				=> $result2['PS_NUMBER'], 
							'PS_ISBENEEMAILED' 		=> '1', 
							'PS_BENEEMAILDATETIME' 	=> new Zend_Db_Expr("now()")
							);			
						// var_dump($insertArr);
					$db->insert('T_EMAIL_NOTIFICATION_TRX', $insertArr);
				}
				else if(empty($result2['PS_ISBENEEMAILED']))
				{
					$send = true;
					$insertArr = 
						array(
							'TRANSACTION_ID' 		=> $result2['TRANSACTION_ID'], 
							'PS_NUMBER'				=> $result2['PS_NUMBER'], 
							'PS_ISBENEEMAILED' 		=> '1', 
							'PS_BENEEMAILDATETIME' 	=> new Zend_Db_Expr("now()")
							);		
							// var_dump($insertArr);	
					$db->insert('T_EMAIL_NOTIFICATION_TRX', $insertArr);
				}
				
				elseif($result2['PS_ISBENEEMAILED'] == null && $result2['PS_ISSOURCEEMAILED'] == 1)
				{
					$send = true;
					$updateArr = 
						array( 
							'PS_ISBENEEMAILED' 		=> '1',
							'PS_BENEEMAILDATETIME' 	=> new Zend_Db_Expr("now()")
							);
					$whereArr = array('TRANSACTION_ID =?'	=> $result2['TRANSACTION_ID']);			
					$db->update('T_EMAIL_NOTIFICATION_TRX', $updateArr, $whereArr);
				}
			}
			// die;
			
			if($send == true)
			{


















	
			//var_dump($transactionData);die;
			//if autopup
			
			//echo '<pre>';
			//var_dump($pslip);die;
			
			
			
			$translate = array(
				'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
				'[[master_bank_app_name]]' 		=> $templateEmailMasterBankAppName,
				'[[MASTER_BANK_TELP]]' 			=> $templateEmailMasterBankTelp,
				'[[MASTER_BANK_EMAIL]]' 		=> $templateEmailMasterBankEmail,
				'[[MASTER_BANK_WAPP]]' 			=> $templateEmailMasterBankWapp,
				'[[MASTER_BANK_EMAIL1]]' 		=> $templateEmailMasterBankEmail1,
				'[[PS_NUMBER]]' 				=> $result2['PS_NUMBER'],
				'[[TRANSACTION_ID]]' 			=> $result2['TRANSACTION_ID'],
				'[[TRANSFER_TYPE]]' 			=> $TRANSFER_TYPE,
				'[[PAYMENT_DATE]]'				=> $PAYMENT_DATE,
				'[[SUBJECT]]' 					=> $result2['PS_SUBJECT'],
				'[[SOURCE_ACCOUNT]]' 			=> $SOURCE_ACCOUNT,
				'[[BENEFICIARY_ACCOUNT]]' 		=> $BENEFICIARY_ACCOUNT,
				'[[BENEFICIARY_BANK]]' 			=> $result2['BENEFICIARY_BANK_NAME'],
				'[[SOURCE_BANK]]' 				=> $result2['SOURCE_BANK_NAME'],
				'[[SOURCE_NAME]]' 				=> $result2['SOURCE_ACCOUNT_NAME'],
				'[[DESC]]' 						=> $result2['TRA_MESSAGE'],
				'[[TOTAL]]' 					=> $TOTAL,
			);

			$subject = 'Payment Notification';

			if($pslip['PS_TYPE'] == $paymentTypeCode['autotopup']){

				$autotopupData = $result2['DATA'];

				//if exceed warn level
				if($autotopupData['STATUS'] == '6' || $autotopupData['STATUS'] == '7'){

					$subject = 'WARNING SALDO AIRLINES';

					$translate['[[AIRLINE_NAME]]'] = $autotopupData['AIRLINE_DATA']['AIRLINE_NAME'];
					$translate['[[TRAVELAGENT_ID]]'] = $autotopupData['TRAVELAGENT_ID'];
					$translate['[[comp_name]]'] = $autotopupData['CUST_NAME'];

					if($autotopupData['STATUS'] == '6'){
						$translate['[[WARN_LEVEL]]'] = 'warning level 1 (IDR '.number_format($autotopupData['AIRLINE_DATA']['WARN_LEVEL1']).')';
					}
					else if($autotopupData['STATUS'] == '7'){
						$translate['[[WARN_LEVEL]]'] = 'warning level 2 (IDR '.number_format($autotopupData['AIRLINE_DATA']['WARN_LEVEL2']).')';
					}

					$translate['[[AMOUNT]]'] = '<b><u>'.number_format($autotopupData['CURRENT_BALANCE']).'.</u></b>';
				}	
				else if($autotopupData['STATUS'] == '3'){
					$subject = 'Failed check saldo airlines';

					$translate['[[AIRLINE_NAME]]'] = $autotopupData['AIRLINE_DATA']['AIRLINE_NAME'];
					$translate['[[TRAVELAGENT_ID]]'] = $autotopupData['TRAVELAGENT_ID'];
					$translate['[[comp_name]]'] = $autotopupData['CUST_NAME'];
				}
			}

			$mailContent = strtr($template,$translate); 
			//var_dump($emailList);die;
			
			
				$email = $result2['BENEFICIARY_EMAIL'];
				$subject = 'Incoming Transfer Notification';
				$result = Application_Helper_Email::sendEmail($email,$subject,$mailContent);
	
				// $db->commit();
				$string  = "Sending Email For Beneficiary";
				Application_Helper_General::cronLog(basename(__FILE__),$string,1);
			
			
		}


				
				
			
		}
		catch(Exception $e) 
		{
			var_dump($e);die;
			$db->rollBack();
			$string  = "Sending Email For BeneficiaryFailed";
			Application_Helper_General::cronLog(basename(__FILE__),$string,0); 
		}
	}
}
?>
