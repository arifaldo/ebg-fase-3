<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'Service/Token.php'; //added new
require_once 'SGO/Helper/AES.php';
require_once 'Service/Inquiry.php';
class singlepayment_LocalremittanceController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$futureDate = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));

		$modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$arr 					= $modelCity->getCity();
		$anyValue = '-- '.$this->language->_('Select City'). ' --';
		$cityCodeArr 			= array(''=> $anyValue);
		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		$this->view->cityCodeArr 	= $cityCodeArr;

		$arrCCY 					= $modelCity->getCCYId();

		$this->view->arrCcyId 	= $arrCCY;



		$arrPurpose 					= $modelCity->getTransPurposeId();

		$this->view->arrTPurpose 	= $arrPurpose;
		$Settings = new Settings();
		$address_mandatory = $Settings->getSetting('address_mandatory');
		$this->view->address_mandatory = $address_mandatory;

		$this->view->TransferDate = '1';
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts(array("ACCT_TYPE" => array('10','20')));	// show acc in IDR only
		//print_r($AccArr);die;
		if(!empty($AccArr)){
		$accsrc = $AccArr['0']['ACCT_NO'];
		$accsrctype = $AccArr['0']['ACCT_TYPE'];
		}
		$chargetype = $this->_request->getParam('chargetype');
		$this->view->chargetype = $chargetype;

		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;

		$beneficiary_acct_keyin = $Settings->getSetting('beneficiary_acct_keyin');
		$this->view->beneficiary_acct_keyin = $beneficiary_acct_keyin;
		//$svcInquiry = new Service_Inquiry();
		// $svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
	/*	$resultKursLld = $svcInquiry->ExchangeRateInquiry('USD');
		if($resultKursLld['ResponseCode']=='00'){
			$kursList = $resultKursLld['KursDevisaUmum'];
			$kursrate = '';
			$kursusdbuy = 0;
			$kursusdsell = 0;
			$stringkurs = '';
			foreach($kursList as $row){
				$row["Buy"] = str_replace(',','',$row["Buy"]);
				$row["Sell"] = str_replace(',','',$row["Sell"]);
				$stringkurs .= '<input type="hidden" id="kursbuy'.$row['Kurs'].'" name="kursbuy'.$row['Kurs'].'" value="'.$row["Buy"].'">';
				$stringkurs .= '<input type="hidden" id="kurssell'.$row['Kurs'].'" name="kurssell'.$row['Kurs'].'" value="'.$row["Sell"].'">';
			}
		}else{
// 			$kursusdbuy = 0;
// 			$kursusdsell = 0;
		}

		$this->view->stringkurs = $stringkurs;
*/

// 		$this->view->kurslldsell = $kursusdsell;

		$this->view->kurslldbuy = $kursusdbuy;
		$this->view->kurslldsell = $kursusdsell;
		//Transaction Purpose


		// $purposeList = array(''=>'-- Select Transaction Purpose --');
		// foreach ($purposeArr as $key => $value ){

		// 	$purposeList[$value['CODE']] = $value['DESCRIPTION'];
		// }
		// $this->view->TransPurposeArr = $purposeList;
		//end Transaction Purpose

		$date_val	= date('d/m/Y'); echo '<br>';
		$PS_EFDATE_VAL = $this->_request->getParam('PS_EFDATE');
		$this->view->paymentDate = $PS_EFDATE_VAL;


		if ($PS_EFDATE_VAL == $date_val){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}
		else{
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}

		$PS_PERIODIC_EVERY = $this->_request->getParam('PERIODIC_EVERY');
		$PS_PERIODIC_EVERY_DATE = $this->_request->getParam('PERIODIC_EVERYDATE');
		$PS_ENDDATE_VAL = ($this->_request->getParam('PS_ENDDATEPERIODIC'))?$this->_request->getParam('PS_ENDDATEPERIODIC'):date('d/m/Y');
		$this->view->endDatePeriodic = $PS_ENDDATE_VAL;

		$jenisTransfer =  $this->_request->getParam('tranferdatetype');
		$jenisPeriodic =  $this->_request->getParam('tranferdateperiodictype');

		//die ($jenisTransfer) ;
		if ($jenisTransfer == '1'){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
			$this->view->per = 0;
		}elseif ($jenisTransfer == '2'){
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
			$this->view->per = 0;
		}elseif ($jenisTransfer == '3'){
			$periodictranfer = $this->language->_('Periodic Transfer');
			$this->view->paymentType = $periodictranfer;
			$this->view->per = 1;
			if ($jenisPeriodic == '5'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY;
			}elseif ($jenisPeriodic == '6'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY_DATE;
			}
		}

		$data = $this->_db->fetchRow(
			$this->_db->select()
			->from(array('C' => 'M_USER'))
			->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
			->limit(1)
		);

		$this->view->userId				= $data['USER_ID'];
		$this->view->usermobilephone	= $data['USER_MOBILE_PHONE'];
		$this->view->tokentype = $data['TOKEN_TYPE'];
		$this->view->tokenIdUser = $data['TOKEN_ID'];

		$tokenType = $data['TOKEN_TYPE'];
		$tokenIdUser = $data['TOKEN_ID'];

		//added new hard token
		//$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		//$challengeCode = $HardToken->generateChallengeCode1();
		//$this->view->challengeCode = $challengeCode;

		//$challengeCodeSub = substr($challengeCode, 0,2);
		//$this->view->challengeCodeReq = $challengeCode;


		$this->view->error = false;
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$USE_CONFIRM_PAGE = true;
    	$isResultPage 	  = false;
    	$payReff 		  = '';

    	$periodicEveryArr = array(
    								'1' => $this->language->_('Monday'),
    								'2' => $this->language->_('Tuesday'),
    								'3' => $this->language->_('Wednesday'),
    								'4' => $this->language->_('Thursday'),
    								'5' => $this->language->_('Friday'),
    								'6' => $this->language->_('Saturday'),
    								'7' => $this->language->_('Sunday'),);
		$periodicEveryDateArr = range(1,31);

    	// Set variables needed in view
    	$paramSettingID = array('range_futuredate', //'auto_release', 'cut_off_time_inhouse'
								'cut_off_time_skn'   , 'cut_off_time_rtgs',
								'threshold_rtgs'     , 'threshold_lld');
		$typeOfSelect = array(
												'd' => 'Day(s)',
												'm' => 'Month(s)',
												'y' => 'Year(s)',
											);
		$settings 			= new Application_Settings();
		$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
		$ccyList  			= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
		$lldTypeArr  		= $settings->getLLDDOMType();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		// $lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		// $lldRelationshipArr = $settings->getLLDDOMRelationship();
		// $lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
		//$citizenshipArr 	= array_combine($this->_citizenship["desc"], $this->_citizenship["code"]);	// Resident => 'R', Non Resident => 'NR'
		//$nationalityArr 	= array_combine($this->_nationality["desc"], $this->_nationality["code"]);	// WNI => 'W', WNA => 'N'


		//$AccArr 	  = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR"), "ACCT_TYPE" => array('10','20')));


		// $this->view->TransactorArr = $lldRelationshipArr;
		// $this->view->IdentyArr = $lldIdenticalArr;

		//kurs rate
		$config = Zend_Registry::get('config');
		$appBankCode = $config['app']['bankcode'];

		// $svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
		//die;
// 		$resultKurs	= $svcInquiry->rateInquiry($appBankCode); //only for display
		//print_r($resultKurs);die;
		$this->view->kursArr = json_encode($resultKurs["DataList"]);
		$this->view->kursCount = count($resultKurs["DataList"]);


		$model = new purchasing_Model_Purchasing();

		$purposeArr = $model->getTranspurpose();
		$purposeList = array(''=>'-- Select Transaction Purpose --');
		foreach ($purposeArr as $key => $value ){

			$purposeList[$value['CODE']] = $value['DESCRIPTION'];
		}

		$this->view->TransPurposeArr = $purposeList;

		$filter 		= new Application_Filtering();
		$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
		$PS_EFDATE 		= date('d/m/Y');
		$isConfirmPage 	= $this->_getParam('confirmPage');
		$pdf 	= $this->_getParam('pdf');

		$process 	= $this->_getParam('process');
		if($this->_getParam('process') == 'back'){
			Zend_Session::namespaceUnset('TD');
		}
		$sessionNamespace 					= new Zend_Session_Namespace('TD');
		$submitBtn 	= ($this->_request->isPost() && $process == "submit")? true: false;

		// get ps_number data for repair payment
    	if (!empty($PS_NUMBER) && !$this->_request->isPost())
    	{
    		$paramList = array("WA" 			=> false,
							   "ACCOUNT_LIST" 	=> $this->_accountList,
							   "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
    		$select   = $CustomerUser->getPayment($paramList, false);	// not distinct, cause text cannot be selected as DISTINCT, it's not comparable
			$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);

			$select->columns(array(	"tra_message"			=> "T.TRA_MESSAGE",
									"tra_refno"				=> "T.TRA_ADDITIONAL_MESSAGE",
									"acbenef_email"			=> "T.BENEFICIARY_EMAIL",
									"acbenef_address"		=> "T.BENEFICIARY_ADDRESS",
									"acbenef_citizenship"	=> "T.BENEFICIARY_CITIZENSHIP", //WNI WNA W / R
									"acbenef_resident"		=> "T.BENEFICIARY_RESIDENT",
									"bank_name"				=> "T.BENEFICIARY_BANK_NAME",
									"bank_city"				=> "T.BENEFICIARY_BANK_CITY",
									"clr_code"				=> "T.CLR_CODE",
									"lld_desc"				=> "T.LLD_DESC",
									"lld_code"				=> "T.LLD_CODE",
									"transfer_type"			=> "T.TRANSFER_TYPE",
									"acbenef_category"		=> "T.BENEFICIARY_CATEGORY",
									"acbenef_identity_type" => "T.BENEFICIARY_ID_TYPE",
									"acbenef_identity_num"	=> "T.BENEFICIARY_ID_NUMBER",
									"acbenef_address1"		=> "T.BENEFICIARY_BANK_ADDRESS1",
									"acbenef_address2"		=> "T.BENEFICIARY_BANK_ADDRESS2",
									"acbenef_pob"			=> "T.POB_NUMBER",
									"acbenef_country"		=> "T.BENEFICIARY_BANK_COUNTRY",
									"lld_identity"			=> "T.LLD_IDENTITY",
									"lld_trans_purpose"		=> "T.LLD_TRANSACTION_PURPOSE",
									"lld_trans_rel"			=> "T.LLD_TRANSACTOR_RELATIONSHIP"
									//"acbenef_bankname"		=> "T.BENEFICIARY_ACCOUNT_NAME",
									//"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
							  	   )
							 );
			//echo $select;die;
			$pslipData 			= $this->_db->fetchRow($select);

			//print_r($pslipData);die;
			if (!empty($pslipData))
			{
				$PS_SUBJECT 		= $pslipData['paySubj'];
				// $PS_EFDATE  		= $pslipData['efdate'];
				$PS_EFDATE  		= date("d/m/Y",strtotime($pslipData['efdate']));

				$TRA_AMOUNT  		= Application_Helper_General::displayMoney($pslipData['amount']);
				$TRA_MESSAGE 		= $pslipData['tra_message'];
				$TRA_ADDMESSAGE 			= $pslipData['tra_refno'];
				$ACCTSRC 			= $pslipData['accsrc'];
				$ACCTSRC_CCY		= $pslipData['accsrc_ccy'];
				$ACBENEF  			= $pslipData['acbenef'];
				$ACBENEF_CCY  		= $pslipData['acbenef_ccy'];
				$CURR_CODE			= $pslipData['ccy'];
				$ACBENEF_BANKNAME	= $pslipData['acbenef_bankname'];

				$ACBENEF_ALIAS  	= $pslipData['acbenef_alias'];
				$ACBENEF_EMAIL  	= $pslipData['acbenef_email'];
				$ACBENEF_ADDRESS  	= $pslipData['acbenef_address'];
				$ACBENEF_CITIZENSHIP_CODE = $pslipData['acbenef_resident'];
				$ACBENEF_NATIONALITY_CODE = $pslipData['acbenef_citizenship'];
				$ACBENEF_CITIZENSHIP = ($ACBENEF_CITIZENSHIP_CODE == "R")? 'RESIDENT' : 'NON RESIDENT';
				$ACBENEF_NATIONALITY = ($ACBENEF_NATIONALITY_CODE == "W")? 'WNI' : 'WNA';
				$BANK_NAME			= $pslipData['bank_name'];
				$BANKNAME			= $pslipData['bank_name'];
				$BANK_CITY			= $pslipData['bank_city'];
				$CLR_CODE			= $pslipData['clr_code'];
				$LLD_DESC			= $pslipData['lld_desc'];
				$LLD_CODE			= $pslipData['lld_code'];
				$SWIFT_CODE			= $pslipData['SWIFT_CODE'];
				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE'];
				$TRANSFER_TYPE  	= $pslipData['transfer_type'];	// 1, 2

				$ACBENEF_CATEGORY = $lldCategoryArr[$pslipData['acbenef_category']];
				$ACBENEF_IDENTITY_TYPE = $pslipData['acbenef_identity_type'];
				$ACBENEF_IDENTITY_TYPE_DISP = $lldBeneIdentifArr[$ACBENEF_IDENTITY_TYPE];
				$ACBENEF_IDENTITY_NUM  = $pslipData['acbenef_identity_num'];

				$ACBENEF_BANK_ADD1 = $pslipData['acbenef_address1'];
				$ACBENEF_BANK_ADD2 = $pslipData['acbenef_address2'];
				$ACBENEF_POBNUMB   = $pslipData['acbenef_pob'];
				$country_code   = $pslipData['acbenef_country'];

				$LLD_IDENTITY  = $pslipData['lld_identity'];
				$LLD_TRANSACTOR_RELATIONSHIP = $pslipData['lld_trans_rel'];
				$TRANS_PURPOSE = $pslipData['lld_trans_purpose'];

				//get country name
				$getCountry = $this->_db->select()
								->from(array('M_COUNTRY'))
								->where('COUNTRY_CODE = ?', $country_code);

				$getCountry = $this->_db->fetchRow($getCountry);
				$ACBENEF_COUNTRY = $getCountry['COUNTRY_NAME'];


				if($TRANSFER_TYPE == 3)
					$CHARGE_TYPE = 'OUR';
				else
					$CHARGE_TYPE = 'SHA';

				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
				elseif ($PS_TYPE != $this->_paymenttype["code"]["remittance"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}

				$trfTypeArr = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);	// Array([2] => SKN, [1] => RTGS)
				$trfTypeArr += array('ONLINE' => 'ONLINE');
				$TRANSFER_TYPE = $trfTypeArr[$TRANSFER_TYPE];	// SKN, RTGS




			}
			else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
    	}

		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP'))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}

		//tambahan pentest
		$chargetype = $this->_request->getParam('chargetype');
		$this->view->chargetype = $chargetype;

		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;

		$this->view->PS_EFDATEFUTURE = $futureDate;


		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');


		if($this->_request->isPost())
		{
			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');

			if($this->_getParam('randomTransact') == $sessionNameRand->randomTransact){
				try {
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					$blocksize = 256;  // can be 128, 192 or 256
					$ACBENEF =  SGO_Helper_AES::decrypt( $this->_request->getParam('ACBENEF'), $passwordRand, $blocksize );
					$ACCTSRC =  SGO_Helper_AES::decrypt( $this->_request->getParam('ACCTSRC'), $passwordRand, $blocksize );
					$TRA_AMOUNT =  SGO_Helper_AES::decrypt( $this->_request->getParam('TRA_AMOUNT'), $passwordRand, $blocksize );
					$responseCodeReq =  SGO_Helper_AES::decrypt( $this->_request->getParam('responseCodeReq'), $passwordRand, $blocksize );
					$responseCodeEncrpty =  SGO_Helper_AES::decrypt( $this->_request->getParam('responseCode'), $passwordRand, $blocksize );
				}
				catch (Exception $e) {
					$ACBENEF = '';
					$ACCTSRC = '';
					$TRA_AMOUNT = '';
					$responseCodeReq = '';
					$responseCodeEncrpty = '';
				}

				//get all fields in form
// print_r($TRA_AMOUNT);die;
				$CURR_CODE = $this->_getParam('CURR_CODE');
				$CHARGE_TYPE = $this->_getParam('chargetype');
// print_r($ACBENEF);die;
				$ACCTSRC_CCY = $this->_getParam('ACCTSRC_CCY');

				$TRA_MESSAGE = $this->_getParam('TRA_MESSAGE');
				$TRANS_PURPOSE = $this->_getParam('TRANSPURPOSE_SELECT');

				$ACBENEF_NAME = $ACBENEF;
				$ACBENEF_CCY = $this->_getParam('ACBENEF_CURRENCY');




				$ACBENEF_ADDRESS = $this->_getParam('BANK_ADDRESS');
				$ACBENEF_CITIZENSHIP = $this->_getParam('ACBENEF_CITIZENSHIP'); // Resident / Non Resident
				$ACBENEF_CITIZENSHIP_CODE = ($ACBENEF_CITIZENSHIP == "Resident")? 'R' : 'NR';
				$ACBENEF_NATIONALITY = $this->_getParam('ACBENEF_NATIONALITY'); // WNI / WNA
				$ACBENEF_NATIONALITY_CODE = ($ACBENEF_NATIONALITY == "WNI")? 'W' : 'N';
				$ACBENEF_CATEGORY = $this->_getParam('LLD_CATEGORY');

				$ACBENEF = $this->_getParam('LLD_CATEGORY');

				// foreach($lldCategoryArr as $key => $val){
				// 	if($val == $ACBENEF_CATEGORY)
				// 		$ACBENEF_CATEGORY_CODE = $key;
				// }

				$ACBENEF_IDENTITY_TYPE = $this->_getParam('ACBENEF_IDENTY');
				$ACBENEF_IDENTITY_NUM = $this->_getParam('NRC');
				$ACBENEF_PHONE = $this->_getParam('ACBENEF_PHONE');

				$ACBENEF_BANKNAME = $this->_getParam('ACBENEF_BANKNAME');
				$BANKNAME = $this->_getParam('BANK_NAME');
				$ACBENEF_CONTACT = $this->_getParam('ACBENEF_CONTACT');
				$ACBENEF_REGION = $this->_getParam('ACBENEF_REGION');
				$BRANCH_NAME = $this->_getParam('BRANCH_NAME');

	// 			print_r($BRANCH_NAME);
	// die;
				$ACBENEF_BANK_ADD1 = $this->_getParam('ACBENEF_BANK_ADD1');
				$ACBENEF_BANK_ADD2 = $this->_getParam('ACBENEF_BANK_ADD2');
				$ACBENEF_CITY = $this->_getParam('ACBENEF_CITYNAME');
				$ACBENEF_POBNUM = $this->_getParam('ACBENEF_POBNUMB');
				$ACBENEF_COUNTRY = $this->_getParam('ACBENEF_COUNTRY');
				$ACBENEF_EMAIL = $this->_getParam('ACBENEF_EMAIL');
				// $LLD_IDENTITY = $this->_getParam('LLD_IDENTITY_SELECT');
				// $LLD_TRANSACTOR_RELATIONSHIP = $this->_getParam('LLD_TRANSACTOR_RELATIONSHIP_SELECT');
				$ACBENEF_IDENTITY_TYPE_DISP = $this->_getParam('ACBENEF_IDENTY_disp');

				$SWIFT_CODE = $this->_getParam('SWIFT_CODE');
				$AMOUNTUSD = $this->_getParam('amountusd');


				$this->view->error = false;

				$CHARGE_TYPE 		= $filter->filter($CHARGE_TYPE		, "SELECTION");
				// print_r($CHARGE_TYPE);die;
				$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PS_SUBJECT')		, "PS_SUBJECT");
				$TRA_AMOUNT 		= $filter->filter($TRA_AMOUNT		, "AMOUNT");
				$TRA_MESSAGE 		= $filter->filter($TRA_MESSAGE		, "TRA_MESSAGE");
				$TRA_ADDMESSAGE 			= $filter->filter($this->_request->getParam('TRA_ADDMESSAGE')		, "TRA_ADDMESSAGE");
				$ACCTSRC 			= $filter->filter($ACCTSRC			, "ACCOUNT_NO");
				$ACBENEF 			= $filter->filter($ACBENEF			, "ACCOUNT_NO");
				$TRANS_PURPOSE 		= $filter->filter($TRANS_PURPOSE		, "LLD_CODE");

				$ACBENEF_NAME 	= $filter->filter($ACBENEF_NAME	, "ACCOUNT_NAME");
				$ACBENEF_CONTACT 	= $filter->filter($ACBENEF_CONTACT	, "ACBENEF_CONTACT");
				$ACBENEF_REGION 	= $filter->filter($ACBENEF_REGION	, "ACBENEF_REGION");
				$BRANCH_NAME 	= $filter->filter($BRANCH_NAME	, "BRANCH_NAME");

				$ACBENEF_CCY 		= $filter->filter($ACBENEF_CCY		, "SELECTION");
				$ACBENEF_ADDRESS	= $filter->filter($ACBENEF_ADDRESS	, "ADDRESS");
				$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS')	, "ACCOUNT_ALIAS");

				//$ACBENEF_CITIZENSHIP= $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
				$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: 'Resident';

				$ACBENEF_NATIONALITY= $filter->filter($ACBENEF_NATIONALITY, "SELECTION");
				$this->view->ACBENEF_NATIONALITY= (isset($ACBENEF_NATIONALITY))	? $ACBENEF_NATIONALITY	: 'WNI';

				$ACBENEF_CATEGORY_CODE 	= $filter->filter($ACBENEF_CATEGORY_CODE, "LLD_CODE");
				$ACBENEF_IDENTITY_TYPE 	= $filter->filter($ACBENEF_IDENTITY_TYPE, "LLD_CODE");
				$ACBENEF_IDENTITY_NUM 	= $filter->filter($ACBENEF_IDENTITY_NUM	, "NRC");
				$ACBENEF_PHONE 	= $filter->filter($ACBENEF_PHONE	, "ACBENEF_PHONE");


				$ACBENEF_BANKNAME	= $filter->filter($ACBENEF_BANKNAME		, "BANK_NAME");
				$ACBENEF_BANK_ADD1		= $filter->filter($ACBENEF_BANK_ADD1		, "ADDRESS");
				$ACBENEF_BANK_ADD2		= $filter->filter($ACBENEF_BANK_ADD2		, "ADDRESS");
				$ACBENEF_CITY		= $filter->filter($ACBENEF_CITY		, "ADDRESS");
				$ACBENEF_EMAIL 		= $filter->filter($ACBENEF_EMAIL	, "EMAIL");

				$LLD_IDENTITY 		= $filter->filter($LLD_IDENTITY	, "LLD_CODE");
				$LLD_TRANSACTOR_RELATIONSHIP 	= $filter->filter($LLD_TRANSACTOR_RELATIONSHIP	, "LLD_CODE");

				$SWIFT_CODE			= $filter->filter($SWIFT_CODE		, "BANK_CODE");

				$AGREEMENT			= $filter->filter($this->_request->getParam('agreement')		, "agreement");
				$CHARGE_TYPE 		= $filter->filter($CHARGE_TYPE	, "SELECTION");


				$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
				// print_r($TRA_AMOUNT);die;
				$TrfDateType 		= $filter->filter($this->_request->getParam('tranferdatetype')	, "SELECTION");

				if(!empty($CHARGE_TYPE))
					$TRANSFER_TYPE = $CHARGE_TYPE;


				$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2'), "challengeCodeReq2");
				$challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq')	, "challengeCodeReq");
				$responseCodeReq 	= $filter->filter($responseCodeReq, "responseCodeReq");
				$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);

				$this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);


				if($this->_getParam('process') == 'back'){
					$TRANSFER_TYPE = '';
					$AGREEMENT = '';
					$PS_NUMBER = '';
					$PS_SUBJECT = '';
					$TRA_AMOUNT = '';
					$TRA_MESSAGE = '';
					$TRA_ADDMESSAGE = '';
					$TRA_MESSAGE_len = '';
					$TRA_ADDMESSAGE_len = '';
					$CURR_CODE = '';
					$TRANS_PURPOSE = '';
					$ACCTSRC = '';
					$ACCTSRC_view = '';
					$ACCTSRC_CCY = '';
					$ACBENEF = '';
					$ACBENEF_NAME = '';
					$ACBENEF_CCY = '';
					$ACBENEF_CITIZENSHIP = '';
					$ACBENEF_PHONE = '';
					$ACBENEF_ADDRESS = '';
					$ACBENEF_NATIONALITY = '';
					$ACBENEF_ALIAS = '';
					$ACBENEF_IDENTITY_TYPE 	= '';
					$ACBENEF_IDENTITY_NUM 	= '';
					$ACBENEF_CATEGORY 	= '';
					$ACBENEF_EMAIL = '';
					$ACBENEF_BANKNAME = '';
					$ACBENEF_BANK_ADD1 = '';
					$ACBENEF_BANK_ADD2 = '';
					$ACBENEF_CITY = '';
					$ACBENEF_POBNUM = '';
					$ACBENEF_COUNTRY = '';
					$SWIFT_CODE = '';
					$LLD_IDENTITY = '';
					$LLD_TRANSACTOR_RELATIONSHIP = '';
					$CHARGE_TYPE = '1';
					$TrfDateType = '1';
				}

				if(empty($sessionNamespace->psNumber))
				{
					// post submit payment
					if ($submitBtn)
					{
						$errorMsg = '';
						$PS_EFDATE			= date('d/m/Y');


						$filter->__destruct();
						unset($filter);
						{
							if($ACCTSRC_CCY!='USD'){

							}

							if($CURR_CODE == "USD"){
							$settingObj= new Settings();
							//print_r($TRA_AMOUNT);echo ' ';
							//print_r($settingObj->getSetting("threshold_lld_remittance"	, 0));die;

							}
							//cek date kosong
							if(!$PS_EFDATE){
								$error_msg		 		= $this->language->_('Payment Date can not be left blank').'.';
								$this->view->error 		= true;
								$this->view->ERROR_MSG	= $error_msg;

							}
							else{

								$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
								if(!$validateDateFormat->isValid($PS_EFDATE))
								{
									$error_msg = $this->language->_('Invalid Payment Date Format. Payment Date Format must be').' dd/MM/yyyy';
									$this->view->error 		= true;
									$this->view->ERROR_MSG	= $error_msg;
								}
								else{
									//new kebutuhan pentest (revisi security)
									if($isConfirmPage != 1){
										$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
										$sessionNameConfrim->benefAcct = $ACBENEF;
										$sessionNameConfrim->sourceAcct = $ACCTSRC;
										$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
									}
									else{
										$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
										$sessionNameConfrim->benefAcct;
										$sessionNameConfrim->sourceAcct;
										$sessionNameConfrim->traAmount;
									}
									if(!empty($sessionNameConfrim->sourceAcct)){
										$ACCTSRC = $sessionNameConfrim->sourceAcct;
									}
									$accsrcData = $CustomerUser->getAccounts(array("ACCT_NO" => $ACCTSRC));
									//$accsrcData = $model->getBeneficiaryByAccount($sessionNameConfrim->sourceAcct);
									//print_r($ACCTSRC);die;
									//$acctsrcCCY =  $accsrcData['0']['CURR_CODE'];
									$acctsrcCCY = $accsrcData['0']['CCY_ID'];

									// $svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
									$resultKursEx = $svcInquiry->exrateInquiry($ACBENEF_CCY);

									//rate inquiry for display
									if($resultKursEx['ResponseCode']=='00'){
										$kursList = $resultKursEx['KursDevisaUmum'];
										$kursrate = '';

										foreach($kursList as $row){
											if($acctsrcCCY == "IDR"){
												if($row["Kurs"] == $ACBENEF_CCY){
// 													print_r($row);die;
													$kursrate = Application_Helper_General::displayMoney($row["Sell"]);
													$kursbuy = Application_Helper_General::displayMoney($row["Buy"]);
												}
											}
											elseif($acctsrcCCY == "USD"){
												if($ACBENEF_CCY == "IDR" && $row["Kurs"] == "USD"){
													$kursrate = Application_Helper_General::displayMoney($row["Buy"]);
													$kursbuy = 0;
												}
												elseif($ACBENEF_CCY == "USD" && $row["Kurs"] == "USD"){
													$kursrate = Application_Helper_General::displayMoney($row["Buy"]);
													$kursbuy = 0;
												}
												else{
													$kursrate = 0;
													$kursbuy = 0;
												}
											}else{
												$kursrate = 0;
												$kursbuy = 0;
											}
										}
									}else{
										$kursrate = 0;
										$kursbuy = 0;
									}

									$this->view->ratekurs = $kursrate;
								


									//for Trf & Prov, CCY = ccy source acct
									//for FA, CCY = ccy trfed amount
									//charges
									$chargeTrf = $this->_db->fetchRow(
												$this->_db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 6)
												->where("CHARGE_CCY = ?", $acctsrcCCY)
												->where("CUST_ID = ?", (string)$this->_custIdLogin)
											);

									$chargeTrfAmt = $chargeTrf["CHARGE_AMT"];
									$chargeTrfCcy = $chargeTrf["CHARGE_AMOUNT_CCY"];
									// print_r($chargeTrf);die;

									//charges end

									$trfFee = $chargeTrfAmt; //sama dgn ccy source acct

// 									$resultKursEx = $svcInquiry->rateInquiry();

									//rate inquiry for display
									// if($resultKursEx['ResponseCode']=='00'){
									// 	$kursList = $resultKursEx['DataList'];
									// 	$kursrate = '';
									// 	$kursprovccyb = 'N/A';
									// 	$kursprovccys = 'N/A';
									// 	$kursproFAveb = 'N/A';
									// 	$kursproFAves = 'N/A';
									// 	$kursproveb = 'N/A';
									// 	$kursproves = 'N/A';
									// 	//print_r($kursList);die;
									// 	foreach($kursList as $row){
									// 		if($row["currency"] == $chargeTrfCcy){
									// 			$row["buy"] = str_replace(',','',$row["buy"]);
									// 			$row["sell"] = str_replace(',','',$row["sell"]);
									// 			$kursproves = $row["sell"];
									// 			$kursproveb = $row["buy"];
									// 		}

									// 		if($row["currency"] == $chargeFACcy){
									// 			$row["buy"] = str_replace(',','',$row["buy"]);
									// 			$row["sell"] = str_replace(',','',$row["sell"]);
									// 			$kursproFAves = $row["sell"];
									// 			$kursproFAveb = $row["buy"];
									// 		}

									// 		if($row["currency"] == $chargeProvCcy){
									// 			$row["buy"] = str_replace(',','',$row["buy"]);
									// 			$row["sell"] = str_replace(',','',$row["sell"]);
									// 			$kursprovccys = $row["sell"];
									// 			$kursprovccyb = $row["buy"];
									// 		}

									// 		if($acctsrcCCY == "IDR"){
									// 			if($row["currency"] == $ACBENEF_CCY){
									// 				// 													print_r($row);die;
									// 				$kursrate = str_replace(',','',$row["sell"]);
									// 				$kursbuy = str_replace(',','',$row["buy"]);
									// 				$bookrate = str_replace(',','',$row["book"]);
									// 				$bookbuy = 0;
									// 			}
									// 		}
									// 		elseif($acctsrcCCY == "USD"){
									// 		//	if($ACBENEF_CCY == "IDR" && $row["currency"] == "USD"){
									// 		//		$kursrate = str_replace(',','',$row["buy"]);
									// 		//		$kursbuy = 0;
									// 		//	}
									// 		//	print_r($row["currency"]);
									// 			if($ACBENEF_CCY == "USD" && $row["currency"] == "USD"){
									// 				$kursrate = str_replace(',','',$row["buy"]);
									// 				$kursbuy = 0;
									// 				$bookrate = str_replace(',','',$row["book"]);
									// 				$bookbuy = 0;
									// 			}

									// 		}else{
									// 			$kursrate = 0;
									// 			$kursbuy = 0;
									// 			$bookrate = 0;
									// 			$bookbuy = 0;
									// 		}


									// 	}
									// }else{
									// 	$kursrate = 0;
									// 	$kursbuy = 0;
									// 	$bookrate = 0;
									// 	$bookbuy = 0;
									// }
									//print_r($bookrate);die;
									//									print_r($kursrate);
									//									print_r($kursbuy);die;
									// $this->view->ratekurs = Application_Helper_General::displayMoney($kursrate);

									//book rate inquiry

									// 									$resultBook		= $svcInquiry->bookrateInquiry($appBankCode);

// 									if($resultKurs['ResponseCode']=='00'){
// 										$bookList = $resultKurs['DataList'];
// 										$bookrate = '';

// 										foreach($bookList as $row){
// 											if($acctsrcCCY == "IDR"){
// 												if($row["Kurs"] == $ACBENEF_CCY){
// 													$bookrate = str_replace(',','',$row["Sell"]);
// 													$bookbuy = str_replace(',','',$row["Buy"]);
// 												}
// 											}
// 											elseif($acctsrcCCY == "USD"){
// 												if($ACBENEF_CCY == "IDR" && $row["Kurs"] == "USD"){
// 													$bookrate = str_replace(',','',$row["Buy"]);
// 													$bookbuy = 0;
// 												}
// 												elseif($ACBENEF_CCY == "USD" && $row["Kurs"] == "USD"){
// 													$bookrate = str_replace(',','',$row["Buy"]);
// 													$bookbuy = 0;
// 												}
// 												else{
// 													$bookrate = 0;
// 													$bookbuy = 0;
// 												}
// 											}else{
// 												$bookrate = 0;
// 												$bookbuy = 0;
// 											}
// 										}
// 									}else{
// 										$bookrate = 0;
// 										$bookbuy = 0;
// 									}

									$notice = "<i>(Rates are subject to change without prior notice)</i>";



									if($TRANSFER_TYPE == "9"){
										$chargeTrf = $this->_db->fetchRow(
												$this->_db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 6)
												->where("CHARGE_CCY = ?", $acctsrcCCY)
												->where("CUST_ID = ?", (string)$this->_custIdLogin)
											);

									$chargeTrfAmt = $chargeTrf["CHARGE_AMT"];
									$chargeTrfCcy = $chargeTrf["CHARGE_AMOUNT_CCY"];

									// print_r($chargeTrfCcy);die;
										$chargedetail = $acctsrcCCY." ".Application_Helper_General::displayMoney($chargeTrfAmt);
										$totaleq = ($TRA_AMOUNT_num + $chargeTrfAmt);
										$this->view->charge = true;
										// print_r($TRA_AMOUNT_num);print_r($chargeTrfAmt);
										$totalview = $acctsrcCCY." ".Application_Helper_General::displayMoney($totaleq);

									}elseif($TRANSFER_TYPE == "10"){
										$chargeTrf = $this->_db->fetchRow(
												$this->_db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 6)
												->where("CHARGE_CCY = ?", $acctsrcCCY)
												->where("CUST_ID = ?", (string)$this->_custIdLogin)
											);

									$chargeTrfAmt = $chargeTrf["CHARGE_AMT"];
									$chargeTrfCcy = $chargeTrf["CHARGE_AMOUNT_CCY"];
										$this->view->charge = false;

										$chargedetail = $acctsrcCCY." ".Application_Helper_General::displayMoney($chargeTrfAmt);
										$totaleq = ($TRA_AMOUNT_num);
										$totalview = $acctsrcCCY." ".Application_Helper_General::displayMoney($totaleq);

									}
									// print_r($TRANSFER_TYPE);die;
									// print_r($TRA_AMOUNT_num);
// 									echo '<pre>';
									// var_dump($chargedetail);
									// var_dump($totalview);
									// die;
									// if(!empty($totaleq)){
									// 	$totalequsd = $totaleq/$kursbuy;
									// }else{
									// 	$totalequsd = 0;
									// }


									$this->view->chargedetail = $chargedetail;

									$forcharge = new Zend_Session_Namespace('forcharge');
									$forcharge->chargesDet = $chargedetail;

									$this->view->totalview = $totalview;

									$paramPayment = array();
									$paramTrxArr	  = array();

									$paramPayment = array(	"CATEGORY" 					=> "SINGLE LOCALREM",
															"FROM" 						=> "F",				// F: Form, I: Import
															"PS_NUMBER"					=> $PS_NUMBER,
															"PS_SUBJECT"				=> $PS_SUBJECT,
															"PS_EFDATE"					=> $PS_EFDATE,
															"_dateFormat"				=> $this->_dateDisplayFormat,
															"_dateDBFormat"				=> $this->_dateDBFormat,
															"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
															"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
															"_createPB"					=> false,	// cannot create PB trx
															"_createDOM"				=> false,	// cannot create DOM trx
															"_createREM"				=> $this->view->hasPrivilege('CDFT'),	// cannot create REM trx
														 );


									$paramTrxArr[0] = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
															"TRA_AMOUNT" 				=> $sessionNameConfrim->traAmount,
															"TRA_AMOUNTEQ"				=> $totaleq,
															//"TRA_AMOUNTEQ"				=> $totalinvalas,
															"TRA_MESSAGE" 				=> $TRA_MESSAGE,
															"TRA_ADDMESSAGE" 				=> $TRA_ADDMESSAGE,
															"ACCTSRC" 					=> $sessionNameConfrim->sourceAcct,
															"ACBENEF" 					=> $sessionNameConfrim->benefAcct,
															"ACBENEF_CCY" 				=> $ACBENEF_CCY,
															"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,

														// for Beneficiary data, except (bene CCY and email), must be passed by reference
															"ACBENEF_BANKNAME" 			=> &$ACBENEF_NAME,
															"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
															"ACBENEF_CITIZENSHIP" 		=> &$ACBENEF_CITIZENSHIP_CODE,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
															"ACBENEF_NATIONALITY" 		=> &$ACBENEF_NATIONALITY_CODE,
															"ACBENEF_ADDRESS1" 			=> &$ACBENEF_ADDRESS,
														//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
														//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

															"CURR_CODE"					=> $CURR_CODE, // sent amount ccy
															"CHARGE_TYPE"				=> $CHARGE_TYPE, //sha : our

															"BANK_CODE" 				=> $SWIFT_CODE,
															"BANK_NAME" 			=> $BANKNAME,
															"BANK_BRANCH" 				=> $BRANCH_NAME,
															"BANK_ADDRESS1" 		=> $ACBENEF_BANK_ADD1,
															"BANK_ADDRESS2" 		=> $ACBENEF_BANK_ADD2,
															"ACBENEF_CITY"				=> $ACBENEF_CITY,
															"ACBENEF_COUNTRY"			=> $ACBENEF_COUNTRY,
															"ACBENEF_POBNUM"			=> $ACBENEF_POBNUM,
														//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,

															"LLD_IDENTITY" 			=> $LLD_IDENTITY,
															"LLD_CATEGORY" 				=> $ACBENEF_CATEGORY_CODE, //yang akan dikirimkan ke vlink field 1
															"LLD_TRANSACTOR_RELATIONSHIP" 	=> $LLD_TRANSACTOR_RELATIONSHIP,
															"TRANS_PURPOSE" 				=> $TRANS_PURPOSE,
															"ACBENEF_IDENTITY_TYPE" 	=> $ACBENEF_IDENTITY_TYPE,
															"ACBENEF_IDENTITY_NUM" 		=> $ACBENEF_IDENTITY_NUM, //yang akan dikirimkan ke vlink field 2
															"ACBENEF_PHONE"	=> $ACBENEF_PHONE,

															//"ACCTSRC_ID_NUMBER"			 => $accsrcData['0']['BENEFICIARY_ID_NUMBER'],
															//"ACCTSRC_CITIZENSHIP"		 => $accsrcData['0']['BENEFICIARY_CITIZENSHIP'],
															//"ACCTSRC_RESIDENT"			 => $accsrcData['0']['BENEFICIARY_RESIDENT'],
															//"ACCTSRC_CATEGORY"			 => $accsrcData['0']['BENEFICIARY_CATEGORY'],

															// "TRANSFER_FEE"				=> $trfFee,
															"FULLAMOUNT_FEE"			=> $chargeTrfAmt,
															// "PROVISION_FEE"				=> $provFee,
															// "RATE_SELL"					=> $kursrate,
															// "RATE_BUY"					=> $kursbuy
															//"RATE_BUY"					=> $totalequsd

														 );

									if($isConfirmPage != 1){
										$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
										$sessionNameConfrim->ACBENEF_BANKNAME = $paramTrxArr[0]['ACBENEF_BANKNAME'];
									}
									else{
										$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
										$sessionNameConfrim->ACBENEF_BANKNAME;
									}


									$resAcct = array();
									// print_r($ACBENEF_REGION);die;
									$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
									$validate->setFlagConfirmPage(($isConfirmPage == 1)?TRUE:FALSE); //tujuan untuk set supaya jangan manggil inquiry lg di page ke 2
									// echo '<pre>';print_r($paramTrxArr);die;
									$resultVal	= $validate->checkCreateLocalRemit($paramPayment, $paramTrxArr, $resAcct);

									$sourceAccountType = $resAcct['productType'];
									//$infoWarning = $resAcct['infoHoliday']['infoWarning']; //permintaan mayapada hari libur dikasih warning saja

									if($isConfirmPage != 1){
										//update user acct info
										$sourceAcctCitizenship = $resAcct['Citizenship'];
										$sourceAcctNationality = $resAcct['National'];
										$sourceAcctIdType = $resAcct['IdentificationType'];
										$sourceAcctIdNumber = $resAcct['IdentificationNumber'];
										$sourceAcctCategory = $resAcct['Category'];
										$sourceAcctAddress = $resAcct['Address'];

										$updSource = array('ACCT_RESIDENT' => $sourceAcctCitizenship,
															'ACCT_CITIZENSHIP' => $sourceAcctNationality,
															'ACCT_CATEGORY' => $sourceAcctCategory,
															'ACCT_ID_TYPE' => $sourceAcctIdType,
															'ACCT_ID_NUMBER' => $sourceAcctIdNumber);

										$where = array();
										$where['ACCT_NO = ?'] = $sessionNameConfrim->sourceAcct;
										$where['CUST_ID = ?'] = $this->_custIdLogin;

										$this->_db->update('M_CUSTOMER_ACCT', $updSource, $where);
									}
									//done

									$reffId = $paramTrxArr[0]['ReffId'];
									if($isConfirmPage != 1){
										$this->view->reffIdSend = $reffId;
										$this->view->sourceAcctTypeGet = $sourceAccountType;
									}
									else{
										$reffIdGet 		= $this->_getParam('reffId');
										$this->view->reffIdSend = $reffIdGet;

										$sourceAcctTypeGet 		= $this->_getParam('sourceAcctType');
										$this->view->sourceAcctTypeGet = $sourceAcctTypeGet;
									}

									//validasi hard token page 1 -- begin
									// if($tokenType == '1'){ //sms token
									// 	$resultToken = TRUE;
									// }
									// elseif($tokenType == '2'){ //hard token
									// 	if($isConfirmPage == 1){
									// 		$resultToken = TRUE; //jika validasi appli 2 berhasil
									// 	}
									// 	else{
									// 		if(empty($responseCodeReq)){
									// 			$errMessage = 'Error : '.$this->language->_('Response Token cannot be left blank').'.';
									// 			$this->view->ERROR_MSG_TOKEN = $errMessage;
									// 		}
									// 		else{
									// 			//$resHard = $HardToken->verifyHardToken($challengeCodeReq2.$challengeCodeReq, $responseCodeReq);
									// 			//$resultToken = $resHard['ResponseCode'] == '0000';

									// 			$resultToken = TRUE;
									// 			//set user lock token gagal
									// 			$CustUser = new CustomerUser($this->_userIdLogin);
									// 			if ($resHard['ResponseCode'] != '0000'){
									// 				$tokenFailed = $CustUser->setFailedTokenMustLogout($resHard['ResponseTO']);
									// 				if ($tokenFailed)
									// 				$this->_forward('home');
									// 			}
									// 		}
									// 	}
									// }
									// elseif($tokenType == '3'){ //mobile token
									// 	$resultToken = TRUE;
									// }
									// else{$resultToken = TRUE;}
									//validasi hard token page 1 -- end

									$resultToken = TRUE;
									$datacif = $this->_db->fetchRow(
											$this->_db->select()
											->from(array('C' => 'M_USER'))
											->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
											->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
											->limit(1)
									);
									//print_r($datacif['CUST_CIF']);die;
									$svcInquiryCek = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype,null,null,$datacif['CUST_CIF']);
									//$resultCek		= $svcInquiryCek->getCheckUnderlying();

									$datacif = $this->_db->fetchRow(
											$this->_db->select()
											->from(array('C' => 'M_USER'))
											->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
											->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
											->limit(1)
									);
		//print_r($datacif);die;
									$resultToken = true;
									$demo = true;
		if(!$demo){
		// die;
		$select	= $this->_db->select()
							->from(array('T'=> 'T_TRANSACTION'),
								   array('TOTALUSD' 	=> 'SUM(T.EQUIVALENT_AMOUNT_USD)'								   		))
							->join(array('P'=> 'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',array())
							->where("P.CUST_ID 	= ?", (string)$datacif['CUST_ID'])
							->where("T.SOURCE_ACCOUNT_CCY = ? ",'IDR')
							->where("T.TRA_STATUS = ?", 3)
							->where("T.TRANSFER_TYPE in (3,4,7,8,9,10)")
							->where("MONTH(CONVERTSGO('DATE', P.PS_CREATED)) = MONTH(CONVERTSGO('DATE', GETDATE()))")
							->where("YEAR(CONVERTSGO('DATE', P.PS_CREATED)) = YEAR(CONVERTSGO('DATE', GETDATE()))");
		$remitData = $this->_db->fetchAll($select);
		if(!empty($remitData)){
			$totalamount = $remitData['0']['TOTALUSD'];
		}else{
			$totalamount = 0;
		}
			if($ACCTSRC_CCY =='USD' && $ACBENEF_CCY == 'USD'){
			    $remainLying = 0;
			   // $remainMonthTrx = $totalamount;
			    $checkUnderLying = true;
			    $checkUnderLyingService = true;
				//print_r($checkUnderLying);die;
			}else{
			    $resultCek		= $svcInquiryCek->getCheckUnderlying();
			    if(!empty($resultCek)){
			    $remainLying = $resultCek['remainBalance'];
			    $remainMonthTrx = $totalamount;
			    //print_r($remainLying);
			    //echo ' ';
			    //print_r($totalamount);
			    if($remainLying>=$totalamount){
			        $checkUnderLying = true;
			    }else{
			        $checkUnderLying = false;
			    }
			    $checkUnderLyingService = true;
			    }else{
				 $remainLying = 0;
			   // $remainMonthTrx = $totalamount;
			         $checkUnderLying = false;
			  	 $checkUnderLyingService = false;
			    }
			}
		}else{
				$checkUnderLying = true;
				$resultToken  = false;
				$checkUnderLyingService = true;
				// print_r($validate->isError());die;
		}
									//if($validate->isError() === false && $this->view->error === false)	// payment data is valid
									if($checkUnderLying && $validate->isError() === false )	// payment data is valid - new added
									{
										// die;
										$payment 		= $validate->getPaymentInfo();


										if ($USE_CONFIRM_PAGE == true && $isConfirmPage == false)
										{
											// if(!isset($arrPS_EFDATE[$key+1]))

											if ($ACBENEF_CCY == "IDR" && $ACCTSRC_CCY == "IDR"){

											}else{

												// get e-rate for notif kurs
												$svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);

										$kurssell = 0;
										$kursbuy = 0;


											}// notif kurs
														// print_r($resAcct);die;
											$isConfirmPage 	= true;

											$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
											$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
											$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_ALIAS"];
											$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
											$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $resAcct['CCY'], $resAcct['accountName'], "", $ACCTSRC_TYPE);
											$chargesCCY 	= $ACCTSRC_CCY;

											$validate->__destruct();
											unset($validate);

										}
										else
										{

											//check country in OFAC
											// $checkOFAC = $this->_db->select()
											// 	->from(array('M_OFAC'))
											// 	->where('country_name = ?', $ACBENEF_COUNTRY);

											$checkOFAC = array();

											if(empty($checkOFAC)){

												$validate->__destruct();
												unset($validate);

												$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
												$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
												$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_ALIAS"];
												$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
												$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $resAcct['CCY'], $resAcct['accountName'], "", $ACCTSRC_TYPE);
												$chargesCCY 	= $ACCTSRC_CCY;

												$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');

												$cutswift = substr($SWIFT_CODE,0,-3);

												//get nostro detail
												// $nostrodata = $this->_db->select()
												// 					->from(array('M_MEMBER_BANK'))
												// 					->where('CCY = ?', $ACBENEF_CCY)
												// 					->where('BANK_CODE = ?', $cutswift);

												// $nostrodata = $this->_db->fetchRow($nostrodata);
												// $nostrocode = (isset($nostrodata['NOSTRO_SWIFTCODE']))? $nostrodata['NOSTRO_SWIFTCODE'] : "";
												// $nostroname = (isset($nostrodata['NOSTRO_NAME']))? $nostrodata['NOSTRO_NAME'] : "";

													// $svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
// 												$resultKurs		= $svcInquiry->rateInquiry($ACBENEF_CCY);


												// if($resultKurs['ResponseCode']=='00'){

												// 	//$this->view->kurslist = json_encode($resultKurs['DataList']);
												// 	//$kurslist = json_encode($resultKurs['DataList']['USD']);

												// 	foreach ($resultKurs['DataList'] as $key => $val){
 											// 			if ($val['currency'] == $ACBENEF_CCY){

												// 			$kursbuy = $val['buy'];
												// 			$kurssell = $val['sell'];
												// 			$kursbuy = str_replace(',', '', $kursbuy);
												// 			$kurssell = str_replace(',', '', $kurssell);
 											// 			}
												// 		if ($val['currency'] == 'USD'){
												// 			$kursbuyusd = $val['buy'];
												// 			$kurssellusd = $val['sell'];
												// 			$kursbuyusd = str_replace(',', '', $kursbuyusd);
												// 			$kurssellusd = str_replace(',', '', $kurssellusd);
 											// 			}

												// 	}
												// }else{
												// 	$kursbuy = 'N/A';
												// 	$kurssell = 'N/A';
												// }

												//get country code
												// $getCountry = $this->_db->select()
												// 				->from(array('M_COUNTRY'))
												// 				->where('COUNTRY_NAME = ?', $ACBENEF_COUNTRY);

												// $getCountry = $this->_db->fetchRow($getCountry);
												// $countrycode = $getCountry['COUNTRY_CODE'];
												// $ACBENEF_NAME = $this->_getParam('ACBENEF');
												$param = array();
												$param['PS_SUBJECT'] 					= $PS_SUBJECT;
												$param['PS_CCY'] 					= $CURR_CODE;
												$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
												$param['TRA_AMOUNT'] 					= $sessionNameConfrim->traAmount;
												$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;
												$param['TRA_ADDMESSAGE'] 					= $TRA_ADDMESSAGE;

												$param['SOURCE_ACCOUNT'] 				= $sessionNameConfrim->sourceAcct;
												$param['SOURCE_ACCOUNT_CCY'] 			= $CURR_CODE;
												$param['SOURCE_ACCOUNT_NAME'] 			= $ACCTSRC_NAME;
												$param['sourceAccountType'] 			= $sourceAcctTypeGet;

												$param['BENEFICIARY_ACCOUNT'] 			= $sessionNameConfrim->benefAcct;
												$param['BENEFICIARY_ACCOUNT_CCY'] 		= $CURR_CODE;
												$param['BENEFICIARY_ACCOUNT_NAME'] 		= $ACBENEF_NAME;
												$param['BENEFICIARY_ALIAS_NAME'] 		= $ACBENEF_ALIAS;
												$param['BENEFICIARY_ADDRESS'] 			= $ACBENEF_ADDRESS;
												$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;
												$param['BENEFICIARY_CITIZENSHIP'] 		= $ACBENEF_NATIONALITY_CODE;
												$param['BENEFICIARY_RESIDENT'] 			= $ACBENEF_CITIZENSHIP_CODE;
												$param['BENEFICIARY_MOBILE_PHONE_NUMBER'] 			= $ACBENEF_PHONE;

												// $param['BENEFICIARY_MOBILE_PHONE_NUMBER'] 			= $ACBENEF_PHONE;
												$param['BENEFICIARY_BANK_NAME'] 		= $BRANCH_NAME;
												$param['BENEFICIARY_BANK_CITY'] 		= $ACBENEF_CITY;
												$param['BENEFICIARY_BANK_COUNTRY'] 		= $ACBENEF_COUNTRY;
												$param['POB_NUMBER']					= $ACBENEF_POBNUM;
												$param['BENEFICIARY_BANK_ADDRESS1']		= $ACBENEF_ADDRESS;
												$param['BENEFICIARY_BANK_ADDRESS2']		= $ACBENEF_BANK_ADD2;

												$param['USER_ID'] 		= $this->_userIdLogin;

												$param['ReffId'] 						= $reffIdGet;


												$param['SWIFT_CODE']					= $SWIFT_CODE;

												$param['TRANSFER_TYPE']		 			= $TRANSFER_TYPE; // OUR or SHA
												$param['LLD_CATEGORY']		 			= $ACBENEF_CATEGORY_CODE;
												$param['LLD_IDENTITY']		 			= $LLD_IDENTITY;
												$param['LLD_TRANSACTOR_RELATIONSHIP']	= $LLD_TRANSACTOR_RELATIONSHIP;
												$param['LLD_PURPOSE']		 			= $TRANS_PURPOSE;
												$param['LLD_DESCRIPTION']		 		= "";

												$param['LLD_BENEIDENTIF']		 		= $ACBENEF_IDENTITY_TYPE;
												$param['LLD_BENENUMBER']		 		= $ACBENEF_IDENTITY_NUM;


												$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
												$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
												$param['_priviCreate'] 					= 'CRSP';

												$param['TRANSFER_FEE']					= $trfFee;
												$param['FULLAMOUNT_FEE']				= $faFee;
												$param['PROVISION_FEE']					= $provFee;
												$param['RATE_BUY']						= $bookbuy;
												$param['RATE_SELL']						= $bookrate; //rate yg dipakai
												$param['DISPLAY_RATE']					= Application_Helper_General::convertDisplayMoney($kursrate);
												$param['DISPLAY_RATE_BUY']				= Application_Helper_General::convertDisplayMoney($kursbuy);

												$param['TRA_AMOUNTEQ']					= floor($totaleq*100)/100;



												//$param['AMOUNT_USD']					= ($sessionNameConfrim->traAmount*$kurssell) /$kursbuyusd;
												$param['NOSTRO_SWIFTCODE']				= $nostrocode;
												$param['NOSTRO_NAME']					= $nostroname;
												$param['TOTAL_CHARGES']					= $trfFee + $faFeeSourceCcy + $provFee;
												//print_r(($sessionNameConfrim->traAmount)); echo ' ';print_r($kursbuyusd);
												//print_r($param);die;
												$dateNow = date("Y-m-d H:i:s");



												try
												{

													$SinglePayment = new SinglePayment($PS_NUMBER,$this->_custIdLogin,$this->_userIdLogin);

													if (!empty($PS_NUMBER))
													{	$SinglePayment->isRepair = true;	}


													$resWs = array();
													//print_r($profee);
													// echo '<pre>';
													// print_r($param);die;

													$result = $SinglePayment->createPaymentLocalRemit($param);
													$ns = new Zend_Session_Namespace('FVC');
													//$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';

													if (!empty($PS_NUMBER))
														$ns->backURL = '/paymentworkflow/requestrepair/index/m/1';
													else
														$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';


													if ($result === true){
														$this->_redirect('/notification/success/index');
													}else{ //// TODO: what to do, if failed create payment
														//$this->_redirect('/notification/error/index');
													}

												}
												catch(Exception $e)
												{
													// Zend_Debug::dump($e);die;
													//$result = 'Code : '.$e->getCode().', Message : '.$e->getMessage();
													//echo "tes 123: ".$result;
												}


												/* FORCE LOGOUT */
												$locked = $this->_db->select()
												->from(array('M_USER'),array('USER_ISLOCKED'))
												->where('USER_ID = ?', $this->_userIdLogin)
												->limit(1);
												$locked = $this->_db->fetchRow($locked);
												$locked = $locked['USER_ISLOCKED'];

												if (isset($locked) && $locked == '1'){
													$CustomerUser->forceLogout();
													$this->redirect('/default/index/logout');
												}
											}
											else{
												$errMessage 	= $this->language->_("Beneficiary bank is in OFAC list. Payment can't be processed.");

												$this->view->error 		= true;
												$this->view->ERROR_MSG	= $errMessage;
											}
										}
									}
									else
									{


										$errMessage 	= '';
										$errorMsg 		= (!empty($errorMsg)) ? $errorMsg : $validate->getErrorMsg();
										$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

										$validate->__destruct();
										unset($validate);
										if(!empty($errorMsg) || !empty($errorTrxMsg))
											$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));

										$this->view->error 		= true;
										//print_r($errMessage);die;
										$this->view->ERROR_MSG	= $errMessage;

										if(empty($responseCodeReq)){}else{
											$this->view->ERROR_MSG_TOKEN = 'Error : '.$this->language->_('Invalid Token');
										}
										if($checkUnderLying){}else{
				//die('here1');
											$this->view->error 		= true;
											$this->view->ERROR_MSG	= $this->language->_('Underlying Error');

										}
										if($checkUnderLyingService){}else{
				//die('here1');
											$this->view->error 		= true;
											$this->view->ERROR_MSG	= $this->language->_('Underlying Service Error');

										}

//		die('here');
									} //end validate
								}
							}
						} //end validate dkk
					}
					else{
						$this->_redirect('/singlepayment/localremittance');
						$isConfirmPage 	= false;
					} // end if not submit
				}
				else{
					$ACCTSRC_view 	= $sessionNamespace->ACCTSRC_view;
					$charges 	= $sessionNamespace->charges;
					$payReff = $PS_NUMBER = $sessionNamespace->psNumber;
					$isConfirmPage 	= $sessionNamespace->isConfirmPage;
					$isResultPage 	= $sessionNamespace->isResultPage;
				}
			}
			else{
//				echo "<script type='text/javascript'>alert('Session expired')</script>";
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/singlepayment/localremittance');
			}

			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;

			//tambahn pentest
			if($tranferdatetype =='1'){
				$this->view->PS_EFDATE 			= $PS_EFDATE;
			}
			elseif ($tranferdatetype =='3'){
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORI;//
			}
			else{
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			}

		}
		else
		{
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;

			Zend_Session::namespaceUnset('TD');
		}


		//$this->view->infoWarning = $infoWarning;

		$TRA_MESSAGE_len = (isset($TRA_MESSAGE))? strlen($TRA_MESSAGE): 0;
		$TRA_ADDMESSAGE_len 	 = (isset($TRA_ADDMESSAGE))  ? strlen($TRA_ADDMESSAGE)  : 0;

		$TRA_MESSAGE_len = 140 - $TRA_MESSAGE_len;
		$TRA_ADDMESSAGE_len 	 = 200 - $TRA_ADDMESSAGE_len;

		$settingObj = new Settings();
		$this->view->COT_SKN			= $settingObj->getSetting("cut_off_time_skn"	, "00:00:00");
		$this->view->COT_RTGS			= $settingObj->getSetting("cut_off_time_rtgs", "00:00:00");
		$this->view->COT_BI				= $settingObj->getSetting("cut_off_time_bi"	, "00:00:00");
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);
		$this->view->LIMITLLD		= $settingObj->getSetting("threshold_lld_remittance"	, 0);
		$this->view->LIMITLLDMONEY		= Application_Helper_General::displayMoney($settingObj->getSetting("threshold_lld_remittance"	, 0));

		$this->view->AccArr 			= $AccArr;
		$this->view->transferTypeArr 	= array(
												'9' => '9',
												'10' => '10'
		);

		//$this->view->citizenshipArr 	= $citizenshipArr;
		//$this->view->nationalityArr 	= $nationalityArr;

		$this->view->lldCategoryArr 	= $lldCategoryArr;
		$this->view->lldIdenticalArr 	= $lldIdenticalArr;
		$this->view->lldRelationshipArr = $lldRelationshipArr;
		$this->view->lldPurposeArr 		= $lldPurposeArr;
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		$this->view->lldSenderIdentifArr  = $lldSenderIdentifArr;

		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';

		$this->view->PS_EFDATE 			= (isset($PS_EFDATE))			? $PS_EFDATE			: '';

		$amttemp = (isset($sessionNameConfrim->traAmount))? $sessionNameConfrim->traAmount : (!empty($TRA_AMOUNT)) ? $TRA_AMOUNT : '';
		$traamount = Application_Helper_General::displayMoney($amttemp);
		$this->view->TRA_AMOUNT 		= (isset($traamount))			? $traamount: '';
		$this->view->ACCTSRC_CCY 		= (isset($ACCTSRC_CCY))			? $ACCTSRC_CCY : '';
		$this->view->TRA_MESSAGE 		= (isset($TRA_MESSAGE))			? $TRA_MESSAGE			: '';
		$this->view->TRA_ADDMESSAGE 	= (isset($TRA_ADDMESSAGE))			? $TRA_ADDMESSAGE			: '';
		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;
		$this->view->TRA_ADDMESSAGE_len	= $TRA_ADDMESSAGE_len;

		$this->view->ACCTSRC 			= (isset($sessionNameConfrim->sourceAcct))				? $sessionNameConfrim->sourceAcct				: (!empty($ACCTSRC))? $ACCTSRC : '';
		$this->view->ACCTSRC_view		= (isset($ACCTSRC_view))		? $ACCTSRC_view			: '';
		$this->view->ACBENEF 			= (isset($ACBENEF_NAME))				? $ACBENEF_NAME				: (!empty($ACBENEF_NAME))? $ACBENEF_NAME : '';

		if($TRANSFER_TYPE == 'ONLINE'){
			$this->view->ACBENEF_BANKNAME_ONLINE	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
			$this->view->trType	= 'ONLINE';
		}
		else{
			$this->view->ACBENEF_BANKNAME	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: (!empty($ACBENEF_BANKNAME))? $ACBENEF_BANKNAME : '';
			$this->view->trType	= 'OTHER';
		}

		$this->view->CURR_CODE 			= (isset($CURR_CODE))			? $CURR_CODE			: '';
		$this->view->TRANSPURPOSE 		= (isset($TRANS_PURPOSE))		? $TRANS_PURPOSE			: '';

		if(!empty($TRANS_PURPOSE))
			$this->view->LLD_TRANSACTION_PURPOSE = $purposeList[$TRANS_PURPOSE];

		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->ACBENEF_EMAIL 		= (isset($ACBENEF_EMAIL))		? $ACBENEF_EMAIL		: '';
		$this->view->ACBENEF_CURRENCY 	= (isset($ACBENEF_CCY))		? $ACBENEF_CCY		: '';
		$this->view->ACBENEF_ADDRESS 	= (isset($ACBENEF_ADDRESS))		? $ACBENEF_ADDRESS		: '';
		//$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: 'R';

		$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: '';
		$this->view->ACBENEF_NATIONALITY= (isset($ACBENEF_NATIONALITY))	? $ACBENEF_NATIONALITY	: '';

		$this->view->BANK_NAME			= (isset($BANKNAME))		? $BANKNAME		: '';
		$this->view->ACBENEF_CITYNAME	= (isset($ACBENEF_CITY))			? $ACBENEF_CITY			: '';

		//$this->view->TRANSFER_TYPE 		= (isset($TRANSFER_TYPE))		? $TRANSFER_TYPE		: "SKN";
		$this->view->SWIFT_CODE 			= (isset($SWIFT_CODE))			? $SWIFT_CODE			: "";
		$this->view->ACBENEF_BANK_ADD1 		= (isset($ACBENEF_BANK_ADD1))	? $ACBENEF_BANK_ADD1			: "";
		$this->view->ACBENEF_BANK_ADD2 		= (isset($ACBENEF_BANK_ADD2))	? $ACBENEF_BANK_ADD2			: "";
		$this->view->ACBENEF_COUNTRY 		= (isset($ACBENEF_COUNTRY))			? $ACBENEF_COUNTRY			: "";
		$this->view->ACBENEF_POBNUMB 		= (isset($ACBENEF_POBNUM))			? $ACBENEF_POBNUM			: "";
		$this->view->ACBENEF_PHONE 		= (isset($ACBENEF_PHONE))			? $ACBENEF_PHONE			: "";

		$this->view->BRANCH_NAME 		= (isset($BRANCH_NAME))			? $BRANCH_NAME			: "";
		$this->view->BANK_ADDRESS 		= (isset($ACBENEF_ADDRESS))			? $ACBENEF_ADDRESS			: "";
		$this->view->ACBENEF_REGION 		= (isset($ACBENEF_REGION))			? $ACBENEF_REGION			: "";
		$this->view->ACBENEF_CONTACT 		= (isset($ACBENEF_CONTACT))			? $ACBENEF_CONTACT			: "";


		$this->view->LLD_CATEGORY 		= (isset($ACBENEF_CATEGORY))		? $ACBENEF_CATEGORY			: "";
		$this->view->LLD_IDENTITY 		= (isset($LLD_IDENTITY))		? $LLD_IDENTITY			: "";
		$this->view->LLD_TRANSACTOR_RELATIONSHIP 	= (isset($LLD_TRANSACTOR_RELATIONSHIP))	? $LLD_TRANSACTOR_RELATIONSHIP		: "";
		$this->view->LLD_PURPOSE 		= (isset($LLD_PURPOSE))			? $LLD_PURPOSE			: "";
		$this->view->LLD_DESCRIPTION 	= (isset($LLD_DESCRIPTION))		? $LLD_DESCRIPTION		: "";
		$this->view->ACBENEF_IDENTY 	= (isset($ACBENEF_IDENTITY_TYPE))		? $ACBENEF_IDENTITY_TYPE	: "";
		$this->view->ACBENEF_IDENTY_disp 	= (isset($ACBENEF_IDENTITY_TYPE_DISP))		? $ACBENEF_IDENTITY_TYPE_DISP	: "";
		$this->view->NRC 	= (isset($ACBENEF_IDENTITY_NUM))		? $ACBENEF_IDENTITY_NUM		: "";

		$this->view->PERIODIC_EVERY 	= (isset($PERIODIC_EVERY))		? $PERIODIC_EVERY		: '0';
		$this->view->PERIODIC_EVERYDATE = (isset($PERIODIC_EVERYDATE))	? $PERIODIC_EVERYDATE	: '0';
		$this->view->chargetype 		= (isset($CHARGE_TYPE))			? $CHARGE_TYPE			: '9';
		$this->view->TrfDateType 		= (isset($TrfDateType))			? $TrfDateType			: '1';
		$this->view->TrfPeriodicType 	= (isset($TrfPeriodicType))		? $TrfPeriodicType		: ''; //$TrfPeriodicType;

		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;

		if($isResultPage === true){
			$accData = $this->_db->select()
							->from('M_CUSTOMER_ACCT')
							->where('ACCT_NO = ?', $sessionNameConfrim->sourceAcct)
							->where('CUST_ID = ?', (string)$this->_custIdLogin);

			$accData = $this->_db->fetchRow($accData);
			$sourceAcctCitizenship = $accData['ACCT_RESIDENT'];
			$sourceAcctNationality = $accData['ACCT_CITIZENSHIP'];
			$sourceAcctCategory = $accData['ACCT_CATEGORY'];
			$sourceAcctIdType = $accData['ACCT_ID_TYPE'];
			$sourceAcctIdNumber = $accData['ACCT_ID_NUMBER'];
		}

		$resident = (isset($sourceAcctCitizenship))? $sourceAcctCitizenship : '';
		$nationality = (isset($sourceAcctNationality))? $sourceAcctNationality : '';

		$this->view->SOURCE_RESIDENT = ($resident == 'R')? 'Resident' : 'Non Resident';
		$this->view->SOURCE_NATIONALITY = ($nationality == 'W')? 'WNI' : 'WNA';
		$this->view->SOURCE_CATEGORY = (isset($sourceAcctCategory))? $lldCategoryArr[$sourceAcctCategory] : '';
		$this->view->SOURCE_ID_TYPE = (isset($sourceAcctIdType))? $lldSenderIdentifArr[$sourceAcctIdType] : '';
		$this->view->SOURCE_ID_NUMBER = (isset($sourceAcctIdNumber))? $sourceAcctIdNumber : '';
		//print_r($param['AMOUNT_USD']);die;


		$svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype,$acctsrcCCY);
// 		$resultKursLld = $svcInquiry->rateInquiry();
// 		if($resultKursLld['ResponseCode']=='00'){
// 			$kursList = $resultKursLld['DataList'];
// 			//print_r($kursList);die;
// 			$kursrate = '';
// 			$kursusdbuy = 0;
// 			$kursusdsell = 0;
// 			$stringkurs = '';
// 			$stringjava = '';
// 			foreach($kursList as $row){
// 				$row["buy"] = str_replace(',','',$row["buy"]);
// 				$row["sell"] = str_replace(',','',$row["sell"]);
// 				$stringkurs .= '<input type="hidden" id="kursbuy'.$row['currency'].'" name="kursbuy'.$row['currency'].'" value="'.$row["buy"].'">';
// 				$stringkurs .= '<input type="hidden" id="kurssell'.$row['currency'].'" name="kurssell'.$row['currency'].'" value="'.$row["sell"].'">';

// 				$stringjava .= 'var tagkursbuy = "kursbuy'.$row['currency'].'";
// 				var kursbuytrx'.$row['currency'].' = document.getElementById("kursbuy'.$row['currency'].'").value;
// 				kursbuytrx'.$row['currency'].' = kursbuytrx'.$row['currency'].'.split(",").join("");
// 				';
// 				$stringjava .= 'var tagkurssell = "kurssell'.$row['currency'].'";
// 				var kursselltrx'.$row['currency'].' = document.getElementById("kurssell'.$row['currency'].'").value;
// 				kursselltrx'.$row['currency'].' = kursselltrx'.$row['currency'].'.split(",").join("");
// 				';

// 				$stringelse .= 'else if(sourceccy == "IDR" && beneccy == "'.$row['currency'].'"){
// 		       		amount = traamount*kursselltrx'.$row['currency'].';
//  		       		amountusd = amount /kursbuytrxusd;
//  	 		       	displaylld = true;
//  		   	    }';

// 			}

// 			echo "<script type=\"text/javascript\">
// 					function cekamount(){
//  		        var traamount = document.getElementById(\"TRA_AMOUNT\").value;
// 			traamount = traamount.split(\",\").join(\"\");
//  		       var sourceccy = document.getElementById(\"ACCTSRC_CCY\").value;
//   		       var beneccy 	 = document.getElementById(\"ACBENEF_CURRENCY\").value;

// 				".$stringjava."
// 				var tagkursbuyusd = \"kursbuyUSD\";
// 				var kursbuytrxusd = document.getElementById(\"kursbuyUSD\").value;
// 				kursbuytrxusd = kursbuytrxusd .split(\",\").join(\"\");

// 				var amount = 0;
// 				var displaylld = false;
//  		       	    if(sourceccy == \"IDR\" && beneccy == \"JPY\"){
// 					//alert(kursselltrxjpy);
//  	 		       		amount = traamount*kursselltrxJPY;
//  	 		       		amountusd = amount /kursbuytrxUSD;
//  	 	 		       	displaylld = true;
//  	 		   }".$stringelse."

//  	 		   	var limitLLD = document.getElementById(\"LIMITLLD\").value;

// 			if(displaylld == true){
// 			document.getElementById(\"amountusd\").value = amountusd;

// 			if(parseInt(amountusd)>=parseInt(limitLLD) ){
//     		var x = document.getElementById(\"myHide\");
//     		var xtr = document.getElementById(\"myHidetr\");
//     		var xlld = document.getElementById(\"myHidelld\");
//     		var xs = document.getElementById(\"myHides\");
//    			x.style.display = \"\";
//     		xs.style.display = \"\";
//     		xtr.style.display = \"\";
//     		xlld.style.display = \"\";
// 	    	}else{
// 	    	var x = document.getElementById(\"myHide\");
// 	    	var xtr = document.getElementById(\"myHidetr\");
//     		var xlld = document.getElementById(\"myHidelld\");
//     		var xs = document.getElementById(\"myHides\");
//     		xs.style.display = \"none\";
//     		x.style.display = \"none\";
//     		xtr.style.display = \"none\";
//     		xlld.style.display = \"none\";

// 		    }
// 			}
// }
// 									      </script>
// 									     ";
// 		}else{
// // 			$kursusdbuy = 0;
// // 			$kursusdsell = 0;
// 		}

		$this->view->stringkurs = $stringkurs;

		if($this->_getParam('process') != 'back'){
			$this->view->PS_EVERY_PERIODIC1 		= (($this->_getParam('PS_EVERY_PERIODIC1')))			? $this->_getParam('PS_EVERY_PERIODIC1')			: 0;
			$this->view->PS_EVERY_PERIODIC5 		= (($this->_getParam('PS_EVERY_PERIODIC5')))			? $this->_getParam('PS_EVERY_PERIODIC5')			: 1;
			$this->view->PS_EVERY_PERIODIC6 		= (($this->_getParam('PS_EVERY_PERIODIC6')))			? $this->_getParam('PS_EVERY_PERIODIC6')			: 0;
			$this->view->payDateType 		= (isset($payDateType))			? $payDateType			: 2;
		}
		if(isset($PS_EVERY_PERIODIC_UOM)){ if($PS_EVERY_PERIODIC_UOM) $temps = $PS_EVERY_PERIODIC_UOM; };
		$this->view->PS_EVERY_PERIODIC_UOM 		= (isset($temps))	?  $temps : 1;
		$this->view->EXPIRY_DATE 		= (isset($EXPIRY_DATE))			? $EXPIRY_DATE			: '';
		if (isset($PS_NUMBER) && $pdf == 1)
		{
			$isConfirmPage = true;
			$isResultPage  = true;
			$payReff = $PS_NUMBER;
		}

		$this->view->confirmPage		= $isConfirmPage;
		$this->view->resultPage			= $isResultPage;
		$this->view->payReff			= $payReff;
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;
		$this->view->typeOfSelect		= $typeOfSelect;

		if (isset($PS_NUMBER) && $pdf == 1)
		{
			$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Payment',$outputHTML);
		}
		if($this->_request->getParam('printtrx') == 1){
			$this->_forward('printtrxremit', 'index', 'widget', array('data_caption' => 'Transfer Remittance'));
		}
		Application_Helper_General::writeLog('CDFT','Transfer Local Remittance');
	}
}
