<?php
/**
 * Temprootcommunity model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Temptrafficapicharges extends Changemanagement_Model_Tempchanges {
	protected $_moduleId = 'BGR';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {

		$minamtData = $this->dbObj->fetchAll($this->dbObj->select()
				  	  ->from('TEMP_CHARGES_API')
				  	  ->where('CHANGES_ID = ?',$this->_changeId)
				  	  );

		if(!empty($minamtData)){
        	
        	foreach ($minamtData as $minamt) {

        		$cekdata = $this->dbObj->fetchAll($this->dbObj->select()
	        				->from('M_CHARGES_API')
	        				->where('CUST_ID = ?', $minamt['CUST_ID'])
	        	);
	        	
	        	if(!empty($cekdata)){	
	        		
	         		$this->dbObj->delete('M_CHARGES_API', $this->dbObj->quoteInto('CUST_ID = ?', $minamt['CUST_ID']));
	
	        		$insertArr = array_diff_key($minamt,array('TEMP_ID'=>'','CHANGES_ID'=>''));
	        		$insertArr['CHARGES_APPROVED'] = date('Y-m-d H:i:s');
	        		$insertArr['CHARGES_APPROVEDBY'] = $actor;
	        		$minamtinsert = $this->dbObj->insert('M_CHARGES_API',$insertArr);
	        	}else{
	        		$insertArr = array_diff_key($minamt,array('TEMP_ID'=>'','CHANGES_ID'=>''));
	        		$insertArr['CHARGES_APPROVED'] = date('Y-m-d H:i:s');
	        		$insertArr['CHARGES_APPROVEDBY'] = $actor;
	        		$minamtinsert = $this->dbObj->insert('M_CHARGES_API',$insertArr);
	        		
	        	}

	        	if(!(boolean)$minamtinsert) {
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(API Charges)';
					return false;
				}
        	}

        	$delete = $this->dbObj->delete('TEMP_CHARGES_API',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
        }

        return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate() {
		
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate() {
		
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

		//delete from TEMP_CHARGES_API
		$this->dbObj->delete('TEMP_CHARGES_API',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));


		return true;

	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit($actor = null) {

	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteDelete() {
		//will never be called
	}
}