<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/Customer.php';		
	require_once 'CMD/Payment.php';
	require_once 'General/Settings.php';  
	
	$emailSent = 0;
	$payReffArr = array();
	$type = 'REJECTED';
	$config    = Zend_Registry::get('config');
	
	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	
	$template = file_get_contents(LIBRARY_PATH.'/email template/Approver Releaser Maker Email Notification.html');
	$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
	$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
	
	$transFT   = array( '[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
						'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
						'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl													
					  );			
	$FOOTER_ID = strtr($FOOTER_ID, $transFT);
	$FOOTER_EN = strtr($FOOTER_EN, $transFT);
				
	$db = Zend_Db_Table::getDefaultAdapter();
	$subject = 'EMAIL REJECTED PAYMENT FOR MAKER';
	
	
	$paymentType  = $config["payment"]["type"];	
	$transferType = $config["transfer"]["type"];
	$paymentStatus = $config["payment"]["status"];

	$payReffArr = array();
	
	$data = $db->SELECT()->DISTINCT()
						->FROM(array('P'=>'T_PSLIP'),array('CUST_ID','PS_CREATEDBY'))
						->JOINLEFT(array('E' => 'T_EMAIL_NOTIFICATION'),'P.PS_NUMBER = E.PS_NUMBER',array())
						->WHERE('P.PS_STATUS = ?', (string)$paymentStatus["code"]["rejected"])	
						->WHERE('E.PS_ISREJECTEDEMAILED = 0 OR E.PS_ISREJECTEDEMAILED is null')
						->QUERY()->FETCHALL();
	if($data)
	{
		foreach($data as $cust)
		{
			$rejectList = $db->SELECT()
								->FROM('M_USER',array('CUST_ID','USER_ID','USER_FULLNAME','USER_EMAIL'))
								->WHERE('CUST_ID =?',$cust['CUST_ID'])
								->WHERE('USER_ID = ?',$cust['PS_CREATEDBY'])
								->WHERE('USER_ISEMAIL = 1')
								->QUERY()->FETCHALL();
								
			if($rejectList)
			{
				foreach($rejectList as $cust_user)
				{					
					$payment = new Payment();
					$query = $payment->getPaymentList();
					$query ->JOINLEFT(array('TEN'=>'T_EMAIL_NOTIFICATION'),'P.PS_NUMBER = TEN.PS_NUMBER',array())
								-> WHERE('PS_ISREJECTEDEMAILED <> 1 or PS_ISREJECTEDEMAILED is NULL')
								->WHERE('UPPER(CUST_ID) = ?',$cust_user['CUST_ID'])
								->WHERE('UPPER(PS_CREATEDBY) = ?',$cust_user['USER_ID'])
								->WHERE('P.PS_STATUS = ?', (string)$paymentStatus["code"]["rejected"]);
					$paymentList = $db->FETCHALL($query);
					
					if($paymentList)
					{
						$content = '';
						$counter = 0;
						foreach($paymentList as $paymentdata)
						{
							$caseBeneBank = "CASE P.PS_CATEGORY WHEN 'BULK PAYMENT' THEN CASE P.PS_TYPE WHEN '5' THEN '".$config['app']['bankname']."' ELSE '-' END ELSE CASE P.PS_TYPE WHEN '2' THEN T.BENEFICIARY_BANK_NAME ELSE '".$config['app']['bankname']."' END END";
							$caseMessage = "CASE P.PS_CATEGORY WHEN 'SINGLE PAYMENT' THEN T.TRA_MESSAGE ELSE '-' END";
							
							$info = $db->select()->distinct()
										->from(array('P' => 'T_PSLIP'),array(	'benebank' 	=> new Zend_Db_Expr($caseBeneBank),
																				'message' 	=> new Zend_Db_Expr($caseMessage)))
										->join(array('T' => 'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',array())
										->where('P.PS_NUMBER LIKE '.$db->quote($paymentdata['payReff']));
							//echo $info;die;
							$inforesult = $db->fetchRow($info);
							
								$payReffArr[] = $paymentdata['payReff'];
								$content .= '
								<tr>
									<td><font>'.++$counter.'</font></td>
									<td><font>'.$paymentdata['payReff'].'</font></td>
									<td><font>'.$paymentdata['paySubj'].'</font></td>
									';
								if($paymentdata['accsrc'] == '-')
								{
									$content .= '	<td><font>'.$paymentdata['accsrc'].'</font></td>
									';
								}
								else{
								$content .= '	<td><font>'.$paymentdata['accsrc'].'( '.$paymentdata['accsrc_ccy'].' ) - '.$paymentdata['accsrc_name'].'</font></td>
								';
								}
								if($paymentdata['acbenef'] == '-')
								{
									$content .= '	<td><font>'.$paymentdata['acbenef'].'</font></td>
									';
								}
								else{
								$content .= '	<td><font>'.$paymentdata['acbenef'].'( '.$paymentdata['acbenef_ccy'].' ) - '.$paymentdata['acbenef_name'].'</font></td>
								';
								}
								$content .= '	<td><font>'.$paymentdata['ccy'].' '.Application_Helper_General::displayMoney($paymentdata['amount']).'</font></td>
									<td><font>'.$inforesult['message'].'</font></td>
									<td><font>'.Application_Helper_General::convertDate($paymentdata['efdate'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
									<td><font>'.$inforesult['benebank'].'</font></td>
									<td><font>'.$paymentdata['PS_CREATEDBY'].'</font></td>								
									<td><font>'.Application_Helper_General::convertDate($paymentdata['created'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
									<td><font>'.$paymentdata['payType'].'</font></td>
									<td><font>'.$paymentdata['payStatus'].'</font></td>
								</tr>';	
								$translate = array( '[[TITLE]]' => 'Rejected Payment Notification for Maker',
													'[[USER_FULLNAME]]' => $cust_user['USER_FULLNAME'],
													'[[USER_ID]]' => $cust_user['USER_ID'],
													'[[CUST_ID]]' => $cust_user['CUST_ID'],															
										);			
								$mailContent = strtr($template,$translate);			
							
						}														
							
							if($content != ''){
								$subject = 'Rejected Email Notification';		
								$translate = array( 
											'[[DATA_CONTENT]]' => $content,
											'[[FOOTER_ID]]' 		=> $FOOTER_ID,
											'[[FOOTER_EN]]' 		=> $FOOTER_EN,
											'[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
											'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
											'[[APP_URL]]' 				=> $templateEmailMasterBankAppUrl,
											'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
											'[[master_bank_fax]]' 	=> $templateEmailMasterBankFax,
											'[[master_bank_address]]' 	=> $templateEmailMasterBankAddress,
											'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,
										);
								$mailContent = strtr($mailContent,$translate);																												
								$status = Application_Helper_Email::sendEmail($cust_user['USER_EMAIL'],$subject,$mailContent);									
							}												
							
							if($status)
							{
								$emailSent++;
							}
							echo $emailSent.' '.$status.' Sent';
					}
					
					$payment->__destruct();
					unset($payment);
				}
			}
		}
		$msg = '';
		if($emailSent)
		{$msg .= $emailSent.' Mail Sent';}

		Application_Helper_General::cronLog(basename(__FILE__),$msg,1);
		Application_Helper_General::setPaymentEmailNotification($type, $payReffArr);	
	}		

?>
