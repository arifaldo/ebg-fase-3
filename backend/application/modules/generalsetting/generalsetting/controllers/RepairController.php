<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class Generalsetting_RepairController extends Application_Main
{
	
	public function initController(){
			  $this->_helper->layout()->setLayout('newpopup');
	}

    public function indexAction()
	{
		$this->view->nocd = false;
		$changes_id = $this->_getParam('changes_id');
		
		$select = $this->_db->select()
					->from(array('A' => 'TEMP_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					->where('MODULE_ID = ?' , 'GNS')
					->where('CHANGES_ID = ?', $changes_id)
					->query()->FetchAll();

		if($select)
		{
			$setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');
			foreach($setting as $key=>$value)
			{			
				$this->view->$key = $value;
			}

			// convert to validate
//			$threshold_rtgs = $this->_getParam('threshold_rtgs');		
//			$threshold_rtgs_val =	Application_Helper_General::convertDisplayMoney($threshold_rtgs);
//			$this->_setParam('threshold_rtgs',$threshold_rtgs_val);

			$threshold_lld = $this->_getParam('threshold_lld');		
			$threshold_lld_val =	Application_Helper_General::convertDisplayMoney($threshold_lld);
			$this->_setParam('threshold_lld',$threshold_lld_val);

			$admin_fee_company = $this->_getParam('admin_fee_company');		
			$admin_fee_company_val =	Application_Helper_General::convertDisplayMoney($admin_fee_company);
			$this->_setParam('admin_fee_company',$admin_fee_company_val);
			
			$admin_fee_account = $this->_getParam('admin_fee_account');		
			$admin_fee_account_val =	Application_Helper_General::convertDisplayMoney($admin_fee_account);
			$this->_setParam('admin_fee_account',$admin_fee_account_val);
			
			$global_charges_skn = $this->_getParam('global_charges_skn');		
			$global_charges_skn_val =	Application_Helper_General::convertDisplayMoney($global_charges_skn);
			$this->_setParam('global_charges_skn',$global_charges_skn_val);
			
			$global_charges_rtgs = $this->_getParam('global_charges_rtgs');		
			$global_charges_rtgs_val =	Application_Helper_General::convertDisplayMoney($global_charges_rtgs);
			$this->_setParam('global_charges_rtgs',$global_charges_rtgs_val);
			
			$min_amount_skn = $this->_getParam('min_amount_skn');		
			$min_amount_skn_val =	Application_Helper_General::convertDisplayMoney($min_amount_skn);
			$this->_setParam('min_amount_skn',$min_amount_skn_val);
			
			$max_amount_skn = $this->_getParam('max_amount_skn');		
			$max_amount_skn_val =	Application_Helper_General::convertDisplayMoney($max_amount_skn);
			$this->_setParam('max_amount_skn',$max_amount_skn_val);
			
			$min_amount_rtgs = $this->_getParam('min_amount_rtgs');		
			$min_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($min_amount_rtgs);
			$this->_setParam('min_amount_rtgs',$min_amount_rtgs_val);
			
			
			$max_amount_rtgs = $this->_getParam('max_amount_rtgs');		
			$max_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($max_amount_rtgs);
			$this->_setParam('max_amount_rtgs',$max_amount_rtgs_val);

			$threshold_lld_remittance = $this->_getParam('threshold_lld_remittance');		
			$threshold_lld_remittance_val =	Application_Helper_General::convertDisplayMoney($threshold_lld_remittance);
			$this->_setParam('threshold_lld_remittance',$threshold_lld_remittance_val);

			$remittance_limit_per_month = $this->_getParam('remittance_limit_per_month');
			$remittance_limit_per_month_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_per_month);
			$this->_setParam('remittance_limit_per_month',$remittance_limit_per_month_val);

			
			$escrow_acct_idr = $this->_getParam('escrow_acct_idr');
			// $escrow_acct_idr_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_idr);
			$this->_setParam('escrow_acct_idr',$escrow_acct_idr);
				
			$escrow_acct_usd = $this->_getParam('escrow_acct_usd');
			// $escrow_acct_usd_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_usd);
			$this->_setParam('escrow_acct_usd',$escrow_acct_usd);
			
// 			$remittance_limit_per_month = $this->_getParam('remittance_limit_per_month');		
// 			$remittance_limit_per_month_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_per_month);
// 			$this->_setParam('remittance_limit_per_month',$remittance_limit_per_month_val);
			
			
			
			/* $SKN_cost = $this->_getParam('SKN_cost');		
			$SKN_cost_val =	Application_Helper_General::convertDisplayMoney($SKN_cost);
			$this->_setParam('SKN_cost',$SKN_cost_val);

			$RTGS_cost = $this->_getParam('RTGS_cost');		
			$RTGS_cost_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_cost',$RTGS_cost_val); */
			
			/* $SKN_charge = $this->_getParam('SKN_charge');		
			$SKN_charge_val =	Application_Helper_General::convertDisplayMoney($SKN_charge);
			$this->_setParam('SKN_charge',$SKN_charge_val);

			$RTGS_charge = $this->_getParam('RTGS_charge');		
			$RTGS_charge_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_charge',$RTGS_charge_val); */
			
			//FILTER
			{
			$filters = array(
							
							);
			}

			//VALIDATE
			{
			$validators = array(
									
															);
			}

			$cek = $this->_db->select()
								->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID','SETTING_VALUE','SETTING_ID'))
								->WHERE ('MODULE_ID = ?','GNS')
								->query()->FetchAll();
			// echo '<pre>';
			// print_r($cek);die;
			$this->view->colomnlist = $cek;

			if($cek != null)
			{
				if($this->_request->isPost() && $this->view->hasPrivilege('STCR'))
				{
					
					//validasi global admin fee
					
					
					$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
					if($this->_getParam('email_exception'))
					{
						$validate = new validate;
						$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('email_exception'));
					}
					else
					{
						$cek_multiple_email = true;
					}
					 
					//validasi multiple email
					if($this->_getParam('email_valas'))
					{
						$validate = new validate;
						$cek_multiple_email_valas = $validate->isValidEmailMultiple($this->_getParam('email_valas'));
					}
					else
					{
						$cek_multiple_email_valas = true;
					}
					
					if($zf_filter->isValid())
					{
						
							$this->_db->beginTransaction();
							try{
								$arrfck = array();
								$info = "GENERAL SETTING";
								foreach ($cek as $key => $value) {
									$arrfck[] = $value['SETTING_ID'];
								}

								//$change_id = $this->suggestionWaitingApproval('Change General Setting',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','MODULE_ID','GNS');
								//$change_id = 1;
								// $arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd','clause_template');
								// print_r($arrfck);die;
								//DELETE DATA TEMP
										$where = array('CHANGES_ID = ?' => $changes_id);
										$this->_db->delete('TEMP_SETTING',$where);
										
								foreach($setting as $key=>$value){
									if (in_array($key, $arrfck)) {
										$valuee = $this->_getParam2($key);									
										$data = array(	'CHANGES_ID' => $changes_id,
														'SETTING_ID' => $key,
														'SETTING_VALUE' => htmlspecialchars_decode($valuee),
														'MODULE_ID' => 'GNS',
														);
										$this->_db->insert('TEMP_SETTING',$data);							
									}									
									// else
									// {
									// 	$data = array(	'CHANGES_ID' => $changes_id,
									// 			'SETTING_ID' => $key,
									// 			'SETTING_VALUE' => ($zf_filter->$key),
									// 			'MODULE_ID' => 'GNS',
									// 			);							
									// }
										
								}

									$this->_db->commit();
									
									//UPDATE STATUS GLOBAL CHANGES		
									$this->updateGlobalChanges($changes_id,$info);	
									Application_Helper_General::writeLog('STCR','Submiting Repair General Setting');
									$this->view->success = true;						    
									$msg = "Settings Change Request Saved";
									$this->view->report_msg = $msg;				
									$this->setbackURL('/changemanagement');
									$this->_redirect('/popup/submited/index');		    	    	
							}
							catch(Exception $e)
							{
									$this->_db->rollBack();					
							}
					}
					else{
						$docErr = $this->language->_('Error in processing form values. Please correct values and re-submit.');
						$this->view->report_msg = $docErr;

						foreach($zf_filter->getMessages() as $key=>$err){
							$xxx = 'x'.$key;
							$this->view->$xxx = $this->displayError($err);
						}
						if (isSet($cek_multiple_email) && $cek_multiple_email == false){
							$this->view->xemail_exception = $this->language->_('Invalid email format');
						}else {
							$this->view->xemail_exception = null;
						}
						
						if (isSet($cek_multiple_email_valas) && $cek_multiple_email_valas == false){
							$this->view->xemail_valas = $this->language->_('Invalid email format');
						}else {
							$this->view->xemail_valas = null;
						}
					}
				}
			}
			else{
				$this->view->error = true;
				//$this->fillParam($zf_filter_input);
				$this->view->report_msg = $this->language->_('Changes not found');
			}
			
			if($this->_request->isPost()){
				$this->view->isenabled = $this->_getParam('isenabled');
				$this->view->disable_note = $zf_filter->disable_note;
				$this->view->note = $zf_filter->note;
				$this->view->paging_frontend = $zf_filter->paging_frontend;
				$this->view->max_import_bene = $zf_filter->max_import_bene;
				$this->view->max_import_single_payment = $zf_filter->max_import_single_payment;
				$this->view->range_futuredate = $zf_filter->range_futuredate;
				$this->view->archiveafter = $zf_filter->archiveafter;
				$this->view->cut_off_time_rtgs = $zf_filter->cut_off_time_rtgs;
				$this->view->cut_off_time_skn = $zf_filter->cut_off_time_skn;
				$this->view->cut_off_time_remittance = $zf_filter->cut_off_time_remittance;
				$this->view->cut_off_time_bi = $zf_filter->cut_off_time_bi;
				
				$this->view->start_running_balance = $zf_filter->start_running_balance;
				$this->view->cut_off_running_balance = $zf_filter->cut_off_running_balance;
				
				$this->view->starting_time_remittance = $zf_filter->starting_time_remittance;
				$this->view->threshold_rtgs = $zf_filter->threshold_rtgs;
				$this->view->threshold_lld = $zf_filter->threshold_lld;
				$this->view->threshold_lld_remittance = $zf_filter->threshold_lld_remittance;
// 				$this->view->remittance_limit_per_month = $zf_filter->remittance_limit_per_month;
				$this->view->max_row_multi_transaction = $zf_filter->max_row_multi_transaction;
				$this->view->max_import_bulk = $zf_filter->max_import_bulk;
				$this->view->clause_template = $this->_getParam('clause_template');
				$this->view->ftemplate_remittance = $this->_getParam('ftemplate_remittance');
				
				// $this->view->cut_off_time_inhouse = $zf_filter->cut_off_time_inhouse;
				$this->view->pwd_expired_day = $zf_filter->pwd_expired_day;
				$this->view->timeoutafter = $zf_filter->timeoutafter;
				$this->view->locknologinafter = $zf_filter->locknologinafter;
				$this->view->lockfailloginafter = $zf_filter->lockfailloginafter;
				$this->view->lockfailtokenafter = $zf_filter->lockfailtokenafter;
				$this->view->password_history = $zf_filter->password_history;
				$this->view->maxfpassword = $zf_filter->maxfpassword;
				$this->view->b_timeoutafter = $zf_filter->b_timeoutafter;
				$this->view->b_locknologinafter = $zf_filter->b_locknologinafter;
				$this->view->b_lockfailloginafter = $zf_filter->b_lockfailloginafter;
				$this->view->bpassword_history = $zf_filter->bpassword_history;
				$this->view->maxbpassword = $zf_filter->maxbpassword;
				$this->view->email_submission = $zf_filter->email_submission;
			//	$this->view->ftemplate_newuser = $zf_filter->ftemplate_newuser;
			//	$this->view->femailtemplate_newuser = $zf_filter->femailtemplate_newuser;
			//	$this->view->ftemplate_resetpwd = $zf_filter->ftemplate_resetpwd;
			//	$this->view->femailtemplate_resetpwd = $zf_filter->femailtemplate_resetpwd;
			//	$this->view->btemplate_newuser = $zf_filter->btemplate_newuser;
			//	$this->view->bemailtemplate_newuser = $zf_filter->bemailtemplate_newuser;
			//	$this->view->btemplate_resetpwd = $zf_filter->btemplate_resetpwd;
			//	$this->view->bemailtemplate_resetpwd = $zf_filter->bemailtemplate_resetpwd;		
				$this->view->ftemplate_newuser = $this->_getParam('ftemplate_newuser');
				$this->view->femailtemplate_newuser = $this->_getParam('femailtemplate_newuser');
				$this->view->ftemplate_resetpwd = $this->_getParam('ftemplate_resetpwd');
				$this->view->femailtemplate_resetpwd = $this->_getParam('femailtemplate_resetpwd');
				$this->view->btemplate_newuser = $this->_getParam('btemplate_newuser');
				$this->view->bemailtemplate_newuser = $this->_getParam('bemailtemplate_newuser');
				$this->view->btemplate_resetpwd = $this->_getParam('btemplate_resetpwd');
				$this->view->bemailtemplate_resetpwd = $this->_getParam('bemailtemplate_resetpwd');
				$this->view->threshold_webservice  = $zf_filter->threshold_webservice;
				$this->view->min_length_userid  = $zf_filter->min_length_userid;
				$this->view->max_length_userid  = $zf_filter->max_length_userid;
				$this->view->minfpassword  = $zf_filter->minfpassword;
				$this->view->min_length_buserid  = $zf_filter->min_length_buserid;
				$this->view->max_length_buserid  = $zf_filter->max_length_buserid;
				$this->view->minbpassword  = $zf_filter->minbpassword;
		     
			
				$disable_note = $zf_filter->disable_note;
				$note = $zf_filter->note;
			}
			else{
				$disable_note = $setting['disable_note'];
				$note = $setting['note'];
			}
			
			$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;	
			$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
			
			$disable_note_len 	 = 200 - $disable_note_len;		
			$note_len 	 = 200 - $note_len;	

			$this->view->disable_note_len		= $disable_note_len;
			$this->view->note_len		= $note_len;
		}
		else
		{
			$this->view->error = true;
			//$this->fillParam($zf_filter_input);
			$this->view->report_msg = $this->language->_('Changes not found');
		}
		Application_Helper_General::writeLog('STCR','Viewing Repair General Setting');
	}
}