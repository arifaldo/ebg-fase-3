<?php
/**
 * Tempchargestemplatelist model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempchargestemplatelist extends Changemanagement_Model_Tempchanges 
{	

	protected $_moduleId = 'CTL';
	private $mytempid=0;
	/**
     * Approve Changes for changes type: New & edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null)
	{
	    $templatelist = $this->dbObj->select()
						  ->from('TEMP_TEMPLATE_CHARGES_OTHER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		
		if(!count($templatelist)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(templatelist)';
			return false;
        }
		$MyTempid = $templatelist['TEMP_ID'];
		$myTEMPLATE_ID = $templatelist['TEMPLATE_ID'];
		
		$updateArr = array_diff_key($templatelist,array('TEMP_ID'=>'','CHANGES_ID'=>'','TEMPLATE_ID'=>'','CREATED'=>'','CREATEDBY'=>'','ISEDITED'=>''));
		$updateArr['UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY']   = $actor;
		$whereArr = array('TEMPLATE_ID = ?'=> $myTEMPLATE_ID);
				  
		$tplupdate = $this->dbObj->update('M_TEMPLATE_CHARGES_OTHER',$updateArr,$whereArr);
		
		//update record
		if(!(boolean)$tplupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Charges Template List)';
			return false;
		}
		$this->dbObj->delete('M_TEMPLATE_CHARGES_OTHER_DETAIL',$this->dbObj->quoteInto('TEMPLATE_ID = ?',$myTEMPLATE_ID));
		$templatelistdetail = $this->dbObj->select()
						  ->from('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL')
						  ->where('TEMP_ID = ?',$MyTempid)
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach($templatelistdetail as $vtld)
		{
		    $insertArr2 = array_diff_key($vtld,array('TEMP_ID'=>'','CHANGES_ID'=>'','TEMPLATE_DETAIL_ID'=>''));
		    $insertArr2['TEMPLATE_ID']   = $myTEMPLATE_ID;
			$this->dbObj->insert('M_TEMPLATE_CHARGES_OTHER_DETAIL',$insertArr2);
		}
		
				
		$deleteChanges  = $this->deleteEdit();
		
		if(!$deleteChanges)
			return false;
		return true;
	}
	public function approveNew($actor = NULL)
	{
	    $templatelist = $this->dbObj->select()
						  ->from('TEMP_TEMPLATE_CHARGES_OTHER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		
		if(!count($templatelist)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(templatelist)';
			return false;
        }
		//var_dump($templatelist);
		$MyTempid = $templatelist['TEMP_ID']; // we use TEMP_ID and CHANGE_ID to make sure we get the right data
		//--------------------------------INSERT TO TABLE M_TEMPLATE_CHARGES_OTHER----------------------------------------------------------
		$insertArr = array_diff_key($templatelist,array('TEMP_ID'=>'','CHANGES_ID'=>'','TEMPLATE_ID'=>''));
		$insertArr['UPDATED']     = new Zend_Db_Expr('now()');
		$insertArr['UPDATEDBY']   = $actor;
		$userins = $this->dbObj->insert('M_TEMPLATE_CHARGES_OTHER',$insertArr);
		
		//$userins = TRUE;
		if(!(boolean)$userins) 
		{
		    $this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Charges Template List)';
			return false;
		}
		
		$lastReqId = $this->dbObj->fetchOne('select @@identity');
		$myTEMPLATE_ID = $lastReqId;
		$templatelistdetail = $this->dbObj->select()
						  ->from('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL')
						  ->where('TEMP_ID = ?',$MyTempid)
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetchAll(Zend_Db::FETCH_ASSOC);
						
		foreach($templatelistdetail as $vtld)
		{
		    $insertArr2 = array_diff_key($vtld,array('TEMP_ID'=>'','CHANGES_ID'=>'','TEMPLATE_DETAIL_ID'=>''));
		    $insertArr2['TEMPLATE_ID']   = $myTEMPLATE_ID;
			$this->dbObj->insert('M_TEMPLATE_CHARGES_OTHER_DETAIL',$insertArr2);
		}
		
		 
	    $deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
		//will never be called
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
		//will never be called		
	}
	
	/**
     * Approve Changes for changes type: Delete
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDelete($actor = null) {
		

		$templatelist = $this->dbObj->select()
						  ->from('TEMP_TEMPLATE_CHARGES_OTHER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		
		if(!count($templatelist)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(templatelist)';
			return false;
        }
		$MyTempid = $templatelist['TEMP_ID'];
		$myTEMPLATE_ID = $templatelist['TEMPLATE_ID'];
		$del1 = $this->dbObj->delete('M_TEMPLATE_CHARGES_OTHER',$this->dbObj->quoteInto('TEMPLATE_ID = ?',$myTEMPLATE_ID));
		if(!(boolean)$del1) 
		{
		    $this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Charges Template List OTHER)';
			return false;
		}
		
		$del2 = $this->dbObj->delete('M_TEMPLATE_CHARGES_OTHER_DETAIL',$this->dbObj->quoteInto('TEMPLATE_ID = ?',$myTEMPLATE_ID));
		if(!(boolean)$del2) 
		{
		    $this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Charges Template List OTHER DETAIL)';
			return false;
		}
		
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
      
		//delete from TEMP_APP_BOUNDARY
		//delete from TEMP_APP_BOUNDARY
		$appboundarydelete = $this->dbObj->delete('TEMP_TEMPLATE_CHARGES_OTHER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		//delete from TEMP_APP_BOUNDARY_GROUP
		$appboundarygroupdelete = $this->dbObj->delete('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_APP_BOUNDARY
		$appboundarydelete = $this->dbObj->delete('TEMP_TEMPLATE_CHARGES_OTHER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		//delete from TEMP_APP_BOUNDARY_GROUP
		$appboundarygroupdelete = $this->dbObj->delete('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
		//will never be called
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
		//will never be called
	}
	
	/**
     * Delete Changes for changes type: Delete
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDelete() {
		//delete from TEMP_APP_BOUNDARY
		$appboundarydelete = $this->dbObj->delete('TEMP_TEMPLATE_CHARGES_OTHER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		
		//delete from TEMP_APP_BOUNDARY_GROUP
		$appboundarygroupdelete = $this->dbObj->delete('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}
}