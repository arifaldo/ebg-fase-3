<?php
class transactionreport_Model_Systemlog
{
	protected $_db;

	protected $_masterglobalstatus;

    // constructor
	public function __construct()
	{
		$config = Zend_Registry::get('config');
		$this->_masterglobalstatus = $config['master']['globalstatus'];
		$this->_beneftype = $config['account']['beneftype'];
		$this->_chargesto = $config['charges']['to'];
		$this->_providertype = $config['provider']['type'];
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

    public function getFPrivilege()
    {
		$data = $this->_db->select()->distinct()
								->from(array('A' => 'M_FPRIVILEGE'),array('FPRIVI_ID', 'FPRIVI_DESC'))
								->order('FPRIVI_DESC ASC')
				 				->query()->fetchAll();
       return $data;
    }
    
	public function getTransactionLog($fParam,$sortBy,$sortDir,$filter)
    {
				
		$select2 = $this->_db->select()->from(
													array('T_INTERFACE_LOG'),
													array(															
															'FUNCTION_NAME',
															'STRING',
															'UID',
															'LOG_DATE',
															'ACTION',
														)
												);

		if($filter == null || $filter == true)
		{
			if(!empty($fParam['datefrom']) && empty($fParam['dateto']))
					$select2->where("DATE(LOG_DATE) >= ".$this->_db->quote($fParam['datefrom']));

			if(empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(LOG_DATE) <= ".$this->_db->quote($fParam['dateto']));

			if(!empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(LOG_DATE) BETWEEN ".$this->_db->quote($fParam['datefrom'])." and ".$this->_db->quote($fParam['dateto']));
		}

		if($filter == TRUE)
		{
			if($fParam['function'])
			{
				$select2->where("FUNCTION_NAME LIKE ".$this->_db->quote($fParam['function']));
			}

			if($fParam['string'])
			{
			   $select2->where("STRING LIKE ".$this->_db->quote('%'.$fParam['string'].'%'));
			}

			if($fParam['uid'])
			{
			   $select2->where("UID LIKE ".$this->_db->quote('%'.$fParam['uid'].'%'));
			}
		}

		$select2->order($sortBy.' '.$sortDir);
		//echo $select2;
		return $this->_db->fetchAll($select2);
    }
    
	public function getEmailLog($fParam,$sortBy,$sortDir,$filter)
    {
														
		$select2 = $this->_db->select()->from(
													array('T_EMAIL_LOG'),
													array(															
															'EMAIL_RECEIVER',															
															'EMAIL_CONTENT',
															'EMAIL_DATE',
															'EMAIL_STATUS',																													
														)
												);

		if($filter == null || $filter == true)
		{
			if(!empty($fParam['datefrom']) && empty($fParam['dateto']))
					$select2->where("DATE(EMAIL_DATE) >= ".$this->_db->quote($fParam['datefrom']));

			if(empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(EMAIL_DATE) <= ".$this->_db->quote($fParam['dateto']));

			if(!empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(EMAIL_DATE) BETWEEN ".$this->_db->quote($fParam['datefrom'])." and ".$this->_db->quote($fParam['dateto']));
		}

		if($filter == TRUE)
		{
			/*if($fParam['function'])
			{
				$select2->where("FUNCTION_NAME LIKE ".$this->_db->quote($fParam['function']));
			}*/

			if($fParam['emailreceiver'])
			{
			   $select2->where("EMAIL_RECEIVER LIKE ".$this->_db->quote('%'.$fParam['emailreceiver'].'%'));
			}
		}

		$select2->order($sortBy.' '.$sortDir);
		//echo $select2;
		return $this->_db->fetchAll($select2);
    }
}