<?php


require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';

class releasepayment_ReleasependingpaymentController extends Application_Main
{
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$curdate = (date("j M Y"));
		$this->view->curdate = $curdate;
		$single = 0;
		$multi = 0;
		$date = (date("Y-m-d"));
		
		$release = $this->_getParam('release');
	
		$select = $this->_db->select()->distinct()
			->from(array('A' => 'T_PSLIP'))
			->where("PS_STATUS = '7'")
			// ->where("PS_CATEGORY = 'SINGLE PAYMENT'")
			// ->where("PS_CATEGORY = 'OPEN TRANSFER'")
			// ->where("PS_CATEGORY = 'SWEEP PAYMENT'")
			->where("PS_CATEGORY = 'SINGLE PAYMENT' OR PS_CATEGORY = 'OPEN TRANSFER' OR PS_CATEGORY = 'SWEEP PAYMENT'")
			->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
			->query()->fetchAll();
			//Zend_Debug::dump($select);die;

			// echo $select;die();
		foreach($select as $a)
		{
			$single++;
		}
		//Zend_Debug::dump($single);die;
		$this->view->single=$single;
		
		$select2 = $this->_db->select()->distinct()
			->from(array('A' => 'T_PSLIP'))
			->where("PS_STATUS = '7'")
			// ->where("PS_CATEGORY = 'MULTI PAYMENT'")
			// ->where("PS_CATEGORY = 'SWEEP PAYMENT'")
			->where("PS_CATEGORY = 'MULTI PAYMENT' OR PS_CATEGORY = 'OPEN TRANSFER' OR PS_CATEGORY = 'SWEEP PAYMENT'")
			->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
			-> query() ->fetchAll();
			
		foreach($select2 as $b)
		{
			$multi++;
		}
		
		$this->view->multi=$multi;
		
		if($release && ($multi > 0 || $single > 0))
		{
			Application_Helper_General::writeLog('RLSP','Release Pending Payment');
			$qrelease = $this->_db->select()
						->from(array('A' => 'T_PSLIP'),array('A.PS_NUMBER','A.CUST_ID','A.PS_CATEGORY','A.PS_TXCOUNT'))
						->where("A.PS_STATUS = '7'")
						->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
						->where("PS_CATEGORY != 'BULK PAYMENT'")
						->query()->fetchAll();

			$payslip = 0;
			$traslip = 0;
			$paysuccess = 0;
			$payfailed = 0;
			$invalidpayslip = 0;
			$trasuccess = 0;
			$trafailed = 0;
			$failedattempt = 0;
			
			foreach($qrelease as $releaselist)
			{
				$payslip++;
				$traslip += $releaselist['PS_TXCOUNT'];

				$cekpayment = $this->_db->select()
										->from(array('A' => 'T_PSLIP'),array())
										->join(array('B' => 'T_TRANSACTION'), 'A.PS_NUMBER = B.PS_NUMBER',array('PS_NUMBER','B.TRANSACTION_ID'))
										->where("PS_STATUS = '7'")
										->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
										->where("B.PS_NUMBER = '".($releaselist['PS_NUMBER']."'"))
										->where("A.PS_CATEGORY != 'BULK PAYMENT'")
										->query()->fetchAll();
				if(!$cekpayment)
				{
					$invalidpayslip++;
				}
				else
				{
					$Payment = new Payment($releaselist['PS_NUMBER'], $releaselist['CUST_ID'], 'BACKEND');
					$resultRelease = $Payment->releasePayment();
					
//					if ($resultRelease['status'] == '00')
//					{
//						$paysuccess++;
//						$trasuccess = $trasuccess + $releaselist['PS_TXCOUNT'];
//					}
//					if ($resultRelease['status'] != '00' && $resultRelease['status'] != 'XT')
//					{
//						$payfailed++;
//						$trafailed = $trafailed + $releaselist['PS_TXCOUNT'];
//					}
					
					
					
					$cektrans = $this->_db->select()
										->from(array('t' => 'T_TRANSACTION'),array( "trasuccess"   => "sum(if(t.TRA_STATUS = 3, 1, 0))", 
																					"trafailed"    => "sum(if(t.TRA_STATUS = 4, 1, 0))",
																					))
										->where("t.PS_NUMBER = '".($releaselist['PS_NUMBER']."'"));
					$cektrans = $this->_db->fetchRow($cektrans);
				
					
					if ($resultRelease['status'] == '00')
					{
						$paysuccess++;
						$trasuccess = $trasuccess + $cektrans['trasuccess'];
						$trafailed  = $trafailed  + $cektrans['trafailed'];
					}
					if ($resultRelease['status'] != '00' && $resultRelease['status'] != 'XT')
					{
						$payfailed++;
						$trasuccess = $trasuccess + $cektrans['trasuccess'];
						$trafailed  = $trafailed  + $cektrans['trafailed'];
					}
					
					if ($resultRelease['status'] == 'XT')
					{
						$failedattempt++;
					}
				}
			}
			
			$this->view->success = 'success';
			$this->view->payslip = $payslip;
			$this->view->traslip = $traslip;
			$this->view->paysuccess = $paysuccess;
			$this->view->payfailed = $payfailed;
			$this->view->invalidpayslip = $invalidpayslip;
			$this->view->trasuccess = $trasuccess;
			$this->view->trafailed = $trafailed;
			$this->view->failedattempt = $failedattempt;
			
			$single = 0;
			$multi = 0;
			
			$select = $this->_db->select()->distinct()
			->from(array('A' => 'T_PSLIP'))
			->where("PS_STATUS = '7'")
			->where("PS_CATEGORY = 'SINGLE PAYMENT'")
			->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
			->query()->fetchAll();
			
			foreach($select as $a)
			{
				$single++;
			}
			
			$this->view->single=$single;
			
			$select2 = $this->_db->select()->distinct()
				->from(array('A' => 'T_PSLIP'))
				->where("PS_STATUS = '7'")
				->where("PS_CATEGORY = 'MULTI PAYMENT'")
				->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
				-> query() ->fetchAll();
				
			foreach($select2 as $b)
			{
				$multi++;
			}
			
			$this->view->multi=$multi;
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('RLLS','Viewing Release Pending Payment');
		}
	}
}
