<?php


require_once 'Zend/Controller/Action.php';


class biller_IndexController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		
		$model 			 = new biller_Model_Biller();
// 		$billerCharges = new setbillercharges_Model_Setbillercharges();
		$providerArr 	 = $model->getServiceProvider();
    	$serviceTypeArr 	 = $model->getServiceType();
    	
		$options = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));
		unset($options[3]);
		array_unshift($options,'---'.$this->language->_('Any Value').'---');
		foreach($options as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		// print_r($optpayStatusRaw);die;
		//$this->view->options = $options;
		$this->view->options = $optpayStatusRaw;
		$this->view->var = $providerArr;
		$this->view->serviceTypeArr = $serviceTypeArr;
		
			
		
		$fields = array	(
							// 'biller_code'  			=> array	(
							// 										'field' => 'PROVIDER_CODE',
							// 										'label' => $this->language->_('Biller Code'),
							// 										'sortable' => true
							// 									),
							'biller_name'  			=> array	(
																	'field' => 'BILLER',
																	'label' => $this->language->_('Biller Name'),
																	'sortable' => true
																),
							'service_type'  			=> array	(
																	'field' => 'SERVICE_TYPE',
																	'label' => $this->language->_('Service Category'),
																	'sortable' => true
																),
							'provider_type'  			=> array	(
																	'field' => 'PROVIDER_TYPE',
																	'label' => $this->language->_('Service Type'),
																	'sortable' => true
																),
							'status'  		=> array	(
																	'field' => 'PROVIDER_STATUS',
																	'label' => 'Status',
																	'sortable' => true
																),
							'suggest_date'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTED',
																	'label' => $this->language->_('Last Suggestion'),
																	'sortable' => true
																),
							'suggestor'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTEDBY',
																	'label' => $this->language->_('Suggester'),
																	'sortable' => true
																),
							'suggest_date'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTED',
																	'label' => $this->language->_('Last Suggestion'),
																	'sortable' => true
																),
							'suggestor'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTEDBY',
																	'label' => $this->language->_('Suggester'),
																	'sortable' => true
																),
							'approval_date'  		=> array	(
																	'field' => 'PROVIDER_UPDATED',
																	'label' => $this->language->_('Last Approval'),
																	'sortable' => true
																),
							'approver'  		=> array	(
																	'field' => 'PROVIDER_UPDATEDBY',
																	'label' => $this->language->_('Last Approver'),
																	'sortable' => true
																),
						);

		$filterlist = array("BILLER_CODE","BILLER_NAME","BILLER_STATUS","SUGESTBY","APPROVEBY","SUGEST_DATE","APPROVE_DATE","SERVICE_CATEGORY");
		
		$this->view->filterlist = $filterlist;
						
		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'BILLER_CODE'    	=> array('StripTags','StringTrim'),
	                       'BILLER_NAME'  	=> array('StripTags','StringTrim','StringToUpper'),
	                       'BILLER_STATUS'  	=> array('StripTags','StringTrim'),
						   'SUGESTBY'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'APPROVEBY'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'SUGEST_DATE'  	=> array('StripTags','StringTrim'),
						   'SUGEST_DATE_END'  	=> array('StripTags','StringTrim'),
						   'APPROVE_DATE'  		=> array('StripTags','StringTrim'),
						   'APPROVE_DATE_END'  		=> array('StripTags','StringTrim'),
		                   'SERVICE_CATEGORY'    	=> array('StripTags','StringTrim'),
	    
	                      );
	                      
	    $validator = array('filter' 		=> array(),
	                       'BILLER_CODE'    	=> array(),
	                       'BILLER_NAME'  	=> array(),
	                       'BILLER_STATUS'  	=> array(),
						   'SUGESTBY'  	=> array(),
						   'APPROVEBY'  	=> array(),
						   'SUGEST_DATE'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'SUGEST_DATE_END'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'APPROVE_DATE'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'APPROVE_DATE_END'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	 	 	  			   'SERVICE_CATEGORY'    	=> array(),
	                      );


	    $dataParam = array("BILLER_CODE","BILLER_NAME","BILLER_STATUS","SUGESTBY","APPROVEBY","SERVICE_CATEGORY");
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "SUGEST_DATE" || $value == "APPROVE_DATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		// print_r($dataParamValue);die;
			if(!empty($this->_request->getParam('sugestdate'))){
				$createarr = $this->_request->getParam('sugestdate');
					$dataParamValue['SUGEST_DATE'] = $createarr[0];
					$dataParamValue['SUGEST_DATE_END'] = $createarr[1];
			}
			if(!empty($this->_request->getParam('approvedate'))){
				$updatearr = $this->_request->getParam('approvedate');
					$dataParamValue['APPROVE_DATE'] = $updatearr[0];
					$dataParamValue['APPROVE_DATE_END'] = $updatearr[1];
			}
	                      
		$fParam = array();	              
		// print_r($dataParamValue);die;        
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    // $filter = $zf_filter->getEscaped('filter');
	    $filter 		= $this->_getParam('filter');
	    $billerCode = $fParam['billerCode'] = html_entity_decode($zf_filter->getEscaped('BILLER_CODE'));
		$billerName = $fParam['billerName'] = html_entity_decode($zf_filter->getEscaped('BILLER_NAME'));
		$billerStatus = $fParam['billerStatus'] = html_entity_decode($zf_filter->getEscaped('BILLER_STATUS'));
		$suggestor 	= $fParam['suggestor'] 	= html_entity_decode($zf_filter->getEscaped('SUGESTBY'));
		$approver 	= $fParam['approver'] 	= html_entity_decode($zf_filter->getEscaped('APPROVEBY'));
		$datefrom 	= $fParam['datefrom'] 	= html_entity_decode($zf_filter->getEscaped('SUGEST_DATE'));
		$udatefrom 	= $fParam['udatefrom'] 	= html_entity_decode($zf_filter->getEscaped('APPROVE_DATE'));
		$dateto 	= $fParam['dateto'] 	= html_entity_decode($zf_filter->getEscaped('SUGEST_DATE_END'));
		$udateto 	= $fParam['udateto'] 	= html_entity_decode($zf_filter->getEscaped('APPROVE_DATE_END'));
		$serviceCategory = $fParam['serviceCategory'] = html_entity_decode($zf_filter->getEscaped('SERVICE_CATEGORY'));
		
		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');
		
		$page = (Zend_Validate::is($page,'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		if($filter == $this->language->_('Set Filter'))
		{
			$this->view->fDateTo    = $dateto;
			$this->view->ufDateTo    = $udateto;
			$this->view->fDateFrom  = $datefrom;
			$this->view->ufDateFrom  = $udatefrom;
			 
			if(!empty($datefrom))
			{
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$fParam['datefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($dateto))
			{
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
			}
			if(!empty($udatefrom))
			{
				$FormatDate = new Zend_Date($udatefrom, $this->_dateDisplayFormat);
				$fParam['udatefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($udateto))
			{
				$FormatDate = new Zend_Date($udateto, $this->_dateDisplayFormat);
				$fParam['udateto']    = $FormatDate->toString($this->_dateDBFormat);
			}

		    if($billerCode)
		    {
	       		$this->view->billerCode = $billerCode;
		    }
		    if($serviceCategory)
		    {
	       		$this->view->serviceCategory = $serviceCategory;
		    }
		    
		    
			if($billerName)
			{
	       		$this->view->billerName = $billerName;
			}
			
			if($billerStatus)
			{
	       		$this->view->billerStatus = $billerStatus;
			}
			
			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
		    }
			if($approver)
		    {
	       		$this->view->approver = $approver;
		    }
		}

		$data = $model->getProvider($fParam,$sortBy,$sortDir,$filter);
		
    	$this->paging($data);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	if($csv || $pdf)
    	{
    		$arr = $data;
    		
    		foreach($arr as $key=>$value)
			{
				unset($arr[$key]["PROVIDER_ID"]);
				$arr[$key]["PROVIDER_SUGGESTED"] = Application_Helper_General::convertDate($value["PROVIDER_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
    		
	    	if($csv)
			{
				Application_Helper_General::writeLog('VBIL','Download CSV Setup Biller');
				$this->_helper->download->csv(array('Biller Code','Biller Name','Service Catagory','Service Type','Status','Latest Suggestion','Suggester','Latest Approval','Latest Approver'),$arr,null,'List Biller');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('VBIL','Download PDF Setup Biller');
				$this->_helper->download->pdf(array('Biller Code','Biller Name','Service Catagory','Service Type','Status','Latest Suggestion','Suggester','Latest Approval','Latest Approver'),$arr,null,'List Biller');
			}
    	}else if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'List Biller', 'data_header' => $fields));
			}
    	else
    	{
    		Application_Helper_General::writeLog('VBIL','View Setup Biller');
    	}

    	unset($dataParamValue['SUGEST_DATE_END']);
    	unset($dataParamValue['APPROVE_DATE_END']);
    	if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}