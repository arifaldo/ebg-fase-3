<?php


require_once 'Zend/Controller/Action.php';


class companycharges_RepairController extends Application_Main
{
	public function indexAction()
	{
		$changeid = $this->_getParam('changes_id');
		//var_dump($changeid);
		$this->view->changes_id = $changeid;

		$cekCashCollateral = $this->_db->select()
			->from("T_GLOBAL_CHANGES")
			->where("DISPLAY_TABLENAME = ? ", "Cash Collateral")
			->where("CHANGES_STATUS = ?", "RR")
			->query()->fetchAll();

		if (count($cekCashCollateral) > 0) {
			$this->view->cashCollateral = 1;

			$getData = $this->_db->select()
				->from("TEMP_CHARGES_OTHER")
				->where("CHANGES_ID = ?", $changeid)
				->query()->fetchAll();

			$this->view->data = $getData[0];
		}

		//$select2 = $this->_db->select()->distinct()
		//			        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
		//$select2 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
		//$result2 = $this->_db->fetchRow($select2);

		//$custid = $result2['KEY_FIELD'];

		$custid = 'GLOBAL';
		$this->view->custid = $custid;

		$selectccy = $this->_db->select()
			->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));

		$listccy = $selectccy->query()->fetchAll();
		$paramccy = array();
		foreach ($listccy as $key => $value) {
			$paramccy[$value['CCY_ID']] = $value['CCY_ID'];
		}
		// echo '<pre>';
		// print_r($paramccy);
		$this->view->dataccy = $paramccy;
		$this->view->listccy = $listccy;

		$sqlQuery1 = "
			SELECT 
				
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT 
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM TEMP_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?
				AND CHANGES_ID = ?) B 
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
		//ORDER BY CHARGE_ID DESC";

		$transfee = $this->_db->fetchAll($sqlQuery1, array('3', $custid, $changeid));

		$fullfee = $this->_db->fetchAll($sqlQuery1, array('4', $custid, $changeid));

		$prfee = $this->_db->fetchAll($sqlQuery1, array('5', $custid, $changeid));
		//	echo '<pre>';
		//	var_dump($custid);
		//	echo $transfee;
		//var_dump($transfee);die;
		foreach ($prfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'prccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keymin = 'prmin' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keymin = $value['CHARGE_PROV_MIN_AMT'];

			$keymax = 'prmax' . $value['CCY_ID'];
			print_r($keyamt);
			echo "<br>";
			$this->view->$keymax = $value['CHARGE_PROV_MAX_AMT'];

			$keypct = 'prpct' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keypct = $value['CHARGE_PCT'];
			// }
		}
		//die;

		foreach ($fullfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'faccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keyamt = 'faamt' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}
		// echo "<pre>";
		// print_r($transfee);
		foreach ($transfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'tfccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keyamt = 'tfamt' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}

		$this->view->transfee = $transfee;


		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		//var_dump($submit);die;
		if ($submit) {

			if (count($cekCashCollateral) > 0) {

				$this->updateGlobalChanges($changeid, "Cash Collateral");

				$getParams = $this->_request->getParams();

				$updateArr = [
					"CHARGES_PCT" => Application_Helper_General::convertDisplayMoney($getParams["bank_provision_fee"]),
					"CHARGES_ADM" => Application_Helper_General::convertDisplayMoney($getParams["bank_administration_fee"]),
					"CHARGES_STAMP" => Application_Helper_General::convertDisplayMoney($getParams["bank_stamp_fee"])
				];

				$this->_db->update("TEMP_CHARGES_OTHER", $updateArr, [
					"CHANGES_ID = ?" => $changeid
				]);

				$this->setbackURL('/changemanagement');
				return $this->_redirect('/popup/submited/index');
			}

			$params = $this->getRequest()->getParams();
			$ccy = $params['ccy'];

			$this->_db->beginTransaction();
			Application_Helper_General::writeLog('CHCR', 'Repairing Administration fee company charges (' . $changeid . ')');

			$info = 'Global Charges';
			$info2 = $this->language->_('Set Charges Monthly per Account');
			try {
				$where = array('CHANGES_ID = ?' => $changeid);
				$this->_db->delete('TEMP_CHARGES_REMITTANCE', $where);

				foreach ($params['ccy'] as $val) {
					$type = 'charges_type' . $ccy;
					$type2 = $params[$type];

					$ccytf = 'ccytf' . $val;
					$ccytf2 = $params[$ccytf];

					$tfamount = 'tfamount' . $val;
					$tfamount2 = $params[$tfamount];

					$ccyfa = 'ccyfa' . $val;
					$ccyfa2 = $params[$ccyfa];

					$faamount = 'faamount' . $val;
					$faamount2 = $params[$faamount];

					$ccypf = 'ccypf' . $val;
					$ccypf2 = $params[$ccypf];

					$min = 'min' . $val;
					$min2 = $params[$min];

					$max = 'max' . $val;
					$max2 = $params[$max];

					$pcy = 'pcy' . $val;
					$pcy2 = $params[$pcy];




					if ($ccytf2 != '-') {
						if ($tfamount2 == null) {
							$tfamount2 = '0.00';
						}
						$charge_amt =	Application_Helper_General::convertDisplayMoney($tfamount2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';



						$params_insert_3 = array(
							'CHANGES_ID' => $changeid,
							'CUST_ID' => $custid,
							'CHARGE_TYPE' => '3',
							'CHARGE_CCY' => $val,
							'CHARGE_AMOUNT_CCY' => $ccytf2,
							'CHARGE_AMT' => $charge_amt

						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE', $params_insert_3);
					}

					if ($ccyfa2 != '-') {
						if ($faamount2 == null) {
							$faamount2 = '0.00';
						}
						$charge_amt =	Application_Helper_General::convertDisplayMoney($faamount2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';

						// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Full Amount Fee',$cust_id,$cust_name);

						$params_insert_4 = array(
							'CHANGES_ID' => $changeid,
							'CUST_ID' => $custid,
							'CHARGE_TYPE' => '4',
							'CHARGE_CCY' => $val,
							'CHARGE_AMOUNT_CCY' => $ccytf2,
							'CHARGE_AMT' => $charge_amt

						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE', $params_insert_4);
					}

					if ($ccypf2 != '-') {

						$charge_min =	Application_Helper_General::convertDisplayMoney($min2);
						$charge_max =	Application_Helper_General::convertDisplayMoney($max2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';

						if ($pcy2 == null) {
							$pcy2 = '0.00';
						}

						// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Provision Fee',$cust_id,$cust_name);

						$params_insert_5 = array(
							'CHANGES_ID' => $changeid,
							'CUST_ID' => $custid,
							'CHARGE_TYPE' => '5',
							'CHARGE_CCY' => $val,
							'CHARGE_AMOUNT_CCY' => $ccypf2,
							'CHARGE_PROV_MIN_AMT' => $charge_min,
							'CHARGE_PROV_MAX_AMT' => $charge_max,
							'CHARGE_PCT' => $pcy2,
						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE', $params_insert_5);
					}
				}



				//UPDATE STATUS GLOBAL CHANGES		
				$this->updateGlobalChanges($changeid, $info);
				Application_Helper_General::writeLog('STCR', 'Submiting Repair Charges Fee');
				$this->_db->commit();
				$this->view->success = true;
				$msg = "Settings Change Request Saved";
				$this->view->report_msg = $msg;
				$this->setbackURL('/changemanagement');
				$this->_redirect('/popup/submited/index');
			} catch (Exception $e) {
				var_dump($e);
				die;
				$this->_db->rollBack();
			}
		} else {
			Application_Helper_General::writeLog('CHCR', 'View Repair Administration fee company charges page (' . $changeid . ')');
		}
	}
}
