<?php
require_once 'Zend/Controller/Action.php';

class userlist_ViewController extends customer_Model_Customer
{

    public function indexAction() 
    {
  	   $user_id = strtoupper($this->_getParam('user_id'));
  	   $suspend = $this->_getParam('suspend');
  	   $unsuspend = $this->_getParam('unsuspend');
  	   $delete = $this->_getParam('delete');
   
       if($user_id)
       {
		$select	= $this->_db->select()
								->distinct()
								->from	(
											array('MB'=>'M_BUSER'),
											array(
													'BUSER_ID'=>'MB.BUSER_ID',
													'BUSER_STATUS'=>'MB.BUSER_STATUS',
													'BUSER_NAME'=>'MB.BUSER_NAME',
													'BUSER_EMAIL'=>'MB.BUSER_EMAIL',
													'BGROUP_ID'=>'MB.BGROUP_ID',
													'BUSER_ISLOCKED'=>'MB.BUSER_ISLOCKED',
													'BUSER_LASTLOGIN'=>'MB.BUSER_LASTLOGIN',
													'BGROUP_DESC'=>'MG.BGROUP_DESC'
												)
										)
								->joinLeft	(
												array('MG' => 'M_BGROUP'),
												'MB.BGROUP_ID = MG.BGROUP_ID',
												array()
											)
								->where('MB.BUSER_ID =?',$user_id);
						
		$resultdata = $this->_db->fetchAll($select);  
		//$arrgroup =Application_Helper_Array::listArray($group,'BGROUP_ID','BGROUP_DESC');  
		//$this->view->arrGroup = $arrgroup;
		
		
		//$resultdata = $this->getCustomer($user_id);
         
		
		$select2 = $this->_db->select()
					        ->from(array('TEMP_BUSER'));
		$select2 -> where("USER_ID LIKE ".$this->_db->quote($user_id));
		$result2 = $select2->query()->FetchAll();
		//Zend_Debug::dump($result2);die;
          if($resultdata[0]['BUSER_ID'])
          {
				$this->view->user_id      = strtoupper($resultdata[0]['BUSER_ID']);
       			$this->view->group     = $resultdata[0]['BGROUP_DESC'];
       			$this->view->status    = $resultdata[0]['BUSER_STATUS'];
       			$this->view->name    = $resultdata[0]['BUSER_NAME'];
       			$this->view->email = $resultdata[0]['BUSER_EMAIL'];
       			$this->view->description    = "-";
       			$this->view->failed     = "-";
       			$this->view->userislogoin = $resultdata[0]['BUSER_LASTLOGIN'];
       			$this->view->userislocked  = $resultdata[0]['BUSER_ISLOCKED'];
       			$this->view->createdate    = "-";
       			$this->view->creator      = "-";
       			$this->view->last_suggestion      = "-";
       			$this->view->last_suggestor    = "-";
       			$this->view->last_approval  = "-";
       			$this->view->last_approver  = "-";
					
      		 
           }
           else $user_id = null; 
       }// End if cust_id == true
    
        if(!$user_id)
        {
           $error_remark = $this->getErrorRemark('22','Customer ID');
           //insert log
	       try 
	       {
	         $this->_db->beginTransaction();
	         $this->backendLog($this->_actionID['view'],$this->_moduleID['cust'],null,null,$error_remark);
	         $this->_db->commit();
	       }
           catch(Exception $e)
           {
 	          $this->_db->rollBack();
	          Application_Log_GeneralLog::technicalLog($e);
	       }
	    
           $this->_helper->getHelper('FlashMessenger')->addMessage('F');
           $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
           $this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));	
         }
    
        $this->view->user_id = $user_id;
        $this->view->status_type = $this->_masteruserStatus;
        $this->view->modulename = $this->_request->getModuleName();
    
        //insert log
        try
        {
	       $this->_db->beginTransaction();
	     
           $this->_db->commit();
	    }
	    catch(Exception $e) 
	    {
	       $this->_db->rollBack();
  	       Application_Log_GeneralLog::technicalLog($e);
	    }
	    
	    
/////code below by: Ade///////////////////////////////////////////////////////////////////////////////////    
		if(!$result2)
		{	
	   		if($suspend && $resultdata[0]['BUSER_STATUS'] == '1')
	    	{
	    		$this->_db->beginTransaction();
				try
				{
					$info = "Backend User";
					$change_id = $this->suggestionWaitingApproval('Backend User',$info,$this->_changeType['code']['suspend'],null,'M_BUSER','TEMP_BUSER',$resultdata[0]['BUSER_ID'],$resultdata[0]['BUSER_NAME']);
					$data = array(
								'CHANGES_ID' 		=> $change_id, 
								'BUSER_ID'			=> $resultdata[0]['BUSER_ID'], 
								'BUSER_NAME' 		=> $resultdata[0]['BUSER_NAME'], 
								'BGROUP_ID' 		=> $resultdata[0]['BGROUP_ID'],								
								'BUSER_DESC' 		=> $resultdata[0]['BGROUP_DESC'],								
								'BUSER_EMAIL'		=> $resultdata[0]['BUSER_EMAIL'],
								'BUSER_STATUS'		=> '2'
									);				
					$this->_db->insert('TEMP_BUSER',$data);
					$temp = 1;
				}
				catch(Exception $e)
				{
					$temp = 0;
					$this->_db->rollBack();					
				}
			
				if($temp == 1)
				{
					$this->_db->commit();
					$this->_redirect('/notification/success/index');
				}
				if($temp == 0)
				{
					$this->_db->rollBack();
				}
	    	}
	    
   			if($unsuspend && $resultdata[0]['BUSER_STATUS'] == '2')
	   		{
	    		$this->_db->beginTransaction();
				try
				{
					$info = "Backend User";
					$change_id = $this->suggestionWaitingApproval('Backend User',$info,$this->_changeType['code']['suspend'],null,'M_BUSER','TEMP_BUSER',$resultdata[0]['BUSER_ID'],$resultdata[0]['BUSER_NAME']);
					$data = array(
								'CHANGES_ID' 		=> $change_id, 
								'BUSER_ID'			=> $resultdata[0]['BUSER_ID'], 
								'BUSER_NAME' 		=> $resultdata[0]['BUSER_NAME'], 
								'BGROUP_ID' 		=> $resultdata[0]['BGROUP_ID'],								
								'BUSER_DESC' 		=> $resultdata[0]['BGROUP_DESC'],								
								'BUSER_EMAIL'		=> $resultdata[0]['BUSER_EMAIL'],
								'BUSER_STATUS'		=> '1'
								);				
					$this->_db->insert('TEMP_BUSER',$data);
					$temp = 1;
				}
				catch(Exception $e)
				{
					$temp = 0;
					$this->_db->rollBack();					
				}
			
				if($temp == 1)
				{
					$this->_db->commit();
					$this->_redirect('/notification/success/index');
				}
				if($temp == 0)
				{
					$this->_db->rollBack();
				}
	    	}
	    	
			if($delete)
	   		{
	    		$this->_db->beginTransaction();
				try
				{
					$info = "Backend User";
					$change_id = $this->suggestionWaitingApproval('Backend User',$info,$this->_changeType['code']['suspend'],null,'M_BUSER','TEMP_BUSER',$resultdata[0]['BUSER_ID'],$resultdata[0]['BUSER_NAME']);
					$data = array(
								'CHANGES_ID' 		=> $change_id, 
								'BUSER_ID'			=> $resultdata[0]['BUSER_ID'], 
								'BUSER_NAME' 		=> $resultdata[0]['BUSER_NAME'], 
								'BGROUP_ID' 		=> $resultdata[0]['BGROUP_ID'],								
								'BUSER_DESC' 		=> $resultdata[0]['BGROUP_DESC'],								
								'BUSER_EMAIL'		=> $resultdata[0]['BUSER_EMAIL'],
								'BUSER_STATUS'		=> '3'
								);				
					$this->_db->insert('TEMP_BUSER',$data);
					$temp = 1;
				}
				catch(Exception $e)
				{
					$temp = 0;
					$this->_db->rollBack();					
				}
			
				if($temp == 1)
				{
					$this->_db->commit();
					$this->_redirect('/notification/success/index');
				}
				if($temp == 0)
				{
					$this->_db->rollBack();
				}
	    	}
		}
		
		if($result2)
		{
			$change = "disabled";
			$this->view->change = $change;
		}
	}	
}




