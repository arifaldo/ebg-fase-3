<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';

class Localremittancechar_IndexController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();

		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

		$buser_id = $this->_userIdLogin;
		$this->view->buser_id 	= $buser_id;

		$settings =  new Settings();

		$minLengthPassword = $settings->getSetting('minbpassword');
		$maxLengthPassword = $settings->getSetting('maxbpassword');
		
		
		
		$this->view->minlengthpass = $minLengthPassword;
		$this->view->maxlengthpass = $maxLengthPassword;
		$model = new remittancechar_Model_Remittancechar();
		
		$this->view->tempcek = $model->cekTemp();
// 		$this->view->paginator = $model->getRemittanceType($charge_type);
		$this->view->chargesfee = $model->getRemittanceTypeIndex('6');
		
		
		
		Application_Helper_General::writeLog('CHOP','Change My Password');
	}

}

