<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';

class directdebit_DeleteController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
		$password = $sessionNamespace->token; 
		$this->view->token = $sessionNamespace->token;
		$AESMYSQL = new Crypt_AESMYSQL();
		$NUM_ID      = urldecode($this->_getParam('numid'));
		$DIRECT_ID = $AESMYSQL->decrypt($NUM_ID, $password);

		
		if($DIRECT_ID){

			$select = $this->_db->select()
					 ->from(array('C' => 'T_DIRECTDEBIT'),array('*','company'	=> new Zend_Db_Expr("CONCAT(COMP_NAME , ' (' , COMP_ID , ')  ' )")))
					 ->joinleft(array('X'=>'M_DOMESTIC_BANK_TABLE'), 'X.SWIFT_CODE = C.DEBIT_BANK',array('BANK_NAME'))
					 ->where('DIRECT_ID = ?', $DIRECT_ID)
					 ->query()->FetchAll();

			// echo "<pre>";
			// var_dump($DIRECT_ID);
			// var_dump($select);

            $selectcomp = $this->_db->select()
                        ->from(array('A' => 'M_CUSTOMER'),array('*'))
                        ->where('A.CUST_ID = ?',$select[0]['COMP_ID'])
                        ->query()->fetchAll();

            $this->view->compinfo = $selectcomp[0];

			
			$this->view->COMP_NAME = $select[0]['COMP_NAME'];
			$this->view->CLIENT_REFF = $select[0]['CLIENT_REFF'];
			$this->view->CLIENT_NAME = $select[0]['CLIENT_NAME'];
			$this->view->DEBIT_ACCT = $select[0]['DEBIT_ACCT'];
			$this->view->ACCT_NAME = $select[0]['ACCT_NAME'];
			$this->view->DEBIT_BANK = $select[0]['DEBIT_BANK'];
			$this->view->COMP_ID = $select[0]['COMP_ID'];

            if($this->_request->isPost())
            {   

                $adapter = new Zend_File_Transfer_Adapter_Http();
                $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'pdf'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv and *.pdf'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));
                $adapter->setOptions(array('ignoreNoFile'=>true));
                // echo "test";die;
                if ($adapter->isValid ())
                {
                
                    $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                    if(empty($adapter->getFileName ())){
                        $newFileName = '';
                    }else{
                        $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
                    // var_dump($sourceFileName);
                    }
                    //var_dump($adapter->getFileName ());die;
                    $adapter->addFilter ( 'Rename',$newFileName  );

                    if ($adapter->receive ())
                    {
                        
                        $select = $this->_db->select()
                                ->from(array('A' => 'T_DIRECTDEBIT'),array('*'))                      
                                ->where('A.DIRECT_ID = ?',$DIRECT_ID);
                        $datapool                   = $this->_db->fetchAll($select);
                        $insertArr = $datapool['0'];
                        
                        $change_id = $this->suggestionWaitingApproval('Notional Pooling',$info,$this->_changeType['code']['delete'],null,'T_DIRECTDEBIT','TEMP_DIRECTDEBIT',$selectcomp[0]['CUST_ID'],$selectcomp[0]['CUST_NAME'],$selectcomp[0]['CUST_ID'],$selectcomp[0]['CUST_NAME']);

                        $insertArr['CHANGES_ID'] = $change_id;
                        $insertArr['DEBIT_NOTES'] =  $this->_getParam("deleteNotes");
                        $insertArr['DIR_STATUS'] =  '2';
                        $insertArr['DOC_ID'] =  $this->_getParam("doc_id");
                        // $insertArr['N_UD_FILE'] =  $newFileName;
                        $insertArr['DIR_SUGESTED'] = new Zend_Db_Expr('now()');
                        $insertArr['DIR_SUGESTEDBY'] = $this->_userIdLogin;
                        unset($insertArr['DIRECT_ID']);
                        unset($insertArr['DIR_APPROVED']);
                        unset($insertArr['DIR_APPROVEDBY']);
                        unset($insertArr['DIR_TYPE']);
                        
                        
                        try{
                            $insert = $this->_db->insert('TEMP_DIRECTDEBIT',$insertArr);

                            $where = array('CLIENT_REFF = ?' => $insertArr['CLIENT_REFF']);
                            $result = $this->_db->update('T_DIRECTDEBIT',array('DIR_STATUS' => '0'),$where);

                        }catch(Exception $e){
                            var_dump($e); die;
                            $this->_db->rollBack();
                        }
                       

                        // suggest status
                        $rowUpdated = $this->_db->update(
                            'T_GLOBAL_CHANGES',
                            array(
                                'CHANGES_INFORMATION' => 'Delete Auto Debit',
                                'LASTUPDATED' => new Zend_Db_Expr("now()"),
                            ),
                            $this->_db->quoteInto('CHANGES_ID = ?', $change_id)
                        );

                        $rowHistory = $this->_db->insert(
                            'T_PSLIP_HISTORY',
                            array(
                                'PS_REASON' => 'Suggest Delete',
                                'USER_LOGIN' => $this->_userIdLogin,
                                'PS_NUMBER' => $insertArr['CLIENT_REFF'],
                                'DATE_TIME' => new Zend_Db_Expr("now()"),
                            )
                        );
                        $this->setbackURL('/'.$this->_request->getModuleName().'/index');
                        $this->_redirect('/notification/submited/index');
                    }
                
                }
            }
        }
		
    }
}