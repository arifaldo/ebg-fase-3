<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class loan_ApproveController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
		
		//select m_user - begin
		$userData = $CustomerUser->getUser($this->_userIdLogin);
		
		$fields = array(
						'LOAN_TYPE'      	=> array('field' => 'LOAN_TYPE',
													'label' => $this->language->_('Loan Type'),
													'sortable' => true),
						'DATE_TIME'      	=> array('field' => 'DATE_TIME',
													'label' => $this->language->_('Date Time Request'),
													'sortable' => true),
						'LOAN_PURPOSE'  	=> array('field' => 'LOAN_PURPOSE',
													'label' => $this->language->_('Loan Purpose'),
													'sortable' => true),
						'LOAN_AMOUNT'  		=> array('field' => 'LOAN_AMOUNT',
													'label' => $this->language->_('Loan Amount'),
													'sortable' => true),
						'LOAN_INSTALLMENT'  	=> array('field' => 'LOAN_AMOUNT',
													'label' => $this->language->_('Loan Installment'),
													'sortable' => true),
						'LOAN_MONTH'  			=> array('field' => 'LOAN_MONTH',
													'label' => $this->language->_('Loan Month'),
													'sortable' => true),
						'COMPANY_NAME'  			=> array('field' => 'COMPANY_NAME',
													'label' => $this->language->_('Company Name'),
													'sortable' => true),
						'LOAN_SUGGESTEDBY'  			=> array('field' => 'LOAN_SUGGESTEDBY',
													'label' => $this->language->_('Suggested By'),
													'sortable' => true),
						'ACTION'  			=> array('field' => 'ACTION',
													'label' => $this->language->_('Action'),
													'sortable' => true)
				      );

		$filterlist = array('LOAN_TYPE','LOAN_SUGGESTED_DATE','LOAN_STATUS');

		$this->view->filterlist = $filterlist;
				      
		$page    = $this->_getParam('page');
		
		// $sortBy  = $this->_getParam('sortby','B.PS_UPDATED');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('PS_UPDATED');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		// $sortDir = $this->_getParam('sortdir','desc');
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$select2 = $this->_db->select()
					        ->from(array('L' => 'T_LOAN'))
					        ->where('L.LOAN_STATUS = ?', 'WA')
					        ->join(array('C' => 'M_CUSTOMER'), 'L.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'));
		
		$filterArr = array(
           'LOAN_TYPE'		=> array('StripTags','StringTrim','StringToUpper'),	                                              
		   'LOAN_STATUS'			=> array('StripTags')
          );
	                      
		$dataParam = array("LOAN_TYPE","LOAN_AMOUNT","LOAN_STATUS");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}
		}

		if(!empty($this->_request->getParam('efdate'))){
			$efdatearr = $this->_request->getParam('efdate');
			$dataParamValue['LOAN_SUGGESTED_DATE'] = $efdatearr[0];
			$dataParamValue['LOAN_SUGGESTED_DATE_END'] = $efdatearr[1];
		}
	                      
		$validator = array(                  
		   'LOAN_TYPE'	=> array(),	 
		   'LOAN_STATUS'			=> array(),						   
		   'LOAN_SUGGESTED_DATE'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		   'LOAN_SUGGESTED_DATE_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	      );
	   
	     $zf_filter 	= new Zend_Filter_Input($filterArr,$validator,$dataParamValue);

	     if ($zf_filter->isValid()) {
	     	$filter 		= TRUE;
	     }
	    
	     
	     $loantype 	= html_entity_decode($zf_filter->getEscaped('LOAN_TYPE'));	 
		 $loanstatus	= html_entity_decode($zf_filter->getEscaped('LOAN_STATUS'));		 
		 $datefrom 		= html_entity_decode($zf_filter->getEscaped('LOAN_SUGGESTED_DATE'));
		 $dateto 		= html_entity_decode($zf_filter->getEscaped('LOAN_SUGGESTED_DATE_END'));
		 
		 
		if($filter == null)
		{	
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}
		
		if($filter_clear == '1'){
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';
			$datefrom = '';
			$dateto = '';
			
		}
		
		if($filter == null || $filter ==TRUE)
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;	 
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
	            }
		
		if(!empty($datefrom) && empty($dateto))
	            $select2->where("L.LOAN_SUGGESTED >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("L.LOAN_SUGGESTED <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("L.LOAN_SUGGESTED between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}

		
		if($filter == TRUE)
	    {		
	    	
	    	if($loantype!=null)
			{ 				
				$this->view->loantype = $loantype;
				$select2->where("L.LOAN_TYPE LIKE ".$this->_db->quote($loantype));
			}
	    	
			if($loanstatus != null)
			{
			   $this->view->loanstatus = $loanstatus;
		       $select2->where("L.LOAN_STATUS LIKE ".$this->_db->quote($loanstatus));
			}
		}

		$select2->order($sortBy.' '.$sortDir);
		
		if($this->_request->isPost() )
		{
			
			$loan_id = $this->_getParam('change_id');
			
			if(count($loan_id) > 0)
			{			
			
				if($this->_getParam('submit')=='Approve')
				{	
				          
					foreach($loan_id as $key=>$row){						
						try 
						{
							$this->_db->beginTransaction();		
		
							$dataapprove =  array(					
													'LOAN_STATUS' => 'AP', 
													'LOAN_APPROVED' => date('Y-m-d H:i:s'), 
													'LOAN_APPROVEDBY' => $this->_userIdLogin,
													'LOAN_UPDATED' => date('Y-m-d H:i:s'), 
													'LOAN_UPDATEDBY' => $this->_userIdLogin
												);							
							
							$where = array('LOAN_ID = ?' => $row);
							$result = $this->_db->update('T_LOAN',$dataapprove,$where);
							Application_Helper_General::writeLog('APLO','Approve Loan '.$row);	
							
							$this->_db->commit();							
							
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
							
						}
				    }     
				    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');					
					
					
				}elseif($this->_getParam('submit')=='Reject')
				{
					
					foreach($loan_id as $key=>$row){						
						try 
						{
							$this->_db->beginTransaction();		
		
							$dataapprove =  array(					
													'LOAN_STATUS' => 'RJ',
													'LOAN_UPDATED' => date('Y-m-d H:i:s'), 
													'LOAN_UPDATEDBY' => $this->_userIdLogin
												);							
							
							$where = array('LOAN_ID = ?' => $row);
							$result = $this->_db->update('T_LOAN',$dataapprove,$where);
							Application_Helper_General::writeLog('RJLO','Reject Loan '.$row);	
							
							$this->_db->commit();							
							
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
							
						}
				    }     
				    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');		
					
				}
			
			}else{
				$this->view->error = TRUE;
				$this->view->report_msg = $this->language->_('Error: Please select a loan');
			}
			
		}
		
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$arr = $this->_db->fetchAll($select2);
			foreach ($arr as $key => $value)
			{
				unset($arr[$key]['CUST_ID']);
				//echo $key;
				$arr[$key]["PS_REQUESTED"] = Application_Helper_General::convertDate($value["PS_REQUESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

				$arr[$key]["SOURCE_ACCOUNT"] = $arr[$key]["SOURCE_ACCOUNT"]." [".$arr[$key]["SOURCE_ACCOUNT_CCY"]."] / ".$arr[$key]["SOURCE_ACCOUNT_NAME"];
				unset($arr[$key]["SOURCE_ACCOUNT_CCY"]);
				unset($arr[$key]["SOURCE_ACCOUNT_NAME"]);

				$arrProductType = array('1'=>'Cheque','2'=>'Bilyet Giro');
				$arr[$key]["PRODUCT_TYPE"] = $arrProductType[$arr[$key]["PRODUCT_TYPE"]];

				unset($arr[$key]['PS_APPROVEBY']);

				$requestby = $arr[$key]['PS_REQUESTBY'];
				unset($arr[$key]['PS_REQUESTBY']);

				$arr[$key]["ADMIN_FEE"] = "IDR ".Application_Helper_General::displayMoney($arr[$key]["ADMIN_FEE"]);
				unset($arr[$key]["SUGGEST_STATUS"]);

				$arr[$key]["PS_REQUESTBY"] = $requestby;

				
				// $arr[$key]["SOURCE_ACCOUNT_NAME"]= $value["SOURCE_ACCOUNT_NAME"];
				// $arr[$key]["BENEFICIARY_ACCOUNT_NAME"]= $value["BENEFICIARY_ACCOUNT_NAME"];
				// $arr[$key]["BENEFICIARY_ACCOUNT_CCY"]= $value["BENEFICIARY_ACCOUNT_CCY"].' / '.$value["TRA_AMOUNT"];
				// $arr[$key]["PS_TYPE"]= $this->language->_($value["PS_TYPE"]).' ('.$value["TRANSFER_TYPE"].')';
				
				// $amount = Application_Helper_General::displayMoney( $value['TRA_AMOUNT'] );
				
				// $updated = Application_Helper_General::convertDate($value["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				// $efdate = Application_Helper_General::convertDate($value["PS_EFDATE"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
				// $arr[$key]["AMOUNT_EQ"]= $amounteq;
				
				// $status = $this->language->_($value["TRA_STATUS"]);
				
				// $pstype = $this->language->_($value["PS_TYPE"]);
				
				// $arr[$key]["PS_UPDATED"] = $updated;
				// $arr[$key]["PS_EFDATE"] = $efdate;
				// $arr[$key]["TRA_STATUS"] = $status;
				// $arr[$key]["PS_TYPE"] = $pstype;
				
			}
// 			echo "<pre>";
// 			print_r($arr);die;
			$header = Application_Helper_Array::simpleArray($fields, 'label');
			//Zend_Debug::dump($arr);die;
			if($csv)
			{
				Application_Helper_General::writeLog('DTRX','Download CSV Transaction Report');
				//Zend_Debug::dump($arr);die;
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->csv($header,$arr,null,'Executed Transaction');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('DTRX','Download PDF Transaction Report');
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->pdf($header,$arr,null,'Executed Transaction');
			}
			if($this->_request->getParam('print') == 1){
				
	                $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields));
	        }
		}
		else
		{
			Application_Helper_General::writeLog('DTRX','View Transaction Report');
		}
// 		echo '<pre>';
// 		print_r($select2->query());die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($select2);

		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}
}