<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class user_ViewController extends user_Model_User{


  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));
//    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
  // $this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$cust_id.'/user_id/'.$user_id);

    $error_remark   = null;

    if($user_id && $cust_id)
    {
        $select = $this->_db->SELECT()
                             ->FROM(array('u'=>'M_USER'))
               ->JOINLEFT(array('c'=>'M_CUSTOMER'),'c.CUST_ID = u.CUST_ID',array('CUST_NAME','CUST_CONTACT'))
                             //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
                             ->WHERE('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->WHERE('UPPER(u.USER_ID)='.$this->_db->quote((string)$user_id));
        $resultdata = $this->_db->fetchRow($select);

      if(is_array($resultdata))
      {

        // if($resultdata['USER_RRESET'] == 1 || $resultdata['USER_RPWD_ISBYBO'] == 1){
        //   $this->view->showwarning = true;
        // }
        $this->view->cust_name  = $resultdata['CUST_NAME'];
        $this->view->user_name  = $resultdata['USER_FULLNAME'];
        $this->view->user_email = $resultdata['USER_EMAIL'];
        $this->view->user_phone = $resultdata['USER_PHONE'];
        $this->view->token_id   = $resultdata['TOKEN_ID'];
        $this->view->user_ext   = $resultdata['USER_EXT'];
        $this->view->user_isnew   = $resultdata['USER_ISNEW'];
    $this->view->user_pass   = $resultdata['USER_PASSWORD'];

        //$this->view->user_group = $resultdata['FGROUP_NAME'];
        $this->view->user_status = $resultdata['USER_STATUS'];
        $this->view->user_islogin = $resultdata['USER_ISLOGIN'];
        $this->view->user_failed_attempt =  $resultdata['USER_FAILEDATTEMPT'];
        $this->view->user_is_login  = $resultdata['USER_ISLOGIN'];
        $this->view->user_is_locked = $resultdata['USER_ISLOCKED'];
        $this->view->lock_reason = $resultdata['USER_LOCKREASON'];
        $this->view->user_is_webservices = $resultdata['USER_ISWEBSERVICES'];
		
		
		$debit  = $this->_db->select()
                                  ->from('M_USER_DEBIT')
                                  ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
								  ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                                  ->query()->fetchAll();  		
		$debitarr = '';
		if(!empty($debit)){
			
			foreach($debit as $kval => $dval){
				if(empty($debit[$kval+1]['USER_DEBITNUMBER'])){
					$debitarr .= $dval['USER_DEBITNUMBER'];
				}else{
					$debitarr .= $dval['USER_DEBITNUMBER'].';';
				}
			}
		}		
		$this->view->debitarr = $debitarr;
		
		//$this->view->user_debitnumber = $resultdata['USER_ISWEBSERVICES'];

        $this->view->user_lastlogin   = $resultdata['USER_LASTLOGIN'];
        $this->view->user_created     = $resultdata['USER_CREATED'];
        $this->view->user_createdby   = $resultdata['USER_CREATEDBY'];
        $this->view->user_suggested   = $resultdata['USER_SUGGESTED'];
    if(empty($resultdata['USER_SUGGESTEDBY'])){
      $this->view->user_suggestedby = $resultdata['CUST_CONTACT'];
    }else{
      $this->view->user_suggestedby = $resultdata['USER_SUGGESTEDBY'];
    }
        
        $this->view->user_updated     = $resultdata['USER_UPDATED'];
        $this->view->user_updatedby   = $resultdata['USER_UPDATEDBY'];


        if($resultdata['USER_ISEMAIL']) $userIsEmail = $this->language->_('Yes');
      else $userIsEmail = $this->language->_('No');
        $this->view->userIsEmail = $userIsEmail;

    $this->view->statusreset=1;
    //Permintaan SASYA VERISSA BOENTORO 26-04-2013 FSD Forgot Password - CP2 Changing.pdf fixed by SUHANDI
      if(!$resultdata['USER_ISNEW'])
      {
          if($resultdata['USER_RRESET'] ==1 || $resultdata['USER_RPWD_ISBYBO']==1)
      {
        $this->view->statusreset=0;
      }
      }

      if($resultdata['USER_RRESET'] ==1 || $resultdata['USER_RPWD_ISBYBO']==1)
      {
        $this->view->statusreset=0;
      }
        // else
        // {
            // if($resultdata['USER_RRESET'] ==1)
      // {
        // $this->view->statusreset=0;
      // }
        // }

        $setting = new Settings();
        $system_type = $setting->getSetting('system_type');
      //$getprivilege = $this->getprivilege($system_type);

        //privilege
        $this->view->priviAll = Application_Helper_Array::listArray($this->getPrivilege($system_type),'FPRIVI_ID','FPRIVI_DESC');

        $fuserId    = $cust_id . $user_id;
        $priviList  = $this->_db->select()
                                ->from(array('u'=>'M_FPRIVI_USER'))
                                ->where('UPPER(u.FUSER_ID)='.$this->_db->quote((string)$fuserId))
                                ->query()->fetchAll();

        $priviListArr = Application_Helper_Array::simpleArray($priviList,'FPRIVI_ID');
        $this->view->priviList = $priviListArr;


        //cek apakah list template sesuai list privilege yang dipilih
        $priviTemplate = array();
        foreach($this->getPriviTemplate() as $row)
        {
           $priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
        }


        $flag_template = 0;
        $user_role     = '';
        $template_id   = '';
        if(count($priviTemplate) > 0)
        {
           foreach($priviTemplate as $key => $row)
           {
                $count_row = count($row);
                $count_priviListArr = count($priviListArr);
                if($count_row == $count_priviListArr)
                {
                    $arr_diff = array_diff_key(array_flip($priviListArr),$row);
                    if(count($arr_diff) == 0)
                    {
                       $template_id = $key;
                       $flag_template = 1;
                       break;
                    }
                }
            }
        }

        if($flag_template == 0) $user_role = 'Custom';
        else if($flag_template == 1)
        {
           $select = $this->_db->select()
                               ->from('M_FTEMPLATE')
                               ->where('UPPER(FTEMPLATE_ID)='.$this->_db->quote($template_id))
                               ->query()->fetch();
           $user_role = $select['FTEMPLATE_DESC'];
        }

        $this->view->user_role = $user_role;
       // Zend_Debug::dump($priviTemplate); echo "<br/><br/>";
        // Zend_Debug::dump($priviListArr);

        //END privilege

        $select = $this->_db->select()
                               ->from('TEMP_USER',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote($cust_id))
                               ->where('UPPER(USER_ID)='.$this->_db->quote($user_id));

        $result = $this->_db->fetchOne($select);
        if($result){ $temp = 0; }else{ $temp = 1; }
        $this->view->user_temp = $temp;

        //ambil user limit
        $this->view->userLimitData = $this->_db->select()
          ->from('M_USER_LIMIT')
      ->where('CUST_ID = ?', $cust_id)
      ->where('USER_ID = ?', $user_id)
      ->query()
      ->fetchAll();

      }else{ $user_id = null; }


      //--------------------message Success dari action forceLogout---------------------------------
           $resetMsgSucces = $this->_getParam('resetMsgSucces');
           if(is_null($resetMsgSucces) || $resetMsgSucces = '')
           {
               $this->view->resetMsgSucces = '';
           }
           else
           {
               $this->view->resetMsgSucces = 'User is now logged out';
           }
      //------------------END message Success dari action forceLogout-------------------------------
    }

    if(!$user_id)
    {
      // $error_remark = 'User ID is empty';
      //insert log
    try
    {
      $this->_db->beginTransaction();
      $fulldesc = 'CUST_ID:'.$cust_id;
      $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
    $this->_db->commit();
    }
      catch(Exception $e){
    $this->_db->rollBack();
    SGO_Helper_GeneralLog::technicalLog($e);
    }

    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      // $this->_redirect('/user/custlist');

      $encrypted_custid = $AESMYSQL->encrypt($cust_id);
      $enccustid = urlencode($encrypted_custid);

      $this->_redirect('/customer/view/index/cust_id/'.$enccustid);
    }

    $this->view->cust_id = $cust_id;
    $this->view->change_type = $this->_changeType;
    $this->view->status_type = $this->_masterglobalstatus;
    $this->view->login_type = $this->_masterhasStatus;
    $this->view->user_id = $user_id;
    $this->view->modulename = $this->_request->getModuleName();

    //insert log
  try
  {
    $this->_db->beginTransaction();
//    if(!$this->_request->isPost()){
//    Application_Helper_General::writeLog('CSLS','View Frontend User Detail [' .$user_id . ']');
//    }
    $this->_db->commit();
  }
    catch(Exception $e){
    $this->_db->rollBack();
  }


    if(!$this->_getParam('resetMsgSucces')){
    Application_Helper_General::writeLog('CSLS','View Frontend User Detail [' .$user_id . ']');
  }
  }
  
  
  public function resendAction()
  {
     $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;
	$rand = $sessionNamespace->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));
  
  
    $users = $this->_db->select()
              ->from('M_USER')
              ->where('USER_ID = ?',$user_id)
              ->where('CUST_ID = ?',$cust_id)
              ->query()
              ->fetchAll();
//var_dump($users);die;
    if(is_array($users) && count($users))
    {
      foreach($users as $key=>$val)
      {
        
        $updateArr['USER_DATEPASS']     = new Zend_Db_Expr('now() + INTERVAL 1 DAY');
        $updateArr['USER_UPDATED'] = new Zend_Db_Expr('now()');
        //$updateArr['CUST_UPDATED']     = new Zend_Db_Expr('now()');
        //$updateArr['CUST_UPDATEDBY']   = $actor;

        $whereArr = array('CUST_ID = ?'=>(string)$val['CUST_ID'],'USER_ID = ?'=>(string)$val['USER_ID']);
        


        try{
          // echo '<pre>';
          // print_r($insertArr);die;
          $customerupdate = $this->_db->update('M_USER',$updateArr,$whereArr);
        //$userins = $this->_db->insert('M_USER',$insertArr);
        // var_dump($userins); die;
        }catch(exception $e)
        {
          print_r($e);die;
        }
        // die('here');
        
        $user_id = $val['USER_ID'];
        



        $set = new Settings();
        $templateEmailMasterBankAddress = $set->getSetting('master_bank_address');
        $templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
        $templateEmailMasterBankAppUrl = $set->getSetting('master_bank_app_url');
        $templateEmailMasterBankCity = $set->getSetting('master_bank_city');
        $templateEmailMasterBankCountry = $set->getSetting('master_bank_country');
        $templateEmailMasterBankEmail = $set->getSetting('master_bank_email');
        $templateEmailMasterBankEmail1 = $set->getSetting('master_bank_email1');
        $templateEmailMasterBankFax = $set->getSetting('master_bank_fax');
        $templateEmailMasterBankName = $set->getSetting('master_bank_name');
        $templateEmailMasterBankProvince = $set->getSetting('master_bank_province');
        $templateEmailMasterBankTelp = $set->getSetting('master_bank_telp');
        $templateEmailMasterBankWebsite = $set->getSetting('master_bank_website');
        $templateEmailMasterBankWapp = $set->getSetting('master_bank_wapp');
        // $templateEmailMasterBankAppname = $set->getSetting('master_bank_app_name');
        $url_fo = $set->getSetting('url_fo');


        $FEmailResetPass = $set->getSetting('femailtemplate_newuser');

        // print_r($isi);die;
        // $actual_link = $_SERVER['SERVER_NAME'];
        // $key = md5 ('permataNet92');
        // $encrypt = new Crypt_AES ();
        // $user = ( $encrypt->encrypt ( $user_id ) );
        // $cust = ( $encrypt->encrypt ( $isi['CUST_ID'] ) );

      //  $newPassword = '<a href="'.$actual_link.':9996/pass/index?safetycheck=&cust_id='.urlencode($cust).'&user_id='.urlencode($user).'" >Confirm</a>';
        $actual_link = $_SERVER['SERVER_NAME'];
        $key = md5 ('permataNet92');
        $encrypt = new Crypt_AES ();
        // $user = ( $encrypt->encrypt ( $user_id ) );
        // $user = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $user_id, MCRYPT_MODE_ECB);
        // $cust = ( $encrypt->encrypt ( $isi['CUST_ID'] ) );
        $cust = ( $this->sslEnc(  $val['CUST_ID'] ) );
        $user = ( $this->sslEnc(  $val['USER_ID'] ) );
        $date = date('Y-m-d h:i:s', strtotime("+1 days"));

        $newPassword = $url_fo.'/pass/index?safetycheck=&cust_id='.urlencode($cust).'&user_id='.urlencode($user);
        // $template = str_ireplace('[[exp_date]]',Application_Helper_General::convertDate($date,'d-MMM-yyyy HH:mm:ss','yyyy-MM-dd HH:mm:ss'),$template);
        // print_r($newPassword);die;
        $datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
        $data = array( '[[user_fullname]]' => $val['USER_FULLNAME'],
                    '[[comp_accid]]' => $val['CUST_ID'],
                    '[[user_login]]' => $val['USER_ID'],
                    '[[user_email]]' => $val['USER_EMAIL'],
                    '[[confirm_link]]' => $newPassword,
                    '[[master_bank_name]]' => $templateEmailMasterBankName,
                    '[[master_bank_telp]]' => $templateEmailMasterBankTelp,
                    '[[master_bank_email]]' => $templateEmailMasterBankEmail,
                    '[[master_bank_wapp]]' => $templateEmailMasterBankWapp,
                    '[[master_bank_app_name]]' => $templateEmailMasterBankAppName,
                    '[[exp_date]]' => $datenow

                    );

        $FEmailResetPass = strtr($FEmailResetPass,$data);
        $status = Application_Helper_Email::sendEmail($val['USER_EMAIL'],'New User Account Information',$FEmailResetPass);
        
		$encrypted_custid = $AESMYSQL->encrypt($val['CUST_ID'], $rand);
		$encrypted_userid = $AESMYSQL->encrypt($val['USER_ID'], $rand);
		$enccustid = urlencode($encrypted_custid);
		$encuserid = urlencode($encrypted_userid);
		// $this->_redirect('/user/view/index/cust_id/'.$encrypted_custid.'/user_id/'.$encuserid);  
		// $this->setbackURL('/customer');  
    //     $this->_redirect('/notification/success/index');
    return $status;

      }
    }
  
    
  }
  
  public function sslPrm()
  {
  return array("6a1f325be4c0492063e83a8cb2cb9ae7","IV (optional)","aes-128-cbc");
  }

  public function sslEnc($msg)
  {
    list ($pass, $iv, $method)=$this->sslPrm();
     return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
  }
}
