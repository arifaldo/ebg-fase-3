<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempcoa extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'COA';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_USER
		$charges_acct = $this->dbObj->select()
						  	->from('TEMP_SETTING')
						  	->where('CHANGES_ID = ?',$this->_changeId)
							->where('SETTING_ID = ?','charges_acct_IDR')
						  	->query()
						  	->fetch(Zend_Db::FETCH_OBJ);
		$setting_val	= $charges_acct->SETTING_VALUE;
		if(!isset($setting_val)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(chargest_acct)';
			return false;
        }
		else{
			$updateArr = array('SETTING_VALUE'=>$setting_val);
			$whereArr = "SETTING_ID = 'charges_acct_IDR'";
			$userupdate = $this->dbObj->update('M_SETTING',$updateArr,$whereArr);			
		}
		//update record
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(COA Setting)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) {
		//will never be called
	}
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_SETTING',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() {
	}
}