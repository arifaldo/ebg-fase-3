<?php

Class transactiondailylimit_Model_Transactiondailylimitsetting
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getSetting()
    {
		$data = $this->_db->select()
					->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					->where('MODULE_ID = ?' , 'TRX')
					->query()->FetchAll();
		
       return $data;
    }

	
    public function getTempSetting($changes_id)
    {
		$data = $this->_db->select()
					->from(array('A' => 'TEMP_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					->where('MODULE_ID = ?' , 'TRX')
					->where('CHANGES_ID = ?', $changes_id)
					->query()->FetchAll();
		
       return $data;
    }
  
    public function cekTemp()
    {
		$data = $this->_db->select()
					->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID'))
					->WHERE ('MODULE_ID = ?','TRX')
					->query()->FetchAll();
		
       return $data;
    }
  
    public function insertTemp($data)
    {
		$this->_db->insert('TEMP_SETTING',$data);	
    }
  
    public function deleteTemp($changes_id)
    {
		$where = array('CHANGES_ID = ?' => $changes_id);
		$this->_db->delete('TEMP_SETTING',$where);
    }
  
    public function cekChanges($changesid)
    {
		$cek = $this->_db->fetchone($this->_db->select()
									->from(('T_GLOBAL_CHANGES'),array('MODULE'))
									->where('MODULE = ?' , 'transactiondailylimit')
									->where('CHANGES_ID= ?' , $changesid)
									->where("CHANGES_STATUS IN ('WA','RR')"));
		
		return $cek;
    }
  
    public function getChangesData($changesid)
    {
		$data = $this->_db->FetchRow("SELECT CHANGES_STATUS , CREATED , CREATED_BY  FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = ".$this->_db->quote($changesid));
		
		return $data;
    }

}