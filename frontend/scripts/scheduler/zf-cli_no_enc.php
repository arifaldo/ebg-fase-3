<?php
date_default_timezone_set('Asia/Jakarta');
set_time_limit(0);
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? 'development' : 'development'));

define('UPLOAD_PATH',APPLICATION_PATH . '/../data/uploads');    
// Ensure library/ is on include_path

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
)));

/** Zend_Application */
require_once 'Zend/Application.php';
require_once 'Zend/Config/Ini.php';
//require_once 'Zend/Debug.php';

//parse config from ini files in configs folder 
//and merge them as one Zend_Config object to be passed to Zend_Application constructor
//NOTE : merging ini files is done here (before Zend_Application->dispatch() is called)
//in order to have the default plugin resources loaded automatically

//Create a Zend_Config_Ini object as the main config
$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini',
							  APPLICATION_ENV,
							  array('allowModifications'=>true));
							  
//uses php5 SPL DirectoryIterator class 
$dir = new DirectoryIterator(APPLICATION_PATH.'/configs');
foreach($dir as $file) {
	if(!$file->isDot() && $file->isFile() && $file->getFileName() != 'application.ini') {
		// WARNING : All loaded ini files must have the same sections from main ini file
		$cfg = new Zend_Config_Ini(APPLICATION_PATH.'/configs/'.$file->getFileName(),
								   APPLICATION_ENV, 
								   array('allowModifications'=>true));
		//merge into main config
		$config->merge($cfg);
	}
}
// Create application, bootstrap, and run
$application = new Zend_Application( APPLICATION_ENV, $config);
$application->bootstrap();
//$application->gbootstrap();
$bootstrap = $application->getBootstrap();
Zend_Controller_Front::getInstance()->setParam('bootstrap', $bootstrap);
//define error remark
$data = new Application_Cache_ErrorRemark();
$data->preDispatch(); 

