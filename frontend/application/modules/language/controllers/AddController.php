<?php
class language_AddController extends Application_Main {
	public function indexAction(){
		
	
		$model = new language_Model_Index();
		
		
		$sql = $model->getLang();
		$arrLang = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				$arrLang += array($val['ID'] => $val['DESC']);
			}
		}
		$this->view->arrLang = $arrLang;
		
		$sql = $model->getUi();
		//$arrUi = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				//$arrUi += array($val['ID'] => $val['DESC']);
				$arrUi = array($val['ID'] => $val['DESC']);
			}
		}
		$this->view->arrUi = $arrUi;
		
		$params 	= $this->_request->getParams();
		
		foreach($params as $key => $val){
			$this->view->{$key} = $val;
		}
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		
		$options = array('allowEmpty' => FALSE);
		
		$validators = array(
			'ui'			=> 	array(
				'Digits',
				new Zend_Validate_InArray(array_values(array_flip($arrUi))),
			),
			'oriPhrase'		=> array(),
			'traPhrase'		=> array(),
			'traPhrase'		=> array(),
			'lang'			=> 	array(
				'Digits',
				new Zend_Validate_InArray(array_values(array_flip($arrLang))),
			),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		// print_r($params);die;
		if ($filter->isValid() && isset($_POST['submit'])){
		
			$this->_db->beginTransaction();
			try{
				$mt = $this->_db->select()
					->from('M_TRANSLATE')
					->where('TEXT = ?', $filter->getEscaped('oriPhrase'))
					->where('M_TRANSLATE_UI_ID = 1')
					->limit(1)
				;
				
				$mt = $this->_db->fetchRow($mt);
				
				$lastUid = 0;
				if(isset($mt['ID'])){
					$lastUid = (int) $mt['ID'];
				}else{
					$tableMt = 'M_TRANSLATE';
					$bindMt = array(
							'M_TRANSLATE_UI_ID' => '1',
							'TEXT' => $filter->getEscaped('oriPhrase'),
							'CREATED' => date('Y-m-d H:i:s'),
							'CREATEDBY' => $this->_userIdLogin,
							'UPDATEDBY' => $this->_userIdLogin,
					);
					print_r($bindMt);
					try {
						$this->_db->insert($tableMt, $bindMt);	
					} catch (Exception $e) {
						print_r($e);die;
					}
					
					
					$lastUid = $this->_db->lastInsertId($tableMt);
				}
				
				$tableMtd = 'M_TRANSLATE_DETAIL';
				$bindMtd = array(
					'M_TRANSLATE_ID' => $lastUid,
					'M_TRANSLATE_LANG_ID' => $filter->getEscaped('lang'),
					'TEXT' => $filter->getEscaped('traPhrase'),
					'CREATED' => date('Y-m-d H:i:s'),
					'CREATEDBY' => $this->_userIdLogin,
					'UPDATEDBY' => $this->_userIdLogin,
					//'WEB' => 'Frontend',
				);
				print_r($bindMtd);
				try {
					$this->_db->insert($tableMtd, $bindMtd);	
				} catch (Exception $e) {
					print_r($e);die;
				}
				
				
				foreach($params as $key => $val){
					$this->view->{$key} = NULL;
				}
				
				$this->_db->commit();
				// die;
				$this->_redirect('language/index');
			}catch(Exception $e){
				$this->_db->rollBack();
				$error = $e->getMessage();
			}
		}else{
			$error = $filter->getMessages();
		}
		
		Zend_Debug::dump($error);
	}	
}
