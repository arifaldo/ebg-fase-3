<?php

require_once 'Zend/Controller/Action.php';

class Companycharges_TransferfeeController extends Application_Main
{

    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');



		

		$cust_id = 'GLOBAL';
		$this->view->custid = $cust_id;
		
		$cekpbglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_WITHIN'),array('*'));
		$cekpbglobal ->where("A.CUST_ID = 'GLOBAL'");
		$cektemppbglobal = $this->_db->fetchAll($cekpbglobal);
		 
		$cekothglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
		$cekothglobal ->where("A.CUST_ID = 'GLOBAL'");
		$cektempotherglobal = $this->_db->fetchAll($cekothglobal);
	if(!empty($cektemppbglobal) || !empty($cektempotherglobal) ){
			$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";
			$this->view->errorpb = $docErr;
			$this->view->changestatuspb = "disabled";
		}
		
 		$cust_name = $this->_custNameLogin;

 			$pstypeArr = array(1 => 'RTGS',2 => 'SKN',8 => 'Domestic Online',0 => 'Inhouse');
		 
			$this->view->pstype = $pstypeArr;
			
			$selectpbglobal = $this->_db->select()
						        ->from(array('A' => 'M_CHARGES_WITHIN'),array('*'));
			$selectpbglobal ->where("A.CUST_ID = 'GLOBAL'");
			$pbglobal = $this->_db->fetchAll($selectpbglobal);
			 
			if(!empty($pbglobal)){
			 $pbamount = $pbglobal['0']['AMOUNT'];
		 }else{
			 $pbamount = 0;
		 }
		 //var_dump($pbamount);
			$this->view->pbglobal = $pbamount;
		
			 
			//$this->view->pbglobal = $pbglobal;
			
			$selectdomglobal = $this->_db->select()
						        ->from(array('A' => 'M_CHARGES_OTHER'),array('*'));
			$selectdomglobal ->where("A.CUST_ID = 'GLOBAL'");
			$domglobal = $this->_db->fetchAll($selectdomglobal);
			 
			$this->view->domglobal = $domglobal;

			


		// var_dump($cust_name);exit();
		// $select = $this->_db->select()
		// 			->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
		// 			->where('MODULE_ID = ?' , 'GNS')
		// 			->query()->FetchAll();
		// $setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		// foreach($setting as $key=>$value)
		// {
		// 	if(!empty($key))
		// 		$this->view->$key = $value;
		// }

			//FILTER
			{
			$filters = array(
							// 'isenabled' => array('StringTrim','StripTags'),
							// 'disable_note' => array('StringTrim','StripTags'),
							// 'note' => array('StringTrim','StripTags'),

						
            			    
							);
			}

			//VALIDATE
			{
			$validators = array(
									// 'isenabled' => array(	'NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),

									// 'disable_note' => array(	'NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),
									// 'note' => array('NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),
								
									
															);
			}

				$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				$arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');

				

		if($this->_request->isPost()){

			$params = $this->getRequest()->getParams();
			$ccy = $params['ccy'];

			if($zf_filter->isValid() )
			{
				
				
				$this->_db->beginTransaction();
					//die;
					try
					{
				    //Zend_Debug::dump($resultunion); die;
					// $info = 'Charges';
					// $info2 = $this->language->_('Set Charges Monthly per Account');
					
					// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_MONTHLY','TEMP_CHARGES_MONTHLY',$custid,$custname,$custid,null,'monthlyaccount');

					$info = 'Global Charges';
						$info2 = 'Set Realtime Charges';
						
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_WITHIN','TEMP_CHARGES_WITHIN',$cust_id,'Realtime Local Transfer Charges',$cust_id,null,'realtimecharges');
					// print_r($resultunion);die;
					$resultunion= array('1','2','8');

					$idamt = 'realtime_charges_ihhouse';
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);

					$data= array(
												'CHANGES_ID' 		=> $change_id,
												'CUST_ID' 			=> $cust_id,
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $amt,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> $chargeacct,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);	

					foreach($resultunion as $row)
					{
						
							$idamt = 'charges'.$row;
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
							
								$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> $row,
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $amt,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

							$this->_db->insert('TEMP_CHARGES_OTHER',$data1);									
								//echo $row['ACCT_NO'].' aaa<br />';
								//Zend_Debug::dump($data);
									
					}
					
					
					
					//if($error == 0)
				//	{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/charges/chargesglobal');
						$this->_redirect('/notification/submited/index');
				//	}
				}
				
				catch(Exception $e)
				{
					var_dump($e);die;
					//die;
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
				// $this->setbackURL('/companycharges/monthlyfee/index/custid/GLOBAL');
				//$this->_redirect('/notification/submited/index');
				// die('here1');
			}
			else{
				die('fs');
				$docErr = 'Error in processing form values. Please correct values and re-submit.';
				$this->view->report_msg = $docErr;
				// die('here');
				foreach($zf_filter->getMessages() as $key=>$err){
				$xxx = 'x'.$key;
				// print_r($zf_filter->getMessages());die;
				$this->view->$xxx = $this->displayError($err);
				}
			}
			// var_dump($ccy); exit();

			foreach($setting as $key=>$value)
			{
				if (in_array($key, $arrfck)){
					$this->view->$key =   $this->_getParam($key);
				}
				else{
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
				}
			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		}
		else{
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			Application_Helper_General::writeLog('STLS','View Frontend Global Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 200 - $disable_note_len;
		$note_len 	 = 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;


	}
}
