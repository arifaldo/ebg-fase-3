<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';

class requestbook_CreateController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	protected $_destinationUploadDir = '';
	protected $_listCCYValidate = '';

	public function initController()
	{       
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 	
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;		
	}
	
	public function indexAction()
	{		

		$this->_helper->layout()->setLayout('newlayout');
		
		$Settings = new Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();		
		//Zend_Debug::dump($ccyList);die;
		
		$this->view->userIdLogin  = $this->_userIdLogin;
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;

		// $this->view->custsame 			= false;
		
		
		//added new hard token
		/*$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();

		$challengeCodeSub = substr($challengeCode, 0,2);
		$this->view->challengeCodeReq = $challengeCodeSub;*/
		//validasi token -- end
		
		if(!$this->_request->isPost() && $this->_getParam('isback'))
		{
			$sessionNamespace = new Zend_Session_Namespace('requestbookcreate');
			$content = $sessionNamespace->content;			
			$this->fillParams($content['SUBJECT'],$content['ACCTSRC'],$content['ACCTSRC_NAME'],$content['PRODUCT_TYPE'],$content['NUMBEROF_SHEET'],$content['ADMIN_FEE']);
			//$this->view->ACBENEF_NAME = $content['BENEFICIARY_NAME'];
			unset($_SESSION['requestbookcreate']); 
		}
		
		if($this->_request->isPost() )
		{
			$SUBJECT 		= $this->_getParam('SUBJECT');
			$ACCTSRC 		= $this->_getParam('ACCTSRC');
			$PRODUCT_TYPE 	= $this->_getParam('producttype');			
			$NUMBEROF_SHEET = $this->_getParam('numberofsheet');
			
	//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//	
			/*$ACCTSRC    = $filter->filter(ACCTSRC  , "ACCOUNT_NO");								
			$ACBENEF_ALIAS   = $filter->filter($PRODUCT_TYPE , "ACCOUNT_ALIAS");			
			$ACBENEF_CCY   = $filter->filter($NUMBEROF_SHEET  , "SELECTION");*/
			
			//validasi token -- begin
			 /*$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2')	, "challengeCodeReq2");
			 $challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq')	, "challengeCodeReq");
			
			
			 $responseCodeReq 	= $filter->filter($this->_request->getParam('responseCodeReq')	, "responseCodeReq");
			 //$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);
			 $this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);*/
			 //validasi token -- end

			/* $paramBene = array("TRANSFER_TYPE"     => 'PB',
								"FROM"      		=> 'B',
								"ACBENEF"     		=> $ACBENEF,  
								"ACBENEF_CCY"    	=> $ACBENEF_CCY,   
								"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,  
								"ACBENEF_EMAIL"    	=> $ACBENEF_EMAIL,  
								"_addBeneficiary"   => $this->view->hasPrivilege('BADA'),  
								"_beneLinkage"    	=> $privibenelinkage,  
							   );
												
			 $validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			 $validateACBENEF->setPrefixMessageAccount('', '');

			 $validateACBENEF->check($paramBene);*/		 
			
			//validate field 
			$dataValidate =  array(					
				'source_acct' => $ACCTSRC, 
				'prod_type' => $PRODUCT_TYPE, 
				'numberof_sheet' => $NUMBEROF_SHEET
			);
			
			$resultValidate = $this->validateField($dataValidate);
						 
			//print_r($paramBene);die;
			 if($resultValidate===true){
			 	
			 	$accData = $this->_db->select()
			       ->from('M_CUSTOMER_ACCT')
			       ->where('ACCT_NO = ?', $ACCTSRC)
			       ->where('CUST_ID = ?', (string)$this->_custIdLogin);
			       
			    $accData = $this->_db->fetchRow($accData);
			    
			    $ACCTSRC_NAME 	= $accData['ACCT_NAME'];
			    $ACCTSRC_CCY 	= $accData['CCY_ID'];
			 	
				$content = array(
								'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
								'USER_ID_LOGIN' 			=> $this->_userIdLogin,								
								'SUBJECT' 					=> $SUBJECT,
								'ACCTSRC' 					=> $ACCTSRC,
								'ACCTSRC_NAME' 				=> $ACCTSRC_NAME, //$ACCTSRC_NAME,
								'ACCTSRC_CCY' 				=> $ACCTSRC_CCY,								
								'PRODUCT_TYPE' 				=> $PRODUCT_TYPE,
								'NUMBEROF_SHEET'	 		=> $NUMBEROF_SHEET,
								'_beneLinkage'     			=> $privibenelinkage,
					);
					
					$content['ADMIN_FEE'] = "20000";
					/*$content['paramAccount'] = $responseData;
					$content['payment'] = $payment;*/
					
			 //validasi hard token page 1 -- begin
					/*$message = $Settings->getSetting('token_confirm_message');
					$trans = array("[Response_Code]" => $responseCode);
					$message = strtr($message, $trans);
					
					if($tokenType == '1'){ //sms token
						if(empty($responseCodeReq)){
							$resultToken = FALSE;
							$this->view->error = true;
							$errMessage = 'Error : Response Token cannot be left blank.';
							$this->view->report_msg = $errMessage;
						}
						else
						{
							$token = new Service_Token(NULL,$this->_userIdLogin);
							$token->setMsisdn($usermobilephone);
							$token->setMessage($message);

							$res = $token->confirm();
							$resultToken = $res['ResponseCode'] == '0000';
						}
					}
					elseif($tokenType == '2'){ //hard token
						
						if(empty($responseCodeReq)){
							$resultToken = FALSE;
							$this->view->error = true;
							$errMessage = 'Error : Response Token cannot be left blank.';
							$this->view->report_msg = $errMessage;
						}
						else
						{
							
							$resHard = $HardToken->verifyHardToken($challengeCodeReq2.$challengeCodeReq, $responseCodeReq);
							$resultToken = $resHard['ResponseCode'] == '0000';
							
							//set user lock token gagal
							$CustUser = new CustomerUser($this->_custId, $this->_userIdLogin);							
							if ($resHard['ResponseCode'] != '0000'){
								$tokenFailed = $CustUser->setFailedTokenMustLogout();
								if ($tokenFailed)
									$this->_forward('home');
							}
						}
					}
					elseif($tokenType == '3'){ //mobile token
						$resultToken = TRUE;
					}
					else{$resultToken = TRUE;}*/
					//validasi hard token page 1 -- end	
					
					//if($resultToken == TRUE){	
						//print_r($content);die;
						$sessionNamespace = new Zend_Session_Namespace('requestbookcreate');
						$sessionNamespace->mode = 'Add';
						$sessionNamespace->content = $content;
						$this->_redirect('/requestbook/create/confirm');
					//}
					/*else{
						//echo $resultToken."aaa"; die;
						$this->view->ACBENEF = $ACBENEF;
						$this->view->CURR_CODE = $CURR_CODE;
						$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
						
						if($resultToken == FALSE){
							if($res['ResponseCode'] == 'XT'){
								$errMessage = 'Error : Service Rejected';
								$this->view->resulttoken = $errMessage;
							}
							else{								
								$errMessage = 'Error : Invalid Token';
								$this->view->resulttoken = $errMessage;
							}
						}
					}*/
					
					/*$sessionNamespace = new Zend_Session_Namespace('requestbookcreate');
					$sessionNamespace->mode = 'Add';
					$sessionNamespace->content = $content;
					$this->_redirect('/predefinedbeneficiary/inhouseacct/confirm');*/
				
			}
			else
			{			
				 	
				$this->view->error = true;
				//$this->fillParams($ACBENEF,$ACBENEF_CCY,$ACBENEF_ALIAS,$ACBENEF_EMAIL);
				$docErr = $resultValidate->getMessages();
//				$validateACBENEF->__destruct();
//				unset($validateACBENEF);
				$this->view->report_msg = $docErr;				
			}
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('BADA','Viewing Add Beneficiary Account');
		}
	}
	
	public function confirmAction()
	{
		
		$sessionNamespace = new Zend_Session_Namespace('requestbookcreate');
		$content = $sessionNamespace->content;
//		Zend_Debug::dump($content);
		//print_r($content);die;
		$mode = $sessionNamespace->mode;
		
		$this->view->mode = $mode;
		$this->fillParams($content['SUBJECT'],$content['ACCTSRC'],$content['ACCTSRC_NAME'],$content['PRODUCT_TYPE'],$content['NUMBEROF_SHEET'],$content['ADMIN_FEE']);
		
		/*$this->view->ACCTSRC_NAME = $content['BENEFICIARY_NAME'];
		$this->view->BENEFICIARY_RESIDENT = $content['BENEFICIARY_RESIDENT'];
		if($content['BENEFICIARY_RESIDENT']=='R'){
			$content['BENEFICIARY_RESIDENTTEXT'] = 'Resident';
		}else if($content['BENEFICIARY_RESIDENT']=='NR'){
			$content['BENEFICIARY_RESIDENTTEXT'] = 'Non Resident';
		}
		$this->view->BENEFICIARY_RESIDENTTEXT = $content['BENEFICIARY_RESIDENTTEXT'];
		$this->view->BENEFICIARY_CITIZENSHIP = $content['BENEFICIARY_CITIZENSHIP'];
		if($content['BENEFICIARY_CITIZENSHIP']=='W'){
			$content['BENEFICIARY_CITIZENSHIPTEXT'] = 'WNI';
		}else if($content['BENEFICIARY_CITIZENSHIP']=='N'){
			$content['BENEFICIARY_CITIZENSHIPTEXT'] = 'WNA';
		}
		$this->view->BENEFICIARY_CITIZENSHIPTEXT = $content['BENEFICIARY_CITIZENSHIPTEXT'];
		$this->view->BENEFICIARY_ID_NUMBER = $content['BENEFICIARY_ID_NUMBER'];
		if($content['BENEFICIARY_ID_TYPE'] == 'KIT'){
			$idtype = 'KITAS';
		}elseif($content['BENEFICIARY_ID_TYPE'] == 'PAS'){
			$idtype = 'PASPOR';
		}else{
			$idtype = $content['BENEFICIARY_ID_TYPE'];
		}
		$this->view->BENEFICIARY_ID_TYPE = $idtype;
		$this->view->BENEFICIARY_CATEGORY = $content['BENEFICIARY_CATEGORY'];
		if($content['BENEFICIARY_CATEGORY']=='1'){
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Individual';
		}else if($content['BENEFICIARY_CATEGORY']=='2'){
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Government';
		}else if($content['BENEFICIARY_CATEGORY']=='3'){
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Bank';
		}else if($content['BENEFICIARY_CATEGORY']=='4'){
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Financial';
		}else if($content['BENEFICIARY_CATEGORY']=='5'){
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Company';
		}else if($content['BENEFICIARY_CATEGORY']=='6'){
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Other';
		} 
		$this->view->BENEFICIARY_CATEGORYTEXT = $content['BENEFICIARY_CATEGORYTEXT'];*/
		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			// $this->view->custsame 			= true;
		}
		
		//validasi token -- begin
		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$data2 					= $this->_db->fetchRow($select3);
		$tokenIdUser 			= $data2['TOKEN_ID'];
		$tokenType 				= $data2['TOKEN_TYPE'];
		$usermobilephone 		= $data2['USER_MOBILE_PHONE'];
		$this->view->tokentype 	= $data2['TOKEN_TYPE'];

		
		if($this->_request->isPost() )
		{	
			if($this->_getParam('submit')=='Back')
			{
				
				if($mode=='Add'){					
					$this->_redirect('/requestbook/create/index/isback/1');
				}elseif ($mode=='Edit'){
					$this->_redirect('/requestbook/edit/index/benef_id/'.$content['BENEFICIARY_ID'].'/isback/1');	
				}
			}
			else
			{
				try 
				{
					//$Beneficiary = new Beneficiary();
					$reqbook		= new requestbook_Model_Requestbook();
					$this->_db->beginTransaction();					
					if($mode=='Add'){
						$add = $reqbook->add($content);
						Application_Helper_General::writeLog('BADA','Add Request Book '.$content['ACCTSRC']);
					}elseif ($mode=='Edit'){
						$update = $reqbook->update($content);
						Application_Helper_General::writeLog('BEDA','Edit Request Book '.$content['ACCTSRC']);	
					}
					$this->_db->commit();
					
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e) 
				{
					$this->_db->rollBack();
					
				}
			}
		}
	}
	
	
	private function fillParam($zf_filter_input)
	{
		/*if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID'); 
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS'); 
		$this->view->ACCTSRC = ($zf_filter_input->isValid('ACCTSRC')) ? $zf_filter_input->ACCTSRC : $this->_getParam('ACCTSRC');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');*/
	}
	
	private function fillParams($SUBJECT,$ACCTSRC,$ACCTSRC_NAME,$PRODUCT_TYPE,$NUMBEROF_SHEET,$ADMIN_FEE)
	{
		//if($BENEFICIARY_ID) $this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		$this->view->SUBJECT 	= $SUBJECT;
		$this->view->ACCTSRC_NAME 	= $ACCTSRC_NAME;
		$this->view->ACCTSRC 		= $ACCTSRC;
		$this->view->PRODUCT_TYPE 	= $PRODUCT_TYPE;
		$this->view->NUMBEROF_SHEET = $NUMBEROF_SHEET;
		$this->view->ADMIN_FEE 		= $ADMIN_FEE;
	}
	
	private function validateField($data){
											
		$filters = array(
			'source_acct' 		=> array('StripTags','StringTrim'), 			
			'prod_type' 		=> array('StripTags','StringTrim'), 					
			'numberof_sheet'		=> array('StripTags','StringTrim')
		);
		
		$validators = array(
			'source_acct' => array(
				'NotEmpty',
				/*'Digits',
				new Zend_Validate_StringLength(array('max'=>10,'min'=>10)),*/
				'messages' => array(
					'Source Account cannot be left blank. Please correct it.',
					/*'Beneficiary Account must be numbers. Please correct it.',
					'Beneficiary Account length must be 10 digits. Please correct it.',*/
					)											   					   
			),
						
			'prod_type' => 	array(	
				'NotEmpty',
				//new Zend_Validate_Alnum(),
				//new Zend_Validate_StringLength(array('min'=>3,'max'=>3)),
				//array('InArray', array('haystack' => $this->_listCCYValidate)),
				'messages' => array	(
					'Product Type cannot be left blank. Please correct it.',
					//'04',
					//'04',
					//'Currency is not on the database.'
				)
			),
			
			'numberof_sheet' => array(
				'NotEmpty',
				/*'Digits',
				new Zend_Validate_StringLength(array('max'=>10,'min'=>10)),*/
				'messages' => array(
					'Number of Sheets cannot be left blank. Please correct it.',
					/*'Beneficiary Account must be numbers. Please correct it.',
					'Beneficiary Account length must be 10 digits. Please correct it.',*/
					)											   					   
			),
		);
		
		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$data);
		if($zf_filter_input->isValid()) {
			return true;
		}else{
			return $zf_filter_input;
		}
	}
	
	public function setfavoriteAction()
	{
		$fav = $this->_request->getParam('isfavorite');
		$flag = false;
		foreach($fav as $val)
		{
			if($val == 1)
			{
				$flag = true;
				break;
			}			
		}
		
		// if($flag)
		// {
			foreach($fav as $key=>$value)
			{
				$Beneficiary = new Beneficiary();
				$Beneficiary->setFavorite($key,$value);
			}
			$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
			$this->_redirect('/notification/success');
		// }
		// else
		// {
			// $error_remark = 'Error: Please check selection.';
		    // $this->_helper->getHelper('FlashMessenger')->addMessage('F');
		    // $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
		    // $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
		// }
		
	}
	
	public function suggestdeleteAction()
	{
		$benef_id = $this->_getParam('benef_id');
		$benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
		
		if($benef_id)
		{
			$Beneficiary = new Beneficiary();
			$Beneficiary->suggestDelete($benef_id);
		}
		$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
		$this->_redirect('/notification/success');
	}
}
