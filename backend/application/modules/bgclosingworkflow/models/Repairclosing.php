<?php

class bgclosingworkflow_Model_Repairclosing
{
    protected $_db;

    public function __construct()
    {
        $this->_db = Zend_Db_Table::getDefaultAdapter();
    }

    /**
     * @param array|null $filter
     * @param int|null $status
     * 
     * @return void
     */
    public function getAllClosingBg(array $filter = null, int $status = null, array $counterWarrantyType = []): array
    {
        $select = $this->_db->select()
            ->from(['TBGC' => 'TEMP_BANK_GUARANTEE_CLOSE'])
            ->joinLeft(['TBG' => 'T_BANK_GUARANTEE'], 'TBGC.BG_NUMBER = TBG.BG_NUMBER', [
                '*',
                'BRANCH_NAME' => new Zend_Db_Expr('(SELECT MB.BRANCH_NAME FROM M_BRANCH MB WHERE MB.BRANCH_CODE = TBG.BG_BRANCH LIMIT 1)'),
                'CUST_NAME' => new Zend_Db_Expr('(SELECT MC.CUST_NAME FROM M_CUSTOMER MC WHERE MC.CUST_ID = TBG.CUST_ID LIMIT 1)'),
                'CURRENCY_BG' => new Zend_Db_Expr('(SELECT TBGD.PS_FIELDVALUE FROM T_BANK_GUARANTEE_DETAIL TBGD WHERE LOWER(TBGD.PS_FIELDNAME) = \'currency\' LIMIT 1)')
            ])
            ->joinLeft(['MB' => 'M_BRANCH'], 'TBG.BG_BRANCH = MB.BRANCH_CODE', []);

        $auth = Zend_Auth::getInstance()->getIdentity();
        if ($auth->userHeadQuarter == "NO") {
            $select->where('MB.ID = ?', $auth->userBranchId); // Add Bahri
        }

        $select->where('TBGC.CHANGE_TYPE = ?', '3');

        $select->where('TBG.COUNTER_WARRANTY_TYPE IN (?)', $counterWarrantyType);

        // cek status
        if ($status) $select->where('TBGC.SUGGESTION_STATUS = ?', $status);

        // jika tidak ada filter
        if (!$filter) {
            $select->order('TBGC.LASTUPDATED DESC');
            return $this->_db->fetchAll($select);
        }

        $specialCondition = ['BG_AMOUNT'];
        foreach ($filter as $key => $value) {
            # code...
            if (!in_array($key, $specialCondition)) {
                $select->where('LOWER(TBG.' . $key . ') LIKE ?', '%' . $value . '%');
            } else {
                // special condition if needed 
                if (strtolower($key) == 'bg_amount') $select->where('TBG.BG_AMOUNT >= ?', str_replace([',', ''], '', $value));
            }
        }

        $select->order('TBGC.LASTUPDATED DESC');
        return $this->_db->fetchAll($select);
    }
}
