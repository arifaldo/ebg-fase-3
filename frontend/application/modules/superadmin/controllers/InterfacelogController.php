<?php

class superadmin_InterfacelogController extends Zend_Controller_Action 
{
	
	public function init()
	{
		$this->_helper->layout()->setLayout('layout_superadmin');
		$this->_db 	= Zend_Db_Table::getDefaultAdapter();
		$objSessionNamespace = new Zend_Session_Namespace( 'sysadmin' );
		
		if(isset($objSessionNamespace->userIdLogin) && isset($objSessionNamespace->userNameLogin))
		{
			$suser_id	= $objSessionNamespace->userIdLogin;
			$this->view->userId = $suser_id;
		}
		else
		{
			$this->_redirect('/superadmin');
		}
	}
	
	public function indexAction() 
	{
		$select = $this->_db->select()->distinct()
										->from(
													array('T_INTERFACE_LOG'),
													array(
															'SERVICE_NAME',
														)
												);
/* LOG_ID
LOG_REQ
LOG_RESP
SERVICE_TYPE
CHANNEL_ID
SERVICE_NAME
UUID
REF_ID
XML_REQ
XML_RESP
RESP_CODE
RESP_DESC */

		$arrfunction 				= $this->_db->fetchAll($select);
		$this->view->arrfunction 	= $arrfunction;
		
		$fields = array	(	
							'CHANNEL_ID'   => 	array	(	
															'field'    => 'CHANNEL_ID',
															'label'    => 'Channel ID',
															'sortable' => false
														),
							'SERVICE_NAME'  	   	=> 	array	(	
															'field'    => 'SERVICE_NAME',
															'label'    => 'Service Name',
															'sortable' => false
														),
							'LOG_REQ'   => 	array	(	
															'field'    => 'LOG_REQ',
															'label'    => 'Request Date',
															'sortable' => false
														),
							'LOG_RESP' 		=> 	array	(	
															'field'    => 'LOG_RESP',
															'label'    => 'Response Date',
															'sortable' => false
														),
							'UUID' 		=> 	array	(	
															'field'    => 'UUID',
															'label'    => 'UUID',
															'sortable' => false
														),
							'REF_ID' 		=> 	array	(	
															'field'    => 'REF_ID',
															'label'    => 'REF_ID',
															'sortable' => false
														),
							'XML_REQ' 		=> 	array	(	
															'field'    => 'XML_REQ',
															'label'    => 'XML_REQ',
															'sortable' => false
														),
							'XML_RESP' 		=> 	array	(	
															'field'    => 'XML_RESP',
															'label'    => 'XML_RESP',
															'sortable' => false
														),
							'RESP_CODE' 		=> 	array	(	
															'field'    => 'RESP_CODE',
															'label'    => 'RESP_CODE',
															'sortable' => false
														),
							'RESP_DESC' 		=> 	array	(	
															'field'    => 'RESP_DESC',
															'label'    => 'RESP_DESC',
															'sortable' => false
														),
						);
						
		$this->view->fields = $fields;

		//$select->order($sortBy.' '.$sortDir);
		$filterArr = array(
								'filter' 			=> array('StripTags','StringTrim'),
								'SERVICE_NAME' 	=> array('StripTags','StringTrim'),
								'LOG_REQ'    		=> array('StripTags','StringTrim'),
								'UUID' 				=> array('StripTags','StringTrim'),
								'limit'  			=> array('StripTags','StringTrim'),
								'sortdate'  			=> array('StripTags','StringTrim'),
							);
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());

		if($zf_filter->getEscaped('filter')=='Set Filter')
		{
			$select = $this->_db->select()->from(
													array('T_INTERFACE_LOG'),
													array(
															'*'
														)
												);
			if($zf_filter->getEscaped('limit'))
			{
				$select->limit($zf_filter->getEscaped('limit'),1);
				$this->view->FUNCTION_NAME = $zf_filter->getEscaped('FUNCTION_NAME');
			}
			else
			{
				$select->limit(50,1);
			}			
			
			if($zf_filter->getEscaped('SERVICE_NAME'))
			{
				$select->where('SERVICE_NAME LIKE '.$this->_db->quote('%'.$zf_filter->getEscaped('SERVICE_NAME').'%'));
				$this->view->SERVICE_NAME = $zf_filter->getEscaped('SERVICE_NAME');
			}
			if($zf_filter->getEscaped('UUID'))
			{
				$select->where('UUID LIKE '.$this->_db->quote('%'.$zf_filter->getEscaped('UUID').'%'));
				$this->view->fSTRING = $zf_filter->getEscaped('UUID');
			}
			
			if($zf_filter->getEscaped('LOG_REQ'))
			{
				$from   = 	(Zend_Date::isDate($zf_filter->getEscaped('LOG_REQ'),"dd/MM/yyyy"))?new Zend_Date($zf_filter->getEscaped('from'),$this->_dateDisplayFormat):false;
				
				if($from)
				{
					$select->where('DATE(LOG_REQ) >= '.$this->_db->quote($from));
				}
				$this->view->LOG_DATE = $zf_filter->getEscaped('LOG_REQ');
			}
			if($zf_filter->getEscaped('sortdate'))
			{
				$sortBy = 'LOG_REQ';
				$sortDir = $zf_filter->getEscaped('sortdate');
				$select->order($sortBy.' '.$sortDir);
				$this->view->sortdate = $zf_filter->getEscaped('sortdate');
			}

			$arr = $this->_db->fetchAll($select);
			

			$this->view->arr = $arr;
		}
		
	}
}