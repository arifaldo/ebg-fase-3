<?php

class virtualpayment_TransactionController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';
	
	
	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
	}
	
	public function indexAction (){
		
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
				'VP_NUMBER'	=> array(
						'field'	=> 'VP_NUMBER',
						'label'	=> $this->language->_('VA Number'),
						'sortable'	=> true
				),
				'BANK_REFNO'	=> array(
						'field'	=> 'BANK_REFNO',
						'label'	=> $this->language->_('Payment Reff #'),
						'sortable'	=> true
				),
				'CUST_ID'	=> array(
						'field' => 'CUST_ID',
						'label' => $this->language->_('Company Code'),
						'sortable'	=> true
				),
				'CUST_BIN'	=> array(
						'field' => 'CUST_BIN',
						'label' => $this->language->_('BIN'),
						'sortable'	=> true
				),
				'INVOICE_NAME'	=> array(
						'field' => 'INVOICE_NAME',
						'label' => $this->language->_('Invoice Name'),
						'sortable'	=> true
				),
				'INVOICE_DESC'	=> array(
						'field' => 'INVOICE_DESC',
						'label' => $this->language->_('Invoice Description'),
						'sortable'	=> true
				),
				'CCY'	=> array(
						'field' => 'CCY',
						'label' => $this->language->_('CCY'),
						'sortable'	=> true
				),
				'AMOUNT'	=> array(
						'field' => 'AMOUNT',
						'label' => $this->language->_('Amount'),
						'sortable'	=> true
				),
				'VP_MODE'	=> array(
						'field' => 'VP_MODE',
						'label' => $this->language->_('VA Type'),
						'sortable'	=> true
				),
				'PAID_STATUS'	=> array(
						'field' => 'PAID_STATUS',
						'label' => $this->language->_('Status'),
						'sortable'	=> true
				),
				
				'VP_CREATED'	=> array(
						'field' => 'VP_CREATED',
						'label' => $this->language->_('Upload Date'),
						'sortable'	=> true
				),
				
				'VP_PAID_DATE'	=> array(
						'field' => 'VP_PAID_DATE',
						'label' => $this->language->_('Paid Date'),
						'sortable'	=> true
				),
		
		);
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($fields, 'field');
		
//		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[0]);
		$sortBy  		=($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('VP_CREATED');
		
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
//		unset($stringParam['module']);
//		unset($stringParam['controller']);
//		unset($stringParam['action']);
//		unset($stringParam['filter']);
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
				'INST_ID'			=> array(),
				'INST_NAME'			=> array(),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		
		$CUST_BIN = $this->_getParam('CUST_BIN');
		$VP_NUMBER = $this->_getParam('VP_NUMBER');
		$this->view->cust_bin = $CUST_BIN;
		$this->view->vp_number = $VP_NUMBER;
		
		$uploadStart = $this->_getParam('uploadStart');
		$uploadEnd = $this->_getParam('uploadEnd');
		$payStart = $this->_getParam('payStart');
		$payEnd = $this->_getParam('payEnd');
		
		$va_type = $this->_getParam('va_type');
		$this->view->va_type = $va_type;
		
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		
		$optSync = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'1' => $this->language->_('Paid'),
				'2' => $this->language->_('Unpaid'),
				'3' => $this->language->_('Delete'),
		);
		
		$optSyncVpMode = array(
				//'' => '-- Please Select --',
				'0' => $this->language->_('Open'),
				'1' => $this->language->_('Close'),
		);
		
//		$optFrom = array(
//				//'' => '-- Please Select --',
//				'' => '-',
//		);
		$this->view->optSync 		= $optSync;
		$this->view->optSyncVpMode 	= $optSyncVpMode;
		//$this->view->optFrom 		= $optFrom;
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
		
//			$select = $this->_db->select()
//			->from(
//					array('D' => 'T_VP_DETAIL'),
//					array('VP_NUMBER','INSTITUTIONS_ID','INVOICE_NO','INVOICE_NAME','AMOUNT','VP_EFDATE','VP_STATUS')
//			)
//			
//			$select->joinLeft(
//					array('M' => 'M_INSTITUSI'),'D.INSTITUTIONS_ID = M.INST_ID')	
//					
//			->order($sortBy.' '.$sortDir);

			$select = $this->_db->select()
//				->from(array('A' => 'T_VP_TRANSACTION'),array('VP_NUMBER','BANK_REFNO','CUST_ID','MU.CUST_NAME','CUST_BIN','INVOICE_NAME','INVOICE_DESC','CCY','AMOUNT','PAID_STATUS','DEVICE_ID','VP_CREATED','VP_PAID_DATE','VP_MODE'));
				->from(array('A' => 'T_VP_TRANSACTION_HISTORY'),array('ID','VP_NUMBER','BANK_REFNO','CUST_ID','CUST_BIN','INVOICE_NAME','INVOICE_DESC','CCY','AMOUNT','VP_MODE','PAID_STATUS','VP_CREATED','VP_PAID_DATE'));
//				->from(array('A' => 'T_VP_DETAIL'),array('A.VP_NUMBER','A.INSTITUTIONS_ID','A.INVOICE_NO','A.INVOICE_NAME','A.AMOUNT','A.VP_EFDATE','MB.PAID_STATUS','MB.PAID_FROM'));
//			$select->joinLeft(array('MU'=>'M_CUSTOMER'), 'A.CUST_ID = MU.CUST_ID', array('MU.CUST_NAME'));		
//			$select->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'A.VP_NUMBER = MB.VP_NUMBER', array(''));		
			$select ->order($sortBy.' '.$sortDir);
			$select->where('A.CUST_ID = '.$this->_db->quote($this->_custIdLogin));
			
			
			//$select->where('INST_NAME like "%pajak%"');
			
			if (!empty($CUST_BIN)){
				$select->where('A.CUST_BIN like '.$this->_db->quote('%'.$CUST_BIN.'%'));
			}
			
			if (!empty($VP_NUMBER)){
				$select->where('A.VP_NUMBER like '.$this->_db->quote('%'.$VP_NUMBER.'%'));
			}
			
			if (!empty($va_type) || $va_type == '0'){
				$select->where('A.VP_MODE = ?', $va_type);
			}
			
			if($uploadStart)
			{
				$FormatDate 	= new Zend_Date($uploadStart, $this->_dateDisplayFormat);
				$updatedFrom   	= $FormatDate->toString($this->_dateDBFormat);	
				$select->where('DATE(VP_CREATED) >= ?', $updatedFrom);
			}
			
			if($uploadEnd)
			{
				$FormatDate 	= new Zend_Date($uploadEnd, $this->_dateDisplayFormat);
				$updatedTo   	= $FormatDate->toString($this->_dateDBFormat);	
				$select->where('DATE(VP_CREATED) <= ?', $updatedTo);
			}
			
			if($payStart)
			{
				$FormatDate 	= new Zend_Date($payStart, $this->_dateDisplayFormat);
				$createdFrom   	= $FormatDate->toString($this->_dateDBFormat);	
				$select->where('DATE(VP_PAID_DATE) >= ?', $createdFrom);
			}
			
			if($payEnd)
			{
				$FormatDate 	= new Zend_Date($payEnd, $this->_dateDisplayFormat);
				$createdTo   	= $FormatDate->toString($this->_dateDBFormat);	
				$select->where('DATE(VP_PAID_DATE) <= ?', $createdTo);
			}
			
			$this->view->uploadStart = $uploadStart;
			$this->view->uploadEnd = $uploadEnd;
			$this->view->payStart = $payStart;
			$this->view->payEnd = $payEnd;
				
			// 			echo $select;
			$data = $this->_db->fetchAll($select);
//			unset($data['ID']);
//			Zend_Debug::dump($data);
		}
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
		
		if($csv || $pdf)
		{
			$arr = $this->_db->fetchAll($select);
			
			foreach ($arr as $key => $value)
			{
				//echo $key;
				$arr[$key]["VP_CREATED"] = Application_Helper_General::convertDate($value["VP_CREATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$arr[$key]["VP_PAID_DATE"] = Application_Helper_General::convertDate($value["VP_PAID_DATE"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$arr[$key]["AMOUNT"] = SGO_Helper_GeneralFunction::displayCurrency($value["AMOUNT"]);
				if($value["VP_MODE"] == '0'){
					$arr[$key]["VP_MODE"] = 'Open';	
				}
				else{
					$arr[$key]["VP_MODE"] = 'Close';	
				}
				
				if($value["PAID_STATUS"] == '3'){
					$arr[$key]["PAID_STATUS"] = 'Delete';	
				}
				elseif($value["PAID_STATUS"] == '2'){
					$arr[$key]["PAID_STATUS"] = 'Unpaid';	
				}
				elseif($value["PAID_STATUS"] == '1'){
					$arr[$key]["PAID_STATUS"] = 'Paid';	
				}
//				$arr[$key]["PS_UPDATED"] = Application_Helper_General::convertDate($value["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
//				$arr[$key]["PS_EFDATE"] = Application_Helper_General::convertDate($value["PS_EFDATE"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
//				$arr[$key]["SOURCE_ACCOUNT_NAME"]= $value["SOURCE_ACCOUNT_NAME"].' / '.$value["SOURCE_ACCOUNT_ALIAS_NAME"];
//				$arr[$key]["BENEFICIARY_ACCOUNT_NAME"]= $value["BENEFICIARY_ACCOUNT_NAME"].' / '.$value["BENEFICIARY_ALIAS_NAME"];
//				$arr[$key]["BENEFICIARY_ACCOUNT_CCY"]= $value["BENEFICIARY_ACCOUNT_CCY"].' / '.$value["TRA_AMOUNT"].$persenLabel;
//				$arr[$key]["PS_TYPE"]= $this->language->_($value["PS_TYPE"]).' ('.$value["TRANSFER_TYPE"].')';
//				unset($arr[$key]["SOURCE_ACCOUNT_ALIAS_NAME"]);
//				unset($arr[$key]["BENEFICIARY_ALIAS_NAME"]);
//				unset($arr[$key]["TRA_AMOUNT"]);
//				unset($arr[$key]["TRANSFER_TYPE"]);
//				$arr[$key]["TRA_STATUS"]= $this->language->_($value["TRA_STATUS"]);
				
			}
			
			$header = Application_Helper_Array::simpleArray($fields, 'label');
			//Zend_Debug::dump($arr);die;
			if($csv)
			{
				Application_Helper_General::writeLog('VVPT','Download CSV Virtual Payment Transaction');
				//Zend_Debug::dump($arr);die;
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->csv($header,$arr,null,'Virtual Payment Upload');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('VVPT','Download PDF Virtual Payment Transaction');
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->pdf($header,$arr,null,'Virtual Payment Upload');
			}
		}
		Application_Helper_General::writeLog('VVPT','View Virtual Payment Upload');
		
		$this->paging($data);
	}
	
}
