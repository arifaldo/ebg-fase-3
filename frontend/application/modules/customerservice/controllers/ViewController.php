<?php

class customerservice_ViewController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$arrquestioncategory 	= array_combine($this->_questioncategory["code"],$this->_questioncategory["desc"]);
		$questioncategoryArray 	= array( '' => '--- '.$this->language->_('Please Select').' --- ');
		$questioncategoryArray += array_combine(array_values($this->_questioncategory['code']),array_values($this->_questioncategory['desc']));				
		//$this->view->questioncategoryArray 		= $questioncategoryArray;
		
		foreach($questioncategoryArray as $key => $value){ if($key != 5) $optpayStatusRaw[$key] = $this->language->_($value); }
	
		$optPayType = $optpayStatusRaw;
		$this->view->questioncategoryArray 	= $optPayType;
		
		
		$casequestioncategory = "(CASE CS.QUESTION_CATEGORY ";
  		foreach($arrquestioncategory as $key => $val)	{ $casequestioncategory .= " WHEN ".$key." THEN '".$val."'"; }
  		$casequestioncategory .= " END)";
		
  		$date = $this->language->_('Date');
		
  		$date_language = $this->language->_('Date');
		$category_language = $this->language->_('Category');
		$question_language = $this->language->_('Question');
		$answered_language = $this->language->_('Answered Date');
		$answer_language = $this->language->_('Answer');
		$answeredby_language = $this->language->_('Answered By');
		
		
		$fields = array	(	
							'QUESTION_DATE'  	=> 	array	(	
																'field'    => 'QUESTION_DATE',
																'label'    => $date_language,
																'sortable' => TRUE
															),
							'CATEGORY' => 	array	(	
																'field'    => 'QUESTION_CATEGORY',
																'label'    => $category_language,
																'sortable' => TRUE
															),
							'QUESTION_Q' 		=> 	array	(	
																'field'    => 'QUESTION_Q',
																'label'    => $question_language,
																'sortable' => TRUE
															),
							'QUESTION_A_DATE' 	=> 	array	(	
																'field'    => 'QUESTION_A_DATE',
																'label'    => $answered_language,
																'sortable' => TRUE
															),
							'QUESTION_A' 		=> 	array	(	
																'field'    => 'QUESTION_A',
																'label'    => $answer_language,
																'sortable' => TRUE
															),
							'BUSER_ID' 			=> 	array	(	
																'field'    => 'BUSER_ID',
																'label'    => $answeredby_language,
																'sortable' => TRUE
															),
						);

		$filterlist = array('QUEST_DATE','ANS_DATE','CATEGORY');
		
		$this->view->filterlist = $filterlist;

						
		$this->view->fields = $fields;
		
		$page    = $this->_getParam('page');		
        $sortBy  = $this->_getParam('sortby','QUESTION_DATE');
        $sortDir = $this->_getParam('sortdir','desc');
		
        $page 		= (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
        $sortBy 	= (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
				
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array(
								'filter' 		=> array('StripTags','StringTrim'),
								'QUEST_DATE' 	=> array('StripTags','StringTrim'),
								'QUEST_DATE_END' 	=> array('StripTags','StringTrim'),
								'ANS_DATE' 	=> array('StripTags','StringTrim'),
								'ANS_DATE_END' 		=> array('StripTags','StringTrim'),
								'CATEGORY' 		=> array('StripTags','StringTrim'),
							);

		$dataParam = array("CATEGORY");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
			if(!empty($this->_request->getParam('questdate'))){
				$createarr = $this->_request->getParam('questdate');
					$dataParamValue['QUEST_DATE'] = $createarr[0];
					$dataParamValue['QUEST_DATE_END'] = $createarr[1];
			}
			if(!empty($this->_request->getParam('ansdate'))){
				$updatearr = $this->_request->getParam('ansdate');
					$dataParamValue['ANS_DATE'] = $updatearr[0];
					$dataParamValue['ANS_DATE_END'] = $updatearr[1];
			}

			

		$zf_filter 		= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter 		= $zf_filter->getEscaped('filter');
		$questionfrom 	= $zf_filter->getEscaped('QUEST_DATE');
		$questionto 	= $zf_filter->getEscaped('QUEST_DATE_END');
		$answerfrom 	= $zf_filter->getEscaped('ANS_DATE');
		$answerto 		= $zf_filter->getEscaped('ANS_DATE_END');
		$category 		= $zf_filter->getEscaped('CATEGORY');
		
		$select = $this->_db->select()
							->from(	array(	'CS'				=> 'CUSTOMER_SERVICES'),
									array(	'QUESTION_DATE'		=> 'QUESTION_DATE',	
											'CATEGORY'			=> $casequestioncategory,
											'QUESTION_Q'		=> 'QUESTION_Q',	
											'QUESTION_A_DATE'	=> 'QUESTION_A_DATE',	
											'QUESTION_A'		=> 'QUESTION_A',	
											'BUSER_ID'			=> 'BUSER_ID',	
											)
									);
		if($filter==$this->language->_('Set Filter'))
		{
			if($questionfrom)
			{
				$FormatDate = new Zend_Date($questionfrom, $this->_dateDisplayFormat);
				$from  = $FormatDate->toString($this->_dateDBFormat);	
				
				$select->where('convertsgo("date",CS.QUESTION_DATE) >= '.$this->_db->quote($from));
				$this->view->questionfrom = $questionfrom;
			}
			
			if($questionto)
			{
				$FormatDate = new Zend_Date($questionto, $this->_dateDisplayFormat);
				$from  = $FormatDate->toString($this->_dateDBFormat);
				
				$select->where('convertsgo("date",CS.QUESTION_DATE) <= '.$this->_db->quote($from));
				$this->view->questionto = $questionto;
			}
			
			if($answerfrom)
			{
				$FormatDate = new Zend_Date($answerfrom, $this->_dateDisplayFormat);
				$from  = $FormatDate->toString($this->_dateDBFormat);
				
				$select->where('convertsgo("date",CS.QUESTION_A_DATE) >= '.$this->_db->quote($from));
				$this->view->answerfrom = $answerfrom;
			}
			
			if($answerto)
			{
				$FormatDate = new Zend_Date($answerto, $this->_dateDisplayFormat);
				$from  = $FormatDate->toString($this->_dateDBFormat);
				
				$select->where('convertsgo("date",CS.QUESTION_A_DATE) <= '.$this->_db->quote($from));
				$this->view->answerto = $answerto;
			}
			
			if($category)
			{
				$select->where('CS.QUESTION_CATEGORY = '.$this->_db->quote($category));
				$this->view->category = $category;
			}
			
		}
		$select->where('CS.USER_ID = '.$this->_db->quote($this->_userIdLogin));
		Application_Helper_General::writeLog('CSVQ','Customer Service View Question');
		
		$select->order($sortBy.' '.$sortDir);
		
		$arr = $this->_db->fetchAll($select);
		$this->paging($arr);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}
