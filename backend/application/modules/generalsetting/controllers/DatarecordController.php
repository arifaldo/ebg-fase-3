<?php

require_once 'Zend/Controller/Action.php';

class Generalsetting_DatarecordController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()
			->from(array('A' => 'M_SETTING'), array('SETTING_ID', 'SETTING_VALUE'))
			->where('MODULE_ID = ?', 'GNS')
			->query()->FetchAll();
		$setting = Application_Helper_Array::listArray($select, 'SETTING_ID', 'SETTING_VALUE');

		foreach ($setting as $key => $value) {
			if (!empty($key))
				$this->view->$key = $value;
		}

		// convert to validate
		//			$threshold_rtgs = $this->_getParam('threshold_rtgs');		
		//			$threshold_rtgs_val =	Application_Helper_General::convertDisplayMoney($threshold_rtgs);
		//			$this->_setParam('threshold_rtgs',$threshold_rtgs_val);

		$threshold_lld = $this->_getParam('threshold_lld');
		$threshold_lld_val =	Application_Helper_General::convertDisplayMoney($threshold_lld);
		$this->_setParam('threshold_lld', $threshold_lld_val);

		$admin_fee_company = $this->_getParam('admin_fee_company');
		$admin_fee_company_val =	Application_Helper_General::convertDisplayMoney($admin_fee_company);
		$this->_setParam('admin_fee_company', $admin_fee_company_val);

		$admin_fee_account = $this->_getParam('admin_fee_account');
		$admin_fee_account_val =	Application_Helper_General::convertDisplayMoney($admin_fee_account);
		$this->_setParam('admin_fee_account', $admin_fee_account_val);

		$global_charges_skn = $this->_getParam('global_charges_skn');
		$global_charges_skn_val =	Application_Helper_General::convertDisplayMoney($global_charges_skn);
		$this->_setParam('global_charges_skn', $global_charges_skn_val);

		$global_charges_rtgs = $this->_getParam('global_charges_rtgs');
		$global_charges_rtgs_val =	Application_Helper_General::convertDisplayMoney($global_charges_rtgs);
		$this->_setParam('global_charges_rtgs', $global_charges_rtgs_val);

		$threshold_lld_remittance = $this->_getParam('threshold_lld_remittance');
		$threshold_lld_remittance_val =	Application_Helper_General::convertDisplayMoney($threshold_lld_remittance);
		$this->_setParam('threshold_lld_remittance', $threshold_lld_remittance_val);

		$remittance_limit_permonthly = $this->_getParam('remittance_limit_permonthly');
		$remittance_limit_permonthly_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_permonthly);
		$this->_setParam('remittance_limit_permonthly', $remittance_limit_permonthly_val);


		$min_amount_skn = $this->_getParam('min_amount_skn');
		$min_amount_skn_val =	Application_Helper_General::convertDisplayMoney($min_amount_skn);
		$this->_setParam('min_amount_skn', $min_amount_skn_val);

		$max_amount_skn = $this->_getParam('max_amount_skn');
		$max_amount_skn_val =	Application_Helper_General::convertDisplayMoney($max_amount_skn);
		$this->_setParam('max_amount_skn', $max_amount_skn_val);

		$min_amount_rtgs = $this->_getParam('min_amount_rtgs');
		$min_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($min_amount_rtgs);
		$this->_setParam('min_amount_rtgs', $min_amount_rtgs_val);

		$max_amount_rtgs = $this->_getParam('max_amount_rtgs');
		$max_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($max_amount_rtgs);
		$this->_setParam('max_amount_rtgs', $max_amount_rtgs_val);

		$escrow_acct_idr = $this->_getParam('escrow_acct_idr');
		// $escrow_acct_idr_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_idr);
		$this->_setParam('escrow_acct_idr', $escrow_acct_idr);

		$escrow_acct_usd = $this->_getParam('escrow_acct_usd');
		// $escrow_acct_usd_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_usd);
		$this->_setParam('escrow_acct_usd', $escrow_acct_usd);

		$remittance_limit_per_month = $this->_getParam('remittance_limit_per_month');
		$remittance_limit_per_month_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_per_month);
		$this->_setParam('remittance_limit_per_month', $remittance_limit_per_month_val);

		/* $SKN_cost = $this->_getParam('SKN_cost');		
			$SKN_cost_val =	Application_Helper_General::convertDisplayMoney($SKN_cost);
			$this->_setParam('SKN_cost',$SKN_cost_val);

			$RTGS_cost = $this->_getParam('RTGS_cost');		
			$RTGS_cost_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_cost',$RTGS_cost_val); */

		/* $SKN_charge = $this->_getParam('SKN_charge');		
			$SKN_charge_val =	Application_Helper_General::convertDisplayMoney($SKN_charge);
			$this->_setParam('SKN_charge',$SKN_charge_val);

			$RTGS_charge = $this->_getParam('RTGS_charge');		
			$RTGS_charge_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_charge',$RTGS_charge_val); */

		//FILTER
		{
			$filters = array(

				'paging_frontend' => array('StringTrim', 'StripTags'),
				'max_import_bene' => array('StringTrim', 'StripTags'),
				'max_import_single_payment' => array('StringTrim', 'StripTags'),
				'max_import_bulk' => array('StringTrim', 'StripTags'),
				'max_ntu_date' => array('StringTrim', 'StripTags'),
				'max_row_page' => array('StringTrim', 'StripTags'),
				'max_claim_period' => array('StringTrim', 'StripTags'),
				'konstanta_setahun' => array('StringTrim', 'StripTags'),
				'otp_exp_trx' => array('StringTrim', 'StripTags'),
				'toleransi_kurs' => array('StringTrim', 'StripTags'),
			);
		}

		//VALIDATE
		{
			$validators = array(

				'paging_frontend' => array(
					'NotEmpty',
					'Digits',
					array('StringLength', array('min' => 1, 'max' => 13)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Too many significant digits. Maximum digit allowed 13 digit(s)'
					)
				),
				'max_import_bene' => array(
					'NotEmpty',
					'Digits',
					new Zend_Validate_Between(array('min' => 1, 'max' => 1000)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Value between 1 - 1000',
					)
				),
				'max_import_single_payment' => array(
					'NotEmpty',
					'Digits',
					new Zend_Validate_Between(array('min' => 1, 'max' => 1000)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Value between 1 - 1000'
					)
				),
				'max_import_bulk' => array(
					'NotEmpty',
					'Digits',
					array('StringLength', array('min' => 1, 'max' => 13)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Too many significant digits. Maximum digit allowed 13 digit(s)'
					)
				),
				'max_ntu_date' => array(
					'NotEmpty',
					'Digits',
					array('StringLength', array('min' => 1, 'max' => 13)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Too many significant digits. Maximum digit allowed 13 digit(s)'
					)
				),
				'max_row_page' => array(
					'NotEmpty',
					'Digits',
					array('StringLength', array('min' => 1, 'max' => 13)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Too many significant digits. Maximum digit allowed 13 digit(s)'
					)
				),
				'max_claim_period' => array(
					'NotEmpty',
					'Digits',
					array('StringLength', array('min' => 1, 'max' => 13)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Too many significant digits. Maximum digit allowed 13 digit(s)'
					)
				),
				'konstanta_setahun' => array(
					'NotEmpty',
					'Digits',
					array('StringLength', array('min' => 1, 'max' => 13)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Too many significant digits. Maximum digit allowed 13 digit(s)'
					)
				),
				'otp_exp_trx' => array(
					'NotEmpty',
					'Digits',
					array('StringLength', array('min' => 1, 'max' => 13)),
					'messages' => array(
						'Can not be empty',
						'Must be numeric values',
						'Too many significant digits. Maximum digit allowed 13 digit(s)'
					)
				),
				'toleransi_kurs' => array(
                    'NotEmpty',
                    new Zend_Validate_Regex('/^\d+(\.\d{1,2})?$/'),
                    array('Between', array('min' => 0.01, 'max' => 100.00)),
                    'messages' => array(
                        $this->language->_('Can not be empty'),
                        $this->language->_('Invalid Format'),
                        [
                            Zend_Validate_Between::NOT_BETWEEN =>
                            "Amount must between '%min%' and '%max%'",
                        ],
                    )
                ),

			);
		}

		$zf_filter = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

		// $arrfck = array('max_row_page','max_ntu_date','max_claim_period');
		$arrfck = array('paging_frontend', 'max_ntu_date', 'max_claim_period', 'konstanta_setahun', 'otp_exp_trx', 'toleransi_kurs');

		$cek = $this->_db->select()
			->FROM(array('B' => 'TEMP_SETTING'), array('MODULE_ID'))
			->WHERE('MODULE_ID = ?', 'GNS')
			->query()->FetchAll();
		if ($cek == null) {
			if ($this->_request->isPost() && $this->view->hasPrivilege('STUD')) {


				if ($zf_filter->isValid()) {
					$this->_db->beginTransaction();
					try {

						$info = "GENERAL SETTING";
						$change_id = $this->suggestionWaitingApproval('Global Data record Setting', $info, $this->_changeType['code']['edit'], null, 'M_SETTING', 'TEMP_SETTING', '', '');
						//$change_id = 1;

						foreach ($setting as $key => $value) {
							if (in_array($key, $arrfck)) {
								$valuee = $this->_getParam2($key);
								$data = array(
									'CHANGES_ID' => $change_id,
									'SETTING_ID' => $key,
									'SETTING_VALUE' => htmlspecialchars_decode($valuee),
									'MODULE_ID' => 'GNS',
								);
								$this->_db->insert('TEMP_SETTING', $data);
							}
						}

						Application_Helper_General::writeLog('STUD', 'Update General Setting');
						$this->_db->commit();
						$this->view->success = true;
						$msg = "Settings Change Request Saved";
						$this->view->report_msg = $msg;
					} catch (Exception $e) {
						$this->_db->rollBack();
					}
				} else {
					$docErr = 'Error in processing form values. Please correct values and re-submit.';
					$this->view->report_msg = $docErr;

					foreach ($zf_filter->getMessages() as $key => $err) {
						$xxx = 'x' . $key;
						// $this->view->$xxx = $this->displayError($err);
						foreach ($err as $key => $value) {
                            # code...
                            $this->view->$xxx = $value;
                            break;
                        }
					}
				}
			}
		} else {
			$this->view->error = true;
			//$this->fillParam($zf_filter_input);
			$this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
		}


		if ($this->_request->isPost()) {

			foreach ($setting as $key => $value) {
				if (in_array($key, $arrfck)) {
					$this->view->$key =   $this->_getParam($key);
				} else {
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
				}
			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		} else {
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			Application_Helper_General::writeLog('STLS', 'View General Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 200 - $disable_note_len;
		$note_len 	 = 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;
	}
}
