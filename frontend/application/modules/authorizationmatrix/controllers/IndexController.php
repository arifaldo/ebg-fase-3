<?php
require_once 'Zend/Controller/Action.php';

class authorizationmatrix_IndexController extends Application_Main
{
	//protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{     
        //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$cust_id = $this->_custIdLogin;

	    $select = $this->_db->select()
						->from(array('MAB'=>'M_APP_BOUNDARY'))
						->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
						->where('MAB.CUST_ID = ?', (string)$cust_id)
						->group('BOUNDARY_ID');

		$result = $this->_db->fetchAll($select);

		$dataArr = array();
		// $alf = 'A';
		// $alf++;
		// ++$alf;
		// print_r((int)$alf);
		// $nogroup = sprintf("%02d", 1);
		// print_r($result);die;
		foreach($result as $row){
			list($grouptype,$groupname,$groupnum) = explode("_",$row['GROUP_USER_ID']);
			if($grouptype == 'N')
				$name = 'Group '.trim($groupnum,'0');
			else
				$name = 'Special Group';

			$selectUser = $this->_db->select()
									->from(array('M_APP_GROUP_USER'),array('USER_ID'))
									->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
									// echo $selectUser;die;
									->query()->fetchall();

			$userlist = '';

			$policy = explode(' AND ', $row['POLICY']);

			foreach ($policy as $key => $value) {
				if($value == 'SG'){
					$group = 'S_'.$cust_id;
					$selectUser = $this->_db->select()
									->from(array('M_APP_GROUP_USER'),array('USER_ID'))
									->where('GROUP_USER_ID = ?', $group)
									// echo $selectUser;die;
									->query()->fetchall();	
					foreach($selectUser as $val){
						if(empty($userlist))
							$userlist .= $val['USER_ID'];
						else
							$userlist .= ', '.$val['USER_ID'];
					}													
				}else{



                        $alphabet = array("A"=>1,"B"=>2,"C"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>26,);

                        $policyor = explode(' OR ', $value);
                        // print_r($policyor);die;
                        foreach ($policyor as $numb => $valpol) {
                          if($valpol == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist)){
                            $userlist .= $val['USER_ID'];
                          }
                          else{
                            $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }else{
                          $nogroup = sprintf("%02d", $alphabet[$valpol]);
                          // print_r($valpol);
                          $group = 'N_'.$cust_id.'_'.$nogroup;
                          $selectUser = $this->_db->select()
                                  ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                  ->where('GROUP_USER_ID = ?', $group)
                                  // echo $selectUser;die;
                                  ->query()->fetchall();  
                                // print_r($selectUser);  
                          foreach($selectUser as $val){
                            if(empty($userlist))
                              $userlist .= $val['USER_ID'];
                            else
                              $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }
                        

                      }
			}
			// print_r($policy);die;
			// $nogroup = sprintf("%02d", $key+1);

			// foreach($selectUser as $val){
			// 	if(empty($userlist))
			// 		$userlist .= $val['USER_ID'];
			// 	else
			// 		$userlist .= ', '.$val['USER_ID'];
			// }

			$arrTraType     = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);

			$dataArr[] = array(	
								'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
								'CURRENCY' => $row['CCY_BOUNDARY'],
								'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN'])." - ".Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
								'GROUP_NAME' => $row['POLICY'],
								'USERS' => $userlist);
		}

		$this->view->dataBoundary = $dataArr;
		$this->view->cust_id = $cust_id;
		Application_Helper_General::writeLog('VABG','View Approver Boundary Group');

	}
}
