<?php
class mapping_AddController extends Application_Main {


	public function indexAction(){
		
		$this->_helper->layout()->setLayout('newlayout');

		$params 	= $this->_request->getParams();
	
		$filter = array(
			'*'	=> array('StringTrim','StripTags'),
		);
		
		$options = array('allowEmpty' => FALSE);
		
		$validators = array(
			'tag'			=> array(),
			'text'			=> array(),
			'mappingto'		=> array(),
			'desc'			=> array()
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		if ($filter->isValid() && isset($_POST['submit'])){
		
			$this->_db->beginTransaction();
			try{
				$tableName = 'M_MAPPING';
				$arrData = array(
					'TAG' => $filter->getEscaped('tag'),
					'TEXT' => $filter->getEscaped('text'),
					'MAPPING_TO' => $filter->getEscaped('mappingto'),
					'DESCRIPTION' => $filter->getEscaped('desc'),
				);
				$this->_db->insert($tableName, $arrData);
				
				$this->_db->commit();
				$this->_redirect('mapping/index');
			}catch(Exception $e){
				$this->_db->rollBack();
				$error = $e->getMessage();
			}
		}else{
			$error = $filter->getMessages();
		}
	}

	public function tagautocompleteAction(){

		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

	    $tag = $this->_getParam('tag');

	    $selectTag = $this->_db->select()
	    			 ->distinct()
					 ->from(array('T' => 'M_MAPPING'), 'T.TAG')
					 ->where("T.TAG LIKE "."'%".$tag."%'");

		$dataTag = $this->_db->fetchAll($selectTag);

		if (!empty($dataTag)) { ?>
				<?php foreach ($dataTag as $key => $value) { ?>
					
					<div class="tagSuggestion" onClick="selectTag('<?php echo $value["TAG"]; ?>');"><?php echo  $value["TAG"]; ?></div>

				<?php } ?>
		<?php } 
	}	
}

?>
