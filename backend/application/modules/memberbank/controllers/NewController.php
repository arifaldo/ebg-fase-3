<?php

require_once 'Zend/Controller/Action.php';

class Memberbank_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');
	    $this->setbackURL('/nostrobank/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$this->view->report_msg = array();

    	$model = new nostrobank_Model_Nostrobank();
	    $this->view->ccyArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCCYList(),'CCY_ID','CCY_ID'));

		if($this->_request->isPost() )
		{
			$filters = array(
			                 'ccy'     => array('StringTrim','StripTags','StringToUpper'),
							 'country_name' => array('StringTrim','StripTags','StringToUpper'),
							 'country_code' => array('StringTrim','StripTags','StringToUpper'),
							 'benef_bank_name'    => array('StringTrim','StripTags','StringToUpper'),
							 'bank_code'    => array('StringTrim','StripTags','StringToUpper'),
							 'nostro_name'     => array('StringTrim','StripTags','StringToUpper'),
							 'nostro_swiftcode'=> array('StringTrim','StripTags','StringToUpper'),
							);

			$validators = array(
			                    'ccy' => array('NotEmpty',
													 'messages' => array(
																         $this->language->_('Can not be empty'),
			                                                             )
													),
								'country_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>128)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Data too long (max 128 chars)')
																         )
														),
								'country_code' => array('NotEmpty',
														'messages' => array(
																$this->language->_('Can not be empty'),
														)
								),
								'benef_bank_name'      => array('NotEmpty',
													 //new Zend_Validate_StringLength(array('max'=>8)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		//$this->language->_( 'Data too long (max 8 chars)')
																         )
														),
								'bank_code'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>8)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Bank Code too long (max 8 chars)')
																         )
														),
								'nostro_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>50)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Data too long (max 50 chars)')
																         )
														),
								'nostro_swiftcode' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>11)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																		 $this->language->_( 'Data too long (max 11 chars)')
			                                                             )
													),
							   );


			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);


			if($zf_filter_input->isValid())
			{
// 				$checkUnique = $model->checkNostroBank($zf_filter_input->ccy,$zf_filter_input->bank_code);
// 				print_r('here');die;
// 				if($checkUnique == true){
					$content = array(
									'CCY' 	 => $zf_filter_input->ccy,
									'NOSTRO_NAME' 	 => $zf_filter_input->nostro_name,
									'NOSTRO_SWIFTCODE' => $zf_filter_input->nostro_swiftcode,
									'COUNTRY_CODE' => $zf_filter_input->country_code,
									'COUNTRY_NAME'	 => $zf_filter_input->country_name,
									'BANK_CODE' => $zf_filter_input->bank_code,
									'BANK_NAME' => $zf_filter_input->benef_bank_name,
	 								'CREATED'  => date('Y-m-d H:i:s'),
	 								'CREATEDBY' 	 => $this->_userIdLogin
							       );

					try
					{
// 						print_r($content);die;
						//-------- insert --------------
						$this->_db->beginTransaction();

						$this->_db->insert('M_MEMBER_BANK',$content);

						// $this->_db->commit();
						$id = $this->_db->lastInsertId();
						Application_Helper_General::writeLog('NBAD','Add Member Bank. CCY : ['.$zf_filter_input->ccy.'], Beneficiary Bank : ['.$zf_filter_input->benef_bank.']');
						$this->view->success = true;
						$this->view->report_msg = array();

						// $this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();

						foreach(array_keys($filters) as $field)
							$this->view->$field = $zf_filter_input->getEscaped($field);

						$errorMsg = 'exception';
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					}

					$this->view->notunique = false;
// 				}
// 				else{
// 					$this->view->notunique = true;
// 					foreach(array_keys($filters) as $field)
// 							$this->view->$field = $zf_filter_input->getEscaped($field);
// 				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->success = false;
		        $this->view->notunique = false;
                $this->view->report_msg = $errorArray;
			}
		}

		Application_Helper_General::writeLog('NBAD','Add Member Bank');
	}

}
