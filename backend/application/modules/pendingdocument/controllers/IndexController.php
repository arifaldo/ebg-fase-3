<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class pendingdocument_IndexController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  // public function indexAction()
  // {
  //   $this->_helper->_layout->setLayout('newlayout');

  //   $settings = new Settings();
  //   $system_type = $settings->getSetting('system_type');
  //   $this->view->systemType = $system_type;

  //   $conf = Zend_Registry::get('config');
  //   $this->_bankName = $conf['app']['bankname'];
  //   $this->view->masterbankname = $this->_bankName;

  //   $csv = $this->_getParam('csv');
  //   $pdf = $this->_getParam('pdf');

  //   $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
  //   Application_Helper_General::writeLog('PDBG','View Insurance Counter Guarantee Pending List');

  //   $fields = array(
  //     /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
  //                                  'label' => $this->language->_('Alias Name'),
  //                                  'sortable' => true),*/
  //     'regno'     => array(
  //       'field'    => 'BG_NUMBER',
  //       'label'    => $this->language->_('Reg No# /  BG No#'),
  //     ),
  //     'subject'     => array(
  //       'field'    => 'PS_FIELDNAME',
  //       'label'    => $this->language->_('Nomor Ijin Prinsip / Nomor Kontra Garansi'),
  //     ),
  //     'principal'     => array(
  //       'field'    => 'CUST_NAME',
  //       'label'    => $this->language->_('Asuransi'),
  //     ),
  //     'branch'     => array(
  //       'field'    => 'BRANCH_NAME',
  //       'label'    => $this->language->_('Cabang Asuransi'),
  //     ),
  //     'bgissued'     => array(
  //       'field'    => 'BG_ISSUED',
  //       'label'    => $this->language->_('Tanggal Terbit'),
  //     ),
  //     'bgclaimdate'     => array(
  //       'field'    => 'BG_CLAIM_DATE',
  //       'label'    => $this->language->_('Tenggat Dokumen'),
  //     ),



  //   );

  //   $displayDateFormat  			= $this->_dateDisplayFormat;


  //   $filterlist = array('BG Number','Principle', 'Principle Agreement Number', 'Counter Guarantee Number', 'Insurance Name','START_DATE','Claim Date');
  //   $dataParamValue = array();

  //   $this->view->filterlist = $filterlist;

  //   $filterArr = array(
  //     'PS_FIELDNAME'  => array('StringTrim', 'StripTags'),
  //     'CUST_NAME'  => array('StringTrim', 'StripTags'),
  //     'BG_NUMBER'   => array('StringTrim', 'StripTags'),
  //     'BRANCH_NAME'   => array('StringTrim', 'StripTags'),
  // 	'changesCreatedFrom' => array('StringTrim', 'StripTags'),
  // 	'changesCreatedTo'	=> array('StringTrim', 'StripTags'),
  //   );

  //   $startDateFrom = (Zend_Date::isDate($dataParamValue['START_DATE'], $displayDateFormat)) ?
  // 		new Zend_Date($dataParamValue['START_DATE'], $displayDateFormat) :
  // 		false;
  // 	$startDateTo 	= (Zend_Date::isDate($dataParamValue['START_DATE_END'], $displayDateFormat)) ?
  // 		new Zend_Date($dataParamValue['START_DATE_END'], $displayDateFormat) :
  // 		false;

  //     if ($startDateFrom) {

  //       $this->view->changesCreatedFrom = $startDateFrom->toString($displayDateFormat);
  //     }
  //     if ($startDateTo)
  //       $this->view->changesCreatedTo   = $startDateTo->toString($displayDateFormat);
  //   // print_r($filterArr);die;
  //   // if POST value not null, get post, else get param ,"SOURCE_ACCOUNT_NAME","BENEFICIARY_ACCOUNT","BENEFICIARY_ACCOUNT_NAME","TRANSACTION_ID","PS_CCY"
  //   $dataParam = array('PS_FIELDNAME', 'CUST_NAME', 'BG_NUMBER', 'BRANCH_NAME');
  //   $dataParamValue = array();
  //   foreach ($dataParam as $dtParam) {

  //     // print_r($dtParam);die;
  //     if (!empty($this->_request->getParam('wherecol'))) {
  //       $dataval = $this->_request->getParam('whereval');
  //       foreach ($this->_request->getParam('wherecol') as $key => $value) {
  //         if ($dtParam == $value) {
  //           if (!empty($dataParamValue[$dtParam])) {
  //             $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
  //           }
  //           $dataParamValue[$dtParam] = $dataval[$key];
  //         }
  //       }
  //     }

  //     // $dataPost = $this->_request->getPost($dtParam);
  //     // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
  //   }


  //   $options = array('allowEmpty' => true);
  //   $validators = array(
  //     'PS_FIELDNAME'        => array(),
  //     'CUST_NAME'        => array(),
  //     // 'SOURCE_ACCOUNT'     => array(array('InArray', array('haystack' => array_keys($optarrAccount)))),
  //     'BG_NUMBER'     => array(),
  //     'BRANCH_NAME'     => array()

  //   );

  //   $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

  //   // print_r($fCreatedEnd);die;


  //   $subject    = $zf_filter->getEscaped('PS_FIELDNAME');
  //   $status    = $zf_filter->getEscaped('BG_STATUS');
  //   $fbgnumb    = $zf_filter->getEscaped('BG_NUMBER');
  //   $branchname     = $zf_filter->getEscaped('BRANCH_NAME');
  //   $custname    = $zf_filter->getEscaped('CUST_NAME');





  //   if ($filter == NULL && $clearfilter != 1) {
  //     //      $fUpdatedStart  = date("d/m/Y");
  //     //      $fUpdatedEnd  = date("d/m/Y");

  //     // echo "f0 c1";
  //   } else {
  //   }

  //   //$whereIn = [7, 20, 21, 22, 23];
  //   $config        = Zend_Registry::get('config');
  //   $BgType     = $config["bg"]["status"]["desc"];
  //   $BgCode     = $config["bg"]["status"]["code"];

  //   $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

  //   $this->view->arrStatus = $arrStatus;

  //   $arrChangeType = array(
  //     '0' => 'New',
  //     //'2' => 'Indirect',
  //     '1' => 'Amendment Changes',
  //     '2' => 'Amendment Draft'
  //   );

  //   $this->view->arrChangeType = $arrChangeType;

  //   $arrWarrantyType = array(
  //     '1' => 'Full Cover',
  //     //'2' => 'Indirect',
  //     '2' => 'Line Facility',
  //     '3' => 'Insurance'
  //   );

  //   $this->view->arrWarrantyType = $arrWarrantyType;


  //   //print 
  //   $caseStatus = "(CASE BG_STATUS ";
  //   foreach($arrStatus as $key=>$val)
  //   {
  //     $caseStatus .= " WHEN ".$key." THEN '".$val."'";
  //   }
  //   $caseStatus .= " END)";

  //   $caseCounter = "(CASE COUNTER_WARRANTY_TYPE ";
  //   foreach($arrWarrantyType as $key=>$val)
  //   {
  //     $caseCounter .= " WHEN ".$key." THEN '".$val."'";
  //   }
  //   $caseCounter .= " END)";

  //   $caseType = "(CASE CHANGE_TYPE ";
  //   foreach($arrChangeType as $key=>$val)
  //   {
  //     $caseType .= " WHEN ".$key." THEN '".$val."'";
  //   }
  //   $caseType .= " END)";


  //   $select = $this->_db->select()
  //     ->from(array('A' => 'T_BANK_GUARANTEE'),
  //        array(
  //          'A.BG_REG_NUMBER',
  //          'A.BG_NUMBER',
  //          'A.BG_SUBJECT',
  //          'C.CUST_NAME',
  //          'C.CUST_ID',
  //          'A.RECIPIENT_NAME',
  //          'B.BRANCH_NAME',
  //          'A.BG_INSURANCE_CODE',
  //          'A.COUNTER_WARRANTY_TYPE',
  //          'A.CHANGE_TYPE',
  //          'A.TIME_PERIOD_END',
  //          'A.BG_STATUS',
  //          'D.PS_FIELDNAME',
  //          'D.PS_FIELDVALUE',
  //          'A.BG_ISSUED',
  //          'A.BG_CLAIM_DATE'
  //          ))
  //     ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
  //     ->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = A.BG_INSURANCE_CODE', array('CUST_NAME','CUST_ID'))
  //     ->joinLeft(array('D' => 'T_BANK_GUARANTEE_DETAIL'), 'D.BG_NUMBER = A.BG_NUMBER', array('PS_FIELDNAME','PS_FIELDVALUE'));
  //   //  ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
  //     //->where('A.BG_STATUS NOT IN (?)', '15','16');
  //     //->order('BG_CREATED DESC');
  //   $auth = Zend_Auth::getInstance()->getIdentity();
  // 	if($auth->userHeadQuarter == "NO"){
  // 		$select->where('B.ID = ?', $auth->userBranchId); // Add Bahri
  // 	}


  //   // if ($status) {
  //     $select->where("BG_STATUS = ? ", 15);
  //     $select->where("COUNTER_WARRANTY_TYPE = ? ", 3);
  //     $select->where("CGINS_STATUS = ? ", 0);
  //   // }

  //   if ($subject) {
  //     $subjectArr = explode(',', $subject);
  //     //  $select->where("UPPER(BG_SUBJECT)  like ?",'%'.$subjectArr.'%' );

  //     $select->where("BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
  //   }

  //   if ($fbgnumb)
  //   // {  $select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and SOURCE_ACCOUNT = ?) > 0", (string)$fAcctsrc);   }
  //   { //$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fAcctsrc.'%'));  
  //     $fAcctsrcArr = explode(',', $fbgnumb);
  //     $select->where("BG_NUMBER  LIKE ?", '%' . $fAcctsrcArr . '%');
  //   } // ACCSRC ?????

  //   if ($branchname) { //$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBenefsrc.'%'));  
  //     $fBenefsrcArr = explode(',', $branchname);
  //     $select->where("BG_NUMBER  LIKE ?", '%' . $fBenefsrcArr . '%');
  //   }


  //   $txt = $this->_getParam('txt');
  //   if($txt || $csv || $pdf || $this->_request->getParam('print'))
  //   {

  //     $arr = $this->_db->fetchAll($select);

  //     foreach($arr as $key => $val)
  //     {

  //       unset($arr[$key]['TIME_PERIOD_END']);
  //       unset($arr[$key]['BG_REG_NUMBER']);
  //       unset($arr[$key]['BG_INSURANCE_CODE']);
  //       unset($arr[$key]['COUNTER_WARRANTY_TYPE']);
  //       unset($arr[$key]['CHANGE_TYPE']);
  //       unset($arr[$key]['BG_STATUS']);
  //     }

  //     $header = Application_Helper_Array::simpleArray($fields, 'label');
  //     if($csv)
  //     {

  //       $this->_helper->download->csv($header,$arr,null,$this->language->_('Pending Document List')); 
  //       Application_Helper_General::writeLog('PDBG','Download CSV Pending Document List');
  //     }
  //     else if($pdf)
  //     {
  //       foreach($arr as $key => $val)
  //       {
  //         unset($arr[$key]['TIME_PERIOD_END']);
  //         unset($arr[$key]['BG_REG_NUMBER']);
  //         unset($arr[$key]['BG_INSURANCE_CODE']);
  //         unset($arr[$key]['COUNTER_WARRANTY_TYPE']);
  //         unset($arr[$key]['CHANGE_TYPE']);
  //         unset($arr[$key]['BG_STATUS']);
  //       }
  //       $this->_helper->download->pdf($header,$arr,null,$this->language->_('Customer List BG')); 
  //       Application_Helper_General::writeLog('PDBG','Download PDF Pending Document List');
  //     }
  //     else if($this->_request->getParam('print') == 1){

  //       $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Customer List BG', 'data_header' => $fields));
  //     }

  //   if($txt)
  //   {

  //         Application_Helper_General::writeLog('PDBG','Download TXT Transaction Report');
  //   $arrTxt = $this->_db->fetchAll($select);


  // 	$txtData = array();
  // 	foreach($arrTxt as $ky => $vl){
  // 		/*$selectcity = $this->_db->select()
  //                       ->from(array('M_CITY'),array('*'))
  // 				  ->where('CITY_NAME = ?',$vl['RECIPIENT_CITY']);
  // 		$city = $this->_db->fetchRow($selectcity);
  // 		if(empty($city)){
  // 			$citycode = $city['CITY_CODE'];
  // 		}else{
  // 			$citycode = '0300';
  // 		}*/

  // 		$txtData[$ky]['FLAG'] = 'D';
  // 		$txtData[$ky]['BG_NUMBERA'] = $vl['BG_NUMBER'].'A';
  // 		$txtData[$ky]['BG_NUMBER'] = $vl['BG_NUMBER'];
  // 		$txtData[$ky]['CUST_NAME'] = $vl['CUST_NAME'];
  // 		$txtData[$ky]['SEGMENT'] = 'F05';
  // 		$txtData[$ky]['AGUNAN'] = '1';
  // 		$txtData[$ky]['KODEAGUNAN'] = '252';
  // 		$txtData[$ky]['PERINGKATAGUNAN'] = '';
  // 		$txtData[$ky]['KODEPERINGKAT'] = '';
  // 		$txtData[$ky]['JENISPENGIKAT'] = '99';
  // 		$txtData[$ky]['TGLPENGIKAT'] = str_replace('-','',$vl['TIME_PERIOD_START']);
  // 		$txtData[$ky]['RECIPIENT_NAME'] = $vl['RECIPIENT_NAME'];
  // 		$txtData[$ky]['GUARANTEE_TRANSACTION'] = $vl['GUARANTEE_TRANSACTION'];
  // 		$txtData[$ky]['RECIPIENT_ADDRES'] = $vl['RECIPIENT_ADDRES'];
  // 		//$txtData[$ky]['CITY_CODE'] = $$citycode;
  // 		$txtData[$ky]['BG_AMOUNT'] = $vl['BG_AMOUNT'];
  // 		$txtData[$ky]['BG_AMOUNTLK'] = $vl['BG_AMOUNT'];
  // 		$txtData[$ky]['DATELK'] = str_replace('-','',$vl['TIME_PERIOD_START']);
  // 		$txtData[$ky]['NILAIPENILAI'] = '';
  // 		$txtData[$ky]['NAMAPENILAI'] = '';
  // 		$txtData[$ky]['DATEPENILAI'] = '';
  // 		$txtData[$ky]['PARIPASUSTATS'] = 'T';
  // 		$txtData[$ky]['PARIPASUPECENT'] = '';
  // 		$txtData[$ky]['STATUSKREDIT'] = 'T';
  // 		$txtData[$ky]['STATUSINSURANCE'] = 'Y';
  // 		$txtData[$ky]['DESC'] = '';
  // 		$txtData[$ky]['KACAB'] = '70';
  // 		$txtData[$ky]['OPERASIDATA'] = 'C';


  // 	}

  //         $this->_helper->download->txtblank(null,$txtData,null,'Bank Guarantee List');
  //       }
  //   }

  //   $select = $select->query()->fetchAll();
  //   $this->paging($select);


  //   $conf = Zend_Registry::get('config');


  //   $this->view->bankname = $conf['app']['bankname'];


  //   // $arrStatus = array('1' => 'Waiting for review',
  //   //                     '2' => 'Waiting for approve',
  //   //                     '3' => 'Waiting to release',
  //   //                     '4' => 'Waiting for bank approval',
  //   //                     '5' => 'Issued',
  //   //                     '6' => 'Expired',
  //   //                     '7' => 'Canceled',
  //   //                     '8' => 'Claimed by applicant',
  //   //                     '9' => 'Claimed by recipient',
  //   //                     '10' => 'Request Repair',
  //   //                     '11' => 'Reject',
  //   //                   );





  //   $arrType = array(
  //     1 => 'Standard',
  //     2 => 'Custom'
  //   );

  //   $arrLang = array(
  //     1 => 'Indonesian',
  //     2 => 'English',
  //     3 => 'Bilingual'
  //   );
  //   $this->view->langArr = $arrLang;
  //   $this->view->formatArr = $arrType;
  //   $this->view->fields = $fields;



  //   if (!empty($dataParamValue)) {

  //     $this->view->createdStart = $dataParamValue['BG_CREATED'];
  //     $this->view->createdEnd = $dataParamValue['BG_CREATED_END'];
  //     $this->view->efdateStart = $dataParamValue['BG_DATEFROM'];
  //     $this->view->efdateEnd = $dataParamValue['BG_DATEFROM_END'];



  //     unset($dataParamValue['BG_DATEFROM_END']);
  //     unset($dataParamValue['BG_CREATED_END']);

  //     //var_dump($dataParamValue);
  //     foreach ($dataParamValue as $key => $value) {
  //       //foreach($value as $ss => $vs){

  //       $duparr = explode(',', $value);
  //       if (!empty($duparr)) {

  //         foreach ($duparr as $ss => $vs) {
  //           $wherecol[] = $key;
  //           $whereval[] = $vs;
  //         }
  //       } else {
  //         $wherecol[] = $key;
  //         $whereval[] = $value;
  //       }

  //       //}


  //     }
  //     //var_dump($wherecol);
  //     //var_dump($whereval);die;

  //     $this->view->wherecol     = $wherecol;
  //     $this->view->whereval     = $whereval;
  //   }

  // }

  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');

    $auth = Zend_Auth::getInstance()->getIdentity();
    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if (count($temp) > 1) {
      if ($temp[0] == 'F' || $temp[0] == 'S') {
        if ($temp[0] == 'F')
          $this->view->error = 1;
        else
          $this->view->success = 1;
        $msg = '';
        unset($temp[0]);
        foreach ($temp as $value) {
          if (!is_array($value))
            $value = array($value);
          $msg .= $this->view->formErrors($value);
        }
        // $this->view->report_msg = $msg;
      }
    }

    // $userId = $this->_custIdLogin;
    // var_dump($userId); die;

    $select = array();
    $displayDateFormat        = $this->_dateDisplayFormat;
    $databaseSaveFormat       = $this->_dateDBFormat;
    $this->view->dateJqueryFormat    = $this->_dateJqueryFormat;

    //$privi = Zend_Registry::get('privilege');	// get privi ID
    $privi = $this->_priviId;

    //get filtering param
    $filterArr = array(
      'filter'      => array('StringTrim', 'StripTags'),
      'BG_NUMBER'       => array('StringTrim', 'StripTags'),
      // 'PRINCIPLE' 	  		=> array('StringTrim', 'StripTags'),
      'PRINCIPLE_AGREEMENT_NUMBER'       => array('StringTrim', 'StripTags'),
      // 'COUNTER_GUARANTEE_NUMBER' 			=> array('StringTrim', 'StripTags'),
      'INSURANCE_NAME'     => array('StringTrim', 'StripTags'),
      'BRANCH_NAME'     => array('StringTrim', 'StripTags'),
      'START_DATE'   => array('StringTrim', 'StripTags'),
      'START_DATE_END'   => array('StringTrim', 'StripTags'),
      'CLAIM_DATE'     => array('StringTrim', 'StripTags'),
      'CLAIM_DATE_END' => array('StringTrim', 'StripTags'),
    );

    $validator = array(
      'filter'     => array(),
      'BG_NUMBER'    => array(),
      // 'PRINCIPLE'  	=> array(),
      'PRINCIPLE_AGREEMENT_NUMBER'      => array(),
      // 'COUNTER_GUARANTEE_NUMBER'		=> array(),
      'INSURANCE_NAME'        => array(),
      'BRANCH_NAME'        => array(),
      'START_DATE'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'START_DATE_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'CLAIM_DATE'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'CLAIM_DATE_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    );

    // $dataParam = array('BG_NUMBER', 'PRINCPLE', 'PRINCIPLE_AGREEMENT_NUMBER', 'COUNTER_GUARANTEE_NUMBER', 'INSURANCE_NAME');
    $dataParam = array('BG Number', 'Principle Agreement Number', 'Insurance Name', 'Branch Name', 'START_DATE', 'CLAIM_DATE');
    $dataParamValue = array();

    if ($this->_request->getParam('wherecol')) {
      $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
      $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
      // print_r($dataParam);die;

      // print_r($output);die;
      // print_r($this->_request->getParam('wherecol'));
      foreach ($dataParam as $no => $dtParam) {

        if (!empty($this->_request->getParam('wherecol'))) {
          $dataval = $this->_request->getParam('whereval');
          // print_r($dataval);
          $order = 0;
          foreach ($this->_request->getParam('wherecol') as $key => $value) {
            if ($value == "BG_NUMBER") {
              $order--;
            }
            if ($dtParam == $value) {
              $dataParamValue[$dtParam] = $dataval[$order];
            }
            $order++;
          }
        }
      }
    }

    // print_r($dataParamValue);
    // die;
    // print_r($this->_request->getParam('whereval'));die;
    // if (!empty($this->_request->getParam('sgdate'))) {
    // 	$sgarr = $this->_request->getParam('sgdate');
    // 	$dataParamValue['PS_SUGGESTED'] = $sgarr[0];
    // 	$dataParamValue['PS_SUGGESTED_END'] = $sgarr[1];
    // }

    $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
    $filter     = $this->_getParam('filter');

    $this->setbackURL();

    $fields = array(
      'bgNumber'    => array(
        'field' => 'BG_NUMBER',
        'label' => $this->language->_('BG Number/Principle'),
        'sortable' => true
      ),
      'principleNumber'  => array(
        'field' => 'PRIN_GRANTED_DATE',
        'label' => $this->language->_('Principle Agreement Number/Counter Guarantee Number'),
        'sortable' => true
      ),
      'insurance'  => array(
        'field' => 'CUST_NAME',
        'label' => $this->language->_('Insurance Name'),
        'sortable' => true
      ),
      'branch'  => array(
        'field' => 'BRANCH_NAME',
        'label' => $this->language->_('Branch Name'),
        'sortable' => true
      ),
      'startdate' => array(
        'field' => 'BG_ISSUED',
        'label' => $this->language->_('Tanggal Terbit'),
        'sortable' => true
      ),
      'claimdate' => array(
        'field' => 'BG_CG_DEADLINE',
        'label' => $this->language->_('Tanggal Tenggat'),
        'sortable' => true
      ),
    );


    // $filterlist = array('BG Number', 'Principle Agreement Number', 'Insurance Name', 'Branch Name', 'START_DATE', 'CLAIM_DATE');
    $filterlist = array('BG Number', 'Principle Agreement Number', 'Insurance Name', 'Branch Name');

    $this->view->filterlist = $filterlist;

    //get page, sortby, sortdir
    $page    = $this->_getParam('page');
    $sortBy  = $this->_getParam('sortby', 'startdate');
    $sortDir = $this->_getParam('sortdir', 'desc');

    //validate parameters before passing to view and query
    $page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
    $sortBy = (Zend_Validate::is(
      $sortBy,
      'InArray',
      array(array_keys($fields))
    )) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    //Zend_Debug::dump($sortBy);die;
    $sortDir = (Zend_Validate::is(
      $sortDir,
      'InArray',
      array('haystack' => array('asc', 'desc'))
    )) ? $sortDir : 'asc';
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;

    if ($filter == TRUE) {
      $bgnumber   = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));
      $principle     = html_entity_decode($zf_filter->getEscaped('PRIN_GRANTED_DATE'));
      $cust_name     = html_entity_decode($zf_filter->getEscaped('CUST_NAME'));
      $branch     = html_entity_decode($zf_filter->getEscaped('Branch Name'));
      // print_r($cust_name);
      //       	die('here');
      $startDateFrom = (Zend_Date::isDate($dataParamValue['START_DATE'], $displayDateFormat)) ?
        new Zend_Date($dataParamValue['START_DATE'], $displayDateFormat) :
        false;
      $startDateTo   = (Zend_Date::isDate($dataParamValue['START_DATE_END'], $displayDateFormat)) ?
        new Zend_Date($dataParamValue['START_DATE_END'], $displayDateFormat) :
        false;

      $claimDateFrom = (Zend_Date::isDate($dataParamValue['CLAIM_DATE'], $displayDateFormat)) ?
        new Zend_Date($dataParamValue['CLAIM_DATE'], $displayDateFormat) :
        false;
      $claimDateTo   = (Zend_Date::isDate($dataParamValue['CLAIM_DATE_END'], $displayDateFormat)) ?
        new Zend_Date($dataParamValue['CLAIM_DATE_END'], $displayDateFormat) :
        false;




      $this->view->bgnumber   = $bgnumber;
      $this->view->principle     = $principle;
      $this->view->cust_name     = $cust_name;
      $this->view->branch     = $branch;

      if ($startDateFrom) {

        $this->view->changesCreatedFrom = $startDateFrom->toString($displayDateFormat);
      }
      if ($startDateTo) {
        $this->view->changesCreatedTo   = $startDateTo->toString($displayDateFormat);
      }
      if ($claimDateFrom) {

        $this->view->changesCreatedFrom = $claimDateFrom->toString($displayDateFormat);
      }
      if ($claimDateTo) {
        $this->view->changesCreatedTo   = $claimDateTo->toString($displayDateFormat);
      }
    }


    if (!empty($privi))    // kalo privilege is empty, gak bisa generate report, for all data
    {


      //compose query (use filtering and sorting)
      // $select = $this->_db->select()
      // 	->from(array('G' => 'T_GLOBAL_CHANGES'),
      // 	array( '*',
      // 	'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)))
      // 	// ->where('BG_CLAIM_DATE = '.$this->_db->quote('B')) // B : Backend / F : Frontend. ############# REMOVE KARENA ADA CREATE USER DARI FO !??!###############
      // 	->where("G.MODULE IN (".$whPriviID.")");

      $selectbo = $this->_db->select()
        ->from(
          array('A' => 'T_BANK_GUARANTEE'),
          array(
            'A.BG_REG_NUMBER',
            'A.BG_NUMBER',
            'A.BG_SUBJECT',
            'C.CUST_NAME',
            'C.CUST_ID',
            'A.RECIPIENT_NAME',
            'B.BRANCH_NAME',
            'A.BG_INSURANCE_CODE',
            'A.COUNTER_WARRANTY_TYPE',
            'A.CHANGE_TYPE',
            'A.TIME_PERIOD_END',
            'A.BG_STATUS',
            //  'D.PS_FIELDNAME',
            //  'D.PS_FIELDVALUE',
            'A.BG_ISSUED',
            'A.BG_CG_DEADLINE'
          )
        )
        ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
        ->joinLeft(["D" => "T_BANK_GUARANTEE_DETAIL"], "A.BG_REG_NUMBER = D.BG_REG_NUMBER AND D.PS_FIELDNAME = 'Principle Agreement Number'", ["PAN" => "D.PS_FIELDVALUE"])
        ->joinLeft(["E" => "T_BANK_GUARANTEE_DETAIL"], "A.BG_REG_NUMBER = E.BG_REG_NUMBER AND E.PS_FIELDNAME = 'Counter Guarantee Number'", ["CGN" => "E.PS_FIELDVALUE"])
        ->joinLeft(["F" => "T_BANK_GUARANTEE_DETAIL"], "A.BG_REG_NUMBER = F.BG_REG_NUMBER AND F.PS_FIELDNAME = 'Insurance Name'", ["INS_CODE" => "F.PS_FIELDVALUE"])
        ->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = F.PS_FIELDVALUE', array('CUST_NAME', 'CUST_ID'));

      // var_dump($selectbo->query()->fetchAll());
      // die();

      // ->joinLeft(array('D' => 'T_BANK_GUARANTEE_DETAIL'), 'D.BG_NUMBER = A.BG_NUMBER', array('PS_FIELDNAME','PS_FIELDVALUE'));

    }

    array('BG Number', 'Principle Agreement Number', 'Insurance Name', 'Branch Name', 'START_DATE', 'CLAIM_DATE');

    if ($this->_request->getParam("filter")) {
      //print_r($startDateFrom);die;
      //where clauses
      if ($dataParamValue["BG Number"]) {
        $selectbo->where("UPPER(A.BG_NUMBER) LIKE " . $this->_db->quote('%' . $dataParamValue["BG Number"] . '%'));
      }

      if ($dataParamValue["Principle Agreement Number"]) {
        $selectbo->where("UPPER(D.PS_FIELDVALUE) LIKE " . $this->_db->quote('%' . $dataParamValue["Principle Agreement Number"] . '%'));
      }

      if ($dataParamValue["Insurance Name"]) {
        $selectbo->where("UPPER(C.CUST_NAME) LIKE " . $this->_db->quote('%' . $dataParamValue["Insurance Name"] . '%'));
      }

      if ($dataParamValue["Branch Name"]) {
        // $selectbo->where("UPPER(B.BRANCH_NAME) LIKE " . $this->_db->quote('%' . $dataParamValue["Branch Name"] . '%'));
        $selectbo->where("UPPER(B.ID) LIKE " . $this->_db->quote('%' . $dataParamValue["Branch Name"] . '%'));
      }

      if ($startDateFrom) {
        $selectbo->where("DATE(A.BG_ISSUED) >= DATE(" . $this->_db->quote($startDateFrom->toString($databaseSaveFormat)) . ")");
      }

      if ($startDateTo) {
        $selectbo->where("DATE(A.BG_ISSUED) <= DATE(" . $this->_db->quote($startDateTo->toString($databaseSaveFormat)) . ")");
      }

      if ($claimDateFrom) {
        $selectbo->where("DATE(A.BG_CG_DEADLINE) >= DATE(" . $this->_db->quote($claimDateFrom->toString($databaseSaveFormat)) . ")");
      }

      if ($claimDateTo) {
        $selectbo->where("DATE(A.BG_CG_DEADLINE) <= DATE(" . $this->_db->quote($claimDateTo->toString($databaseSaveFormat)) . ")");
      }
    }

    $selectbo->where("BG_STATUS = ? ", 15);
    $selectbo->where("COUNTER_WARRANTY_TYPE = ? ", 3);
    $selectbo->where("CGINS_STATUS = ? ", 0);

    $result = $this->_db->fetchAll($selectbo);


    // 	        print_r($select->query());die;
    //echo $select;die();

    // 			Zend_Debug::dump($select->__toString());die;



    $this->view->fields     = $fields;
    $this->view->paginatorParam = $result;

    $this->paging($result);

    $csv = $this->_getParam('csv');
    if ($this->_request->isPost()) {
      if ($csv) {

        $array_header = [
          $this->language->_('No'),
          $this->language->_('BG Number / Prinsipal'),
          $this->language->_(' Principle Agreement Number/Counter Guarantee Number'),
          $this->language->_('Insurance Name'),
          $this->language->_('Branch Name'),
          $this->language->_('Tanggal Terbit'),
          $this->language->_('Tanggal Tenggat'),
        ];

        $csv_data = [];
        foreach ($result as $key => $value) {

          $temp = [
            $key + 1,
            $value["BG_REG_NUMBER"] . " / " . $value["CUST_NAME"],
            $value["PAN"] . " / " . $value["CGN"],
            $value['CUST_NAME'],
            $value['BRANCH_NAME'],
            date("d M Y", strtotime($value["BG_ISSUED"])),
            date("d M Y", strtotime($value['BG_CG_DEADLINE']))
          ];

          array_push($csv_data, $temp);
        }

        Application_Helper_General::writeLog('SBGP', 'Download CSV Pending Document List');
        $this->_helper->download->csv($array_header, $csv_data, null, $this->language->_('Pending Document List') . " - " . date("dMYHis"));
      }
    }

    if (!empty($dataParamValue)) {
      $this->view->sgdateStart = $dataParamValue['START_DATE'];
      $this->view->sgdateEnd = $dataParamValue['START_DATE_END'];

      unset($dataParamValue['START_DATE_END']);
      foreach ($dataParamValue as $key => $value) {
        $wherecol[]  = $key;
        $whereval[] = $value;
      }
      //var_dump($wherecol);die('here');

      // print_r($whereval);die;
    } else {
      $wherecol = array();
      $whereval = array();
    }


    $this->view->wherecol     = $wherecol;
    $this->view->whereval     = $whereval;
  }
}
