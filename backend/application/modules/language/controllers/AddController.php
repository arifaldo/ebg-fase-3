<?php
class language_AddController extends Application_Main {
	public function indexAction(){
		$this->_helper->layout()->setLayout('newlayout');
	
		$model = new language_Model_Index();
		
		
		$sql = $model->getLang();
		$arrLang = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				$arrLang += array($val['ID'] => $val['DESC']);
			}
		}
		$this->view->arrLang = $arrLang;
		
		$sql = $model->getUi();
		// var_dump($sql);die;
		//$arrUi = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				//$arrUi += array($val['ID'] => $val['DESC']);
				$arrUi[$val['ID']] = $val['DESC'];
			}
		}
		$this->view->arrUi = $arrUi;
		
		$params 	= $this->_request->getParams();
		
		foreach($params as $key => $val){
			$this->view->{$key} = $val;
		}
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		
		$options = array('allowEmpty' => FALSE);
		
		$validators = array(
			'ui'			=> 	array(
				'Digits',
				new Zend_Validate_InArray(array_values(array_flip($arrUi))),
			),
			'oriPhrase'		=> array(),
			'traPhrase'		=> array(),
			'traPhrase'		=> array(),
			'lang'			=> 	array(
				'Digits',
				new Zend_Validate_InArray(array_values(array_flip($arrLang))),
			),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);

		if($this->_request->isPost()){

			$this->_db->beginTransaction();

			if (empty($params['ui'])) {
				$error_msg[] = $this->language->_('Interface cannot be left blank').".";
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}elseif (empty($params['oriPhrase'])) {
				$error_msg[] = $this->language->_('Original Phrase cannot be left blank').".";
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}elseif (empty($params['traPhrase'])) {
				$error_msg[] = $this->language->_('Translate cannot be left blank').".";
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}elseif (empty($params['lang'])) {
				$error_msg[] = $this->language->_('Language cannot be left blank').".";
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}

			try{
				$mt = $this->_db->select()
					->from('M_TRANSLATE')
					->where('TEXT = ?', $filter->getEscaped('oriPhrase'))
					->where('M_TRANSLATE_UI_ID = ?',$filter->getEscaped('oriPhrase'))
					->limit(1)
				;
				
				$mt = $this->_db->fetchRow($mt);
				
				$lastUid = 0;
				if(isset($mt['ID'])){
					$lastUid = (int) $mt['ID'];
				}else{
					$tableMt = 'M_TRANSLATE';
					$bindMt = array(
							'M_TRANSLATE_UI_ID' => $filter->getEscaped('ui'),
							'TEXT' => $filter->getEscaped('oriPhrase'),
							'CREATED' => date('Y-m-d H:i:s'),
							'CREATEDBY' => $this->_userIdLogin,
							'UPDATEDBY' => $this->_userIdLogin,
					);

					$this->_db->insert($tableMt, $bindMt);
					
					$lastUid = $this->_db->lastInsertId($tableMt);
				}
				
				$tableMtd = 'M_TRANSLATE_DETAIL';
				$bindMtd = array(
					'M_TRANSLATE_ID' => $lastUid,
					'M_TRANSLATE_LANG_ID' => $filter->getEscaped('lang'),
					'TEXT' => $filter->getEscaped('traPhrase'),
					'CREATED' => date('Y-m-d H:i:s'),
					'CREATEDBY' => $this->_userIdLogin,
					'UPDATEDBY' => $this->_userIdLogin,
					//'WEB' => 'Frontend',
				);
				$this->_db->insert($tableMtd, $bindMtd);
				
				foreach($params as $key => $val){
					$this->view->{$key} = NULL;
				}
				
				$this->_db->commit();


						$model = new language_Model_Index();
						$rawDictionary = $model->getDictionary(array('ui'=>$filter->getEscaped('oriPhrase')));
						
				$tmx = '<?xml version="1.0" ?>
				<!DOCTYPE tmx SYSTEM "tmx14.dtd">
				<tmx version="1.4">
				<header creationtool="CTmx" creationtoolversion="1.0" datatype="winres" segtype="sentence" adminlang="en-us" srclang="en-us" o-tmf="txt">
				</header>
				<body>
					APPENDAREA
				</body>
				</tmx>';
						
						$words = array();
						if (count($rawDictionary)){
							foreach ($rawDictionary as $key => $val){
								if (!(in_array($val['WORD'], $words))){
									$words = array_merge($words, array($val['WORD']));
								}
							}
						}
						
						
						
						$pairWords = array();
						if(count($words)){
							foreach ($words as $word){
								foreach ($rawDictionary as $key => $val){
									if (strcmp($word, $val['WORD']) == 0){
										$lang = explode('_', $val['CODE']);
										$pairWords = array_merge_recursive($pairWords, 
											array(
												$word => array(
													array(
														'lang' => $lang[0],
														'seg' => $val['TEXT'],
													)
												)
											)
										);
									}
								}
							}
						}
						
						$appendText = NULL;
						foreach ($pairWords as $key => $arr){
							$appendText .= '<tu tuid="'.$key.'">'."\r";
							foreach ($arr as $id => $val){
								$appendText .= '<tuv xml:lang="'.$val['lang'].'"><seg>'.$val['seg'].'</seg></tuv>'."\r";
							}
							$appendText .= '</tu>'."\r";
						}
						
						$tmx = strtr($tmx, array('APPENDAREA' => $appendText));

						if($filter->getEscaped('oriPhrase') == '2'){
							$tmxPath = LIBRARY_PATH.'/data/languages/backend/language.tmx';
						}else{
							$tmxPath = LIBRARY_PATH.'/data/languages/frontend/language.tmx';
						}
						//$tmxPath = LIBRARY_PATH.'data/languages/backend/language.tmx';
						
				//		if(file_exists($tmxPath)){
				//			echo'ada';
				//		}
				//		else{
				//			echo'tidak ada';
				//		}
				//		Zend_Debug::dump($tmxPath);die;
						if (file_exists($tmxPath)){
							unlink($tmxPath);
							//file_put_contents(LIBRARY_PATH.'data/languages/backend/language.tmx', $tmx);

							if($filter->getEscaped('oriPhrase') == '2'){
								file_put_contents(LIBRARY_PATH.'/data/languages/backend/language.tmx', $tmx);
							}else{
								file_put_contents(LIBRARY_PATH.'/data/languages/frontend/language.tmx', $tmx);	
							}
							
							
						}

						$files = glob(APPLICATION_PATH.'/../../library/data/cache/language/frontend/*'); // get all file names		
						foreach($files as $file){ // iterate files
						  if(is_file($file))
						    unlink($file); // delete file
						}
						
						Zend_Translate::clearCache();






				// $this->_redirect('language/index');
				$this->setbackURL('/language/index');
				$this->_redirect('/notification/success/index');
			}catch(Exception $e){
				$this->_db->rollBack();
				$error = $e->getMessage();
			}

		}
		
		// if ($filter->isValid() && isset($_POST['submit'])){
		
			
		// }else{
		// 	$error = $filter->getMessages();
		// }
		
		// Zend_Debug::dump($error);
	}	
}
