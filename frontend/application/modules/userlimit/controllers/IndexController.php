<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';


class userlimit_IndexController extends Application_Main{

	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$this->setbackURL('/'.$this->_request->getModuleName().'/index/'); 

	    $moduleDB = 'USF';
	    $model = new userlimit_Model_Userlimit();
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$system_type = $setting->getSetting('system_type');
		$this->view->system_type = $system_type; 
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash; 
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
	  	$cust_id = $this->_custIdLogin;
	  	$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>25)))? strtoupper($cust_id) : '';

	  	$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

		$filterlist = array('USER_ID','USER_FULLNAME','USER_SUGGESTED','USER_UPDATED');
		
		$this->view->filterlist = $filterlist;

	  	
	  	if($cust_id)
	  	{
	  	   $select = $this->_db->select()
	  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME','CUST_STATUS'))
	  	                         ->where('CUST_ID='.$this->_db->quote((string)$cust_id));
	  	                         
	      $result = $this->_db->fetchRow($select);
	      
	      if($result['CUST_ID'] && $result['CUST_STATUS'] == 1)
	      {
	      	$this->view->cust_name = $result['CUST_NAME'];
	      	$this->view->cust_status = $result['CUST_STATUS'];
	      }
	      else{ $cust_id = null; }
	  	}
	  	
	  	if($cust_id)
	  	{
	  		$cust = new Customer($cust_id);
	  		$userList = $cust->getUserList();
	  		$this->view->userlist = $userList;
			$fields = array('userid'   => array('field'    => 'USER_ID',
			                                  'label'    => 'User ID',
			                                  'sortable' => true),

			              'username' => array('field'    => 'USER_FULLNAME',
			                                  'label'    => 'Name',
			                                  'sortable' => true),

			              'last_suggest' => array('field'    => 'USER_SUGGESTED',
			                                  'label'    => 'Latest Suggestion',
			                                  'sortable' => true),

			              'last_suggester' => array('field'    => 'USER_SUGGESTEDBY',
			                                  'label'    => 'Latest Suggester',
			                                  'sortable' => true),

			              'last_approve'   => array('field'    => 'USER_UPDATED',
			                                  'label'    => 'Latest Approval',
			                                  'sortable' => true),

			              'last_approver'   => array('field'    => 'USER_UPDATEDBY',
			                                  'label'    => 'Latest Approver',
			                                  'sortable' => true),
			             );

			$csv = $this->_getParam('csv');
			$page = $this->_getParam('page');
			$sortBy = $this->_getParam('sortby','USER_ID');
			$sortDir = $this->_getParam('sortdir','ASC');
			$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
			$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
			$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

			$filterArr = array('filter' => array('StripTags','StringTrim'),
			                 'USER_ID'    => array('StripTags','StringTrim','StringToUpper'),
			                 'USER_FULLNAME'  => array('StripTags','StringTrim','StringToUpper'),
			                 'USER_SUGGESTED' => array('StripTags','StringTrim'),
			                 'USER_UPDATED' => array('StripTags','StringTrim'),
			                 'USER_SUGGESTED_END' => array('StripTags','StringTrim'),
			                 'USER_UPDATED_END' => array('StripTags','StringTrim')
			                );




			$dataParam = array("USER_ID","USER_FULLNAME");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
			if(!empty($this->_request->getParam('updatedate'))){
				$createarr = $this->_request->getParam('updatedate');
					$dataParamValue['USER_UPDATED'] = $createarr[0];
					$dataParamValue['USER_UPDATED_END'] = $createarr[1];
			}
			

			if(!empty($this->_request->getParam('sugestdate'))){
				$efdatearr = $this->_request->getParam('sugestdate');
					$dataParamValue['USER_SUGGESTED'] = $efdatearr[0];
					$dataParamValue['USER_SUGGESTED_END'] = $efdatearr[1];
			}

			                
			$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
			// $filter    = $zf_filter->getEscaped('filter');
			$this->view->currentPage = $page;
			$this->view->sortBy      = $sortBy;
			$this->view->sortDir     = $sortDir;
			$this->view->user_msg    = $this->_helper->getHelper('FlashMessenger')->getMessages();

			$filter 		= $this->_getParam('filter');
			$filter_clear 	= $this->_getParam('filter_clear');

			$filterSend = array();
			$filterSend['cust_id'] = $cust_id;


			if($filter == TRUE)
			{
				$userid = html_entity_decode($zf_filter->getEscaped('USER_ID'));
				$uname = html_entity_decode($zf_filter->getEscaped('USER_FULLNAME'));
				$suggestStart = $zf_filter->getEscaped('USER_SUGGESTED');
				$suggestEnd = $zf_filter->getEscaped('USER_SUGGESTED_END');
				$approveStart = $zf_filter->getEscaped('USER_UPDATED');
				$approveEnd = $zf_filter->getEscaped('USER_UPDATED_END');

				$this->view->userid = $userid;
				$this->view->uname = $uname;
				$this->view->suggestStart = $suggestStart;
				$this->view->suggestEnd	= $suggestEnd;
				$this->view->approveStart = $approveStart;
				$this->view->approveEnd = $approveEnd;

				if($userid != null){
					$filterSend['userid'] = $userid;
				}

				if($uname != null){
					$filterSend['uname'] = $uname;
				}

				if($suggestStart != null){
					$FormatDate 	= new Zend_Date($suggestStart, $this->_dateDisplayFormat);
					$suggestStart  	= $FormatDate->toString($this->_dateDBFormat);	
					$filterSend['suggeststart'] = $suggestStart;
				}

				if($suggestEnd != null){
					$FormatDate 	= new Zend_Date($suggestEnd, $this->_dateDisplayFormat);
					$suggestEnd  	= $FormatDate->toString($this->_dateDBFormat);	
					$filterSend['suggestend'] = $suggestEnd;
				}

				if($approveStart != null){
					$FormatDate 	= new Zend_Date($approveStart, $this->_dateDisplayFormat);
					$approveStart  	= $FormatDate->toString($this->_dateDBFormat);	
					$filterSend['approvestart'] = $approveStart;
				}

				if($approveEnd != null){
					$FormatDate 	= new Zend_Date($approveEnd, $this->_dateDisplayFormat);
					$approveEnd  	= $FormatDate->toString($this->_dateDBFormat);	
					$filterSend['approveEnd'] = $approveEnd;
				}
			}

			if($filter_clear == TRUE){
				// $suggestEnd 	= date("d/m/Y");
				// $approveEnd 	= date("d/m/Y");
				$this->view->suggestStart  = '';
				$this->view->suggestEnd  = '';
				$this->view->approveStart  = '';
				$this->view->approveEnd  = '';
				
				// $FormatDate 	= new Zend_Date('01/01/1970', $this->_dateDisplayFormat);
				// $suggestStart  = $FormatDate->toString($this->_dateDBFormat);	
				// $filterSend['suggeststart'] = $suggestStart;
				
				// $FormatDate 	= new Zend_Date($suggestEnd, $this->_dateDisplayFormat);
				// $suggestEnd  = $FormatDate->toString($this->_dateDBFormat);	
				// $filterSend['suggestend'] = $suggestEnd;

				// $FormatDate 	= new Zend_Date('01/01/1970', $this->_dateDisplayFormat);
				// $approveStart  = $FormatDate->toString($this->_dateDBFormat);	
				// $filterSend['approvestart'] = $approveStart;
				
				// $FormatDate 	= new Zend_Date($approveEnd, $this->_dateDisplayFormat);
				// $approveEnd  = $FormatDate->toString($this->_dateDBFormat);	
				// $filterSend['approveend'] = $approveEnd;

				// var_dump($filterSend);
			}

			$sorting = $sortBy.' '.$sortDir;
			$result = $model->getUser($filterSend,$sorting);
			
			// var_dump($select);die;
			$this->paging($result);
			$data_val=array();
			foreach($result as $valu){
				$paramTrx = array ("USER_ID" => $valu['USER_ID'],
								"USER_FULLNAME"	=> $valu['USER_FULLNAME'],
								"USER_SUGGESTED"	=> $valu['USER_SUGGESTED'],
								"USER_SUGGESTEDBY"	=> $valu['USER_SUGGESTEDBY'],
								"USER_UPDATED"	=> $valu['USER_UPDATED'],
								"USER_UPDATEDBY"	=> $valu['USER_UPDATEDBY'],
							
				);
				array_push($data_val, $paramTrx);
			}

			$filterlistdata = array("Filter");
			foreach($dataParamValue as $fil => $val){
					$paramTrx = $fil . " - " . $val;
					array_push($filterlistdata, $paramTrx);
			}

			if ($csv) {
				// $this->_helper->download->csv(array($this->language->_('User Name'),$this->language->_('CCY'),$this->language->_('Daily Limit'),$this->language->_('Last Suggested By	'), $this->language->_('Last Approved By')),$data_val,null,'User Daily Limit Scheme');
				$header  = Application_Helper_Array::simpleArray($fields, "label");
				$listable = array_merge_recursive(array($header), $data_val);
				$this->_helper->download->csv($filterlistdata, $listable, null, 'User Limit Scheme');
			  }
			$this->view->fields = $fields;
			$this->view->filter = $filter;
	  	}
		
	  	if(!empty($this->_request->getParam('wherecol'))){
        $this->view->wherecol     = $this->_request->getParam('wherecol');
      }

      if(!empty($this->_request->getParam('whereopt'))){
        $this->view->whereopt     = $this->_request->getParam('whereopt');
      }

      if(!empty($this->_request->getParam('whereval'))){
        $this->view->whereval     = $this->_request->getParam('whereval');
      }
    	$this->view->cust_id = $cust_id;
		Application_Helper_General::writeLog('VMLU','View User Limit List');

		if(!empty($dataParamValue)){
	    		$this->view->updateStart = $dataParamValue['USER_UPDATED'];
	    		$this->view->updateEnd = $dataParamValue['USER_UPDATED_END'];
	    		$this->view->sugestStart = $dataParamValue['USER_SUGGESTED'];
	    		$this->view->sugestEnd = $dataParamValue['USER_SUGGESTED_END'];

	    	  	unset($dataParamValue['USER_UPDATED_END']);
			    unset($dataParamValue['USER_SUGGESTED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	  	
	}
	
}