<?php
class ErrorController extends Application_Main
{

    public function errorAction()
    {
        $this->_helper->layout()->disableLayout();
        $errors = $this->_getParam('error_handler');

        $select = $this->_db->select()
            ->from(array('M_SETTING'), array('*'))
            ->query()->fetchAll();
        $setting = Application_Helper_Array::listArray($select, 'SETTING_ID', 'SETTING_VALUE');
        // echo '<pre>';print_r($setting);
        $this->view->masterTitle = $setting['master_bank_app_name'];
        $this->view->masterEmail = $setting['master_bank_email'];
        $this->view->masterFax   = $setting['master_bank_fax'];
        $this->view->masterBankTelp   = $setting['master_bank_telp'];

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Application error';
                break;
        }

        // Log exception, if logger available
        if ($log = $this->getLog()) {
            //$log->crit($this->view->message, $errors->exception);
            //$logFormat = date('r') . ', "%message%" (%priorityName%)' . PHP_EOL;
            //$simpleFormatter = new Zend_Log_Formatter_Simple($logFormat);
            //$log->setFormatter($simpleFormatter);
            switch ($errors->type) {
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                    // 404 error -- controller or action not found
                    $priority = Zend_Log::WARN;
                    break;
                default:
                    // application error
                    $priority = Zend_Log::ERR;
                    break;
            }

            $requestParam = '';
            foreach ($errors->request->getParams() as $key => $val) {
                $requestParam .= $key . ':' . $val . ', ';
            }
            $requestParam = substr($requestParam, 0, -2);

            $log->log(
                $this->view->message . PHP_EOL .
                    'Error Message: ' . $errors->exception->getMessage() . PHP_EOL .
                    'Request Param: ' . $requestParam . PHP_EOL .
                    'Stack Trace:' . PHP_EOL . $errors->exception->getTraceAsString() . PHP_EOL,
                $priority
            );
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request = $errors->request;

        echo "<pre>";
        var_dump($errors);
        echo "</pre>";
    }




    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasPluginResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }
}
