<?php

require_once 'Zend/Controller/Action.php';

class Generalsetting_SwiftsettingController extends Application_Main
{

    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newlayout');
        $select = $this->_db->select()
            ->from(array('A' => 'M_SETTING'), array('SETTING_ID', 'SETTING_VALUE'))
            ->where('MODULE_ID = ?', 'GNS')
            ->query()->FetchAll();
        $setting = Application_Helper_Array::listArray($select, 'SETTING_ID', 'SETTING_VALUE');

        foreach ($setting as $key => $value) {
            if (!empty($key))
                $this->view->$key = $value;
        }

        //FILTER
        {
            $filters = array(

                'starting_time_remittance' => array('StringTrim', 'StripTags'),
                'cut_off_time_remittance' => array('StringTrim', 'StripTags'),
                'charges_pengiriman_remittance' => array('StringTrim', 'StripTags'),
                'charges_provisi_rate_remittance' => array('StringTrim', 'StripTags'),
                'charges_provisi_min_remittance' => array('StringTrim', 'StripTags'),
                'charges_provisi_max_remittance' => array('StringTrim', 'StripTags'),
                'charges_OUR_remittance' => array('StringTrim', 'StripTags'),
            );
        }

        //VALIDATOR
        {
            $validators = array(

                'starting_time_remittance' => array(
                    'NotEmpty',
                    new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
                    new Zend_Validate_Date(array('format' => 'HH:mm:ss')),
                    array('StringLength', array('min' => 8, 'max' => 8)),
                    'messages' => array(
                        'Can not be empty',
                        'Invalid Time Format',
                        'Invalid Time Format',
                        'Invalid Time Format'
                    )
                ),
                'cut_off_time_remittance' => array(
                    'NotEmpty',
                    new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
                    new Zend_Validate_Date(array('format' => 'HH:mm:ss')),
                    array('StringLength', array('min' => 8, 'max' => 8)),
                    'messages' => array(
                        'Can not be empty',
                        'Invalid Time Format',
                        'Invalid Time Format',
                        'Invalid Time Format'
                    )
                ),
                'charges_pengiriman_remittance' => array(
                    'NotEmpty',
                    array('Between', array('min' => 0.01, 'max' => 9999999999999.99)),
                    new Zend_Validate_Regex('/^\d+(\.\d{1,2})?$/'),
                    'messages' => array(
                        $this->language->_('Can not be empty'),
                        $this->language->_('Amount must be greater than 0.00'),
                        $this->language->_('Value between 0.01 - 9999999999999.99'),
                    )
                ),

                'charges_provisi_rate_remittance' => array(
                    'NotEmpty',
                    new Zend_Validate_Regex('/^\d+(\.\d{1,3})?$/'),
                    new Zend_Validate_Between(['min' => 0.001, 'max' => 9999999999999.99]),
                    'messages' => array(
                        $this->language->_('Can not be empty'),
                        $this->language->_('Invalid format'),
                        array(
                            Zend_Validate_Between::NOT_BETWEEN =>
                            'Value must between 0.001 - 9999999999999.99',
                        )
                    )
                ),

                'charges_provisi_min_remittance' => array(
                    'NotEmpty',
                    new Zend_Validate_Regex('/^\d+(\.\d{1,2})?$/'),
                    array('Between', array('min' => 0.01, 'max' => $this->_getParam('charges_provisi_max_remittance') ?: 9999999999999.99)),
                    'messages' => array(
                        $this->language->_('Can not be empty'),
                        $this->language->_('Invalid Format'),
                        [
                            Zend_Validate_Between::NOT_BETWEEN =>
                            "Amount must between '%min%' and '%max%'",
                        ],
                    )
                ),
                'charges_provisi_max_remittance' => array(
                    'NotEmpty',
                    new Zend_Validate_Regex('/^\d+(\.\d{1,2})?$/'),
                    array('Between', array('min' => $this->_getParam('charges_provisi_min_remittance') ?: 0.01, 'max' => 9999999999999.99)),
                    'messages' => array(
                        $this->language->_('Can not be empty'),
                        $this->language->_('Amount must be greater than 0.00'),
                        [
                            Zend_Validate_Between::NOT_BETWEEN =>
                            "Amount must between '%min%' and '%max%'",
                        ],
                    )
                ),
                'charges_OUR_remittance' => array(
                    'NotEmpty',
                    new Zend_Validate_Regex('/^\d+(\.\d{1,2})?$/'),
                    array('Between', array('min' => 0.01, 'max' => 9999999999999.99)),
                    'messages' => array(
                        $this->language->_('Can not be empty'),
                        $this->language->_('Invalid Format'),
                        [
                            Zend_Validate_Between::NOT_BETWEEN =>
                            "Amount must between '%min%' and '%max%'",
                        ],
                    )
                ),

            );
        }

        $zf_filter = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

        $arrfck = array('starting_time_remittance', 'cut_off_time_remittance', 'charges_pengiriman_remittance', 'charges_provisi_rate_remittance', 'charges_provisi_min_remittance', 'charges_provisi_max_remittance', 'charges_OUR_remittance');

        $cek = $this->_db->select()
            ->FROM(array('B' => 'TEMP_SETTING'), array('MODULE_ID'))
            ->WHERE('MODULE_ID = ?', 'GNS')
            ->query()->FetchAll();
        if ($cek == null) {
            if ($this->_request->isPost() && $this->view->hasPrivilege('STUD')) {
                //validasi global admin fee


                if ($zf_filter->isValid()) {

                    $this->_db->beginTransaction();
                    try {

                        $info = "GENERAL SETTING";
                        $change_id = $this->suggestionWaitingApproval('Pengaturan SWIFT', $info, $this->_changeType['code']['edit'], null, 'M_SETTING', 'TEMP_SETTING', '', '');
                        //$change_id = 1;

                        foreach ($setting as $key => $value) {
                            if (in_array($key, $arrfck)) {
                                $valuee = $this->_getParam2($key);
                                $data = array(
                                    'CHANGES_ID' => $change_id,
                                    'SETTING_ID' => $key,
                                    'SETTING_VALUE' => htmlspecialchars_decode($valuee),
                                    'MODULE_ID' => 'GNS',
                                );
                                $this->_db->insert('TEMP_SETTING', $data);
                            }
                        }

                        Application_Helper_General::writeLog('STUD', 'Update General Setting');
                        $this->_db->commit();
                        $this->view->success = true;
                        $msg = "Settings Change Request Saved";
                        $this->view->report_msg = $msg;
                    } catch (Exception $e) {
                        $this->_db->rollBack();
                    }
                } else {
                    $docErr = 'Error in processing form values. Please correct values and re-submit.';
                    $this->view->report_msg = $docErr;

                    foreach ($zf_filter->getMessages() as $key => $err) {
                        $xxx = 'x' . $key;
                        // $this->view->$xxx = $this->displayError($err);
                        foreach ($err as $key => $value) {
                            # code...
                            $this->view->$xxx = $value;
                            break;
                        }
                    }
                }
            }
        } else {
            $this->view->error = true;
            //$this->fillParam($zf_filter_input);
            $this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
        }


        if ($this->_request->isPost()) {

            foreach ($setting as $key => $value) {
                if (in_array($key, $arrfck)) {
                    $this->view->$key =   $this->_getParam($key);
                } else {
                    $this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
                }
            }

            $disable_note = $zf_filter->disable_note;
            $note = $zf_filter->note;
        } else {
            $disable_note = $setting['disable_note'];
            $note = $setting['note'];
            Application_Helper_General::writeLog('STLS', 'View General Setting');
        }

        $disable_note_len      = (isset($disable_note))  ? strlen($disable_note)  : 0;
        $note_len      = (isset($note))  ? strlen($note)  : 0;

        $disable_note_len      = 200 - $disable_note_len;
        $note_len      = 200 - $note_len;

        $this->view->disable_note_len        = $disable_note_len;
        $this->view->note_len        = $note_len;
    }
}
