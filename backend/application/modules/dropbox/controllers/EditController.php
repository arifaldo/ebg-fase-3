<?php

require_once 'Zend/Controller/Action.php';

class dropbox_EditController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$HELP_ID   	= $this->_getParam('FILE_ID');
		$this->view->HELP_ID = $HELP_ID;

		$filterArr = array(
			'filter' => array('StringTrim', 'StripTags'),
			'submit' => array('StringTrim', 'StripTags')
		);

		$zf_filter 	= new Zend_Filter_Input($filterArr, array(), $this->_request->getParams());

		$filter 	= $zf_filter->getEscaped('filter');
		$submit 	= $zf_filter->getEscaped('submit');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$maxFileSize = "1024";
		$fileExt = "csv,pdf,doc,docx";

		$select = $this->_db->select()
			->from(
				'T_FILE_SUBMIT',
				array(
					'FILE_ID',
					'CUST_ID',
					'FILE_UPLOADEDBY',
					'FILE_NAME',
					'FILE_DESCRIPTION',
					'FILE_DOWNLOADED',
					'FILE_DELETED',
					'FILE_DOWNLOADEDBY',
					'FILE_DELETEDBY',
					'FILE_ISEMAILED',
					'FILE_SYSNAME'
				)
			)
			->where('FILE_TYPE = ?', '1')
			->where('FILE_ID =?', $HELP_ID);

		$data = $this->_db->fetchRow($select);

		$selectcust =  $this->_db->select()
			->from('M_CUSTOMER', array('CUST_ID', 'CUST_NAME'));

		$cust = $this->_db->fetchAll($selectcust);

		$arrcust = Application_Helper_Array::listArray($cust, 'CUST_ID', 'CUST_ID');

		$this->view->arrcust = $arrcust;

		//tinggal di set size validatornya
		if ($this->_request->isPost()) {
			$attahmentDestination 	= UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			$params = $this->_request->getParams();

			$sourceFileName = $adapter->getFileName();
			$error_msg = [];

			if ($sourceFileName == null) {
				$sourceFileName = null;
				$fileType = null;

				$error_msg[] = $this->language->_('Error : File size cannot be left blank.');
				$this->view->error			= true;
				$this->view->report_msg = $this->displayError($error_msg);

				$this->view->company		= $data['CUST_ID'];
				$this->view->document		= $data['FILE_NAME'];
				$this->view->description = $data['FILE_DESCRIPTION'];
			} else {
				if ($_FILES['document']['size'] > 1024000) {
					$error_msg[] = $this->language->_('Error : File size cannot be greater than 1 MB.');
					$this->view->error			= true;
					$this->view->report_msg = $this->displayError($error_msg);

					$this->view->company		= $data['CUST_ID'];
					$this->view->document		= $data['FILE_NAME'];
					$this->view->description = $data['FILE_DESCRIPTION'];
				}

				$sourceFileName = substr(basename($adapter->getFileName()), 0);
				if ($_FILES["document"]["type"]) {
					$adapter->setDestination($attahmentDestination);
					$maxFileSize 			= $this->getSetting('Fe_attachment_maxbyte');
					$fileType = $adapter->getMimeType();
					$size = $_FILES["document"]["size"];
				} else {
					$fileType = null;
					$size = null;
				}
			}

			$paramsName['sourceFileName'] = $sourceFileName;
			$paramsName['description']  	= $this->_getParam('description');

			$filtersName = array(
				// 'company' 			=> array('StringTrim'),
				'document'			=> array('StringTrim'),
				'description'		=> array('StringTrim'),
			);

			$validatorsName = array(
				// 'company' => array(
				// 	'NotEmpty',
				// 	'messages' => array($this->language->_('Error : To Company cannot be left blank.'))
				// ),
				'document' => array(
					'NotEmpty',
					'messages' => array($this->language->_('Error : File cannot be left blank.'))
				),
				'description' => array(
					'NotEmpty',
					'messages' => array($this->language->_('Error : Description cannot be left blank.'))
				),
			);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);

			// if (empty($params['company'])) {
			// 	$error_msg[] = $this->language->_('Error : To Company cannot be left blank.');
			// 	$this->view->error 		= true;
			// 	$this->view->report_msg	= $this->displayError($error_msg);

			// 	$this->view->company = $data['CUST_ID'];
			// 	$this->view->document = $data['FILE_NAME'];
			// 	$this->view->description = $data['FILE_DESCRIPTION'];
			// }

			if (empty($params['description'])) {
				$error_msg[] = $this->language->_('Error : Description cannot be left blank.');
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);

				// $this->view->company = $data['CUST_ID'];
				$this->view->document = $data['FILE_NAME'];
				$this->view->description = $data['FILE_DESCRIPTION'];
			}

			if (!$zf_filter_input_name->isValid()) {
				return false;
			}

			if (!empty($_FILES['document']['name'])) {
				$fileTypeMessage = explode('/', $fileType);
				$fileType =  $fileTypeMessage[1];
				$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
				$extensionValidator->setMessage("Extension file must be *.csv /.dbf / .pdf");

				$maxFileSize	= "1024000";
				$size 				= number_format($size);

				$adapter->setValidators(array($extensionValidator));

				if ($adapter->isValid()) {
					$date = date("dmy");
					$time = date("his");

					$newFileName = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . $sourceFileName;
					$adapter->addFilter('Rename', $newFileName);

					if ($adapter->receive()) {
						$fileName = $sourceFileName;
					}
				} else {
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}
			} else {
				$fileName = $data['FILE_NAME'];
				$newFileName = $data['FILE_SYSNAME'];
			}

			try {
				$this->_db->beginTransaction();
				$param = array(
					// 'CUST_ID'				=> $params['company'],
					'CUST_ID'				=> $data['CUST_ID'],
					'FILE_TYPE'				=> '1',
					'FILE_UPLOADEDBY'			=> $this->_userIdLogin,
					'FILE_NAME' 			=> $fileName,
					'FILE_DESCRIPTION'		=> $params['description'],
					'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
					'FILE_DOWNLOADED'		=> $data['FILE_DOWNLOADED'],
					'FILE_DELETED'			=> $data['FILE_DELETED'],
					'FILE_DOWNLOADEDBY'		=> $data['FILE_DOWNLOADEDBY'],
					'FILE_DELETEDBY'		=> $data['FILE_DELETEDBY'],
					'FILE_ISEMAILED'		=> $data['FILE_ISEMAILED'],
					'FILE_SYSNAME'			=> $newFileName,
				);

				$where = array('FILE_ID = ?' => $HELP_ID);
				$query = $this->_db->update("T_FILE_SUBMIT", $param, $where);
				$this->_db->commit();
				// Application_Helper_General::writeLog('HLUD', 'Edit Dropbox. FILE ID: [' . $HELP_ID . '], File name : [' . $fileName . ']');
				$test = $selectcust->where('CUST_ID = ?', 'FIF00A1');
				$test = $test->query()->fetch();
				Application_Helper_General::writeLog('DUCC', 'Edit file Dropbox ' . $fileName . ' from Company ' . $test['CUST_NAME']);
				$this->setbackURL('/dropbox');
				$this->_redirect('/notification/success/index');
			} catch (Exception $e) {
				$this->_db->rollBack();
			}
		}

		$this->view->file = $data['HELP_FILENAME'];
		if ($this->_request->isPost()) {
			$this->view->helpTopic = (isset($zf_filter_input_name->helpTopic)) ? $zf_filter_input_name->helpTopic : $this->getRequest()->getParam('helpTopic');
			$this->view->description = (isset($zf_filter_input_name->description)) ? $zf_filter_input_name->description : $this->getRequest()->getParam('description');
		} else {
			$this->view->company = $data['CUST_ID'];
			$this->view->document = $data['FILE_NAME'];
			$this->view->description = $data['FILE_DESCRIPTION'];
		}
		$disable_note_len 	 = (isset($params['description']))  ? strlen($params['description'])  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 100 - $disable_note_len;
		$note_len 	 = 100 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;
		if (!$this->_request->isPost()) {
			// Application_Helper_General::writeLog('HLUD', 'Viewing Edit Help');
		}
	}
}
