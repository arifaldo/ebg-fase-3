<?php
/**
 * 使用Get的方式返回：challenge和capthca_id 此方式以实现前后端完全分离的开发模式 专门实现failback
 * @author Tanxu
 */
//error_reporting(0);
// require_once dirname(dirname(__FILE__)) . '/lib/class.geetestlib.php';
// require_once dirname(dirname(__FILE__)) . '/config/config.php';
// require_once 'General/Gclient.php';
require_once 'General/Settings.php';
require_once 'General/CustomerUser.php';
require_once 'General/Login.php';
require_once 'General/Gclient.php';
require_once 'SGO/Helper/AES.php';
require_once 'SGO/Extendedmodule/GoogleAPI/vendor/autoload.php';
// require_once 'SGO/Extendedmodule/Getest/config/config.php';

class GcallbackController extends Application_Main {

	public function getUserIP()
	{
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}

	public function get_browser_name($user_agent)
	{
	    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
	    elseif (strpos($user_agent, 'Edge')) return 'Edge';
	    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
	    elseif (strpos($user_agent, 'Safari')) return 'Safari';
	    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
	    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

	    return 'Other';
	}


	public function indexAction(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();


		$gClient = Gclient::getGClient();

		$param = $this->_request->getParams();
		// echo "<pre>";
		// print_r($param);
		if(isset($param['code'])){
			// print_r($param['code']);die;
			$idtoken = (string)$param['code'];
			// var_dump($gClient);
			try {
				$token = $gClient->fetchAccessTokenWithAuthCode($idtoken);	
				// var_dump($token);die('here');
			} catch (Exception $e) {
				// var_dump($e);die;
			}
			
			$_SESSION['access_token'] = $token;
		}
		// die;
		try {

			$oAuth = new Google_Service_Oauth2($gClient);
			$userData = $oAuth->userinfo_v2_me->get();

			// echo "<pre>";
			// print_r($userData);die;

			$_SESSION['LastName'] = $userData['familyName'];
			$_SESSION['email'] = $userData['email'];
			$_SESSION['Firstname'] = $userData['givenName'];
			$_SESSION['id'] = $userData['id'];
			$_SESSION['picture'] = $userData['picture'];
			// var_dump($_SESSION);die;
			$result = $this->_db->fetchRow(
					$this->_db->SELECT()
					->FROM('M_USER')
					->WHERE('USER_STATUS = ? ','1')
					->WHERE('USER_EMAIL = ?',$_SESSION['email'])
					// ->WHERE('CUST_ID = ?',$custId)
				);
			// echo "<pre>";
			// var_dump($result);die;
			if(empty($result)){
				$flashMessenger = $this->_helper->getHelper('FlashMessenger');
				$flashMessenger->addMessage('Email is not registered');
				// $this->_redirect ( '/default' );
				$this->_redirect ( '/' );
			}
			$dataCustomer = $this->_db->fetchRow(
				$this->_db->select(array('CUST_ID','CUST_NAME','CUST_SAME_USER'))
					->FROM('M_CUSTOMER')
					->WHERE('CUST_ID = ?',$result['CUST_ID'])
					->WHERE('CUST_STATUS = 1')
			);
			// die;

					$custId = $result['CUST_ID'];
					$userId = $result['USER_ID'];

						$Settings = new Settings();
						$allSetting = $Settings->getAllSetting();

						$minDiff = $allSetting['timeoutafter'];

						$objSessionNamespace = new Zend_Session_Namespace( 'Zend_Auth' );
						$timeExprSession = 600 + $minDiff *60;//tambah 600 karena scheduler force logout 10 menit sekali
						$objSessionNamespace->setExpirationSeconds( $timeExprSession );
						// set keterangan login ke auth
						$CustomerUser	= new CustomerUser(strtoupper($custId), strtoupper($userId));
						$listAccount 	= $CustomerUser->getAccounts();
						$accountList 	= Application_Helper_Array::simpleArray($listAccount,'ACCT_NO');

						$listPrivilege = $CustomerUser->getPrivileges();
						$privilege = Application_Helper_Array::simpleArray($listPrivilege,'FPRIVI_ID');

						$privilege[] = 'systemPrivilege';



						$auth = Zend_Auth::getInstance();
						$data = new stdClass();
						// print_r($result);die;
						$data->isCommLinkage = false;
						$data->userIdLogin    = $result['USER_ID'];
						$data->userNameLogin  = ucwords(strtolower($result['USER_FULLNAME']));
						$data->custNameLogin  = ucwords(strtolower($dataCustomer['CUST_NAME']));
						$data->priviIdLogin   = $privilege;
						$data->allSetting     = $allSetting;
						// $data->userGroupLogin = $groupArr;
						$data->custIdLogin    = strtoupper($custId);
						$data->tokenIdLogin    = $result['TOKEN_ID'];
						$data->accountList    = $accountList;
						$data->lastLogin = $result['USER_LASTLOGIN'];
						$data->custNameLogin = $dataCustomer['CUST_NAME'];
						$data->custSameUser = $dataCustomer['CUST_SAME_USER'];
						$data->userEmailLogin = $_SESSION['email'];


						$data->userBranch = $login ['BRANCH_NAME'];
						$data->userBranchId = $login ['ID'];
						$data->userBranchCode = $login ['BRANCH_CODE'];


						
						if(!empty($notif)){
								$data->notifCount = count($notif);	
								$data->notif = $notif;
						}
					

						//theme
						$data->_userTheme = $dataComp['USER_THEME'];


						// cek userSessionHash before login
						$userSessionHash	= $this->_db->fetchrow (
									$this->_db->SELECT ('SESSION_HASH')->FROM ( 'M_USER' )
									->WHERE ( 'USER_ID = ?', $result['USER_ID'] )
									->WHERE ('CUST_ID =?',$result['CUST_ID'])
									);
						//


						// ---------------------------------------- HASH -----------------------------------------------  //
						$hash = $this->hashKey($userId,$custId);
						$data->userHash     = $hash;

						$auth->getStorage()->write($data);
						// End set keterangan login ke auth
						//$browser = substr($_SERVER['HTTP_USER_AGENT'], 0,24);
						$browser = $_SERVER['HTTP_USER_AGENT'];
						$sessionHash = md5($_SERVER['REMOTE_ADDR'].$browser);
// Zend_Debug::dump($auth);die;
						//update USER_ISLOGIN
						$updateArr['USER_ISLOGIN'] = 1;
						$updateArr['USER_ISLOCKED'] = 0;
						$updateArr['USER_LASTLOGIN'] = new Zend_Db_Expr('now()');
						$updateArr['USER_LASTACTIVITY'] = new Zend_Db_Expr('now()');
						$updateArr['USER_FAILEDATTEMPT'] = 0;
						$updateArr['USER_ISNEW'] = 0;
						$updateArr['USER_HASH'] = $hash;
						$updateArr['SESSION_HASH'] = $sessionHash;
						$whereArr = array('CUST_ID = ?' => (string)$custId,
								'USER_ID = ?' => (string)$userId);
						$customerupdate = $this->_db->update('M_USER',$updateArr,$whereArr);


						if ($userSessionHash['SESSION_HASH'] != $sessionHash){

							//send email
							$EmailLoginNotif 				= $allSetting['femailtemplate_loginnotif'];
							$templateEmailMasterBankName	= $allSetting['master_bank_name'];
							$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
							$templateEmailMasterBankEmail	= $allSetting['master_bank_email'];
							$templateEmailMasterBankTelp	= $allSetting['master_bank_telp'];

							$userData	= $this->_db->fetchrow (
									$this->_db->SELECT ()->FROM ( 'M_USER' )
									->WHERE ( 'USER_ID = ?', $userId )
									->WHERE ('CUST_ID =?',$custId)
								);

							$lastlogin = Application_Helper_General::convertDate($userData['USER_LASTLOGIN'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

							// masking user id
							$lengthUserId = strlen($userId);
							$maskchar = substr($userId,1,$lengthUserId-3);
							$lengthMaskchar = strlen($maskchar);
							$mask = str_repeat("*",$lengthMaskchar);
							$userIdMask = str_replace($maskchar, $mask, $userId)."<br>";
							// end masking

							$data = array( '[[user_fullname]]' => $userData['USER_FULLNAME'],
										'[[user_login]]' => $userIdMask,
										'[[user_email]]' => $userData['USER_EMAIL'],
										'[[user_lastlogin]]' => $lastlogin,
										'[[user_agent]]' => $this->get_browser_name($_SERVER['HTTP_USER_AGENT']),//$browser,
										'[[master_bank_name]]' => $templateEmailMasterBankName,
										'[[master_bank_app_name]]' => $templateEmailMasterBankAppName,
										'[[master_bank_email]]' => $templateEmailMasterBankEmail,
										'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp
										);

							$EmailLoginNotif = strtr($EmailLoginNotif,$data);
							Application_Helper_Email::sendEmail($userData['USER_EMAIL'],'Login Notification',$EmailLoginNotif);
							//end send email
						}


						//update latest transaction cache
						$this->latestTransaction($custId,$userId);
					// 	print_r($cekpass);
					// die;
						Application_Helper_General::writeLog('FLGN','Login Success '.$user_ip = $this->getUserIP());
						$this->_redirect ( '/home/dashboard' );


						// $this->_redirect('/notification/success');

			// header(string 'Location: /home/dashboard');
			// exit();

		} catch (Exception $e) {
			// print_r($e);die;
			$flashMessenger = $this->_helper->getHelper('FlashMessenger');
			$flashMessenger->addMessage('Google Auth Error. Please Try Again Later');
			$this->_redirect ( '/default' );

		}

	}

	public function hashKey($userId,$custId) {
		do
		{
			$key = null;
			for($i=0;$i<3;$i++)
				$key .= rand(0,9);

			$browser = $_SERVER['HTTP_USER_AGENT'];
			$hash = md5($_SERVER['REMOTE_ADDR'].$browser.$key);
			$checkHash = null;
			// CHECK HASH
			$checkHash = $this->_db->fetchOne(
																			$this->_db->SELECT()
																								->FROM('M_USER' ,array('USER_HASH'))
																								->WHERE('USER_ID =?',$userId)
																								->WHERE('CUST_ID =?',$custId)
																								->WHERE('USER_HASH =?',$hash)
																			);
		}while(!empty($checkHash));

		return $hash;
	}

	private function latestTransaction($custId,$userId){
    	$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);

    	$casePayType = "(CASE P.PS_TYPE ";
  		foreach($arrPayType as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";

    	$frontendOptions = array ('lifetime' => 900,
								  'automatic_serialization' => true );
		$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/latesttrans/' ); // Directory where to put the cache files
		$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );

		$cacheID = 'LT'.$custId.$userId;

		if(!$result = $cache->load($cacheID))
		{
			$select = $this->_db->select()
								->from(array('P' => 'T_PSLIP'), array())
								->join(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
								->joinLeft(array('M' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = M.PROVIDER_ID', array('P.PS_NUMBER','PAYMENT_TYPE' => $casePayType,'T.BENEFICIARY_ACCOUNT','P.PS_CCY','P.PS_TOTAL_AMOUNT','M.PROVIDER_NAME'))
								->where('P.CUST_ID = ?', $custId)
								->where('P.PS_RELEASER_USER_LOGIN = ?', $userId)
								->where('P.PS_STATUS = 5')
								->where('T.TRA_STATUS = 3')
								->order('P.PS_CREATED DESC')
								->limit(5);

			$result = $this->_db->fetchAll($select);

			$cache->save($result,$cacheID);
		}
    }

	public function langAction() {
		$set = $this->_getParam('set');

		//// Start Multi Language
		$defaultlanguage=$set;
		$ns = new Zend_Session_Namespace('language');
		$sessionLanguage = $ns->langCode;

		if(!is_null($sessionLanguage)){
			$setlang = $sessionLanguage;
		}else{
			$setlang = $locale->getLanguage();
		}
		if (!empty($setlang) && isset($setlang)) {
			$defaultlanguage=$setlang;
			$ns->langCode = $defaultlanguage;
		}
		Zend_Loader::loadClass('Zend_Translate');

		$frontendOptions = array(
			'cache_id_prefix' => 'MyLanguage',
			'lifetime' => 86400,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => APPLICATION_PATH.'/../../library/data/cache/language/frontend');
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		Zend_Translate::setCache($cache);

		$translate = new Zend_Translate('tmx', APPLICATION_PATH.'/../../library/data/languages/frontend/language.tmx', $defaultlanguage);
// 		Zend_Debug::dump($translate);
		Zend_Registry::set('language', $translate);
		//// End Multi Language

		$lang = $set;
		$ns->langCode = $lang;

	}

	public function logoutAction() {
		Application_Helper_General::writeLog('FLGT','Logout From System '.$user_ip = $this->getUserIP());
		if(Zend_Auth::getInstance()->hasIdentity()){
			$auth = Zend_Auth::getInstance()->getIdentity();
    	  	$this->_userIdLogin   = $auth->userIdLogin;
    	  	$this->_custIdLogin   = $auth->custIdLogin;
    	  	Zend_Auth::getInstance()->clearIdentity();
			$CustomerUser	= new CustomerUser(strtoupper($this->_custIdLogin),strtoupper($this->_userIdLogin));
			$CustomerUser->forceLogout();

			$forceBySession = $this->_request->getParam('session');

			if($forceBySession == '1'){
				$this->_redirect('/authorizationacl/index/disableuser');
			}
		}
		$this->_redirect('/');

//		$this->_redirector->gotoUrl('login/');
	}

}



 ?>
