<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class systemlog_EmaillogController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model = new systemlog_Model_Systemlog();

		$activity = $model->getFPrivilege();

		$login = array('FPRIVI_ID' => 'FLGN', 'FPRIVI_DESC' => 'Login');
		$logout = array('FPRIVI_ID' => 'FLGT', 'FPRIVI_DESC' => 'Logout');
		$changepass = array('FPRIVI_ID' => 'CHMP', 'FPRIVI_DESC' => 'Change My Password');
		array_unshift($activity, $changepass);
		array_unshift($activity, $logout);
		array_unshift($activity, $login);

		/*$select = $this->_db->select()->distinct()
										->from(
													array('T_INTERFACE_LOG'),
													array(
															'FUNCTION_NAME',
														)
												);
		$arrfunction 				= $this->_db->fetchAll($select);		
		$this->view->arrfunction 	= $arrfunction;*/

		$emailreceiver_lg = $this->language->_('Email Receiver');
		$emailcontent_lg = $this->language->_('Email Content');
		$email_date_lg = $this->language->_('Email Date');
		$emailstatus_lg = $this->language->_('Email Status');

		$fields = array(
			'EMAIL_RECEIVER'    => array(
				'field' => 'EMAIL_RECEIVER',
				'label' => $emailreceiver_lg,
				'sortable' => true
			),
			/*'EMAIL_CONTENT'     => array('field' => 'EMAIL_CONTENT',
											      'label' => $emailcontent_lg,
											      'sortable' => true),*/
			'EMAIL_DATE'      	=> array(
				'field' => 'EMAIL_DATE',
				'label' => $email_date_lg,
				'sortable' => true
			),
			'EMAIL_STATUS'   	=> array(
				'field' => 'EMAIL_STATUS',
				'label' => $emailstatus_lg,
				'sortable' => true
			),
			'EMAIL_RESPONSE' => [
				'field' => 'EMAIL_RESPONSE',
				'label' => 'SMTP Response',
				'sortable' => true
			]
		);

		$filterlist = array('EMAIL_DATE', 'EMAIL_RECEIVER');

		$this->view->filterlist = $filterlist;

		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'EMAIL_DATE');
		$sortDir = $this->_getParam('sortdir', 'desc');
		$filter_clear    = $this->_getParam('filter_clear');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$filterArr = array(
			'filter'				=> array('StripTags', 'StringTrim'),
			'EMAIL_RECEIVER'		=> array('StripTags', 'StringTrim', 'StringToUpper'),
			//'uid'    			=> array('StripTags','StringTrim','StringToUpper'),	    				   
			//'SEARCH_FUNCTION' 	=> array('StripTags','StringToUpper'),
			'EMAIL_DATE' 			=> array('StripTags', 'StringTrim'),
			'EMAIL_DATE_END' 			=> array('StripTags', 'StringTrim'),
		);

		$validator = array(
			'filter'			 	=> array(),
			'EMAIL_RECEIVER'		=> array(),
			// 'uid'    			=> array(),	    				   
			//'SEARCH_FUNCTION' 	=> array(),
			'EMAIL_DATE' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'EMAIL_DATE_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		$dataParam = array('EMAIL_RECEIVER');
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "EMAIL_DATE") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}
		// print_r($dataParamValue);
		// die;	
		// print_r($this->_request->getParam('whereval'));die;
		if (!empty($this->_request->getParam('emaildate'))) {
			$emailarr = $this->_request->getParam('emaildate');
			$dataParamValue['EMAIL_DATE'] = $emailarr[0];
			$dataParamValue['EMAIL_DATE_END'] = $emailarr[1];
		}


		$fParam = array();
		$datefrom = "";
		$dateto = "";

		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
		$filter 		= $this->_getParam('filter');
		$emailreceiver 	= $fParam['emailreceiver']		= html_entity_decode($zf_filter->getEscaped('EMAIL_RECEIVER'));
		//$uid 		= $fParam['uid']		= html_entity_decode($zf_filter->getEscaped('uid'));
		//$function 	= $fParam['function']	= html_entity_decode($this->_getParam('SEARCH_FUNCTION'));
		$datefrom 	= $fParam['datefrom']	= html_entity_decode($zf_filter->getEscaped('EMAIL_DATE'));
		$dateto	 	= $fParam['dateto']		= html_entity_decode($zf_filter->getEscaped('EMAIL_DATE_END'));

		if ($filter == null) {
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}

		if ($filter == null || $filter == TRUE) {
			$this->view->fDateTo    = $dateto;
			$this->view->fDateFrom  = $datefrom;
		}

		if (!empty($datefrom)) {
			$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
			$fParam['datefrom']  = $FormatDate->toString($this->_dateDBFormat);
		}

		if (!empty($dateto)) {
			$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
			$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
		}

		if ($filter == TRUE) {

			/*if($function)
			{
				$this->view->function = $function;				
			}*/

			if ($emailreceiver) {
				$this->view->emailreceiver = $emailreceiver;
			}

			/*if($uid)
			{
			   $this->view->uid = $uid;
			}*/
		}
		if ($filter_clear == TRUE) {
			$datefrom = '00/00/0000';
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';

			$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
			$fParam['datefrom']  = $FormatDate->toString($this->_dateDBFormat);

			$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
			$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
		}
		if ($fParam['dateto'] == '' || $fParam['datefrom'] == '') {
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
			$fParam['datefrom']  = $FormatDate->toString($this->_dateDBFormat);

			$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
			$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
		}

		//var_dump($fParam);
		// $data = $model->getEmailLog($fParam,$sortBy,$sortDir,$filter);

		// GET EMAIL LOG
		$today = date("Ymd");
		$nametext = $today . '.txt';
		$path_file = LIBRARY_PATH . '/data/logs/email/';

		$myfile =  file($path_file . $nametext);

		$maxLine = 1000;

		$saveLog = [];

		$count = 1;
		foreach (array_reverse($myfile) as $line) {
			$saveLog[] = json_decode($line, true);

			if ($count == $maxLine) break;
			$count++;
		}

		$data = $saveLog;

		$this->paging($data);

		if ($this->_request->getParam('print') == 1) {

			// echo "<pre>";
			// var_dump($data);
			// die();

			foreach ($data as $key => $row) {
				$data[$key]['EMAIL_RECEIVER'] = $row['EMAIL_RECEIVER'];
				$data[$key]['EMAIL_CONTENT'] = '<textarea readonly rows="5" cols="60" style="overflow:auto;resize:none;color:black;font-family: Arial, Helvetica, sans-serif;">' . $row['EMAIL_CONTENT'] . '</textarea>';
				$data[$key]['EMAIL_DATE'] = Application_Helper_General::convertDate($row['EMAIL_DATE'], "dd MMM yyyy HH:mm:ss ");
				$data[$key]['EMAIL_STATUS'] = $row['EMAIL_STATUS'];
			}

			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Email Log', 'data_header' => $fields));
		}

		if ($pdf || $csv) {
			$arr = $data;

			foreach ($arr as $key => $value) {
				$arr[$key]["EMAIL_DATE"] = Application_Helper_General::convertDate($value["EMAIL_DATE"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}

			if ($csv) {
				Application_Helper_General::writeLog('SYLG', 'Download CSV Email Log');
				$this->_helper->download->csv(array('Email Receiver', 'Email Content', 'Email Date', 'Status'), $arr, null, 'Email Log');
			}

			if ($pdf) {
				Application_Helper_General::writeLog('SYLG', 'Download PDF Email Log');
				$this->_helper->download->pdf(array('Email Receiver', 'Email Content', 'Email Date', 'Status'), $arr, null, 'Email Log');
			}
		} else {
			Application_Helper_General::writeLog('SYLG', 'View Email Log');
		}

		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if (!empty($dataParamValue)) {
			$this->view->emaildateStart = $dataParamValue['EMAIL_DATE'];
			$this->view->emaildateEnd = $dataParamValue['EMAIL_DATE_END'];

			unset($dataParamValue['EMAIL_DATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
			// print_r($whereval);die;
		}
	}
}
