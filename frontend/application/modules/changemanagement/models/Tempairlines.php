<?php
/**
 * Tempgroup model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempairlines extends Changemanagement_Model_Tempchanges {	
	protected $_moduleId = '';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		
		//query from TEMP_AIRLINES_SETTINGS
		$airlineData = $this->dbObj->select()
						  	  ->from('TEMP_AIRLINES_SETTINGS')
						  	  ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($airlineData)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(airlineinq)';
			return false;
			return false;
		}
	
		//insert to master table M_AIRLINES_SETTINGS
		$insertArr = array_diff_key($airlineData,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['APPROVED_BY'] = $actor;
		$insertArr['APPROVED_DATE'] = new Zend_Db_Expr('now()');

		if ($insertArr['BALANCE_CYCLE'] == '30m') {
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+30 minutes"));
		}
		else if($insertArr['BALANCE_CYCLE'] == '1h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+1 hour"));
		}
		else if($insertArr['BALANCE_CYCLE'] == '2h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+2 hour"));
		}
		else if($insertArr['BALANCE_CYCLE'] == '3h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+3 hour"));
		}

		$insertArr['EF_DATE'] = $EF_DATE;

		$airlineins = $this->dbObj->insert('M_AIRLINES_SETTINGS',$insertArr);

		if(!(boolean)$airlineins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(airlineins)';
			return false;
		}
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		//query from TEMP_AIRLINES_SETTINGS
		$airlineData = $this->dbObj->select()
						  	  ->from('TEMP_AIRLINES_SETTINGS')
						  	  ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($airlineData)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(airlineinq)';
			return false;
		}
		
		//update record
		$updateArr = array_diff_key($airlineData,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>''));
		$updateArr['APPROVED_BY'] = $actor;
		$updateArr['APPROVED_DATE'] = new Zend_Db_Expr('now()');

		if ($updateArr['BALANCE_CYCLE'] == '30m') {
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+30 minutes"));
		}
		else if($updateArr['BALANCE_CYCLE'] == '1h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+1 hour"));
		}
		else if($updateArr['BALANCE_CYCLE'] == '2h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+2 hour"));
		}
		else if($updateArr['BALANCE_CYCLE'] == '3h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+3 hour"));
		}

		$updateArr['EF_DATE'] = $EF_DATE;

		unset($updateArr['CREATED_BY']);
		unset($updateArr['CREATED_DATE']);

		$whereArr = array($this->dbObj->quoteInto('CUST_ID = ?',$airlineData['CUST_ID']), $this->dbObj->quoteInto('AIRLINE_CODE = ?',$airlineData['AIRLINE_CODE']));
		$airlineupdate = $this->dbObj->update('M_AIRLINES_SETTINGS',$updateArr,$whereArr);
		
		if(!(boolean)$airlineupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(airlineupdate)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}

	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
		
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	
	}
	
	public function approveDelete() {
		//query from TEMP_AIRLINES_SETTINGS
		$airlineData = $this->dbObj->select()
						  	  ->from('TEMP_AIRLINES_SETTINGS')
						  	  ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($airlineData)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(airlineinq)';
			return false;
		}

		$where['CUST_ID = ?'] = $airlineData['CUST_ID'];
		$where['AIRLINE_CODE = ?'] = $airlineData['AIRLINE_CODE'];
		$airlinedelete = $this->dbObj->delete('M_AIRLINES_SETTINGS', $where);

		if(!(boolean)$airlinedelete) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(airlinedelete)';
			return false;
		}
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;

		return true;
	}
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

		//delete from TEMP_AIRLINES_SETTINGS
		$tempairlinedelete = $this->dbObj->delete('TEMP_AIRLINES_SETTINGS',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_AIRLINES_SETTINGS
		$tempairlinedelete = $this->dbObj->delete('TEMP_AIRLINES_SETTINGS',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}

	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
		
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteDelete() {
		//delete from TEMP_AIRLINES_SETTINGS
		$tempairlinedelete = $this->dbObj->delete('TEMP_AIRLINES_SETTINGS',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
}