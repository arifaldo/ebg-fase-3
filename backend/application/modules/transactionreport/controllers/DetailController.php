<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'Crypt/AESMYSQL.php';

class transactionreport_DetailController extends Application_Main
{

	public function initController(){
			  $this->_helper->layout()->setLayout('newpopup');
	}

	public function indexAction(){

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$transactionid = $AESMYSQL->decrypt($this->_getParam('transactionid'), $password);

		$pdf 			= $this->_getParam('pdf');

		$arrTraType 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		$arrTraStatus	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		// 6,4 THEN 'Multi Credit'
		// 7,5 THEN 'Multi Debet'
		$paytypeall		= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		foreach($paytypeall as $key => $value){

			if( $key != 8 && $key != 9 && $key != 10){

				if($key == 4 OR $key == 6) {
					$value = $paytypeall[6];
					$optpaytypeAll[$key] = $value;
				}
				else if($key == 5 OR $key == 7) {
					$value = $paytypeall[7];
					$optpaytypeAll[$key] = $value;
				}
				else $optpaytypeAll[$key] = $value;
			}

		}

		foreach($arrTraType as $key => $value){
			//if($key <= 2 ){
				$arrTraTypeRaw[$key] = $value;
			//}
		}

		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($optpaytypeAll as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";

		$caseTraType = "(CASE T.TRANSFER_TYPE ";
  		foreach($arrTraTypeRaw as $key => $val)	{ $caseTraType .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraType .= " END)";

		$caseTraStatus = "(CASE T.TRA_STATUS ";
  		foreach($arrTraStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";
		//print_r($casePayType);die;
		$select = $this->_db->select()
							->from(		array(	'T'=>'T_TRANSACTION'),array())
							->joinLeft(	array(	'P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',
										array(	'companycode'	=>'C.CUST_ID',
												'companyname'	=>'C.CUST_NAME',
												'payref'		=>'P.PS_NUMBER',
												'pssubject'		=>'P.PS_SUBJECT',
												'createddate'	=>'P.PS_CREATED',
												'updateddate'	=>'P.PS_UPDATED',
												'transferdate'	=>'P.PS_EFDATE',
												'acctsrc'		=> new Zend_Db_Expr("
																	CONCAT( T.SOURCE_ACCOUNT , ' (' , T.SOURCE_ACCOUNT_CCY , ')  ' , T.SOURCE_ACCOUNT_NAME ) "),
												'beneacct'		=> new Zend_Db_Expr("
																	CONCAT( T.BENEFICIARY_ACCOUNT , ' (' , T.BENEFICIARY_ACCOUNT_CCY , ')  ' , T.BENEFICIARY_ACCOUNT_NAME ) "),
												'benebankname'	=>'T.BENEFICIARY_BANK_NAME',
												'ccy'			=>'P.PS_CCY',
												'traamount'		=>'T.TRA_AMOUNT',
												'transactionid'	=>'T.TRANSACTION_ID',
												'tramessage'	=>'T.TRA_MESSAGE',
												'traaddmessage'	=>'T.TRA_ADDITIONAL_MESSAGE',
												'trarefno'		=>'T.TRA_REFNO',
												'sourceccy'		=>'T.SOURCE_ACCOUNT_CCY',

												'clrcode'		=>'T.CLR_CODE',
												'swiftcode'		=>'T.SWIFT_CODE',
												'benefbankcode'	=>'T.BENEF_ACCT_BANK_CODE',
												'bankcode'		=>'T.BANK_CODE',
												'bankresponse'	=>'T.BANK_RESPONSE',
												'nostrobank'	=>'T.NOSTRO_NAME',
												'bankname1'		=>'T.BENEFICIARY_BANK_ADDRESS1',
												'bankname2'		=>'T.BENEFICIARY_BANK_ADDRESS2',
												'bankname3'		=>'T.BENEFICIARY_BANK_ADDRESS3',
												'countryname'	=>'CT.COUNTRY_NAME',
												'T.BENEFICIARY_ACCOUNT_CCY',
												'PAYTYPE'		=>'P.PS_TYPE',
												'TRANSFERTYPE'	=>'T.TRANSFER_TYPE',
												'TRANSFEESTATUS'=>'T.TRANSFER_FEE_STATUS',
												'paymenttype'	=>$casePayType,
												'transfertype'	=>$caseTraType,
												'transferstatus'=>$caseTraStatus,
												'eqamountusd' => 'T.EQUIVALENT_AMOUNT_USD',
												'eqamountidr' => 'T.EQUIVALENT_AMOUNT_IDR',
												'transferfee'	=>'T.TRANSFER_FEE',
												'fullamountfee'	=>'T.FULL_AMOUNT_FEE',
												'provisionfee'	=>'T.PROVISION_FEE',
												'bookrate'		=>'T.BOOK_RATE',
												'T.RATE',
												'bookbuy'		=>'T.BOOK_RATE_BUY',
												'traremain'		=>'T.TRA_REMAIN',
												'T.*','P.*','B.*'

												))
							->joinLeft(array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())
							->joinLeft(array(	'B'=>'M_BANK_TABLE'),'T.BENEF_ACCT_BANK_CODE = B.BANK_CODE',array())
							->joinLeft(array(	'CT'=>'M_COUNTRY'),'T.BENEFICIARY_BANK_COUNTRY = CT.COUNTRY_CODE',array())
							->where("T.TRANSACTION_ID =?",$transactionid);
		 // echo "<pre>";
		 // echo $select->__toString();
		 // die;


		$dataTx = $this->_db->fetchRow($select);
		//echo '<pre>';
		//var_dump($dataTx);die;

		if($dataTx["benebankname"])	$benebankname = ' / '.$dataTx["benebankname"];
		else						$benebankname = " ";


		/* 	tra_status 3 (success) -> transfercharge Y
			tra_status 4 (failed) -> transfercharge N
		*/

		if($dataTx["TRANSFEESTATUS"] == 1) 		$transfercharge = 'Y';
		if($dataTx["TRANSFEESTATUS"] == 0) 		$transfercharge = 'N';
		if($dataTx["TRANSFEESTATUS"] == NULL) 	$transfercharge = '-';

		$ccytrffee = $dataTx['ccy'];
		$ccytrfFA = $dataTx['ccy'];
		$ccytrfpro = $dataTx['ccy'];
		if($dataTx["PAYTYPE"] == "3"){


			if($dataTx['sourceccy'] == "USD" && $data['ccy'] == "USD"){
//				$totalinvalas = $dataTx['traamount'] + $dataTx['transferfee'] + $dataTx['provisionfee'] + $dataTx['fullamountfee'];
//				$totalinidr = $totalinvalas * $dataTx['bookrate'];
				$totalinvalas = $dataTx['eqamountusd'];
				$totalinidr = $dataTx['traamount'];
			}
			else{
				//$totalinidr = (($dataTx['traamount'] + $dataTx['fullamountfee']) * $dataTx['bookrate']) + $dataTx['transferfee'];
				$totalinidr = $dataTx['eqamountidr'];
				$totalinvalas = $dataTx['traamount'];
			}
			//print_r($dataTx['sourceccy']);die;
			$selecttrffee = $this->_db->select()->from(array('T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'3')
			->where("T.CUST_ID =?",$dataTx['companycode'])
			->where("T.CHARGE_CCY =? ", $dataTx['sourceccy'])
			;
			//echo $selecttrffee;
			$trffee = $this->_db->fetchRow($selecttrffee);

			$selecttrfFA = $this->_db->select()
			->from(		array(	'T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'4')
			->where("T.CUST_ID =?",$dataTx['companycode'])
			->where("T.CHARGE_CCY =?",$dataTx['BENEFICIARY_ACCOUNT_CCY'])
			;
			$trfFA = $this->_db->fetchRow($selecttrfFA);

			$selecttrfpro = $this->_db->select()->from(array('T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'5')
			->where("T.CUST_ID =?",$dataTx['companycode'])
			->where("T.CHARGE_CCY =? ", $dataTx['sourceccy'])
			;
			//echo $selecttrffee;
			$trfpro = $this->_db->fetchRow($selecttrfpro);
			$ccytrfpro = $trfpro['CHARGE_AMOUNT_CCY'];
			//print_r($trfFA);
			$ccytrffee = $trffee['CHARGE_AMOUNT_CCY'];
			$trffee = $ccytrffee.' '.Application_Helper_General::displayMoney($dataTx['transferfee']);
			$ccytrfFA = $trfFA['CHARGE_AMOUNT_CCY'];
			//$total = $dataTx['ccy'].' '.Application_Helper_General::displayMoney($totalinvalas).' (IDR '.Application_Helper_General::displayMoney($totalinidr).')';
			$total = 'IDR '.Application_Helper_General::displayMoney($totalinidr);
			if($dataTx['sourceccy']=='USD' && $dataTx['BENEFICIARY_ACCOUNT_CCY'] == 'USD'){
			$total = 'USD '.Application_Helper_General::displayMoney($totalinidr);
			}

		}
		elseif($dataTx['ccy'] != 'IDR'){
			$totalinidr = $dataTx['traamount'] * $dataTx['bookrate'];
			$total = $dataTx['ccy'].' '.Application_Helper_General::displayMoney($dataTx['traamount']).' (IDR '.Application_Helper_General::displayMoney($dataTx['eqamountidr']).')';
		}
		else{
			$trffee = $dataTx['ccy'].' '.Application_Helper_General::displayMoney($dataTx['transferfee']);
//print_r($dataTx);die;
			if($dataTx['PAYTYPE']== '14' || $dataTx['PAYTYPE'] == '15'){
				$amttotal = $dataTx['transferfee'] + $dataTx['traremain'];
			}else{
				$amttotal = $dataTx['transferfee'] + $dataTx['traamount'];
			}
			$total = $dataTx['ccy'].' '.Application_Helper_General::displayMoney($amttotal);
		}

		if($dataTx['transfertype'] == 'OUR' || $dataTx['transfertype'] == 'SHA')
			$bankcode = $dataTx['swiftcode'];
		elseif($dataTx['transfertype'] == 'SKN' || $dataTx['transfertype'] == 'RTGS')
			$bankcode = $dataTx['clrcode'];
		elseif($dataTx['transfertype'] == 'ONLINE')
			$bankcode = $dataTx['bankcode'];
		else
			$bankcode = $dataTx['benefbankcode'];

		//print_r($dataTx);die;
		$_tableMst[0]["label"] = $this->language->_('Company Code');
		$_tableMst[1]["label"] = $this->language->_('Company Name');
		$_tableMst[2]["label"] = $this->language->_('Payment Ref#');
		$_tableMst[3]["label"] = $this->language->_('Payment Type');
		$_tableMst[4]["label"] = $this->language->_('Transaction Ref')."#";
		$_tableMst[5]["label"] = $this->language->_('Transfer Type');
		$_tableMst[6]["label"] = $this->language->_('Payment Subject');
		$_tableMst[7]["label"] = $this->language->_('Updated Date');
		$_tableMst[8]["label"] = $this->language->_('Payment Date');
		$_tableMst[9]["label"] = $this->language->_('Source Account');
		if($dataTx["transfertype"] == 'FA' || $dataTx["transfertype"] == 'No FA') {
			$_tableMst[10]["label"] = $this->language->_('Beneficiary Name');
			$_tableMst[11]["label"] = $this->language->_('Beneficiary NRC');
			$_tableMst[12]["label"] = $this->language->_('Beneficiary Phone');


		}else{
		$_tableMst[10]["label"] = $this->language->_('Beneficiary Account');
		$_tableMst[11]["label"] = $this->language->_('Bank Code');

		}


		$_tableMst[13]["label"] = $this->language->_('Bank Name');
		if($dataTx["transfertype"] == 'FA' || $dataTx["transfertype"] == 'No FA') {
			$_tableMst[15]["label"] = $this->language->_('Bank City');
		}else{
		if($dataTx["PS_TYPE"]=='19' || $dataTx["PS_TYPE"]=='20' || $dataTx["PS_TYPE"]=='23' ){
				
			}else{
		$_tableMst[14]["label"] = $this->language->_('Nostro Bank');
			}
		}
		if($dataTx["PS_TYPE"]=='19' || $dataTx["PS_TYPE"]=='20' || $dataTx["PS_TYPE"]=='23' ){
				
			}else{
		$_tableMst[16]["label"] = $this->language->_('Country');
			}
		$_tableMst[17]["label"] = $this->language->_('Currency / Amount');
		if($dataTx["transfertype"] == 'FA' || $dataTx["transfertype"] == 'No FA') {

		}else{
			if($dataTx["PS_TYPE"]=='19' || $dataTx["PS_TYPE"]=='20' || $dataTx["PS_TYPE"]=='23' ){
				
			}else{
				$_tableMst[18]["label"] = $this->language->_('Rate');
			}
			//
			$_tableMst[19]["label"] = $this->language->_('Transfer Fee');
		}


		if($dataTx["PS_TYPE"]=='19' || $dataTx["PS_TYPE"]=='20' || $dataTx["PS_TYPE"]=='23' ){
				
			}else{
		if($dataTx["transfertype"] != 'No FA') {
			$_tableMst[20]["label"] = $this->language->_('Full Amount Fee');
		}elseif($dataTx["transfertype"] == 'FA'){

		}else if($dataTx["transfertype"] != 'FA' && $dataTx["transfertype"] != 'No FA'){
			$_tableMst[21]["label"] = $this->language->_('Provision Fee');
		}
		$_tableMst[22]["label"] = $this->language->_('Book Rate');
			}
		$_tableMst[23]["label"] = $this->language->_('Total');
		$_tableMst[24]["label"] = $this->language->_('Message');
		$_tableMst[25]["label"] = $this->language->_('Additional Message');
		$_tableMst[26]["label"] = $this->language->_('Status');
		$_tableMst[27]["label"] = $this->language->_('Response');
		if($dataTx["transfertype"]=='PB'){
			$dataTx["transfertype"] = 'Same Bank';
		}
		if($dataTx["PS_TYPE"]=='19'){
			$dataTx["transfertype"] = 'Cash Pooling - Same Bank Remain Balance';
		}else if($dataTx["PS_TYPE"]=='20'){
			$dataTx["transfertype"] = 'Cash Pooling - Same Bank Maintain Balance';
		}else if($dataTx["PS_TYPE"]=='23'){
			$dataTx["transfertype"] = 'Cash Pooling - Other Bank Remain Balance';
		}


	
		$_tableMst[0]["value"] = $dataTx["companycode"];
		$_tableMst[1]["value"] = $dataTx["companyname"];
		$_tableMst[2]["value"] = $dataTx["payref"];
		$_tableMst[3]["value"] = $dataTx["paymenttype"];
		$_tableMst[4]["value"] = $dataTx["transactionid"];
		$_tableMst[5]["value"] = $dataTx["transfertype"];
		$_tableMst[6]["value"] = $dataTx["pssubject"];
		$_tableMst[7]["value"] = Application_Helper_General::convertDate($dataTx['updateddate'], $this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		$_tableMst[8]["value"] = Application_Helper_General::convertDate($dataTx['transferdate'], $this->_dateViewFormat);
		$_tableMst[9]["value"] = $dataTx["acctsrc"];
		if($dataTx["transfertype"] == 'FA' || $dataTx["transfertype"] == 'No FA') {
			$_tableMst[10]["value"] = $dataTx["BENEFICIARY_ACCOUNT_NAME"];
			$_tableMst[11]["value"] = $dataTx["BENEFICIARY_ID_NUMBER"];
			$_tableMst[12]["value"] = $dataTx["BENEFICIARY_MOBILE_PHONE_NUMBER"];
		}else{
			$_tableMst[10]["value"] = $dataTx["beneacct"];
			$_tableMst[11]["value"] = $bankcode;
		}

		if($dataTx["transfertype"] == 'FA' || $dataTx["transfertype"] == 'No FA') {
			$app = Zend_Registry::get('config');
		$appBankname = $app['app']['bankname'];
			$_tableMst[13]["value"] = $appBankname.' - '.$dataTx["BANK_NAME"];
		}else{
		$_tableMst[13]["value"] = $dataTx["BANK_NAME"];
		}

		if($dataTx["transfertype"] == 'FA' || $dataTx["transfertype"] == 'No FA') {
			$_tableMst[15]["value"] = $dataTx["BENEFICIARY_BANK_CITY"];
		}else{
			if($dataTx["PS_TYPE"]=='19' || $dataTx["PS_TYPE"]=='20' || $dataTx["PS_TYPE"]=='23' ){
				
			}else{
			$_tableMst[14]["value"] = $dataTx["nostrobank"];
			}
		}
		if($dataTx["PS_TYPE"]=='19' || $dataTx["PS_TYPE"]=='20' || $dataTx["PS_TYPE"]=='23' ){
				
			}else{
		$_tableMst[16]["value"] = $dataTx["countryname"];
			}
		if($dataTx['PAYTYPE'] == '15'){
			$_tableMst[17]["value"] = $dataTx['ccy'].' '.Application_Helper_General::displayMoney($dataTx['traremain']);
		}else{
			$_tableMst[17]["value"] = $dataTx['ccy'].' '.Application_Helper_General::displayMoney($dataTx['traamount']);
		}
		if($dataTx['sourceccy']=='USD' && $dataTx['BENEFICIARY_ACCOUNT_CCY'] == 'USD'){
			$dataTx['RATE'] = '0.00';
			//$total = $dataTx['ccy'].' '.Application_Helper_General::displayMoney($dataTx['traamount']);
		}
		if($dataTx["transfertype"] == 'FA' || $dataTx["transfertype"] == 'No FA') {
		}else{
			
			if($dataTx["PS_TYPE"]=='19' || $dataTx["PS_TYPE"]=='20' || $dataTx["PS_TYPE"]=='23' ){
				
			}else{
				$_tableMst[18]["value"] = 'IDR '.Application_Helper_General::displayMoney($dataTx['RATE']);
			}
		$_tableMst[19]["value"] = $trffee;
		}
		
		if($dataTx["PS_TYPE"]=='19' || $dataTx["PS_TYPE"]=='20' || $dataTx["PS_TYPE"]=='23' ){
				
			}else{
if($dataTx["transfertype"] == 'FA' ) {
	$amttotal = $dataTx['traamount'] + $dataTx['fullamountfee'];
$total =	$dataTx['ccy'].' '.Application_Helper_General::displayMoney($amttotal);
}
if($dataTx["transfertype"] == 'No FA' ) {
	$amttotal = $dataTx['traamount'];
	$total = $dataTx['ccy'].' '.Application_Helper_General::displayMoney($amttotal);
}
if($dataTx["transfertype"] != 'No FA' ) {
		$_tableMst[20]["value"] = $ccytrfFA.' '.Application_Helper_General::displayMoney($dataTx['fullamountfee']);
	}
		if($dataTx["transfertype"] == 'FA' || $dataTx["transfertype"] == 'No FA') {

		}else{
		//$_tableMst[21]["value"] = $ccytrfpro.' '.Application_Helper_General::displayMoney($dataTx['provisionfee']);
		}
		$_tableMst[22]["value"] = 'IDR '.Application_Helper_General::displayMoney($dataTx['bookrate']);
			}
		$_tableMst[23]["value"] = $total;
		$_tableMst[24]["value"] = $dataTx["tramessage"];
		$_tableMst[25]["value"] = $dataTx["traaddmessage"];
		$_tableMst[26]["value"] = $dataTx["transferstatus"];
		$_tableMst[27]["value"] = $dataTx["bankresponse"];
		//var_dump($dataTx);
		if($dataTx["PS_TYPE"]=='38'){
			$this->view->bgnumb = $dataTx['TRA_MESSAGE'];
		}

		// $this->view->PSNUMBER 		= $PSNUMBER;
		$this->view->transactionid 	= $transactionid;
		$this->view->tableMst 	= $_tableMst;
		$this->view->pdf 		= ($pdf)? true: false;

//		 echo "<pre>";
//		 print_r($_tableMst);
//		 die;

		//--------------------- EXPORT PDF ------------------------
		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render('/detail/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Transaction Detail',$outputHTML);
			Application_Helper_General::writeLog('RPTX','Download PDF Detail Transaction Report');
		}
		// --------------- END OF EXPORT PDF ----------------------

		Application_Helper_General::writeLog('RPTX','View Detail Transaction Report');

	}
}
