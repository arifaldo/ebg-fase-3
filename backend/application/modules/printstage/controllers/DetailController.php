<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class Printstage_detailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');
        $this->view->userIdLogin = $this->_userIdLogin;
        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');

        $stamp_fee = $settings->getSetting('stamp_fee');
        $this->view->stamp_fee = $stamp_fee;
        $adm_fee = $settings->getSetting('adm_fee');
        $this->view->adm_fee = $adm_fee;

        $this->view->systemType = $system_type;
        $this->view->ProvFee = 2000000;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;


        // $numb = $this->_getParam('bgnumb');

        // decrypt numb
        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token     = $rand;
        $this->view->token = $sessionNamespace->token;

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        Application_Helper_General::writeLog('PSBG', "View Detail Printing Stage BG NUMBER : " . $BG_NUMBER);

        $this->view->encrypt_numb = urlencode($this->_getParam('bgnumb'));

        // others attachment

        $checkOthersAttachment = $this->_db->select()
            ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
            ->where("BG_REG_NUMBER = '$BG_NUMBER'")
            ->order('A.INDEX ASC')
            ->query()->fetchAll();

        if (count($checkOthersAttachment) > 0) {
            $this->view->othersAttachment = $checkOthersAttachment;
        }

        // end others attachment

        if (!empty($BG_NUMBER)) {

            $verifyData = $this->_db->select()
                ->from(array('A' => 'TEMP_BGVERIFY_DETAIL'), array('*'))
                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();
            //var_dump($verifyData);die;
            if (!empty($verifyData)) {
                foreach ($verifyData as $vl) {
                    //if()
                    $label = 'check' . $vl['INDEX'];
                    //var_dump($label);
                    $this->view->$label = true;
                    $labeldate = 'verify' . $vl['INDEX'];
                    $verifydate = Application_Helper_General::convertDate($vl['VERIFIED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                    $this->view->$labeldate = $verifydate . ' by ' . $vl['VERIFIEDBY'];
                }
            }

            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.SP_OBLIGEE_CODE = B.CUST_ID', array('B.CUST_NAME'))

                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->where('A.BG_STATUS = ?', 15)
                ->query()->fetchAll();

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))

                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();
            //$datas = $this->_request->getParams();
            //echo '<pre>';
            //var_dump($bgdata);
            //die;


            if (!empty($bgdata)) {

                $get_cash_collateral = $this->_db->select()
                    ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
                    ->where("CUST_ID = ?", "GLOBAL")
                    ->where("CHARGES_TYPE = ?", "10")
                    ->query()->fetchAll();

                $this->view->cash_collateral = $get_cash_collateral[0];

                $this->view->data = $bgdata[0];
                $data = $bgdata[0];

                $attahmentDestination = UPLOAD_PATH . '/document/submit/';

                $get_pdf = file_get_contents($attahmentDestination . $data["DOCUMENT_ID"] . ".pdf");
                // $get_pdf = file_get_contents($attahmentDestination . "96ec7bee-8ee9-4af8-b3d9-87db1700b7ad.pdf");
                // $get_total_pages = preg_match_all("/\/Count\W/", $get_pdf, $dummy);
                $get_total_pages = strstr($get_pdf, "Count");
                $get_total_pages = substr($get_total_pages, 0, 7);
                $get_total_pages = intval(explode(" ", $get_total_pages)[1]);

                $this->view->total_halaman_pdf = $get_total_pages;

                $getbuserlogin = $this->_db->select()
                    ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH', "BUSER_ID"))
                    ->joinLeft(["BR" => "M_BRANCH"], "A.BUSER_BRANCH = BR.ID")
                    ->where('A.BUSER_ID = ?', $this->_userIdLogin);
                $buser = $this->_db->fetchRow($getbuserlogin);

                $getbuserreleaser = $this->_db->select()
                    // ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH', "BUSER_ID"))
                    // ->joinLeft(["BR" => "M_BRANCH"], "A.BUSER_BRANCH = BR.ID")
                    ->from(["BR" => "M_BRANCH"])
                    ->where('BR.BRANCH_CODE = ?', $data["BG_BRANCH"]);
                $buserreleaser = $this->_db->fetchRow($getbuserreleaser);

                $changereleaser = false;
                $onlyprint = false;
                if ($buser["BRANCH_CODE"] == $buserreleaser["BRANCH_CODE"]) {
                    $changereleaser = true;
                } else {
                    $onlyprint = true;
                }
                $this->view->changereleaser = $changereleaser;
                $this->view->onlyprint = $onlyprint;

                $getBranchReleaser = $this->_db->select()
                    // ->from(["MBSR" => "M_BUSER"])
                    // ->joinLeft(["MB" => "M_BRANCH"], "MBSR.BUSER_BRANCH = MB.ID")
                    ->from(["MB" => "M_BRANCH"])
                    ->where("MB.BRANCH_CODE = ?", $data["BG_BRANCH"])
                    // ->where("MBSR.BUSER_ID = ?", $data["BG_UPDATEDBY"])
                    ->query()->fetchAll();

                $this->view->branchReleaser = $getBranchReleaser[0]["BRANCH_NAME"];
                $this->view->branchReleaserCode = $getBranchReleaser[0]["BRANCH_CODE"];

                if (!empty($data["BG_BRANCH_PUBLISHER"])) {
                    $getBranchPublisher = $this->_db->select()
                        ->from("M_BRANCH")
                        ->where("BRANCH_CODE = ?", $data["BG_BRANCH_PUBLISHER"])
                        ->query()->fetchAll();

                    $this->view->branchPublisher = $getBranchPublisher[0]["BRANCH_NAME"];
                    $this->view->branchPublisherCode = $getBranchPublisher[0]["BRANCH_CODE"];
                } else {
                    $getBranchPublisher = $this->_db->select()
                        ->from(["MB" => "M_BRANCH"])
                        // ->where("MBSR = ?", $data["BG_UPDATEDBY"])
                        ->where("MB.BRANCH_CODE = ?", $data["BG_BRANCH"])
                        ->query()->fetchAll();

                    $this->view->branchPublisher = $getBranchPublisher[0]["BRANCH_NAME"];
                    $this->view->branchPublisherCode = $getBranchPublisher[0]["BRANCH_CODE"];
                }

                $principleData = [];
                // if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
                foreach ($bgdatadetail as $key => $value) {
                    $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
                }

                $this->view->principleData = $principleData;
                // }

                if (!empty($principleData["SLIK OJK Document"])) {
                    $temp_slikojk = explode("_", $principleData["SLIK OJK Document"]);
                    $new_temp_name = $temp_slikojk[0] . "_" . $temp_slikojk[1] . "_" . $temp_slikojk[2] . "_" . $temp_slikojk[3] . "_";

                    $this->view->slikojk = str_replace($new_temp_name, "", $principleData["SLIK OJK Document"]);
                }

                if (!empty($data["KUASA_DIREKSI_FILE"])) {
                    $temp_suratpermohonan = explode("_", $data["KUASA_DIREKSI_FILE"]);
                    $new_temp_name = $temp_suratpermohonan[0] . "_" . $temp_suratpermohonan[1] . "_" . $temp_suratpermohonan[2] . "_" . $temp_suratpermohonan[3] . "_";

                    $this->view->suratpermohonan = str_replace($new_temp_name, "", $data["KUASA_DIREKSI_FILE"]);
                }

                if (!empty($data["AGREE_FORMAT"])) {
                    $temp_agreeformat = explode("_", $data["AGREE_FORMAT"]);
                    $new_temp_name = $temp_agreeformat[0] . "_" . $temp_agreeformat[1] . "_" . $temp_agreeformat[2] . "_" . $temp_agreeformat[3] . "_";

                    $this->view->agreeformat = str_replace($new_temp_name, "", $data["AGREE_FORMAT"]);
                }

                if (!empty($data["MEMO_LEGAL"])) {
                    $temp_memolegal = explode("_", $data["MEMO_LEGAL"]);
                    $new_temp_name = $temp_memolegal[0] . "_" . $temp_memolegal[1] . "_" . $temp_memolegal[2] . "_" . $temp_memolegal[3] . "_";

                    $this->view->memolegal = str_replace($new_temp_name, "", $data["MEMO_LEGAL"]);
                }

                if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {
                    $this->view->isinsurance = true;
                }

                switch ($bgdata[0]["CHANGE_TYPE"]) {
                    case '0':
                        $this->view->suggestionType = "New";
                        break;
                    case '1':
                        $this->view->suggestionType = "Amendment Changes";
                        break;
                    case '2':
                        $this->view->suggestionType = "Amendment Draft";
                        break;
                }

                // Get data T_BANK_GUARANTEE_HISTORY
                $dataHistory = $this->_db->select()
                    ->from(
                        array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
                        array('*')
                    )
                    ->where('TBGH.BG_REG_NUMBER = ?', $BG_NUMBER);
                $dataHistory = $this->_db->fetchAll($dataHistory);

                $this->view->dataHistory = $dataHistory;

                $config = Zend_Registry::get('config');
                $historyStatusCode = $config["history"]["status"]["code"];
                $historyStatusDesc = $config["history"]["status"]["desc"];
                $historyStatusArr = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

                $this->view->historyStatusArr   = $historyStatusArr;



                $config     = Zend_Registry::get('config');
                $Bgcomplationdesc     = $config["bg"]["complation"]["desc"];
                $Bgcomplationcode     = $config["bg"]["complation"]["code"];

                $arrComplation = array_combine(array_values($Bgcomplationcode), array_values($Bgcomplationdesc));

                $this->view->arrComplation = $arrComplation;

                $selectcomp = $this->_db->select()
                    ->from(array('A' => 'M_CUSTOMER'), array('*'))
                    //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                    ->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
                    ->where('A.CUST_ID =' . $this->_db->quote((string)$bgdata['0']['CUST_ID']))
                    ->query()->fetchAll();

                $this->view->compinfo = $selectcomp[0];

                // kontra garansi dan provision fee jika kontra garansi insurance -----------

                $provision_insurance = $this->_db->select()
                    ->from(["A" => "M_CUST_LINEFACILITY"], ["FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
                    ->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
                    ->query()->fetchAll();

                if ($data["COUNTER_WARRANTY_TYPE"] == "3") $this->view->provision_insurance = $provision_insurance[0];

                // --------------------------------------------------------------------------


                // QR CODE --------------------------------------------------------------

                $this->view->qrposition = [
                    "ds" => "Jangan Tampilkan",
                    "top-left" => "Atas Kiri",
                    "top-right" => "Atas Kanan",
                    "bottom-left" => "Bawah Kiri",
                    "bottom-right" => "Bawah Kanan",
                ];

                $size = [];
                for ($i = 15; $i <= 40; $i++) {
                    $size[strval($i)] = $i;
                }

                $this->view->size = $size;

                // end QR CODE ----------------------------------------------------------

                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))
                    ->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
                    ->where("C.REG_NUMBER = ?", (string) $BG_NUMBER);
                // ->where("C.GROUP = ?" , (string)$value);
                // echo $selectapprover;die;
                $usergroup = $this->_db->fetchAll($selectapprover);

                if (!empty($usergroup)) {
                    // die;
                    // $this->view->pdf = true;
                    $this->view->validbtn = false;
                } else {
                    if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
                        $boundary = $this->validatebtn('50', $bgdata['0']['BG_AMOUNT'], 'IDR', $BG_NUMBER);
                    } else {
                        $boundary = $this->validatebtn('51', $bgdata['0']['BG_AMOUNT'], 'IDR', $BG_NUMBER);
                    }
                    if ($boundary) {
                        // die;
                        $this->view->validbtn = false;
                    } else {
                        // die;
                        $this->view->validbtn = true;
                    }
                }
                if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
                    $policyBoundary = $this->findPolicyBoundary(50, $bgdata['0']['BG_AMOUNT']);
                    $approverUserList = $this->findUserBoundary(50, $bgdata['0']['BG_AMOUNT']);
                } else {
                    $policyBoundary = $this->findPolicyBoundary(51, $bgdata['0']['BG_AMOUNT']);
                    $approverUserList = $this->findUserBoundary(51, $bgdata['0']['BG_AMOUNT']);
                }
                //echo '<pre>';
                //var_dump($policyBoundary);die;
                $this->view->policyBoundary = $policyBoundary;


                // get linefacillity

                $get_linefacility = $this->_db->select()
                    ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_ID"])
                    ->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
                    ->query()->fetchAll();

                $get_bgamount_on_risks = $this->_db->select()
                    ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
                    ->where("BG_STATUS IN (?)", ["15", "16"])
                    ->where("COUNTER_WARRANTY_TYPE = ?", "2")
                    ->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
                    ->query()->fetchAll();

                $total_bgamount_on_risk = 0;

                foreach ($get_bgamount_on_risks as $key => $get_bgamount_on_risk) {
                    $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
                }

                $total_bgamount_on_temp = 0;

                $get_bgamount_on_temps = $this->_db->select()
                    ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
                    ->where("COUNTER_WARRANTY_TYPE = '2'")
                    ->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
                    ->query()->fetchAll();

                foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
                    $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
                }

                $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
                $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

                $this->view->linefacility = $get_linefacility[0];

                // end get linefacility


                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

                $this->view->BG_PUBLISH = $arrbgpublish[$data['BG_PUBLISH']];

                if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == "3") {
                    $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
                    $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

                    $insuranceBranch = $this->_db->select()
                        ->from("M_INS_BRANCH")
                        ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
                        ->query()->fetchAll();

                    $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
                    $this->view->insuranceAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];
                }

                if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
                    $bgdatasplit = $this->_db->select()
                        ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->joinLeft(["MCA" => "M_CUSTOMER_ACCT"], "A.ACCT = MCA.ACCT_NO", [])
                        ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                        ->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
                        ->where("MCA.ACCT_DESC NOT LIKE ?", "%giro%")
                        ->query()->fetchAll();

                    $bgdataescrow = $this->_db->select()
                        ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                        ->where("A.ACCT_DESC LIKE ?", "%escrow%")
                        ->query()->fetchAll();

                    $bgdatasplit = array_merge($bgdatasplit, $bgdataescrow);

                    foreach ($bgdatasplit as $key => $value) {
                        $temp_save = $this->_db->select()
                            ->from("M_CUSTOMER_ACCT")
                            ->where("ACCT_NO = ?", $value["ACCT"])
                            ->query()->fetchAll();

                        $bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
                        $bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
                    }

                    $this->view->fullmember = $bgdatasplit;
                }



                $selectHistory    = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $BG_NUMBER);

                $history = $this->_db->fetchAll($selectHistory);

                $cust_approver = 1;

                $bg_submission_hisotrys = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                    ->where("CUST_ID IN (?) ", [$selectcomp[0]["CUST_ID"], $bgdata[0]["BG_INSURANCE_CODE"]]);

                $bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

                $getBoundary = $this->_db->select()
                    ->from('M_APP_BOUNDARY')
                    ->where('CUST_ID = ?', $data['CUST_ID'])
                    ->where('BOUNDARY_MIN <= \'' . $data['BG_AMOUNT'] . '\'')
                    ->where('BOUNDARY_MAX >= \'' . $data['BG_AMOUNT'] . '\'')
                    ->query()->fetch();

                $checkBoundary = strpos($getBoundary['POLICY'], 'THEN');
                if ($checkBoundary === false) $checkBoundary = strpos($getBoundary['POLICY'], 'AND');
                $checkCount = 1;

                foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {

                    // maker
                    if ($bg_submission_hisotry['HISTORY_STATUS'] == 1) {
                        $makerStatus = 'active';
                        $makerIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['USER_FULLNAME'];
                        // $custFullname = $customer[0]['USER_ID'];

                        $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                        $align = 'align="center"';

                        $this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }

                    // approver
                    if ($checkBoundary === false) {
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
                            $approverStatus = 'active';
                            $approverIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custFullname = $customer[0]['USER_ID'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

                            $this->view->approverStatus = $approverStatus;
                        }
                    } else {
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
                            $approverStatus = 'active';
                            $approverIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            // $custFullname = $customer[0]['USER_FULLNAME'];
                            if ($checkCount === 1) {
                                $custFullname = $customer[0]['USER_FULLNAME'];
                                $checkCount++;
                            } else {
                                $custFullname = $custFullname . "<br><span>" . $customer[0]['USER_FULLNAME'] . "</span>";
                                $checkCount = 1;
                            }

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

                            $this->view->approverStatus = $approverStatus;
                        }
                    }

                    if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
                        //if releaser
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 8 || $bg_submission_hisotry['HISTORY_STATUS'] == 23) {
                            $releaserStatus = 'active';
                            $releaserIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custFullname = $customer[0]['USER_ID'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                            $this->view->releaserStatus = $releaserStatus;
                        }
                    } else {
                        //if releaser
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5) {
                            $releaserStatus = 'active';
                            $releaserIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custFullname = $customer[0]['USER_ID'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                            $this->view->releaserStatus = $releaserStatus;
                        }
                    }


                    // if ($bg_submission_hisotry['HISTORY_STATUS'] == 4 || $bg_submission_hisotry['HISTORY_STATUS'] == 14) {
                    // 	$releaserStatus = 'active';
                    // 	$releaserIcon = '<i class="fas fa-check"></i>';

                    // 	$custlogin = $bg_submission_hisotry['USER_LOGIN'];

                    // 	$selectCust	= $this->_db->select()
                    // 		->from('M_USER')
                    // 		->where("USER_ID = ?", $custlogin)
                    // 		->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                    // 	$customer = $this->_db->fetchAll($selectCust);

                    // 	$custFullname = $customer[0]['USER_FULLNAME'];

                    // 	$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                    // 	$align = 'align="center"';

                    // 	$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span></span></div>';
                    // 	$this->view->releaserStatus = $releaserStatus;
                    // }
                }

                foreach ($history as $row) {

                    if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
                        if ($row['HISTORY_STATUS'] == 8 || $row['HISTORY_STATUS'] == 24) {
                            $makerStatus = 'active';
                            $insuranceIcon = '<i class="fas fa-check"></i>';
                            /*$approveStatus = 'active';
							$reviewStatus = 'active';
							$releaseStatus = 'active';
							$releaseIcon = '<i class="fas fa-check"></i>';*/
                            $insuranceStatus = 'active';
                            $reviewStatus = '';
                            //$releaseStatus = '';
                            //$releaseIcon = '';

                            $makerOngoing = '';
                            $reviewerOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = '';

                            $custlogin = $row['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin);
                            //->where("CUST_ID = ?", $row['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            // $custFullname = $customer[0]['USER_ID'];
                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custEmail 	  = $customer[0]['USER_EMAIL'];
                            // $custPhone	  = $customer[0]['USER_PHONE'];

                            $insuranceApprovedBy = $custFullname;

                            $align = 'align="center"';
                            $marginLeft = '';
                            if ($cust_reviewer == 0 && $cust_approver == 0) {
                                $align = '';
                                $marginLeft = 'style="margin-left: 15px;"';
                            }

                            $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                            $this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                        }
                    }

                    if ($row['HISTORY_STATUS'] == 6) {
                        $verifyStatus = 'active';
                        $verifyIcon = '<i class="fas fa-check"></i>';

                        $verifyOngoing = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = 'ongoing';
                            $releaserOngoing = '';
                        } else {
                            //$reviewerOngoing = 'ongoing';
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            $releaserOngoing = '';
                        }

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

                        $align = 'align="center"';
                        $marginRight = '';

                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginRight = 'style="margin-right: 15px;"';
                        }

                        $this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
                    }

                    //if reviewer done
                    if ($row['HISTORY_STATUS'] == 7) {
                        $bankReviewStatus = "active";
                        $bankReviewOngoing = "ongoing";
                        $bankReviewIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        $this->view->bankReviewedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }

                    if ($row['HISTORY_STATUS'] == 14) {
                        $releaseStatus = "active";
                        $releaseOngoing = "ongoing";
                        $releaseIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

                        $getApprover = $this->_db->select()
                            ->from(['TBA' => 'T_BGAPPROVAL'], ['*'])
                            ->joinLeft(['MB' => 'M_BUSER'], 'TBA.USER_ID = MB.BUSER_ID', ['BUSER_NAME'])
                            ->where('REG_NUMBER = ?', $bgdata['0']['BG_REG_NUMBER'])
                            ->query()->fetchAll();

                        $approverText = '';
                        foreach ($getApprover as $userApprove) {
                            # code...
                            $approverText .= '<div ' . $align . ' class="textTheme">' . date('d-M-Y', strtotime($userApprove['CREATED'])) . '<br><span ' . $marginRight . '>' . $userApprove['BUSER_NAME'] . '</span></div>';
                        }

                        $this->view->approverApprovedBy = $approverText;
                    }

                    if ($row['HISTORY_STATUS'] == 17) {
                        $signingStatus = "active";
                        $signingOngoing = "ongoing";
                        $signingIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        $this->view->signingPreparedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }

                    //if releaser done
                    // if ($row['HISTORY_STATUS'] == 5) {
                    //     $makerStatus = 'active';
                    //     /*$approveStatus = 'active';
                    // 	$reviewStatus = 'active';
                    // 	$releaseStatus = 'active';
                    // 	$releaseIcon = '<i class="fas fa-check"></i>';*/
                    //     $approveStatus = '';
                    //     $reviewStatus = '';
                    //     $releaseStatus = '';
                    //     $releaseIcon = '';

                    //     $makerOngoing = '';
                    //     $reviewerOngoing = '';
                    //     $approverOngoing = '';
                    //     $releaserOngoing = '';

                    //     $custlogin = $row['USER_LOGIN'];

                    //     $selectCust    = $this->_db->select()
                    //         ->from('M_USER')
                    //         ->where("USER_ID = ?", $custlogin)
                    //         ->where("CUST_ID = ?", $row['CUST_ID']);

                    //     $customer = $this->_db->fetchAll($selectCust);

                    //     $custFullname = $customer[0]['USER_FULLNAME'];
                    //     // $custEmail 	  = $customer[0]['USER_EMAIL'];
                    //     // $custPhone	  = $customer[0]['USER_PHONE'];

                    //     $releaserApprovedBy = $custFullname;

                    //     $align = 'align="center"';
                    //     $marginLeft = '';
                    //     if ($cust_reviewer == 0 && $cust_approver == 0) {
                    //         $align = '';
                    //         $marginLeft = 'style="margin-left: 15px;"';
                    //     }

                    //     $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                    //     // $this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    // }
                }

                // TTD BANK
                $getDocumentId = $data["DOCUMENT_ID"];
                $getSigner = $this->_db->select()
                    ->from("T_BANK_GUARANTEE_SIGNER")
                    ->where("DOCUMENT_ID = ?", $getDocumentId)
                    ->order("SIGNER_TYPE ASC")
                    ->query()->fetchAll();

                $getAllSignerDetail = $this->_db->select()
                    ->from("M_BUSER")
                    ->where("BUSER_ID IN (?)", [$data['TTD1_USERID'], $data['TTD2_USERID']])
                    ->query()->fetchAll();


                $signedDateTime = date('d-M-Y', strtotime($getSigner[1]["SIGNER_DATE"]));

                $this->view->signedBy = '<div ' . $align . ' class="textTheme">' . $signedDateTime . '<br><span>' . $getAllSignerDetail[0]["BUSER_NAME"] . '</span><br><span>' . $getAllSignerDetail[1]["BUSER_NAME"] . '</span></div>';

                //approvernamecircle jika sudah ada yang approve
                if (!empty($userid)) {

                    $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

                    $flipAlphabet = array_flip($alphabet);

                    $approvedNameList = array();
                    $i = 0;
                    //var_dump($userid);die;
                    foreach ($userid as $key => $value) {

                        //select utk nama dan email
                        $selectusername = $this->_db->select()
                            ->from(array('M_BUSER'), array(
                                '*'
                            ))
                            ->where("BUSER_ID = ?", (string) $value);

                        $username = $this->_db->fetchAll($selectusername);

                        //select utk cek user berada di grup apa
                        $selectusergroup    = $this->_db->select()
                            ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
                                '*'
                            ))

                            ->where("C.BUSER_ID 	= ?", (string) $value);

                        $usergroup = $this->_db->fetchAll($selectusergroup);

                        $groupuserid = $usergroup[0]['GROUP_BUSER_ID'];
                        $groupusername = $usergroup[0]['BUSER_ID'];
                        $groupuseridexplode = explode("_", $groupuserid);

                        if ($groupuseridexplode[0] == "S") {
                            $usergroupid = "SG";
                        } else {
                            $usergroupid = $alphabet[$groupuseridexplode[2]];
                        }

                        // $tempuserid = "";
                        // foreach ($approverNameCircle as $row => $data) {
                        // 	foreach ($data as $keys => $val) {
                        // 		if ($keys == $usergroupid) {
                        // 			if (preg_match("/active/", $val)) {
                        // 				continue;
                        // 			}else{
                        // 				if ($groupuserid == $tempuserid) {
                        // 					continue;
                        // 				}else{
                        // 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
                        // 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
                        // 				}
                        // 				$tempuserid = $groupuserid;
                        // 			}
                        // 		}
                        // 	}
                        // }

                        array_push($approvedNameList, $username[0]['BUSER_NAME']);

                        $efdate = $approveEfDate[$i];

                        $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['BUSER_NAME'] . ' (' . $usergroupid . ')</div>';
                        $i++;
                    }

                    $this->view->approverApprovedBy = $approverApprovedBy;


                    //kalau sudah approve semua
                    if (!$checkBoundary) {
                        $approveStatus = '';
                        $approverOngoing = '';
                        $approveIcon = '';
                        $releaserOngoing = 'ongoing';
                    }
                }



                $selectsuperuser = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'))
                    ->where("C.REG_NUMBER 	= ?", $BG_NUMBER)
                    ->where("C.GROUP 	= 'SG'");

                $superuser = $this->_db->fetchAll($selectsuperuser);

                if (!empty($superuser)) {
                    $userid = $superuser[0]['USER_ID'];

                    //select utk nama dan email
                    $selectusername = $this->_db->select()
                        ->from(array('M_BUSER'), array(
                            '*'
                        ))
                        ->where("BUSER_ID = ?", (string) $userid);

                    $username = $this->_db->fetchAll($selectusername);

                    $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['BUSER_NAME'] . ' (' . $usergroupid . ')</div>';

                    $approveStatus = '';
                    $approverOngoing = '';
                    $approveIcon = '';
                    $releaserOngoing = 'ongoing';
                }
                // <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
                //define circle
                $makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . '" disabled>' . $makerIcon . ' </button>';

                $insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . '" disabled>' . $insuranceIcon . ' </button>';

                foreach ($reviewerList as $key => $value) {

                    $textColor = '';
                    if ($value == $reviewerApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }
                // 
                $bankReviewNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $bankReviewStatus . '" disabled>' . $bankReviewIcon . ' </button>';

                $groupNameList = $approverUserList['GROUP_NAME'];
                unset($approverUserList['GROUP_NAME']);

                if ($approverUserList != '') {
                    //echo '<pre>';
                    //var_dump($approverUserList);die;
                    foreach ($approverUserList as $key => $value) {
                        $approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
                        $i = 1;
                        foreach ($value as $key2 => $value2) {

                            $textColor = '';
                            if (in_array($value2, $approvedNameList)) {
                                $textColor = 'text-white-50';
                            }

                            if ($i == count($value)) {
                                $approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
                            } else {
                                $approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
                            }
                            $i++;
                        }
                    }
                } else {
                    $approverListdata = 'There is no Approver User';
                }
                // 
                $spandata = '';
                if (!empty($approverListdata) && !$error_msg2) {
                    $spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
                }

                $approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . '" disabled>' . $approveIcon . '
						' . $spandata . '
					</button>';

                foreach ($releaserList as $key => $value) {

                    $textColor = '';
                    if ($value == $releaserApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }
                // 
                $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . '" disabled>' . $releaseIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span> </button>';

                $verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $verifyStatus . ' ' . $verifyOngoing . '" disabled>' . $verifyIcon . ' </button>';


                $this->view->policyBoundary = $policyBoundary;
                $this->view->makerNameCircle = $makerNameCircle;
                $this->view->bankReviewNameCircle = $bankReviewNameCircle;
                $this->view->approverNameCircle = $approverNameCircle;
                // $this->view->releaserNameCircle = $releaserNameCircle;
                $this->view->bankApproveNameCircle = $releaserNameCircle;
                $this->view->verifyNameCircle = $verifyNameCircle;
                $this->view->insuranceNameCircle = $insuranceNameCircle;

                $this->view->verifyStatus = $verifyStatus;
                $this->view->makerStatus = $makerStatus;
                $this->view->approveStatus = $approveStatus;
                $this->view->reviewStatus = $reviewStatus;
                $this->view->releaseStatus = $releaseStatus;


                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                $this->view->arrbgType = $arrbgType;

                $data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }


                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


                $config            = Zend_Registry::get('config');
                $BgType         = $config["bg"]["status"]["desc"];
                $BgCode         = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

                $this->view->arrStatus = $arrStatus;

                $comDesc         = $config["bg"]["complation"]["desc"];
                $comCode         = $config["bg"]["complation"]["code"];

                $comStatus = array_combine(array_values($comCode), array_values($comDesc));

                $this->view->comStatus = $comStatus;

                $CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);
                $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                $AccArr = $CustomerUser->getAccounts($param);
                //var_dump($AccArr);die;

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                $this->view->bankFormatNumber = $data['BG_FORMAT'];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                // $arrWaranty = array(
                // 	1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                // 	2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                // 	3 => 'Insurance'

                // );

                // $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

                // if (!empty($data['USAGE_PURPOSE'])) {
                //     $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                //     foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                //         $str = 'checkp' . $val;
                //         //var_dump($str);
                //         $this->view->$str =  'checked';
                //     }
                // }

                $this->view->usage_purpose = $data['USAGE_PURPOSE'];

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                } else {
                    $data["BG_NUMBER"] = substr($data["BG_NUMBER"], 0, 3) . "-" . substr($data["BG_NUMBER"], 3, 6) . "-" . substr($data["BG_NUMBER"], 9, 5) . "-" . substr($data["BG_NUMBER"], 14);
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];
                $this->view->data = $data;
                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->contact_person = $data['RECIPIENT_CP'];
                $this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->contact_email = $data['RECIPIENT_EMAIL'];


                // $obligeename = $this->_db->select()
                // ->from('M_CUSTOMER', array('CUST_NAME'
                // ))
                // ->where('CUST_ID = ?',$data['SP_OBLIGEE_CODE']);

                // $custname = $db->fetchRow($obligeename);


                $this->view->obligee_name = $data['CUST_NAME'];

                $this->view->obligee_code = $data['SP_OBLIGEE_CODE'];

                $this->view->comment = $data['SERVICE'];

                $this->view->fileName = $data['FILE'];
                $this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                $this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];
                //$this->view->GT_TYPE = $data['GT_DOC_TYPE'];
                $this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

                $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];


                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];
                $Settings = new Settings();
                $claim_period = $Settings->getSetting('claim_period');
                $this->view->claim_period = $claim_period;

                if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                                a.USER_LOGIN,
                                b.`USER_FULLNAME` AS u_name,
                                c.`BUSER_NAME` AS b_name,
                                a.DATE_TIME,
                                a.BG_REASON,
                                a.HISTORY_STATUS,
                                a.BG_REG_NUMBER
                                
                                
                              FROM
                                T_BANK_GUARANTEE_HISTORY AS a
                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    $result =  $this->_db->fetchAll($selectQuery);
                    if (!empty($result)) {
                        $data['REASON'] = $result['0']['BG_REASON'];
                    }
                    $this->view->reqrepair = true;
                    $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }

                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];

                // Marginal Deposit View ------------------------
                // Marginal Deposit Principal ---------------
                $numb = $data['BG_REG_NUMBER'];
                $bgdatadetailmd = $this->_db->select()
                    ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                if (!empty($bgdatadetailmd)) {
                    foreach ($bgdatadetailmd as $key => $value) {
                        if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
                            $this->view->marginalDepositPercentage =   $value['PS_FIELDVALUE'];
                        }
                    }
                }
                // Marginal Deposit Principal ---------------

                // Marginal Deposit Eksisting ---------------
                $bgdatamdeks = $this->_db->select()
                    // ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                    // ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamdeks = $bgdatamdeks;
                // Marginal Deposit Eksisting ---------------
                // Marginal Deposit View ------------------------

                if ($this->_request->isPost()) {

                    $kirim = $this->_request->getParam("kirim");
                    $get_all_param = $this->_request->getParams();


                    if ($get_all_param["cetak_status"] == "1") {
                        // bila status cetak berhasil
                    } else {
                        // bila status cetak gagal
                    }

                    if ($kirim) {
                        $this->_db->update("T_BANK_GUARANTEE", [
                            "BG_BRANCH_PUBLISHER" => $this->_request->getParam("save_bg_publisher")
                        ], [
                            "BG_REG_NUMBER = ?" => $BG_NUMBER
                        ]);
                    } else {
                        $this->_db->update("T_BANK_GUARANTEE", [
                            "COMPLETION_STATUS" => "3",
                            "BG_UPDATED" => new Zend_Db_Expr('now()')
                        ], [
                            "BG_REG_NUMBER = ?" => $BG_NUMBER
                        ]);
                    }

                    $this->setbackURL('/' . $this->_request->getModuleName() . '/list/');
                    Application_Helper_General::writeLog('PSBG', "Printing Stage");
                    return $this->_redirect('/notification/success/index');
                }
            }
        }
    }


    public function validateapproval($policy, $list, $bgnumb)
    {
        die('a');
        $command = ' ' . $policy . ' ';
        $command = strtoupper($command);

        $cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
        var_dump($cleanCommand);
        $commandspli = explode('THEN', $cleanCommand);
        $commandnew = '';
        foreach ($commandspli as $ky => $valky) {
            if ($commandspli[$ky + 1] != '') {
                $commandnew .= '(' . $valky . ') THEN ';
            } else {
                $commandnew .= '(' . $valky . ')';
            }
        }

        //transform to php logical operator syntak
        $translate = array(
            'AND' => '&&',
            'OR' => '||',
            'THEN' => '&&',
            'A' => '$A',
            'B' => '$B',
            'C' => '$C',
            'D' => '$D',
            'E' => '$E',
            'F' => '$F',
            'G' => '$G',
            'H' => '$H',
            'I' => '$I',
            'J' => '$J',
            'K' => '$K',
            'L' => '$L',
            'M' => '$M',
            'N' => '$N',
            'O' => '$O',
            'P' => '$P',
            'Q' => '$Q',
            'R' => '$R',
            // 'S' => '$S',
            'T' => '$T',
            'U' => '$U',
            'V' => '$V',
            'W' => '$W',
            'X' => '$X',
            'Y' => '$Y',
            'Z' => '$Z',
            'SG' => '$SG',
        );

        $phpCommand =  strtr($commandnew, $translate);

        $param = array(
            '0' => '$A',
            '1' => '$B',
            '2' => '$C',
            '3' => '$D',
            '4' => '$E',
            '5' => '$F',
            '6' => '$G',
            '7' => '$H',
            '8' => '$I',
            '9' => '$J',
            '10' => '$K',
            '11' => '$L',
            '12' => '$M',
            '13' => '$N',
            '14' => '$O',
            '15' => '$P',
            '16' => '$Q',
            '17' => '$R',
            // '18' => '$S',
            '19' => '$T',
            '20' => '$U',
            '21' => '$V',
            '22' => '$W',
            '23' => '$X',
            '24' => '$Y',
            '25' => '$Z',
            '26' => '$SG',
        );
        // print_r($phpCommand);die;
        // function str_replace_first($from, $to, $content,$row)
        // {
        //     $from = '/'.preg_quote($from, '/').'/';
        //     return preg_replace($from, $to, $content, $row);
        // }

        $command = str_replace('(', '', $policy);
        $command = str_replace(')', '', $command);
        $list = explode(' ', $command);
        // print_r($list);die;
        $approver = array();
        foreach ($list as $key => $value) {
            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))

                    ->where("C.REG_NUMBER = ?", (string) $bgnumb)
                    ->where("C.GROUP = ?", (string) $value);
                // echo $selectapprover;die;
                $usergroup = $this->_db->fetchAll($selectapprover);
                // print_r($usergroup);
                $approver[$value] = $usergroup;
            }
        }
        // print_r($approver);die;
        foreach ($param as $url) {
            if (strpos($phpCommand, $url) !== FALSE) {
                $ta = substr_count($phpCommand, $url);
                // print_r($list);die;

                if (!empty($approver)) {
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                            foreach ($approver[$value] as $row => $val) {
                                if (!empty($val)) {
                                    $values = 'G' . $value;
                                    ${$values}[$row + 1] = true;
                                }
                            }
                        }
                    }
                }
                for ($i = 1; $i <= $ta; $i++) {
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                            $values = 'G' . $value;
                            if (empty(${$values}[$i])) {
                                ${$values}[$i] = false;
                            }
                        }
                    }

                    $BG_NUMBER = $i;
                    $label = str_replace('$', '$G', $url);

                    $replace = $label . '[' . $BG_NUMBER . ']';

                    $alf = str_replace('$', '', $url);
                    $values = 'G' . $alf;

                    if (${$values}[$i] == $replace) {
                        $phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
                        //	 print_r($phpCommand);
                    } else {
                        $phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
                        //	 print_r($phpCommand);
                    }
                }
            }
        }
        $keywords = preg_split("/[\s,]+/", $cleanCommand);
        $result =  false;
        //print_r($phpCommand);
        eval('$result = ' . "$phpCommand;");
        //var_dump ($result);die;
        return $result;
    }



    public function findPolicyBoundary($transfertype, $amount)
    {


        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BGBOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        //echo $selectuser;die;
        $datauser = $this->_db->fetchAll($selectuser);

        return $datauser[0]['POLICY'];
    }

    //return tombol jika blm ada yg approve
    public function findUserBoundary($transfertype, $amount)
    {



        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        //echo $selectuser;die();
        $datauser = $this->_db->fetchAll($selectuser);

        $command = str_replace('(', '', $datauser[0]['POLICY']);
        $command = str_replace(')', '', $command);
        $command = $command . ' SG';
        $list = explode(' ', $command);

        $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

        $flipAlphabet = array_flip($alphabet);

        foreach ($list as $row => $data) {
            foreach ($alphabet as $key => $value) {
                if ($data == $value) {
                    $groupuser[] = $flipAlphabet[$data];
                }
            }
        }

        $uniqueGroupUser = array_unique($groupuser);

        foreach ($uniqueGroupUser as $key => $value) {
            if ($value == '27') {
                $selectGroupName    = $this->_db->select()
                    ->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
                    ->where("C.GROUP_BUSER_ID LIKE ?", '%S_%');
            } else {
                $selectGroupName    = $this->_db->select()
                    ->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
                    ->where("C.GROUP_BUSER_ID LIKE ?", '%_' . $value . '%');
            }

            $groupNameList = $this->_db->fetchAll($selectGroupName);

            array_unique($groupNameList[0]);

            $uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
        }

        foreach ($uniqueGroupName as $row => $data) {
            foreach ($alphabet as $key => $value) {
                if ($row == $key) {
                    $newUniqueGroupName[$value] = $data;
                }
            }
        }

        foreach ($groupuser as $key => $value) {

            //if special group
            if ($value == 27) {
                $likecondition = "S_%";
            } else {
                $likecondition = "%_" . $value . "%";
            }

            $selectgroup = $this->_db->select()
                ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
                    'BUSER_ID'
                ))

                ->where("C.GROUP_BUSER_ID LIKE ?", (string) $likecondition);

            $group_user = $this->_db->fetchAll($selectgroup);

            $groups[][$alphabet[$value]] = $group_user;
        }
        //	echo '<pre>';
        //var_dump($groups);
        $tempGroup = array();
        foreach ($groups as $key => $value) {

            foreach ($value as $data => $values) {

                foreach ($values as $row => $val) {
                    $userid = $val['BUSER_ID'];

                    $selectusername = $this->_db->select()
                        ->from(array('M_BUSER'), array(
                            '*'
                        ))

                        ->where("BUSER_ID = ?", (string) $userid);
                    //echo $selectusername;echo ' ';
                    $username = $this->_db->fetchAll($selectusername);

                    if (!in_array($data, $tempGroup)) {
                        $userlist[$data][] = $username[0]['BUSER_NAME'];
                    }
                }

                array_push($tempGroup, $data);

                // $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
                // 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
            }
        }

        $userlist['GROUP_NAME'] = $newUniqueGroupName;

        return $userlist;
    }




    public function validatebtn($transfertype, $amount, $ccy, $psnumb)
    {
        //die;


        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BGBOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        // echo $selectuser;

        $datauser = $this->_db->fetchAll($selectuser);
        if (empty($datauser)) {

            return true;
        }

        $selectusergroup    = $this->_db->select()
            ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
                '*'
            ))

            ->where("C.BUSER_ID 	= ?", (string) $this->_userIdLogin);

        $usergroup = $this->_db->fetchAll($selectusergroup);




        $this->view->boundarydata = $datauser;
        //var_dump($usergroup);die;
        // print_r($this->view->boundarydata);die;
        if (!empty($usergroup)) {
            $cek = false;

            foreach ($usergroup as $key => $value) {
                $group = explode('_', $value['GROUP_BUSER_ID']);
                $alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
                $groupalfa = $alphabet[(int) $group[2]];
                // print_r($groupalfa);echo '-';
                $usergroup[$key]['GROUP'] = $groupalfa;
                //var_dump($usergroup);
                // print_r($datauser);die;
                foreach ($datauser as $nub => $val) {
                    $command = str_replace('(', '', $val['POLICY']);
                    $command = str_replace(')', '', $command);
                    $list = explode(' ', $command);

                    //var_dump($list);
                    foreach ($list as $row => $data) {

                        if ($data == $groupalfa) {
                            $cek = true;
                            // die('ter');
                            break;
                        }
                    }
                }
            }
            //die;



            if ($group[0] == 'S') {
                $cek = true;
            }
            // echo $cek;
            // print_r($cek);die;
            if (!$cek) {
                // die('here');
                return false;
            }
        }
        $tempusergroup = $usergroup;

        if ($cek) {




            $command = ' ' . $datauser['0']['POLICY'] . ' ';
            $command = strtoupper($command);

            $cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

            //transform to php logical operator syntak
            $translate = array(
                'AND' => '&&',
                'OR' => '||',
                'THEN' => 'THEN$',
                'A' => '$A',
                'B' => '$B',
                'C' => '$C',
                'D' => '$D',
                'E' => '$E',
                'F' => '$F',
                'G' => '$G',
                'H' => '$H',
                'I' => '$I',
                'J' => '$J',
                'K' => '$K',
                'L' => '$L',
                'M' => '$M',
                'N' => '$N',
                'O' => '$O',
                'P' => '$P',
                'Q' => '$Q',
                'R' => '$R',
                // 'S' => '$S',
                'T' => '$T',
                'U' => '$U',
                'V' => '$V',
                'W' => '$W',
                'X' => '$X',
                'Y' => '$Y',
                'Z' => '$Z',
                'SG' => '$SG',
            );

            $phpCommand =  strtr($cleanCommand, $translate);
            //var_dump($phpCommand);die;
            $param = array(
                '0' => '$A',
                '1' => '$B',
                '2' => '$C',
                '3' => '$D',
                '4' => '$E',
                '5' => '$F',
                '6' => '$G',
                '7' => '$H',
                '8' => '$I',
                '9' => '$J',
                '10' => '$K',
                '11' => '$L',
                '12' => '$M',
                '13' => '$N',
                '14' => '$O',
                '15' => '$P',
                '16' => '$Q',
                '17' => '$R',
                // '18' => '$S',
                '19' => '$T',
                '20' => '$U',
                '21' => '$V',
                '22' => '$W',
                '23' => '$X',
                '24' => '$Y',
                '25' => '$Z',
                '26' => '$SG',
            );
            // print_r($phpCommand);die;
            function str_replace_first($from, $to, $content, $row)
            {
                $from = '/' . preg_quote($from, '/') . '/';
                return preg_replace($from, $to, $content, $row);
            }

            $command = str_replace('(', ' ', $val['POLICY']);
            $command = str_replace(')', ' ', $command);
            $list = explode(' ', $command);
            // print_r($list);die;
            // var_dump($command)

            $thendata = explode('THEN', $command);
            // print_r($thendata);echo '<br>';die;
            $cthen = count($thendata);
            // print_r($thendata);die;
            $secondcommand = str_replace('(', '', trim($thendata[0]));
            $secondcommand = str_replace(')', '', $secondcommand);
            $secondcommand = str_replace('AND', '', $secondcommand);
            $secondcommand = str_replace('OR', '', $secondcommand);
            $secondlist = explode(' ', $secondcommand);
            // print_r($secondlist);die;
            // print_r($grouplist);die;
            // print_r($thendata[$i]);die;
            // return true;
            if (!empty($secondlist)) {
                foreach ($usergroup as $key => $value) {
                    foreach ($secondlist as $row => $thenval) {
                        // print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
                        if (trim($value['GROUP']) == trim($thenval)) {
                            $thengroup = true;
                            $grouplist[] = trim($thenval);
                            //die('here');
                        }
                    }
                }
            }

            //var_dump($cthen);
            if ($cthen >= 2) {
                foreach ($usergroup as $key => $value) {
                    // print_r($value);
                    foreach ($thendata as $row => $thenval) {
                        // echo '|';print_r($thenval);echo '==';
                        // print_r($value['GROUP']);echo '|';echo '<br/>';
                        // $thengroup = true;
                        $newsecondcommand = str_replace('(', '', trim($thenval));
                        $newsecondcommand = str_replace(')', '', $newsecondcommand);
                        $newsecondcommand = str_replace('AND', '', $newsecondcommand);
                        $newsecondcommand = str_replace('OR', '', $newsecondcommand);
                        $newsecondlist = explode(' ', $newsecondcommand);
                        //var_dump($newsecondcommand);
                        if (in_array(trim($value['GROUP']), $newsecondlist)) {
                            //if (trim($value['GROUP']) == trim($thenval)) {
                            $thengroup = true;
                            $grouplist[] = trim($thenval);
                            //die('here');
                        }
                    }
                }
            }
            //var_dump($grouplist);die;
            //var_dump($thengroup);die;
            // var_dump($thengroup);die;
            // // print_r($group);die;
            // // echo $thengroup;die;
            //echo '<pre>';
            //var_dump($thengroup);
            //var_dump($thendata);
            //print_r($thendata);echo '<br/>';die('here');
            if ($thengroup == true) {


                for ($i = 1; $i <= $cthen; ++$i) {
                    $oriCommand = $phpCommand;
                    //echo $oriCommand;die;
                    $indno = $i;
                    //echo $oriCommand;echo '<br>';

                    for ($a = $cthen - $indno; $a >= 1; --$a) {

                        if ($i > 1) {
                            $replace = 'THEN$ $' . trim($thendata[$a + 1]);
                        } else {
                            $replace = 'THEN$ $' . trim($thendata[$a]);
                        }

                        $oriCommand = str_replace($replace, "", $oriCommand);
                    }


                    //print_r($thendata);echo '<br>';die();

                    //die;
                    // if($i == 3){
                    // echo 'command : ';echo $oriCommand;echo '<br/>';
                    // }
                    //print_r($oriCommand);echo '<br>';
                    //print_r($list);echo '<br>';die;


                    $result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
                    // print_r($i);
                    //var_dump($result);
                    //echo 'result-';var_dump($result);echo '-';die;
                    if ($result) {
                        // die;
                        // echo $thendata[$i+1];die('eere');
                        // print_r($i);
                        // print_r($thendata);die;



                        $replace = 'THEN$ $' . trim($thendata[$i + 1]);
                        // var_dump($replace);die;
                        // print_r($i);
                        if (!empty($thendata[$i + 1])) {
                            //die;
                            $oriCommand = str_replace($replace, "", $phpCommand);
                        } else {
                            // die;
                            $thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
                            $thirdcommand = str_replace(')', '', $thirdcommand);
                            $thirdcommand = str_replace('AND', '', $thirdcommand);
                            $thirdcommand = str_replace('OR', '', $thirdcommand);
                            $thirdlist = explode(' ', $thirdcommand);
                            //						var_dump($secondlist);
                            //						var_dump($grouplist);
                            //							die;

                            if (!empty($secondlist)) {
                                foreach ($grouplist as $key => $valg) {
                                    foreach ($secondlist as $row => $value) {
                                        if ($value == $valg) {
                                            //echo 'sini';
                                            return false;
                                        }
                                    }
                                }
                            }
                            $oriCommand = $phpCommand;
                        }
                        // print_r($thendata[$i]);die;
                        // if($i == 3){
                        // echo $oriCommand;die;
                        // echo '<br/>';	
                        // }
                        // print_r($i);
                        //echo $oriCommand;
                        $result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
                        //var_dump($result);echo '<br/>';
                        if (!$result) {
                            // die;
                            //print_r($groupalfa);
                            //print_r($thendata[$i]);

                            if ($groupalfa == trim($thendata[$i])) {
                                //echo 'heer';
                                return true;
                            } else {

                                //return true;
                            }
                            /*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
                        } else {

                            //return false;

                            // $result = $this->generate($phpCommand,$list,$param,$psnumb);
                            // print_r($phpCommand);
                            // if($result){}
                            // die('here');
                        }
                        // var_dump($result);
                        // die;


                    } else {
                        //die('here');
                        $secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
                        $secondcommand = str_replace(')', '', $secondcommand);
                        $secondcommand = str_replace('AND', '', $secondcommand);
                        $secondcommand = str_replace('OR', '', $secondcommand);
                        $secondlist = explode(' ', $secondcommand);
                        //var_dump($i);
                        // var_dump($thendata);
                        //print_r($grouplist);
                        //die;
                        //if($groupalfa == $gro)
                        $approver = array();
                        $countlist = array_count_values($list);
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                                $selectapprover    = $this->_db->select()
                                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                                        'USER_ID'
                                    ))

                                    // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                                    ->where("C.REG_NUMBER = ?", (string) $psnumb)
                                    ->where("C.GROUP = ?", (string) $value);
                                //	 echo $selectapprover;
                                $usergroup = $this->_db->fetchAll($selectapprover);
                                // print_r($usergroup);
                                $approver[$value] = $usergroup;
                                if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
                                    //var_dump($countlist[$value]);
                                    //var_dump($tempusergroup['0']['GROUP']);  
                                    //die('gere');
                                    return false;
                                }
                            }
                        }

                        //$array = array(1, "hello", 1, "world", "hello");


                        //echo '<pre>';
                        //var_dump($list);
                        //var_dump($approver);
                        //die;
                        //var_dump($secondlist[0]);die;
                        //foreach($approver as $app => $valpp){
                        if (!empty($thendata[$i])) {
                            if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
                                //die('gere');
                                return false;
                            }
                        }

                        //echo '<pre>';
                        //var_dump($thendata);
                        //var_dump($grouplist);
                        //var_dump($approver);
                        //var_dump($groupalfa);
                        //die;
                        foreach ($grouplist as $key => $vg) {
                            $newgroupalpa = str_replace('AND', '', $vg);
                            $newgroupalpa = str_replace('OR', '', $newgroupalpa);
                            $groupsecondlist = explode(' ', $newgroupalpa);
                            //var_dump($newgroupalpa);
                            //&& empty($approver[$groupalfa])
                            if (in_array($groupalfa, $groupsecondlist)) {
                                // echo 'gere';die;
                                return true;
                                //die('ge');
                            }

                            if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
                                // echo 'gere';die;
                                return true;
                                //die('ge');
                            }
                        }

                        // var_dump($approver[$groupalfa]);
                        // var_dump($grouplist);
                        //var_dump($groupalfa);die;

                        //echo 'ger';die;
                        //print_r($secondlist);die;
                        //	 return true;
                        if (!empty($secondlist)) {
                            foreach ($grouplist as $key => $valg) {
                                foreach ($secondlist as $row => $value) {
                                    if ($value == $valg) {

                                        if (empty($thendata[1])) {
                                            //die;
                                            return true;
                                        }
                                        //else{
                                        //	return false;
                                        //	}
                                    }
                                }
                            }
                        }

                        //	echo 'here';die;
                        $secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);
                        // var_dump($secondresult);
                        //print_r($thendata[$i-1]);
                        //die;
                        //if()
                        //	 echo '<pre>';
                        //var_dump($grouplist);die;
                        foreach ($grouplist as $key => $valgroup) {
                            //print_r($valgroup);
                            //var_dump($thendata[$i]); 


                            if (trim($valg) == trim($thendata[$i])) {
                                $cekgroup = false;
                                //die('here');
                                if ($secondresult) {
                                    return false;
                                } else {
                                    return true;
                                }

                                //break;
                            }

                            //else if (trim($valg) == trim($thendata[$i - 1])) {
                            //		$cekgroup = false;
                            // die('here');
                            //	return false;
                            //	}
                        }
                        //die;
                        //if (!$cekgroup) {
                        // die('here');
                        //return false;
                        //}
                    }

                    //		    echo '<br/>';
                    //	${$command} = $oriCommand;
                }
            } else if (!empty($thendata) && $thengroup == false) {

                //var_dump($groupalfa)die;
                foreach ($thendata as $ky => $vlue) {
                    $newsecondcommand = str_replace('(', '', trim($vlue));
                    $newsecondcommand = str_replace(')', '', $newsecondcommand);
                    $newsecondcommand = str_replace('AND', '', $newsecondcommand);
                    $newsecondcommand = str_replace('OR', '', $newsecondcommand);
                    $newsecondlist = explode(' ', $newsecondcommand);
                    if ($newsecondlist['0'] == $groupalfa) {
                        return true;
                    }
                }
                return false;
            }




            $approver = array();
            // print_r($list);die;  	
            foreach ($list as $key => $value) {
                if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                    $selectapprover    = $this->_db->select()
                        ->from(array('C' => 'T_BGAPPROVAL'), array(
                            'USER_ID'
                        ))

                        // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                        ->where("C.REG_NUMBER = ?", (string) $psnumb)
                        ->where("C.GROUP = ?", (string) $value);
                    //	 echo $selectapprover;
                    $usergroup = $this->_db->fetchAll($selectapprover);
                    // print_r($usergroup);
                    $approver[$value] = $usergroup;
                }
            }
            //die;




            // print_r($phpCommand);die;
            foreach ($param as $url) {
                if (strpos($phpCommand, $url) !== FALSE) {
                    $ta = substr_count($phpCommand, $url);
                    // print_r($list);die;

                    if (!empty($approver)) {
                        // print_r($approver);die;
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                                foreach ($approver[$value] as $row => $val) {
                                    // print_r($approver);die;
                                    if (!empty($val)) {
                                        $values = 'G' . $value;
                                        ${$values}[$row + 1] = true;
                                        // print_r($B);
                                    }

                                    // print_r($val);
                                }
                            }
                        }
                    }

                    for ($i = 1; $i <= $ta; $i++) {
                        // print_r($list);die;
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                                $values = 'G' . $value;
                                // print_r(${$values});
                                if (empty(${$values}[$i])) {
                                    ${$values}[$i] = false;
                                }
                                // if(${$value}[$i])
                            }
                        }
                        // print_r($phpCommand);die;
                        $BG_NUMBER = $i;
                        $label = str_replace('$', '$G', $url);

                        $replace = $label . '[' . $BG_NUMBER . ']';

                        $alf = str_replace('$', '', $url);
                        $values = 'G' . $alf;
                        // print_r($values);die;
                        if (${$values}[$i] == $replace) {
                            $phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
                            // print_r($phpCommand);
                        } else {
                            $phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
                            // print_r($phpCommand);die;
                        }
                        // }
                        // }

                    }
                    // print_r($GB);die;

                }
            }

            $keywords = preg_split("/[\s,]+/", $cleanCommand);
            $result =  false;
            $thendata = explode('THEN$', $phpCommand);
            //var_dump($thendata);die;
            if (!empty($thendata['1'])) {
                $phpCommand = '';
                foreach ($thendata as $tkey => $tval) {
                    $phpCommand .= '(';
                    $phpCommand .= $tval . ')';
                    if (!empty($thendata[$tkey + 1])) {
                        $phpCommand .= ' && ';
                    }
                }
            } else {

                $phpCommand = str_replace('THEN$', '&&', $phpCommand);
            }
            //var_dump($phpCommand);
            if (!empty($phpCommand)) {
                eval('$result = ' . "$phpCommand;");
            } else {
                return false;
            }
            //var_dump($result);die;
            if (!$result) {

                return true;
            }
            // die('here2');
            //var_dump ($result);die;
            //return $result;
        } else {
            // die('here');
            return true;
        }
    }



    public function generate($command, $list, $param, $psnumb, $group, $thengroup)
    {

        $phpCommand = $command;

        // echo $command;die;

        $approver = array();

        $count_list = array_count_values($list);
        //print_r($count_list);
        foreach ($list as $key => $value) {
            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))

                    // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                    ->where("C.REG_NUMBER = ?", (string) $psnumb)
                    ->where("C.GROUP = ?", (string) $value);
                // echo $selectapprover;
                $usergroup = $this->_db->fetchAll($selectapprover);
                // print_r($usergroup);
                $approver[$value] = $usergroup;
            }
        }
        //var_dump($param);die;
        //var_dump($group);
        foreach ($approver as $appval) {
            $totaldata = count($approver[$group]);
            $totalgroup = $count_list[$group];
            //var_dump($totaldata);
            //var_dump($totalgroup);
            if ($totalgroup == $totaldata && $totalgroup != 0) {

                return false;
            }
        } //die;
        //die;





        foreach ($param as $url) {

            if (strpos($phpCommand, $url) !== FALSE) {
                $ta = substr_count($phpCommand, $url);
                // print_r($list);die;

                if (!empty($approver)) {
                    // print_r($approver);die;
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                            foreach ($approver[$value] as $row => $val) {
                                // print_r($approver);die;
                                if (!empty($val)) {
                                    $values = 'G' . $value;
                                    ${$values}[$row + 1] = true;
                                    // print_r($B);
                                }

                                // print_r($val);
                            }
                        }
                    }
                }


                // print_r($approver);die;

                for ($i = 1; $i <= $ta; $i++) {

                    foreach ($list as $key => $value) {
                        if (!empty($value)) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                                $values = 'G' . $value;
                                // print_r(${$values});
                                if (empty(${$values}[$i])) {
                                    ${$values}[$i] = false;
                                }
                                // if(${$value}[$i])
                            }
                        }
                    }


                    $BG_NUMBER = $i;
                    $label = str_replace('$', '$G', $url);
                    // print_r($phpCommand);die('here');
                    $replace = $label . '[' . $BG_NUMBER . ']';

                    $alf = str_replace('$', '', $url);
                    $values = 'G' . $alf;

                    if (${$values}[$i] == $replace) {
                        $phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
                        // print_r($phpCommand);
                    } else {
                        $phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
                        // print_r($phpCommand);die;
                    }
                    // }
                    // }

                }
                // print_r($GB);die;

            }
        }

        $keywords = preg_split("/[\s,]+/", $cleanCommand);
        $result =  false;
        $phpCommand = str_replace('THEN$', '&&', $phpCommand);
        //print_r($phpCommand);echo '<br/>';

        if (!empty($phpCommand)) {
            eval('$result = ' . "$phpCommand;");
            //var_dump($thengroup);
            // var_dump($result);

            if ($result) {
                //var_dump($thengroup);die;
                if ($thengroup) {
                    return true;
                } else {
                    return false;
                }
            } else {

                if ($thengroup) {
                    return false;
                } else {

                    return true;
                }
            }
            // return $result;
        } else {
            return false;
        }

        // var_dump ($result);die;




    }

    public function getbgpublisherAction()
    {
        $this->_helper->layout()->setLayout('popup');
        $getAllBranch = $this->_db->select()
            ->from("M_BRANCH", ["*"])
            ->where("HEADQUARTER = 'NO'")
            ->query()->fetchAll();

        $this->paging($getAllBranch);
    }

    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function Terbilangen($nilai)
    {
        $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 20 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 100) {
            return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilangen($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilangen($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function generateTransactionID($branch, $bgtype)
    {

        $currentDate = date("ymd");
        $seqNumber   = mt_rand(1111, 9999);
        $trxId = strval($branch) . strval($currentDate) . strval($bgtype) . strval($seqNumber);

        return $trxId;
    }

    public function checkpaperAction()
    {
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_REG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        $get_bg_number = $this->_db->select()
            ->from("T_BANK_GUARANTEE", ["BG_NUMBER"])
            ->where("BG_REG_NUMBER = ?", $BG_REG_NUMBER)
            ->query()->fetch();

        $get_bg_number = $get_bg_number["BG_NUMBER"];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $kode_kertas = $this->_request->getParam("kode_kertas");
        $nomor_kertas_awal = $this->_request->getParam("nomor_kertas_awal");
        $nomor_kertas_akhir = $this->_request->getParam("nomor_kertas_akhir");
        $paper_branch = $this->_request->getParam("paper_branch");

        $kode_kertas_rusak = $this->_request->getParam("kode_kertas_rusak");
        $nomor_kertas_rusak_awal = $this->_request->getParam("nomor_kertas_rusak_awal");
        $nomor_kertas_rusak_akhir = $this->_request->getParam("nomor_kertas_rusak_akhir");
        $catatan_kerusakan = $this->_request->getParam("catatan_kerusakan");

        $check_kode_kertas = $this->_db->select()
            ->from("M_PAPER_GROUP")
            ->where("PAPER_KEY = ?", $kode_kertas)
            ->where("PAPER_BRANCH = ?", $paper_branch)
            ->query()->fetchAll();

        if (count($check_kode_kertas) < 1) {
            $result = json_encode(["status" => "failed", "data" => "Kode kertas tidak ada di Bank cabang pencetak"]);
            echo $result;
            return false;
        }

        $checkError = [0, 0, 0];
        $kertasTerpakai = '';

        $save_kertas = [];
        foreach ($check_kode_kertas as $detail) {
            $save_length = strlen($detail["PAPER_FROM"]);
            for ($i = intval($nomor_kertas_awal); $i <= intval($nomor_kertas_akhir); $i++) {
                $temp_nomor = str_repeat("0", $save_length - strlen($i)) . $i;

                // if (!($i >= intval($detail["PAPER_FROM"])) && !($i <= intval($detail["PAPER_TO"]))) {
                //     echo json_encode(["status" => "failed", "data" => "Nomor kertas " . $temp_nomor . " tidak tersedia di bank cabang pencetak"]);
                //     return false;
                // }

                if (!($i >= intval($detail["PAPER_FROM"])) || !($i <= intval($detail["PAPER_TO"]))) {
                    // echo json_encode(["status" => "failed", "data" => "Nomor kertas " . $temp_nomor . " tidak tersedia di bank cabang pencetak"]);
                    // return false;
                    $checkError[0] += 1;
                }

                $cek_m_paper = $this->_db->select()
                    ->from("M_PAPER")
                    ->where("PAPER_ID = ?", $kode_kertas . $temp_nomor)
                    ->where("PAPER_GROUP = ?", $detail["PAPER_GROUP"]);

                $cek_m_paper = $this->_db->fetchRow($cek_m_paper);

                if (!empty($cek_m_paper)) {
                    $checkError[1] += 1;
                    if (!$kertasTerpakai) {
                        $kertasTerpakai = $kode_kertas . $temp_nomor;
                    }
                    // echo json_encode(["status" => "failed", "data" => "Nomor kertas " . $kode_kertas . $temp_nomor . " sudah terpakai"]);
                    // return false;
                }

                if (($i >= $detail["PAPER_FROM"] && $i <= $detail["PAPER_TO"])) {
                    array_push($save_kertas, $kode_kertas . $temp_nomor);
                }
            }
        }

        if ($checkError[0] == count($check_kode_kertas)) {
            echo json_encode(["status" => "failed", "data" => "Nomor kertas " . $temp_nomor . " tidak tersedia di bank cabang pencetak"]);
            return false;
        }

        if ($checkError[1]) {
            echo json_encode(["status" => "failed", "data" => "Nomor kertas " . $kertasTerpakai . " sudah terpakai"]);
            return false;
        }

        if (count($save_kertas) > 0) {
            foreach ($save_kertas as $kertas) {
                $this->_db->insert("M_PAPER", [
                    "PAPER_ID" => $kertas,
                    "STATUS" => "1",
                    "PAPER_GROUP" => $detail["PAPER_GROUP"],
                    "PAPER_UPDATED" => new Zend_Db_Expr('now()'),
                    "PAPER_UPDATEDBY" => $this->_userIdLogin,
                    "NOTES" => $get_bg_number
                ]);
            }
        }

        if ($kode_kertas_rusak) {
            $check_kode_kertas_rusak = $this->_db->select()
                ->from("M_PAPER_GROUP")
                ->where("PAPER_KEY = ?", $kode_kertas_rusak)
                ->where("PAPER_BRANCH = ?", $paper_branch)
                ->query()->fetchAll();


            foreach ($check_kode_kertas_rusak as $detail) {
                $save_length = strlen($detail["PAPER_FROM"]);
                for ($i = intval($nomor_kertas_rusak_awal); $i <= intval($nomor_kertas_rusak_akhir); $i++) {
                    $temp_nomor = str_repeat("0", $save_length - strlen($i)) . $i;

                    $cek_m_paper = $this->_db->select()
                        ->from("M_PAPER")
                        ->where("PAPER_ID = ?", $kode_kertas . $temp_nomor)
                        ->where("PAPER_GROUP = ?", $detail["PAPER_GROUP"]);

                    $cek_m_paper = $this->_db->fetchRow($cek_m_paper);

                    if (!empty($cek_m_paper)) {

                        $this->_db->update("M_PAPER", [
                            "STATUS" => "2",
                            "PAPER_UPDATED" => new Zend_Db_Expr('now()'),
                            "NOTES" => $catatan_kerusakan
                        ], [
                            "PAPER_ID = ?" => $kode_kertas_rusak . $temp_nomor,
                            "PAPER_GROUP = ?" => $detail["PAPER_GROUP"]
                        ]);
                    }
                }
            }
        }

        $save_length = strlen($detail["PAPER_FROM"]);
        Application_Helper_General::writeLog('PSBG', "Printing Stage With Paper Number : " . $check_kode_kertas[0]["PAPER_KEY"] . str_repeat("0", $save_length - strlen($i)) . intval($nomor_kertas_awal) . " - " . $check_kode_kertas[0]["PAPER_KEY"] . str_repeat("0", $save_length - strlen($i)) . intval($nomor_kertas_akhir));

        // echo json_encode(["status" => "success", "data" => [$kode_kertas, $nomor_kertas_awal, $nomor_kertas_akhir]]);
        // echo json_encode(["status" => "success", "data" => [$kode_kertas, $check_kode_kertas[0]["PAPER_KEY"] . $check_kode_kertas[0]["PAPER_FROM"], $check_kode_kertas[0]["PAPER_KEY"] . $check_kode_kertas[0]["PAPER_TO"]]]);
        echo json_encode(["status" => "success", "data" => [$kode_kertas, $check_kode_kertas[0]["PAPER_KEY"] . str_repeat("0", $save_length - strlen($i)) . intval($nomor_kertas_awal), $check_kode_kertas[0]["PAPER_KEY"] . str_repeat("0", $save_length - strlen($i)) . intval($nomor_kertas_akhir)]]);
        return false;
    }

    public function downloadAction()
    {
        // decrypt numb
        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token     = $rand;
        $this->view->token = $sessionNamespace->token;

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        $bgdata = $this->_db->select()
            ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
            ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
            ->where('A.BG_STATUS = ?', 15)
            ->query()->fetch();

        $attahmentDestination = UPLOAD_PATH . '/document/submit/';
        return $this->_helper->download->file($bgdata["DOCUMENT_ID"] . ".pdf", $attahmentDestination . $bgdata["DOCUMENT_ID"] . ".pdf");
    }

    public function showfinaldocAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // decrypt numb

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;

        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $bg_reg_number = $AESMYSQL->decrypt($BG_NUMBER, $password);

        $file = $this->_request->getParam("file");

        $get_document_id = $this->_db->select()
            ->from("T_BANK_GUARANTEE", ["DOCUMENT_ID"])
            ->where("BG_REG_NUMBER = ?", $bg_reg_number)
            ->query()->fetch();

        $get_file_name = $get_document_id["DOCUMENT_ID"] . ".pdf";

        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=" . $get_file_name);
        // @readfile('/app/bg/library/data/uploads/document/submit/' . $get_file_name);
        @readfile(UPLOAD_PATH . '/document/submit/' . $get_file_name);
    }
}
