<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';


function lockUser($result,$timeout)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	
	try{
		$db->beginTransaction();
		
		$res 	= null;
		$string = '';
		foreach($result as $row)
		{
			unset($where);
			$custid = $row['custid'];
			$userid = $row['userid'];
			$lockreason = "New User is suspended. No login after ".$timeout." day(s). Last created was at ".$row['lastcreated'];	
			$update = array('USER_STATUS'  		=> '2',
							'USER_LOCKREASON'	=> $lockreason);		
			$where[] = "CUST_ID = '".$custid."'";
			$where[] = "USER_ID = '".$userid."'"; // User Status 1: User active 3: User deleted, 2: User Suspended
			$res += $db->update('M_USER', $update, $where);
			
			$string .= "$custid -> $userid , "; 
			
				
		}
		
		echo "ROW EFFECTED = ".$res;
		
		$db->commit();
		Application_Helper_General::cronLog('autoLockNewUserFrontend', $string, '1');			   
	}
	catch(Exception $e) 
	{
		$db->rollBack();
		$string = $e->getMessage();
		Application_Helper_General::cronLog('autoLockNewUserFrontend', $string, '0');		
	}
}


function autoLockNewUserFrontend($timeout){
	
	if($timeout >= 0){
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$result = $db->select()
					->from (array('U'=>'M_USER'),array())
					->joinLeft(	array('C'=>'M_CUSTOMER'),'U.CUST_ID = C.CUST_ID',
								array(
										'userid'		=>'U.USER_ID',
										'custid'		=>'C.CUST_ID',
										'lastcreated'	=>'U.USER_CREATED',
										
									))
										
					->where ( "	U.USER_STATUS = '1' 
								AND U.USER_ISNEW = '1' 
								AND U.USER_CREATED is not NULL 
								AND U.USER_LASTLOGIN is NULL" )
								
					->group (array('U.USER_ID','C.CUST_ID','U.USER_CREATED'))
						 
					->having('ADDDATE(USER_CREATED,'.$timeout.') <= now() ')
						 
					->query()->fetchall()
					;
					
// 					die($result);
			
			if(is_array($result) && count($result) > 0){
				
// 				print_r ($result);
				lockUser($result,$timeout);
				
			}
			else{
				echo "No Data";
			}
		
		
	}
	
}

autoLockNewUserFrontend(5);

?>