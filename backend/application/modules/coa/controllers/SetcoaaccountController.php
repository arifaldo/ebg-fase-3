<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class coa_SetcoaaccountController extends Application_Main
{
	public function indexAction() 
	{

		$set = new Settings();
		$coa = $set->getSetting('charges_acct_IDR');
		$filters = array( 'COAAccount' => array('StripTags','StringTrim')
							);
		$validators = array(
							
							'COAAccount' => array('NotEmpty','Digits',array('StringLength',array('min'=>10,'max'=>10)),
												'messages' => array(
																	'Can not be empty, Account Number length must be 10 digits',
																	'Must be numeric values',
																	'Account number length must be 10 digits'
																	)
												)	
						);    								  										  							  
		$zf_filters = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		$submit = $this->_getParam('submit');
		$cek = $this->_db->select()
							->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID'))
							->WHERE ('MODULE_ID = ?','COA')
							->query()->FetchAll();												
		if($cek == null){
			if($submit == 'Submit'){			
				$this->_db->beginTransaction();
					if($zf_filters->isValid())
						try{
							$info = 'OLD COA ACCOUNT : '.$coa.',NEW COA ACCOUNT : '.$zf_filters->COAAccount;
						
							$change_id = $this->suggestionWaitingApproval('COA Account',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','','');
		//					$change_id = 1;
							$data = array(	'CHANGES_ID' => $change_id,
					     					'SETTING_ID' => 'charges_acct_IDR',
											'SETTING_VALUE' => $zf_filters->COAAccount,
					     					'MODULE_ID' => $this->_request->getModuleName(),
											);
							$this->_db->insert('TEMP_SETTING',$data);
							$this->_db->commit();
							$this->_redirect('/notification/success/index');
						}
						catch(Exception $e)
						{
								$this->_db->rollBack();					
						}
					else{
						$this->view->error = true;
						$docErr = ('Error in processing form values. Please correct values and re-submit.');
						$this->view->report_msg = $docErr;
						
						foreach($zf_filters->getMessages() as $key=>$err){
							$xxx = 'x'.$key;
							$this->view->$xxx = $this->displayError($err);
						}
					}
					
				}			
			}
		else
			{
				$this->view->nochanges = true;
				$this->view->nochanges_msg = 'No changes allowed for this record while awaiting approval for previous change.';
			}
		
//		Zend_Debug::dump($coa);die;                 	
		$this->view->COAAccount = $coa;
	}
}

?>