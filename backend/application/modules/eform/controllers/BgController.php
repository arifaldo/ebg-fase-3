	<?php
    require_once 'Zend/Controller/Action.php';
    require_once 'General/CustomerUser.php';
    require_once 'SGO/Helper/AES.php';
    require_once 'General/Settings.php';
    require_once 'Crypt/AESMYSQL.php';
    require_once("Service/Account.php");

    class eform_BgController extends Application_Main
    {
        public function downloadgtAction()
        {

            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            // get download parameter
            $getName = urldecode($this->_request->getParam('download'));
            $getBgRegNumber = $this->_request->getParam('bgnumb');

            // get Helper
            Application_Helper_General::downloadGuarantedTransaction($getName);
        }
    }
