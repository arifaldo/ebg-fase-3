<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class customerbg_DetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        // $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $bgnumb = $this->_getParam('bgnumb');


        if (!empty($bgnumb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->where('A.BG_REG_NUMBER = ?', $bgnumb)
                ->query()->fetchAll();

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.BG_REG_NUMBER = ?', $bgnumb)
                ->query()->fetchAll();
            //$datas = $this->_request->getParams();
            // var_dump($datas);

            $dataselect = $bgdata['0'];
            $selectcomp = $this->_db->select()
                ->from(array('A' => 'M_CUSTOMER'), array('*'))
                ->where('A.CUST_ID = ?', $dataselect['CUST_ID'])
                ->query()->fetchAll();

            $this->view->compinfo = $selectcomp[0];


            if (!empty($bgdata)) {
                // var_dump($bgdata);
                $data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

                $config            = Zend_Registry::get('config');
                $BgType     = $config["bg"]["status"]["desc"];
                $BgCode     = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));
                $this->view->arrStatus = $arrStatus;

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Bank Guarantee Line Facility',
                    3 => 'Insurance'

                );

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->ACCT_NO = $data['ACCT_NO'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];
                $this->view->BG_STATUS = $data['BG_STATUS'];

                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];


                $CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);
                $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                // $param = array('CCY_IN' => 'IDR');
                $AccArr = $CustomerUser->getAccounts($param);
                // var_dump($AccArr);

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Currency') {
                                $this->view->Currency =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);


                $back = $this->_getParam('back');
                if ($back) {
                    $this->_redirect('/customer');
                }
                //echo '<pre>';
                //var_dump($back);die;

                $download = $this->_getParam('download');
                //print_r($edit);die;
                if ($download) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                }

                $BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
                //print_r($edit);die;
                if ($BG_APPROVE_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/bg/';
                    //var_dump($attahmentDestination.$data['BG_APPROVE_DOC']);die;
                    $this->_helper->download->file($data['BG_APPROVE_DOC'], $attahmentDestination . $data['BG_APPROVE_DOC']);
                }

                $claim            = $this->_getParam('claim');
                $reject             = $this->_getParam('reject');
                $suspend             = $this->_getParam('suspend');
                $unsuspend             = $this->_getParam('unsuspend');
                $download2 = $this->_getParam('pdf');
                //var_dump($this->_request->getParams());die;
                if ($download2) {
                    //echo '<pre>';
                    //var_dump($bgdata);die;
                    $tempdir = APPLICATION_PATH . "/../public/QRImages/";

                    $teks_qrcode    = 'http://' . $_SERVER['HTTP_HOST'] . "/bgchecking/detail/index/bgnumb/" . $bgdata['0']['BG_NUMBER'];
                    $namafile       = $bgdata['0']['BG_NUMBER'] . ".png";
                    $quality        = "H";
                    $ukuran         = 5;
                    $padding        = 1;
                    $qrImage        = $tempdir . $namafile;

                    QRCode::png($teks_qrcode, $tempdir . $namafile, $quality, $ukuran, $padding);
                    //var_dump($qrImage);die;
                    $outputHTML = "<tr><td>" . $this->view->render('/detail/indexpdf.phtml') . "</td></tr>";
                    //var_dump(APPLICATION_PATH);die;

                    $path  = APPLICATION_PATH . '/../public/QRImages/';
                    $files = scandir($path);
                    $files = array_diff(scandir($path), array('.', '..'));
                    $dh = opendir($path);
                    while ($file = readdir($dh)) {
                        if ($file == $namafile) {
                            $imageData = base64_encode(file_get_contents($path . '' . $namafile));
                            $image = $path . '' . $data['IMAGE'];
                            $src = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;
                        }
                    }

                    $path  = APPLICATION_PATH . '/../public/images/image001.jpg';
                    $imageData = base64_encode(file_get_contents($path));
                    $image = $path;
                    $srcbank = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;
                    $terbilangs = $this->Terbilang($data['BG_AMOUNT']);
                    //echo '<pre>';
                    //var_dump($data);
                    //var_dump($terbilangs);
                    //die;
                    //$path = 'myfolder/myimage.png';
                    //$type = pathinfo($path, PATHINFO_EXTENSION);
                    //$data = file_get_contents($path);
                    //$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $translate = array(
                        '[[qrcode]]'             => $src,
                        '[[bankimage]]'            => $srcbank,
                        '[[bg_numb]]'            => $data['BG_NUMBER'],
                        '[[date_start]]'        => $data['TIME_PERIOD_START'],
                        '[[city]]'        => $data['RECIPIENT_CITY'],
                        '[[benef_name]]'        => $data['RECIPIENT_NAME'],
                        '[[benef_add]]'        => $data['RECIPIENT_ADDRES'],
                        '[[benef_city]]'        => $data['RECIPIENT_CITY'],
                        '[[app_name]]'        => $selectcomp[0]['CUST_NAME'],
                        '[[app_add]]'        => $selectcomp[0]['CUST_ADDRESS'],
                        '[[app_city]]'        => $selectcomp[0]['CUST_CITY'],
                        '[[amount]]'        => number_format($data['BG_AMOUNT'], 2, ',', '.'),
                        '[[terbilang]]'        => $terbilangs,
                        '[[guarantee_letter]]'        => $data['GUARANTEE_TRANSACTION'],
                        '[[date_start]]'        => $this->indodate($data['TIME_PERIOD_START']),
                        '[[date_end]]'        => $this->indodate($data['TIME_PERIOD_END']),


                    );

                    $outputHTMLnew = strtr($outputHTML, $translate);

                    //echo $outputHTMLnew;die;
                    $setting = new Settings();
                    $master_bank_app_name = $setting->getSetting('master_bank_app_name');
                    $master_bank_name = $setting->getSetting('master_bank_name');
                    try {
                        $this->_helper->download->newPdfblank(null, null, null, $master_bank_app_name . ' - ' . $master_bank_name, $outputHTMLnew);
                        die;
                    } catch (Exception $e) {
                        var_dump($e);
                        die;
                    }
                    //die;
                }

                if ($reject) {
                    $cancelBy = $this->_getParam('cancelBy');
                    $cancelNotes = $this->_getParam('cancelNotes');

                    $attahmentDestination     = UPLOAD_PATH . '/document/bg/';
                    $adapter                 = new Zend_File_Transfer_Adapter_Http();

                    $sourceFileName = $adapter->getFileName();

                    if ($sourceFileName == null) {
                        $sourceFileName = null;
                        $fileType = null;
                    } else {
                        $sourceFileName = substr(basename($adapter->getFileName()), 0);
                        if ($_FILES["uploadSourcecancel"]["type"]) {
                            $adapter->setDestination($attahmentDestination);
                            $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                            $fileType = $adapter->getMimeType();
                            $size = $_FILES["uploadSourcecancel"]["size"];
                        } else {
                            $fileType = null;
                            $size = null;
                        }
                    }

                    $fileExt                 = "pdf";
                    $fileTypeMessage = explode('/', $fileType);
                    $fileType =  $fileTypeMessage[1];
                    $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                    // $extensionValidator->setMessage("Extension file must be *.pdf");

                    $maxFileSize = "1024000";
                    $size = number_format($size);

                    $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                    $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                    $adapter->setValidators(array($extensionValidator, $sizeValidator));

                    if ($adapter->isValid()) {
                        // $newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
                        // $adapter->addFilter('Rename', $newFileName);

                        if ($adapter->receive()) {
                            try {
                                $this->_db->beginTransaction();

                                /*$dataApprove =  array(					
                                                                'BG_STATUS' 		=> '10',
                                                                'BG_CANCELEDBY'		=> $cancelBy,
                                                                'BG_CANCEL_NOTES'	=> $cancelNotes,
                                                                'BG_UNDERLYING_DOC'	=> $sourceFileName,
                                                                'BG_UPDATED' 		=> date('Y-m-d H:i:s'), 
                                                                'BG_UPDATEDBY' 		=> $this->_userIdLogin
                                                            );							
                                        
                                        $where = array('BG_REG_NUMBER = ?' => $bgnumb);
                                        $result = $this->_db->update('T_BANK_GUARANTEE',$dataApprove,$where);
                                        */

                                $data['BG_STATUS'] = 7;
                                $data['BG_CANCELEDBY']  = $cancelBy;
                                $data['BG_CANCEL_NOTES']  = $cancelNotes;
                                $data['USAGE_PURPOSE']  = implode(",", $data['USAGE_PURPOSE']);
                                $data['BG_UNDERLYING_DOC']  = $sourceFileName;
                                $data['BG_UPDATED']  = date('Y-m-d H:i:s');
                                $data['BG_UPDATEDBY']  = $this->_userIdLogin;
                                // insert ke T_GLOBAL_CHANGES
                                $CUST_NAME = $selectcomp[0]['CUST_NAME'];
                                $info = 'Customer ID = ' . $data['CUST_ID'] . ', Customer Name = ' . $key_value . ', BG Register Number = ' . $data['BG_REG_NUMBER'];
                                $change_id = $this->suggestionWaitingApproval('Bank Guarantee', $info, $this->_changeType['code']['delete'], null, 'T_BANK_GUARANTEE', 'TEMP_BANK_GUARANTEE', $data['CUST_ID'], $CUST_NAME, $data['CUST_ID'], $CUST_NAME);
                                $data['CHANGES_ID'] = $change_id;
                                $this->_db->insert('TEMP_BANK_GUARANTEE', $data);

                                $this->_db->commit();

                                $this->setbackURL('/' . $this->_request->getModuleName());
                                $this->_redirect('/notification/submited/index');

                                $this->_db->commit();
                            } catch (Exception $e) {
                                echo "error";
                                die;
                                $this->_db->rollBack();
                            }
                            /*$this->setbackURL('/'.$this->_request->getModuleName());
                                    $this->_redirect('/notification/success');*/
                        }
                    } else {

                        $errors = array($adapter->getMessages());
                        var_dump($errors);
                        die;
                    }
                }

                if ($claim) {
                    $claimBy = $this->_getParam('claimBy');
                    $claimNotes = $this->_getParam('claimNotes');

                    $attahmentDestination     = UPLOAD_PATH . '/document/bg/';
                    $adapter                 = new Zend_File_Transfer_Adapter_Http();

                    $sourceFileName = $adapter->getFileName();

                    if ($sourceFileName == null) {
                        $sourceFileName = null;
                        $fileType = null;
                    } else {
                        $sourceFileName = substr(basename($adapter->getFileName()), 0);
                        if ($_FILES["uploadSourceclaim"]["type"]) {
                            $adapter->setDestination($attahmentDestination);
                            $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                            $fileType = $adapter->getMimeType();
                            $size = $_FILES["uploadSourceclaim"]["size"];
                        } else {
                            $fileType = null;
                            $size = null;
                        }
                    }

                    $fileExt                 = "pdf";
                    $fileTypeMessage = explode('/', $fileType);
                    $fileType =  $fileTypeMessage[1];
                    $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                    // $extensionValidator->setMessage("Extension file must be *.pdf");

                    $maxFileSize = "1024000";
                    $size = number_format($size);

                    $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                    $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                    $adapter->setValidators(array($extensionValidator, $sizeValidator));

                    if ($adapter->isValid()) {
                        // $newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
                        // $adapter->addFilter('Rename', $newFileName);

                        if ($adapter->receive()) {
                            try {
                                $this->_db->beginTransaction();

                                /*if($claimBy == "Applicant"){
                    
                                            $dataApprove =  array(					
                                                                    'BG_STATUS' 		=> '8',
                                                                    'BG_CLAIMEDBY'		=> $claimBy,
                                                                    'BG_CLAIM_NOTES'	=> $claimNotes,
                                                                    'BG_UNDERLYING_DOC'	=> $sourceFileName,
                                                                    'BG_UPDATED' 		=> date('Y-m-d H:i:s'), 
                                                                    'BG_UPDATEDBY' 		=> $this->_userIdLogin
                                                                );	
                                        }else{
                                            $dataApprove =  array(					
                                                                    'BG_STATUS' 		=> '9',
                                                                    'BG_CLAIMEDBY'		=> $claimBy,
                                                                    'BG_CLAIM_NOTES'	=> $claimNotes,
                                                                    'BG_UNDERLYING_DOC'	=> $sourceFileName,
                                                                    'BG_UPDATED' 		=> date('Y-m-d H:i:s'), 
                                                                    'BG_UPDATEDBY' 		=> $this->_userIdLogin
                                                                );
                                        }						
                                        
                                        $where = array('BG_REG_NUMBER = ?' => $bgnumb);
                                        $result = $this->_db->update('T_BANK_GUARANTEE',$dataApprove,$where);
                                        */

                                $data['BG_STATUS'] = 9;
                                $data['BG_CLAIMEDBY']  = $claimBy;
                                $data['BG_CLAIM_NOTES']  = $claimNotes;
                                $data['USAGE_PURPOSE']  = implode(",", $data['USAGE_PURPOSE']);
                                $data['BG_UNDERLYING_DOC']  = $sourceFileName;
                                $data['BG_UPDATED']  = date('Y-m-d H:i:s');
                                $data['BG_UPDATEDBY']  = $this->_userIdLogin;
                                // insert ke T_GLOBAL_CHANGES
                                $CUST_NAME = $selectcomp[0]['CUST_NAME'];
                                $info = 'Customer ID = ' . $data['CUST_ID'] . ', Customer Name = ' . $key_value . ', BG Register Number = ' . $data['BG_REG_NUMBER'];
                                $change_id = $this->suggestionWaitingApproval('Bank Guarantee', $info, $this->_changeType['code']['edit'], null, 'T_BANK_GUARANTEE', 'TEMP_BANK_GUARANTEE', $data['CUST_ID'], $CUST_NAME, $data['CUST_ID'], $CUST_NAME);
                                $data['CHANGES_ID'] = $change_id;
                                $this->_db->insert('TEMP_BANK_GUARANTEE', $data);

                                $this->_db->commit();

                                $this->setbackURL('/' . $this->_request->getModuleName());
                                $this->_redirect('/notification/submited/index');

                                $this->_db->commit();
                            } catch (Exception $e) {
                                $this->_db->rollBack();
                            }
                            /*$this->setbackURL('/'.$this->_request->getModuleName());
                                    $this->_redirect('/notification/success');*/
                        }
                    } else {
                        echo "error";
                        die;
                        $errors = array($adapter->getMessages());
                        var_dump($errors);
                        die;
                    }
                }

                if ($suspend) {

                    $suspendNotes = $this->_getParam('suspendNotes');
                    $attahmentDestination     = UPLOAD_PATH . '/document/bg/';
                    $adapter                 = new Zend_File_Transfer_Adapter_Http();
                    $sourceFileName = $adapter->getFileName();

                    if ($sourceFileName == null) {
                        $sourceFileName = null;
                        $fileType = null;
                    } else {
                        $sourceFileName = substr(basename($adapter->getFileName()), 0);
                        if ($_FILES["uploadSourceSuspend"]["type"]) {
                            $adapter->setDestination($attahmentDestination);
                            $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                            $fileType = $adapter->getMimeType();
                            $size = $_FILES["uploadSourceSuspend"]["size"];
                        } else {
                            $fileType = null;
                            $size = null;
                        }
                    }

                    $fileExt                 = "pdf";
                    $fileTypeMessage = explode('/', $fileType);
                    $fileType =  $fileTypeMessage[1];
                    $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                    // $extensionValidator->setMessage("Extension file must be *.pdf");

                    $maxFileSize = "1024000";
                    $size = number_format($size);

                    $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                    $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                    $adapter->setValidators(array($extensionValidator, $sizeValidator));

                    if ($adapter->isValid()) {
                        // $newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
                        // $adapter->addFilter('Rename', $newFileName);

                        if ($adapter->receive()) {
                            try {
                                $this->_db->beginTransaction();

                                /*$dataApprove =  array(					
                                                                'BG_STATUS' 		=> '12',
                                                                'BG_CANCELEDBY'		=> '',//$cancelBy,
                                                                'BG_CANCEL_NOTES'	=> $suspendNotes,
                                                                'BG_UNDERLYING_DOC'	=> $sourceFileName,
                                                                'BG_UPDATED' 		=> date('Y-m-d H:i:s'), 
                                                                'BG_UPDATEDBY' 		=> $this->_userIdLogin
                                                            );							
                                        
                                        $where = array('BG_REG_NUMBER = ?' => $bgnumb);
                                        $result = $this->_db->update('T_BANK_GUARANTEE',$dataApprove,$where);*/
                                $data['BG_STATUS'] = 12;
                                $data['REASON']  = $suspendNotes;
                                $data['USAGE_PURPOSE']  = implode(",", $data['USAGE_PURPOSE']);
                                $data['BG_UNDERLYING_DOC']  = $sourceFileName;
                                $data['BG_UPDATED']  = date('Y-m-d H:i:s');
                                $data['BG_UPDATEDBY']  = $this->_userIdLogin;
                                // insert ke T_GLOBAL_CHANGES
                                $CUST_NAME = $selectcomp[0]['CUST_NAME'];
                                $info = 'Customer ID = ' . $data['CUST_ID'] . ', Customer Name = ' . $key_value . ', BG Register Number = ' . $data['BG_REG_NUMBER'];
                                $change_id = $this->suggestionWaitingApproval('Bank Guarantee', $info, $this->_changeType['code']['suspend'], null, 'T_BANK_GUARANTEE', 'TEMP_BANK_GUARANTEE', $data['CUST_ID'], $CUST_NAME, $data['CUST_ID'], $CUST_NAME);
                                $data['CHANGES_ID'] = $change_id;
                                $this->_db->insert('TEMP_BANK_GUARANTEE', $data);

                                $this->_db->commit();

                                $this->setbackURL('/' . $this->_request->getModuleName());
                                $this->_redirect('/notification/submited/index');
                            } catch (Exception $e) {
                                echo "error";
                                die;
                                $this->_db->rollBack();
                            }
                            /*$this->setbackURL('/'.$this->_request->getModuleName());
                                    $this->_redirect('/notification/success');*/
                        }
                    } else {

                        $errors = array($adapter->getMessages());
                        var_dump($errors);
                        die;
                    }
                }

                if ($unsuspend) {

                    $UnSuspendNotes = $this->_getParam('UnSuspendNotes');
                    $attahmentDestination     = UPLOAD_PATH . '/document/bg/';
                    $adapter                 = new Zend_File_Transfer_Adapter_Http();
                    $sourceFileName = $adapter->getFileName();

                    if ($sourceFileName == null) {
                        $sourceFileName = null;
                        $fileType = null;
                    } else {
                        $sourceFileName = substr(basename($adapter->getFileName()), 0);
                        if ($_FILES["uploadSourceUnSuspend"]["type"]) {
                            $adapter->setDestination($attahmentDestination);
                            $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                            $fileType = $adapter->getMimeType();
                            $size = $_FILES["uploadSourceUnSuspend"]["size"];
                        } else {
                            $fileType = null;
                            $size = null;
                        }
                    }

                    $fileExt                 = "pdf";
                    $fileTypeMessage = explode('/', $fileType);
                    $fileType =  $fileTypeMessage[1];
                    $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                    // $extensionValidator->setMessage("Extension file must be *.pdf");

                    $maxFileSize = "1024000";
                    $size = number_format($size);

                    $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                    $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                    $adapter->setValidators(array($extensionValidator, $sizeValidator));

                    if ($adapter->isValid()) {
                        // $newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
                        // $adapter->addFilter('Rename', $newFileName);

                        if ($adapter->receive()) {
                            try {
                                $this->_db->beginTransaction();

                                /*$dataApprove =  array(					
                                                                'BG_STATUS' 		=> '12',
                                                                'BG_CANCELEDBY'		=> '',//$cancelBy,
                                                                'BG_CANCEL_NOTES'	=> $suspendNotes,
                                                                'BG_UNDERLYING_DOC'	=> $sourceFileName,
                                                                'BG_UPDATED' 		=> date('Y-m-d H:i:s'), 
                                                                'BG_UPDATEDBY' 		=> $this->_userIdLogin
                                                            );							
                                        
                                        $where = array('BG_REG_NUMBER = ?' => $bgnumb);
                                        $result = $this->_db->update('T_BANK_GUARANTEE',$dataApprove,$where);*/
                                $data['BG_STATUS'] = 13;
                                $data['REASON']  = $UnSuspendNotes;
                                $data['USAGE_PURPOSE']  = implode(",", $data['USAGE_PURPOSE']);
                                $data['BG_UNDERLYING_DOC']  = $sourceFileName;
                                $data['BG_UPDATED']  = date('Y-m-d H:i:s');
                                $data['BG_UPDATEDBY']  = $this->_userIdLogin;
                                // insert ke T_GLOBAL_CHANGES
                                $CUST_NAME = $selectcomp[0]['CUST_NAME'];
                                $info = 'Customer ID = ' . $data['CUST_ID'] . ', Customer Name = ' . $key_value . ', BG Register Number = ' . $data['BG_REG_NUMBER'];
                                $change_id = $this->suggestionWaitingApproval('Bank Guarantee', $info, $this->_changeType['code']['unsuspend'], null, 'T_BANK_GUARANTEE', 'TEMP_BANK_GUARANTEE', $data['CUST_ID'], $CUST_NAME, $data['CUST_ID'], $CUST_NAME);
                                $data['CHANGES_ID'] = $change_id;
                                $this->_db->insert('TEMP_BANK_GUARANTEE', $data);

                                $this->_db->commit();

                                $this->setbackURL('/' . $this->_request->getModuleName());
                                $this->_redirect('/notification/submited/index');
                            } catch (Exception $e) {
                                echo "error";
                                die;
                                $this->_db->rollBack();
                            }
                            /*$this->setbackURL('/'.$this->_request->getModuleName());
                                    $this->_redirect('/notification/success');*/
                        }
                    } else {

                        $errors = array($adapter->getMessages());
                        var_dump($errors);
                        die;
                    }
                }
            }
        }
        $back = $this->_getParam('back');
        if ($back) {
            $this->_redirect('/bgreport');
        }
        //echo '<pre>';
        //var_dump($back);die;

    }

    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }
}
