<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';

class reportdu_DetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	protected $_destinationUploadDir = '';
	protected $_listCCYValidate = '';

	public function initController()
	{       
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 	
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
        		
	}
	
	public function indexAction()
	{		

		$this->_helper->layout()->setLayout('newlayout');
		
		$Settings = new Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();		
		//Zend_Debug::dump($ccyList);die;
		
		$this->view->userIdLogin  = $this->_userIdLogin;
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
        
        $getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;

        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

        // $ddata = $this->_db->select()
        //             ->from(array('A' => 'T_UNDERLYING'),array('*'))
        //             ->query()->fetchAll();

        // echo "<pre>";
        // var_dump($ddata);

        // $payref = $numb = $this->_getParam('DOC_ID');
        $DOC_ID = $numb = $this->_getParam('docid');

        if($DOC_ID){

            $dudata = $this->_db->select()
                    ->from(array('A' => 'T_UNDERLYING'),array('*'))
                    ->where('A.DOC_ID = ?',$DOC_ID)
                    ->query()->fetchAll();

            if($dudata){
                
                $data = $dudata[0];

                $this->view->DOC_ID = $data['DOC_ID'];
                $this->view->CCY = $data['CCY'];

                $config    		= Zend_Registry::get('config');
                $paymentStatus 	= $config["payment"]["status"];
                $paymentType 	= $config["payment"]["type"];
                $transferType 	= $config["transfer"]["type"];

                // case when string
                $caseArr 		 = Application_Helper_General::casePaymentType($paymentType, $transferType);
                $casePaymentType = $caseArr["PS_TYPE"];
                $caseACCTSRC 	 = $caseArr["ACCTSRC"];
                $caseACBENEF 	 = $caseArr["ACBENEF"];
                $casePaymentStatus = Application_Helper_General::caseArray($paymentStatus);

                $select = $this->_db->select()
						->from(array('T' => 'T_TRANSACTION'), array('*'))
						->join(array('P' => 'T_PSLIP'), 'T.PS_NUMBER = P.PS_NUMBER', array('PS_SUBJECT','PS_CCY','PS_TOTAL_AMOUNT','PS_TYPE','PS_STATUS', 'payType' => new Zend_Db_Expr("CASE $casePaymentType ELSE 'N/A' END"), 'payStatus' => new Zend_Db_Expr("CASE P.PS_STATUS $casePaymentStatus ELSE 'N/A' END")))
						// ->where('P.CUST_ID = '.$this->_db->quote((string)$this->_custIdLogin))
						->where('T.DOC_ID = ?', $DOC_ID)
						->query()->fetchAll();

                $this->view->select = $select;

            }
        }

    }
}