<?php
class language_ChangeController extends Application_Main {
	
	public function indexAction(){
		$model = new language_Model_Index();
		$sql = $model->getLang();
		$arrLang = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				$code = explode('_', $val['CODE']);
				$arrLang += array($code[0] => $val['DESC']);
			}
		}
		$this->view->arrLang 	= $arrLang;
		
		$ns = new Zend_Session_Namespace('language');
		
		if($this->_request->isPost()){
			$lang = $this->_request->getPost("lang");
			if (!(empty($lang))){
				if (array_key_exists($lang, $arrLang)){
					
					$data['USER_LANG'] = $lang;
					$where['USER_ID = ?'] = $this->_userIdLogin;
					$where['CUST_ID = ?'] = $this->_custIdLogin;
					$this->_db->update('M_USER',$data,$where);
					
					$ns->langCode = $lang;
				}
			}
			
			$this->_redirect('language/change');
		}
		
		$this->view->lang		= $ns->langCode;
		Application_Helper_General::writeLog('CLAN','Change Language');	
	}
}
