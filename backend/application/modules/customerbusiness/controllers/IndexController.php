<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class customerbusiness_IndexController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $filter_clear     = $this->_getParam('clearfilter');
    
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        $AccArr = $CustomerUser->getAccounts();
        $this->view->AccArr = $AccArr;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        $fields = array(            
            'custcode'     => array('field'   => 'CUST_ID',
                                 'label'   => $this->language->_('Customer Code'),
                                ),
            'custname' => array('field'  => 'CUST_NAME',
                                  'label'  => $this->language->_('Customer Name'),
                                ),
            'transfer'   => array('field'    => 'TRF_TYPE',
                                'label'    => $this->language->_('Transfer Type'),
                                ),
            'profilename'     => array('field'    => 'PROFILE_NAME',
                                  'label'    => $this->language->_('Business Profile Adapter Name'),
                                ),
            'fileformat'  => array('field'    => 'FILE_FORMAT',
                                'label'    => $this->language->_('File Format'),
                                ),
            'status'  => array('field'  => 'STATUS',
                                  'label'  => $this->language->_('Adapter Status'),
                                ),
        );

        $filterlist = array('CUST_ID','CUST_NAME','TRANSFER_TYPE');

        $this->view->filterlist = $filterlist;
                  
        $page    = $this->_getParam('page');        
        $csv    = $this->_getParam('csv');        
        
        $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('C.CUST_ID');
        $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        
        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
        
        $this->view->currentPage = $page;
        $this->view->sortBy      = $sortBy;
        $this->view->sortDir     = $sortDir;
        
        $select2 = $this->_db->select()
                      ->from(array('BG' => 'M_ADAPTER_PROFILE'))
                      ->join(array('C' => 'M_CUST_ADAPTER_PROFILE'), 'BG.PROFILE_ID = C.ADAPTER_PROFILE_ID', array('*'))
                      ->join(array('D' => 'M_CUSTOMER'), 'C.CUST_ID = D.CUST_ID', array('CUST_NAME' => 'D.CUST_NAME'))
                      ->order('C.CUST_ID ASC');
        
        $filterArr = array(
                      'CUST_ID'   => array('StripTags','StringTrim','StringToUpper'),                                               
                      'CUST_NAME'   => array('StripTags','StringTrim','StringToUpper'),                                               
                      'TRANSFER_TYPE'   => array('StripTags','StringTrim','StringToUpper')
                          );
                            
        $dataParam = array('CUST_ID','CUST_NAME','TRANSFER_TYPE');
        $dataParamValue = array();

        foreach ($dataParam as $dtParam)
        {          
          if(!empty($this->_request->getParam('wherecol'))){
            $dataval = $this->_request->getParam('whereval');
              foreach ($this->_request->getParam('wherecol') as $key => $value) {
                if($dtParam==$value){
                  $dataParamValue[$dtParam] = $dataval[$key];
                }
              }

          }
        }

                          
        $validator = array(                  
         'CUST_ID'  => array(),  
         'CUST_NAME'      => array(),              
         'TRANSFER_TYPE'      => array()
          );
       
        $zf_filter   = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);

        if ($zf_filter->isValid()) {
          $filter     = TRUE;
        }
        
         
        $cust_id  = html_entity_decode($zf_filter->getEscaped('CUST_ID'));   
        $cust_name  = html_entity_decode($zf_filter->getEscaped('CUST_NAME'));     
        $transfer_type    = html_entity_decode($zf_filter->getEscaped('TRANSFER_TYPE'));
       
       
        if($filter == null)
        { 
          $datefrom = (date("d/m/Y"));
          $dateto = (date("d/m/Y"));
          $this->view->fDateFrom  = (date("d/m/Y"));
          $this->view->fDateTo  = (date("d/m/Y"));
        }
        
        if($filter_clear == '1'){
          $this->view->fDateFrom  = '';
          $this->view->fDateTo  = '';
          $datefrom = '';
          $dateto = '';
          
        }
      
        if($filter == null || $filter ==TRUE)
        {
          $this->view->fDateFrom = $datefrom;
          $this->view->fDateTo = $dateto;  
        //   if(!empty($datefrom))
        //   {
        //     $FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
        //     $datefrom  = $FormatDate->toString($this->_dateDBFormat); 
        //   }
                
        //   if(!empty($dateto))
        //   {
        //       $FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
        //       $dateto    = $FormatDate->toString($this->_dateDBFormat);
        //   }
      
        //   if(!empty($datefrom) && empty($dateto))
        //       $select2->where("BG.TIME_PERIOD_START >= ".$this->_db->quote($datefrom));
                
        //   if(empty($datefrom) && !empty($dateto))
        //       $select2->where("BG.TIME_PERIOD_END <= ".$this->_db->quote($dateto));
                  
        //   if(!empty($datefrom) && !empty($dateto))
        //       $select2->where("BG.TIME_PERIOD_START >= ".$this->_db->quote($datefrom)." and BG.TIME_PERIOD_END <= ".$this->_db->quote($dateto));
        }

      
        if($filter == TRUE)
        {   
          
          if($cust_id!=null)
          {         
            $this->view->cust_id = $cust_id;
            $select2->where("C.CUST_ID LIKE ".$this->_db->quote('%'.$cust_id.'%'));
          }
          
          if($cust_name != null)
          {
             $this->view->cust_name = $cust_name;
               $select2->where("D.CUST_NAME LIKE ".$this->_db->quote('%'.$cust_name.'%'));
          }

          if($transfer_type != null)
          {
             $this->view->transfer_type = $transfer_type;
               $select2->where("C.TRF_TYPE LIKE ".$this->_db->quote('%'.$transfer_type.'%'));
          }
        }

        $select2->order($sortBy.' '.$sortDir);
      
        if($csv || $pdf || $this->_request->getParam('print'))
        {
          $arr = $this->_db->fetchAll($select2);
          $dataList = array();
          foreach ($arr as $key => $value)
          {
            if($value['STATUS'] == '1'){
                $status = 'Approved';
            }
            $list = array(
                'CUST_ID' => $value['CUST_ID'],
                'CUST_NAME' => $value['CUST_NAME'],
                'TRF_TYPE' => $value['TRF_TYPE'],
                'PROFILE_NAME' => $value['PROFILE_NAME'],
                'FILE_FORMAT' => $value['FILE_FORMAT'],
                'STATUS' => $status
            );

            array_push($dataList, $list);
          }
    
          $header = Application_Helper_Array::simpleArray($fields, 'label');
          if($csv)
          {
            Application_Helper_General::writeLog('DTRX','Download CSV Transaction Report');
            $this->_helper->download->csv($header,$dataList,null,'Customer Business Profile Adapter List');
          }
          
          if($pdf)
          {
            Application_Helper_General::writeLog('DTRX','Download PDF Transaction Report');
            $this->_helper->download->pdf($header,$arr,null,'Executed Transaction');
          }
          if($this->_request->getParam('print') == 1){
            
                      $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields));
              }
      }
      else
      {
        Application_Helper_General::writeLog('DTRX','View Transaction Report');
      }
  
        $this->view->fields = $fields;
        $this->view->filter = $filter;
        $this->paging($select2);

        if(!empty($dataParamValue)){
          foreach ($dataParamValue as $key => $value) {
            $wherecol[] = $key;
            $whereval[] = $value;
          }
            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
          }

    }

}