<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';


class paymentworkflow_ApprovalController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	protected $_paymentRef;
	protected $_tableMst;
	protected $_hasPriviApprovePayment 	= false;
	protected $_hasPriviApproveBene 	= false;
	protected $_hasPriviRepairPayment 	= false;
	protected $_hasPriviRejectPayment 	= false;
	protected $_controllerList 	= "waitingforapproval";

	protected $_bankName;

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
		$this->_paymentstatus   	= $conf['payment']['status'];
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$filter 			= new Application_Filtering();
		$params 					= $this->_request->getParams();
		// print_r($params);
		// $payreff 			= $this->_getParam('payReff');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$trfType1 = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($filter->filter($this->_getParam('payReff'), "PS_NUMBER"));
		$payreff = $AESMYSQL->decrypt($PS_NUMBER, $password);
		$PS_NUMBER = $payreff;

		// $PS_NUMBER 			= $filter->filter($this->_getParam('payReff'), "PS_NUMBER");
		$pdf 				= $this->_getParam('pdf');
		$process 			= $filter->filter($this->_getParam('process'), "BUTTON");
		$approve 			= $this->_getParam('approve');
		$repair 			= $this->_getParam('repair');
		$reject 			= $this->_getParam('reject');
		$this->_paymentRef 	= $PS_NUMBER;
		$this->view->payreff = $payreff;

		$sessionNamespace = new Zend_Session_Namespace('URL_CP_WA');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ?
			$sessionNamespace->URL : '/' . $this->view->modulename . '/' . $this->_controllerList . '/index';


		$userOnBehalf		= $this->_getParam('userOnBehalf', "SELECTION");
		$challengeCode		= $filter->filter($this->_getParam('challengeCode'), "SELECTION");
		$responseCode		= $filter->filter($this->_getParam('responseCode'), "SELECTION");
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$userOnBehalfList =  $CustUser->getUserOnBehalf();

		$this->view->userOnBehalfList	= $userOnBehalfList;
		$this->view->userOnBehalf		= $userOnBehalf;
		if (count($userOnBehalfList) < 1) {
			$error = true;
			$errorMsg[] = "no releaser available";
		}

		// check user may release
		// $paramPayment = array_merge($pslip, $setting);

		// $validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		//		$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr, $step);
		//
		//		if($validate->isError() === true)
		//		{
		//			$error = true;
		//			$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
		//		}
		// End Check validation

		//get Token ID User
		$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?', $userOnBehalf)
			->where('CUST_ID = ?', $this->_custIdLogin)
			->limit(1);

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];
		// check privi...
		$this->_hasPriviApprovePayment 	= $this->view->hasPrivilege('PAPV');
		$this->_hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');
		$this->_hasPriviRepairPayment 	= $this->view->hasPrivilege('PRRP');
		//$this->_hasPriviRepairPayment 	= $this->view->hasPrivilege('PRPP');
		$this->_hasPriviRejectPayment 	= $this->view->hasPrivilege('PRJT');

		$paramPayment = array(
			"WA" 				=> true,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);
		$select->orWhere("P.PS_STATUS = '1' AND P.PS_TYPE IN (38) AND P.CUST_ID = ? ",$this->_custIdLogin);
		// echo $select;die;
		$pslip = $this->_db->fetchRow($select);

		$selectapprover	= $this->_db->select()
			->from(array('C' => 'T_APPROVAL'), array(
				'USER_ID'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
			->where("C.PS_NUMBER = ?", (string) $pslip['PS_NUMBER']);
		// ->where("C.GROUP = ?" , (string)$value);
		// echo $selectapprover;die;
		$usergroup = $this->_db->fetchAll($selectapprover);


		//var_dump($usergroup);die;
		if (!empty($usergroup)) {
			// die;
			// $this->view->pdf = true;
			$this->view->validbtn = false;
		} else {
			if (!empty($pslip)) {

				$boundary = $this->validatebtn($pslip['PS_TYPE'], $pslip['amount'], $pslip['ccy'], $pslip['PS_NUMBER']);
				 //var_dump($boundary);die;
				/*$userBoundary = $this->findBoundary($pslip['PS_TYPE'],$pslip['amount']);*/
				/*$policyBoundary = $this->findPolicyBoundary($pslip['PS_TYPE'],$pslip['amount']);*/
			}

			 //print_r($boundary);die;
			if ($boundary) {
				// die;
				//reverse by DW
				$this->view->validbtn = true;
			} else {
				// die;
				//reverse by DW
				$this->view->validbtn = false;
			}
		}
		
		
		
		// die;
		$selectCheckToken = $this->_db->select()
			->from(array('M_CUSTOMER'), array(
				'CUST_APP_TOKEN'
			))
			->where("CUST_ID = ?", (string) $this->_custIdLogin);

		$checkToken = $this->_db->fetchAll($selectCheckToken);

		$this->view->checkToken = $checkToken[0]["CUST_APP_TOKEN"];

		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_USER'), array(
				'*'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroupspc = $this->_db->fetchAll($selectusergroup);

		
		//var_dump($usergroup);die;
		if (!empty($usergroupspc)) {
			foreach ($usergroupspc as $key => $value) {

				$groupuser = explode("_", $value['GROUP_USER_ID']);

				if ($groupuser[0] == "S") {
					// $usergroupid = "SG";
					$this->view->validbtn = true;
					break;
				}
			}
		}
		
		if (!empty($usergroup)) {
			// die;
			// $this->view->pdf = true;
			$this->view->validbtn = false;
		}
		//else{
		//	$this->view->validbtn = false;
		//}
		//var_dump($this->view->validbtn);die;
		
		$selectdatachange = $this->_db->select()
			  ->from('T_GLOBAL_CHANGES');
		$selectdatachange -> where("CUST_ID = ".$this->_db->quote($this->_custIdLogin)." OR CUST_ID ='BANK'");
		// $selectdatachange -> where("CHANGES_STATUS = ? ",'WA');
		$selectdatachange -> where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS ='RR'");
		// $selectdatachange -> where("CHANGES_STATUS = ? ",'RR');
		$selectdatachange -> where("COMPANY_CODE = ? ",$this->_custIdLogin);
		$selectdatachange -> where("DISPLAY_TABLENAME = ? ",'Approver Matrix');

		// echo $selectdatachange;die();
		 $selectdatachange1 = $selectdatachange->query()->FetchAll();

		 // echo "<pre>";
		 // var_dump($selectdatachange1);
		 // die();
		$error_msg2 = false;
		if(!empty($selectdatachange1)){
		  $this->view->validbtn = false;
		  $error_msg2 = true;
		  $this->view->error_msg2  = 'Cannot authorize, waiting for approval matrix changes.';
		}

		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'ACBENEF_ALIAS_NAME'	=> 'TT.BENEFICIARY_ALIAS_NAME',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					// 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $this->_bankName . "'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN B.BENEFICIARY_ISAPPROVE = '0' THEN 'Waiting Approval'
																				 WHEN B.BENEFICIARY_ISAPPROVE = '1' THEN 'Approved'
																				 ELSE '-'
																			END"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		// echo $selectTrx;die;
		// 							print_r($select->query());die;
		$pslipTrxData = $this->_db->fetchAll($selectTrx);



		$this->view->PS_TYPE = $pslip['PS_TYPE'];
		$this->view->AMOUNT_IDR = $pslip['EQUIVALENT_AMOUNT_IDR'];
		$this->view->ACCT_CCY = $pslip['SOURCE_ACCOUNT_CCY'];


		if (!empty($pslip)) {
			if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5' || $pslip['PS_TYPE'] == '11') {
				$PS_TYPE = '18';
			} else {
				$PS_TYPE = $pslip['PS_TYPE'];
			}


			if ($pslip['PS_TYPE'] == '20' || $pslip['PS_TYPE'] == '19') {
				$selectuser	= $this->_db->select()
					->from(array('C' => 'M_APP_BOUNDARY'), array(
						'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
						'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
						'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
						'C.TRANSFER_TYPE',
						'C.POLICY'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.TRANSFER_TYPE 	= ?", (string) $PS_TYPE);
				// ->where("C.BOUNDARY_MIN 	<= ?" , $pslip['amount'])
				// ->where("C.BOUNDARY_MAX 	>= ?" , $pslip['amount']);	
			} else {
				$selectuser	= $this->_db->select()
					->from(array('C' => 'M_APP_BOUNDARY'), array(
						'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
						'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
						'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
						'C.TRANSFER_TYPE',
						'C.POLICY'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.TRANSFER_TYPE 	= ?", (string) $PS_TYPE)
					->where("C.BOUNDARY_MIN 	<= ?", $pslip['amount'])
					->where("C.BOUNDARY_MAX 	>= ?", $pslip['amount']);
			}

			// echo $selectuser;
			$datauser = $this->_db->fetchAll($selectuser);



			// print_r($pslip);die;
			$this->view->datauserdata = $datauser;
		}
		if (!empty($pslip)) {
			// Payment Status is not Waiting Approval
			if ($pslip["PS_STATUS"] != $this->_paymentstatus["code"]["waitingforapproval"]) {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error') . ": " . $this->language->_('Payment Status has changed') . ".");
				// $this->_redirect('/'.$this->view->modulename.'/'.$this->_controllerList.'/index');
			}
		} else	// ps_number is invalid, or not belong to customer, or user don't have right to view this payment
		{
			// die('here');
			// print_r('expression');die();
			// $this->_helper->getHelper('FlashMessenger')->addMessage("F");
			// $this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
			// $this->_redirect('/'.$this->view->modulename.'/'.$this->_controllerList.'/index');
		}


		if ($reject) {
			$token = true;
			if ($checkToken['0']['CUST_APP_TOKEN'] == '1') {
				$HardToken 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
				$resHard = $HardToken->validateOtp($responseCode);
				$resultToken = $resHard['ResponseCode'] == '0000';

				if ($resHard['ResponseCode'] != '00') {
					$tokenFailed = $CustUser->setLogToken(); //log token activity

					$error = true;
					$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
					$token = false;
					if ($tokenFailed === true) {
						$this->_redirect('/default/index/logout');
					}
				} else {
					$paymentData = array('PS_RELEASER_CHALLENGE' => $challengeCode);

					$whereData 	 = array(
						'PS_NUMBER = ?' => (string) $PS_NUMBER,
						'CUST_ID = ?'	 => (string) $this->_custIdLogin
					);

					$this->_db->update('T_PSLIP', $paymentData, $whereData);
					$token = true;
				}
			}
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviRejectPayment && $token) {
				$PS_REASON = $filter->filter($this->_getParam('PS_REASON_REJECT'), "MESSAGE");
				$Payment->rejectPayment($PS_REASON);
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to reject payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		if ($repair) {
			$token = true;
			if ($checkToken['0']['CUST_APP_TOKEN'] == '1') {
				$HardToken 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
				$resHard = $HardToken->validateOtp($responseCode);
				$resultToken = $resHard['ResponseCode'] == '0000';

				if ($resHard['ResponseCode'] != '00') {
					$tokenFailed = $CustUser->setLogToken(); //log token activity

					$error = true;
					$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
					$token = false;
					if ($tokenFailed === true) {
						$this->_redirect('/default/index/logout');
					}
				} else {
					$paymentData = array('PS_RELEASER_CHALLENGE' => $challengeCode);

					$whereData 	 = array(
						'PS_NUMBER = ?' => (string) $PS_NUMBER,
						'CUST_ID = ?'	 => (string) $this->_custIdLogin
					);

					$this->_db->update('T_PSLIP', $paymentData, $whereData);
					$token = true;
				}
			}
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviRepairPayment && $token) {
				$PS_REASON = $filter->filter($this->_getParam('PS_REASON_REPAIR'), "MESSAGE");
				$Payment->requestRepair($PS_REASON);
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to request repair payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		$filter->__destruct();
		unset($filter);

		$tra_type1 = $trfType1[$pslip['TRANSFER_TYPE']];

		$tra_type2	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");
		$tra_type3 = $tra_type2[$pslip['TRANSFER_TYPE']];
                
        if ($pslip['PS_TYPE'] == '19') {
             $payType = 'CP Same Bank Remains';
        }else if ($pslip['PS_TYPE'] == '20') {
             $payType = 'CP Same Bank Maintains';
        }else if ($pslip['PS_TYPE'] == '23') {
             $payType = 'CP Others Remains - '.$tra_type3;
        }else if ($pslip['PS_TYPE'] == '21') {
             $payType = 'MM - '.$tra_type3;
        }else if($pslip['PS_TYPE'] == '16' || $pslip['PS_TYPE'] == '17' || $pslip['PS_TYPE'] == '25' ||  $pslip['PS_TYPE'] == '4' ||  $pslip['PS_TYPE'] == '26'  ){
			$payType = $pslip['payType'];
		}else if ($pslip['PS_TYPE'] == '38') {
             $payType = 'Inhouse - BG';
        }else {
            $payType = $pslip['payType'] . ' - ' . $tra_type1;
        }

		$this->view->payType = $payType;

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }

		// View Data
		$this->_tableMst[0]["label"] = $this->language->_('Payment Ref') . '#';
		$this->_tableMst[1]["label"] = $this->language->_('Created Date');
		$this->_tableMst[2]["label"] = $this->language->_('Updated Date');
		$this->_tableMst[3]["label"] = $this->language->_('Payment Date');
		$this->_tableMst[4]["label"] = $this->language->_('Payment Subject');
		$this->_tableMst[5]["label"] = $this->language->_('Master Account');
		$this->_tableMst[6]["label"] = $this->language->_('Payment Type');
		$this->_tableMst[7]["label"] = $this->language->_('Source Bank Name');
		$this->_tableMst[8]["label"] = $this->language->_('Source Alias');
		$this->_tableMst[9]["label"] = $this->language->_('Beneficiary Alias');

		$this->_tableMst[0]["value"] = $PS_NUMBER;
		$this->_tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->_dateTimeDisplayFormat);
		$this->_tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->_dateTimeDisplayFormat);
		$this->_tableMst[3]["value"] = Application_Helper_General::convertDate($pslip['efdate'], $this->_dateViewFormat);
		$this->_tableMst[4]["value"] = $pslip['paySubj'];
		$this->_tableMst[5]["value"] = "";
		if (!empty($pslipTrxData) && ($pslipTrxData['0']['TRANSFER_TYPE'] == '8' || $pslipTrxData['0']['TRANSFER_TYPE'] == '7')) {

			$pslip['payType'] = $pslipTrxData['0']['TRANSFER_TYPE_disp'];
		}
		$this->_tableMst[6]["value"] = $pslip['payType'];

		if (empty($pslip['SOURCE_ACCT_BANK_CODE'])) {
			$bankname = $this->_bankName;
		} else {
			$bankname = $bankNameArr[$pslip['SOURCE_ACCT_BANK_CODE']];
		}

		$sourcealias = '-';
		if($pslip['SOURCE_ACCOUNT_ALIAS_NAME'] != ''){
			$sourcealias = $pslip['SOURCE_ACCOUNT_ALIAS_NAME'];
		}
		$benefalias = '-';
		if($pslip['BENEFICIARY_ALIAS_NAME'] != ''){
			$benefalias = $pslip['BENEFICIARY_ALIAS_NAME'];
		}

		$this->_tableMst[7]["value"] = $bankname;
		$this->_tableMst[8]["value"] = $sourcealias;
		$this->_tableMst[9]["value"] = $benefalias;


		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && !$pdf) {
			// download trx bulk file
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
		}

		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT") {
			$this->view->transtype  = $pslip['payType'];
			$this->view->pstype 	= $pslip['PS_TYPE'];
			$this->view->bulkdetail = true;
			$this->view->trxcount = $pslip['PS_TXCOUNT'];
			$this->view->trxamount = $pslip['PS_TOTAL_AMOUNT'];
			$this->view->trxccy		= $pslip['acbenef_ccy'];
			$this->view->trxsubject = $pslip['PS_SUBJECT'];
			$this->view->trxdate 	= Application_Helper_General::convertDate($pslip['efdate'], $this->_dateViewFormat);
			$this->view->sourceacc = $pslip['SOURCE_ACCOUNT'];
			$this->view->sourcename = $pslip['accsrc_bankname'];
		}

		// if($pslip["PS_CATEGORY"] == "SWEEP PAYMENT"){
		// 	$this->view->trxsubject = $pslip['PS_SUBJECT'];
		// 	$this->view->trxdate 	= Application_Helper_General::convertDate($pslip['efdate'], $this->_dateViewFormat);
		// }
		
		// if($pslip["PS_CATEGORY"] == "SINGLE PAYMENT"){
			$this->view->trxsubject = $pslip['PS_SUBJECT'];
			$this->view->trxdate 	= Application_Helper_General::convertDate($pslip['efdate'], $this->_dateViewFormat);
		// }
		// 		print_r($pslip["PS_TYPE"]);
		// 						print_r($this->_paymenttype["code"]);
		// separate credit and debet view
		// if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"]) {
		// 	$this->sweepin($pslip);
		// } elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"]) {
		// 	$this->sweepout($pslip);
		// } elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]) {
		// 	// 		    echo 'here';
		// 	$this->debet($pslip);
		// } else {
		// 	// print_r($pslip);
		// 	// die('here1');
		// 	$this->credit($pslip);
		// }
	//	var_dump($pslip["PS_TYPE"]);die;
		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"]) {
			$this->sweepin($pslip);
		} elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"]) {
			$this->sweepout($pslip);
		} elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]) {
			// 		    echo 'here';
			$this->debet($pslip);
		} elseif ($pslip["PS_TYPE"] == '17' && ($pslip["PS_BILLER_ID"] == '1158' || $pslip["PS_BILLER_ID"] == '1156')) {
			$this->etax($pslip);
		} elseif ($pslip["PS_TYPE"] == '18') {
			$this->sp2d($pslip);
		} elseif ($pslip["PS_TYPE"] == '19') {
			// 		    echo 'here';
			$this->sweepin($pslip);
		}elseif ($pslip["PS_TYPE"] == '23') {
			// 		    echo 'here';
			$this->sweepoutothers($pslip);
		} elseif ($pslip["PS_TYPE"] == '20') {
			// 		    echo 'here';
			$this->sweepout($pslip);
		}elseif ($pslip["PS_TYPE"] == '21') {
			// 		    echo 'here';
			$this->creditopen($pslip);
		} else {
			//die('gee');
			$this->credit($pslip);
		}
		// 		die;
		// approve after validate payment	
		if ($approve && $this->view->valError === false) {
			// die('here');
			$token = true;
			// var_dump($checkToken['0']['CUST_APP_TOKEN']);
			if ($checkToken['0']['CUST_APP_TOKEN'] == '1') {
				$HardToken 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
				$resHard = $HardToken->validateOtp($responseCode);
				$resultToken = $resHard['ResponseCode'] == '0000';

				if ($resHard['ResponseCode'] != '00') {
					$tokenFailed = $CustUser->setLogToken(); //log token activity

					$error = true;
					$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
					$token = false;
					if ($tokenFailed === true) {
						$this->_redirect('/default/index/logout');
					}
				} else {
					$paymentData = array('PS_RELEASER_CHALLENGE' => $challengeCode);

					$whereData 	 = array(
						'PS_NUMBER = ?' => (string) $PS_NUMBER,
						'CUST_ID = ?'	 => (string) $this->_custIdLogin
					);

					$this->_db->update('T_PSLIP', $paymentData, $whereData);
					$token = true;
				}
			}

			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			// var_dump($this->_hasPriviApprovePayment);var_dump($token);die;
			if ($this->_hasPriviApprovePayment && $token) {
				// die;
				$Payment->approvePayment();
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to approve payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT") {
			$this->view->fields			 = array();
			//$this->view->tableDtl 		 = array();
			$this->view->TITLE_DTL		 = "";
		}
		if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '6' || $pslip['PS_TYPE'] == '7' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '11' || $pslip['PS_TYPE'] == '16' || $pslip['PS_TYPE'] == '17') {
			$this->view->repairbtn = 'hidden';
		}
		//print_r($pslip);die;
		$this->view->pslip 			= $pslip;
		$this->view->PS_NUMBER 		= $PS_NUMBER;
		$this->view->tableMst 		= $this->_tableMst;
		$this->view->totalTrx 		= $pslip["numtrx"];

		$select  = $this->_db->select()
			->from(array('P' => 'T_PSLIP'), array('P.*', 'A.*'))
			->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
			->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array('*'))
			->joinLeft(array('C' => 'T_PSLIP_DETAIL'), 'P.PS_NUMBER = C.PS_NUMBER', array('*'))
			->where('P.PS_NUMBER = ?', $PS_NUMBER);

		$cekPsNumber = $this->_db->fetchRow($select);
		$PS_EVERY_PERIODIC_UOM = $cekPsNumber['PS_EVERY_PERIODIC_UOM'];

		if ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 1) {
			$select = $this->_db->select()
				->from(array('C' => 'T_PERIODIC_DAY', array('C.DAY_ID')))
				->where("C.PERIODIC_ID  = ?", $cekPsNumber["PS_PERIODIC"]);

			$PERIODIC_DAY = $this->_db->fetchAll($select);
			$days = array();
			foreach ($PERIODIC_DAY as $key) {
				$days[] = (int) $key['DAY_ID'];
			}
			$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
			$dateNow = date("Y-m-d H:i");
			$result = $date;
			foreach ($days as $day) {
				if ($dateNow > $result) {
					$date = $dateNow;
					$dayofweek = date('N', strtotime($date));
					$result = date('Y-m-d', strtotime(($day - $dayofweek) . ' day', strtotime($date)));
					$result = $result . ' ' . $cekPsNumber["PS_EFTIME"];
				}
			}

			if ($result < $dateNow) {
				$date = date("Y-m-d", strtotime("+1 week", strtotime($date)));
				$dayofweek = date('N', strtotime($date));
				$result = date('Y-m-d H:i', strtotime(($days[0] - $dayofweek) . ' day', strtotime($date)));
			}
			$date = $result;
			$date = strtotime($date);
			if (date('Y-m-d', $date) == date('Y-m-d')) {
				$nextExecute = $this->language->_('Today') . ' @' . date('H:i', $date);
			} else {
				$nextExecute = date('d M Y @H:i', $date);
			}
		} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 2 || $cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 3) {
			$date = $cekPsNumber["PS_PERIODIC_NEXTDATE"];
			$nextExecute = $date;
		} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 5) {
			$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
			$dateNow = date("Y-m-d H:i");

			if ($dateNow > $date) {
				$date = $dateNow;
			}
			$dayofweek = date('N', strtotime($date));
			$result = date('Y-m-d', strtotime(($cekPsNumber['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
			$result = $result . ' ' . $cekPsNumber["PS_EFTIME"];
			if ($result < $dateNow) {
				$date = date("Y-m-d", strtotime("+1 week"));
				$result = date('Y-m-d', strtotime(($cekPsNumber['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
			}
			$nextExecute = date("Y-m-d", strtotime($result));
		} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 6) {
			$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
			$dateNow = date("Y-m-d H:i");

			if ($date < $dateNow) {
				$nextExecute = date("Y-m-d", strtotime("+1 month", strtotime($date)));
			}
		}

		// if ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] != 1) {
		// 	$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
		// 	// var_dump($date);
		// 	$date = strtotime($date);
		// 	if (date('Y-m-d', $date) == date('Y-m-d')) {
		// 		$nextExecute = $this->language->_('Today') . ' @' . date('H:i', $date);
		// 	} else {
		// 		$nextExecute = date('d M Y @H:i', $date);
		// 	}
		// }
		// var_dump($date);
		// die();
		$this->view->nextpaymentdate = substr(Application_Helper_General::convertDate($nextExecute, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
		$this->view->billerid = $cekPsNumber['BILLER_ORDER_ID'];
		if($cekPsNumber['PS_TYPE'] == '17'){
			$billertype = 'Payment';
		}else if($cekPsNumber['PS_TYPE'] == '16'){
			$billertype = 'Purchase';
		}
		$this->view->billertype = $billertype;

		if($cekPsNumber['PS_TYPE'] == '17' || $cekPsNumber['PS_TYPE'] == '16' ){
			
			$biller  = $this->_db->select()
							->from(array('D' => 'T_PSLIP_DETAIL'), array('*'))
							->where('D.PS_NUMBER = ?', $PS_NUMBER)
							->query()->fetchAll();
		}

		foreach($biller as $key => $val){
			if($val['PS_FIELDNAME'] == 'Type Of Transaction'){
				$billtype = $val['PS_FIELDVALUE'];
			}else if($val['PS_FIELDNAME'] == 'Service Provider'){
				$billname = $val['PS_FIELDVALUE'];
			}else if($val['PS_FIELDNAME'] == 'Customer Name'){
				$billcust = $val['PS_FIELDVALUE'];
			}
		}

		//$this->view->billertype = $billtype;
		if(empty($billname)){
			$str = $this->string_between_two_string($cekPsNumber['TRA_MESSAGE'],'ID Pelanggan = ',' no');
			$billname = $str;
			
		}
		$logdata = json_decode($cekPsNumber['LOG'], true); 
			if($logdata['Module'] == 'Phone'){
				$custname = $logdata['CUSTOMER_NAME'];
				$this->view->billercust = $custname;
			}
			$billtype = $logdata['TypeOfTrans'];
	//var_dump($billername);die;
		if($pslip['PS_BILLER_ID'] == '1158'){
			$billtype = 'Tax';
			$billname = 'MPN G3';
		}
		$custname = $logdata['CUSTOMER_NAME'];
		$this->view->billercust = $custname;
		$this->view->billertype = $billtype;
		$this->view->billername = $billname;

		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"]) {
			// 			print_r($pslip);die;
			if(!empty($pslip['PERIODIC'])){
			$select	= $this->_db->select()
				// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
				->from(array('TTS' => 'T_PERIODIC'))
				->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
				->where('TTS.PS_PERIODIC = ?', $pslip['PERIODIC']);
			//echo $select;
			$sweepdetail = $this->_db->fetchAll($select);
			//die('here');
			//print_r($sweepdetail);die;
			//			if($pslip['BALANCE_TYPE']=='2'){
			$totalamount = 0;
			$temptotal = 0;
			foreach ($sweepdetail as $key => $value) {
				$totalamount = $temptotal + $value['TRA_REMAIN'];
				$temptotal = $totalamount;
			}
			}else{
				$temptotal = '-';
			}
			// 				echo '<pre>';
			//				print_r($temptotal);die;
			$this->view->totalAmt 			= $temptotal;
			//			}else{
			//				$this->view->totalAmt 			= $pslip["amount"];
			//			}

		} else {
			//print_r($pslip);die;
			if ($pslip['PS_TYPE'] == '3' && $pslip['acbenef_ccy'] == 'USD' && $pslip['accsrc_ccy'] == 'USD') {
				$this->view->totalAmt 			= $pslip["EQUIVALEN_IDR"];
				$this->view->ps_ccy 			= $pslip["ccy"];
			} else if ($pslip['PS_TYPE'] == '3') {
				$this->view->totalAmt 			= $pslip["EQUIVALEN_IDR"];
				$this->view->ps_ccy 			= 'IDR';
			} else {
				$this->view->totalAmt 			= $pslip["amount"];
				$this->view->ps_ccy 			= $pslip["ccy"];
			}
		}
		$this->view->allowRepair 	 	= ($this->_hasPriviRepairPayment && $pslip["PS_CATEGORY"] != "BULK PAYMENT" && $pslip["PS_BILLER_ID"] != "1156" && $pslip["PS_BILLER_ID"] != "1158");
		$this->view->allowReject 	 	= $this->_hasPriviRejectPayment;
		$this->view->pdf 				= ($pdf) ? true : false;
		$this->view->payReff 			= $this->_getParam('payReff');



		if ($pdf) {

			$this->view->pdfCustName = $this->_custNameLogin;

			foreach ($pslipTrxData as $key => $value) {

				$this->view->pdfBankName 		= $value['BANK_NAME'];
				$this->view->pdfAccount 		= $value['ACBENEF'];
				$this->view->pdfAccountName 	= $value['ACBENEF_NAME'];
				$this->view->pdfAccountAlias 	= $value['ACBENEF_ALIAS_NAME'];
			}

			$paymentRef = $pslip['PS_NUMBER'];
			$Payment = new 	Payment($paymentRef);
			$listHistory =  $Payment->getHistory();

			$this->view->paymentHistory =  $listHistory;
			$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;

			$this->view->pdfPsNumber 		= $pslip['PS_NUMBER'];
			$this->view->pdfPsSubject 		= $pslip['PS_SUBJECT'];
			$this->view->pdfPayType 		= $pslip['payType'];
			$this->view->pdfPsCreated 	 	= $pslip['PS_CREATED'];
			$this->view->pdfPsTotalAmount 	= $pslip['PS_TOTAL_AMOUNT'];
			$this->view->pdfPsCcy		 	= $pslip['PS_CCY'];
			$this->view->pdfAccsrcBankName 	= $pslip['accsrc_bankname'];
			$this->view->pdfSourceAccount 	= $pslip['SOURCE_ACCOUNT'];
			$this->view->pdfAccsrcName 		= $pslip['accsrc_name'];
			$this->view->pdfAccsrcAlias 	= $pslip['accsrc_alias'];

			$frontendOptions = array(
				'lifetime' => 86400,
				'automatic_serialization' => true
			);
			$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
			$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

			$cacheID = 'USERLIST';

			$userlist = $cache->load($cacheID);
			$this->view->pdfUserlist 	= $userlist;

			$tempdir = APPLICATION_PATH . "/../public/QRImages/";

			$teks_qrcode    = "Membuat QR Code dengan PHP";
			$namafile       = $pslip['PS_NUMBER'] . ".png";
			$quality        = "H";
			$ukuran         = 5;
			$padding        = 1;
			$qrImage        = $tempdir . $namafile;
			QRCode::png($teks_qrcode, $tempdir . $namafile, $quality, $ukuran, $padding);

			$outputHTML = $this->view->render($this->view->controllername . '/pdf.phtml');
			//echo $outputHTML;die();
			$this->_helper->download->pdfWithQr(null, null, null, 'Approval', $outputHTML);
		}



		//cari policy
		if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5') {
			$PS_TYPE = '18';
		} else {
			$PS_TYPE = $pslip['PS_TYPE'];
		}

		$policyBoundary = $this->findPolicyBoundary($PS_TYPE, $pslip['amount']);
		//echo '<pre>';
		//var_dump($policyBoundary);die;
		$checkBoundary = $boundary;

		//cek privilage
		$custidlike = '%' . $this->_custIdLogin . '%';

		//select user with reviewer privilage
		$selectReviewer = $this->_db->select()
			->from(array('P' => 'M_FPRIVI_USER'))
			->from(array('U' => 'M_USER'))
			->where("P.FPRIVI_ID = 'RVPV' AND P.FUSER_ID LIKE ?", (string) $custidlike)
			->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
			->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

		$userReviewer = $this->_db->fetchAll($selectReviewer);

		$reviewerList = array();
		foreach ($userReviewer as $row) {
			$userIdReviewer = explode($this->_custIdLogin, $row['FUSER_ID']);

			//get user name
			$selectReviewerName = $this->_db->select()
				->from('M_USER')
				->where("USER_ID = ?", (string) $userIdReviewer[1]);

			$userReviewerName = $this->_db->fetchAll($selectReviewerName);

			array_push($reviewerList, $userReviewerName[0]['USER_FULLNAME']);
		}

		//function utk munculin button dengan policy grup jika belum ada yg approve
		if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5' ) {
			$PS_TYPE = '18';
		} else {
			$PS_TYPE = $pslip['PS_TYPE'];
		}

		$approverUserList = $this->findUserBoundary($PS_TYPE, $pslip['amount']);


		//select user with releaser privilage
		$selectReleaser = $this->_db->select()
			->from(array('P' => 'M_FPRIVI_USER'))
			->from(array('U' => 'M_USER'))
			->where("P.FPRIVI_ID = 'PRLP' AND P.FUSER_ID LIKE ?", (string) $custidlike)
			->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
			->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

		$userReleaser = $this->_db->fetchAll($selectReleaser);

		$releaserList = array();
		foreach ($userReleaser as $row) {
			$userIdReleaser = explode($this->_custIdLogin, $row['FUSER_ID']);

			//get user name
			$selectReleaserName = $this->_db->select()
				->from('M_USER')
				->where("USER_ID = ?", (string) $userIdReleaser[1]);

			$userReleaserName = $this->_db->fetchAll($selectReleaserName);

			array_push($releaserList, $userReleaserName[0]['USER_FULLNAME']);
		}

		//cek ada privilage reviewer atau approver
		$selectpriv = $this->_db->select()
			->from(array('M_CUSTOMER'))
			->where("CUST_ID = ?", $this->_custIdLogin);

		$userpriv = $this->_db->fetchAll($selectpriv);

		if ($userpriv[0]['CUST_REVIEW'] != 1) {
			$cust_reviewer = 0;
		} else {
			$cust_reviewer = 1;
		}

		if ($userpriv[0]['CUST_APPROVER'] != 1) {
			$cust_approver = 0;
		} else {
			$cust_approver = 1;
		}

		//cek t_pslip_history


		//$nostats = array(5,2);
		//$nostats = array();
		$selectHistory	= $this->_db->select()
			->from('T_PSLIP_HISTORY')
			->where("PS_NUMBER = ?", $PS_NUMBER);
		//	->where("HISTORY_STATUS NOT IN (?)",$nostats);

		$history = $this->_db->fetchAll($selectHistory);
		
		//echo '<pre>';
		//var_dump($pslip);die;
		foreach ($history as $row) {
			//if maker done
			if ($row['HISTORY_STATUS'] == 1) {
				$makerStatus = 'active';
				$makerIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
					$reviewerOngoing = '';
					$approverOngoing = '';
					$releaserOngoing = 'ongoing';
				} else {
					$reviewerOngoing = 'ongoing';
					$approverOngoing = '';
					$releaserOngoing = '';
				}

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin)
					->where("CUST_ID = ?", $row['CUST_ID']);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				$custEmail 	  = $customer[0]['USER_EMAIL'];
				$custPhone	  = $customer[0]['USER_PHONE'];

				$makerApprovedBy = $custFullname;

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

				$align = 'align="center"';
				$marginRight = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginRight = 'style="margin-right: 15px;"';
				}

				$this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
			}
			//if reviewer done
			if ($row['HISTORY_STATUS'] == 15) {
				$makerStatus = 'active';
				$reviewStatus = 'active';
				$reviewIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'ongoing';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin)
					->where("CUST_ID = ?", $row['CUST_ID']);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$reviewerApprovedBy = $custFullname;

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->reviewerApprovedBy = '<div align="center" class="textTheme">' . $efdate . '<br>' . $custFullname . '</div>';
			}
			//if approver done
			if ($row['HISTORY_STATUS'] == 2) {
				$makerStatus = 'active';
				$approveStatus = '';
				$reviewStatus = 'active';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'ongoing';
				$releaserOngoing = '';
				
				$custlogin = $row['USER_LOGIN'];
				
				$selectuserapp	= $this->_db->select()
				->from(array('C' => 'T_APPROVAL'), array(
					'*'
				))
				->where("C.PS_NUMBER = ?", $PS_NUMBER)
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				//->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
				;

				$userapprove = $this->_db->fetchAll($selectuserapp);
				//tampung data user yang sudah approve
				if(!empty($userapprove) && empty($userid)){
					foreach($userapprove as $vl){
						$userid[] = $vl['USER_ID'];
					}
				}
				//$userid[] = $custlogin;

				$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
			}
			//if releaser done
			if ($row['HISTORY_STATUS'] == 5) {
				$makerStatus = 'active';
				$approveStatus = 'active';
				$reviewStatus = 'active';
				$releaseStatus = 'active';
				$releaseIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = '';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin)
					->where("CUST_ID = ?", $row['CUST_ID']);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$releaserApprovedBy = $custFullname;

				$align = 'align="center"';
				$marginLeft = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginLeft = 'style="margin-left: 15px;"';
				}

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
			}
		}

		//approvernamecircle jika sudah ada yang approve
		if (!empty($userid)) {

			$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

			$flipAlphabet = array_flip($alphabet);

			$approvedNameList = array();
			$i = 0;
			//var_dump($userid);die;
			foreach ($userid as $key => $value) {

				//select utk nama dan email
				$selectusername = $this->_db->select()
					->from(array('M_USER'), array(
						'*'
					))
					->where("USER_ID = ?", (string) $value)
					->where("CUST_ID = ?", (string) $this->_custIdLogin);

				$username = $this->_db->fetchAll($selectusername);

				//select utk cek user berada di grup apa
				$selectusergroup	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array(
						'*'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.USER_ID 	= ?", (string) $value);

				$usergroup = $this->_db->fetchAll($selectusergroup);

				$groupuserid = $usergroup[0]['GROUP_USER_ID'];
				$groupusername = $usergroup[0]['USER_ID'];
				$groupuseridexplode = explode("_", $groupuserid);

				if ($groupuseridexplode[0] == "S") {
					$usergroupid = "SG";
				} else {
					$usergroupid = $alphabet[$groupuseridexplode[2]];
				}

				// $tempuserid = "";
				// foreach ($approverNameCircle as $row => $data) {
				// 	foreach ($data as $keys => $val) {
				// 		if ($keys == $usergroupid) {
				// 			if (preg_match("/active/", $val)) {
				// 				continue;
				// 			}else{
				// 				if ($groupuserid == $tempuserid) {
				// 					continue;
				// 				}else{
				// 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
				// 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
				// 				}
				// 				$tempuserid = $groupuserid;
				// 			}
				// 		}
				// 	}
				// }

				array_push($approvedNameList, $username[0]['USER_FULLNAME']);

				$efdate = $approveEfDate[$i];

				$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
				$i++;
			}
			
			$this->view->approverApprovedBy = $approverApprovedBy;


			//kalau sudah approve semua
			if (!$checkBoundary) {
				$approveStatus = '';
				$approverOngoing = '';
				$approveIcon = '';
				$releaserOngoing = 'ongoing';
			}
		}

		$selectsuperuser = $this->_db->select()
			->from(array('C' => 'T_APPROVAL'))
			->where("C.PS_NUMBER 	= ?", $PS_NUMBER)
			->where("C.GROUP 	= 'SG'");

		$superuser = $this->_db->fetchAll($selectsuperuser);

		if (!empty($superuser)) {
			$userid = $superuser[0]['USER_ID'];

			//select utk nama dan email
			$selectusername = $this->_db->select()
				->from(array('M_USER'), array(
					'*'
				))
				->where("USER_ID = ?", (string) $userid)
				->where("CUST_ID = ?", (string) $this->_custIdLogin);

			$username = $this->_db->fetchAll($selectusername);

			$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

			$approveStatus = '';
			$approverOngoing = '';
			$approveIcon = '';
			$releaserOngoing = 'ongoing';
		}
		// <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
		//define circle
		$makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '
					
				</button>';

		foreach ($reviewerList as $key => $value) {

			$textColor = '';
			if ($value == $reviewerApprovedBy) {
				$textColor = 'text-white-50';
			}

			$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
		}
		// 
		$reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '
		
					</button>';
		
		$groupNameList = $approverUserList['GROUP_NAME'];
		unset($approverUserList['GROUP_NAME']);

		if ($approverUserList != '') {
			//echo '<pre>';
			//var_dump($approverUserList);die;
			foreach ($approverUserList as $key => $value) {
				$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
				$i = 1;
				foreach ($value as $key2 => $value2) {

					$textColor = '';
					if (in_array($value2, $approvedNameList)) {
						$textColor = 'text-white-50';
					}

					if ($i == count($value)) {
						$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
					} else {
						$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
					}
					$i++;
				}
			}
		} else {
			$approverListdata = 'There is no Approver User';
		}
		// 
		$spandata = '';
		if (!empty($approverListdata) && !$error_msg2) {
			$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
		}

		$approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '
						' . $spandata . '
					</button>';

		foreach ($releaserList as $key => $value) {

			$textColor = '';
			if ($value == $releaserApprovedBy) {
				$textColor = 'text-white-50';
			}

			$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
		}
		// 
		$releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '
		<span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span>
					</button>';

		// $makerNameCircle = str_ireplace('{active}', $makerStatus, $makerNameCircle);
		// $reviewerNameCircle = str_ireplace('{active}', $reviewStatus, $reviewerNameCircle);
		// $approverNameCircle = str_ireplace('{active}', $approveStatus, $approverNameCircle);
		// $releaserNameCircle = str_ireplace('{active}', $releaseStatus, $releaserNameCircle);

		// $makerNameCircle = str_ireplace('{ongoing}', $makerOngoing, $makerNameCircle);
		// $reviewerNameCircle = str_ireplace('{ongoing}', $reviewerOngoing, $reviewerNameCircle);
		// $approverNameCircle = str_ireplace('{ongoing}', $approverOngoing, $approverNameCircle);
		// $releaserNameCircle = str_ireplace('{ongoing}', $releaserOngoing, $releaserNameCircle);
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$this->view->cust_reviewer = $cust_reviewer;
		$this->view->cust_approver = $cust_approver;
		if($error_msg2){
			$policyBoundary = '';
		}
		
		$this->view->policyBoundary = $policyBoundary;
		$this->view->makerNameCircle = $makerNameCircle;
		$this->view->reviewerNameCircle = $reviewerNameCircle;
		$this->view->approverNameCircle = $approverNameCircle;
		$this->view->releaserNameCircle = $releaserNameCircle;

		$this->view->makerStatus = $makerStatus;
		$this->view->approveStatus = $approveStatus;
		$this->view->reviewStatus = $reviewStatus;
		$this->view->releaseStatus = $releaseStatus;
	}

	private function debet($pslip)
	{
		$PS_NUMBER = $this->_paymentRef;

		$this->_tableMst[5]["label"] = "Beneficiary Account";
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_bankname"], $pslip["acbenef_alias"]);
		$this->_tableMst[5]["label"] = "Source Bank Name";
		$this->_tableMst[5]["value"] = $this->_bankName;


		// Table Detail Header
		$fields = array(
			"ACBENEF_NAME"		=> $this->language->_('Source Account Name (Alias Name)'),
			"ACBENEF"			=> $this->language->_('Source Account'),
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_REFNO"  	   	=> $this->language->_('Additional Message'),
			"ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
		);
		
		
		$config = Zend_Registry::get('config');
		$paystatusarr = array_combine(array_values($config['payment']['status']['code']),array_values($config['payment']['status']['desc']));
			//var_dump();die;
  		$casePayStatus = "(CASE C.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";

		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF'					=> 'TT.BENEFICIARY_ACCOUNT',
					'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
					'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
					'ACBENEF_CCY'				=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_ALIAS_NAME is null THEN TT.BENEFICIARY_ACCOUNT_NAME
																				 ELSE CONCAT(TT.BENEFICIARY_ACCOUNT_NAME , ' (' , TT.BENEFICIARY_ALIAS_NAME , ')')
																			END"),
					//'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
					'TRA_AMOUNT'				=> 'C.PS_TOTAL_AMOUNT',
					'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
					'TRA_REFNO'					=> 'TT.TRA_REFNO',
					'PS_PERIODIC'				=> 'C.PS_PERIODIC', 
					'STATUS'					=> $casePayStatus,
				)
			)
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$ACCTSRC_arr = array();
		$tableDtl = array();

		foreach ($pslipTrx as $p => $pTrx) {
			// Create array acctsrc for validation
			if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]])) {
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
			} else {
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
			}

			// create table detail data
			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];

				if ($key == "TRA_AMOUNT") {
					$value = Application_Helper_General::displayMoney($value);
				} elseif ($key == "ACBENEF") {
					$value = $value . " [" . $pTrx[$key . "_CCY"] . "]";
				}

				$value = ($value == "") ? "-" : $value;
	
	
				
				$tableDtl[$p][$key] = $value;
				$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $this->_bankName;
				
			}
		}

		//var_dump($ACCTSRC_arr);
		//var_dump($ACBENEF_IDarr);die;
		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		if(empty($pslip["ccy"])){
			$pslip["ccy"] = 'IDR';
		}
		$paramApprove = array(
			"FROM" 				=> "D",						// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);

		$validate->checkApprove($paramApprove);
		
		$refIdSweepIn = $pslipTrx['REF_ID'];
		$PeriodIdSweepIn = $pslipTrx['0']['PS_PERIODIC'];
		if(!empty($PeriodIdSweepIn)){
		$selectday	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC_DAY'))
			// ->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$report_day = $this->_db->fetchAll($selectday);

		//print_r($pslipTrx);
		$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC'))
			->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
		// 		echo $select;die;
		$sweepScheme = $this->_db->fetchRow($select);
		//print_r($sweepScheme);die;
		if ($sweepScheme['SESSION_TYPE'] == "1") {
			$sessionSweep			= 'Morning (06.00 a.m - 08.00 a.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "2") {
			$sessionSweep			= 'Evening (12.00 p.m - 2.00 p.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "3") {
			$sessionSweep			= 'Night (05.00 p.m - 07.00 p.m )';
		}
		if ($sweepScheme['PS_EVERY_PERIODIC'] == '1') {
			$sweepScheme['DAYNAME'] = 'Monday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '2') {
			$sweepScheme['DAYNAME'] = 'Tuesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '3') {
			$sweepScheme['DAYNAME'] = 'Wednesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '4') {
			$sweepScheme['DAYNAME'] = 'Thursday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '5') {
			$sweepScheme['DAYNAME'] = 'Friday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '6') {
			$sweepScheme['DAYNAME'] = 'Saturday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '7') {
			$sweepScheme['DAYNAME'] = 'Sunday';
		}

				$frequen = '';
		if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '1'){

			$frequen = 'Daily';
			if (!empty($report_day)) {
				foreach ($report_day as $key => $value) {
					$this->view->{'check' . $value['DAY_ID']} = 'checked';
				}
			}

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '2'){
			$frequen = 'Weekly';

			$this->view->{'check' . $sweepScheme['PS_EVERY_PERIODIC']} = 'checked';

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '3'){
			$frequen = 'Monthly';
			$this->view->datemonth = $sweepScheme['PS_EVERY_PERIODIC'];
		}
		$this->view->futuretrx = false;
		$this->view->frequen = $frequen;
		}else{
			$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		}

		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment

		$this->view->fields			 = $fields;
		$this->view->tableDtl 		 = $tableDtl;
		$this->view->TITLE_MST		 = $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 = $this->language->_('Transfer From');
	}


	private function sweepin($pslip)
	{
		$PS_NUMBER = $this->_paymentRef;

			

		$fields = array(
			"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
			"ACBENEF"			=> $this->language->_('Beneficiary Account'),
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			//"TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
			"ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
			//"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			// "TRANSFER_FEE"  	=> 'Transfer Charge',
			"RATE"  	   	=> $this->language->_('Rate'),
			//"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			//"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			//"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			//"TOTAL"  	=> $this->language->_('Total'),
			//"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'),
			//"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

			"ACBENEF_EMAIL" 	=> $this->language->_('Email'),
			"ACBENEF_ISAPPROVE" => $this->language->_('Status'),
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);

		$fieldsdetail = array(
			 "TRA_REMAIN"  	   	=> $this->language->_('Remains on source'),
			 "PS_MIN_AMOUNT"  	=> $this->language->_('Minimum Transfer Amount')
		);

		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], empty($pslip["accsrc_alias"]) ? '' : $pslip["accsrc_alias"]);

		
		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		$ACBENEF_IDarr = array();
		$tableDtl = array();

		$app = Zend_Registry::get('config');
		$appBankname = $app['app']['bankname'];

		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'TT.BENEFICIARY_ALIAS_NAME',
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'C.PS_TOTAL_AMOUNT',
					'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
					'PS_MIN_AMOUNT'			=> 'C.PS_MIN_AMOUNT',
					// 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'TT.RATE',
					'TT.PROVISION_FEE',
					'TT.NOSTRO_NAME',
					'TT.FULL_AMOUNT_FEE',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'C.CUST_ID', 'TT.SOURCE_ACCOUNT_CCY',
					'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
					'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			 WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN C.PS_STATUS = '1' THEN 'Waiting Approval'
																				 WHEN C.PS_STATUS = '2' THEN 'Approved'
																				 ELSE '-'
																			END"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo $selectTrx;die;
		$pslipTrx = $this->_db->fetchAll($selectTrx);
		$this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		$this->view->addmessage = $pslipTrx['0']['TRA_ADDMESSAGE'];
		$this->view->TRA_REMAIN 	= $pslipTrx['0']['TRA_REMAIN'];
		$this->view->PS_MIN_AMOUNT  = $pslipTrx['0']['PS_MIN_AMOUNT'];
		// echo "<pre>";
		// 	print_r($pslipTrx);die;

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }

		// $fieldsdetail = array(
		// 	 "TRA_AMOUNT"  	   	=> $this->language->_('Actual Amount'),
		// 	 "TOTAL_CHARGES"  	=> $this->language->_('Deduction Amount'),
		// 	 "PS_TOTAL_AMOUNT"	=> $this->language->_('Total Amount')
		// );

		foreach ($pslipTrx as $p => $pTrx) {
			// Create array bene for validation
			if (!empty($pTrx["ACBENEF_ID"])) {
				$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
			}

			$trfType = $pTrx["TRANSFER_TYPE"];

			$psCategory = $pslip['PS_CATEGORY'];

			// create table detail data
			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];

				/* if ($key == "TRANSFER_FEE")
				{
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);	
				} */
				/*
				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}
				elseif ($key == "ACBENEF")
				{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
				*/
				if ($key == 'ACBENEF' && ($pTrx['TRANSFER_TYPE'] == '10' || $pTrx['TRANSFER_TYPE'] == '9')) {
					$value = '';
				}
				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN" || $key == "PROVISION_FEE") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['PS_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == "TRANSFER_FEE" && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {

					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;
					$trffee = $this->_db->fetchRow($selecttrffee);

					if ($value != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					}
				}
				if ($key == "FULL_AMOUNT_FEE" && !empty($pTrx['ACBENEF_CCY'])) {

					$selecttrfFA = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '4')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['ACBENEF_CCY']);
					// echo $selecttrfFA;die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);
					if ($pTrx['TRANSFER_TYPE'] == '10') {
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else if ($pTrx['TRANSFER_TYPE'] == '9') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID']);
						// echo $selecttrfFA;die;
						$trfFA = $this->_db->fetchRow($selecttrfFA);
					}
					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);
				}

				if ($key == 'PROVISION_FEE' && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {




					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['TRANSFER_TYPE_disp'] == 'PB') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							$value = $pTrx['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']) . ' (IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']) . ')';
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != '-') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}



				if ($key == "TRANSFER_TYPE_disp") {
					if ($value == 'PB') {
						$value = 'In House';
					}
				}

				if($key == 'ACBENEF_NAME'){
						if(empty($pTrx['BENEFICIARY_ALIAS_NAME'])){
							$alias = '-'; 
						}else{
							$alias = $pTrx['BENEFICIARY_ALIAS_NAME'];
						}
						$value = $value;

					}


				$value = ($value == "") ? "-" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
				$tableDtl[$p]['TRA_REMAIN']  		= 'IDR '.Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
				$tableDtl[$p]['PS_MIN_AMOUNT']  	= 'IDR '.Application_Helper_General::displayMoney($pTrx['PS_MIN_AMOUNT']);
			}

			if (!empty($pslip['BANK_CODE'])) {
				$bankcode = $pslip['BANK_CODE'];
			} else {
				$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
			}
			//var_dump($bankcode);

			if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
				$bankname = '-';
			} else if (empty($bankcode)) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$bankcode];
			}

			$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
			
			if (!empty($pTrx['SOURCE_ACCT_BANK_CODE'])) {
						$bankcode = $pTrx['SOURCE_ACCT_BANK_CODE'];
						$bankname = $bankNameArr[$bankcode];
					}
					$tableDtl[$p]['SOURCE_ACCT_BANK_CODE'] = $bankname;
		}


		$this->_tableMst[6]["label"] = $this->language->_('Source Bank Name');
		$this->_tableMst[6]["value"] = $tableDtl[0]['SOURCE_ACCT_BANK_CODE'];
		$this->_tableMst[7]["label"] = $this->language->_('Source Alias');
		$this->_tableMst[8]["label"] = $this->language->_('Beneficiary Alias');
		
		$sourcealias = '-';
		if($pslip['SOURCE_ACCOUNT_ALIAS_NAME'] != ''){
			$sourcealias = $pslip['SOURCE_ACCOUNT_ALIAS_NAME'];
		}
		$benefalias = '-';
		if($pslip['BENEFICIARY_ALIAS_NAME'] != ''){
			$benefalias = $pslip['BENEFICIARY_ALIAS_NAME'];
		}

		$this->_tableMst[7]["value"] = $sourcealias;
		$this->_tableMst[8]["value"] = $benefalias;
		
		$config = Zend_Registry::get('config');
		$paystatusarr = array_combine(array_values($config['payment']['status']['code']),array_values($config['payment']['status']['desc']));
			//var_dump();die;
  		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";
  			

		$select	= $this->_db->select()
			->from(
				array('TP' => 'T_PSLIP'),
				array(
					'REF_ID'					=> 'TP.REFF_ID',
					'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
					'EFTIME'					=> 'TP.PS_EFTIME',
					'REMAIN'					=> 'TP.PS_REMAIN',
					'STATUS'					=> $casePayStatus,
					'TRANS'						=> 'TP.PS_TXCOUNT'
				)
			)
			->where('TP.PS_NUMBER = ?', $PS_NUMBER);
			//echo $select;die;
		$pslipTrx = $this->_db->fetchRow($select);
		
		if($pslipTrx['TRANS'] > 1){
			//$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$this->view->downloadurl = $downloadURL;
			$this->view->trans = $pslipTrx['TRANS'];
		}

		if($pslipTrx['REMAIN'] == '0.00'){
			$this->view->remain = '-';
		}else{
			$this->view->remain = Application_Helper_General::displayMoney($pslipTrx['REMAIN']);
		}

		$this->view->eftime = $pslipTrx['EFTIME'];
		$this->view->paystatus = $pslipTrx['STATUS'].' - Untransfered';
		$refIdSweepIn = $pslipTrx['REF_ID'];
		$PeriodIdSweepIn = $pslipTrx['PS_PERIODIC'];
		if(!empty($PeriodIdSweepIn)){
		$selectday	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC_DAY'))
			// ->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$report_day = $this->_db->fetchAll($selectday);

		//print_r($pslipTrx);
		$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC'))
			->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
		// 		echo $select;die;
		$sweepScheme = $this->_db->fetchRow($select);
		//print_r($sweepScheme);die;
		if ($sweepScheme['SESSION_TYPE'] == "1") {
			$sessionSweep			= 'Morning (06.00 a.m - 08.00 a.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "2") {
			$sessionSweep			= 'Evening (12.00 p.m - 2.00 p.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "3") {
			$sessionSweep			= 'Night (05.00 p.m - 07.00 p.m )';
		}
		if ($sweepScheme['PS_EVERY_PERIODIC'] == '1') {
			$sweepScheme['DAYNAME'] = 'Monday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '2') {
			$sweepScheme['DAYNAME'] = 'Tuesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '3') {
			$sweepScheme['DAYNAME'] = 'Wednesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '4') {
			$sweepScheme['DAYNAME'] = 'Thursday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '5') {
			$sweepScheme['DAYNAME'] = 'Friday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '6') {
			$sweepScheme['DAYNAME'] = 'Saturday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '7') {
			$sweepScheme['DAYNAME'] = 'Sunday';
		}

				$frequen = '';
		if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '1'){
			$frequen = 'Daily';

			if (!empty($report_day)) {
				foreach ($report_day as $key => $value) {
					$this->view->{'check' . $value['DAY_ID']} = 'checked';
				}
			}

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '2'){
			$frequen = 'Weekly';

			$this->view->{'check' . $sweepScheme['PS_EVERY_PERIODIC']} = 'checked';

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '3'){
			$frequen = 'Monthly';
			$this->view->datemonth = $sweepScheme['PS_EVERY_PERIODIC'];
		}

		$this->view->futuretrx = false;
		$this->view->frequen = $frequen;
		}else{
			$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		}

		// 		$sweepScheme['PS_EVERY_PERIODIC'];
		//$this->_tableMst[5]["label"] = $this->language->_('Beneficiary Account');
		//$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_bankname"], $pslip["acbenef_alias"]);

		//$this->_tableMst[6]["label"] = $this->language->_('Recurring');
		//$this->_tableMst[6]["value"] = $sweepScheme['DAYNAME'];

		//$this->_tableMst[7]["label"] = $this->language->_('Session');
		//$this->_tableMst[7]["value"] = $sessionSweep;

		//$this->_tableMst[8]["label"] = $this->language->_('End Date Scheme');
		//$this->_tableMst[8]["value"] = Application_Helper_General::convertDate($sweepScheme['PS_PERIODIC_ENDDATE'], $this->_dateViewFormat);

		if ($pslip['BALANCE_TYPE'] == '2') {
			$persen	= "(%)";
		} else {
			$persen	= "";
		}

	$this->view->sweepdata = $sweepScheme;
		// Table Detail Header
		// $fields = array(
		// 	"ACCTSRC_NAME"		=> $this->language->_('Source Account Name (Alias Name)'),
		// 	"ACCTSRC"			=> $this->language->_('Source Account'),
		// 	"TRA_MESSAGE" 		=> $this->language->_('Message'),
		// 	"TRA_ADDITIONAL_MESSAGE"  	   	=> $this->language->_('Additional Message'),
		// 	"ACCTSRC_CCY"  	   	=> $this->language->_('CCY'),
		// 	"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
		// 	"BALANCE_TYPE" 		=> $this->language->_('Balance Type'),
		// );

		// $select	= $this->_db->select()
		// 	->from(
		// 		array('TT' => 'T_TRANSACTION'),
		// 		array(
		// 			'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
		// 			'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
		// 			'ACCTSRC_NAME'				=> new Zend_Db_Expr("
		// 																	CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
		// 																		 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME , ' (' , TT.SOURCE_ACCOUNT_ALIAS_NAME , ')')
		// 																	END"),
		// 			//'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
		// 			'TRA_AMOUNT'				=> 'TT.TRA_AMOUNT',
		// 			'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
		// 			'TRA_ADDITIONAL_MESSAGE'					=> 'TT.TRA_ADDITIONAL_MESSAGE',
		// 			'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE 
  //       																		FROM T_PERIODIC_DETAIL Y 
  //       																		inner join T_PSLIP Z 
  //       																		on Y.PS_PERIODIC = Z.PS_PERIODIC 
  //       																		where 
  //       																		Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
		// 		)
		// 	)
		// 	->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// $pslipTrx = $this->_db->fetchAll($select);

		// $this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		// $this->view->addmessage = $pslipTrx['0']['TRA_ADDITIONAL_MESSAGE'];
		// // 		print_r($pslipTrx);die;
		// $ACBENEF_IDarr = array();
		// $ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		// $ACCTSRC_arr = array();
		// $tableDtl = array();
		// //print_r($pslip);
		// //die('here');
		// if ($pslip['PS_TYPE'] != '14') {

		// 	foreach ($pslipTrx as $p => $pTrx) {

		// 		// Create array acctsrc for validation
		// 		if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]])) {
		// 			$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
		// 		} else {
		// 			$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
		// 			$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
		// 		}

		// 		// create table detail data
		// 		foreach ($fields as $key => $field) {
		// 			$value = $pTrx[$key];

		// 			if ($key == "TRA_AMOUNT") {
		// 				$value = Application_Helper_General::displayMoney($value);
		// 			} elseif ($key == "ACCTSRC") {
		// 				$value = $value . " [" . $pTrx[$key . "_CCY"] . "]";
		// 			}

		// 			$value = ($value == "") ? "&nbsp;" : $value;

		// 			$tableDtl[$p][$key] = $value;
		// 		}
		// 	}
		// } else {

		// 	$select	= $this->_db->select()
		// 		// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
		// 		->from(array('TTS' => 'T_PERIODIC'))
		// 		->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
		// 		->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
		// 	// 		echo $select;die;
		// 	$sweepdetail = $this->_db->fetchAll($select);
		// 	// 			print_r($sweepdetail);die;
		// 	foreach ($pslipTrx as $p => $pTrx) {
		// 		// Create array acctsrc for validation
		// 		if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]])) {
		// 			$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
		// 		} else {
		// 			$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
		// 			$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
		// 		}

		// 		// create table detail data
		// 		foreach ($fields as $key => $field) {
		// 			$value = $pTrx[$key];

		// 			if ($key == "TRA_AMOUNT") {
		// 				$value = Application_Helper_General::displayMoney($sweepdetail[$p]['TRA_REMAIN']);
		// 			} elseif ($key == "ACCTSRC") {
		// 				$value = $value . " [" . $pTrx[$key . "_CCY"] . "]";
		// 			}

		// 			$value = ($value == "") ? "&nbsp;" : $value;

		// 			$tableDtl[$p][$key] = $value;
		// 		}
		// 	}
		// }
		//         print_r($tableDtl);die;
		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		if(empty($sweepScheme["TRA_REMAIN"])){
			$sweepScheme["TRA_REMAIN"] =  $pslip["amount"];
		}
		$paramApprove = array(
			"FROM" 				=> "D",						// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"BALANCETYPE"			=> $pslip['BALANCE_TYPE'],
			"PS_TYPE"				=> $pslip["PS_TYPE"],
			"PS_REMAIN"				=> $sweepScheme["TRA_REMAIN"],
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);

		$validate->checkApprove($paramApprove);
		//	print_r($validate->getErrorMsg());die;
		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment
		// 	   print_r($tableDtl);die;
		$this->view->fieldsdetail	= $fieldsdetail;
		$this->view->fields			 = $fields;
		$this->view->tableDtl 		 = $tableDtl;
		$this->view->TITLE_MST		 = $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 = $this->language->_('Transfer From');
		$this->view->labelpersen 	 = $persen;
	}

	function moneyAliasFormatter($n)
	{
		// first strip any formatting;
		return str_replace('.00', '', Application_Helper_General::displayMoney($n));
		// $n = (0+str_replace(",", "", $n));

		// // is this a number?
		// if (!is_numeric($n)) return false;

		// // now filter it;
		// if ($n > 1000000000000) return round(($n/1000000000000), 2).' T';
		// elseif ($n > 1000000000) return round(($n/1000000000), 2).' B';
		// elseif ($n > 1000000) return round(($n/1000000), 2).' M';
		// elseif ($n > 1000) return $n;

		// return number_format($n);
	}

	private function sweepout($pslip)
	{
		require_once 'General/Charges.php';
		$PS_NUMBER = $this->_paymentRef;
		$conf = Zend_Registry::get('config');
		$paymentStatus   	= $conf['payment']['status'];
		//var_dump($conf['payment']['status']);die;
		$paystatusarr = array_combine(array_values($paymentStatus['code']),array_values($paymentStatus['desc']));
			
  		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";
  			
  		



		$select	= $this->_db->select()
			->from(
				array('TP' => 'T_PSLIP'),
				array(
					'REF_ID'					=> 'TP.REFF_ID',
					'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
					'EFTIME'					=> 'TP.PS_EFTIME',
					'REMAIN'					=> 'TP.PS_REMAIN',
					'STATUS'					=> $casePayStatus,
				)
			)
			->where('TP.PS_NUMBER = ?', $PS_NUMBER);
		$pslipTrx = $this->_db->fetchRow($select);

		if($pslipTrx['REMAIN'] == '0.00'){
			$this->view->remain = '-';
		}else{
			$this->view->remain = Application_Helper_General::displayMoney($pslipTrx['REMAIN']);
		}

		$this->view->eftime = $pslipTrx['EFTIME'];
		$this->view->paystatus = $pslipTrx['STATUS'].' - Untransfered';
		$PeriodIdSweepIn = $pslipTrx['PS_PERIODIC'];
		$refIdSweepIn = $pslipTrx['REF_ID'];
		if(!empty($PeriodIdSweepIn)){
		$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC'))
			->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$sweepScheme = $this->_db->fetchRow($select);
		//print_r($sweepScheme);die;

		$arrday = array(
				'0' => 'sun',
				'1' => 'mon',
				'2' => 'tue',
				'3' => 'wed',
				'4' => 'thu',
				'5' => 'fry',
				'6' => 'sat'

			);

		$selectday	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC_DAY'))
			// ->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$report_day = $this->_db->fetchAll($selectday);
		
		//echo '<pre>';
		//var_dump($report_day);die;
		if (!empty($report_day)) {
						foreach ($report_day as $key => $value) {
							// $this->view->{'check' . $value['DAY_ID']} = 'checked';
							${'pooling_' . $arrday[$value['DAY_ID']]} = $value['LIMIT_AMOUNT'];
						}
		}
		//var_dump($sweepScheme);die;

		$this->view->pooling_sun = $pooling_sun;
		$this->view->pooling_mon = $pooling_mon;
		$this->view->pooling_tue = $pooling_tue;
		$this->view->pooling_wed = $pooling_wed;
		$this->view->pooling_thu = $pooling_thu;
		$this->view->pooling_fry = $pooling_fry;
		$this->view->pooling_sat = $pooling_sat;

		$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
		$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
		$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
		$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
		$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
		$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
		$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);

		$frequen = '';
		if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '1'){
			$frequen = 'Daily';

			if (!empty($report_day)) {
				foreach ($report_day as $key => $value) {
					$this->view->{'check' . $value['DAY_ID']} = 'checked';
				}
			}

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '2'){
			$frequen = 'Weekly';

			$this->view->{'check' . $sweepScheme['PS_EVERY_PERIODIC']} = 'checked';

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '3'){
			$frequen = 'Monthly';
			$this->view->datemonth = $sweepScheme['PS_EVERY_PERIODIC'];
		}

		$this->view->futuretrx = false;
		$this->view->frequen = $frequen;
		}else{
			$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		}

		if ($sweepScheme['SESSION_TYPE'] == "1") {
			$sessionSweep			= 'Morning (06.00 a.m - 08.00 a.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "2") {
			$sessionSweep			= 'Evening (12.00 p.m - 2.00 p.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "3") {
			$sessionSweep			= 'Night (05.00 p.m - 07.00 p.m )';
		}
		if ($sweepScheme['PS_EVERY_PERIODIC'] == '1') {
			$sweepScheme['DAYNAME'] = 'Monday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '2') {
			$sweepScheme['DAYNAME'] = 'Tuesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '3') {
			$sweepScheme['DAYNAME'] = 'Wednesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '4') {
			$sweepScheme['DAYNAME'] = 'Thursday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '5') {
			$sweepScheme['DAYNAME'] = 'Friday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '6') {
			$sweepScheme['DAYNAME'] = 'Saturday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '7') {
			$sweepScheme['DAYNAME'] = 'Sunday';
		}

		$this->view->sweepdata = $sweepScheme;

		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		$this->_tableMst[6]["label"] = $this->language->_('Recurring');
		$this->_tableMst[6]["value"] = $sweepScheme['DAYNAME'];

		$this->_tableMst[7]["label"] = $this->language->_('Session');
		$this->_tableMst[7]["value"] = $sessionSweep;

		$this->_tableMst[8]["label"] = $this->language->_('End Date Scheme');
		$this->_tableMst[8]["value"] = Application_Helper_General::convertDate($sweepScheme['PS_PERIODIC_ENDDATE'], $this->_dateViewFormat);

		if ($pslip['BALANCE_TYPE'] == '2') {
			$persen	= "(%)";
		} else {
			$persen	= "";
		}
		
		$fieldsdetail = array(
			 "TRA_REMAIN"  	   	=> $this->language->_('Maintains on beneficiary'),
			 "PS_MIN_AMOUNT"  	=> $this->language->_('Minimum Transfer Amount')
		);
		// Table Detail Header
		$fields = array(
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name'),
			"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
			"ACBENEF"			=> $this->language->_('Beneficiary Account'),
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_ADDITIONAL_MESSAGE"  	   	=> $this->language->_('Additional Message'),
			//"TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
			"ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
			//"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			// 				"TRANSFER_FEE"  	=> 'Transfer Charge',
			//"RATE"  	   	=> $this->language->_('Rate'),
			//"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			"SOURCE_ACCT_BANK_CODE"  	   	=> $this->language->_('Source Bank Name'),
			//"BENEF_ACCT_BANK_CODE"  	   	=> $this->language->_('Transfer Fee'),
			//"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			//"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			//"TRA_AMOUNT_TOTAL"  	=> $this->language->_('Total'),
			//"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'), 
			//"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

			"ACBENEF_EMAIL" 	=> $this->language->_('Email'),
			//"ACBENEF_ISAPPROVE" => $this->language->_('Status')
		);

		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> 'TT.BENEFICIARY_ACCOUNT_NAME',
					'TT.BENEFICIARY_ALIAS_NAME',
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDITIONAL_MESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'TRA_ADDITIONAL_MESSAGE' => 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
					'PS_MIN_AMOUNT'			=> 'C.PS_MIN_AMOUNT',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL', 'TRA_AMOUNT_TOTAL'			=> 'TT.TRA_AMOUNT',
					'TRA_AMOUNT'			=> 'C.PS_TOTAL_AMOUNT',
					'TT.FULL_AMOUNT_FEE', 'TT.PROVISION_FEE', 'TT.RATE',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'SOURCE_ACCT_BANK_CODE'			=> 'TT.SOURCE_ACCT_BANK_CODE',
					'BENEF_ACCT_BANK_CODE'			=> 'TT.BENEF_ACCT_BANK_CODE',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $this->_bankName . "'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN B.BENEFICIARY_ISAPPROVE = '0' THEN 'Waiting Approval'
																				 WHEN B.BENEFICIARY_ISAPPROVE = '1' THEN 'Approved'
																				 ELSE '-'
																			END"),
					'BALANCE_TYPE'				=> 'C.BALANCE_TYPE',
					// 'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
        			// 															FROM T_PERIODIC_DETAIL Y
        			// 															inner join T_PSLIP Z
        			// 															on Y.PS_PERIODIC = Z.PS_PERIODIC
        			// 															where
        			// 															Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array('PS_TOTAL_AMOUNT'))
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);
		//var_dump($pslipTrx['0']['TRA_MESSAGE']);die;
		$this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		$this->view->addmessage = $pslipTrx['0']['TRA_ADDITIONAL_MESSAGE'];
		$this->view->TRA_REMAIN 	= $pslipTrx['0']['TRA_REMAIN'];
		$this->view->PS_MIN_AMOUNT  = $pslipTrx['0']['PS_MIN_AMOUNT'];
		
		// 		print_r($pslipTrx);die;
		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];
		
		
		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }
		$ACBENEF_IDarr = array();
		$tableDtl = array();

		if ($pslip['PS_TYPE'] != '15') {
			foreach ($pslipTrx as $p => $pTrx) {
				// Create array bene for validation
				if (!empty($pTrx["ACBENEF_ID"])) {
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}

				$trfType = $pTrx["TRANSFER_TYPE"];

				// create table detail data
				foreach ($fields as $key => $field) {
					$value = $pTrx[$key];

					/* if ($key == "TRANSFER_FEE")
					 {
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);
					} */

					if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE") {
						$value = Application_Helper_General::displayMoney($value);
					} elseif ($key == "ACBENEF") {
						$value = $value . " [" . $pTrx[$key . "_CCY"] . "]";
					} elseif ($key == "TRANSFER_TYPE_disp") {
						if ($value == 'PB') {
							$value = 'In House';
						}
					}else if($key == "BENEF_ACCT_BANK_CODE"){
						$value = $bankNameArr[$value];
					}else if($key == "SOURCE_ACCT_BANK_CODE"){
						$value = $bankNameArr[$value];
					}else if($key == 'ACBENEF_NAME'){
						if(empty($pTrx['BENEFICIARY_ALIAS_NAME'])){
							$alias = '-'; 
						}else{
							$alias = $pTrx['BENEFICIARY_ALIAS_NAME'];
						}
						$value = $value;

					}

					$value = ($value == "") ? "" : $value;

					$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
					$tableDtl[$p]['TRA_REMAIN']  		= 'IDR '.Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					$tableDtl[$p]['PS_MIN_AMOUNT']  	= 'IDR '.Application_Helper_General::displayMoney($pTrx['PS_MIN_AMOUNT']);
				}
			}
		} else {
			if(!empty($PeriodIdSweepIn)){
			$select	= $this->_db->select()
				// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
				->from(array('TTS' => 'T_PERIODIC'))
				->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
				->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
			// 		echo $select;die;
			$sweepdetail = $this->_db->fetchAll($select);
			}
			foreach ($pslipTrx as $p => $pTrx) {
				// Create array bene for validation
				if (!empty($pTrx["ACBENEF_ID"])) {
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}

				$trfType = $pTrx["TRANSFER_TYPE"];

				// create table detail data
				foreach ($fields as $key => $field) {
					$value = $pTrx[$key];

					/* if ($key == "TRANSFER_FEE")
					 {
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);
					} */

					if ($key == "TRA_AMOUNT" || $key == "TRA_AMOUNT_TOTAL") {
						$value = Application_Helper_General::displayMoney($sweepdetail[$p]['TRA_REMAIN']);
					} elseif ($key == "RATE") {
						if ($sweepdetail[$p]['RATE'] == '') {
							$sweepdetail[$p]['RATE'] = '0.00';
						}
						$value = 'IDR ' . Application_Helper_General::displayMoney($sweepdetail[$p]['RATE']);
					} elseif ($key == "TRANSFER_FEE") {
						if ($sweepdetail[$p]['TRANSFER_FEE'] == '') {
							$sweepdetail[$p]['TRANSFER_FEE'] = '0.00';
						}
						$value = 'IDR ' . Application_Helper_General::displayMoney($sweepdetail[$p]['TRANSFER_FEE']);
					} elseif ($key == "PROVISION_FEE") {
						if ($sweepdetail[$p]['PROVISION_FEE'] == '') {
							$sweepdetail[$p]['PROVISION_FEE'] = '0.00';
						}
						$value = 'IDR ' . Application_Helper_General::displayMoney($sweepdetail[$p]['PROVISION_FEE']);
					} elseif ($key == "FULL_AMOUNT_FEE") {
						if ($sweepdetail[$p]['FULL_AMOUNT_FEE'] == '') {
							$sweepdetail[$p]['FULL_AMOUNT_FEE'] = '0.00';
						}
						$value = 'IDR ' . Application_Helper_General::displayMoney($sweepdetail[$p]['FULL_AMOUNT_FEE']);
					} elseif ($key == "ACBENEF") {
						$value = $value . " [" . $pTrx[$key . "_CCY"] . "]";
					} elseif ($key == "TRANSFER_TYPE_disp") {
						if ($value == 'PB') {
							$value = 'In House';
						}
					}

					$value = ($value == "") ? "-" : $value;
					$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $this->_bankName;
					
					$tableDtl[$p]['SOURCE_ACCT_BANK_CODE'] = $this->_bankName;

					$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
				}
			}
		}

		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		if(empty($sweepScheme["TRA_REMAIN"])){
			$sweepScheme["TRA_REMAIN"] = $pslipTrx['0']['TRA_REMAIN'];
		}
		$paramApprove = array(
			"FROM" 				=> "D",	// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"BALANCETYPE"			=> $pslip['BALANCE_TYPE'],
			"PS_TYPE"				=> $pslip["PS_TYPE"],
			"PS_REMAIN"				=> $sweepScheme["TRA_REMAIN"],
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);
		//print_r($paramApprove);die;
		$validate->checkApprove($paramApprove);

		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment
		$this->view->fieldsdetail	= $fieldsdetail;
		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
		$this->view->TITLE_MST		 	= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer To');
		$this->view->labelpersen 	 	= $persen;
	}

	function string_between_two_string($str, $starting_word, $ending_word)
	{
		$subtring_start = strpos($str, $starting_word);
		//Adding the strating index of the strating word to 
		//its length would give its ending index
		$subtring_start += strlen($starting_word);  
		//Length of our d sub string
		$size = strpos($str, $ending_word, $subtring_start) - $subtring_start;  
		// Return the substring from the index substring_start of length size 
		return substr($str, $subtring_start, $size);  
	}

	private function credit($pslip)
	{
		require_once 'General/Charges.php';
		$PS_NUMBER = $this->_paymentRef;
		
		if(empty($pslip["accsrc_bankname"])){
			$selectcustact = $this->_db->select()->from(array('T' => 'M_CUSTOMER_ACCT'), array('*'))
							->where("T.ACCT_NO =?", $pslip["accsrc"]); 
							//->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							//->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
						//echo $selecttrffee;
						$custact = $this->_db->fetchRow($selectcustact);
			 $pslip["accsrc_bankname"] = $custact['ACCT_NAME'];			
			}

		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], empty($pslip["accsrc_alias"]) ? '' : $pslip["accsrc_alias"]);
		
		// Table Detail Header
		$fields = array(
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name'),
			"BENEFICIARY_BANK_CITY"		=> $this->language->_('City'),
			"ACBENEF"			=> $this->language->_('Beneficiary Account'),
			"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
			"BENEFICIARY_ALIAS_NAME"		=> $this->language->_('Beneficiary Alias Name'),
			"BENEFICIARY_ADDRESS"		=> $this->language->_('Beneficiary Address 1'),
			"BENEFICIARY_ADDRESS2"		=> $this->language->_('Beneficiary Address 2'),
			"ACBENEF_EMAIL" 	=> $this->language->_('Beneficiary Email'),

			"BENEFICIARY_CITIZENSHIP" 	=> $this->language->_('Citizenship'),
			"BENEFICIARY_RESIDENT" 	=> $this->language->_('Nationalilty'),
			"BENEFICIARY_CATEGORY" 	=> $this->language->_('Beneficiary Category'),
			"BENEFICIARY_ID_TYPE" 	=> $this->language->_('Beneficiary ID Type'),
			"BENEFICIARY_ID_NUMBER" 	=> $this->language->_('Beneficiary ID Number'),
			// "TRA_MESSAGE" 		=> $this->language->_('Message'),
			// "TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			// "TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
			// "ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
			 // "TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			// "TRANSFER_FEE"  	=> 'Transfer Charge',
			// "RATE"  	   	=> $this->language->_('Rate'),
			// "TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			// "FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			// "PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			// "TOTAL"  	=> $this->language->_('Total'),
			
			// "NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

			
			// "ACBENEF_ISAPPROVE" => $this->language->_('Status'),
			// "BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);
		
		
		if($pslip['PS_TYPE']=='3'){
				$fieldsdetail = array(
			
			// "TRA_MESSAGE" 		=> $this->language->_('Message'),
			// "TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			// "TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
			// "ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
			 "TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			 "RATE"  	   	=> $this->language->_('Rate'),
			 "TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			 
			 "FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			 "PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			  "TOTAL"  	=> $this->language->_('Total'),
			
			// "NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

			
			// "ACBENEF_ISAPPROVE" => $this->language->_('Status'),
			// "BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);
		}else if($pslip['PS_TYPE'] == '1' && $pslip['PS_CCY'] == 'USD'){
				$fieldsdetail = array(
			
				// "TRA_MESSAGE" 		=> $this->language->_('Message'),
				// "TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
				// "TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
				// "ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
				 "TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
				 "RATE"  	   	=> $this->language->_('Rate'),
				 "TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
				  "TOTAL"  	=> $this->language->_('Total'),
				
				// "NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

				
				// "ACBENEF_ISAPPROVE" => $this->language->_('Status'),
				// "BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
			);
		}else{
		$fieldsdetail = array(
			
			// "TRA_MESSAGE" 		=> $this->language->_('Message'),
			// "TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			// "TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
			// "ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
			 "TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			 "TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			 //"RATE"  	   	=> $this->language->_('Rate'),
			// "FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			// "PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			  "TOTAL"  	=> $this->language->_('Total'),
			
			// "NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

			
			// "ACBENEF_ISAPPROVE" => $this->language->_('Status'),
			// "BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);
		}




		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		$ACBENEF_IDarr = array();
		$tableDtl = array();

		$app = Zend_Registry::get('config');
		$appBankname = $app['app']['bankname'];


		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'BENEFICIARY_BANK_CITY',
					'BENEFICIARY_ALIAS_NAME',
					'BENEFICIARY_ADDRESS',
					'BENEFICIARY_ADDRESS2',
					'BENEFICIARY_ID_TYPE',
					'BENEFICIARY_ID_NUMBER',
					'BENEFICIARY_CITIZENSHIP' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CITIZENSHIP = 'W' THEN 'WNI'
																				 WHEN TT.BENEFICIARY_CITIZENSHIP = 'R' THEN 'WNA'
																				 WHEN TT.BENEFICIARY_CITIZENSHIP = 'N' THEN 'WNA'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_RESIDENT' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_RESIDENT = 'R' THEN 'Residence'
																				 WHEN TT.BENEFICIARY_RESIDENT = 'W' THEN 'Non Residence'
																				 WHEN TT.BENEFICIARY_RESIDENT = 'NR' THEN 'Non Residence'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_CATEGORY' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CATEGORY = '1' THEN 'Individual'
																				 WHEN TT.BENEFICIARY_CATEGORY = '2' THEN 'Government'
																				 WHEN TT.BENEFICIARY_CATEGORY = '3' THEN 'Bank'
																				 WHEN TT.BENEFICIARY_CATEGORY = '4' THEN 'Non Bank Financial Institution'
																				 WHEN TT.BENEFICIARY_CATEGORY = '5' THEN 'Company'
																				 WHEN TT.BENEFICIARY_CATEGORY = '6' THEN 'Other'
																				 ELSE '-'
																			END"),

					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					// 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'TT.RATE',
					'TT.PROVISION_FEE',
					'TT.NOSTRO_NAME',
					'TT.SP2D_NO',
					'C.PS_FILE',
					'TT.FULL_AMOUNT_FEE',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'C.CUST_ID', 'TT.SOURCE_ACCOUNT_CCY',
					'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
					'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			 WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN C.PS_STATUS = '1' THEN 'Waiting Approval'
																				 WHEN C.PS_STATUS = '2' THEN 'Approved'
																				 ELSE '-'
																			END"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo $selectTrx;die;
		$pslipTrx = $this->_db->fetchAll($selectTrx);
		//echo '<pre>';
		//var_dump($pslipTrx);die;
		if($pslip['PS_TYPE'] == '29'){
		
				$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadfile', 'payReff' => $PS_NUMBER), null, true);
				$this->view->message = 'SP2D no: '.$pslipTrx['0']['SP2D_NO'].'<a href="'.$downloadURL.'" ><input type="button" class="btnwhite" value="Download" > </a>';
				//$this->_tableMstleft[5]["value"] = 'SP2D no: '.$pslipTrx['0']['SP2D_NO'].'<a href="'.$downloadURL.'" ><input type="button" class="btnwhite" value="Download" > </a>';
		}else{
			$this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		}
		$this->view->addmessage = $pslipTrx['0']['TRA_ADDMESSAGE'];
		// echo "<pre>";
		// 	print_r($pslipTrx);die;

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }


		foreach ($pslipTrx as $p => $pTrx) {
			// Create array bene for validation
			if (!empty($pTrx["ACBENEF_ID"])) {
				$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
			}

			$trfType = $pTrx["TRANSFER_TYPE"];

			$psCategory = $pslip['PS_CATEGORY'];

			// create table detail data
			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];

				/* if ($key == "TRANSFER_FEE")
				{
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);	
				} */
				/*
				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}
				elseif ($key == "ACBENEF")
				{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
				*/
				if ($key == 'ACBENEF' && ($pTrx['TRANSFER_TYPE'] == '10' || $pTrx['TRANSFER_TYPE'] == '9')) {
					$value = '';
				}else if($key == 'ACBENEF'){
					$value = $value.' ( '.$pTrx['ACBENEF_CCY'].' )';
				}
				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN" || $key == "PROVISION_FEE") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['PS_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == "TRANSFER_FEE" && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {

					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;
					$trffee = $this->_db->fetchRow($selecttrffee);

					if ($value != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					}
				}
				if ($key == "FULL_AMOUNT_FEE" && !empty($pTrx['ACBENEF_CCY'])) {

					$selecttrfFA = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '4')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['ACBENEF_CCY']);
					// echo $selecttrfFA;die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);
					if ($pTrx['TRANSFER_TYPE'] == '10') {
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else if ($pTrx['TRANSFER_TYPE'] == '9') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID']);
						// echo $selecttrfFA;die;
						$trfFA = $this->_db->fetchRow($selecttrfFA);
					}
					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);
				}

				if ($key == 'PROVISION_FEE' && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {




					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['TRANSFER_TYPE_disp'] == 'PB') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							$value = $pTrx['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']) . ' (IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']) . ')';
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != '-') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}



				if ($key == "TRANSFER_TYPE_disp") {
					if ($value == 'PB') {
						$value = 'In House';
					}
				}


				$value = ($value == "") ? "-" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if (!empty($pslip['BANK_CODE'])) {
				$bankcode = $pslip['BANK_CODE'];
			} else if(!empty($pslip['BENEF_ACCT_BANK_CODE'])){
				$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
			}else{
				$bankcode = $pslip['CLR_CODE'];
			}

			if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
				$bankname = '-';
			}else if(!empty($pslip['BENEFICIARY_BANK_NAME'])){
				$bankname = $pslip['BENEFICIARY_BANK_NAME']; 
			} else if (empty($bankcode)) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$bankcode];
			}
			
			if($pslip['PS_TYPE']=='25'){
				$bankname = '-';
			}

			if($pTrx['TRANSFER_FEE'] == ''){
					$pTrx['TRANSFER_FEE'] = 0;
				}

			$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
			$tableDtl[$p]['TRA_AMOUNT']  		= Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
				$tableDtl[$p]['TRANSFER_FEE'] 		=  'IDR '.Application_Helper_General::displayMoney($pTrx['TRANSFER_FEE']);
				$tableDtl[$p]['TRANSFER_TYPE'] 		=  $pTrx['TRANSFER_TYPE'];
				
				if($pTrx['TRANSFER_TYPE'] == '3' ||  $pTrx['TRANSFER_TYPE'] == '4'){
					$tableDtl[$p]['PROVISION_FEE'] 		=  $pTrx['ACBENEF_CCY'].' '.Application_Helper_General::displayMoney($pTrx['PROVISION_FEE']);
					$tableDtl[$p]['FULL_AMOUNT_FEE'] 		=  $pTrx['ACBENEF_CCY'].' '.Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);
					$tableDtl[$p]['RATE'] 		=  'IDR '.Application_Helper_General::displayMoney($pTrx['RATE']);
					//die('gere');
					$ratetransfer = $pTrx['TRANSFER_FEE']/$pTrx['RATE'];
					$total  = ((float)$pTrx['TRA_AMOUNT']+(float)$ratetransfer)+(float)$pTrx['PROVISION_FEE']+(float)$pTrx['FULL_AMOUNT_FEE'];
					//
					$totalAmount = $total;
					$totaleq = (float)$total*(float)$pTrx['RATE'];
					//var_dump($total);
					$tableDtl[$p]['EQ_AMOUNT'] 		=  Application_Helper_General::displayMoney($totaleq);
					$tableDtl[$p]['TOTAL'] 	= Application_Helper_General::displayMoney($totalAmount).' ( IDR '.Application_Helper_General::displayMoney($totaleq ).' )';			
				}else if($pTrx['TRANSFER_TYPE'] == '8' ||  $pTrx['TRANSFER_TYPE'] == '7'){
					$tableDtl[$p]['RATE'] 		=  'IDR '.Application_Helper_General::displayMoney($pTrx['RATE']);
					$ratetransfer = $pTrx['TRANSFER_FEE']/$pTrx['RATE'];
					$totalAmount = ((float)$pTrx['TRA_AMOUNT']+(float)$ratetransfer);
					$totaleq = (float)$totalAmount*(float)$pTrx['RATE'];
					$tableDtl[$p]['TOTAL'] 	= Application_Helper_General::displayMoney($totalAmount).' ( IDR '.Application_Helper_General::displayMoney($totaleq ).' )';	
				}else{
					$tableDtl[$p]['EQ_AMOUNT'] 		=  Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
					$totalAmount 						= $pTrx['TRA_AMOUNT'] 	+ $pTrx['TRANSFER_FEE'];
					$tableDtl[$p]['TOTAL'] 	= Application_Helper_General::displayMoney($totalAmount);			
				}		
		}

		// print_r($tableDtl);die();

		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array(
			"FROM" 				=> "D",	// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
			"PS_TYPE"			=> $pslip['PS_TYPE']
		);
		// var_dump($paramApprove);die;
		$validate->checkApprove($paramApprove);

		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment
		
		$config = Zend_Registry::get('config');
		$paystatusarr = array_combine(array_values($config['payment']['status']['code']),array_values($config['payment']['status']['desc']));
			//var_dump();die;
  		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";
  			



		$select	= $this->_db->select()
			->from(
				array('TP' => 'T_PSLIP'),
				array(
					'REF_ID'					=> 'TP.REFF_ID',
					'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
					'EFTIME'					=> 'TP.PS_EFTIME',
					'REMAIN'					=> 'TP.PS_REMAIN',
					'TP.PS_MIN_AMOUNT',
					'STATUS'					=> $casePayStatus,
					'TRANS'						=> 'TP.PS_TXCOUNT'
				)
			)
			->where('TP.PS_NUMBER = ?', $PS_NUMBER);
			//echo $select;die;
		$pslipTrx = $this->_db->fetchRow($select);
		
		if($pslipTrx['TRANS'] > 1){
			//$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$this->view->downloadurl = $downloadURL;
			$this->view->trans = $pslipTrx['TRANS'];
		}
		$this->view->paystatus = $pslipTrx['STATUS'].' - Untransfered';
		// echo "<pre>";
		
		
		$refIdSweepIn = $pslipTrx['REF_ID'];
		$PeriodIdSweepIn = $pslipTrx['PS_PERIODIC'];
		if(!empty($PeriodIdSweepIn)){
		$selectday	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC_DAY'))
			// ->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$report_day = $this->_db->fetchAll($selectday);

		//print_r($pslipTrx);
		$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC'))
			->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
		// 		echo $select;die;
		$sweepScheme = $this->_db->fetchRow($select);
		//print_r($sweepScheme);die;
		if ($sweepScheme['SESSION_TYPE'] == "1") {
			$sessionSweep			= 'Morning (06.00 a.m - 08.00 a.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "2") {
			$sessionSweep			= 'Evening (12.00 p.m - 2.00 p.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "3") {
			$sessionSweep			= 'Night (05.00 p.m - 07.00 p.m )';
		}
		if ($sweepScheme['PS_EVERY_PERIODIC'] == '1') {
			$sweepScheme['DAYNAME'] = 'Monday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '2') {
			$sweepScheme['DAYNAME'] = 'Tuesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '3') {
			$sweepScheme['DAYNAME'] = 'Wednesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '4') {
			$sweepScheme['DAYNAME'] = 'Thursday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '5') {
			$sweepScheme['DAYNAME'] = 'Friday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '6') {
			$sweepScheme['DAYNAME'] = 'Saturday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '7') {
			$sweepScheme['DAYNAME'] = 'Sunday';
		}

				$frequen = '';
		if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '1'){

			$frequen = 'Daily';
			if (!empty($report_day)) {
				foreach ($report_day as $key => $value) {
					$this->view->{'check' . $value['DAY_ID']} = 'checked';
				}
			}

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '2' || $sweepScheme['PS_EVERY_PERIODIC_UOM'] == '5'){
			$frequen = 'Weekly';

			$this->view->{'check' . $sweepScheme['PS_EVERY_PERIODIC']} = 'checked';

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '3' || $sweepScheme['PS_EVERY_PERIODIC_UOM'] == '6'){
			$frequen = 'Monthly';
			$this->view->datemonth = $sweepScheme['PS_EVERY_PERIODIC'];
		}
		$this->view->futuretrx = false;
		$this->view->frequen = $frequen;
		}else{
			$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		}

		$this->view->sweepdata = $sweepScheme;
		
		if(empty($pslipTrx['PS_SWEEP_TYPE'])){
			$pslipTrx['PS_SWEEP_TYPE'] = 0;
		}
		$this->view->PS_SWEEP_TYPE = $pslipTrx['PS_SWEEP_TYPE'];
		$this->view->eftime = $pslipTrx['EFTIME'];
		$this->view->TRA_REMAIN 	= $pslipTrx['REMAIN'];
		$this->view->PS_MIN_AMOUNT  = $pslipTrx['PS_MIN_AMOUNT'];
		//$this->view->frequen = '1x';
		$this->view->fields 			= $fields;
		$this->view->fieldsdetail		= $fieldsdetail;
		$this->view->tableDtl 			= $tableDtl;
		$this->view->TITLE_MST		 	= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer To');
	}


	private function sweepoutothers($pslip)
	{
		require_once 'General/Charges.php';
		$PS_NUMBER = $this->_paymentRef;


		$settings 			= new Application_Settings();
		
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		
		$config = Zend_Registry::get('config');
		$pay = $config['payment']['status'];
		$paystatusarr = array_combine(array_values($pay['code']),array_values($pay['desc']));
			//var_dump($pay);die;
  		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";
  			
  		



		$select	= $this->_db->select()
			->from(
				array('TP' => 'T_PSLIP'),
				array(
					'REF_ID'					=> 'TP.REFF_ID',
					'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
					'EFTIME'					=> 'TP.PS_EFTIME',
					'REMAIN'					=> 'TP.PS_REMAIN',
					'STATUS'					=> $casePayStatus,
				)
			)
			->where('TP.PS_NUMBER = ?', $PS_NUMBER);
		$pslipTrx = $this->_db->fetchRow($select);

		if($pslipTrx['REMAIN'] == '0.00'){
			$this->view->remain = '-';
		}else{
			$this->view->remain = Application_Helper_General::displayMoney($pslipTrx['REMAIN']);
		}

		$this->view->eftime = $pslipTrx['EFTIME'];
		$this->view->paystatus = $pslipTrx['STATUS'].' - Untransfered';
		$PeriodIdSweepIn = $pslipTrx['PS_PERIODIC'];
		$refIdSweepIn = $pslipTrx['REF_ID'];
		if(!empty($PeriodIdSweepIn)){
		$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC'))
			->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$sweepScheme = $this->_db->fetchRow($select);
		//print_r($sweepScheme);die;


		$selectday	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC_DAY'))
			// ->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$report_day = $this->_db->fetchAll($selectday);

		$frequen = '';
		if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '1'){
			$frequen = 'Daily';

			if (!empty($report_day)) {
				foreach ($report_day as $key => $value) {
					$this->view->{'check' . $value['DAY_ID']} = 'checked';
				}
			}

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '2'){
			$frequen = 'Weekly';
			
			$this->view->{'check' . $sweepScheme['PS_EVERY_PERIODIC']} = 'checked';

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '3'){
			$frequen = 'Monthly';
			$this->view->datemonth = $sweepScheme['PS_EVERY_PERIODIC'];
		}else{
			$frequen = 'Daily';
		}

		$this->view->futuretrx = false;
		$this->view->frequen = $frequen;
		}else{
			$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		}

		if ($sweepScheme['SESSION_TYPE'] == "1") {
			$sessionSweep			= 'Morning (06.00 a.m - 08.00 a.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "2") {
			$sessionSweep			= 'Evening (12.00 p.m - 2.00 p.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "3") {
			$sessionSweep			= 'Night (05.00 p.m - 07.00 p.m )';
		}
		if ($sweepScheme['PS_EVERY_PERIODIC'] == '1') {
			$sweepScheme['DAYNAME'] = 'Monday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '2') {
			$sweepScheme['DAYNAME'] = 'Tuesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '3') {
			$sweepScheme['DAYNAME'] = 'Wednesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '4') {
			$sweepScheme['DAYNAME'] = 'Thursday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '5') {
			$sweepScheme['DAYNAME'] = 'Friday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '6') {
			$sweepScheme['DAYNAME'] = 'Saturday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '7') {
			$sweepScheme['DAYNAME'] = 'Sunday';
		}

		$this->view->sweepdata = $sweepScheme;

		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		$this->_tableMst[6]["label"] = $this->language->_('Recurring');
		$this->_tableMst[6]["value"] = $sweepScheme['DAYNAME'];

		$this->_tableMst[7]["label"] = $this->language->_('Session');
		$this->_tableMst[7]["value"] = $sessionSweep;

		$this->_tableMst[8]["label"] = $this->language->_('End Date Scheme');
		$this->_tableMst[8]["value"] = Application_Helper_General::convertDate($sweepScheme['PS_PERIODIC_ENDDATE'], $this->_dateViewFormat);
		
		
		

		if ($pslip['BALANCE_TYPE'] == '2') {
			$persen	= "(%)";
		} else {
			$persen	= "";
		}
		// Table Detail Header
		// $fields = array(
		// 	"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name'),
		// 	"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
		// 	"ACBENEF"			=> $this->language->_('Beneficiary Account'),
		// 	"TRA_MESSAGE" 		=> $this->language->_('Message'),
		// 	"TRA_ADDITIONAL_MESSAGE"  	   	=> $this->language->_('Additional Message'),
		// 	//"TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
		// 	"ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
		// 	//"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
		// 	// 				"TRANSFER_FEE"  	=> 'Transfer Charge',
		// 	//"RATE"  	   	=> $this->language->_('Rate'),
		// 	//"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
		// 	//"SOURCE_ACCT_BANK_CODE"  	   	=> $this->language->_('Source Bank Name'),
		// 	//"BENEF_ACCT_BANK_CODE"  	   	=> $this->language->_('Transfer Fee'),
		// 	//"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
		// 	//"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
		// 	//"TRA_AMOUNT_TOTAL"  	=> $this->language->_('Total'),
		// 	//"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'),
		// 	//"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

		// 	"ACBENEF_EMAIL" 	=> $this->language->_('Email'),
		// 	//"ACBENEF_ISAPPROVE" => $this->language->_('Status')
		// );
		
		$fields = array(
			"BENEFICIARYTYPE"			=> $this->language->_('Beneficiary Type'),
			"BENEF_ACCT_BANK_CODE" 		=> $this->language->_('Bank Name'),
			"ACBENEF"					=> $this->language->_('Beneficiary Account'),
			"ACBENEF_NAME"				=> $this->language->_('Beneficiary Account Name'),
			"ACBENEF_CCY"  	   			=> $this->language->_('Currency'),
			"ACBENEF_EMAIL" 			=> $this->language->_('Beneficiary Email'),
		);
		
		$fieldsdetail = array(
			 "TRA_AMOUNT"  	   	=> $this->language->_('Actual Amount'),
			 "TOTAL_CHARGES"  	=> $this->language->_('Deduction Amount'),
			 "PS_TOTAL_AMOUNT"	=> $this->language->_('Total Amount'),
			 "BENEFICIARY_CATEGORY"	=> $this->language->_('Beneficiary Category'),
			 "LLD_IDENTITY"	=> $this->language->_('Identity'),
			 "LLD_TRANSACTOR_RELATIONSHIP"	=> $this->language->_('Transactor Relationship'),
			 "LLD_TRANSACTION_PURPOSE"	=> $this->language->_('Transactor Purpose')
		);
		
		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }

		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> 'TT.BENEFICIARY_ACCOUNT_NAME' ,// ' (' , TT.BENEFICIARY_ALIAS_NAME , ')')"),
					'TT.BENEFICIARY_ALIAS_NAME',
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDITIONAL_MESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'TRA_ADDITIONAL_MESSAGE' => 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
					'PS_MIN_AMOUNT'			=> 'C.PS_MIN_AMOUNT',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL', 'TRA_AMOUNT_TOTAL'			=> 'TT.TRA_AMOUNT',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					'TT.FULL_AMOUNT_FEE', 'TT.PROVISION_FEE', 'TT.RATE',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'SOURCE_ACCT_BANK_CODE'			=> 'TT.SOURCE_ACCT_BANK_CODE',
					'BENEF_ACCT_BANK_CODE'			=> 'TT.BENEF_ACCT_BANK_CODE',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $this->_bankName . "'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN B.BENEFICIARY_ISAPPROVE = '0' THEN 'Waiting Approval'
																				 WHEN B.BENEFICIARY_ISAPPROVE = '1' THEN 'Approved'
																				 ELSE '-'
																			END"),
					'BALANCE_TYPE'				=> 'C.BALANCE_TYPE',
					// 'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
        			// 															FROM T_PERIODIC_DETAIL Y
        			// 															inner join T_PSLIP Z
        			// 															on Y.PS_PERIODIC = Z.PS_PERIODIC
        			// 															where
        			// 															Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
																						
					'LLD_IDENTITY',
					'BENEFICIARY_CATEGORY',					
					'LLD_TRANSACTION_PURPOSE',
					'LLD_TRANSACTOR_RELATIONSHIP'
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array('PS_SWEEP_TYPE'))
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);
		//var_dump($pslipTrx['0']['TRA_MESSAGE']);die;
		$this->view->PS_SWEEP_TYPE = $pslipTrx['0']['PS_SWEEP_TYPE'];
		$this->view->tra_type = $pslipTrx['0']['TRANSFER_TYPE'];
		$this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		$this->view->addmessage = $pslipTrx['0']['TRA_ADDITIONAL_MESSAGE'];
		$this->view->TRA_REMAIN 	= $pslipTrx['0']['TRA_REMAIN'];
		$this->view->PS_MIN_AMOUNT  = $pslipTrx['0']['PS_MIN_AMOUNT'];
		
		// 		print_r($pslipTrx);die;
		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		$ACBENEF_IDarr = array();
		$tableDtl = array();

		if ($pslip['PS_TYPE'] != '15') {
			foreach ($pslipTrx as $p => $pTrx) {
				// Create array bene for validation
				if (!empty($pTrx["ACBENEF_ID"])) {
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}

				$trfType = $pTrx["TRANSFER_TYPE"];

				// create table detail data
				foreach ($fields as $key => $field) {
					$value = $pTrx[$key];

					/* if ($key == "TRANSFER_FEE")
					 {
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);
					} */

					if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE") {
						$value = Application_Helper_General::displayMoney($value);
					} elseif ($key == "ACBENEF") {
						$value = $value . " [" . $pTrx[$key . "_CCY"] . "]";
					} elseif ($key == "TRANSFER_TYPE_disp") {
						if ($value == 'PB') {
							$value = 'In House';
						}
					}else if($key == 'ACBENEF_NAME'){
						if(empty($pTrx['BENEFICIARY_ALIAS_NAME'])){
							$alias = '-'; 
						}else{
							$alias = $pTrx['BENEFICIARY_ALIAS_NAME'];
						}
						//var_dump($value);
						$value = $value;

					}

					$value = ($value == "") ? "-" : $value;

					$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
					//var_dump($pTrx);die;
					if (!empty($pTrx['BENEF_ACCT_BANK_CODE'])) {
						$bankcode = $pTrx['BENEF_ACCT_BANK_CODE'];
						$bankname = $bankNameArr[$bankcode];
					}
					$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
					if (!empty($pTrx['SOURCE_ACCT_BANK_CODE'])) {
						$bankcode = $pTrx['SOURCE_ACCT_BANK_CODE'];
						$bankname = $bankNameArr[$bankcode];
					}
					$tableDtl[$p]['SOURCE_ACCT_BANK_CODE'] = $bankname;
					
					if (!empty($pTrx['LLD_IDENTITY'])) {
						$lldidentity = $pTrx['LLD_IDENTITY'];
						$lldident = $lldIdenticalArr[$lldidentity];
					}
					$tableDtl[$p]['LLD_IDENTITY'] = $lldident;
					
					if (!empty($pTrx['LLD_TRANSACTOR_RELATIONSHIP'])) {
						$lldrelation = $pTrx['LLD_TRANSACTOR_RELATIONSHIP'];
						$lldrelations = $lldRelationshipArr[$lldrelation];
					}
					$tableDtl[$p]['LLD_TRANSACTOR_RELATIONSHIP'] = $lldrelations;
					
					if (!empty($pTrx['LLD_TRANSACTION_PURPOSE'])) {
						$lldp = $pTrx['LLD_TRANSACTION_PURPOSE'];
						$lldpurpose = $lldPurposeArr[$lldp];
					}
					$tableDtl[$p]['LLD_TRANSACTION_PURPOSE'] = $lldpurpose;
					
					
					if (!empty($pTrx['BENEFICIARY_CATEGORY'])) {
						$b_cat = $pTrx['BENEFICIARY_CATEGORY'];
						$bcategory = $lldCategoryArr[$b_cat];
					}
					$tableDtl[$p]['BENEFICIARY_CATEGORY'] = $bcategory;
					
					if (!empty($pTrx['TRANSFER_TYPE'])) {
						if ($pTrx['TRANSFER_TYPE'] == '1' || $pTrx['TRANSFER_TYPE'] == '2') {
							$beneftype = 'SKN/RTGS';
						}else{
							$beneftype = 'Online';
						}
					}
					$tableDtl[$p]['BENEFICIARYTYPE']  	= $beneftype;
					
				}
			}
		//	echo '<pre>';
	//		var_dump($pTrx);die;
			//die;
		} else {
			$select	= $this->_db->select()
				// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
				->from(array('TTS' => 'T_PERIODIC'))
				->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
				->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
			// 		echo $select;die;
			$sweepdetail = $this->_db->fetchAll($select);

			foreach ($pslipTrx as $p => $pTrx) {
				// Create array bene for validation
				if (!empty($pTrx["ACBENEF_ID"])) {
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}

				$trfType = $pTrx["TRANSFER_TYPE"];

				// create table detail data
				foreach ($fields as $key => $field) {
					$value = $pTrx[$key];

					/* if ($key == "TRANSFER_FEE")
					 {
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);
					} */

					if ($key == "TRA_AMOUNT" || $key == "TRA_AMOUNT_TOTAL") {
						$value = Application_Helper_General::displayMoney($sweepdetail[$p]['TRA_REMAIN']);
					} elseif ($key == "RATE") {
						if ($sweepdetail[$p]['RATE'] == '') {
							$sweepdetail[$p]['RATE'] = '0.00';
						}
						$value = 'IDR ' . Application_Helper_General::displayMoney($sweepdetail[$p]['RATE']);
					} elseif ($key == "TRANSFER_FEE") {
						if ($sweepdetail[$p]['TRANSFER_FEE'] == '') {
							$sweepdetail[$p]['TRANSFER_FEE'] = '0.00';
						}
						$value = 'IDR ' . Application_Helper_General::displayMoney($sweepdetail[$p]['TRANSFER_FEE']);
					} elseif ($key == "PROVISION_FEE") {
						if ($sweepdetail[$p]['PROVISION_FEE'] == '') {
							$sweepdetail[$p]['PROVISION_FEE'] = '0.00';
						}
						$value = 'IDR ' . Application_Helper_General::displayMoney($sweepdetail[$p]['PROVISION_FEE']);
					} elseif ($key == "FULL_AMOUNT_FEE") {
						if ($sweepdetail[$p]['FULL_AMOUNT_FEE'] == '') {
							$sweepdetail[$p]['FULL_AMOUNT_FEE'] = '0.00';
						}
						$value = 'IDR ' . Application_Helper_General::displayMoney($sweepdetail[$p]['FULL_AMOUNT_FEE']);
					} elseif ($key == "ACBENEF") {
						$value = $value . " [" . $pTrx[$key . "_CCY"] . "]";
					} elseif ($key == "TRANSFER_TYPE_disp") {
						if ($value == 'PB') {
							$value = 'In House';
						}
					}

					$value = ($value == "") ? "-" : $value;

					$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
					
				}
			}
		}
		
		$this->_tableMst[9]["label"] = $this->language->_('Source Bank Name');
		$this->_tableMst[9]["value"] = $tableDtl[0]['SOURCE_ACCT_BANK_CODE'];
		$this->_tableMst[10]["label"] = $this->language->_('Source Alias');
		$this->_tableMst[11]["label"] = $this->language->_('Beneficiary Alias');
		
		
		$sourcealias = '-';
		if($pslip['SOURCE_ACCOUNT_ALIAS_NAME'] != ''){
			$sourcealias = $pslip['SOURCE_ACCOUNT_ALIAS_NAME'];
		}
		$benefalias = '-';
		if($pslip['BENEFICIARY_ALIAS_NAME'] != ''){
			$benefalias = $pslip['BENEFICIARY_ALIAS_NAME'];
		}

		$this->_tableMst[10]["value"] = $sourcealias;
		$this->_tableMst[11]["value"] = $benefalias;
		
	//	echo '<pre>';
	//var_dump($tableDtl);die;
		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array(
			"FROM" 				=> "D",	// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"BALANCETYPE"			=> $pslip['BALANCE_TYPE'],
			"PS_TYPE"				=> $pslip["PS_TYPE"],
			"PS_REMAIN"				=> $sweepScheme["TRA_REMAIN"],
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);
		//print_r($paramApprove);die;
		$validate->checkApprove($paramApprove);

		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment

		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
		$this->view->fieldsdetail	= $fieldsdetail;
		$this->view->TITLE_MST		 	= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer To');
		$this->view->labelpersen 	 	= $persen;
	}
	
	private function creditopen($pslip)
	{
		require_once 'General/Charges.php';
		$PS_NUMBER = $this->_paymentRef;
		
		$settings 			= new Application_Settings();
		
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();

		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], empty($pslip["accsrc_alias"]) ? '' : $pslip["accsrc_alias"]);

		// Table Detail Header
		$fields = array(
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name'),
			"BENEFICIARY_BANK_CITY"		=> $this->language->_('City'),
			"ACBENEF"			=> $this->language->_('Beneficiary Account'),
			"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
			"BENEFICIARY_ALIAS_NAME"		=> $this->language->_('Beneficiary Alias Name'),
			"BENEFICIARY_ADDRESS"		=> $this->language->_('Beneficiary Address 1'),
			"BENEFICIARY_ADDRESS2"		=> $this->language->_('Beneficiary Address 2'),
			"ACBENEF_EMAIL" 	=> $this->language->_('Beneficiary Email'),

			"BENEFICIARY_CITIZENSHIP" 	=> $this->language->_('Citizenship'),
			"BENEFICIARY_RESIDENT" 	=> $this->language->_('Nationalilty'),
			"BENEFICIARY_CATEGORY" 	=> $this->language->_('Beneficiary Category'),
			"BENEFICIARY_ID_TYPE" 	=> $this->language->_('Beneficiary ID Type'),
			"BENEFICIARY_ID_NUMBER" 	=> $this->language->_('Beneficiary ID Number'),
			// "TRA_MESSAGE" 		=> $this->language->_('Message'),
			// "TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			// "TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
			// "ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
			 //"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			// "TRANSFER_FEE"  	=> 'Transfer Charge',
			// "RATE"  	   	=> $this->language->_('Rate'),
			// "TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			// "FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			// "PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			// "TOTAL"  	=> $this->language->_('Total'),
			
			// "NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

			
			// "ACBENEF_ISAPPROVE" => $this->language->_('Status'),
			// "BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);
		
		
		$fieldsdetail = array(
			
			// "TRA_MESSAGE" 		=> $this->language->_('Message'),
			// "TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			// "TRANSFER_TYPE_disp" => $this->language->_('Transfer Type'),
			// "ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
			 "TRA_AMOUNT"  	   	=> $this->language->_('Total Amount'),
			 "TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			 //"RATE"  	   	=> $this->language->_('Rate'),
			// "FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			// "PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			  "TOTAL"  	=> $this->language->_('Total'),
			 "BENEFICIARY_CATEGORY"	=> $this->language->_('Beneficiary Category'),
			 "LLD_IDENTITY"	=> $this->language->_('Identity'),
			 "LLD_TRANSACTOR_RELATIONSHIP"	=> $this->language->_('Transactor Relationship'),
			 "LLD_TRANSACTION_PURPOSE"	=> $this->language->_('Transactor Purpose')
			
			// "NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),

			
			// "ACBENEF_ISAPPROVE" => $this->language->_('Status'),
			// "BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);




		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		$ACBENEF_IDarr = array();
		$tableDtl = array();

		$app = Zend_Registry::get('config');
		$appBankname = $app['app']['bankname'];


		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'BENEFICIARY_BANK_CITY',
					'BENEFICIARY_ALIAS_NAME',
					'BENEFICIARY_ADDRESS',
					'BENEFICIARY_ADDRESS2',
					'BENEFICIARY_ID_TYPE',
					'BENEFICIARY_ID_NUMBER',
					'BENEFICIARY_CITIZENSHIP' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CITIZENSHIP = 'W' THEN 'WNI'
																				 WHEN TT.BENEFICIARY_CITIZENSHIP = 'R' THEN 'WNA'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_RESIDENT' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_RESIDENT = 'R' THEN 'Residence'
																				 WHEN TT.BENEFICIARY_RESIDENT = 'W' THEN 'Non REsidence'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_CATEGORY' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CATEGORY = '1' THEN 'Individual'
																				 WHEN TT.BENEFICIARY_CATEGORY = '2' THEN 'Government'
																				 WHEN TT.BENEFICIARY_CATEGORY = '3' THEN 'Bank'
																				 WHEN TT.BENEFICIARY_CATEGORY = '4' THEN 'Non Bank Financial Institution'
																				 WHEN TT.BENEFICIARY_CATEGORY = '5' THEN 'Company'
																				 WHEN TT.BENEFICIARY_CATEGORY = '6' THEN 'Other'
																				 ELSE '-'
																			END"),

					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'C.PS_TOTAL_AMOUNT',
					'TOTAL_CHARGES'			=> 'TT.TOTAL_CHARGES',		
					// 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'TT.RATE',
					'TT.PROVISION_FEE',
					'TT.NOSTRO_NAME',
					'C.PS_PERIODIC',
					'REF_ID'					=> 'C.REFF_ID',
					'PS_PERIODIC'					=> 'C.PS_PERIODIC',
					'EFTIME'					=> 'C.PS_EFTIME',
					'REMAIN'					=> 'C.PS_REMAIN',
					'TT.FULL_AMOUNT_FEE',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'C.CUST_ID', 'TT.SOURCE_ACCOUNT_CCY',
					'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
					'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			 WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN C.PS_STATUS = '1' THEN 'Waiting Approval'
																				 WHEN C.PS_STATUS = '2' THEN 'Approved'
																				 ELSE '-'
																			END"),
					'LLD_IDENTITY',
					'BENEFICIARY_CATEGORY',					
					'LLD_TRANSACTION_PURPOSE',
					'LLD_TRANSACTOR_RELATIONSHIP'
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo $selectTrx;die;
		$pslipTrx = $this->_db->fetchAll($selectTrx);
		
		$this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		$this->view->addmessage = $pslipTrx['0']['TRA_ADDMESSAGE'];
		
		$PeriodIdSweepIn = $pslipTrx['0']['PS_PERIODIC'];
		//var_dump($PeriodIdSweepIn); 
		$refIdSweepIn = $pslipTrx['0']['REF_ID'];
		if(!empty($PeriodIdSweepIn)){
		$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC'))
			->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
		//echo $select;
		$sweepScheme = $this->_db->fetchRow($select);
		//print_r($sweepScheme);die;


		$selectday	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from(array('TTS' => 'T_PERIODIC_DAY'))
			// ->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
			->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$report_day = $this->_db->fetchAll($selectday);

		$frequen = '';
		if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '1'){
			$frequen = 'Daily';

			if (!empty($report_day)) {
				foreach ($report_day as $key => $value) {
					$this->view->{'check' . $value['DAY_ID']} = 'checked';
				}
			}

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '2'){
			$frequen = 'Weekly';

			$this->view->{'check' . $sweepScheme['PS_EVERY_PERIODIC']} = 'checked';

		}else if($sweepScheme['PS_EVERY_PERIODIC_UOM'] == '3'){
			$frequen = 'Monthly';
			$this->view->datemonth = $sweepScheme['PS_EVERY_PERIODIC'];
		}else{
			$frequen = 'Daily';
		}
		//var_dump($frequen);
		$this->view->futuretrx = false;
		$this->view->frequen = $frequen;
		}else{
			$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		}

		if ($sweepScheme['SESSION_TYPE'] == "1") {
			$sessionSweep			= 'Morning (06.00 a.m - 08.00 a.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "2") {
			$sessionSweep			= 'Evening (12.00 p.m - 2.00 p.m )';
		} elseif ($sweepScheme['SESSION_TYPE'] == "3") {
			$sessionSweep			= 'Night (05.00 p.m - 07.00 p.m )';
		}
		if ($sweepScheme['PS_EVERY_PERIODIC'] == '1') {
			$sweepScheme['DAYNAME'] = 'Monday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '2') {
			$sweepScheme['DAYNAME'] = 'Tuesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '3') {
			$sweepScheme['DAYNAME'] = 'Wednesday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '4') {
			$sweepScheme['DAYNAME'] = 'Thursday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '5') {
			$sweepScheme['DAYNAME'] = 'Friday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '6') {
			$sweepScheme['DAYNAME'] = 'Saturday';
		} else if ($sweepScheme['PS_EVERY_PERIODIC'] == '7') {
			$sweepScheme['DAYNAME'] = 'Sunday';
		}

		$this->view->sweepdata = $sweepScheme;
		// echo "<pre>";
		// 	print_r($pslipTrx);die;

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }
		
		//echo '<pre>';
		//var_dump($pslipTrx);die;
		foreach ($pslipTrx as $p => $pTrx) {
			// Create array bene for validation
			if (!empty($pTrx["ACBENEF_ID"])) {
				$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
			}

			$trfType = $pTrx["TRANSFER_TYPE"];

			$psCategory = $pslip['PS_CATEGORY'];

			// create table detail data
			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];

				/* if ($key == "TRANSFER_FEE")
				{
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);	
				} */
				/*
				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}
				elseif ($key == "ACBENEF")
				{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
				*/
				if ($key == 'ACBENEF' && ($pTrx['TRANSFER_TYPE'] == '10' || $pTrx['TRANSFER_TYPE'] == '9')) {
					$value = '';
				}else if($key == 'ACBENEF'){
					$value = $value.'('.$pTrx['ACBENEF_CCY'].')';
				}
				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN" || $key == "PROVISION_FEE") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['ACBENEF_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == "TRANSFER_FEE" && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {

					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;
					$trffee = $this->_db->fetchRow($selecttrffee);

					if ($value != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					}
				}
				if ($key == "FULL_AMOUNT_FEE" && !empty($pTrx['ACBENEF_CCY'])) {

					$selecttrfFA = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '4')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['ACBENEF_CCY']);
					// echo $selecttrfFA;die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);
					if ($pTrx['TRANSFER_TYPE'] == '10') {
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else if ($pTrx['TRANSFER_TYPE'] == '9') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID']);
						// echo $selecttrfFA;die;
						$trfFA = $this->_db->fetchRow($selecttrfFA);
					}
					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);
				}

				if ($key == 'PROVISION_FEE' && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {




					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['TRANSFER_TYPE_disp'] == 'PB') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							$value = $pTrx['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']) . ' (IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']) . ')';
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != '-') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}



				if ($key == "TRANSFER_TYPE_disp") {
					if ($value == 'PB') {
						$value = 'In House';
					}
				}
				
				if (!empty($pTrx['LLD_IDENTITY'])) {
						$lldidentity = $pTrx['LLD_IDENTITY'];
						$lldident = $lldIdenticalArr[$lldidentity];
					}
					$tableDtl[$p]['LLD_IDENTITY'] = $lldident;
					
					if (!empty($pTrx['LLD_TRANSACTOR_RELATIONSHIP'])) {
						$lldrelation = $pTrx['LLD_TRANSACTOR_RELATIONSHIP'];
						$lldrelations = $lldRelationshipArr[$lldrelation];
					}
					$tableDtl[$p]['LLD_TRANSACTOR_RELATIONSHIP'] = $lldrelations;
					
					if (!empty($pTrx['LLD_TRANSACTION_PURPOSE'])) {
						$lldp = $pTrx['LLD_TRANSACTION_PURPOSE'];
						$lldpurpose = $lldPurposeArr[$lldp];
					}
					$tableDtl[$p]['LLD_TRANSACTION_PURPOSE'] = $lldpurpose;
					
					
					if (!empty($pTrx['BENEFICIARY_CATEGORY'])) {
						$b_cat = $pTrx['BENEFICIARY_CATEGORY'];
						$bcategory = $lldCategoryArr[$b_cat];
					}
					$tableDtl[$p]['BENEFICIARY_CATEGORY'] = $bcategory;
					


				$value = ($value == "") ? "-" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
				$tableDtl[$p]['TRA_AMOUNT']  		= Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
				$tableDtl[$p]['TRANSFER_FEE'] 		=  Application_Helper_General::displayMoney($pTrx['TRANSFER_FEE']);
				$totalAmount 						= $pTrx['TRA_AMOUNT'] 	- $pTrx['TRANSFER_FEE'];
				$tableDtl[$p]['TOTAL'] 	= Application_Helper_General::displayMoney($totalAmount);			
			}

			if (!empty($pslip['BANK_CODE'])) {
				$bankcode = $pslip['BANK_CODE'];
			} else {
				$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
			}

			if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
				$bankname = '-';
			} else if (empty($bankcode)) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$bankcode];
			}
			
			$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
		}

		// print_r($tableDtl);die();

		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array(
			"FROM" 				=> "D",	// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);
		// var_dump($paramApprove);die;
		$validate->checkApprove($paramApprove);

		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment
		
		$config = Zend_Registry::get('config');
		$paystatusarr = array_combine(array_values($config['payment']['status']['code']),array_values($config['payment']['status']['desc']));
			//var_dump();die;
  		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";
  			



		$select	= $this->_db->select()
			->from(
				array('TP' => 'T_PSLIP'),
				array(
					'REF_ID'					=> 'TP.REFF_ID',
					'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
					'EFTIME'					=> 'TP.PS_EFTIME',
					'REMAIN'					=> 'TP.PS_REMAIN',
					'STATUS'					=> $casePayStatus,
					'TRANS'						=> 'TP.PS_TXCOUNT'
				)
			)
			->where('TP.PS_NUMBER = ?', $PS_NUMBER);
			//echo $select;die;
		$pslipTrx = $this->_db->fetchRow($select);
		
		if($pslipTrx['TRANS'] > 1){
			//$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$this->view->downloadurl = $downloadURL;
			$this->view->trans = $pslipTrx['TRANS'];
		}
		$this->view->paystatus = $pslipTrx['STATUS'].' - Untransfered';
		// echo "<pre>";
		// print_r($tableDtl);die();
		//$this->view->frequen = '1x';
		$this->view->fields 			= $fields;
		$this->view->fieldsdetail		= $fieldsdetail;
		$this->view->tableDtl 			= $tableDtl;
		$this->view->TITLE_MST		 	= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer To');
	}

	public function findPolicyBoundary($transfertype, $amount)
	{

		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);
		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		 //echo $selectuser;die;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{


		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);	
		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		 //echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}
		
		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}
	
		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
	//	echo '<pre>';
//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		//die;

		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);

		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		// echo $selectuser;
		
		$datauser = $this->_db->fetchAll($selectuser);
		 if(empty($datauser)){
			 
			 return true;
			 
		 }
		 
		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_USER'), array(
				'*'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);

		
		

		$this->view->boundarydata = $datauser;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {
				$group = explode('_', $value['GROUP_USER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				// print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				//var_dump($usergroup);
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);

					//var_dump($list);
					foreach ($list as $row => $data) {
						
						if ($data == $groupalfa) {
							$cek = true;
							// die('ter');
							break;
						}
					}
				}
			}
			//die;
			
			
			
			if ($group[0] == 'S') {
				$cek = true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		}
		$tempusergroup = $usergroup;

		if ($cek) {




			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);
		
			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
			
			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);
			//var_dump($phpCommand);die;
			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);
			// print_r($list);die;
			// var_dump($command)

			$thendata = explode('THEN', $command);
			// print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);
			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {
					foreach ($secondlist as $row => $thenval) {
						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			//var_dump($cthen);
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
							// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						$newsecondcommand = str_replace('(', '', trim($thenval));
						$newsecondcommand = str_replace(')', '', $newsecondcommand);
							$newsecondcommand = str_replace('AND', '', $newsecondcommand);
						$newsecondcommand = str_replace('OR', '', $newsecondcommand);
							$newsecondlist = explode(' ', $newsecondcommand);
						//var_dump($newsecondcommand);
						if (in_array(trim($value['GROUP']), $newsecondlist)) {
						//if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}
			//var_dump($grouplist);die;
			//var_dump($thengroup);die;
			// var_dump($thengroup);die;
			// // print_r($group);die;
			// // echo $thengroup;die;
			//echo '<pre>';
			//var_dump($thengroup);
			//var_dump($thendata);
			//print_r($thendata);echo '<br/>';die('here');
			if ($thengroup == true) {

				 
				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;
					//echo $oriCommand;die;
					$indno = $i;
					//echo $oriCommand;echo '<br>';

					for ($a = $cthen - $indno; $a >= 1; --$a) {

						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}


					//print_r($thendata);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					//print_r($oriCommand);echo '<br>';
					//print_r($list);echo '<br>';die;
					
					
					$result = $this->generate($oriCommand, $list, $param, $psnumb,$groupalfa,$thengroup);
					// print_r($i);
					//var_dump($result);
					  //echo 'result-';var_dump($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							//die;
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);
	//						var_dump($secondlist);
	//						var_dump($grouplist);
//							die;

							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											//echo 'sini';
											return false;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						 //echo $oriCommand;
						$result = $this->generate($oriCommand, $list, $param, $psnumb,$groupalfa,$thengroup);
						 //var_dump($result);echo '<br/>';
						if (!$result) {
							// die;
							//print_r($groupalfa);
							//print_r($thendata[$i]);
							
							if($groupalfa == trim($thendata[$i])){
								//echo 'heer';
								return true;
							}else{
								
								//return true;
							}
							/*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
						} else {
							
							//return false;
								
							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						 //die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);
						//var_dump($i);
						// var_dump($thendata);
						 //print_r($grouplist);
						 //die;
						 //if($groupalfa == $gro)
							 $approver = array();
							$countlist = array_count_values($list);
							foreach ($list as $key => $value) {
								if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
									$selectapprover	= $this->_db->select()
										->from(array('C' => 'T_APPROVAL'), array(
											'USER_ID'
										))
										->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
										// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
										->where("C.PS_NUMBER = ?", (string) $psnumb)
										->where("C.GROUP = ?", (string) $value);
								//	 echo $selectapprover;
									$usergroup = $this->_db->fetchAll($selectapprover);
									// print_r($usergroup);
									$approver[$value] = $usergroup;
									if($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value){
										//var_dump($countlist[$value]);
										//var_dump($tempusergroup['0']['GROUP']);  
										//die('gere');
										return false;
										
									}
								}
							} 
							
							//$array = array(1, "hello", 1, "world", "hello");
							
							
							//echo '<pre>';
							//var_dump($list);
							//var_dump($approver);
							//die;
							//var_dump($secondlist[0]);die;
							//foreach($approver as $app => $valpp){
								if(!empty($thendata[$i])){
									if(empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']){
										//die('gere');
										return false;
									}
								}
							
							//echo '<pre>';
							//var_dump($thendata);
							//var_dump($grouplist);
							//var_dump($approver);
							//var_dump($groupalfa);
							//die;
							 foreach($grouplist as $key => $vg){
								 $newgroupalpa = str_replace('AND', '', $vg);
									$newgroupalpa = str_replace('OR', '', $newgroupalpa);
									$groupsecondlist = explode(' ', $newgroupalpa);
									 //var_dump($newgroupalpa);
									 //&& empty($approver[$groupalfa])
								 if(in_array($groupalfa,$groupsecondlist)  ){
									 // echo 'gere';die;
									 return true;
									 //die('ge');
								 }
								 
								 if($vg == $groupalfa  && count($approver[$groupalfa]) > 0){
									 // echo 'gere';die;
									 return true;
									 //die('ge');
								 }
								 
							 }
							
							// var_dump($approver[$groupalfa]);
							// var_dump($grouplist);
							 //var_dump($groupalfa);die;
							 
							 //echo 'ger';die;
						//print_r($secondlist);die;
					//	 return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {
										
										if(empty($thendata[1])){
											//die;
												return true;
										}
										//else{
										//	return false;
									//	}
									}
								}
							}
						}
						 
					//	echo 'here';die;
						 $secondresult = $this->generate($thendata[$i-1],$list,$param,$psnumb,$groupalfa,$thengroup);
						// var_dump($secondresult);
						 //print_r($thendata[$i-1]);
						 //die;
						 //if()
						//	 echo '<pre>';
						 //var_dump($grouplist);die;
						foreach ($grouplist as $key => $valgroup) {
							//print_r($valgroup);
							//var_dump($thendata[$i]); 
							
							
							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								 //die('here');
								 if($secondresult){
										return false;
								 }else{
									 return true;
								 }
								
								//break;
							}
							
							//else if (trim($valg) == trim($thendata[$i - 1])) {
						//		$cekgroup = false;
								// die('here');
							//	return false;
						//	}
						}
						//die;
						//if (!$cekgroup) {
							// die('here');
							//return false;
						//}
					}

					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {
				
				//var_dump($groupalfa)die;
				foreach ($thendata as $ky => $vlue) {
					$newsecondcommand = str_replace('(', '', trim($vlue));
					$newsecondcommand = str_replace(')', '', $newsecondcommand);
					$newsecondcommand = str_replace('AND', '', $newsecondcommand);
					$newsecondcommand = str_replace('OR', '', $newsecondcommand);
					$newsecondlist = explode(' ', $newsecondcommand);
					if ($newsecondlist['0'] == $groupalfa) {
						return true;
					}
				}
				return false;
			}
			
			


			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_APPROVAL'), array(
							'USER_ID'
						))
						->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.PS_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
				//	 echo $selectapprover;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			//die;
			



			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$numb = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $numb . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			$thendata = explode('THEN$',$phpCommand);
			//var_dump($thendata);die;
			if(!empty($thendata['1'])){
					$phpCommand = '';
				foreach($thendata as $tkey => $tval){
					$phpCommand .= '(';
					$phpCommand .= $tval.')';
					if(!empty($thendata[$tkey+1])){
					$phpCommand .= ' && ';
					}
				}
			}else{
				
				$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			
			}
			//var_dump($phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			//var_dump($result);die;
			if(!$result){ 
			
				return true;
			}
			// die('here2');
			 //var_dump ($result);die;
			//return $result;
		} else {
			// die('here');
			return true;
		}
	}



	public function generate($command, $list, $param, $psnumb,$group,$thengroup)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();
		 
		$count_list = array_count_values($list);
		//print_r($count_list);
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.PS_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		//var_dump($param);die;
		//var_dump($group);
		foreach($approver as $appval){
			$totaldata = count($approver[$group]);
			$totalgroup = $count_list[$group];
			//var_dump($totaldata);
			//var_dump($totalgroup);
			if($totalgroup == $totaldata && $totalgroup != 0){
				
				return false;
			}
		}//die;
		//die;
		 

		

		 
		foreach ($param as $url) {
			
			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$numb = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $numb . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}
		
		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		//print_r($phpCommand);echo '<br/>';

		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			//var_dump($thengroup);
			// var_dump($result);
			 
			if ($result) {
//var_dump($thengroup);die;
				if($thengroup){
						return true;
				}else{
					return false;
				}
				
			} else {
				
				if($thengroup){
						return false;
				}else{
					
					return true;
				}
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;




	}

	public function like($str, $searchTerm)
	{
		$searchTerm = strtolower($searchTerm);
		$str = strtolower($str);
		$pos = strpos($str, $searchTerm);
		if ($pos === false)
			return false;
		else
			return true;
	}

	public function detailAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		$filter 			= new Application_Filtering();
		$PS_NUMBER 			= $filter->filter($this->_getParam('payReff'), "PS_NUMBER");
		$pdf 				= $filter->filter($this->_getParam('pdf'), "BUTTON");
		$process 			= $filter->filter($this->_getParam('process'), "BUTTON");
		$approve 			= $this->_getParam('approve');
		$repair 			= $this->_getParam('repair');
		$reject 			= $this->_getParam('reject');
		$this->_paymentRef 	= $PS_NUMBER;

		$sessionNamespace = new Zend_Session_Namespace('URL_CP_WA');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ?
			$sessionNamespace->URL : '/' . $this->view->modulename . '/' . $this->_controllerList . '/index';

		// check privi...
		$this->_hasPriviApprovePayment 	= $this->view->hasPrivilege('PAPV');
		$this->_hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');
		$this->_hasPriviRepairPayment 	= $this->view->hasPrivilege('PRRP');
		//$this->_hasPriviRepairPayment 	= $this->view->hasPrivilege('PRPP');
		$this->_hasPriviRejectPayment 	= $this->view->hasPrivilege('PRJT');

		$paramPayment = array(
			"WA" 				=> true,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);
		

		// echo $select;die;
		$pslip = $this->_db->fetchRow($select);



		$selectapprover	= $this->_db->select()
			->from(array('C' => 'T_APPROVAL'), array(
				'USER_ID'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
			->where("C.PS_NUMBER = ?", (string) $pslip['PS_NUMBER']);
		// ->where("C.GROUP = ?" , (string)$value);
		// echo $selectapprover;die;
		$usergroup = $this->_db->fetchAll($selectapprover);
		// print_r($usergroup);die;
		if (!empty($usergroup)) {
			// die;
			// $this->view->pdf = true;
			$this->view->validbtn = false;
		} else {
			if (!empty($pslip)) {
				if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5' || $pslip['PS_TYPE'] == '11') {
					$PS_TYPE = '18';
				} else {
					$PS_TYPE = $pslip['PS_TYPE'];
				}
				$boundary = $this->validatebtn($PS_TYPE, $pslip['amount'], $pslip['ccy'], $pslip['PS_NUMBER']);
			}

			// print_r($boundary);die;
			if ($boundary) {
				// die;
				$this->view->validbtn = false;
			} else {
				// die;
				$this->view->validbtn = true;
			}
		}

		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					// 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $this->_bankName . "'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN B.BENEFICIARY_ISAPPROVE = '0' THEN 'Waiting Approval'
																				 WHEN B.BENEFICIARY_ISAPPROVE = '1' THEN 'Approved'
																				 ELSE '-'
																			END"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		// echo $selectTrx;die;
		// 							print_r($select->query());die;
		$pslipTrxData = $this->_db->fetchAll($selectTrx);
		// print_r($pslipTrxData);die;

		if (!empty($pslip)) {
			if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5' || $pslip['PS_TYPE'] == '11') {
				$PS_TYPE = '18';
			} else {
				$PS_TYPE = $pslip['PS_TYPE'];
			}


			if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23') {
				if($transfertype == '19' || $transfertype == '20'){
						$transfertype = array('19','20');
				}else{
					$transfertype = array('23');
				}
				$selectuser	= $this->_db->select()
					->from(array('C' => 'M_APP_BOUNDARY'), array(
						'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
						'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
						'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
						'C.TRANSFER_TYPE',
						'C.POLICY'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.TRANSFER_TYPE 	IN (?) ",  $transfertype);
				// ->where("C.BOUNDARY_MIN 	<= ?" , $pslip['amount'])
				// ->where("C.BOUNDARY_MAX 	>= ?" , $pslip['amount']);
			} else {
				$selectuser	= $this->_db->select()
					->from(array('C' => 'M_APP_BOUNDARY'), array(
						'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
						'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
						'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
						'C.TRANSFER_TYPE',
						'C.POLICY'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.TRANSFER_TYPE 	= ?", (string) $PS_TYPE)
					->where("C.BOUNDARY_MIN 	<= ?", $pslip['amount'])
					->where("C.BOUNDARY_MAX 	>= ?", $pslip['amount']);
			}

			// echo $selectuser;
			$datauser = $this->_db->fetchAll($selectuser);

			// print_r($datauser);die;
			$this->view->datauserdata = $datauser;
		}
		if (!empty($pslip)) {
			// Payment Status is not Waiting Approval
			if ($pslip["PS_STATUS"] != $this->_paymentstatus["code"]["waitingforapproval"]) {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error') . ": " . $this->language->_('Payment Status has changed') . ".");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		} else	// ps_number is invalid, or not belong to customer, or user don't have right to view this payment
		{
			// print_r('asgasgas');die();

			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
			$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
		}


		if ($reject) {
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviRejectPayment) {
				$PS_REASON = $filter->filter($this->_getParam('PS_REASON'), "MESSAGE");
				$Payment->rejectPayment($PS_REASON);
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to reject payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		if ($repair) {
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviRepairPayment) {
				$PS_REASON = $filter->filter($this->_getParam('PS_REASON'), "MESSAGE");
				$Payment->requestRepair($PS_REASON);
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to request repair payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		$filter->__destruct();
		unset($filter);

		// View Data
		$this->_tableMst[0]["label"] = $this->language->_('Payment Ref') . '#';
		$this->_tableMst[1]["label"] = $this->language->_('Created Date');
		$this->_tableMst[2]["label"] = $this->language->_('Updated Date');
		$this->_tableMst[3]["label"] = $this->language->_('Payment Date');
		$this->_tableMst[4]["label"] = $this->language->_('Payment Subject');
		$this->_tableMst[5]["label"] = $this->language->_('Master Account');
		$this->_tableMst[6]["label"] = $this->language->_('Payment Type');

		$this->_tableMst[0]["value"] = $PS_NUMBER;
		$this->_tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->_dateTimeDisplayFormat);
		$this->_tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->_dateTimeDisplayFormat);
		$this->_tableMst[3]["value"] = Application_Helper_General::convertDate($pslip['efdate'], $this->_dateViewFormat);
		$this->_tableMst[4]["value"] = $pslip['paySubj'];
		$this->_tableMst[5]["value"] = "";
		if (!empty($pslipTrxData) && ($pslipTrxData['0']['TRANSFER_TYPE'] == '8' || $pslipTrxData['0']['TRANSFER_TYPE'] == '7')) {

			$pslip['payType'] = $pslipTrxData['0']['TRANSFER_TYPE_disp'];
		}
		$this->_tableMst[6]["value"] = $pslip['payType'];

		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && !$pdf) {
			// download trx bulk file
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
		}
		// 		print_r($pslip["PS_TYPE"]);
		// 						print_r($this->_paymenttype["code"]);
		// separate credit and debet view
		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"]) {
			$this->sweepin($pslip);
		} elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"]) {
			$this->sweepout($pslip);
		} elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]) {
			// 		    echo 'here';
			$this->debet($pslip);
		} elseif ($pslip["PS_TYPE"] == '17' && ($pslip["PS_BILLER_ID"] == '1158' || $pslip["PS_BILLER_ID"] == '1156')) {
			$this->etax($pslip);
		} elseif ($pslip["PS_TYPE"] == '18') {
			$this->sp2d($pslip);
		} elseif ($pslip["PS_TYPE"] == '19') {
			// 		    echo 'here';
			$this->sweepin($pslip);
		} elseif ($pslip["PS_TYPE"] == '20') {
			// 		    echo 'here';
			$this->sweepout($pslip);
		}elseif ($pslip["PS_TYPE"] == '23') {
			// 		    echo 'here';
			$this->sweepout($pslip);
		} else {
			$this->credit($pslip);
		}
		// 		die;
		// approve after validate payment	
		// var_dump($approve);
		// var_dump($this->view->valError);die;
		if ($approve && $this->view->valError === false) {
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviApprovePayment) {

				$Payment->approvePayment();
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to approve payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT") {
			$this->view->fields			 = array();
			$this->view->tableDtl 		 = array();
			$this->view->TITLE_DTL		 = "";
		}
		if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '6' || $pslip['PS_TYPE'] == '7' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '11' || $pslip['PS_TYPE'] == '16' || $pslip['PS_TYPE'] == '17') {
			$this->view->repairbtn = 'hidden';
		}
		//print_r($pslip);die;
		$this->view->pslip 			= $pslip;
		$this->view->PS_NUMBER 			= $PS_NUMBER;
		$this->view->tableMst 			= $this->_tableMst;
		$this->view->totalTrx 			= $pslip["numtrx"];

		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"]) {
			// 			print_r($pslip);die;

			$select	= $this->_db->select()
				// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
				->from(array('TTS' => 'T_PERIODIC'))
				->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
				->where('TTS.PS_PERIODIC = ?', $pslip['PERIODIC']);
			//echo $select;
			$sweepdetail = $this->_db->fetchAll($select);
			//die('here');
			//print_r($sweepdetail);die;
			//			if($pslip['BALANCE_TYPE']=='2'){
			$totalamount = 0;
			$temptotal = 0;
			foreach ($sweepdetail as $key => $value) {
				$totalamount = $temptotal + $value['TRA_REMAIN'];
				$temptotal = $totalamount;
			}
			// 				echo '<pre>';
			//				print_r($temptotal);die;
			$this->view->totalAmt 			= $temptotal;
			//			}else{
			//				$this->view->totalAmt 			= $pslip["amount"];
			//			}

		} else {
			//print_r($pslip);die;
			if ($pslip['PS_TYPE'] == '3' && $pslip['acbenef_ccy'] == 'USD' && $pslip['accsrc_ccy'] == 'USD') {
				$this->view->totalAmt 			= $pslip["EQUIVALEN_IDR"];
				$this->view->ps_ccy 			= $pslip["ccy"];
			} else if ($pslip['PS_TYPE'] == '3') {
				$this->view->totalAmt 			= $pslip["EQUIVALEN_IDR"];
				$this->view->ps_ccy 			= 'IDR';
			} else {
				$this->view->totalAmt 			= $pslip["amount"];
				$this->view->ps_ccy 			= $pslip["ccy"];
			}
		}
		$this->view->allowRepair 	 	= ($this->_hasPriviRepairPayment && $pslip["PS_CATEGORY"] != "BULK PAYMENT" && $pslip["PS_BILLER_ID"] != "1156" && $pslip["PS_BILLER_ID"] != "1158");
		$this->view->allowReject 	 	= $this->_hasPriviRejectPayment;
		$this->view->pdf 				= ($pdf) ? true : false;
		$this->view->payReff 			= $this->_getParam('payReff');


		if ($pdf) {
			$outputHTML = "<tr><td>" . $this->view->render($this->view->controllername . '/index.phtml') . "</td></tr>";
			$this->_helper->download->pdf(null, null, null, 'Approval', $outputHTML);
		}
	}

	public function oldAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$filter 			= new Application_Filtering();
		$PS_NUMBER 			= $filter->filter($this->_getParam('payReff'), "PS_NUMBER");
		$pdf 				= $filter->filter($this->_getParam('pdf'), "BUTTON");
		$process 			= $filter->filter($this->_getParam('process'), "BUTTON");
		$approve 			= ($process == "approve") 	? true : false;
		$repair 			= ($process == "repair") 	? true : false;
		$reject 			= ($process == "reject") 	? true : false;
		$this->_paymentRef 	= $PS_NUMBER;

		$sessionNamespace = new Zend_Session_Namespace('URL_CP_WA');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ?
			$sessionNamespace->URL : '/' . $this->view->modulename . '/' . $this->_controllerList . '/index';

		// check privi...
		$this->_hasPriviApprovePayment 	= $this->view->hasPrivilege('PAPV');
		$this->_hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');
		$this->_hasPriviRepairPayment 	= $this->view->hasPrivilege('PRRP');
		//$this->_hasPriviRepairPayment 	= $this->view->hasPrivilege('PRPP');
		$this->_hasPriviRejectPayment 	= $this->view->hasPrivilege('PRJT');

		$paramPayment = array(
			"WA" 				=> true,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);
		// echo $select;die;
		$pslip = $this->_db->fetchRow($select);



		$selectapprover	= $this->_db->select()
			->from(array('C' => 'T_APPROVAL'), array(
				'USER_ID'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
			->where("C.PS_NUMBER = ?", (string) $pslip['PS_NUMBER']);
		// ->where("C.GROUP = ?" , (string)$value);
		// echo $selectapprover;die;
		$usergroup = $this->_db->fetchAll($selectapprover);
		// print_r($usergroup);die;
		if (!empty($usergroup)) {
			// die;
			// $this->view->pdf = true;
			$this->view->validbtn = false;
		} else {
			if (!empty($pslip)) {
				$boundary = $this->validatebtn($pslip['PS_TYPE'], $pslip['amount'], $pslip['ccy'], $pslip['PS_NUMBER']);
			}

			// print_r($boundary);die;
			if ($boundary) {
				// die;
				$this->view->validbtn = false;
			} else {
				// die;
				$this->view->validbtn = true;
			}
		}

		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					// 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $this->_bankName . "'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN B.BENEFICIARY_ISAPPROVE = '0' THEN 'Waiting Approval'
																				 WHEN B.BENEFICIARY_ISAPPROVE = '1' THEN 'Approved'
																				 ELSE '-'
																			END"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		// echo $selectTrx;die;
		// 							print_r($select->query());die;
		$pslipTrxData = $this->_db->fetchAll($selectTrx);
		// print_r($pslipTrxData);die;

		if (!empty($pslip)) {
			if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5' || $pslip['PS_TYPE'] == '11') {
				$PS_TYPE = '18';
			} else {
				$PS_TYPE = $pslip['PS_TYPE'];
			}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $PS_TYPE)
				->where("C.BOUNDARY_MIN 	<= ?", $pslip['amount'])
				->where("C.BOUNDARY_MAX 	>= ?", $pslip['amount']);
			// echo $selectuser;
			$datauser = $this->_db->fetchAll($selectuser);

			// print_r($datauser);die;
			$this->view->datauserdata = $datauser;
		}
		if (!empty($pslip)) {
			// Payment Status is not Waiting Approval
			if ($pslip["PS_STATUS"] != $this->_paymentstatus["code"]["waitingforapproval"]) {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error') . ": " . $this->language->_('Payment Status has changed') . ".");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		} else	// ps_number is invalid, or not belong to customer, or user don't have right to view this payment
		{
			// print_r('asgasgas124124124');die();
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
			// $this->_redirect('/'.$this->view->modulename.'/'.$this->_controllerList.'/index');
		}


		if ($reject) {
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviRejectPayment) {
				$PS_REASON = $filter->filter($this->_getParam('PS_REASON'), "MESSAGE");
				$Payment->rejectPayment($PS_REASON);
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to reject payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		if ($repair) {
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviRepairPayment) {
				$PS_REASON = $filter->filter($this->_getParam('PS_REASON'), "MESSAGE");
				$Payment->requestRepair($PS_REASON);
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to request repair payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		$filter->__destruct();
		unset($filter);

		// View Data
		$this->_tableMst[0]["label"] = $this->language->_('Payment Ref') . '#';
		$this->_tableMst[1]["label"] = $this->language->_('Created Date');
		$this->_tableMst[2]["label"] = $this->language->_('Updated Date');
		$this->_tableMst[3]["label"] = $this->language->_('Payment Date');
		$this->_tableMst[4]["label"] = $this->language->_('Payment Subject');
		$this->_tableMst[5]["label"] = $this->language->_('Master Account');
		$this->_tableMst[6]["label"] = $this->language->_('Payment Type');

		$this->_tableMst[0]["value"] = $PS_NUMBER;
		$this->_tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->_dateTimeDisplayFormat);
		$this->_tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->_dateTimeDisplayFormat);
		$this->_tableMst[3]["value"] = Application_Helper_General::convertDate($pslip['efdate'], $this->_dateViewFormat);
		$this->_tableMst[4]["value"] = $pslip['paySubj'];
		$this->_tableMst[5]["value"] = "";
		if (!empty($pslipTrxData) && ($pslipTrxData['0']['TRANSFER_TYPE'] == '8' || $pslipTrxData['0']['TRANSFER_TYPE'] == '7')) {

			$pslip['payType'] = $pslipTrxData['0']['TRANSFER_TYPE_disp'];
		}
		$this->_tableMst[6]["value"] = $pslip['payType'];

		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && !$pdf) {
			// download trx bulk file
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
		}
		// 		print_r($pslip["PS_TYPE"]);
		// 						print_r($this->_paymenttype["code"]);
		// separate credit and debet view
		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"]) {
			$this->sweepin($pslip);
		} elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"]) {
			$this->sweepout($pslip);
		} elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]) {
			// 		    echo 'here';
			$this->debet($pslip);
		} else {
			$this->credit($pslip);
		}
		// 		die;
		// approve after validate payment	
		if ($approve && $this->view->valError === false) {
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviApprovePayment) {

				$Payment->approvePayment();
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = $this->view->backURL;
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to approve payment.");
				$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT") {
			$this->view->fields			 = array();
			$this->view->tableDtl 		 = array();
			$this->view->TITLE_DTL		 = "";
		}
		if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '6' || $pslip['PS_TYPE'] == '7' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '11' || $pslip['PS_TYPE'] == '16' || $pslip['PS_TYPE'] == '17') {
			$this->view->repairbtn = 'hidden';
		}
		//print_r($pslip);die;
		$this->view->pslip 			= $pslip;
		$this->view->PS_NUMBER 			= $PS_NUMBER;
		$this->view->tableMst 			= $this->_tableMst;
		$this->view->totalTrx 			= $pslip["numtrx"];

		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"]) {
			// 			print_r($pslip);die;

			$select	= $this->_db->select()
				// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
				->from(array('TTS' => 'T_PERIODIC'))
				->joinLeft(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*'))
				->where('TTS.PS_PERIODIC = ?', $pslip['PERIODIC']);
			//echo $select;
			$sweepdetail = $this->_db->fetchAll($select);
			//die('here');
			//print_r($sweepdetail);die;
			//			if($pslip['BALANCE_TYPE']=='2'){
			$totalamount = 0;
			$temptotal = 0;
			foreach ($sweepdetail as $key => $value) {
				$totalamount = $temptotal + $value['TRA_REMAIN'];
				$temptotal = $totalamount;
			}
			// 				echo '<pre>';
			//				print_r($temptotal);die;
			$this->view->totalAmt 			= $temptotal;
			//			}else{
			//				$this->view->totalAmt 			= $pslip["amount"];
			//			}

		} else {
			//print_r($pslip);die;
			if ($pslip['PS_TYPE'] == '3' && $pslip['acbenef_ccy'] == 'USD' && $pslip['accsrc_ccy'] == 'USD') {
				$this->view->totalAmt 			= $pslip["EQUIVALEN_IDR"];
				$this->view->ps_ccy 			= $pslip["ccy"];
			} else if ($pslip['PS_TYPE'] == '3') {
				$this->view->totalAmt 			= $pslip["EQUIVALEN_IDR"];
				$this->view->ps_ccy 			= 'IDR';
			} else {
				$this->view->totalAmt 			= $pslip["amount"];
				$this->view->ps_ccy 			= $pslip["ccy"];
			}
		}
		$this->view->allowRepair 	 	= ($this->_hasPriviRepairPayment && $pslip["PS_CATEGORY"] != "BULK PAYMENT");
		$this->view->allowReject 	 	= $this->_hasPriviRejectPayment;
		$this->view->pdf 				= ($pdf) ? true : false;
		$this->view->payReff 			= $this->_getParam('payReff');


		if ($pdf) {
			$outputHTML = "<tr><td>" . $this->view->render($this->view->controllername . '/index.phtml') . "</td></tr>";
			$this->_helper->download->pdf(null, null, null, 'Approval', $outputHTML);
		}
	}

	public function historyAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		$filter 			= new Application_Filtering();

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER"));
		$payreff = $AESMYSQL->decrypt($PS_NUMBER, $password);

		$PS_NUMBER = $payreff;

		$this->view->PS_NUMBER = $PS_NUMBER;
	}
	private function etax($pslip)
	{
		$PS_NUMBER = $this->_paymentRef;
		$dataEtax = json_decode($pslip['LOG']);
		// echo '<pre>';
		// var_dump($dataEtax);die;

		$this->view->dataEtax = $dataEtax;

		$this->_tableMst[4]["value"] = $dataEtax->paymentSubject;
		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		if ($pslip['PS_BILLER_ID'] == '1158') {
			//table detail
			$compulsory = array('0' => 'No', '1' => 'Yes');
			$taxType = array('0' => 'NPWP', '1' => 'Non NPWP');
			$identity = array(
				'1' => 'KTP',
				'2' => 'NPWP',
				'3' => 'SIM',
				'4' => 'PASPOR',
				'5' => 'KITAS'
			);
			// roki
			$month = array(
				$this->language->_('January'),
				$this->language->_('February'),
				$this->language->_('March'),
				$this->language->_('April'),
				$this->language->_('May'),
				$this->language->_('June'),
				$this->language->_('July'),
				$this->language->_('August'),
				$this->language->_('September'),
				$this->language->_('October'),
				$this->language->_('November'),
				$this->language->_('December')
			);
			$map_code = array();
			$depositType = array();
			$select = $this->_db->select()
				->from(array('M_MAP_CODE'), array('MAP_CODE', 'MAP_NAME'))
				->query()->fetchAll();

			foreach ($select as $key) {
				$map_code[$key['MAP_CODE']] = $key['MAP_NAME'] . " (" . $key['MAP_CODE'] . ")";
			}
			if (isset($dataEtax->akuncode)) {
				$select = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_MAP_CODE'), array('DEPOSIT_CODE', 'DEPOSIT_NAME'))
						->where('MAP_CODE = ?', $dataEtax->akuncode)
				);

				foreach ($select as $key) {
					$depositType[$key['DEPOSIT_CODE']] = $key['DEPOSIT_NAME'] . " (" . $key['DEPOSIT_CODE'] . ")";
				}
			}
			
			$tableDetail[0]["label"] = $this->language->_('Billing ID').' / '.$this->language->_('Code');
			 $tableDetail[0]["value"] = '2020032300014203';

			 $tableDetail[1]["label"] = $this->language->_('MAP Code');
			 $tableDetail[1]["value"] = $map_code[$dataEtax->akuncode];
			 
			 $tableDetail[2]["label"] = $this->language->_('KJS');
			 $tableDetail[2]["value"] = $depositType[$dataEtax->deposittype];
			 
			 $tableDetail[3]["label"] = $this->language->_('NPWP Number');
			 $tableDetail[3]["value"] = $dataEtax->asessableNpwp;

			 $tableDetail[4]["label"] = $this->language->_("Taxpayer's Name");
			 $tableDetail[4]["value"] = $dataEtax->asessablename;

			 $tableDetail[5]["label"] = $this->language->_("Taxpayer's Address");
			 $tableDetail[5]["value"] = $dataEtax->asessableaddress;

			 $tableDetail[6]["label"] = $this->language->_("Taxpayer's City");
			 $tableDetail[6]["value"] = $dataEtax->asessablecity;
			
			 $tableDetail[7]["label"] = $this->language->_('SK Number');
			 $tableDetail[7]["value"] = $dataEtax->skNumber;
			 
			 $tableDetail[8]["label"] = '';
			 $tableDetail[8]["value"] = $this->language->_('Format : Serial No / SKP Type / Tax Year / KPP Code / Release Year');
			 
			 
 			 $tableDetail[9]["label"] = $this->language->_('Tax Object Number (NOP)');
			 $tableDetail[9]["value"] = $dataEtax->taxobjectnumber;
			 
			 $tableDetail[10]["label"] = $this->language->_("Taxayer's ID eg(KTP,SIM) ");
			 $tableDetail[10]["value"] = $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;

			$tableDetail[11]["label"] = $this->language->_("Depositor's NPWP Number");
			 $tableDetail[11]["value"] = $dataEtax->payerNpwp;
			 
			 $tableDetail[12]["label"] = $this->language->_("Depositor's Name");
			 $tableDetail[12]["value"] = $dataEtax->payername;
			 
			 $tableDetail[13]["label"] = '';
			 $tableDetail[13]["value"] = '';

			 $tableDetail[14]["label"] = $this->language->_("Tax Method Period");
			 $tableDetail[14]["value"] = 'bold';
			 
			$tableDetail[15]["label"] = $this->language->_("Month");
			 $tableDetail[15]["value"] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2];
			 
			 $tableDetail[16]["label"] = $this->language->_("Year");
			 $tableDetail[16]["value"] = $dataEtax->periodic;

			// $tableDetail[0]["label"] = $this->language->_('Billing Code');
			// $tableDetail[0]["value"] = '2020032300014203';

			// $tableDetail[1]["label"] = $this->language->_('Amount');
			// $tableDetail[1]["value"] = 'IDR ' . $dataEtax->amount;

			// $tableDetail[2]["label"] = $this->language->_('Payer NPWP');
			// $tableDetail[2]["value"] = $dataEtax->payerNpwp;

			// $tableDetail[3]["label"] = $this->language->_('Payer Name');
			// $tableDetail[3]["value"] = $dataEtax->payername;

			// $tableDetail[4]["label"] = $this->language->_('Compulsory');
			// $tableDetail[4]["value"] = $compulsory[$dataEtax->chargetype] . ' ' . ($dataEtax->chargetype == '1' ? ' - (' . $dataEtax->chargeid . ')' : '');

			// $tableDetail[5]["label"] = $this->language->_('Asessable NPWP');
			// $tableDetail[5]["value"] = $dataEtax->asessableNpwp;

			// $tableDetail[6]["label"] = $this->language->_('Asessable Name');
			// $tableDetail[6]["value"] = $dataEtax->asessablename;

			// $tableDetail[7]["label"] = $this->language->_('Asessable Address');
			// $tableDetail[7]["value"] = $dataEtax->asessableaddress;

			// $tableDetail[8]["label"] = $this->language->_('Asessable City');
			// $tableDetail[8]["value"] = $dataEtax->asessablecity;

			// $tableDetail[9]["label"] = $this->language->_('Asessable Identity');
			// $tableDetail[9]["value"] = $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;

			// $tableDetail[10]["label"] = $this->language->_('Map / Akun Code');
			// $tableDetail[10]["value"] = $map_code[$dataEtax->akuncode];

			// $tableDetail[11]["label"] = $this->language->_('Deposit Type');
			// $tableDetail[11]["value"] = $depositType[$dataEtax->deposittype];

			// $tableDetail[12]["label"] = $this->language->_('Tax Object Number (NOP)');
			// $tableDetail[12]["value"] = $dataEtax->taxobjectnumber;

			// $tableDetail[13]["label"] = $this->language->_('SK Number');
			// $tableDetail[13]["value"] = $dataEtax->skNumber;

			// $tableDetail[14]["label"] = $this->language->_('Remark');
			// $tableDetail[14]["value"] = $dataEtax->remark;

			// $tableDetail[15]["label"] = $this->language->_('Tax Period  Payment');
			// $tableDetail[15]["value"] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2] . ' ' . $dataEtax->periodic;
		} elseif ($pslip['PS_BILLER_ID'] == '1156') {
			// unset($this->_tableMst[4]); // remove array key payment subject

			// $tableDetail[0]["label"] = $this->language->_('Billing Code');
			// $tableDetail[0]["value"] = $dataEtax->orderId;

			// $tableDetail[1]["label"] = $this->language->_('Amount');
			// $tableDetail[1]["value"] = 'IDR ' . Application_Helper_General::displayMoney($dataEtax->amount);

			// $billingPrefix = substr($dataEtax->orderId, 0, 1);

			// if ($billingPrefix == '0' || $billingPrefix == '1' || $billingPrefix == '2' || $billingPrefix == '3') {

			// 	$tableDetail[2]["label"] = $this->language->_('NPWP');
			// 	$tableDetail[2]["value"] = $dataEtax->dataUi->npwp;

			// 	$tableDetail[3]["label"] = $this->language->_('Customer Name');
			// 	$tableDetail[3]["value"] = $dataEtax->dataUi->customer_name;

			// 	$tableDetail[4]["label"] = $this->language->_('Customer Address');
			// 	$tableDetail[4]["value"] = $dataEtax->dataUi->customer_address;

			// 	$tableDetail[5]["label"] = $this->language->_('Map / Akun Code');
			// 	$tableDetail[5]["value"] = $dataEtax->dataUi->account_map;

			// 	$tableDetail[6]["label"] = $this->language->_('Deposit Type');
			// 	$tableDetail[6]["value"] = $dataEtax->dataUi->type;

			// 	$tableDetail[7]["label"] = $this->language->_('Tax Object Number (NOP)');
			// 	$tableDetail[7]["value"] = $dataEtax->dataUi->NOP;

			// 	$tableDetail[8]["label"] = $this->language->_('SK Number');
			// 	$tableDetail[8]["value"] = $dataEtax->dataUi->sk_number;

			// 	$tableDetail[9]["label"] = $this->language->_('Tax Period Payment');
			// 	$tableDetail[9]["value"] = $dataEtax->dataUi->period;
			// } elseif ($billingPrefix == '4' || $billingPrefix == '5' || $billingPrefix == '6') {

			// 	$tableDetail[2]["label"] = $this->language->_('Customer Name');
			// 	$tableDetail[2]["value"] = $dataEtax->dataUi->customer_name;

			// 	$tableDetail[3]["label"] = $this->language->_('ID Type Customer');
			// 	$tableDetail[3]["value"] = $dataEtax->dataUi->customer_id;

			// 	$tableDetail[4]["label"] = $this->language->_('Document Type');
			// 	$tableDetail[4]["value"] = $dataEtax->dataUi->document_type;

			// 	$tableDetail[5]["label"] = $this->language->_('Document Number');
			// 	$tableDetail[5]["value"] = $dataEtax->dataUi->document_number;

			// 	$tableDetail[6]["label"] = $this->language->_('Document Date');
			// 	$tableDetail[6]["value"] = $dataEtax->dataUi->document_date;

			// 	$tableDetail[7]["label"] = $this->language->_('KPBC Code');
			// 	$tableDetail[7]["value"] = $dataEtax->dataUi->kppbc_code;
			// } elseif ($billingPrefix == '7' || $billingPrefix == '8' || $billingPrefix == '9') {

			// 	$tableDetail[2]["label"] = $this->language->_('Customer Name');
			// 	$tableDetail[2]["value"] = $dataEtax->dataUi->customer_name;

			// 	$tableDetail[3]["label"] = $this->language->_('K/L');
			// 	$tableDetail[3]["value"] = $dataEtax->dataUi->k_l;

			// 	$tableDetail[4]["label"] = $this->language->_('Echelon Unit 1');
			// 	$tableDetail[4]["value"] = $dataEtax->dataUi->eselon_unit;

			// 	$tableDetail[5]["label"] = $this->language->_('Code Unit');
			// 	$tableDetail[5]["value"] = $dataEtax->dataUi->work_unit;
			// }
			// //adons Type of Tax First row
			// $typeOfTax = $this->_db->fetchRow(
			// 	$this->_db->select()
			// 		->from(array('M_SERVICE_PROVIDER'), array('PROVIDER_NAME'))
			// 		->where('PROVIDER_ID = ?', $pslip['PS_BILLER_ID'])
			// );
			// $tableTypeofTax[0]["label"] = $this->language->_('Type of Tax');
			// $tableTypeofTax[0]["value"] = $typeOfTax['PROVIDER_NAME'];

			// $tableDetail = array_merge($tableTypeofTax, $tableDetail);
		}
		 $this->view->tableDetail = $tableDetail;
		/////
		$fields = array(
			"operatorNama" 	=> $this->language->_('Type Of Tax'),
			"orderId" 		=> $this->language->_('Billing Code'),
			"TRA_AMOUNT"	=> $this->language->_('Amount'),						
		);

		if ($dataEtax->taxType == 'DJP'){
			$fields['npwp']				= $this->language->_('NPWP');
			$fields['customer_name']	= $this->language->_('Customer Name');
			$fields['customer_address']	= $this->language->_('Customer Address');
			$fields['account_map']		= $this->language->_('Map / Akun Code');
			$fields['type']				= $this->language->_('Deposit Type');
			$fields['nop']				= $this->language->_('Tax Object Number (NOP)');
			$fields['sk_number']		= $this->language->_('SK Number');
			$fields['period']			= $this->language->_('Tax Period Payment');
		}elseif ($dataEtax->taxType == 'DJBC'){
			$fields['customer_name']	= $this->language->_('Customer Name');
			$fields['customer_id']		= $this->language->_('ID Type Customer');
			$fields['document_type']	= $this->language->_('Document Type');
			$fields['document_number']	= $this->language->_('Document Number');
			$fields['document_date']	= $this->language->_('Document Date');
			$fields['kppbc_code']		= $this->language->_('KPBC Code');
		}elseif ($dataEtax->taxType == 'DJA'){
			$fields['customer_name']	= $this->language->_('Customer Name');
			$fields['k_l']				= $this->language->_('K/L');
			$fields['eselon_unit']		= $this->language->_('Echelon Unit 1');
			$fields['work_unit']		= $this->language->_('Code Unit');			
		}

		// Table Detail Header
		// $fields = array(
		// 	"orderId" 		=> $this->language->_('Billing Code'),
		// 	"TRA_AMOUNT"		=> $this->language->_('Amount'),
		// 	"payerNpwp"		=> $this->language->_('Payer NPWP'),
		// 	"payername"		=> $this->language->_('Payer Name'),
		// 	"compulsory"		=> $this->language->_('Compulsory'),
		// 	"asessableNpwp"		=> $this->language->_('Asessable NPWP'),
		// 	"asessablename"			=> $this->language->_('Asessable Name'),
		// 	"asessableaddress"		=> $this->language->_('Asessable Address'),			
		// 	"asessablecity" 	=> $this->language->_('Asessable City'),			 
		// 	"asessableidentity"	=> $this->language->_('Asessable Identity'),
		// 	"map_code"			=> $this->language->_('Map / Akun Code'),
		// 	"depositType"		=> $this->language->_('Deposit Type'),						
		// 	"taxobjectnumber"	=> $this->language->_('Tax Object Number (NOP'),			
		// 	"skNumber"			=> $this->language->_('SK Number'),			
		// 	"remark"			=> $this->language->_('Remark'),			
		// 	"tax_period_payment"	=> $this->language->_('Tax Period  Payment'),			
		// );		
		
		$fieldsdetail = array(
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			"TOTAL_CHARGES"  	=> $this->language->_('Fee'),
			"PS_TOTAL_AMOUNT"	=> $this->language->_('Total Amount')
	   	);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];
		
		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		$tableDtl = array();

		$app = Zend_Registry::get('config');
		$appBankname = $app['app']['bankname'];

		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'BENEFICIARY_BANK_CITY',
					'BENEFICIARY_ALIAS_NAME',
					'BENEFICIARY_ADDRESS',
					'BENEFICIARY_ADDRESS2',
					'BENEFICIARY_ID_TYPE',
					'BENEFICIARY_ID_NUMBER',
					'BENEFICIARY_CITIZENSHIP' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CITIZENSHIP = 'W' THEN 'WNI'
																				 WHEN TT.BENEFICIARY_CITIZENSHIP = 'R' THEN 'WNA'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_RESIDENT' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_RESIDENT = 'R' THEN 'Residence'
																				 WHEN TT.BENEFICIARY_RESIDENT = 'W' THEN 'Non REsidence'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_CATEGORY' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CATEGORY = '1' THEN 'Individual'
																				 WHEN TT.BENEFICIARY_CATEGORY = '2' THEN 'Government'
																				 WHEN TT.BENEFICIARY_CATEGORY = '3' THEN 'Bank'
																				 WHEN TT.BENEFICIARY_CATEGORY = '4' THEN 'Non Bank Financial Institution'
																				 WHEN TT.BENEFICIARY_CATEGORY = '5' THEN 'Company'
																				 WHEN TT.BENEFICIARY_CATEGORY = '6' THEN 'Other'
																				 ELSE '-'
																			END"),

					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					'TOTAL_CHARGES'			=> 'TT.TOTAL_CHARGES',
					'TT.RATE',
					'TT.PROVISION_FEE',
					'TT.NOSTRO_NAME',
					'TT.FULL_AMOUNT_FEE',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',					
					'LLD_TRANSACTION_PURPOSE'	=> 'TT.LLD_TRANSACTION_PURPOSE',
					'LLD_DESC'				=> 'TT.LLD_DESC',
					'C.CUST_ID', 'TT.SOURCE_ACCOUNT_CCY',
					'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
					'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			 WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN C.PS_STATUS = '1' THEN 'Waiting Approval'
																				 WHEN C.PS_STATUS = '2' THEN 'Approved'
																				 ELSE '-'
																			END"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo $selectTrx;die;
		$pslipTrx = $this->_db->fetchAll($selectTrx);
		
		// $this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		// $this->view->addmessage = $pslipTrx['0']['TRA_ADDMESSAGE'];
		// echo "<pre>";
		// 	print_r($pslipTrx);die;

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }


		foreach ($pslipTrx as $p => $pTrx) {
			// Create array bene for validation
			if (!empty($pTrx["ACBENEF_ID"])) {
				$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
			}

			$trfType = $pTrx["TRANSFER_TYPE"];

			$psCategory = $pslip['PS_CATEGORY'];

			// create table detail data
			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];
				
				if ($key == 'ACBENEF' && ($pTrx['TRANSFER_TYPE'] == '10' || $pTrx['TRANSFER_TYPE'] == '9')) {
					$value = '';
				}
				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN" || $key == "PROVISION_FEE") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['PS_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == "TRANSFER_FEE" && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {

					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;
					$trffee = $this->_db->fetchRow($selecttrffee);

					if ($value != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					}
				}
				if ($key == "FULL_AMOUNT_FEE" && !empty($pTrx['ACBENEF_CCY'])) {

					$selecttrfFA = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '4')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['ACBENEF_CCY']);
					// echo $selecttrfFA;die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);
					if ($pTrx['TRANSFER_TYPE'] == '10') {
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else if ($pTrx['TRANSFER_TYPE'] == '9') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID']);
						// echo $selecttrfFA;die;
						$trfFA = $this->_db->fetchRow($selecttrfFA);
					}
					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);
				}

				if ($key == 'PROVISION_FEE' && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {
					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['TRANSFER_TYPE_disp'] == 'PB') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							$value = $pTrx['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']) . ' (IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']) . ')';
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != '-') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}

				if ($key == "TRANSFER_TYPE_disp") {
					if ($value == 'PB') {
						$value = 'In House';
					}
				}

				if ($key == "BENEFICIARY_ID_NUMBER") {
					$value = $pTrx['BENEFICIARY_ID_TYPE'].' '.$pTrx['BENEFICIARY_ID_NUMBER'];
				}

				if ($key == "SP2D_DATE" || $key == "SPM_DATE") {
					$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);
				}

				if ($key == "LLD_DESC") {
					$value = nl2br($value);
				}

				$value = ($value == "") ? "-" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if (!empty($pslip['BANK_CODE'])) {
				$bankcode = $pslip['BANK_CODE'];
			} else {
				$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
			}

			if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
				$bankname = '-';
			} else if (empty($bankcode)) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$bankcode];
			}

			$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
			
			$tableDtl[$p]['operatorNama']		= $dataEtax->operatorNama;
			$tableDtl[$p]['orderId']			= $dataEtax->orderId;

			//DJP
			$tableDtl[$p]['npwp']				= $dataEtax->dataUi->npwp;
			$tableDtl[$p]['customer_name']		= $dataEtax->dataUi->customer_name;
			$tableDtl[$p]['customer_address']	= $dataEtax->dataUi->customer_address;			
			$tableDtl[$p]['account_map']		= $dataEtax->dataUi->account_map;
			$tableDtl[$p]['type']				= $dataEtax->dataUi->type;
			$tableDtl[$p]['nop']				= $dataEtax->dataUi->NOP;
			$tableDtl[$p]['sk_number']			= $dataEtax->dataUi->sk_number;
			$tableDtl[$p]['period']				= $dataEtax->dataUi->period;
			
			//DJBC
			$tableDtl[$p]['customer_name']		= $dataEtax->dataUi->customer_name;
			$tableDtl[$p]['customer_id']		= $dataEtax->dataUi->customer_id;
			$tableDtl[$p]['document_type']		= $dataEtax->dataUi->document_type;
			$tableDtl[$p]['document_number']	= $dataEtax->dataUi->document_number;
			$tableDtl[$p]['document_date']		= $dataEtax->dataUi->document_date;
			$tableDtl[$p]['kppbc_code']			= $dataEtax->dataUi->kppbc_code;

			//DJA
			$tableDtl[$p]['customer_name']		= $dataEtax->dataUi->customer_name;
			$tableDtl[$p]['k_l']				= $dataEtax->dataUi->k_l;
			$tableDtl[$p]['eselon_unit']		= $dataEtax->dataUi->eselon_unit;
			$tableDtl[$p]['work_unit']			= $dataEtax->dataUi->work_unit;
			
			// $tableDtl[$p]['payerNpwp']			= $dataEtax->payerNpwp;
			// $tableDtl[$p]['payername']			= $dataEtax->payername;			
			// $tableDtl[$p]['asessableNpwp']		= $dataEtax->asessableNpwp;
			// $tableDtl[$p]['asessablename']		= $dataEtax->asessablename;
			// $tableDtl[$p]['asessableaddress']	= $dataEtax->asessableaddress;
			// $tableDtl[$p]['asessablecity']		= $dataEtax->asessablecity;
			// $tableDtl[$p]['taxobjectnumber']	= $dataEtax->taxobjectnumber;
			// $tableDtl[$p]['skNumber']			= $dataEtax->skNumber;
			// $tableDtl[$p]['remark']				= $dataEtax->remark;
			// $tableDtl[$p]['compulsory']			= $compulsory[$dataEtax->chargetype] . ' ' . ($dataEtax->chargetype == '1' ? ' - (' . $dataEtax->chargeid . ')' : '');
			// $tableDtl[$p]['asessableidentity']	= $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;
			// $tableDtl[$p]['map_code'] 			= $map_code[$dataEtax->akuncode];
			// $tableDtl[$p]['depositType'] 		= $depositType[$dataEtax->deposittype];
			// $tableDtl[$p]['tax_period_payment'] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2] . ' ' . $dataEtax->periodic;
			
			$tableDtl[$p]['TRA_AMOUNT']  	= Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
			$tableDtl[$p]['TOTAL_CHARGES'] 	=  Application_Helper_General::displayMoney($pTrx['TOTAL_CHARGES']);
			$totalAmount = $pTrx['TRA_AMOUNT'] - $pTrx['TOTAL_CHARGES'];
			$tableDtl[$p]['PS_TOTAL_AMOUNT'] 	= Application_Helper_General::displayMoney($totalAmount);			
		}

		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array(
			"FROM" 				=> "D",						// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);

		$validate->checkApprove($paramApprove);

		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment

		$config = Zend_Registry::get('config');
		$paystatusarr = array_combine(array_values($config['payment']['status']['code']),array_values($config['payment']['status']['desc']));
			//var_dump();die;
  		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";


		$select	= $this->_db->select()
			->from(
				array('TP' => 'T_PSLIP'),
				array(
					'REF_ID'					=> 'TP.REFF_ID',
					'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
					'EFTIME'					=> 'TP.PS_EFTIME',
					'REMAIN'					=> 'TP.PS_REMAIN',
					'STATUS'					=> $casePayStatus,
					'TRANS'						=> 'TP.PS_TXCOUNT'
				)
			)
			->where('TP.PS_NUMBER = ?', $PS_NUMBER);
			//echo $select;die;
		$pslipTrx = $this->_db->fetchRow($select);
		
		if($pslipTrx['TRANS'] > 1){
			//$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$this->view->downloadurl = $downloadURL;
			$this->view->trans = $pslipTrx['TRANS'];
		}
		$this->view->paystatus = $pslipTrx['STATUS'].' - Untransfered';

		$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		$this->view->frequen 		= '1x';
		$this->view->fields 		= $fields;
		$this->view->fieldsdetail	= $fieldsdetail;
		$this->view->tableDtl 		= $tableDtl;
		$this->view->TITLE_MST		= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		= $this->language->_('Transaction');
	}
	private function sp2d($pslip)
	{
		// echo '<pre>';
		// var_dump($pslip);
		// die;
		$PS_NUMBER = $this->_paymentRef;

		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		$tableDetail[0]["label"] = $this->language->_('No SP2D');
		$tableDetail[0]["value"] = $pslip['SP2D_NO'];

		$tableDetail[1]["label"] = $this->language->_('SP2D Date');
		$tableDetail[1]["value"] = Application_Helper_General::convertDate($pslip['SP2D_DATE'], $this->_dateViewFormat);

		$tableDetail[2]["label"] = $this->language->_('No SPM');
		$tableDetail[2]["value"] = $pslip['SPM_NO'];

		$tableDetail[3]["label"] = $this->language->_('SPM Date');
		$tableDetail[3]["value"] = Application_Helper_General::convertDate($pslip['SPM_DATE'], $this->_dateViewFormat);

		$tableDetail[4]["label"] = $this->language->_('SKPD Name');
		$tableDetail[4]["value"] = $pslip['SKPD_NAME'];

		$tableDetail[5]["label"] = $this->language->_('SPP Type');
		$tableDetail[5]["value"] = $pslip['SPP_TYPE'];

		$tableDetail[6]["label"] = $this->language->_('Beneficiary Account Name');
		$tableDetail[6]["value"] = $pslip['BENEFICIARY_ACCOUNT_NAME'];

		$tableDetail[7]["label"] = $this->language->_('Beneficiary Account Number');
		$tableDetail[7]["value"] = $pslip['BENEFICIARY_ACCOUNT'];

		$tableDetail[8]["label"] = $this->language->_('Beneficiary Identity');
		$tableDetail[8]["value"] = $pslip['BENEFICIARY_ID_TYPE'].' '.$pslip['BENEFICIARY_ID_NUMBER'];

		$tableDetail[9]["label"] = $this->language->_('Transaction Purpose');
		$tableDetail[9]["value"] = $pslip['LLD_TRANSACTION_PURPOSE'];

		$tableDetail[10]["label"] = $this->language->_('Description');
		$tableDetail[10]["value"] = nl2br($pslip['LLD_DESC']);

		$tableDetail[11]["label"] = $this->language->_('Actual Amount');
		$tableDetail[11]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']);

		$tableDetail[12]["label"] = $this->language->_('Deduction Amount');
		$tableDetail[12]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['TOTAL_CHARGES']);

		$tableDetail[13]["label"] = $this->language->_('Total Amount');
		$tableDetail[13]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['PS_TOTAL_AMOUNT']);

		$this->view->tableDetail = $tableDetail;
		/////

		// Table Detail Header
		$fields = array(
			"SP2D_NO" 		=> $this->language->_('No SP2D'),
			"SP2D_DATE"		=> $this->language->_('SP2D Date'),
			"SPM_NO"		=> $this->language->_('No SPM'),
			"SPM_DATE"		=> $this->language->_('SPM Date'),
			"SKPD_NAME"		=> $this->language->_('SKPD Name'),
			"SPP_TYPE"		=> $this->language->_('SPP Type'),
			"ACBENEF"			=> $this->language->_('Beneficiary Account'),
			"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),			
			"BENEFICIARY_ID_NUMBER" 	=> $this->language->_('Beneficiary Identity'),			 
			"LLD_TRANSACTION_PURPOSE"	=> $this->language->_('Transaction Purpose'),
			"LLD_DESC"			=> $this->language->_('Description')			
		);		
		
		$fieldsdetail = array(
			 "TRA_AMOUNT"  	   	=> $this->language->_('Actual Amount'),
			 "TOTAL_CHARGES"  	=> $this->language->_('Deduction Amount'),
			 "PS_TOTAL_AMOUNT"	=> $this->language->_('Total Amount')
		);	

		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		$ACBENEF_IDarr = array();
		// $ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];
		$tableDtl = array();

		$app = Zend_Registry::get('config');
		$appBankname = $app['app']['bankname'];

		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'BENEFICIARY_BANK_CITY',
					'BENEFICIARY_ALIAS_NAME',
					'BENEFICIARY_ADDRESS',
					'BENEFICIARY_ADDRESS2',
					'BENEFICIARY_ID_TYPE',
					'BENEFICIARY_ID_NUMBER',
					'BENEFICIARY_CITIZENSHIP' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CITIZENSHIP = 'W' THEN 'WNI'
																				 WHEN TT.BENEFICIARY_CITIZENSHIP = 'R' THEN 'WNA'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_RESIDENT' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_RESIDENT = 'R' THEN 'Residence'
																				 WHEN TT.BENEFICIARY_RESIDENT = 'W' THEN 'Non REsidence'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_CATEGORY' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CATEGORY = '1' THEN 'Individual'
																				 WHEN TT.BENEFICIARY_CATEGORY = '2' THEN 'Government'
																				 WHEN TT.BENEFICIARY_CATEGORY = '3' THEN 'Bank'
																				 WHEN TT.BENEFICIARY_CATEGORY = '4' THEN 'Non Bank Financial Institution'
																				 WHEN TT.BENEFICIARY_CATEGORY = '5' THEN 'Company'
																				 WHEN TT.BENEFICIARY_CATEGORY = '6' THEN 'Other'
																				 ELSE '-'
																			END"),

					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					'TOTAL_CHARGES'			=> 'TT.TOTAL_CHARGES',
					'TT.RATE',
					'TT.PROVISION_FEE',
					'TT.NOSTRO_NAME',
					'TT.FULL_AMOUNT_FEE',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'SP2D_NO'				=> 'TT.SP2D_NO',
					'SP2D_DATE'				=> 'TT.SP2D_DATE',
					'SPM_NO'				=> 'TT.SPM_NO',
					'SPM_DATE'				=> 'TT.SPM_DATE',
					'SKPD_NAME'				=> 'TT.SKPD_NAME',
					'SPP_TYPE'				=> 'TT.SPP_TYPE',
					'LLD_TRANSACTION_PURPOSE'	=> 'TT.LLD_TRANSACTION_PURPOSE',
					'LLD_DESC'				=> 'TT.LLD_DESC',
					'C.CUST_ID', 'TT.SOURCE_ACCOUNT_CCY',
					'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
					'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			 WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN C.PS_STATUS = '1' THEN 'Waiting Approval'
																				 WHEN C.PS_STATUS = '2' THEN 'Approved'
																				 ELSE '-'
																			END"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo $selectTrx;die;
		$pslipTrx = $this->_db->fetchAll($selectTrx);
		
		$this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		$this->view->addmessage = $pslipTrx['0']['TRA_ADDMESSAGE'];
		// echo "<pre>";
		// 	print_r($pslipTrx);die;

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }


		foreach ($pslipTrx as $p => $pTrx) {
			// Create array bene for validation
			if (!empty($pTrx["ACBENEF_ID"])) {
				$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
			}

			$trfType = $pTrx["TRANSFER_TYPE"];

			$psCategory = $pslip['PS_CATEGORY'];

			// create table detail data
			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];

				/* if ($key == "TRANSFER_FEE")
				{
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);	
				} */
				/*
				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}
				elseif ($key == "ACBENEF")
				{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
				*/
				if ($key == 'ACBENEF' && ($pTrx['TRANSFER_TYPE'] == '10' || $pTrx['TRANSFER_TYPE'] == '9')) {
					$value = '';
				}
				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN" || $key == "PROVISION_FEE") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['PS_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == "TRANSFER_FEE" && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {

					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;
					$trffee = $this->_db->fetchRow($selecttrffee);

					if ($value != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					}
				}
				if ($key == "FULL_AMOUNT_FEE" && !empty($pTrx['ACBENEF_CCY'])) {

					$selecttrfFA = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '4')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['ACBENEF_CCY']);
					// echo $selecttrfFA;die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);
					if ($pTrx['TRANSFER_TYPE'] == '10') {
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else if ($pTrx['TRANSFER_TYPE'] == '9') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID']);
						// echo $selecttrfFA;die;
						$trfFA = $this->_db->fetchRow($selecttrfFA);
					}
					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);
				}

				if ($key == 'PROVISION_FEE' && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {
					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['TRANSFER_TYPE_disp'] == 'PB') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							$value = $pTrx['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']) . ' (IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']) . ')';
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != '-') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}

				if ($key == "TRANSFER_TYPE_disp") {
					if ($value == 'PB') {
						$value = 'In House';
					}
				}

				if ($key == "BENEFICIARY_ID_NUMBER") {
					$value = $pTrx['BENEFICIARY_ID_TYPE'].' '.$pTrx['BENEFICIARY_ID_NUMBER'];
				}

				if ($key == "SP2D_DATE" || $key == "SPM_DATE") {
					$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);
				}

				if ($key == "LLD_DESC") {
					$value = nl2br($value);
				}

				$value = ($value == "") ? "-" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if (!empty($pslip['BANK_CODE'])) {
				$bankcode = $pslip['BANK_CODE'];
			} else {
				$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
			}

			if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
				$bankname = '-';
			} else if (empty($bankcode)) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$bankcode];
			}

			$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;

			$tableDtl[$p]['TRA_AMOUNT']  	= Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
			$tableDtl[$p]['TOTAL_CHARGES'] 	=  Application_Helper_General::displayMoney($pTrx['TOTAL_CHARGES']);
			$totalAmount = $pTrx['TRA_AMOUNT'] - $pTrx['TOTAL_CHARGES'];
			$tableDtl[$p]['PS_TOTAL_AMOUNT'] 	= Application_Helper_General::displayMoney($totalAmount);
		}

		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $pslip['PS_NUMBER']);
		$paramApprove = array(
			"FROM" 				=> "D",						// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);

		$validate->checkApprove($paramApprove);

		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment

		$config = Zend_Registry::get('config');
		$paystatusarr = array_combine(array_values($config['payment']['status']['code']),array_values($config['payment']['status']['desc']));
			//var_dump();die;
  		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";


		$select	= $this->_db->select()
			->from(
				array('TP' => 'T_PSLIP'),
				array(
					'REF_ID'					=> 'TP.REFF_ID',
					'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
					'EFTIME'					=> 'TP.PS_EFTIME',
					'REMAIN'					=> 'TP.PS_REMAIN',
					'STATUS'					=> $casePayStatus,
					'TRANS'						=> 'TP.PS_TXCOUNT'
				)
			)
			->where('TP.PS_NUMBER = ?', $PS_NUMBER);
			//echo $select;die;
		$pslipTrx = $this->_db->fetchRow($select);
		
		if($pslipTrx['TRANS'] > 1){
			//$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$this->view->downloadurl = $downloadURL;
			$this->view->trans = $pslipTrx['TRANS'];
		}
		$this->view->paystatus = $pslipTrx['STATUS'].' - Untransfered';
		$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		$this->view->frequen 		= '1x';
		$this->view->fields 		= $fields;
		$this->view->fieldsdetail	= $fieldsdetail;
		$this->view->tableDtl 		= $tableDtl;
		$this->view->TITLE_MST		= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		= $this->language->_('Transaction');
	}
}
