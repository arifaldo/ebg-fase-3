<?php

class manualupdate_Model_Suggestiondetail
{
    protected $_db;

    public function __construct()
    {
        $this->_db = Zend_Db_Table::getDefaultAdapter();
    }

    /**
     * @param string $changesId
     * 
     * @return [type]
     */
    public function getDetailRequest(string $changesId)
    {
        try {
            $result = $this->_db->select()
                ->from('TEMP_TRANSACTION_STATUS_MANUAL_UPDATE')
                ->where('CHANGES_ID = ?', $changesId)
                ->query()->fetch();
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th);
            die();
        }

        return $result;
    }

    /**
     * @param string $transactionId
     * 
     * @return [type]
     */
    public function getDetailTransaction(string $transactionId)
    {
        try {
            $result = $this->_db->select()
                ->from(['TBT' => 'T_BG_TRANSACTION'], [
                    '*',
                    'DETAIL_BG' => new Zend_Db_Expr('(SELECT CONCAT_WS(\'~|~\', "BG_NUMBER", BG_NUMBER, "BRANCH_NAME", (SELECT BRANCH_NAME FROM M_BRANCH WHERE BRANCH_CODE = BG_BRANCH LIMIT 1)) FROM T_BANK_GUARANTEE WHERE CLOSE_REF_NUMBER = TBT.CLOSE_REF_NUMBER LIMIT 1)')
                ])
                ->where('TRANSACTION_ID = ?', $transactionId)
                ->query()->fetch();
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th);
            die();
        }

        return $result;
    }

    /**
     * @param string $changesId
     * 
     * @return [type]
     */
    public function getDetailGlobalChanges(string $changesId)
    {
        try {
            $result = $this->_db->select()
                ->from('T_GLOBAL_CHANGES')
                ->where('CHANGES_ID = ?', $changesId)
                ->query()->fetch();
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th);
            die();
        }

        return $result;
    }

    /**
     * @param string $changesId
     * @param string $reason
     * 
     * @return [type]
     */
    public function updateRequestRepair(string $changesId, string $reason)
    {
        try {
            $this->_db->update('T_GLOBAL_CHANGES', [
                'READ_STATUS' => '2',
                'CHANGES_REASON' => $reason

            ], [
                'CHANGES_ID = ?' => $changesId
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th);
            die();
        }

        return true;
    }

    /**
     * @param string $changesId
     * @param string $reason
     * @param string $userlogin
     * 
     * @return [type]
     */
    public function updateReject(string $changesId, string $reason, string $userlogin)
    {
        try {
            $this->_db->update('T_GLOBAL_CHANGES', [
                'CHANGES_STATUS' => 'RJ'
            ], [
                'CHANGES_ID = ?' => $changesId
            ]);

            $this->_db->update('TEMP_TRANSACTION_STATUS_MANUAL_UPDATE', [
                'UPDATE_REJECTED' => new Zend_Db_Expr('now()'),
                'UPDATE_REJECTEDBY' => $userlogin,
                'REJECT_REASON' => $reason,
                'SUGGESTION_STATUS' => '2'
            ], [
                'CHANGES_ID = ?' => $changesId
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th);
            die();
        }

        return true;
    }

    /**
     * @param string $changesId
     * @param array $detailRequest
     * @param string $userlogin
     * 
     * @return [type]
     */
    public function updateApprove(string $changesId, array $detailRequest, string $userlogin)
    {
        try {
            $this->_db->update('T_GLOBAL_CHANGES', [
                'CHANGES_STATUS' => 'AP'
            ], [
                'CHANGES_ID = ?' => $changesId
            ]);

            $this->_db->update('T_BG_TRANSACTION', [
                'TRA_STATUS' => '2',
                'UPDATE_MEMO_FILE' => $detailRequest['UPDATE_MEMO_FILE'],
                'UPDATE_MEMO_FILE_BY' => $detailRequest['UPDATE_SUGGESTEDBY'],
                'UPDATE_MEMO_FILE_TIME' => $detailRequest['UPDATE_SUGGESTED'],
                'UPDATE_SUGGESTED' => $detailRequest['UPDATE_SUGGESTED'],
                'UPDATE_REMARKS' => $detailRequest['UPDATE_REMARKS'],
                'UPDATE_SUGGESTED' => $detailRequest['UPDATE_SUGGESTED'],
                'UPDATE_SUGGESTEDBY' => $detailRequest['UPDATE_SUGGESTEDBY'],
                'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
                'UPDATE_APPROVEDBY' => $userlogin,
            ], [
                'TRANSACTION_ID = ?' => $detailRequest['TRANSACTION_ID']
            ]);

            $this->_db->delete('TEMP_TRANSACTION_STATUS_MANUAL_UPDATE', [
                'CHANGES_ID = ?' => $changesId
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th);
            die();
        }

        return true;
    }

    public function repairChanges(string $changesId, array $updateData)
    {
        try {
            // UPDATE TEMP DATA
            $this->_db->update('TEMP_TRANSACTION_STATUS_MANUAL_UPDATE', $updateData, [
                'CHANGES_ID = ?' => $changesId
            ]);

            // UPDATE GLOBAL CHANGES
            $this->_db->update('T_GLOBAL_CHANGES', [
                'CHANGES_STATUS' => 'RR',
                'READ_STATUS' => '1'
            ], [
                'CHANGES_ID = ?' => $changesId
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            $this->_db->rollBack();
        }

        return true;
    }

    /**
     * @param string $closeRefNumber
     * 
     * @return [type]
     */
    public function updateStatusRelease(string $closeRefNumber)
    {
        try {
            // UPDATE STATUS RELEASE
            $this->_db->update('T_BG_PSLIP', [
                'PS_STATUS' => '5'
            ], [
                'CLOSE_REF_NUMBER = ?' => $closeRefNumber
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            $this->_db->rollBack();
        }

        return true;
    }

    /**
     * @param string $closeRefNumber
     * 
     * @return [type]
     */
    public function checkIfAllTrxDone(string $closeRefNumber)
    {
        try {
            // CHECK ALL TRANSACTIONS
            $result = $this->_db->select()
                ->from('T_BG_TRANSACTION')
                ->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
                ->where('TRA_STATUS != ?', '1')
                ->where('TRA_STATUS != ?', '2')
                ->query()->fetchAll();
        } catch (\Throwable $th) {
            //throw $th;
            $this->_db->rollBack();
        }

        return $result ? false : true;
    }

    /**
     * @param string $closeRefNumber
     * 
     * @return [type]
     */
    public function getSwifCode(string $closeRefNumber)
    {
        try {
            // CHECK ALL TRANSACTIONS
            $result = $this->_db->select()
                ->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
                ->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
                ->where('LOWER(PS_FIELDNAME) = ?', 'swift code')
                ->query()->fetch();
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th);
            die();
        }

        return $result['PS_FIELDVALUE'];
    }

    public function getDetailBank(string $swiftCode)
    {
        try {
            // CHECK ALL TRANSACTIONS
            $result = $this->_db->select()
                ->from('M_DOMESTIC_BANK_TABLE')
                ->where('LOWER(SWIFT_CODE) = ?', strtolower($swiftCode))
                ->query()->fetch();
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th);
            die();
        }

        return $result;
    }
}
