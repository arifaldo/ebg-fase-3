<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once "Service/Account.php";
require_once 'General/Account.php';
require_once 'General/Settings.php';
require_once 'General/CustomerUser.php';

class customer_NewController extends customer_Model_Customer
 {

    public function indexAction()
    {
		$this->_helper->layout()->setLayout('newlayout'); 
        //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

        $this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');

        $error_remark = null; $key_value = null;

        //convert limit idr & usd agar bisa masuk ke database
	    $limitidr = $this->_getParam('cust_limit_idr');
	    $limitidr = Application_Helper_General::convertDisplayMoney($limitidr);
	    $this->_setParam('cust_limit_idr',$limitidr);

	    $limitusd = $this->_getParam('cust_limit_usd');
	    $limitusd = Application_Helper_General::convertDisplayMoney($limitusd);
	    $this->_setParam('cust_limit_usd',$limitusd);
	    //END convert limit idr & usd agar bisa masuk ke database 

	    $setting = new Settings();			  	
		$tokenType = $setting->getSetting('tokentype');

		$this->view->show_register_account = $setting->getSetting('system_type');
		$system_type = $setting->getSetting('system_type');
		$this->view->system_type = $system_type;
		$tokenTypeCode = array_flip($this->_tokenType['code']);
		$tokenTypeDesc = $this->_tokenType['desc'];

		$this->view->tokenTypeText = $tokenTypeDesc[$tokenTypeCode[$tokenType]];


		$arrSecure = array(0=>"No", 1=>"Yes");

		$this->view->secure = $arrSecure;
	
	
		$select = $this->_db->select()->distinct()
					->from(array('M_CUSTOMER'),array('CUST_ID'))
					->where("CUST_STATUS != '3'")
					->order('CUST_ID ASC')
				 	-> query() ->fetchAll();
					$comp = array();
		foreach($select as $vl){
			$comp[] = $vl['CUST_ID'];
		}
	$selectglobal = $this->_db->select()
						->from(array('B' => 'M_CHARGES_WITHIN'),array('*'))
						->joinleft(array('D' => 'M_CHARGES_OTHER'), 'B.CUST_ID = D.CUST_ID',array('*'))
						->where("B.CUST_ID NOT IN (?) ",$comp);
		//echo $selectglobal;die;
		
		$arrglobal = $this->_db->fetchAll($selectglobal);		
		$chargeArr = array();
		foreach($arrglobal as $ky => $vl){
				if($vl['CHARGES_TYPE'] == '1'){
					//die;
					$chargeArr[$vl['CUST_ID']]['RTGS'] = $vl['CHARGES_AMT'];	 
				}
				if($vl['CHARGES_TYPE'] == '2'){
					$chargeArr[$vl['CUST_ID']]['SKN'] = $vl['CHARGES_AMT'];	 
				}
				if($vl['CHARGES_TYPE'] == '2'){
					$chargeArr[$vl['CUST_ID']]['ONLINE'] = $vl['CHARGES_AMT'];	
				}
					$chargeArr[$vl['CUST_ID']]['INHOUSE'] = $vl['AMOUNT']; 
					   
				$chargeArr[$vl['CUST_ID']]['INHOUSE'] = $vl['AMOUNT'];	
				$chargeArr[$vl['CUST_ID']]['APPROVED'] =  $vl['CHARGES_APPROVEDBY'].'('.$vl['CHARGES_APPROVED'].')';	
				$chargeArr[$vl['CUST_ID']]['SUGGESTED'] =  $vl['CHARGES_SUGGESTEDBY'].'('.$vl['CHARGES_SUGGESTED'].')';	
				$chargeArr[$vl['CUST_ID']]['PACKAGE_NAME'] =  $vl['PACKAGE_NAME'];	
				
				
		}
		
		$this->view->chargelist = $chargeArr;

		if($this->_request->isPost() && $this->_getParam('submit') == $this->language->_('Submit'))
		{
			$this->view->inputParam = $this->getRequest()->getParams();
			// echo '<pre>';
			// print_r($this->view->inputParam);die;
			$cust_id = strtoupper($this->_getParam('cust_id'));
			$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>12)))? $cust_id : null;

		    /*echo strtoupper($this->_getParam('cust_id'));
			die;*/

			$customer_filter = array();
			$haystack_cust_type = array($this->_custType['code']['company'],$this->_custType['code']['individual']);
			$haystack_step_approve = array($this->_masterhasStatus['code']['yes'],$this->_masterhasStatus['code']['no']);
			$cust_acc = array();
			$cust_limit = array();
  	   		$tempacc = array('idr','usd');

			$filters = array('cust_id'           => array('StripTags','StringTrim','StringToUpper'),
			               //  'cust_cif'          => array('StripTags','StringTrim'),
			                 'cust_securities'   => array('StripTags','StringTrim'),
			                 'cust_code' 		 => array('StripTags','StringTrim'),
			                 'cust_name'         => array('StripTags','StringTrim'),
			                 'cust_type'         => array('StripTags','StringTrim','StringToUpper'),
			                 'cust_workfield'    => array('StripTags','StringTrim'),
						     'cust_address'      => array('StripTags','StringTrim'),
			                 'cust_city'         => array('StripTags','StringTrim'),
			                 'cust_zip'          => array('StripTags','StringTrim'),
			                 'country_code'      => array('StripTags','StringTrim'),
			                 'cust_special'      => array('StripTags','StringTrim'),
							 'cust_province'     => array('StripTags','StringTrim'),
			                 'cust_contact'      => array('StripTags','StringTrim'),
			                 'cust_phone'        => array('StripTags','StringTrim'),
						     'cust_ext'          => array('StripTags','StringTrim'),
			                 'cust_fax'          => array('StripTags','StringTrim'),
						     'cust_email'        => array('StripTags','StringTrim'),
			                 'cust_website'      => array('StripTags','StringTrim'),
			                 'cust_special_rate'   => array('StripTags','StringTrim'),
			                 'cust_limit_idr'      => array('StripTags','StringTrim'),
			                 'cust_limit_usd'      => array('StripTags','StringTrim'),
			                //'cust_charges_status' => array('StripTags','StringTrim'),


			                 'cust_review'   => array('StripTags','StringTrim'),
			                 'cust_approver'   => array('StripTags','StringTrim'),
			                 'cust_app_token'   => array('StripTags','StringTrim'),
			                 'cust_rls_token'   => array('StripTags','StringTrim'),
			                 'cust_finance'   => array('StripTags','StringTrim'),


			                 'admin1'   => array('StripTags','StringTrim'),
			                 'emailadmin1'   => array('StripTags','StringTrim'),
			                 'phoneadmin1'   => array('StripTags','StringTrim'),
			                 'tokenadmin1'   => array('StripTags','StringTrim'),

			                 'admin2'   => array('StripTags','StringTrim'),
			                 'emailadmin2'   => array('StripTags','StringTrim'),
			                 'phoneadmin2'   => array('StripTags','StringTrim'),
			                 'tokenadmin2'   => array('StripTags','StringTrim'),

			                 // 'admin3'   => array('StripTags','StringTrim'),
			                 // 'emailadmin3'   => array('StripTags','StringTrim'),
			                 // 'phoneadmin3'   => array('StripTags','StringTrim'),
			                 // 'tokenadmin3'   => array('StripTags','StringTrim'),
			                 'cust_rdn_key'   => array('StripTags','StringTrim'),
			                 'cust_role_adm'   => array('StripTags','StringTrim'),
			                 'req_id'          => array('StripTags','StringTrim'),

			                 //'cust_monthlyfee_status'  => array('StripTags','StringTrim'),
			                 //'cust_token_auth'  => array('StripTags','StringTrim'),
							 /* 'cust_emobile'        => array('StripTags','StringTrim'),
			                 'cust_isemobile'      => array('StripTags','StringTrim'), */
			                );

			if($system_type == "2" || $system_type == "1" || $system_type == "3"){
				$filters += array('cust_cif'          => array('StripTags','StringTrim'));
			}

			$validators =  array('cust_id'        => array('NotEmpty',
      	                                                   'Alnum',
      	                                                    array('StringLength',array('min'=>5,'max'=>16)),
      	                                                       array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_ID')),
      	                                                       array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_ID')),
      	                                                       'messages' => array($this->language->_('Can not be empty'),
      	                                                                           $this->language->_('Invalid Company Code Format'),
//      	                                                                           $this->language->_('Invalid Company Code Format'),
																						$this->language->_('Company Code length min').' 6 '.$this->language->_('max') . '16',
      	                                                                           $this->language->_('Company code already in use. Please use another'),
      	                                                                           $this->language->_('Company code already suggested. Please use another'))
      	                                                      ),

      	                     
      	                     'cust_securities'                => array('NotEmpty',
												                //array('StringLength',array('min'=>1,'max'=>80)),
												               'messages' => array($this->language->_('Can not be empty'),
								 					  	      	         		   //$this->getErrorRemark('04','Company Name')
								 					  	      	         		   )
												              ),
      	                     // 'cust_code'                => 
							 'cust_name'                => array('NotEmpty',
												                //array('StringLength',array('min'=>1,'max'=>80)),
												               'messages' => array($this->language->_('Can not be empty'),
								 					  	      	         		   //$this->getErrorRemark('04','Company Name')
								 					  	      	         		   )
												              ),

                             'cust_type'                => array('allowEmpty' => true,
												              	 array('StringLength',array('max'=>16)),
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												              	 				   $this->language->_('The maximum characters allowed is').' 16',
												                                   //$this->getErrorRemark('22','Company Type')
												                                   )
											                  ),


                             'cust_workfield'            => array('allowEmpty' => true,
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												                                   //$this->getErrorRemark('22','Company Type')
												                                   )
											                  ),


						     'cust_address'            => array('allowEmpty' => true,
												                array('StringLength',array('max'=>200)),
												               'messages' => array(
												                					$this->language->_('The maximum characters allowed is').' 200'
												                                  // $this->getErrorRemark('04','Company Address')
												                                   )
												              ),



						     'cust_city'               => array(array('StringLength',array('min'=>1,'max'=>20)),
												               'allowEmpty' => true,
												               'messages' => array(
												                                   $this->language->_('The maximum characters allowed is').' 20'
												                                  )
												              ),

						     'cust_zip'           => array(//array('StringLength',array('min'=>1,'max'=>10)),
												               'allowEmpty' => true,
						     									'Digits',
												               'messages' => array(
												                                // $this->getErrorRemark('04','Zip')
												                                  )
												              ),

						     'country_code'           => array(//array('StringLength',array('min'=>1,'max'=>10)),
												               'allowEmpty' => true,
												               'messages' => array(
												                              // $this->getErrorRemark('04','Country')
												                                  )
												              ),

                           'cust_province'      => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
												               'messages' => array($this->language->_('The maximum characters allowed is').' 128')
												              ),

                           'cust_contact'       => array('allowEmpty' => true,
												         array('StringLength',array('min'=>1,'max'=>128)),
												         'messages' => array(
												                             $this->language->_('The maximum characters allowed is').' 128')
												         ),

						    'cust_phone'        => array('Digits',
												         array('StringLength',array('min'=>1,'max'=>20)),
												         'allowEmpty' => true,
												         'messages' => array($this->language->_('Invalid phone number format'),
												                             $this->language->_('The maximum characters allowed is').' 20'
												                            )
												              ),

                            'cust_ext'          => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
                            									'Digits',
												               'messages' => array(
												                                  $this->language->_('The maximum characters allowed is').' 128'
												                                   )
												              ),

                            'cust_fax'          => array('Digits',
												         'allowEmpty' => true,
												         'messages' => array(
												                            $this->language->_('Invalid fax number format')
												                            )
												              ),

                            'cust_email'        => array( //new Application_Validate_EmailAddress(),
												          // array('StringLength',array('min'=>1,'max'=>128)),
												          // 'allowEmpty' => true,
												          // 'messages' => array(
												          //                    //$this->language->_('Invalid email format'),
												          //                    $this->language->_('Email lenght cannot be more than 128'),
												          //                    )
							                            	'NotEmpty',
																			                //array('StringLength',array('min'=>1,'max'=>80)),
																			               'messages' => array($this->language->_('Can not be empty'))
                                                        ),

                            'cust_website'      => 	array('allowEmpty' => true,
												              	 // array('StringLength',array('max'=>16)),
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												              	 				   // $this->language->_('The maximum characters allowed is').' 16',
												                                   // $this->getErrorRemark('22','Company Type')
												                                   )
											                  ),


                           /* 'cust_charges_status'      => array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                    $this->language->_('invalid format')
                                                                                          )
                                                              ),

                             'cust_monthlyfee_status'   => array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                   $this->language->_('invalid format')
                                                                                          )
                                                              ),
                             'cust_token_auth'      	=> array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                   $this->language->_('invalid format')
                                                                                          )
                                                              ),*/


                             'cust_special_rate'      	=> array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                       $this->language->_('invalid format')
                                                                                          )
                                                              ),
                             'cust_special' => array('allowEmpty' => true,'messages' => array()),
                            'cust_limit_idr' => array('allowEmpty' => true,'messages' => array()),
							'cust_limit_usd' => array('allowEmpty' => true,'messages' => array()),
							'cust_review'           => array('allowEmpty' => true,'messages' => array()),
							'cust_approver'           => array('allowEmpty' => true,'messages' => array()),
							'cust_app_token'           => array('allowEmpty' => true,'messages' => array()),
							'cust_rls_token'           => array('allowEmpty' => true,'messages' => array()),
							'cust_finance'           => array('allowEmpty' => true,'messages' => array()),


							'admin1'           => array('allowEmpty' => true,'messages' => array()),
							'emailadmin1'           => array('allowEmpty' => true,'messages' => array()),
							'phoneadmin1'           => array('allowEmpty' => true,'messages' => array()),
							'tokenadmin1'           => array('allowEmpty' => true,'messages' => array()),

							'admin2'           => array('allowEmpty' => true,'messages' => array()),
							'emailadmin2'           => array('allowEmpty' => true,'messages' => array()),
							'phoneadmin2'           => array('allowEmpty' => true,'messages' => array()),
							'tokenadmin2'           => array('allowEmpty' => true,'messages' => array()),
							'cust_role_adm'           => array('allowEmpty' => true,'messages' => array()),
							 'req_id'           => array(//array('StringLength',array('min'=>1,'max'=>10)),
												               'allowEmpty' => true,
												               'messages' => array(
												                              // $this->getErrorRemark('04','Country')
												                                  )
												              ),
							// 'admin3'           => array('allowEmpty' => true,'messages' => array()),
							// 'emailadmin3'           => array('allowEmpty' => true,'messages' => array()),
							// 'phoneadmin3'           => array('allowEmpty' => true,'messages' => array()),
							// 'tokenadmin3'           => array('allowEmpty' => true,'messages' => array()),
							// 'cust_rdn_key'           => array('allowEmpty' => true,'messages' => array()),

							// 'check_reviewer'   => array('StripTags','StringTrim'),
			    //              'check_approver'   => array('StripTags','StringTrim'),
			    //              'switch_token_approver'   => array('StripTags','StringTrim'),
			    //              'switch_token_releaser'   => array('StripTags','StringTrim'),
			    //              'service_type1'   => array('StripTags','StringTrim'),
                            /*  'cust_emobile'      => array(
                                                          //array('StringLength',array('min'=>1,'max'=>100)),
                                                          array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_EMOBILE')),
      	                                                  array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_EMOBILE')),
                                                          'allowEmpty' => true,
                                                          'messages' => array(
                                                                            'Emobile No. already assigned in other company.',
      	                                                                    'Emobile No already suggested. Please use another'
                                                                             )
                                                          ),

                            'cust_isemobile'      => array(//array('StringLength',array('min'=>1,'max'=>100)),
                                                           'Digits',
												           'allowEmpty' => true,
                                                           'messages' => array(
                                                                               'Invalid emobile number format',
                                                                              )
                                                           ),  */
							);

				if($system_type == "2" || $system_type == "1" || $system_type == "3"){
					$validators += array('cust_cif'                 => array(
      	                                                      //array('StringLength',array('min'=>1,'max'=>19)),
												               array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_CIF')),
                                                               array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_CIF')),
												               'allowEmpty' => true,
												               'messages' => array(//$this->getErrorRemark('04','Company CIF'),
                                                                                   $this->language->_('CIF is already registered. Please use another'),
                                                                                   $this->language->_('CIF is already suggested. Please use another')
                                                                                   )
												              ));
				}

	if($this->_getParam('cust_securities') == 1){
		 	 $validators += array('cust_code'                => array('NotEmpty',
		 	 															// array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_CODE','exclude'=>$expid)),
                                                               // array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_CODE')),
												               'messages' => array($this->language->_('Can not be empty'),	
                                                               						// $this->language->_('Securities Company Code is already registered. Please use another'),
                                                                                   // $this->language->_('Securities Company Code is already suggested. Please use another')							 					  	      	         		
								 					  	      	         		   )
												              )
								);
		 }		

		// echo '<pre>';
     	// print_r($this->_request->getParams());
       $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

       // echo "<pre>";
       // print_r($zf_filter_input->req_id);die;


		//validasi user_id : paling sedikit harus punya 1 huruf dan 1 angka
	      if($zf_filter_input->cust_id)
	      {
	         $user_id =  $zf_filter_input->cust_id;
	         $check_alpha  = 0;
	         $check_digits = 0;
	         $strlen_user_id = strlen($user_id);

	         for($i=0; $i<$strlen_user_id; $i++)
	         {
	            if(Zend_Validate::is($user_id[$i],'Alpha'))  $check_alpha  = 1;
	            if(Zend_Validate::is($user_id[$i],'Digits')) $check_digits = 1;
	         }

	         if($check_alpha == 1 && $check_digits == 1) $flag_user_id = 'T';
	         else $flag_user_id = 'F';
	      }
	      else
	      {
	         $flag_user_id = 'T';
	      }

		//validasi multiple email
	      if($this->_getParam('cust_email'))
	      {
			$validate = new validate;
	      	$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('cust_email'));
	      }
	      else
	      {
	      	$cek_multiple_email = true;
	      }

       //validasi emobile, cust phone dan cust fax = harus dimulai dengan '62'
	   $flag_cust_emobile = 'T';
       /* $flag_cust_isemobile_error = false;
	   if($zf_filter_input->cust_emobile)
       {
           $cust_emobile_62 = substr($zf_filter_input->cust_emobile,0,2);

           if($cust_emobile_62 != '62') $flag_cust_emobile = 'F';
           else $flag_cust_emobile = 'T';
       }
       else
       {
           if($zf_filter_input->cust_isemobile == '' || is_null($zf_filter_input->cust_isemobile) || $zf_filter_input->cust_isemobile == 0)
           {
              $flag_cust_emobile = 'T';
           }
           else
           {
              $flag_cust_emobile = 'F';
              $flag_cust_isemobile_error = true;
           }
        } */


       $flag_cust_phone = 'T';
      /* if($zf_filter_input->cust_phone)
       {
          $cust_phone_62   = substr($zf_filter_input->cust_phone,0,2);

          if($cust_phone_62 != '62') $flag_cust_phone = 'F';
          else $flag_cust_phone = 'T';
       }
       else $flag_cust_phone = 'T';*/

       $flag_cust_fax = 'T';
       /*if($zf_filter_input->cust_fax)
       {
          $cust_fax_62     = substr($zf_filter_input->cust_fax,0,2);

          if($cust_fax_62 != '62') $flag_cust_fax = 'F';
          else $flag_cust_fax = 'T';
       }
       else $flag_cust_fax = 'T';*/
      //END validasi emobile = harus dimulai dengan '62'






       if($zf_filter_input->isValid() && $flag_user_id == 'T' && $flag_cust_emobile == 'T' && $flag_cust_phone == 'T' && $flag_cust_fax == 'T' && $cek_multiple_email == true)
       {
      	  $info = 'Customer ID = '.$zf_filter_input->cust_id.', Customer Name = '.$zf_filter_input->cust_name;
          $cust_data = $this->_custData;
          // echo '<pre>';
          // print_r($validators);die;
	  	  foreach($validators as $key=>$value)
	  	  {
	  	  	// print_r($key);
	  	  	// echo '<br/>';
	  	  	// print_r($zf_filter_input->$key);
	  	    if($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;
	  	  }
	  	  // print_r($cust_data);
	  	  // die;


	  	  try
	      {
		     $this->_db->beginTransaction();
		    //  if($system_type == "3"){
		    //  	$cust_data['CUST_CIF'] = $cust_data['CUST_ID'];
		    //  }

		     if(is_null($cust_data['CUST_CHARGES_STATUS']) || $cust_data['CUST_CHARGES_STATUS'] == '') $cust_data['CUST_CHARGES_STATUS'] = 0;
		     if(is_null($cust_data['CUST_MONTHLYFEE_STATUS']) || $cust_data['CUST_MONTHLYFEE_STATUS'] == '')   $cust_data['CUST_MONTHLYFEE_STATUS'] = 0;
		     if(is_null($cust_data['CUST_TOKEN_AUTH']) || $cust_data['CUST_TOKEN_AUTH'] == '')   $cust_data['CUST_TOKEN_AUTH'] = 0;
		     if(is_null($cust_data['CUST_SPECIAL_RATE']) || $cust_data['CUST_SPECIAL_RATE'] == '')     $cust_data['CUST_SPECIAL_RATE'] = 0;
		     if(is_null($cust_data['CUST_SPECIAL']) || $cust_data['CUST_SPECIAL'] == '')     $cust_data['CUST_SPECIAL'] = 'N';
		      if(is_null($cust_data['CUST_LIMIT_IDR']) || $cust_data['CUST_LIMIT_IDR'] == '')     $cust_data['CUST_LIMIT_IDR'] = 0;
		      if(is_null($cust_data['CUST_LIMIT_USD']) || $cust_data['CUST_LIMIT_USD'] == '')     $cust_data['CUST_LIMIT_USD'] = 0;
		     // if(is_null($cust_data['CUST_ISEMOBILE']) || $cust_data['CUST_ISEMOBILE'] == '')           $cust_data['CUST_ISEMOBILE'] = 0;



		     $cust_data['CUST_STATUS'] = 1;
		     $cust_data['CUST_CREATED']     = null;
		     $cust_data['CUST_CREATEDBY']   = null;
		     $cust_data['CUST_UPDATED']     = null;
		     $cust_data['CUST_UPDATEDBY']   = null;


		     $cust_data['CUST_SUGGESTED']    = new Zend_Db_Expr('now()');
		     $cust_data['CUST_SUGGESTEDBY']  = $this->_userIdLogin;

		     if($cust_data['CUST_APP_TOKEN'] == 'on'){
		     	$cust_data['CUST_APP_TOKEN'] = '1';
		     }

		     if($cust_data['CUST_RLS_TOKEN'] == 'on'){
		     	$cust_data['CUST_RLS_TOKEN'] = '1';
		     }

		      if($zf_filter_input->cust_role_adm == 'on'){
		     	$cust_data['CUST_ADM_ROLE'] = '1';
		     }

		     if($cust_data['CUST_FINANCE'] == '3'){
		     	$cust_data['CUST_LIMIT_IDR'] = 1;
		     	$cust_data['CUST_LIMIT_USD'] = 1;
		     }
		     // $cust_data['CUST_ADM_ROLE'] = ;
		      // Zend_Debug::dump( $cust_data );
	          // die;
		     if(!empty($this->_getParam('same_release'))){
		     	$cust_data['CUST_SAME_USER'] = $this->_getParam('same_release');
		     }

		     // insert ke T_GLOBAL_CHANGES
		     $change_id = $this->suggestionWaitingApproval('Customer',$info,$this->_changeType['code']['new'],null,'M_CUSTOMER','TEMP_CUSTOMER',$zf_filter_input->cust_id,$zf_filter_input->cust_name,$zf_filter_input->cust_id,$zf_filter_input->cust_name);

		     try{
				 
				 if($this->_getParam('charges') == 'global'){
						 $cust_data['CUST_SPECIAL']   = 'N';
					 }
					 if($this->_getParam('charges') == 'special'){
						 $cust_data['CUST_SPECIAL']   = 'Y';
						 $cust_charge = $this->_getParam('chargesspecial');
						 $selectcharge = $this->_db->select()
                            ->from('M_CHARGES_WITHIN', array('*'))
                            ->where('CUST_ID = ? ',$cust_charge);

		        		$charge_data = $this->_db->fetchRow($selectcharge);
		        		if(!empty($charge_data)){
		        			$cust_data['CUST_CHARGESID'] = $charge_data['CHARGES_ID'];
		        		}
						 
					 }
		     	$this->insertTempCustomer($change_id,$cust_data);
				
				
					 
					 if($this->_getParam('charges') == 'custom'){
							$inhousecharge = $this->_getParam('chargeinhouse');
						 if(empty($this->_getParam('chargeinhouse'))){
							$inhousecharge = 0; 
						 }
								$data= array(
												'CHANGES_ID' 		=> $change_id,
												'PACKAGE_NAME'		=> NULL,
												'CUST_ID' 			=> $cust_data['CUST_ID'],
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $inhousecharge,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> NULL,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);

								$rtgscharge = $this->_getParam('chargertgs');
							 if(empty($this->_getParam('chargertgs'))){
								$rtgscharge = 0; 
							 }
						
							$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_data['CUST_ID'],
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '1',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $rtgscharge,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

								$this->_db->insert('TEMP_CHARGES_OTHER',$data1);
								
								$skncharge = $this->_getParam('chargeskn');
								 if(empty($this->_getParam('chargeskn'))){
									$skncharge = 0; 
								 }
								
								$data2 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_data['CUST_ID'],
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '2',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $skncharge,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

								$this->_db->insert('TEMP_CHARGES_OTHER',$data2);
								
								$onlinecharge = $this->_getParam('chargeonline');
								 if(empty($this->_getParam('chargeonline'))){
									$onlinecharge = 0; 
								 }
								
								$data8 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_data['CUST_ID'],
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '8',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $onlinecharge,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
 
								$this->_db->insert('TEMP_CHARGES_OTHER',$data8);
					 
					 }
				
				
		     	foreach ($zf_filter_input->req_id as $key => $value) {
       							 $content = array('CHANGES_ID'       => $change_id,
									'CUST_ID'          => $cust_data['CUST_ID'],
									'ACCT_NO'          => $value,
									'CCY_ID'           => Application_Helper_General::getCurrCode($this->_getParam('ccy'.$value)),
									'ACCT_EMAIL'       => null,
									'ACCT_STATUS'      => 1,
									'ACCT_SUGGESTED'   => new Zend_Db_Expr('now()'),
									'ACCT_SUGGESTEDBY' => $this->_userIdLogin,
									'PLAFOND'          => null,
									'ACCT_SOURCE'      => null,
									'ACCT_NAME'        => $this->_getParam('accountName'.$value),
									'ACCT_TYPE'        => $this->_getParam('productType'.$value),
									'ACCT_DESC'        => $this->_getParam('acct_desc'.$value),
								//	'ACCT_ALIAS_NAME'  => isset($acct_data['ACCT_ALIAS_NAME']) ? $acct_data['ACCT_ALIAS_NAME'] : null,
									'ACCT_ALIAS_NAME'  => $this->_getParam('accountName'.$value),
		                            'ORDER_NO'         => $key,
									// 'ACCT_NAME'        => isset($acct_data['ACCT_NAME']) ? $acct_data['ACCT_NAME'] : null,
									'GROUP_ID'         => null,
									'ACCT_RESIDENT'         => null,
						       		'ACCT_CITIZENSHIP'         => null,
						       		'ACCT_CATEGORY'         =>  null,
						       		'ACCT_ID_TYPE'         => null,
						       		'ACCT_ID_NUMBER'         => null,
								   );
         			// print_r($content);
	  			  $insert = $this->_db->insert('TEMP_CUSTOMER_ACCT',$content);
       			}
       			// die;
		     }catch(Exception $e)
		  	{
		  		print_r($e);die;
		  	}

		  	// 'user_email'     => array('StripTags','StringTrim'),
     //                    'user_fullname'  => array('StripTags','StringTrim'),
     //                    'user_phone'     => array('StripTags','StringTrim'),
     //                    'user_ext'             => array('StripTags','StringTrim'),
     //                    'token_id'             => array('StripTags','StringTrim'),
     //                    'user_isemail'         => array('StripTags','StringTrim'),

     //                    'cust_id'        => array('StripTags','StringTrim','StringToUpper'),
			  	// echo "<pre>";
		  		// print_r($cust_data);die;

		  		// print_r($this->_getParam('cust_email'));
		  // 	 	$acct_data['ACCT_STATUS'] = 1;
			 // 	$acct_data['ACCT_SUGGESTED']   = new Zend_Db_Expr('now()');
				// $acct_data['ACCT_SUGGESTEDBY'] = $this->_userIdLogin;


		  		// $this->insertTempCustomerAcct($change_id,$acct_data);


		  		$comp_nameOriSub = preg_replace("/[^0-9a-zA-Z]/", "", $cust_data['CUST_NAME']);
		  		$generateuserid = strtoupper(substr($comp_nameOriSub, 0,4)).'_';
		  		// print_r($cust_data);die;
		  		// echo $generateuserid;die;
		  		try{
				$count = 0;
				if(!empty($cust_data['ADMIN1'])){
					$count += 1;
					$userfullnameGet = $cust_data['ADMIN1'];
					$userfullnameGetSub = preg_replace("/[^0-9a-zA-Z]/", "", $userfullnameGet);
					$userfullname = strtoupper();

					$generate_userid = $generateuserid;
					$squenceNumberUser = '0'.$count;
					$hasil_userid = $generate_userid.'SYSADM01';

					$c_data = array('CUST_ID'        => $cust_data['CUST_ID'],
		//  	                       'USER_ID'        => null,
				                   'USER_FULLNAME'  => $cust_data['ADMIN1'],
		        				   'USER_PASSWORD'  => null,
		                           'USER_EMAIL'     => $cust_data['EMAILADMIN1'],
		                           'USER_PHONE'     => $cust_data['PHONEADMIN1'],
		                           'USER_STATUS'    => 2,
		                           'USER_EXT'       => null,
		                           'TOKEN_ID'       => $cust_data['TOKENADMIN1'],
		                           'USER_ISWEBSERVICES' => null,
		                           'USER_ISEMAIL'       => 1,
		  	                       //'FGROUP_ID'      => null,
		  	                       //'USER_HASTOKEN'  => null
		  	                       'USER_SUGGESTED'   => null,
		       					   'USER_SUGGESTEDBY' => null,
		                            );
					$c_data['USER_ID'] = $hasil_userid;
					$c_data['USER_CREATED']     = null;
					$c_data['USER_CREATEDBY']   = null;
					$c_data['USER_UPDATED']     = null;
					$c_data['USER_UPDATEDBY']   = null;
					$c_data['FGROUP_ID']			= 'A02';
				  	$c_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
					$c_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
					$c_data['USER_INDEX']			= '1';
					$selectcomp = $this->_db->select()
                            ->from('M_FPRIVILEGE_TEMPLATE', array('FPRIVI_ID'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('FTEMPLATE_ID = ? ','A02');

		        	$tempColumn = $this->_db->fetchAll($selectcomp);
		        // print_r($cust_data);die;

		     	 	$this->insertTempPrivilege($change_id,$tempColumn,$c_data);

		     	 	$this->updateTempadm($change_id,$hasil_userid);
		     	 	// print_r($cust_data);die;
		  			$this->insertTempUseradmin1($change_id,$c_data);


				}

				if(!empty($cust_data['ADMIN2'])){
					// print_r($cust_data);die;
					$count += 1;
					$userfullnameGet = $cust_data['ADMIN2'];
					$userfullnameGetSub = preg_replace("/[^0-9a-zA-Z]/", "", $userfullnameGet);
					$userfullname = strtoupper(substr($userfullnameGetSub, 0,9));

					$generate_userid = $generateuserid;
					$squenceNumberUser = '0'.$count;
					$hasil_userid = $generate_userid.'SYSADM02';

					$c_data = array('CUST_ID'        => $cust_data['CUST_ID'],
		//  	                       'USER_ID'        => null,
				                   'USER_FULLNAME'  => $cust_data['ADMIN2'],
		        				   'USER_PASSWORD'  => null,
		                           'USER_EMAIL'     => $cust_data['EMAILADMIN2'],
		                           'USER_PHONE'     => $cust_data['PHONEADMIN2'],
		                           'USER_STATUS'    => 2,
		                           'USER_EXT'       => null,
		                           'TOKEN_ID'       => $cust_data['TOKENADMIN2'],
		                           'USER_ISWEBSERVICES' => null,
		                           'USER_ISEMAIL'       => 1,
		  	                       //'FGROUP_ID'      => null,
		  	                       //'USER_HASTOKEN'  => null
		  	                       'USER_SUGGESTED'   => null,
		       					   'USER_SUGGESTEDBY' => null,
		                            );
					$c_data['USER_ID'] = $hasil_userid;
					$c_data['USER_CREATED']     = null;
					$c_data['USER_CREATEDBY']   = null;
					$c_data['USER_UPDATED']     = null;
					$c_data['USER_UPDATEDBY']   = null;
					$c_data['FGROUP_ID']			= 'A02';
				  	$c_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
					$c_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
					$c_data['USER_INDEX']			= '2';
					$selectcomp = $this->_db->select()
                            ->from('M_FPRIVILEGE_TEMPLATE', array('FPRIVI_ID'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('FTEMPLATE_ID = ? ','A02');

		        	$tempColumn = $this->_db->fetchAll($selectcomp);
		        // print_r($cust_data);die;

		     	 	$this->insertTempPrivilege($change_id,$tempColumn,$c_data);

		     	 	$this->updateTempadm1($change_id,$hasil_userid);

		  			$this->insertTempUseradmin2($change_id,$c_data);
				}


		// 		if(!empty($cust_data['ADMIN3'])){
		// 			$count += 1;

		// 			$userfullnameGet = $cust_data['ADMIN3'];
		// 			$userfullnameGetSub = preg_replace("/[^0-9a-zA-Z]/", "", $userfullnameGet);
		// 			$userfullname = strtoupper(substr($userfullnameGetSub, 0,9));

		// 			$generate_userid = $generateuserid;
		// 			$squenceNumberUser = '0'.$count;
		// 			$hasil_userid = $generate_userid.$userfullname.$squenceNumberUser;

		// 			$c_data = array('CUST_ID'        => $cust_data['CUST_ID'],
		// //  	                       'USER_ID'        => null,
		// 		                   'USER_FULLNAME'  => $cust_data['ADMIN3'],
		//         				   'USER_PASSWORD'  => null,
		//                            'USER_EMAIL'     => $cust_data['EMAILADMIN3'],
		//                            'USER_PHONE'     => $cust_data['PHONEADMIN3'],
		//                            'USER_STATUS'    => 2,
		//                            'USER_EXT'       => null,
		//                            'TOKEN_ID'       => null,
		//                            'USER_ISWEBSERVICES' => null,
		//                            'USER_ISEMAIL'       => 1,
		//   	                       //'FGROUP_ID'      => null,
		//   	                       //'USER_HASTOKEN'  => null
		//   	                       'USER_SUGGESTED'   => null,
		//        					   'USER_SUGGESTEDBY' => null,
		//                             );
		// 			$c_data['USER_ID'] = $hasil_userid;
		// 			$c_data['USER_CREATED']     = null;
		// 			$c_data['USER_CREATEDBY']   = null;
		// 			$c_data['USER_UPDATED']     = null;
		// 			$c_data['USER_UPDATEDBY']   = null;
		// 			$c_data['FGROUP_ID']			= 'A02';
		// 		  	$c_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
		// 			$c_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
		// 			$c_data['USER_INDEX']			= '3';

		// 			$selectcomp = $this->_db->select()
  //                           ->from('M_FPRIVILEGE_TEMPLATE', array('FPRIVI_ID'))
  //                           // ->where('CUST_NAME = ? ',$tblName)
  //                           ->where('FTEMPLATE_ID = ? ','A02');

		//         	$tempColumn = $this->_db->fetchAll($selectcomp);
		//         // print_r($cust_data);die;

		//      	 	$this->insertTempPrivilege($change_id,$tempColumn,$c_data);
		//      	 	$this->updateTempsys($change_id,$hasil_userid);
		//   			$this->insertTempUseradmin3($change_id,$c_data);
		// 		}

				//tambahan utk adapter
		  		// $this->insertTempAdapterProfile($change_id, $cust_data['CUST_ID']);

					}catch(Exception $e)
			  	{
			  		print_r($e);die;
			  	}
				// print_r($count);die;


  	   			// $this->_getParam('squenceNumberUser');


		  		 // $info = 'User ID = '.$hasil_userid.', User Name = '.$zf_filter_input->user_name;


		        // if(is_null($user_data['USER_ISEMAIL']) || $user_data['USER_ISEMAIL']=='')
			  	// {
			  	// $cust_data['USER_ISEMAIL'] = 1;
			  	// }

			 //  	$cust_data['USER_ID'] = $hasil_userid;
			 //  	$cust_data['USER_STATUS'] = 2;
			 //  	$cust_data['USER_CREATED']     = null;
				// $cust_data['USER_CREATEDBY']   = null;
				// $cust_data['USER_UPDATED']     = null;
				// $cust_data['USER_UPDATEDBY']   = null;

			 //  	$cust_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
				// $cust_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
				// echo '<pre>';
				// print_r($cust_data);die;

		  		// $count_privi = count($priviId);


			 Application_Helper_General::writeLog('CCAD','Customer has been added, Cust ID : '.$zf_filter_input->cust_id. ' Cust Name : '.$zf_filter_input->cust_name.' Change id : '.$change_id);

		     $this->_db->commit();

     		$set = new Settings();
    		$template = $set->getSetting('bemailtemplate_greeting_eregis');
			$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
			$templateEmailMasterBankName = $set->getSetting('master_bank_name');
			$masterBankEmail = $set->getSetting('master_bank_email');
			$masterBankTlp = $set->getSetting('master_bank_telp');
			$masterBankWapp = $set->getSetting('master_bank_wapp');

			// $cont_name = $this->_getParam('cust_contact');
			// $comp_name = $this->_getParam('cust_name');
			// $email_user = $this->_getParam('cust_email');

			$template = str_ireplace('[[contact_name]]',$zf_filter_input->cust_contact,$template);
			$template = str_ireplace('[[company_name]]',$zf_filter_input->cust_name,$template);
			$template = str_ireplace('[[master_bank_app_name]]',$templateEmailMasterBankAppName,$template);
		 	$template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);
			$template = str_ireplace('[[master_bank_email]]',$masterBankEmail,$template);
			$template = str_ireplace('[[master_bank_telp]]',$masterBankTlp,$template);
			$template = str_ireplace('[[master_bank_wapp]]',$masterBankWapp,$template);

			$subject = "Greeting Registration";

			//Application_Helper_Email::queueEmail($email,$subject,$template,__FILE__.__LINE__);
			Application_Helper_Email::sendEmail($zf_filter_input->cust_email,$subject,$template);

		     $this->_redirect('/notification/submited/index');
		     //$this->_redirect('/notification/success/index');
	  	  }
		  catch(Exception $e)
		  {
		    $this->_db->rollBack();
		    $error_remark = 'exception';
	      }


		  if(isset($error_remark))
		  {
		  }
		  else
		  {
		    $this->view->success = 1;
		    $msg   = 'Record added sucessfully';
		    $key_value = $zf_filter_input->cust_id;
		    $this->view->customer_msg = $msg;
		   //insert log
		   try
		   {
			  $this->_db->beginTransaction();

			  Application_Helper_General::writeLog('CCAD',' Create Customer ['.$this->view->cust_id.']');

			  $this->_db->commit();
		   }
		   catch(Exception $e)
		   {
			  $this->_db->rollBack();
			  Application_Log_GeneralLog::technicalLog($e);
		   }
		  }
	  }// END IF IS VALID
	  else
	  {

	    $this->view->error = 1;
	  	$this->view->cust_id   = ($zf_filter_input->isValid('cust_id'))? $zf_filter_input->cust_id : $this->_getParam('cust_id');
	  	$this->view->cust_name = ($zf_filter_input->isValid('cust_name'))? $zf_filter_input->cust_name : $this->_getParam('cust_name');
	  	$this->view->cust_cif = ($zf_filter_input->isValid('cust_cif'))? $zf_filter_input->cust_cif : $this->_getParam('cust_cif');
	  	$this->view->cust_special = ($zf_filter_input->isValid('cust_special'))? $zf_filter_input->cust_special : $this->_getParam('cust_special');
	  	$this->view->cust_securities = ($zf_filter_input->isValid('cust_securities'))? $zf_filter_input->cust_securities : '0';
	  	$this->view->cust_code = ($zf_filter_input->isValid('cust_code'))? $zf_filter_input->cust_code : $this->_getParam('cust_code');

	    $this->view->cust_type = ($zf_filter_input->isValid('cust_type'))? $zf_filter_input->cust_type : $this->_getParam('cust_type');
	  	$this->view->cust_workfield = ($zf_filter_input->isValid('cust_workfield'))? $zf_filter_input->cust_workfield : $this->_getParam('cust_workfield');
		$this->view->cust_address = ($zf_filter_input->isValid('cust_address'))? $zf_filter_input->cust_address : $this->_getParam('cust_address');
		$this->view->cust_city = ($zf_filter_input->isValid('cust_city'))? $zf_filter_input->cust_city : $this->_getParam('cust_city');
		$this->view->cust_zip  = ($zf_filter_input->isValid('cust_zip'))? $zf_filter_input->cust_zip : $this->_getParam('cust_zip');
		$this->view->cust_province = ($zf_filter_input->isValid('cust_province'))? $zf_filter_input->cust_province : $this->_getParam('cust_province');
		$this->view->country = ($zf_filter_input->isValid('country'))? $zf_filter_input->country : $this->_getParam('country');
		$this->view->cust_contact  = ($zf_filter_input->isValid('cust_contact'))? $zf_filter_input->cust_contact : $this->_getParam('cust_contact');
		$this->view->cust_phone    = ($zf_filter_input->isValid('cust_phone'))? $zf_filter_input->cust_phone : $this->_getParam('cust_phone');
		$this->view->cust_ext      = ($zf_filter_input->isValid('cust_ext'))? $zf_filter_input->cust_ext : $this->_getParam('cust_ext');
		$this->view->cust_fax      = ($zf_filter_input->isValid('cust_fax'))? $zf_filter_input->cust_fax : $this->_getParam('cust_fax');
        $this->view->cust_email    = ($zf_filter_input->isValid('cust_email'))? $zf_filter_input->cust_email : $this->_getParam('cust_email');
        $this->view->cust_website  = ($zf_filter_input->isValid('cust_website'))? $zf_filter_input->cust_website : $this->_getParam('cust_website');
        $this->view->country_code  = ($zf_filter_input->isValid('country_code'))? $zf_filter_input->country_code : $this->_getParam('country_code');
         $this->view->cust_limit_idr  = ($zf_filter_input->isValid('cust_limit_idr'))? $zf_filter_input->cust_limit_idr : $this->_getParam('cust_limit_idr');
         $this->view->cust_limit_usd  = ($zf_filter_input->isValid('cust_limit_usd'))? $zf_filter_input->cust_limit_usd : $this->_getParam('cust_limit_usd');

         $this->view->same_release  = $this->_getParam('same_release');

         $this->view->cust_review  = ($zf_filter_input->isValid('cust_review'))? $zf_filter_input->cust_review : $this->_getParam('cust_review');
         $this->view->cust_approver  = ($zf_filter_input->isValid('cust_approver'))? $zf_filter_input->cust_approver : $this->_getParam('cust_approver');
         $this->view->cust_app_token  = ($zf_filter_input->isValid('cust_app_token'))? $zf_filter_input->cust_app_token : $this->_getParam('cust_app_token');
         $this->view->cust_rls_token  = ($zf_filter_input->isValid('cust_rls_token'))? $zf_filter_input->cust_rls_token : $this->_getParam('cust_rls_token');
         $this->view->cust_finance  = ($zf_filter_input->isValid('cust_finance'))? $zf_filter_input->cust_finance : $this->_getParam('cust_finance');
         $this->view->admin1  = ($zf_filter_input->isValid('admin1'))? $zf_filter_input->admin1 : $this->_getParam('admin1');
         $this->view->emailadmin1  = ($zf_filter_input->isValid('emailadmin1'))? $zf_filter_input->emailadmin1 : $this->_getParam('emailadmin1');
         $this->view->phoneadmin1  = ($zf_filter_input->isValid('phoneadmin1'))? $zf_filter_input->phoneadmin1 : $this->_getParam('phoneadmin1');
         $this->view->tokenadmin1  = ($zf_filter_input->isValid('tokenadmin1'))? $zf_filter_input->tokenadmin1 : $this->_getParam('tokenadmin1');
         $this->view->admin2  = ($zf_filter_input->isValid('admin2'))? $zf_filter_input->admin2 : $this->_getParam('admin2');
         $this->view->emailadmin2  = ($zf_filter_input->isValid('emailadmin2'))? $zf_filter_input->emailadmin2 : $this->_getParam('emailadmin2');
         $this->view->phoneadmin2  = ($zf_filter_input->isValid('phoneadmin2'))? $zf_filter_input->phoneadmin2 : $this->_getParam('phoneadmin2');
         $this->view->tokenadmin2  = ($zf_filter_input->isValid('tokenadmin2'))? $zf_filter_input->tokenadmin2 : $this->_getParam('tokenadmin2');

         // $this->view->admin3  = ($zf_filter_input->isValid('admin3'))? $zf_filter_input->admin3 : $this->_getParam('admin3');
         // $this->view->emailadmin3  = ($zf_filter_input->isValid('emailadmin3'))? $zf_filter_input->emailadmin3 : $this->_getParam('emailadmin3');
         // $this->view->phoneadmin3  = ($zf_filter_input->isValid('phoneadmin3'))? $zf_filter_input->phoneadmin3 : $this->_getParam('phoneadmin3');
         // $this->view->tokenadmin3  = ($zf_filter_input->isValid('tokenadmin3'))? $zf_filter_input->tokenadmin3 : $this->_getParam('tokenadmin3');

         $this->view->cust_rdn_key  = ($zf_filter_input->isValid('cust_rdn_key'))? $zf_filter_input->cust_rdn_key : $this->_getParam('cust_rdn_key');

         $this->view->cust_role_adm = ($zf_filter_input->isValid('cust_role_adm'))? $zf_filter_input->cust_role_adm : $this->_getParam('cust_role_adm');


         // $this->view->tokenadmin2  = ($zf_filter_input->isValid('tokenadmin2'))? $zf_filter_input->tokenadmin2 : $this->_getParam('tokenadmin2');




        //$this->view->cust_charges_status     = ($zf_filter_input->isValid('cust_charges_status'))? $zf_filter_input->cust_charges_status : $this->_getParam('cust_charges_status');
        //$this->view->cust_monthlyfee_status   = ($zf_filter_input->isValid('cust_monthlyfee_status'))? $zf_filter_input->cust_monthlyfee_status : $this->_getParam('cust_monthlyfee_status');
        //$this->view->cust_token_auth   = ($zf_filter_input->isValid('cust_token_auth'))? $zf_filter_input->cust_token_auth : $this->_getParam('cust_token_auth');

        $error = $zf_filter_input->getMessages();

        //format error utk ditampilkan di view html
        $errorArray = null;
        foreach($error as $keyRoot => $rowError)
        {
           foreach($rowError as $errorString)
           {
              $errorArray[$keyRoot] = $errorString;
           }
        }

        if($flag_user_id == 'F')  $errorArray['cust_id'] = $this->language->_('Invalid Company Code format');

        if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['cust_email'] = $this->language->_('Invalid format');

        $this->view->customer_msg  = $errorArray;


	  }
    }//END if($this->_request->isPost())
	else if($this->_getParam('pdf')){

		foreach ($this->_request->getParams() as $key => $value) {
			$this->view->$key = $value;
		}

		$setting = new Settings();
		$selectAggrement = $this->_db->select()
						->from('M_SETTING')
						->where('SETTING_ID = ?','ftemplate_agreement');
		$terms = $this->_db->fetchRow($selectAggrement);

		$templateEmailMasterBankName = $setting->getSetting('master_bank_email');
		$terms['SETTING_VALUE'] = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankName,$terms['SETTING_VALUE']);
		
		$this->view->terms = $terms['SETTING_VALUE'];

		$outputHTML = "<tr><td>" . $this->view->render('/new/indexpdf.phtml') . "</td></tr>";

		$this->_helper->download->newPdf(null, null, null, $this->language->_('New_Customer_').$this->view->cust_name, $outputHTML);
	}
    else
    {
        /*$this->view->cust_charges_status  = 0;
        $this->view->cust_monthlyfee_status   = 0;
        $this->view->cust_token_auth   = 0;*/
        $this->view->country_code         = 'ID';
    }
		$this->view->button = true;


	// $this->view->cust_securities = (isset($this->_getParam('cust_securities')))? $this->_getParam('cust_securities') : '0';
	if(!empty($this->_getParam('cust_securities'))){
		$this->view->cust_securities = 	$this->_getParam('cust_securities');
	}else{
		$this->view->cust_securities = '0';
	}
	// if(isSet($this->_getParam('cust_securities'))) $this->view->cust_securities = $this->language->_('Invalid format');
    $this->view->modulename      =  $this->_request->getModuleName();
    $this->view->customer_type   =  $this->_custType;
    $this->view->approve_type    =  $this->_masterhasStatus;
    $this->view->lengthCustId    =  $this->_custIdLength;




  }

  	public function oldAction()
    {
		$this->_helper->layout()->setLayout('newlayout');
        //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

        $this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');

        $error_remark = null; $key_value = null;

        //convert limit idr & usd agar bisa masuk ke database
	    $limitidr = $this->_getParam('cust_limit_idr');
	    $limitidr = Application_Helper_General::convertDisplayMoney($limitidr);
	    $this->_setParam('cust_limit_idr',$limitidr);

	    $limitusd = $this->_getParam('cust_limit_usd');
	    $limitusd = Application_Helper_General::convertDisplayMoney($limitusd);
	    $this->_setParam('cust_limit_usd',$limitusd);
	    //END convert limit idr & usd agar bisa masuk ke database


		if($this->_request->isPost() && $this->_getParam('submit') == $this->language->_('Submit'))
		{
			$this->view->inputParam = $this->getRequest()->getParams();
			$cust_id = strtoupper($this->_getParam('cust_id'));
			$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>12)))? $cust_id : null;

		    /*echo strtoupper($this->_getParam('cust_id'));
			die;*/

			$customer_filter = array();
			$haystack_cust_type = array($this->_custType['code']['company'],$this->_custType['code']['individual']);
			$haystack_step_approve = array($this->_masterhasStatus['code']['yes'],$this->_masterhasStatus['code']['no']);
			$cust_acc = array();
			$cust_limit = array();
  	   		$tempacc = array('idr','usd');

			$filters = array('cust_id'           => array('StripTags','StringTrim','StringToUpper'),
			                 'cust_cif'          => array('StripTags','StringTrim'),
			                 'cust_name'         => array('StripTags','StringTrim'),
			                 'cust_type'         => array('StripTags','StringTrim','StringToUpper'),
			                 'cust_workfield'    => array('StripTags','StringTrim'),
						     'cust_address'      => array('StripTags','StringTrim'),
			                 'cust_city'         => array('StripTags','StringTrim'),
			                 'cust_zip'          => array('StripTags','StringTrim'),
			                 'country_code'      => array('StripTags','StringTrim'),
							 'cust_province'     => array('StripTags','StringTrim'),
			                 'cust_contact'      => array('StripTags','StringTrim'),
			                 'cust_phone'        => array('StripTags','StringTrim'),
						     'cust_ext'          => array('StripTags','StringTrim'),
			                 'cust_fax'          => array('StripTags','StringTrim'),
						     'cust_email'        => array('StripTags','StringTrim'),
			                 'cust_website'      => array('StripTags','StringTrim'),
			                 'cust_limit_idr'      => array('StripTags','StringTrim'),
			                 'cust_limit_usd'      => array('StripTags','StringTrim'),
			                //'cust_charges_status' => array('StripTags','StringTrim'),
			                 'cust_special_rate'   => array('StripTags','StringTrim'),
			                 //'cust_monthlyfee_status'  => array('StripTags','StringTrim'),
			                 //'cust_token_auth'  => array('StripTags','StringTrim'),
							 /* 'cust_emobile'        => array('StripTags','StringTrim'),
			                 'cust_isemobile'      => array('StripTags','StringTrim'), */
			                );

			$validators =  array('cust_id'        => array('NotEmpty',
      	                                                   'Alnum',
      	                                                    array('StringLength',array('min'=>6,'max'=>16)),
      	                                                       array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_ID')),
      	                                                       array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_ID')),
      	                                                       'messages' => array($this->language->_('Can not be empty'),
      	                                                                           $this->language->_('Invalid Company Code Format'),
//      	                                                                           $this->language->_('Invalid Company Code Format'),
																						$this->language->_('Company Code length min').' 6 '.$this->language->_('max') . '16',
      	                                                                           $this->language->_('Company code already in use. Please use another'),
      	                                                                           $this->language->_('Company code already suggested. Please use another'))
      	                                                      ),

      	                     'cust_cif'                 => array(
      	                                                      //array('StringLength',array('min'=>1,'max'=>19)),
												               array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_CIF')),
                                                               array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_CIF')),
												               'allowEmpty' => true,
												               'messages' => array(//$this->getErrorRemark('04','Company CIF'),
                                                                                   $this->language->_('CIF is already registered. Please use another'),
                                                                                   $this->language->_('CIF is already suggested. Please use another')
                                                                                   )
												              ),

							 'cust_name'                => array('NotEmpty',
												                //array('StringLength',array('min'=>1,'max'=>80)),
												               'messages' => array($this->language->_('Can not be empty'),
								 					  	      	         		   //$this->getErrorRemark('04','Company Name')
								 					  	      	         		   )
												              ),

                             'cust_type'                => array('allowEmpty' => true,
												              	 array('StringLength',array('max'=>16)),
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												              	 				   $this->language->_('The maximum characters allowed is').' 16',
												                                   //$this->getErrorRemark('22','Company Type')
												                                   )
											                  ),


                             'cust_workfield'            => array('allowEmpty' => true,
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												                                   //$this->getErrorRemark('22','Company Type')
												                                   )
											                  ),


						     'cust_address'            => array('allowEmpty' => true,
												                array('StringLength',array('max'=>200)),
												               'messages' => array(
												                					$this->language->_('The maximum characters allowed is').' 200'
												                                  // $this->getErrorRemark('04','Company Address')
												                                   )
												              ),



						     'cust_city'               => array(array('StringLength',array('min'=>1,'max'=>20)),
												               'allowEmpty' => true,
												               'messages' => array(
												                                   $this->language->_('The maximum characters allowed is').' 20'
												                                  )
												              ),

						     'cust_zip'           => array(//array('StringLength',array('min'=>1,'max'=>10)),
												               'allowEmpty' => true,
						     									'Digits',
												               'messages' => array(
												                                // $this->getErrorRemark('04','Zip')
												                                  )
												              ),

						     'country_code'           => array(//array('StringLength',array('min'=>1,'max'=>10)),
												               'allowEmpty' => true,
												               'messages' => array(
												                              // $this->getErrorRemark('04','Country')
												                                  )
												              ),

                           'cust_province'      => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
												               'messages' => array($this->language->_('The maximum characters allowed is').' 128')
												              ),

                           'cust_contact'       => array('allowEmpty' => true,
												         array('StringLength',array('min'=>1,'max'=>128)),
												         'messages' => array(
												                             $this->language->_('The maximum characters allowed is').' 128')
												         ),

						    'cust_phone'        => array('Digits',
												         array('StringLength',array('min'=>1,'max'=>20)),
												         'allowEmpty' => true,
												         'messages' => array($this->language->_('Invalid phone number format'),
												                             $this->language->_('The maximum characters allowed is').' 20'
												                            )
												              ),

                            'cust_ext'          => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
                            									'Digits',
												               'messages' => array(
												                                  $this->language->_('The maximum characters allowed is').' 128'
												                                   )
												              ),

                            'cust_fax'          => array('Digits',
												         'allowEmpty' => true,
												         'messages' => array(
												                            $this->language->_('Invalid fax number format')
												                            )
												              ),

                            'cust_email'        => array( //new Application_Validate_EmailAddress(),
												          array('StringLength',array('min'=>1,'max'=>128)),
												          'allowEmpty' => true,
												          'messages' => array(
												                             //$this->language->_('Invalid email format'),
												                             $this->language->_('Email lenght cannot be more than 128'),
												                             )
                                                        ),

                           //  'cust_website'      => array(new Application_Validate_Hostname(),
                           //                                     array('StringLength',array('min'=>1,'max'=>100)),
												               // 'allowEmpty' => true,
                           //                                     'messages' => array($this->language->_('Invalid URL format'),
                           //                                                         $this->language->_('invalid format'))
                           //                                    ),

                            'cust_website'      => 	array('allowEmpty' => true,
												              	 // array('StringLength',array('max'=>16)),
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												              	 				   // $this->language->_('The maximum characters allowed is').' 16',
												                                   // $this->getErrorRemark('22','Company Type')
												                                   )
											                  ),

                           /* 'cust_charges_status'      => array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                    $this->language->_('invalid format')
                                                                                          )
                                                              ),

                             'cust_monthlyfee_status'   => array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                   $this->language->_('invalid format')
                                                                                          )
                                                              ),
                             'cust_token_auth'      	=> array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                   $this->language->_('invalid format')
                                                                                          )
                                                              ),*/


                             'cust_special_rate'      	=> array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                       $this->language->_('invalid format')
                                                                                          )
                                                              ),
                            'cust_limit_idr' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 1 - 9999999999.99',
															'Must be numeric values',
															)),
							'cust_limit_usd' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 1 - 9999999999.99',
															'Must be numeric values',
															)),

                            /*  'cust_emobile'      => array(
                                                          //array('StringLength',array('min'=>1,'max'=>100)),
                                                          array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_EMOBILE')),
      	                                                  array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_EMOBILE')),
                                                          'allowEmpty' => true,
                                                          'messages' => array(
                                                                            'Emobile No. already assigned in other company.',
      	                                                                    'Emobile No already suggested. Please use another'
                                                                             )
                                                          ),

                            'cust_isemobile'      => array(//array('StringLength',array('min'=>1,'max'=>100)),
                                                           'Digits',
												           'allowEmpty' => true,
                                                           'messages' => array(
                                                                               'Invalid emobile number format',
                                                                              )
                                                           ),  */
							);



       $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		//validasi user_id : paling sedikit harus punya 1 huruf dan 1 angka
	      if($zf_filter_input->cust_id)
	      {
	         $user_id =  $zf_filter_input->cust_id;
	         $check_alpha  = 0;
	         $check_digits = 0;
	         $strlen_user_id = strlen($user_id);

	         for($i=0; $i<$strlen_user_id; $i++)
	         {
	            if(Zend_Validate::is($user_id[$i],'Alpha'))  $check_alpha  = 1;
	            if(Zend_Validate::is($user_id[$i],'Digits')) $check_digits = 1;
	         }

	         if($check_alpha == 1 && $check_digits == 1) $flag_user_id = 'T';
	         else $flag_user_id = 'F';
	      }
	      else
	      {
	         $flag_user_id = 'T';
	      }

		//validasi multiple email
	      if($this->_getParam('cust_email'))
	      {
			$validate = new validate;
	      	$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('cust_email'));
	      }
	      else
	      {
	      	$cek_multiple_email = true;
	      }

       //validasi emobile, cust phone dan cust fax = harus dimulai dengan '62'
	   $flag_cust_emobile = 'T';
       /* $flag_cust_isemobile_error = false;
	   if($zf_filter_input->cust_emobile)
       {
           $cust_emobile_62 = substr($zf_filter_input->cust_emobile,0,2);

           if($cust_emobile_62 != '62') $flag_cust_emobile = 'F';
           else $flag_cust_emobile = 'T';
       }
       else
       {
           if($zf_filter_input->cust_isemobile == '' || is_null($zf_filter_input->cust_isemobile) || $zf_filter_input->cust_isemobile == 0)
           {
              $flag_cust_emobile = 'T';
           }
           else
           {
              $flag_cust_emobile = 'F';
              $flag_cust_isemobile_error = true;
           }
        } */


       $flag_cust_phone = 'T';
      /* if($zf_filter_input->cust_phone)
       {
          $cust_phone_62   = substr($zf_filter_input->cust_phone,0,2);

          if($cust_phone_62 != '62') $flag_cust_phone = 'F';
          else $flag_cust_phone = 'T';
       }
       else $flag_cust_phone = 'T';*/

       $flag_cust_fax = 'T';
       /*if($zf_filter_input->cust_fax)
       {
          $cust_fax_62     = substr($zf_filter_input->cust_fax,0,2);

          if($cust_fax_62 != '62') $flag_cust_fax = 'F';
          else $flag_cust_fax = 'T';
       }
       else $flag_cust_fax = 'T';*/
      //END validasi emobile = harus dimulai dengan '62'






       if($zf_filter_input->isValid() && $flag_user_id == 'T' && $flag_cust_emobile == 'T' && $flag_cust_phone == 'T' && $flag_cust_fax == 'T' && $cek_multiple_email == true)
       {
      	  $info = 'Customer ID = '.$zf_filter_input->cust_id.', Customer Name = '.$zf_filter_input->cust_name;
          $cust_data = $this->_custData;

	  	  foreach($validators as $key=>$value)
	  	  {
	  	    if($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;
	  	  }


	  	  try
	      {
		     $this->_db->beginTransaction();

		     if(is_null($cust_data['CUST_CHARGES_STATUS']) || $cust_data['CUST_CHARGES_STATUS'] == '') $cust_data['CUST_CHARGES_STATUS'] = 0;
		     if(is_null($cust_data['CUST_MONTHLYFEE_STATUS']) || $cust_data['CUST_MONTHLYFEE_STATUS'] == '')   $cust_data['CUST_MONTHLYFEE_STATUS'] = 0;
		     if(is_null($cust_data['CUST_TOKEN_AUTH']) || $cust_data['CUST_TOKEN_AUTH'] == '')   $cust_data['CUST_TOKEN_AUTH'] = 0;
		     if(is_null($cust_data['CUST_SPECIAL_RATE']) || $cust_data['CUST_SPECIAL_RATE'] == '')     $cust_data['CUST_SPECIAL_RATE'] = 0;
		      if(is_null($cust_data['CUST_LIMIT_IDR']) || $cust_data['CUST_LIMIT_IDR'] == '')     $cust_data['CUST_LIMIT_IDR'] = 1;
		      if(is_null($cust_data['CUST_LIMIT_USD']) || $cust_data['CUST_LIMIT_USD'] == '')     $cust_data['CUST_LIMIT_USD'] = 1;
		     // if(is_null($cust_data['CUST_ISEMOBILE']) || $cust_data['CUST_ISEMOBILE'] == '')           $cust_data['CUST_ISEMOBILE'] = 0;



		     $cust_data['CUST_STATUS'] = 1;
		     $cust_data['CUST_CREATED']     = null;
		     $cust_data['CUST_CREATEDBY']   = null;
		     $cust_data['CUST_UPDATED']     = null;
		     $cust_data['CUST_UPDATEDBY']   = null;


		     $cust_data['CUST_SUGGESTED']    = new Zend_Db_Expr('now()');
		     $cust_data['CUST_SUGGESTEDBY']  = $this->_userIdLogin;


		     /* Zend_Debug::dump( $cust_data );
	          die;*/

		     // insert ke T_GLOBAL_CHANGES
		     $change_id = $this->suggestionWaitingApproval('Customer',$info,$this->_changeType['code']['new'],null,'M_CUSTOMER','TEMP_CUSTOMER',$zf_filter_input->cust_id,$zf_filter_input->cust_name,$zf_filter_input->cust_id,$zf_filter_input->cust_name);
		     $this->insertTempCustomer($change_id,$cust_data);


		     //if(count($customer_filter)) $this->setUserData($customer_filter,$zf_filter_input,$change_id);
		  	 //if($countacc) $this->insertTempAcc($countacc,$change_id,$cust_acc);
		  	 //if(count($cust_limit)) $this->insertTempLimit($change_id,$cust_limit);

		     //log CRUD
			 Application_Helper_General::writeLog('CCAD','Customer has been added, Cust ID : '.$zf_filter_input->cust_id. ' Cust Name : '.$zf_filter_input->cust_name.' Change id : '.$change_id);

		     $this->_db->commit();

		     $this->_redirect('/notification/submited/index');
		     //$this->_redirect('/notification/success/index');
	  	  }
		  catch(Exception $e)
		  {
		    $this->_db->rollBack();
		    $error_remark = 'exception';
	      }


		  if(isset($error_remark))
		  {
		  }
		  else
		  {
		  	die('here');
		    $this->view->success = 1;
		    $msg   = 'Record added sucessfully';
		    $key_value = $zf_filter_input->cust_id;
		    $this->view->customer_msg = $msg;
		   //insert log
		   try
		   {
			  $this->_db->beginTransaction();

			  Application_Helper_General::writeLog('CCAD',' Create Customer ['.$this->view->cust_id.']');

			  $this->_db->commit();
		   }
		   catch(Exception $e)
		   {
			  $this->_db->rollBack();
			  Application_Log_GeneralLog::technicalLog($e);
		   }
		  }
	  }// END IF IS VALID
	  else
	  {
	  	// die('here2');
	    $this->view->error = 1;
	  	$this->view->cust_id   = ($zf_filter_input->isValid('cust_id'))? $zf_filter_input->cust_id : $this->_getParam('cust_id');
	  	$this->view->cust_name = ($zf_filter_input->isValid('cust_name'))? $zf_filter_input->cust_name : $this->_getParam('cust_name');
	    $this->view->cust_type = ($zf_filter_input->isValid('cust_type'))? $zf_filter_input->cust_type : $this->_getParam('cust_type');
	    $this->view->cust_cif = ($zf_filter_input->isValid('cust_cif'))? $zf_filter_input->cust_cif : $this->_getParam('cust_cif');
	    $this->view->cust_special = ($zf_filter_input->isValid('cust_special'))? $zf_filter_input->cust_special : $this->_getParam('cust_special');
	    
	    // print_r($zf_filter_input->cust_cif);
	    // print_r($this->_getParam('cust_cif'));die;
	  	$this->view->cust_workfield = ($zf_filter_input->isValid('cust_workfield'))? $zf_filter_input->cust_workfield : $this->_getParam('cust_workfield');
		$this->view->cust_address = ($zf_filter_input->isValid('cust_address'))? $zf_filter_input->cust_address : $this->_getParam('cust_address');
		$this->view->cust_city = ($zf_filter_input->isValid('cust_city'))? $zf_filter_input->cust_city : $this->_getParam('cust_city');
		$this->view->cust_zip  = ($zf_filter_input->isValid('cust_zip'))? $zf_filter_input->cust_zip : $this->_getParam('cust_zip');
		$this->view->cust_province = ($zf_filter_input->isValid('cust_province'))? $zf_filter_input->cust_province : $this->_getParam('cust_province');
		$this->view->country = ($zf_filter_input->isValid('country'))? $zf_filter_input->country : $this->_getParam('country');
		$this->view->cust_contact  = ($zf_filter_input->isValid('cust_contact'))? $zf_filter_input->cust_contact : $this->_getParam('cust_contact');
		$this->view->cust_phone    = ($zf_filter_input->isValid('cust_phone'))? $zf_filter_input->cust_phone : $this->_getParam('cust_phone');
		$this->view->cust_ext      = ($zf_filter_input->isValid('cust_ext'))? $zf_filter_input->cust_ext : $this->_getParam('cust_ext');
		$this->view->cust_fax      = ($zf_filter_input->isValid('cust_fax'))? $zf_filter_input->cust_fax : $this->_getParam('cust_fax');
        $this->view->cust_email    = ($zf_filter_input->isValid('cust_email'))? $zf_filter_input->cust_email : $this->_getParam('cust_email');
        $this->view->cust_website  = ($zf_filter_input->isValid('cust_website'))? $zf_filter_input->cust_website : $this->_getParam('cust_website');
        $this->view->country_code  = ($zf_filter_input->isValid('country_code'))? $zf_filter_input->country_code : $this->_getParam('country_code');
         $this->view->cust_limit_idr  = ($zf_filter_input->isValid('cust_limit_idr'))? $zf_filter_input->cust_limit_idr : $this->_getParam('cust_limit_idr');
         $this->view->cust_limit_usd  = ($zf_filter_input->isValid('cust_limit_usd'))? $zf_filter_input->cust_limit_usd : $this->_getParam('cust_limit_usd');

        //$this->view->cust_charges_status     = ($zf_filter_input->isValid('cust_charges_status'))? $zf_filter_input->cust_charges_status : $this->_getParam('cust_charges_status');
        //$this->view->cust_monthlyfee_status   = ($zf_filter_input->isValid('cust_monthlyfee_status'))? $zf_filter_input->cust_monthlyfee_status : $this->_getParam('cust_monthlyfee_status');
        //$this->view->cust_token_auth   = ($zf_filter_input->isValid('cust_token_auth'))? $zf_filter_input->cust_token_auth : $this->_getParam('cust_token_auth');

        $error = $zf_filter_input->getMessages();

        //format error utk ditampilkan di view html
        $errorArray = null;
        foreach($error as $keyRoot => $rowError)
        {
           foreach($rowError as $errorString)
           {
              $errorArray[$keyRoot] = $errorString;
           }
        }

        if($flag_user_id == 'F')  $errorArray['cust_id'] = $this->language->_('Invalid Company Code format');

        if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['cust_email'] = $this->language->_('Invalid format');
        die('heres');
        $this->view->customer_msg  = $errorArray;


	  }
    }//END if($this->_request->isPost())
	else if($this->_getParam('pdf') == 'PDF'){
		Application_Helper_General::writeLog('CSAD','Download PDF Create Customer User [BO]');
		$this->view->button = false;
		$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/index.phtml')."</td></tr>";
		$this->_helper->download->pdf(null,null,null,'Create Customer User',$outputHTML);
		/*$this->view->cust_charges_status  = '';
        $this->view->cust_monthlyfee_status = '';
        $this->view->cust_token_auth = '';*/

        $this->view->country_code = '';
	}
    else
    {
        /*$this->view->cust_charges_status  = 0;
        $this->view->cust_monthlyfee_status   = 0;
        $this->view->cust_token_auth   = 0;*/
        $this->view->country_code         = 'ID';
    }
		$this->view->button = true;




    $this->view->modulename      =  $this->_request->getModuleName();
    $this->view->customer_type   =  $this->_custType;
    $this->view->approve_type    =  $this->_masterhasStatus;
    $this->view->lengthCustId    =  $this->_custIdLength;




  }


  public function cifAction(){
  	 $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $areaArr = $select = $this->_db->select()
                           ->distinct()
                           ->from('M_SERVICE_AREA',array('AREA_NAME'))
                           ->query()->fetchAll();
        //$this->view->branchArr = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($model->getBranch(),"CODE","NAME");
        $area = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($areaArr,"AREA_NAME","AREA_NAME");
        // print_r($area);die;
        foreach ($area as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
  }

  public function cifaccountAction(){


  	 $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
         	 $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];
       // return $app;

        $core = array();
    $svcAccount = new Service_Account($tblName, Application_Helper_General::getCurrNum('IDR'), $app);
    $result   = $svcAccount->cifaccountInquiry($core);
    
    // echo '<pre>';
    // print_r($result);
    // print_r($core);die;
    if($core->responseCode == '0000' || $result['ResponseCode']=='0000' ){
          $arrResult = array();
          $err = array();

          if(count($core->accountList) > 1){
//            print_r($core->accountList);
            //array data lebih dari 1
            foreach ($core->accountList as $key => $val){
              if($val->status == '0'){
                $arrResult = array_merge($arrResult, array($val->accountNo => $val));
                array_push($err, $val->accountNo);
              }
//              echo 'here';
            }
          }
          else{
            //array kurang dari 2
            foreach ($core as $data){
              if($data->status == '0'){
                $arrResult = array_merge($arrResult, array($data->accountNo => $data));
                array_push($err, $data->accountNo);
              }
//              echo 'here1';
//            Zend_Debug::dump($data);
            }
          }

        $final = $arrResult;
        if(count($final) < 1){
          $this->view->data1 = '0';
        }
        else{
          $this->view->data = $arrResult;
        }

//        $dataPro = array();
        $dataProPlan = array('xxxxx');
          foreach ($final as $dataProductPlan){
            // print_r($dataProductPlan);die;
              if($dataProductPlan->status == '0'){
                if (isset($dataProductPlan->productType))
                {
                $select_product_type  = $this->_db->select()
                      ->from(array('M_PRODUCT_TYPE'),array('PRODUCT_NAME'))
                      ->where("PRODUCT_CODE = ?", $dataProductPlan->productType);
        //              ->where('PRODUCT_PLAN = ?','62');
                      // echo $select_product_type;
                $userData_product_type = $this->_db->fetchAll($select_product_type);
  //
                // print_r($userData_product_type);
                $dataProductPlan->planName = $userData_product_type[0]['PRODUCT_NAME'];
                // $dataProductPlanOri = (($dataProductPlan->planName == null)?'':$dataProductPlan);
                }else{
                  $dataProductPlan = $dataProductPlan;
                }

              // print_r($dataProductPlan);die;
             $dataUser[] = $dataProductPlan;
              }
          }

     }

        $optHtml = "<table cellpadding='3' cellspacing='0' border='0' class='table table-sm table-striped'><tr class='headercolor'><th width='3%' valign='top' class='tablehead fth-header'></th><th  valign='top'  class='tablehead fth-header'>Account Number</th><th  valign='top'  class='tablehead fth-header'>Account Name</th><th  valign='top'  class='tablehead fth-header'>Currency</th><th  valign='top'  class='tablehead fth-header'>Type</th></tr>";


        if(!empty($dataUser)){
        	// print_r($dataUser);die();
        foreach($dataUser as $key => $row){
             $i = $i + 1;
            $acct_source = 3;
            $acct_desc   = $row->planName;

            if ($row->ccy == '000') {
            	$cekIDR = 'IDR';
            }

            $button_checkbox = "<input type='checkbox' name='req_id[]' id='".$row->accountNo."' value='".$row->accountNo."'>";
            // $button_checkbox = $this->formCheckbox("req_id[{$row->accountNo}]",null);
            $td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
            // if ($this->data[$key] != ""){
                if($row->accountNo == ''){
                }
                else{
                    // $optHtml .= '<tr>td class="'.$td_css.'"><input type="checkbox" name="req_id[]" id="'.$row->accountNo.'" onclick="check()" value="'.$row->accountNo.'"></td><td class="'.$td_css.'">'.$row->accountNo.'</td><td class="'.$td_css.'"><input type="hidden" name="accountName'.$row->accountNo.'" value="'.$row->accountName.'">'.$row->accountName.'</td>';
                    $optHtml .= '<tr><td class="'.$td_css.'"><input type="checkbox" name="req_id[]" id="'.$row->accountNo.'"  value="'.$row->accountNo.'"></td><td class="'.$td_css.'">'.$row->accountNo.'</td><td class="'.$td_css.'"><input type="hidden" name="accountName'.$row->accountNo.'" id="accountName'.$row->accountNo.'" value="'.$row->accountName.'">'.$row->accountName.'</td><td class="'.$td_css.'"><input type="hidden" name="ccy'.$row->accountNo.'" id="ccy'.$row->accountNo.'" value="'.$cekIDR.'">'.$cekIDR.'</td><td class="'.$td_css.'"><input type="hidden" name="acct_desc'.$row->accountNo.'" id="acct_desc'.$row->accountNo.'" value="'.$acct_desc.'"><input type="hidden" name="productType'.$row->accountNo.'" value="'.$row->productType.'">'.$acct_desc.'<input type="hidden" name="planCode'.$row->accountNo.'" value="'.$row->planCode.'"><input type="hidden" name="ccy'.$row->ccy.'" value="'.$row->planCode.'"><input type="hidden" name="productName'.$row->accountNo.'" id="productName'.$row->accountNo.'" value="'.$acct_desc.'"></td></tr>';

                }
            // }
        }
        }else{
        	// if($this->data == ''){
        $optHtml .= '<tr><td colspan="5" class="tbl-evencontent" align="center">'.$this->language->_('No Data').'</td></tr>';

        }
        $optHtml .= '</table>';
        echo $optHtml;
  }


}
