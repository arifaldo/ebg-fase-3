<?php
require_once 'Zend/Controller/Action.php';

class notification_SuccessdownloadController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$ns = new Zend_Session_Namespace('FVC');
		$this->_helper->_layout->setLayout('newlayout');
			$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
			$param  =                      $this->_request->getParams();
			// print_r($param);die;
        	$data = $sessionNamespace->content;
        	// print_r($data);die;
        	$header = $data['header'];
        	$dataexcel = $data['dataexcel'];
        	if($param['print']=='1'){

        			$result = $this->_helper->download->csv($header,$dataexcel,null,'Payroll Adapter');  
            		Application_Helper_General::writeLog('DARC','Export CSV View Payment');    	
            		$this->view->print = '1';
        	}
		    

		if (isset($ns->backURL) && !(empty($ns->backURL))){
			$this->view->backURL = $ns->backURL;
		}
		
		$ns->backURL = '';
	}
}
