<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';

class newexchangerate_IndexController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$this->setbackURL();

		$selectCcy = $this->_db->select()
                     ->distinct()
                     ->from(array('C' => 'M_CURRENCY'), '*');

        $dataCcy = $this->_db->fetchAll($selectCcy);

        $ccyOpt = '';
        foreach ($dataCcy as $key => $value) {
        	$selected = '';
        	if ($value['CCY_ID'] == 'IDR') {
        		$selected = 'selected';
        	}
        	$ccyOpt .= '<option value="'.$value['CCY_ID'].'" '.$selected.'>'.$value['CCY_ID'].' ('.$value['CCY_DESCRIPTION'].')</option>';
        }

        $this->view->ccyOpt = $ccyOpt;

        $save 	= $this->_getParam('submit');

        if ($save) {
        	
        	$this->_redirect('/notification/success/comingsoon');

        }

        //call api exchange rate
  		//$clientUser  =  new SGO_Soap_ClientUser();
		// $success = $clientUser->callapi('exchangerate',$request,'http://192.168.86.26:20809/b2b/inquiry/exchangerate');
		
		// if($clientUser->isTimedOut()){
		// 	$returnStruct = array(
		// 			'ResponseCode'	=>'XT',
		// 			'ResponseDesc'	=>'Service Timeout',					
		// 			'Cif'			=>'',
		// 			'AccountList'	=> '',
		// 	);
		// }else{
		// 	$result  = $clientUser->getResult();
		// 	// print_r($result);die;
		// 	if($result->error_code == '0000'){
				
		// 		//if success to do here

		// 	}else{
		// 		//else
		// 	}
		// }
	}

	public function depositAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$selectCcy = $this->_db->select()
                     ->distinct()
                     ->from(array('C' => 'M_CURRENCY'), '*');

        $dataCcy = $this->_db->fetchAll($selectCcy);

        $ccyOpt = '';
        foreach ($dataCcy as $key => $value) {
        	$selected = '';
        	if ($value['CCY_ID'] == 'IDR') {
        		$selected = 'selected';
        	}
        	$ccyOpt .= '<option value="'.$value['CCY_ID'].'" '.$selected.'>'.$value['CCY_ID'].' ('.$value['CCY_DESCRIPTION'].')</option>';
        }

        $this->view->ccyOpt = $ccyOpt;
	}

}
