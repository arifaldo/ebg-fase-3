<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';
require_once 'General/CustomerUser.php';

class virtualpayment_UploadController extends Application_Main
{
	protected 	$_moduleDB 	= 'RTF';
	protected	$_rowNum 	= 0;
	protected	$_errmsg 	= null;	
	protected	$params		= null;
	
	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  	
	} 
	
	public function indexAction()
	{
		if($this->_request->isPost() )
		{			
			$adapter1 					= new Zend_File_Transfer_Adapter_Http();
			$params 					= $this->_request->getParams();
			$sourceFileName 			= $adapter1->getFileName();
			
			if($sourceFileName == null)
			{
				$sourceFileName = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter1->getFileName()), 0);
			}
			
			$paramsName['sourceFileName'] 	= $sourceFileName;
			
			$filtersName = array(
									'sourceFileName'		=> array('StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File size too big or left blank"
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			if($zf_filter_input_name->isValid())
			{
				$filter 	= new Application_Filtering();
				$adapter 	= new Zend_File_Transfer_Adapter_Http ();
				$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
//				$max		= $this->getSetting('max_import_single_payment');
				$max		= $this->getSetting('max_import_bulk');
				
				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
													'Error: Extension file must be *.csv'
												);
				
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
											'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
										);
						
				$adapter->setValidators(array($extensionValidator,$sizeValidator));
					
				if ($adapter->isValid ()) 
				{
					$sourceFileName = $adapter->getFileName();
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
						
					$adapter->addFilter ( 'Rename',$newFileName  );
						
					if ($adapter->receive ())
					{
						$Csv = new Application_Csv ($newFileName,",");
						$csvData = $Csv->readAll ();
//						Zend_Debug::dump($csvData);
//						die;
						
						@unlink($newFileName);
						
						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}
						
						if ($totalRecords){
							if($totalRecords <= $max)
							{
								$no =0;
								foreach ( $csvData as $columns )
								{
									if(count($columns)==8)
									{
										//$params['COMP_ID'] 				= trim($columns[0]);
										$params['INSTITUTIONS_ID'] 		= trim($columns[0]);
										$params['INVOICE_NAME'] 		= trim($columns[1]);
										$params['INVOICE_DESC'] 		= trim($columns[2]);
										$params['CCY'] 					= trim($columns[3]);
										$params['AMOUNT'] 				= trim($columns[4]);
										$params['AREA'] 				= trim($columns[5]);
										$params['BENEFICIARY_ACCOUNT'] 	= trim($columns[6]);
										$params['BENEFICIARY_NAME'] 	= trim($columns[7]);
											
										//$COMP_ID 			= $filter->filter($params['COMP_ID'],"CUST_ID");
										$INSTITUTIONS_ID 	= $filter->filter($params['INSTITUTIONS_ID'],"INSTITUTIONS_ID");
										$INVOICE_NAME 		= $filter->filter($params['INVOICE_NAME'],"INVOICE_NAME");
										$INVOICE_DESC 		= $filter->filter($params['INVOICE_DESC'],"INVOICE_DESC");
										$CCY 				= $filter->filter($params['CCY'],"VP_CCY");
										$AMOUNT 			= $filter->filter($params['AMOUNT'],"VP_AMOUNT");
//										$VP_MODE 			= $filter->filter($params['VP_MODE'],"VP_MODE");
										$AREA	 			= $filter->filter($params['AREA'],"AREA");
										$BENEFICIARY_ACCOUNT = $filter->filter($params['BENEFICIARY_ACCOUNT'],"BENEFICIARY_ACCOUNT");
										$BENEFICIARY_NAME 	= $filter->filter($params['BENEFICIARY_NAME'],"BENEFICIARY_NAME");
//										$PS_STATUS 			= $filter->filter($params['PS_STATUS'],"PS_STATUS");
											
										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($AMOUNT);
										
							
										$paramPayment = array(
												"CATEGORY" 					=> 'VIRTUAL PAYMENT',
												"FROM" 						=> "I",				// F: Form, I: Import
												//"PS_NUMBER"					=> "",
												//"PS_SUBJECT"				=> '',
												"PS_EFDATE"					=> date('d/m/y'),
												"_dateFormat"				=> $this->_dateUploadFormat,
												"_dateDBFormat"				=> $this->_dateDBFormat,
										);
							
										$paramTrxArr[0] = array(
												"TRANSFER_TYPE" 			=> 'VP',
												"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
												"ACBENEF_CCY" 				=> strtoupper($CCY),
												"COMP_ID" 					=> $this->_custIdLogin,
												"INSTITUTIONS_ID" 			=> $INSTITUTIONS_ID,
//												"ACBENEF" 					=> '23445678745', //
//												"ACBENEF_CCY" 				=> 'IDR',
//												"ACBENEF_ALIAS" 			=> 'TEST',
												
												"INVOICE_NAME" 				=> $INVOICE_NAME,
												"INVOICE_DESC" 				=> $INVOICE_DESC,
												"AREA" 						=> $AREA,
												"BENEFICIARY_ACCOUNT" 		=> $BENEFICIARY_ACCOUNT,
												"BENEFICIARY_NAME" 			=> $BENEFICIARY_NAME,
										);
											
										$arr[$no]['paramPayment'] = $paramPayment;
										$arr[$no]['paramTrxArr'] = $paramTrxArr;
									}
									else
									{
										$this->view->error 		= true;
										break;
									}
									$no++;
								}
									
								if(!$this->view->error)
								{
							
									$resultVal	= $validate->checkCreateVp($arr);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();
									//Zend_Debug::dump($payment);die;
									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
//									Zend_Debug::dump($errorTrxMsg);
//									die;
									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;
										
									$this->_redirect('/virtualpayment/upload/confirm');
								}
									
							}
							else
							{
								$this->view->error2 = true;
								$this->view->max 	= $max;
							}
						}else{
							$this->view->error = true;
						}
					}
				}
				else
				{
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}
			}
			else
			{
				$this->view->error3		= true;
			}
		}
		Application_Helper_General::writeLog('IFVP','Viewing Virtual Payment by Import File (CSV)');
		//Application_Helper_General::writeLog('CRIP','Viewing Create Single Payment In House by Import File (CSV)');
	}
	
	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
		$data = $sessionNamespace->content;
//		Zend_Debug::dump($data);		
//		die;
		if(!$data["payment"]["countTrxCCY"])
		{
			$this->_redirect("/authorizationacl/index/nodata");
		}
		
		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];
		
		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}
		
		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}
		
		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}
		
		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}
		
		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;
		
		$fields = array(
						'PaymentType'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Type'),
														'sortable' => false
													),
						'CCY'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('CCY'),
														'sortable' => false
													),
						'Payment Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Success'),
														'sortable' => false
													),
						'Total Amount Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Success'),
														'sortable' => false
													),
						'Payment Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Failed'),
														'sortable' => false
													),
						'Total Amount Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Failed'),
														'sortable' => false
													),
                        );
		$this->view->fields = $fields;
		
		if($this->_request->isPost() )
		{			
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmImportCredit']); 
				$this->_redirect('/virtualpayment/upload');
			}
			
			foreach($data["arr"] as $row)
			{
//				Zend_Debug::dump($data["arr"]);
//				die;
				$param['COMP_ID'] 			= $this->_userIdLogin;
				//$param['PS_EFDATE']  		= Application_Helper_General::convertDate($row['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateUploadFormat);
				$param['INSTITUTIONS_ID'] 	= $row['paramTrxArr'][0]['INSTITUTIONS_ID'];
				$param['INVOICE_NAME']  	= $row['paramTrxArr'][0]['INVOICE_NAME'];
				$param['INVOICE_DESC']		= $row['paramTrxArr'][0]['INVOICE_DESC'];
				$param['CCY'] 				= $row['paramTrxArr'][0]['ACBENEF_CCY'];
				$param['AMOUNT'] 			= $row['paramTrxArr'][0]['TRA_AMOUNT'];
				$param['AREA'] 				= $row['paramTrxArr'][0]['AREA'];
				$param['BENEFICIARY_ACCOUNT'] = $row['paramTrxArr'][0]['BENEFICIARY_ACCOUNT'];
				$param['BENEFICIARY_NAME'] 	  = $row['paramTrxArr'][0]['BENEFICIARY_NAME'];

//				$param['_addBeneficiary'] 			= $row['paramPayment']['_addBeneficiary'];
//				$param['_beneLinkage'] 				= $row['paramPayment']['_beneLinkage'];
//				$param["_dateFormat"]				= $row['paramPayment']['_dateFormat'];
//				$param["_dateDBFormat"]				= $row['paramPayment']['_dateDBFormat'];
//				$param["_createPB"]					= $row['paramPayment']['_createPB'];
//				$param["_createDOM"]				= $row['paramPayment']['_createDOM'];
//				$param["_createREM"]				= $row['paramPayment']['_createREM'];
								
	  
				try 
				{
					$SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);					
					//$result = $SinglePayment->createPayment($param);
					//Zend_Debug::dump($param);die;
					$result = $SinglePayment->createVirtualPayment($param);
					
				}
				catch(Exception $e)
				{
					Application_Helper_General::exceptionLog($e);
					$errr = 1;
				}
			}
			unset($sessionNamespace->content);
			
			$this->_helper->getHelper('FlashMessenger')->addMessage('/'.$this->view->modulename.'/'.$this->view->controllername.'/index');
			
			if ($errr != 1)
			{	$this->_redirect('/notification/success/index');	}
			else
			{	$this->_redirect('/notification/success/index');	}	// TODO: what to do, if failed create payment
			
		}
	}
}
