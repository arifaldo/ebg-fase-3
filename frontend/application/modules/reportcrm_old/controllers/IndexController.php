<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';
class reportcrm_IndexController extends Application_Main {



    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $select = $this->_db->select()
                            ->from('information_schema.tables', array('table_name'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo');

        $tableList = $this->_db->fetchAll($select);

        $report =  $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
                             ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                             ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin))
                     );
        foreach ($report as $key => $value) {
            $report[$key]['REPORT_CREATED'] = Application_Helper_General::convertDate($value['REPORT_CREATED'],$this->displayDateTimeFormat,$this->defaultDateFormat);
        }
        $this->view->report = $report;

         $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                                    ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "PS_CREATED",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "PS_PERIODIC",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
            // echo $select;die;
        $tempColumn = $this->_db->fetchAll($select);
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }

        }
        // echo '<pre>';
        // print_r($tempColumn);die;
        $this->view->colomndata = $tempColumn;

        $this->view->tableList = $tableList;
        
        if($this->_request->isPost()){

            $params     = $this->_request->getParams();
            // print_r($params);die;
            $filters    = array('tablename' => array('StringTrim','StripTags','HtmlEntities'),
                                'save_as' => array('StringTrim','StripTags','HtmlEntities'),
                                'tablecols' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities'),
                                'datalimit' => array('StringTrim','StripTags','HtmlEntities'),
                                'wherecol' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereopt' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereval' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_name' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_email' => array('StringTrim','StripTags','HtmlEntities'),
                                'label' => array('StringTrim','StripTags','HtmlEntities'),
                                'colomn' => array('StringTrim','StripTags','HtmlEntities'),
                                'type' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_schedule' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_date' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_day' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_time' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_data' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_month' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortby' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities')


            );

            $validators =  array('tablename'     => array(),
                                'save_as'      => array(),
                                'tablecols'      => array(),
                                'sortasc'        => array(),
                                'sortdesc'       => array(),
                                'wherecol'       => array(),
                                'whereopt'       => array(),
                                'whereval'          => array('allowEmpty'=>true
                                                            // new Zend_Validate_Regex(array('pattern' => '/^[0-9A-Za-z\\s-_.]+$/')),
                                                            // 'messages' => array('Invalid report condition')
                                                    ),
                                'datalimit'         => array('allowEmpty' => true,
                                                            'Digits',
                                                            'messages' => array('Invalid data limit format')
                                                    ),
                                'report_name'       => array('NotEmpty',
                                                            array('StringLength',array('max'=>200)),
                                                            'messages' => array('Can not be empty',
                                                                            'Report name length cannot be more than 200',
                                                                        )
                                                    ),
                                'label'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),

                                'colomn'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'type'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'report_email'      => array('allowEmpty'=>true,
                                                            array('StringLength',array('max'=>128)),
                                                            'messages' => array(//'Can not be empty',
                                                                            'Email length cannot be more than 128',
                                                                        )
                                                    ),
                                'report_schedule'   => array('allowEmpty'=>true,
                                                            'Alpha',
                                                            'messages' => array(//'Can not be empty',
                                                                                'Invalid schedule'
                                                                        )
                                                    ),
                                'report_date'       => array('allowEmpty'=>true),
                                'report_day'       => array('allowEmpty'=>true),
                                'report_time'       => array('allowEmpty'=>true),
                                'report_data'       => array('allowEmpty'=>true),
                                'report_month'       => array('allowEmpty'=>true),
                                'sortby'       => array('allowEmpty'=>true),
                                'sortasc'       => array('allowEmpty'=>true),
                                'sortdesc'       => array('allowEmpty'=>true)


            );

            $zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

            $cek_multiple_email = true;

            if($zf_filter->report_email)
            {
                $validate = new Validate();
                $cek_multiple_email = $validate->isValidEmailMultiple($zf_filter->report_email);
            }
            // echo 'here';die;
            if($zf_filter->isValid() && $cek_multiple_email == true)
            {
                // print_r($zf_filter);die;
                // print_r($zf_filter->sortasc);die;
                $selectedCols = implode(",", $zf_filter->tablecols);
                // $sortAsc = implode(",", $zf_filter->sortasc);
                // $sortDesc = implode(",", $zf_filter->sortdesc);

                $whereCols = $zf_filter->wherecol;
                $whereOpts = $zf_filter->whereopt;
                $whereVals = $zf_filter->whereval;

                $optsArr = array("EQUAL" => "=",
                                "NOT EQUAL" => "<>",
                                "LESS THAN" => "<",
                                "GREATER THAN" => ">",
                                "LESS THAN OR EQUAL TO" => "<=",
                                "GREATER THAN OR EQUAL TO" => ">="
                );

                echo "<pre>";
                print_r($whereCols);
                print_r($whereVals);

                if(!empty($whereCols)){
                    $tempWhere = array();
                    $index = 0;
                    // $withoutDuplicates = array_unique(array_map("strtoupper", $whereCols));
                    // print_r($withoutDuplicates);
                    $row = 0;
                    // print_r($whereVals);
                    // print_r($whereCols);
                    foreach($whereCols as $key => $val){
                        $tempName = explode("-", $val);
                        $colName = $tempName[0];
                        // if($key%2==0){


                            // print_r($test);
                        if($whereOpts[$key] != "LIKE"){
                            $opt = $optsArr[$whereOpts[$index]];
                            // print_r($whereVals);
                            $whereval = $whereCols[$key+1];
                        }
                        else{
                            $opt = $whereOpts[$index];
                            $whereval = "%".$whereCols[$key+1]."%";
                        }
                        // if($whereVals==''){
                        //     $whereval = 0;
                        // }
                        $duplicate = in_array($val, $whereCols);
                        // print_r($duplicate);
                        if($duplicate){
                            // print_r($whereval);
                            if(empty($whereVals[$row])){
                                $whereVals[$row] = 0;
                            }
                            if($row == '0'){
                                if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    $val = Application_Helper_General::convertDate($whereVals[$row], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    // $val = $whereVals[$row];
                                }else{
                                    $val = $whereVals[$row];
                                }
                                $tempWhere[$row] = $colName." ".$opt." ".$val." ;";
                            }else{
                                if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    $val = Application_Helper_General::convertDate($whereVals[$row], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                     // $val = $whereVals[$row];
                                }else{
                                    $val = $whereVals[$row];
                                }
                                $tempWhere[$row-1] .= " ".$colName." ".$opt." ".$val." ;";
                            }
                            // print_r($tempWhere);
                            $row++;
                        }else{
                            if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    $val = Application_Helper_General::convertDate($whereval, $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    // $val = $whereVals[$row];
                                }else{
                                    $val = $whereVals[$row];
                                }
                            $tempWhere[$row] = $colName." ".$opt." ".$val." ;";
                            // print_r($tempWhere);
                            $row++;
                        }

                        $index++;
                        // }
                        // $index++;
                    }

                    // print_r($tempWhere);

                    $wheres = implode(";",$tempWhere);
                    $wheres = str_replace(';;', ';', $wheres);
                }
                // print_r($zf_filter->report_data);




                // print_r($wheres);
               //  $report = $this->_request->getParam('report_radio');
               // if(empty($report)){
               //      $    
               // }

                if(empty($zf_filter->report_email)){
                    $schedule = null;
                }
                else{
                    if(empty($zf_filter->report_schedule))
                        $schedule = NULL;
                    else
                        $schedule = strtolower($zf_filter->report_schedule);
                }

                if(empty($zf_filter->datalimit))
                    $limit = 0;
                else
                    $limit = $zf_filter->datalimit;

                $insArr = array("REPORT_NAME" => $zf_filter->report_name,
                                "REPORT_FILE" => $zf_filter->save_as,
                                "REPORT_TABLE" => $zf_filter->tablename,
                                "REPORT_COLUMNS" => $selectedCols,
                                "REPORT_WHERE" => $wheres,
                                // "REPORT_SORT_ASC" => $sortAsc,
                                // "REPORT_SORT_DESC" => $sortDesc,
                                "REPORT_LIMIT" => $limit,
                                "REPORT_EMAIL" => $zf_filter->report_email,
                                "REPORT_SCHEDULE" => $schedule,
                                "REPORT_CUST" => $this->_custIdLogin,
                                "REPORT_CREATED" => new Zend_Db_Expr("GETDATE()"),
                                "REPORT_CREATEDBY" => $this->_userIdLogin,
                                "REPORT_DATA" => $zf_filter->report_data

                );
                // print_r($insArr);die;
                

                if($zf_filter->sortby == '1'){
// "REPORT_SORT_ASC" => $sortAsc,
                    $insArr['REPORT_SORT_ASC'] =  $zf_filter->sortasc;
                }else if($zf_filter->sortby == '2'){
                    $insArr['REPORT_SORT_DESC'] =  $zf_filter->sortdesc;
                }


                if($schedule == 'monthly'){
                    $insArr['REPORT_DATE'] =  $zf_filter->report_date;
                }else if($schedule == 'weekly'){
                    $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                    $insArr['REPORT_TIME'] =  $zf_filter->report_time;
                }else if($schedule == 'daily'){
                    // $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                    $insArr['REPORT_TIME'] =  $zf_filter->report_time;
                }else if($schedule == 'yearly'){
                    $insArr['REPORT_MONTH'] =  $zf_filter->report_month;
                    $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                }

                try{
                    $this->_db->beginTransaction();

                    $this->_db->insert('T_REPORT_GENERATOR',$insArr);

                    $lastId = $this->_db->lastInsertId();
                    // print_r($zf_filter->label);die;
                    if(!empty($zf_filter->label)){
                        foreach ($zf_filter->label as $key => $value) {
                                    // print_r($value);
                                if($zf_filter->type[$key]=='datetime'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='date'){
                                    $type = 3;
                                }elseif($zf_filter->type[$key]=='varchar'){
                                    $type = 1;
                                }elseif($zf_filter->type[$key]=='text'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='decimal' || $zf_filter->type[$key]=='int'){
                                    $type = 2;
                                }else{
                                    $type = 1;
                                }

                                $colmnArr = array("COLM_NAME" => $value,
                                    "COLM_FIELD" => $zf_filter->colomn[$key],
                                    "COLM_REPORD_ID" => $lastId,
                                    "COLM_INDEX" => $key,
                                    "COLM_TYPE" => $type
                                );
                                // print_r($colmnArr);die;
                                $this->_db->insert('T_REPORT_COLOMN',$colmnArr);
                        }

                    }

                    Application_Helper_General::writeLog('ADRG','New report has been added, Report Name : '.$zf_filter->report_name. ' Creator : '.$this->_custIdLogin." | ".$this->_userIdLogin);

                    $this->_db->commit();
                    $this->setbackURL('/reportcrm/report');
                    $this->_redirect('/notification/success/index');
                }
                catch(Exception $e){
                    // print_r($e);die;
                    $this->_db->rollBack();
                    $error_remark = $this->language->_('An Error Occured. Please Try Again');
                }
            // die;
                if(isset($error_remark))
                {
                    // die;
                    Application_Helper_General::writeLog('ADRG','Add Report');
                    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
                    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
                    $this->_redirect('/reportcrm');
                }
            }
            else{
                $this->view->tablename = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $this->view->save_as = ($zf_filter->isValid('save_as'))? $zf_filter->save_as : $this->_getParam('save_as');
                $this->view->tablecols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $this->view->sortasc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $this->view->sortdesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');
                $this->view->datalimit = ($zf_filter->isValid('datalimit'))? $zf_filter->datalimit : $this->_getParam('datalimit');
                $this->view->wherecol = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                $this->view->whereopt = ($zf_filter->isValid('whereopt'))? $zf_filter->whereopt : $this->_getParam('whereopt');
                $this->view->whereval = ($zf_filter->isValid('whereval'))? $zf_filter->whereval : $this->_getParam('whereval');
                $this->view->report_name = ($zf_filter->isValid('report_name'))? $zf_filter->report_name : $this->_getParam('report_name');
                $this->view->report_email = ($zf_filter->isValid('report_email'))? $zf_filter->report_email : $this->_getParam('report_email');
                $this->view->report_schedule = ($zf_filter->isValid('report_schedule'))? $zf_filter->report_schedule : $this->_getParam('report_schedule');

                $error = $zf_filter->getMessages();
           // print_r($error);die;
                $errorArray = null;
                foreach($error as $keyRoot => $rowError)
                {
                   foreach($rowError as $errorString)
                   {
                      $errorArray[$keyRoot] = $errorString;
                   }
                }

                if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['report_email'] = 'Invalid email format';

                $this->view->error_msg = $errorArray;

                $tblName = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")');

                $tempColumn = $this->_db->fetchAll($select);

                $this->view->columnList = $tempColumn;

                $tblCols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $tblAsc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $tblDesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');

                $leftCols = array();
                $leftAsc = array();
                $leftDesc = array();
                foreach($tempColumn as $row){
                    if(!in_array($row['COLUMN_NAME'], $tblCols))
                        $leftCols[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblAsc))
                        $leftAsc[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblDesc))
                        $leftDesc[] = $row['COLUMN_NAME'];
                }

                $this->view->leftcols = $leftCols;
                $this->view->leftasc = $leftAsc;
                $this->view->leftdesc = $leftDesc;

                $whereCols = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                if(!empty($whereCols)){
                    $wherectr = count($whereCols)+1;
                }
                else{
                    $wherectr = 1;
                }

                $this->view->wherectr = $wherectr;
            }
        }
        else{
            $this->view->wherectr = 1;
        }
    }


    public function columnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('COLUMN_NAME'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")');
                            // echo $select;die;
        $data = $this->_db->fetchAll($select);
        foreach($data as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."'>".$row['COLUMN_NAME']."</option>";
        }

        echo $optHtml;
    }


     public function previewAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $colomn = $this->_getParam('colomn');
        $limit = $this->_getParam('limit');
        $selectdata = '';
        foreach ($colomn as $key => $value) {
            if(!empty($colomn[$key+1])){
                $selectdata .= $value.' , ';    
            }else{
                $selectdata .= $value;
            }
            
        }

         $paramPayment = array("WA"              => false,
                              "ACCOUNT_LIST"    => $this->_accountList,
                              "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
                             );
        
        // get payment query
        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        $select   = $CustomerUser->getPayment($paramPayment);
        // $select = 'SELECT '.$selectdata.' FROM T_TRANSACTION as A LEFT JOIN T_PSLIP as B ON A.PS_NUMBER = B.PS_NUMBER';

        // $select = $this->_db->select()
        //                     ->from('information_schema.columns', array('COLUMN_NAME'))
        //                     ->where('table_schema = ?', 'bank_mayapada_cm_demo')
        //                     ->where('table_name in ("T_PSLIP","T_TRANSACTION")');
        //                     // echo $select;die;
        // $data = $this->_db->fetchAll($select);
        // foreach($data as $row){
        //     $optHtml.="<option value='".$row['COLUMN_NAME']."'>".$row['COLUMN_NAME']."</option>";
        // }



        if(!empty($limit)){
            $select->limit($limit);
        }
               
       // echo '<pre>';
       //  echo $select;die;
        $data = $this->_db->fetchAll($select);

        $optHtml = '<table class="table table-bordered table-condensed flip-content"><tr>';
        foreach ($colomn as $key => $value) {
            $optHtml .= '<th>'.$value.'</th>';
        }
        $optHtml .= '</tr>';
        $total = count($data);

        for ($i=0; $i < $total ; $i++) { 
            $optHtml .= '<tr>';
            foreach ($colomn as $key => $value) {
                $optHtml .= '<td>'.$data[$i][$value].'</td>';
            }
            $optHtml .= '</tr>';
        }
        // foreach ($data as $key => $value) {
            
        // }

        echo $optHtml;
    }

    

    public function wherecolumnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                            ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "PS_NUMBER",
                                    "LLD_DESC",
                                    "TRANSACTION_ID",
                                    "PS_SUBJECT",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "PS_CREATED",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "TRA_MESSAGE",
                                    "TRA_ADDITIONAL_MESSAGE",
                                    "PS_PERIODIC",
                                    "CUST_ID",
                                    "RATE",
                                    "RATE_BUY",
                                    "BOOK_RATE",
                                    "BOOK_RATE_BUY",
                                    "PROVISION_FEE",
                                    "FULL_AMOUNT_FEE",
                                    "TOTAL_CHARGES",
                                    "RATE_BUY",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "TRANSFER_FEE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
                            // ->where('DATA_TYPE != ? ','datetime')
                            // ->where('DATA_TYPE != ? ','date');

        $tempColumn = $this->_db->fetchAll($select);
        $optHtml = "<option value=''>-- ".$this->language->_('Any Value')." --</option>";
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }

        }
        foreach($tempColumn as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."-".$row['DATA_TYPE']."'>".$row['COLOMN']."</option>";
        }

        echo $optHtml;
    }


     public function wherecolumnnewAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                            ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "PS_NUMBER",
                                    "LLD_DESC",
                                    "TRANSACTION_ID",
                                    "PS_SUBJECT",
                                    "LOG",
                                    "REFF_ID",
                                    "PS_CREATED",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "TRA_MESSAGE",
                                    "TRA_ADDITIONAL_MESSAGE",
                                    "PS_PERIODIC",
                                    "CUST_ID",
                                    "RATE",
                                    "RATE_BUY",
                                    "BOOK_RATE",
                                    "BOOK_RATE_BUY",
                                    "PROVISION_FEE",
                                    "FULL_AMOUNT_FEE",
                                    "TOTAL_CHARGES",
                                    "RATE_BUY",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "TRANSFER_FEE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
                            // ->where('DATA_TYPE != ? ','datetime')
                            // ->where('DATA_TYPE != ? ','date');

        $tempColumn = $this->_db->fetchAll($select);
        $optHtml = "<option value=''>-- ".$this->language->_('Any Value')." --</option>";
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }

        }
        foreach($tempColumn as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."-".$row['DATA_TYPE']."'>".$row['COLOMN']."</option>";
        }

        echo $optHtml;
    }




    public function wherecolumnpsstatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $arrPayStatus   = array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
        foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }


        // $tempColumn = $this->_db->fetchAll($select);
        $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($optpayStatusRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }

     public function wherecolumnpstypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
		$excludepstype = $this->_getParam('exclude');
		//var_dump($excludepstype);die;
//         $payType    = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
//         foreach($payType as $key => $value){
// //          if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);

//              $optpaytypeRaw[$key] = $this->language->_($value);
//         }


//         // $tempColumn = $this->_db->fetchAll($select);
//         // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
//         // $optPayStatus   = $opt + $optpayStatusRaw;
//         // print_r($optpayStatusRaw);die;
//         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

//         foreach($optpaytypeRaw as $key => $row){
//             if($tblName==$key){
//                 $select = 'selected';
//             }else{
//                 $select = '';
//             }
//             $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
//         }

        $conf = Zend_Registry::get('config');
        $paymentType = $conf['payment']['type'];

        $setting = new Settings();
        $system_type    = $setting->getSetting('system_type');

        $paymentTypeFlip = array_flip($paymentType['code']);
        //$system_type = 2;
        if ($system_type == 2) {
        $selectTrx  = $this->_db->select()
            ->distinct()
            ->from( array('T_PSLIP'),array('PS_TYPE'))
            ->where('PS_TYPE IN (18,19,20,21,23)');
            //->group('PS_TYPE')
            //->order("PS_TYPE ASC");
        }else if($system_type == 1){
            $selectTrx  = $this->_db->select()
            ->distinct()
            ->from( array('T_PSLIP'),array('PS_TYPE'))
            ->where('PS_TYPE NOT IN (18,19,20,21,23)');
            //->group('PS_TYPE')
            //->order("PS_TYPE ASC");
        }else{
        $selectTrx  = $this->_db->select()
            ->distinct()
            ->from( array('T_PSLIP'),array('PS_TYPE'));
            
        }
        //var_dump($exclude);
		if(!empty($excludepstype)){
			$selectTrx->where('PS_TYPE NOT IN (?)',$excludepstype);
		}
		
		$selectTrx->group('PS_TYPE')
            ->order("PS_TYPE ASC");
		
		//echo $selectTrx;
        $ACBENEFArr = $this->_db->fetchAll($selectTrx);

        if ($system_type == 1) {
            unset($paymentType['code']['opentransfer']);
            unset($paymentType['code']['sweeptransfer']);
            unset($paymentType['code']['opensweep']);
            unset($paymentType['code']['opensweepother']);

            unset($ACBENEFArr[14]);
            unset($ACBENEFArr[15]);
            unset($ACBENEFArr[16]);
            unset($ACBENEFArr[17]);

        }

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        //var_dump($ACBENEFArr);
        foreach ($ACBENEFArr as $key => $value) {

            if ($value['PS_TYPE'] != NULL) {

                if ($system_type == 2) {
                    
                    if ($value['PS_TYPE'] == '19') {
                     $payType = 'CP Same Bank Remains';
                     $pstype = $value['PS_TYPE'];
                    }else if ($value['PS_TYPE'] == '20') {
                         $payType = 'CP Same Bank Maintains';
                         $pstype = $value['PS_TYPE'];
                    }else if ($value['PS_TYPE'] == '23') {
                         $payType = 'CP Others Remains';
                         $pstype = $value['PS_TYPE']; 
                    }else if ($value['PS_TYPE'] == '21') {
                         $payType = 'Money Movement';
                         $pstype = $value['PS_TYPE'];
                    }
                        
                }else if($system_type == 1){

                    $payType = $paymentType['desc'][$paymentTypeFlip[$value['PS_TYPE']]];
                    $pstype = $value['PS_TYPE'];
                    
                }else{

                    $payType = $paymentType['desc'][$paymentTypeFlip[$value['PS_TYPE']]];
                    $pstype = $value['PS_TYPE'];

                }
                
                if($tblName==$pstype){
                    $select = 'selected';
                }else{
                    $select = '';
                }

                if ($pstype) {
                    $optHtml.="<option value='".$pstype."' ".$select.">".$payType."</option>";
                }
                
            }
            
        }
        
        if ($pstype) {
            echo $optHtml;
        }
        
    }

    public function wherecolumntratypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $idtablename = $this->_getParam('idtablename');
        // $payType    = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
        // foreach($payType as $key => $value){
//          if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);

             // $optpaytypeRaw[$key] = $this->language->_($value);
        // }

        $setting = new Settings();
        $system_type    = $setting->getSetting('system_type');

        // if ($system_type == 2) {
            
        //     if($tblName == '21' || $tblName == '23'){

        //     $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." </option>";
        //     $optHtml .= "<option value='0'> ".$this->language->_('Online')." </option>";
        //     $optHtml .= "<option value='1'> ".$this->language->_('RTGS')." </option>";
        //     $optHtml .= "<option value='2'> ".$this->language->_('SKN')." </option>";

        //     }

        // }else{

            if ($tblName == '1'){

                 $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
                 $optHtml .= "<option value='0'>".$this->language->_('In House')." </option>";
                 $optHtml .= "<option value='7'>".$this->language->_('In House (Sell)')."</option>";
                 $optHtml .= "<option value='8'>".$this->language->_('In House (Buy)')."</option>";
                 $optHtml .= "<option value='6'>".$this->language->_('VA')."</option>";

             }else if ($tblName == '7,5' || $tblName == '11,11' || $tblName == '15,15' || $tblName == '14,14'){

                 $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
                 $optHtml .= "<option value='0'>-- ".$this->language->_('In House')." --</option>";
             }
             else if ($tblName == '2'){

                 $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
                 $optHtml .= "<option value='2'>-- ".$this->language->_('SKN')." --</option>";
                 $optHtml .= "<option value='1'>-- ".$this->language->_('RTGS')." --</option>";
                 $optHtml .= "<option value='5'>-- ".$this->language->_('Online')." --</option>";
            //       document.form.trfType.options[3] = new Option('ONLINE','8');
             }
             else if ($tblName == '3'){

                 $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
                 $optHtml .= "<option value='3'>".$this->language->_('OUR')." </option>";
                 $optHtml .= "<option value='4'> ".$this->language->_('SHA')." </option>";
             }
             else if($tblName == '6,4'){
                 $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
                 $optHtml .= "<option value='0'> ".$this->language->_('In House')." </option>";
                 $optHtml .= "<option value='2'> ".$this->language->_('SKN')." </option>";
                 $optHtml .= "<option value='1'> ".$this->language->_('RTGS')." </option>";
             }

             else if($tblName == '16'){
                 $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
                 $optHtml .= "<option value='13'> ".$this->language->_('Handphone Voucher')." </option>";
                 $optHtml .= "<option value='14'> ".$this->language->_('Electricity Voucher')." </option>";
             }
             else if($tblName == '17'){
                 $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." </option>";
                 $optHtml .= "<option value='1'> ".$this->language->_('Electricity')." </option>";
                 $optHtml .= "<option value='2'> ".$this->language->_('PAM')." </option>";
                 $optHtml .= "<option value='3'> ".$this->language->_('Phone')." </option>";
                 $optHtml .= "<option value='4'> ".$this->language->_('Internet')." </option>";
                 $optHtml .= "<option value='5'> ".$this->language->_('Cable TV')." </option>";
                 $optHtml .= "<option value='6'> ".$this->language->_('Tax')." </option>";
                 $optHtml .= "<option value='7'> ".$this->language->_('Credit Card')." </option>";
                 $optHtml .= "<option value='8'> ".$this->language->_('Installment')." </option>";
                 $optHtml .= "<option value='9'> ".$this->language->_('Insurance')." </option>";
                 $optHtml .= "<option value='10'> ".$this->language->_('Education')." </option>";
                 $optHtml .= "<option value='11'> ".$this->language->_('Ticket')." </option>";

             }else if($tblName == '21'){

                // $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." </option>";
                // $optHtml .= "<option value='0'> ".$this->language->_('Online')." </option>";
                // $optHtml .= "<option value='1'> ".$this->language->_('RTGS')." </option>";
                // $optHtml .= "<option value='2'> ".$this->language->_('SKN')." </option>";

                $tra_type   = array("0" => "Online", "2" => "SKN", "1" => "RTGS");
                $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
                foreach ($tra_type as $key => $value) {
                    // if($idtablename==$key){
                    //     $select = 'selected';
                    // }else{
                    //     $select = '';
                    // }

                    $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
                }

            }else if($tblName == '23'){
                 $tra_type   = array( "2" => "SKN", "1" => "RTGS");
                $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
                foreach ($tra_type as $key => $value) {
                    // if($idtablename==$key){
                    //     $select = 'selected';
                    // }else{
                    //     $select = '';
                    // }

                    $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
                }
            }
             else {
                 $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
             }

        // }

        // echo $optHtml;
        $result['optHtml'] = $optHtml;
        $result['tblName'] = $tblName;
        $result['idtablename'] = $idtablename;

        echo json_encode($result);
    }



    public function wheresourceaccountAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $arrAccount     = $CustomerUser->getAccounts();
		
		$masterData = $this->_db->fetchAll(
            $this->_db->select()
              ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
              ->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
              ->where('A.CUST_ID = ?', $this->_custIdLogin)
              ->where('A.USER_LOGIN = ?', $this->_userIdLogin)
              ->where('A.MAXLIMIT > 0')
            //  ->where('A.MAXLIMIT_OPEN = 1')
          );
		

        if(is_array($arrAccount) && count($arrAccount) > 0){
            foreach($arrAccount as $key => $value){
				foreach ($masterData as $newkey => $valuemaster) {
				if($arrAccount[$key]["ACCT_NO"] == $valuemaster['ACCT_NO']){
                $val        = $arrAccount[$key]["ACCT_NO"];
                $ccy        = $arrAccount[$key]["CCY_ID"];
                $acctname   = $arrAccount[$key]["ACCT_NAME"];
                //$acctalias    = $arrAccount[$key]["ACCT_ALIAS_NAME"];
                $accttype   = ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';  // 10 : saving, 20 : giro;

                $arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';
				}
				}
            }
        }
        else { $arrAccountRaw = array();}

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($arrAccountRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }

    public function wheresourceaccountopenAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $arrAccount     = $CustomerUser->getAccounts();
		
		$masterData = $this->_db->fetchAll(
            $this->_db->select()
              ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
              ->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
              ->where('A.CUST_ID = ?', $this->_custIdLogin)
              ->where('A.USER_LOGIN = ?', $this->_userIdLogin)
              ->where('A.MAXLIMIT > 0')
            //  ->where('A.MAXLIMIT_OPEN = 1')
          );
		
		
        if(is_array($arrAccount) && count($arrAccount) > 0){
            foreach($arrAccount as $key => $value){

				foreach ($masterData as $newkey => $valuemaster) {
				if($arrAccount[$key]["ACCT_NO"] == $valuemaster['ACCT_NO']){

                $val        = $arrAccount[$key]["ACCT_NO"];
                $ccy        = $arrAccount[$key]["CCY_ID"];
                $acctname   = $arrAccount[$key]["ACCT_NAME"];
                //$acctalias    = $arrAccount[$key]["ACCT_ALIAS_NAME"];
                $accttype   = ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';  // 10 : saving, 20 : giro;

                $arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';
				}
				}
            }
        }
        else { $arrAccountRaw = array();}

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($arrAccountRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }


        //open banking acct
        $complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                       
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );   
        $comp = "'";
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";

        $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_APIKEY'))
                         ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                          ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                         ->where("A.CUST_ID IN (".$comp.")")
                         ->order('A.APIKEY_ID ASC')
                );
        
        $account = array();
        foreach ($acctlist as $key => $value) {
			
            $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
            $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
            $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
            $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
            $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
            $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
            $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
        }
        
        $i = 0;
        foreach ($account as $key => $value) {
			foreach ($masterData as $newkey => $valuemaster) {
				if($value['account_number'] == $valuemaster['ACCT_NO']){
					
            $acct[$i]['ACCT_NO'] = $value['account_number'];
            $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
            $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
            $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
            $acct[$i]['ACCT_NAME'] = $value['account_name'];

            $acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
            $acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
            $acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
            $acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
            $i++;
				}
			}
        }

        $newacct = array();
        foreach ($acct as $key => $value) {

            if($tblName==$value['ACCT_NO']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['ACCT_NO']."' ".$select.">".$value['ACCT_NO']." [".$value['ACCT_BANK']."] - ".$value['ACCT_NAME']."</option>";
        }


        echo $optHtml;
    }




     public function whereuserAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $cust_id = (string)$this->_custIdLogin;
        $cust = new Customer($cust_id);
        $userList = $cust->getUserList();
        $tblName = $this->_getParam('id');
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        // alert($tblName);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($userList)){
        foreach($userList as $key => $row){
            if($tblName==$row['USER_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['USER_ID']."' ".$select.">".$row['USER_ID']."</option>";
        }
        }

        echo $optHtml;
    }

    // public function whereuserAction()
    // {
    //     $this->_helper->viewRenderer->setNoRender();
    //     $this->_helper->layout()->disableLayout();
    //     $cust_id = (string)$this->_custIdLogin;
    //     $cust = new Customer($cust_id);
    //     $userList = $cust->getUserList();
    //     $tblName = $this->_getParam('id');
    //     // $tempColumn = $this->_db->fetchAll($select);
    //     // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
    //     // $optPayStatus   = $opt + $optpayStatusRaw;
    //     // print_r($arrAccountRaw);die;
    //     // alert($tblName);
    //     $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
    //     if(!empty($userList)){
    //     foreach($userList as $key => $row){
    //         if($tblName==$row['USER_ID']){
    //             $select = 'selected';
    //         }else{
    //             $select = '';
    //         }
    //         $optHtml.="<option value='".$row['USER_ID']."' ".$select.">".$row['USER_ID']."</option>";
    //     }
    //     }

    //     echo $optHtml;
    // }


     public function approvestatAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $aprovalstatus = $this->_aprovalstatus;
        $aprovalstatus = array(''=>'-- '. $this->language->_('All') .' --');
        $aprovalstatus += Application_Helper_Array::globalvarArray($this->_aprovalstatus);
        foreach($aprovalstatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		$tblName = $this->_getParam('id');
        unset($aprovalstatus[2]);
        unset($aprovalstatus[3]);
        unset($aprovalstatus[6]);

        unset($optpayStatusRaw[2]);
        unset($optpayStatusRaw[3]);
        unset($optpayStatusRaw[6]);

         foreach ($optpayStatusRaw as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

   public function categoryAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
         $tblName = $this->_getParam('id');

    $arrquestioncategory    = array_combine($this->_questioncategory["code"],$this->_questioncategory["desc"]);
        $questioncategoryArray  = array( '' => '--- '.$this->language->_('Please Select').' --- ');
        $questioncategoryArray += array_combine(array_values($this->_questioncategory['code']),array_values($this->_questioncategory['desc']));
        //$this->view->questioncategoryArray        = $questioncategoryArray;

        foreach($questioncategoryArray as $key => $value){ if($key != 5) $optpayStatusRaw[$key] = $this->language->_($value); }

        $optPayType = $optpayStatusRaw;
        // print_r($optPayType);die;
        foreach ($optPayType as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
        // $this->view->questioncategoryArray  = $optPayType;


    }

    public function ccyAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       $selectidr = '';
       $selectusd = '';
       if(!empty($tblName)){
            if($tblName=='IDR'){
                $selectidr = 'selected';
            }else{
                $selectusd = '';
            }

       }

        // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        $optHtml.="<option value='IDR' ".$selectidr.">IDR</option>";
        $optHtml.="<option value='USD' ".$selectusd.">USD</option>";


        echo $optHtml;
    }


    public function actlistAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $activity = $this->_db->select()->distinct()
                                ->from(array('A' => 'M_FPRIVILEGE'),array('FPRIVI_ID', 'FPRIVI_DESC'))
                                ->order('FPRIVI_DESC ASC')
                                ->query()->fetchAll();

        $login = array('FPRIVI_ID'=>'FLGN','FPRIVI_DESC'=>'Login');
        $logout = array('FPRIVI_ID'=>'FLGT','FPRIVI_DESC'=>'Logout');
        $changepass = array('FPRIVI_ID'=>'CHMP','FPRIVI_DESC'=>'Change My Password');
        $resetpass = array('FPRIVI_ID'=>'RFPW','FPRIVI_DESC'=>'Reset Forgot Password');
        array_unshift($activity,$changepass);
        array_unshift($activity,$logout);
        array_unshift($activity,$login);
        array_unshift($activity,$resetpass);

        $activityarr = Application_Helper_Array::listArray($activity,'FPRIVI_ID','FPRIVI_DESC');
        asort($activityarr);
        // print_r($activityarr);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach ($activityarr as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }
	
	public function benetypedomAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
		if($tblName == 0){
			$select0 = 'selected';
		}else if($tblName == 1){
			$select1 = 'selected';
		}else if($tblName == 2){
			$select2 = 'selected';
		}
			$optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
			 $optHtml .= "<option value='0' ".$select0."> ".$this->language->_('Online')." </option>";
             $optHtml .= "<option value='1' ".$select1.">  ".$this->language->_('RTGS')." </option>";
             $optHtml .= "<option value='2' ".$select2."> ".$this->language->_('SKN')." </option>";
		
		echo $optHtml;
	}
    public function sugesttypeAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $changesTypeCodeArr   = $this->_suggestType['code'];

        unset($this->_suggestType['code']['activate']);
        unset($this->_suggestType['desc']['activate']);
        unset($this->_suggestType['code']['deactivate']);
        unset($this->_suggestType['desc']['deactivate']);
        $options = array_combine(array_values($this->_suggestType['code']),array_values($this->_suggestType['desc']));

        // print_r($options);die;
        foreach ($options as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }


     public function trastatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        // $arrPayStatus   = array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
        $statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
        foreach($statusarr as $key => $value){ $optpayStatusRaw[$key] = $this->language->_($value); }


        // $tempColumn = $this->_db->fetchAll($select);
        $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($optpayStatusRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }


    

    public function sugeststatusAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $suggestionStatusCP = array( "" => '--'.$this->language->_('Any Value')."--",
                                            "UR"=> $this->language->_('Unread Suggestion') ,
                                            "RS"=> $this->language->_('Read Suggestion'),
                                            "RR"=> $this->language->_('Request Repaired'),
                                            "RP"=> $this->language->_('Repaired Suggestion'),
        );
    $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
     foreach ($suggestionStatusCP as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }


    public function areaAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $areaArr = $select = $this->_db->select()
                           ->distinct()
                           ->from('M_SERVICE_AREA',array('AREA_NAME'))
                           ->query()->fetchAll();
        //$this->view->branchArr = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($model->getBranch(),"CODE","NAME");
        $area = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($areaArr,"AREA_NAME","AREA_NAME");
        // print_r($area);die;
        foreach ($area as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function sugestdataAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $listSuggestData  = array(
        //'General Setting'=>$this->language->_('General Setting'),
        //'Minimum Amount and Currency Available'=>$this->language->_('Minimum Amount and Currency Available'),
        // 'Holiday Setting'=>$this->language->_('Holiday Setting'),
        // 'Customer'=>$this->language->_('Customer'),
        // 'Bank Account'=>$this->language->_('Bank Account'),
        // 'User Account'=>$this->language->_('User Account'),
        // 'User Limit'=>$this->language->_('User Limit'),
        // 'User Daily Limit'=>$this->language->_('User Daily Limit'),
        // 'Approver Group'=>$this->language->_('Approver Group'),
        // 'Approver Group Boundary'=>$this->language->_('Approver Group Boundary'),
        // 'Backend User'=>$this->language->_('Backend User'),
        // 'Backend Group'=>$this->language->_('Backend Group'),
        // 'Charges'=>$this->language->_('Charges'),
        // 'Charges Template'=>$this->language->_('Charges Template'),
        'User Account' => $this->language->_('User Account'),
        'User List' => $this->language->_('User List'),
        'User Limit'=>$this->language->_('User Limit'),
        'User Daily Limit'=>$this->language->_('User Daily Limit'),
        'Approver Group'=>$this->language->_('Approver Group'),
        'Approver Group Boundary'=>$this->language->_('Approver Group Boundary'),
        // 'COA Account'=>'COA Account',
        // 'Charges Template'=>'Charges Template',
        // 'Charges'=>'Charges',
        // 'System Balance'=>'System Balance',
        // 'Global Scheme Parameter'=>'Global Scheme Parameter',
        // 'Scheme Configuration'=>'Scheme Configuration',
        // 'Physical Document Configuration'=>'Physical Document Configuration',
        // 'Root Community'=>'Root Community',
        // 'Principal'=>'Principal',
        // 'Community'=>'Community',
        // 'Member'=>'Member',
        // 'Assign User to Community'=>'Assign User to Community',
    );
    ksort($listSuggestData);
    $listSuggestData = array('all'=>'--'.$this->language->_('Any Value').'--')+$listSuggestData;

     // $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach ($listSuggestData as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }

    public function benetypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       $selectidr = '';
       $selectusd = '';
       if(!empty($tblName)){
            if($tblName=='IDR'){
                $selectidr = 'selected';
            }else{
                $selectusd = '';
            }

       }
	   //var_dump($tblName);
	   
	   //var_dump($select);
	   $select = '';
       if($tblName == '1'){$select =  'selected';}
		
        $type =$this->language->_('In House');
        $optHtml.="<option value = '1' ".$select.">".$type."</option>";
		$select = '';
        if($tblName == '2'){$select =  'selected';}
        $type =$this->language->_('Domestic');
        $optHtml.="<option value = '2' ".$select.">".$type."</option>";
		$select = '';
        if($tblName == '3'){$select =  'selected';}
        $type =$this->language->_('Remittance');
        $optHtml.="<option value = '3' ".$select.">".$type."</option>";
		$select = '';
        if($tblName == '4'){$select =  'selected';}
        $type =$this->language->_('Local Remittance');
        $optHtml.="<option value = '4' ".$select.">".$type."</option>";
		


        echo $optHtml;
    }


      public function benefAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

//      if ($priviBeneLinkage == true)
//      {
            $select = $this->_db->select()
                                ->from(array('B'            => 'M_BENEFICIARY_USER'),
                                       array('ACBENEF_ID'   => 'B.BENEFICIARY_ID')
                                       )
                                ->join(array('A'=>'M_BENEFICIARY'), 'A.BENEFICIARY_ID = B.BENEFICIARY_ID',array('A.BENEFICIARY_ACCOUNT'))
                                ->where("B.CUST_ID  = ?" , (string)$this->_custIdLogin)
                                ->where("B.USER_ID  = ?" , (string)$this->_userIdLogin);
         // echo "<pre>";
     // echo $select->__toString();
//          die;
            $ACBENEFArr = $this->_db->fetchAll($select);
            // $ACBENEFArr = Application_Helper_Array::simpleArray($ACBENEFArr, "ACBENEF_ID");
            // print_r($ACBENEFArr);
            // bila empty, user bene linkage cannot see all payment
            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['ACBENEF_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
            $optHtml.="<option value='".$value['ACBENEF_ID']."' ".$select.">".$value['BENEFICIARY_ACCOUNT']."</option>";
        }


        // $optHtml.="<option value='USD'>USD</option>";


        echo $optHtml;
    }

    public function benefopenAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

//      if ($priviBeneLinkage == true)
//      {
            $select = $this->_db->select()
                                ->from(array('B'            => 'M_BENEFICIARY_USER'),
                                       array('ACBENEF_ID'   => 'B.BENEFICIARY_ID')
                                       )
                                ->join(array('A'=>'M_BENEFICIARY'), 'A.BENEFICIARY_ID = B.BENEFICIARY_ID',array('A.BENEFICIARY_ACCOUNT'))
                                ->where("B.CUST_ID  = ?" , (string)$this->_custIdLogin)
                                ->where("B.USER_ID  = ?" , (string)$this->_userIdLogin);
         // echo "<pre>";
     // echo $select->__toString();
//          die;
            $ACBENEFArr = $this->_db->fetchAll($select);
            // $ACBENEFArr = Application_Helper_Array::simpleArray($ACBENEFArr, "ACBENEF_ID");
            // print_r($ACBENEFArr);
            // bila empty, user bene linkage cannot see all payment
            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['ACBENEF_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
            $optHtml.="<option value='".$value['ACBENEF_ID']."' ".$select.">".$value['BENEFICIARY_ACCOUNT']."</option>";
        }


         //open banking acct
        $complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                       
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );   
        $comp = "'";
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";

        $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_APIKEY'))
                         ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                          ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                         ->where("A.CUST_ID IN (".$comp.")")
                         ->order('A.APIKEY_ID ASC')
                );
        
        $account = array();
        foreach ($acctlist as $key => $value) {
            $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
            $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
            $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
            $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
            $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
            $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
            $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
        }
        
        $i = 0;
        foreach ($account as $key => $value) {

            $acct[$i]['ACCT_NO'] = $value['account_number'];
            $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
            $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
            $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
            $acct[$i]['ACCT_NAME'] = $value['account_name'];

            $acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
            $acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
            $acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
            $acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
            $i++;
        }

        $newacct = array();
        foreach ($acct as $key => $value) {

            if($tblName==$value['ACCT_NO']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['ACCT_NO']."' ".$select.">".$value['ACCT_NO']." [".$value['ACCT_BANK']."] - ".$value['ACCT_NAME']."</option>";
        }



        echo $optHtml;
    }

    //addtional for checkque


    public function whereproducttypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $productType = array(
            "1"=> $this->language->_('Cheque') ,
            "2"=> $this->language->_('Bilyet Giro')
        );


        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($productType as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function wherestatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $status = array(
            "AP"=> $this->language->_('Approve') ,
            "RJ"=> $this->language->_('Reject'),
            "WA"=> $this->language->_('Waiting Approve')
        );

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($status as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function loantypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $loantypeArr = array(
            '1' => 'Term Loan',
            '2' => 'Demand Loan'
        );
            
                    
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($loantypeArr as $key => $value) {
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
    }

    //ongoing frequently
    public function frequentlyAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $frequentlyArr = array(
            '1x' => '1x',
            'Daily' => 'Daily',
            'Weekly' => 'Weekly',
            'Every Day of Monday' => 'Every Day of Monday',
            'Every Day of Tuesday' => 'Every Day of Tuesday',
            'Every Day of Wednesday' => 'Every Day of Wednesday',
            'Every Day of Thursday' => 'Every Day of Thursday',
            'Every Day of Friday' => 'Every Day of Friday',
            'Every Day of Saturday' => 'Every Day of Saturday',
            'Every Day of Sunday' => 'Every Day of Sunday'
        );

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($frequentlyArr as $key => $value) {
            if($tblName==$value){
                $select = 'selected';
            }else{
                $select = '';
            }
            // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
            $optHtml.="<option value='".$value."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
    }

    public function ongoingstatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        $ongoingStatusArr = array(
            1 => 'Ongoing',
            2 => 'Canceled'
        );

        foreach($ongoingStatusArr as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }
	
	
	public function trastatusoldAction()
    { 
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        // $ongoingStatusArr = array(
        //     1 => 'Ongoing',
        //     2 => 'Canceled'
        // );

        $arrStatus = array('1' => 'Untransfered',
                            '2' => 'Processed',
                            '3' => 'Success',
                            '4' => 'Failed',
                            '9' => 'Exception',
                            '13' => 'In Progress',
                            '0' => 'Closed'
                            
                          );

        foreach($arrStatus as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }

    public function bgstatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        // $ongoingStatusArr = array(
        //     1 => 'Ongoing',
        //     2 => 'Canceled'
        // );

        $arrStatus = array('1' => 'Waiting for review',
                            '2' => 'Waiting for approve',
                            '3' => 'Waiting to release',
                            '4' => 'Waiting for bank approval',
                            '5' => 'Issued',
                            '6' => 'Expired',
                            '7' => 'Canceled',
                            '8' => 'Claimed by applicant',
                            '9' => 'Claimed by recipient',
                            '10' => 'Request Repair',
                          );

        foreach($arrStatus as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }



}
