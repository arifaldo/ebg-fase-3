<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class userdailylimit_EditController extends userdailylimit_Model_Userdailylimit 
{

  public function indexAction()  
  { 
    $this->_helper->layout()->setLayout('newlayout');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));
//     $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $user_id : null;
    
     $ccy_id = strtoupper($AESMYSQL->decrypt($this->_getParam('ccy_id'), $password));
     $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
     $error_remark = null;
     
     //convert dailylimit agar bisa masuk ke database
     $dailyLimit = $this->_getParam('dailylimit');
     $dailyLimit = Application_Helper_General::convertDisplayMoney($dailyLimit);
     $this->_setParam('dailylimit',$dailyLimit);  
     //END convert dailylimit agar bisa masuk ke database
     
     
     $this->view->userDailyLimit_msg = array();
     

    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_LIMIT_IDR','CUST_LIMIT_USD'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             //->where('UPPER(CUST_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
                             ->where('CUST_STATUS!=3');
      $result = $this->_db->fetchRow($select);
      if(!$result)$cust_id = null;
      else{
        $companyLim = array();
        $companyLim['IDR'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_IDR']);
        $companyLim['USD'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_USD']);
      }
    }
    
    if(!$cust_id)
    {
      $error_remark = $this->getErrorRemark('22','Customer ID');
      //insert log
      try 
      {
      $this->_db->beginTransaction();
        if($this->_request->isPost())
        {
        $action   = strtoupper($this->_changeType['code']['edit']);
        $fulldesc = Application_Helper_General::displayFullDesc($_POST);
      }
      else
      {
        $action = strtoupper($this->_actionID['view']);
        $fulldesc = null;
      }
      $this->backendLog($action,strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
        $this->_db->commit();
    }
    catch(Exception $e) 
    {
      $this->_db->rollBack();
        SGO_Helper_GeneralLog::technicalLog($e);
    }
        
    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }
    
    
    if($user_id && $ccy_id)
    {
      $select  =  $this->_db->select()
                           ->from(array('d'=>'M_DAILYLIMIT'),array('DAILYLIMIT'))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id))
                             ->where('d.DAILYLIMIT_STATUS!=3');
      $result = $this->_db->fetchOne($select);
      
      if($result)
      {
        $select = $this->_db->select()
                             ->from('TEMP_DAILYLIMIT',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                               ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id));
        $result = $this->_db->fetchOne($select);
        
        if($result)
        {
          $acct_no= null;
          $error_remark = $this->getErrorRemark('03','User Daily Limit');
        }
      }
      else{ $acct_no = null; }
    }
    
    
   /* if(!$user_id)
    {
      if(!$error_remark)$error_remark = $this->getErrorRemark('22','Customer Account');
      //insert log
      try 
      {
      $this->_db->beginTransaction();
        if($this->_request->isPost())
        {
        $action   = strtoupper($this->_changeType['code']['edit']);
        $fulldesc = Application_Helper_General::displayFullDesc($_POST);
      }
      else
      {
        $action = strtoupper($this->_actionID['view']);
        $fulldesc = 'CUST_ID:'.$cust_id;
      }
      $this->backendLog($action,strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
        $this->_db->commit();
    }
    catch(Exception $e) 
    {
      $this->_db->rollBack();
        SGO_Helper_GeneralLog::technicalLog($e);
    }
        
    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
    }*/
    
    
     $select = $this->_db->select()
                             ->from(array('d'=>'M_DAILYLIMIT'))
                             ->join(array('u'=>'M_USER'),'d.USER_LOGIN=u.USER_ID and d.CUST_ID=u.CUST_ID',array('USER_FULLNAME'))
                            // ->where('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id))
                             ->where('d.DAILYLIMIT_STATUS!=3');

     $resultdata = $this->_db->fetchRow($select);                        
     
     $this->view->ccy_id  = $resultdata['CCY_ID'];
     $this->view->cust_id = $cust_id;
     $this->view->user_id = $user_id;
     
     
     $select = $this->_db->select()
                           ->from('M_CUSTOMER',array('CUST_NAME'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
     $this->view->cust_name = $this->_db->fetchOne($select);
    
    
     $this->view->modulename = $this->_request->getModuleName();
    
    $getCcy = $this->getCcy();
      $sumArr = array();
      foreach($getCcy as $val){
        $tempCCY = $val['CCY_ID'];
        $selectSum = $this->_db->select()
                              ->from('M_DAILYLIMIT', new Zend_Db_Expr('SUM(DAILYLIMIT)'))
                              ->where('UPPER(CUST_ID)= ?', $cust_id)
                              ->where('DAILYLIMIT_STATUS != ?', '3')
                              ->where('CCY_ID = ?', $tempCCY);
                              // echo $selectSum;
        $sumArr[$val['CCY_ID']] = $this->_db->fetchOne($selectSum);
      }
     
    //$errorLim = true;
    
    $selectSum = $this->_db->select()
                              ->from('M_DAILYLIMIT',array('DAILYLIMIT'))
                              ->where('UPPER(CUST_ID)= ?', $cust_id)
                              ->where('CCY_ID = ?', $tempCCY)
                ->where('USER_LOGIN = ?', $user_id);
    $datasum   = $this->_db->fetchOne($selectSum);
    
    $sumuser = 0;
    if(!empty($datasum)){
      $sumuser = $datasum['DAILYLIMIT'];
    }
        $dailylimLeft = $companyLim[$ccy_id] ;
        // - Application_Helper_General::convertDisplayMoney($sumArr[$ccy_id]);
    // $dailylimLeft = $dailylimLeft + $sumuser;
    
    $warning_remark = "Remaining amount of  company daily limit in ".$ccy_id." is ".Application_Helper_General::displayMoney($dailylimLeft)." ( from ".$ccy_id." ".Application_Helper_General::displayMoney($companyLim[$ccy_id])." )";
    //$errorArray['dailylimit'] = $warning_remark;
    $this->view->error_daily = $warning_remark;
    
     
    if($this->_request->isPost())
    {
      
      

      $filters = array('user_login'     => array('StripTags','StringTrim','StringToUpper'),
                       'ccy_id'         => array('StripTags','StringTrim','StringToUpper'),
                       'dailylimit'     => array('StripTags','StringTrim'),
                       'cust_id'        => array('StripTags','StringTrim','StringToUpper'),
                      );

      $validators =  array('cust_id'       => array(),
      
                   'user_login'       => array(),

                           'ccy_id'        => array(),
                                        
              
                           'dailylimit'    => array('NotEmpty',
                              'Float',
                                //array('GreaterThan',0),
                              array('StringLength',array('min'=>0,'max'=>16)), //batas = 13 digit,16
                              'messages' => array($this->language->_('Can not be empty'),
                                               $this->language->_('Invalid Maximum Amount Format'),
                                             //    $this->language->_('Maximum Amount must be greater than zero'),
                                         $this->language->_('Too many significant digits.Maximum digit allowed : 16 digit(s)'))
                              ),    
                         );

                
      $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
      
    
    
      if($zf_filter_input->isValid())
      {
        $ccy = $ccy_id;
        $dailylim = $zf_filter_input->dailylimit;
    
  /*  $selectSum = $this->_db->select()
                              ->from('M_DAILYLIMIT'),array('DAILYLIMIT'))
                              ->where('UPPER(CUST_ID)= ?', $cust_id)
                              ->where('CCY_ID = ?', $tempCCY)
                ->where('USER_LOGIN = ?', $user_id);
    $datasum   = $this->_db->fetchOne($selectSum);
    
    $sumuser = 0;
    if(!empty($datasum)){
      $sumuser = $datasum['DAILYLIMIT'];
    }
        $dailylimLeft = $companyLim[$ccy] - Application_Helper_General::convertDisplayMoney($sumArr[$ccy]);
    $dailylimLeft = $dailylimLeft + $sumuser;

    */
        $errorLim = false;

        // $validlimit = true;
       
        // print_r($dailylim);echo '==';
        // print_r($dailylimLeft);die;
        if($dailylim > $dailylimLeft ){

            $errorLim = true;
            $error_remark = "Maximum Amount of Daily Limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);

        //    if(!empty($resultdata['DAILYLIMIT'])){
        //   if($dailylim < $resultdata['DAILYLIMIT']){
        //     // die;
        //       $info = 'Cust ID = '.$cust_id.', User Id = '.$user_id.', Ccy Id = '.$ccy_id;
                
        //         $dailyLimitData = $this->_dailyLimitData;
                
        //       foreach($validators as $key=>$value)
        //       {
        //         if($zf_filter_input->$key)$dailyLimitData[strtoupper($key)] = $zf_filter_input->$key;
        //       }

        //       $dailyLimitData['CUST_ID'] = $cust_id;
        //       $dailyLimitData['USER_LOGIN'] = $user_id;
        //       $dailyLimitData['CCY_ID'] = $ccy_id;
              
        //         //$dailyLimitData['DAILYLIMIT_STATUS'] = 1;
        //         $dailyLimitData['DAILYLIMIT_STATUS']  = $resultdata['DAILYLIMIT_STATUS'];
        //     $dailyLimitData['CREATED']       = $resultdata['CREATED'];
        //     $dailyLimitData['CREATEDBY']     = $resultdata['CREATEDBY'];
        //     $dailyLimitData['UPDATED']       = $resultdata['UPDATED'];
        //     $dailyLimitData['UPDATEDBY']     = $resultdata['UPDATEDBY'];
                
                
        //         $dailyLimitData['SUGGESTED']    = new Zend_Db_Expr('now()');
        //     $dailyLimitData['SUGGESTEDBY']  = $this->_userIdLogin;

        //       try 
        //       {
        //       $this->_db->beginTransaction();
              
        //       $change_id = $this->suggestionWaitingApproval('User Daily Limit',$info,strtoupper($this->_changeType['code']['edit']),null,'M_DAILYLIMIT','TEMP_DAILYLIMIT',$user_id,$resultdata['USER_FULLNAME'],$cust_id);
              
        //       $this->insertTempDailyLimit($change_id,$dailyLimitData);
              
        //       //log CRUD
        //       Application_Helper_General::writeLog('DLUD','User Daily Limit has been Updated (edit), User Id : '.$user_id. ' CCY : '.$resultdata['CCY_ID'].' Change id : '.$change_id);
              
        //       $this->_db->commit();
              
        //       $this->_redirect('/notification/submited/index');
        //       //$this->_redirect('/notification/success/index');
        //       }
        //     catch(Exception $e)
        //     {
        //       $this->_db->rollBack();
        //       $error_remark = $this->getErrorRemark('82');
        //       SGO_Helper_GeneralLog::technicalLog($e);
        //     }
        //   }else{
        //     $errorLim = true;
        //     $error_remark = "Maximum Amount of Daily Limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);
        //   }
        // }else{

        //   $errorLim = true;
        //   // remaining amount of company dailylimit in ccy is sisa (from ccy total )
        //   // $error_remark = "Remaining amount of  company daily limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);
        //  // $remain = (int)$companyLim[$ccy]-(int)$dailylimLeft;
        //  // print_r($dailylimLeft);
        //    $error_remark = "Remaining amount of  company daily limit in ".$ccy." is ".Application_Helper_General::displayMoney($dailylimLeft)." ( from ".$ccy." ".Application_Helper_General::displayMoney($companyLim[$ccy])." )";

        //  }
        }
        else{
              $info = 'Cust ID = '.$cust_id.', User Id = '.$user_id.', Ccy Id = '.$ccy_id;
              
              $dailyLimitData = $this->_dailyLimitData;
              
            foreach($validators as $key=>$value)
            {
              if($zf_filter_input->$key)$dailyLimitData[strtoupper($key)] = $zf_filter_input->$key;
            }

            $dailyLimitData['CUST_ID'] = $cust_id;
            $dailyLimitData['USER_LOGIN'] = $user_id;
            $dailyLimitData['CCY_ID'] = $ccy_id;
            
              //$dailyLimitData['DAILYLIMIT_STATUS'] = 1;
              $dailyLimitData['DAILYLIMIT_STATUS']  = $resultdata['DAILYLIMIT_STATUS'];
          $dailyLimitData['CREATED']       = $resultdata['CREATED'];
          $dailyLimitData['CREATEDBY']     = $resultdata['CREATEDBY'];
          $dailyLimitData['UPDATED']       = $resultdata['UPDATED'];
          $dailyLimitData['UPDATEDBY']     = $resultdata['UPDATEDBY'];
              
              
              $dailyLimitData['SUGGESTED']    = new Zend_Db_Expr('now()');
          $dailyLimitData['SUGGESTEDBY']  = $this->_userIdLogin;

            try 
            {
            $this->_db->beginTransaction();
            
            $change_id = $this->suggestionWaitingApproval('User Daily Limit',$info,strtoupper($this->_changeType['code']['edit']),null,'M_DAILYLIMIT','TEMP_DAILYLIMIT',$user_id,$resultdata['USER_FULLNAME'],$cust_id);
            
            $this->insertTempDailyLimit($change_id,$dailyLimitData);
            
            //log CRUD
            Application_Helper_General::writeLog('DLUD','User Daily Limit has been Updated (edit), User Id : '.$user_id. ' CCY : '.$resultdata['CCY_ID'].' Change id : '.$change_id);
            
            $this->_db->commit();
            
            $this->_redirect('/notification/submited/index');
            //$this->_redirect('/notification/success/index');
            }
          catch(Exception $e)
          {
            $this->_db->rollBack();
            $error_remark = $this->getErrorRemark('82');
            SGO_Helper_GeneralLog::technicalLog($e);
          }
        }
          
        if(isset($error_remark)){
          
        //insert log
          try 
          {
            $this->_db->beginTransaction();
            
            Application_Helper_General::writeLog('DLUD','Update user daily limit');
           
            $this->_db->commit();
          }
          catch(Exception $e) 
          {
            $this->_db->rollBack();
            SGO_Helper_GeneralLog::technicalLog($e);
          }

          $errorArray = array();
          if($errorLim == true){
            $errorArray['dailylimit'] = $error_remark;
            $this->view->userDailyLimit_msg = $errorArray;
          }
          else{
            $this->view->error_msg = $error_remark;
          }
          
          $this->view->ccy_id  = $ccy_id;
          $this->view->user_id = $user_id;
          $this->view->max_amt = ($zf_filter_input->isValid('dailylimit'))? $zf_filter_input->dailylimit : $this->_getParam('dailylimit');
        }
      
    
    }
    else
    {
      $this->view->error = 1;
      
      $this->view->max_amt = ($zf_filter_input->isValid('dailylimit'))? $zf_filter_input->max_amt : $this->_getParam('dailylimit');
      $this->view->ccy_id  = $ccy_id;
      $this->view->user_id = $user_id;
        
        $error = $zf_filter_input->getMessages();
    
         //format error utk ditampilkan di view html 
         $errorArray = null;
         foreach($error as $keyRoot => $rowError)
         {
            foreach($rowError as $errorString)
            {
              $errorArray[$keyRoot] = $errorString;
            }
         }
         
       
        
         $this->view->userDailyLimit_msg = $errorArray;
      }
    }
    else
    {
       $this->view->max_amt = Application_Helper_General::displayMoney($resultdata['DAILYLIMIT']);
    }
    
    
 
    
    
    //insert log
  try 
  {
    $this->_db->beginTransaction();
    if(!$this->_request->isPost()){
    Application_Helper_General::writeLog('DLUD','Update User Daily Limit ');
    }
    
    $this->_db->commit();
  }
    catch(Exception $e)
    {
    $this->_db->rollBack();
    SGO_Helper_GeneralLog::technicalLog($e);
  }
  }
  
  
}