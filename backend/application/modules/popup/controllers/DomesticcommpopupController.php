<?php
require_once 'CMD/Validate/Validate.php';
require_once 'CMD/Beneficiary.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';

class popup_DomesticcommpopupController extends Application_Main
{
	 protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
		$this->_helper->layout()->setLayout('popup');
		$this->view->CITIZENSHIP = 'R';
		$this->view->isError = true;
	}

	public function indexAction() 
	{
		
		if($this->_request->isPost())
		{
			$this->view->CITIZENSHIP = 'R';
			$privibenelinkage = $this->view->hasPrivilege('BLBU');
			$Beneficiary = new Beneficiary();
			$filter  = new Application_Filtering();
			$settings = new Application_Settings();
			$ccyList  = $settings->setCurrencyRegistered();		
			
			if($this->_request->isPost() )
			{		
				$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
				$CURR_CODE = $this->_getParam('CURR_CODE');
				$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
				$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');		
				$ACBENEF_ADDRESS = $this->_request->getParam('ADDRESS');
				$ACBENEF_CITIZENSHIP= $this->_request->getParam('CITIZENSHIP');
				$BANK_NAME   = $this->_request->getParam('BANK_NAME');
				$BANK_CITY   = $this->_request->getParam('BANK_CITY');
				$CLR_CODE   = $this->_request->getParam('CLR_CODE');
				$ACBENEF_BANKNAME  = $this->_request->getParam('ACBENEF_BANKNAME');

				
		//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//	
				$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT , "ACCOUNT_NO");
				$ACBENEF_BANKNAME  = $filter->filter($ACBENEF_BANKNAME , "ACCOUNT_NAME");
				$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS , "ACCOUNT_ALIAS");
				$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL , "EMAIL");
				$ACBENEF_CCY   = $filter->filter($CURR_CODE  , "SELECTION");
				$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS , "ADDRESS");
				$ACBENEF_CITIZENSHIP= $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
				$BANK_NAME   = $filter->filter($BANK_NAME , "BANK_NAME");
				$BANK_CITY   = $filter->filter($BANK_CITY  , "ADDRESS");
				$CLR_CODE   = $filter->filter($CLR_CODE  , "BANK_CODE");
				
				$paramBene = array("TRANSFER_TYPE"      	=> 'SKN',
									"FROM"      			=> 'B',
									 "ACBENEF"     			=> $ACBENEF,  
									 "ACBENEF_CCY"    		=> $ACBENEF_CCY,  
									 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,  
									 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,  
									 "ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,  
									 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,  
									 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,  
									 "BANK_CODE"    	 	=> $CLR_CODE,  
									 "_addBeneficiary"   	=> true,  
									 "_beneLinkage"    		=> true,  
									);									
									
				$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
				$validateACBENEF->check($paramBene);
				$this->view->isError = $validateACBENEF->isError();
				
				if($ACBENEF_EMAIL)
			    {
					$validate = new validate;
			      	$cek_multiple_email = $validate->isValidEmailMultiple($ACBENEF_EMAIL);
			    }
			    else
			    {
			    	$cek_multiple_email == true;
			    }
				
				if ($validateACBENEF->isError() || $cek_multiple_email==false)
				{			 	
					$this->view->error = true;
					$docErr = 'Error: '.$validateACBENEF->getErrorMsg();
					$validateACBENEF->__destruct();
					unset($validateACBENEF);
					
					if(isSet($cek_multiple_email) && $cek_multiple_email == false) $docErr = 'Error: Invalid email format';
					
					$this->view->report_msg = $docErr;				
				}
			
				$this->fillParams($ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY);
			}
		}
	}

	public function detailAction() 
	{	
		$acct = $this->_getParam('acct');
		$commCode = $this->_getParam('comm_code');
		$acctData = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('C' => 'M_COMM_ACCT'))						
							->where("C.CODE	= ? ", $commCode)		
							->where("C.ACCT_OWNER	= 'P' ")				
							->where("C.ACCT_GROUP	= 'O' ")
							->where("C.ACCT_NO= ? ", $acct)
						);	
	
		$this->view->ACBENEF_ALIAS = $acctData['ACCT_ALIAS'];
		$this->view->ACBENEF = $acctData['ACCT_NO'];
		$this->view->ACBENEF_BANKNAME = $acctData['ACCT_NAME'];
		$this->view->ACBENEF_EMAIL = $acctData['ACCT_EMAIL'];
		$this->view->CURR_CODE = $acctData['ACCT_CCY'];
		$this->view->ADDRESS = $acctData['ACCT_ADDRESS1'];
		$this->view->CITIZENSHIP = $acctData['CITIZENSHIP'];
		$this->view->BANK_NAME = $acctData['ACCT_BANK_NAME'];
		$this->view->CLR_CODE = $acctData['CLR_CODE'];
		$this->view->BANK_CITY = $acctData['ACCT_BANK_CITY'];
	}
	
	private function fillParams($ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_ID=null)
	{
		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ADDRESS = $ACBENEF_ADDRESS;
		$this->view->CITIZENSHIP = $ACBENEF_CITIZENSHIP;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
		$this->view->BANK_NAME = $BANK_NAME;
		$this->view->CLR_CODE = $CLR_CODE;
		$this->view->BANK_CITY = $BANK_CITY;
	}

}
