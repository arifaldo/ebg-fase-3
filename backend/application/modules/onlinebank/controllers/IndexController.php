<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Onlinebank_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{

	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

	    $fields = array(						
						'bank_code'     => array('field' => 'BANK_OL_CODE',
											      'label' => $this->language->_('Bank Code'),
											      'sortable' => true),
						'bank_name'      => array('field' => 'BANK_OL_NAME',
											      'label' => $this->language->_('Bank Name'),
											      'sortable' => true),						
				      );

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','BANK_OL_CODE');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'bank_code' 	 => array('StringTrim','StripTags'),
							'bank_name'      => array('StringTrim','StripTags','StringToUpper'),
		);

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$getData = Application_Helper_Array::SimpleArray($fields,'field');

		/*$select = $this->_db->select()
					        ->from(array('A' => 'M_DOMESTIC_ONLINE_BANK_TABLE'),array_merge($getData,array('BANK_ID')))
					        ->where('BANK_ID <> 1');
							//->where("BANK_ID=?", $BANK_ID);*/
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_DOMESTIC_ONLINE_BANK_TABLE'),array('BANK_ID','BANK_OL_CODE','BANK_OL_NAME')); 
		//echo $select; die;
		if($filter == TRUE || $csv || $pdf || $this->_request->getParam('print'))
		{
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fbank_code = $zf_filter->getEscaped('bank_code');
			$fbank_name     = $zf_filter->getEscaped('bank_name');
			/*$fswift_code    = $zf_filter->getEscaped('swift_code');
			$fcity_code     = $zf_filter->getEscaped('CITY');*/

	        if($fbank_code) $select->where('UPPER(BANK_OL_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fbank_code).'%'));
	        if($fbank_name)     $select->where('BANK_OL_NAME LIKE '.$this->_db->quote('%'.strtoupper($fbank_name).'%'));
	        /*if($fswift_code)    $select->where('UPPER(SWIFT_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fswift_code).'%'));
	        if($fcity_code)          $select->where('UPPER(CITY_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fcity_code).'%'));*/

			$this->view->bank_code 	= $fbank_code;
			$this->view->bank_name  = $fbank_name;
			/*$this->view->swift_code  = $fswift_code;
			$this->view->CITY     = $fcity_code;*/
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);

		$select = $this->_db->fetchall($select);

		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['BANK_ID']);
		}


		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {
				$this->_helper->download->csv($header,$selectPdfCsv,null,'online_bank_list');
				Application_Helper_General::writeLog('DBLS','Download CSV Online Bank');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$selectPdfCsv,null,'online_bank_list');
				Application_Helper_General::writeLog('DBLS','Download PDF Online Bank');
		}
		else if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $selectPdfCsv, 'data_caption' => 'Online Bank', 'data_header' => $fields));
		}
		else
		{
				Application_Helper_General::writeLog('DBLS','View Online Bank');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

	}

}