<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Nostrobank_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');  
	
	    $filters = array('nostroid' => array('StringTrim', 'StripTags'));
							 
		$validators =  array(
					    'nostroid' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_NOSTRO_BANK', 'field' => 'NOSTRO_ID')),
							       	  'messages' => array(
							   						  'Cannot be empty',
							   					      'Nostro bank is not found',
							   						     ) 
							             ),
					        );
			
		if(array_key_exists('nostroid',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
				    $this->_db->beginTransaction();
				    
					$nostroid  = $zf_filter_input->getEscaped('nostroid'); 
					
					$this->_db->delete('M_NOSTRO_BANK',array('NOSTRO_ID = ?'=>$nostroid));
				   	
					
					$this->_db->commit();		
						 
					Application_Helper_General::writeLog('NBUD','Delete Nostro Bank. Nostro Bank ID : ['.$nostroid.']');
					$this->_redirect('/notification/success/index');
						
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
					$this->_redirect('/'.$this->_request->getModuleName().'/index');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					}					
				}

				Application_Helper_General::writeLog('NBUD','Update Nostro Bank');
				//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
				$this->_redirect('/'.$this->_request->getModuleName().'/index');
			} 
			
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('NBUD','Update Nostro Bank');
			
			//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
			$this->_redirect('/'.$this->_request->getModuleName().'/index');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
