<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'Service/Token.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';

class singledirectdebit_DetailreleaseController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	protected $_bankName;

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
		$this->_transferStatus 	= $conf["transfer"]["status"];
	}

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$filter 			= new Application_Filtering();

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		// var_dump($temp);
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $temp['1'];
			}
		}

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;


		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		//$AESMYSQL = new Crypt_AESMYSQL();
		//$PS_NUMBER 			= urldecode($filter->filter($this->_getParam('payReff'), "PS_NUMBER"));
		$PS_NUMBER = $this->_getParam('payReff');
		//var_dump($PS_NUMBER);die;

		$pdf 				= $filter->filter($this->_getParam('pdf'), "BUTTON");
		$cancelfuturedatebtn = $filter->filter($this->_getParam('cancelfuturedate'), "BUTTON");
		$backBtn			= $filter->filter($this->_getParam('back'), "BUTTON");
		$this->_paymentRef 	= $PS_NUMBER;

		$select = $this->_db->select()
			->from('T_PSLIP', array('*'))
			->where("PS_NUMBER = '" . $PS_NUMBER . "' AND CUST_ID = '" . $this->_custIdLogin . "'");

		// echo $select->__toString();
		// die;



		$cekPsNumber = $this->_db->fetchRow($select);

		$pslip = $cekPsNumber;
		//cari policy
		$policyBoundary = $this->findPolicyBoundary($pslip['PS_TYPE'], $pslip['PS_TOTAL_AMOUNT']);

		$checkBoundary = $this->validatebtn($pslip['PS_TYPE'], $pslip['PS_TOTAL_AMOUNT'], $pslip['PS_CCY'], $pslip['PS_NUMBER']);

		//cek privilage
		$custidlike = '%' . $this->_custIdLogin . '%';

		//select user with reviewer privilage
		$selectReviewer = $this->_db->select()
			->from(array('P' => 'M_FPRIVI_USER'))
			->joinInner(array('U' => 'M_USER'), "SUBSTRING_INDEX(P.FUSER_ID,'FIF00A1',-1) = U.USER_ID" )
			->where("P.FPRIVI_ID = 'ADRV' ")
			// ->where("P.FPRIVI_ID = 'ADRV' AND P.FUSER_ID LIKE ?", (string) $custidlike)
			->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
			->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

		$userReviewer = $this->_db->fetchAll($selectReviewer);

		$reviewerList = array();
		foreach ($userReviewer as $row) {
			$userIdReviewer = explode($this->_custIdLogin, $row['FUSER_ID']);

			//get user name
			$selectReviewerName = $this->_db->select()
				->from('M_USER')
				->where("USER_ID = ?", (string) $userIdReviewer[1]);

			$userReviewerName = $this->_db->fetchAll($selectReviewerName);

			array_push($reviewerList, $userReviewerName[0]['USER_FULLNAME']);
		}

		//function utk munculin button dengan policy grup jika belum ada yg approve
		if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5' || $pslip['PS_TYPE'] == '11') {
			$PS_TYPE = '18';
		} else {
			$PS_TYPE = $pslip['PS_TYPE'];
		}

		$approverUserList = $this->findUserBoundary($PS_TYPE, $pslip['PS_TOTAL_AMOUNT']);


		//select user with releaser privilage
		$selectReleaser = $this->_db->select()
			->from(array('P' => 'M_FPRIVI_USER'))
			->joinInner(array('U' => 'M_USER'), "SUBSTRING_INDEX(P.FUSER_ID,'FIF00A1',-1) = U.USER_ID" )
			->where("P.FPRIVI_ID = 'ADRL' ")
			// ->where("SUBSTRING_INDEX(P.FUSER_ID,'FIF00A1',-1) = U.USER_ID ")
			->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
			->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

		$userReleaser = $this->_db->fetchAll($selectReleaser);

		$releaserList = array();
		foreach ($userReleaser as $row) {
			$userIdReleaser = explode($this->_custIdLogin, $row['FUSER_ID']);

			//get user name
			$selectReleaserName = $this->_db->select()
				->from('M_USER')
				->where("USER_ID = ?", (string) $userIdReleaser[1]);

			$userReleaserName = $this->_db->fetchAll($selectReleaserName);

			array_push($releaserList, $userReleaserName[0]['USER_FULLNAME']);
		}

		$selectdatachange = $this->_db->select()
			  ->from('T_GLOBAL_CHANGES');
		$selectdatachange -> where("CUST_ID = ".$this->_db->quote($this->_custIdLogin)." OR CUST_ID ='BANK'");
		// $selectdatachange -> where("CHANGES_STATUS = ? ",'WA');
		$selectdatachange -> where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS ='RR'");
		// $selectdatachange -> where("CHANGES_STATUS = ? ",'RR');
		$selectdatachange -> where("COMPANY_CODE = ? ",$this->_custIdLogin);
		$selectdatachange -> where("DISPLAY_TABLENAME = ? ",'Approver Matrix');

		// echo $selectdatachange;die();
		 $selectdatachange1 = $selectdatachange->query()->FetchAll();

		 // echo "<pre>";
		 // var_dump($selectdatachange1);
		 // die();
		
		$error_msg2 = false;
		if(!empty($selectdatachange1)){
		  $this->view->validbtn = false;
		  $error_msg2 = true;
		  $this->view->error_msg2  = 'Cannot authorize, waiting for approval matrix changes.';
		}

		//cek ada privilage reviewer atau approver
		$selectpriv = $this->_db->select()
			->from(array('M_CUSTOMER'))
			->where("CUST_ID = ?", $this->_custIdLogin);

		$userpriv = $this->_db->fetchAll($selectpriv);

		if ($userpriv[0]['CUST_REVIEW'] != 1) {
			$cust_reviewer = 0;
		} else {
			$cust_reviewer = 1;
		}

		if ($userpriv[0]['CUST_APPROVER'] != 1) {
			$cust_approver = 0;
		} else {
			$cust_approver = 0;
		}

		if(!empty($pslip['PS_PERIODIC'])){
		$liststats = array(1,15,2);
		$selectlistPS = $this->_db->select()->from(array('P' => 'T_PSLIP'), array('A.*'))
				->joinLeft(array('A' => 'T_PSLIP_HISTORY'), 'P.PS_NUMBER = A.PS_NUMBER', array())
			->where("P.PS_PERIODIC = ?", $pslip['PS_PERIODIC'])
			->where("A.HISTORY_STATUS IN (?)", $liststats);
		$listPeriodic = $this->_db->fetchAll($selectlistPS);
		}else{
			$listPeriodic = array();
		}
		//var_dump($listPeriodic);die;

		$selectHistory	= $this->_db->select()
			->from('T_PSLIP_HISTORY')
			->where("PS_NUMBER = ?", $PS_NUMBER);

		$history = $this->_db->fetchAll($selectHistory);
	
		$history = array_merge($listPeriodic,$history); 
		//echo '<pre>';
		//var_dump($history);die;

		$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
		// echo $selectQuery;
		$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

		// var_dump($usergoogleAuth);die;
		if (!empty($usergoogleAuth)) {
			$this->view->googleauth = true;
			//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
			$settingObj = new Settings();
			$maxtoken = $settingObj->getSetting("max_failed_token");
			$this->view->tokenfail = (int)$maxtoken-1;
			if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
				//die;
				$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
				
				
				
				$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
				$this->view->tokenfail = $tokenfail;
			}
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
		}
		else{
			$this->view->nogoauth = '1';
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
		}

		$this->view->googleauth = true;

		$paramSQL = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramSQL);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);
		//echo $select;
		$pslip = $this->_db->fetchRow($select);
		$pslip['efdate'] = $pslip['PS_EFDATE'];
		
		$userOnBehalf		= $this->_getParam('userOnBehalf', "SELECTION");
		$challengeCode		= $filter->filter($this->_getParam('challengeCode'), "SELECTION");

		$inputtoken1 		= $this->_getParam('inputtoken1');
		$inputtoken2 		= $this->_getParam('inputtoken2');
		$inputtoken3 		= $this->_getParam('inputtoken3');
		$inputtoken4 		= $this->_getParam('inputtoken4');
		$inputtoken5 		= $this->_getParam('inputtoken5');
		$inputtoken6 		= $this->_getParam('inputtoken6');
 
		$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;
		$params 					= $this->_request->getParams();
	//	var_dump($params);
		//$responseCode		= $filter->filter($responseCode, "SELECTION");

		$release 			= $this->_getParam('release');
		$reject 			= $this->_getParam('reject');
		$appBankname = $app['app']['bankname'];

		$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
		$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);
		$settingObj = new Settings();
		$setting = array(
			"COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
			"COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
			"COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
			"COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
			"COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
			'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
			"range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
			"auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
			"_dateFormat" 			=> $this->_dateDisplayFormat,
			"_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
			"_transfertype" 		=> array_flip($this->_transfertype["code"]),
		);
		$paramPayment = array_merge($pslip, $setting);
		// var_dump($paramPayment);
        $this->_hasPriviApprovePayment 	= $this->view->hasPrivilege('ADRL');
		$this->_hasPriviRejectPayment 	= $this->view->hasPrivilege('ADRL');
		$step = 4;

		$this->view->allowReject 	 	= $this->_hasPriviRejectPayment;
		
		$selectTrx = $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
					'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
					'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																		CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																			 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																		END"),
					//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_ALIAS_NAME'	=> 'TT.BENEFICIARY_ALIAS_NAME',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE '-' END"),
					'TRA_STATUS'			=> 'TT.TRA_STATUS',
					'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE '-' END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
					'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
					'CLR_CODE'				=> 'TT.CLR_CODE',
					'TT.RATE',
					'TT.PROVISION_FEE',
					'TT.NOSTRO_NAME',
					'TT.FULL_AMOUNT_FEE',
					'C.PS_CCY', 'C.CUST_ID',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
					'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
					'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
        																FROM T_PERIODIC_DETAIL Y
        																inner join T_PSLIP Z
        																on Y.PS_PERIODIC = Z.PS_PERIODIC
        																where
        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
				)
				)
				->joinLeft(array('C' => 'T_PSLIP'), 'C.PS_NUMBER = TT.PS_NUMBER', array())
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);
			
			// var_dump($selectTrx);die;
			$paramTrxArr = $this->_db->fetchAll($selectTrx);
			$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
			
			
        if ($release) {
			
			if (!empty($usergoogleAuth)) {
				$token = false;
				$pga = new PHPGangsta_GoogleAuthenticator();
				// var_dump($usergoogleAuth['0']['GOOGLE_CODE']);
				// var_dump($responseCode);die;
					$settingObj = new Settings();
					$gduration = $settingObj->getSetting("google_duration");
					// var_dump($usergoogleAuth['0']['GOOGLE_CODE']);
					 // var_dump($responseCode);
					 //  var_dump($gduration);
				 if ($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, $gduration)) {
					 $resultToken = $resHard['ResponseCode'] == '0000';
					
					 $datatoken = array(
														'USER_FAILEDTOKEN' => 0
													);

													$wheretoken =  array();
													$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
													$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
													$data = $this->_db->update('M_USER',$datatoken,$wheretoken);
					$token = true;
				 } else {
					 $tokenFailed = $CustUser->setLogToken(); //log token activity
					//echo 'here';
					 $error = true;
					 $errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
					 $this->view->popauth = true;
					 if ($tokenFailed === true) {
						 $this->_redirect('/default/index/logout');
					 }
				 }
				
				//$resultToken = $resHard['ResponseCode'] == '0000';
			} else {
		
				$HardToken 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
				$resHard = $HardToken->validateOtp($responseCode);
				$resultToken = $resHard['ResponseCode'] = 'XT';

				if ($resHard['ResponseCode'] != '00') {
					$tokenFailed = $CustUser->setLogToken(); //log token activity

					$error = true;
					$errorMsg[] = "Google Auth hasn't been registered'";	//$verToken['ResponseDesc'];

					if ($tokenFailed === true) {
						$this->_redirect('/default/index/logout');
					}
				} else {
					$paymentData = array('PS_RELEASER_CHALLENGE' => $challengeCode);

					$whereData 	 = array(
						'PS_NUMBER = ?' => (string) $PS_NUMBER,
						'CUST_ID = ?'	 => (string) $this->_custIdLogin
					);
					
					$tokenFailed = $CustUser->setLogToken(); //log token activity

					$error = true;
					$errorMsg[] = "Google Auth hasn't been registered'";

					//$this->_db->update('T_PSLIP', $paymentData, $whereData);
				}
			}
			//var_dump($usergoogleAuth);
			//var_dump($token);die;
			//echo '<pre>';
			//var_dump($paramPayment);die;
			$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr, $step);
			// print_r($check);die;
			if ($validate->isError() === true) {
				$error = true;
				$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
			}
			//var_dump($this->_hasPriviReleasePayment);
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($token) {
				$resultRelease = $Payment->releasePayment();
				if ($resultRelease['status'] == '00') {
					$this->setbackURL('/' . $this->_request->getModuleName() . '/debitlist/index/release/1');
					$this->_redirect('/notification/success/index');
				} else {
					$this->_helper->getHelper('FlashMessenger')->addMessage($PS_NUMBER);
					//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
					//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
					$this->_redirect('/notification/index/release');
				}
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to release payment.");
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/payReff/' . $this->_paymentRef);
				//$this->_redirect('/' . $this->view->modulename . '/' . $this->_controllerList . '/index');
			}
		}

		if ($reject) {
			$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
			if ($this->_hasPriviRejectPayment) {
				$PS_REASON = $filter->filter($this->_getParam('PS_REASON_REJECT'), "MESSAGE");
				$Payment->rejectPayment($PS_REASON);
				$this->setbackURL('/' . $this->_request->getModuleName() . '/debitlist/index/release/1');
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Sorry, you don't have privilege to reject payment.");
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/payReff/' . $PS_NUMBER);
			}
		}
		
		foreach ($history as $row) {
			//if maker done
			if ($row['HISTORY_STATUS'] == 1) {
				$currentStatus = 1;
				$makerStatus = 'active';
				$makerIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
					$reviewerOngoing = '';
					$approverOngoing = '';
					$releaserOngoing = 'ongoing';
				} else {
					$reviewerOngoing = 'ongoing';
					$approverOngoing = '';
					$releaserOngoing = '';
				}

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				$custEmail 	  = $customer[0]['USER_EMAIL'];
				$custPhone	  = $customer[0]['USER_PHONE'];

				$makerApprovedBy = $custFullname;

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

				$align = 'align="center"';
				$marginRight = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginRight = 'style="margin-right: 15px;"';
				}

				$this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
			}
			//if reviewer done
			if ($row['HISTORY_STATUS'] == 15) {
				$currentStatus = 15;
				$makerStatus = 'active';
				$reviewStatus = 'active';
				$reviewIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'ongoing';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$reviewerApprovedBy = $custFullname;

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->reviewerApprovedBy = '<div align="center" class="textTheme">' . $efdate . '<br>' . $custFullname . '</div>';
			}
			//if approver done
			if ($row['HISTORY_STATUS'] == 2) {
				$currentStatus = 2;
				$makerStatus = 'active';
				$approveStatus = '';
				$reviewStatus = 'active';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'active';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				//tampung data user yang sudah approve
				$userid[] = $custlogin;

				$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
			}
			//if releaser done
			if ($row['HISTORY_STATUS'] == 5) {
				$currentStatus = 5;
				$makerStatus = 'active';
				$approveStatus = 'active';
				$reviewStatus = 'active';
				$releaseStatus = 'active';
				$releaseIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = '';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$releaserApprovedBy = $custFullname;

				$align = 'align="center"';
				$marginLeft = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginLeft = 'style="margin-left: 15px;"';
				}
				
				if($custFullname == ''){
					$custFullname = 'System';
				}

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';

				break;
			}

			//if rejected
			if($row['HISTORY_STATUS'] == 4){

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];

				if ($custlogin == 'System') {
					$custFullname = 'System';
				}

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

				//if current status 1 = maker, then 4 = rejected, show rejected icon on review icon
				if ($currentStatus == 1) {
					$reviewStatus = 'rejected';
					$reviewIcon = '<i class="fa fa-times"></i>';

					$makerOngoing = '';
					$reviewerOngoing = 'ongoing';
					$approverOngoing = '';
					$releaserOngoing = '';

					$this->view->reviewerApprovedBy = '<div align="center" class="text-danger">' . $efdate . '<br>' . $custFullname . '</div>';
				}
				//if current status 15 = review, then 4 = rejected, show rejected icon on approver icon
				else if($currentStatus == 15){
					$approveStatus = 'rejected';
					$approveIcon = '<i class="fa fa-times"></i>';

					$makerOngoing = '';
					$reviewerOngoing = '';
					$approverOngoing = 'ongoing';
					$releaserOngoing = '';

					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $custFullname . '</div>';
				}

				else if($currentStatus == 2){
					$rejected = true;
					$rejectedDate = $efdate;
					$rejectedBy = $custFullname;
				}

				//if currentstatus = releaser then next ada rejected, maka itu rejected by system, gausah ditampilkan
			}
		}

		//approvernamecircle jika sudah ada yang approve
		if (!empty($userid)) {

			$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

			$flipAlphabet = array_flip($alphabet);

			$approvedNameList = array();
			$i = 0;
			foreach ($userid as $key => $value) {

				//select utk nama dan email
				$selectusername = $this->_db->select()
					->from(array('M_USER'), array(
						'*'
					))
					->where("USER_ID = ?", (string) $value);

				$username = $this->_db->fetchAll($selectusername);

				//select utk cek user berada di grup apa
				$selectusergroup	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array(
						'*'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.USER_ID 	= ?", (string) $value);

				$usergroup = $this->_db->fetchAll($selectusergroup);

				$groupuserid = $usergroup[0]['GROUP_USER_ID'];
				$groupusername = $usergroup[0]['USER_ID'];
				$groupuseridexplode = explode("_", $groupuserid);

				if ($groupuseridexplode[0] == "S") {
					$usergroupid = "SG";
					$approveIcon = '<i class="fas fa-check"></i>';
				} else {
					$usergroupid = $alphabet[$groupuseridexplode[2]];
				}

				// $tempuserid = "";
				// foreach ($approverNameCircle as $row => $data) {
				// 	foreach ($data as $keys => $val) {
				// 		if ($keys == $usergroupid) {
				// 			if (preg_match("/active/", $val)) {
				// 				continue;
				// 			}else{
				// 				if ($groupuserid == $tempuserid) {
				// 					continue;
				// 				}else{
				// 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
				// 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
				// 				}
				// 				$tempuserid = $groupuserid;
				// 			}
				// 		}
				// 	}
				// }

				array_push($approvedNameList, $username[0]['USER_FULLNAME']);

				$efdate = $approveEfDate[$i];

				$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

				$i++;
			}

			//kalau sudah approve semua
			if (!$checkBoundary) {
				$approveStatus = 'active';
				$approverOngoing = '';
				$approveIcon = '<i class="fas fa-check"></i>';
				$releaserOngoing = 'ongoing';
			}

			//if udah approve semua and next = 4(rejected), maka rejected pd tahap releaser
			if (!$checkBoundary && $rejected) {
				$releaseStatus = 'rejected';
				$releaseIcon = '<i class="fa fa-times"></i>';

				$align = 'align="center"';
				$marginLeft = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginLeft = 'style="margin-left: 15px;"';
				}
				$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $rejectedDate . '<br><span ' . $marginLeft . '>' . $rejectedBy . '</span></div>';
			}
			// if current status 2 = approver and next = 4 (rejected), then ada yg reject pd tahapn approver
			else if($rejected){
				$approveStatus = 'rejected';
				$approveIcon = '<i class="fa fa-times"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'ongoing';
				$releaserOngoing = '';

				$approverApprovedBy[] = '<div align="center" class="textDanger">' . $efdate . '<br>' . $rejectedBy . '</div>';
			}
		}

		$this->view->approverApprovedBy = $approverApprovedBy;

		$selectsuperuser = $this->_db->select()
			->from(array('C' => 'T_APPROVAL'))
			->where("C.PS_NUMBER 	= ?", $PS_NUMBER)
			->where("C.GROUP 	= 'SG'");

		$superuser = $this->_db->fetchAll($selectsuperuser);

		if (!empty($superuser)) {
			$userid = $superuser[0]['USER_ID'];

			//select utk nama dan email
			$selectusername = $this->_db->select()
				->from(array('M_USER'), array(
					'*'
				))
				->where("USER_ID = ?", (string) $userid);

			$username = $this->_db->fetchAll($selectusername);


			$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

			$approveStatus = 'active';
			$approverOngoing = '';
			$approveIcon = '<i class="fas fa-check"></i>';
			$releaserOngoing = 'ongoing';

			//if udah superuser approve and next = 4(rejected), maka rejected pd tahap releaser
			if ($rejected) {
				$releaseStatus = 'rejected';
				$releaseIcon = '<i class="fa fa-times"></i>';
			}
		}


		$this->view->approverApprovedBy = $approverApprovedBy;
		// echo "<pre>";
		// var_dump($pslip);die;

		//if active no hovertext
		if ($makerStatus == 'active') {
			//define circle
			$makerNameCircle = '<img src="/assets/themes/assets/newlayout/img/maker.PNG"> <br/><button id="makerCircle" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . '" style="cursor: auto;" disabled>' . $makerIcon . '</button>';
		}
		else{
			//define circle
			$makerNameCircle = '<img src="/assets/themes/assets/newlayout/img/maker.PNG"> <br/><button id="makerCircle" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '
					<span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">' . $makerApprovedBy . '</p></span>
				</button>';
		}

		
		// echo $makerNameCircle;die;
		// var_dump($makerNameCircle);die;

		foreach ($reviewerList as $key => $value) {

			$textColor = '';
			if ($value == $reviewerApprovedBy) {
				$textColor = 'text-white-50';
			}

			$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
		}

		//if active / rejected no hovertext
		if ($reviewStatus == 'active' || $reviewStatus == 'rejected') {
			$reviewerNameCircle = '<img src="/assets/themes/assets/newlayout/img/reviewer.png"> <br/><button class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' " style="cursor: auto;" disabled>'.$reviewIcon.'</button>';
		}
		else{
			$reviewerNameCircle = '<img src="/assets/themes/assets/newlayout/img/reviewer.png"> <br/><button class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '
					<span class="hovertextcontent" style="text-align: center;">' . $reviewerListView . '</span></button>';
		}
		

		$groupNameList = $approverUserList['GROUP_NAME'];
		//echo '<pre>';
		//var_dump($approverUserList);die;
		unset($approverUserList['GROUP_NAME']);
		
		if ($approverUserList != '') {
			foreach ($approverUserList as $key => $value) {
				$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
				$i = 1;
				foreach ($value as $key2 => $value2) {

					$textColor = '';
					if (in_array($value2, $approvedNameList)) {
						$textColor = 'text-white-50';
					}

					if ($i == count($value)) {
						$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
					} else {
						$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
					}
					$i++;
				}
			}
		} else {
			$approverListdata = 'There is no Approver User';
		}

		$spandata = '';
		if (!empty($approverListdata) && !$error_msg2) {
			$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
		}
		
		//if active / rejected no hovertext
		if ($approveStatus == 'active' || $approveStatus == 'rejected') {
			$spandata = '';
			$approverNameCircle = '<img src="/assets/themes/assets/newlayout/img/approver.png"> <br/><button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' " disabled style="cursor: auto;">' . $approveIcon . ' ' . $spandata  . '</button>';
		}
		else{
			$approverNameCircle = '<img src="/assets/themes/assets/newlayout/img/approver.png"> <br/><button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . ' ' . $spandata  . '</button>';
		}

		foreach ($releaserList as $key => $value) {

			$textColor = '';
			if ($value == $releaserApprovedBy) {
				$textColor = 'text-white-50';
			}

			$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
		}

		//if active / rejected no hovertext
		if ($releaseStatus == 'active' || $releaseStatus == 'rejected') {

			$releaserNameCircle = '<img src="/assets/themes/assets/newlayout/img/releaser.png"> <br/><button id="releaserCircle" class="btnCircleGroup  ' . $releaseStatus . ' ' . $releaserOngoing . '" style="cursor: auto;" disabled>' . $releaseIcon . '</button>';
		}
		else{
			$releaserNameCircle = '<img src="/assets/themes/assets/newlayout/img/releaser.png"> <br/><button id="releaserCircle" class="btnCircleGroup  ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '
					<span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span></button>';
			
		}

		$this->view->cust_reviewer = $cust_reviewer;
		$this->view->cust_approver = $cust_approver;
		if($error_msg2){
			$policyBoundary = '';
		}


		$this->view->policyBoundary = $policyBoundary;
		$this->view->makerNameCircle = $makerNameCircle;
		$this->view->reviewerNameCircle = $reviewerNameCircle;
		$this->view->approverNameCircle = $approverNameCircle;
		$this->view->releaserNameCircle = $releaserNameCircle;

		$this->view->makerStatus = $makerStatus;
		$this->view->approveStatus = $approveStatus;
		$this->view->reviewStatus = $reviewStatus;
		$this->view->releaseStatus = $releaseStatus;

		if (!$cekPsNumber) {

			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Invalid Payment Ref#");
			$this->_redirect('/display/viewpayment/index');
		} else {
			// var_dump($cancelfuturedatebtn);
			if ($cancelfuturedatebtn) {
				$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
				$Payment->cancelFutureDate();

				$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/payReff/' . $this->_paymentRef);
				$this->_redirect('/notification/success');
			}

			$paramPayment = array(
				"WA" 				=> false,
				"ACCOUNT_LIST" 	=> $this->_accountList,
				"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
			);
			if($cekPsNumber['PS_TYPE'] == '28' || $cekPsNumber['PS_TYPE'] == '27'){
				$paramPayment['autodebit'] = true;	
			}
			// get payment query
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$select   = $CustUser->getPaymentOutgoing($paramPayment);
			$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

			$pslip = $this->_db->fetchRow($select);

			// echo "<pre>";
			// var_dump($pslip);
			// die();

			$PSSTATUS  = $pslip["PS_STATUS"];
			$PSNUMBER  = $pslip["payReff"];
			$payStatus = $pslip["payStatus"];

			$this->view->AccArr = $CustUser->getAccounts();
			$this->view->AccArrDomestic = $CustUser->getAccounts(array("CCY_IN" => array("IDR")));

			// Set variables needed in view
			$settings = new Application_Settings();
			$ccyList  = $settings->setCurrencyRegistered();

			$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
			$lldRelationshipArr = $settings->getLLDDOMRelationship();
			$lldCategoryArr = $settings->getLLDDOMCategory();

			$model = new purchasing_Model_Purchasing();

			$purposeArr = $model->getTranspurpose();

			$purposeList = array('' => '-- Select Transaction Purpose --');
			foreach ($purposeArr as $key => $value) {
				$purposeList[$value['CODE']] = $value['DESCRIPTION'];
			}

			$this->view->TransPurposeArr = $purposeList;

			$selectuser = $this->_db->select()
				->from(array('A' => 'M_USER'));
			$selectuser->where("A.CUST_ID = " . $this->_db->quote($this->_custIdLogin));
			$this->view->dataact = $selectuser->query()->fetchAll();

			$selectccy = $this->_db->select()
				->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
			$this->view->dataccy = $selectccy->query()->fetchAll();

			$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
			$caseTransStatus = "(CASE A.TRA_STATUS ";
			foreach($transstatusarr as $key=>$val)
			{
				$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
			}
			$caseTransStatus .= " ELSE 'N/A' END)";

			$selecttransaction = $this->_db->select()
				->from(array('A' => 'T_TRANSACTION'), array('A.*','TRANSFER_STATUS' => $caseTransStatus))
				->where("A.PS_NUMBER = ?", $PSNUMBER);
			//echo $selecttransaction;die;
			$this->view->datatrx = $dataTranscation = $selecttransaction->query()->fetchAll();
			$transStatus = $dataTranscation[0]['TRANSFER_STATUS'];

			// print_r($this->view->datatrx);die();


			$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
			$ccyList  			= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'

			//for skn rtgs
			$lldTypeArr  		= $settings->getLLDDOMType();
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
			$lldRelationshipArr = $settings->getLLDDOMRelationship();
			$lldPurposeArr 		= $settings->getLLDDOMPurpose();
			$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
			$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
			$residentArr 		= array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
			$citizenshipArr		= array("W" => "WNI", "N" => "WNA");

			$this->view->residentArr = $residentArr;
			$this->view->citizenshipArr = $citizenshipArr;
			$this->view->lldCategoryArr = $lldCategoryArr;
			$this->view->lldBeneIdentifArr = $lldBeneIdentifArr;
			$this->view->TransactorArr = $lldRelationshipArr;
			$this->view->IdentyArr = $lldIdenticalArr;


			$anyValue = '-- ' . $this->language->_('Select City') . ' --';
			$select = $this->_db->select()
				->from(array('A' => 'M_CITY'), array('*'));
			$select->order('CITY_NAME ASC');
			$arr = $this->_db->fetchall($select);
			$cityCodeArr 			= array('' => $anyValue);
			$cityCodeArr 			+= Application_Helper_Array::listArray($arr, 'CITY_CODE', 'CITY_NAME');
			$this->view->cityCodeArr 	= $cityCodeArr;

			if ($this->view->hasPrivilege('MTSP')) {
				$usetemp = 1;
			} else {
				$usetemp = 0;
			}

			$this->view->usetemplate = $usetemp;

			if ($PSSTATUS == 5) {

				// COMPLETED WITH (_) TRANSACTION (S) FAILED
				// TRA_STATUS FAILED (4)

				$select = $this->_db->select()
					->from('T_TRANSACTION', array('countfailed' => 'count(TRANSACTION_ID)'))
					->where("TRA_STATUS = '4' AND PS_NUMBER = ?", $PSNUMBER);
				$countFailed = $this->_db->fetchOne($select);

				if ($countFailed == 0) 	$payStatus = $payStatus;
				else 					$payStatus = 'Completed with ' . $countFailed . ' Failed Transaction(s)';
			} else $payStatus = $payStatus;

			//ambil dari slip_detail - begin
			$getPaymentDetail 	= new display_Model_Paymentreport();
			$pslipdetail = $getPaymentDetail->getPslipDetail($PS_NUMBER);

			$htmldataDetailDetail = '';

			foreach ($pslipdetail as $pslipdetaillist) {
				if ($pslipdetaillist['PS_FIELDTYPE'] == 1) {
					$value = Application_Helper_General::displayMoney($pslipdetaillist['PS_FIELDVALUE']);
				} elseif ($pslipdetaillist['PS_FIELDTYPE'] == 2) {
					//$value = Application_Helper_General::convertDate($pslipdetaillist['PS_FIELDVALUE'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					$value = $pslipdetaillist['PS_FIELDVALUE'];
				} elseif ($pslipdetaillist['PS_FIELDTYPE'] == 3) {
					$value = $pslipdetaillist['PS_FIELDVALUE'];
				} else {
					$value = '';
				}

				$htmldataDetailDetail .= '
				<tr>
					<td class="">' . $this->language->_($pslipdetaillist['PS_FIELDNAME']) . '</td>
					<td class="">' . $this->language->_($value) . '</td>
				</tr>
				';
			}
			// print_r($htmldataDetailDetail);die;
			if (isset($htmldataDetailDetail)) $this->view->templateDetail = $htmldataDetailDetail;
			//ambil dari slip_detail - end
			$this->_tableMstleft[0]["label"] = $this->language->_('Payment Type');
			$this->_tableMstleft[1]["label"] = $this->language->_('Payment Ref') . "#";
			$this->_tableMstleft[2]["label"] = $this->language->_('Payment Subject');
			$this->_tableMstleft[3]["label"] = $this->language->_('Trans#');
			if ($pslip['PS_TYPE'] == 27){
				$this->_tableMstleft[4]["label"] = $this->language->_('Email');
			}
			if ($pslip['PS_TYPE'] == 19 && $pslip['numtrx'] > 1 || $pslip['PS_TYPE'] == 28) {
				$this->_tableMstleft[5]["label"] = ' ';
			}else{
				$this->_tableMstleft[5]["label"] = $this->language->_('Message');
			}
			// $this->_tableMstleft[5]["label"] = $this->language->_('Additional Message');

			$tra_type1 = $trfType[$pslip['TRANSFER_TYPE']];

			$tra_type2	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");
			$tra_type3 = $tra_type2[$pslip['TRANSFER_TYPE']];
                    
            if ($pslip['PS_TYPE'] == 19) {
                 $payType = 'CP Same Bank Remains';
            }else if ($pslip['PS_TYPE'] == 20) {
                 $payType = 'CP Same Bank Maintains';
            }else if ($pslip['PS_TYPE'] == 27) {
                 $payType = 'Single Auto Debit';
            }else if ($pslip['PS_TYPE'] == 28) {
                 $payType = 'Bulk Auto Debit';
            }else if ($pslip['PS_TYPE'] == 23) {
                 $payType = 'CP Others Remains - '.$tra_type3;
            }else if ($pslip['PS_TYPE'] == 21) {
                 $payType = 'MM - '.$tra_type3;
            }else if($pslip['PS_TYPE'] == '25' || $pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '26' || $pslip['PS_TYPE'] == '28' || $pslip['PS_TYPE'] == '3' ){
				$payType = $pslip['payType'];
			}else {
                $payType = $pslip['payType'] . ' - ' . $tra_type1;
            }

			if($pslip['paySubj'] == "" ){
				$paySubj = 'no subject';
			}else{
				$paySubj = $pslip['paySubj'];
			}

			$this->_tableMstleft[0]["value"] = $payType;
			$this->_tableMstleft[1]["value"] = $PS_NUMBER;
			$this->_tableMstleft[2]["value"] = $paySubj;
			// $buttondownload = '';
			if ($pslip['numtrx'] > 1) {
				// download trx bulk file
				$downloadURL = $this->view->url(array('module' => 'singledirectdebit', 'controller' => 'detailrelease', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $pslip['PS_NUMBER']), null, true);

				$buttondownload = $this->view->formButton('download', $this->language->_('download'), array('class' => 'btnwhite hov ml-1', 'onclick' => "window.location = '" . $downloadURL . "';"));
			}
			$this->_tableMstleft[3]["value"] = $pslip['numtrx'] . " " . $buttondownload;
			
			// $this->_tableMstleft[3]["value"] = $pslip[''];
			if ($pslip['PS_TYPE'] == 27){
				$this->_tableMstleft[4]["value"] = $pslip['BENEFICIARY_EMAIL'];
			}

			if ( $pslip['PS_CATEGORY'] == 'OPEN TRANSFER') {
			if(!empty($pslip['BENEFICIARY_ID'])){
			 $this->getBenefData($pslip['BENEFICIARY_ID']);
			}else{
				$citizenshipArr	= array("W" => "WNI", "N" => "WNA");
				$residentArr = array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);

				$benefData = $pslip;
				//var_dump($benefData['BENEFICIARY_RESIDENT']);die;
				//echo '<pre>';
				//var_dump($detail[0]);die;
				$tableMst[0]["label"] = $this->language->_('Beneficiary Bank');
				$tableMst[1]["label"] = $this->language->_('City');
				$tableMst[2]["label"] = $this->language->_('Beneficiary Account');
				$tableMst[3]["label"] = $this->language->_('Beneficiary Name');
				$tableMst[4]["label"] = $this->language->_('Beneficiary Alias Name');
				$tableMst[5]["label"] = $this->language->_('Beneficiary Address 1');
				$tableMst[6]["label"] = $this->language->_('Beneficiary Address 2');
				$tableMst[7]["label"] = $this->language->_('Beneficiary Email');
				$tableMst[8]["label"] = $this->language->_('Citizenship');
				$tableMst[9]["label"] = $this->language->_('Nationality');
				$tableMst[10]["label"] = $this->language->_('Beneficiary Category');
				$tableMst[11]["label"] = $this->language->_('Beneficiary ID Type');
				$tableMst[12]["label"] = $this->language->_('Beneficiary ID Number');

				if (!empty($benefData['BENEF_ACCT_BANK_CODE'])) {
                      $benefBankName = Application_Helper_General::getBankTableName($benefData['BENEF_ACCT_BANK_CODE']);
                 }
                else{
                       $benefBankName = $this->masterBankName;
                }
				$tableMst[0]["value"] = $benefBankName;
				$tableMst[1]["value"] = empty($benefData['BANK_CITY']) ? '-' : $benefData['BANK_CITY'];
				$tableMst[2]["value"] = $benefData['BENEFICIARY_ACCOUNT'];
				$tableMst[3]["value"] = $benefData['BENEFICIARY_ACCOUNT_NAME'];
				$tableMst[4]["value"] = empty($benefData['BENEFICIARY_ALIAS_NAME']) ? '-' : $benefData['BENEFICIARY_ALIAS_NAME'];
				$tableMst[5]["value"] = empty($benefData['BENEFICIARY_ADDRESS']) ? '-' : $benefData['BENEFICIARY_ADDRESS'];
				$tableMst[6]["value"] = empty($benefData['BENEFICIARY_ADDRESS2']) ? '-' : $benefData['BENEFICIARY_ADDRESS2'];
				$tableMst[7]["value"] = empty($benefData['BENEFICIARY_EMAIL']) ? '-' : $benefData['BENEFICIARY_EMAIL'];
				$tableMst[8]["value"] = empty($benefData['BENEFICIARY_RESIDENT']) ? '-' : $residentArr[$benefData['BENEFICIARY_RESIDENT']];
				$tableMst[9]["value"] = empty($benefData['BENEFICIARY_CITIZENSHIP']) ? '-' : $citizenshipArr[$benefData['BENEFICIARY_CITIZENSHIP']];
				$tableMst[10]["value"] = empty($LLD_CATEGORY_POST) ? '-' : $LLD_CATEGORY_POST;
				$tableMst[11]["value"] = empty($benefData['BENEFICIARY_ID_TYPE']) ? '-' : $benefData['BENEFICIARY_ID_TYPE'];
				$tableMst[12]["value"] = empty($benefData['BENEFICIARY_ID_NUMBER']) ? '-' : $benefData['BENEFICIARY_ID_NUMBER'];

				$this->view->tableBene 		= $tableMst;
			}
		}
			

			// if (($pslip['PS_CATEGORY'] == 'SINGLE PAYMENT' || $pslip['PS_CATEGORY'] == 'OPEN TRANSFER')) {
			// 	if ($pslip['TRANSFER_TYPE'] == 0) {
			// 		if ($pslip['SOURCE_ACCT_BANK_CODE'] == $pslip['BENEF_ACCT_BANK_CODE']) {
			// 			$payType = $pslip['payType'] . ' - ' . 'PB';
			// 		} else {
			// 			$payType = $pslip['payType'] . ' - ' . 'ONLINE';
			// 		}
			// 	} else if ($pslip['TRANSFER_TYPE'] == 1) {
			// 		$payType = $pslip['payType'];
			// 	} else if ($pslip['TRANSFER_TYPE'] == 2) {
			// 		$payType = $pslip['payType'];
			// 	} else {
			// 		$payType = $pslip['payType'];
			// 	}
			// } else if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

			// 	if ($pslip['PS_TYPE'] == '19') {
			// 		$poolingType = 'Remains';
			// 	} else {
			// 		$poolingType = 'Maintains';
			// 	}

			// 	$payType = $pslip['payType'] . ' - ' . $poolingType;
			// } else {
			// 	$payType = $pslip['payType'];
			// }

			$select  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'), array('P.*', 'A.*'))
				->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
				->where('P.PS_NUMBER = ?', $PS_NUMBER);

			$cekPsNumber = $this->_db->fetchRow($select);
			$PS_EVERY_PERIODIC_UOM = $cekPsNumber['PS_EVERY_PERIODIC_UOM'];
			$PS_PERIODIC_NUMBER = $cekPsNumber['PS_PERIODIC_NUMBER'];
			$PS_EVERY_PERIODIC_NEW = $cekPsNumber['PS_EVERY_PERIODIC'];

			if ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 1) {
				$select = $this->_db->select()
					->from(array('C' => 'T_PERIODIC_DAY', array('C.DAY_ID')))
					->where("C.PERIODIC_ID  = ?", $cekPsNumber["PS_PERIODIC"]);

				$PERIODIC_DAY = $this->_db->fetchAll($select);
				$days = array();
				foreach ($PERIODIC_DAY as $key) {
					$days[] = (int) $key['DAY_ID'];
				}
				$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
				$dateNow = date("Y-m-d H:i");
				$result = $date;
				foreach ($days as $day) {
					if ($dateNow > $result) {
						$date = $dateNow;
						$dayofweek = date('N', strtotime($date));
						$result = date('Y-m-d', strtotime(($day - $dayofweek) . ' day', strtotime($date)));
						$result = $result . ' ' . $cekPsNumber["PS_EFTIME"];
					}
				}

				if ($result < $dateNow) {
					$date = date("Y-m-d", strtotime("+1 week", strtotime($date)));
					$dayofweek = date('N', strtotime($date));
					$result = date('Y-m-d H:i', strtotime(($days[0] - $dayofweek) . ' day', strtotime($date)));
				}
				$date = $result;
				$date = strtotime($date);
				if (date('Y-m-d', $date) == date('Y-m-d')) {
					$nextExecute = $this->language->_('Today') . ' @' . date('H:i', $date);
				} else {
					$nextExecute = date('d M Y @H:i', $date);
				}
			} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 2 || $cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 3) {
				$date = $cekPsNumber["PS_PERIODIC_NEXTDATE"];
				$nextExecute = $date;

			} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 5) {
				$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
				$dateNow = date("Y-m-d H:i");

				if ($dateNow > $date) {
					$date = $dateNow;
				}
				$dayofweek = date('N', strtotime($date));
				$result = date('Y-m-d', strtotime(($cekPsNumber['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
				$result = $result . ' ' . $cekPsNumber["PS_EFTIME"];
				if ($result < $dateNow) {
					$date = date("Y-m-d", strtotime("+1 week"));
					$result = date('Y-m-d', strtotime(($cekPsNumber['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
				}
				$nextExecute = date("Y-m-d", strtotime($result));
			} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 6) {
				$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
				$dateNow = date("Y-m-d H:i");

				if ($date < $dateNow) {
					$nextExecute = date("Y-m-d", strtotime("+1 month", strtotime($date)));
				}
			}

			// if ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] != 1) {
			// 	$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
			// 	// var_dump($date);
			// 	$date = strtotime($date);
			// 	if (date('Y-m-d', $date) == date('Y-m-d')) {
			// 		$nextExecute = $this->language->_('Today') . ' @' . date('H:i', $date);
			// 	} else {
			// 		$nextExecute = date('d M Y @H:i', $date);
			// 	}
			// }

			// var_dump($nextExecute);
			// die();
			
			$nextpaymentdate = substr(Application_Helper_General::convertDate($nextExecute, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);

			
			
			$arrdayongoing = array(
					'0' => 'sun',
					'1' => 'mon',
					'2' => 'tue',
					'3' => 'wed',
					'4' => 'thu',
					'5' => 'fry',
					'6' => 'sat'

				);

			$PeriodIdSweepIn = $pslip['PS_PERIODIC'];
			if($PeriodIdSweepIn){
			$selectday	= $this->_db->select()
				->from(array('TTS' => 'T_PERIODIC_DAY'))
				->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
			
			$report_day = $this->_db->fetchAll($selectday);
			
			//var_dump($report_day);
			if (!empty($report_day)) {
							foreach ($report_day as $key => $value) {
								//var_dump($value['DAY_ID']); 
								${'pooling_' . $arrdayongoing[$value['DAY_ID']]} = $value['LIMIT_AMOUNT'];
							}
			}
			//var_dump($arrdayongoing);
			//var_dump($pooling_sun);
			$this->view->pooling_sun = $pooling_sun;
			$this->view->pooling_mon = $pooling_mon;
			$this->view->pooling_tue = $pooling_tue;
			$this->view->pooling_wed = $pooling_wed;
			$this->view->pooling_thu = $pooling_thu;
			$this->view->pooling_fry = $pooling_fry;
			$this->view->pooling_sat = $pooling_sat;

			$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
			$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
			$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
			$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
			$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
			$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
			$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);
			
			
			$selectperiodic	= $this->_db->select()
				->from(array('TTS' => 'T_PERIODIC'))
				->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);			
			$selectperiodic = $this->_db->fetchRow($selectperiodic);
			$nowdate = date('Y-m-d');
			
			}

			$this->_tableMstright[0]["label"] = $this->language->_('Frequently');
			
			//if sweep 
			if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

				if  (empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[2]["label"] = $this->language->_('Execute Date');
					$this->_tableMstright[2]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					$repeat = '1x';
				}else{
					if ($PS_EVERY_PERIODIC_UOM == '3' || $PS_EVERY_PERIODIC_UOM == '6') {
						$this->_tableMstright[1]["label"] = $this->language->_('Repeat on date');
					}else{
						$this->_tableMstright[1]["label"] = $this->language->_('Repeat on');
					}
					
					$this->_tableMstright[2]["label"] = $this->language->_('Start From');
				}

				$this->_tableMstright[3]["label"] = $this->language->_('Time');

				if (!empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[4]["label"] = $this->language->_('Execute Date');
					$this->_tableMstright[4]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
				}
				
				if  (empty($pslip['PS_PERIODIC'])) {
					// $this->_tableMstright[2]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					// $repeat = '1x';
					// $repeatOn = '-';

					// $efDate = date_create($pslip['PS_EFDATE']);
					// $newEfDate = date_format($efDate, "d M Y");

					// $startFrom = $newEfDate;
				}
				else{
					
					$select	= $this->_db->select()
							->from(array('P'	 		=> 'T_PSLIP'))
							->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
							->joinLeft(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
							->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
							->GROUP('D.DAY_ID');

					$periodicData = $this->_db->fetchAll($select);
					
					$arrday = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);
					
					if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 1) {

						$repeat = 'Daily';
						$repeatOnPdf = '<div class="input-group">';
						foreach ($periodicData as $key => $value) {
						$repeatOnPdf .= substr($arrday[$value['DAY_ID']], 0, 1).'   ';
						}
						//echo '<pre>'; 
						//var_dump($periodicData);die;
						$repeatOnPdf .= '</div>';
						
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						foreach ($periodicData as $key => $value) {
						$labelcheck = 'check'.$value['DAY_ID'];
						${$labelcheck} = 'checked';
						
						}
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 2 || $periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 5) {
						$repeat = 'Weekly';
						$repeatOnPdf = $arrday[$periodicData[0]['PS_EVERY_PERIODIC']];
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						
						$labelcheck = 'check'.$periodicData[0]['PS_EVERY_PERIODIC'];
						${$labelcheck} = 'checked';
 
						 foreach ($periodicData as $key => $value) {
						 $labelcheck = 'check'.$value['DAY_ID'];
						 ${$labelcheck} = 'checked';
						
						 }
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else {
						$repeat = 'Monthly';
						$repeatOn = $periodicData[0]['PS_EVERY_PERIODIC'];
						$repeatOnPdf = $periodicData[0]['PS_EVERY_PERIODIC'];
					}

					$startDate = date_create($periodicData[0]['PS_PERIODIC_STARTDATE']);
					$newStartDate = date_format($startDate, "d M Y");

					$endDate = date_create($periodicData[0]['PS_PERIODIC_ENDDATE']);
					$newEndDate = date_format($endDate, "d M Y");

					$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;

					// $arrday = array(
					// 	'0' => 'Sunday',
					// 	'1' => 'Monday',
					// 	'2' => 'Tuesday',
					// 	'3' => 'Wednesday',
					// 	'4' => 'Thursday',
					// 	'5' => 'Friday',
					// 	'6' => 'Saturday'
					// );

					// if ($pslip['PS_TYPE'] == '19') {
					// 	//remain
					// 	$amountlabel = $this->language->_('Remain on source');

					// } else if ($pslip['PS_TYPE'] == '20') {
					// 	$amountlabel = $this->language->_('Maintain Beneficiary');

					// 	$maintainsData = '';

					// 	foreach ($periodicData as $key => $value) {
					// 		$maintainsData .= substr($arrday[$value['DAY_ID']], 0, 3) . ' IDR ' . Application_Helper_General::displayMoney($value['LIMIT_AMOUNT']) . '<br>';
					// 	}

					// 	$content = "<div class='text-center'>".$maintainsData."</div>";

					// 	// $amount = '<button type="button" class="btngrad hov" data-html="true" tabindex="0" data-toggle="popover" data-trigger="focus" title="Maintains Config" data-content="'.$content.'">View</button>';
						
					// }
					$this->_tableMstright[1]["value"] = $repeatOn;
					$this->_tableMstright[2]["value"] = $startFrom;
				}

				$AESMYSQL = new Crypt_AESMYSQL();
				$encrypted_pwd = $AESMYSQL->encrypt($PS_NUMBER, $rand);
				$encreff = urlencode($encrypted_pwd);

				if (($pslip['PS_STATUS'] == 5  || $pslip['PS_STATUS'] == 7 || $pslip['PS_STATUS'] == 14) && $pslip['PS_EFDATE'] >= $nowdate && $selectperiodic['PS_PERIODIC_ENDDATE'] >= $nowdate) {
					$this->_tableMstright[0]["value"] = ($repeat == '1x') ? $repeat : $repeat .' (FreqRef# <a href="/paymentworkflow/ongoing/viewdetail/payReff/'.$encreff.'">'.$PS_PERIODIC_NUMBER.'</a>)';
				}else{
					$this->_tableMstright[0]["value"] = ($repeat == '1x') ? $repeat : $repeat;
				}

				if ($pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '23') {
					$amountlabel = $this->language->_('Remain on source');
				} else if ($pslip['PS_TYPE'] == '20') {
					$amountlabel = $this->language->_('Maintain Beneficiary');					
				}

				
				
				$amount1 	 = '<span>' . $pslip['ccy'] .' '.Application_Helper_General::displayMoney($pslip['PS_MIN_AMOUNT']).'</span>';

				$eftime = date('h:i A', strtotime($pslip['PS_EFTIME']));
				$this->_tableMstright[3]["value"] = $eftime;
				$this->_tableMstright[5]["label"] = $amountlabel;
				if ($pslip['PS_TXCOUNT'] > 1) {
					$amount= '-';
					$pdfamount = '-';
					$counttrans = $pslip['numtrx']."<strong class='formreq' style='color:red;'>&nbsp;*</strong>";
				}else{
					$counttrans = $pslip['numtrx'];
					if ($pslip["PS_TYPE"] == '20' && $PS_EVERY_PERIODIC_UOM == '1') {
						$amount = ' - <button type="button" class="btngrad hov" class="toogleModal text-primary" data-toggle="modal" data-target="#poolingConfirmModal">View Config</button>';
						$pdfamount = '-'."<strong class='formreq' style='color:red;'>&nbsp;*</strong>";
					}else{
						$amount = '<span>' . $pslip['ccy'] . ' '.Application_Helper_General::displayMoney($pslip['TRA_REMAIN']).'</span>';
						$pdfamount = '<span>' . $pslip['ccy'] . ' '.Application_Helper_General::displayMoney($pslip['TRA_REMAIN']).'</span>';
					}
				}
				$this->_tableMstright[5]["value"] = $amount;
				$this->_tableMstright[6]["label"] = 'Minimum Transfer';
				$this->_tableMstright[6]["value"] = $amount1;

				if ($pslip['TRANSFER_TYPE'] == '1' && $pslip['PS_TYPE'] == '23') {
					$this->_tableMstright[7]["label"] = $this->language->_('If sweep exceed RTGS');
					$this->_tableMstright[7]["value"] = 'Split if amount exceed RTGS limit';
				}else if($pslip['TRANSFER_TYPE'] == '2' && $pslip['PS_TYPE'] == '23'){
					$this->_tableMstright[7]["label"] = $this->language->_('If sweep exceeds SKN');
					$this->_tableMstright[7]["value"] = 'Cancel if amount exceed SKN limit';
				}
				
				$this->_tableMstright[8]["label"] = $this->language->_('Status');
				$this->_tableMstright[8]["value"] = $payStatus.' - '.$transStatus;
			}
			else{

				if  (empty($pslip['PS_PERIODIC'])) {
					$repeat = '1x';
					$this->_tableMstright[1]["label"] = $this->language->_('Execute Date');
					$this->_tableMstright[1]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					$this->_tableMstright[2]["label"] = $this->language->_(' ');
					$this->_tableMstright[2]["value"] = '';
				}
				else{
					// $select	= $this->_db->select()
					// 		->from(array('S' => 'T_PERIODIC'))
					// 		->where("S.PS_PERIODIC = ?", (string) $pslip['PS_PERIODIC']);

					// $periodicData = $this->_db->fetchAll($select);

					// if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] === null) {
					// 	$repeat = '1x';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '1') {
					// 	$repeat = 'Daily';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '2') {
					// 	$repeat = 'Weekly';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '3') {
					// 	$repeat = 'Monthly';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '4') {
					// 	$repeat = 'Yearly';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '5') {
					// 	$periodicEveryArr = array(
					// 		'1' => $this->language->_('Monday'),
					// 		'2' => $this->language->_('Tuesday'),
					// 		'3' => $this->language->_('Wednesday'),
					// 		'4' => $this->language->_('Thursday'),
					// 		'5' => $this->language->_('Friday'),
					// 		'6' => $this->language->_('Saturday'),
					// 		'7' => $this->language->_('Sunday'),
					// 	);
					// 	$repeat = $this->language->_('Every Day of ') . $periodicEveryArr[$periodicData[0]['PS_EVERY_PERIODIC']];
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '6') {
					// 	$repeat = $this->language->_('Every Date of ') . $periodicData[0]['PS_EVERY_PERIODIC'];
					// } else {
					// 	$repeat = '-';
					// }

					

					$select1	= $this->_db->select()
							->from(array('P'	 		=> 'T_PSLIP'))
							->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
							->joinLeft(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
							->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
							->GROUP('D.DAY_ID');

					$periodicData1 = $this->_db->fetchAll($select1);
					//echo '<pre>';
					//var_dump($periodicData1);die; 
					$arrday = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);
					if ($periodicData1[0]['PS_EVERY_PERIODIC_UOM'] == 1) {

						$repeat = 'Daily';
						$repeatOnPdf = '<div class="input-group">';
						foreach ($periodicData1 as $key => $value) {
							$repeatOnPdf .= substr($arrday[$value['DAY_ID']], 0, 1).'   ';
						}
						$repeatOnPdf .= '</div>';
						
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						foreach ($periodicData1 as $key => $value) {
						$labelcheck = 'check'.$value['DAY_ID'];
						//var_dump($labelcheck);
						${$labelcheck} = 'checked';
						
						}
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;">
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" checked />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						//echo $repeatOn;				
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
						
					} else if ($periodicData1[0]['PS_EVERY_PERIODIC_UOM'] == 2 || $periodicData1[0]['PS_EVERY_PERIODIC_UOM'] == 5) {
						$repeat = 'Weekly';
						$repeatOnPdf = $arrday[$periodicData1[0]['PS_EVERY_PERIODIC']];
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						
						$labelcheck = 'check'.$periodicData1[0]['PS_EVERY_PERIODIC'];
						${$labelcheck} = 'checked';

						 foreach ($periodicData1 as $key => $value) {
						 $labelcheck = 'check'.$value['DAY_ID'];
						 ${$labelcheck} = 'checked';
						
						 }
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else {
						$repeat = 'Monthly';
						$repeatOn = $PS_EVERY_PERIODIC_NEW;
						$repeatOnPdf = $PS_EVERY_PERIODIC_NEW;
					}

					$startDate = date_create($periodicData1[0]['PS_PERIODIC_STARTDATE']);
					$newStartDate = date_format($startDate, "d M Y");

					$endDate = date_create($periodicData1[0]['PS_PERIODIC_ENDDATE']);
					$newEndDate = date_format($endDate, "d M Y");

					$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;

					if ($PS_EVERY_PERIODIC_UOM == '3') {
						$this->_tableMstright[3]["label"] = $this->language->_('Repeat on date');
					}else{
						$this->_tableMstright[3]["label"] = $this->language->_('Repeat on');
					}

					$this->_tableMstright[3]["value"] = $repeatOn;
					$this->_tableMstright[4]["label"] = $this->language->_('Start From');
					$this->_tableMstright[4]["value"] = $startFrom;
				}

				$AESMYSQL = new Crypt_AESMYSQL();
				$encrypted_pwd = $AESMYSQL->encrypt($PS_NUMBER, $rand);
				$encreff = urlencode($encrypted_pwd);

				if (($pslip['PS_STATUS'] == 5  || $pslip['PS_STATUS'] == 7 || $pslip['PS_STATUS'] == 14) && $pslip['PS_EFDATE'] >= $nowdate && $selectperiodic['PS_PERIODIC_ENDDATE'] >= $nowdate) {
					$this->_tableMstright[0]["value"] = ($repeat == '1x') ? $repeat : $repeat .' (FreqRef# <a href="/paymentworkflow/ongoing/viewdetail/payReff/'.$encreff.'">'.$PS_PERIODIC_NUMBER.'</a>)';
				}else{
					$this->_tableMstright[0]["value"] = ($repeat == '1x') ? $repeat : $repeat;
				}

				if (!empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[5]["label"] = $this->language->_('Execute Date');
					$this->_tableMstright[5]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
				}
				
				$this->_tableMstright[6]["label"] = $this->language->_('Total Amount');
				$this->_tableMstright[6]["value"] = '<span style="color: red;"><b>' .$pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['amount']).'</b></span>';
				$this->_tableMstright[7]["label"] = $this->language->_('Status');
				$this->_tableMstright[7]["value"] = $payStatus.' - '.$transStatus;

			}
			
			$this->view->pdfpaytype = $payType;
			$this->view->pdfpsnumber = $PS_NUMBER;
			$this->view->pdfpaysubject= $pslip['paySubj'];
			$this->view->pdftrans= $counttrans;
			$this->view->pdftramessage = $pslip['TRA_MESSAGE'];

			$this->view->PS_EVERY_PERIODIC_UOM = $PS_EVERY_PERIODIC_UOM;
			$this->view->pdfnextpaymentdate = $nextpaymentdate;
			$this->view->pdfrepeaton = $repeatOnPdf;
			$this->view->pdftime = $eftime;
			$this->view->pdfstartfrom = $startFrom;
			$this->view->pdftraremain = $pdfamount;
			$this->view->pdfminamount = '<span>' . $pslip['ccy'] .' '.Application_Helper_General::displayMoney($pslip['PS_MIN_AMOUNT']).'</span>';

			$this->view->pdffrequently = $repeat;
			$this->view->pdfpaymentdate = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
			$this->view->pdftotalamount = '<span style="color: red;"><b>' .$pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['amount']).'</b></span>';
			$this->view->pdfstatus = $payStatus.' - '.$transStatus;

			// if ($pslip['PS_TYPE'] == '19') {
			// 	//remain
			// 	$amountlabel = $this->language->_('Remain on source');
			// 	$amount 	 = 'IDR n/a';
			// } else if ($pslip['PS_TYPE'] == '20') {
			// 	$amountlabel = $this->language->_('Maintains beneficiary');
			// 	$amount 	 = 'IDR (as config)';
			// } else {
			// 	$amountlabel = $this->language->_('Total Amount');
			// 	$amount 	 = $pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['amount']);
			// }
			// $this->_tableMstright[2]["label"] = $amountlabel;
			// $this->_tableMstright[2]["value"] = $amount;
			// $this->_tableMstright[3]["label"] = $this->language->_('Payment Status');
			// $this->_tableMstright[3]["value"] = $payStatus;
			// $this->_tableMstright[4]["label"] = $this->language->_('Transaction Status');
			// $this->_tableMstright[4]["value"] = $transStatus;

			// $this->_tableMstleft[1]["label"] = $this->language->_('Payment Subject');
			// $this->_tableMstleft[2]["label"] = $this->language->_('Payment Date');
			// $this->_tableMstleft[3]["label"] = $this->language->_('Message');
			// $this->_tableMstleft[4]["label"] = $this->language->_('Additional Message');


			// View Data
			$this->_tableMst[0]["label"] = $this->language->_('Payment Ref') . "#";
			$this->_tableMst[1]["label"] = $this->language->_('Created Date');
			$this->_tableMst[2]["label"] = $this->language->_('Updated Date');
			$this->_tableMst[3]["label"] = $this->language->_('Execute Date');
			$this->_tableMst[4]["label"] = $this->language->_('Payment Subject');
			$this->_tableMst[5]["label"] = $this->language->_('Master Account');
			$this->_tableMst[6]["label"] = $this->language->_('Payment Type');
			$this->_tableMst[7]["label"] = $this->language->_('Payment Status');
			$this->_tableMst[8]["label"] = $this->language->_('Source Bank Name');
			
			$this->_tableMst[0]["value"] = $PS_NUMBER;
			$this->_tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			$this->_tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			$this->_tableMst[3]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
			$this->_tableMst[4]["value"] = $pslip['paySubj'];
			$this->_tableMst[5]["value"] = "";
			$this->_tableMst[6]["value"] = $pslip['payType'];
			$this->_tableMst[7]["value"] = $payStatus;

			/*	pending future date = 7 ;
				display button Cancel Future Date, if payment status 'Pending Future Date'
			*/
			//echo "pstype: ";
			//echo $pslip["PS_STATUS"];
			$selectBank	= $this->_db->select()
				->from(
					array('B' => 'M_BANK_TABLE'),
					array(
						'BANK_CODE'			=> 'B.BANK_CODE',
						'BANK_NAME'			=> 'B.BANK_NAME'
					)
				);

			$bankList = $this->_db->fetchAll($selectBank);

			foreach ($bankList as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			if (empty($pslip['SOURCE_ACCT_BANK_CODE'])) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$pslip['SOURCE_ACCT_BANK_CODE']];
			}

			$this->_tableMst[8]["value"] = $bankname;

			if ($pslip['PS_STATUS'] == 7 && $this->view->hasPrivilege('CFDT')) {
				$this->view->paypendingfuture = true;
			} // privillege cancel future date.

			//View File dihidden untuk PAYROLL
			if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && !$pdf && $pslip["PS_TYPE"] != "11") {
				// download trx bulk file
				$downloadURL = $this->view->url(array('module' => 'singledirectdebit', 'controller' => 'detailrelease', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
				$this->_tableMst[9]["label"] = $this->language->_('View File');
				$this->_tableMst[9]["value"] = $this->view->formButton('download', $this->language->_('download'), array('class' => 'btngreen hov', 'onclick' => "window.location = '" . $downloadURL . "';"));
			}

			// separate credit and debet view
			if (
				// $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] ||
				$pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]
			) {
				// die('hee');
				$this->debet($pslip);
			} elseif ($pslip["PS_BILLER_ID"] == '1156' || $pslip["PS_BILLER_ID"] == '1158') {
				// echo 'ss';die;
				$this->etax($pslip);
			} else {
				// die('sene');

				$this->credit($pslip);
			}
			//var_dump($this->_tableMstleft);die;
			//Zend_Debug::dump($pslip["PS_TYPE"]); die;
			// && $pslip['PS_STATUS'] != 5  && != 6 Hannichr2010
			// View Transfer to di hidden untuk PAYROLL
			if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && $pslip['PS_STATUS'] != 5 && $pslip['PS_STATUS'] != 6 || $pslip['PS_TYPE'] == 11) {
				// $this->view->fields			 = array();
				// $this->view->tableDtl 		 = array();
				// $this->view->TITLE_DTL		 = "";
			}

			/*
				Status payment :
					1. PayStatus (5) completed + TransStatus (3) success
					2. PayStatus (4) rejected
					2. PayStatus (6) transferfailed + TransStatus (4) Failed


			*/
			$txtMessage = '';
			if ($pslip['PS_STATUS'] == 4) {
				$txtMessage 				= $this->language->_('Payment Rejected');
				$this->view->flagstatus 	= true;
			}
			if (($pslip['PS_STATUS'] == 5) || ($pslip['PS_STATUS'] == 7)) {
				$custId 	= $this->_custIdLogin;

				$UUID = $this->_db->select()
					->from(
						array('P'		=> 'T_PSLIP'),
						array('uuid'	=> 'UUID')
					)
					->where('PS_NUMBER = ' . $this->_db->quote($PS_NUMBER) . ' AND CUST_ID = ?', $custId);

				$UUID = $this->_db->fetchOne($UUID);
				//->query()
				//->fetchOne();

				$daterelease = $this->_db->select()
					->from(
						array('P'		=> 'T_PSLIP_HISTORY'),
						array('date'	=> 'DATE_TIME')
					)
					->where('PS_NUMBER = ' . $this->_db->quote($PS_NUMBER) . ' AND CUST_ID = ?', $custId);

				$daterelease = $this->_db->fetchOne($daterelease);
				//date("d/m/Y H:i:s")
				if ($daterelease) {
					$daterelease = Application_Helper_General::convertDate($daterelease, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					//$daterelease = date("d/m/Y H:i:s",$daterelease);
				} else $daterelease = '';

				//->query()
				//->fetchOne();
				$txtMessage = "
					<p><h4><u>" . $this->language->_('Release ' . $pslip["payStatus"]) . "</u></h4></p>
					<p><h5>" . $this->language->_('Release Date') . ": <b>" . $daterelease . " </b></h5></p>";

				$this->view->flagstatus 	= true;

				//<p><h5>Trace ID: <b>".$UUID."</b></h5></p>
			}
			if ($pslip['PS_STATUS'] == 6) {
				$txtMessage 				= $this->language->_('Transfer Failed');
				$this->view->flagstatus 	= true;
			}
			if ($pslip['PS_STATUS'] == 9) {
				$txtMessage 				= $this->language->_('Payment Exception');
				$this->view->flagstatus 	= true;
			}

			$this->view->txtMessage = $txtMessage;

			$this->view->pslip 				= $pslip;
			$this->view->PS_NUMBER 			= $PS_NUMBER;
			$this->view->tableMst 			= $this->_tableMst;


			$this->view->tableMstleft 			= $this->_tableMstleft;
			$this->view->tableMstright 			= $this->_tableMstright;



			$this->view->totalTrx 			= $pslip["numtrx"];
			$persenLabel = $pslip["BALANCE_TYPE"] == '2' ? ' %' : '';
			// 			print_r($pslip);die;
			//	if(!empty($persenLabel)){
			//		$this->view->totalAmt 			= $pslip["TRA_AMOUNT"];
			//}else
			if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
				//die;
				$this->view->totalAmt			= $pslip['PS_REMAIN'];
			} else {
				//print_r($pslip);die;
				if ($pslip['PS_TYPE'] == '3' && $pslip['acbenef_ccy'] == 'USD' && $pslip['accsrc_ccy'] == 'USD') {
					$this->view->totalAmt 			= $pslip["ccy"] . ' ' . $pslip["EQUIVALEN_IDR"];
					$this->view->ps_ccy 			= $pslip["ccy"];
				} else if ($pslip['PS_TYPE'] == '3') {
					//$this->view->totalAmt 			= 'IDR '.$pslip["amount"].' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
					$this->view->totalAmt 			= 'IDR ' . Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']);
					$this->view->ps_ccy 			= 'IDR';
				} else {
					$this->view->totalAmt 			= $pslip["ccy"] . ' ' . $pslip["amount"];
					$this->view->ps_ccy 			= $pslip["ccy"];
				}
				/*	if($pslip['ccy']!='IDR'){
					$this->view->totalAmt 			= $pslip['ccy'].' '.$pslip["amount"].' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
				}else{
					$this->view->totalAmt 			= $pslip['ccy'].' '.$pslip["amount"];
				}
			*/
			}
			//print_r($pslip);die;
			if ($pslip['PS_TYPE'] == '3' || ($pslip['PS_TYPE'] == '1')) {
				$this->view->totaltext 			= true;
			} else {
				$this->view->totaltext 			= false;
			}
			$this->view->pdf 				= ($pdf) ? true : false;

			if ($pdf) {

				$Payment = new 	Payment($pslip['PS_NUMBER']);
				$listHistory =  $Payment->getHistory();
				$this->view->paymentHistory =  $listHistory;
				$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;

				$selectUser = $this->_db->select()
				->from(array('A' => 'M_USER'), array('A.USER_ID','A.USER_FULLNAME'))
				->where('A.USER_STATUS != 3')
				->where('A.CUST_ID = ?', $this->_custIdLogin);

				$selectUserList = $selectUser->query()->fetchAll();

				foreach ($selectUserList as $key => $value) {
					$newUserList[$value['USER_ID']] = $value['USER_FULLNAME'];
				}

				$this->view->dataUser = $newUserList;
				$this->view->custName = $this->_custNameLogin; 

				$pstatusDisp = array(
					17 => 'Waiting for Review',
					1 => 'Waiting for Approval',
					2 => 'Waiting for Release'
				);

				$this->view->pdfstatusdisp 	= $pstatusDisp[$pslip['PS_STATUS']];

				$setting = new Settings();
				$master_bank_app_name = $setting->getSetting('master_bank_app_name');
				$master_bank_name = $setting->getSetting('master_bank_name');
				$this->view->master_bank_app_name = $master_bank_app_name;
				$this->view->master_bank_name = $master_bank_name;

				$frontendOptions = array(
					'lifetime' => 86400,
					'automatic_serialization' => true
				);
				$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
				$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

				$cacheID = 'USERLIST';

				$userlist = $cache->load($cacheID);
				$this->view->pdfUserlist 	= $userlist;

				$tempdir = APPLICATION_PATH . "/../public/QRImages/";

				$teks_qrcode    = "Membuat QR Code dengan PHP";
				$namafile       = $pslip['PS_NUMBER'] . ".png";
				$quality        = "H";
				$ukuran         = 5;
				$padding        = 1;
				$qrImage        = $tempdir . $namafile;
				QRCode::png($teks_qrcode, $tempdir . $namafile, $quality, $ukuran, $padding);

				$outputHTML = "<tr><td>" . $this->view->render('/viewdetail/indexpdf.phtml') . "</td></tr>";
				// echo $outputHTML;die; 
				$this->_helper->download->newPdf(null, null, null, $master_bank_app_name . ' - ' . $master_bank_name , $outputHTML);
			}

			Application_Helper_General::writeLog('DARC', 'Detail View Payment');
		}
	}

	function moneyAliasFormatter($n)
	{
		// first strip any formatting;
		return str_replace('.00', '', Application_Helper_General::displayMoney($n));
		// $n = (0+str_replace(",", "", $n));

		// // is this a number?
		// if (!is_numeric($n)) return false;

		// // now filter it;
		// if ($n > 1000000000000) return round(($n/1000000000000), 2).' T';
		// elseif ($n > 1000000000) return round(($n/1000000000), 2).' B';
		// elseif ($n > 1000000) return round(($n/1000000), 2).' M';
		// elseif ($n > 1000) return $n;

		// return number_format($n);
	}

	public function findPolicyBoundary($transfertype, $amount)
	{

		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype);

		// if($transfertype != '19')
		$pstype = array(19, 20);
		if (!in_array($transfertype, $pstype)) {
			$selectuser->where("C.BOUNDARY_MIN 	<= ?", $amount);
			$selectuser->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}
		// echo $selectuser;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{

		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype);

		// if($transfertype != '19')
		$pstype = array(19, 20);
		if (!in_array($transfertype, $pstype)) {
			$selectuser->where("C.BOUNDARY_MIN 	<= ?", $amount);
			$selectuser->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}
		// echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}
		$groupuser[] = 27;
		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			$selectGroupName	= $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'));
				if($value == 27){
					$selectGroupName->where("C.GROUP_USER_ID LIKE ?", 'S_' . $this->_custIdLogin .  '%');
				}else{
					$selectGroupName->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
				}
				

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}
	//	echo '<pre>';
	//	var_dump($groupuser);die;
		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "%S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}

		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("USER_ID = ?", (string) $userid);

					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	private function debet($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"], $this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
		foreach ($arrTransferStatus as $key => $val) {
			$caseTraStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseTraStatus .= " END)";

		$this->_tableMst[5]["label"] = $this->language->_('Beneficiary Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_bankname"], $pslip["acbenef_alias"]);

		// Table Detail Header
		$fields = array(
			"ACCTSRC_NAME"		=> $this->language->_('Source Account Name'),
			"ACCTSRC"			=> $this->language->_('Source Account'),
			"ACCSRC_ALIAS" => $this->language->_('Source Account Alias'),
			"ACBENEF_ALIAS"			=> $this->language->_('Beneficiary Alias'),
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			"ACCTSRC_CCY"  	   	=> $this->language->_('CCY'),
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			"TRA_STATUS"  	   	=> $this->language->_('Transaction Status'),
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);

		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
					'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
					'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),
					'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
					'ACCTSRC_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																				 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME)
																			END"),
					//'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
					'TRA_AMOUNT'				=> 'TT.TRA_AMOUNT',
					'TRA_ADDMESSAGE'				=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
					'TRA_REFNO'					=> 'TT.TRA_REFNO',
					'TRA_STATUS'				=> $caseTraStatus
				)
			)
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);

		$selectBank	= $this->_db->select()
			->from(
				array('B' => 'M_BANK_TABLE'),
				array(
					'BANK_CODE'			=> 'B.BANK_CODE',
					'BANK_NAME'			=> 'B.BANK_NAME'
				)
			);

		$bankList = $this->_db->fetchAll($selectBank);

		foreach ($bankList as $key => $value) {
			$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		$tableDtl = array();
		if (count($pslipTrx) <= 50) {
			foreach ($pslipTrx as $p => $pTrx) {

				// create table detail data
				$psCategory = $pslip['PS_CATEGORY'];
				foreach ($fields as $key => $field) {
					$value = $pTrx[$key];

					if ($key == "TRA_AMOUNT") {
						$value = Application_Helper_General::displayMoney($value);
					}


					$value = ($value == "") ? "-" : $value;

					$tableDtl[$p][$key] = $value;
				}

				if (!empty($pslip['BANK_CODE'])) {
					$bankcode = $pslip['BANK_CODE'];
				} else {
					$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
				}

				if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
					$bankname = '-';
				} else if (empty($bankcode)) {
					$bankname = $this->_bankName;
				} else {
					$bankname = $bankNameArr[$bankcode];
				}

				$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
				$tableDtl[$p]['BENEFICIARY_ID'] = $pTrx['ACBENEF_ID'];
			}
			
			$selectclient = $this->_db->select()
                    ->from(array('B' => 'T_DIRECTDEBIT'),array('CLIENT_REFF', 'CLIENT_NAME'))
                    ->where("B.DEBIT_ACCT = ?", $tableDtl[0]['ACBENEF'])
                    ->query()->fetchAll();

			// echo "<pre>";
			// var_dump($tableDtl);
			// var_dump($selectclient);
			
			$this->view->clientReff			= $selectclient[0]['CLIENT_REFF'];
			$this->view->clientName			= $selectclient[0]['CLIENT_NAME'];
			$this->view->fields			 = $fields;
			$this->view->tableDtl 		 = $tableDtl;

		}

		$this->view->TITLE_MST		 = $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 = $this->language->_('Transfer From');


		if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {
			$this->view->TITLE_MST		 = $this->language->_('Sweep To');
			$this->view->TITLE_DTL		 = $this->language->_('Sweep From');
		}
	}

	private function credit($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"], $this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
		foreach ($arrTransferStatus as $key => $val) {
			$caseTraStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseTraStatus .= " END)";

		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		// Table Detail Header
		$fields = array(
			"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
			"ACBENEF"			=> $this->language->_('Beneficiary Account'),

			//		"BENEFICIARY_CATEGORY"			=> $this->language->_('Beneficiary Category'),
			//		"BENEFICIARY_ID_TYPE"			=> $this->language->_('Beneficiary Identification Type'),
			//		"BENEFICIARY_ID_NUMBER"			=> $this->language->_('Beneficiary Identification Number'),
			//		"BENEFICIARY_CITY_CODE"			=> $this->language->_('Beneficiary City'),

			"BENEFICIARY_ID_NUMBER"			=> $this->language->_('Beneficiary NRC'),
			"BENEFICIARY_MOBILE_PHONE_NUMBER"			=> $this->language->_('Beneficiary Phone'),
			"ACBENEF_ALIAS"			=> $this->language->_('Beneficiary Alias'),
			"ACCSRC_ALIAS" => $this->language->_('Source Account Alias'),
			/* "TRA_MESSAGE" 		=> $this->language->_('Message'),
						"TRA_REFNO"  	   	=> $this->language->_('Additional Message'),
						"TRANSFER_TYPE"	   	=> $this->language->_('Transfer Type'),
						"PS_CCY"  	   	=> $this->language->_('CCY'),
						"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
						//"TRANSFER_FEE"  	=> 'Transfer Charge',
						"BANK_NAME"  	   	=> $this->language->_('Bank'),
						"TRA_STATUS" 		=> $this->language->_('Transaction Status'),
						*/
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			"TRANSFER_TYPE"	   	=> $this->language->_('Transfer Type'),
			"PS_CCY"  	   	=> $this->language->_('CCY'),
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),

			"RATE"  	   	=> $this->language->_('Rate'),
			"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			"TOTAL"  	=> $this->language->_('Total'),
			"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'),
			"BENEFICIARY_BANK_ADDRESS1"  	   	=> $this->language->_('Beneficiary Bank Address'),
			"BENEFICIARY_BANK_CITY"  	   	=> $this->language->_('Beneficiary Bank City'),
			"BENEFICIARY_BANK_COUNTRY"  	   	=> $this->language->_('Country'),
			"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),
			"TRA_STATUS" 		=> $this->language->_('Transaction Status'),
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);
		//print_r($pslip);die;
		if ($pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME)"),
						'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
						'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'C.PS_CCY', 'C.EQUIVALENT_AMOUNT_IDR', 'C.PS_TYPE',
						'BENEFICIARY_CATEGORY'				=> 'TT.BENEFICIARY_CATEGORY',
						'BENEFICIARY_ID_TYPE'				=> 'TT.BENEFICIARY_ID_TYPE',
						'BENEFICIARY_ID_NUMBER'				=> 'TT.BENEFICIARY_ID_NUMBER',
						'BENEFICIARY_CITY_CODE'				=> 'TT.BENEFICIARY_CITY_CODE',
						'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',

						'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
						'TRA_REFNO'				=> 'TT.TRA_REFNO',

						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House(Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House(Buy)'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT', 'TT.SOURCE_ACCOUNT_CCY',
						'C.CUST_ID',
						#'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $this->_bankName . "'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'TRA_STATUS' 			=> $caseTraStatus
					)
				)
				->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
				->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
				//->joinLeft	(   array('D' => 'T_PERIODIC_DETAIL'), 'C.PS_PERIODIC = D.PS_PERIODIC', array() )
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		} else {

			$app = Zend_Registry::get('config');
			$appBankname = $app['app']['bankname'];

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME)"),
						'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
						'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),

						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'C.PS_TXCOUNT',
						'C.PS_CCY', 'C.PS_TYPE',
						'C.CUST_ID',
						'TT.EQUIVALENT_AMOUNT_IDR',
						'BENEFICIARY_CATEGORY'				=> 'TT.BENEFICIARY_CATEGORY',
						'BENEFICIARY_ID_TYPE'				=> 'TT.BENEFICIARY_ID_TYPE',
						'BENEFICIARY_ID_NUMBER'				=> 'TT.BENEFICIARY_ID_NUMBER',
						'BENEFICIARY_CITY_CODE'				=> 'TT.BENEFICIARY_CITY_CODE',
						'TT.BENEFICIARY_MOBILE_PHONE_NUMBER',
						'TT.RATE', 'TT.SOURCE_ACCOUNT_CCY',
						'TT.PROVISION_FEE',
						'TT.NOSTRO_NAME',
						'TT.BENEFICIARY_BANK_ADDRESS1',
						'TT.BENEFICIARY_BANK_CITY',
						'TT.FULL_AMOUNT_FEE',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

						'TRA_REFNO'				=> 'TT.TRA_REFNO',
						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House(Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House(Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
						'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
						#'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'TRA_STATUS' 			=> $caseTraStatus
					)
				)
				->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
				->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		}
		//echo $select;die;
		$pslipTrx = $this->_db->fetchAll($select);
		// print_r($pslipTrx);die;
		$tableDtl = array();
		// if(count($pslipTrx) <= 50){
		$selectBank	= $this->_db->select()
			->from(
				array('B' => 'M_BANK_TABLE'),
				array(
					'BANK_CODE'			=> 'B.BANK_CODE',
					'BANK_NAME'			=> 'B.BANK_NAME'
				)
			);

		$bankList = $this->_db->fetchAll($selectBank);

		foreach ($bankList as $key => $value) {
			$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		foreach ($pslipTrx as $p => $pTrx) {

			if ($pTrx['PS_TYPE'] == 19 && $pTrx['PS_TXCOUNT'] > 1 || $pTrx['PS_TYPE'] == 11 || $pslip['PS_TYPE'] == 28) {
				$this->_tableMstleft[5]["value"] = '';
			}else{
				$this->_tableMstleft[5]["value"] = $pTrx['TRA_MESSAGE'];
			}
			
			$this->view->pdftramessage = $pTrx['TRA_MESSAGE'];

			if ($pslip['PS_TYPE'] == '17' && $pslip['PS_BILLER_ID'] == '1156') {
				$this->_tableMstleft[5]["value"] = '';
			}
			// $this->_tableMstleft[5]["value"] = $pTrx['TRA_ADDMESSAGE']; 

			// if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {
			// 	$this->_tableMstright[6]["value"] = $pTrx['TRA_STATUS'];
			// }
			// else{
			// 	$this->_tableMstright[3]["value"] = $pTrx['TRA_STATUS'];
			// }

			// $this->_tableMstleft[4]["value"] = $pTrx['TRA_ADDMESSAGE'];
			$psCategory = $pslip['PS_CATEGORY'];
			//print_r($fields);die;
			// create table detail data

			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];
				// print_r($key);die;
				if ($key == 'ACBENEF' && (trim($pTrx['TRANSFER_TYPE']) == 'FA' || trim($pTrx['TRANSFER_TYPE']) == 'No FA')) {
					$value = '-';
					// die;
				}
				if ($key == "BENEFICIARY_CATEGORY") {
					$LLD_CATEGORY = $value;
					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}
					$value = $LLD_CATEGORY_POST;
				}
				if ($key == "BENEFICIARY_CITY_CODE") {
					$CITY_CODE = $value;
					$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
					$select = $this->_db->select()
						->from(array('A' => 'M_CITY'), array('*'));
					$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
					$arr = $this->_db->fetchall($select);
					$value = $arr[0]['CITY_NAME'];
				}

				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['PS_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						//print_r($pTrx);die;
						if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD' && $pslip['PS_TYPE'] == '1') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							//$value = $pTrx['PS_CCY'].' '.Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']).' (IDR '.Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']).')';
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']);
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}

				if ($key == 'RATE') {
					$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
				}
				//print_r($pTrx);die;
				if ($key == "FULL_AMOUNT_FEE") {
					if ($pTrx['TRANSFER_TYPE'] == 'FA') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
					} elseif ($pTrx['TRANSFER_TYPE'] == 'No FA') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '4')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
					}



					// echo $selecttrfFA;
					// print_r($pTrx);die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);

					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);

					//print_r($value);die;
				}


				if ($key == 'PROVISION_FEE') {
					//print_r($pTrx);die;
					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrfpro;die;
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != 'N/A') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}

				if ($key == "TRANSFER_FEE") {
					//print_r($value);die;
					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;

					$trffee = $this->_db->fetchRow($selecttrffee);
					if ($pTrx['TRANSFER_FEE'] != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRANSFER_FEE']);
					} else {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' 0.00';
					}
				}
 
				if ($key == "BENEF_ACCT_BANK_CODE") {
					if (!empty($pslip['BANK_CODE'])) {
						$bankcode = $pslip['BANK_CODE'];
					} else {
						$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
					}

					if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
						$bankname = '-';
					}else if (!empty($pslip['BENEFICIARY_BANK_NAME'])) {
						$bankname = $pslip['BENEFICIARY_BANK_NAME'];
					} else if (empty($bankcode)) {
						$bankname = $this->_bankName;
					} else {
						$bankname = $bankNameArr[$bankcode];
					}

					$value = $bankname;
				}

				//print_r($value);die;
				$value = ($value == "") ? "-" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
				$tableDtl[$p]['BENEFICIARY_ID'] = $pTrx['ACBENEF_ID'];
			}
		}

		if($pTrx['PS_TYPE'] == 11){
			unset($this->_tableMstleft[5]);
		}
		// }
		echo "<pre>";
		// print_r($fields);
		// print_r($tableDtl);die;
		$selectclient = $this->_db->select()
                    ->from(array('B' => 'T_DIRECTDEBIT'),array('CLIENT_REFF', 'CLIENT_NAME'))
                    ->where("B.DEBIT_ACCT = ?", $tableDtl[0]['ACBENEF'])
                    ->query()->fetchAll();

		// var_dump($selectclient);

			
		$this->view->clientReff			= $selectclient[0]['CLIENT_REFF'];
		$this->view->clientName			= $selectclient[0]['CLIENT_NAME'];
		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;

		if (count($tableDtl) >= 50) {
			$this->view->countTable = true;
		} else {
			$this->view->countTable = false;
		}
		$this->view->TITLE_MST		 	= $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer From');

		if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {
			$this->view->TITLE_MST		 = $this->language->_('Sweep To');
			$this->view->TITLE_DTL		 = $this->language->_('Sweep From');
		}
	}

	private function etax($pslip)
	{
		$PS_NUMBER = $this->_paymentRef;
		$getPaymentDetail 	= new display_Model_Paymentreport();
		$detail = $getPaymentDetail->getPaymentDetail($pslip["TRANSACTION_ID"]);
		$dataEtax = json_decode($detail[0]['LOG']);
		 //echo '<pre>';
		// var_dump($pslip);die; 
		$detailPslip = $getPaymentDetail->getPslipDetail($pslip["PS_NUMBER"]);
		foreach ($detailPslip as $key => $value) {
			if ($value['PS_FIELDNAME'] == 'NTB/NTP'){
				$ntb = $value['PS_FIELDVALUE'];
			}
			if ($value['PS_FIELDNAME'] == 'NTPN'){
				$ntpn = $value['PS_FIELDVALUE'];
			}
			if ($value['PS_FIELDNAME'] == 'STAN'){
				$stan = $value['PS_FIELDVALUE'];
			}
		}

		//$this->_tableMstleft[1]["value"] = $dataEtax->paymentSubject;
		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$ACCTSRC_arr = array();
		if ($pslip['PS_BILLER_ID'] == '1158') {
			//table detail
			$compulsory = array('0' => 'No', '1' => 'Yes');
			$taxType = array('0' => 'NPWP', '1' => 'Non NPWP');
			$identity = array(
				'1' => 'KTP',
				'2' => 'NPWP',
				'3' => 'SIM',
				'4' => 'PASPOR',
				'5' => 'KITAS'
			);
			// roki
			$month = array(
				$this->language->_('January'),
				$this->language->_('February'),
				$this->language->_('March'),
				$this->language->_('April'),
				$this->language->_('May'),
				$this->language->_('June'),
				$this->language->_('July'),
				$this->language->_('August'),
				$this->language->_('September'),
				$this->language->_('October'),
				$this->language->_('November'),
				$this->language->_('December')
			);
			$map_code = array();
			$depositType = array();
			$select = $this->_db->select()
				->from(array('M_MAP_CODE'), array('MAP_CODE', 'MAP_NAME'))
				->query()->fetchAll();

			foreach ($select as $key) {
				$map_code[$key['MAP_CODE']] = $key['MAP_NAME'] . " (" . $key['MAP_CODE'] . ")";
			}
			if (isset($dataEtax->akuncode)) {
				$select = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_MAP_CODE'), array('DEPOSIT_CODE', 'DEPOSIT_NAME'))
						->where('MAP_CODE = ?', $dataEtax->akuncode)
				);

				foreach ($select as $key) {
					$depositType[$key['DEPOSIT_CODE']] = $key['DEPOSIT_NAME'] . " (" . $key['DEPOSIT_CODE'] . ")";
				}
			}
		
			$tableMst[0]["label"] = $this->language->_('Payment Ref#');
			$tableMst[0]["value"] = $pslip['PS_NUMBER'];

			$tableMst[1]["label"] = $this->language->_('Created Date');
			$tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

			$tableMst[2]["label"] = $this->language->_('Updated Date');
			$tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

			$tableMst[3]["label"] = $this->language->_('Payment Subject');
			$tableMst[3]["value"] = $dataEtax->paymentSubject;

			$tableMst[4]["label"] = $this->language->_('Source Account');
			$tableMst[4]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], "");

			$tableMst[5]["label"] = $this->language->_('Payment Status');
			$tableMst[5]["value"] = $pslip["payStatus"];

			$tableMst[6]["label"] = $this->language->_('Billing Code');
			$tableMst[6]["value"] = '2020032300014203';

			$tableMst[7]["label"] = $this->language->_('Amount');
			$tableMst[7]["value"] = 'IDR ' . $dataEtax->amount;

			$tableMst[8]["label"] = $this->language->_('Payer NPWP');
			$tableMst[8]["value"] = $dataEtax->payerNpwp;

			$tableMst[9]["label"] = $this->language->_('Payer Name');
			$tableMst[9]["value"] = $dataEtax->payername;

			$tableMst[10]["label"] = $this->language->_('Compulsory');
			$tableMst[10]["value"] = $compulsory[$dataEtax->chargetype] . ' ' . ($dataEtax->chargetype == '1' ? ' - (' . $dataEtax->chargeid . ')' : '');

			$tableMst[11]["label"] = $this->language->_('Asessable NPWP');
			$tableMst[11]["value"] = $dataEtax->asessableNpwp;

			$tableMst[12]["label"] = $this->language->_('Asessable Name');
			$tableMst[12]["value"] = $dataEtax->asessablename;

			$tableMst[13]["label"] = $this->language->_('Asessable Address');
			$tableMst[13]["value"] = $dataEtax->asessableaddress;

			$tableMst[14]["label"] = $this->language->_('Asessable City');
			$tableMst[14]["value"] = $dataEtax->asessablecity;

			$tableMst[15]["label"] = $this->language->_('Asessable Identity');
			$tableMst[15]["value"] = $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;

			$tableMst[16]["label"] = $this->language->_('Map / Akun Code');
			$tableMst[16]["value"] = $map_code[$dataEtax->akuncode];

			$tableMst[17]["label"] = $this->language->_('Deposit Type');
			$tableMst[17]["value"] = $depositType[$dataEtax->deposittype];

			$tableMst[18]["label"] = $this->language->_('Tax Object Number (NOP)');
			$tableMst[18]["value"] = $dataEtax->taxobjectnumber;

			$tableMst[19]["label"] = $this->language->_('SK Number');
			$tableMst[19]["value"] = $dataEtax->skNumber;

			$tableMst[20]["label"] = $this->language->_('Remark');
			$tableMst[20]["value"] = $dataEtax->remark;

			$tableMst[21]["label"] = $this->language->_('Tax Period  Payment');
			$tableMst[21]["value"] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2] . ' ' . $dataEtax->periodic;

			$tableMst[22]["label"] = $this->language->_('NTB / NTP');
			$tableMst[22]["value"] = $ntb;

			$tableMst[23]["label"] = $this->language->_('NTPN');
			$tableMst[23]["value"] = $ntpn;

			$tableMst[24]["label"] = $this->language->_('STAN');
			$tableMst[24]["value"] = $stan;

			$tableMst[25]["label"] = $this->language->_('Transaction Status');
			$tableMst[25]["value"] = $detail[0]["TRANSFER_STATUS"];
		} elseif ($pslip['PS_BILLER_ID'] == '1156') {
			//adons Type of Tax First row
			$typeOfTax = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('M_SERVICE_PROVIDER'), array('PROVIDER_NAME'))
					->where('PROVIDER_ID = ?', $pslip['PS_BILLER_ID'])
			);
			$tableMst[0]["label"] = $this->language->_('Payment Ref#');
			$tableMst[0]["value"] = $pslip['PS_NUMBER'];

			$tableMst[1]["label"] = $this->language->_('Created Date');
			$tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

			$tableMst[2]["label"] = $this->language->_('Updated Date');
			$tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

			$tableMst[3]["label"] = $this->language->_('Source Account');
			$tableMst[3]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], "");

			$tableMst[4]["label"] = $this->language->_('Payment Status');
			$tableMst[4]["value"] = $pslip["payStatus"];

			$tableMst[5]["label"] = $this->language->_('Type of Tax');
			$tableMst[5]["value"] = $typeOfTax['PROVIDER_NAME'];

			$tableMst[6]["label"] = $this->language->_('Billing Code');
			$tableMst[6]["value"] = $dataEtax->orderId;

			$tableMst[7]["label"] = $this->language->_('Amount');
			$tableMst[7]["value"] = 'IDR ' . Application_Helper_General::displayMoney($dataEtax->amount);

			$billingPrefix = substr($dataEtax->orderId, 0, 1);

			if ($billingPrefix == '0' || $billingPrefix == '1' || $billingPrefix == '2' || $billingPrefix == '3') {
				$tableMst[8]["label"] = $this->language->_('NPWP');
				$tableMst[8]["value"] = $dataEtax->dataUi->npwp;

				$tableMst[9]["label"] = $this->language->_('Customer Name');
				$tableMst[9]["value"] = $dataEtax->dataUi->customer_name;

				$tableMst[10]["label"] = $this->language->_('Customer Address');
				$tableMst[10]["value"] = $dataEtax->dataUi->customer_address;

				$tableMst[11]["label"] = $this->language->_('Map / Akun Code');
				$tableMst[11]["value"] = $dataEtax->dataUi->account_map;

				$tableMst[12]["label"] = $this->language->_('Deposit Type');
				$tableMst[12]["value"] = $dataEtax->dataUi->type;

				$tableMst[13]["label"] = $this->language->_('Tax Object Number (NOP)');
				$tableMst[13]["value"] = $dataEtax->dataUi->NOP;

				$tableMst[14]["label"] = $this->language->_('SK Number');
				$tableMst[14]["value"] = $dataEtax->dataUi->sk_number;

				$tableMst[15]["label"] = $this->language->_('Tax Period Payment');
				$tableMst[15]["value"] = $dataEtax->dataUi->period;

				$tableMst[16]["label"] = $this->language->_('NTB / NTP');
				$tableMst[16]["value"] = $ntb;

				$tableMst[17]["label"] = $this->language->_('NTPN');
				$tableMst[17]["value"] = $ntpn;

				$tableMst[18]["label"] = $this->language->_('STAN');
				$tableMst[18]["value"] = $stan;

				$tableMst[19]["label"] = $this->language->_('Transaction Status');
				$tableMst[19]["value"] = $detail[0]["TRANSFER_STATUS"];
			} elseif ($billingPrefix == '4' || $billingPrefix == '5' || $billingPrefix == '6') {

				$tableMst[8]["label"] = $this->language->_('Customer Name');
				$tableMst[8]["value"] = $dataEtax->dataUi->customer_name;

				$tableMst[9]["label"] = $this->language->_('ID Type Customer');
				$tableMst[9]["value"] = $dataEtax->dataUi->customer_id;

				$tableMst[10]["label"] = $this->language->_('Document Type');
				$tableMst[10]["value"] = $dataEtax->dataUi->document_type;

				$tableMst[11]["label"] = $this->language->_('Document Number');
				$tableMst[11]["value"] = $dataEtax->dataUi->document_number;

				$tableMst[12]["label"] = $this->language->_('Document Date');
				$tableMst[12]["value"] = $dataEtax->dataUi->document_date;

				$tableMst[13]["label"] = $this->language->_('KPBC Code');
				$tableMst[13]["value"] = $dataEtax->dataUi->kppbc_code;

				$tableMst[14]["label"] = $this->language->_('NTB / NTP');
				$tableMst[14]["value"] = $ntb;

				$tableMst[15]["label"] = $this->language->_('NTPN');
				$tableMst[15]["value"] = $ntpn;

				$tableMst[16]["label"] = $this->language->_('STAN');
				$tableMst[16]["value"] = $stan;

				$tableMst[17]["label"] = $this->language->_('Transaction Status');
				$tableMst[17]["value"] = $detail[0]["TRANSFER_STATUS"];
			} elseif ($billingPrefix == '7' || $billingPrefix == '8' || $billingPrefix == '9') {

				$tableMst[8]["label"] = $this->language->_('Customer Name');
				$tableMst[8]["value"] = $dataEtax->dataUi->customer_name;

				$tableMst[9]["label"] = $this->language->_('K/L');
				$tableMst[9]["value"] = $dataEtax->dataUi->k_l;

				$tableMst[10]["label"] = $this->language->_('Echelon Unit 1');
				$tableMst[10]["value"] = $dataEtax->dataUi->eselon_unit;

				$tableMst[11]["label"] = $this->language->_('Code Unit');
				$tableMst[11]["value"] = $dataEtax->dataUi->work_unit;

				$tableMst[12]["label"] = $this->language->_('NTB / NTP');
				$tableMst[12]["value"] = $ntb;

				$tableMst[13]["label"] = $this->language->_('NTPN');
				$tableMst[13]["value"] = $ntpn;

				$tableMst[14]["label"] = $this->language->_('STAN');
				$tableMst[14]["value"] = $stan;

				$tableMst[15]["label"] = $this->language->_('Transaction Status');
				$tableMst[15]["value"] = $detail[0]["TRANSFER_STATUS"];
			}
		}
		//var_dump($tableMst);die;
		return $tableMst;
		/////
	}

	private function sp2d($pslip)
	{
		$getPaymentDetail 	= new display_Model_Paymentreport();
		$detail = $getPaymentDetail->getPaymentDetail($pslip["PS_NUMBER"]);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$tableMst1[0]["label"] = $this->language->_('Payment Ref#');
		$tableMst1[0]["value"] = $pslip['PS_NUMBER'];

		$tableMst1[1]["label"] = $this->language->_('Created Date');
		$tableMst1[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

		$tableMst1[2]["label"] = $this->language->_('Updated Date');
		$tableMst1[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

		$tableMst1[3]["label"] = $this->language->_('Payment Date');
		$tableMst1[3]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);

		$tableMst1[4]["label"] = $this->language->_('Payment Subject');
		$tableMst1[4]["value"] = $pslip['PS_SUBJECT'];

		$tableMst1[5]["label"] = $this->language->_('Source Account');
		$tableMst1[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], "");

		$tableMst1[6]["label"] = $this->language->_('Payment Type');
		$tableMst1[6]["value"] = $pslip['payType'];

		$tableMst1[7]["label"] = $this->language->_('Payment Status');
		$tableMst1[7]["value"] = $pslip["payStatus"];

		$tableMst[0]["label"] = $this->language->_('No SP2D');
		$tableMst[0]["value"] = $pslip['SP2D_NO'];

		$tableMst[1]["label"] = $this->language->_('SP2D Date');
		$tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['SP2D_DATE'], $this->_dateViewFormat);

		$tableMst[2]["label"] = $this->language->_('No SPM');
		$tableMst[2]["value"] = $pslip['SPM_NO'];

		$tableMst[3]["label"] = $this->language->_('SPM Date');
		$tableMst[3]["value"] = Application_Helper_General::convertDate($pslip['SPM_DATE'], $this->_dateViewFormat);

		$tableMst[4]["label"] = $this->language->_('SKPD Name');
		$tableMst[4]["value"] = $pslip['SKPD_NAME'];

		$tableMst[5]["label"] = $this->language->_('SPP Type');
		$tableMst[5]["value"] = $pslip['SPP_TYPE'];

		$tableMst[6]["label"] = $this->language->_('Beneficiary Account Name');
		$tableMst[6]["value"] = $pslip['BENEFICIARY_ACCOUNT_NAME'];

		$tableMst[7]["label"] = $this->language->_('Beneficiary Account Number');
		$tableMst[7]["value"] = $pslip['BENEFICIARY_ACCOUNT'];

		$tableMst[8]["label"] = $this->language->_('Beneficiary Identity');
		$tableMst[8]["value"] = $pslip['BENEFICIARY_ID_TYPE'] . ' ' . $pslip['BENEFICIARY_ID_NUMBER'];

		$tableMst[9]["label"] = $this->language->_('Transaction Purpose');
		$tableMst[9]["value"] = $pslip['LLD_TRANSACTION_PURPOSE'];

		$tableMst[10]["label"] = $this->language->_('Description');
		$tableMst[10]["value"] = nl2br($pslip['LLD_DESC']);

		$tableMst[11]["label"] = $this->language->_('Actual Amount');
		$tableMst[11]["value"] = 'IDR ' . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']);

		$tableMst[12]["label"] = $this->language->_('Deduction Amount');
		$tableMst[12]["value"] = 'IDR ' . Application_Helper_General::displayMoney($pslip['TOTAL_CHARGES']);

		$tableMst[13]["label"] = $this->language->_('Total Amount');
		$tableMst[13]["value"] = 'IDR ' . Application_Helper_General::displayMoney($pslip['PS_TOTAL_AMOUNT']);

		$tableMst[14]["label"] = $this->language->_('Transaction Status');
		$tableMst[14]["value"] = $detail[0]["TRANSFER_STATUS"];

		$tableMst = array_merge($tableMst1, $tableMst);
		return $tableMst;
		/////
	}

	public function downloadtrxAction()
	{
		$PS_NUMBER 			= trim(strip_tags($this->_getParam('payReff')));

		$paramPayment = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);
		// print_r($pslip["PS_TYPE"]);die;
		if (!empty($pslip)) {
			// separate credit and debet view
			if (
				// $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || removed globar var ?
				$pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]
			)
				$this->downloadTrxDebet($PS_NUMBER);
			else
				$this->downloadTrxCredit($PS_NUMBER, $pslip['payType'], $pslip['PS_TYPE']);
		} else {
			$data = array();
			$data[0][0] = "Invalid Payment Number";
			$this->_helper->download->csv(array(), $data, null, $PS_NUMBER);
		}
	}

	protected function downloadTrxCredit($PS_NUMBER, $PS_TYPE, $paymenttype)
	{
		//		"BENEFICIARY ACC","BENEFICIARY NAME","CCY","AMOUNT","MESSAGE","ADDITIONAL MESSAGE","EMAIL ADDRESS","TRANSFER TYPE","BANK CODE"," BANK NAME ","BANK CITY","CITIZENSHIP"
		//		"2003019114","Account Name","USD","3,000.50","MSG USD WITHIN","ADDITIONAL MSG","admin@yahoo.com","PB","","",""
		//		"2003019126","Baharudin","IDR","3,500.50","MSG IDR SKN","PAYMENT DESC 2004","admin2@yahoo.com","SKN","00023234","BNI, Ambon","Thamrin","R"
		//		"2003019120","Antony","IDR","1000","MSG IDR RTGS","PAYMENT DESC 2005","admin2@yahoo.com","RTGS","00023235","BCA, SBY","Address 12","NR"

		$caseTransferType = Application_Helper_General::caseArray($this->_transfertype);

		 //print_r($PS_TYPE);die;

		if ($PS_TYPE == 'Payroll') {
			$header = array($this->language->_('BENEFICIARY ACC'), $this->language->_('CCY'), $this->language->_('AMOUNT'), $this->language->_('MESSAGE'));

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE'
					)
				)
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);
			$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
		} else if ($PS_TYPE == 'Multi Credit - Import') {
			$header = array($this->language->_('DESTINATION ACC'), $this->language->_('CCY'), $this->language->_('AMOUNT'), $this->language->_('MESSAGE'), $this->language->_('TRANSFER TYPE'), $this->language->_('CLEARING CODE'));

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_TYPE'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '5' THEN 'ONLINE'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'SELL'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'BUY'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 ELSE ''
																			END"),
						'CLR_CODE'				=> 'TT.CLR_CODE',

					)
				)
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);
			$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
		}else if(strtoupper($PS_TYPE) == strtoupper('Bulk Biller')){
			$header = array($this->language->_('Biller'), $this->language->_('Order Id'), $this->language->_('Amount'));

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'BILLER_ID'				=> 'TT.BILLER_ID',
						'BILLER_ORDER_ID'			=> 'TT.BILLER_ORDER_ID',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT'
						//'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE'
					)
				)
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);
			$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
		} else {

			$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
			$caseTransStatus = "(CASE TT.TRA_STATUS ";
			foreach($transstatusarr as $key=>$val)
			{
				$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
			}
			$caseTransStatus .= " ELSE 'N/A' END)";

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_NAME'			=> 'TT.BENEFICIARY_ACCOUNT_NAME',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						//'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

						'TRA_REFNO'				=> 'TT.TRA_REFNO',
						'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
						'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
						'BANK_CODE'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 WHEN TT.TRANSFER_TYPE = '1' THEN TT.CLR_CODE
																				 WHEN TT.TRANSFER_TYPE = '2' THEN TT.CLR_CODE
																				 ELSE TT.SWIFT_CODE
																			END"),
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'BANK_CITY'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 ELSE TT.BENEFICIARY_BANK_CITY
																			END"),
						'CITIZENSHIP'			=> 'TT.BENEFICIARY_CITIZENSHIP',
						'RESIDENT_STATUS'			=> 'TT.BENEFICIARY_RESIDENT',
						'PS_CREATED'			=> 'C.PS_CREATED',
						'REFF_ID'				=> 'C.REFF_ID',
						'PS_TXCOUNT'			=> 'C.PS_TXCOUNT',
						'PS_CCY'				=> 'C.PS_CCY',
						'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
						'TRA_STATUS'			=> $caseTransStatus,
					)
				)
				->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array('*'))
				->joinLeft(	array(	'DD' => 'T_DIRECTDEBIT' ),'TT.BENEFICIARY_ACCOUNT = DD.DEBIT_ACCT',array('*'))
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);

			if ($paymenttype == '19') {

				$PS_CREATED = explode('-',$data[0]['PS_CREATED']);
				$PSCREATED  = explode(' ',$PS_CREATED[2]);
				
		      	$headerData = array($this->language->_('SWEEP FROM ACCT'), $this->language->_('REMAIN BALANCE'), $this->language->_('MESSAGE'), $this->language->_('CUST REF'));
		      	
		      	foreach ($data as $p => $pTrx)
				{
					$paramTrx = array(  
					          "0" => $pTrx['SOURCE_ACCOUNT'],
					          "1" => $pTrx['TRA_REMAIN'],
					          "2" => $pTrx['TRA_MESSAGE'],
					          "3" => $pTrx['REFF_ID'],
					        );

					$newData[] = $paramTrx;
				}
				
			}else{
				$tAmount = str_replace('.00', '', $data[0]['PS_TOTAL_AMOUNT']);
				$headerData = array($this->language->_('0'), $this->language->_('AUTODEBIT1'), Application_Helper_General::convertDate($data[0]['PS_CREATED'],$this->viewDateFormat,$this->defaultDateFormat), $data[0]['PS_TXCOUNT'], $tAmount, $data[0]['ACBENEF_CCY']);
				$top = array($this->language->_('0'), $this->language->_('CLIENT REFF'), $this->language->_('DEBITED ACCOUNT'), $this->language->_('DEBITED AMOUNT'), $this->language->_('CLIENT EMAIL'), $this->language->_('MESSAGE (30)'));
				$newData2 = array();
				foreach($data as $key => $val){
					$list = array(
						'a' => '1',
						'b' => $val['CLIENT_REFF'],
						'c' => $val['ACBENEF'],
						'd' => $val['TRA_AMOUNT'],
						'e' => $val['ACBENEF_EMAIL'],
						'f' => $val['TRA_MESSAGE'],
					);

					array_push($newData2, $list);
				}
				$newData = array_merge_recursive(array($top), $newData2);
				// $newData = $data;
				// echo "<pre>";
				// var_dump($newData);die;

			}
			

			// $this->_helper->download->csv($headerData, $newData, null, $PS_NUMBER);
			$this->_helper->download->txtBatch($headerData, $newData, null, $this->language->_('Bulk Auto Debit'));
		}
		//$header = array("BENEFICIARY ACC","BENEFICIARY NAME","CCY","AMOUNT","MESSAGE","ADDITIONAL MESSAGE","EMAIL ADDRESS","TRANSFER TYPE","BANK CODE"," BANK NAME" ,"BANK CITY","CITIZENSHIP","RESIDENT STATUS");

	}

	protected function downloadTrxDebet($PS_NUMBER)
	{
		//		ACCT SOURCE,AMOUNT,MESSAGE,ADDITIONAL MESSAGE
		//		"0000000000000000","350,000.90","MESSAGE","ADDITIONAL MESSAGE"

		$header = array("ACCT SOURCE", "AMOUNT", "MESSAGE");
		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					//	'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

					//	'TRA_REFNO'				=> 'TT.TRA_REFNO',
				)
			)
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
	}

	public function templateaddAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$cross = $this->_getParam('cross');
		$transpose = $this->_getParam('transpose');
		$exrate = $this->_getParam('exrate');
		$refnum = $this->_getParam('refnum');
		$custnum = $this->_getParam('custnum');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "1",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_CROSS' 			=> $cross,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_EXCHANGE' 		=> $exrate,
			'TEMP_REFNUM' 			=> $refnum,
			'TEMP_CUSTREF' 			=> $custnum,
			'TEMP_LOCKED'			=> $locked,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function templateadddomesticAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$tratype = $this->_getParam('tratype');
		$bene = $this->_getParam('bene');
		$beneaddress = $this->_getParam('beneaddress');
		$citizenship = $this->_getParam('citizenship');
		$nationality = $this->_getParam('nationality');
		$benecategory = $this->_getParam('benecategory');
		$beneidtype = $this->_getParam('beneidtype');
		$beneidnum = $this->_getParam('beneidnum');
		$bank = $this->_getParam('bank');
		$city = $this->_getParam('city');
		$email_domestic = $this->_getParam('email_domestic');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');


		try {
			$this->_db->beginTransaction();
			$insertArr = array(
				'T_NAME' 			=> $template,
				'T_CREATEDBY' 		=> $this->_userIdLogin,
				'T_CREATED' 		=> new Zend_Db_Expr("now()"),
				'T_GROUP'			=> $this->_custIdLogin,
				'T_TARGET'			=> $target,
				'T_TYPE'			=> '2',
			);

			$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);



			$lastId = $this->_db->lastInsertId();

			if (!empty($bene)) {
				$select = $this->_db->select()
					->from(array('A' => 'M_BENEFICIARY'), array('*'));
				$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
				$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
				$benedata = $this->_db->fetchall($select);
				$benename = $benedata['0']['BENEFICIARY_NAME'];
			} else {
				$benedata = array();
				$benename = '';
			}

			// print_r($insertArr);die;

			$insertData = array(
				'TEMP_ID' 				=> $lastId,
				'TEMP_SUBJECT' 			=> $subject,
				'TEMP_SOURCE' 			=> $source,
				'TEMP_TRATYPE' 			=> $tratype,
				'TEMP_BENE' 			=> $bene,
				'TEMP_BENE_NAME' 		=> $benename,
				'TEMP_BENEADDRESS' 		=> $beneaddress,
				'TEMP_CITIZENSHIP' 		=> $citizenship,
				'TEMP_NATIONALITY' 		=> $nationality,
				'TEMP_BENCATEGORY' 		=> $benecategory,
				'TEMP_BENEIDTYPE' 		=> $beneidtype,
				'TEMP_BENEIDNUM' 		=> $beneidnum,
				'TEMP_BANK' 			=> $bank,
				'TEMP_CITY' 			=> $city,
				'TEMP_EMAIL_DOMESTIC' 	=> $email_domestic,

				'TEMP_AMOUNT' 			=> $amount,
				'TEMP_NOTIF' 			=> $notif,
				'TEMP_EMAIL' 			=> $email,
				'TEMP_SMS' 				=> $sms,
				'TEMP_LOCKED'			=> $locked,
			);

			$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollback();
			Zend_Debug::Dump($e->getMessages());
		}

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function templateaddremittanceAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();


		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$benename2 = $this->_getParam('benename');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$chargetype = $this->_getParam('chargetype');
		$transpose = $this->_getParam('transpose');
		$emaildomestic = $this->_getParam('emaildomestic');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');
		$messagetext = $this->_getParam('messagetext');
		$identity = $this->_getParam('identity');
		$transactor = $this->_getParam('transactor');
		$ccy = $this->_getParam('ccy');
		$bankname = $this->_getParam('bankname');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "3",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_CHARGE_TYPE' 		=> $chargetype,
			'TEMP_MESSAGE' 		=> $messagetext,
			'TEMP_EMAIL_DOMESTIC' 	=> $emaildomestic,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_IDENTY' 			=> $identity,
			'TEMP_RELATIONSHIP'		=> $transactor,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_LOCKED'			=> $locked,
			'TEMP_BANK'				=> $bankname,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}


	public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		// die;

		if ($transfertype == '19' || $transfertype == '20') {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);

		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		// echo $selectuser;
		$datauser = $this->_db->fetchAll($selectuser);
		// print_r($datauser);die;
		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_USER'), array(
				'*'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);

		// print_r($usergroup);die();


		$this->view->boundarydata = $datauser;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {
				$group = explode('_', $value['GROUP_USER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				// print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);


					foreach ($list as $row => $data) {
						if ($data == $groupalfa) {
							$cek = true;
							// die('ter');
							break;
						}
					}
				}
			}
			if ($group[0] == 'S') {
				$cek = true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		}


		if ($cek) {




			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);

			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);

			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);
			// print_r($list);die;
			// var_dump($command)
			$thendata = explode('THEN', $command);
			// print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);
			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {
					foreach ($secondlist as $row => $thenval) {
						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			// var_dump($thendata);
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
					// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}
			// var_dump($grouplist);
			// // print_r($group);die;
			// // echo $thengroup;die;
			// echo '<pre>';
			// var_dump($thengroup);
			// var_dump($thendata);die;
			// print_r($thendata);echo '<br/>';die('here');
			if ($thengroup == true) {

				// echo $oriCommand;die;
				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;

					$indno = $i;
					// echo $oriCommand;echo '<br>';
					for ($a = $cthen - $indno; $a >= 1; --$a) {

						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}


					// print_r($oriCommand);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					// print_r($oriCommand);echo '<br>';
					// print_r($list);echo '<br>';

					$result = $this->generate($oriCommand, $list, $param, $psnumb);
					// print_r($i);

					// echo 'result-';print_r($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);


							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											return true;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						// echo $oriCommand;die;
						$result = $this->generate($oriCommand, $list, $param, $psnumb);
						// echo $result;echo '<br/>';die;
						if (!$result) {
							// die;
							foreach ($grouplist as $key => $valg) {
								// print_r($valg);die;
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							}
						} else {
							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						// die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);
						// print_r($secondlist);die;
						// print_r($grouplist);die;
						// print_r($thendata[$i]);die;
						// return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {
										return false;
									}
								}
							}
						}
						// $secondresult = $this->generate($thendata[$i-1],$list,$param,$psnumb);
						// print_r($thendata[$i-1]);
						// die;
						// print_r($thendata);die;
						foreach ($grouplist as $key => $valg) {
							// print_r($valg);
							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								// die('here');
								return true;
							} else if (trim($valg) == trim($thendata[$i - 1])) {
								$cekgroup = false;
								// die('here');
								return false;
							}
						}
						if (!$cekgroup) {
							// die('here');
							return true;
						}
					}
					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {
				// var_dump($groupalfa);die;
				foreach ($thendata as $ky => $vlue) {
					if ($vlue == $groupalfa) {
						return true;
					}
				}
				return false;
			}
			// die;


			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_APPROVAL'), array(
							'USER_ID'
						))
						->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.PS_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
					// echo $selectapprover;die;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			// print_r($approver);die;



			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$numb = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $numb . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			// print_r($phpCommand);die;
			$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			// die('here2');
			// var_dump ($result);die;
			return $result;
		} else {
			// die('here');
			return true;
		}
	}


	public function generate($command, $list, $param, $psnumb)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();
		// print_r($list);die;  	
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.PS_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;die;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		// print_r($approver);die;


		// print_r($approver);
		foreach ($param as $url) {
			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$numb = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $numb . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}

		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		// print_r($phpCommand);echo '<br/>';die;
		// var_dump($phpCommand);die;
		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			// var_dump($result);die;
			if ($result) {
				// var_dump($result);die;
				return false;
			} else {
				return true;
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;
	}

	public function summarydetailAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		$filter 			= new Application_Filtering();
		$PS_NUMBER 			= $filter->filter($this->_getParam('payReff'), "PS_NUMBER");

		$paramSQL = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);


		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramSQL);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);

		// $tableMst[0]["label"] = $this->language->_('Payment Type');
		// $tableMst[1]["label"] = $this->language->_('#Trans');
		// $tableMst[2]["label"] = $this->language->_('Recurring');
		// // var_dump($pslip);die;
		// if (($pslip['PS_CATEGORY'] == 'SINGLE PAYMENT' || $pslip['PS_CATEGORY'] == 'OPEN TRANSFER')) {
		// 	if ($pslip['TRANSFER_TYPE'] == 0) {
		// 		if ($pslip['SOURCE_ACCT_BANK_CODE'] == $pslip['BENEF_ACCT_BANK_CODE']) {
		// 			$payType = $pslip['payType'] . ' - ' . 'PB';
		// 		} else {
		// 			$payType = $pslip['payType'] . ' - ' . 'ONLINE';
		// 		}
		// 	} else if ($pslip['TRANSFER_TYPE'] == 1) {
		// 		$payType = $pslip['payType'] . ' - ' . 'SKN';
		// 	} else if ($pslip['TRANSFER_TYPE'] == 2) {
		// 		$payType = $pslip['payType'] . ' - ' . 'RTGS';
		// 	} else {
		// 		$payType = $pslip['payType'];
		// 	}
		// } else if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

		// 	if ($pslip['PS_TYPE'] == '19') {
		// 		$poolingType = 'Remains';
		// 	} else {
		// 		$poolingType = 'Maintains';
		// 	}

		// 	$payType = $pslip['payType'] . ' - ' . $poolingType;
		// } else {
		// 	$payType = $pslip['payType'];
		// }

		// if (!empty($pslip['PERIODIC'])) {
		// 	$recurring = $this->language->_('Yes');
		// } else {
		// 	$recurring = $this->language->_('No');
		// }

		// $tableMst[0]["value"] = $payType;
		// $tableMst[1]["value"] = $pslip['PS_TXCOUNT'];
		// $tableMst[2]["value"] = $recurring;

		// if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {



		// 	$arrday = array(
		// 		'0' => 'Sunday',
		// 		'1' => 'Monday',
		// 		'2' => 'Tuesday',
		// 		'3' => 'Wednesday',
		// 		'4' => 'Thursday',
		// 		'5' => 'Friday',
		// 		'6' => 'Saturday'
		// 	);



		// 	$select	= $this->_db->select()
		// 		->from(array('P'	 		=> 'T_PSLIP'))
		// 		->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
		// 		->join(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
		// 		->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
		// 		->GROUP('D.DAY_ID');


		// 	$periodicData = $this->_db->fetchAll($select);

		// 	$tableMst[3]["label"] = $this->language->_('Repeat');
		// 	$tableMst[4]["label"] = $this->language->_('Repeat On');
		// 	$tableMst[5]["label"] = $this->language->_('Start From');
		// 	$tableMst[6]["label"] = $this->language->_('Time');

		// 	if ($pslip['PS_TYPE'] == 19) {
		// 		$tableMst[7]["label"] = $this->language->_('Remains on Source');

		// 		$remainsOrMaintains = '<h6 class="text-danger"><b>' . $pslip['ccy'] . ' n/a </b></h6>';
		// 	} else {
		// 		$tableMst[7]["label"] = $this->language->_('Maintains Destination');

		// 		$remainsOrMaintains = '';
		// 		foreach ($periodicData as $key => $value) {
		// 			$remainsOrMaintains .= substr($arrday[$value['DAY_ID']], 0, 3) . ' IDR ' . Application_Helper_General::displayMoney($value['LIMIT_AMOUNT']) . '<br>';
		// 		}
		// 	}

		// 	if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 1) {


		// 		if (empty($pslip['PS_PERIODIC'])) {
		// 			$repeat = 'No';
		// 			$repeatOn = '-';
		// 		} else {
		// 			$repeat = 'Daily';
		// 			$repeatOn = '<div class="input-group">';
		// 			foreach ($periodicData as $key => $value) {


		// 				$repeatOn .= '<div class="roundChk" style="margin-left: -4%;">
		// 								<input type="checkbox" disabled class="checkboxday" id="checkbox' . $value['DAY_ID'] . '" value="' . $value['DAY_ID'] . '" checked />
		// 								<label for="checkbox' . $value['DAY_ID'] . '">' . substr($arrday[$value['DAY_ID']], 0, 1) . '</label>
		// 							</div>';
		// 			}
		// 			$repeatOn .= '</div>';
		// 		}
		// 	} else if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 2) {
		// 		if (empty($pslip['PS_PERIODIC'])) {
		// 			$repeat = 'No';
		// 			$repeatOn = '-';
		// 		} else {
		// 			$repeat = 'Weekly';
		// 			$repeatOn = $arrday2[$pslip['PS_EVERY_PERIODIC']];
		// 		}
		// 	} else {
		// 		if (empty($pslip['PS_PERIODIC'])) {
		// 			$repeat = 'No';
		// 			$repeatOn = '-';
		// 		} else {
		// 			$repeat = 'Monthly';
		// 			$repeatOn = $pslip['PS_EVERY_PERIODIC'] . ' of every month';
		// 		}
		// 	}

		// 	if (empty($pslip['PS_PERIODIC'])) {
		// 		$efDate = date_create($pslip['PS_EFDATE']);
		// 		$newEfDate = date_format($efDate, "d M Y");

		// 		$startFrom = $newEfDate;
		// 	} else {

		// 		$startDate = date_create($periodicData[0]['PS_PERIODIC_STARTDATE']);
		// 		$newStartDate = date_format($startDate, "d M Y");

		// 		$endDate = date_create($periodicData[0]['PS_PERIODIC_ENDDATE']);
		// 		$newEndDate = date_format($endDate, "d M Y");

		// 		$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;
		// 	}

		// 	$tableMst[3]["value"] = $repeat;
		// 	$tableMst[4]["value"] = $repeatOn;
		// 	$tableMst[5]["value"] = $startFrom;
		// 	$tableMst[6]["value"] = $pslip['PS_EFTIME'];
		// 	$tableMst[7]["value"] = $remainsOrMaintains;
		// } else {
		// 	$tableMst[3]["label"] = $this->language->_('Total Amount');
		// 	$tableMst[4]["label"] = $this->language->_('Total Transfer Fee');
		// 	$tableMst[5]["label"] = $this->language->_('Total Payment');


		// 	$tableMst[3]["value"] = $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['PS_TOTAL_AMOUNT']);
		// 	$tableMst[4]["value"] = Application_Helper_General::displayMoney($pslip['FULL_AMOUNT_FEE'] == '0' ? '-' : $pslip['ccy'] . ' ' . $pslip['FULL_AMOUNT_FEE']);
		// 	$tableMst[5]["value"] = '<h6 class="text-danger"><b>' . $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney(($pslip['PS_TOTAL_AMOUNT'] + $pslip['FULL_AMOUNT_FEE'])) . '</b></h6>';
		// }

		if ($pslip["PS_TYPE"] == '19' || $pslip["PS_TYPE"] == '20') {
			
			if ($pslip["PS_TYPE"] == '19') {
				$tableMst[0]["label"] = $this->language->_('Remains on source');
			}else{
				$tableMst[0]["label"] = $this->language->_('Maintains on beneficiary');	
			}
			
			$tableMst[1]["label"] = $this->language->_('Minimum Transfer Amount');

			$tableMst[0]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['TRA_REMAIN']);
			$tableMst[1]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['PS_MIN_AMOUNT']);

			$this->view->tableMst 		= $tableMst;
			$this->view->pslip = $pslip["PS_TYPE"];

		}else{

			$tableMst[0]["label"] = $this->language->_('Total Amount');
			$tableMst[1]["label"] = $this->language->_('Total Transfer Fee');
			$tableMst[2]["label"] = $this->language->_('Total Payment');

			$tableMst[0]["value"] = $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']);
			$tableMst[1]["value"] = $pslip['TRANSFER_FEE'] == '0' ? '-' : $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['TRANSFER_FEE']);
			$tableMst[2]["value"] = '<h6 style="color: red;"><b>' . $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney(($pslip['PS_TOTAL_AMOUNT'] )) . '</b></h6>';

			if (($pslip["PS_BILLER_ID"] == '1156' || $pslip["PS_BILLER_ID"] == '1158') && $pslip["PS_TYPE"] == '17') {
				$tableMst = $this->etax($pslip);
			} elseif ($pslip["PS_TYPE"] == '18') {
				$tableMst = $this->sp2d($pslip);
			}

			$this->view->tableMst 		= $tableMst;

		}
		
	}

	public function acctdetailAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		$filter 			= new Application_Filtering();
		// $benefId 		= $filter->filter($this->_getParam('benef'), "BENEFICIARY_ID");
		$sourceacct = $this->_getParam('sourceacct');

		// $select = $this->_db->select()
		// 	->from('M_BENEFICIARY', array('*'))
		// 	->where("BENEFICIARY_ID = ?", $benefId); 

		// $benefData = $this->_db->fetchRow($select);

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$param = array('ACCT_NO' => $sourceacct);
		$AccArr = $CustomerUser->getAccounts($param);

		// $citizenshipArr	= array("W" => "WNI", "N" => "WNA");
		// $residentArr = array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);

		// $settings 			= new Application_Settings();
		// $LLD_array 			= array();
		// $LLD_DESC_arrayCat 	= array();
		// $lldTypeArr  		= $settings->getLLDDOMType();

		// $LLD_CATEGORY_POST = '';
		// if (!empty($benefData['BENEFICIARY_CATEGORY'])) {
		// 	$lldCategoryArr  	= $settings->getLLDDOMCategory();
		// 	$LLD_array["CT"] 	= $benefData['BENEFICIARY_CATEGORY'];
		// 	$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$benefData['BENEFICIARY_CATEGORY']];
		// }

		// $tableMst[0]["label"] = $this->language->_('Beneficiary Bank');
		// $tableMst[1]["label"] = $this->language->_('City');
		// $tableMst[2]["label"] = $this->language->_('Beneficiary Account');
		// $tableMst[3]["label"] = $this->language->_('Beneficiary Name');
		// $tableMst[4]["label"] = $this->language->_('Beneficiary Alias Name');
		// $tableMst[5]["label"] = $this->language->_('Beneficiary Address 1');
		// $tableMst[6]["label"] = $this->language->_('Beneficiary Address 2');
		// $tableMst[7]["label"] = $this->language->_('Beneficiary Email');
		// $tableMst[8]["label"] = $this->language->_('Citizenship');
		// $tableMst[9]["label"] = $this->language->_('Nationality');
		// $tableMst[10]["label"] = $this->language->_('Beneficiary Category');
		// $tableMst[11]["label"] = $this->language->_('Beneficiary ID Type');
		// $tableMst[12]["label"] = $this->language->_('Beneficiary ID Number');

		// $tableMst[0]["value"] = empty($benefData['BANK_NAME']) ? $this->_bankName : $benefData['BANK_NAME'];
		// $tableMst[1]["value"] = empty($benefData['BANK_CITY']) ? '-' : $benefData['BANK_CITY'];
		// $tableMst[2]["value"] = $benefData['BENEFICIARY_ACCOUNT'];
		// $tableMst[3]["value"] = $benefData['BENEFICIARY_NAME'];
		// $tableMst[4]["value"] = empty($benefData['BENEFICIARY_ALIAS']) ? '-' : $benefData['BENEFICIARY_ALIAS'];
		// $tableMst[5]["value"] = empty($benefData['BENEFICIARY_ADDRESS']) ? '-' : $benefData['BENEFICIARY_ADDRESS'];
		// $tableMst[6]["value"] = empty($benefData['BENEFICIARY_ADDRESS2']) ? '-' : $benefData['BENEFICIARY_ADDRESS2'];
		// $tableMst[7]["value"] = empty($benefData['BENEFICIARY_EMAIL']) ? '-' : $benefData['BENEFICIARY_EMAIL'];
		// $tableMst[8]["value"] = empty($benefData['BENEFICIARY_RESIDENT']) ? '-' : $residentArr[$benefData['BENEFICIARY_RESIDENT']];
		// $tableMst[9]["value"] = empty($benefData['BENEFICIARY_CITIZENSHIP']) ? '-' : $citizenshipArr[$benefData['BENEFICIARY_CITIZENSHIP']];
		// $tableMst[10]["value"] = empty($LLD_CATEGORY_POST) ? '-' : $LLD_CATEGORY_POST;
		// $tableMst[11]["value"] = empty($benefData['BENEFICIARY_ID_TYPE']) ? '-' : $benefData['BENEFICIARY_ID_TYPE'];
		// $tableMst[12]["value"] = empty($benefData['BENEFICIARY_ID_NUMBER']) ? '-' : $benefData['BENEFICIARY_ID_NUMBER'];
		
		$tableMst[0]["label"] = $this->language->_('Debit Account');
		$tableMst[1]["label"] = $this->language->_('Debit Account Name');
		$tableMst[2]["label"] = $this->language->_('CCY ID');

		$tableMst[0]["value"] = $AccArr[0]["ACCT_NO"];
		$tableMst[1]["value"] = $AccArr[0]["ACCT_NAME"];
		$tableMst[2]["value"] = $AccArr[0]["CCY_ID"];

		$this->view->tableMst 		= $tableMst;
	}
}
