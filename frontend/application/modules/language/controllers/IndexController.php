<?php
class language_IndexController extends Application_Main {
	public function indexAction(){
		$model = new language_Model_Index();
		
		$sql = $model->getLang();
		$arrLang = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				$arrLang += array($val['CODE'] => $val['DESC']);
			}
		}
		$this->view->arrLang = $arrLang;
		
		$sql = $model->getUi();
		$arrUi = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				$arrUi += array($val['ID'] => $val['DESC']);
			}
		}
		$this->view->arrUi = $arrUi;
		
		$tmxPath = array(
			'frontend' => APPLICATION_PATH.'/../../library/data/languages/frontend/language.tmx',
			'backend' => APPLICATION_PATH.'/../../library/data/languages/backend/language.tmx',
		);
		
		$filename = $tmxPath['frontend'];
		
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$headers = array	(
			'ui'	=> array(
				'field'	=> 'UI',
				'label'	=> $this->language->_('UI'),
				'sortable'	=> true
			),
			'language'	=> array(
				'field' => 'LANGUAGE',
				'label' => $this->language->_('Language'),
				'sortable'	=> true
			),
			'phrase'	=> array(
				'field' => 'PHRASE',
				'label' => $this->language->_('Phrase'),
				'sortable' => true
			),
			'translated'	=> array(
				'field' => 'TRANSLATED',
				'label' => $this->language->_('Translated'),
				'sortable' => true
			),
			'update'	=> array(
				'field' => 'MTDUPDATED',
				'label' => $this->language->_('Update'),
				'sortable' => true
			),
		);
		
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($headers, 'field');
		
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $headers) ? $headers[$this->_getParam('sortby')]['field'] : $field[4]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
			'ui'			=> 	array(
									new Zend_Validate_InArray(array_values(array_flip($arrUi))),
								),
			'oriPhrase'		=> array(),
			'traPhrase'		=> array(),
			'lang'			=> 	array(
									new Zend_Validate_InArray(array_values(array_flip($arrLang))),
								),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		$data = array();
		
		$paramsSql = array(
			'sortBy' => $sortBy,
			'sortDir' => $sortDir,
		);
			
		
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
			
			$ui = $filter->getEscaped('ui');
			if (!empty($ui)){
				$paramsSql['ui'] = $ui;
			}
			
			$lang = $filter->getEscaped('lang');
			if (!empty($lang)){
				$paramsSql['lang'] = $lang;
			}
			
			$oriPhrase = $filter->getEscaped('oriPhrase');
			if (!empty($oriPhrase)){
				$paramsSql['oriPhrase'] = $oriPhrase;
			}
			
			$traPhrase = $filter->getEscaped('traPhrase');
			if (!empty($traPhrase)){
				$paramsSql['traPhrase'] = $traPhrase;
			}
		}else{
			Zend_Debug::dump($filter->getMessages());
		}
		
		$data = $model->getList($paramsSql);
		
		$this->view->headers 		= $headers;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
		$this->paging($data);
	}	
}
