<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class transactionmonitoring_IndexController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $successTotal = $this->_db->select()
                ->from(array('A' => 'T_TRANSACTION'),array('*'))
                ->where('A.TRA_STATUS = ? ', '4')
                ->where('YEAR(A.DATE_UPDATE) = ?' , '2021')
                ->query()->fetchAll();
        
        $failureTotal = $this->_db->select()
                ->from(array('A' => 'T_TRANSACTION'),array('*'))
                ->where('A.TRA_STATUS = ? ', '3')
                ->where('YEAR(A.DATE_UPDATE) = ?' , '2021')
                ->query()->fetchAll();

        $total = count($successTotal) + count($failureTotal);
        $srate = (($total - count($successTotal))/$total)*100;
        $frate = (($total - count($failureTotal))/$total)*100;

        $this->view->successTotal = count($successTotal);
        $this->view->failureTotal = count($failureTotal);
        $this->view->srate = $srate;
        $this->view->frate = $frate;
        $this->view->total = $total;
        $this->view->year = '2021';

        

        if($this->_request->isPost() )
        {
            $year = $this->_getParam('year');
            $month = $this->_getParam('month');
			// $year = "2020";
            if($year != '0' ){
            $newsuccess = $this->_db->select()
                ->from(array('A' => 'T_TRANSACTION'),array('*'))
                ->where('A.TRA_STATUS = ? ', '4')
                ->where('YEAR(A.DATE_UPDATE) = ?' , $year)
                ->query()->fetchAll();
            
            $newfail = $this->_db->select()
                ->from(array('A' => 'T_TRANSACTION'),array('*'))
                ->where('A.TRA_STATUS = ? ', '3')
                ->where('YEAR(A.DATE_UPDATE) = ?' , $year)
                ->query()->fetchAll();

                $total2 = count($newsuccess) + count($newfail);
                $srate2 = (($total2 - count($newsuccess))/$total2)*100;
                $frate2 = (($total2 - count($newfail))/$total2)*100;

                $this->view->newsuccess = count($newsuccess);
                $this->view->newfail = count($newfail);
                $this->view->srate = $srate2;
                $this->view->frate = $frate2;
                $this->view->total = $total2;
                $this->view->year = $year;
            }
                
        }

        
    }

}