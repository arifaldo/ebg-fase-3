<?php
require_once 'Zend/Controller/Action.php';

class home_ChatwithticketingController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$sessTicketId  = new Zend_Session_Namespace('sessTicketId');
		if(isset($sessTicketId->content)){
			Zend_Session::namespaceUnset('sessTicketId');
		}

		$ticketId 	 = $this->language->_('Ticket Number');
		$category 	 = $this->language->_('Category');
		$subject 	 = 'Subjek';
		$createdDate = $this->language->_('Created Date');
		$createdBy 	 = $this->language->_('Created By');
		$status 	 = $this->language->_('Status');

		$fields = [
			'ticketId' => [
				'field' 	=> 'TICKET_ID',
				'label'		=> $ticketId,
				'sortable'	=> true
			],
			'category' => [
				'field' 	=> 'CATEGORY',
				'label'		=> $category,
				'sortable'	=> true
			],
			'subject' => [
				'field' 	=> 'SUBJECT',
				'label'		=> $subject,
				'sortable'	=> true
			],
			'createdDate' => [
				'field' 	=> 'CREATED_DATE',
				'label'		=> $createdDate,
				'sortable'	=> true
			],
			'createdBy' => [
				'field' 	=> 'CREATED_BY',
				'label'		=> $createdBy,
				'sortable'	=> true
			],
			'status' => [
				'field' 	=> 'STATUS',
				'label'		=> $status,
				'sortable'	=> true
			]
		];

		//get page, sortby, sortdir
		$detail   		= $this->_getParam('detail');
		$submit 		= $this->_getParam('submit');
		$page 			= $this->_getParam('page');
		$sortBy 		= $this->_getParam('sortby');
		$sortDir 		= $this->_getParam('sortdir');
		$filter_clear 	= $this->_getParam('filter_clear');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0)) ? $page : 1;
		            
		$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc')))) ? $sortDir : 'desc';

		//get filtering param
		$filters = array(
			'filter'		=>  array('StringTrim', 'StripTags'),
			'fCategory'		=>  array('StringTrim', 'StripTags'),
			'fSubject'		=>  array('StringTrim', 'StripTags'),
			'fCreatedDate'	=>  array('StringTrim', 'StripTags'),
			'fCreatedBy'	=>  array('StringTrim', 'StripTags'),
			'fStatus'		=>  array('StringTrim', 'StripTags'),
		);

		$validators = array(
			'filter'		=>  array('allowEmpty' => true),
			'fCategory'		=>  array('allowEmpty' => true),
			'fSubject'		=>  array('allowEmpty' => true),
			'fCreatedDate'	=>  array('allowEmpty' => true),
			'fCreatedBy'	=>  array('allowEmpty' => true),
			'fStatus'		=>  array('allowEmpty' => true),
		);

		$optionValidators = array('breakChainOnFailure' => false);

		$zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optionValidators);

		$filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');

		if($filter == TRUE && $filter_clear == NULL){
			$fCategory 		= $filterParam['fCategory'] = $this->_request->getParam('fCategory');
			$fSubject 		= $filterParam['fSubject'] = $this->_request->getParam('fSubject');
			$fCreatedDate 	= html_entity_decode($zf_filter->getEscaped('fCreatedDate'));
			$fCreatedBy 	= $filterParam['fCreatedBy'] = $this->_request->getParam('fCreatedBy');
			$fStatus 		= $filterParam['fStatus'] = $this->_request->getParam('fStatus');
		}
			
		if($fCreatedDate){
			$FormatDate = new Zend_Date($fCreatedDate, $this->_dateDisplayFormat);
			$filterParam['fCreatedDate']  = $FormatDate->toString($this->_dateDBFormat);	
		}

		$data = $this->_db->select()
			->from(
				array('T' => 'TICKETING'),
				array(
					'TICKET_ID'		=> 'T.TICKET_ID',
					'CATEGORY'		=> new Zend_Db_Expr("(CASE T.CATEGORY
															WHEN '1' THEN 'Transaction'
															WHEN '2' THEN 'User Administration'
															WHEN '3' THEN 'Report Problem'
															WHEN '4' THEN 'Other'
								  		 				END)"),
					'SUBJECT'		=> 'T.SUBJECT',
					'CREATED_DATE'	=> 'T.CREATED_DATE',
					'CREATED_BY'	=> 'T.CREATED_BY',
					'STATUS'		=> new Zend_Db_Expr("(CASE T.STATUS
															WHEN '0' THEN 'Close'
															WHEN '1' THEN 'Open'
								  		 				END)")
				)
			)
			// ->joinLeft(
				// array('TD' => 'TICKETING_DETAIL'),'TD.TICKET_ID = T.TICKET_ID',array()
			// )
			->where('T.CUST_CODE = ?', $this->_custIdLogin)
			->where('T.CREATED_BY = ?', $this->_userIdLogin);

		if($filter == TRUE){
			if($fCategory){
				$data->where('T.CATEGORY = ?', $fCategory);
			}
			if($fStatus){
				$fStatus = ($fStatus==1) ? 1 : 0;
				$data->where('T.STATUS = ?', $fStatus);
			}
		}
		$data->order($sortBy.' '.$sortDir);
		$data = $this->_db->fetchAll($data);
		// echo '<pre><br>';print_r($data);die;
		$this->paging($data);

		$categoryArr = [
			'1'	=> 'Transaction',
			'2'	=> 'User Administration',
			'3'	=> 'Report Problem',
			'4' => 'Other'
		];
		$statusArr = [
			'1'	=> 'Open',
			'2'	=> 'Close'
		];

		$this->view->currentPage	= $page;
		$this->view->sortby 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->fields 		= $fields;
		$this->view->filter 		= $filter;
		$this->view->fCategory 		= $fCategory;
		$this->view->fSubject 		= $fSubject;
		$this->view->fCreatedDate 	= $fCreatedDate;
		$this->view->fCreatedBy 	= $fCreatedBy;
		$this->view->fStatus 		= $fStatus;
		$this->view->categoryArr 	= $categoryArr;
		$this->view->statusArr 		= $statusArr;

		if($submit){
			//get filtering param
			$filters = array(
				'category'	=>  array('StringTrim', 'StripTags'),
				'subject'	=>  array('StringTrim', 'StripTags'),
				'comment'	=>  array('StringTrim', 'StripTags'),
			);

			$validators = array(
				'category'	 => array(
									'NotEmpty',
									'messages' => array($this->language->_('Error: Category cannot be empty'))
								),
				'subject'	 => array(
									'NotEmpty',
									'messages' => array($this->language->_('Error: Subject cannot be empty'))
								),
				'comment'	 => array(
									'NotEmpty',
									'messages' => array($this->language->_('Error: Comment cannot be empty'))
								),
			);

			$params['category']	= $this->_getParam('category');
			$params['subject']	= $this->_getParam('subject');
			$params['comment']	= $this->_getParam('comment');

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

			if($zf_filter_input->isValid()){
				try{
					$this->_db->beginTransaction();

					$data = [
						'TICKET_ID'		=> date('ymdHis'),
						'CUST_CODE'		=> $this->_custIdLogin,
						'CREATED_DATE'	=> date('Y-m-d H:i:s'),
						'CREATED_BY'	=> $this->_userIdLogin,
						'CATEGORY'		=> $zf_filter_input->category,
						'SUBJECT'		=> $zf_filter_input->subject
					];
					$this->_db->insert('TICKETING', $data);

					$dataDetail = [
						'TICKET_ID'		=> date('ymdHis'),
						'USER_ID'		=> $this->_userIdLogin,
						'USER_TYPE'		=> 1,
						'COMMENT_DATE'	=> date('Y-m-d H:i:s'),
						'COMMENT'		=> $zf_filter_input->comment
					];
					$this->_db->insert('TICKETING_DETAIL', $dataDetail);

					$this->_db->commit();

		 			$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
					$this->_redirect('/notification/success');

					Application_Helper_General::writeLog('CSCQ','Customer Service Create Ticketing');
				}
				catch(Exception $error){
					$this->_db->rollBack();
					$errMsg = $this->language->_('Data Fail To Insert');
					$this->view->error = true;
					$this->view->errMsg = $errMsg;
				}
			}else{
				$errors = $zf_filter_input->getMessages();
				$errDesc['category'] = (isset($errors['category'])) ? $errors['category'] : null;
				$errDesc['subject']  = (isset($errors['subject'])) ? $errors['subject'] : null;
				$errDesc['comment']  = (isset($errors['comment'])) ? $errors['comment'] : null;

				$this->view->error = true;
				$this->view->errDesc  = $errDesc;
				$this->view->category = $params['category'];
				$this->view->subject  = $params['subject'];
				$this->view->comment  = $params['comment'];
			}
		}elseif($detail){
			$sessTicketId  = new Zend_Session_Namespace('sessTicketId');
			$sessTicketId->content = $detail;
			$this->_redirect('home/chatwithticketing/detail');
		}else{
			Application_Helper_General::writeLog('CSVQ','Customer Service View Ticketing');
		}
	}

	public function detailAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$submit = $this->_getParam('submit');

		$sessTicketId  = new Zend_Session_Namespace('sessTicketId');
		$ticketId = $sessTicketId->content;
		$categoryArr = [
			'1'	=> 'Transaction',
			'2'	=> 'User Administration',
			'3'	=> 'Report Problem',
			'4' => 'Lainnya'
		];
		if(isset($ticketId)){
			$data = $this->_db->select()
				->from(
					array('T' => 'TICKETING'),
					array(
						'TICKET_ID'		=> 'T.TICKET_ID',
						'CATEGORY'		=> new Zend_Db_Expr("(CASE T.CATEGORY
																WHEN '1' THEN 'Transaction'
																WHEN '2' THEN 'User Administration'
																WHEN '3' THEN 'Report Problem'
																WHEN '4' THEN 'Other'
									  		 				END)"),
						'SUBJECT'		=> 'T.SUBJECT',
						'CREATED_DATE'	=> 'T.CREATED_DATE',
						'CREATED_BY'	=> 'T.CREATED_BY',
						'CLOSED_DATE'	=> 'T.CLOSED_DATE',
						'CLOSED_BY'		=> 'T.CLOSED_BY',
						'STATUS'		=> new Zend_Db_Expr("(CASE T.STATUS
																WHEN '0' THEN 'Close'
																WHEN '1' THEN 'Open'
									  		 				END)")
					)
				)
				->where('T.TICKET_ID = ?', $ticketId);
			$data = $this->_db->fetchRow($data);

			$dataDetail = $this->_db->select()
				->from(
					array('TD' => 'TICKETING_DETAIL'),array('*')
				)
				->where('TD.TICKET_ID = ?', $ticketId);
			$dataDetail = $this->_db->fetchAll($dataDetail);

			$this->view->data 		= $data;
			$this->view->dataDetail = $dataDetail;

			if($submit){
				//get filtering param
				$filters = array('comment' => array('StringTrim', 'StripTags'));

				$validators = array(
					'comment' => array(
									'NotEmpty',
									'messages' => array($this->language->_('Error: Comment cannot be empty'))
								 )
				);

				$params['comment']	= $this->_getParam('comment');

				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

				if($zf_filter_input->isValid()){
					try{
						$this->_db->beginTransaction();

						$dataDetail = [
							'TICKET_ID'		=> $ticketId,
							'USER_ID'		=> $this->_userIdLogin,
							'USER_TYPE'		=> 1,
							'COMMENT_DATE'	=> date('Y-m-d H:i:s'),
							'COMMENT'		=> $zf_filter_input->comment
						];
						$this->_db->insert('TICKETING_DETAIL', $dataDetail);

						$this->_db->commit();

			 			$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/detail');
						$this->_redirect('/notification/success');
					}
					catch(Exception $error){
						$this->_db->rollBack();
						$errMsg = $this->language->_('Failed to send a comment');

						$this->view->error  = true;
						$this->view->errMsg = $errMsg;
					}
				}else{
					$errors = $zf_filter_input->getMessages();
					$errDesc['comment']  = (isset($errors['comment'])) ? $errors['comment'] : null;

					$this->view->errDesc = $errDesc;
					$this->view->comment = $params['comment'];
				}
			}else{
				Application_Helper_General::writeLog('CSVQ','Customer Service View Ticketing Detail #'.$ticketId);
			}
		}
	}
}