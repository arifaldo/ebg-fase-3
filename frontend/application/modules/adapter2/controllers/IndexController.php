<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/Payment.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class adapter2_IndexController extends Application_Main
{
    protected $_moduleDB = 'RTF'; //masih harus diganti

    protected $_destinationUploadDir = '';
    protected $_maxRow = '';

    public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

    public function indexAction()
    {
        $this->setbackURL();
        $this->_helper->_layout->setLayout('newlayout');
        $settingObj = new Settings();
        $this->view->THRESHOLD_LLD      = $settingObj->getSetting("threshold_lld"   , 0);

        $this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        $AccArr       = $CustomerUser->getAccounts();
        $this->view->AccArr =  $AccArr;
        $listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');

        if($this->_request->isPost() )
        {
            $this->_request->getParams();
            // $data = $this->_request->getParams();
// print_r($data);die;


            $filter = new Application_Filtering();
            $confirm = false;
            $error_msg[0] = "";

            $PS_SUBJECT     = $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
            // $PS_EFDATE      = $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
            // $ACCTSRC        = $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");

            // if(!$ACCTSRC)
            // {
            //     $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Source Account cannot be left blank.').'';
            //     $this->view->error      = true;
            //     $this->view->report_msg = $this->displayError($error_msg);
            // }
            // else if(!$PS_EFDATE)
            // {
            //     $error_msg[0] = 'Error: Payment Date can not be left blank.';
            //     $this->view->error      = true;
            //     $this->view->report_msg = $this->displayError($error_msg);
            // }
            // else
            // {
                $paramSettingID = array('range_futuredate', 'auto_release_payment');

                $settings = new Application_Settings();
                $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
                $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'txt', 'json', 'xml', 'xls', 'xlsx'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv, *.json, *.xml, *.txt, *.xls, *.xlsx'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));

                $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);

                $extension = explode('.', $sourceFileName);

                $extensionName = $extension[1];

                $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                $adapter->addFilter ( 'Rename',$newFileName  );

                if ($adapter->isValid ())
                    {

                    if ($adapter->receive()) {
                         
                        $file_contents = file_get_contents($newFileName);

                        //if csv occured
                        if ($extensionName === 'csv') {

                             //PARSING CSV HERE
                            $data = $this->_helper->parser->parseCSV($newFileName);
                        }
                        //if txt occured
                        else if ($extensionName === 'txt') {

                           $lines = file($newFileName);
                            foreach($lines as $line) {
                                $data[] = explode(',', $line);
                            }
                        }
                        //if json occured
                        else if ($extensionName === 'json') {

                            $datajson = json_decode($file_contents, 1);
                            $i = 0;
                            foreach ($datajson as $key => $value) {
                                if ($i == 0) {
                                    $data[$i] = array_keys($value);
                                    $data[$i + 1] = array_values($value);
                                }
                                else{
                                    $data[$i+1] = array_values($value);
                                }

                                $i++;
                            }
                        }
                        //if xml occured
                        else if($extensionName === 'xml'){

                            header('Content-Type: text/plain');
                            $file_contents = file_get_contents($newFileName);

                            $xml = (array) simplexml_load_string($file_contents);

                            // print_r($xml);die();
                             $i = 0;
                            foreach ($xml as $key => $value) {
                                foreach ($value as $key2 => $value2) {
                                    if ($i == 0) {
                                        $data[$i] = array_keys((array)$value2);
                                        $data[$i + 1] = array_values((array)$value2);
                                    }
                                    else{
                                        $data[$i+1] = array_values((array)$value2);
                                    }

                                    $i++;
                                }
                            }

                            for($i=0; $i<count($data); $i++){
                                if ($i > 0) {
                                    foreach ($data[$i] as $key => $value) {
                                        if (empty($value)) {
                                            $data[$i][$key] = null;
                                        }
                                    }
                                }
                            }
                        }
                        else if($extensionName === 'xls' || $extensionName === 'xlsx'){
                            try {
                                $inputFileType = IOFactory::identify($newFileName);
                                $objReader = IOFactory::createReader($inputFileType);
                                $objPHPExcel = $objReader->load($newFileName);
                            } catch(Exception $e) {
                                die('Error loading file "'.pathinfo($newFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                            }

                            $sheet = $objPHPExcel->getSheet(0);
                            $highestRow = $sheet->getHighestRow();
                            $highestColumn = $sheet->getHighestColumn();
                             
                            for ($row = 1; $row <= $highestRow; $row++){                        
                                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

                                $data[$row - 1] = $rowData[0];
                            }
                        }
                       
                        @unlink($newFileName);

                        if(!empty($data))
                        {
                            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                            $sessionNamespace->content = $data;
                            $sessionNamespace->fileName = $sourceFileName;
                            $this->_redirect('/adapter2/index/confirm');  
                        }
                        else //kalo total record = 0
                        {
                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formats').'.';
                            $this->view->error      = true;
                            $this->view->report_msg = $this->displayError($error_msg);
                        }
                    }
                }
                else
                {
                    $this->view->error = true;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                        if($key=='fileUploadErrorNoFile')
                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                        else
                            $error_msg[0] = $val;
                        break;
                    }
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;
                }   

            if($confirm)
            {
                $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                $sessionNamespace->content = $csvData2;
                $this->_redirect('/adapter2/index/confirm');
            }

            $this->view->PSSUBJECT = $PS_SUBJECT;
            $this->view->ACCTSRC = $ACCTSRC;
            $this->view->PSEFDATE = $PS_EFDATE;
        }
        Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
        Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
    }

    public function confirmAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
        $data = $sessionNamespace->content;
        $this->view->refresh = '0';
        $this->view->data = $data;
        $param = $this->_request->getParams();

        // print_r($data);die();
       
        if($this->_request->isPost()  )
        {
            $label = $this->_getParam('label');
            $index = $this->_getParam('index');
            $value = $this->_getParam('value');

            $type = $this->_getParam('type');
            $fileName = $this->_getParam('fileName');

            if (empty($fileName)) {
                $explodeFileName = explode('.', $sessionNamespace->fileName);
                $fileName = $explodeFileName[0];
            }

            // print_r($label);
            // echo '<br>';
            // print_r($index);
            // echo '<br>';
            // print_r($value);
            // echo '<br>';
            // die();     

            // print_r($data);die();

            if ($type == 'csv' || $type == 'txt' || $type == 'xls' || $type == 'xlsx') {

                $fields = array (   'bene'                   => array    (
                                                                        'field' => 'bene',
                                                                        'label' => $this->language->_('BENEFICIARY ACC'),
                                                                        'sortable' => true
                                                                ),
                            'ccy'                   => array    (
                                                                        'field' => 'ccy',
                                                                        'label' => $this->language->_('CCY'),
                                                                        'sortable' => true
                                                                    ),
                            'amount'                   => array    (
                                                                        'field' => 'amount',
                                                                        'label' => $this->language->_('AMOUNT'),
                                                                        'sortable' => true
                                                                    ),  
                            'message'                    => array    (
                                                                        'field' => 'message',
                                                                        'label' => $this->language->_('MESSAGE'),
                                                                        'sortable' => true
                                                                    ),
                            'additional'               => array    (
                                                                        'field' => 'additional',
                                                                        'label' => $this->language->_('ADDITIONAL MESSAGE'),
                                                                        'sortable' => true
                                                                    )
                        );

                $header = $label;
                $header[] = 'MESSAGE';
                $header[] = 'ADDTIONAL MESSAGE';

                $field  = Application_Helper_Array::simpleArray($fields, "field");
                    // print_r($param['index']);
                $total = count($data[0]);

                foreach ($value as $val) {
                    $newValue[] = json_decode($val, 1);
                }

                // $totalcolom = count($newValue);

                // if ($totalcolom > 2) {
                //     $totalcolom--;
                // }

                // for ($i=1; $i < $totalcolom; $i++){
                //      foreach ($index as $no => $val) {
                //         // print_r($val);print_r($total);die;
                        
                //         if($val == $total){
                //             $dataexcel[$i-1]['ccy'] = 'IDR';    
                //         }elseif($val == $total+1){
                //             foreach ($param['wherecol'] as $key => $value) {
                //                $dataexcel[$i-1]['message'] .= $data[$value][$val];
                //             }
                                
                //         }elseif($val == $total+2){
                //             // $dataexcel[$i-1]['additional'] = '';    
                //             foreach ($param['wherecoladd'] as $key => $value) {
                //                $dataexcel[$i-1]['additional'] .= $data[$value][$val];
                //             }
                //         }else{
                //             $tagfield = $field[$no];
                //             $dataexcel[$i-1][$tagfield] = $data[$i][$val];
                //         }
                //     }
                // }

                // print_r($dataValue);die();

                // for ($i=1; $i < $totalcolom; $i++){
                //     if(!empty($param['wherecol'])){
                //         foreach ($param['wherecol'] as $key => $value) {
                //             if(empty($param['wherecol'][$key+1])){
                //                 $dataexcel[$i-1]['message'] .= $data[$i][$value];
                //             }else{
                //                 $dataexcel[$i-1]['message'] .= $data[$i][$value].' - ';
                //             }
                           
                //         }

                //     }
                //     if(!empty($param['wherecoladd'])){
                //         foreach ($param['wherecoladd'] as $key => $value) {
                //             if(empty($param['wherecoladd'][$key+1])){
                //            $dataexcel[$i-1]['additional'] .= $data[$i][$value];
                //             }else{
                //             $dataexcel[$i-1]['additional'] .= $data[$i][$value].' - ';
                //             }
                //         }
                //     }   
                // } 

                // print_r($dataexcel);die();

                foreach ($newValue as $key => $value) {
                    $count = count($value);

                    for($i=0; $i<$count; $i++){
                        $dataValue[$i][] = $value[$i]; 
                    }
                } 

                foreach ($dataValue as $key => $value) {
                    if(!empty($param['wherecol'])){
                        foreach ($param['wherecol'] as $key2 => $value2) {
                           if(empty($param['wherecol'][$key2+1])){
                                $dataValue[$key]['message'] .= $data[$key+1][$value2];
                            }else{
                                $dataValue[$key]['message'] .= $data[$key+1][$value2].' - ';
                            }
                        }
                    }
                    else{
                         $dataValue[$key]['message'] = null;
                    }
                }

                foreach ($dataValue as $key => $value) {
                    if(!empty($param['wherecoladd'])){
                        foreach ($param['wherecoladd'] as $key2 => $value2) {
                           if(empty($param['wherecoladd'][$key2+1])){
                                $dataValue[$key]['additional'] .= $data[$key+1][$value2];
                            }else{
                                $dataValue[$key]['additional'] .= $data[$key+1][$value2].' - ';
                            }
                        }
                    }
                    else{
                         $dataValue[$key]['additional'] = null;
                    }
                }
            }
            else{

                foreach ($value as $val) {
                    $newValue[] = json_decode($val, 1);
                }

                foreach ($newValue as $key => $value) {
                    $count = count($value);

                    for($i=0; $i<$count; $i++){
                        $dataValue[$i][$label[$key]] = $value[$i]; 
                    }
                }

                foreach ($dataValue as $key => $value) {
                    if(!empty($param['wherecol'])){
                        foreach ($param['wherecol'] as $key2 => $value2) {
                           if(empty($param['wherecol'][$key2+1])){
                                $dataValue[$key]['message'] .= $data[$key+1][$value2];
                            }else{
                                $dataValue[$key]['message'] .= $data[$key+1][$value2].' - ';
                            }
                        }
                    }
                    else{
                         $dataValue[$key]['message'] = null;
                    }
                }

                foreach ($dataValue as $key => $value) {
                    if(!empty($param['wherecoladd'])){
                        foreach ($param['wherecoladd'] as $key2 => $value2) {
                           if(empty($param['wherecoladd'][$key2+1])){
                                $dataValue[$key]['additional'] .= $data[$key+1][$value2];
                            }else{
                                $dataValue[$key]['additional'] .= $data[$key+1][$value2].' - ';
                            }
                        }
                    }
                    else{
                         $dataValue[$key]['additional'] = null;
                    }
                }

                if($type == 'json'){
                    $json_dataValue = json_encode($dataValue);
                }
                else if($type == 'xml'){

                    foreach ($dataValue as $key => $value) {
                        foreach ($value as $key2 => $value2) {
                            if (strpos($key2, ' ') !== false) {
                                $newKey = str_replace(' ', '_', $key2);
                                $newDataValue[$key][$newKey] = $value2;
                                unset($dataValue[$key][$key2]);
                                $merged_dataValue[$key] = array_merge($newDataValue[$key], $dataValue[$key]);
                            }
                        }
                    }

                    if (!empty($merged_dataValue)) {
                        $final_dataValue = $merged_dataValue;
                    }
                    else{
                        $final_dataValue = $dataValue;
                    }

                    // creating object of SimpleXMLElement
                    $xml_data = new SimpleXMLElement('<?xml version="1.0"?><Payroll></Payroll>');

                    // function call to convert array to xml
                    $this->_helper->parser->array_to_xml($final_dataValue,$xml_data);

                    $xml_dataValue = $xml_data->asXML();
                }
            }

            $save = $this->_getParam('finish');

            if($save == 'Save'){

                foreach ($index as $key => $value) {

                    foreach ($data[0] as $key2 => $value2) {
                        
                        if ($key == $key2) {
                            $row = $key;
                        }
                    }


                    $insArr = array("ADP_CUST" => $this->_custIdLogin,
                            "ADP_USER" => $this->_userIdLogin,
                            "ADP_ROW" =>  $row,
                            "ADP_INDEX" => $value
                    );

                    // print_r($insArr);die();
                    $this->_db->insert('M_ADAPTER',$insArr);

                }
                if(!empty($param['wherecoladd'])){
                foreach ($param['wherecoladd'] as $key => $value) {
                    if(empty($param['wherecoladd'][$key+1])){
                        $colmnadd .= $value;
                    }else{
                        $colmnadd .= $value.',';
                    }
                    
                }
                      $insmessageadd = array("ADP_CUST" => $this->_custIdLogin,
                            "ADP_USER" => $this->_userIdLogin,
                            "ADP_ROW" => $colmnadd,
                            "ADP_INDEX" => '3'
                    );
                    $this->_db->insert('M_ADAPTER',$insmessageadd);
                }
              

                if(!empty($param['wherecol'])){
                    foreach ($param['wherecol'] as $key => $value) {
                    if(empty($param['wherecol'][$key+1])){
                        $colmn .= $value;
                    }else{
                        $colmn .= $value.',';
                    }
                    
                }
                    $insmessage = array("ADP_CUST" => $this->_custIdLogin,
                            "ADP_USER" => $this->_userIdLogin,
                            "ADP_ROW" => $colmn,
                            "ADP_INDEX" => '3'
                    );
                    $this->_db->insert('M_ADAPTER',$insmessage);
                }
                $this->_redirect('/notification/success');

            }else if($save == 'Download'){

                // print_r($dataexcel);die();

               try{
                    $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                    if ($type == 'csv') {
                        $result = $this->_helper->download->csv($header,$dataValue,null,$fileName, 1);     
                    }
                    else if($type == 'json'){
                        $result = $this->_helper->download->json($json_dataValue, $fileName);
                    }
                    else if($type == 'xml'){
                        $result = $this->_helper->download->xml($xml_dataValue, $fileName);
                    }
                    else if($type == 'txt'){
                        $result = $this->_helper->download->txt($header,$dataValue,null,$fileName);  
                    }
                     else if($type == 'xls'){
                        $result = $this->_helper->download->xls($header,$dataValue,null,$fileName); 
                    }
                     else if($type == 'xlsx'){
                        $result = $this->_helper->download->xlsx($header,$dataValue,null,$fileName); 
                    }
                
               }catch ( Exception $e ) {
                // print_r($e);die;
            
                }
            }
        }

    }

    public function wherecolumnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
        $data = $sessionNamespace->content;
        // print_r($data);die('here');
        // $tblName = $this->_getParam('id');
//         $payType    = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
//         foreach($payType as $key => $value){ 
// //          if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);
                
//              $optpaytypeRaw[$key] = $this->language->_($value); 
//         }
        

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($data[0] as $key => $row){
            $optHtml.="<option value='".$key."'>".$row."</option>";
        }

        echo $optHtml;
    }
}
