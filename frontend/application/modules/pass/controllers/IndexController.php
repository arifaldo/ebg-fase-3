<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'General/CustomerUser.php';
require_once('Crypt/AESMYSQL.php');

class pass_IndexController extends Application_Main
{
	public function initController()
	{
		$this->_helper->_layout->setLayout('newlayout');
	}

	public function indexAction()
	{
		if (Zend_Auth::getInstance()->hasIdentity()) {
			$this->_redirect('/home/dashboard');
		}
		$cust = $this->getRequest()->getParam('cust_id');
		$user = $this->getRequest()->getParam('user_id');
		$code = urldecode($this->getRequest()->getParam('code'));
		// print_r($user);die;
		// print_r($this->getRequest()->getParams());die;

		$key = md5('permataNet92');
		// $pass = ($zf_filter_input->password);

		// $encrypt = new Crypt_AESMYSQL ();
		$encrypt = new Crypt_AESMYSQL();
		// echo "<pre>";
		// print_r($encrypt);die;
		$decrypcust_id =  ($this->sslDec($cust));

		$decrypuser_id = ($this->sslDec($user));
		// mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $user, MCRYPT_MODE_ECB);
		// $decrypuser_id  =  ( $encrypt->decrypt ( $user) );
		//var_dump($decrypcust_id);
		//var_dump($decrypuser_id);die;
		// echo '<br/>';
		// print_r($cust_id);
		// die;
		// $decrypuser_id = str_replace('\0', '', $this->decrypt($user, "permataNet92"));
		// $decrypcust_id = str_replace('\0', '', $this->decrypt($cust, "permataNet92"));
		// $decrypuser_id = $this->decrypt('�ʹ���', "permataNet92");

		// $decrypcust_id = $this->decrypt($cust, "permataNet92");
		$settings =  new Settings();
		// print_r($user);die;
		$maxLengthPassword = $settings->getSetting('maxfpassword');
		$minLengthPassword = $settings->getSetting('minfpassword');
		$keepMaxPasswordHistory = $settings->getSetting('password_history');
		$this->view->maxlengthpass = $maxLengthPassword;
		$this->view->minlengthpass = $minLengthPassword;

		// $model = new poss_Model_Pass();
		// print_r($decrypcust_id);die;
		$select = $this->_db->select()
			->from(array('A' => 'M_USER'));
		// ->joinLeft(array('G' => 'M_GROUPING'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'));
		$select->where("A.CUST_ID = " . str_replace('\0', '', $this->_db->quote($decrypcust_id)));
		$select->where("A.USER_ID = " . str_replace('\0', '', $this->_db->quote($decrypuser_id)));
		//$select->where("A.USER_CODE = ?",$code);

		$select->where("A.USER_DATEPASS >= ?", date('Y-m-d H:i:s'));
		//echo $select;
		$data = $this->_db->fetchOne($select);

		//print_r($data);die;
		if (!empty($data)) {
			$this->view->accDisplay = false;
			if ($this->_request->isPost()) {
				//customer data
				$filters = array(
					'pass'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'confirm_pass'    => array('StripTags', 'StringTrim', 'HtmlEntities')
				);

				$validators =  array(
					'pass'                => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty'),
						)
					),
					'confirm_pass'                => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty'),
						)
					),
				);



				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
				//validasi multiple email

				$getUserDetail = $this->_db->select()
					->from('M_USER', [
						'USER_ID',
						'USER_FULLNAME'
					])
					->where('USER_ID = ?', $decrypuser_id)
					->where('CUST_ID = ?', $decrypcust_id)
					->query()->fetch();

				if (!$getUserDetail) {
					$getUserDetail = $this->_db->select()
						->from('TEMP_USER', [
							'USER_ID',
							'USER_FULLNAME'
						])
						->where('USER_ID = ?', $decrypuser_id)
						->where('CUST_ID = ?', $decrypcust_id)
						->query()->fetch();
				}

				if (!$zf_filter_input->pass)
					$errDesc['conpass'] = "Error: Confirm New Password cannot be left blank. Please correct it.";
				elseif ($zf_filter_input->pass <> $zf_filter_input->confirm_pass)
					$errDesc['conpass'] = "Error: Sorry, but the two passwords you entered are not same.";
				elseif (!$zf_filter_input->confirm_pass)
					$errDesc['newpass'] = "Error: New Password cannot be left blank. Please correct it.";
				elseif ((strlen($zf_filter_input->pass) < $minLengthPassword) || (strlen($zf_filter_input->pass) > $maxLengthPassword))
					$errDesc['newpass'] = "Error: Minimum char " . $minLengthPassword . " and maximum char " . $maxLengthPassword . " for New Password length. Please correct it.";
				// 		//$errDesc['newpass'] = "Error: Minimum char ".$minLengthPassword." and maximum char ".$maxLengthPassword." for New Password length. Please correct it.";
				// $errDesc['newpass'] = $language->_('Error: Minimum char')." ".$minLengthPassword. " ".$language->_('and maximum char')." ".$maxLengthPassword." ".$language->_('for New Password length. Please correct it').".";
				elseif (Application_Helper_General::checkPasswordStrength($zf_filter_input->pass) < 3)
					$errDesc['newpass'] = "Error: New Password must containt at least one uppercase character, one lowercase character and one number.Please correct it.";
				elseif (Application_Helper_General::checkPasswordSpecialCharacter($zf_filter_input->pass) < 1)
					$errDesc['newpass'] = "Error: Please input one special character!";
				elseif (Application_Helper_General::checkPassContainUserId($zf_filter_input->pass, strtolower($getUserDetail['USER_ID']))) {
					$errDesc['newpass'] = "Kesalahan: Password mengandung id user!";
				} elseif (Application_Helper_General::checkPassContainUserFullName($zf_filter_input->pass, str_replace(' ', '', strtolower($getUserDetail['USER_FULLNAME'])))) {
					$errDesc['newpass'] = "Kesalahan: Password mengandung nama user!";
				} elseif (Application_Helper_General::checkGeneralPassword($zf_filter_input->pass)) {
					$errDesc['newpass'] = "Kesalahan: Password yang digunakan termasuk general password";
				}

				// elseif (! ($zf_filter_input->isValid($zf_filter_input->pass)) ) {
				// 	$errDesc['newpass'] = "Error: New Password may not contain whitespaces or special characters";
				// }
				//var_dump(Application_Helper_General::checkPasswordSpecialCharacter($zf_filter_input->pass));
				//print_r($errDesc);die;

				if ($zf_filter_input->isValid() && empty($errDesc)) {


					//   		$custData = array();
					// foreach($validators as $key=>$value)
					// {
					//    	$custData[strtoupper($key)] = $zf_filter_input->getEscaped($key);	
					// }

					$CustomerUser =  new CustomerUser($decrypcust_id, $decrypuser_id);
					$result = array();
					$failed = 0;

					if (count($errDesc) > 0) $failed = 1;
					$result = $CustomerUser->changePasswordNew(null, $zf_filter_input->pass, $failed);
					// print_r($result);die;
					if ((is_array($result)  && $result !== true) ||  $failed == 1) {
						if (count($result) > 0) {
							foreach ($result as $key => $value) {
								$errDesc[$key] = $value;
							}
						}
						$this->view->error 	= true;
						$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';

						$this->view->errDesc = $errDesc;
					} else {
						// die;
						Application_Helper_General::writeLog('CHMP', 'Change My Password Cust ID : ' . $decrypcust_id . ',User ID : ' . $decrypuser_id);
						// $this->setbackURL('/home');
						$this->_redirect('/pass/index/success');
					}



					// $cust_id = $tempID;
					$toUpload = true;
				} else {
					// die;
					$toUpload = false;
					$this->view->error = 1;

					$this->view->errMsg = 'Error in processing form values. Please correct values and re-submit';

					foreach ($validators as $key => $value) {
						$this->view->$key = ($zf_filter_input->isValid($key)) ? $zf_filter_input->getEscaped($key) : $this->_getParam($key);
					}


					// $error = $zf_filter_input->getMessages();

					// foreach($error as $keyRoot => $rowError)
					// {
					foreach ($errDesc as $key => $value) {
						$keyname = "error_" . $key;
						// print_r($keyname);die;
						$this->view->$keyname = $this->language->_($value);
					}
					// }


				}

				//customer data -- ends




			} else {


				$this->view->accDisplay = false;
				// $this->view->userDisplay = 'none';
				// $this->view->accDisplay = 'none';
			}
		} else {
			// die;
			$this->view->accDisplay = true;
		}
	}

	// function decrypt($encrypted_string, $encryption_key) {
	//     $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	//     $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	//     $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
	//     return $decrypted_string;
	// }

	public function sslPrm()
	{
		return array("6a1f325be4c0492063e83a8cb2cb9ae7", "IV (optional)", "aes-128-cbc");
	}

	public function sslDec($msg)
	{
		list($pass, $iv, $method) = $this->sslPrm();
		return trim(urldecode(openssl_decrypt(urldecode($msg), $method, $pass, false, $iv)));
	}

	function decrypt($encrypted_string)
	{
		$dirty = array("+", "/", "=");
		$clean = array("_PLUS_", "_SLASH_", "_EQUALS_");

		$string = base64_decode(str_replace($clean, $dirty, $encrypted_string));

		$decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, 'permataNet92', $string, MCRYPT_MODE_ECB, 'permataNet92');

		return $decrypted_string;
	}

	public function successAction()
	{
	}
}
