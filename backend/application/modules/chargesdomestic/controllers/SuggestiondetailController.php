<?php

require_once 'Zend/Controller/Action.php';

class Chargesdomestic_SuggestiondetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$changeid = $this->_getParam('changes_id');
  	
	$select = $this->_db->select()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
	$select -> where("A.CHANGES_ID = ?", $changeid);
	$select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
	$result = $select->query()->FetchAll();
	$this->view->result=$result;
	$custid = $result[0]['KEY_FIELD'];
	
 	if(!$result)
	{
		$this->_redirect('/notification/invalid/index');
	}
	
	//Zend_Debug::dump($result);die;
	
	$select2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));	
	$select2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
	$result2 = $this->_db->fetchRow($select2);
	$this->view->result2 = $result2;
			     
///////////////CURRENT////////////////////////////////////////////////////////////////////////		
  	$select3 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_OTHER'),array('*'));
	$select3 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
	$select3 -> where("CHARGES_TYPE = '1'");
	$result3 = $this->_db->fetchRow($select3);
	
	if($result3)
	{
		$this->view->rtgsamtcur = Application_Helper_General::displayMoney($result3['CHARGES_AMT']);
		$this->view->rtgsccycur = $result3['CHARGES_CCY'];
	}
		
	$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_OTHER'),array('*'));
	$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
	$select4 -> where("CHARGES_TYPE = '2'");
	$result4 = $this->_db->fetchRow($select4);
	
  	if($result4)
	{
		$this->view->sknamtcur = Application_Helper_General::displayMoney($result4['CHARGES_AMT']);
		$this->view->sknccycur = $result4['CHARGES_CCY'];
	}
	
	if($result3 && $result4)
	{
		$this->view->enable = true;
	}
////////////////////////////////////////////////////////////////////////////////////////////		

	
///////////////SUGGESTED////////////////////////////////////////////////////////////////////////
  	$select5 = $this->_db->select()->distinct()
			        	->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
	$select5 -> where("CHANGES_ID = ?", $changeid);
	$select5 -> where("CHARGES_TYPE = '1'");
	$result5 = $this->_db->fetchRow($select5);
	//Zend_Debug::dump($result5);die;
	if($result5)
	{
		$this->view->rtgsamtsug = Application_Helper_General::displayMoney($result5['CHARGES_AMT']);
		$this->view->rtgsccysug = $result5['CHARGES_CCY'];
	}
		
	$select6 = $this->_db->select()->distinct()
			        	->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
	$select6 -> where("CHANGES_ID = ?", $changeid);
	$select6 -> where("CHARGES_TYPE = '2'");
	$result6 = $this->_db->fetchRow($select6);
	
  	if($result6)
	{
		$this->view->sknamtsug = Application_Helper_General::displayMoney($result6['CHARGES_AMT']);
		$this->view->sknccysug = $result6['CHARGES_CCY'];
	}
////////////////////////////////////////////////////////////////////////////////////////////////
	
	$this->view->changes_id = $changeid;
    $this->view->typeCode = array_flip($this->_changeType['code']);
    $this->view->typeDesc = $this->_changeType['desc'];
    $this->view->modulename = $this->_request->getModuleName();
	//echo $select2; die;
    if(!$this->_request->isPost()){
    Application_Helper_General::writeLog('CHCL','View Domestic charges changes list page ('.$custid.')');
    }
  }
}