<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'CMD/Validate/Validate.php';

class agent_RepairController extends Application_Main{

  public function indexAction(){
    $this->_helper->layout()->setLayout('popup');
    //$this->view->user_msg  = array();
	$model = new agent_Model_Agent();
  	//$this->_moduleDB = strtoupper($this->_moduleID['cust']);
  	$changes_id = $this->_getParam('changes_id');
  	$changes_id = (Zend_Validate::is($changes_id,'Digits'))? $changes_id : 0;

	// $modelCust = new customer_Model_Customer();
 //    $countryArr = Application_Helper_Array::listArray($modelCust->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');

  	$userid = $this->_getParam('user_id');
    $expid = "USER_ID != ".$this->_db->quote($userid);

    $this->view->user_idd = $userid;

    $this->_userAuth = Zend_Auth::getInstance()->getIdentity();
	$branchCode = $this->_userAuth->branchCode;
	$branchName = $this->_userAuth->branchName;

	$this->view->branchCode = $branchCode;
	$this->view->branchName = $branchName;
		$select = $this->_db->select()
							->from(array('L' => 'M_BUSER'),array('L.BUSER_BRANCH'))
							->joinLeft(array('B' => 'M_BRANCH'),'B.ID = L.BUSER_BRANCH',array('B.*'))
							->where('BUSER_ID = ?', $this->_userIdLogin);
							// ->where('ACCT_STATUS = ?', '1');
							// echo $select;die;
		$branchdata = $this->_db->fetchRow($select);
// print_r($branchdata);die;
	$this->view->branchCode = $branchdata['BRANCH_CODE'];
	$this->view->branchName = $branchdata['BRANCH_NAME'];


	$areaArr = $model->getServiceArea();
	$this->view->areaArr = $areaArr;
	$this->view->accArr = array();

    $this->view->error = 0;

  	//jika change id ada isinya maka true
  	if($changes_id)
  	{
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID','CHANGES_TYPE'))
                             ->where('CHANGES_ID='.$this->_db->quote($changes_id));
      $resultdata = $this->_db->fetchRow($select);

      //jika change id ada di database maka true
      if($resultdata['CHANGES_ID'])
      {
		$this->view->changes_id = $changes_id;
        $result = $model->getTempAgent($changes_id);

      	 if($result['TEMP_ID'])
      	 {
      	   $change_type = strtoupper($resultdata['CHANGES_TYPE']);
      	   $user_id = $result['USER_ID'];
      	 }
      	 else{ $changes_id = 0; }
      }
      else{ $changes_id = 0; }
  	}

  	//jika change id tidak ada isinya maka error
    if(!$changes_id)
    {
      $error_remark = $this->language->_('Changes ID is not found');
      //insert log
	  try
	  {
	  	$this->_db->beginTransaction();
	    if($this->_request->isPost())
	    {
	      $action = strtoupper($this->_actionID['repair']);
	      $fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($_POST);
	    }
	    else
	    {
	      $action = strtoupper($this->_actionID['view']);
	      $fulldesc = null;
	    }
		$this->backendLog($action,strtoupper($this->_moduleID['cust']),null,$fulldesc,$error_remark);
		$this->_db->commit();
	  }
      catch(Exception $e)
      {
 		$this->_db->rollBack();
		SGO_Helper_GeneralLog::technicalLog($e);
	  }

      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      //$this->_redirect($this->_backURL);
      $this->_redirect('/popuperror/index/index');
    }



  	if($this->_request->isPost())
  	{
  	   if($change_type == "N"){
  	   	$filters = array(
  	   					'user_mobile_phone'        => array('StripTags','StringTrim'),
                        'user_id'             => array('StripTags','StringTrim','StringToUpper'),
                        'user_email'     => array('StripTags','StringTrim'),
                        'user_fullname'  => array('StripTags','StringTrim','StringToUpper'),
                        'user_classification'     => array('StripTags','StringTrim','StringToUpper'),
                        'user_service_area'             => array('StripTags','StringTrim','StringToUpper'),
                        'user_service_address'             => array('StripTags','StringTrim','StringToUpper'),
       					'user_address'   => array('StripTags','StringTrim','StringToUpper'),
                        'user_city'         => array('StripTags','StringTrim','StringToUpper'),
                        'user_zip'   => array('StripTags','StringTrim'),
  	   	                'user_province'     => array('StripTags','StringTrim','StringToUpper'),
  	   	                'user_country'     => array('StripTags','StringTrim','StringToUpper'),
  	   	                'user_acct_no'   => array('StripTags','StringTrim'),
  	   	                'user_acct_name'   => array('StripTags','StringTrim','StringToUpper'),
                      );
	   
       	$validators =  array(
       						'user_mobile_phone'	=> array('NotEmpty',
												     'messages' => array($this->language->_('Can not be empty'))
											        ),
                            'user_id'       => array('NotEmpty',
      	                                            array('Db_NoRecordExists',array('table'=>'M_USER_LKP','field'=>'USER_ID')),
      	                                            array('Db_NoRecordExists',array('table'=>'TEMP_USER_LKP','field'=>'USER_ID','exclude'=>$expid)),
                                                    'messages' => array('User ID tidak boleh kosong.',
																		 'User ID telah terdaftar sebagain agen. Mohon gunakan User ID yang lain.',
																		 'User ID telah disarankan sebagai agen. Mohon gunakan User ID yang lain.')
																		  ),
                           'user_fullname'	=> array('NotEmpty',
												     'messages' => array($this->language->_('Can not be empty'))
											        ),
                           'user_service_area'	=> array('allowEmpty' => true,
															array('StringLength',array('max'=>15)),
														   'messages' => array(
																				'Maksimum karakter yang diijinkan adalah 15 karakter'
																			   )
														  ),
                           'user_service_address'	=> array('allowEmpty' => true,
															array('StringLength',array('max'=>200)),
														   'messages' => array(
																				'Maksimum karakter yang diijinkan adalah 200 karakter'
																			   )
														  ),
                 //           'user_acct_no'	=> array('NotEmpty',
												     // 'messages' => array($this->language->_('Can not be empty'))
											      //   ),
                 //           'user_acct_name'	=> array('NotEmpty',
												     // 'messages' => array($this->language->_('Can not be empty'))
											      //   ),
                           'user_email'     => array('NotEmpty',
								'EmailAddress',
								'messages' => array(
									$this->language->_('Can not be empty'),
									'Format email salah'
								)
							),

                           'user_address'	=> array('allowEmpty'=> true),
                           'user_city'	=> array('allowEmpty'=> true),
                           'user_zip'	=> array('allowEmpty'=> true),
                           'user_province'	=> array('allowEmpty'=> true),
                           'user_country'	=> array('allowEmpty'=> true),

                 //           'user_classification' => array('NotEmpty',
												     // 'messages' => array($this->language->_('Can not be empty'))
											      //   ),
					     );
  	   }
  	   else{
  	   	$filters = array(
  	   					'user_service_area' => array('StripTags','StringTrim','StringToUpper'),
                        'user_email'        => array('StripTags','StringTrim'),
                        'user_classification' => array('StripTags','StringTrim','StringToUpper'),
                        'user_service_address'=> array('StripTags','StringTrim','StringToUpper'),
                      );
	   
       	$validators =  array(
                            'user_service_area'	=> array('allowEmpty' => true,
															array('StringLength',array('max'=>15)),
														   'messages' => array(
																				'Maksimum karakter yang diijinkan adalah 15 karakter'
																			   )
														  ),
                           'user_service_address'	=> array('allowEmpty' => true,
															array('StringLength',array('max'=>200)),
														   'messages' => array(
																				'Maksimum karakter yang diijinkan adalah 200 karakter'
																			   )
														  ),
                           'user_email'     => array('NotEmpty',
								'EmailAddress',
								'messages' => array(
									$this->language->_('Can not be empty'),
									'Format email salah'
								)
							),
                 //           'user_classification' => array('NotEmpty',
												     // 'messages' => array($this->language->_('Can not be empty'))
											      //   ),
					     );
  	   }

	  $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);


      if($zf_filter_input->isValid())
	  {

	  	if($change_type == "N"){
	  		$info = 'User ID = '.$zf_filter_input->user_id.', User Name = '.$zf_filter_input->user_fullname;

	      	$user_data = array(
	      									   'USER_ID'	=> null,
											   'USER_BRANCH_CODE' => $branchCode,
											   'USER_STATUS'  => null,
											   'USER_EMAIL'     => null,
											   'USER_MOBILE_PHONE' => null,
											   'USER_SERVICE_AREA'    => null,
											   'USER_SERVICE_ADDRESS' => null,
											   'USER_FULLNAME' => null,
											   'USER_CITY' => null,
											   'USER_PROVINCE' => null,
											   'USER_CLASSIFICATION' => null,
											   'USER_ACCT_NO' => null,
						);

	      	foreach($validators as $key=>$value)
		  	{
				if(array_key_exists(strtoupper($key), $user_data))  $user_data[strtoupper($key)] = $zf_filter_input->$key;
		  	}
		  	
	  	}
	  	else{
	  		$info = 'User ID = '.$zf_filter_input->user_id.', User Name = '.$tempData['USER_FULLNAME'];
			$user_data = array(
							   'USER_SERVICE_ADDRESS' => $zf_filter_input->user_service_address,
							   'USER_EMAIL'  => $zf_filter_input->user_email,
							   'USER_CLASSIFICATION'  => $zf_filter_input->user_classification,
							   'USER_SERVICE_AREA'	=> $zf_filter_input->user_service_area
							);
	  	}

	  	$user_data['USER_SUGGESTED']    = new Zend_Db_Expr('GETDATE()');
		$user_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;

	    
		try
	  	{
		  $this->_db->beginTransaction();

		  //update T_GLOBAL_CHANGES
		  $this->updateGlobalChanges($changes_id,$info,null,null,null,null);
		  //update TEMP_USER
		  $model->updateTempUser($changes_id,$user_data);
		  //log CRUD
		  //Application_Helper_General::writeLog('CSCR','User has been Repaired, User ID : '.$zf_filter_input->user_id. ' User Name : '.$zf_filter_input->user_name.' Change id : '.$changes_id);
          $this->_db->commit();
          $this->_redirect('/popup/successpopup');
	  	}
		catch(Exception $e)
		{
		  $this->_db->rollBack();
		  SGO_Helper_GeneralLog::technicalLog($e);

		  $this->view->error_remark = "Gagal memperbaiki data Agen. Silahkan coba lagi.";
		}

	   	if($this->view->error_remark){
	   		if($change_type == "N"){
				$userid = $zf_filter_input->user_id;
			  	$select = $this->_db->select()
									->from(array('M_USER_ACCT'),array('ACCT_NO','ACCT_NAME'))
									->where('USER_ID = ?', $userid)
									->where('ACCT_STATUS = ?', '1');

				$acctData = $this->_db->fetchAll($select);
				$this->view->accArr = $acctData;
			}

			foreach($validators as $key=>$value)
		  	{
				$this->view->$key = $zf_filter_input->$key;
		  	}
		}

	  }//END if($zf_filter_input->isValid())
	  else
	  {
			$this->view->error = 1;
			foreach($validators as $key=>$value)
			{
				$this->view->$key = ($zf_filter_input->$key) ? $zf_filter_input->$key : $this->_getParam($key);
			}

			$error = $zf_filter_input->getMessages();
			
			foreach($error as $keyRoot => $rowError)
			{
				foreach($rowError as $errorString)
				{
					$keyname = "error_" . $keyRoot;
					$this->view->$keyname = $this->language->_($errorString);
				}
			}
			
			if($change_type == "N"){
				$userid = $this->_getParam('user_id');
			  	$select = $this->_db->select()
									->from(array('M_USER_ACCT'),array('ACCT_NO','ACCT_NAME'))
									->where('USER_ID = ?', $userid)
									->where('ACCT_STATUS = ?', '1');

				$acctData = $this->_db->fetchAll($select);
				$this->view->accArr = $acctData;
			}
		}
    }
    else
    {
        //get data dari temp agent        
		foreach($result as $key =>$value){
			$id = strtolower($key);
			$this->view->$id = $result[$key];
		}

		// $modelAcc = new useraccount_Model_Useraccount();
  //       $accData = $modelAcc->getUserAcct(array('user_id'=>$user_id,'acct_no'=>$result['USER_ACCT_NO']));
  //       $this->view->user_acct_name = $accData['ACCT_NAME'];
		
		$detail = $model->getMasterDetail($user_id);

		$this->view->user_fullname = $result['USER_FULLNAME'];
		$this->view->user_address = $result['USER_SERVICE_ADDRESS'];
		$this->view->user_city = $result['USER_CITY'];
		// $this->view->user_zip = $detail['USER_ZIP'];
		$this->view->user_province = $result['USER_PROVINCE'];
		// $this->view->user_country = $countryArr[$detail['USER_COUNTRY']];
		
		//insert log
		try
		{
		  $this->_db->beginTransaction();
		  //Application_Helper_General::writeLog('CSCR','Viewing Repair Customer');
		  $this->_db->commit();
		}
		catch(Exception $e)
		{
		  $this->_db->rollBack();
		  SGO_Helper_GeneralLog::technicalLog($e);
		}
    }
  }


}



