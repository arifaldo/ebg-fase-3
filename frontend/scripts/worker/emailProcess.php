<?php
//die('here');
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'CMD/Payment.php';
require_once 'General/Settings.php';
	
	
	//$thisClass = $this;
	$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
		
		$datapers = array();
		$logName = 'Service_SMS::sendSMS';
		$pathFile = null;
		$printToDisplay = true;
		$logFileType = "ALL";
		
		
		
	
		$loggerService = SGO_Extendedmodule_Loggerservice_Service::getInstance();
		$logger = $loggerService->getLogger($logName, $pathFile, $printToDisplay, $logFileType);
		//var_dump($logger);
		//var_dump($queueService);die;
		$sms = array();
		$callback = function($sms) use ($logger,$queueService) {
			
			
			
			
			$request = json_decode($sms->body, true);
		 	
			//$Payment = new Payment($request['PS_NUMBER'], $request['CUST_ID'], 'System');
			
			$emailList = $request['emailList'];
			$transactionData = $request['transactionData'];
			$sendTransfer = $request['sendTransfer']; 
			//var_dump($emailList); 
			//var_dump($sendTransfer);
			//var_dump($transactionData);die;
			if (is_array($emailList) && count($emailList) > 0){
			$setting 	= new Settings();
			
		 //ini_set('display_errors', 1);
		 //ini_set('display_startup_errors', 1);
		 //error_reporting(E_ALL);
			
			$template						= $setting->getSetting('femailtemplate_paymentnotification','');
			$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name','');
			$templateEmailMasterBankAppName 	= $setting->getSetting('master_bank_app_name','');
			$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp','');
			$templateEmailMasterBankWapp 	= $setting->getSetting('master_bank_wapp','');
			$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email','');
			$templateEmailMasterBankEmail1 	= $setting->getSetting('master_bank_email1','');
			$date 							= date("d M Y H:i:s");

			$config    		= Zend_Registry::get('config');
			$paymentType 	= $config["payment"]["type"];
			$transferType 	= $config["transfer"]["type"]["code"];
			$transferTypedesc 	= $config["transfer"]["type"]["desc"];
			$paymentType 	= $config["payment"]["type"]["desc"];
			$paymentTypeCode 	= $config["payment"]["type"]["code"];
			$db = Zend_Db_Table::getDefaultAdapter();
	
			//var_dump($transactionData);die;
			//if autopup
			if ($transactionData['0']['PS_NUMBER'] == 'autotopup') {
				$pslip['PS_TYPE'] = $paymentTypeCode['autotopup'];
				$pslip['CUST_NAME'] = $transactionData[0]['DATA']['CUST_NAME'];
				$transactionData['0']['PS_NUMBER'] = empty($transactionData[0]['DATA']['TRANSACTION_ID']) ? '-' : $transactionData[0]['DATA']['TRANSACTION_ID'];
			}
			else{
				$select	= $db->select()
							->from(array('P'	 		=> 'T_PSLIP'),array('*'))
							->where('P.PS_NUMBER = ?',$transactionData['0']['PS_NUMBER']);
				$pslipdata =  $db->fetchAll($select);
				$pslip = $pslipdata[0];
				
				$selectcust	= $db->select()
							->from(array('P'	 		=> 'M_CUSTOMER'),array('*'))
							->where('P.CUST_ID = ?',$pslip['CUST_ID']);
				$custdata =  $db->fetchAll($selectcust);
				$custd = $custdata[0];
				//var_dump($custd);die;
				$pslip['CUST_NAME'] = $custd['CUST_NAME'];
			}
			
			
			$totaldata = count($transactionData);
			$str = '';
			//echo '<pre>';
			
			$payType 	= Application_Helper_General::filterPaymentType($paymentType, $transferType);
			
			$paytypesarr = array_combine(array_values($paymentTypeCode),array_values($paymentType));
			$tra_type = array_combine(array_values($transferType),array_values($transferTypedesc));
		//	$transferType 	= Application_Helper_General::filterPaymentTypeRekon($paymentType, $transferType);

				foreach($payType as $key => $value){
		// 			if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);

					 $optpaytypeRaw[$key] = $value;
				}
				$opttype = array();
				foreach($transferType as $key => $val){
					//var_dump($val);
					$opttype[$val] = $transferTypedesc[$key];
				}
			//var_dump($transferTypedesc);
			//var_dump($opttype);die;
			//$status = ($sendTransfer['ResponseCode'] == '0000')?'Success':'Failed';
			
			if($sendTransfer['ResponseCode'] == '0000' || $sendTransfer['ResponseCode'] == '00' || $sendTransfer->error_code == '0000' || $sendTransfer->error_code == '00' ){
				$status = 'Success';
			}else if($sendTransfer['ResponseCode'] == '9999' || $sendTransfer['ResponseCode'] == '99' || $sendTransfer['ResponseCode'] == NULL){
				$status = 'Suspect';
			}else if($sendTransfer->error_code == '9999' || $sendTransfer->error_code == '99'){
				$status = 'Suspect';
			}else{
				$status = 'Failed';
			}
			if(!empty($transactionData[0]['BENEF_ACCT_BANK_CODE'])){
			$select	= $db->select()
				->from(
					array('TA' => 'M_BANK_TABLE'),
					array(
						'BANK_NAME' 			=> 'TA.BANK_NAME'
					)
				)
				
				->where("TA.BANK_CODE 	= ?" , $transactionData[0]['BENEF_ACCT_BANK_CODE']);

			$beneBank = $db->fetchRow($select);
			$transactionData[0]['BENEFICIARY_BANK_NAME'] = $beneBank['BANK_NAME'];
			}

			if(!empty($transactionData[0]['SOURCE_ACCT_BANK_CODE'])){
			$select	= $db->select()
				->from(
					array('TA' => 'M_BANK_TABLE'),
					array(
						'BANK_NAME' 			=> 'TA.BANK_NAME'
					)
				)
				
				->where("TA.BANK_CODE 	= ?" , $transactionData[0]['SOURCE_ACCT_BANK_CODE']);

			$sourceBank = $db->fetchRow($select);
			$transactionData[0]['SOURCE_BANK_NAME'] = $sourceBank['BANK_NAME'];
			
			}
			//echo '<pre>';
			//var_dump($pslip);die;
			
			if($totaldata == 1){
			
					$str .= '<table style="margin:15px 0;color:rgba(0,0,0,0.70);border-collapse:collapse" width="100%">';
				
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Tanggal</td>';
					$datenow = date('Y-m-d');
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.Application_Helper_General::convertDate($datenow, 'dd MMM yyyy').'</td>';
					$str.= '</tr>';
					if($pslip['PS_TYPE'] == '1'){
						
					$tratype = array('0' => 'PB','1' => 'RTGS','2' => 'SKN','7' => 'PB');
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jenis Transaksi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$tratype[$transactionData[0]['TRANSFER_TYPE']].'</td>';
					$str.= '</tr>';
					}else if($pslip['PS_TYPE'] == '2'){
						
					$tratype = array('0' => 'PB','1' => 'RTGS','2' => 'SKN','5' => 'ONLINE');
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jenis Transaksi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$tratype[$transactionData[0]['TRANSFER_TYPE']].'</td>';
					$str.= '</tr>';
					}else if($pslip['PS_TYPE'] == '16' || $pslip['PS_TYPE'] == '17'){

					}else{
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jenis Transaksi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$paytypesarr[$pslip['PS_TYPE']].' - '.$tra_type[$transactionData[0]['TRANSFER_TYPE']].'</td>';
					$str.= '</tr>';	
					}
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">No. Transaksi </td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['PS_NUMBER'].'</td>';
					$str.= '</tr>';
					
			if($pslip['PS_TYPE'] == '1' && $transactionData[0]['TRANSFER_TYPE'] == '0'){
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].' / '.$transactionData[0]['SOURCE_BANK_NAME'].' / '.$transactionData[0]['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].' / '.$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TRA_AMOUNT']).'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
					$str.= '</tr>';
			}else if($pslip['PS_TYPE'] == '1' && $transactionData[0]['TRANSFER_TYPE'] == '7'){
					$str.= '<tr>'; 
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].' / '.$transactionData[0]['SOURCE_BANK_NAME'].' / '.$transactionData[0]['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].' / '.$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
					if($transactionData['0']['BENEFICIARY_ACCOUNT_CCY'] != 'IDR'){
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT_CCY'].' '.$transactionData[0]['TRA_AMOUNT'].'</td>';
					}else{
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($transactionData[0]['TRA_AMOUNT']).'</td>';
					}
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
					$str.= '</tr>';
					
			}else if($pslip['PS_TYPE'] == '1' && $transactionData[0]['TRANSFER_TYPE'] == '5'){		
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].' / '.$transactionData[0]['SOURCE_BANK_NAME'].' / '.$transactionData[0]['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].' / '.$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
				
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nominal</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TRA_AMOUNT']).'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Biaya Admin</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TOTAL_CHARGES']).'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
					$total = $transactionData[0]['TOTAL_CHARGES']+$transactionData[0]['TRA_AMOUNT']; 
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($total).'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
					$str.= '</tr>';
			}else if($pslip['PS_TYPE'] == '1' && $transactionData[0]['TRANSFER_TYPE'] == '5'){		
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].' / '.$transactionData[0]['SOURCE_BANK_NAME'].' / '.$transactionData[0]['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].' / '.$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
				
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_BANK_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nominal</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TRA_AMOUNT']).'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Biaya Admin</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TOTAL_CHARGES']).'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
					$total = $transactionData[0]['TOTAL_CHARGES']+$transactionData[0]['TRA_AMOUNT']; 
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($total).'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
					$str.= '</tr>';
			}else if($pslip['PS_TYPE'] == '1' && $transactionData[0]['TRANSFER_TYPE'] == '6'){
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">No. Virtual Account</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nama</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
					$total = $transactionData[0]['TOTAL_CHARGES']+$transactionData[0]['TRA_AMOUNT'];
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($total).'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
					$str.= '</tr>';
					
			}else if($pslip['PS_TYPE'] == '16' || $pslip['PS_TYPE'] == '17'){
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].' / '.$transactionData[0]['SOURCE_BANK_NAME'].' / '.$transactionData[0]['SOURCE_ACCOUNT_NAME'].'</td>';
					$str.= '</tr>';
					
					$select	= $db->select()
							->from(array('P'	 		=> 'T_PSLIP_DETAIL'),array('*'))
							->where('P.PS_NUMBER = ?',$transactionData['0']['PS_NUMBER']);
					$pslipdetail =  $db->fetchAll($select);
					 
					if(!empty($pslipdetail)){
						foreach($pslipdetail as $val){

								$str.= '<tr>';
								$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">'.$val['PS_FIELDNAME'].'</td>';
								$ccy = '';
								if($val['PS_FIELDTYPE']=='2'){
									$ccy = 'IDR';
									$val['PS_FIELDVALUE'] = round($val['PS_FIELDVALUE'], 0);
								}
								$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$ccy.' '.$val['PS_FIELDVALUE'].'</td>';
								$str.= '</tr>';
						}
					}
					
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
					$str.= '</tr>';
					
			}

			//if autotopup
			else if($pslip['PS_TYPE'] == $paymentTypeCode['autotopup']){

					$autotopupData = $transactionData[0]['DATA'];

					//if exceed warn level
					if($autotopupData['STATUS'] == '6' || $autotopupData['STATUS'] == '7'){

						$template	= $setting->getSetting('femailtemplate_topupthresholdnotification','');
					}
					else if($autotopupData['STATUS'] == '3'){
						
						$template	= $setting->getSetting('femailtemplate_topupfailednotification','');
					}
					else{

						if ($autotopupData['STATUS'] == 2) {
							$status = 'Success';
							$color = 'green';
						}
						else if ($autotopupData['STATUS'] == 4) {
							$status = 'Failed';
							$color = 'red';
						}
						else if ($autotopupData['STATUS'] == 5) {
							$status = 'Suspect';
							$color = 'red';
						}

						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$autotopupData['SOURCE_ACCOUNT'].' / '.$autotopupData['ACCOUNT_NAME'].'</td>';
						$str.= '</tr>';

						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$autotopupData['BANK_NAME'].'</td>';
						$str.= '</tr>';
						
						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Kode Maskapai</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$autotopupData['AIRLINE_CODE'].' - '.$autotopupData['AIRLINE_DATA']['AIRLINE_NAME'].'</td>';
						$str.= '</tr>';

						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Travel Agent ID</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$autotopupData['TRAVELAGENT_ID'].'</td>';
						$str.= '</tr>';

						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($autotopupData['TOPUP_AMOUNT']).'</td>';
						$str.= '</tr>';

						$str.= '<tr>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold;color:'.$color.';" width="60%">'.$status.'</td>';
						$str.= '</tr>';
					}
			}else if($pslip['PS_TYPE'] == '3'){
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].' / '.$transactionData[0]['SOURCE_BANK_NAME'].' / '.$transactionData[0]['SOURCE_ACCOUNT_NAME'].'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].' / '.$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'].'</td>';
				$str.= '</tr>'; 
			
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_BANK_NAME'].'</td>';
				$str.= '</tr>';
				
				if($transactionData[0]['TRANSFER_TYPE'] == '4'){
					$jenistrx = 'SHA';
				}else{
					$jenistrx = 'OUR';
				}
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jenis Biaya</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$jenistrx.'</td>';
				$str.= '</tr>';
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nominal</td>';
				if($chargeTrfCcy != 'IDR'){
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT_CCY'].' '.$transactionData[0]['TRA_AMOUNT'].'</td>';
				}else{
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($transactionData[0]['TRA_AMOUNT']).'</td>';	
				}
				$str.= '</tr>';
				
									$chargeTrf = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 3)
												->where("CHARGE_CCY = ?", $transactionData[0]['SOURCE_ACCOUNT_CCY'])
												->where("CUST_ID = ?", (string)$pslip['CUST_ID'])
											);

									if(empty($chargeTrf)){
										$chargeTrf = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 3)
												->where("CHARGE_CCY = ?", $transactionData[0]['SOURCE_ACCOUNT_CCY'])
												->where("CUST_ID = ?", 'GLOBAL')
											);
									}

									//$chargeTrfAmt = $chargeTrf["CHARGE_AMT"];
									$chargeTrfCcy = $chargeTrf["CHARGE_AMOUNT_CCY"];
									$chargeFA = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 4)
												->where("CHARGE_CCY = ?", $transactionData[0]['BENEFICIARY_ACCOUNT_CCY'])
												->where("CUST_ID = ?", (string)$pslip['CUST_ID'])
											);
									
									if(empty($chargeFA)){
										$chargeFA = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 4)
												->where("CHARGE_CCY = ?", $transactionData[0]['BENEFICIARY_ACCOUNT_CCY'])
												->where("CUST_ID = ?", 'GLOBAL')
											);
									}
									
									
									$chargeFACcy = $chargeFA["CHARGE_AMOUNT_CCY"];
									
									$chargeProv = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 5)
												->where("CHARGE_CCY = ?", $transactionData[0]['SOURCE_ACCOUNT_CCY'])
												->where("CUST_ID = ?", (string)$pslip['CUST_ID'])
											);

									if(empty($chargeProv)){
											$chargeProv = $db->fetchRow(
												$db->select()
												->from(array('C' => 'M_CHARGES_REMITTANCE'))
												->where("CHARGE_TYPE = ?", 5)
												->where("CHARGE_CCY = ?", $transactionData[0]['SOURCE_ACCOUNT_CCY'])
												->where("CUST_ID = ?", 'GLOBAL')
											);
										}

									$chargeProvMinAmt = $chargeProv["CHARGE_PROV_MIN_AMT"];
									$chargeProvMaxAmt = $chargeProv["CHARGE_PROV_MAX_AMT"];
									$chargeProvPct = $chargeProv["CHARGE_PCT"];
									$chargeProvCcy = $chargeFA["CHARGE_AMOUNT_CCY"];
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Biaya Admin</td>';
				if($chargeTrfCcy != 'IDR'){
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$chargeTrfCcy.' '.$transactionData[0]['TRANSFER_FEE'].'</td>';
				}else{
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$chargeTrfCcy.' '.number_format($transactionData[0]['TRANSFER_FEE']).'</td>';
				}
				
				$str.= '</tr>';
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Biaya Full Amount</td>';
				if($chargeFACcy != 'IDR'){
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$chargeFACcy.' '.$transactionData[0]['FULL_AMOUNT_FEE'].'</td>';	
				}else{
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$chargeFACcy.' '.number_format($transactionData[0]['FULL_AMOUNT_FEE']).'</td>';
				}
				
				$str.= '</tr>';
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Biaya Provisi</td>';
				if($chargeFACcy != 'IDR'){
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$chargeProvCcy.' '.$transactionData[0]['PROVISION_FEE'].'</td>';
				}else{
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$chargeProvCcy.' '.number_format($transactionData[0]['PROVISION_FEE']).'</td>';
				}
				
				$str.= '</tr>';
				
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
				
				if($transactionData[0]['BENEFICIARY_ACCOUNT_CCY'] == 'USD' && $transactionData[0]['SOURCE_ACCOUNT_CCY'] == 'USD'){
					
						$total = $transactionData[0]['EQUIVALENT_AMOUNT_USD'];
						//var_dump($total);die('here');
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT_CCY'].' '.$total.'</td>';
				}else if($transactionData[0]['BENEFICIARY_ACCOUNT_CCY'] != 'IDR'){
						$total = $transactionData[0]['EQUIVALENT_AMOUNT_USD'];
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT_CCY'].' '.$total.' ( IDR '.number_format($transactionData[0]['EQUIVALENT_AMOUNT_IDR']).' )</td>';
				}else{
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT_CCY'].' '.number_format($total).'</td>';
				}
				
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
				$str.= '</tr>';
			}else if($pslip['PS_TYPE'] == '21'){
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
				if(empty($transactionData[0]['SOURCE_ACCOUNT_NAME'])){
						$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].'</td>';
				}else{
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].' / '.$transactionData[0]['SOURCE_BANK_NAME'].' / '.$transactionData[0]['SOURCE_ACCOUNT_NAME'].'</td>';
				}
				
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
				if(empty($transactionData[0]['BENEFICIARY_ACCOUNT_NAME'])){
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].'</td>';
				}else{
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].' / '.$transactionData[0]['BENEFICIARY_BANK_NAME'].' / '.$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'].'</td>';
				}
				$str.= '</tr>'; 
			
				// $str.= '<tr>';
				// $str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
				// $str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_BANK_NAME'].'</td>';
				// $str.= '</tr>';

				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nominal</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TRA_AMOUNT']).'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Biaya Admin</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TOTAL_CHARGES']).'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
				$total = $transactionData[0]['TOTAL_CHARGES']+$transactionData[0]['TRA_AMOUNT'];
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($total).'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
				$str.= '</tr>';
			}
			else{
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['SOURCE_ACCOUNT'].' / '.$transactionData[0]['SOURCE_BANK_NAME'].' / '.$transactionData[0]['SOURCE_ACCOUNT_NAME'].'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_ACCOUNT'].' / '.$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'].'</td>';
				$str.= '</tr>'; 
			
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Bank</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$transactionData[0]['BENEFICIARY_BANK_NAME'].'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Nominal</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TRA_AMOUNT']).'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Biaya Admin</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($transactionData[0]['TOTAL_CHARGES']).'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
				$total = $transactionData[0]['TOTAL_CHARGES']+$transactionData[0]['TRA_AMOUNT'];
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($total).'</td>';
				$str.= '</tr>';
				$str.= '<tr>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Status</td>';
				$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$status.'</td>';
				$str.= '</tr>';
			}
				
			$str.= '</table>';	
				
				
			}else{
				$config    = Zend_Registry::get('config');
				$caseBeneBank = "CASE T.TRANSFER_TYPE WHEN '0' THEN '".$config['app']['bankname']."' 
										ELSE T.BENEFICIARY_BANK_NAME END";
				$caseTransferType = "CASE T.TRANSFER_TYPE
																								WHEN '0' THEN 'In House' 
																								WHEN '1' THEN 'RTGS'
																								WHEN '2' THEN 'SKN' END";
				$casePayType = "(CASE P.PS_TYPE ";
				
				$caseArr 		 	= Application_Helper_General::casePaymentType($paymentType, $transferType);
				//foreach($optpaytypeRawNew['PS_TYPE'] as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
					$casePayType .= $caseArr['PS_TYPE'];
				$casePayType .= " END";
				//var_dump($caseArr);
				//echo $casePayType;die;
				
				
				$config    		= Zend_Registry::get('config');
			$paymentType 	= $config["payment"]["type"]["desc"];
			$paymentTypeCode 	= $config["payment"]["type"]["code"];
		//$transferType 	= $config["transfer"]["type"]["code"];
		//$transferTypedesc 	= $config["transfer"]["type"]["desc"];
			$paytypesarr = array_combine(array_values($paymentTypeCode),array_values($paymentType));
			//$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
		//var_dump($paytypesarr);die;
    	$casePayType = "(CASE P.PS_TYPE ";
			foreach($paytypesarr as $key=>$val)
			{
				$casePayType .= " WHEN ".$key." THEN '".$val."'";
			}
				$casePayType .= " WHEN '110' OR '120' THEN PS_CATEGORY";
				$casePayType .= " ELSE 'N/A' END)";
				
				
				$select2 = $db->select()//->distinct()
								->from(array('T' => 'T_TRANSACTION'),array('TRANSACTION_ID'))
								->joinleft(array('P' => 'T_PSLIP'), 'T.PS_NUMBER = P.PS_NUMBER',array('PS_NUMBER'))
								->joinLeft(array('E' => 'T_EMAIL_NOTIFICATION_TRX'), 'T.TRANSACTION_ID = E.TRANSACTION_ID', array(	'T.*',
																																	'P.*',
																																	'benebank' => new Zend_Db_Expr($caseBeneBank),
																																	'transferType' => new Zend_Db_Expr($caseTransferType),
																																	'paymenttype' => new Zend_Db_Expr($casePayType),
																																	'E.PS_ISBENEEMAILED', 
																																	'E.PS_ISSOURCEEMAILED',) )
								->where('P.PS_STATUS = 5')
								//->where('T.TRA_STATUS = 3')
								->where('P.PS_NUMBER = ?',$transactionData['0']['PS_NUMBER'])
								//->where('A.SOURCE_ACCOUNT LIKE '.$db->quote($result['SOURCE_ACCOUNT']))
								//->where("E.PS_ISSOURCEEMAILED NOT LIKE '1' OR E.PS_ISSOURCEEMAILED IS NULL")
								//->where("DATE(B.PS_EFDATE) <= DATE(ADDDATE(now(), 1))")
								->query() ->fetchAll();
									//var_dump(count($select2));
		 			//Zend_Debug::dump($select2);
		 		//die($select2);			
				$sourcename = $result['SOURCE_ACCOUNT_NAME'];
				$sourcealname = $result['SOURCE_ACCOUNT_ALIAS_NAME'];
				$sourceccy = $result['SOURCE_ACCOUNT_CCY'];
				$sourceacc = $result['SOURCE_ACCOUNT'];
				
				$totaltrx = count($select2);
				$totals = 0;
				$totalf = 0;
				$totalp = 0;
				foreach($select2 as $key=>$val){
					if($val['TRA_STATUS'] == '3'){
						$totals++;
					}else if($val['TRA_STATUS'] == '4'){
						$totalf++;
					}else{
						$totalp++;
					}
				}
				
				$str .= '<table style="margin:15px 0;color:rgba(0,0,0,0.70);border-collapse:collapse" width="100%">';
					
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">No. Transaksi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$select2['0']['PS_NUMBER'].'</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jenis Transaksi</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$paytypesarr[$pslip['PS_TYPE']].'</td>';
					$str.= '</tr>';	
					if($select2['0']['PS_TYPE'] == '4' || $select2['0']['PS_TYPE'] == '11'){
							$str.= '<tr>';
							$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Dari Rekening</td>';
							$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$select2['0']['SOURCE_ACCOUNT'].' - '.$select2['0']['SOURCE_ACCOUNT_NAME'].'</td>';
							$str.= '</tr>';
					}else if($select2['0']['PS_TYPE'] == '5' || $select2['0']['PS_TYPE'] == '18' || $select2['0']['PS_TYPE'] == '19'){
							$str.= '<tr>';
							$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Rekening Tujuan</td>';
							$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$select2['0']['BENEFICIARY_ACCOUNT'].' - '.$select2['0']['BENEFICIARY_ACCOUNT_NAME'].'</td>';
							$str.= '</tr>';
					}
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Total Transaksi	</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">'.$totaltrx.' Transaksi('.$totals.' Sukses,'.$totalf.' Gagal,'.$totalp.' Pending)</td>';
					$str.= '</tr>';
					$str.= '<tr>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0" width="40%">Jumlah Transfer</td>';
					$str .= '<td style="border-top:1px solid rgba(0,0,0,0.12);border-bottom:1px solid rgba(0,0,0,0.12);padding:10px 0;font-weight:bold" width="60%">IDR '.number_format($select2['0']['PS_TOTAL_AMOUNT']).'</td>';
					$str.= '</tr>';
				$str.= '</table>';
					
				
			/*	$str .= '<table style="margin:15px 0;color:rgba(0,0,0,0.70);border-collapse:collapse" width="100%">';
				$str .= '<tr>
						<th>No</th>
						<th>No Referensi</th>
						<th>No Transaksi</th>
						<th>Subjek</th>
						<th>Rekening Penerima</th>
						<th>Jumlah</th>
						<th>Deskripsi</th>
						<th>Tanggal Pembayaran</th>
						<th>Bank Tujuan</th>
						<th>Tipe Transfer</th>
					</tr>';
				$content = '';
				$counter = 0;
				foreach($select2 as $result2)
				{
					$beneficiary = $result2['BENEFICIARY_ACCOUNT'].' ('.$result2['BENEFICIARY_ACCOUNT_CCY'].') '.$result2['BENEFICIARY_ACCOUNT_NAME'].' / '.$result2['BENEFICIARY_ALIAS_NAME'];
					$amount = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.Application_Helper_General::displayMoney($result2['TRA_AMOUNT']);
					$content .= '
					<tr>
						<td><font>'.++$counter.'</font></td>
						<td><font>'.$result2['PS_NUMBER'].'</font></td>
						<td><font>'.$result2['TRANSACTION_ID'].'</font></td>
						<td><font>'.$result2['PS_SUBJECT'].'</font></td>
						<td><font>'.$beneficiary.'</font></td>
						<td><font>'.$amount.'</font></td>
						<td><font>'.$result2['TRA_MESSAGE'].'</font></td>
						<td><font>'.Application_Helper_General::convertDate($result2['PS_EFDATE'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
						<td><font>'.$result2['benebank'].'</font></td>
						<td><font>'.$result2['paymenttype'].'</font></td>
						';	
				}
				$str .= $content;
				*/
					
			}
			
			$translate = array(
				'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
				'[[master_bank_app_name]]' 			=> $templateEmailMasterBankAppName,
				'[[MASTER_BANK_TELP]]' 			=> $templateEmailMasterBankTelp,
				'[[MASTER_BANK_EMAIL]]' 		=> $templateEmailMasterBankEmail,
				'[[MASTER_BANK_WAPP]]' 			=> $templateEmailMasterBankWapp,
				'[[MASTER_BANK_EMAIL1]]' 		=> $templateEmailMasterBankEmail1,
				'[[PS_NUMBER]]' 				=> $transactionData[0]['PS_NUMBER'],
				'[[BENEFICIARY_ACCOUNT]]' 		=> $transactionData[0]['BENEFICIARY_ACCOUNT'],
				'[[BENEFICIARY_ACCOUNT_NAME]]' 	=> $transactionData[0]['BENEFICIARY_ACCOUNT_NAME'],
				'[[comp_name]]'					=> $pslip['CUST_NAME'],
				'[[transaction_list]]' 			=> $str,
				'[[DATE]]' 						=> $date,
				'[[MESSAGE]]' 					=> $transactionData[0]['TRA_MESSAGE'],
				'[[TRA_AMOUNT]]' 				=> $transactionData[0]['SOURCE_ACCOUNT_CCY'].' '.number_format($transactionData[0]['TRA_AMOUNT']),
				//'[[STATUS_EMAIL]]' 				=> $status,
				'[[STATUS]]' 					=> $status,
				'[[TRANSFER]]' 					=> 'Transfer',
			);

			$subject = 'Payment Notification';

			if($pslip['PS_TYPE'] == $paymentTypeCode['autotopup']){

				$autotopupData = $transactionData[0]['DATA'];

				//if exceed warn level
				if($autotopupData['STATUS'] == '6' || $autotopupData['STATUS'] == '7'){

					$subject = 'WARNING SALDO AIRLINES';

					$translate['[[AIRLINE_NAME]]'] = $autotopupData['AIRLINE_DATA']['AIRLINE_NAME'];
					$translate['[[TRAVELAGENT_ID]]'] = $autotopupData['TRAVELAGENT_ID'];
					$translate['[[comp_name]]'] = $autotopupData['CUST_NAME'];

					if($autotopupData['STATUS'] == '6'){
						$translate['[[WARN_LEVEL]]'] = 'warning level 1 (IDR '.number_format($autotopupData['AIRLINE_DATA']['WARN_LEVEL1']).')';
					}
					else if($autotopupData['STATUS'] == '7'){
						$translate['[[WARN_LEVEL]]'] = 'warning level 2 (IDR '.number_format($autotopupData['AIRLINE_DATA']['WARN_LEVEL2']).')';
					}

					$translate['[[AMOUNT]]'] = '<b><u>'.number_format($autotopupData['CURRENT_BALANCE']).'.</u></b>';
				}	
				else if($autotopupData['STATUS'] == '3'){
					$subject = 'Failed check saldo airlines';

					$translate['[[AIRLINE_NAME]]'] = $autotopupData['AIRLINE_DATA']['AIRLINE_NAME'];
					$translate['[[TRAVELAGENT_ID]]'] = $autotopupData['TRAVELAGENT_ID'];
					$translate['[[comp_name]]'] = $autotopupData['CUST_NAME'];
				}
			}

			$mailContent = strtr($template,$translate); 
			//var_dump($emailList);die;
			foreach ($emailList as $data){
				//var_dump($subject);
				//var_dump($mailContent);
				//var_dump($data['USER_EMAIL']);die;
				//echo $mailContent;die;
				Application_Helper_Email::sendEmail($data['USER_EMAIL'],$subject,$mailContent);
			}
		}

	                
			
		};
		//$queueConsumer = '';
		//var_dump($queueConsumer);
		$queueConsumer = $queueService->getQueueByProfileName("QUEUE_EMAIL");
		
		try {
			$queueConsumer->waitAndProcess($callback);
		} catch (Exception $e) {
			$param = [];
			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e->getMessage(),NULL,FALSE),$param);
			SGO_Helper_Supervisor::doExitByExitCodeName("RESTART");
		}

?>
