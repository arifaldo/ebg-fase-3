<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

//NOTE:
//Watch the modulename, filename and classname carefully
class Requesttoken_IndexController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{
	    if (Zend_Registry::isRegistered('language')){
			$language = Zend_Registry::get('language');
		}
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$cust_id = $this->_custIdLogin;
		$user_id = $this->_userIdLogin;
		$this->view->cust_id  	= $cust_id;
		$this->view->user_id 	= $user_id;
		
		$settings =  new Settings();
				
		$maxLengthPassword = $settings->getSetting('maxfpassword');
		$minLengthPassword = $settings->getSetting('minfpassword');
		$keepMaxPasswordHistory = $settings->getSetting('password_history');
		$this->view->maxlengthpass = $maxLengthPassword;
		$this->view->minlengthpass = $minLengthPassword;
		
		if(is_string($cust_id) && is_string($user_id))
		{
			if($this->_request->isPost())
			{
				
				$filters = array('old_password'     => array('StripTags','StringTrim'),
											 'new_password'     => array('StripTags','StringTrim'),
											 'confirm_password' => array('StripTags','StringTrim'),
										);

				$zf_filter_input = new Zend_Filter_Input($filters,null,$this->_request->getPost());

				$oldpass 										 = $zf_filter_input->old_password;
				$newpass									 = $zf_filter_input->new_password;
				$CON_NEWUSER_PASSWORD		 = $zf_filter_input->confirm_password;				
				
				$errDesc = array();
				/*if (!$oldpass)
					$errDesc['oldpass'] = "Error: Current Password cannot be left blank. Please correct it.";

				if (!$CON_NEWUSER_PASSWORD)
					$errDesc['conpass'] = "Error: Confirm New Password cannot be left blank. Please correct it.";				
				elseif ($newpass <> $CON_NEWUSER_PASSWORD)
					$errDesc['conpass'] = "Error: Sorry, but the two passwords you entered are not same.";  
				
				$validator = new Zend_Validate_Alnum();
				
				if (!$newpass)
					$errDesc['newpass'] = "Error: New Password cannot be left blank. Please correct it.";
				elseif ((strlen($newpass) < $minLengthPassword) || (strlen($newpass) > $maxLengthPassword))
					//$errDesc['newpass'] = "Error: Minimum char ".$minLengthPassword." and maximum char ".$maxLengthPassword." for New Password length. Please correct it.";
					//$errDesc['newpass'] = "Error: Minimum char ".$minLengthPassword." and maximum char ".$maxLengthPassword." for New Password length. Please correct it.";
					$errDesc['newpass'] = $language->_('Error: Minimum char')." ".$minLengthPassword. " ".$language->_('and maximum char')." ".$maxLengthPassword." ".$language->_('for New Password length. Please correct it').".";
				elseif (Application_Helper_General::checkPasswordStrength($newpass) < 3)
					$errDesc['newpass'] = "Error: New Password must containt at least one uppercase character, one lowercase character and one number.Please correct it.";													
				elseif ($newpass == $oldpass){
					$errDesc['newpass'] = "Error: Password must be different from old password";
				}elseif (! ($validator->isValid($newpass)) ) {
					$errDesc['newpass'] = "Error: New Password may not contain whitespaces or special characters";
				}*/

				if(!empty($newpass)) //if(!empty($oldpass) && !empty($newpass))
				{
					/*$CustomerUser =  new CustomerUser($cust_id,$user_id);
					$result = array();
					$failed = 0;
					if(count($errDesc)>0) $failed = 1;
					$result = $CustomerUser->changePassword($oldpass,$newpass,$failed);

					if( (is_array($result)  && $result !== true) ||  $failed == 1 )
					{
						if(count($result)>0)
						{
							foreach($result as $key=>$value)	
							{						
								$errDesc[$key] = $value;							
							}
						}
							$this->view->error 	= true;
							$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';

							$this->view->errDesc = $errDesc;
					}
					else{
							Application_Helper_General::writeLog('CHMP','Change My Password Cust ID : '.$cust_id.',User ID : '.$user_id);
							$this->setbackURL('/home');
							$this->_redirect('/notification/success/index');
					}*/
					$this->setbackURL('/home');
					$this->_redirect('/notification/success/index');
				}
				else
				{							
					$this->view->error 	= true;
					$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';
					$this->view->errDesc = $errDesc;					
				}	
			}
		}
		Application_Helper_General::writeLog('CHMP','Viewing Change My Password');
	}	
}

