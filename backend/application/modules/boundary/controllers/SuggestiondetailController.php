<?php

require_once 'Zend/Controller/Action.php';

class boundary_SuggestiondetailController extends customer_Model_Customer
{
  	protected $_moduleDB = 'CST';

	public function indexAction()
  	{
  		$this->_helper->layout()->setLayout('popup');
  		
      $changesid = strip_tags( trim($this->_getParam('changes_id')) );
  		$params = $this->_request->getParams();
  		$select = $this->_db->fetchRow("SELECT CHANGES_STATUS, CREATED, CREATED_BY, KEY_VALUE, COMPANY_CODE, COMPANY_NAME FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = ".$this->_db->quote($changesid)." AND (CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR')");
                
    if($select)
    {     
      $this->view->changes_id = $changesid;
      $this->view->created = $select['CREATED'];
      $this->view->createdby = $select['CREATED_BY'];
      $this->view->cust_id = $cust_id = $select['COMPANY_CODE'];
      $this->view->cust_name = $select['COMPANY_NAME'];
      // foreach($options as $codestat=>$descstat)
      // {
      //   if($codestat == $select['CHANGES_STATUS'])
      //   {
      //     $this->view->status = $descstat;
      //   }
      // }
      
//      DATA BARU       
      $selectnew = $this->_db->select()
          ->FROM(array('B' => 'TEMP_APP_GROUP_USER')) 
                    ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))  
                    ->where('CHANGES_ID = ?',$changesid)      
          ->query()->FetchAll();
      // print_r($selectnew);die;
      $newAppGroupArray = array();
      $listgroup = array();
      foreach($selectnew as $row)
      {
        $newAppGroupArray[ $row['GROUP_NAME'] ][] = $row['USER_ID'];
        $listgroup[ $row['GROUP_NAME'] ] = $row['GROUP_USER_ID'];
      }
      $this->view->databarugroup = $listgroup;
      $this->view->databaru = $newAppGroupArray;
      // echo '<pre>';
      // print_r($newAppGroupArray);die;
//      DATA LAMA     
      $selectold = $this->_db->select()
          ->from(array('A' => 'M_APP_GROUP_USER'))
                    ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
          ->query()->FetchAll();        
      
      $oldAppGroupArray = array();
      $oldgroup = array();
      foreach($selectold as $row)
      {
        $oldAppGroupArray[ $row['GROUP_NAME'] ][] = $row['USER_ID'];
        $oldgroup[ $row['GROUP_NAME'] ] = $row['GROUP_USER_ID'];
      }
      $this->view->datalamagroup = $oldgroup;  
      $this->view->datalama = $oldAppGroupArray;  

      $paytypeall   = $optpaytypeAll = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
    $this->view->paymenttype    = $paytypeall;
//    print_r($paytypeall);die;
      //matrix

      $suggested = $this->_db->select()
                  ->from(array('A' => 'TEMP_APP_BOUNDARY'),array('CCY_BOUNDARY','BOUNDARY_MIN','BOUNDARY_MAX','ROW_INDEX','CUST_ID','POLICY','TRANSFER_TYPE'));
      $suggested -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changesid));
      $suggested -> where("A.BOUNDARY_ISUSED LIKE '1'");
      $rsuggested = $suggested->query()->FetchAll();

      $newSuggestArray = array();
      foreach($rsuggested as $row)
      {
        $newSuggestArray[ $row['TRANSFER_TYPE'] ][] = $row['BOUNDARY_MAX'];
      }
      $this->view->newSuggest = $newSuggestArray;
      // echo $suggested;die;
      // print_r($rsuggested);die;
      $this->view->suglist = $rsuggested;

      $suggested = $this->_db->select()
                  ->from(array('A' => 'TEMP_APP_BOUNDARY'),array('CCY_BOUNDARY','TRANSFER_TYPE'));
      $suggested -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changesid));
      $suggested -> where("A.BOUNDARY_ISUSED LIKE '1'");
      $suggested->group('A.CCY_BOUNDARY');
      $suggested->group('A.TRANSFER_TYPE');
      // echo $suggested;die;
      $rsuggested = $suggested->query()->FetchAll();
      
      // print_r($rsuggested);die;
      $this->view->suglistccy = $rsuggested;

      $current = $this->_db->select()
                  ->from(array('A' => 'M_APP_BOUNDARY'),array('CCY_BOUNDARY','BOUNDARY_MIN','BOUNDARY_MAX','BOUNDARY_ID','CUST_ID','POLICY','TRANSFER_TYPE'));
      $current -> where("A.CUST_ID LIKE ".$this->_db->quote($cust_id));
      $current -> where("A.BOUNDARY_ISUSED LIKE '1'");
      $rcurrent = $current->query()->FetchAll();
      $oldSuggestArray = array();
      foreach($rsuggested as $row)
      {
        $oldSuggestArray[ $row['TRANSFER_TYPE'] ][] = $row['BOUNDARY_MAX'];
      }
      $this->view->oldSuggest = $oldSuggestArray;
      $this->view->curlist = $rcurrent;

      $currentccy = $this->_db->select()
                  ->from(array('A' => 'M_APP_BOUNDARY'),array('CCY_BOUNDARY','TRANSFER_TYPE'));
      $currentccy -> where("A.CUST_ID LIKE ".$this->_db->quote($cust_id));
      $currentccy -> where("A.BOUNDARY_ISUSED LIKE '1'");
      $currentccy->group('A.CCY_BOUNDARY');
      $currentccy->group('A.TRANSFER_TYPE');
      $rcurrentccy = $currentccy->query()->FetchAll();
      $this->view->curlistccy = $rcurrentccy;




      // Zend_Debug::dump($oldAppGroupArray);die;
      $this->view->cek = 1;
    }
    else{
      $this->_redirect('/notification/invalid/index');
      $this->view->cek = 0;     
    }
    if(!$this->_request->isPost()){
    Application_Helper_General::writeLog('AGCL','Viewing Update Approver Group');
    }
  		
	}
}



