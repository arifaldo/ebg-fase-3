<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class bankforexlimit_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->view->modulename);

		$model = new bankforexlimit_Model_Bankforexlimit();

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$listccy = $model->getCCYlist();
    	
    	$this->view->listccy = $listccy;
//     	print_r($listccy);die;
    	$this->view->report_msg = array();



		if($this->_request->isPost() )
		{	
			$ccyCode = $this->_getParam('ccyCode');
// 			print_r($ccyCode);
			$max_buy = $this->_getParam('max_buy');
			$max_buy_val =	Application_Helper_General::convertDisplayMoney($max_buy);
			$this->_setParam('max_buy',$max_buy_val);
			
			$max_sell = $this->_getParam('max_sell');
			$max_sell_val =	Application_Helper_General::convertDisplayMoney($max_sell);
			$this->_setParam('max_sell',$max_sell_val);
			
			$filters = array(
							 'ccyCode'    => array('StringTrim','StripTags'),
							 'max_buy'	=> array('StringTrim','StripTags'),
							 'max_sell'	=> array('StringTrim','StripTags'),
							 
							);

					$validators = array('ccyCode'  => array('NotEmpty',
// 																'Digits',
// 																 new Zend_Validate_StringLength(array('min'=>2,'max'=>3)),
																array('Db_NoRecordExists',array('table'=>'M_FOREX','field'=>'CCY')),
																array('Db_NoRecordExists',array('table'=>'M_FOREX_TEMP','field'=>'CCY')),
																'messages' => array(
																				   $this->language->_('Can not be empty'),
// 																				   $this->language->_('Invalid Format'),
// 																					$this->language->_('Data too long (min. 2 chars and max. 3 chars)'),
																					$this->language->_(' This currency has been registered. Please choose another currency'),
																					$this->language->_(' This currency has been registered. Please choose another currency'),
																					)
																		),
																		
											'max_buy'  => array('allowEmpty'=> TRUE,
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 9999999999.99',
															'Value between 1 - 9999999999.99',	)),

											'max_sell' => array('allowEmpty'=> TRUE,
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 9999999999.99',
															'Value between 1 - 9999999999.99',	)),
										   );

// 	echo "<pre>";
	
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			$error = $zf_filter_input->getMessages();
// 		print_r($error);die;
			$info = 'User ID = '.$this->_userIdLogin.', User Name = '.$zf_filter_input->user_name;
			if(empty($error))
			{
				
// 				print_r($content);die;
				try
				{

					//-------- insert --------------
					 $this->_db->beginTransaction();
//						  $change_id = $this->suggestionWaitingApproval('Customer',$info,$this->_changeType['code']['new'],null,'M_USER','TEMP_USER',$zf_filter_input->user_id,$user_data['USER_FULLNAME'],null);
// 						  $change_id = $this->suggestionWaitingApproval('Bank Forex',$info,$this->_changeType['code']['new'],null,'M_FOREX','TEMP_FOREX',null,$this->_userIdLogin,null);
// 						  print_r($change_id);die;
						  $content = array(
// 						  		'TEMP_ID' 	=> $change_id,
						  		'CCY' 	=> $ccyCode,
						  		'BUY_AMOUNT' 	=> $zf_filter_input->max_buy,
						  		'SELL_AMOUNT'  => $zf_filter_input->max_sell,
						  		'CREATEDBY'  => $this->_userIdLogin,
						  		'CREATED'  => new Zend_Db_Expr('GETDATE()')
						  );

// 						  
						  // dump the SQL
// 						  print_r($content);die;
						  $inserdata = $model->insertData($content);
// 						  print_r($inserdata);die;
					Application_Helper_General::writeLog('BXAD','Add Bank Forex Limit. CCY : ['.$zf_filter_input->ccyCode.'], Max Buy Limit : ['.$zf_filter_input->max_buy.'], Max Sell Limit : ['.$zf_filter_input->max_sell.']');
					$this->_db->commit();

					$this->view->success = true;
					$this->view->error = false;

					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					$error = $zf_filter_input->getMessages();
						
					//format error utk ditampilkan di view html
					foreach($error as $keyRoot => $rowError)
					{
						foreach($rowError as $errorString)
						{
							$errID = 'error_'.$keyRoot;
							$this->view->$errID = $errorString;
						}
					}
					$this->view->error = true;
				}
			}else
					{
						$this->view->error = true;
						$this->fillParam($zf_filter_input);
						$error = $zf_filter_input->getMessages();

						//format error utk ditampilkan di view html
						foreach($error as $keyRoot => $rowError)
						{
						   foreach($rowError as $errorString)
						   {
								$errID = 'error_'.$keyRoot;
								$this->view->$errID = $errorString;
						   }
						}
						$this->view->error = true;
					}
			
			
		}

		Application_Helper_General::writeLog('BXAD','Add Bank Forex Limit');
	}

    private function fillParam($zf_filter_input)
	{
		$this->view->ccyCode = ($zf_filter_input->isValid('ccyCode')) ? $zf_filter_input->ccyCode : $this->_getParam('ccyCode');
		$this->view->max_buy = ($zf_filter_input->isValid('max_buy')) ? $zf_filter_input->max_buy : $this->_getParam('max_buy');
		$this->view->max_sell     = ($zf_filter_input->isValid('max_sell')) ? $zf_filter_input->max_sell : $this->_getParam('max_sell');
	}



}