<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';


class useropenlimit_IndexController extends Application_Main{

	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$this->setbackURL('/'.$this->_request->getModuleName().'/index/'); 

	    $moduleDB = 'USF';
	    $model = new useropenlimit_Model_Userlimit();
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$system_type = $setting->getSetting('system_type');
		$this->view->system_type = $system_type; 
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash; 
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
		$conf = Zend_Registry::get('config');

		$this->view->bankName = $conf['app']['bankname'];
	  	$cust_id = $this->_custIdLogin;
	  	$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>25)))? strtoupper($cust_id) : '';

	  	$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

		$filterlist = array('USER_ID','USER_FULLNAME','BANK_NAME','ACCT_NUM');
		
		$this->view->filterlist = $filterlist;

	  	
	  	if($cust_id)
	  	{
	  	   $select = $this->_db->select()
	  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME','CUST_STATUS'))
	  	                         ->where('CUST_ID='.$this->_db->quote((string)$cust_id));
	  	                         
	      $result = $this->_db->fetchRow($select);
	      
	      if($result['CUST_ID'] && $result['CUST_STATUS'] == 1)
	      {
	      	$this->view->cust_name = $result['CUST_NAME'];
	      	$this->view->cust_status = $result['CUST_STATUS'];
	      }
	      else{ $cust_id = null; }
	  	}
	  	
	  	if($cust_id)
	  	{
	  		$cust = new Customer($cust_id);
	  		$userList = $cust->getUserList();
	  		$this->view->userlist = $userList;
			$fields = array('userid'   => array('field'    => 'USER_ID',
			                                  'label'    => 'User Name',
			                                  'sortable' => true),

			              'bank_name' => array('field'    => 'bank_name',
			                                  'label'    => 'Bank',
			                                  'sortable' => true),
			              'Account_number' => array('field'    => 'Account_number',
			                                  'label'    => 'Account Number',
			                                  'sortable' => true),
			              'ccy' => array('field'    => 'ccy',
			                                  'label'    => 'CCY',
			                                  'sortable' => true),
			              'maxlimit' => array('field'    => 'maxlimit',
			                                  'label'    => 'Maker Limit',
			                                  'sortable' => true),
			              'lastsuggestedby' => array('field'    => 'lastsuggestedby',
			                                  'label'    => 'Last Suggested by',
			                                  'sortable' => true),

			              'lastapprovedby' => array('field'    => 'lastapprovedby',
			                                  'label'    => 'Last Approved by',
			                                  'sortable' => true),

			              // 'last_approve'   => array('field'    => 'USER_UPDATED',
			              //                     'label'    => 'Latest Approval',
			              //                     'sortable' => true),

			              // 'last_approver'   => array('field'    => 'USER_UPDATEDBY',
			              //                     'label'    => 'Latest Approver',
			              //                     'sortable' => true),
			             );

			$csv = $this->_getParam('csv');
			$page = $this->_getParam('page');
			$sortBy = $this->_getParam('sortby','USER_ID');
			$sortDir = $this->_getParam('sortdir','ASC');
			$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
			$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
			$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

			$filterArr = array(
			                 'USER_ID'    => array('StripTags','StringTrim','StringToUpper'),
			                 'BANK_NAME'    => array('StripTags','StringTrim','StringToUpper'),
			                 'USER_FULLNAME'  => array('StripTags','StringTrim','StringToUpper'),
			                 'ACCT_NUM'  => array('StripTags','StringTrim','StringToUpper'),
			                 'USER_SUGGESTED' => array('StripTags','StringTrim'),
			                 'USER_UPDATED' => array('StripTags','StringTrim'),
			                 'USER_SUGGESTED_END' => array('StripTags','StringTrim'),
			                 'USER_UPDATED_END' => array('StripTags','StringTrim')
			                );




			$dataParam = array("USER_ID","USER_FULLNAME","BANK_NAME","ACCT_NUM");
			$dataParamValue = array();
			foreach ($dataParam as $dtParam)
			{
	
				 
				 
				if(!empty($this->_request->getParam('whereco'))){
					$dataval = $this->_request->getParam('whereval');
						foreach ($this->_request->getParam('whereco') as $key => $value) {
							if($dtParam==$value){
								if(!empty($dataParamValue[$dtParam])){
									$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
								}
								
								$dataParamValue[$dtParam] = $dataval[$key];
							}
						}
	
				}
	
				
	
				// $dataPost = $this->_request->getPost($dtParam);
				// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
			if(!empty($this->_request->getParam('updatedate'))){
				$createarr = $this->_request->getParam('updatedate');
					$dataParamValue['USER_UPDATED'] = $createarr[0];
					$dataParamValue['USER_UPDATED_END'] = $createarr[1];
			}
			

			if(!empty($this->_request->getParam('sugestdate'))){
				$efdatearr = $this->_request->getParam('sugestdate');
					$dataParamValue['USER_SUGGESTED'] = $efdatearr[0];
					$dataParamValue['USER_SUGGESTED_END'] = $efdatearr[1];
			}

			$options = array('allowEmpty' => true);
			$validators = array(
								'USER_ID'    => array(),
								'BANK_NAME'    => array(),
								'USER_FULLNAME'  => array(),
								'ACCT_NUM'  => array(),
								'USER_SUGGESTED' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
								'USER_UPDATED' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
								'USER_SUGGESTED_END' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
								'USER_UPDATED_END' => array(new Zend_Validate_Date($this->_dateDisplayFormat))
			
			);
			                
			$zf_filter = new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
			// $filter    = $zf_filter->getEscaped('filter');
			$this->view->currentPage = $page;
			$this->view->sortBy      = $sortBy;
			$this->view->sortDir     = $sortDir;
			$this->view->user_msg    = $this->_helper->getHelper('FlashMessenger')->getMessages();

			$filter 		= $this->_getParam('filter');
			$filter_clear 	= $this->_getParam('filter_clear');

			$filterSend = array();
			$filterSend['cust_id'] = $cust_id;


			if($filter == TRUE)
			{
				$userid = html_entity_decode($zf_filter->getEscaped('USER_ID'));
				$bankname = html_entity_decode($zf_filter->getEscaped('BANK_NAME'));
				$uname = html_entity_decode($zf_filter->getEscaped('USER_FULLNAME'));
				$acctnum = html_entity_decode($zf_filter->getEscaped('ACCT_NUM'));
				$suggestStart = $zf_filter->getEscaped('USER_SUGGESTED');
				$suggestEnd = $zf_filter->getEscaped('USER_SUGGESTED_END');
				$approveStart = $zf_filter->getEscaped('USER_UPDATED');
				$approveEnd = $zf_filter->getEscaped('USER_UPDATED_END');

				$this->view->userid = $userid;
				$this->view->uname = $uname;
				$this->view->suggestStart = $suggestStart;
				$this->view->suggestEnd	= $suggestEnd;
				$this->view->approveStart = $approveStart;
				$this->view->approveEnd = $approveEnd;

				if($userid != null){
					$useridArr = explode(',', $userid);
					$filterSend['userid'] = $useridArr;
				}

				if($bankname != null){
					$banknameArr = explode(',', $bankname);
					$filterSend['bankname'] = $banknameArr;
				}

				if($uname != null){
					$unameArr = explode(',', $uname);
					$filterSend['uname'] = $unameArr;
				}

				if($acctnum != null){
					$acctnumArr = explode(',', $acctnum);
					$filterSend['acctnum'] = $acctnumArr;
				}

				if($suggestStart != null){
					$FormatDate 	= new Zend_Date($suggestStart, $this->_dateDisplayFormat);
					$suggestStart  	= $FormatDate->toString($this->_dateDBFormat);	
					$filterSend['suggeststart'] = $suggestStart;
				}

				if($suggestEnd != null){
					$FormatDate 	= new Zend_Date($suggestEnd, $this->_dateDisplayFormat);
					$suggestEnd  	= $FormatDate->toString($this->_dateDBFormat);	
					$filterSend['suggestend'] = $suggestEnd;
				}

				if($approveStart != null){
					$FormatDate 	= new Zend_Date($approveStart, $this->_dateDisplayFormat);
					$approveStart  	= $FormatDate->toString($this->_dateDBFormat);	
					$filterSend['approvestart'] = $approveStart;
				}

				if($approveEnd != null){
					$FormatDate 	= new Zend_Date($approveEnd, $this->_dateDisplayFormat);
					$approveEnd  	= $FormatDate->toString($this->_dateDBFormat);	
					$filterSend['approveEnd'] = $approveEnd;
				}
			}

			if($filter_clear == TRUE){
				// $suggestEnd 	= date("d/m/Y");
				// $approveEnd 	= date("d/m/Y");
				$this->view->suggestStart  = '';
				$this->view->suggestEnd  = '';
				$this->view->approveStart  = '';
				$this->view->approveEnd  = '';
				
				// $FormatDate 	= new Zend_Date('01/01/1970', $this->_dateDisplayFormat);
				// $suggestStart  = $FormatDate->toString($this->_dateDBFormat);	
				// $filterSend['suggeststart'] = $suggestStart;
				
				// $FormatDate 	= new Zend_Date($suggestEnd, $this->_dateDisplayFormat);
				// $suggestEnd  = $FormatDate->toString($this->_dateDBFormat);	
				// $filterSend['suggestend'] = $suggestEnd;

				// $FormatDate 	= new Zend_Date('01/01/1970', $this->_dateDisplayFormat);
				// $approveStart  = $FormatDate->toString($this->_dateDBFormat);	
				// $filterSend['approvestart'] = $approveStart;
				
				// $FormatDate 	= new Zend_Date($approveEnd, $this->_dateDisplayFormat);
				// $approveEnd  = $FormatDate->toString($this->_dateDBFormat);	
				// $filterSend['approveend'] = $approveEnd;

				// var_dump($filterSend);
			}

			$sorting = $sortBy.' '.$sortDir;
			$result = $model->getUser($filterSend,$sorting);

			// echo "<pre>";
			// var_dump($result);
			// die();
                              
			$this->paging($result);
			$this->view->fields = $fields;
			$this->view->filter = $filter;

			$data_val=array();
			foreach($result as $valu){
				$paramTrx = array (
								"USER_FULLNAME"	=> $valu['USER_FULLNAME'],
								"BANK_NAME"	=> $valu['BANK_NAME'],
								"ACCT_NO"	=> $valu['ACCT_NO'],
								"ACCT_CCY"	=> $valu['ACCT_CCY'],
								"MAXLIMIT"	=> $valu['MAXLIMIT'],
								"SUGGESTED"	=> $valu['SUGGESTEDBY']."(" . $valu['SUGGESTED'] . ")",
								"UPDATED"	=> $valu['UPDATEDBY']."(" . $valu['UPDATED'] . ")",
							
				);
				array_push($data_val, $paramTrx);
			}

			$filterlistdata = array("Filter");
			foreach($dataParamValue as $fil => $val){
					$paramTrx = $fil . " - " . $val;
					array_push($filterlistdata, $paramTrx);
			}

			// echo "<pre>";
			// var_dump($dataParamValue);
			// var_dump($filterlistdata);
			if ($csv) {
				// $this->_helper->download->csv(array($this->language->_('User Name'),$this->language->_('CCY'),$this->language->_('Daily Limit'),$this->language->_('Last Suggested By	'), $this->language->_('Last Approved By')),$data_val,null,'User Daily Limit Scheme');
				$header  = Application_Helper_Array::simpleArray($fields, "label");
				$listable = array_merge_recursive(array($header), $data_val);
				$this->_helper->download->csv($filterlistdata, $listable, null, 'User Maker Limit');
			  }

	  	}
		
	//   	if(!empty($this->_request->getParam('wherecol'))){
    //     $this->view->wherecol     = $this->_request->getParam('wherecol');
    //   }

    //   if(!empty($this->_request->getParam('whereopt'))){
    //     $this->view->whereopt     = $this->_request->getParam('whereopt');
    //   }

    //   if(!empty($this->_request->getParam('whereval'))){
    //     $this->view->whereval     = $this->_request->getParam('whereval');
    //   }
    	$this->view->cust_id = $cust_id;
		Application_Helper_General::writeLog('VMLU','View User Limit List');
		if (!empty($dataParamValue)) {
			// $this->view->createdStart = $dataParamValue['PS_CREATED'];
			// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
		//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
		//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

			// unset($dataParamValue['PS_CREATED_END']);
			// unset($dataParamValue['PS_EFDATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',',$value);
							if(!empty($duparr)){
								
								foreach($duparr as $ss => $vs){
									$wherecol[]	= $key;
									$whereval[] = $vs;
								}
							}else{
									$wherecol[]	= $key;
									$whereval[] = $value;
							}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
				
    	}
	// 	if(!empty($dataParamValue)){
	//     		$this->view->updateStart = $dataParamValue['USER_UPDATED'];
	//     		$this->view->updateEnd = $dataParamValue['USER_UPDATED_END'];
	//     		$this->view->sugestStart = $dataParamValue['USER_SUGGESTED'];
	//     		$this->view->sugestEnd = $dataParamValue['USER_SUGGESTED_END'];

	//     	  	unset($dataParamValue['USER_UPDATED_END']);
	// 		    unset($dataParamValue['USER_SUGGESTED_END']);
	// 		foreach ($dataParamValue as $key => $value) {
	// 			$wherecol[]	= $key;
	// 			$whereval[] = $value;
	// 		}
    //     $this->view->wherecol     = $wherecol;
    //     $this->view->whereval     = $whereval;
    //  // print_r($whereval);die;
    //   }
	  	
	}

	public function banknameAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $selectTrx	= $this->_db->select()
			->from(	array('M_BANK_TABLE'),array('*'));
			
		$ACBENEFArr = $this->_db->fetchAll($selectTrx);

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['BANK_CODE']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['BANK_CODE']."' ".$select.">".$value['BANK_NAME']."</option>";
        }

        echo $optHtml;
    }
	
}

