<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';


class purchasing_NewhandphoneController extends Application_Main
{
	public function initModel()
    {
		$this->model 					= new payment_Model_Payment();
		$this->param['SERVICE_TYPE'] 	= 14;
		$this->param['PROVIDER_TYPE'] 	= 2;
		$this->param['fetch'] 			= 'fetchAll';
		$this->param['USER_ID'] 		= $this->_userIdLogin;
		$this->CustomerUser 			= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    }

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$select = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE','CATALOG_ID'))
				->where("A.NOMINAL > 0")
				->where("A.PROVIDER_ID = 1074");
		//$select ->order('PROVIDER_NAME ASC');
		$this->view->telkomsel = $select->query()->FetchAll();
		$select1 = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE','CATALOG_ID'))
				->where("A.NOMINAL > 0")
				->where("A.PROVIDER_ID = 1075");
		$this->view->indosat = $select1->query()->FetchAll();
		$select2 = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE','CATALOG_ID'))
				->where("A.NOMINAL > 0")
				->where("A.PROVIDER_ID = 1080");
		$this->view->smartfren = $select2->query()->FetchAll();
		$select3 = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE','CATALOG_ID'))
				->where("A.NOMINAL > 0")
				->where("A.PROVIDER_ID = 1083");
		$this->view->three = $select3->query()->FetchAll();
		$select4 = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE','CATALOG_ID'))
				->where("A.NOMINAL > 0")
				->where("A.PROVIDER_ID = 1078");
		$this->view->xl = $select4->query()->FetchAll();

		$select5 = $this->_db->fetchAll(
						$this->_db->select()
					->from(array('A'=>'T_PSLIP'),array('*'))
					->joinLeft(array('B' => 'T_TRANSACTION'),'A.PS_NUMBER = B.PS_NUMBER',array('*'))
					->joinLeft(array('C' => 'M_SERVICE_PROVIDER'),'A.PS_BILLER_ID = C.PROVIDER_ID',array('C.SERVICE_TYPE'))
					->where('A.CUST_ID = '.$this->_db->quote($this->_custIdLogin)) 
					->where('C.SERVICE_TYPE = 14')
					->where('PS_TYPE IN (?)',array('16,17'))
					->order('PS_EFDATE DESC')
					->limit(5)
				); 

		$this->view->historyData = $select5;

		$sessionNamespace 			= new Zend_Session_Namespace('phone');
		$paramSession 				= $sessionNamespace->paramSession;
		$paramac = array(
				'CCY_IN' => 'IDR'
		);
		$AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
		$this->view->AccArr 		= $AccArr;

		$anyValue = '-- '.$this->language->_('Any Value'). ' --';

		$arr 						= $this->model->getProviderId($this->param['SERVICE_TYPE'],$this->param['PROVIDER_TYPE']);
		$providerArr 				= array(''=> $anyValue);
		$providerArr 				+= Application_Helper_Array::listArray($arr,'PROVIDER_ID','PROVIDER_NAME');
		$this->view->providerArr 	= $providerArr;

		//select m_user - begin
		$userData = $this->CustomerUser->getUser($this->_userIdLogin);

		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];
		$this->view->userIdLogin 	= $this->_userIdLogin;

		$tokenType 					= $userData['TOKEN_TYPE'];
		$tokenIdUser 				= $userData['TOKEN_ID'];
		$this->view->token = false;
		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);

		//select m_user - end
		$list 							= $this->model->cekList($this->param);
		$this->view->paymentList 		= $list;

		//Zend_Debug::dump($this->_getAllParams());

		//added cms purchase payment get repair - begin
		$PS_NUMBER 						= $this->_getParam('PS_NUMBER');
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';

    	if (!empty($PS_NUMBER))
    	{
    		$paramList = array("WA" 			=> false,
							   "ACCOUNT_LIST" 	=> $this->_accountList,
							   "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
    		$select   = $this->CustomerUser->getPayment($paramList, false);
			$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);

			$select->columns(array(	"tra_message"		=> "T.TRA_MESSAGE",
									"tra_refno"			=> "T.TRA_REFNO",
									"acbenef_email"		=> "T.BENEFICIARY_EMAIL",
									//"acbenef_bankname"	=> "T.BENEFICIARY_ACCOUNT_NAME",
									//"acbenef_alias"		=> "T.BENEFICIARY_ALIAS_NAME",
							  	   )
							 );

			$pslipData 	= $this->_db->fetchRow($select);
			//Zend_Debug::dump($pslipData);

    		if (!empty($pslipData))
			{
				// $PS_EFDATE  		= $pslipData['efdate'];
				$PS_EFDATE  		= date("d/m/Y",strtotime($pslipData['efdate']));

				$param['ACCTSRC'] 	= $pslipData['accsrc'];
				$logObj 			= json_decode($pslipData['LOG']);
				$param 				= (array) $logObj;
				$operator 			= $param['operator'];
				$sgoProductCode 	= $param['sgoProductCode'];

				$model 				= new purchasing_Model_Purchasing();
				$listvoucherNew 	= $model->getProviderId($operator);
				$providerIdArrtest 	= array('empty' => ' --- Please Select ---');
				$providerIdArrtest 	+= Application_Helper_Array::listArray($listvoucherNew,'NOMINAL','NOMINAL');
				// var_dump($providerIdArrtest); die;
				$cekAmountList 		= $this->view->amountList 		= $listvoucherNew;

				$userDataServiceProvider 	= $this->CustomerUser->getServiceProvider($operator);
				$provInquiry 				= $this->view->provInquiry = $userDataServiceProvider['PROVIDER_INQUIRY_SERVICE_STATUS'];

				$getAmount 			= $model->getProductCode($sgoProductCode);
				$sgoProductCodeRep 	= $getAmount['0']['SGO_PRODUCT_CODE'];
				//Zend_Debug::dump($getAmount);
			}
    	}
		//added cms purchase payment get repair - end

		if($this->_getParam('operator'))
		{
			//select m_service_provider - begin
			$userDataServiceProvider 	= $this->CustomerUser->getServiceProvider($this->_getParam('operator'));
			$provInquiry = $this->view->provInquiry = $userDataServiceProvider['PROVIDER_INQUIRY_SERVICE_STATUS'];

			//get voucher nominal base providerid
			$model 						= new purchasing_Model_Purchasing();
			$listvoucherNew 			= $model->getProviderId($this->_getParam('operator'));
			$providerIdArrtest 			= array('empty' => ' --- Please Select ---');
			$providerIdArrtest 			+= Application_Helper_Array::listArray($listvoucherNew,'NOMINAL','NOMINAL');
			$cekAmountList 				= $this->view->amountList 		= $listvoucherNew;
			// var_dump($providerIdArrtest); die;

			$msg = array();
			$param = array();
			$param['SERVICE_TYPE'] 		= 14;
			$param['PROVIDER_TYPE'] 	= 2; //1
			$param['operator']			= $this->_getParam('operator');
			$param['ACCTSRC']			= $this->_getParam('ACCTSRC');
			$param['cek']				= $this->_getParam('cek');
			$param['radioCheck']		= 1;
			$param['payment']			= $this->_getParam('payment');
			$param['USER_ID'] 			= $this->_userIdLogin;
			$param['responseCodeReq'] 	= $this->_getParam('responseCodeReq');
			$param['TOKEN_TYPE'] 		= $tokenType;
			$param['TOKEN_ID'] 			= $tokenIdUser;
			$param['CUST_ID'] 			= $this->_custIdLogin;
			$param['ACCT_NO'] 			= $this->_getParam('ACCTSRC');
			$param['USER_MOBILE_PHONE'] = $userMobilePhone;

			$param['provInquiry'] 	= $provInquiry;

			
			if($this->_getParam('operatortype') == '1'){
				$sgo_product_code_data	= $this->model->getSgoProductCodeNominal($this->_getParam('amounttel'));
				$sgo_product_code 		= $this->_getParam('amounttel');
			}elseif ($this->_getParam('operatortype') == '2') {
				$sgo_product_code_data	= $this->model->getSgoProductCodeNominal($this->_getParam('amountind'));
				$sgo_product_code 		= $this->_getParam('amountind');
			}elseif ($this->_getParam('operatortype') == '3') {
				$sgo_product_code_data	= $this->model->getSgoProductCodeNominal($this->_getParam('amountxl'));
				$sgo_product_code 		= $this->_getParam('amountxl');
			}elseif ($this->_getParam('operatortype') == '4') {
				$sgo_product_code_data	= $this->model->getSgoProductCodeNominal($this->_getParam('amountthree'));
				$sgo_product_code 		= $this->_getParam('amountthree');
			}elseif ($this->_getParam('operatortype') == '5') {
				$sgo_product_code_data	= $this->model->getSgoProductCodeNominal($this->_getParam('amountsmart'));
				$sgo_product_code 		= $this->_getParam('amountsmart');
			}

			
			$amount 				= empty($sgo_product_code_data['0']['NOMINAL'])?'':$sgo_product_code_data['0']['NOMINAL'];
			

			
			$param['productCode'] 	= $sgo_product_code; //STCKAI
			$param['amount'] 		= Application_Helper_General::convertDisplayMoney($amount);
			// var_dump($param['amount']); die;

			if($param['radioCheck']==1)
			{
				$param['orderId'] = $this->_getParam('orderId');
			}
			
			$nominalData			= $this->model->getNominal($this->_getParam('operator'), $sgo_product_code);
			// echo "<pre>";
			// var_dump($sgo_product_code);
			// print_r($this->_request->getParams());die;
			// print_r($this->_request->getParams());
			// Zend_Debug::dump($param);


								// }
			// }else{
				// $sendProvider 		= new SinglePayment(null,$this->_custIdLogin, $this->_userIdLogin);
				// $resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg);
			// }
			if($this->_custSameUser){
				// die('here');
				if(!$this->view->hasPrivilege('PRLP')){
										// die('here');
										$msg = $this->language->_("Error: You don't have privilege to release payment");
										$this->view->error = true;
										$resProvider = false;
										$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
				}else{
					$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
					$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
				}
			}else{
				$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
				$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
			}
			// var_dump($msg);
			// var_dump($resProvider);die;
			// $resProvider = true;
			if($resProvider == TRUE && $nominalData == TRUE)
			{
				$this->view->error = false;
				if($this->_custSameUser){
// die;
					// if(!$this->view->hasPrivilege('PRLP')){
					// 				// die('here');
					// 				$msg = $this->language->_("Error: You don't have privilege to release payment");
					// 				$this->view->error = true;
					// 				$resProvider = false;
					// 				$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
					// 			}else{
									// die('ds');
									// echo 'sisni';
								// $challengeCode		= $this->_getParam('challengeCode');
								
								// $inputtoken1 		= $this->_getParam('inputtoken1');
								// $inputtoken2 		= $this->_getParam('inputtoken2');
								// $inputtoken3 		= $this->_getParam('inputtoken3');
								// $inputtoken4 		= $this->_getParam('inputtoken4');
								// $inputtoken5 		= $this->_getParam('inputtoken5');
								// $inputtoken6 		= $this->_getParam('inputtoken6');

								// $responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;

								// $userOnBehalf = $this->_userIdLogin;
								// $HardToken 	= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
								// $resHard = $HardToken->verifyHardToken($challengeCode, $responseCode);
								// $resultToken = $resHard['ResponseCode'] == '0000';

								// if ($resHard['ResponseCode'] != '0000'){
								// 	$tokenFailed = $CustUser->setLogToken(); //log token activity

								// 	$this->view->error = true;
								// 	$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

								// 	if ($tokenFailed === true)
								// 	{
								// 		$this->_redirect('/default/index/logout');
								// 	}
								// }

								// }

					}

				if(!$this->view->error){
				if($msg['detailCustomer']['0']['ACCT_TYPE'] == '10'){
					$msg['detailCustomer']['0']['ACCT_TYPE'] = 'SAVING';
				}
				$paramSession['operator']			= $param['operator'];
				$paramSession['ACCTSRC']			= $param['ACCTSRC'];
				$paramSession['ccy']				= $msg['detailCustomer']['0']['CCY_ID'];
				$paramSession['ACCT_NAME']			= $msg['responseData']['0']['customer_name'];
				$paramSession['ACCT_NAME_DATA']		= $msg['detailCustomer']['0']['ACCT_NAME'];
				$paramSession['ACCT_ALIAS_NAME']	= $msg['detailCustomer']['0']['ACCT_NAME'];
				$paramSession['ACCT_TYPE']			= $msg['detailCustomer']['0']['ACCT_TYPE'];
				$paramSession['FREEZE_STATUS']		= $msg['detailCustomer']['0']['FREEZE_STATUS'];
				$paramSession['MAXLIMIT']			= $msg['detailCustomer']['0']['MAXLIMIT'];
				$paramSession['amount']				= $msg['responseData']['0']['amount'];
				$paramSession['disAmount']			= $msg['responseData']['0']['denomination'];
				$paramSession['fee']				= $msg['fee']['0'];
				$paramSession['disFee']				= $msg['responseData']['0']['price'] - $msg['responseData']['0']['denomination'];
				$paramSession['total']				= $msg['responseData']['0']['amount']+$msg['fee']['0'];
				$paramSession['sgoProductCode']		= $msg['param']['0']['productCode'];
				$paramSession['operatorNama']		= $msg['provider']['operatorName'];
				$paramSession['CUST_ID']			= $this->_custIdLogin;
				$paramSession['USER_ID']			= $this->_userIdLogin;
				$paramSession['TOKEN_TYPE']			= $tokenType;
				$paramSession['USER_MOBILE_PHONE']	= $userMobilePhone;

				$paramSession['message']			= $msg['param']['0']['Message'];
				$paramSession['PS_CATEGORY']		= $msg['param']['0']['PS_CATEGORY'];
				$paramSession['TypeOfTrans']		= $msg['param']['0']['TypeOfTrans'];
				$paramSession['PS_CATEGORY_EMAIL']	= $msg['param']['0']['PS_CATEGORY_EMAIL'];
				$paramSession['PS_TYPE']			= $msg['param']['0']['PS_TYPE'];
				$paramSession['Module']				= $msg['param']['0']['Module'];

				$paramSession['cek']				= $param['cek'];

				$paramSession['orderId']			= $param['orderId'];

				$paramSession['PS_NUMBER']			= $PS_NUMBER;

// 				Zend_Debug::dump($paramSession);die;

				$sessionNamespace->paramSession 	= $paramSession;
				$this->_redirect('/purchasing/newhandphone/next');
				}else{
					// echo 'here';
						$errors 	= $msg;
						$error		= true;
				}
			}
			else
			{
				// var_dump($this->view->hasPrivilege('PRLP'));
				// if(!$this->view->hasPrivilege('PRLP')){
				// echo 'gere';
				// var_dump($msg);die;
				$errors 	= $msg;
				$error		= true;
				// }
			}

			if($error)
			{
				// die;
				if($this->_getParam('next')== $this->language->_('Next')){
					$this->view->error 					= $error;
					$this->view->operatorErr 			= (isset($errors['operator']))? $errors['operator'] : null;
					$this->view->orderIdErr 			= (isset($errors['orderId']))? $errors['orderId'] : null;
					$this->view->paymentErr 			= (isset($errors['payment']))? $errors['payment'] : null;
					$this->view->ACCTSRCErr 			= (isset($errors['ACCTSRC']))? $errors['ACCTSRC'] : null;
					$this->view->amountErr 				= (isset($errors['amount']))? $errors['amount'] : null;
					$this->view->responseCodeReqErr 	= (isset($errors['responseCodeReq']))? $errors['responseCodeReq'] : null;
					$this->view->xACCTSRC 				= (isset($errMsg))? $errMsg : null;
				}

				$this->view->operator				= $param['operator'];
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				$this->view->cek					= $param['cek'];
				$this->view->themes					= $this->_getParam('themes');
				$this->view->amount					= $param['productCode'];



				if($provInquiry == 'N'){
					$this->view->amount				= $this->_getParam('amount');
				}

				$this->view->orderId				= $param['orderId'];
				// print_r($msg);die;
				if(!empty($msg)){
					$this->view->ERROR_MSG_N 		= $msg;
				}else if(count($msg) < 1){
					$this->view->ERROR_MSG_N 		= 'Error : Invalid Token APPLI 2';
				}
			}
		}
		else
		{
			unset($_SESSION['phone']);
		}
		Application_Helper_General::writeLog('PHPV','Purchase Handphone Voucher');

		///added cms purchase payment get repair - begin
		if (!empty($PS_NUMBER) && !$this->_getParam('operator')){

				$this->view->operator		= $param['operator'];
				$this->view->ACCTSRC		= $param['ACCTSRC'];
				$this->view->cek			= $param['cek'];
				$this->view->amount			= $sgoProductCodeRep;
				$this->view->orderId		= $param['orderId'];
		}
		//added cms purchase payment get repair - end
	}

	public function nextAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->userId	= $this->_userIdLogin;
		$SinglePayment 		= new SinglePayment( null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('phone');
		$paramSession 		= $sessionNamespace->paramSession;
		//added repair purchase payment - begin
		$PS_NUMBER 			= $paramSession["PS_NUMBER"];
		//Zend_Debug::dump($paramSession);
		$this->view->paramSession	= $paramSession;
		$PS_EFDATE			= date('Y-m-d');
		$date				= $this->model->date();

		//select m_user - begin
		$userData = $this->CustomerUser->getUser($this->_userIdLogin);

		//added token type
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);

		$paramSession['PS_EFDATE'] 	= $PS_EFDATE;
		$paramSession['date'] 		= $date;

		$paramSession['dateDisplayFormat'] 	= $this->_dateDisplayFormat;
		$paramSession['dateDBFormat'] 		= $this->_dateDBFormat;
		$this->view->token = false;
		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID','GOOGLE_CODE')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];
		 


			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			if(!empty($tokenGoogle)){
				// die('here');
				$this->view->googleauth = true;
			}
		}

		//ADDED PRIV
		$paramSession['priv1'] = $this->view->hasPrivilege('BADA');
		$paramSession['priv2'] = $this->view->hasPrivilege('BLBU');
		$paramSession['priv3'] = "PHPV";

		//added cms purchase payment get repair - begin
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))? $PS_NUMBER: '';

    	if (!empty($PS_NUMBER))
    	{
    		$paramSession['isRepair'] = true;
    	}
    	else{
    		$paramSession['isRepair'] = false;
    	}
    	//added cms purchase payment get repair - end

		if($this->_getParam('submit1') == TRUE)
		{

			$inputtoken1 		= $this->_getParam('inputtoken1');
			$inputtoken2 		= $this->_getParam('inputtoken2');
			$inputtoken3 		= $this->_getParam('inputtoken3');
			$inputtoken4 		= $this->_getParam('inputtoken4');
			$inputtoken5 		= $this->_getParam('inputtoken5');
			$inputtoken6 		= $this->_getParam('inputtoken6');

			$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;
								
			if($this->_custSameUser){




				if(!empty($tokenGoogle)){

								$pga = new PHPGangsta_GoogleAuthenticator();
						    	// var_dump($data2['GOOGLE_CODE']);
						    	$setting 		= new Settings();
								$google_duration 	= $setting->getSetting('google_duration');
								$resultcapca = $pga->verifyCode($tokenGoogle, $responseCode, $google_duration);
									// var_dump($resultcapca);
						   //  	var_dump($responseCode);
						   //  	var_dump($tokenGoogle);die;
							        if($resultcapca)
							        {
							        	$resultToken = $resHard['ResponseCode'] == '0000';
							        	// die('here');

							        }else{
							        	$tokenFailed = $CustUser->setLogToken(); //log token activity

										$error = true;
										$this->view->error = true;
										$errorMsg[] = $this->language->_('Invalid Google Auth Code');	//$verToken['ResponseDesc'];
										$this->view->ERROR_MSG =  $this->language->_('Invalid Google Auth Code');

										// if ($tokenFailed === true)
										// {
										// 	$this->_redirect('/default/index/logout');
										// }
							        }


								}else{

								$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
								$verToken 	= $Token->verify($challengeCode, $responseCode);

								if ($verToken['ResponseCode'] != '00'){
									$tokenFailed = $CustUser->setLogToken(); //log token activity

									$error = true;
									$this->view->error = true;
									$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
									$this->view->ERROR_MSG =  $this->language->_('Invalid Response Code');

									if ($tokenFailed === true)
									{
										$this->_redirect('/default/index/logout');
									}
								}
								}
					// var_dump($error);die;			
			if(!$error){
				$sendProvider 		= new SinglePayment(null,$this->_custIdLogin, $this->_userIdLogin);
				$resWS = array();
				$resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg, $resWS);
				}else{
					$resProvider = false;
				}
			}else{
				$sendProvider 		= new SinglePayment(null,$this->_custIdLogin, $this->_userIdLogin);
				$resWS = array();
				$resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg, $resWS);
			}
 			//Zend_Debug::dump(json_decode($resWS['ResponseData']['data']),'memew');
 		// 	var_dump($error);
 		// 	echo "<pre>";
 		// 	var_dump($resWS);
			// var_dump($resProvider);die;
 			$responseData = isset($resWS['ResponseData']['data']);
			if($resProvider == TRUE){

				$paramSession['operator']			= $paramSession['operator'];
				$paramSession['ACCTSRC']			= $paramSession['ACCTSRC'];
				$paramSession['ccy']				= $paramSession['ccy'];
				$paramSession['ACCT_NAME']			= $paramSession['ACCT_NAME'];
				$paramSession['ACCT_NAME_DATA']		= $paramSession['ACCT_NAME_DATA'];
				$paramSession['ACCT_ALIAS_NAME']	= $paramSession['ACCT_ALIAS_NAME'];
				$paramSession['amount']				= $paramSession['amount'];
				$paramSession['fee']				= $paramSession['fee'];
				$paramSession['total']				= $paramSession['total'];
				$paramSession['orderId']			= $paramSession['orderId'];
				$paramSession['sgoProductCode']		= $paramSession['sgoProductCode'];
				$paramSession['operatorNama']		= $paramSession['operatorNama'];
				$paramSession['CUST_ID']			= $paramSession['CUST_ID'];
				$paramSession['USER_ID']			= $paramSession['USER_ID'];
				$paramSession['PSNumber']			= $msg['PS_NUMBER']['0'];
				$paramSession['date']				= $paramSession['date'];
				$paramSession['paymentMessage']		= $msg['paymentMessage']['0'];

				$paramSession['trans_id']			= $responseData['refnum'];

				$sessionNamespace->paramSession 	= $paramSession;
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';
				if($this->_custSameUser){
										//		die('sini');
											$paramSQL = array("WA" 				=> false,
															  "ACCOUNT_LIST" 	=> $this->_accountList,
															  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
															 );

											// get payment query
											$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
											$select   = $CustUser->getPayment($paramSQL);
											$select->where('P.PS_NUMBER = ?' , (string) $msg['PS_NUMBER']['0']);
											// echo $select;
											$pslip = $this->_db->fetchRow($select);
											$settingObj = new Settings();
											$setting = array("COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
															 "COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
															 "COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
															 "COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
															 "COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
															 'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
															 "range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
															 "auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
															 "_dateFormat" 			=> $this->_dateDisplayFormat,
															 "_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
															 "_transfertype" 		=> array_flip($this->_transfertype["code"]),
															);

											$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
											$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

											$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
											$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

											$app = Zend_Registry::get('config');
											$appBankname = $app['app']['bankname'];

											$selectTrx = $this->_db->select()
											  ->from(	array(	'TT' => 'T_TRANSACTION'),
														array(
																'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
																'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
																'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
																//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
																'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
																'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
																'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
																'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
																'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
																'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
																'TRA_REFNO'				=> 'TT.TRA_REFNO',
																'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
																'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
																'TRA_STATUS'			=> 'TT.TRA_STATUS',
																'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
																'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
																'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
																'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
																'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'CLR_CODE'				=> 'TT.CLR_CODE',
																'TT.RATE',
																'TT.PROVISION_FEE',
																'TT.NOSTRO_NAME',
																'TT.FULL_AMOUNT_FEE',
																'C.PS_CCY','C.CUST_ID',
																'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
																'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
																'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$appBankname."'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('".$appBankname."',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('".$appBankname."',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
																'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
					        																FROM T_PERIODIC_DETAIL Y
					        																inner join T_PSLIP Z
					        																on Y.PS_PERIODIC = Z.PS_PERIODIC
					        																where
					        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
															  )
														)
												->joinLeft(	array(	'C' => 'T_PSLIP' ),'C.PS_NUMBER = TT.PS_NUMBER',array())
												->where('TT.PS_NUMBER = ?', $msg['PS_NUMBER']['0']);
												// var_dump($msg);
							// echo $selectTrx;die;
												$paramTrxArr = $this->_db->fetchAll($selectTrx);

												$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $msg['PS_NUMBER']['0']);
												$paramPayment = array_merge($pslip, $setting);
												// echo '<pre>';
												// print_r($paramPayment);
												// print_r($paramTrxArr);
												// die;
												$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
												$infoWarnOri = (!empty($check['infoWarning'])?'*) '.$check['infoWarning']:'');
												$sessionNameConfrim->infoWarnOri = $infoWarnOri;

												if($validate->isError() === true)
												{
													$error = true;
													$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
												}

												$Payment = new Payment($msg['PS_NUMBER']['0'], $this->_custIdLogin, $this->_userIdLogin);
												// if ($this->_hasPriviReleasePayment){
													$resultRelease = $Payment->releasePayment();
													// print_r($resultRelease);die;
													$this->view->ps_numb = $msg[0]['PS_NUMBER'];
													$this->view->hidetoken = true;
													if ($resultRelease['status'] == '00'){
														$ns = new Zend_Session_Namespace('FVC');
										    			$ns->backURL = $this->view->backURL;
										    			$this->view->releaseresult = true;
														// $this->_redirect('/notification/success/index');
													}
													else
													{
														// die('here');
														$this->view->releaseresult = false;
														$this->_helper->getHelper('FlashMessenger')->addMessage($result);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
														$this->_redirect('/notification/index/release');
													}
												// }
										}else{
											//die('cek');
											$this->_redirect('/notification/success/index');
										}
				//$this->_redirect('/purchasing/newhandphone/confirm');

			}
			else{
				$errors 	= $msg;
				$error		= true;
			}

			if($error)
			{
				$this->view->error 		= $error;
				$this->view->ERROR_MSG 	= (isset($errors))? $errors : null;
			}
		}
		else if ($this->_getParam('submit') == true)
		{
			$this->_redirect('/purchasing/newhandphone/index');
		}

	}

	public function confirmAction()
	{
		$pdf = $this->_getParam('pdf');
		$sessionNamespace 					= new Zend_Session_Namespace('phone');
		$this->view->ERROR_MSG_N			= $paramSession['paymentMessage'];
		$paramSession 						= $sessionNamespace->paramSession;

		if(!$paramSession)
		{
			$this->cancel();
		}

		if ($this->_getParam('submit') == TRUE)
		{
			$this->_redirect('/purchasing/newhandphone/index');
		}

		$sessionNamespace->paramSession 	= $paramSession;
		$this->view->paramSession			= $paramSession;

		if($pdf)
		{
			$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($paramSession['PSNumber'], $paramSession);
			$this->_helper->download->pdf(null,null,null,'Payment Report Detail',$datapdf);
		}
	}

	public function cancel()
	{
		unset($_SESSION['phone']);
		$this->_redirect("/home/index");
	}

	public function billinquiryAction()
    {
    	// die('here');
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $productCode = $this->_getParam('productCode');
        $sourceAcct = $this->_getParam('sourceAcct');
        $orderId = $this->_getParam('orderId');

	   	$biller 		= new Service_Biller();

	   	$param = array(
	   		'ACCTSRC' => $sourceAcct,
	   		'orderId' => $orderId,
	   		'productCode' => $productCode
	   	);

	   	$res 		= $biller->BillInquiry($param);
	
		// if($res['ResponseCode'] == '00'){
		// 	print_r($res);die();
		// }
		// if(empty($res['ResponseCode']))
		// {
		// 	print_r($res);die();
		// 	$res['ResponseCode'] = '9999';
		// 	$res['ResponseDesc'] = 'Service Call Failure.';
		// }

		echo json_encode($res);
    }
}
