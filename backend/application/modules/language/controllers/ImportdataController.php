<?php
require_once 'Zend/Controller/Action.php';

class language_ImportdataController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	protected $_destinationUploadDir = '';
	protected $_maxRow = '';

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
	}

	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL();
		$model = new language_Model_Index();
		$sql = $model->getUi();
		$arrUi = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				$arrUi += array($val['ID'] => $val['DESC']);
			}
		}
		// print_r($arrUi);die;
		$this->view->arrUi = $arrUi;
		

		if($this->_request->isPost() )
		{
		    $this->_request->getParams();

		    $data = $this->_request->getParams();


			$filter = new Application_Filtering();
			$confirm = false;
			$error_msg[0] = "";

			$BULK_TYPE 	= $filter->filter($this->_request->getParam('ui'), "ui");
			// print_r($BULK_TYPE);die;
			

		if(!empty($BULK_TYPE)){
			// $PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			// $PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$UI 		= $filter->filter($this->_request->getParam('ui'), "ui");
			if(!$UI)
			{
				$error_msg[0] = $this->language->_('UI cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else
			{
				// $paramSettingID = array('range_futuredate', 'auto_release_payment');

				// $settings = new Application_Settings();
				// $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				// $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				// $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				// $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();
				$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					$this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end

						$totalRecords = count($csvData);
						
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}

						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{
								$rowNum = 0;


								foreach ( $csvData as $row )
								{
									// print_r(count($row));die;
									if(count($row)==3)
									{
										$rowNum++;
										$ori = trim($row[0]);
										$indo = strtoupper(trim($row[1]));
										$engh = trim($row[2]);
									
										$this->_db->beginTransaction();
										try{
												$mt = $this->_db->select()
													->from('M_TRANSLATE')
													->where('TEXT = ?', $ori)
													->limit(1)
												;
												
												$mt = $this->_db->fetchRow($mt);
												
												$lastUid = 0;
												if(isset($mt['ID'])){
													$lastUid = (int) $mt['ID'];
												}else{
													$tableMt = 'M_TRANSLATE';
													$bindMt = array(
															'M_TRANSLATE_UI_ID' => $UI,
															'TEXT' => $ori,
															'CREATED' => date('Y-m-d H:i:s'),
															'CREATEDBY' => $this->_userIdLogin,
															'UPDATEDBY' => $this->_userIdLogin,
													);
													$this->_db->insert($tableMt, $bindMt);
													
													$lastUid = $this->_db->lastInsertId($tableMt);
												}
												
												if(!empty($indo)){
													$tableMtd = 'M_TRANSLATE_DETAIL';
												$bindMtd = array(
													'M_TRANSLATE_ID' => $lastUid,
													'M_TRANSLATE_LANG_ID' => '1',
													'TEXT' => $indo,
													'CREATED' => date('Y-m-d H:i:s'),
													'CREATEDBY' => $this->_userIdLogin,
													'UPDATEDBY' => $this->_userIdLogin,
													//'WEB' => 'Frontend',
												);
												$this->_db->insert($tableMtd, $bindMtd);

												}


												if(!empty($engh)){
													$tableMtd = 'M_TRANSLATE_DETAIL';
												$bindMtd = array(
													'M_TRANSLATE_ID' => $lastUid,
													'M_TRANSLATE_LANG_ID' => '2',
													'TEXT' => $engh,
													'CREATED' => date('Y-m-d H:i:s'),
													'CREATEDBY' => $this->_userIdLogin,
													'UPDATEDBY' => $this->_userIdLogin,
													//'WEB' => 'Frontend',
												);
												$this->_db->insert($tableMtd, $bindMtd);
													
												}
												
												
												foreach($params as $key => $val){
													$this->view->{$key} = NULL;
												}
												
												$this->_db->commit();
												// $this->_redirect('language/index');
											}catch(Exception $e){
												print_r($e);die;
												$this->_db->rollBack();
												$error = $e->getMessage();
											}

										
									}
									else
									{
										$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
										break;
									}
								}
							}
							
							else
							{
								$error_msg[0] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}

							
							if(!$error_msg[0])
							{
								$this->_redirect('language/index');
								// $validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								// $resWs = array();

								// if($validate->isError() === false)
								// {
								// 	$confirm = true;

								// 	$validate->__destruct();
								// 	unset($validate);
								// }
								// else
								// {
								// 	$errorMsg 		= $validate->getErrorMsg();
								// 	$errorTrxMsg 	= $validate->getErrorTrxMsg();

								// 	$validate->__destruct();
								// 	unset($validate);

								// 	if($errorMsg)
								// 	{
								// 		$error_msg[0] = $errorMsg;
								// 		$this->view->error 		= true;
								// 		$this->view->report_msg	= $this->displayError($error_msg);
								// 	}
								// 	else
								// 	{
								// 		$confirm = true;
								// 	}
								// }
							}
						}
						else 
						{
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}
					}
				}
				else
				{
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
				}
			}





		


			if($confirm)
			{
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				if($BULK_TYPE=='0'){
					$content['sourceAccountType'] = $sourceAccountType;	
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
					$sessionNamespace->content = $content;
					$this->_redirect('/multicredit/bulk/confirm');
				
				}else if($BULK_TYPE=='1'){
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
					$sessionNamespace->content = $content;
					$this->_redirect('/multidebet/bulk/confirm');

				}else if($BULK_TYPE=='2'){
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
					$sessionNamespace->content = $content;
					$this->_redirect('/payroll/bulk/confirm');
				}else if($BULK_TYPE == '3'){
				
									$resWs = array();
									$err 	= array();
									
									$resultVal	= $validate->checkCreate($arr, $resWs);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();
									
									//Zend_Debug::dump($resWs);die;
									$i = 0;
									foreach($resWs as $key=>$dataAcctType){
										//Zend_Debug::dump($dataAcctType);	
										$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
									}
									
										
									//die;
									
									$sourceAccountType 	= $resWs['accountType'];
									
									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
									$content['sourceAccountType'] 	= $sourceAccountType;
															
									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;
										
									$this->_redirect('/singlepayment/import/confirm');


				}

				
						

		}

			$this->view->BULK_TYPE = $BULK_TYPE;
			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACCTSRC = $ACCTSRC;
			$this->view->PSEFDATE = $PS_EFDATE;
		}
		Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
	}

	private function resData($benefAccount){
			$select	= $this->_db->select()
								->from(array('B'	 			=> 'M_BENEFICIARY'), array('BANK_NAME','BENEFICIARY_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_CITIZENSHIP','BENEFICIARY_RESIDENT','BENEFICIARY_ID_NUMBER','BENEFICIARY_ID_TYPE','BENEFICIARY_CITY_CODE','BENEFICIARY_CATEGORY')
									   );
			$select->where("B.BENEFICIARY_ACCOUNT = ?", $benefAccount);

			$bene = $this->_db->fetchAll($select);
			return $bene;
	}

	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$data = $sessionNamespace->content;
//		die;
		$sourceAcct = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']];

		$this->view->CCY = $sourceAcct['CCY_ID'];
		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'].' / '.$sourceAcct['ACCT_NAME'].' / '.$sourceAcct['CCY_ID'];
		//if($sourceAcct['ACCT_ALIAS']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS'];

		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];

		$chargesAmt = array();
		$totalChargesAmt = 0;

		foreach($data['paramTrxArr'] as $row)
		{
//			$benefAccount = $row["ACBENEF"];
//			if($row["TRANSFER_TYPE"] !="PB"){
//				$dataRes = $this->resData($benefAccount);
//
//				$row['BENEFICIARY_ID_NUMBER'] = $dataRes[0]['BENEFICIARY_ID_NUMBER'];
//				$row['BENEFICIARY_ID_TYPE'] = $dataRes[0]['BENEFICIARY_ID_TYPE'];
//				$row['BENEFICIARY_CITY_CODE'] = $dataRes[0]['BENEFICIARY_CITY_CODE'];
//				$row['BENEFICIARY_CATEGORY'] = $dataRes[0]['BENEFICIARY_CATEGORY'];
//				$row['BENEFICIARY_BANK_NAME'] = $dataRes[0]['BANK_NAME'];
//				$row['BENEFICIARY_CITIZENSHIP'] = $dataRes[0]['BENEFICIARY_CITIZENSHIP'];
//				$row['BENEFICIARY_RESIDENT'] = $dataRes[0]['BENEFICIARY_RESIDENT'];
//				$row['BENEFICIARY_ACCOUNT_NAME'] = $dataRes[0]['BENEFICIARY_NAME'];
//			}

			if(!isset($chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']])) $chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] = 0;
			//$chargesObj = Charges::factory($this->_custIdLogin, $row['TRANSFER_TYPE']);

//			if(empty($row['TRANSFER_TYPE'])){
//				$transferType = '0';
//			}
//			else{
//				$transferType = $row['TRANSFER_TYPE'];
//			}
			$chargesObj = Charges::factory($this->_custIdLogin, isset($row['TRANSFER_TYPE']));

			$paramCharges = array("accsrc" => $row['ACCTSRC'], "transferType" => $row['TRANSFER_TYPE']);
			if(!isset($data['errorTrxMsg'][$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']]))
			{
// 				$chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] += $chargesObj->getCharges($paramCharges);
// 				$totalChargesAmt += $chargesObj->getCharges($paramCharges);
			}
		}
		$this->view->chargesAmt = $chargesAmt;
		$this->view->totalChargesAmt = $totalChargesAmt;

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$settings = new Settings();
		$this->view->cutOffSKN = $settings->getSetting('cut_off_time_skn');
		$this->view->cutOffRTGS = $settings->getSetting('cut_off_time_rtgs');
		$this->view->cutOffBI = $settings->getSetting('cut_off_time_bi');

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/multicredit/bulk');
			}

			if ($data["payment"]["countTrxPB"] == 0)
				$priviCreate = 'CBPI';
			else
				$priviCreate = 'CBPW';

			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= $this->_paymenttype['code']['bulkcredit'];
			$param['PS_CCY']  = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];

			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
						'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
						'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
						'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
						'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
//						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'], //ADA
						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
						'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
						'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'], //ADA
						'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
						//'ACBENEF_ADDRESS1' 			=> $row['ACBENEF_ADDRESS1'],
						'BENEFICIARY_RESIDENT' 		=> $row['BENEFICIARY_RESIDENT'],
						'CLR_CODE' 					=> $row['BANK_CODE'],
						'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'], //SKN,RTGS,PB
						'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
						'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
						'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
						'TRA_REFNO' 				=> $row['TRA_REFNO'],
						'sourceAccountType' 		=> $data['sourceAccountType'],

						'BENEFICIARY_BANK_NAME' 	=> $row['BANK_NAME'], //ADA
						'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
						'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
						'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
						'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],

				);
			}

			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = $priviCreate;
			//$param['sourceAccountType'] = $data['sourceAccountType'];


			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
//			Zend_Debug::dump($param);die;
			$result = $BulkPayment->createPayment($param);
			if($result)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/notification/success');
			}
			else
			{
				$this->view->error = true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/multicredit/bulk');
			}
		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}
}
