<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';
require_once 'General/ExchangeRate.php';
require_once 'Service/TransferCharge.php';

set_time_limit(0);
$db = Zend_Db_Table::getDefaultAdapter();

$config    		= Zend_Registry::get('config');
$monthlyFee 	= $config["monthlyfee"]["type"]; 
$chargesType 	= $config["charges"]["type"]["code"]; 

$setting 		= new Settings();
$auto_enabled 	= $setting->getSetting("auto_monthlyfee", 0);
$logPaymentType = "admfeeaccount";
$monthlyFeeType = $monthlyFee["code"][$logPaymentType];
$mainDesc		= $monthlyFee["desc"][$logPaymentType];
$rate			= new ExchangeRate();

echo "<pre>";
echo "<br>auto_enabled: ".$auto_enabled."<br>";
print_r($monthlyFee);
print_r($chargesType);

if($auto_enabled == 1) //auto admin fee is enabled, run script
{
	// 1. get COA Account (account to receive money charged), and it's currency   charges_acct_IDR
	$coa_ccy 	 = "IDR";
	$coa_account = $setting->getSetting("charges_acct_".$coa_ccy);
	
	echo "coa account: ".$coa_account."<br>";
	
	if (!empty($coa_account))
	{
		// 2. query to get companies with comp_admfeestatus=1
		$select	= $db->select()
					 ->from(array('A'	 		=> 'M_CUSTOMER'),
						    array('CUST_ID' 	=> 'A.CUST_ID',
						   		  'CUST_NAME' 	=> 'A.CUST_NAME',
						   		 )
						    )
					 ->where("A.CUST_STATUS != '3'")				// company status, not deleted
					 ->where("A.CUST_MONTHLYFEE_STATUS = '1'")		// status is enabled
					 ->where("A.CUST_MONTHLYFEE_TYPE   = ?", $monthlyFeeType);				
		$companies = $db->fetchAll($select);
		//$sqlstr = "SELECT comp_accid,comp_admfeetype FROM sb_company WHERE comp_admfeestatus=1";
		
		print_r($companies);
		
		$number_of_account_charged_success = 0;
		$number_of_account_charged_failed  = 0;
		foreach($companies as $company)
		{
			$company_id   = $company["CUST_ID"];
			$company_name = $company["CUST_NAME"];
			
			// 3. Get all accounts that need to be charged for this company, last charged < this month
			$select	= $db->select()
						 ->from(array('A'	 		 => 'M_ADMFEE_MONTHLY'),
							    array('ACCT_NO' 	 => 'A.ACCT_NO',
							   		  'CHARGES_ACCT' => 'A.CHARGES_ACCT_NO',
							   		  'CHARGES_CCY'  => 'C.CCY_ID',
							   		  'AMOUNT' 		 => 'A.AMOUNT',
							   		  'CHARGES_ACCT_NAME' => 'C.ACCT_NAME',
							   		 )
							    )
						 ->join(array('C' => 'M_CUSTOMER_ACCT'), 'A.CHARGES_ACCT_NO = C.ACCT_NO', array())	// TODO: yg dicek status deleted?
						 ->where("A.CUST_ID      = ?", $company_id)			
						 ->where("A.MONTHLYFEE_TYPE = ?", $monthlyFeeType)				
						 ->where("C.ACCT_STATUS != 3")				
						 ->where("IFNULL(TIMESTAMPDIFF(MONTH, A.LAST_EXECUTED, now()), 1) > 0");			
			$accounts = $db->fetchAll($select);
			
//			$sqlstr=
//			"SELECT sb_admfeeperaccount.accsrc,amount,
//			sb_accsrc.curr_code,curr_num
//			FROM sb_admfeeperaccount
//			LEFT JOIN sb_accsrc ON sb_admfeeperaccount.accsrc=sb_accsrc.accsrc
//			LEFT JOIN sb_currency ON sb_accsrc.curr_code = sb_currency.curr_code
//			LEFT JOIN sb_company ON sb_admfeeperaccount.comp_accid = sb_company.comp_accid
//			WHERE sb_admfeeperaccount.comp_accid='".$comp_accid[$i]."' 
//			AND sb_company.comp_status != 3 AND sb_accsrc.accsrc_status != 3 
//			AND (DATE(last_executed) < DATE( concat( YEAR( NOW( ) ) , '-' , MONTH( NOW( ) ) , '-01' ) ) )
//			ORDER BY accsrc";
			
			//echo $select->__toString();
			echo "<br>Accounts:<br>";
			print_r($accounts);
			
			foreach($accounts as $account)
			{
				$account_no   	= $account["ACCT_NO"];
				$charges_acct 	= $account["CHARGES_ACCT"];
				$charges_acct_name = $account["CHARGES_ACCT_NAME"];
				$charges_ccy 	= $account["CHARGES_CCY"];
				$amount 		= $account["AMOUNT"];
				
				$LOB		= Application_Helper_General::getLOB($charges_acct, $charges_ccy);
				$realCOA 	= str_replace("XX", $LOB, $coa_account);
				
				if($amount <= 0) //if amount to charge is 0, skip account
				{
					//UPDATE 26MAY2011 - DO NOT UPDATE LAST_EXECUTED IF CHARGING FAILS
					/*
					$sqlstr=
					"UPDATE sb_admfeeperaccount SET ".
					sqlSet("last_executed","NOW()","n",0).
					" WHERE accsrc='".$accsrc[$j]."' AND comp_accid='".$comp_accid[$i]."'";
					$jRs3->jExecuteSql($sqlstr);
					*/
					
					$description = $mainDesc.". Account : ".$account_no." charge amount is 0.";
					
					// insert log
					$logData = array(	'LOG_DATETIME' 		=> new Zend_Db_Expr("now()"),
										'CUST_ID' 			=> $company_id,
										'ACCT_NO' 			=> $account_no,
										'CHARGES_ACCT_NO' 	=> $charges_acct,
										'CCY' 				=> $charges_ccy,
										'AMOUNT' 			=> 0,
										'COA' 				=> $realCOA,
										'PAYMENT_TYPE' 		=> $logPaymentType,
										'CHARGES_TYPE' 		=> $chargesType["pb"],
										'DESCRIPTION' 		=> $description,
										'STATUS' 			=> 0,	// charge failed
									);
					$db->insert('T_CHARGES_LOG', $logData);
				}
				else	// amount > 0
				{
					//if source account is not IDR, convert to IDR
//					if ($coa_ccy != $charges_ccy)
//					{
//						$amountInCoaCCY = $rate->convertAmount($amount, $charges_ccy, $coa_ccy);
//					}
//					else
//					{
//						$amountInCoaCCY = $amount;
//					}
					
//					if ($amountInCoaCCY === false)
//					{
//						// failed rate
//						$number_of_account_charged_failed++;
//						
//						//UPDATE 26MAY2011 - DO NOT UPDATE LAST_EXECUTED IF CHARGING FAILS
//						/*
//						$sqlstr=
//						"UPDATE sb_admfeeperaccount SET ".
//						sqlSet("last_executed","NOW()","n",0).
//						" WHERE accsrc='".$accsrc[$j]."' AND comp_accid='".$comp_accid[$i]."'";
//						$jRs3->jExecuteSql($sqlstr);
//						*/
//						
//						$rate_err = $rate->getRateInfo();
//						$rate_err_msg = "";
//						
//						if (!empty($rate_err["ERROR_MSG"]))
//						{	$rate_err_msg = implode("<br>", $rate_err["ERROR_MSG"]);		}
//						
//						$description = $mainDesc.". Check rate failed. Error: ".$rate_err_msg;
//					
//						// insert log
//						$logData = array(	'LOG_DATETIME' 		=> new Zend_Db_Expr("now()"),
//											'CUST_ID' 			=> $company_id,
//											'ACCT_NO' 			=> $account_no,
//											'CHARGES_ACCT_NO' 	=> $charges_acct,
//											'CCY' 				=> $charges_ccy,
//											'AMOUNT' 			=> $amount,
//											'PAYMENT_TYPE' 		=> $logPaymentType,
//											'CHARGES_TYPE' 		=> $chargesType["pb"],
//											'DESCRIPTION' 		=> $description,
//											'STATUS' 			=> 0,	// charge failed
//										);
//						$db->insert('T_CHARGES_LOG', $logData);
//						
//					}
//					else
//					{
						// rate success
						// $amount  amount to charge
						// skip check balance, in PRK phase CP1
						
						$param = array();	  
						$param['SOURCE_AMOUNT']   			= $amount;  	 
				     	$param['SOURCE_ACCOUNT_CCY'] 		= Application_Helper_General::getCurrNum($charges_ccy); 
				     	$param['SOURCE_ACCOUNT']   			= $charges_acct; 		
				     	$param['BENEFICIARY_ACCOUNT']    	= $realCOA;  		
				     	$param['SOURCE_ACCOUNT_NAME'] 		= $charges_acct_name; 
				     
						$data 			= new Service_TransferCharge($param);
						$sendTransfer 	= $data->sendTransfer();
						
						$uuid = (empty($sendTransfer['RawRequest']->ChannelHeader->messageID))? "": $sendTransfer['RawRequest']->ChannelHeader->messageID;
						
						if ($sendTransfer['ResponseCode'] == '00') //success
						{
							$number_of_account_charged_success++;
							
							//update last_executed to NOW()
							$updateData = array('LAST_EXECUTED' => new Zend_Db_Expr("now()"));
							
							$whereData  = array('CUST_ID 		= ?' => (string) $company_id,
												'ACCT_NO 		= ?' => (string) $account_no,
												'MONTHLYFEE_TYPE   = ?' => (string) $monthlyFeeType,
											    );
							
							$db->update('M_ADMFEE_MONTHLY', $updateData, $whereData); 
							
							$description = $mainDesc.". Successfully charged.";
					
							// insert log
							$logData = array(	'LOG_DATETIME' 		=> new Zend_Db_Expr("now()"),
												'CUST_ID' 			=> $company_id,
												'ACCT_NO' 			=> $account_no,
												'CHARGES_ACCT_NO' 	=> $charges_acct,
												'CCY' 				=> $charges_ccy,
												'AMOUNT' 			=> $amount,
												'COA' 				=> $realCOA,
												'PAYMENT_TYPE' 		=> $logPaymentType,
												'CHARGES_TYPE' 		=> $chargesType["pb"],
												'DESCRIPTION' 		=> $description,
												'TRANSACTION_ID' 	=> $uuid,
												'STATUS' 			=> 1,	// charge success
											);
							$db->insert('T_CHARGES_LOG', $logData);
						}
						else //failed
						{
							$number_of_account_charged_failed++;
							
							//UPDATE 26MAY2011 - DO NOT UPDATE LAST_EXECUTED IF CHARGING FAILS
							/*
							$sqlstr=
							"UPDATE sb_admfeeperaccount SET ".
							sqlSet("last_executed","NOW()","n",0).
							" WHERE accsrc='".$accsrc[$j]."' AND comp_accid='".$comp_accid[$i]."'";
							$jRs3->jExecuteSql($sqlstr);
							*/
							
//							$charger_err=$charger->errormsg($res);
//							$description.='Host Error Message : '.$charger_err;	

							$description = $mainDesc.". Failed charged. Error: ".$sendTransfer['ResponseCode'].":".$sendTransfer['ResponseDesc'];
					
							// insert log
							$logData = array(	'LOG_DATETIME' 		=> new Zend_Db_Expr("now()"),
												'CUST_ID' 			=> $company_id,
												'ACCT_NO' 			=> $account_no,
												'CHARGES_ACCT_NO' 	=> $charges_acct,
												'CCY' 				=> $charges_ccy,
												'AMOUNT' 			=> $amount,
												'COA' 				=> $realCOA,
												'PAYMENT_TYPE' 		=> $logPaymentType,
												'CHARGES_TYPE' 		=> $chargesType["pb"],
												'DESCRIPTION' 		=> $description,
												'TRANSACTION_ID' 	=> $uuid,
												'STATUS' 			=> 0,	// charge success
											);
							$db->insert('T_CHARGES_LOG', $logData);
						}	// end failed charging

//					}	// end rate success
					
				}	// end amount > 0
				
			}	//loop for all account

			echo "<hr>";
		}	//loop for all companies
		
		$total_number_of_accounts = $number_of_account_charged_failed + $number_of_account_charged_success;
		
		$result_msg=
		'Number of accounts processed : '.$total_number_of_accounts.'<br />'.
		'Number of accounts charged successfuly : '.$number_of_account_charged_success.'<br />'.
		'Number of accounts not charged (failed) : '.$number_of_account_charged_failed;

	}
	else
	{
		$result_msg = "<br>COA Account is empty";
	}
}
else //end auto monthly fee is enabled
{
	$result_msg = 'Auto monthly fee is currently disabled';
	//update last_executed to now (free of charge for running month)
	//for all account in sb_admfeeperaccount
	
	//UPDATE 26MAY2011 - DO NOT UPDATE LAST_EXECUTED IF CHARGING FAILS
	/*
	$sqlstr=
	"UPDATE sb_admfeeperaccount SET ".
	sqlSet("last_executed","NOW()","n",0);
	$jRs3->jExecuteSql($sqlstr);
	*/
}

echo $result_msg.'<br><br>';

$filename = basename(__FILE__);
Application_Helper_General::cronLog($filename, $result_msg);
?>