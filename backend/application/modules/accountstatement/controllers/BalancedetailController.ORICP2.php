<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
class accountstatement_BalancedetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$listId = $this->_db->select()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID'))
						->order('CUST_ID ASC')
						->query()->fetchAll();     	
		$this->view->listCustId = Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');

		$cust_id = $this->_getParam('cust_id');
		$cust_name = $this->_getParam('cust_name');
		$accnumber = $this->_getParam('accnumber');
		$filter = $this->_getParam('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		if( ($filter == 'Set Filter' || $csv || $pdf) && ( !empty($cust_id) || !empty($cust_name) ) )
		{
			$this->view->cust_id = $cust_id;
			$this->view->cust_name = $cust_name;
			$this->view->accnumber = $accnumber;

			$query  = $this->_db->SELECT()
												->FROM(array('C' => 'M_CUSTOMER'));
			
			if(!empty($cust_id))
				$query->WHERE("C.CUST_ID = ?",$cust_id);
			if(!empty($cust_name))
				$query->WHERE("C.CUST_NAME like ".$this->_db->quote('%'.$cust_name.'%'));

			$query->ORDER('C.CUST_NAME');
			$arrCustInfo = $this->_db->fetchAll( $query);

			foreach($arrCustInfo as $key=>$custInfo)
			{
				$arrCustname[$key]		= $custInfo['CUST_NAME'];
				$arrCustID[$key]		= $custInfo['CUST_ID'];
				$arrCustadd[$key]		= $custInfo['CUST_ADDRESS'];
				$arrCustcity[$key]		= $custInfo['CUST_CITY'];
				$arrCustprov[$key]		= $custInfo['CUST_PROVINCE'];
				$arrCustzip[$key]			= $custInfo['CUST_ZIP'];
				$arrCustphone[$key]	= $custInfo['CUST_PHONE'];
				$arrCustfax[$key]			= $custInfo['CUST_FAX'];

				$query = $this->_db->select()
													 ->from(array('A' => 'M_CUSTOMER_ACCT'))
													 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
													 ->where("A.ACCT_STATUS = 1")
													 ->where("A.CUST_ID = ?",$custInfo['CUST_ID'])
													 ->order('B.GROUP_NAME DESC')
													 ->order('A.ORDER_NO ASC');
				if($accnumber)
					$query->where("A.ACCT_NO like ".$this->_db->quote('%'.$accnumber.'%'));
					
				$data = $this->_db->fetchAll( $query );

				$isGroup = false;
				$isSystemBalance = false;
				$isTolerance = false;
				if(count($data) > 0)
				{
					foreach($data as $row)
					{
						if($row['GROUP_ID'])
						{
							$isGroup = true;
						}
						if($row['ISSYSTEMBALANCE'])
						{
							$isSystemBalance = true;
						}
						if($row['ISPLAFONDDATE'] && strtotime($row['PLAFOND_START']) <= strtotime(date('Y-m-d')) && strtotime($row['PLAFOND_END']) >= strtotime(date('Y-m-d'))  )
						{
							$isTolerance = true;
						}
					}
					$viewIsGroup[$key] = $isGroup;
					$viewIsSystemBalance[$key] = $isSystemBalance;
					$viewIsTolerance[$key] = $isTolerance;
					$arrCustId[$key] = $custInfo['CUST_ID'];
					$arrResultdata[$key] = $data;
				}
			}
	// SENT TO VIEW =====================================================================		
			$this->view->custname	= $arrCustname;
			$this->view->arrCustID	= $arrCustID;
			$this->view->custadd		= $arrCustadd;
			$this->view->custcity		= $arrCustcity;
			$this->view->custprov		= $arrCustprov;
			$this->view->custzip		= $arrCustzip;
			$this->view->custphone	= $arrCustphone;
			$this->view->custfax		= $arrCustfax;
			$this->view->isGroup 		= $viewIsGroup;
			$this->view->isSystemBalance = $viewIsSystemBalance;
			$this->view->isTolerance = $viewIsTolerance;
			$this->view->arrcustId 	= $arrCustId;
			$this->view->resultdata 	= $arrResultdata;
	// END SENT TO VIEW =====================================================================		

			$logDesc = 'Viewing';

	// PDF & CSV ============================================================================
			if($csv)
			{
				$arr = array();
				if(is_array($arrCustId))
				{
					foreach($arrCustId as $key=>$custId)
					{
						$isSystemBalance 	= $viewIsSystemBalance[$key];
						$isGroup 					= $viewIsGroup[$key];
						$data 							= $arrResultdata[$key];

						$arr[count($arr)+1] = array('Customer ID ',$custId);

						if(!$isSystemBalance){
								$header = array(
									'Account No',
									'Account Alias',
									'Account Name',
									'CCY',
									'Type',
									'Status',
									'Available Balance',
									'Yesterday Clearing',
									'Effective Balance'
								);
						}else{
							if($isTolerance){
								$header = array(
									'Account No',
									'Account Alias',
									'Account Name',
									'CCY',
									'Type',
									'Status',
									'Available Balance',
									'Plafond',
									'Outstanding',
									'Yesterday Clearing',
									'Effective Balance',
									'Tolerance',
									'Tolerance Start Date',
									'Tolerance End Date',
									'Maker Amount Hold',
									'System Balance',
								);
							}else{
								$header = array(
									'Account No',
									'Account Alias',
									'Account Name',
									'CCY',
									'Type',
									'Status',
									'Available Balance',
									'Plafond',
									'Outstanding',
									'Yesterday Clearing',
									'Effective Balance',
									'Tolerance',
									'Maker Amount Hold',
									'System Balance',
								);
							}
						}

						if(!$isGroup)
						{
							$arr[count($arr)+1] = $header;
							$i = count($arr)+2;
							$arr_value_tot_group =  array();
							$arr_display_tot_group =  array();
							foreach($data as $row)
							{
							
								$systemBalance = new SystemBalance($row['CUST_ID'],$row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']));
								$systemBalance->checkBalance();
								
								
								if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
								  $arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getAvailableBalance();
								}
								else {
								  $arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getAvailableBalance();
								}
								$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row['CCY_ID']]);
								
								if(!$isSystemBalance){				
									$arr[$i][] = $row['ACCT_NO'];
									$arr[$i][] = $row['ACCT_ALIAS_NAME'];
									$arr[$i][] = $systemBalance->getCoreAccountName();
									$arr[$i][] = $row['CCY_ID'];
									$arr[$i][] = $row['ACCT_DESC'];
									$arr[$i][] = ($row ['FREEZE_STATUS'] == '09') ? 'Freezed' : $systemBalance->getCoreAccountStatusDesc();
									$arr[$i][] = $systemBalance->getAvailableBalance();
									$arr[$i][] = $systemBalance->getYesterdayClearing();
									$arr[$i][] = $systemBalance->getEffectiveBalance();
								}else{
									$arr[$i][] = $row['ACCT_NO'];
									$arr[$i][] = $row['ACCT_ALIAS_NAME'];
									$arr[$i][] = $systemBalance->getCoreAccountName();
									$arr[$i][] = $row['CCY_ID'];
									$arr[$i][] = $row['ACCT_DESC'];
									$arr[$i][] = ($row ['FREEZE_STATUS'] == '09') ? 'Freezed' : $systemBalance->getCoreAccountStatusDesc();
									$arr[$i][] = $systemBalance->getAvailableBalance();
									$arr[$i][] = $systemBalance->getCorePlafond();
									$arr[$i][] = $systemBalance->getCoreOutstandingPlafond();
									$arr[$i][] = $systemBalance->getYesterdayClearing();
									$arr[$i][] = $systemBalance->getEffectiveBalance();
									
									$plafondStart = $row ['PLAFOND_START'] ? Application_Helper_General::convertDate($row ['PLAFOND_START'],$this->displayDateFormat,$this->defaultDateFormat) : 'N/A';
									$plafondEnd  = $row ['PLAFOND_END'] ? Application_Helper_General::convertDate($row ['PLAFOND_END'],$this->displayDateFormat,$this->defaultDateFormat) : 'N/A';
									$sysBal = ($systemBalance->getSystemBalance()=='N/A') ? $systemBalance->getSystemBalance() : Application_Helper_General::displayMoney($systemBalance->getSystemBalance());
									$tolerance = ($systemBalance->getTolerance()=='N/A') ? $systemBalance->getTolerance() : Application_Helper_General::displayMoney($systemBalance->getTolerance());
									
									$arr[$i][] = $tolerance;
									if($isTolerance){
										$arr[$i][] = $plafondStart;
										$arr[$i][] = $plafondEnd;
									}
									$arr[$i][] = Application_Helper_General::displayMoney($systemBalance->getMakerAmountHold());
									$arr[$i][] = $sysBal;

								}
								$i++;
							}
							$display_tot_group = implode("; ",$arr_display_tot_group);
							$arr[$i][] = 'TOTAL AVAILABLE BALANCE : '.$display_tot_group;	
						}
						else
						{
							$i = count($arr)+1;
							$group = null;
							
							$resultdataGroup =  array();
							foreach ($data as $rowGroup){
								$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup; 
							}
							$i = count($arr)+1;
							$group = null;
							$arr_value_tot_group = array();
							foreach ( $resultdataGroup as $groupname=>$rowPerGroup ) 
							{
								$i++;
								$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
								$groupname = ($groupname) ? $groupname : 'OTHERS';
								$arr[$i][] = $groupname;
								$i++;
								$arr[$i][] = $header;
								
									if(count($rowPerGroup) > 0){
										$arr_value_tot_group =  array();
										$arr_display_tot_group =  array();
										foreach ($rowPerGroup as $row){
										$i++;	
										$systemBalance = new SystemBalance($row['CUST_ID'],$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
										$systemBalance->checkBalance();
											if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
												$arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getAvailableBalance();
											}else{
												$arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getAvailableBalance();
											}
											$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);

										if(!$isSystemBalance){				
											$arr[$i][] = $row['ACCT_NO'];
											$arr[$i][] = $row['ACCT_ALIAS_NAME'];
											$arr[$i][] = $systemBalance->getCoreAccountName();
											$arr[$i][] = $row['CCY_ID'];
											$arr[$i][] = $row['ACCT_DESC'];
											$arr[$i][] = ($row ['FREEZE_STATUS'] == '09') ? 'Freezed' : $systemBalance->getCoreAccountStatusDesc();
											$arr[$i][] = $systemBalance->getAvailableBalance();
											$arr[$i][] = $systemBalance->getYesterdayClearing();
											$arr[$i][] = $systemBalance->getEffectiveBalance();
										}else{
											$arr[$i][] = $row['ACCT_NO'];
											$arr[$i][] = $row['ACCT_ALIAS_NAME'];
											$arr[$i][] = $systemBalance->getCoreAccountName();
											$arr[$i][] = $row['CCY_ID'];
											$arr[$i][] = $row['ACCT_DESC'];
											$arr[$i][] = ($row ['FREEZE_STATUS'] == '09') ? 'Freezed' : $systemBalance->getCoreAccountStatusDesc();
											$arr[$i][] = $systemBalance->getAvailableBalance();
											$arr[$i][] = $systemBalance->getCorePlafond();
											$arr[$i][] = $systemBalance->getCoreOutstandingPlafond();
											$arr[$i][] = $systemBalance->getYesterdayClearing();
											$arr[$i][] = $systemBalance->getEffectiveBalance();
											
											$plafondStart = $row ['PLAFOND_START'] ? Application_Helper_General::convertDate($row ['PLAFOND_START'],$this->view->displayDateFormat,$this->view->defaultDateFormat) : 'N/A';
											$plafondEnd  = $row ['PLAFOND_END'] ? Application_Helper_General::convertDate($row ['PLAFOND_END'],$this->view->displayDateFormat,$this->view->defaultDateFormat) : 'N/A';
											$sysBal = ($systemBalance->getSystemBalance()=='N/A') ? $systemBalance->getSystemBalance() : Application_Helper_General::displayMoney($systemBalance->getSystemBalance());
											$tolerance = ($systemBalance->getTolerance()=='N/A') ? $systemBalance->getTolerance() : Application_Helper_General::displayMoney($systemBalance->getTolerance());
															
											$arr[$i][] = $tolerance;
											if($isTolerance){
												$arr[$i][] = $plafondStart;
												$arr[$i][] = $plafondEnd;
											}
											$arr[$i][] = Application_Helper_General::displayMoney($systemBalance->getMakerAmountHold());
											$arr[$i][] = $sysBal;
											
										}
											
										}
									}
									$i++;	
									$display_tot_group = implode("; ",$arr_display_tot_group);	
									$arr[$i][] = 'TOTAL AVAILABLE BALANCE : '.$display_tot_group;
							}
						}
					}
				}
					$logDesc = 'Export to CSV';
					$this->_helper->download->csv(array(),$arr,null,'Balance Detail');  
			
			}
			
				if($pdf){
					Application_Helper_General::writeLog('BAIQ','Print PDF');
					$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf.phtml')."</td></tr>";
					$this->_helper->download->pdf(null,null,null,'Balance Detail',$outputHTML);
				}
	// PDF & CSV ======================================================================
		}
		else if($filter == 'Clear Filter')
		{
			$this->_redirect('/accountstatement/balancedetail');
		}
	// WRITE LOG ======================================================================
		Application_Helper_General::writeLog('BAIQ',$logDesc);
	}
	
}
