<?php
require_once 'Zend/Controller/Action.php';

class eformworkflow_HeaderhandleController extends Application_Main
{
    protected $_moduleDB = 'RTF';
    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // echo 'hello';
        // return;
        $bg_reg_number = $this->_getParam('bgregnumber');
        $header_name = $this->_getParam('header_name');
        $header_lists = [
            'TypeofBankGuarantee',
            'BankGuaranteeNominalandValidityPeriod',
            'BankGuaranteeRecipient',
            'GuaranteeTransaction',
            'CounterWarranty',
            'ProvisionFeeAdministrationandOtherExpenses',
            'FormatandLanguage',
            'OthersAttachmentDocuments',
        ];

        $temp = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE"], ["*"])
            ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->where('A.BG_REG_NUMBER = ?', $bg_reg_number)
            ->where('A.BG_STATUS IN (2, 22)')
            ->query()->fetchAll();

        $tbg = $this->_db->select()
            ->from(["A" => "T_BANK_GUARANTEE"], ["*"])
            ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->where('A.BG_REG_NUMBER = ?', $bg_reg_number)
            ->where('A.BG_STATUS IN (2, 22)')
            ->query()->fetchAll();

        if ($temp[0]['CHANGE_TYPE'] != '0' && !empty($tbg)) {
            $diff = array_diff($temp[0], $tbg[0]);
            if (!empty($diff)) {
                if (in_array($header_name, $header_lists)) {
                    echo '&nbsp; <span style="font-size: 0.8em;" class="bg-white px-2 border border-danger rounded-circle"><i class="text-danger">i</i></span>';
                }
            }
        }
    }
}
