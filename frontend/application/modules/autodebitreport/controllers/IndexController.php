<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

class autodebitreport_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function setPaymentType($paymentType, $transferType)
	{
		$filterType		 = array();

		foreach($paymentType["code"] as $key => $val)
		{
			//|| $key == "remittance"
			if ($key == "within" || $key == "domestic" || $key == "remittance" || $key == "purchase" || $key == "payment" )
			{
				$filterType[$paymentType["code"][$key]] = $paymentType["desc"][$key];
			}
			elseif ($key == "multicredit")
			{
				$code = $paymentType["code"]["multicredit"].",".$paymentType["code"]["bulkcredit"];
				$desc = $paymentType["desc"][$key];
				$filterType["{$code}"] = $desc;
			}
			elseif ($key == "multidebet")
			{ 
				$code = $paymentType["code"]["multidebet"].",".$paymentType["code"]["bulkdebet"];
				$desc = $paymentType["desc"][$key];
				$filterType["{$code}"] = $desc;
			}
		}

		return $filterType;
	}

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];


		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
        	$selectBank	= $this->_db->select()
			->from(
				array('B' => 'M_BANK_TABLE'),
				array(
					'BANK_CODE'			=> 'B.BANK_CODE',
					'BANK_NAME'			=> 'B.BANK_NAME'
				)
			);

			$bankList = $this->_db->fetchAll($selectBank);

			foreach ($bankList as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}

			$cache->save($bankNameArr,$cacheID);
        }


		

		$this->bankNameList = $bankNameArr;
	}

	public function indexAction()
	{
// print_r($this->_request->getParams());die;
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$range_reporthistory = $setting->getSettingVal('range_reporthistory');
		$this->view->range_reporthistory = $range_reporthistory;
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		
		
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$filter 		= new Application_Filtering();

		$opt[""] = "-- " .$this->language->_('Please Select'). " --";

		$payType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		//$optpaytype 	= $this->setPaymentType($this->_paymenttype,$this->_transfertype);
		$arrPayStatus	= array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		$trfStatus 		= array_combine($this->_transferstatus["code"], $this->_transferstatus["desc"]);

		$arrAccount 	= $CustomerUser->getAccounts();

		foreach($payType as $key => $value){
// 			if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);

			 $optpaytypeRaw[$key] = $this->language->_($value);
		}
		//foreach($trfType as $key => $value){ if($key != 3 && $key != 4) $filterTrfType[$key] = $this->language->_($value); }
		foreach($trfType as $key => $value){ $filterTrfType[$key] = $this->language->_($value); }
		foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }

		if(is_array($arrAccount) && count($arrAccount) > 0){
			foreach($arrAccount as $key => $value){

				$val 		= $arrAccount[$key]["ACCT_NO"];
				$ccy 		= $arrAccount[$key]["CCY_ID"];
				$acctname 	= $arrAccount[$key]["ACCT_NAME"];
				//$acctalias 	= $arrAccount[$key]["ACCT_ALIAS_NAME"];
				$accttype 	= ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';	// 10 : saving, 20 : giro;

				$arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';

			}
		}
		else { $arrAccountRaw = array();}


		unset ($arrPayStatus[10]);
		unset ($arrPayStatus[15]);
		unset ($arrPayStatus[16]);

		$optPayType 	= $opt + $optpaytypeRaw;

		//$optPayStatus 	= $opt + $arrPayStatus;
		$optPayStatus 	= $opt + $optpayStatusRaw;
		unset($optPayStatus[10]);
		unset($optPayStatus[15]);
		unset($optPayStatus[16]);

		//hilangkan payment type payroll, Sweep In, Sweep Out
		$optPayTypePayroll = "11,11";
		$optPayTypeSweepIn = "14,14";
		$optPayTypeSweepOut = "15,15";
		$optPayTypeCredit = "6,4";
		$optPayTypeDebit = "7,5";
		//unset($optPayType[$optPayTypePayroll]);
		//unset($optPayType[$optPayTypeSweepIn]);
		//unset($optPayType[$optPayTypeSweepOut]);

		//unset($optPayType[$optPayTypeDebit]);
		//unset($optPayType[$optPayTypeCredit]);

		$optarrAccount 	= $opt + $arrAccountRaw;

		$this->view->optPayType 	= $optPayType;
		$this->view->optPayStatus 	= $optPayStatus;
		$this->view->optarrAccount 	= $optarrAccount;

		$fields = array	(	'payReff'  					=> array	(
																		'field' => 'payReff',
																		'label' => $this->language->_('Payment Ref').'# - '.$this->language->_('Trans ID').' - '.$this->language->_('Subject'),
																		'sortable' => true
																),
							'efdate'  					=> array	(
																		'field' => 'efdate',
																		'label' => $this->language->_('Payment Date'),
																		'sortable' => true
																	),
							'updated'  					=> array	(
																		'field' => 'updated',
																		'label' => $this->language->_('Last Updated'),
																		'sortable' => true
																	),
							// 'created'  					=> array	(
							// 											'field' => 'created',
							// 											'label' => $this->language->_('Created Date'),
							// 											'sortable' => true
							// 										),
							//'paySubj'  					=> array	(
							//											'field' => 'paySubj',
							//											'label' => $this->language->_('Subject'),
							//											'sortable' => true
							//										),
							'clientreff'  		=> array('field' 	=> 'client',
												 'label' 	=> $this->language->_('Client Reff').' / '.$this->language->_('Client Name'),
												 'sortable' => true),
							'accsrc'  		=> array('field' 	=> 'accsrc',
												 'label' 	=> $this->language->_('Debited From Account'),
												 'sortable' => true),
							'acbenef'  		=> array('field' 	=> 'acbenef',
												 'label' 	=> $this->language->_('Credited to Account'),
												 'sortable' => true),
							/*'accsrc_name' 				=> array	(
																		'field' => 'accsrc_name',
																		'label' => $this->language->_('Source Account Name'),
																		'sortable' => true
																	),*/
							
							/*'acbenef_name'  			=> array	(
																		'field' => 'acbenef_name',
																		'label' => $this->language->_('Beneficiary Name'),
																		'sortable' => true
																	),*/
							//'numtrx' 					 => array	(
							//											'field' => 'numtrx',
							//											'label' => '#'.$this->language->_('Trans <br>'),
							//											'sortable' => true
							//										),
							'amount'  			=> array('field' 	=> 'amount',
												 'label' 	=> $this->language->_('Amount'),
												 'sortable' => true),
							/*'amount'  					=> array	(
																		'field' => 'amount',
																		'label' => $this->language->_('Amount'),
																		'sortable' => true
																	),*/
							
							
							// 'payStatus'    				=> array	(
							// 											'field' => 'payStatus',
							// 											'label'  => $this->language->_('Payment Status'),
							// 											'sortable' => true
							// 										),
							//'payType'  					=> array	(
							//											'field'	=> 'payType',
							//											'label' 	=> $this->language->_('Payment - Trf Type'),
							//											'sortable' => true
							//										),
							//'payStatus'  					=> array	(
							//											'field'	=> 'payStatus',
							//											'label' 	=> $this->language->_('Payment - Trans Status'),
							//											'sortable' => true
							//										),
						);
// ,'SOURCE_ACCOUNT_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_ACCOUNT_NAME','TRANSACTION_ID',,'PS_CCY'
		$filterlist = array('PS_CREATED','PS_NUMBER','PS_SUBJECT','SOURCE_ACCOUNT','BENEFICIARY_ACCOUNT','PS_EFDATERPT','PS_UPDATED','PS_STATUS','PS_TYPE','TRANSFER_TYPE');

		$this->view->filterlist = $filterlist;

		// print_r($this->_request->getParams());die;
		//get page, sortby, sortdir
		$page    		= $this->_getParam('page');
		$sortBy  		=($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updated');
		$sortDir 		= $this->_getParam('sortdir');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');

		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');

		$this->view->sortBy			= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;

		//validate parameters before passing to view and query
		$page 		= (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		$sortBy 	= (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';

		$this->view->currentPage = $page;

		$filterArr = array(	'PS_UPDATED' 	=> array('StringTrim','StripTags'),
							'PS_EFDATERPT' 	=> array('StringTrim','StripTags'),
							'PS_CREATED' 	=> array('StringTrim','StripTags'),
							'PS_SUBJECT'		=> array('StringTrim','StripTags'),
							'SOURCE_ACCOUNT' 	=> array('StringTrim','StripTags'),
							'BENEFICIARY_ACCOUNT' 	=> array('StringTrim','StripTags'),

							'PS_UPDATED_END' 	=> array('StringTrim','StripTags'),
							'PS_EFDATE_END' 	=> array('StringTrim','StripTags'),
							'PS_CREATED_END' 		=> array('StringTrim','StripTags'),

							'PS_NUMBER' 		=> array('StringTrim','StripTags','StringToUpper'),
							// 'TRANSACTION_ID'	=> array('StringTrim','StripTags'),
							// 'PS_CCY'	=> array('StringTrim','StripTags'),
							'PS_STATUS'	=> array('StringTrim','StripTags'),
							// 'PS_TYPE' 	=> array('StringTrim','StripTags'),
							'TRANSFER_TYPE' 	=> array('StringTrim','StripTags'),


		);

		// print_r($filterArr);die;
		// if POST value not null, get post, else get param ,"SOURCE_ACCOUNT_NAME","BENEFICIARY_ACCOUNT","BENEFICIARY_ACCOUNT_NAME","TRANSACTION_ID","PS_CCY"
		$dataParam = array('PS_SUBJECT',"SOURCE_ACCOUNT",'BENEFICIARY_ACCOUNT',"PS_NUMBER","PS_STATUS","TRANSFER_TYPE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					} 

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
			if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}
			if(!empty($this->_request->getParam('updatedate'))){
				$updatearr = $this->_request->getParam('updatedate');
					$dataParamValue['PS_UPDATED'] = $updatearr[0];
					$dataParamValue['PS_UPDATED_END'] = $updatearr[1];
			}

			if(!empty($this->_request->getParam('efdaterpt'))){
				$efdatearr = $this->_request->getParam('efdaterpt');
					$dataParamValue['PS_EFDATERPT'] = $efdatearr[0];
					$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
			}




		// print_r($dataParamValue);die;
		// The default is set so all fields allow an empty string
		$options = array('allowEmpty' => true);
		$validators = array(
						'PS_UPDATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_EFDATERPT' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_CREATED'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),

						'PS_UPDATED_END' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_EFDATE_END' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_CREATED_END'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),


						'PS_SUBJECT'				=> array(),
						// 'SOURCE_ACCOUNT' 		=> array(array('InArray', array('haystack' => array_keys($optarrAccount)))),
						'SOURCE_ACCOUNT' 		=> array(),
						'BENEFICIARY_ACCOUNT' 		=> array(),
						// 'SOURCE_ACCOUNT_NAME'		=> array(),		// $filter!
						// 'BENEFICIARY_ACCOUNT'		=> array(),
						// 'BENEFICIARY_ACCOUNT_NAME'		=> array(),
						'PS_NUMBER'		=> array(),
						// 'TRANSACTION_ID'		=> array(),
						// 'PS_CCY'		=> array(),
						// 'PS_STATUS'		=> array(),
						'PS_STATUS' => array(array('InArray', array('haystack' => array_keys($optPayStatus)))),	// $filter!
						//'PS_TYPE' 	=> array(array('InArray', array('haystack' => array_keys($optPayType)))),	// $filter!
						// 'PS_TYPE' 	=> array(),	// $filter!
						'TRANSFER_TYPE' => array(array('InArray', array('haystack' => array_keys($filterTrfType)))),	// $filter!

						);

		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);


		$fUpdatedStart 	= $zf_filter->getEscaped('PS_UPDATED');
		$fUpdatedEnd 	= $zf_filter->getEscaped('PS_UPDATED_END');
		$fPaymentStart 	= $zf_filter->getEscaped('PS_EFDATERPT');
		$fPaymentEnd 	= $zf_filter->getEscaped('PS_EFDATE_END');

		$fCreatedStart 	= $zf_filter->getEscaped('PS_CREATED');
		$fCreatedEnd 	= $zf_filter->getEscaped('PS_CREATED_END');
		// print_r($fCreatedEnd);die;


		$subject 		= $zf_filter->getEscaped('PS_SUBJECT');
		$fAcctsrc 		= $zf_filter->getEscaped('SOURCE_ACCOUNT');
		$fBenefsrc 		= $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');

		$fPaymentReff 	= $zf_filter->getEscaped('PS_NUMBER');
		$fPaymentStatus = $zf_filter->getEscaped('PS_STATUS');
		// $fPaymentType 	= $zf_filter->getEscaped('PS_TYPE');
		$fTransferType 	= $zf_filter->getEscaped('TRANSFER_TYPE');


		if($filter == NULL && $clearfilter != 1){
//			$fUpdatedStart 	= date("d/m/Y");
//			$fUpdatedEnd 	= date("d/m/Y");

			// echo "f0 c1";
		}
		else{

			if($filter != NULL){
				$fUpdatedStart 	= $zf_filter->getEscaped('PS_UPDATED');
				$fUpdatedEnd 	= $zf_filter->getEscaped('PS_UPDATED_END');
				// echo "f1 c0";
			}
			else if($clearfilter == 1){
				$fUpdatedStart 	= "";
				$fUpdatedEnd 	= "";
				// echo "f0 c0";
			}
		}


		//jika tombol csv dan pdf ditekan
		if($pdf || $csv)
		{
		    $fUpdatedStart 	= $zf_filter->getEscaped('PS_UPDATED');
		    $fUpdatedEnd 	= $zf_filter->getEscaped('PS_UPDATED_END');
		}

		$paramPayment = array("WA" 				=> false,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );

		// get payment query
		$select   = $CustomerUser->getPayment($paramPayment);

		// Filter Data
		if($fUpdatedStart)
		{
			$FormatDate 	= new Zend_Date($fUpdatedStart, $this->_dateDisplayFormat);
			$updatedFrom   	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_UPDATED) >= ?', $updatedFrom);
		}

		if($fUpdatedEnd)
		{
			$FormatDate 	= new Zend_Date($fUpdatedEnd, $this->_dateDisplayFormat);
			$updatedTo   	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_UPDATED) <= ?', $updatedTo);
		}

		if($fCreatedStart)
		{
			$FormatDate 	= new Zend_Date($fCreatedStart, $this->_dateDisplayFormat);
			$createdFrom   	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_CREATED) >= ?', $createdFrom);
		}

		if($fCreatedEnd)
		{
			$FormatDate 	= new Zend_Date($fCreatedEnd, $this->_dateDisplayFormat);
			$createdTo   	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_CREATED) <= ?', $createdTo);
		}

		if($fPaymentStart)
		{
			$FormatDate 	= new Zend_Date($fPaymentStart, $this->_dateDisplayFormat);
			$payDateFrom   	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) >= ?', $payDateFrom);
		}

		if($fPaymentEnd)
		{
			$FormatDate 	= new Zend_Date($fPaymentEnd, $this->_dateDisplayFormat);
			$payDateTo  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) <= ?', $payDateTo);
		}

		if($subject)
		{	
			$subjectArr = explode(',', $subject);
					$select->where("UPPER(P.PS_SUBJECT)  in (?)",$subjectArr );

		//	$select->where("P.PS_SUBJECT LIKE ".$this->_db->quote('%'.$subject.'%'));	
		}

		if($fAcctsrc)
		// {	$select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and SOURCE_ACCOUNT = ?) > 0", (string)$fAcctsrc);		}
		{	//$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fAcctsrc.'%'));	
			$fAcctsrcArr = explode(',', $fAcctsrc);
					$select->where("T.SOURCE_ACCOUNT  in (?)",$fAcctsrcArr );
		}	// ACCSRC ?????

		if($fBenefsrc)
		{	//$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBenefsrc.'%'));	
			$fBenefsrcArr = explode(',', $fBenefsrc);
					$select->where("T.BENEFICIARY_ACCOUNT  in (?)",$fBenefsrcArr );
		}

		if($fPaymentReff)
		{	
			$fPaymentReffArr = explode(',', $fPaymentReff);
					$select->where("P.PS_NUMBER  in (?)",$fPaymentReffArr );
			//$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPaymentReff.'%'));	}
		}
		
		if($fPaymentStatus)
		{
			//$fPayStatus	 	= explode(",", $fPaymentStatus);
			$fPaymentStatusArr = explode(',', $fPaymentStatus);
					$select->where("P.PS_STATUS  in (?)",$fPaymentStatusArr );
			//$select->where("P.PS_STATUS = ? ", $fPaymentStatus);
		}
		//var_dump($fPaymentType);
		// if($fPaymentType)
		// {
		// 	$fPayType 	 	= explode(",", $fPaymentType);
		// 	$select->where("P.PS_TYPE in (?) ", $fPayType);

		// }

		if($fTransferType != '')
		{
		//	$select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and TRANSFER_TYPE = ?) > 0", (string)$fTransferType);
			$select->where("T.TRANSFER_TYPE = ? ", $fTransferType);
		}
//		 echo "<pre>";
//		 echo $select;die;

		$select->where("P.PS_TYPE in (27,28)");
		$select->where("P.PS_STATUS IN (4,5,6,9,21)");
		//$select->group("T.PS_NUMBER");
		$select->order("T.REEXECUTE_INDEX DESC");

		// $select->order($sortBy.' '.$sortDir);

		// echo $select;die();

		$dataSQL = $this->_db->fetchAll($select);

		// echo "<pre>";

		// var_dump($dataSQL);die;

		if ($csv || $pdf || $this->_request->getParam('print'))
		{	$header  = Application_Helper_Array::simpleArray($fields, "label");		}
		else
		{
			// $this->paging($dataSQL);
			// $this->view->paginator = $dataSQL;
		}

		$data = array();
//echo '<pre>';
//var_dump($dataSQL);
		foreach ($dataSQL as $d => $dt)
		{
							//print_r($dt);die;
			$persenLabel = $dt["BALANCE_TYPE"] == '2' ? ' %' : '';
			foreach ($fields as $key => $field)
			{
				$value 	   = $dt[$key];
				$PSSTATUS  = $dt["PS_STATUS"];
				$PSNUMBER  = $dt["payReff"];
				$payStatus = $dt["payStatus"];

				//if reexecute then, number trx = 1, biar gk keliatan spt bulk
				if ($dt['numtrx'] > 0 && ($dt['PS_CATEGORY'] == 'SINGLE PAYMENT' || $dt['PS_CATEGORY'] == 'OPEN TRANSFER')) {
					$dt['numtrx'] = 1;
				}
				
				if($key =='acbenef' && ($dt['PS_TYPE']=='12' || $dt['PS_TYPE']=='16' || $dt['PS_TYPE']=='17')){
					$value = '-';
				}
				
				//if($key == 'payReff'){
					//$value = $dt['PS_NUMBER'].' - '.$dt['TRANSACTION_ID'];
				//}
				
				elseif ($key == "accsrc" && $value != "-"){
					if($dt['PS_TYPE'] == '4'){
						$value = '-';
					}else{
						if (!empty($dt['BENEFICIARY_BANK_NAME'])) {
							$bankName = $dt['BENEFICIARY_BANK_NAME'];
						}else if(!empty($dt['BENEF_ACCT_BANK_CODE'])){
							$bankName = $this->bankNameList[$dt['BENEF_ACCT_BANK_CODE']];
						}else{
							$bankName = $this->_bankName;
						}
						

						if(empty($dt["acbenef_alias"]) || $dt["acbenef_alias"] == '-'){
							$value = $dt['acbenef'].'('.$dt['acbenef_ccy'].') /'."<br>".$bankName.' / <br>'.$dt['acbenef_name']." (S)";
						}
						else{
							$value = $dt['acbenef'].'('.$dt['acbenef_ccy'].') /'."<br>".$bankName.' / <br>'.$dt['acbenef_alias']." (A)";
						}
					}
				}
				
				
				if($key == "clientreff"){
					$selectclient = $this->_db->select()
											->from('T_DIRECTDEBIT',array('*'))
											->where("COMP_ID = ?",$this->_custIdLogin)
											->where("DEBIT_ACCT = ?",$dt["acbenef"]);
											
											
						$clientdata = $this->_db->fetchRow($selectclient);
						$value = $clientdata['CLIENT_REFF'].' / '.$clientdata['CLIENT_NAME'];
						//var_dump($clientdata);die;
				}
				
				

				if ($key == "payType"){
					
                    $dt['TRANSFER_TYPE1'] = $trfType[$dt['TRANSFER_TYPE']];

                    $tra_type	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");

                    $tra_type1 = $tra_type[$dt['TRANSFER_TYPE']];
                    
                    if ($dt['PS_TYPE'] == 19) {
                         $payType = 'CP Same Bank Remains';
                    }else if ($dt['PS_TYPE'] == 20) {
                         $payType = 'CP Same Bank Maintains';
                    }else if ($dt['PS_TYPE'] == 23) {
                         $payType = 'CP Others Remains - '.$tra_type1;
                    }else if ($dt['PS_TYPE'] == 21) {
                         $payType = 'MM - '.$tra_type1;
                    }else if($dt['PS_TYPE'] == '25' || $dt['PS_TYPE'] == '4' || $dt['PS_TYPE'] == '26' || $dt['PS_TYPE'] == '28' || $dt['PS_TYPE'] == '3' || $dt['PS_TYPE'] == '27'){
						$payType = $dt['payType'];
					}else {
                        $payType = $dt['payType'] . ' - ' . $dt['TRANSFER_TYPE1'];
                    }

                    $value = $payType;
				}

/*
				if ($key == "payStatus"){
					if($PSSTATUS == 5){

						// COMPLETED WITH (_) TRANSACTION (S) FAILED
						// TRA_STATUS FAILED (4)

						$select = $this->_db->select()
											->from('T_TRANSACTION',array('countfailed'=>'count(TRANSACTION_ID)'))
											->where("TRA_STATUS = '4' AND PS_NUMBER = ?",$PSNUMBER);
						$countFailed = $this->_db->fetchOne($select);

						if($countFailed == 0) 	$value = $payStatus .' - '.$trfStatus[$dt['TRA_STATUS']];
						else 					$value = 'Completed with '.$countFailed.' Failed Transaction(s)';

					}
					else $value = $payStatus.' - '.$trfStatus[$dt['TRA_STATUS']];

				} */

				/*if ($key == "amount" && !$csv)	{
					//print_r($dt['PS_STATUS']);die;
					if(!empty($persenLabel)){
						if(($dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15')){
							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
							$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';
							}else{
							$value = '-';
							}
						}else{
							$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';

						}
					}elseif(($dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15') ){

							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
							$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
							}else{
							$value = '-';
							}
					}else{
						$value = Application_Helper_General::displayMoney($value);
					}


				}
				elseif ($key == "amount" && $csv)	{
						if(!empty($persenLabel)){
						if(($dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15')){
							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
							$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';
							}else{
							$value = '-';
							}
						}else{
							$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';

						}
						}elseif(($dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15')){
							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
							$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
							}else{
							$value = '-';
							}

						}else{
							$value = Application_Helper_General::displayMoney($value);
						}
					}*/
				// elseif ($key == "created")		{ $value = Application_Helper_General::convertDate($value,$this->view->displayDateTimeFormat); }
				elseif ($key == "efdate"){ 
					if (empty($value)) {

						$value = '-';

					}else{

						$value = Application_Helper_General::convertDate($value,$this->_dateViewFormat,$this->view->defaultDateFormat);

					} 
				}
				elseif ($key == "updated")		{ $value = Application_Helper_General::convertDate($value,$this->view->displayDateTimeFormat); }
				elseif (($key == "acbenef") && $value != "-")
				{
					/*$value = $value."[".$dt[$key.'_ccy']."]";*/
					if($dt['numtrx'] > 1 && $dt['PS_TYPE'] == '4'){
						if ($dt['SOURCE_ACCT_BANK_CODE'] == '') {
							$bankName = $this->_bankName;
						}
						else{
							$bankName = $this->bankNameList[$dt['SOURCE_ACCT_BANK_CODE']];
						}

						if(empty($dt["acct_alias"]) || $dt["acct_alias"] == '-'){
							$value = $dt['accsrc'].'('.$dt['accsrc_ccy'].') /'."<br>".$bankName.' / <br>'.$dt['accsrc_name'];
						}
						else{
							$value = $dt['accsrc'].'('.$dt['accsrc_ccy'].') /'."<br>".$bankName.' / <br>'.$dt['accsrc_alias'];
						}
					}else if($dt['numtrx'] > 1 && $dt['PS_TYPE']!='11'){
						$value = '-';
					}else{

						if ($dt['SOURCE_ACCT_BANK_CODE'] == '') {
							$bankName = $this->_bankName;
						}
						else{
							$bankName = $this->bankNameList[$dt['SOURCE_ACCT_BANK_CODE']];
						}

						if(empty($dt["acct_alias"]) || $dt["acct_alias"] == '-'){
							$value = $dt['accsrc'].'('.$dt['accsrc_ccy'].') /'."<br>".$bankName.' / <br>'.$dt['accsrc_name'];
						}
						else{
							$value = $dt['accsrc'].'('.$dt['accsrc_ccy'].') /'."<br>".$bankName.' / <br>'.$dt['accsrc_alias'];
						}
					}
				}
				elseif ($key == "amount" && $value != "-"){
					if($dt['PS_TYPE']=='19' || $dt['PS_TYPE']=='20' || $dt['PS_TYPE']=='23'){
						$value = '-';
					}else{
						$value = $dt['ccy']." ".Application_Helper_General::displayMoney($dt['amount']);
					}
				}

				// elseif ($key == "accsrc_name")	{
					// $alias_name  = (!empty($dt["accsrc_alias"]) && $dt["accsrc_alias"] != "-")? " / ".$dt["accsrc_alias"]: "";
					// $value		 = trim($value).$alias_name;
				// }
				// elseif ($key == "acbenef_name"){
					// $alias_name  = (!empty($dt["acbenef_alias"]) && $dt["acbenef_alias"] != "-")? " / ".$dt["acbenef_alias"]: "";
					// $value		 = trim($value).$alias_name;
				// }

				$value = ($value == "" && !$csv)? "": $value;
				$data[$d]['paySubj'] = $dt['PS_SUBJECT'];
				$data[$d]['TRANSACTION_ID'] = $dt['TRANSACTION_ID'];
				$data[$d][$key] = $value;
			}

		}

		unset($fields['paySubj']);

		//echo '<pre>';
		//var_dump($data);die;
		$this->paging($data);
		$filterlistdata = array("Filter");
			foreach($dataParamValue as $fil => $val){
					$paramTrx = $fil . " - " . $val;
					array_push($filterlistdata, $paramTrx);
		}
		if($csv)
		{
			// echo "<pre>";
			// print_r($header);
			// print_r($data);die;
			$listable = array_merge_recursive(array($header), $data);
			$this->_helper->download->csv($filterlistdata,$listable,null,'List View Payment');
			Application_Helper_General::writeLog('DARC','Export CSV View Payment');
		}
		elseif($pdf)
		{
			$this->_helper->download->pdf($header,$data,null,'List View Payment');
			Application_Helper_General::writeLog('DARC','Export PDF View Payment');
		}
		else if($this->_request->getParam('print')){
			$filterlistdatax = $this->_request->getParam('data_filter');
			$this->_forward('printtable', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'List View Payment', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
		}
		else
		{
			$stringParam = array(
								'updatedStart' 	=> $fUpdatedStart,
								'updatedEnd'	=> $fUpdatedEnd,
								'createdStart'	=> $fCreatedStart,
								'createdEnd'	=> $fCreatedEnd,
								'paymentStart'	=> $fPaymentStart,
								'paymentEnd'	=> $fPaymentEnd,
								'accsrc'		=> $fAcctsrc,
								'payReff'		=> $fPaymentReff,
								'paymentStatus'	=> $fPaymentStatus,
								// 'paymentType'	=> $fPaymentType,
								'transferType'	=> $fTransferType,
								'clearfilter'	=> $clearfilter,

								);
			/*
			$stringParam = array('payReff'		=> $fPaymentReff,
								 'payType'		=> $fPaymentType,
								 'trfType'		=> $fTrfType,
								 'createdFrom'	=> $fCreatedFrom,
								 'createdTo'	=> $fCreatedTo,
								 'payDateFrom'	=> $fPayDateFrom,
								 'payDateTo'	=> $fPayDateTo,
							    );
			*/

			//echo "fPaymentStatus: $fPaymentStatus";
			//echo "fAcctsrc: $fAcctsrc";
			//echo "fPaymentType: $fPaymentType";

			// $this->view->filterPayType 		= $filterPayType;
			// $this->view->optfilterPayStatus 	= $optfilterPayStatus;
			// $this->view->CustomerArr 		= $CustomerArr;
			// print_r($fCreatedEnd);die;
			// print_r($optarrAccount);die;
			$this->view->optPayType 	= $optPayType;
			$this->view->optPayStatus 	= $optPayStatus;
			$this->view->optarrAccount 	= $optarrAccount;


			$this->view->updatedStart 	= $fUpdatedStart;
			$this->view->updatedEnd 	= $fUpdatedEnd;
			$this->view->createdStart 	= $fCreatedStart;
			$this->view->createdEnd 	= $fCreatedEnd;
			$this->view->paymentStart 	= $fPaymentStart;
			$this->view->paymentEnd 	= $fPaymentEnd;
			$this->view->accsrc 		= $fAcctsrc;
			$this->view->payReff 		= $fPaymentReff;
			$this->view->paymentStatus 	= $fPaymentStatus;
			// $this->view->paymentType 	= $fPaymentType;
			$this->view->transferType 	= $fTransferType;

			//$this->view->query_string_params = $stringParam;
			 
			if(!empty($dataParamValue)){

				$this->view->createdStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
	    		$this->view->efdateStart = $dataParamValue['PS_UPDATED'];
	    		$this->view->efdateEnd = $dataParamValue['PS_UPDATED_END'];
	    		$this->view->paymentStart = $dataParamValue['PS_EFDATERPT'];
	    		$this->view->paymentEnd = $dataParamValue['PS_EFDATE_END'];


	    	  	unset($dataParamValue['PS_CREATED_END']);
				unset($dataParamValue['PS_EFDATE_END']);
				unset($dataParamValue['PS_UPDATED_END']);
					//var_dump($dataParamValue);
					foreach ($dataParamValue as $key => $value) {
						//foreach($value as $ss => $vs){
								
								$duparr = explode(',',$value);
								if(!empty($duparr)){
									
									foreach($duparr as $ss => $vs){
										$wherecol[]	= $key;
										$whereval[] = $vs;
									}
								}else{
										$wherecol[]	= $key;
										$whereval[] = $value;
								}
								
						//}
						
						
					}
					//var_dump($wherecol);
					//var_dump($whereval);die;
					
		        $this->view->wherecol     = $wherecol;
		        $this->view->whereval     = $whereval;

		      }

			$this->view->data 				= $data;
			$this->view->fields 			= $fields;
			$this->view->filter 			= $filter;
	        $this->view->sortBy 			= $sortBy;
			$this->view->sortDir 			= $sortDir;
			$this->view->clearfilter 		= $clearfilter;
			$this->view->filterlistdata = $filterlistdata;
			

			// $this->view->currentPage 		= $page;
			// $this->view->paginator 			= $data;

			Application_Helper_General::writeLog('DARC','View Payment');

			//$this->view->sortBy = $sortBy;
			//$this->view->sortDir = $sortDir;

		}

	}
}
