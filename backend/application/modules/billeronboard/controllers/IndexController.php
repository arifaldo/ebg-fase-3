<?php


require_once 'Zend/Controller/Action.php';


class billeronboard_IndexController extends Application_Main
{
	public function indexAction() 
	{
		$model 			 = new billeronboard_Model_Billeronboard();
		$billeronboardCharges = new setbilleronboardcharges_Model_Setbilleronboardcharges();
		$providerArr 	 = $billeronboardCharges->getServiceProvider();
    	$serviceTypeArr 	 = $billeronboardCharges->getServiceType();
//     	
//     	$serviceTypeArr = array(
//     			[0] => Array ( ['SERVICE_ID'] => '5',['SERVICE_NAME'] => 'Cable TV')
//     	);

    	$serviceTypeArr[1] = array('SERVICE_ID' => '', 'SERVICE_NAME' => 'E-Commerce');
//     	print_r($serviceTypeArr);die;
		$options = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));
		unset($options[3]);
		array_unshift($options,'---'.$this->language->_('Any Value').'---');
		foreach($options as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
	
		//$this->view->options = $options;
		$this->view->options = $optpayStatusRaw;
		$this->view->var = $providerArr;
		$this->view->serviceTypeArr = $serviceTypeArr;
		
			
		
		$fields = array	(
							'biller_code'  			=> array	(
																	'field' => 'PROVIDER_CODE',
																	'label' => $this->language->_('E-Commerce Code'),
																	'sortable' => true
																),
							'biller_name'  			=> array	(
																	'field' => 'PROVIDER_NAME',
																	'label' => $this->language->_('E-Commerce Name'),
																	'sortable' => true
																),
							'domain'  			=> array	(
																	'field' => 'PROVIDER_DOMAIN',
																	'label' => $this->language->_('Domain'),
																	'sortable' => true
																),
							'key_id'  			=> array	(
																	'field' => 'PROVIDER_KEY_ID',
																	'label' => $this->language->_('Key ID'),
																	'sortable' => true
																),
							'status'  		=> array	(
																	'field' => 'PROVIDER_STATUS',
																	'label' => 'Status',
																	'sortable' => true
																),
							'suggest_date'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTED',
																	'label' => $this->language->_('Latest Suggestion'),
																	'sortable' => true
																),
							'suggestor'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTEDBY',
																	'label' => $this->language->_('Suggester'),
																	'sortable' => true
																),
							'suggest_date'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTED',
																	'label' => $this->language->_('Latest Suggestion'),
																	'sortable' => true
																),
							'suggestor'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTEDBY',
																	'label' => $this->language->_('Suggester'),
																	'sortable' => true
																),
							'approval_date'  		=> array	(
																	'field' => 'PROVIDER_UPDATED',
																	'label' => $this->language->_('Latest Approval'),
																	'sortable' => true
																),
							'approver'  		=> array	(
																	'field' => 'PROVIDER_UPDATEDBY',
																	'label' => $this->language->_('Approver'),
																	'sortable' => true
																),
						);
						
		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'billerCode'    	=> array('StripTags','StringTrim'),
	                       'billerName'  	=> array('StripTags','StringTrim','StringToUpper'),
	                       'billerStatus'  	=> array('StripTags','StringTrim'),
						   'suggestor'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'approver'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'fDateFrom'  	=> array('StripTags','StringTrim'),
						   'ufDateFrom'  	=> array('StripTags','StringTrim'),
						   'fDateTo'  		=> array('StripTags','StringTrim'),
						   'ufDateTo'  		=> array('StripTags','StringTrim'),
		                   'serviceCategory'    	=> array('StripTags','StringTrim'),
	    
	                      );
	                      
	    $validator = array('filter' 		=> array(),
	                       'billerCode'    	=> array(),
	                       'billerName'  	=> array(),
	                       'billerStatus'  	=> array(),
						   'suggestor'  	=> array(),
						   'approver'  	=> array(),
						   'fDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'ufDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'ufDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	 	 	  			   'serviceCategory'    	=> array(),
	                      );
	                      
		$fParam = array();	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $billerCode = $fParam['billerCode'] = html_entity_decode($zf_filter->getEscaped('billerCode'));
		$billerName = $fParam['billerName'] = html_entity_decode($zf_filter->getEscaped('billerName'));
		$billerStatus = $fParam['billerStatus'] = html_entity_decode($zf_filter->getEscaped('billerStatus'));
		$suggestor 	= $fParam['suggestor'] 	= html_entity_decode($zf_filter->getEscaped('suggestor'));
		$approver 	= $fParam['approver'] 	= html_entity_decode($zf_filter->getEscaped('approver'));
		$datefrom 	= $fParam['datefrom'] 	= html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$udatefrom 	= $fParam['udatefrom'] 	= html_entity_decode($zf_filter->getEscaped('ufDateFrom'));
		$dateto 	= $fParam['dateto'] 	= html_entity_decode($zf_filter->getEscaped('fDateTo'));
		$udateto 	= $fParam['udateto'] 	= html_entity_decode($zf_filter->getEscaped('ufDateTo'));
		$serviceCategory = $fParam['serviceCategory'] = html_entity_decode($zf_filter->getEscaped('serviceCategory'));
		
		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');
		
		$page = (Zend_Validate::is($page,'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		if($filter == $this->language->_('Set Filter'))
		{
			$this->view->fDateTo    = $dateto;
			$this->view->ufDateTo    = $udateto;
			$this->view->fDateFrom  = $datefrom;
			$this->view->ufDateFrom  = $udatefrom;
			 
			if(!empty($datefrom))
			{
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$fParam['datefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($dateto))
			{
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
			}
			if(!empty($udatefrom))
			{
				$FormatDate = new Zend_Date($udatefrom, $this->_dateDisplayFormat);
				$fParam['udatefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($udateto))
			{
				$FormatDate = new Zend_Date($udateto, $this->_dateDisplayFormat);
				$fParam['udateto']    = $FormatDate->toString($this->_dateDBFormat);
			}

		    if($billerCode)
		    {
	       		$this->view->billerCode = $billerCode;
		    }
		    if($serviceCategory)
		    {
	       		$this->view->serviceCategory = $serviceCategory;
		    }
		    
		    
			if($billerName)
			{
	       		$this->view->billerName = $billerName;
			}
			
			if($billerStatus)
			{
	       		$this->view->billerStatus = $billerStatus;
			}
			
			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
		    }
			if($approver)
		    {
	       		$this->view->approver = $approver;
		    }
		}

		$data = $model->getProvider($fParam,$sortBy,$sortDir,$filter);
		/*foreach($data as $key=>$value)
		{
			
			$data[$key]["SERVICE_TYPE"] = 'E-Commerce';
		}*/
		
    	$this->paging($data);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	if($csv || $pdf || $this->_request->getParam('print'))
    	{
    		$arr = $data;
    		
    		foreach($arr as $key=>$value)
			{
				unset($arr[$key]["PROVIDER_ID"]);
				$arr[$key]["PROVIDER_SUGGESTED"] = Application_Helper_General::convertDate($value["PROVIDER_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
    		
	    	if($csv)
			{
				Application_Helper_General::writeLog('VECP','Download CSV E-Commerce Payment List');
				$this->_helper->download->csv(array('E-Commerce Payment','E-Commerce Payment Name','Service Catagory','Service Type','Status','Latest Suggestion','Suggester','Latest Approval','Latest Approver'),$arr,null,'E-Commerce Payment List');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('VECP','Download PDF E-Commerce Payment List');
				$this->_helper->download->pdf(array('E-Commerce Payment','E-Commerce Payment Name','Service Catagory','Service Type','Status','Latest Suggestion','Suggester','Latest Approval','Latest Approver'),$arr,null,'E-Commerce Payment List');
			}
			if($this->_request->getParam('print') == 1){
				$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'E-Commerce Payment List', 'data_header' => $fields));
			}
		
    	}
    	else
    	{
    		Application_Helper_General::writeLog('VECP','View E-Commerce Payment List');
    	}
	}
}