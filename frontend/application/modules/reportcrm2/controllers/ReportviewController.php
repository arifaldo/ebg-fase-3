<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

class reportcrm2_ReportviewController extends Application_Main
{
    protected $_moduleDB = 'RTF';

    public function setPaymentType($paymentType, $transferType)
    {
        $filterType      = array();

        foreach($paymentType["code"] as $key => $val)
        {
            //|| $key == "remittance"
            if ($key == "within" || $key == "domestic" || $key == "remittance" || $key == "purchase" || $key == "payment" )
            {
                $filterType[$paymentType["code"][$key]] = $paymentType["desc"][$key];
            }
            elseif ($key == "multicredit")
            {
                $code = $paymentType["code"]["multicredit"].",".$paymentType["code"]["bulkcredit"];
                $desc = $paymentType["desc"][$key];
                $filterType["{$code}"] = $desc;
            }
            elseif ($key == "multidebet")
            {
                $code = $paymentType["code"]["multidebet"].",".$paymentType["code"]["bulkdebet"];
                $desc = $paymentType["desc"][$key];
                $filterType["{$code}"] = $desc;
            }
        }

        return $filterType;
    }



    public function indexAction()
    {

        $this->_helper->_layout->setLayout('newlayout');

        $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
        if(count($temp)>1){
            if($temp[0]=='F' || $temp[0]=='S'){
                if($temp[0]=='F')
                    $this->view->error = 1;
                else
                    $this->view->success = 1;
                $msg = ''; unset($temp[0]);
                foreach($temp as $value)
                {
                    if(!is_array($value))
                        $value = array($value);
                    $msg .= $this->view->formErrors($value);
                }
                $this->view->report_msg = $msg;
            }
        }

        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $arrAccount     = $CustomerUser->getAccounts();

        if(is_array($arrAccount) && count($arrAccount) > 0){
            foreach($arrAccount as $key => $value){

                $val        = $arrAccount[$key]["ACCT_NO"];
                $ccy        = $arrAccount[$key]["CCY_ID"];
                $acctname   = $arrAccount[$key]["ACCT_NAME"];
                //$acctalias    = $arrAccount[$key]["ACCT_ALIAS_NAME"];
                $accttype   = ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';  // 10 : saving, 20 : giro;

                $arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';

            }
        }
        else { $arrAccountRaw = array();}
// 1679091c5a880faf6fb5e6087eb1b2dc
// echo $this->encryptIt('6');die;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'cmdemo';
        $secret_iv = 'democm';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $string = $this->_getParam('report');


        // $string = '10';
        // $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        // $output = base64_encode($output);
         $report = openssl_decrypt(base64_decode($this->_getParam('report')), $encrypt_method, $key, 0, $iv);


        // $report_id = $this->decryptIt($this->_getParam('report'));
        // print_r($output);die;
        $selectcolomn = $this->_db->select()
                            ->from('T_REPORT_COLOMN', array('COLM_NAME','COLM_FIELD','COLM_TYPE'))
                            ->where('COLM_REPORD_ID = ?', $report);

        $ColomnList = $this->_db->fetchAll($selectcolomn);

        $this->view->colomn = $ColomnList;

        $pdf = $this->_getParam('pdf');
        // print_r($pdf);die;

        //get page, sortby, sortdir
        $page           = $this->_getParam('page');
        $sortBy         =($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updated');
        $sortDir        = $this->_getParam('sortdir');
        $filter         = $this->_getParam('filter');
        $clearfilter    = $this->_getParam('clearfilter');

        $csv            = $this->_getParam('csv');
        $pdf            = $this->_getParam('pdf');

        $this->view->sortBy         = $sortBy;
        $this->view->sortDir        = $sortDir;
        $this->view->filter         = $filter;
        $this->view->clearfilter    = $clearfilter;
        if(empty($report)){
            $this->_redirect('home/dashboard');
        }
        $selectreport = $this->_db->select()
                            ->from('T_REPORT_GENERATOR', array('*'))
                            ->where('ID = ?', $report);
                            // echo $selectreport;die;
        $reportdata = $this->_db->fetchRow($selectreport);

        // $where['CUST_ID = ?'] = $this->_custIdLogin;
        $views = $reportdata['REPORT_VIEWS']+1;

        $updateArr = array('REPORT_VIEWS' => $views);
        $where['ID = ?'] = $report;
        $this->_db->update('T_REPORT_GENERATOR',$updateArr,$where);
        // print_r($reportdata);die;
        // echo '<pre>';
        // print_r($reportdata['REPORT_SORT_ASC']);die;

        //validate parameters before passing to view and query
        $page       = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

        // $sortBy     = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        // $sortDir    = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';

        $this->view->currentPage = $page;



        // The default is set so all fields allow an empty string


        // if($pdf)
        // {

        //     $outputHTML = "<tr><td>".$this->view->render('reportview/index.phtml')."</td></tr>";
        //     // print_r($outputHTML);die('ter');
        //     $this->_helper->download->pdf(null,null,null,'Approval',$outputHTML);
        // }

        //jika tombol csv dan pdf ditekan
        // if($pdf || $csv)
        // {
        //     $fUpdatedStart  = $zf_filter->getEscaped('updatedStart');
        //     $fUpdatedEnd    = $zf_filter->getEscaped('updatedEnd');
        // }

        $paramPayment = array("WA"              => false,
                              "ACCOUNT_LIST"    => $this->_accountList,
                              "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
                             );

        // get payment query
        $select   = $CustomerUser->getPayment($paramPayment);

        // Filter Data


        // echo $select;die;

        // $select->order($sortby.' '.$sortdir);
        // $selectdata = $this->_db->select()->from(array('A' => $select),
        //         array(
        //             'A.*'
        //         ));
        // echo $selectdata;die;

        // echo "<pre>";
        if(!empty($reportdata['REPORT_WHERE'])){
            $whereArr = explode(';', $reportdata['REPORT_WHERE']);
            foreach ($whereArr as $key => $value) {

                $searchkey = ( explode( ' ', $value ));
                // var_dump($searchkey);
                if($searchkey['0'] == ''){
                    if(trim($searchkey['1']) == 'PS_EFDATE' || trim($searchkey['1']) == 'PS_CREATED' || trim($searchkey['1']) == 'PS_UPDATED'){
                    $var = $searchkey['3'];
                    $searchkey['3'] = date("Y-m-d", strtotime($var) );
                    $value = 'DATE('.$searchkey['1'].') '.$searchkey['2'].' "'.$searchkey['3'].'"';
                    }
                }

                if(trim($searchkey['0']) == 'PS_EFDATE' || trim($searchkey['0']) == 'PS_CREATED' || trim($searchkey['0']) == 'PS_UPDATED'){
                    $var = $searchkey['2'];
                    $searchkey['2'] = date("Y-m-d", strtotime($var) );
                    $value = 'DATE('.$searchkey['0'].') '.$searchkey['1'].' "'.$searchkey['2'].'"';
                }

                if(trim($value) != ''){
                    $select->where($value);
                }

            }

        }
        // echo $select;
// die;
        // if(!empty($reportdata['REPORT_DATA'])){
        //             if($reportdata['REPORT_DATA'] == '1'){
        //                 $select->where('MONTH(PS_CREATED) = MONTH(NOW()) AND YEAR(PS_CREATED) = YEAR(NOW())');
        //             }else if($reportdata['REPORT_DATA'] == '2'){
        //                 $select->where('DATE(PS_CREATED) BETWEEN DATE("'.date('Y-m-01', strtotime('-1 MONTH')).'") AND DATE_SUB(CURDATE(),INTERVAL EXTRACT(DAY FROM NOW()) DAY)');
        //             }else if($reportdata['REPORT_DATA'] == '3'){
        //                 $select->where('DATE(PS_CREATED) BETWEEN DATE("'.date('Y-m-01', strtotime('-3 MONTH')).'") AND DATE_SUB( CURDATE( ) ,INTERVAL 3 MONTH )');
        //             }else if($reportdata['REPORT_DATA'] == '4'){
        //                 $select->where('DATE(PS_CREATED) BETWEEN DATE("'.date('Y-m-01', strtotime('-6 MONTH')).'") AND DATE_SUB( CURDATE( ) ,INTERVAL 6 MONTH )');
        //             }else if($reportdata['REPORT_DATA'] == '5'){
        //                 $select->where('YEAR(PS_CREATED) = YEAR(NOW())');
        //             }

        //         }


        //uncomment below untuk periodic schedule
        // if(!empty($reportdata['REPORT_SCHEDULE'])){
        //     if($reportdata['REPORT_SCHEDULE'] == 'daily'){
        //         $select->where('DATE(PS_CREATED) = "'.date('Y-m-d', strtotime('-1 day')).'"');
        //     }

        //     // else if($reportdata['REPORT_SCHEDULE'] == 'hourly'){
        //     //     $select->where('DATE(PS_CREATED) = DATE(NOW()) AND (HOUR(NOW())-1) = HOUR(PS_CREATED)');
        //     // }

        //     else if($reportdata['REPORT_SCHEDULE'] == 'weekly'){
        //         $select->where('WEEK(PS_CREATED) = (WEEK(NOW())-1) AND YEAR(PS_CREATED) = (YEAR(NOW())) ');

        //     }else if($reportdata['REPORT_SCHEDULE'] == 'monthly'){
        //         $select->where('MONTH(PS_CREATED) = (MONTH(NOW())-1) AND YEAR(PS_CREATED) = (YEAR(NOW())) ');
        //     }

        //     // else if($reportdata['REPORT_SCHEDULE'] == 'yearly'){
        //     //     $select->where('YEAR(PS_CREATED) = (YEAR(NOW())-1)');
        //     // }
        // }
        //uncomment above untuk periodic schedule



        // if(!empty($reportdata['REPORT_SCHEDULE'])){
        //     // $sortby = $reportdata['REPORT_SORT_DESC'];
        //     if($reportdata['REPORT_SCHEDULE']=='monthly'){
        //         $select->where('MONTH(PS_CREATED) = MONTH(NOW())');
        //         $select->where('YEAR(PS_CREATED) = YEAR(NOW())');

        //     }
        //     // $sortdesdata = explode(',', $reportdata['REPORT_SORT_DESC']);
        //     // $sortdirdesc = 'DESC';
        //     //  if(!empty($sortdesdata)){
        //     //     foreach ($sortdesdata as $key => $value) {
        //     //             // 4
        //     //             $select->order($value.' DESC');
        //     //     }
        //     // }
        // }
        // echo $select;die;
        // $selectdata = $selectdata->order('TRA_AMOUNT DESC');
        // $selectdata = $selectdata->order('PS_NUMBER DESC');

        // $selectdata->order($sortby.' '.$sortdir);
        if(!empty($reportdata['REPORT_SORT1'])){

            $sort1data = explode(' ', $reportdata['REPORT_SORT1']);
            if(!empty($sort1data)){

                if($sort1data[0] == 'PS_NUMBER'){
                    $sort1data[0] = 'T.PS_NUMBER';
                }
                $select->order($sort1data[0].' '.strtoupper($sort1data[1]));
            }
        }

        if(!empty($reportdata['REPORT_SORT2'])){

            $sort2data = explode(' ', $reportdata['REPORT_SORT2']);
            if(!empty($sort2data)){

                if($sort2data[0] == 'PS_NUMBER'){
                    $sort2data[0] = 'T.PS_NUMBER';
                }
                $select->order($sort2data[0].' '.strtoupper($sort2data[1]));
            }
        }

        if(!empty($reportdata['REPORT_LIMIT'])){
            $select->limit($reportdata['REPORT_LIMIT']);
        }

        // echo '<pre>';
        // echo $select;die;
        $dataSQL = $this->_db->fetchAll($select);

        $statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
        $this->view->trastatus = $statusarr;
        // print_r($header);die;
        if ($csv || $pdf)
        {   $header  = Application_Helper_Array::simpleArray($ColomnList, "COLM_NAME");     }
        else
        {
            // $this->paging($dataSQL);
            // $dataSQL = $this->view->paginator;
        }

        $data = array();
    //  echo '<pre>';
    //  print_r($dataSQL);die;


        $this->paging($dataSQL);
            $dataprint = array();
        foreach ($dataSQL as $key => $value) {

            foreach ($ColomnList as $row => $val) {
                if($val['COLM_FIELD'] == 'PS_STATUS'){
                    $val['COLM_FIELD']  = 'payStatus';
                }
                $dataprint[$key][$val['COLM_FIELD']] =  $value[$val['COLM_FIELD']];

            }
            // $dataprint[$key]['']
        }
        // print_r($dataprint);die;
        if($csv)
        {
            // print_r($reportdata['REPORT_NAME']);die;
            $this->_helper->download->csv($header,$dataprint,null,$reportdata['REPORT_NAME']);  
            Application_Helper_General::writeLog('DARC','Export CSV View Payment');
        }
        elseif($pdf)
        {
            // die;
            // print_r($ColomnList);die;

            $this->_helper->download->pdf($header,$dataprint,null,$reportdata['REPORT_NAME']);
            Application_Helper_General::writeLog('DARC','Export PDF View Payment');
        }
        else
        {
            $stringParam = array(
                                'updatedStart'  => $fUpdatedStart,
                                'updatedEnd'    => $fUpdatedEnd,
                                'createdStart'  => $fCreatedStart,
                                'createdEnd'    => $fCreatedEnd,
                                'paymentStart'  => $fPaymentStart,
                                'paymentEnd'    => $fPaymentEnd,
                                'accsrc'        => $fAcctsrc,
                                'payReff'       => $fPaymentReff,
                                'paymentStatus' => $fPaymentStatus,
                                'paymentType'   => $fPaymentType,
                                'transferType'  => $fTransferType,
                                'clearfilter'   => $clearfilter,

                                );
            /*
            $stringParam = array('payReff'      => $fPaymentReff,
                                 'payType'      => $fPaymentType,
                                 'trfType'      => $fTrfType,
                                 'createdFrom'  => $fCreatedFrom,
                                 'createdTo'    => $fCreatedTo,
                                 'payDateFrom'  => $fPayDateFrom,
                                 'payDateTo'    => $fPayDateTo,
                                );
            */

            //echo "fPaymentStatus: $fPaymentStatus";
            //echo "fAcctsrc: $fAcctsrc";
            //echo "fPaymentType: $fPaymentType";

            // $this->view->filterPayType       = $filterPayType;
            // $this->view->optfilterPayStatus  = $optfilterPayStatus;
            // $this->view->CustomerArr         = $CustomerArr;


            $this->view->optPayType     = $optPayType;
            $this->view->optPayStatus   = $optPayStatus;
            $this->view->optarrAccount  = $optarrAccount;


            $this->view->updatedStart   = $fUpdatedStart;
            $this->view->updatedEnd     = $fUpdatedEnd;
            $this->view->createdStart   = $fCreatedStart;
            $this->view->createdEnd     = $fCreatedEnd;
            $this->view->paymentStart   = $fPaymentStart;
            $this->view->paymentEnd     = $fPaymentEnd;
            $this->view->accsrc         = $fAcctsrc;
            $this->view->payReff        = $fPaymentReff;
            $this->view->paymentStatus  = $fPaymentStatus;
            $this->view->paymentType    = $fPaymentType;
            $this->view->transferType   = $fTransferType;

            //$this->view->query_string_params = $stringParam;

            $this->view->data               = $data;
            $this->view->fields             = $fields;
            $this->view->filter             = $filter;
            $this->view->sortBy             = $sortBy;
            $this->view->sortDir            = $sortDir;

            $this->view->clearfilter        = $clearfilter;
            $this->setbackURL($_SERVER['REQUEST_URI']);
            // $this->view->currentPage         = $page;
            // $this->view->paginator           = $data;

            Application_Helper_General::writeLog('DARC','View Payment');

            //$this->view->sortBy = $sortBy;
            //$this->view->sortDir = $sortDir;

        }

    }
}
