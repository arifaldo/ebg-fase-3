<?php

/**
 * IndexController
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

//NOTE:
//Watch the modulename, filename and classname carefully
class Authorizationacl_IndexController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	public function forbiddenAction() {
		$this->_helper->layout()->setLayout('newlayout');
		// TODO Auto-generated IndexController::indexAction() default action
	}
	public function disableAction(){
		$this->_helper->layout()->setLayout('popup');
		require_once 'General/Settings.php';
		$setting = new Settings();
		$this->view->note = nl2br($setting->getSetting('disable_note'));
		
	}
	
	public function disableuserAction(){ 
		// var_dump('d');die;
		$this->_helper->layout()->setLayout('newlayout'); 
	}
	
	protected function getSetting($settingId){
			$db = Zend_Db_Table::getDefaultAdapter();
			$settingId  = strtoupper($settingId);
			$retVal = $db->fetchOne( "SELECT SETTING_VALUE FROM M_SETTING WHERE UPPER(SETTING_ID) = '{$settingId}'");
			return $retVal;
	}
	

}

