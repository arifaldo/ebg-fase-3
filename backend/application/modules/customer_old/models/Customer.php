<?php

class customer_Model_Customer extends Application_Main
{
    protected $_custData;
    protected $_userData;
    protected $_accData;
    protected $_limitData;

    // constructor
    public function initModel()
    {
        $this->_custData = array(
            'CUST_ID'              => null,
            'CUST_CIF'             => null,
            'CUST_NAME'            => null,
            'CUST_TYPE'            => null,
            'CUST_ADDRESS'         => null,
            'CUST_CITY'            => null,
            'CUST_ZIP'             => null,
            'CUST_PROVINCE'        => null,
            'CUST_CONTACT'         => null,
            'CUST_PHONE'           => null,
            'COUNTRY_CODE'         => null,
            'CUST_EXT'             => null,
            'CUST_FAX'             => null,
            'CUST_EMAIL'           => null,
            'CUST_WEBSITE'         => null,
            'CUST_CHARGES_STATUS'  => null,
            'CUST_MONTHLYFEE_STATUS'   => null,
            'CUST_TOKEN_AUTH'    => null,
            'CUST_LIMIT_IDR'       => null,
            'CUST_LIMIT_USD'       => null

            // 'CUST_EMOBILE'         => null,
            // 'CUST_ISEMOBILE'       => null,
        );

        $this->_userData = array(
            'CUST_ID'        => null,
            'USER_ID'        => null,
            'USER_NAME'      => null,
            'USER_EMAIL'     => null,
            'USER_PHONE'     => null,
            'USER_MOBILENO'  => null,
            'USER_DIVISION'  => null,
            'USER_STATUS'    => null,
            'FGROUP_ID'      => null,
            'USER_HASTOKEN'  => null
        );

        $this->_accData = array(
            'CUST_ID'        => null,
            'ACCT_NO'        => null,
            'CCY_ID'         => null,
            'ACCT_NAME'      => null,
            'ACCT_GROUP'     => null,
            'ACCT_TYPE'      => null,
            'ACCT_LIMIT'     => null,
            'BANK_ID'        => null,
            'COUNTRY_CODE'   => null,
            'BANK_NAME'    => null,
            'BRANCH_CODE'    => null,
            'ACCT_DESC'    => null
        );

        $this->_limitData = array(
            'CUST_ID'         => null,
            'DR_DAILY_LIMIT'    => null,
            'CR_DAILY_LIMIT'    => null,
            'AMT_BLOCK_LIMIT'   => null,
            'CCY_ID'        => null
        );
    }

    public function getCompanyType()
    {
        $select = $this->_db->select()
            ->from('M_COMPANY_TYPE')
            ->query()->fetchAll();
        return $select;
    }

    public function getBusinessEntity()
    {
        $select = $this->_db->select()
            ->from('M_BUSINESS_ENTITY')
            ->query()->fetchAll();
        return $select;
    }

    public function getDebitur()
    {
        $select = $this->_db->select()
            ->from('M_DEBITUR')
            ->query()->fetchAll();
        return $select;
    }



    public function getCreditQuality()
    {
        $select = $this->_db->select()
            ->from('M_CREDIT_QUALITY')
            ->query()->fetchAll();
        return $select;
    }

    public function getBusinessType()
    {
        $select = $this->_db->select()
            ->from('M_BUSINESS_ENTITY')
            ->query()->fetchAll();

        return $select;
    }

    public function getCity()
    {
        $select = $this->_db->select()
            ->from('M_CITYLIST')
            ->where('CITY_CODE IS NOT NULL')
            ->query()->fetchAll();
        return $select;
    }

    public function getAllCustomer()
    {
        $select = $this->_db->select()
            ->from(array('a' => 'M_CUSTOMER'), array('CUST_ID'))
            ->query()->fetchAll();
        return $select;
    }

    public function getPriviTemplate()
    {
        $select = $this->_db->select()
            ->from('M_FPRIVILEGE_TEMPLATE')
            ->query()->fetchAll();
        return $select;
    }

    public function getDailyTransactionTrx()
    {
        $select = $this->_db->select()
            ->from(array('a' => 'T_TRANSACTION'), array())
            ->joinleft(array('b' => 'T_PSLIP'), 'b.PS_NUMBER = a.PS_NUMBER', array(
                'b.PS_CREATED',
                'TOTAL' => 'COUNT(a.`TRANSACTION_ID`)'
            ))

            //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
            ->where('MONTH(b.PS_CREATED) = MONTH(NOW())')
            ->group('CAST(b.`PS_CREATED` AS DATE)')
            //->where('USER_STATUS!=3');
            ->query()->fetchAll();

        return $select;
    }

    public function getCustomer($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('a' => 'M_CUSTOMER'))
            ->joinleft(array('c' => 'M_COUNTRY'), 'a.COUNTRY_CODE=c.COUNTRY_CODE', array('COUNTRY_NAME'))
            ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->query()->fetch();
        return $select;
    }

    public function getCountry()
    {
        $select = $this->_db->select()
            ->from('M_COUNTRY')
            ->order('COUNTRY_NAME ASC')
            ->query()->fetchAll();
        return $select;
    }


    public function getCustomerAcct($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('CA' => 'M_CUSTOMER_ACCT'), array('ACCT_NO', 'CCY_ID', 'ACCT_NAME', 'ACCT_ALIAS_NAME', 'ACCT_DESC', 'ACCT_GROUP', 'ACCT_EMAIL', 'ORDER_NO', 'ACCT_STATUS'))
            ->joinleft(array('g' => 'M_GROUPING'), 'g.GROUP_ID=CA.GROUP_ID', array('GROUP_NAME'))
            ->where('UPPER(CA.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            //->where('ACCT_STATUS!=3')
            ->order(array('ORDER_NO ASC'))
            ->query()->fetchAll();
        return $select;
    }

    public function getInsuranceBranch($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('i' => 'M_INS_BRANCH'), array('INS_BRANCH_NAME', 'INS_BRANCH_EMAIL', 'INS_BRANCH_ACCT', 'LAST_SUGGESTED', 'LAST_APPROVED'))
            ->where('UPPER(i.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->query()->fetchAll();
        return $select;
    }

    public function getCustomerById($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('CUST_ID', 'CUST_NAME'))
            ->where('A.CUST_ID = ?', $cust_id);
        return $this->_db->fetchRow($select);

        // $select = $this->_db->select()
        // ->from(array('M_CUSTOMER'),array('CUST_ID','CUST_NAME'))
        // ->where('CUST_ID = ?', $cust_id)
        // ->fetchRow();
        // return $select;
    }

    public function getDataInsuranceBranch($user_id, $filterParam = null, $filter = null)
    {
        $select = $this->_db->select()
            ->from(
                array('A' => 'M_INS_BRANCH'),
                array('*')
            )
            ->joinLeft(
                array('B' => 'M_CUSTOMER'),
                'B.CUST_ID = A.CUST_ID',
                array('CUST_ID', 'CUST_NAME')
            )
            ->where('UPPER(M.CUST_ID)=' . $this->_db->quote((string)$cust_id));

        if ($filter == TRUE) {
            if ($filterParam['fInsBranchCode']) {
                $select->where('A.CUST_ID LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchCode'] . '%'));
            }
            if ($filterParam['fInsBranchName']) {
                $select->where('A.INS_BRANCH_NAME LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchName'] . '%'));
            }
            if ($filterParam['fInsBranchAcct']) {
                $select->where('A.INS_BRANCH_ACCT LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchAcct'] . '%'));
            }
            if ($filterParam['fInsBranchSuggest']) {
                $select->where('A.LAST_SUGGESTEDBY LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchSuggest'] . '%'));
            }
            if ($filterParam['fInsBranchApprove']) {
                $select->where('A.LAST_APPROVEDBY LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchApprove'] . '%'));
            }
        }
        return $this->_db->fetchAll($select);
    }

    public function getUserAcct($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('u' => 'M_USER'), array(
                'USER_ID',
                'USER_FULLNAME',
                'USER_STATUS',
                'USER_EMAIL', /*+ ADDED BY SUHANDI +*/
                'USER_PHONE',/*+ ADDED BY SUHANDI +*/
                'USER_ISLOCKED',
                'USER_ISEMAIL',
                'USER_CREATED',/*+ ADDED BY SUHANDI +*/
                'USER_UPDATED',
                'USER_UPDATEDBY',
                'USER_SUGGESTED',
                'USER_SUGGESTEDBY'
            ))
            //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
            ->joinLeft(array('g' => 'M_USER_GROUP'), 'g.USER_ID=u.USER_ID AND g.CUST_ID=u.CUST_ID', array('FGROUP_ID'))
            ->joinLeft(array('f' => 'M_FTEMPLATE'), 'f.FTEMPLATE_ID=g.FGROUP_ID', array('f.FTEMPLATE_DESC'))
            ->where('UPPER(u.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            //->where('USER_STATUS!=3');
            ->query()->fetchAll();

        return $select;
    }

    public function getUserLimit($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('A' => 'M_MAKERLIMIT'), array('USER_LOGIN', 'MAXLIMIT', 'MAKERLIMIT_STATUS'))
            ->joinLeft(array('B' => 'M_USER'), 'A.USER_LOGIN = B.USER_ID AND A.CUST_ID = B.CUST_ID', array('USER_FULLNAME'))
            ->joinLeft(array('C' => 'M_CUSTOMER_ACCT'), 'A.ACCT_NO = C.ACCT_NO', array('ACCT_NO', 'CCY_ID', 'ACCT_NAME')) // ADDED BY SUHANDI
            ->where('A.MAXLIMIT > 0')
            ->where('A.MAXLIMIT_OPEN = 0')
            ->where('A.MAKERLIMIT_STATUS <> 3')
            //->where('C.ACCT_STATUS <> 3')                                                            
            ->where('UPPER(B.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('UPPER(A.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->query()->fetchAll();
        // Zend_Debug::dump($select->__toString());die;
        return $select;
    }

    public function getUserOpenLimit($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('A' => 'M_MAKERLIMIT'), array('USER_LOGIN', 'MAXLIMIT', 'MAKERLIMIT_STATUS', 'ACCT_NAME', 'ACCT_NO', 'ACCT_CCY'))
            ->joinLeft(array('B' => 'M_USER'), 'A.USER_LOGIN = B.USER_ID AND A.CUST_ID = B.CUST_ID', array('USER_FULLNAME'))
            ->joinleft(array('C' => 'M_BANKTABLE'), 'C.BANK_CODE = A.BANK_CODE', array('C.BANK_NAME'))
            //->joinLeft(array('C' => 'M_CUSTOMER_ACCT'),'A.ACCT_NO = C.ACCT_NO',array('ACCT_NO','CCY_ID','ACCT_NAME')) // ADDED BY SUHANDI
            ->where('A.MAXLIMIT > 0')
            ->where('A.MAXLIMIT_OPEN = 1')
            ->where('A.MAKERLIMIT_STATUS <> 3')
            //->where('C.ACCT_STATUS <> 3')                                                            
            ->where('UPPER(B.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('UPPER(A.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->query()->fetchAll();
        //Zend_Debug::dump($select);die;
        return $select;
    }

    public function getUserDailyLimit($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('d' => 'M_DAILYLIMIT'))
            ->join(array('u' => 'M_USER'), 'd.USER_LOGIN=u.USER_ID and d.CUST_ID=u.CUST_ID', array('USER_FULLNAME'))
            ->where('UPPER(u.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('UPPER(d.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('DAILYLIMIT_STATUS!=3')
            ->query()->fetchAll();

        //Zend_Debug::dump($select);die;
        return $select;
    }

    public function getUserTempDailyLimit($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('d' => 'TEMP_DAILYLIMIT'), array('TEMP_ID'))
            ->join(array('u' => 'M_USER'), 'd.USER_LOGIN=u.USER_ID and d.CUST_ID=u.CUST_ID', array('USER_FULLNAME'))
            ->where('UPPER(u.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('UPPER(d.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            //->where('DAILYLIMIT_STATUS!=3')
            ->query()->fetchAll();

        //Zend_Debug::dump($select);die;
        return $select;
    }

    public function getBenefBankAcc($cust_id)
    {
        $select = $this->_db->select()
            ->from('M_BENEFICIARY', array('BENEFICIARY_ACCOUNT', 'CURR_CODE', 'BENEFICIARY_NAME', 'BENEFICIARY_CREATED', 'BENEFICIARY_UPDATED'))
            ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->order(array('BENEFICIARY_ACCOUNT ASC'))
            ->query()->fetchAll();
        return $select;
    }

    public function getBenefUser($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('bu' => 'M_BENEFICIARY_USER'), array('USER_ID'))
            ->join(array('b' => 'M_BENEFICIARY'), 'bu.BENEFICIARY_ID=b.BENEFICIARY_ID', array('BENEFICIARY_ACCOUNT', 'BENEFICIARY_NAME', 'CURR_CODE', 'BANK_NAME', 'BENEFICIARY_TYPE'))
            ->join(array('u' => 'M_USER'), 'bu.USER_ID=u.USER_ID', array('USER_FULLNAME'))
            ->where('UPPER(bu.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('UPPER(u.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->order(array('b.BENEFICIARY_ACCOUNT ASC'))
            ->query()->fetchAll();
        return $select;
    }

    public function getBenefAccount($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('b' => 'M_BENEFICIARY'), array('BENEFICIARY_ACCOUNT', 'BENEFICIARY_NAME', 'CURR_CODE', 'BANK_NAME', 'BENEFICIARY_TYPE'))
            ->where('UPPER(b.CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->order(array('b.BENEFICIARY_ACCOUNT ASC'))
            ->query()->fetchAll();
        return $select;
    }



    public function getAppGroup($cust_id)
    {
        $select = $this->_db->select()
            ->from('M_APP_GROUP_USER')
            ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->query()->fetchAll();
        return $select;
    }

    public function getAppBoundary($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('MAB' => 'M_APP_BOUNDARY'), array('*'))
            ->joinleft(array('MABG' => 'M_APP_BOUNDARY_GROUP'), 'MABG.BOUNDARY_ID=MAB.BOUNDARY_ID', array('*'))  /*+ ADDED BY SUHANDI +*/
            ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('BOUNDARY_ISUSED=1')
            //                           ->order('BOUNDARY_MIN ASC')
            //                           ->order('CCY_BOUNDARY ASC')
            ->query()->fetchAll();
        return $select;
    }


    public function getCustomerActIn($cust_id, $status = null)
    {
        /*$select = $this->_db->select()
                           ->from('M_CUSTOMER')
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('CUST_STATUS='.$status)
                           ->query()->fetch();*/

        $select = $this->_db->select()
            ->from('M_CUSTOMER')
            ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id));

        if ($status != null) $select->where('CUST_STATUS=' . $status);

        return $select->query()->fetch();
    }

    public function getUserIDTmp($custId)
    {
        $select = $this->_db->select()
            ->from(array('u' => 'TEMP_CUSTOMER'), array(
                'CUST_ID',
                'CUST_NAME',
                'CUST_STATUS',
                'CUST_EMAIL',
                'CUST_PHONE'
            ))
            //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
            ->where('UPPER(u.CUST_ID)=' . $this->_db->quote((string)$custId))
            //->where('USER_STATUS!=3');
            ->query()->fetchAll();

        return $select;
    }

    public function getUserID($custId = null)
    {
        $select = $this->_db->select()
            ->from(array('u' => 'M_CUSTOMER'), array(
                'CUST_ID',
                'CUST_NAME',
                'CUST_STATUS',
                'CUST_EMAIL',
                'CUST_PHONE'
            ))
            //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
            ->where('UPPER(u.CUST_ID)=' . $this->_db->quote((string)$custId))
            //->where('USER_STATUS!=3');
            ->query()->fetchAll();

        return $select;
    }

    public function getTempCustomerId($cust_id)
    {
        $select = $this->_db->select()
            ->from('TEMP_CUSTOMER', array('TEMP_ID'))
            ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
            // echo $select;
            ->query()->fetch();
        return $select;
    }

    public function getGlobalchange($cust_id)
    {
        $select = $this->_db->select()
            ->from('T_GLOBAL_CHANGES', array('CHANGES_ID', 'MODULE'))
            ->where('CHANGES_STATUS = ? ', 'WA')
            ->where('UPPER(COMPANY_CODE)=' . $this->_db->quote((string)$cust_id))
            ->query()->fetch();
        return $select;
    }

    public function getGlobalchangeBoundary($cust_id)
    {
        $select = $this->_db->select()
            ->from('T_GLOBAL_CHANGES', array('CHANGES_ID', 'MODULE'))
            ->where('CHANGES_STATUS = ? ', 'WA')
            ->where('MODULE = ? ', 'boundary')
            ->where('UPPER(COMPANY_CODE)=' . $this->_db->quote((string)$cust_id))
            ->query()->fetch();
        return $select;
    }

    public function getMemberId($cust_id)
    {
        $select = $this->_db->select()
            ->from('M_MEMBER', array('MEMBER_ID'))
            ->where('UPPER(MEMBER_CUST)=' . $this->_db->quote((string)$cust_id))
            ->where('UPPER(MEMBER_STATUS)=' . $this->_db->quote($this->_masterStatus['code']['active']))
            ->query()->fetchAll();
        return $select;
    }

    public function getMemberId2($cust_id)
    {
        $select = $this->_db->select()
            ->from(array('a' => 'M_ANCHOR'), array())
            ->join(array('s' => 'M_SCHEME'), 'a.ANCHOR_ID=s.ANCHOR_ID', array())
            ->join(array('c' => 'M_COMMUNITY'), 'c.SCHEME_ID=s.SCHEME_ID', array())
            ->join(array('m' => 'M_MEMBER'), 'm.COMM_ID=c.COMM_ID', array('MEMBER_ID'))
            ->where('UPPER(a.ANCHOR_CUST)=' . $this->_db->quote((string)$cust_id))
            ->where('UPPER(m.MEMBER_STATUS)=' . $this->_db->quote($this->_masterStatus['code']['active']))
            ->query()->fetchAll();
        return $select;
    }

    public function insertTempCustomer($change_id, $cust_data)
    {
        $content = [
            'CHANGES_ID'               => $change_id,
            'CUST_ID'                  => $cust_data['CUST_ID'],
            'CUST_CIF'                 => $cust_data['CUST_CIF'],
            'CUST_SECURITIES'          => $cust_data['CUST_SECURITIES'],
            'CUST_CODE'                => $cust_data['CUST_CODE'],
            'CUST_NAME'                => $cust_data['CUST_NAME'],
            'CUST_MODEL'               => $cust_data['CUST_MODEL'],
            'CUST_TYPE'                => $cust_data['CUST_TYPE'],
            'CUST_ADDRESS'             => $cust_data['CUST_ADDRESS'],
            'CUST_CITY'                => $cust_data['CUST_CITY'],
            'CUST_ZIP'                 => $cust_data['CUST_ZIP'],
            'CUST_PROVINCE'            => $cust_data['CUST_PROVINCE'],
            'COUNTRY_CODE'             => $cust_data['COUNTRY_CODE'],
            'CUST_CONTACT'             => $cust_data['CUST_CONTACT'],
            'CUST_PHONE'               => $cust_data['CUST_PHONE'],
            'CUST_EXT'                 => $cust_data['CUST_EXT'],
            'CUST_CONTACT_PHONE'       => $cust_data['CUST_CONTACT_PHONE'],
            'CUST_EMAIL'               => $cust_data['CUST_EMAIL'],
            'CUST_WEBSITE'             => $cust_data['CUST_WEBSITE'],
            'CUST_CHARGES_STATUS'      => $cust_data['CUST_CHARGES_STATUS'],
            'CUST_MONTHLYFEE_STATUS'   => $cust_data['CUST_MONTHLYFEE_STATUS'],
            'CUST_TOKEN_AUTH'          => $cust_data['CUST_TOKEN_AUTH'],
            'CUST_WORKFIELD'           => $cust_data['CUST_WORKFIELD'],
            'CUST_STATUS'              => $cust_data['CUST_STATUS'],
            'CUST_CREATED'             => $cust_data['CUST_CREATED'],
            'CUST_CREATEDBY'           => $cust_data['CUST_CREATEDBY'],
            'CUST_UPDATED'             => $cust_data['CUST_UPDATED'],
            'CUST_UPDATEDBY'           => $cust_data['CUST_UPDATEDBY'],
            'CUST_SUGGESTED'           => $cust_data['CUST_SUGGESTED'],
            'CUST_SUGGESTEDBY'         => $cust_data['CUST_SUGGESTEDBY'],
            'CUST_LIMIT_IDR'           => $cust_data['CUST_LIMIT_IDR'],
            'CUST_LIMIT_USD'           => $cust_data['CUST_LIMIT_USD'],
            'CUST_BG_LIMIT'            => $cust_data['CUST_BG_LIMIT'],
            'CUST_APP_TOKEN'           => $cust_data['CUST_APP_TOKEN'],
            'CUST_RLS_TOKEN'           => $cust_data['CUST_RLS_TOKEN'],
            'CUST_REVIEW'              => $cust_data['CUST_REVIEW'],
            'CUST_APPROVER'            => $cust_data['CUST_APPROVER'],
            'CUST_FINANCE'             => $cust_data['CUST_FINANCE'],
            'CUST_RDN_KEY'             => $cust_data['CUST_RDN_KEY'],
            'CUST_ADM_ROLE'            => $cust_data['CUST_ADM_ROLE'],
            'CUST_SAME_USER'           => $cust_data['CUST_SAME_USER'],
            'CUST_SPECIAL'             => $cust_data['CUST_SPECIAL'],
            'CUST_CHARGESID'           => $cust_data['CUST_CHARGESID'],
            'CUST_NPWP'                => $cust_data['CUST_NPWP'],
            'CUST_VILLAGE'             => $cust_data['CUST_VILLAGE'],
            'CUST_DISTRICT'            => $cust_data['CUST_DISTRICT'],
            'BUSINESS_TYPE'            => $cust_data['BUSINESS_TYPE'],
            'GO_PUBLIC'                => $cust_data['GO_PUBLIC'],
            'GRUP_BUMN'                => $cust_data['GRUP_BUMN'],
            'DEBITUR_CODE'             => $cust_data['DEBITUR_CODE'],
            'COLLECTIBILITY_CODE'      => $cust_data['COLLECTIBILITY_CODE']
        ];

        $this->_db->insert('TEMP_CUSTOMER', $content);
    }

    public function updateTempCustomer($change_id, $cust_data)
    {
        $content = [
            'CUST_ID'                  => $cust_data['CUST_ID'],
            'CUST_CIF'                 => $cust_data['CUST_CIF'],
            'CUST_SECURITIES'          => $cust_data['CUST_SECURITIES'],
            'CUST_CODE'                => $cust_data['CUST_CODE'],
            'CUST_NAME'                => $cust_data['CUST_NAME'],
            'CUST_MODEL'               => $cust_data['CUST_MODEL'],
            'CUST_TYPE'                => $cust_data['CUST_TYPE'],
            'CUST_ADDRESS'             => $cust_data['CUST_ADDRESS'],
            'CUST_CITY'                => $cust_data['CUST_CITY'],
            'CUST_ZIP'                 => $cust_data['CUST_ZIP'],
            'CUST_PROVINCE'            => $cust_data['CUST_PROVINCE'],
            'COUNTRY_CODE'             => $cust_data['COUNTRY_CODE'],
            'CUST_CONTACT'             => $cust_data['CUST_CONTACT'],
            'CUST_PHONE'               => $cust_data['CUST_PHONE'],
            'CUST_EXT'                 => $cust_data['CUST_EXT'],
            'CUST_CONTACT_PHONE'       => $cust_data['CUST_CONTACT_PHONE'],
            'CUST_EMAIL'               => $cust_data['CUST_EMAIL'],
            'CUST_WEBSITE'             => $cust_data['CUST_WEBSITE'],
            'CUST_CHARGES_STATUS'      => $cust_data['CUST_CHARGES_STATUS'],
            'CUST_MONTHLYFEE_STATUS'   => $cust_data['CUST_MONTHLYFEE_STATUS'],
            'CUST_TOKEN_AUTH'          => $cust_data['CUST_TOKEN_AUTH'],
            'CUST_WORKFIELD'           => $cust_data['CUST_WORKFIELD'],
            'CUST_STATUS'              => $cust_data['CUST_STATUS'],
            'CUST_CREATED'             => $cust_data['CUST_CREATED'],
            'CUST_CREATEDBY'           => $cust_data['CUST_CREATEDBY'],
            'CUST_UPDATED'             => $cust_data['CUST_UPDATED'],
            'CUST_UPDATEDBY'           => $cust_data['CUST_UPDATEDBY'],
            'CUST_SUGGESTED'           => $cust_data['CUST_SUGGESTED'],
            'CUST_SUGGESTEDBY'         => $cust_data['CUST_SUGGESTEDBY'],
            'CUST_LIMIT_IDR'           => $cust_data['CUST_LIMIT_IDR'],
            'CUST_LIMIT_USD'           => $cust_data['CUST_LIMIT_USD'],
            'CUST_BG_LIMIT'            => $cust_data['CUST_BG_LIMIT'],
            'CUST_APP_TOKEN'           => $cust_data['CUST_APP_TOKEN'],
            'CUST_RLS_TOKEN'           => $cust_data['CUST_RLS_TOKEN'],
            'CUST_REVIEW'              => $cust_data['CUST_REVIEW'],
            'CUST_APPROVER'            => $cust_data['CUST_APPROVER'],
            'CUST_FINANCE'             => $cust_data['CUST_FINANCE'],
            'CUST_RDN_KEY'             => $cust_data['CUST_RDN_KEY'],
            'CUST_ADM_ROLE'            => $cust_data['CUST_ADM_ROLE'],
            'CUST_SAME_USER'           => $cust_data['CUST_SAME_USER'],
            'CUST_SPECIAL'             => $cust_data['CUST_SPECIAL'],
            'CUST_CHARGESID'           => $cust_data['CUST_CHARGESID'],
            'CUST_NPWP'                => $cust_data['CUST_NPWP'],
            'CUST_VILLAGE'             => $cust_data['CUST_VILLAGE'],
            'CUST_DISTRICT'            => $cust_data['CUST_DISTRICT'],
            'BUSINESS_TYPE'            => $cust_data['BUSINESS_TYPE'],
            'GO_PUBLIC'                => $cust_data['GO_PUBLIC'],
            'GRUP_BUMN'                => $cust_data['GRUP_BUMN'],
            'DEBITUR_CODE'             => $cust_data['DEBITUR_CODE'],
            'COLLECTIBILITY_CODE'      => $cust_data['COLLECTIBILITY_CODE']
        ];

        $where = ['CHANGES_ID = ?' => $change_id];
        $this->_db->update('TEMP_CUSTOMER', $content, $where);
    }

    public function insertTempPrivilege($change_id, $privilege_data, $user_data)
    {
        // print_r($privilege_data);die;
        foreach ($privilege_data as $privi) {
            $fuser_id = $user_data['CUST_ID'] . $user_data['USER_ID'];

            $content = array(
                'CHANGES_ID' => $change_id,
                'FPRIVI_ID'  => $privi['FPRIVI_ID'],
                'FUSER_ID'   => $fuser_id
            );

            $this->_db->insert('TEMP_FPRIVI_USER', $content);
        }
    }

    public function insertTempAdapterProfile($change_id, $cust_id, $adapterData = NULL)
    {

        $arrTrfType = array(
            'payroll',
            'manytomany',
            'emoney',
            'onetomany',
            'manytoone'
        );

        foreach ($arrTrfType as $trfType) {
            if ($adapterData !== null && $adapterData[$trfType] != 'defaults') {
                $adapter_profile_id = $adapterData[$trfType];

                $content = array(
                    'CHANGES_ID' => $change_id,
                    'CUST_ID' => $cust_id,
                    'TRF_TYPE'  => $trfType,
                    'ADAPTER_PROFILE_ID' => $adapter_profile_id
                );

                $this->_db->insert('TEMP_CUST_ADAPTER_PROFILE', $content);
            }
        }
    }


    protected function updateTempadm($changes_id, $cust_data)
    {
        $content = array(
            'CUST_ADM_ID'                => $cust_data
        );
        $whereArr = array('CHANGES_ID = ?' => $changes_id);
        $customerupdate = $this->_db->update('TEMP_CUSTOMER', $content, $whereArr);
    }

    protected function updateTempadm1($changes_id, $cust_data)
    {
        $content = array(
            'CUST_ADM1_ID'                => $cust_data
        );
        $whereArr = array('CHANGES_ID = ?' => $changes_id);
        $customerupdate = $this->_db->update('TEMP_CUSTOMER', $content, $whereArr);
    }


    // protected function updateSysadm($changes_id,$where,$cust_data)
    // {
    //    $content = $cust_data
    //    $whereArr = array('CHANGES_ID = ?'=>$changes_id,
    //                      'USER_ID' => $where
    //                    );
    //    $customerupdate = $this->_db->update('TEMP_CUSTOMER',$content,$whereArr);

    // }

    protected function updateTempsys($changes_id, $cust_data)
    {
        $content = array(
            'CUST_SYS_ID'                => $cust_data
        );
        $whereArr = array('CHANGES_ID = ?' => $changes_id);
        $customerupdate = $this->_db->update('TEMP_CUSTOMER', $content, $whereArr);
    }

    //    protected function updateTempCustomer($changes_id,$cust_data)
    //    {

    //       $content = array(
    //      // 'CUST_ID'                  => $cust_data['CUST_ID'],
    //      // 'CUST_CIF'                 => $cust_data['CUST_CIF'],
    //                    'CUST_NAME'                => $cust_data['CUST_NAME'],
    //                    'CUST_TYPE'                => $cust_data['CUST_TYPE'],
    //       'CUST_ADDRESS'             => $cust_data['CUST_ADDRESS'],
    //       'CUST_CITY'                => $cust_data['CUST_CITY'],
    //       'CUST_ZIP'                 => $cust_data['CUST_ZIP'],
    //       'CUST_PROVINCE'            => $cust_data['CUST_PROVINCE'],
    //                    'COUNTRY_CODE'             => $cust_data['COUNTRY_CODE'],
    //                    'CUST_CONTACT'             => $cust_data['CUST_CONTACT'],
    //       'CUST_PHONE'               => $cust_data['CUST_PHONE'],
    //                    'CUST_EXT'                 => $cust_data['CUST_EXT'],
    //       'CUST_FAX'                 => $cust_data['CUST_FAX'],
    //       'CUST_EMAIL'               => $cust_data['CUST_EMAIL'],
    //           'CUST_WEBSITE'             => $cust_data['CUST_WEBSITE'],

    //                    // 'CUST_EMOBILE'             => $cust_data['CUST_EMOBILE'],
    //                    // 'CUST_ISEMOBILE'           => $cust_data['CUST_ISEMOBILE'],
    //                    'CUST_CHARGES_STATUS'      => $cust_data['CUST_CHARGES_STATUS'],
    //                    //'CUST_ADMFEE_STATUS'       => $cust_data['CUST_ADMFEE_STATUS'],
    //                    // 'CUST_SPECIAL_RATE'        => $cust_data['CUST_SPECIAL_RATE'],
    //                    'CUST_MONTHLYFEE_STATUS'   => $cust_data['CUST_MONTHLYFEE_STATUS'],
    //                    'CUST_TOKEN_AUTH'   => $cust_data['CUST_TOKEN_AUTH'],

    //                  ///// 'CUST_STATUS'              => $cust_data['CUST_STATUS'],
    //                    'CUST_WORKFIELD'           => $cust_data['CUST_WORKFIELD'],
    //                    'CUST_LIMIT_IDR'       => $cust_data['CUST_LIMIT_IDR'],
    //                    'CUST_LIMIT_USD'       => $cust_data['CUST_LIMIT_USD']
    //                   /* 'CUST_CREATED'             => $cust_data['CUST_CREATED'],
    //                    'CUST_CREATEDBY'           => $cust_data['CUST_CREATEDBY'],*/
    //                    /*'CUST_UPDATED'             => $cust_data['CUST_UPDATED'],
    //                    'CUST_UPDATEDBY'           => $cust_data['CUST_UPDATEDBY'],*/
    //                   /////'CUST_SUGGESTED'        => $cust_data['CUST_SUGGESTED'],
    //                   /////'CUST_SUGGESTEDBY'      => $cust_data['CUST_SUGGESTEDBY'],
    //                    );

    //        //Zend_Debug::dump($cust_data);          die;  

    //       $whereArr = array('CHANGES_ID = ?'=>$changes_id);
    // $customerupdate = $this->_db->update('TEMP_CUSTOMER',$content,$whereArr);


    //    }


    protected function setUserData($prefix, $data, $change_id)
    {
        if (count($prefix)) {
            $group = $this->getGroupUser();
            foreach ($prefix as $a) {
                $user_data = $this->_userData;
                $user_data['CUST_ID'] = $data->cust_id;
                $user_data['FGROUP_ID'] = $group;
                $user_data['USER_STATUS'] = $this->_masterStatus['code']['active'];
                $user_data['USER_HASTOKEN'] = $this->_masterhasStatus['code']['no'];
                $id = 'user_id_' . $a;
                $user_data['USER_ID'] = $data->$id;
                $name = 'user_name_' . $a;
                $user_data['USER_NAME'] = $data->$name;
                $email = 'user_email_' . $a;
                $user_data['USER_EMAIL'] = $data->$email;
                $phone = 'user_phone_' . $a;
                $user_data['USER_PHONE'] = $data->$phone;
                $mobile = 'user_mobileno_' . $a;
                $user_data['USER_MOBILENO'] = $data->$mobile;
                $division = 'user_division_' . $a;
                $user_data['USER_DIVISION'] = $data->$division;
                $this->insertTempUser($change_id, $user_data);
            }
        }
    }


    protected function getGroupUser()
    {
        $select = $this->_db->select()
            ->from('M_FGROUP', array('FGROUP_ID'))
            ->where('UPPER(CUST_ID)=' . $this->_db->quote('BANK'))
            ->where('UPPER(FGROUP_NAME)=' . $this->_db->quote('SYSADMIN'));
        return $this->_db->fetchOne($select);
    }

    protected function getGroup($cust_id, $user_id)
    {
        $select = $this->_db->select()
            ->from(array('U' => 'M_USER_GROUP'), array())
            ->join(array('F' => 'M_FTEMPLATE'), 'U.CUST_ID=F.CUST_ID AND U.USER_ID=F.USER_ID', array())
            ->where('U.UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('U.UPPER(USER_ID)=' . $this->_db->quote((string)$user_id));
        return $this->_db->fetchOne($select);
    }

    protected function insertTempUser($change_id, $user_data)
    {
        $content = array(
            'CHANGES_ID'     => $change_id,
            'CUST_ID'        => $user_data['CUST_ID'],
            'USER_ID'        => $user_data['USER_ID'],
            'USER_NAME'      => $user_data['USER_NAME'],
            'USER_EMAIL'     => $user_data['USER_EMAIL'],
            'USER_PHONE'     => $user_data['USER_PHONE'],
            'USER_MOBILENO'  => $user_data['USER_MOBILENO'],
            'USER_DIVISION'  => $user_data['USER_DIVISION'],
            'USER_STATUS'    => $user_data['USER_STATUS'],
            'FGROUP_ID'      => $user_data['FGROUP_ID'],
            'USER_HASTOKEN'  => $user_data['USER_HASTOKEN']
        );
        if (isset($user_data['TOKEN_SERIALNO'])) $content = array_merge($content, array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
        $this->_db->insert('TEMP_USER', $content);
    }


    protected function insertTempUsersysadm($change_id, $user_data)
    {
        $content = $user_data;
        $content['CHANGES_ID'] = $change_id;
        // $content = array('CHANGES_ID'     => $change_id,
        //      'CUST_ID'        => $user_data['CUST_ID'],
        //                'USER_ID'        => $user_data['USER_ID'],
        //            'USER_FULLNAME'      => $user_data['USER_NAME'],
        //                'USER_EMAIL'     => $user_data['USER_EMAIL'],
        //                'USER_PHONE'     => $user_data['USER_PHONE'],
        //                'USER_MOBILE_PHONE'  => $user_data['USER_MOBILENO'],
        //                'USER_ISEMAIL'  => $user_data['USER_ISEMAIL'],
        //                'USER_FULLNAME'  => $user_data['USER_FULLNAME'],
        //                'USER_STATUS'    => $user_data['USER_STATUS'],
        //                'FGROUP_ID'      => $user_data['FGROUP_ID'],
        //                'USER_INDEX'      => $user_data['USER_INDEX']


        //                // 'USER_HASTOKEN'  => $user_data['USER_HASTOKEN']
        //     );
        if (isset($user_data['TOKEN_SERIALNO'])) $content = array_merge($content, array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
        $this->_db->insert('TEMP_USER', $content);
    }


    protected function insertTempUseradmin1($change_id, $user_data)
    {
        $content = array(
            'CHANGES_ID'     => $change_id,
            'CUST_ID'        => $user_data['CUST_ID'],
            'USER_ID'        => $user_data['USER_ID'],
            'USER_FULLNAME'      => $user_data['USER_NAME'],
            'USER_EMAIL'     => $user_data['USER_EMAIL'],
            'USER_PHONE'     => $user_data['USER_PHONE'],
            'USER_MOBILE_PHONE'  => $user_data['USER_MOBILENO'],
            'USER_ISEMAIL'  => $user_data['USER_ISEMAIL'],
            'USER_FULLNAME'  => $user_data['USER_FULLNAME'],
            'USER_STATUS'    => $user_data['USER_STATUS'],
            'FGROUP_ID'      => $user_data['FGROUP_ID'],
            'USER_INDEX'      => $user_data['USER_INDEX'],
            'USER_SUGGESTED'      => $user_data['USER_SUGGESTED'],
            'USER_SUGGESTEDBY'      => $user_data['USER_SUGGESTEDBY'],
            'TOKEN_ID'   => $user_data['TOKEN_ID']

            // 'USER_HASTOKEN'  => $user_data['USER_HASTOKEN']
        );
        if (isset($user_data['TOKEN_SERIALNO'])) $content = array_merge($content, array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
        $this->_db->insert('TEMP_USER', $content);
    }

    protected function insertTempUseradmin2($change_id, $user_data)
    {
        // echo "<pre>";
        // print_r($user_data);die;
        $content = array(
            'CHANGES_ID'     => $change_id,
            'CUST_ID'        => $user_data['CUST_ID'],
            'USER_ID'        => $user_data['USER_ID'],
            'USER_FULLNAME'      => $user_data['USER_NAME'],
            'USER_EMAIL'     => $user_data['USER_EMAIL'],
            'USER_PHONE'     => $user_data['USER_PHONE'],
            'USER_MOBILE_PHONE'  => $user_data['USER_MOBILENO'],
            'USER_ISEMAIL'  => $user_data['USER_ISEMAIL'],
            'USER_STATUS'    => $user_data['USER_STATUS'],
            'FGROUP_ID'      => $user_data['FGROUP_ID'],
            'USER_FULLNAME'  => $user_data['USER_FULLNAME'],
            'USER_INDEX'      => $user_data['USER_INDEX'],
            'USER_SUGGESTED'      => $user_data['USER_SUGGESTED'],
            'USER_SUGGESTEDBY'      => $user_data['USER_SUGGESTEDBY'],
            'TOKEN_ID'   => $user_data['TOKEN_ID']
            // 'USER_HASTOKEN'  => $user_data['USER_HASTOKEN']
        );
        if (isset($user_data['TOKEN_SERIALNO'])) $content = array_merge($content, array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
        $this->_db->insert('TEMP_USER', $content);
    }

    protected function insertTempUseradmin3($change_id, $user_data)
    {
        $content = array(
            'CHANGES_ID'     => $change_id,
            'CUST_ID'        => $user_data['CUST_ID'],
            'USER_ID'        => $user_data['USER_ID'],
            'USER_FULLNAME'      => $user_data['USER_NAME'],
            'USER_EMAIL'     => $user_data['USER_EMAIL'],
            'USER_PHONE'     => $user_data['USER_PHONE'],
            'USER_MOBILE_PHONE'  => $user_data['USER_MOBILENO'],
            'USER_ISEMAIL'  => $user_data['USER_ISEMAIL'],
            'USER_FULLNAME'  => $user_data['USER_FULLNAME'],
            // 'USER_DIVISION'  => $user_data['USER_DIVISION'],
            'USER_STATUS'    => $user_data['USER_STATUS'],
            'FGROUP_ID'      => $user_data['FGROUP_ID'],
            'USER_INDEX'      => $user_data['USER_INDEX']

            // 'USER_HASTOKEN'  => $user_data['USER_HASTOKEN']
        );
        if (isset($user_data['TOKEN_SERIALNO'])) $content = array_merge($content, array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
        $this->_db->insert('TEMP_USER', $content);
    }


    protected function insertTempAcc($countacc, $change_id, $acc_data)
    {
        if ($countacc) {
            foreach ($acc_data as $row) {
                for ($i = 0; $i < 5; $i++) {
                    $content = array(
                        'CHANGES_ID'     => $change_id,
                        'CUST_ID'        => $row[$i]['CUST_ID'],
                        'ACCT_NO'        => $row[$i]['ACCT_NO'],
                        'CCY_ID'         => $row[$i]['CCY_ID'],
                        'ACCT_NAME'      => $row[$i]['ACCT_NAME'],
                        'ACCT_GROUP'     => $row[$i]['ACCT_GROUP'],
                        'ACCT_TYPE'      => $row[$i]['ACCT_TYPE'],
                        'ACCT_LIMIT'     => $row[$i]['ACCT_LIMIT'],
                        'BANK_ID'        => $row[$i]['BANK_ID'],
                        'COUNTRY_CODE'   => $row[$i]['COUNTRY_CODE'],
                        'BANK_NAME'      => $row[$i]['BANK_NAME'],
                        'BRANCH_CODE'    => $row[$i]['BRANCH_CODE'],
                        'ACCT_DESC'      => $row[$i]['ACCT_DESC']
                    );
                    $this->_db->insert('TEMP_CUSTOMER_ACCT', $content);
                }
            }
        }
    }

    protected function insertTempLimit($change_id, $limit_data)
    {

        if (count($limit_data)) {
            foreach ($limit_data as $row) {
                $content = array(
                    'CHANGES_ID'       => $change_id,
                    'CUST_ID'          => $row['CUST_ID'],
                    'DR_DAILY_LIMIT'   => $row['DR_DAILY_LIMIT'],
                    'CR_DAILY_LIMIT'   => $row['CR_DAILY_LIMIT'],
                    'AMT_BLOCK_LIMIT'  => $row['AMT_BLOCK_LIMIT'],
                    'CCY_ID'           => $row['CCY_ID']
                );
                $this->_db->insert('TEMP_CUSTOMER_LIMIT', $content);
            }
        }
    }

    protected function getTempCustomer($changeId)
    {
        $select  = $this->_db->fetchRow(
            $this->_db->select()
                ->from(array('T' => 'TEMP_CUSTOMER'))
                ->joinleft(array('C' => 'M_COUNTRY'), 'C.COUNTRY_CODE=T.COUNTRY_CODE', array('COUNTRY_NAME'))
                ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS'))
                ->where('T.CHANGES_ID = ?', $changeId)
        );

        return $select;
    }


    protected function getUserCust($userid)
    {
        $select  = $this->_db->fetchRow(
            $this->_db->select()
                ->from(array('T' => 'TEMP_USER'), array('*'))
                ->where('T.USER_ID = ?', $userid)
        );

        return $select;
    }

    protected function getUserCustEdit($userid)
    {
        $select  = $this->_db->fetchRow(
            $this->_db->select()
                ->from(array('T' => 'M_USER'), array('*'))
                ->where('T.USER_ID = ?', $userid)
        );

        return $select;
    }
}
