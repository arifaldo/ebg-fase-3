<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class notification_ChangepwsuccessController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ns = new Zend_Session_Namespace('FVC');
		
		if (isset($ns->backURL) && !(empty($ns->backURL))){
			$this->view->backURL = $ns->backURL;
		}
		$ns->backURL = '';
	}

	public function changeAction() {
		if(Zend_Auth::getInstance()->hasIdentity()){
			$auth = Zend_Auth::getInstance()->getIdentity();
			  $this->_userIdLogin   = $auth->userIdLogin;
			  $this->_custIdLogin   = $auth->custIdLogin;
			  Zend_Auth::getInstance()->clearIdentity();
			$CustomerUser	= new CustomerUser(strtoupper($this->_custIdLogin),strtoupper($this->_userIdLogin));
			$CustomerUser->forceLogout();

			$forceBySession = $this->_request->getParam('session');

			if($forceBySession == '1'){
				$this->_redirect('/authorizationacl/index/disableuser');
			}
		}
		return true;
		// $this->_redirect('/');
	}
}
