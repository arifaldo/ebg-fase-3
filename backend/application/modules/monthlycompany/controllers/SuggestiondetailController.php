<?php

require_once 'Zend/Controller/Action.php';

class Monthlycompany_SuggestiondetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$changeid = $this->_getParam('changes_id');
  	
	$select = $this->_db->select()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
	$select -> where("A.CHANGES_ID = ?", $changeid);
	$select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
	$result = $select->query()->FetchAll();
	$this->view->result=$result;
	$custid = $result[0]['KEY_FIELD'];
	
  	if(!$result)
	{
		$this->_redirect('/notification/invalid/index');
	}
	
	$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		//$selectprk = $selectprk->__toString();
		//$selectnoprk = $selectnoprk->__toString();
		//$selectnoprk2 = $selectnoprk2->__toString();
		
		//$unionquery = $this->_db->select()
								//->union(array($selectprk,$selectnoprk));
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		//$selectunion = $this->_db->select()
		//					->from (($unionquery),array('*'));
		//$resultunion = $selectunion->query()->FetchAll();
		//$this->view->resultaccount = $resultunion;
		//Zend_Debug::dump($resultunion);die;
		
		/*$chargeaccount = $this->_db->select()
								->union(array($selectnoprk2,$selectnoprk));*/
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;
	
	//Zend_Debug::dump($result);die;
	
	$select2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));			
	$select2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
	$result2 = $select2->query()->FetchAll();
	$this->view->result2 = $result2;
			     
		
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("MONTHLYFEE_TYPE = '4'");
			$select4 -> order("TRA_FROM ASC");
			$result4 = $this->_db->fetchAll($select4);
			$this->view->result4cur = $result4;
			//Zend_Debug::dump($result4);die;
		
		$cur = 0;
		foreach($result4 as $list)
		{
   			$idamt = 'amount'.'current'.$cur;
   			$idfrom = 'from'.'current'.$cur;
   			$idto = 'to'.'current'.$cur;
			$amt = Application_Helper_General::convertDisplayMoney($list['AMOUNT']);
			$this->view->$idamt = $amt;
			$this->view->$idfrom = $list['TRA_FROM'];
			$this->view->$idto = $list['TRA_TO'];	
			$cur++;
		}
		
		$cekacct = 'cek'.'current';
		$select3 = $this->_db->select()->distinct()
			        ->from(array('A' => $selectprk),array('*'));
		$select3 -> where("A.ACCT_NO LIKE ".$this->_db->quote($result4[0]['CHARGES_ACCT_NO']));
		$result3 = $this->_db->fetchRow($select3);
		$this->view->$cekacct = $result3['ACCT_NO'].' / '.$result3['ACCT_NAME'].' ('.$result3['CCY_ID'].')';
		$this->view->cur=$cur;
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
  			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'TEMP_CHARGES_MONTHLY'),array('*'));
			$select4 -> where("A.CHANGES_ID = ?", $changeid);
			$select4 -> order("TRA_FROM ASC");
			$result4 = $this->_db->fetchAll($select4);
			$this->view->result4sug = $result4;
		
		$sug = 0;
		foreach($result4 as $list)
		{
   			$idamt = 'amount'.'suggest'.$sug;
   			$idfrom = 'from'.'suggest'.$sug;
   			$idto = 'to'.'suggest'.$sug;
			$amt = Application_Helper_General::convertDisplayMoney($list['AMOUNT']);
			$this->view->$idamt = $amt;
			$this->view->$idfrom = $list['TRA_FROM'];
			$this->view->$idto = $list['TRA_TO'];	
			$this->view->sug = $sug;
			$sug++;
		}
		$cekacct = 'cek'.'suggest';
		$select3 = $this->_db->select()->distinct()
			        ->from(array('A' => $selectprk),array('*'));
		$select3 -> where("A.ACCT_NO LIKE ".$this->_db->quote($result4[0]['CHARGES_ACCT_NO']));
		$result3 = $this->_db->fetchRow($select3);
		$this->view->$cekacct = $result3['ACCT_NO'].' / '.$result3['ACCT_NAME'].' ('.$result3['CCY_ID'].')';

	$this->view->changes_id = $changeid;
    $this->view->typeCode = array_flip($this->_changeType['code']);
    $this->view->typeDesc = $this->_changeType['desc'];
    $this->view->modulename = $this->_request->getModuleName();
	//echo $select2; die;
	Application_Helper_General::writeLog('CHCL','View company charges changes list');
    
  }
}