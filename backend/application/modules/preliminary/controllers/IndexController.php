<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class preliminary_IndexController extends Application_Main
{
    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newlayout');

        $bgRegNumber = $this->_getParam('bgnumb');

        // $table = "M_PRELIMINARY";
        // $tableMember = 'M_PRELIMINARY_MEMBER';

        $cekTable = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE')
            ->where('BG_REG_NUMBER = ?', $bgRegNumber)
            ->query()->fetch();

        // relationship --------------
        $getRelationship = $this->_db->select()
            ->from("BG_RELATIONSHIP_WITH_REPORTER")
            ->query()->fetchAll();

        $newRelation = [];

        foreach ($getRelationship as $key => $value) {
            $newRelation[$value['NO']] = $value['DESC'];
        }

        $this->view->getRelationship = $newRelation;
        // end relationship -------------

        $temp = true;
        if ($cekTable) {
            if (!in_array($cekTable['BG_STATUS'], ['1', '2', '3', '4', '11', '19'])) {
                $table = 'TEMP_BANK_GUARANTEE_PRELIMINARY';
                $tableMember = 'TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER';
            } else {
                $table = "M_PRELIMINARY";
                $tableMember = 'M_PRELIMINARY_MEMBER';
            }
        } else {

            $temp = false;
            $cekTable = $this->_db->select()
                ->from('T_BANK_GUARANTEE')
                ->where('BG_REG_NUMBER = ?', $bgRegNumber)
                ->query()->fetch();

            $table = 'T_BANK_GUARANTEE_PRELIMINARY';
            $tableMember = 'T_BANK_GUARANTEE_PRELIMINARY_MEMBER';
        }

        Zend_Session::namespaceUnset('tempPreliminary');

        $config = Zend_Registry::get('config');

        $entityCode = $config["bg"]["entity"]["code"];
        $entityDesc = $config["bg"]["entity"]["desc"];
        $entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));
        $this->view->entityArr = $entityArr;

        $citizenshipCode = $config["bg"]["citizen"]["code"];
        $citizenshipDesc = $config["bg"]["citizen"]["desc"];
        $citizenshipArr  = array_combine(array_values($citizenshipCode), array_values($citizenshipDesc));
        $this->view->citizenshipArr = $citizenshipArr;

        $statusCode = $config["bg"]["statusowner"]["code"];
        $statusDesc = $config["bg"]["statusowner"]["desc"];
        $statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));
        $this->view->statusArr = $statusArr;

        $cekView = $this->_getParam("view");
        $this->view->cekView = $cekView;

        $getCustId = $this->_getParam("custid");


        $getPosition = $this->_db->select()
            ->from('M_POSITION')
            ->query()->fetchAll();
        $positionArr = Application_Helper_Array::listArray($getPosition, 'CODE', 'POSITION');
        $this->view->positionArr = $positionArr;

        $getRelationship = $this->_db->select()
            ->from('BG_RELATIONSHIP_WITH_REPORTER')
            ->query()->fetchAll();
        $relationshipArr = Application_Helper_Array::listArray($getRelationship, 'NO', 'RELATION_WITH_BANK');
        $this->view->relationshipArr = $relationshipArr;

        // if ($cekView == "") {
        //     return $this->_redirect("/home/dashboard");
        // }

        $cust_id = $cekTable['CUST_ID'];

        $this->view->cust_id = $cust_id;

        $data = $this->_db->select()
            ->from(array('MP' => $table))
            ->joinLeft(
                array('MC' => 'M_CUSTOMER'),
                'MC.CUST_ID = MP.CUST_ID',
                array('CUST_NAME')
            );

        if ($temp) {
            if (!in_array($cekTable['BG_STATUS'], ['1', '2', '3'])) {
                $data->where('BG_REG_NUMBER = ?', $cekTable['BG_REG_NUMBER']);
            } else {
                $data->where('MP.CUST_ID = ?', $cekTable['CUST_ID']);
            }
        } else {
            $data->where('BG_REG_NUMBER = ?', $cekTable['BG_REG_NUMBER']);
        }

        $data->where('MP.CUST_ID = ?', $cekTable['CUST_ID']);

        $data = $data->query()->fetch();

        $getCustName = $this->_db->select()
            ->from("M_CUSTOMER", ["CUST_NAME"])
            ->where("CUST_ID = ?", $cust_id)
            ->query()->fetchAll();
        $data["CUST_NAME"] = $getCustName[0]["CUST_NAME"];

        $this->view->data = $data;

        $dataMember = $this->_db->select()
            ->from($tableMember)
            ->joinLeft(["C" => "M_CITYLIST"], "C.CITY_CODE = KAB_CITY", ["C.CITY_NAME"]);
        if ($temp) {
            if (!in_array($cekTable['BG_STATUS'], ['1', '2', '3'])) {
                $dataMember->where('BG_REG_NUMBER = ?', $cekTable['BG_REG_NUMBER']);
            } else {
                $dataMember->where('CUST_ID = ?', $cekTable['CUST_ID']);
            }
        } else {
            $dataMember->where('BG_REG_NUMBER = ?', $cekTable['BG_REG_NUMBER']);
        }
        // ->where('CUST_ID = ?', $cust_id)
        $dataMember->where('CUST_ID = ?', $cekTable['CUST_ID']);
        $dataMember = $dataMember->query()->fetchAll();
        $this->view->dataMember = $dataMember;
        // echo '<pre>';
        // print_r($data);
        // echo '</pre><br>';

        $id         = $this->_getParam('id');
        $mid         = $this->_getParam('mid');
        $download     = $this->_getParam('download');

        if ($download == 'file') {
            $path = UPLOAD_PATH . '/document/submit/';

            if ($id) {
                if ($id == 1) {
                    $file = $data['NPWP_FILE'];
                } elseif ($id == 2) {
                    $file = $data['PENDIRIAN_NUMBER_FILE'];
                } elseif ($id == 3) {
                    $file = $data['PERUBAHAN_FILE'];
                } elseif ($id == 4) {
                    $file = $data['SK_PERUBAHAN_FILE'];
                } elseif ($id == 5) {
                    $file = $data['SK_PENGESAHAN_FILE'];
                }
                $this->_helper->download->file($file, $path . $file);
            }

            if ($mid) {
                // $member = $this->_db->select()
                //     ->from('M_PRELIMINARY_MEMBER')
                //     ->where('CUST_ID = ?', $cust_id)
                //     ->where('ID_NUMBER = ?', $mid);


                $searchId = array_search($mid, array_column($dataMember, 'ID_NUMBER'));

                if ($searchId !== false) {
                    $member = $dataMember[$searchId];
                    $this->_helper->download->file($member['ID_FILE'], $path . $member['ID_FILE']);
                }
            }
        } else {
            Application_Helper_General::writeLog('VSPD', 'View Preliminary Document');
        }
    }
}
