<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class token_availableController extends Application_Main
{
	public function indexAction() 
	{ 
		$this->_helper->layout->setLayout('newlayout');
		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

		$token = new Service_Token($custId, $userId);
		// echo "<pre>";
		// var_dump($token);
		$res = $token->tokenHardwareList(array());
		// var_dump($res);die;
		$resData = (isset($res['ResponseData']) && count($res['ResponseData']) ? $res['ResponseData'] : array());
		$tokenData = $this->view->tokenData = $resData;
		
		$this->view->paginator = $resData;
 
    	// END proses pengambilan data filter,display all,sorting
		// echo $select2; die;
    
        if($csv || $pdf)
        {
        	$result = $resData;
        	
        	foreach($result as $key=>$value)
			{
				$result[$key]["USER_CREATED"] = Application_Helper_General::convertDate($value["USER_CREATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$result[$key]["USER_SUGGESTED"] = Application_Helper_General::convertDate($value["USER_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$result[$key]["USER_UPDATED"] = Application_Helper_General::convertDate($value["USER_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
			
        	$header = Application_Helper_Array::simpleArray($fields,'label');

	        if($csv)
			{
				Application_Helper_General::writeLog('RPCU','Download CSV Customer User List');
				$this->_helper->download->csv($header,$result,null,'Customer User List');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('RPCU','Download PDF Customer User List');
				$this->_helper->download->pdf($header,$result,null,'Customer User List');
			}
        }
        else
        {
        	Application_Helper_General::writeLog('RPCU','View Customer User List');
        }
        
	    // $this->paging($select2);
	    //die;
	    // $this->view->fields = $fields;
	    // $this->view->filter = $filter;
     //insert log
	} 
}