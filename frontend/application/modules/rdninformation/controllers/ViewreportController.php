<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
class rdninformation_ViewreportController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	protected $_acctArr = array();

	public function initController()
	{

		$model = new rdninformation_Model_Rdninformation();

		$Customer = new CustomerUser($this->_custIdLogin,$this->_userIdLogin);
		$this->_acctArr = $Customer->getAccounts();
		$this->view->AccArr =  $this->_acctArr;


	}

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$this->view->PERIOD 		= 'today';
		Application_Helper_General::writeLog('ACDT','Viewing');

		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;
// print_r($userId );die;
		$model = new rdninformation_Model_Rdninformation();



		$userCIF = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_USER'))
				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
				->limit(1)
		);

		$param['userId'] = $userId;
		$param['custId'] = $custId;

		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];

//		$svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
//		$resultKursEx = $svcInquiry->rateInquiry();
//
//		$kurssell = '';
//		$kurs = '';
//		$book = '';
//		if($resultKursEx['ResponseCode']=='00'){
//				$kursList = $resultKursEx['DataList'];
//				$kurssell = '';
//				$kurs = '';
//	// 		    print_r($resultKursEx['DataList']);die;
//				$this->view->rateKurs 		= $resultKursEx['DataList'];
//
//		}else{
//
//				$this->view->rateKurs 		= array();
//
//		}

		//check param for personal table





		//cifaccouninquiry
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$core = array();
		$svcAccount = new Service_Account($userCIF['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
		$result		= $svcAccount->cifaccountInquiry($core);
		// $result		= array();
		if($result['ResponseDesc'] != 'Success'){
			if($result['ResponseDesc'] != 'No Data'){
			$this->view->error_per = $result['ResponseDesc'];
			}
		}
//		Zend_Debug::dump($result);
//		Zend_Debug::dump($core->accountList);
//		echo count($core->accountList). " acct list"; die;

		if(count($core->accountList) > 0){

				$arrResult = array();
					$err = array();

					if(count($core->accountList) > 1){
// 			   		print_r($core->accountList);
						//array data lebih dari 1
						foreach ($core->accountList as $key => $val){
							if($val->status == '0'){
								$arrResult = array_merge($arrResult, array($val->accountNo => $val));
								array_push($err, $val->accountNo);
							}
// 					   	echo 'here';
						}
					}
					else{
						//array kurang dari 2
						foreach ($core as $data){
							if($data->status == '0'){
								$arrResult = array_merge($arrResult, array($data->accountNo => $data));
								array_push($err, $data->accountNo);
							}
						}
					}

				$final = array();
				$i = 0;
//
				foreach($arrResult as $key => $val){
					$final[$i++] = $val;
				}


				if(count($final) < 1){
					$this->view->data1 = '0';
				}
				else{
					$this->view->data = $final;
				}


//				Zend_Debug::dump($final, "final"); die;
					foreach ($final as $dataProductPlan){
//				   		if($dataProductPlan->status == '0'){

								/*if (isset($dataProductPlan->productPlan) && isset($dataProductPlan->planCode))
								{
								$select_product_type	= $this->_db->select()
											->from(array('M_PRODUCT_TYPE'),'PRODUCT_NAME')
				//							->where('PRODUCT_CODE = ?','10');
				//							->where("PRODUCT_CODE IN(?)", $dataPro)
											->where("PRODUCT_CODE = ?", $dataProductPlan->productPlan)
											->where("PRODUCT_PLAN = ?", $dataProductPlan->planCode);
				//							->where('PRODUCT_PLAN = ?','62');
								$userData_product_type = $this->_db->fetchCol($select_product_type);

								$dataProductPlan->planName = $userData_product_type[0];
								$dataProductPlanOri = (($dataProductPlan->planName == null)?'':$dataProductPlan);
								}else{*/
									$dataProductPlanOri = $dataProductPlan;
								//}

							$dataUser[] = $dataProductPlanOri;

//				   		}
					}
					// var_dump($dataUser);die;
//				echo count($dataUser); die;
				// 	echo "<pre>";
				// Zend_Debug::dump($dataUser,"datauser"); die; 
				if (count($dataUser) > 0)
				{
					$datapers = array();
					$i = 0;
					foreach ( $dataUser as $key=>$row ) {

						$dataprouct = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_PRODUCT_TYPE')) 
									 ->where("PRODUCT_CODE =?", $row->productType)
									 // ->where("PRODUCT_PLAN =?", $row->planCode)
							);
						// print_r($dataprouct);
						if(!empty($dataprouct)){
							if($dataprouct['ACCT_TYPE'] == '4'){
								$Account = new Account($row->accountNo,$row->ccy);
								$Account->setFlag(false);
								$Account->checkBalance();

								// var_dump($Account);die;

								if($row->planCode != NULL && $row->productType != NULL){
									$product = $model->getProduct($row->planCode,$row->productPlan);
									$temp = $model->getCustAccount(array('userId'=>$this->_userIdLogin));

									if (empty($product)){
										$productAggregation = $model->getProductAggregation($Account->getCoreProductPlan(),$row->productType);
										$product_name = $productAggregation['0']['PRODUCT_NAME'];
									}else{
										$product_name = $product['0']['PRODUCT_NAME'];
										$acct_type = $product['0']['ACCT_TYPE'];
										$acct_name = $temp['0']['ACCT_NAME'];
										if($result['ResponseCode'] == '00'){
											$status = "Active";
										}
									}
								}else{
									$product_name = 'N/A';
								}

								$datapers[$i]['ACCT_TYPE'] = $acct_type;
								$datapers[$i]['ACCT_PRODUCT_NAME'] = $product_name;
								$datapers[$i]['ACCT_NO'] = $row->accountNo;
								$datapers[$i]['ACCT_NAME'] = $this->_custNameLogin;
								$datapers[$i]['STATUS'] = $status;
								$datapers[$i]['CCY_ID'] = Application_Helper_General::getCurrCode($row->ccy);
								$datapers[$i]['PRODUCT_NAME'] = $row->PRODUCT_NAME;
								$datapers[$i]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
								$i++;
							}
						}
					}
				}

		}
		// var_dump($datapers);die;
		$this->view->datapers = $datapers;
		$logDesc = 'Viewing';


		Application_Helper_General::writeLog('BAIQ',$logDesc);
		// echo '<pre>';
		// print_r ($datapers); die;
		$this->view->userId = $this->_userIdLogin;
		$this->view->personal = $datapers;
		$this->view->creditcard = $datacc;
		$this->view->deposit = $datadeposit;
		$this->view->loan = $dataloan;
	}

	public function viewAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$this->view->PERIOD 		= 'today';
		Application_Helper_General::writeLog('ACDT','Viewing');

		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;
// print_r($userId );die;
		$model = new rdninformation_Model_Rdninformation();
		
//		$userCIF = $this->_db->fetchRow(
//				$this->_db->select()
//				->from(array('C' => 'M_USER'))
//				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
//				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
//				->limit(1)
//		);

		$param['userId'] = $userId;
		$param['custId'] = $custId;

		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];

//		$svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
//		$resultKursEx = $svcInquiry->rateInquiry();
//
//		$kurssell = '';
//		$kurs = '';
//		$book = '';
//		if($resultKursEx['ResponseCode']=='00'){
//				$kursList = $resultKursEx['DataList'];
//				$kurssell = '';
//				$kurs = '';
//	// 		    print_r($resultKursEx['DataList']);die;
//				$this->view->rateKurs 		= $resultKursEx['DataList'];
//
//		}else{
//
//				$this->view->rateKurs 		= array();
//
//		}

		//check param for personal table





		//cifaccouninquiry
//		$app = Zend_Registry::get('config');
//		$app = $app['app']['bankcode'];
//
//		$core = array();
//		$svcAccount = new Service_Account($userCIF['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
//		$result		= $svcAccount->cifaccountInquiry($core);
//		if($result['ResponseDesc'] != 'Success'){
//			if($result['ResponseDesc'] != 'No Data'){
//			$this->view->error_per = $result['ResponseDesc'];
//			}
//		}
////		Zend_Debug::dump($result);
////		Zend_Debug::dump($core->accountList);
////		echo count($core->accountList). " acct list"; die;
//
//		if(count($core->accountList) > 0){
//
//				$arrResult = array();
//					$err = array();
//
//					if(count($core->accountList) > 1){
//// 			   		print_r($core->accountList);
//						//array data lebih dari 1
//						foreach ($core->accountList as $key => $val){
//							if($val->status == '0'){
//								$arrResult = array_merge($arrResult, array($val->accountNo => $val));
//								array_push($err, $val->accountNo);
//							}
//// 					   	echo 'here';
//						}
//					}
//					else{
//						//array kurang dari 2
//						foreach ($core as $data){
//							if($data->status == '0'){
//								$arrResult = array_merge($arrResult, array($data->accountNo => $data));
//								array_push($err, $data->accountNo);
//							}
//						}
//					}
//
//				$final = array();
//				$i = 0;
////
//				foreach($arrResult as $key => $val){
//					$final[$i++] = $val;
//				}
//
//
//				if(count($final) < 1){
//					$this->view->data1 = '0';
//				}
//				else{
//					$this->view->data = $final;
//				}
//
//
//				//Zend_Debug::dump($final, "final"); die;
//					foreach ($final as $dataProductPlan){
////				   		if($dataProductPlan->status == '0'){
//
//								/*if (isset($dataProductPlan->productPlan) && isset($dataProductPlan->planCode))
//								{
//								$select_product_type	= $this->_db->select()
//											->from(array('M_PRODUCT_TYPE'),'PRODUCT_NAME')
//				//							->where('PRODUCT_CODE = ?','10');
//				//							->where("PRODUCT_CODE IN(?)", $dataPro)
//											->where("PRODUCT_CODE = ?", $dataProductPlan->productPlan)
//											->where("PRODUCT_PLAN = ?", $dataProductPlan->planCode);
//				//							->where('PRODUCT_PLAN = ?','62');
//								$userData_product_type = $this->_db->fetchCol($select_product_type);
//
//								$dataProductPlan->planName = $userData_product_type[0];
//								$dataProductPlanOri = (($dataProductPlan->planName == null)?'':$dataProductPlan);
//								}else{*/
//									$dataProductPlanOri = $dataProductPlan;
//								//}
//
//							$dataUser[] = $dataProductPlanOri;
//
////				   		}
//					}
//					// var_dump($dataUser);die;
////				echo count($dataUser); die;
////				Zend_Debug::dump($dataUser); die;
//				if (count($dataUser) > 0)
//				{
//					$datapers = array();
//
//					foreach ( $dataUser as $key=>$row ) {
//
//						$Account = new Account($row->accountNo,$row->ccy);
//						$Account->setFlag(false);
//						$Account->checkBalance();
//
//						// var_dump($Account);die;
//
//						if($row->planCode != NULL && $row->productType != NULL){
//							$product = $model->getProduct($row->planCode,$row->productPlan);
//							$temp = $model->getCustAccount(array('userId'=>$this->_userIdLogin));
//
//							if (empty($product)){
//								$productAggregation = $model->getProductAggregation($Account->getCoreProductPlan(),$row->productType);
//								$product_name = $productAggregation['0']['PRODUCT_NAME'];
//							}else{
//								$product_name = $product['0']['PRODUCT_NAME'];
//								$acct_type = $product['0']['ACCT_TYPE'];
//								$acct_name = $temp['0']['ACCT_NAME'];
//								if($result['ResponseCode'] == '00'){
//									$status = "Active";
//								}
//							}
//						}else{
//							$product_name = 'N/A';
//						}
//
//						$datapers[$key]['ACCT_TYPE'] = $acct_type;
//						$datapers[$key]['ACCT_PRODUCT_NAME'] = $product_name;
//						$datapers[$key]['ACCT_NO'] = $row->accountNo;
//						$datapers[$key]['ACCT_NAME'] = $Account->getCoreAccountName();
//						$datapers[$key]['STATUS'] = $status;
//						$datapers[$key]['CCY_ID'] = Application_Helper_General::getCurrCode($row->ccy);
//						$datapers[$key]['PRODUCT_NAME'] = $row->PRODUCT_NAME;
//						$datapers[$key]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
//
//						$this->view->datapers = $datapers;
//						$nama = $Account->getCoreAccountName();
//						// var_dump($nama); die;
//
//					}
//				}
//
//		}

		$logDesc = 'Viewing';


		Application_Helper_General::writeLog('BAIQ',$logDesc);
		// echo '<pre>';
		// print_r ($datapers); die;
		$this->view->nama = $nama;



		$set = new Settings();
		$acct_stat_layout = $set->getSetting('acct_stat_layout');
		$this->view->acct_stat_layout = $acct_stat_layout;
		$listAccValidate = Application_Helper_Array::simpleArray($this->_acctArr,'ACCT_NO');

		if(!empty($final)){
		foreach ($final as $dataProductPlan){
						$dataProductPlanOri = $dataProductPlan;
						$dataUser[] = $dataProductPlanOri;
		}
		}

		$page     = $this->_getParam('page');
		$back     = $this->_getParam('back');
		$csv      = $this->_getParam('csv');
		$pdf      = $this->_getParam('pdf');
		$account  = $this->_getParam('ACCTSRC');

		$isNew = (empty($page) && empty($csv) && empty($pdf))? true: false;	// isNew = true, then inquiry to host
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$filterArr = array	(
								'ACCTSRC'		=> 	array('StringTrim','StripTags')
							);

		$validators = array(
								'periodtype' => array(
													'NotEmpty',
													array('InArray', array('haystack' => array('today', 'history'))),
													'messages' => array($this->language->_('Error: You must select Period'),
																		//'Error: You must select Period',
																		//'Error: Period must be today or history',
																		$this->language->_('Error: Period must be today or history'),
																		),
												),
								'ACCTSRC' => array(
													'NotEmpty',
													// array('InArray', array('haystack' => $listAccValidate)),
													'messages' => array($this->language->_('Error: You must select account'),
																		//'Error: You must select account',
																		// $this->language->_('Error: You dont have right to this account'),
																		//'Error: You dont have right to this account',
																	),
												)
							);

		$zf_filter_input = new Zend_Filter_Input($filterArr,$validators,$this->_request->getParams(),$this->_optionsValidator);
		// var_dump($this->_request->getParams());die;

		$period = $zf_filter_input->periodtype;

		$dateFrom = '';
		$dateTo   = '';
		if ($period == 'history')
		{
			$dateFrom = $this->_getParam('history_from');
			$dateTo   = $this->_getParam('history_to');
			if(!empty($dateFrom))
			{
				$this->view->DATE_FROM		=$dateFrom;
				$FormatDate = new Zend_Date($dateFrom, $this->_dateDisplayFormat);
				$dateFromView  = $FormatDate->toString($this->_dateViewFormat);
				$this->view->DATE_FROM_view	= $dateFromView;
				$dateFrom  = $FormatDate->toString($this->_dateDBFormat);
			}
			if(!empty($dateTo))
			{
				$this->view->DATE_TO		= $dateTo;
				$FormatDate = new Zend_Date($dateTo, $this->_dateDisplayFormat);
				$dateToView    = $FormatDate->toString($this->_dateViewFormat);
				$this->view->DATE_TO_view	= $dateToView;
				$dateTo    = $FormatDate->toString($this->_dateDBFormat);
			}
		}
		else{
			$FormatDate = Zend_Date::now();
			$dateNow    = $FormatDate->toString($this->_dateViewFormat);
			$this->view->DATE_FROM_view = $dateNow;
			$this->view->DATE_TO_view = $dateNow;
		}

		$this->view->ACCTSRC 		= $zf_filter_input->ACCTSRC;
		$this->view->PERIOD 		= $period;

		if (!empty($back))
		{
			$this->render( 'index' );
		}
		else
		{
			$errorMsg = "";
			if ($period == 'history' && (empty($dateFrom) || empty($dateTo)))
			{
				$errorMsg = $this->language->_('Error: History date range has not been chosen yet');
			}
			elseif ($period == 'history')
			{
				$dateFromDB = Application_Helper_General::convertDate($dateFrom, $this->_dateDBFormat, $this->_dateDisplayFormat);
				$dateToDB   = Application_Helper_General::convertDate($dateTo, $this->_dateDBFormat, $this->_dateDisplayFormat);
				$dateHistory = Application_Helper_General::convertDate((date("d/m/Y")), $this->_dateDBFormat, $this->_dateDisplayFormat);
//				$dateDiff	= Application_Helper_General::date_diff_days($dateFromDB, $dateHistory);
				$dateDiff	= Application_Helper_General::date_diff_days($dateFromDB, $dateToDB);

				$maxInq 	= date('Y-m-d', strtotime('-89 days', strtotime(date('Y-m-d'))));

				$maxDate	= date('Y-m-d', strtotime('+10 days', strtotime(date('Y-m-d'))));

				if (strtotime($dateFromDB) > strtotime($dateToDB))
				{
					$errorMsg = $this->language->_('Error: From Date must not be greater than To Date');
				}
				//elseif (strtotime($dateToDB) > strtotime(date('Y-m-d')))
				elseif (strtotime($dateToDB) > strtotime($maxDate)) //today + 10 days
				{
					//$errorMsg = $this->language->_('Error: History period must be less than today');
					$errorMsg = $this->language->_('Error: End date greater than today date');
				}
				elseif ($dateDiff > 30)
				{
					$errorMsg = $this->language->_('Error: Account statement are retained for the last 31 days');;
				}
 				elseif ($dateFromDB < $maxInq)
 				{
 					$errorMsg = $this->language->_('Error: Account statement are retained for the last 90 days');
 				}
			}

			if($zf_filter_input->isValid() && $errorMsg == "")
			{

				if (isset($zf_filter_input->ACCTSRC)){

					$model = new rdninformation_Model_Rdninformation();
					$temp = $model->getUserAccount(array('acctNo'=>$zf_filter_input->ACCTSRC));
					// var_dump($temp); die;
					$accData = $temp[0];
					$ccy 		= "IDR";
					$currCode = Application_Helper_General::getCurrNum($ccy);

				}

				if ($isNew === true)
				{
					$data =  new Service_Account($zf_filter_input->ACCTSRC,$currCode);
					$dataInquiry = $data->accountInquiry('AB',FALSE);
					$dataAvalilableBalance = $dataInquiry["Balance"];
				    $dataAccountType = $dataInquiry['ProductType'];

				    if ($period == 'today'){
				    	$dataStatement = $data->accountStatementToday($isMobile = FALSE, $dataAccountType);
				    }
					else{
						$dataStatement = $data->accountStatementHistory($dateFromDB, $dateToDB, $isMobile = FALSE, $dataAccountType);
					}
					// var_dump($dataStatement); die;
					if($dataInquiry['ResponseCode'] == '00' && $dataStatement['ResponseCode'] == '00'){
						$this->view->messageResponse = '00';
					}
					elseif ($dataStatement['ResponseCode'] =='45'){
						$this->view->messageError = $this->language->_('Error : No Transaction History');
						$this->view->messageResponse = '';
					}
					else{
						$this->view->messageError = $this->language->_('Error : Cant Continue the Transaction, Please Retry in a few moment');
						$this->view->messageResponse = '';
					}

					$sessionData = new Zend_Session_Namespace('TRX_INQ');
					$sessionData->Detail 			= $dataStatement['Detail'];
					$sessionData->OpeningBalance 	= $dataStatement['OpeningBalance'];
					$sessionData->ClosingBalance 	= $dataStatement['ClosingBalance'];
					$sessionData->HoldAmount 		= $dataInquiry['HoldAmount'];

					//Zend_Debug::dump($dataInquiry,'$dataInquiry');
					//Zend_Debug::dump($dataStatement,'$dataStatement');
				}
				else
				{
					$sessionData = new Zend_Session_Namespace('TRX_INQ');

					$dataStatement['Detail'] 			= $sessionData->Detail;
					$dataStatement['OpeningBalance'] 	= $sessionData->OpeningBalance;
					$dataStatement['ClosingBalance'] 	= $sessionData->ClosingBalance;
					$dataInquiry['HoldAmount'] 			= $sessionData->HoldAmount;
				}
				//echo "<pre>"; print_r($dataStatement);	die();
				//Zend_Debug::dump($dataStatement['Detail']);
				//$dataStatement['Detail'] = array_reverse($dataStatement['Detail'], true);


				$dataDetails = array();
				$totalDebet = "";
				$totalKredit = "";

				foreach($dataStatement['Detail'] as $dt => $datadtl)
				{

					if($acct_stat_layout == '1'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}

							if( $dt == 0 )
							{
								//$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
							}
							else
							{
								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								//$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							}


							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}
					elseif($acct_stat_layout == '2'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}

							if( $dt == 0 )
							{
//								$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
							}
							else
							{
								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//								$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							}


							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}
					elseif($acct_stat_layout == '1R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}

//						$balance = $datadtl->Balance;
//						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);

						if( $dt == 0 )
						{
//							$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
							$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
						}
						else
						{
							$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//							$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
						}


						$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;

					}
					elseif($acct_stat_layout == '2R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance - $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance + $datadtl->Amount;
						}


//						$typeTransaction = $datadtl->DBCR;
//						$dataAmount = $datadtl->Amount;
//
//
//						if($typeTransaction == 'D'){
//							$debetAmount = $dataAmount;
//
//
//						}
//						elseif($typeTransaction == 'C'){
//							$creditAmount = $dataAmount;
//						}

//						$totBalance = ($dataStatement['ClosingBalance']-$totalKredit+$totalDebet);
//						$tot = $totBalance + $debetAmount - $creditAmount;

						if( $dt == 0 )
						{
							//$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
							$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
						}
						else
						{
							$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							//$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
						}


						$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;


//						$dataStatement['Detail'][$dt]->Balance = $tot;
						//$runningBalance = ($totBalance-$debetAmount)+$creditAmount;
						//$balance = $datadtl->Balance;
//						$balance = $tot;

//						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);

					}


					if ($period == 'history')
					{
//						$balanceSign = substr($datadtl->Balance, 0, 1);
//						$balance	 = substr($datadtl->Balance, 1);
//						if ($balanceSign != '+')
//			   			{	$balance = (-1) * $balance;		}

			   			$balance = $datadtl->Balance;

						//$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);
					}

				}

				$dataStatement['Detail'] = array_reverse($dataStatement['Detail'], true); //di sorting

					//Zend_Debug::dump($dataStatement['Detail'],'detail');
					//Zend_Debug::dump($dataDetails,'dt');

				if ($csv || $pdf) {
					$dataDetailsOri = $dataStatement['Detail'];
				} else {
					$this->paging($dataStatement['Detail']);
					$dataDetailsOri = $this->view->paginator;
				}

				$date_language = $this->language->_('Date');
				$description_language = $this->language->_('Description');
				$ccy_language = $this->language->_('CCY');
				$debet_language = $this->language->_('Debet');
				$credit_language = $this->language->_('Credit');
				$amount_language = $this->language->_('Amount');
				$balance_language = $this->language->_('Balance');

				//$header = array($date_language, $description_language, $ccy_language, $debet_language, $credit_language);
				if($acct_stat_layout == '1'){
					$header = array($date_language, $description_language, $debet_language, $credit_language);
				}
				elseif($acct_stat_layout == '2'){
					$header = array($date_language, $description_language, $amount_language);
				}
				elseif($acct_stat_layout == '1R'){
					$header = array($date_language, $description_language, $debet_language, $credit_language, $balance_language);
				}
				elseif($acct_stat_layout == '2R'){
					$header = array($date_language, $description_language,$ccy_language, $amount_language,'DB/CR', $balance_language);
				}
// 				print_r($acct_stat_layout);die;
				if ($period == 'history')
				{
					//$header[] = 'Balance';
				}

				$dataDetails = array();
				$totalDebet = "";
				$totalKredit = "";

				foreach($dataDetailsOri as $dt => $datadtl)
				{
					//Zend_Debug::dump($datadtl);
					$limiter = ($csv)? " ": "<br>";
					$univDesc  = (!empty($datadtl->UniversalDescription1))? $limiter.$datadtl->UniversalDescription1: "";
					$univDesc .= (!empty($datadtl->UniversalDescription2))? $limiter.$datadtl->UniversalDescription2: "";
					$univDesc .= (!empty($datadtl->UniversalDescription3))? $limiter.$datadtl->UniversalDescription3: "";
					$univDesc .= (!empty($datadtl->UniversalDescription4))? $limiter.$datadtl->UniversalDescription4: "";
					$dataDetails[$dt]['datetime'] 	 = Application_Helper_General::convertDate($datadtl->Date);

//					$dataDetails[$dt]['datetime'] 	 = Application_Helper_General::convertDate(strtotime($datadtl->Datetime), $this->_dateDisplayFormat);
					$dataDetails[$dt]['description'] = $datadtl->Description.$univDesc;
					$dataDetails[$dt]['ccy'] 		 = $ccy;

					if($acct_stat_layout == '1'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
					}
					elseif($acct_stat_layout == '2'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
					}
					elseif($acct_stat_layout == '1R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						$balance = $datadtl->Balance;
						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);

					}
					elseif($acct_stat_layout == '2R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance - $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance + $datadtl->Amount;
						}


//						$typeTransaction = $datadtl->DBCR;
//						$dataAmount = $datadtl->Amount;


//						if($typeTransaction == 'D'){
//							$debetAmount = $dataAmount;
//
//
//						}
//						elseif($typeTransaction == 'C'){
//							$creditAmount = $dataAmount;
//						}
//
//						$totBalance = ($dataStatement['ClosingBalance']-$totalKredit+$totalDebet);
//						$tot = $totBalance - $debetAmount;
//
//						//$runningBalance = ($totBalance-$debetAmount)+$creditAmount;
//						//$balance = $datadtl->Balance;
//						$balance = $tot;

						$dataDetails[$dt]['balance'] = ($csv)? $datadtl->Balance: Application_Helper_General::displayMoney($datadtl->Balance);

					}

					if ($period == 'history')
					{
//						$balanceSign = substr($datadtl->Balance, 0, 1);
//						$balance	 = substr($datadtl->Balance, 1);
//						if ($balanceSign != '+')
//			   			{	$balance = (-1) * $balance;		}

			   			$balance = $datadtl->Balance;

						//$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);
					}

				}

				$stringParam = array('ACCTSRC'		=> $zf_filter_input->ACCTSRC,
									 'periodtype'	=> $period,
									 'history_from'	=> $dateFrom,
									 'history_to'	=> $dateTo,
								    );

				//$dataDetails = array_reverse($dataDetails, true); //di sorting
				$this->view->dataDetails 	= $dataDetails;
				//Zend_Debug::dump($dataDetails);
				$this->view->accData 		= $accData;
				// var_dump($accData); die;
				//$this->view->openingBalance = $dataStatement['OpeningBalance'];

				//5.000.000 - CR(1.500.000) + DB(12.000) = 3512.000  opening balance
				//5.000.000 closing balance
				//$openingBalance 			=
				$this->view->openingBalance = $openingBalance;
				$this->view->closingBalance = $dataStatement['ClosingBalance'];

				$this->view->holdAmount 	= $dataInquiry['HoldAmount'];
				$this->view->totalDebet 	= $totalDebet == 0 ? 0.00 : $totalDebet;
				$this->view->totalKredit 	= $totalKredit == 0 ? 0.00 : $totalKredit;

				//Zend_Debug::dump($dataStatement);
				$this->view->ccy 			= $ccy;
				$this->view->header			= $header;
				$this->view->query_string_params = $stringParam;
				$this->view->pdf 			= ($pdf)? true: false;
				$this->updateQstring();

				$logDesc = 'Account: '.$zf_filter_input->ACCTSRC;
				if($csv)
				{
					$logDesc .= '. Export to CSV';

					// Set Information Data
					$n = count($dataDetails);
						  $dataDetails[$n] = array("");
					$n++; $dataDetails[$n] = array($this->language->_('Information'));
					$n++; $dataDetails[$n] = array($this->language->_('Account'), $zf_filter_input->ACCTSRC.' ('.$accData['ACCT_NAME'].' / '.$ccy.')');
					$n++; $dataDetails[$n] = array($this->language->_('Opening Balance'), $ccy.' '.Application_Helper_General::displayMoney($this->view->openingBalance));
					$n++; $dataDetails[$n] = array($this->language->_('Closing Balance'), $ccy.' '.Application_Helper_General::displayMoney($this->view->closingBalance));
					$n++; $dataDetails[$n] = array($this->language->_('Total Debit'), $ccy.' '.Application_Helper_General::displayMoney($this->view->totalDebet));
					$n++; $dataDetails[$n] = array($this->language->_('Total Credit'), $ccy.' '.Application_Helper_General::displayMoney($this->view->totalKredit));
					if ($period == 'today')
					{
					//$n++; $dataDetails[$n] = array($this->language->_('Hold Amount'), $ccy.' '.Application_Helper_General::displayMoney($this->view->holdAmount));
					}
					$n++; $dataDetails[$n] = array($this->language->_('Start Date'), $this->view->DATE_FROM_view);
					$n++; $dataDetails[$n] = array($this->language->_('End Date'), $this->view->DATE_TO_view);

					$this->_helper->download->csv($header,$dataDetails,null,'RDN Transaction Inquiry');
				}
				elseif(!empty($pdf))
				{
					$logDesc .= '. Print PDF';
					Application_Helper_General::writeLog('ACDT', $logDesc);
					$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/view.phtml')."</td></tr>";

					$this->_helper->download->pdf(null,null,null,'RDN Transaction Inquiry',$outputHTML);
				}
			  	//elseif($this->_request->getParam('print') == 1){
                    			//$data = $arr;//$this->_dbObj->fetchAll($select);
                   			 //$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Transaction Inquiry', 'data_header' => $header));
                		//}

				Application_Helper_General::writeLog('ACDT', $logDesc);
			}
			else
			{
				$this->view->error 		= true;
				$errors 				= $zf_filter_input->getMessages();

				$this->view->ACCTSRCERR		= (isset($errors['ACCTSRC']))? reset($errors['ACCTSRC']) : null;

				if ($this->view->ACCTSRCERR == null)
				{	$this->view->ACCTSRCERR = $errorMsg;	}

				$this->view->ACCTSRC 		= $account;

				$this->render( 'index' );
			}
		}


	}
}
