<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
class accountstatement_LoanaccountController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$fields = array(
						'acct'  => array('field' => 'ACCT_NO',
											   'label' => 'Deposit No',
											   'sortable' => true),
						'acct_name'  => array('field' => 'ACCT_NAME',
											'label' => 'Account Name',
											'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
												'label'    => 'CCY',
												'sortable' => true),
						'acct_name'  => array('field' => 'ACCT_NAME',
											   'label' => 'Product Type',
											   'sortable' => true)
				);
				
		$sortBy  = $this->_getParam('sortby','acct');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
			
		$custInfo = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('C' => 'M_CUSTOMER'))
						->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				);
		$this->view->custname	= $custInfo['CUST_NAME'];
		$this->view->custadd	= $custInfo['CUST_ADDRESS'];
		$this->view->custcity	= $custInfo['CUST_CITY'];
		$this->view->custprov	= $custInfo['CUST_PROVINCE'];
		$this->view->custzip	= $custInfo['CUST_ZIP'];
		$this->view->custphone	= $custInfo['CUST_PHONE'];
		$this->view->custfax	= $custInfo['CUST_FAX'];
										  
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$data = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
				
		$isGroup = false;
		if(count($data))
		{
			foreach($data as $row)
			{
				if($row['GROUP_ID'])
				{
					$isGroup = true;
				}
			}
		}
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$this->view->isGroup = $isGroup;
		$this->view->custId = $this->_custIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
		
		$logDesc = 'Viewing Loan Account Inquiry';
	if($csv)
		{
			$arr = array();
			
			$header = array(
				$this->language->_('Account No'),
				$this->language->_('Product Type'),
				$this->language->_('Plafond'),
				$this->language->_('Outstanding')
			);
			
			if(!$isGroup)
			{
				$arr[0] = $header;
				$i = 1;
				$arr_value_tot_group =  array();
				$arr_display_tot_group =  array();
				foreach($data as $row)
				{
					
					$systemBalance = new SystemBalance($row['CUST_ID'],$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
					$systemBalance->setFlag(false);
					$systemBalance->checkBalance();
					
					
					if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
			          $arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
			        }
			        else {
			          $arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
			        }
					$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);
					$accountStatusModified = $systemBalance->getCoreAccountStatusDesc();
					$model = new accountstatement_Model_Accountstatement();
					if($systemBalance->getCoreProductType() != NULL || $systemBalance->getCoreProductType() != 'N/A'){
						// 					print_r('here');
						$product = $model->getProduct($systemBalance->getCoreProductType(),$row ['PRODUCT_CODE']);
						$product_name = $product['0']['PRODUCT_NAME'];
					}else{
						// 					print_r('here1');
						$product_name = 'N/A';
					}	
					$arr[$i][] = $row['ACCT_NO'];
// 					$arr[$i][] = $row['ACCT_ALIAS_NAME'];
					$arr[$i][] = $product_name;
					$arr[$i][] = '';
// 					$arr[$i][] = $row ['ACCT_DESC'];
// 					$arr[$i][] = $accountStatusModified;
					$arr[$i][] = $systemBalance->getEffectiveBalance();
					
					$i++;
				}
				$display_tot_group = implode("; ",$arr_display_tot_group);
// 				$arr[$i][] = $this->language->_('TOTAL EFFECTIVE BALANCE').' : '.$display_tot_group;	
			}
			else
			{
				$i = 0;
				$group = null;
				
				$resultdataGroup =  array();
				foreach ($data as $rowGroup){
					$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup; 
				}
				$i = 0;
				$group = null;
				$arr_value_tot_group = array();
				foreach ( $resultdataGroup as $groupname=>$rowPerGroup ) 
				{
					$i++;
					$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
					$groupname = ($groupname) ? $groupname : 'OTHERS';
					$arr[$i][] = $groupname;
					$i++;
					$arr[$i][] = $header;
					
						if(count($rowPerGroup) > 0){
							$arr_value_tot_group =  array();
							$arr_display_tot_group =  array();
							foreach ($rowPerGroup as $row){
							$i++;	
							$systemBalance = new SystemBalance($this->_custIdLogin,$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
							$systemBalance->setFlag(false);
							$systemBalance->checkBalance();
							$model = new accountstatement_Model_Accountstatement();
								if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
									$arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
								}else{
									$arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
								}
								$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);
							$accountStatusModified = $systemBalance->getCoreAccountStatusDesc();
// 							print_r($row);die;
							if($systemBalance->getCoreProductType() != NULL || $systemBalance->getCoreProductType() != 'N/A'){
								// 					print_r('here');
								$product = $model->getProduct($systemBalance->getCoreProductType(),$row ['PRODUCT_CODE']);
								$product_name = $product['0']['PRODUCT_NAME'];
							}else{
								// 					print_r('here1');
								$product_name = 'N/A';
							}
							if($product_name==''){
								$product_name = 'N/A';
							}
							$arr[$i][] = $row['ACCT_NO'];
// 							$arr[$i][] = $row['ACCT_ALIAS_NAME'];
							$arr[$i][] = $product_name;
							
							$arr[$i][] = '';
// 							$arr[$i][] = $product_name;
// 							$arr[$i][] = $row ['ACCT_DESC'];
// 							$arr[$i][] = $accountStatusModified;
							$arr[$i][] = $systemBalance->getEffectiveBalance();
							
							}
						}
						$i++;	
						$display_tot_group = implode("; ",$arr_display_tot_group);	
// 						$arr[$i][] = $this->language->_('TOTAL EFFECTIVE BALANCE').' : '.$display_tot_group;
						
						
					
				} 
										
			}
				$logDesc = 'Export to CSV';
				$this->_helper->download->csv(array(),$arr,null,'Loan Account Detail');  
		
		}
		
		if($pdf){
			//Zend_Debug::dump($this->view->render($this->view->controllername.'/pdf.phtml'));die;
			Application_Helper_General::writeLog('BAIQ','Print PDF');
			$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf.phtml')."</td></tr>";
			//Zend_Debug::dump($outputHTML);die;
			$this->_helper->download->pdf(null,null,null,'Loan Account Detail',$outputHTML);
		}
		
		if($this->_request->getParam('print') == 1){
			$arr = $data;
// 			$fields = array(
// 						'ACCT_NO'  => array('field' => 'ACCT_NO',
// 											   'label' => $this->language->_('Account Number'),
// 											   'sortable' => true),
// 						'ACCT_NAME'  => array('field' => 'ACCT_NAME',
// 											   'label' => $this->language->_('Account Name'),
// 											   'sortable' => true),
// 						'CCY_ID'   => array('field'    => 'CCY_ID',
// 											  'label'    => $this->language->_('CCY'),
// 											  'sortable' => true),
			
// 						'ACCT_DESC'   => array('field'    => 'ACCT_DESC',
// 											  'label'    => $this->language->_('Type'),
// 											  'sortable' => true),
			
// 						'FREEZE_STATUS'   => array('field'    => 'FREEZE_STATUS',
// 											  'label'    => $this->language->_('Status'),
// 											  'sortable' => true),
// 						'BALANCE'   => array('field'    => 'BALANCE',
// 											  'label'    => $this->language->_('Available Balance'),
// 											  'sortable' => true)
// 			);
			
			$fields = array(
					'acct'  => array('field' => 'ACCT_NO',
							'label' => 'Deposit No',
							'sortable' => true),
					'acct_name'  => array('field' => 'ACCT_NAME',
							'label' => 'Account Name',
							'sortable' => true),
					'ccy'   => array('field'    => 'CCY_ID',
							'label'    => 'CCY',
							'sortable' => true),
					'acct_name'  => array('field' => 'ACCT_NAME',
							'label' => 'Product Type',
							'sortable' => true)
			);
// 			print_r($arr);die;
//            $data = $arr;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Loan Account Inquiry', 'data_header' => $fields));
        }
		
		Application_Helper_General::writeLog('BAIQ',$logDesc);
	}
	
	public function detailAction(){
		$acct_no = $this->_getParam('acct_no');
		$ccy_id = $this->_getParam('ccy_id');
		$prod = $this->_getParam('prod');
		
		$Account = new Account($acct_no,Application_Helper_General::getCurrNum($ccy_id));
		$Account->setFlag(false);
		$Account->checkBalanceLoan();
		
		// 		if($Account->getCoreProductPlan() != NULL){
		// 			// 					print_r('here');
		// 			$model = new accountstatement_Model_Accountstatement();
		// 			$product = $model->getProduct($Account->getCoreProductPlan(),$prod);
		// 			$product_name = $product['0']['PRODUCT_NAME'];
		// 		}else{
		// 			// 					print_r('here1');
		// 			$product_name = 'N/A';
		// 		}
		
		$this->view->product = $prod;
		$this->view->acct_no = $acct_no;
		$this->view->ccy_id = $ccy_id;
		$this->view->detail = $Account->getCoreAccountDetail();
		
		
	}
	
	
}
