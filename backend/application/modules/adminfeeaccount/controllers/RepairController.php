<?php


require_once 'Zend/Controller/Action.php';


class adminfeeaccount_RepairController extends Application_Main
{
	public function indexAction() 
	{	
  		$changeid = $this->_getParam('changes_id');
		
		$select2 = $this->_db->select()->distinct()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
		$select2 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$result2 = $this->_db->fetchRow($select2);

		$custid = $result2['KEY_FIELD'];
		
		$selectcust = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$selectcust -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $selectcust->query()->FetchAll();
		$this->view->result = $result;
		
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		//$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;
		
		$monthlyfeestatus = $result[0]["CUST_MONTHLYFEE_STATUS"];
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = $this->language->_('Disabled');
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = $this->language->_('Enabled');
		}
		
		foreach($resultunion as $row)
		{	
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'TEMP_ADMFEE_MONTHLY'),array('*'));
			$select4 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($row['ACCT_NO']));
			//$select4 -> where("MONTHLYFEE_TYPE = '1'");
			//echo $select4;
			$result4 = $select4->query()->FetchAll();
			//Zend_Debug::dump($result4);die;
			$i = 0;
			if($result4)
			{
				foreach($result4 as $row2)
				{	
					$acctno = $row2['ACCT_NO'];
   					$idamt = $row2['ACCT_NO'].'amount';
					$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
					$cekacct = 'cek'.$acctno; 
					$this->view->$idamt = $amt;
					//var_dump($this->view->$idamt);
					//var_dump($row2);
					$this->view->$cekacct = $row2['CHARGES_ACCT_NO'];
					$i++;
				}
			}
		}	
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
			if($submit)
			{
				$error = 0;
				Application_Helper_General::writeLog('CHCR','Repairing Administration fee account charges ('.$changeid.')');
				foreach($resultunion as $row)
				{
					$acctno = $row['ACCT_NO'];
					$cekacct = 'cek'.$acctno;
					$idamt = $row['ACCT_NO'].'amount';
					
   					//Zend_Debug::dump($idamt); die;
					$cekamt = $this->_getParam($idamt);
					$amt = Application_Helper_General::convertDisplayMoney($cekamt);
					$temp_amt = str_replace('.','',$amt);
					$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
					$this->view->$idamt = $cekamt;
					
					$cekcekacct = $this->_getParam($acctno);
   					$cek_angka2 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   					$this->view->$cekacct = $cekcekacct;
   					
   					
   					$selectcekcur = $this->_db->select()
				        			->from (($selectprk),array('*'));
					$selectcekcur -> where("ACCT_NO LIKE ".$this->_db->quote($cekcekacct));
					$cekcur = $this->_db->fetchRow($selectcekcur);			    
					//Zend_Debug::dump($cekcekacct);
					
					if($cekcekacct)
					{
							if($cekcur['CCY_ID'] != $row['CCY_ID'])
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account currency must be same');
								$this->view->$errid = $err;
								$error++;
							}
					}
					
					if($cek_angka == false && $cek_angka != null)
					{
							$errid = 'err'.$idamt;
							$err = $this->language->_('Amount value has to be numeric');
							$this->view->$errid = $err;
							$error++;
							//die;
					}
					
					if($cekamt != '0.00')
					{
						if(!$cekcekacct)
						{
							$errid = 'err'.$cekacct;
							$err = $this->language->_('Account should be choosen while the amount is filled');
							$this->view->$errid = $err;
							$error++;
							//die;
						}
						if($cekcekacct)
						{
							if($cek_angka2 == false)
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account value has to be numeric');
								$this->view->$errid = $err;
								$error++;
							}
						}
					}	
				}
				//Zend_Debug::dump($error); die;
					
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
					$info = $this->language->_('Charges');
					$info2 = $this->language->_('Set Charges Administration fee per Account');
					$this->updateGlobalChanges($changeid,$info);
				
					$where = array('CHANGES_ID = ?' => $changeid);
					$this->_db->delete('TEMP_ADMFEE_MONTHLY',$where);
					
					foreach($resultunion as $row)
					{
						$cekacct = $row['ACCT_NO'];
						$idamt = $row['ACCT_NO'].'amount';
						
						$cekamt = $this->_getParam($idamt);
						if($cekamt == null)
						{
							$cekamt = '0.00';
						}
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
						$cekcekacct = $this->_getParam($cekacct);
						
						$data= array(
										'CHANGES_ID' 		=> $changeid,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'AMOUNT' 			=> $amt,
										'MONTHLYFEE_TYPE' 	=> '1',
										'CHARGES_ACCT_NO'	=> $cekcekacct
									);
						$this->_db->insert('TEMP_ADMFEE_MONTHLY',$data);										
						//echo 'aaa';die;
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->_redirect('/popup/successpopup/index');
					}
				}
				
				catch(Exception $e)
				{
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}		
		}
		else
		{
			Application_Helper_General::writeLog('CHCR','View Repair Administration fee account charges page ('.$changeid.')');
		}
	}
}