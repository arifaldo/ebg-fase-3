<?php


require_once 'Zend/Controller/Action.php';


class setcompanycharges_ChargessupplychainController extends Application_Main
{
	public function indexAction() 
	{
		$custid = $this->_getParam('custid');
		
		$select = $this->_db->select()
			->from(array('M_CUSTOMER'));
		$select -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $this->_db->fetchRow($select);
		$this->view->result = $result;

		$chargestatus = $result["CUST_CHARGES_STATUS"];

		if($chargestatus == 0)
		{
			$this->view->chargestatus = 'Disabled';
		}
		if($chargestatus == 1)
		{
			$this->view->chargestatus = 'Enabled';
		}
		
		$select2 = $this->_db->select()
							->from('T_GLOBAL_CHANGES');
		$select2 -> where("KEY_FIELD LIKE ".$this->_db->quote($custid));
		$select2 -> where("KEY_VALUE LIKE ".$this->_db->quote($result['CUST_NAME']));
		$select2 -> where("DISPLAY_TABLENAME = 'Supply Chain Charges Setting'");
		$select2 -> where("CHANGES_STATUS = 'WA'");
		$cek = $select2->query()->FetchAll();
		//Zend_Debug::dump($cek); die;	
		if($cek)
		{
			$docErr = "*No changes allowed for this record while awaiting approval for previous change";	
			$this->view->error = $docErr;
			$this->view->changestatus = "disabled";
		}
		Application_Helper_General::writeLog('CHLS','View Company charges detail (choose supply chain charges) ('.$custid.')');
	}
}
