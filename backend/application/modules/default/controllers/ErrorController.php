<?php

class ErrorController extends Default_Model_Login
{

    public function errorAction()
    {
        $this->_helper->layout()->disableLayout();
        $errors = $this->_getParam('error_handler');

        echo '<pre>';
        var_dump($errors);
        echo '</pre>';

        $select = $this->_db->select()
            ->from(array('M_SETTING'), array('*'))
            ->query()->fetchAll();
        $setting = Application_Helper_Array::listArray($select, 'SETTING_ID', 'SETTING_VALUE');
        // echo '<pre>';print_r($setting);
        $this->view->masterTitle = $setting['master_bank_app_name'];
        $this->view->masterEmail = $setting['master_bank_email'];
        $this->view->masterBankTelp   = $setting['master_bank_telp'];

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Application error';
                break;
        }

        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->crit($this->view->message, $errors->exception);
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request   = $errors->request;
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasPluginResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }
}
