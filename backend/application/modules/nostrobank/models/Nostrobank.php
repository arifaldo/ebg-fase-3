<?php
Class nostrobank_Model_Nostrobank {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getCCYList()
    {
		$select = $this->_db->select()
					        ->from(array('A' => 'M_MINAMT_CCY'),array('CCY_ID'));
		
       	return $this->_db->fetchall($select);
    }

    public function getNostroNames()
    {
		$select = $this->_db->select()
					        ->from(array('A' => 'M_NOSTRO_BANK'),array('NOSTRO_NAME'));
		
       	return $this->_db->fetchall($select);
    }

    public function checkNostroBank($ccy,$nostroname)
    {
		$select = $this->_db->select()
					        ->from(array('A' => 'M_NOSTRO_BANK'),array('totalrow' => 'count(*)'))
					        ->where('CCY = ?', $ccy)
					        ->where('NOSTRO_NAME = ?', $nostroname);
		
       	$res = $this->_db->fetchRow($select);
       	$row = $res['totalrow'];

       	if($row > 0)
       		return false;
       	else
       		return true;
    }

    public function checkNostroBankEdit($ccy,$nostroname,$nostroid)
    {
    $select = $this->_db->select()
                  ->from(array('A' => 'M_NOSTRO_BANK'),array('totalrow' => 'count(*)'))
                  ->where('CCY = ?', $ccy)
                  ->where('NOSTRO_NAME = ?', $nostroname)
                  ->where('NOSTRO_ID != ?', $nostroid);
    
        $res = $this->_db->fetchRow($select);
        $row = $res['totalrow'];

        if($row > 0)
          return false;
        else
          return true;
    }
  
   
}