<?php
require_once 'Zend/Controller/Action.php';

class fprivilagetemplate_EditController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ftemplateid = $this->_getParam('ftempid');

		if (!$this->view->hasPrivilege("PTUD")) {
			return $this->_redirect("/fprivilagetemplate");
		}

		$modelPrivil = new fprivilagetemplate_Model_Fprivilagetemplate();
		$privil = $modelPrivil->getAllPrivilege();

		$templatedesc = $modelPrivil->getTemplate($ftemplateid);
		$fpriviltem = $modelPrivil->getFpriviltem($ftemplateid);

		$saveprivi = [];
		foreach ($fpriviltem as $value) {
			array_push($saveprivi, $value["FPRIVI_ID"]);
		}

		$this->view->saveprivi = json_encode($saveprivi);

		$module = $modelPrivil->getModuleDescArr();

		$arrType = array("" => "---" . $this->language->_('Please Select') . "---", 'F' => "Frontend", 'B' => "Backend");
		$arrSecure = array("" => "---" . $this->language->_('Please Select') . "---", 0 => "No", 1 => "Yes");

		$this->view->isbybo = $arrType;
		$this->view->secure = $arrSecure;

		$this->view->sortingGroup = $module;

		foreach ($privil as $row) {
			$moduleID = $module[$row['FPRIVI_MODULEID']];
			unset($row['FPRIVI_MODULEID']);
			$privilGroup[$moduleID][] = $row;
		}
		ksort($privilGroup);
		// $privilGroup['No Group'] = $privilGroup[''];
		unset($privilGroup['']);

		$this->view->tmplcode = $ftemplateid;
		$this->view->tmplname = $templatedesc['FTEMPLATE_DESC'];
		$this->view->tmplgroup = $templatedesc['FTEMPLATE_GROUP'];
		$this->view->tmplsecurity = $templatedesc['FTEMPLATE_SECURE'];

		$this->view->priv = $fpriviltem;
		// $this->$row['FPRIVI_ID']
		// print_r($fpriviltem);die;
		foreach ($fpriviltem as $row) {
			// print_r($row['FPRIVI_ID']);
			$this->view->$row['FPRIVI_ID'] = 1;
		}
		// die;
		$filterArr = array(
			'tmplcode'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'tmplname'  => array('StripTags', 'StringTrim'),
		);
		$validators = array(
			'tmplcode' => array(
				'NotEmpty',
				'Alnum',
				array('StringLength', array('min' => 3, 'max' => 3)),
				array('Db_NoRecordExists', array('table' => 'M_FTEMPLATE', 'field' => 'FTEMPLATE_ID')),
				'messages' => array(
					$this->language->_('Can not be empty, Template Code length must not be more than 3'),
					$this->language->_('Must be alphabet or numeric values'),
					$this->language->_('Template Code length must be 3'),
					$this->language->_('Template Code already in use. Please use another'),
				)
			),
			'tmplname' => array(
				'NotEmpty',
				//'Alnum',
				array('StringLength', array('min' => 1, 'max' => 128)),
				//array('Db_NoRecordExists', array('table' => 'M_FTEMPLATE', 'field' => 'FTEMPLATE_DESC')),
				'messages' => array(
					$this->language->_('Can not be empty'),
					// $this->language->_('Must be alphabet or numeric values'),
					$this->language->_('Template Code length must not be more than 128'),
					//$this->language->_('Template Code already in use. Please use another'),
				)
			)
		);

		$zf_filters = new Zend_Filter_Input($filterArr, $validators, $this->_request->getParams());

		$re = '%^[a-zA-Z0-9+\- ]*$%';
		$cekspecialchar = preg_match($re, $zf_filters->tmplname) ? 1 : 0;
		if ($this->_request->isPost()) {
			if ($zf_filters->isValid() && $cekspecialchar) {
				$cekcheckbox = 0;
				$this->_db->beginTransaction();
				try {
					//DELETE DATA BEFORE EDIT
					$where = array('FTEMPLATE_ID = ?' => $ftemplateid);
					$this->_db->delete('M_FPRIVILEGE_TEMPLATE', $where);

					foreach ($privil as $row) {
						if ($this->_getParam($row['FPRIVI_ID']) == 1) {
							$cekcheckbox++;
							$data = array(
								'FPRIVI_ID' => $row['FPRIVI_ID'],
								'FPRIVI_DESC' => $row['FPRIVI_DESC'],
								'FTEMPLATE_ID' => $ftemplateid
							);

							$this->_db->insert('M_FPRIVILEGE_TEMPLATE', $data);
						}
						$this->view->$row['FPRIVI_ID'] = $this->_getParam($row['FPRIVI_ID']);
					}

					if ($cekcheckbox == 0) {
						$this->_db->rollBack();
						$this->view->errcheckbox = 'Please Select at least 1 Privilege';
					} else {
						//DELETE DATA BEFORE EDIT

						// $this->_db->delete('M_FTEMPLATE',$where);
						// 
						$updData = array(
							'FTEMPLATE_DESC' => $zf_filters->tmplname
						);
						// $this->_db->insert('M_FTEMPLATE',$data);

						// $updData = array('USER_RPWD_ISEMAILED'=> 1);
						$where =  array();
						// $where['CUST_ID = ?'] 	= $cust_id;
						$where['FTEMPLATE_ID = ?'] 	= $ftemplateid;
						// $where = array('FTEMPLATE_ID = ?' => $ftemplateid);
						$this->_db->update('M_FTEMPLATE', $updData, $where);

						$this->_db->commit();
						Application_Helper_General::writeLog('PTUD', 'Edit Frontend Privilege Template. Template Code :[' . $ftemplateid . ']');
						$this->view->tmplname = $zf_filters->tmplname;
						$this->setbackURL('/fprivilagetemplate/detail/index/ftemplateid/' . $ftemplateid);
						$this->_redirect('/notification/success/index');
					}
				} catch (Exception $e) {
					$this->_db->rollBack();
				}
			} else {
				if (!$cekspecialchar) {
					$this->view->xtmplname = 'Must not contain special characters';
					$zf_filters->tmplname = '';
				}
				foreach ($zf_filters->getMessages() as $key => $err) {
					$xxx = 'x' . $key;
					$this->view->$xxx = $this->displayError($err);
				}
			}
		}
		$this->view->privilGroup = $privilGroup;
		$this->view->modulename = $this->_request->getModuleName();
	}
}
