<?php
require_once 'Zend/Controller/Action.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Helper/Encryption.php';

class manualupdate_suggestiondetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newpopup');
		$this->view->userIdLogin = $this->_userIdLogin;

		// LOAD AESMYSQL
		$AESMYSQL = new Crypt_AESMYSQL();

		// LOAD MODEL
		try {
			//code...
			$model = new manualupdate_Model_Suggestiondetail();
		} catch (\Throwable $th) {
			//throw $th;
			Zend_Debug::dump($th);
			die();
		}

		$params = $this->_request->getParams();
		$changesId = $params['changes_id'];

		// GET DETAIL GLOBAL CHANGES
		$detailGlobalChanges = $model->getDetailGlobalChanges($changesId);

		// GET DETAIL REQUEST
		$detailRequest = $model->getDetailRequest($changesId);
		$detailRequest['NAMA_FILE'] = str_replace([explode('_', $detailRequest['UPDATE_MEMO_FILE'])[0], explode('_', $detailRequest['UPDATE_MEMO_FILE'])[1], explode('_', $detailRequest['UPDATE_MEMO_FILE'])[2], explode('_', $detailRequest['UPDATE_MEMO_FILE'])[3]], '', $detailRequest['UPDATE_MEMO_FILE']);
		$detailRequest['NAMA_FILE'] = trim($detailRequest['NAMA_FILE'], '_');

		// GET DETAIL TRANSACTION
		$detailTrx = $model->getDetailTransaction($detailRequest['TRANSACTION_ID']);

		// GET DETAIL BG
		$explodeDetailBg = explode('~|~', $detailTrx['DETAIL_BG']);
		$detailBg = $this->parseDetailBg($explodeDetailBg);

		// GET SWIFTCODE
		$swiftCode = $model->getSwifCode($detailTrx['CLOSE_REF_NUMBER']);

		// GET DETAIL BANK
		$detailBank = $model->getDetailBank($swiftCode ?: 'BTANIDJA');

		// GET DETAIL CHARGE ACCT
		if ($detailTrx['CHARGE_ACCT']) {
			$detailChargeAcct = $this->getDetailInfoAcct($detailTrx['CHARGE_ACCT']);
		}

		// ENCRYPT TRANSACTION ID
		$detailTrx['ENCRYPT_TRX_ID'] = urlencode($AESMYSQL->encrypt($detailTrx['TRANSACTION_ID'], uniqid()));

		// ENCRYPT CHANGES ID
		$detailRequest['ENCRYPT_CHANGES_ID'] = urlencode($AESMYSQL->encrypt($detailRequest['CHANGES_ID'], uniqid()));

		// LIST OF SERVICE
		$arrListService = [
			1 => 'Transfer Inhouse',
			2 => 'Unlock Deposito',
			3 => 'Unlock Saving',
			4 => 'Open Account',
			5 => 'Deposito Disbursement',
			6 => 'Loan Disbursement',
			7 => 'SKN',
			8 => 'RTGS',
			9 => 'Manual Settlement'
		];

		// MASKING BG NUMBER
		$maskingBgNumber = implode('-', [substr($detailBg['BG_NUMBER'], 0, 3), substr($detailBg['BG_NUMBER'], 3, 6), substr($detailBg['BG_NUMBER'], 9, 5), substr($detailBg['BG_NUMBER'], 14, 4)]);

		// SEND VARIABLE TO VIEW
		$this->view->changesId = $changesId;
		$this->view->detailGlobalChanges = $detailGlobalChanges;
		$this->view->detailTrx = $detailTrx;
		$this->view->detailRequest = $detailRequest;
		$this->view->detailBg = $detailBg;
		$this->view->maskingBgNumber = $maskingBgNumber;
		$this->view->arrListService = $arrListService;
		$this->view->detailBank = $detailBank;

		if ($detailTrx['CHARGE_ACCT']) {
			$this->view->detailChargeAcct = $detailChargeAcct;
		}

		// WRITE LOG
		$privilege = 'VMST';
		$logAction = 'Lihat Detail Permintaan Pembaharuan Status, Transaction Id = ' . $detailTrx['TRANSACTION_ID'];
		Application_Helper_General::writeLog($privilege, $logAction);
	}

	private function parseDetailBg($dataBg)
	{
		$saveDetailBg = [];
		foreach ($dataBg as $key => $value) {
			if ($key % 2 == 1) continue;

			$saveDetailBg[$value] = $dataBg[$key + 1];
		}

		return $saveDetailBg;
	}

	private function getDetailInfoAcct($acctnumber)
	{
		$callService = new Service_Account($acctnumber, '');
		$resultAcctInfo = $callService->inquiryAccontInfo();

		$getCif = $resultAcctInfo['cif'];

		$callServiceCif = new Service_Account('', '', '', '', '', $getCif);
		$resultCifAccount = $callServiceCif->inquiryCIFAccount();

		$filterAcct = array_shift(array_filter($resultCifAccount['accounts'], function ($item) use ($acctnumber) {
			return ltrim($item['account_number'], '0') == ltrim($acctnumber, '0');
		}));

		return array_merge($resultAcctInfo, $filterAcct);
	}

	public function memofileAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$AESMYSQL = new Crypt_AESMYSQL();
		$trxId = $this->_request->getParam('trxid');

		$trxId = $AESMYSQL->decrypt(urldecode($trxId), uniqid());

		$detailTrx = $this->_db->select()
			->from('TEMP_TRANSACTION_STATUS_MANUAL_UPDATE')
			->where('TRANSACTION_ID = ?', $trxId)
			->order('UPDATE_SUGGESTED DESC')
			->query()->fetch();

		$explodeNamaFile = explode('_', $detailTrx['UPDATE_MEMO_FILE']);
		$namaFile = str_replace([$explodeNamaFile[0], $explodeNamaFile[1], $explodeNamaFile[2], $explodeNamaFile[3]], '', $detailTrx['UPDATE_MEMO_FILE']);
		$namaFile = trim($namaFile, '_');

		$attachmentDestination   = UPLOAD_PATH . '/document/memopembaharuan/';
		if (file_exists($attachmentDestination . $detailTrx['UPDATE_MEMO_FILE']) && $detailTrx['UPDATE_MEMO_FILE']) {
			return $this->_helper->download->file(implode('_', [$detailTrx['TRANSACTION_ID'], 'MEMO', $namaFile]), $attachmentDestination . $detailTrx['UPDATE_MEMO_FILE']);
		} else {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode([
				'result' => 'file tidak ditemukan'
			]);
		}
	}

	public function proccessrequestAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// LOAD MODEL
		try {
			//code...
			$model = new manualupdate_Model_Suggestiondetail();
		} catch (\Throwable $th) {
			//throw $th;
			Zend_Debug::dump($th);
			die();
		}

		try {
			//code...
			// LOAD AESMYSQL
			$AESMYSQL = new Crypt_AESMYSQL();

			// GET PARAMS
			$params = $this->_request->getParams();

			// TRANSACTION ID
			$trxId = $params['trxid'];
			$trxId = $AESMYSQL->decrypt(urldecode($trxId), uniqid());

			// CHANGES ID
			$changesId = $params['changesid'];
			$changesId = $AESMYSQL->decrypt(urldecode($changesId), uniqid());

			// ACTION PROCCES
			$action = strtolower($params['actioncode']);

			// HEADER JSON
			header('Content-Type: application/json; charset=utf-8');
			if (!$trxId || !$changesId || !$action) {
				http_response_code(400);
				echo json_encode([
					'result' => false,
					'message' => $this->language->_('Transaction id, changes id, dan action tidak boleh kosong')
				]);
				return false;
			}

			// GET DETAIL REQUEST
			$detailRequest = $model->getDetailRequest($changesId);

			// GET DETAIL TRANSACTION
			$detailTrx = $model->getDetailTransaction($detailRequest['TRANSACTION_ID']);

			// GET DETAIL BG
			$explodeDetailBg = explode('~|~', $detailTrx['DETAIL_BG']);
			$detailBg = $this->parseDetailBg($explodeDetailBg);

			// APPROVE PROCESS
			if ($action == 'ap') {
				// CHECK PRIVILEGE
				if (!$this->view->hasPrivilege('AMST')) {
					http_response_code(401);
					echo json_encode([
						'result' => false,
						'message' => $this->language->_('Unauthorized')
					]);
					return false;
				}

				// UPDATE APPROVE
				if ($model->updateApprove($changesId, $detailRequest, $this->_userIdLogin)) {
					echo json_encode([
						'result' => true,
						'message' => $this->language->_('Sukses')
					]);

					$checkAllTrxIsDone = $model->checkIfAllTrxDone($detailTrx['CLOSE_REF_NUMBER']);

					if ($checkAllTrxIsDone) $model->updateStatusRelease($detailTrx['CLOSE_REF_NUMBER']);

					$bgdata = $this->_db->select()
						->from(array('A' => 'T_BANK_GUARANTEE'), array(
							'*', 'CLOSE_REF_NUMBER' => 'A.CLOSE_REF_NUMBER',
							'INS_NAME' => new Zend_Db_Expr('(SELECT CI.CUST_NAME FROM M_CUSTOMER CI WHERE CI.CUST_ID = A.BG_INSURANCE_CODE LIMIT 1)'),
							'INS_BRANCH_NAME' => new Zend_Db_Expr('(SELECT INS_BRANCH_NAME FROM M_INS_BRANCH WHERE INS_BRANCH_CODE = (SELECT PS_FIELDVALUE FROM T_BANK_GUARANTEE_DETAIL tbgd WHERE tbgd.BG_REG_NUMBER = A.BG_REG_NUMBER AND LOWER(tbgd.PS_FIELDNAME) = \'insurance branch\' LIMIT 1) LIMIT 1)'),
							'CUST_EMAIL_SPESIAL' => 'A.CUST_EMAIL',
							'BG_REG_NUMBER_SPESIAL' => 'A.BG_REG_NUMBER'

						))
						->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
						->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME', 'C.BRANCH_EMAIL'))
						->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
						->joinleft(array('F' => 'T_BG_PSLIP'), 'F.BG_NUMBER = A.BG_NUMBER', array('F.PS_STATUS', 'F.TYPE', 'PSLIP_DONE' => 'F.PS_DONE', 'PSLIP_CHANGE_TYPE' => 'F.TYPE'))
						//->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
						->where('A.BG_NUMBER = ?', $detailTrx['BG_NUMBER'])
						->query()->fetchAll();

					$data = [];
					$data['databg'] = $bgdata[0];

					$dataPslip = array(
						'PS_STATUS' => 6,
					);

					$where['CLOSE_REF_NUMBER = ?'] = $data['databg']['CLOSE_REF_NUMBER'];
					$this->_db->update('T_BG_PSLIP', $dataPslip, $where);
					
					Application_Helper_Email::runTransaction($data['databg']['CLOSE_REF_NUMBER'], '6');

					if ($checkAllTrxIsDone) {

						if ($data['databg']['PSLIP_CHANGE_TYPE'] != 3) {
							// SEND EMAIL TO PRINCIPAL
							$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_2A', 0);
							if ($data['databg']['CUST_EMAIL_SPESIAL']) $this->sendEmail($data['databg']['CUST_EMAIL_SPESIAL'], $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);

							// SEND EMAIL TO KANTOR CABANG PENERBIT
							$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_2C', 0);
							if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);

							// SEND EMAIL TO GROUP CASH COL / NON CASH COL
							$settings = new Settings();

							if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 1) {
								// FULL COVER
								$getGroupFC = $settings->getSettingNew('email_group_cashcoll');
								$getGroupFC = explode(',', $getGroupFC);
								$getGroupFC = array_map('trim', $getGroupFC);
								if ($getGroupFC) {
									foreach ($getGroupFC as $emailGroup) {
										$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_2C', 0);
										if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
									}
								}
							}

							if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 2) {
								// LINE FACILITY
								$getGroupFC = $settings->getSettingNew('email_group_linefacility');
								$getGroupFC = explode(',', $getGroupFC);
								$getGroupFC = array_map('trim', $getGroupFC);
								if ($getGroupFC) {
									foreach ($getGroupFC as $emailGroup) {
										$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_2C', 0);
										if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
									}
								}
							}

							if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 3) {
								// LINE FACILITY
								$getGroupFC = $settings->getSettingNew('email_group_insurance');
								$getGroupFC = explode(',', $getGroupFC);
								$getGroupFC = array_map('trim', $getGroupFC);
								if ($getGroupFC) {
									foreach ($getGroupFC as $emailGroup) {
										$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_2C', 0);
										if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
									}
								}
							}

							// SEND EMAIL TO KANTOR CABANG PENERBIT
							$getInsBranch = $this->_db->select()
								->from('T_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE'])
								->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
								->where('LOWER(PS_FIELDNAME) = ?', 'insurance branch')
								->query()->fetch();

							$getInsBranchEmail = $this->_db->select()
								->from('M_INS_BRANCH')
								->where('INS_BRANCH_CODE = ?', $getInsBranch['PS_FIELDVALUE'])
								->query()->fetch();

							$bgdatasplit = $this->_db->select()
								->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'))
								->where('A.BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER_SPESIAL'])
								->query()->fetchAll();

							$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_2B', 0);
							if ($getInsBranchEmail['INS_BRANCH_EMAIL'] && $bgdatasplit) $this->sendEmail($getInsBranchEmail['INS_BRANCH_EMAIL'], $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
						}

						$data['historydata'] = 'NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'];

						// $getIndexChargeAcctNumber = $this->_db->select()
						// 	->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
						// 	->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
						// 	->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
						// 	->query()->fetch();

						// $getAcctChargeDetail = $this->_db->select()
						// 	->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
						// 	->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
						// 	->where('ACCT_INDEX = ?', $getIndexChargeAcctNumber['ACCT_INDEX'])
						// 	->query()->fetchAll();

						// $nominal = number_format($getAcctChargeDetail["Transaction Amount"], 2);
						$nominal = 0;
					}

					$checkBenef = $this->_db->select()
						->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
						->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
						->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
						->where('ACCT_INDEX = ?', 1)
						->query()->fetch();

					$checkTrx = $this->_db->select()
						->from('T_BG_TRANSACTION')
						->where('TRANSACTION_ID = ?', $detailRequest['TRANSACTION_ID'])
						->query()->fetch();

					if ($checkBenef['PS_FIELDVALUE'] == $checkTrx['BENEF_ACCT'] && $checkBenef['PS_FIELDVALUE']) {
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3B', 0);
						if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

						// SEND EMAIL TO PRINCIPAL
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3A', $nominal);
						if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL_SPESIAL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

						// SEND EMAIL TO OBLIGEE
						// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3B', $nominal);
						// if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

						// SEND EMAIL TO KANTOR CABANG PENERBIT
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', $nominal);
						if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

						// SEND EMAIL TO PRINCIPAL
						// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_prinsipal', $nominal);
						// if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);

						// // SEND EMAIL TO OBLIGEE
						// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_obligee', $nominal);
						// if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);

						// // SEND EMAIL TO KANTOR CABANG PENERBIT
						// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_kcp', $nominal);
						// if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);

						if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 1) {
							// FULL COVER
							$settings = new Settings();
							$getGroupFC = $settings->getSettingNew('email_group_cashcoll');
							$getGroupFC = explode(',', $getGroupFC);
							$getGroupFC = array_map('trim', $getGroupFC);
							if ($getGroupFC) {
								foreach ($getGroupFC as $emailGroup) {
									$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
									if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
								}
							}
						}

						if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 2) {
							// GROUP LINEFACILITY
							$settings = new Settings();
							$getGroupFC = $settings->getSettingNew('email_group_linefacility');
							$getGroupFC = explode(',', $getGroupFC);
							$getGroupFC = array_map('trim', $getGroupFC);
							if ($getGroupFC) {
								foreach ($getGroupFC as $emailGroup) {
									$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
									if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
								}
							}
						}

						$getMd = $this->_db->select()
							->from('T_BANK_GUARANTEE_SPLIT')
							->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
							->query()->fetchAll();

						// SEND EMAIL TO KANTOR CABANG PENERBIT
						if ($data['databg']['COUNTER_WARRANTY_TYPE'] == '3') {

							// GROUP ASURANSI
							$settings = new Settings();
							$getGroupFC = $settings->getSettingNew('email_group_insurance');
							$getGroupFC = explode(',', $getGroupFC);
							$getGroupFC = array_map('trim', $getGroupFC);
							if ($getGroupFC) {
								foreach ($getGroupFC as $emailGroup) {
									$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
									if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
								}
							}

							$getInsBranch = $this->_db->select()
								->from('T_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE'])
								->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
								->where('LOWER(PS_FIELDNAME) = ?', 'insurance branch')
								->query()->fetch();

							$getInsBranchEmail = $this->_db->select()
								->from('M_INS_BRANCH')
								->where('INS_BRANCH_CODE = ?', $getInsBranch['PS_FIELDVALUE'])
								->query()->fetch();

							$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3D', $nominal);
							if ($getInsBranchEmail['INS_BRANCH_EMAIL']) $this->sendEmail($getInsBranchEmail['INS_BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
						}
					}

					$privilege = 'AMST';
					$logAction = 'Menyetujui Usulan Pembaharuan Manual Status Transaksi Gagal menjadi Sukses untuk BG No : ' . $detailBg['BG_NUMBER'] . ', NoRef Transaksi : ' . $detailTrx['TRANSACTION_ID'] . ', Catatan Penolakan : ' . $params['reason'];
					Application_Helper_General::writeLog($privilege, $logAction);
					return true;
				}
			}

			$params['reason'] = 'dump';

			// REJECT PROCESS
			if ($action == 'rj') {
				// CHECK PRIVILEGE
				if (!$this->view->hasPrivilege('AMST')) {
					http_response_code(401);
					echo json_encode([
						'result' => false,
						'message' => $this->language->_('Unauthorized')
					]);
					return false;
				}

				// UPDATE TO REQUEST REPAIRED
				if ($model->updateReject($changesId, $params['reason'], $this->_userIdLogin)) {
					echo json_encode([
						'result' => true,
						'message' => $this->language->_('Sukses')
					]);

					if ($params['log'] == 'false') return true;

					$privilege = 'AMST';
					$logAction = 'Menolak Usulan Pembaharuan Manual Status Transaksi Gagal menjadi Sukses untuk BG No : ' . $detailBg['BG_NUMBER'] . ', NoRef Transaksi : ' . $detailTrx['TRANSACTION_ID'] . ', Catatan Penolakan : ' . $params['reason'];
					Application_Helper_General::writeLog($privilege, $logAction);
					return true;
				}
			}

			// REQUEST REPAIRED
			if ($action == 'rr') {

				// CHECK PRIVILEGE
				if (!$this->view->hasPrivilege('AMST')) {
					http_response_code(401);
					echo json_encode([
						'result' => false,
						'message' => $this->language->_('Unauthorized')
					]);
					return false;
				}

				// UPDATE TO REQUEST REPAIRED
				if ($model->updateRequestRepair($changesId, $params['reason'])) {
					echo json_encode([
						'result' => true,
						'message' => $this->language->_('Sukses')
					]);

					if ($params['log'] == 'false') return true;

					$privilege = 'AMST';
					$logAction = 'Permintaan Perbaikan Pembaharuan Status, Transaction Id = ' . $detailRequest['TRANSACTION_ID'];
					Application_Helper_General::writeLog($privilege, $logAction);
					return true;
				}
			}
		} catch (\Throwable $th) {
			//throw $th;
			Zend_Debug::dump($th);
			die();
		}
	}

	public function repairchangesAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// LOAD MODEL
		try {
			//code...
			$model = new manualupdate_Model_Suggestiondetail();
		} catch (\Throwable $th) {
			//throw $th;
			Zend_Debug::dump($th);
			die();
		}

		$params = $this->_request->getParams();
		$memoFile = $_FILES['memo'];

		$changesId = $params['changes_id'];

		// GET DETAIL REQUEST
		$detailRequest = $model->getDetailRequest($changesId);

		// GET DETAIL TRANSACTION
		$detailTrx = $model->getDetailTransaction($detailRequest['TRANSACTION_ID']);

		// GET DETAIL BG
		$explodeDetailBg = explode('~|~', $detailTrx['DETAIL_BG']);
		$detailBg = $this->parseDetailBg($explodeDetailBg);

		// UPLOAD FILE\
		$attachmentDestination   = UPLOAD_PATH . '/document/memopembaharuan/';
		mkdir($attachmentDestination);

		$extension = array_pop(explode('.', $memoFile['name']));

		$date = date("dmy");
		$time = date("his");
		$namaBaru = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . '_' . rtrim(substr($memoFile['name'], 0, 63), '.pdf') . '.' . $extension;

		try {
			//code...
			move_uploaded_file($memoFile['tmp_name'], $attachmentDestination . $namaBaru);
		} catch (\Throwable $th) {
			//throw $th;
			Zend_Debug::dump($th);
			die();
		}

		// REPAIR CHANGES
		$updateData = [
			'UPDATE_SUGGESTED' => new Zend_Db_Expr('now()'),
			'UPDATE_SUGGESTEDBY' => $this->_userIdLogin,
			'UPDATE_MEMO_FILE' => $namaBaru,
			'UPDATE_REMARKS' => $params['catatan'],
		];

		$model->repairChanges($changesId, $updateData);

		$this->_redirect('/notification/popupsuccess/index');
	}

	private function prepareForSendEmail($data, $emailTemplate, $nominal)
	{
		$settings = new Settings();
		$allSetting = $settings->getAllSetting();

		$config = Zend_Registry::get('config');

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);

		$bgclosingStatus   = $config["bgclosinghistory"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosinghistory"]["status"]["code"];
		$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);

		$getEscrowRelease = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->query()->fetchAll();

		$benefAcctNumber = array_shift(array_filter($getEscrowRelease, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'beneficiary account number';
		}))['PS_FIELDVALUE'];

		$benefAcctName = array_shift(array_filter($getEscrowRelease, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'beneficiary account name';
		}))['PS_FIELDVALUE'];

		$getBankName = $this->_db->select()
			->from(['mdbt' => 'M_DOMESTIC_BANK_TABLE'], ['mdbt.BANK_NAME'])
			->where('mdbt.SWIFT_CODE = ?', new Zend_Db_Expr('(SELECT PS_FIELDVALUE FROM TEMP_BANK_GUARANTEE_DETAIL_CLOSE tbgdc WHERE tbgdc.CLOSE_REF_NUMBER = ' . $this->_db->quote($data['databg']['CLOSE_REF_NUMBER']) . ' AND LOWER(tbgdc.PS_FIELDNAME) = \'swift code\')'))
			->query()->fetch();

		// T BANK GUARANTEE UNDERLYING
		$getAllUnderlying = $this->_db->select()
			->from('T_BANK_GUARANTEE_UNDERLYING')
			->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
			->query()->fetchAll();

		$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

		$underlyingForEmail = '
		<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
			<br>
		<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>
		';

		$getBgCurrency = $this->_db->select()
			->from('T_BANK_GUARANTEE_DETAIL')
			->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER_SPESIAL'])
			->where('LOWER(PS_FIELDNAME) = ?', 'currency')
			->query()->fetch();

		$getBgCurrency = $getBgCurrency ? $getBgCurrency['PS_FIELDVALUE'] : '-';

		// GET SPLIT
		$getSplit = $this->_db->select()
			->from(['A' => 'T_BANK_GUARANTEE_SPLIT'])
			->where('A.BG_NUMBER = ?', $data['databg']['BG_NUMBER'])
			->query()->fetchAll();

		$filterWihtoutEscrow = array_filter($getSplit, function ($item) {
			return strtolower($item['ACCT_DESC']) != 'escrow';
		});

		$filterWihtoutEscrow = $filterWihtoutEscrow ?: [];

		$sumAll = array_sum(array_column($filterWihtoutEscrow, 'AMOUNT'));
		$sumAllNumberFormat = number_format($sumAll, 2);

		$dataEmail = [
			'[[close_ref_number]]' => $data['databg']['CLOSE_REF_NUMBER'],
			'[[bg_number]]' => $data['databg']['BG_NUMBER'],
			'[[bg_subject]]' => $data['databg']["BG_SUBJECT"],
			'[[usage_purpose]]' => ucwords(strtolower($data['databg']['USAGE_PURPOSE_DESC'])),
			'[[cust_name]]' => $data['databg']["CUST_NAME"],
			'[[cust_cp]]' => $data['databg']["CUST_CP"],
			'[[contract_name]]' => $underlyingForEmail,
			'[[bg_amount]]' => implode(' ', [$getBgCurrency, number_format($data['databg']['BG_AMOUNT'], 2)]),
			'[[tra_status]]' => 'Sukses',
			'[[recipient_name]]' => $data['databg']['RECIPIENT_NAME'],
			'[[recipient_cp]]' => $data['databg']['RECIPIENT_CP'],
			'[[counter_warranty_type]]' => $arrbgcg[$data['databg']['COUNTER_WARRANTY_TYPE']],
			'[[change_type]]' => $arrbgclosingType[$data['databg']['CHANGE_TYPE_CLOSE']],
			'[[suggestion_status]]' => $arrbgclosingStatus[15],
			'[[repair_notes]]' => $data['historydata']['BG_REASON'],
			'[[recipient_name]]' => $data['databg']['RECIPIENT_NAME'],
			'[[beneficiary_account_number]]' => $benefAcctNumber,
			'[[time_period_start]]' => date('d M Y', strtotime($data['databg']['TIME_PERIOD_START'])),
			'[[time_period_end]]' => date('d M Y', strtotime($data['databg']['TIME_PERIOD_END'])),
			'[[bank_name]]' => $getBankName['BANK_NAME'],
			'[[beneficiary_account_name]]' => $benefAcctName,
			'[[insurance_name]]' => $data['databg']['INS_NAME'] ?: '-',
			'[[insurance_branch_name]]' => $data['databg']['INS_BRANCH_NAME'],
			'[[ps_status]]' => 'Proses Selesai',
			'[[ps_done]]' => date('d-m-Y'),
			'[[title_counter_type]]' => $data['databg']['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
			'[[md_principal]]' => implode(' ', [$getBgCurrency, $sumAllNumberFormat]),
		];

		$getEmailTemplate = $allSetting[$emailTemplate];
		$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);

		if ($data['databg']['PSLIP_CHANGE_TYPE'] == 3) {
			// GET BENEF ACCOUNT
			$getBenefAccount = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
				->query()->fetch();

			$getBenefAccount = $getBenefAccount['PS_FIELDVALUE'];

			$checkTransaction = $this->_db->select()
				->from('T_BG_TRANSACTION')
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('BENEF_ACCT = ?', $getBenefAccount)
				->query()->fetch();

			$dataEmail = [
				'[[transaction_id]]' => $checkTransaction['TRANSACTION_ID'],
				'[[transaction_amount]]' => implode(' ', [$checkTransaction['TRA_CCY'], number_format($checkTransaction['TRA_AMOUNT'], 2)]),
				'[[transaction_date]]' => date('d-m-Y', strtotime($checkTransaction['TRA_TIME'])),
			];

			$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);
		}

		return $getEmailTemplate;
	}

	private function sendEmail($emailAddress, $subjectEmail, $emailTemplate)
	{
		$setting = new Settings();
		$allSetting = $setting->getAllSetting();

		$data = [
			'[[master_bank_name]]' => $allSetting["master_bank_name"],
			'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_bank_email]]' => $allSetting["master_bank_email"],
			'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
		];

		$getEmailTemplate = strtr($emailTemplate, $data);
		Application_Helper_Email::sendEmail($emailAddress, $subjectEmail, $getEmailTemplate);
	}

	private function combineCodeAndDesc($arrayCode, $arrayDesc)
	{
		return array_combine($arrayCode, array_map(function ($arrayMap) use ($arrayDesc, $arrayCode) {
			return $arrayDesc[$arrayMap];
		}, array_keys($arrayCode)));
	}
}
