<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'General/CustomerUser.php';
class accountstatement_BalancedetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		

		$chooseform = $zf_filter_input->chooseform;
		if($this->_request->isPost() && $chooseform == 'applyloan' ) {

			$loan_type = $this->_getParam('loan_type');
			$loan_purpose = $this->_getParam('loan_purpose');
			$loan_amount = (float)$zf_filter_input->loan_amount;
			$loan_month = $zf_filter_input->loan_month;
			$job_type = $this->_getParam('job_type');
			$job_sector = $zf_filter_input->job_sector;
			$job_income_monthly = $this->_getParam('job_income_monthly');
			$job_income_type = $this->_getParam('job_income_type');
			$guarantee_type = $this->_getParam('guarantee_type');
			$guarantee_address = $zf_filter_input->guarantee_address;
			$guarantee_condition = $this->_getParam('guarantee_condition');
			$guarantee_province = $this->_getParam('guarantee_province');
			$guarantee_city = $this->_getParam('guarantee_city');
			$loan_status = 0;
			
			$content = array(
				'CUST_ID' 	   => $this->_custIdLogin,
				'USER_ID' 	   => $this->_userIdLogin,
				'LOAN_TYPE'	   => $loan_type,
				'LOAN_PURPOSE' => $loan_purpose,
				'LOAN_AMOUNT' => $zf_filter_input->loan_amount,
				'LOAN_INSTALLMENT' => 5,
				'LOAN_MONTH' => $zf_filter_input->loan_month,
				'JOB_TYPE' => $zf_filter_input->job_type,
				'JOB_SECTOR' => $zf_filter_input->job_sector,
				'JOB_INCOME_MONTHLY' => $job_income_monthly,
				'JOB_INCOME_TYPE' => $job_income_type,
				'GUARANTEE_TYPE' => $guarantee_type,
				'GUARANTEE_ADDRESS' => $zf_filter_input->guarantee_address,
				'GUARANTEE_CONDITION' => $guarantee_condition,
				'GUARANTEE_PROVINCE' => $guarantee_province,
				'GUARANTEE_CITY' => $guarantee_city,
				'LOAN_STATUS' => 'WA', //waiting for approval
				'LOAN_SUGGESTED' => date('Y-m-d H:i:s'),
				'LOAN_SUGGESTEDBY' => $this->_userIdLogin
			);
			try {
				$this->_db->insert('T_LOAN', $content); 	
			}
			catch(Exception $e) {
				var_dump($loan_amount); die;
			}
			

			
			$this->setbackURL('/accountstatement/balancedetail'); 
			$this->_redirect('/notification/success/index');
		}

		elseif($this->_request->isPost() && $chooseform == 'applydepo') {
			// $date = Date($this->_getParam('PSEFDATE'));
			// $date = date($this->_getParam('PSEFDATE'), "Y-m-d");
			$contentDepo = array(
				'CUST_ID' 	  	=> $this->_custIdLogin,
				'USER_ID'		=> $this->_userIdLogin,
				// 'OPENING_DATE'	=> $this->_getParam('PSEFDATE'),
				'SOURCE_ACCT'	=> $this->_getParam('add_tempsourceacct'),
				'DEPOSIT_AMOUNT' => $this->_getParam('deposit_amount'),
				'DEPOSIT_MONTH' => $this->_getParam('deposit_month'),
				'ARO' => $this->_getParam('aro'),
				'DEPOSIT_CITY' => $this->_getParam('deposit_city'),
				'DEPOSIT_BRANCH' => $this->_getParam('deposit_branch'),
				'DEPOSIT_ADDRESS' => $this->_getParam('branch_address'),
				'OWNER_OF_FUNDS' => $this->_getParam('ss'),
				'AIM_OBJECTIVE' => $this->_getParam('aim_objective'),
				'DEPOSIT_PURPOSE' => $this->_getParam('deposit_purpose'),
				'AVERAGE_TRANSACTION' => $this->_getParam('average_transaction'),
				'DEPOSIT_STATUS' => 'WA',
				'DEPOSIT_CREATED' => date('Y-m-d H:i:s'),
				'DEPOSIT_CREATEDBY' => $this->_userIdLogin
			);
			try {
				$this->_db->insert('T_DEPOSIT', $contentDepo); 	
			}
			catch(Exception $e) {
				var_dump($contentDepo); die();
			}

			$this->setbackURL('/accountstatement/balancedetail'); 
			$this->_redirect('/notification/success/index');
		}

		$fields = array(
						'acct'  => array('field' => 'ACCT_NO',
											   'label' => 'Account Number',
											   'sortable' => true),
						'acct_name'  => array('field' => 'ACCT_NAME',
											   'label' => 'Account Name',
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => 'CCY',
											  'sortable' => true),
						'status'   => array('field'    => '',
											  'label'    => 'Account Status',
											  'sortable' => true),
						'effective'   => array('field'    => '',
											  'label'    => 'Effective Balance',
											  'sortable' => true)
		);
				
		$sortBy  = $this->_getParam('sortby','acct');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
			
		$custInfo = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('C' => 'M_CUSTOMER'))
						->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				);
		$this->view->custname	= $custInfo['CUST_NAME'];
		$this->view->custadd	= $custInfo['CUST_ADDRESS'];
		$this->view->custcity	= $custInfo['CUST_CITY'];
		$this->view->custprov	= $custInfo['CUST_PROVINCE'];
		$this->view->custzip	= $custInfo['CUST_ZIP'];
		$this->view->custphone	= $custInfo['CUST_PHONE'];
		$this->view->custfax	= $custInfo['CUST_FAX'];
		$this->view->finance	= $custInfo['CUST_FINANCE'];
		$this->view->custemail	= $custInfo['CUST_EMAIL'];
										  // print_r($custInfo);die;
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();
		
		$this->view->AccArr =  $AccArr;

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$core = array();
		$svcAccount = new Service_Account($custInfo['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
		// $result		= $svcAccount->cifaccountInquiry($core);
		// print_r($userCIF);
		$result		= $svcAccount->cifaccountInquiry($core);
		// var_dump($result);die;
		if($result['ResponseDesc'] != 'Success'){
			if($result['ResponseDesc'] != 'No Data'){
			$this->view->error_per = $result['ResponseDesc'];
			}
		}

		 
		// Zend_Debug::dump($core->accountList);
		// echo count($core->accountList). " acct list"; die;
		//$Account = new Account($row->accountNo,$row->ccy);

		if(count($core->accountList) > 0){

				$arrResult = array();
					$err = array();

					if(count($core->accountList) > 1){
// 			   		print_r($core->accountList);
						//array data lebih dari 1
						foreach ($core->accountList as $key => $val){
							if($val->status == '0'){
								$arrResult = array_merge($arrResult, array($val->accountNo => $val));
								array_push($err, $val->accountNo);
							}
// 					   	echo 'here';
						}
					}
					else{
						//array kurang dari 2
						foreach ($core as $data){
							if($data->status == '0'){
								$arrResult = array_merge($arrResult, array($data->accountNo => $data));
								array_push($err, $data->accountNo);
							}
						}
					}

				$final = array();
				$i = 0;
//
				foreach($arrResult as $key => $val){
					$final[$i++] = $val;
				}


				if(count($final) < 1){
					$this->view->data1 = '0';
				}
				else{
					$this->view->data = $final;
				}


//				Zend_Debug::dump($final, "final"); die;
					foreach ($final as $dataProductPlan){

//				   		if($dataProductPlan->status == '0'){

								if (isset($dataProductPlan->productPlan) && isset($dataProductPlan->planCode))
								{
								$select_product_type	= $this->_db->select()
											->from(array('M_PRODUCT_TYPE'),'PRODUCT_NAME')
				//							->where('PRODUCT_CODE = ?','10');
				//							->where("PRODUCT_CODE IN(?)", $dataPro)
											->where("PRODUCT_CODE = ?", $dataProductPlan->productPlan)
											->where("PRODUCT_PLAN = ?", $dataProductPlan->planCode)
											->where("ACCT_TYPE = 4");
				//							->where('PRODUCT_PLAN = ?','62');
								$userData_product_type = $this->_db->fetchCol($select_product_type);

								$dataProductPlan->planName = $userData_product_type[0];
								$dataProductPlanOri = (($dataProductPlan->planName == null)?'':$dataProductPlan);
								}else{
									$dataProductPlanOri = $dataProductPlan;
								}

							$dataUser[] = $dataProductPlanOri;

//				   		}
					}
					// echo "<pre>";
					// var_dump($dataUser);die;
//				echo count($dataUser); die;
//				Zend_Debug::dump($dataUser,"datauser"); die;
				if (count($dataUser) > 0)
				{
					$datapers = array();
					$i = 0;
					foreach ( $dataUser as $key=>$row ) {

						if($row->productType == '40'){

						if (!empty($row->accountNo)){
							$Account = new Account($row->accountNo,$row->ccy);
							$Account->setFlag(false);
							$Account->checkBalance();
	
							// var_dump($Account);die;
	
							if($row->planCode != NULL && $row->productType != NULL){
								$product = $model->getProduct($row->planCode,$row->productPlan);
								$temp = $model->getCustAccount(array('userId'=>$this->_userIdLogin));
	
								if (empty($product)){
									// echo "test";
								}else{
									$product_name = $product['0']['PRODUCT_NAME'];
									$acct_type = $product['0']['ACCT_TYPE'];
									$acct_name = $temp['0']['ACCT_NAME'];
									if($result['ResponseCode'] == '00'){
										$status = "Active";
									}
								}
							}else{
								$product_name = 'RDN GIRO';
							}
	
							$datapers[$i]['ACCT_TYPE'] = $acct_type;
							$datapers[$i]['ACCT_PRODUCT_NAME'] = 'RDN GIRO';
							$datapers[$i]['ACCT_NO'] = $row->accountNo;
							$datapers[$i]['ACCT_NAME'] = $this->_custNameLogin;
							$datapers[$i]['STATUS'] = 'Active';
							$datapers[$i]['CCY_ID'] = Application_Helper_General::getCurrCode($row->ccy);
							$datapers[$i]['PRODUCT_NAME'] = $row->PRODUCT_NAME;
							$datapers[$i]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
							$datapers[1]['ACCT_TYPE'] = $acct_type;
							$datapers[1]['ACCT_PRODUCT_NAME'] = 'RDN GIRO'; 
							$datapers[1]['ACCT_NO'] = '2001029000125';
							$datapers[1]['ACCT_NAME'] = $this->_custNameLogin;
							$datapers[1]['STATUS'] = 'Active';
							$datapers[1]['CCY_ID'] = Application_Helper_General::getCurrCode($row->ccy);
							$datapers[1]['PRODUCT_NAME'] = $row->PRODUCT_NAME;
							$datapers[1]['AVAILABLE_BALANCE'] = 160000000;
							$datapers[2]['ACCT_TYPE'] = $acct_type;
							$datapers[2]['ACCT_PRODUCT_NAME'] = 'RDN GIRO';
							$datapers[2]['ACCT_NO'] = '2001029000161';
							$datapers[2]['ACCT_NAME'] = $this->_custNameLogin;
							$datapers[2]['STATUS'] = 'Active';
							$datapers[2]['CCY_ID'] = Application_Helper_General::getCurrCode($row->ccy);
							$datapers[2]['PRODUCT_NAME'] = $row->PRODUCT_NAME;
							$datapers[2]['AVAILABLE_BALANCE'] = 50000000;
							$i++;
						}

						}

					}
				}

		}

		$this->view->datardn = $datapers;




		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$data = $this->_db->fetchAll(
					$this->_db->select()->distinct()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->group('A.ACCT_NO')						 
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
				
				// echo $data;die;
		$isGroup = false;
		if(count($data))
		{
			foreach($data as $key => $row)
			{
				if($row['CCY_ID'] != 'IDR' && $row['CCY_ID'] != 'USD'){
					$Curency = $this->_db->fetchRow(
						$this->_db->select()
						->from(array('C' => 'M_CURRENCY'),array('*'))
						->where("CCY_NUM = ?",$row['CCY_ID'])
						
					);
					
					
					 $where['ACCT_NO = ?'] = $row['ACCT_NO'];
					 $where['CUST_ID = ?'] = $this->_custIdLogin;

					$updateArr = array('CCY_ID' => $Curency['CCY_ID']);
		

					$this->_db->update('M_CUSTOMER_ACCT',$updateArr,$where);

					$data[$key]['CCY_ID'] = $Curency['CCY_ID'];
					//var_dump($Curency);die;
				}
				
				if($row['GROUP_ID'])
				{
					$isGroup = true;
				}
			}
		}
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		$accountNum = $this->_getParam('accountNum');
		
		$this->view->isGroup = $isGroup;
		$this->view->custId = $this->_custIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
		
		$logDesc = 'Viewing Balance Inquiry';
	if($csv)
		{
			$arr = array();
			
			$header = array(
				$this->language->_('Account No'),
				$this->language->_('Account Alias'),
				$this->language->_('Account Name'),
				$this->language->_('CCY'),
				$this->language->_('Type'),
				'Status',
				$this->language->_('Available Balance')
			);
			
			if(!$isGroup)
			{
				$arr[0] = $header;
				$i = 1;
				$arr_value_tot_group =  array();
				$arr_display_tot_group =  array();
				foreach($data as $row)
				{
					
					$systemBalance = new SystemBalance($row['CUST_ID'],$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
					$systemBalance->setFlag(false);
					$systemBalance->checkBalance();
					
					
					if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
			          $arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
			        }
			        else {
			          $arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
			        }
					$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);
					$accountStatusModified = $systemBalance->getCoreAccountStatusDesc();
						
					$arr[$i][] = $row['ACCT_NO'];
					$arr[$i][] = $row['ACCT_ALIAS_NAME'];
					$arr[$i][] = $systemBalance->getCoreAccountName();
					$arr[$i][] = $row['CCY_ID'];
					$arr[$i][] = $row ['ACCT_DESC'];
					$arr[$i][] = $accountStatusModified;
					$arr[$i][] = $systemBalance->getEffectiveBalance();
					
					$i++;
				}
				$display_tot_group = implode("; ",$arr_display_tot_group);
				$arr[$i][] = $this->language->_('TOTAL EFFECTIVE BALANCE').' : '.$display_tot_group;	
			}
			else
			{
				$i = 0;
				$group = null;
				
				$resultdataGroup =  array();
				foreach ($data as $rowGroup){
					$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup; 
				}
				$i = 0;
				$group = null;
				$arr_value_tot_group = array();
				foreach ( $resultdataGroup as $groupname=>$rowPerGroup ) 
				{
					$i++;
					$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
					$groupname = ($groupname) ? $groupname : 'OTHERS';
					$arr[$i][] = $groupname;
					$i++;
					$arr[$i][] = $header;
					
						if(count($rowPerGroup) > 0){
							$arr_value_tot_group =  array();
							$arr_display_tot_group =  array();
							foreach ($rowPerGroup as $row){
							$i++;	
							$systemBalance = new SystemBalance($this->_custIdLogin,$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
							$systemBalance->setFlag(false);
							$systemBalance->checkBalance();
							$model = new accountstatement_Model_Accountstatement();
								if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
									$arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
								}else{
									$arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
								}
								$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);
							$accountStatusModified = $systemBalance->getCoreAccountStatusDesc();
// 							print_r($row);die;
							if($systemBalance->getCoreProductType() != NULL || $systemBalance->getCoreProductType() != 'N/A'){
								// 					print_r('here');
								$product = $model->getProduct($systemBalance->getCoreProductType(),$row ['PRODUCT_CODE']);
								$product_name = $product['0']['PRODUCT_NAME'];
							}else{
								// 					print_r('here1');
								$product_name = 'N/A';
							}
							
							$arr[$i][] = $row['ACCT_NO'];
							$arr[$i][] = $row['ACCT_ALIAS_NAME'];
							$arr[$i][] = $systemBalance->getCoreAccountName();
							
							$arr[$i][] = $row['CCY_ID'];
							$arr[$i][] = $product_name;
// 							$arr[$i][] = $row ['ACCT_DESC'];
							$arr[$i][] = $accountStatusModified;
							$arr[$i][] = $systemBalance->getEffectiveBalance();
							
							}
						}
						$i++;	
						$display_tot_group = implode("; ",$arr_display_tot_group);	
						$arr[$i][] = $this->language->_('TOTAL EFFECTIVE BALANCE').' : '.$display_tot_group;
						
						
					
				} 
										
			}
				$logDesc = 'Export to CSV';
				$this->_helper->download->csv(array(),$arr,null,'Balance Detail');  
		
		}
		
		if($accountNum && $pdf){
			$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();
		//Zend_Debug::dump($AccArr);die; 
		$Account = new Account($AccArr['0']['ACCT_NO'],$AccArr['0']['CCY_ID']);
		$Account->setFlag(false);
		
			$res = $Account->checkBalanceDeposit();
			$accountListData = $res['AccountList'];
			foreach ( $accountListData as $key=>$row ) {
				if(($row ['accountNo'] == $accountNum)){
					$data_deposit = array(
						'accountNo' => $row ['accountNo'],
						'accountName' => "PT FEDERAL INTERNATIONAL FINANCE",
						'currency' => $row ['currency'],
						'interestRate' => $row ['interestRate'],
						'amount' => $row ['amount'],
						'createDate' => Application_Helper_General::convertDate($row ['createDate'],'d-F',$this->defaultDateFormat),
						'dueDate' => Application_Helper_General::convertDate($row ['dueDate'],'d-F',$this->defaultDateFormat),
						'status' => $row ['status'],
					);
				}else{
					continue;
				}
			}
			
			$this->view->dDeposit = $data_deposit;
			
			$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf_redemption.phtml')."</td></tr>";
			//echo $outputHTML;die;
			$this->_helper->download->pdf(null,null,null,'Term Deposit Redemption',$outputHTML);
		}
		/*
		if($pdf == "1"){
			$res = $Account->checkBalanceDeposit();
			$accountListData = $res['AccountList'];
			foreach ( $accountListData as $key=>$row ) {
				if(($row ['accountNo'] == $accountNum)){
					$data_deposit = array(
						'accountNo' => $row ['accountNo'],
						'accountName' => "PT FEDERAL INTERNATIONAL FINANCE",
						'currency' => $row ['currency'],
						'interestRate' => $row ['interestRate'],
						'amount' => $row ['amount'],
						'createDate' => $row ['createDate'],
						'dueDate' => $row ['dueDate'],
						'status' => $row ['status'],
					);
				}else{
					continue;
				}
			}

			$this->view->dDeposit = $data_deposit;
			
			$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf_redemption.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Term Deposit Redemption',$outputHTML);

		}else{
		//Zend_Debug::dump($this->view->render($this->view->controllername.'/pdf.phtml'));die;
		Application_Helper_General::writeLog('BAIQ','Print PDF');
		$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf.phtml')."</td></tr>";
		//Zend_Debug::dump($outputHTML);die;
		$this->_helper->download->pdf(null,null,null,'Balance Detail',$outputHTML);
		}
		*/
		
		if($this->_request->getParam('print') == 1){
			$arr = $data;
			$fields = array(
						'ACCT_NO'  => array('field' => 'ACCT_NO',
											   'label' => $this->language->_('Account Number'),
											   'sortable' => true),
						'ACCT_NAME'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Account Name'),
											   'sortable' => true),
						'CCY_ID'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
			
						'ACCT_DESC'   => array('field'    => 'ACCT_DESC',
											  'label'    => $this->language->_('Type'),
											  'sortable' => true),
			
						'FREEZE_STATUS'   => array('field'    => 'FREEZE_STATUS',
											  'label'    => $this->language->_('Status'),
											  'sortable' => true),
						'BALANCE'   => array('field'    => 'BALANCE',
											  'label'    => $this->language->_('Available Balance'),
											  'sortable' => true)
			);
			
//            $data = $arr;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Balance Inquiry', 'data_header' => $fields));
        }
		
		Application_Helper_General::writeLog('BAIQ',$logDesc);

		//balance detail

		$fields_deposit = array(
						'depositno'  => array('field' => 'DEPOSIT_NO',
											   'label' => $this->language->_('Deposit No'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
												'label'    => $this->language->_('CCY'),
												'sortable' => true),
						'interest'  => array('field' => 'INTEREST_RATE',
											   'label' => $this->language->_('Interest Rate'),
											   'sortable' => true),
						'availablebal'   => array('field'    => 'AVAIL_BAL',
											  'label'    => $this->language->_('Available Balance'),
											  'sortable' => true),
						'createddate'   => array('field'    => 'CREATED_DATE',
											  'label'    => $this->language->_('Created Date'),
											  'sortable' => true),
						'duedate'   => array('field'    => 'DUE_DATE',
											  'label'    => $this->language->_('Due Date'),
											  'sortable' => true),
				);
				
		$sortBy_deposit  = $this->_getParam('sortby','acct');
		$sortDir_deposit = $this->_getParam('sortdir','asc');
		
		// $sortBy_deposit = (Zend_Validate::is($sortBy_deposit,'InArray',
		// 							 array(array_keys($fields_deposit))
		// 							 ))? $fields_deposit[$sortBy_deposit]['field'] : $fields_deposit[key($fields_deposit)]['field'];

		// $sortDir_deposit = (Zend_Validate::is($sortDir_deposit,'InArray',
		// 								  array('haystack'=>array('asc','desc'))
		// 								  ))? $sortDir_deposit : 'asc';
			
										  
		$this->view->sortByP_deposit = $sortBy_deposit;
		$this->view->sortDir_deposit = $sortDir_deposit;
		
	/*	$data = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->group('A.ACCT_NO')						 
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
		*/
		$userId_deposit = $this->_userIdLogin;
		$model_deposit = new accountstatement_Model_Accountstatement();
		$userCIF_deposit = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_USER'))
				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
				->limit(1)
		);
// 		$userCIF_deposit = $model_deposit->getUserCIF($this->_userIdLogin);
		//print_r($this->_userIdLogin);die;
		$param['userId'] = $userId_deposit;
		$userAcctInfo = $model_deposit->getCustAccount($param);
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];
		
		//print_r($userAcctInfo);die;
		 //var_dump($accountNo);
		 //print_r($accountCcy);
		// print_r($accountType);
		// print_r($userCIF_deposit);
		// print_r($userId_deposit);die;
		// pei
		if(!empty($userAcctInfo)){
		$Account = new Account($accountNo, $accountCcy, $accountType, $userCIF_deposit['CUST_CIF'], $userId_deposit);
		$Account->setFlag(false);

		$res = $Account->checkBalanceDeposit();
		$accountListData = $res['AccountList'];
		//print_r($accountListData);die;
		$csv_deposit = $this->_getParam('csvdepo');
		$pdf_deposit = $this->_getParam('pdfdepo');
		$totalAvailableBalance_deposit = 0;
		
		if (count ( $accountListData ) > 0)
		{
			$data_deposit = array();
			
			foreach ( $accountListData as $key=>$row ) {
				/*$Account = new Account($row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
				 $Account->setFlag(false);
				$Account->checkBalanceDeposit();*/
				if (isset($row ['accountNo'])){
					//				if ($row ['status'] == "A"){
					$data_deposit[$key]['accountNo'] 	= $row ['accountNo'];
					$data_deposit[$key]['currency'] 	= $row ['currency'];
					$data_deposit[$key]['interestRate'] = $row ['interestRate'];
					$data_deposit[$key]['amount'] 		= $row ['amount'];
					$data_deposit[$key]['createDate'] 	= $row ['createDate'];
					$data_deposit[$key]['dueDate'] 		= $row ['dueDate'];
					$data_deposit[$key]['status'] 		= $row ['status'];
					//				}
				}
			}
		}
		}
		// print_r($data_deposit);die;
		$data_deposit = array();
		$data_deposit[0]['accountNo'] = "20520054011";
		$data_deposit[0]['accountName'] = "PT FEDERAL INTERNATIONAL FINANCE";
		$data_deposit[0]['currency'] = "IDR";
		$data_deposit[0]['interestRate'] = "0.0925";
		$data_deposit[0]['amount'] = "100,000,000";
		$data_deposit[0]['createDate'] = "2018-07-15";
		$data_deposit[0]['dueDate'] = "2019-07-15";
		$data_deposit[0]['status'] = "Open";

		$data_deposit[1]['accountNo'] = "20520065209";
		$data_deposit[1]['accountName'] = "PT FEDERAL INTERNATIONAL FINANCE";
		$data_deposit[1]['currency'] = "IDR";
		$data_deposit[1]['interestRate'] = "0.09";
		$data_deposit[1]['amount'] = "300,000,000";
		$data_deposit[1]['createDate'] = "2017-06-16";
		$data_deposit[1]['dueDate'] 		= "2019-06-16";
		$data_deposit[1]['status'] = "Open";
	/*
		$accData = $this->_db->fetchAll(
					$this->_db->select()
					->from('M_CUSTOMER_ACCT',array('ACCT_NAME', 'ACCT_NO', 'CCY_ID'))
					->where('CCY_ID = ?','IDR')
					->where('CUST_ID = ?',$this->_custIdLogin)
					->order('ACCT_NO ASC')
				); */
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$accData 	  = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only		
				
		$this->view->accData = $accData;
		$this->view->custId = $this->_custIdLogin;
		// $this->view->resultdata_deposit = $data_deposit;
// 		$this->view->fields = $fields_deposit;
		
		$logDesc_deposit = 'Viewing';
		
		if($csv_deposit || $pdf_deposit){
			$arr_deposit = $data_deposit;
			$header_deposit = array(
				$this->language->_('Deposit No'),
				$this->language->_('CCY'),
				$this->language->_('Interest Rate'),
				$this->language->_('Available Balance'),
				$this->language->_('Create Date'),
				$this->language->_('Due Date'),
				$this->language->_('Status'),
			);
		}
		if($csv_deposit)
		{
			$logDesc_deposit = 'Export to CSV';
			$this->_helper->download->csv($header_deposit,$arr_deposit,null,$this->language->_('Deposit Account Inquiry'));  
		}
		
		if($pdf_deposit){
			$logDesc_deposit = 'Export to PDF';
			$this->_helper->download->pdf($header_deposit,$arr_deposit,null,$this->language->_('Deposit Account Inquiry'));  
		}
		
		if($this->_request->getParam('print') == 1){
			$arr_deposit = $data_deposit;
			$fields_deposit = array(
						'ACCT_NO'  => array('field' => 'accountNo',
											   'label' => $this->language->_('Deposit No'),
											   'sortable' => true),
						'CCY_ID'   => array('field'    => 'currency',
												'label'    => $this->language->_('CCY'),
												'sortable' => true),
						'INRATE'  => array('field' => 'interestRate',
											   'label' => $this->language->_('Interest Rate'),
											   'sortable' => true),
						'AVAILABLE_AMOUNT'   => array('field'    => 'amount',
												'label'    => $this->language->_('Available Balance'),
												'sortable' => true),
			
						'CREATEDATE'   => array('field'    => 'createDate',
											  'label'    => $this->language->_('Create Date'),
											  'sortable' => true),
			
						'DUEDATE'   => array('field'    => 'dueDate',
											  'label'    => $this->language->_('Due Date'),
											  'sortable' => true),
						
						'STATUS'   => array('field'    => 'status',
											  'label'    => $this->language->_('Status'),
											  'sortable' => true)
						
			);
			
//            $data_deposit = $arr_deposit;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $arr_deposit, 'data_caption' => 'Deposit Inquiry', 'data_header' => $fields_deposit));
        }
		
		
		Application_Helper_General::writeLog('DEIQ',$logDesc_deposit);
// 		$this->view->userId = $this->_userIdLogin;

		$this->view->resultdata_deposit = $data_deposit;
		$this->view->fields_deposit = $fields_deposit;
		$this->view->totalAvailableBalance_deposit = $totalAvailableBalance_deposit;


		//Loan

		$conf = Zend_Registry::get('config');
		$acctDposito = $conf['productaccount']['type']['desc']['loan'];
		$userId_loan = $this->_userIdLogin;
		$fields_loan = array(
						'acct'  => array('field' => 'ACCT_NO',
											   'label' => $this->language->_('Account Number'),
											   'sortable' => true),
						'acct_name'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Account Name'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						'prod_type'   => array('field'    => 'ACCT_PRODUCT_NAME',
											  'label'    => $this->language->_('Product Type'),
											  'sortable' => true),
				);
				
		$sortBy_loan  = $this->_getParam('sortby','acct');
		$sortDir_loan = $this->_getParam('sortdir','asc');
		

		/*$data = array();

		$data[0]['ACCT_NO'] = "100345721001";
		$data[0]['PROD_TYPE'] = "Pinjaman BJB";
		$data[0]['PLAFOND'] = "100,000,000.00";
		$data[0]['OUTSTANDING'] = "60,000,000.00";

		$data[1]['ACCT_NO'] = "1003457823004";
		$data[1]['PROD_TYPE'] = "Pinjaman BJB";
		$data[1]['PLAFOND'] = "150,000,000.00";
		$data[1]['OUTSTANDING'] = "45,000,000.00";*/			
										  
		$this->view->sortBy_loan = $sortBy_loan;
		$this->view->sortDir_loan = $sortDir_loan;
		
	/*	$data = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->where("D.ACCT_TYPE = ". $this->_db->quote($acctDposito))
						 ->group('A.ACCT_NO')						 
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
		*/
		$model_loan = new accountstatement_Model_Accountstatement();
		$userCIF_loan = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_USER'))
				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
				->limit(1)
		);
		
		$param['userId'] = $userId_loan;
		$userAcctInfo_loan = $model_loan->getCustAccount($param);
		$accountNo_loan 	= $userAcctInfo_loan[0]['ACCT_NO'];
		$accountType_loan = $userAcctInfo_loan[0]['ACCT_TYPE'];
		$accountCcy_loan = $userAcctInfo_loan[0]['CCY_ID'];
		//print_r($accountCcy_loan);
		//print_r($accountNo_loan);die;
		if(!empty($userAcctInfo_loan)){
		$Account_loan = new Account($accountNo_loan, $accountCcy_loan, $accountType_loan, $userCIF_loan['CUST_CIF'], $userId_loan);
		$Account_loan->setFlag(false);
		$res_loan = $Account_loan->checkBalanceLoan();
		$accountListData_loan = $res_loan['AccountList'];
		
		
				
		$csv_loan = $this->_getParam('csvloan');
		$pdf_loan = $this->_getParam('pdfloan');
		
		$this->view->custId_loan = $this->_custIdLogin;
// 		$this->view->resultdata = $data;
// 		$this->view->fields = $fields_loan;
		
		$totalAvailableBalance_loan = 0;
		if (count ( $accountListData_loan ) > 0) 
		{
			$data_loan = array();
			$dataprint_loan = array();
			foreach ( $accountListData_loan as $key=>$row ) {
				/*$Account = new Account($row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
				$Account->setFlag(false);
				$Account->checkBalanceLoan();*/
				if (isset($row ['accountNo'])){
					/*$productList_loan = $this->_db->select()->from(
		   		    		array('MPT' => 'M_PRODUCT_TYPE'),
		   		    		array('MPT.PRODUCT_CODE', 'MPT.PRODUCT_NAME')
		   		    	)
	 	   		    	->where('MPT.PRODUCT_CODE = ?',$row ['productPlan'])
	 	   		    	->where('MPT.PRODUCT_PLAN = ?',$row ['planCode'])
	 	   		    	->query()
		    			->fetchAll()
		    		;	    				
		    		$accountName = $productList_loan[0]['PRODUCT_NAME'] == NULL ? "N/A" : $productList_loan[0]['PRODUCT_NAME'];*/
		    		
		    		$productList_loan = $model_loan->getProduct($row ['planCode'],$row ['productPlan']);		    		
					$product_name_loan = $productList_loan[0]['PRODUCT_NAME'] == NULL ? "N/A" : $productList_loan[0]['PRODUCT_NAME'];
					
					//if ($row ['status'] == "A"){								
						$data_loan[$key]['accountNo'] 			= $row ['accountNo'];
						$data_loan[$key]['accountName'] 			= $product_name_loan;
						$data_loan[$key]['status'] 				= $row ['status'];				
						$data_loan[$key]['currency'] 			= $row ['currency'];				
						$data_loan[$key]['plafond'] 				= $row ['plafond'];
						$data_loan[$key]['numberOfInstallment'] 	= $row ['numberOfInstallment'];
						$data_loan[$key]['outstanding'] 			= $row ['outstanding'];
						$data_loan[$key]['interestObligations'] 	= $row ['interestObligations'];
						$data_loan[$key]['installment'] 			= $row ['installment'];
						$data_loan[$key]['totalPayment'] 		= $row ['totalPayment'];
						$data_loan[$key]['interestArrears'] 		= $row ['interestArrears'];
						$data_loan[$key]['arrearsInInstallments']= $row ['arrearsInInstallments'];
						$data_loan[$key]['paymentOfFines'] 		= $row ['paymentOfFines'];
						$data_loan[$key]['termsLoansTimes'] 		= $row ['termsLoansTimes'];
						$data_loan[$key]['accountCreateDate'] 	= $row ['accountCreateDate'];
						$data_loan[$key]['disbursementDate'] 	= $row ['disbursementDate'];
						$data_loan[$key]['nextPaymentDate'] 		= $row ['nextPaymentDate'];	
						$data_loan[$key]['dueDate'] 				= $row ['dueDate'];		
						$dataprint_loan[$key]['accountNo'] 		= $row ['accountNo'];
						$dataprint_loan[$key]['accountName'] 	= $row ['accountName'];
						$dataprint_loan[$key]['plafond'] 		= $row ['plafond'];
						$dataprint_loan[$key]['outstanding'] 	= $row ['outstanding'];						
					//}	
				}
			}
		}
	}
		$logDesc_loan = 'Viewing';
		
		if($csv_loan || $pdf_loan){			
			$arr_loan = $dataprint_loan;
			$header_loan = array(
				$this->language->_('Account No'),
				$this->language->_('Product Type'),
				$this->language->_('Plafond'),
				$this->language->_('Outstanding')
			);
		}
		if($csv_loan)
		{
			$logDesc_loan = 'Export to CSV';
			$this->_helper->download->csv($header_loan,$arr_loan,null,$this->language->_('Loan Account Inquiry'));  
		}
		
		if($pdf_loan){
			$logDesc_loan = 'Export to PDF';
			$this->_helper->download->pdf($header_loan,$arr_loan,null,$this->language->_('Loan Account Inquiry'));  
		}
		
		
		if($this->_request->getParam('print') == 1){
			$arr_loan = $dataprint_loan;
			$fields_loan = array(
						'ACCT_NO'  => array('field' => 'accountNo',
											   'label' => $this->language->_('Account No'),
											   'sortable' => true),
						'ACCT_NAME'  => array('field' => 'accountName',
											   'label' => $this->language->_('Product Type'),
											   'sortable' => true),
						'CCY_ID'   => array('field'    => 'plafond',
											  'label'    => $this->language->_('Plafond'),
											  'sortable' => true),
			
						'PROD_TYPE'   => array('field'    => 'outstanding',
											  'label'    => $this->language->_('Outstanding'),
											  'sortable' => true)
			);
			
//            $data = $arr_loan;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $arr_loan, 'data_caption' => 'Loan Account Inquiry', 'data_header' => $fields_loan));
        }
		
		
		Application_Helper_General::writeLog('BAIQ',$logDesc_loan);
		$this->view->userId_loan = $this->_userIdLogin;
		$this->view->resultdata_loan = $data;
		$this->view->fields_loan = $fields_loan;
		$this->view->totalAvailableBalance_loan = $totalAvailableBalance_loan;

	}

	function updatealiasAction(){
	
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $id = $this->_getParam('id');
        $cur = $this->_getParam('acct');

        // $where['ACCT_NO = ?'] = $acct;

        $where['ACCT_NO = ?'] = $cur;

		$updateArr = array('ACCT_ALIAS_NAME' => $id);
		// console.log()

		$this->_db->update('M_CUSTOMER_ACCT',$updateArr,$where);



        // echo Application_Helper_General::displayMoney($balance);
	}


	function balanceloadAction(){
	
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $id = $this->_getParam('id');
        $cur = $this->_getParam('cur');

        $systemBalance = new SystemBalance($this->_custIdLogin,$id,Application_Helper_General::getCurrNum($cur));
		$systemBalance->setFlag(false);
		$systemBalance->checkBalance();
		$balance  = $systemBalance->getEffectiveBalance();
		
        echo Application_Helper_General::displayMoney($balance);
	}

	function applyloanAction() {

		var_dump($zf_filter_input->cust_id); die;
		$content = array(
			'CUST_ID' 	 => $zf_filter_input->cust_id,
			'ATM_ADDRESS' 	 => $zf_filter_input->atm_address,
			'ATM_CITY' 	 => $zf_filter_input->city_name
			
		);
	}
	
	
}
