<?php

require_once 'Zend/Controller/Action.php';

class Minamountccy_RepairController extends Application_Main
{
	
	public function indexAction()
	{
		$error = FALSE;
		$fields = array(
						'available'      => array('field' => 'CCY_STATUS',
											      'label' => $this->language->_('Available'),
											      'sortable' => false),
						'currencies'           => array('field' => 'CCY_ID',
											      'label' => $this->language->_('Currencies'),
											      'sortable' => false),
						'currency_name'  => array('field' => 'CCY_DESCRIPTION',
											      'label' => $this->language->_('Currency Name'),
											      'sortable' => false),
						'min_transfer_amount'     => array('field' => '',
											      'label' => $this->language->_('Minimum Transfer Amount'),
											      'sortable' => false),
				      );
					  
		$submitArr = array('submit' => array('StripTags','StringTrim'),
        );

		$zf_submit = new Zend_Filter_Input($submitArr,array(),$this->_request->getParams());		
		$submit= $zf_submit->getEscaped('submit');	
		$select2 = $this->_db->select()
					        ->from(array('A' => 'M_CURRENCY'),array())
					        ->joinleft(array('B' => 'TEMP_MINAMT_CCY'),'A.CCY_ID = B.CCY_ID',
					        array('B.CCY_STATUS','A.CCY_DESCRIPTION','A.CCY_ID','A.CCY_NUM','B.MIN_TX_AMT',''));

//		$select = $this->_db->select()
//					        ->from(array('X' => 'M_MINAMT_CCY'));
		$result = $select2->query()->FetchAll();					  
		if($submit == TRUE && $this->view->hasPrivilege('CRCR'))
		{
			$changes_id = $this->_getParam('changes_id');
			$this->view->changes_id = $changes_id;
			$cek = $this->_db->select()
							->from('TEMP_MINAMT_CCY')
							->where('CHANGES_ID = ?',$changes_id)
							->query()->fetchall();
			
			if($cek){
				//Zend_Debug::dump($this->_getAllParams());
				$this->_db->beginTransaction();
				try{
				$temp = 0 ;
				//$info = 'CHANGES MINIMUM AMMOUNT CURRENCY';
				$info = $this->language->_('CHANGES MINIMUM AMMOUNT CURRENCY');
				
				//DELETE DATA TEMP
				$where = array('CHANGES_ID = ?' => $changes_id);
				$this->_db->delete('TEMP_MINAMT_CCY',$where);
				//UPDATE STATUS GLOBAL CHANGES
				$this->updateGlobalChanges($changes_id,$info);
				
				foreach($result as $row){	
						$c = 'c'.$row['CCY_ID'];
						$t = 't'.$row['CCY_ID'];
						$amt = Application_Helper_General::convertDisplayMoney($this->_getParam($t));
						$min_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($min_amt,'Digits')) ? true : false;
						$id = $row['CCY_ID'];
						$checkbox = ($this->_getParam($c)) ? "A" : "I";						
						if($cek_angka || $min_amt == '')
						{				
							//KALO DICENTANG
							if ($checkbox == "A")
							{
								if($this->_getParam($t)!= "0.00" && $this->_getParam($t)!= "" && $min_amt <= 999999999999999 && $min_amt > 0 )
								{								
//								    $change_id = "1";
									$data= array('CHANGES_ID' => $changes_id,
													'CCY_ID' => $row['CCY_ID'],
													'CCY_NUM' => $row['CCY_NUM'],
													'DESCRIPTION' => $row['CCY_DESCRIPTION'],
													'MIN_TX_AMT' => $amt,
													);
									$this->_db->insert('TEMP_MINAMT_CCY',$data);
									$temp+=1;

									$xxx = 'x'.$id;
									$centang = 'centang'.$id;
									$this->view->$centang = $checkbox;
									
									$tamt = 't'.$id;
									$this->view->$tamt = $amt;										
								}
								ELSE{
									$id = $row['CCY_ID'];						
									$docErr = $this->language->_('Error in processing form values. Please correct values and re-submit.');
									$this->view->report_msg = $docErr;

									$xxx = 'x'.$id;
									$centang = 'centang'.$id;
									$this->view->$centang = $checkbox;

									$tamt = 't'.$id;
									$this->view->$tamt = $amt;

									if($this->_getParam($t) <= 0.00)
										$err = $this->language->_('Invalid Amount. Amount must be greater than 0.00');
									else if($min_amt >= 999999999999999)
										$err =$this->language->_('Invalid Amount. Amount must be lower than 9,999,999,999,999.99');
									else
										$err = $this->language->_('Invalid Amount. Amount must be numeric');

									$this->view->$xxx = $err;
									$error = true;
								}
							}	
							// KALO TIDAK DICENTANG
							// KALO TIDAK DICENTANG
								ELSE IF ($checkbox=="I")
								{
									IF( $this->_getParam($t)== "0.00" || $this->_getParam($t)== "" )
									{
									// TIDAK DICENTANG DAN VALUE KOSONG //
										$xxx = 'x'.$id;
										$centang = 'centang'.$id;
										$this->view->$centang = $checkbox;

										$tamt = 't'.$id;
										$this->view->$tamt = $amt;
									}
									ELSE IF($this->_getParam($t)!= "0.00" && $this->_getParam($t)!= "" && $min_amt <= 999999999999999 && $min_amt > 0 ){
									// TIDAK DICENTANG DAN VALUE BENER //
										$id = $row['CCY_ID'];						
										$docErr = $this->language->_('Error in processing form values. Please correct values and re-submit.');
										$this->view->report_msg = $docErr;
										
										$xxx = 'x'.$id;
										$centang = 'centang'.$id;
										$this->view->$centang = $checkbox;
										
										$tamt = 't'.$id;
										$this->view->$tamt = $amt;
										
										$err = 'Please check selection';
										$this->view->$xxx = $err;
										$error = true;
									}
									ELSE
									{
										$id = $row['CCY_ID'];						
										$docErr = $this->language->_('Error in processing form values. Please correct values and re-submit.');
										$this->view->report_msg = $docErr;
										
										$xxx = 'x'.$id;
										$centang = 'centang'.$id;
										$this->view->$centang = $checkbox;
										
										$tamt = 't'.$id;
										$this->view->$tamt = $amt;
										
										if($min_amt <= 0)
											$err = $this->language->_('Invalid Amount. Amount must be greater than 0.00');
										else if($min_amt > 999999999999999)
											$err = $this->language->_('Invalid Amount. Amount must be lower than 9,999,999,999,999.99');
										else
											$err = $this->language->_('Invalid Amount. Amount must be numeric');

										$this->view->$xxx = $err;
										$error = true;
									}
								}
						}
						else
						{ 
							$id = $row['CCY_ID'];
						//	$msg[j] = $this->getErrorRemark('01',"$id Please correct values and re-submit.");
							$docErr = $this->language->_('Error in processing form values. Please correct values and re-submit.');
							$this->view->report_msg = $docErr;
							$xxx = 'x'.$id;
							$err = $this->language->_('Invalid Amount. Amount must be numeric');
							$this->view->$xxx = $err;
							
							$centang = 'centang'.$id;	
							$this->view->$centang = $checkbox;	
							
							$error = true;
						}
					}
					if(!$error && $temp > 0){
						Application_Helper_General::writeLog('CRCR','Submitting Repair Minimum Amount and Currency Available');
						$this->_db->commit();
				    	$this->view->success = true;						    
						$msg = 'Currency Change Request Saved';					
						$this->view->report_msg = $msg;
					}
					else if($error)
					{
						$this->view->error = true;
					}
				}
				catch(Exception $e)
				{
						$this->_db->rollBack();					
				}
			}
			else {
				foreach($result as $row){	
					$c = 'c'.$row['CCY_ID'];
					$t = 't'.$row['CCY_ID'];
					$id = $row['CCY_ID'];								
					
					$amt = Application_Helper_General::convertDisplayMoney($this->_getParam($t));
					$checkbox = ($this->_getParam($c)) ? "A" : "I";
					
					$centang = 'centang'.$id;
					$this->view->$centang = $checkbox;
					
					$tamt = 't'.$id;
					$this->view->$tamt = $amt;	
				}	
		
				$this->view->error = true;
				$docErr = ($this->language->_('No changes allowed for this record while awaiting approval for previous change.'));	
				$this->view->report_msg = $docErr;
			}
						
		}
		$this->view->result = $result;
		$this->view->fields = $fields;
		if(!$this->_request->isPost())
			Application_Helper_General::writeLog('CRCR','Viewing Repair Minimum Amount and Currency Available');
	}
	
}