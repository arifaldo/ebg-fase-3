<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
//SUSPENDED

class user_DeactivateController extends user_Model_User
{

  public function indexAction() 
  {
  	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));
//    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;

    if($user_id)
    {
		$select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(CUST_STATUS)!=3');
		$cust_name = $this->_db->fetchOne($select);

		$select = $this->_db->select()
                             ->from('M_USER')
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(USER_STATUS)=1');
		$user_data = $this->_db->fetchRow($select);
		if($user_data['USER_ID'])
		{
			$select = $this->_db->select()
								   ->from('TEMP_USER',array('TEMP_ID'))
								   ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
								   ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
			$result = $this->_db->fetchOne($select);
			
			if(empty($result))
			{
			  $info = 'User ID = '.$user_id.', User Name = '.$user_data['USER_NAME'];
			  
			  $user_data['USER_STATUS']       = 2;
			  $user_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
			  $user_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
			  
				try 
				{
					$this->_db->beginTransaction();
					$change_id = $this->suggestionWaitingApproval('User Account',$info,strtoupper($this->_changeType['code']['suspend']),null,'M_USER','TEMP_USER',$user_id,$user_data['USER_FULLNAME'],$cust_id);
				
					$this->insertTempUser($change_id,$user_data);
				
					//log CRUD
					Application_Helper_General::writeLog('CSSP','User has been Suspended, User ID : '.$user_id. ' User Name : '.$user_data['USER_FULLNAME'].' Change id : '.$change_id);
					
					$this->_db->commit();
					//$this->setbackURL('/customer/view/index/cust_id/'.$cust_id);
					
					$this->_redirect('/notification/submited/index');
					//$this->_redirect('/notification/success/index');
				}
				catch(Exception $e) 
				{
					$this->_db->rollBack();
				}
			}
			else
			{ 
				 $err_msg = 'Invalid User ID'; 
			}
		}
		else{ $user_id = null; }
	}
    
	    //insert log
		Application_Helper_General::writeLog('CSSP','Suspend');

	}
}