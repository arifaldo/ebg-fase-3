<?php
class language_ChangeController extends Application_Main {
	
	public function indexAction(){
		$this->_helper->layout()->setLayout('newlayout');
		$model = new language_Model_Index();
		$sql = $model->getLang();
		$arrLang = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			foreach ($sql as $key => $val){
				$code = explode('_', $val['CODE']);
				$arrLang += array($code[0] => $this->language->_($val['DESC']));
			}
		}
		$this->view->arrLang 	= $arrLang;
		
		$ns = new Zend_Session_Namespace('language');
		
		if($this->_request->isPost()){
			$lang = $this->_request->getPost("lang");			
			if (!(empty($lang))){
				if (array_key_exists($lang, $arrLang)){

					$data['USER_LANG'] = $lang;
					$where['BUSER_ID = ?'] = $this->_userIdLogin;
					$this->_db->update('M_BUSER',$data,$where);
					
					$ns->langCode = $lang;
				}
			}
			
			$this->_redirect('language/change');
		}
		
		$this->view->lang		= $ns->langCode;
		Application_Helper_General::writeLog('CLAN','Change Language');	
	}
}
