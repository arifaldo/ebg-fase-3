<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class reconadapter_ViewController extends Application_Main {

	public function indexAction()
    { 
    	$this->_helper->_layout->setLayout('newlayout');

        $profileid = $this->_request->getParam('profileid');

        $selectData = $this->_db->select()
                     ->from(array('H' => 'M_RECON_ADAPTER'), '*')
                     ->join(array('D' => 'M_RECON_ADAPTER_DETAIL'), 'H.ADAPTER_ID = D.ADAPTER_ID', '*')
                     ->where('H.ADAPTER_ID = '.$this->_db->quote($profileid));

        $dataAdapter = $this->_db->fetchAll($selectData);

        $statementFileName = $dataAdapter[0]['STATEMENT_FILE_PATH'];
        $reportFileName = $dataAdapter[0]['REPORT_FILE_PATH'];
       
        $statementData = $this->_helper->parser->parseCSV($statementFileName);
        $reportData = $this->_helper->parser->parseCSV($reportFileName);

        $statementStartRow = $dataAdapter[0]['STATEMENT_START_FROM_ROW'];
        $reportStartRow = $dataAdapter[0]['REPORT_START_FROM_ROW'];

        $statementHeaderData = $statementData[$statementStartRow-1];
        $reportHeaderData = $reportData[$reportStartRow-1];

        if(!empty($statementData) && !empty($reportData))
        {    

             //--------------unset data from above start row---------------------
            if (!empty($statementStartRow)) {
                for($i=0; $i<$statementStartRow;$i++){
                    unset($statementData[$i]);
                }
            }

            if (!empty($reportStartRow)) {
                for($i=0; $i<$reportStartRow;$i++){
                    unset($reportData[$i]);
                }
            }

            //---------get rid of empty spaces between column on statement data---------
            $statementEmptySpaces = array();
            foreach ($statementHeaderData as $key => $value) {
                if (empty($value)) {
                    array_push($statementEmptySpaces, $key);
                    unset($statementHeaderData[$key]);
                }
            }
            foreach ($statementData as $key => $value) {
                foreach ($statementEmptySpaces as $spaces) {
                    unset($statementData[$key][$spaces]);
                }
            }

            //---------get rid of empty spaces between column on report data---------
            $reportEmptySpaces = array();
            foreach ($reportHeaderData as $key => $value) {
                if (empty($value)) {
                    array_push($reportEmptySpaces, $key);
                    unset($reportHeaderData[$key]);
                }
            }
            foreach ($reportData as $key => $value) {
                foreach ($reportEmptySpaces as $spaces) {
                    unset($reportData[$key][$spaces]);
                }
            }
            
            $this->view->statementHeaderData = $statementHeaderData;
            $this->view->reportHeaderData = $reportHeaderData;

            $this->view->statementData = $statementData;
            $this->view->reportData = $reportData;

            $this->view->dataAdapter = $dataAdapter;
        }
        else //kalo total record = 0
        {
            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
            $this->view->error      = true;
            $this->view->report_msg = $this->displayError($error_msg);
        }
    }  
}
