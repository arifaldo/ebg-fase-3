<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
class singlepayment_NewvirtualaccountController extends Application_Main
{
	public function initModel()
	{
		$this->model 					= new payment_Model_Payment();
		$this->param['SERVICE_TYPE'] 	= 16;
		$this->param['PROVIDER_TYPE'] 	= 1;
		$this->param['fetch'] 			= 'fetchAll';
		$this->param['USER_ID'] 		= $this->_userIdLogin;
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace 		= new Zend_Session_Namespace('virtualaccount');
		$paramSession 			= $sessionNamespace->paramSession;

		$this->view->radioCheck	= 1;
		$this->view->userIdLogin  = $this->_userIdLogin;

		/*$CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$AccArr 	  			= $CustomerUser->getAccounts();*/

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();

		$this->view->AccArr 	= $AccArr;
		$anyValue = '-- ' . $this->language->_('Any Value') . ' --';
		$arr 						= $this->model->getProviderId(16, 1);
		$providerArr 				= array('' => $anyValue);
		$providerArr 				+= Application_Helper_Array::listArray($arr, 'PROVIDER_ID', 'PROVIDER_NAME');

		$this->view->providerArr 	= $providerArr;

		$list 						= $this->model->cekListVA($this->param);
		$this->view->paymentList 	= $list;


		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();
		$this->view->ccyArr 			= $ccyList;

		//added token
		$userData = $this->_db->select()
			->from(array('M_USER'), array('USER_ID', 'TOKEN_TYPE', 'TOKEN_ID', 'USER_MOBILE_PHONE'))
			->where('USER_ID = ?', $this->_userIdLogin)
			->limit(1);
		$userData = $this->_db->fetchRow($userData);

		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);

		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);

			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		$this->view->googleauth = true;

		$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;

		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();
		$this->view->challengeCode = $challengeCode;
		$this->view->challengeCodeReq = $challengeCode;
		// print_r($this->_request->getParams());die;

		if ($this->_getParam('submitBtn') == TRUE) {
			
			$acctsrc = $this->_getParam('ACCTSRC');
			$radioCheck = $this->_getParam('radioCheck');

			if ($radioCheck == 1) {
				$vaNumber 			= $this->_getParam('orderId');
				$this->view->orderId		= $vaNumber;
			} else if ($radioCheck == 2) {
				$vaNumber			= $this->_getParam('novirtual');
				$this->view->novirtual		= $vaNumber;
			}

			if (empty($acctsrc)) {
				$error = true;
				$error_msg[] = 'Source Account cannot be left blank';
			}
			if (empty($vaNumber)) {
				$error = true;
				$error_msg[] = 'VA Number cannot be left blank';
			}

			//if tidak ada error
			if (!$error) {
				//get data from T_VA_TRANSACTION
				$select = $this->_db->select()
					->from(array('T' => 'T_VA_TRANSACTION'), array('*'))
					// ->join(array('C' => 'T_VA_TRANSACTION_CREDIT'), 'T.VA_NUMBER = C.VA_NUMBER', array('C.AVAILABLE_CREDIT', 'C.CREDIT_SETTLEMENT'))
					->join(array('B' => 'M_CUSTOMER_BIN'), 'T.CUST_BIN = B.CUST_BIN', array('B.*'))
					->join(array('C' => 'M_CUSTOMER'), 'T.CUST_ID = C.CUST_ID', array('C.CUST_NAME'))
					->where('T.DOC_STATUS = 0')
					->where('T.CUST_ID = ?', $this->_custIdLogin)
					->where('B.CUST_ID = ?', $this->_custIdLogin)
					->where('T.EXP_DATE > ?', new Zend_Db_Expr("now()"))
					->where('CONCAT(T.CUST_BIN, T.VA_NUMBER) = ?', $vaNumber)
					->ORwhere('T.VA_NUMBER = ?', $vaNumber)
					->where('T.DOC_STATUS = 0')
					->where('T.EXP_DATE > ?', new Zend_Db_Expr("now()"));

				$vaData = $this->_db->fetchAll($select);

				if (empty($vaData)) {
					$error = true;
					$error_msg[] = 'VA Number not exists or settled or expired, please check your virual account number';
					$this->view->ERROR_MSG = $error_msg;
				}
				else{

					//if billing type FIFO
					if ($vaData[0]['BILLING_TYPE'] == 1) {
						$newVaData[0] = $vaData[0];
						$vaData = $newVaData;
					}
					// LIFO
					else if($vaData[0]['BILLING_TYPE'] == 2){
						$newVaData[0] = $vaData[count($vaData)-1];
						$vaData = $newVaData;
					}

					$paramSession['ACCTSRC']		= $acctsrc;
					$paramSession['VANUMBER']		= $vaNumber;
					$paramSession['VADATA']			= $vaData;
					$sessionNamespace->paramSession = $paramSession;
					$this->_redirect('/singlepayment/newvirtualaccount/next');
				}
			}else{
				$this->view->ERROR_MSG = $error_msg;
			}
			
		} else if ($this->_getParam('submit') == true) {
			$this->cancel();
		} else {
			unset($_SESSION['virtualaccount']);
		}
		Application_Helper_General::writeLog('CRVA', 'Viewing Create Single Payment Virtual Account');
	}

	public function nextAction()
	{
		$this->view->userId	= $this->_userIdLogin;
		$SinglePayment 		= new SinglePayment(null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('virtualaccount');
		$paramSession 		= $sessionNamespace->paramSession;

		$vaData 			= $paramSession['VADATA'];

		$totalAmount = 0;
		$totalInBilling = 0;
		foreach ($vaData as $key => $value) {
			$vaData[$key]['SOURCE_ACCOUNT'] = $paramSession['ACCTSRC'];
			$totalAmount += $value['IN_BILLING'];
			$totalInBilling += $value['IN_BILLING'];
		}

		$this->view->vaData = $vaData;

		//if static and partial payment = over then query available credit
		$vaCreditData = 0;
		if ($vaData[0]['VA_TYPE'] != '0' && $vaData[0]['PARTIAL_TYPE'] == '2') {
			$select = $this->_db->select()
					->from(array('C' => 'T_VA_TRANSACTION_CREDIT'), array('C.AVAILABLE_CREDIT', 'C.CREDIT_SETTLEMENT'))
					->where('C.CUST_ID = ?', $this->_custIdLogin)
					->where('CONCAT(C.CUST_BIN, C.VA_NUMBER) = ?', $vaData[0]['CUST_BIN'].$vaData[0]['VA_NUMBER']);

			$vaCreditData = $this->_db->fetchRow($select);

			$totalAmount -= $vaCreditData['AVAILABLE_CREDIT'];
		}

		$this->view->totalAmount = $totalAmount < 0 ? $totalInBilling : $totalAmount;
		$this->view->vaCreditData = $vaCreditData;

		// echo "<pre>";
		// print_r($totalAmount);die();

		//table
		$fields = array(
			'va_id' => array(
				'field'    => 'VA_ID',
				'label'    => $this->language->_('VA ID'),
				'sortable' => false
			),

			'desc'     => array(
				'field'    => 'DESC',
				'label'    => $this->language->_('Description'),
				'sortable' => false
			),

			'ref_number'   => array(
				'field'    => 'REF_NUMBER',
				'label'    => $this->language->_('Ref Number'),
				'sortable' => false
			),

			'exp_time'     => array(
				'field'    => 'EXP_TIME',
				'label'    => $this->language->_('Expired Time'),
				'sortable' => false
			),

			'ccy'    => array(
				'field'  => 'CCY',
				'label'    => $this->language->_('CCY'),
				'sortable' => false
			),
			'amount'    => array(
				'field'  => 'AMOUNT',
				'label'    => $this->language->_('Amount'),
				'sortable' => false
			),
			'charge'    => array(
				'field'  => 'CHARGE',
				'label'    => $this->language->_('Charge'),
				'sortable' => false
			)
		);

		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir, 'InArray', array('transaction_time' => array('asc', 'desc')))) ? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->fields = $fields;
		//

		// add hamdan
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO' => $paramSession['ACCTSRC']));
		// echo "<pre>";
		// var_dump($detailCustomer);
		// die;
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		// end hamdan

		//added token
		$userData = $this->_db->select()
			->from(array('M_USER'), array('USER_ID', 'TOKEN_TYPE', 'TOKEN_ID', 'USER_MOBILE_PHONE'))
			->where('USER_ID = ?', $this->_userIdLogin)
			->limit(1);
		$userData = $this->_db->fetchRow($userData);
		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);

		//add hardtoken
		//$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);

			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$this->view->hidetoken = true;
			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}
		//Approval matirx
		$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;
		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;
		//////

		if ($this->_getParam('submit') == TRUE) {

			$amount = $this->_getParam('amount');

			if ($vaData[0]['BILLING_TYPE'] == '3') { //if billing type = selected

				if ($vaData[0]['PARTIAL_TYPE'] == '1') { //if partial type = no then checkbox
					$check = $this->_getParam('check2');
				}
				else{ //if partial type = yes/over then radiobutton
					$check = $this->_getParam('radio2');
					$amount = Application_Helper_General::convertDisplayMoney($this->_getParam('amounttopay'));
				}

				//if none selected, then throw error
				if (empty($check)) {
					$error = true;
					$error_msg[] = 'Please select a virtual account to pay';
					$this->view->ERROR_MSG = $error_msg;
				}
				else{
					foreach ($check as $value) {
						$newVaData[] = $vaData[$value]; 
					}
				}

				// if available credit lebih besar drpd in billing ambil amount dari yg input hidden yg khusus
				if ($vaCreditData['AVAILABLE_CREDIT'] > $newVaData[0]['IN_BILLING']) {
					$amount = Application_Helper_General::convertDisplayMoney($this->_getParam('amounttopayoverselected'));
				}

				if ($newVaData[0]['PARTIAL_TYPE'] == '0') {
					if ($amount > $newVaData[0]['IN_BILLING']) {
						$error = true;
						$error_msg[] = 'Amount to pay cannot be over than total in billing';
						$this->view->ERROR_MSG = $error_msg;
					}
				}

				$paramSession['AMOUNTTOPAY'] = $amount;
				$paramSession['CONFIRMVADATA'] = $newVaData;
			}
			else if ($vaData[0]['PARTIAL_TYPE'] == '0' || $vaData[0]['PARTIAL_TYPE'] == '2') { //if partial type = yes / over
				$amountToPay = Application_Helper_General::convertDisplayMoney($this->_getParam('amounttopay'));

				if ($vaData[0]['PARTIAL_TYPE'] == '0') { //if partial type = yes, then validasi amounttopay cannot be over
					if ($amountToPay > $totalAmount) {
						$error = true;
						$error_msg[] = 'Amount to pay cannot be over than total in billing';
						$this->view->ERROR_MSG = $error_msg;
					}
				}

				$this->view->amounttopay = $amountToPay;
				$paramSession['AMOUNTTOPAY'] = $amountToPay; //kalau yes/over amounttopay ambil sesuai inputan
				$paramSession['CONFIRMVADATA'] = $vaData;
			}
			else{
				$paramSession['AMOUNTTOPAY'] = $amount;
				$paramSession['CONFIRMVADATA'] = $vaData;
			}

			if (!$error) {
				$sessionNamespace->paramSession = $paramSession;
				$this->_redirect('/singlepayment/newvirtualaccount/confirm');
			}
			


		} else if ($this->_getParam('submit1') == true) {
			$this->_redirect('/singlepayment/newvirtualaccount/index');
		}

		$this->view->paramSession		= $paramSession;
	}

	public function confirmAction()
	{
		$sessionNamespace 					= new Zend_Session_Namespace('virtualaccount');
		$paramSession 						= $sessionNamespace->paramSession;
		$vaData 							= $paramSession['CONFIRMVADATA'];
		$amountToPay 						= $paramSession['AMOUNTTOPAY'];


		$this->view->vaData = $vaData;
		$this->view->amountToPay = $amountToPay;

		$totalAmount = 0;
		$totalInBilling = 0;
		foreach ($vaData as $key => $value) {
			$totalAmount += $value['IN_BILLING'];
			$totalInBilling += $value['IN_BILLING'];
		}

		//if static and partial payment = over then query available credit
		$vaCreditData = 0;
		if ($vaData[0]['VA_TYPE'] != '0' && $vaData[0]['PARTIAL_TYPE'] == '2') {
			$select = $this->_db->select()
					->from(array('C' => 'T_VA_TRANSACTION_CREDIT'), array('C.AVAILABLE_CREDIT', 'C.CREDIT_SETTLEMENT'))
					->where('C.CUST_ID = ?', $this->_custIdLogin)
					->where('CONCAT(C.CUST_BIN, C.VA_NUMBER) = ?', $vaData[0]['CUST_BIN'].$vaData[0]['VA_NUMBER']);

			$vaCreditData = $this->_db->fetchRow($select);
			$totalAmount -= $vaCreditData['AVAILABLE_CREDIT'];
		}

		$this->view->totalAmount = $totalAmount < 0 ? $totalInBilling : $totalAmount;
		$this->view->vaCreditData = $vaCreditData;

		if ($this->_getParam('submit') == TRUE) {

			//validasi source account (enough saldo, registered, dll)
			$validateACCTSRC = new ValidateAccountSource($paramSession['ACCTSRC'], $this->_custIdLogin, $this->_userIdLogin);

			$check 				= $validateACCTSRC->check($amountToPay);
			if ($check == true) {
				
//TO DO: pakai variabel $amountToPay, utk develop bill method over / yes, amountToPay variabel yang nampung pas user input nominal amount saat bill method over / yes
				foreach ($vaData as $key => $value) {

					$totalInBillingWCharges = $value['IN_BILLING'] + $value['BUYER_FEE'];

					if ($value['PARTIAL_TYPE'] == '1') { //if partial type = no
						$vaData[$key]['AMOUNT'] = $totalInBillingWCharges;
					}
					else if($value['PARTIAL_TYPE'] == '2'){ //over

						$inBillingWAvailableCredit = $value['IN_BILLING'] - $vaCreditData['AVAILABLE_CREDIT'];

						//if available credit lebih besar (bs cover total inbilling), maka bayar pakai available credit
						if ($vaCreditData['AVAILABLE_CREDIT'] > $value['IN_BILLING']) { 
							$vaData[$key]['AMOUNTCREDIT'] = $vaCreditData['AVAILABLE_CREDIT'] - $amountToPay;
							$vaData[$key]['AVAILABLECREDITUSED'] = $value['IN_BILLING'];
							$vaData[$key]['AMOUNT'] = 0; //karna sudah pakai available credit, jadi nnti cuma bayar fee charges saja
						}
						//if available credit lebih kecil (tdk bs cover in billing) dan msih over, maka hitung amount credit leftnya
						else if ($vaCreditData['AVAILABLE_CREDIT'] < $value['IN_BILLING'] && $amountToPay > $inBillingWAvailableCredit) {
							$vaData[$key]['AMOUNTCREDIT'] = $amountToPay - $inBillingWAvailableCredit;
							$vaData[$key]['AVAILABLECREDITUSED'] = $vaCreditData['AVAILABLE_CREDIT'];
							$vaData[$key]['AMOUNT'] = $amountToPay;
						}
						//if available credit lebih kecil (tdk bs cover in billing), maka potong in billing dengan available credit
						else if($vaCreditData['AVAILABLE_CREDIT'] < $value['IN_BILLING'] && $amountToPay <= $inBillingWAvailableCredit){
							$vaData[$key]['AMOUNTCREDIT'] = 0;
							$vaData[$key]['AVAILABLECREDITUSED'] = $vaCreditData['AVAILABLE_CREDIT'];
							$vaData[$key]['AMOUNT'] = $amountToPay;
						}
					}
					else{
						$vaData[$key]['AMOUNT'] = $amountToPay;
					}
				}

				// echo "<pre>";
				// print_r($vaData);die();

				//baru handle yg partial = no bill = total
				$SinglePay 	= new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
				$trx = $SinglePay->createPaymentVA($vaData);

				if ($trx) {
					$this->setbackURL('/singlepayment/newvirtualaccount');
					$this->_redirect('/notification/success/index');
				}
				else{
					$error_msg[] = 'Failed to Insert Data';
					$this->view->ERROR_MSG = $errMsg;
				}
			}
			else{
				$error				= true;
				$errMsg 			= $validateACCTSRC->getErrorMsg();
				$this->view->ERROR_MSG = $errMsg;
			}
		}

		$sessionNamespace->paramSession 	= $paramSession;
		$this->view->paramSession			= $paramSession;

		// add hamdan
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO' => $paramSession['ACCTSRC']));
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		$this->view->PS_NUMBER = $paramSession['PS_NUMBER'];
	}

	public function cancel()
	{
		unset($_SESSION['virtualaccount']);
		$this->_redirect("/home/index");
	}
}
