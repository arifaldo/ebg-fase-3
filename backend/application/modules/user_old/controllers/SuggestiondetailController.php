<?php
require_once 'Zend/Controller/Action.php';

class user_SuggestiondetailController extends user_Model_User{

  public function indexAction()
  {
    $change_id = $this->_getParam('changes_id');
    $change_id = (Zend_Validate::is($change_id,'Digits'))? $change_id : 0;
    $this->_helper->layout()->setLayout('newpopup');

    $this->view->suggestionType = $this->_suggestType;

    if($change_id)
    {
      $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID'))
                             ->where('CHANGES_ID='.$this->_db->quote($change_id))
                             // ->where("CHANGES_FLAG='B'")
                             ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
//                             ->where('UPPER(MODULE)='.$this->_db->quote(strtoupper($this->_request->getModuleName())));
      $result = $this->_db->fetchOne($select);

      if(empty($result))  $this->_redirect('/notification/invalid/index');


      if($result)
      {
        //content send to view
        $select = $this->_db->select()
                               ->from(array('T'=>'TEMP_USER'))
                               ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
                               ->where('T.CHANGES_ID = ?', $change_id);
        $resultdata = $this->_db->fetchRow($select);

        if($result['CHANGES_ID'])
        {
            //----------------------------------------------TEMP DATA--------------------------------------------------------------
        $this->view->changes_id     = $resultdata['CHANGES_ID'];
        $this->view->changes_type   = $resultdata['CHANGES_TYPE'];
        $this->view->changes_status = $resultdata['CHANGES_STATUS'];
        $this->view->read_status    = $resultdata['READ_STATUS'];
        $this->view->created        = $resultdata['CREATED'];
        $this->view->created_by     = $resultdata['CREATED_BY'];


           $cust_id = strtoupper($resultdata['CUST_ID']);
           $user_id = strtoupper($resultdata['USER_ID']);

           if($cust_id)
           {
              $select = $this->_db->select()
                                   ->from('M_CUSTOMER',array('CUST_NAME'))
                                   ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
              $this->view->cust_name = $this->_db->fetchOne($select);
           }

           $this->view->user_id    = $resultdata['USER_ID'];
           $this->view->user_name  = $resultdata['USER_FULLNAME'];
           $this->view->user_email = $resultdata['USER_EMAIL'];
           $this->view->user_phone = $resultdata['USER_PHONE'];
           $this->view->token_id   = $resultdata['TOKEN_ID'];
           $this->view->user_ext   = $resultdata['USER_EXT'];


          $this->view->user_status  = $resultdata['USER_STATUS'];
          $this->view->user_islogin = $resultdata['USER_ISLOGIN'];
          $this->view->user_failed_attempt =  $resultdata['USER_FAILEDATTEMPT'];
          $this->view->user_is_login  = $resultdata['USER_ISLOGIN'];
          $this->view->user_is_locked = $resultdata['USER_ISLOCKED'];

          $this->view->user_lastlogin   = $resultdata['USER_LASTLOGIN'];
          $this->view->user_created     = $resultdata['USER_CREATED'];
          $this->view->user_createdby   = $resultdata['USER_CREATEDBY'];
          $this->view->user_suggested   = $resultdata['USER_SUGGESTED'];
          $this->view->user_suggestedby = $resultdata['USER_SUGGESTEDBY'];
          $this->view->user_updated     = $resultdata['USER_UPDATED'];
          $this->view->user_updatedby   = $resultdata['USER_UPDATEDBY'];

           //--------------- START TEMP USER DEBIT CARD NUMBER ----------------------------
                    $debit  = $this->_db->select()
                    ->from('TEMP_USER_DEBIT')
                    ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
                    ->where('CHANGES_ID='.$change_id)
                    ->query()->fetchAll();  		
          $debitarr = '';
          if(!empty($debit)){

          foreach($debit as $kval => $dval){
          if(empty($debit[$kval+1]['USER_DEBITNUMBER'])){
          $debitarr .= $dval['USER_DEBITNUMBER'];
          }else{
          $debitarr .= $dval['USER_DEBITNUMBER'].';';
          }
          }
          }		
          $this->view->debitarr = $debitarr;
          //----------------END TEMP USER DEBIT CARD NUMBER -----------------------------

          if($resultdata['USER_ISEMAIL']) $userIsEmail = $this->language->_('Yes');
        else $userIsEmail = $this->language->_('No');
          $this->view->userIsEmail = $userIsEmail;


          //privilege
          $this->view->priviAll =  $this->view->priviAll = Application_Helper_Array::listArray($this->getPrivilege(),'FPRIVI_ID','FPRIVI_DESC');

          $fuserId    = $cust_id . $user_id;

          //jika type new dan edit maka data privilege ambil dari tabel temp
          if($this->view->changes_type == 'N' || $this->view->changes_type == 'E')
          {
        
            $priviList  = $this->_db->select()
                                  ->from('TEMP_FPRIVI_USER')
                                  ->where('UPPER(FUSER_ID)='.$this->_db->quote((string)$fuserId))
                                  ->query()->fetchAll();

            $tempmaker  = $this->_db->select()
                  ->from(array('T'=>'TEMP_MAKERLIMIT'))
                  ->join(array('G' => 'TEMP_CUSTOMER_ACCT'), 'G.ACCT_NO = T.ACCT_NO AND G.CHANGES_ID = T.CHANGES_ID', array('T.*','G.*'))
                                  //->from('TEMP_MAKERLIMIT')
                                  ->where('UPPER(T.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                  //echo $tempmaker;die;
                                  ->query()->fetchAll();                                  


            $tempdaily  = $this->_db->select()
                                  ->from('TEMP_DAILYLIMIT')
                                  ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                                  ->query()->fetchAll();                                  
          }
          //jika type data suspend, unsuspend, dan delete maka data privilege diambil dari tabel Master
          else
          {
              $priviList  = $this->_db->select()
                                  ->from('M_FPRIVI_USER')
                                  ->where('UPPER(FUSER_ID)='.$this->_db->quote((string)$fuserId))
                                  ->query()->fetchAll();
          }


          $priviListArr = Application_Helper_Array::simpleArray($priviList,'FPRIVI_ID');
            $this->view->priviList = $priviListArr;
      
      //var_dump($tempdaily);
      //var_dump($tempmaker);die;
      
            if(!empty($tempmaker)){
              $this->view->tempmaker = $tempmaker;
            }

            if(!empty($tempdaily)){
              $this->view->tempdaily = $tempdaily;
            }

          //cek apakah list template sesuai list privilege yang dipilih
            $priviTemplate = array();
            foreach($this->getPriviTemplate() as $row)
            {
               $priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
            }


            $flag_template = 0;
            $user_role     = '';
            $template_id   = '';
            if(count($priviTemplate) > 0)
          {
        //echo '<pre>';
        //var_dump($priviTemplate);
        //var_dump($priviListArr);
              foreach($priviTemplate as $key => $row)
              {
                  $count_row = count($row);
                  $count_priviListArr = count($priviListArr);
                  if($count_row == $count_priviListArr)
                  {
                      $arr_diff = array_diff_key(array_flip($priviListArr),$row);
            //echo '<pre>';
            //var_dump($row);
            // var_dump($arr_diff);
            //die;  
                      if(count($arr_diff) == 0)
                      {
                          $template_id = $key;
                          $flag_template = 1;
                          break;
                      }
                  }
              }
          }
     // die;
  //var_dump($flag_template);die;
          if($flag_template == 0) $user_role = $this->language->_('Custom');
          else if($flag_template == 1)
          {
              $select = $this->_db->select()
                               ->from('M_FTEMPLATE')
                               ->where('UPPER(FTEMPLATE_ID)='.$this->_db->quote($template_id))
                               ->query()->fetch();
              $user_role = $select['FTEMPLATE_DESC'];
          }

          $this->view->user_role = $user_role;

          //END privilege
        //END content send to view
        //----------------------------------------------END TEMP DATA--------------------------------------------------------------

       


        //----------------------------------------------MASTER DATA--------------------------------------------------------------
           $m_resultdata = $this->getUserData($cust_id,$user_id);

           $this->view->m_user_id    = $m_resultdata['USER_ID'];
           $this->view->m_user_name  = $m_resultdata['USER_FULLNAME'];
           $this->view->m_user_email = $m_resultdata['USER_EMAIL'];
           $this->view->m_user_phone = $m_resultdata['USER_PHONE'];
           $this->view->m_token_id   = $m_resultdata['TOKEN_ID'];
           $this->view->m_user_ext   = $m_resultdata['USER_EXT'];

          $this->view->m_user_status         = $m_resultdata['USER_STATUS'];
          $this->view->m_user_islogin        = $m_resultdata['USER_ISLOGIN'];
          $this->view->m_user_failed_attempt = $m_resultdata['USER_FAILEDATTEMPT'];
          $this->view->m_user_is_login       = $m_resultdata['USER_ISLOGIN'];
          $this->view->m_user_is_locked      = $m_resultdata['USER_ISLOCKED'];

          $this->view->m_user_lastlogin   = $m_resultdata['USER_LASTLOGIN'];
          $this->view->m_user_created     = $m_resultdata['USER_CREATED'];
          $this->view->m_user_createdby   = $m_resultdata['USER_CREATEDBY'];
          $this->view->m_user_suggested   = $m_resultdata['USER_SUGGESTED'];
          $this->view->m_user_suggestedby = $m_resultdata['USER_SUGGESTEDBY'];
          $this->view->m_user_updated     = $m_resultdata['USER_UPDATED'];
          $this->view->m_user_updatedby   = $m_resultdata['USER_UPDATEDBY'];



          if($m_resultdata['USER_ISEMAIL']) $m_userIsEmail = $this->language->_('Yes');
        else $m_userIsEmail = $this->language->_('No');
          $this->view->m_userIsEmail = $m_userIsEmail;


          //privilege
          $fuserId    = $cust_id . $user_id;
          $priviList  = $this->_db->select()
                                ->from('M_FPRIVI_USER')
                                ->where('UPPER(FUSER_ID)='.$this->_db->quote((string)$fuserId))
                                ->query()->fetchAll();

          $priviListArr = Application_Helper_Array::simpleArray($priviList,'FPRIVI_ID');
            $this->view->m_priviList = $priviListArr;


          //cek apakah list template sesuai list privilege yang dipilih
            $flag_template = 0;
            $user_role     = '';
            $template_id   = '';
            if(count($priviTemplate) > 0)
          {
              foreach($priviTemplate as $key => $row)
              {
                  $count_row = count($row);
                  $count_priviListArr = count($priviListArr);
                  if($count_row == $count_priviListArr)
                  {
                      $arr_diff = array_diff_key(array_flip($priviListArr),$row);
                      if(count($arr_diff) == 0)
                      {
                          $template_id = $key;
                          $flag_template = 1;
                          break;
                      }
                  }
              }
          }

          if($flag_template == 0) $user_role = $this->language->_('Custom');
          else if($flag_template == 1)
          {
              $select = $this->_db->select()
                               ->from('M_FTEMPLATE')
                               ->where('UPPER(FTEMPLATE_ID)='.$this->_db->quote($template_id))
                               ->query()->fetch();
              $user_role = $select['FTEMPLATE_DESC'];
          }

          $this->view->m_user_role = $user_role;

          //END privilege
          //---------------------------------------------END MASTER DATA--------------------------------------------------------------

          //--------------- START USER DEBIT CARD NUMBER ----------------------------
          $m_debit  = $this->_db->select()
                                  ->from('M_USER_DEBIT')
                                  ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
								  ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                                  ->query()->fetchAll();  		
          $m_debitarr = '';
          if(!empty($m_debit)){
            
            foreach($m_debit as $kval => $dval){
              if(empty($m_debit[$kval+1]['USER_DEBITNUMBER'])){
                $m_debitarr .= $dval['USER_DEBITNUMBER'];
              }else{
                $m_debitarr .= $dval['USER_DEBITNUMBER'].';';
              }
            }
          }		
          $this->view->m_debitarr = $m_debitarr;
          //----------------END USER DEBIT CARD NUMBER -----------------------------

          $this->view->status_type = $this->_masterglobalstatus;




        }
        else{ $change_id = 0; }
      }
      else{ $change_id = 0; }
    }





    if(!$change_id)
    {
      $error_remark = $this->language->_('Changes Id is not found');
      $this->_redirect($this->_backURL);
      $this->_redirect('/popuperror/index/index');
    }

    //insert log
  try
  {
    $this->_db->beginTransaction();
    $fulldesc = 'CHANGES_ID:'.$change_id;
    if(!$this->_request->isPost()){
    Application_Helper_General::writeLog('CSCL','View Frontend User Changes List');
    }

    $this->_db->commit();
  }
    catch(Exception $e){
    $this->_db->rollBack();
  }

    $this->view->changes_id = $change_id;
  }
}
