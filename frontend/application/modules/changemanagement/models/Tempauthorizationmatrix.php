<?php
/**
 * Temprootcommunity model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempauthorizationmatrix extends Changemanagement_Model_Tempchanges {
	protected $_moduleId = 'AUM';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		/*$select = $this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);	*/			 
		
		$boundary = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_APP_BOUNDARY')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
		$custid = $boundary[0]['CUST_ID'];
		//Zend_Debug::dump($boundary);
		
		if($boundary)
		{
			$whereArr2 = array('GROUP_USER_ID LIKE '.$this->dbObj->quote('%'.$custid.'%'));
			$delete2 = $this->dbObj->delete('M_APP_BOUNDARY_GROUP',$whereArr2);
			$whereArr1 = array('CUST_ID = ?'=>$custid);
			$delete1 = $this->dbObj->delete('M_APP_BOUNDARY',$whereArr1);
			
			//Zend_Debug::dump($delete2.' '.$delete1.' '.$custid);die;
			foreach($boundary as $list)
			{
				$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>'','BOUNDARY_ID'=>''));
	       		$insertArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
				$insertArr['CREATED'] = new Zend_Db_Expr('now()');
				$insertArr['CREATEDBY'] = $actor;
				$insert = $this->dbObj->insert('M_APP_BOUNDARY',$insertArr);
				
				$lastId = $this->dbObj->query('SELECT @@identity AS LASTID')->fetch(Zend_Db::FETCH_ASSOC);
				$last = $lastId['LASTID'];
				
				$group = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_APP_BOUNDARY_GROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->where('ROW_INDEX = ?',$list['ROW_INDEX'])
									 );
				
				foreach($group as $grouplist)
				{
					//Zend_Debug::dump($last);die;
					$groupinsertArr = array_diff_key($grouplist,array('TEMP_ID'=>'','CHANGES_ID'=>'','BOUNDARY_ID'=>''));
					$groupinsertArr['BOUNDARY_ID'] = $last;
					//Zend_Debug::dump($groupinsertArr);die;
					$groupinsert = $this->dbObj->insert('M_APP_BOUNDARY_GROUP',$groupinsertArr);
				}
			}
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate() {
		
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate() {
		
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit($actor = null) {

		$delete = $this->dbObj->delete('TEMP_APP_BOUNDARY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$groupdelete = $this->dbObj->delete('TEMP_APP_BOUNDARY_GROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteDelete() {
		//will never be called
	}
}