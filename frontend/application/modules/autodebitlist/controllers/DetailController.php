<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class autodebitlist_DetailController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $ad = $this->_getParam('acct');

        if(!empty($ad)){

            $adData = $this->_db->select()
                             ->from(array('A' => 'T_DIRECTDEBIT'),array('*'))
                             ->where('A.DEBIT_ACCT = ?',$ad)
                             ->query()->fetchAll();

            if(!empty($adData)){

                $data = $adData['0'];
                
                $this->view->CLIENT_REFF = $data['CLIENT_REFF'];
                $this->view->CLIENT_NAME = $data['CLIENT_NAME'];
                $this->view->DEBIT_BANK = $data['DEBIT_BANK'];
                $this->view->DEBIT_ACCT = $data['DEBIT_ACCT'];
                $this->view->DIR_CCY = $data['DIR_CCY'];
                $this->view->DOCUMENT_REFF = $data['DOCUMENT_REFF'];
                $this->view->DOC_ID = $data['DOC_ID'];

                // if canceled
                if($data['DIR_UD_CANCEL'] != ""){
                    $this->view->DIR_UD_CANCEL = $data['DIR_UD_CANCEL'];
                }

                 $arrStatus = array('1' => 'Approved',
                            '2' => 'Rejected',
                            '3' => 'Waiting Approval'
                          ); 
         
                $this->view->STATUS = $arrStatus[$data['DIR_STATUS']];
                $this->view->statuskey = $data['DIR_STATUS'];

                $maxFileSize = "1024"; //"1024000"; //1024
                //~ $fileExt = "csv,dbf";
                $fileExt = "jpeg,jpg,pdf";
                $reject             = $this->_getParam('reject');

                if($reject){
                    
                    $attahmentDestination   = UPLOAD_PATH . '/document/submit/';
                    $errorRemark            = null;
                    $adapter                = new Zend_File_Transfer_Adapter_Http();
                                        
                    $sourceFileName = trim($adapter->getFileName());
                    $datas = $this->_request->getParams();
                    
                    if($sourceFileName == null){
                        // die('here1');
                        $sourceFileName = null;
                        $fileType = null;
                    }
                    else
                    {

                        $sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
                        if($_FILES["uploadSource"]["type"])
                        {
                            $adapter->setDestination($attahmentDestination);
                            $fileType               = $adapter->getMimeType();
                            $size                   = $_FILES["document"]["size"];
                        }
                        else{
                            $fileType = null;
                            $size = null;
                        }
                    }
                    $paramsName['sourceFileName']   = $sourceFileName;
                    //print_r($sourceFileName);die;
                    $fileTypeMessage = explode('/',$fileType);
                    if (isset($fileTypeMessage[1])){
                        $fileType =  $fileTypeMessage[1];
                        $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                        $extensionValidator->setMessage("Extension file must be *.jpg /.jpeg / .pdf");

                        $size           = number_format($size);
                        $sizeTampil     = $size/1000;
                        //$sizeValidator    = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                        // $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");
                        
                        $adapter->setValidators(array($extensionValidator));

                        if($adapter->isValid())
                        {
                            $date = date("dmy");
                            $time = date("his");

                            $newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
                            $adapter->addFilter('Rename', $newFileName);

                            if($adapter->receive())
                            {
                                $filedata = $attahmentDestination.$newFileName;
                                // var_dump($filedata);
                                if (file_exists($filedata)) {
                                    exec("clamdscan --fdpass ".$filedata, $out ,$int);
                                        
                                    // if()
                                        // var_dump($int);
                                        // var_dump($out);
                                        // die;
                                    if($int == 1){
                                        // echo "aa";die;
                                        // unlink($filedata);
                                        // die('here');
                                        $this->view->error = true;
                                        $errors = array(array('FileType'=>'Error: An infected file. Please upload other file'));
                                        $this->view->errorMsg = $errors;


                                    }else{
                                        
                                        $datas['fileName'] = $newFileName;
                                        $datas['sourcefileName'] = $sourceFileName;                                 
                                        
                                        try{
                                            $this->_db->beginTransaction();
                                            $dataUpdate =  array(					
                                                'DIR_STATUS' 		=> '2',
                                                'DIR_APPROVED' 	=> new Zend_Db_Expr('now()'), 
                                                'DIR_APPROVEDBY' 	=> $this->_userIdLogin,
                                                'DIR_FILENAME' 	=> $datas['sourcefileName'],
                                                'DIR_SYS_FILENAME' 	=> $datas['fileName'],
                                                'DIR_UD_CANCEL' 	=> $this->generateTransactionID(),
                                                'DEBIT_NOTES' 	=> $datas['PS_REASON_REJECT'],
                                            );							

                                            $where = array('CLIENT_REFF = ?' => $data['CLIENT_REFF']);
                                            $result = $this->_db->update('T_DIRECTDEBIT',$dataUpdate,$where);
                                            $this->_db->commit();
                                        }catch(Exception $e){
                                            var_dump($e); die;
                                            $this->_db->rollBack();
                                        }
                                            
                                        $rowHistory = $this->_db->insert(
                                            'T_PSLIP_HISTORY',
                                            array(
                                                'PS_REASON' => 'Canceled',
                                                'USER_LOGIN' => $this->_userIdLogin,
                                                'CUST_ID' => $this->_custIdLogin,
                                                'PS_NUMBER' => $data['CLIENT_REFF'],
                                                'DATE_TIME' => new Zend_Db_Expr("now()"),
                                            )
                                        );
                                
                                        $this->setbackURL('/'.$this->_request->getModuleName().'/index');
                                        $this->_redirect('/notification/success');
                                    }
                                }
                            }
                        }
                    }else{
                        // var_dump("check");die;
                    }
                }
            }
        }

        
        
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;  

	    

        if($this->_getParam('FILE_ID')){
            $AESMYSQL = new Crypt_AESMYSQL();
            $PS_NUMBER      = urldecode($this->_getParam('FILE_ID'));
            $FILE_ID = $AESMYSQL->decrypt($PS_NUMBER, $password);

            $select3 = $this->_db->select()
                  ->from(array('A' => 'T_FILE_SUBMIT')); 

            if($FILE_ID)
            {	
                // var_dump($FILE_ID);die;

                $select3->where('FILE_ID =?',$FILE_ID);
                $data = $this->_db->fetchRow($select3);
                
                $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                $this->_helper->download->file($data['FILE_NAME'],$attahmentDestination.$data['FILE_SYSNAME']);
                
                $updateArr = array();
                $updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED']+1;
                $updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;
                
                $whereArr = array('FILE_ID = ?'=>$FILE_ID);
                
                $fileupdate = $this->_db->update('T_FILE_SUBMIT',$updateArr,$whereArr);
                //echo "file downloaded: ".$data['FILE_DOWNLOADED'];
                Application_Helper_General::writeLog('VDEL','Download File Underlying Document '.$data['FILE_NAME']);
            }
            else{
                Application_Helper_General::writeLog('VDEL','View File Underlying Document');
            }
        }

        
        if($this->_getParam('FILE_CANCEL')){

            $filecancel = $this->_db->select()
                  ->from(array('A' => 'T_DIRECTDEBIT')); 

            $AESMYSQL2 = new Crypt_AESMYSQL();
            $PS_NUMBER2      = urldecode($this->_getParam('FILE_CANCEL'));
            $FILE_CANCEL = $AESMYSQL2->decrypt($PS_NUMBER2, $password);

            if($FILE_CANCEL)
            {	

                $filecancel->where('DIR_UD_CANCEL =?',$FILE_CANCEL);
                $data = $this->_db->fetchRow($filecancel);
                
                $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                $this->_helper->download->file($data['DIR_FILENAME'],$attahmentDestination.$data['DIR_UD_CANCEL']);
                
            }
        }

    }

    public function generateTransactionID(){

        $currentDate = date("Ymd");
        $seqNumber   = strtoupper(Application_Helper_General::str_rand(6));
        $trxId = 'UDC'.$currentDate.$seqNumber;

        return $trxId;
    }
}