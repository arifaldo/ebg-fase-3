<?php

require_once 'Zend/Controller/Action.php';


class helplist_EditController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$HELP_ID   	= $this->_getParam('HELP_ID');
		$this->view->HELP_ID = $HELP_ID;
		
		$filterArr = array(	'filter'=> array('StringTrim','StripTags'),
							'submit'=> array('StringTrim','StripTags'));
		
		$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$filter 	= $zf_filter->getEscaped('filter');
		
		$submit 	= $zf_filter->getEscaped('submit');
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}	
		}

		$select = $this->_db->select()
										->from(	'T_HELP',
												array(
														'HELP_ID', 
														'HELP_TOPIC',
														'UPLOADED_BY', 
														'UPLOAD_DATETIME', 
														'HELP_DESCRIPTION', 
														'HELP_FILENAME',
														'HELP_SYS_FILENAME'))
										->where('HELP_ID =?',$HELP_ID);

		$data = $this->_db->fetchRow($select);

		if($submit)
		{
			if($this->_request->isPost())
			{
				$errorRemark = null;
				
				$filters = array(
									'helpTopic'	=> array('StripTags', 'StringTrim'),
									'description'	=> array('StripTags', 'StringTrim'),
								);
				$exclude = "HELP_ID != $HELP_ID and HELP_ISDELETED != 1";
				$validators = array(
										'helpTopic' => array(
																	'NotEmpty',
																	array('StringLength', array('max' => 50)),
																	new Zend_Validate_Regex('/^[a-zA-Z 0-9]*$/'),
																	array('Db_NoRecordExists',array(
																									'table'=>'T_HELP',
																									'field'=>'HELP_TOPIC',
																									'exclude'=>$exclude
																								)
																			),
																	'messages' => array(
																							$this->language->_("Error : Help Topic cannot be left blank"),
																							$this->language->_("Error : Help Topic size more than 50 char"),
																							$this->language->_("Error : Help Topic Format Must Alpha Numeric"),
																							$this->language->_("Error : Help Topic Already Exist")
																						)
															),
										'description' => array()
									);
									
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
				$description 	= ($zf_filter_input->isValid('description'))? $zf_filter_input->description : $this->_request->getParam('description'); 
				$helpTopic 		= ($zf_filter_input->isValid('helpTopic'))? $zf_filter_input->helpTopic : $this->_request->getParam('helpTopic'); 
			
				// $helpTopic 		= $this->getRequest()->getParam('helpTopic');
				// $description 	= $this->getRequest()->getParam('description');

				if($zf_filter_input->isValid())
				{
					try
					{
						$this->_db->beginTransaction();
						$param = array(
							'HELP_EDITEDBY'			=> $this->_userIdLogin, 
							'EDIT_DATETIME' 		=> new Zend_Db_Expr('now()'), 
							'HELP_TOPIC' 			=> $zf_filter_input->helpTopic,
							'HELP_DESCRIPTION'		=> $zf_filter_input->description,
						);	
						
						$where = array('HELP_ID = ?' => $HELP_ID);
						$query = $this->_db->update ( "T_HELP", $param, $where );
						$this->_db->commit();
						Application_Helper_General::writeLog('HLUD','Edit Help. Help ID: ['.$HELP_ID.'], File name : ['.$data['HELP_FILENAME'].']');
						$this->setbackURL('/helplist');
						$this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						$this->_db->rollBack();
						/*die($e);
						$errorRemark = $this->getErrorRemark('82');
						$this->view->errorMsg = array(array($errorRemark));*/
					//	SGO_Helper_GeneralLog::technicalLog($e);
					}
				}
				else
				{
					$this->view->error 				= true;
					$errors 						= $zf_filter_input->getMessages();
					
					$this->view->helpTopicErr			= (isset($errors['helpTopic']))? $errors['helpTopic'] : null;
					$this->view->descriptionErr 		= (isset($errors['description']))? $errors['description'] : null;
				}	
			}
		}
		$this->view->file = $data['HELP_FILENAME'];
		if($this->_request->isPost())
		{
			$this->view->helpTopic = (isset($zf_filter_input->helpTopic)) ? $zf_filter_input->helpTopic : $this->getRequest()->getParam('helpTopic');
			$this->view->description = (isset($zf_filter_input->description)) ? $zf_filter_input->description : $this->getRequest()->getParam('description');
		}
		else
		{
			$this->view->helpTopic = $data['HELP_TOPIC'];
			
			$description = $data['HELP_DESCRIPTION'];
			$this->view->description = $description;
		}
		$disable_note_len 	 = (isset($description))  ? strlen($description)  : 0;	
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
		
		$disable_note_len 	 = 100 - $disable_note_len;		
		$note_len 	 = 100 - $note_len;	

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('HLUD','Viewing Edit Help');
		}
						
	}
}
