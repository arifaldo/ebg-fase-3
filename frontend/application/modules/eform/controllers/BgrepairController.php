<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eform_BgrepairController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {

        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;
        
        $Settings = new Settings();
        
        $toc = $Settings->getSetting('ftemplate_bg');
        $this->view->toc = $toc; 

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin); 
        
        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];
        
       /* 
        $complist = $this->_db->fetchAll(
            $this->_db->select()
                 ->from(array('A' => 'M_USER'),array('CUST_ID'))
               
                 ->where("A.USER_ID = ? ", $this->_userIdLogin)
        );  
        // echo $complist;;die;
        // var_dump($complist);die;
        $comp = "'";
        // print_r($complist);die;
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";
        */
        $comp = "'".$this->_custIdLogin."'";
        
        if($system_type == '2'){

        $acctlist = $this->_db->fetchAll(
            $this->_db->select()
                    ->from(array('A' => 'M_APIKEY'))
                    ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                    ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                    // ->where('A.ACCT_STATUS = ?','5')
                    ->where("A.CUST_ID IN (".$comp.")")
                    ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
        );

        // echo "<pre>";
        // var_dump($acctlist);

        $account = array();
        foreach ($acctlist as $key => $value) {
            $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
            $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
            $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
            $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
            $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
            $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
            $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
        }
        // echo "<pre>";
        // var_dump($account);die;
        // $acct = array();
        $i = 0;
        foreach ($account as $key => $value) {

            $acct[$i]['ACCT_NO'] = $value['account_number'];
            $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
            $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
            $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
            $acct[$i]['ACCT_NAME'] = $value['account_name'];
            $i++;
        }
    
        $this->view->sourceAcct = $acct;
        
        }else{
        
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        //if ($type == 1) {
        //  $AccArr = $CustomerUser->getAccounts();
        //} else {
            $param = array('CCY_IN' => 'IDR');
            $AccArr = $CustomerUser->getAccounts($param);
        //}
        
        if(!empty($AccArr)){
            foreach($AccArr as $i => $value){
            
            $AccArr[$i]['ACCT_BANK'] = $this->_bankName;
            }
        }
        //echo '<pre>';
        //var_dump($AccArr);die;
        $this->view->AccArr = $AccArr;
        }
		
		
		
		
		   $numb = $this->_getParam('payref');
			$this->view->BG_NUMBER = $numb;
         if(!empty($numb)){
         $bgdata = $this->_db->select()
                             ->from(array('A' => 'T_BANK_GUARANTEE'),array('*'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->where('A.BG_REG_NUMBER = ?',$numb)
                             ->where('A.BG_STATUS = ?',10)
                             ->query()->fetchAll();

        $bgdatadetail = $this->_db->select()
                             ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'),array('*'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->where('A.BG_NUMBER = ?',$numb)
                             ->query()->fetchAll();
                             //$datas = $this->_request->getParams();
    // var_dump($datas);
    //var_dump($bgdata);
    
                if(!empty($bgdata)){
                    
                    $data = $bgdata['0'];
					
					if(!empty($data['BG_BRANCH'])){
                        $selectbranch = $this->_db->select()
                                             ->from(array('A' => 'M_BRANCH'),array('*'))
                                             ->where('A.BRANCH_CODE = ?',$data['BG_BRANCH'])
                                             ->query()->fetchAll();
                        //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                        $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                        }

                    $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
                        $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
                        
                         $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                        $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


                        $config    		= Zend_Registry::get('config');
                        $BgType 		= $config["bg"]["status"]["desc"];
                        $BgCode 		= $config["bg"]["status"]["code"];
                
                        $arrStatus = array_combine(array_values($BgCode),array_values($BgType));
         
                        $this->view->arrStatus = $arrStatus;
                        
                        $arrBankFormat = array(
                            1 => 'Bank Standard',
                            2 => 'Special Format (with bank approval)'
                        );
                        
                        $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                        
                        $arrLang = array(
                            1 => 'Indonesian',
                            2 => 'English',
                            3 => 'Billingual',
                        );
                        
                        $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                          $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

                           $param = array('CCY_IN' => 'IDR','ACCT_NO'=>$data['FEE_CHARGE_TO']);
                            $AccArr = $CustomerUser->getAccounts($param);
                        
                         
                        if(!empty($AccArr)){
                            $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                        }
                        
                        $arrWaranty = array(
                            1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                            2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                            3 => 'Insurance'
                            
                        );
                         
                        $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

						//$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];

                         if(!empty($data['USAGE_PURPOSE'])){
                            $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                            foreach($data['USAGE_PURPOSE'] as $key => $val){
                                $str = 'checkp'.$val;
                                //var_dump($str);
                                $this->view->$str =  'checked';
                            }
                        }

                        if($data['BG_NUMBER'] == ''){
                            $data['BG_NUMBER'] = '-';
                        }
                        if($data['BG_SUBJECT'] == ''){
                            $data['BG_SUBJECT'] = '- no subject -';
                        }
                        $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                        $this->view->BG_NUMBER = $data['BG_NUMBER'];
                        $this->view->subject = $data['BG_SUBJECT'];

                        $this->view->recipent_name = $data['RECIPIENT_NAME'];
                        $this->view->address = $data['RECIPIENT_ADDRES'];
                        $this->view->city = $data['RECIPIENT_CITY'];
                        $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                        $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                        $this->view->fileName = $data['FILE'];
                        $this->view->bank_amount = $data['BG_AMOUNT'];
                        $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                        $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                        $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];
                        
                        $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                        $this->view->acct = $data['FEE_CHARGE_TO'];
                        $this->view->status = $data['BG_STATUS'];

                        if($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])){
                              $selectQuery  = "SELECT
                                a.USER_LOGIN,
                                b.`USER_FULLNAME` AS u_name,
                                c.`BUSER_NAME` AS b_name,
                                a.DATE_TIME,
                                a.BG_REASON,
                                a.HISTORY_STATUS,
                                a.BG_REG_NUMBER
                                
                                
                              FROM
                                T_BANK_GUARANTEE_HISTORY AS a
                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                              $result =  $this->_db->fetchAll($selectQuery);
                              if(!empty($result)){
                                $data['REASON'] = $result['0']['BG_REASON'];
                              }
                            $this->view->reqrepair = true;
                            $this->view->reason = $data['REASON'].''.$data['BG_REJECT_NOTES'].$data['BG_CANCEL_NOTES'];
                        }

                        if(!empty($bgdatadetail)){
                            foreach ($bgdatadetail as $key => $value) {

                                    if($data['COUNTER_WARRANTY_TYPE'] == 3){
                                    if($value['PS_FIELDNAME'] == 'Insurance Name'){
                                     $this->view->insuranceName =   $value['PS_FIELDVALUE']; 
                                    }

                                    if($value['PS_FIELDNAME'] == 'Principal Agreement'){
                                     $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE']; 
                                    }

                                    if($value['PS_FIELDNAME'] == 'Amount'){
                                     $this->view->insurance_amount =   $value['PS_FIELDVALUE']; 
                                    }

                                    if($value['PS_FIELDNAME'] == 'Principal Agreement Date'){
                                     $this->view->paDate =   $value['PS_FIELDVALUE']; 
                                    }                                    
                                }else{

                                    if($value['PS_FIELDNAME'] == 'Plafond Owner 1'){
                                     $this->view->owner1 =   $value['PS_FIELDVALUE']; 
                                    }

                                    if($value['PS_FIELDNAME'] == 'Amount Owner 1'){
                                     $this->view->amountowner1 =   $value['PS_FIELDVALUE']; 
                                    }

                                    if($value['PS_FIELDNAME'] == 'Plafond Owner 2'){
                                     $this->view->owner2 =   $value['PS_FIELDVALUE']; 
                                    }

                                    if($value['PS_FIELDNAME'] == 'Amount Owner 2'){
                                     $this->view->amountowner2 =   $value['PS_FIELDVALUE']; 
                                    }

                                    if($value['PS_FIELDNAME'] == 'Plafond Owner 3'){
                                     $this->view->owner3 =   $value['PS_FIELDVALUE']; 
                                    }

                                    if($value['PS_FIELDNAME'] == 'Amount Owner 3'){
                                     $this->view->amountowner3 =   $value['PS_FIELDVALUE']; 
                                    }

                                }

                                }    
                        }
                        

                        
                        $conf = Zend_Registry::get('config');
                        $this->view->bankname = $conf['app']['bankname'];
                        
                      
				}


			}


        $maxFileSize = "1024"; //"1024000"; //1024
        //~ $fileExt = "csv,dbf";
        $fileExt = "jpeg,jpg,pdf";
        if($this->_request->isPost() )
        {
            
            $attahmentDestination   = UPLOAD_PATH . '/document/submit/';
            $errorRemark            = null;
            $adapter                = new Zend_File_Transfer_Adapter_Http();
            $datas = $this->_request->getParams();
            
            $sourceFileName = trim($adapter->getFileName());
             
            if($sourceFileName == null){
                //die('here');
                $sourceFileName = null;
                $fileType = null;
            }
            else
            {

                $sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
                if($_FILES["uploadSource"]["type"])
                {
                    $adapter->setDestination($attahmentDestination);
                    $fileType               = $adapter->getMimeType();
                    $size                   = $_FILES["document"]["size"];
                }
                else{
                    $fileType = null;
                    $size = null;
                }
            }
            $paramsName['sourceFileName']   = $sourceFileName;
            //print_r($sourceFileName);die;
            $fileTypeMessage = explode('/',$fileType);
                    if (isset($fileTypeMessage[1])){
                        $fileType =  $fileTypeMessage[1];
                        $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                        $extensionValidator->setMessage("Extension file must be *.jpg /.jpeg / .pdf");

                        $size           = number_format($size);
                        $sizeTampil     = $size/1000;
                        //$sizeValidator    = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                        // $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");

                        $adapter->setValidators(array($extensionValidator));

                        if($adapter->isValid())
                        {
                            $date = date("dmy");
                            $time = date("his");

                            $newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
                            $adapter->addFilter('Rename', $newFileName);

                            if($adapter->receive())
                            {


                                $filedata = $attahmentDestination.$newFileName;
                                // var_dump($filedata);
                                if (file_exists($filedata)) {
                                exec("clamdscan --fdpass ".$filedata, $out ,$int);
                                    
                                // if()
                                    // var_dump($int);
                                    // var_dump($out);
                                    // die;
                                if($int == 1){

                                    unlink($filedata);
                                    //die('here');
                                    $this->view->error = true;
                                    $errors = array(array('FileType'=>'Error: An infected file. Please upload other file'));
                                    $this->view->errorMsg = $errors;


                                }else{
                                    $datas['fileName'] = $newFileName;
                                    $sessionNamespace = new Zend_Session_Namespace('eform');
                                    $sessionNamespace->content = $datas;
                                    $this->_redirect('/eform/bgrepair/confirm');
                                    
                                    
                                }
                                
                                
                                }
                            }
                            
                        }else{
                            $this->view->error = true;
                            $errors = array($adapter->getMessages());
                            $this->view->errorMsg = $errors;
                        }
                        
                    }else{
                                    $sessionNamespace = new Zend_Session_Namespace('eform');
                                    $sessionNamespace->content = $datas;
                                    
                                    $this->_redirect('/eform/bgrepair/confirm');
                    }
            
            
            
            
            
            
            
            
           

        } 
        // $paramac = array(
        //  'CCY_IN' => 'IDR'
        // );
        // $AccArr                  = $this->CustomerUser->getAccounts($paramac);
        // $this->view->AccArr      = $AccArr;

    }



    public function confirmAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

         $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

      
        
        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

        $sessionNamespace = new Zend_Session_Namespace('eform');
        $data = $sessionNamespace->content;
        
        $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
        
        
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        
            $param = array('CCY_IN' => 'IDR','ACCT_NO'=>$data['acct']);
            $AccArr = $CustomerUser->getAccounts($param);
        
         
        if(!empty($AccArr)){
            $this->view->src_name = $AccArr['0']['ACCT_NAME'];
        }
        
        if(!empty($data['chkpurpose'])){
            foreach($data['chkpurpose'] as $key => $val){
                $str = 'checkp'.$val;
                //var_dump($str);
                $this->view->$str =  'checked';
            }
        }
        
        foreach($data as $key => $val){
            
            if($key != 'language'){
        $this->view->$key = $val;
            }
        }
        
        $this->view->updateStart = Application_Helper_General::convertDate($data['updatedate'][0],$this->view->viewDateFormat,$this->view->defaultDateFormat);
        $this->view->updateEnd = Application_Helper_General::convertDate($data['updatedate'][1],$this->view->viewDateFormat,$this->view->defaultDateFormat);
        
        $arrBankFormat = array(
            1 => 'Bank Standard',
            2 => 'Special Format (with bank approval)'
        );
        
        $this->view->bankFormat = $arrBankFormat[$data['bankFormat']];
        
        $arrLang = array(
            1 => 'Indonesian',
            2 => 'English',
            3 => 'Billingual',
        );
        
        $this->view->languagetext = $arrLang[$data['language']];
        
        $arrWaranty = array(
            1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
            2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
            3 => 'Insurance'
            
        );
         
        $this->view->warranty_type_text = $arrWaranty[$data['warranty_type']];
        //echo '<pre>';
        //var_dump($data);
        
        $download = $this->_getParam('download');
              //print_r($edit);die;
        if($download){
            $attahmentDestination = UPLOAD_PATH . '/document/submit/';
            $this->_helper->download->file($data['fileName'],$attahmentDestination.$data['fileName']);    
        }
        
        if($this->_request->isPost() )
        {
            
                    $usage = '';
                    foreach($data['chkpurpose'] as $val){
                        $usage .= $val.',';
                    }
					
					
					$BG_REG_NUMBER = $data['BG_REG_NUMBER'];
					
                    $usage = substr($usage, 0, -1);
            
                                    try
                                    {
                                        if($data['bank_amount'] == ''){
                                            $data['bank_amount'] = 0;
                                        }
                                        
                                        if($data['amount'] == ''){
                                            $data['amount'] = 0;
                                        }
                                        $this->_db->beginTransaction();
                                        $insertArr = array(
                                               // 'BG_REG_NUMBER'             => $this->generateTransactionID(),
                                                'CUST_ID'               => $this->_custIdLogin,
                                                'BG_STATUS'             => 1,
                                                'BG_SUBJECT'            => $data['subject'],
                                                'RECIPIENT_NAME'            => $data['recipent_name'],
                                                'RECIPIENT_ADDRES'          => $data['address'],
                                                'RECIPIENT_CITY'            => $data['city'],
                                                'RECIPIENT_CONTACT'         => $data['contact_number'],
                                                'GUARANTEE_TRANSACTION'         => $data['comment'],
                                                'FILE'          => $data['fileName'],
                                                'BG_AMOUNT'           => Application_Helper_General::convertDisplayMoney($data['bank_amount']),
                                                'TIME_PERIOD_START'         => $data['updatedate'][0],
                                                'TIME_PERIOD_END'           => $data['updatedate'][1],
                                                
                                                'COUNTER_WARRANTY_TYPE'         => $data['warranty_type'],
                                                'COUNTER_WARRANTY_ACCT_NO'          => $data['account_number'],
                                                'COUNTER_WARRANTY_ACCT_NAME'            => $data['account_name'],
                                                'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney($data['amount']),
                                                'FEE_CHARGE_TO'         => $data['acct'],
                                                'BG_FORMAT'         => $data['bankFormat'],
                                                'BG_LANGUAGE'           => $data['language'],
                                                'USAGE_PURPOSE'         => $usage,
                                                //'BG_CREATED'    => new Zend_Db_Expr('now()'),
                                                //'BG_CREATEDBY'      => $this->_userIdLogin
                                        );
										
										 //$data = array ('BG_STATUS' => '11');
										$where['BG_REG_NUMBER = ?'] = $BG_REG_NUMBER;
										$this->_db->update('T_BANK_GUARANTEE',$insertArr,$where);
                                        
										
										$insertArr['BG_REG_NUMBER'] = $BG_REG_NUMBER;
										//$this->_db->insert('T_BANK_GUARANTEE', $insertArr);
										$wheredelete = array();
                                        $wheredelete['BG_NUMBER = ?'] = $BG_REG_NUMBER;
										$this->_db->delete('T_BANK_GUARANTEE_DETAIL',$wheredelete);
										
										
                                        if($data['warranty_type'] == '2'){
                                            if(!empty($data['owner1'])){
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Plafond Owner 1',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['owner1']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                        
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Amount Owner 1',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['amountowner1']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                                    
                                        }
                                        
                                        if(!empty($data['owner2'])){
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Plafond Owner 2',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['owner2']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                        
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Amount Owner 2',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['amountowner2']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                                    
                                        }
                                        
                                        if(!empty($data['owner3'])){
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Plafond Owner 3',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['owner3']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                        
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Amount Owner 3',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['amountowner3']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                                    
                                        }
                                        
                                        }
                                        
                                        if($data['warranty_type'] == '3'){
                                            $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Insurance Name',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['insuranceName']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                        
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Principal Agreement',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['PrincipalAgreement']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                        
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Principal Agreement Date',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['paDate']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                        
                                        
                                        $arrDetail = array(
                                                        'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                        'CUST_ID' => $insertArr['CUST_ID'],
                                                        'USER_ID' =>  $this->_userIdLogin,
                                                        'PS_FIELDNAME' => 'Amount',
                                                        'PS_FIELDTYPE' => 1,
                                                        'PS_FIELDVALUE' => $data['insurance_amount']
                                                    );
                                        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $arrDetail);
                                            
                                        }
                                        
                                        
                                        
                                        $historyInsert = array(
                                            'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                            'BG_REG_NUMBER'         => $insertArr['BG_REG_NUMBER'],
                                            'CUST_ID'           => $this->_custIdLogin,
                                            'USER_LOGIN'        => $this->_userIdLogin,
                                            'HISTORY_STATUS'    => 1
                                        );

                                        $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
                                        
                                        $this->_db->commit();

                                        //Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);

                                        $this->setbackURL('/'.$this->_request->getModuleName().'/');
                                        $this->_redirect('/notification/success');
                                    }
                                    catch(Exception $e)
                                    {
                                        var_dump($e);die;
                                        $this->_db->rollBack();
                                        //Application_Log_GeneralLog::technicalLog($e);
                                    }
            
            
        }
        
        


    }
    
    public function generateTransactionID(){

        $currentDate = date("Ymd");
        $seqNumber   = strtoupper(Application_Helper_General::str_rand(6));
        $trxId = 'REG'.$currentDate.$seqNumber;

        return $trxId;
    }



}

