<?php
require_once 'General/ForgotPassword.php';

require_once 'SGO/Extendedmodule/Getest/lib/class.geetestlib.php';
require_once 'SGO/Extendedmodule/Getest/config/config.php';


class tools_ForgotpasswordController extends Application_Main {

	public function indexAction() {
		if(Zend_Auth::getInstance()->hasIdentity()){
			$this->_redirect ( '/home/dashboard' );
		}
		$this->_helper->layout()->setLayout('newlayout');

		//---------------------- GENERATE CAPTCHA IMAGE  --------------------------------------
		$captcha = Application_Captcha::generateCaptcha ();
		$this->view->captchaId = $captcha ['id']; //returns the ID given to session &amp; image
		$this->view->captImgDir = $captcha ['imgDir'];
		//---------------------	END GENERATE CAPTCHA IMAGE -------------------------------------


		$GtSdk = new GeetestLib(CAPTCHA_ID, PRIVATE_KEY);
		$app = Zend_Registry::get('config');
		if ($app['captcha']['emulate'] == 1){
				$this->view->capchahide = 'none';
		}else{
				$this->view->capchahide = 'block';
		}

		if ($this->_request->isPost ()) {

			if ($app['captcha']['emulate'] == 1){
		    		$captchaValid = true;
	    	}else{
			
			if ($_SESSION['gtserver'] == 1) {   //服务器正常
				//var_dump($_POST['geetest_challenge']);
				//var_dump($_POST['geetest_validate']);var_dump($_POST['geetest_seccode']);
				$data = array();
			    $resultcapca = $GtSdk->success_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'], $data);
				
			    if ($resultcapca) {

			    	$captchaValid = true;
			        // echo '{"status":"success"}';
			    } else{
			    	$captchaValid = false;
			    	// echo 'here';
			        // echo '{"status":"fail"}';
			    }
			}else{  //服务器宕机,走failback模式
			    if ($GtSdk->fail_validate($_POST['geetest_challenge'],$_POST['geetest_validate'],$_POST['geetest_seccode'])) {
			    	$captchaValid = true;
			        // echo '{"status":"success"}';
			    }else{
			    	$captchaValid = false;
			    	// echo 'here1';
			        // echo '{"status":"fail"}';
			    }
			}

			}

			// ======================== START FILTERING ======================================
			$filters = array (	'userId' 	=> array ('StripTags', 'StringTrim', 'StringToUpper' ),
								// 'custId' 	=> array ('StripTags', 'StringTrim', 'StringToUpper' ),
								'userEmail' => array ('StripTags', 'StringTrim' ) );
			$zf_filter_input = new Zend_Filter_Input ( $filters, null, $this->_getAllParams (), $this->_optionsValidator );
			// ========================== END FILTERING =======================================

			$userId 	= ($zf_filter_input->userId);
			// $custId 	= ($zf_filter_input->custId);
			$userEmail 	= ($zf_filter_input->userEmail);

			//================================BYPASSCAPCAY (|| true========================================//
			if (Application_Captcha::validateCaptcha ( $this->_request->getPost ( 'captcha' ) ) === true || $captchaValid)
			{
				$cekpass 	= false;
				$FP 		= new ForgotPassword();
				$result   	= $FP->requestResetPasswordBackend($userId, $userEmail);

				$cekpass	= ($result['responseCode'] == '00')? true: false;
				$errors		= ($result['responseCode'] == '00')? null: $result['responseDesc'];

				if($cekpass){
					$this->view->msg_success = $this->language->_('Reset password success. Link have been sent to your email.');
				}

			}else {
				$errors = $this->language->_('Invalid Captcha');
				// $this->view->custId = $custId;
				$this->view->userId = $userId;
				$this->view->userEmail = $userEmail;
			}



		} // END REQUEST IS POST

		if(isset($errors)){
			$this->view->errors = $errors;
			// $this->view->custId = $custId;
			$this->view->userId = $userId;
			$this->view->userEmail = $userEmail;
		}
	}
}
