<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class popup_SweepController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{       
		$listCcy = array(''=>'-- Select Currency --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','DESCRIPTION'));
		$this->view->ccy = $listCcy;
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction()
	{
		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
						'benef_acct'  => array('field' => 'ACCT_NO',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						'benef_name'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Beneficiary Account Name'),
											   'sortable' => true),
						'email'  => array('field' => 'ACCT_EMAIL',
											   'label' => $this->language->_('Email Address'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						//'favorite'   => array('field'    => 'ISFAVORITE',
					//						  'label'    => $this->language->_('Favorite'),
					//						  'sortable' => true),
					//	'checked'   => array('field'    => 'BENEFICIARY_ISAPPROVE',
					//						  'label'    => $this->language->_('Checked by Bank'),
					//						  'sortable' => true),
						'date'   => array('field'    => 'ACCT_CREATED',
											  'label'    => $this->language->_('Created Date'),
											  'sortable' => true),
						'status'   => array('field'    => 'ACCT_STATUS',
											  'label'    => $this->language->_('Status'),
											  'sortable' => true),
				);
				
		//get page, sortby, sortdir
		$currency    = $this->_getParam('currency');
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','favorite');
		$sortDir = $this->_getParam('sortdir','desc');
		$id_box = $this->_getParam('id_box');
		//echo $id_box;
		//Zend_Debug::dump($this->_getParam());
		//exit();
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'alias' 	  	=> array('StringTrim','StripTags'),
							'benef_acct'    => array('StringTrim','StripTags','StringToUpper'),
							'benef_name'    => array('StringTrim','StripTags'),
							'favorit'     	=> array('StringTrim','StripTags')
		);
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->id_box = $id_box;
		
		$select	= $this->_db->select()
							->from	(array(	'MCA' => 'M_CUSTOMER_ACCT'),
									 array(	'ACCT_NO'		  => 'MCA.ACCT_NO',
											'CCY_ID'		  => 'MCA.CCY_ID',
											'ACCT_NAME'	 	  => 'MCA.ACCT_NAME',
											'ACCT_ALIAS_NAME' => 'MCA.ACCT_ALIAS_NAME',
											'ACCT_TYPE' 	  => 'MCA.ACCT_TYPE',
											'FREEZE_STATUS'	  => 'MCA.FREEZE_STATUS',
											'MAXLIMIT' 	  	  => 'MML.MAXLIMIT',
									 		'DESC' 	  	  	  => 'MCA.ACCT_DESC',
									 		'EMAIL' 	  	  => 'MCA.ACCT_EMAIL',
											'MCA.ACCT_STATUS',
											'MCA.ACCT_CREATED'
										   ))
							->joinLeft	(array('MML' => 'M_MAKERLIMIT'), 'MCA.ACCT_NO = MML.ACCT_NO', array())
							->where("MCA.CUST_ID 	= ? ", (string)$this->_custIdLogin)
							->where("MML.CUST_ID 	= ? ", (string)$this->_custIdLogin)
							->where("MML.USER_LOGIN = ? ", (string)$this->_userIdLogin)
							->where("MCA.ACCT_STATUS = 1")
                            ->where("MCA.CCY_ID = ?",'IDR')
							->where("MML.MAKERLIMIT_STATUS = 1")
							->where("MML.MAXLIMIT > 0");
		

		$select->order(array("MCA.ACCT_NAME"));
		
		//$result =  $this->_db->fetchAll($select);

		//$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		//$select->where("B.BENEFICIARY_BANKSTATUS = 1");
		//$select->where("B.BENEFICIARY_ISAPPROVE = 1");
		//$select->where("B.CURR_CODE = ?",'IDR');
		
		if($filter == TRUE)
		{
			$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('benef_acct');
			$fName = $zf_filter->getEscaped('benef_name');
			$fFav = $zf_filter->getEscaped('favorit');
			
	        if($fAlias)$select->where('UPPER(MCA.ACCT_ALIAS_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fAlias).'%'));
	        if($fAcct)$select->where('MCA.ACCT_NO LIKE '.$this->_db->quote('%'.strtoupper($fAcct).'%'));
	        if($fName)$select->where('UPPER(MCA.ACCT_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));
	        //if($fFav==1)$select->where('B.ISFAVORITE=1');
			
			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			//if($fFav==1)$this->view->favorit = true;
			//else $this->view->favorit = false;
		}
		//else $this->view->favorit = false;
		
	    $select->order($sortBy.' '.$sortDir);   
	     //echo $select;die;
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->dateTimeDisplayFormat = $this->_dateTimeDisplayFormat;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}
	
	
	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID'); 
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS'); 
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
	}

}
