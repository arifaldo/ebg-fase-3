<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_repairdetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

        // $numb = $this->_getParam('bgnumb');
        // decrypt numb
        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token     = $rand;
        $this->view->token = $sessionNamespace->token;

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        if (!empty($BG_NUMBER)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->where('A.BG_STATUS = ?', 10)
                ->query()->fetchAll();

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();
            //$datas = $this->_request->getParams();
            // var_dump($datas);


            if (!empty($bgdata)) {

                $data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


                $arrStatus = array(
                    '1' => 'Waiting for review',
                    '2' => 'Waiting for approve',
                    '3' => 'Waiting to release',
                    '4' => 'Waiting for bank approval',
                    '5' => 'Issued',
                    '6' => 'Expired',
                    '7' => 'Canceled',
                    '8' => 'Claimed by applicant',
                    '9' => 'Claimed by recipient',
                    '10' => 'Request Repair',
                    '11' => 'Reject'
                );

                $this->view->arrStatus = $arrStatus;

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

                $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                $AccArr = $CustomerUser->getAccounts($param);


                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                // $arrWaranty = array(
                //     1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                //     2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                //     3 => 'Insurance'

                // );

                // $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];

                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];

                if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                                a.USER_LOGIN,
                                b.`USER_FULLNAME` AS u_name,
                                c.`BUSER_NAME` AS b_name,
                                a.DATE_TIME,
                                a.BG_REASON,
                                a.HISTORY_STATUS,
                                a.BG_REG_NUMBER
                                
                                
                              FROM
                                T_BANK_GUARANTEE_HISTORY AS a
                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    $result =  $this->_db->fetchAll($selectQuery);
                    if (!empty($result)) {
                        $data['REASON'] = $result['0']['BG_REASON'];
                    }
                    $this->view->reqrepair = true;
                    $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Date') {
                                $this->view->paDate =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);

                $download = $this->_getParam('download');
                //print_r($edit);die;
                if ($download) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                }

                if ($this->_request->isPost()) {
                    $approve = $this->_getParam('approve');
                    $BG_NUMBER = $this->_getParam('bgnumb');
                    $reject = $this->_getParam('reject');
                    $repair = $this->_getParam('repair');

                    //$datas = $this->_request->getParams();
                    //echo '<pre>';
                    //var_dump($datas);die;



                    //print_r($edit);die;
                    if ($approve) {
                        $data = array('BG_STATUS' => '3');
                        $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                        $this->_db->update('T_BANK_GUARANTEE', $data, $where);

                        $historyInsert = array(
                            'DATE_TIME'         => new Zend_Db_Expr("now()"),
                            'BG_REG_NUMBER'         => $BG_NUMBER,
                            'CUST_ID'           => $this->_custIdLogin,
                            'USER_LOGIN'        => $this->_userIdLogin,
                            'HISTORY_STATUS'    => 3
                        );

                        $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                        $this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
                        $this->_redirect('/notification/success/index');
                    }

                    if ($reject) {
                        $data = array('BG_STATUS' => '11');
                        $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                        $this->_db->update('T_BANK_GUARANTEE', $data, $where);

                        $notes = $this->_getParam('PS_REASON_REJECT');
                        $historyInsert = array(
                            'DATE_TIME'         => new Zend_Db_Expr("now()"),
                            'BG_REG_NUMBER'         => $BG_NUMBER,
                            'CUST_ID'           => $this->_custIdLogin,
                            'USER_LOGIN'        => $this->_userIdLogin,
                            'HISTORY_STATUS'    => 11,
                            'BG_REASON'         => $notes,
                        );

                        $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                        $this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
                        $this->_redirect('/notification/success/index');
                    }


                    if ($repair) {
                        //die('here');
                        //  $data = array ('BG_STATUS' => '10');
                        //  $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;

                        $this->_redirect('/eform/bgrepair/index/payref/' . $BG_NUMBER);
                    }

                    $back = $this->_getParam('back');
                    if ($back) {
                        $this->_redirect('/eformworkflow/review');
                    }
                }
            }
        }
    }
}
