<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));	
	require_once 'General/CustomerUser.php';
	require_once 'General/Settings.php'; 
	
	$config    = Zend_Registry::get('config');
	$paymentStatus = $config["payment"]["status"];

	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	
	$days     = 0; 
	$type = "REPAIR";
	$payReffArr = array();
	$emailSent = 0;
	$failSent = 0;
	$template = file_get_contents(LIBRARY_PATH.'/email template/Approver Releaser Maker Email Notification.html');
	
	$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
	$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
	
	$transFT   = array( '[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
						'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
						'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl													
					  );			
	$FOOTER_ID = strtr($FOOTER_ID, $transFT);
	$FOOTER_EN = strtr($FOOTER_EN, $transFT);
	
	$db = Zend_Db_Table::getDefaultAdapter();
	
	$data = $db->SELECT()->DISTINCT()
						->FROM(array('P'=>'T_PSLIP'),array('CUST_ID'))
						->JOINLEFT(array('E' => 'T_EMAIL_NOTIFICATION'),'P.PS_NUMBER = E.PS_NUMBER',array())
						->where('P.PS_STATUS = ?', (string)$paymentStatus["code"]["requestrepair"])	
						->WHERE('E.PS_ISREPAIREMAILED  <> 1 OR E.PS_ISREPAIREMAILED is null')
						->QUERY()->FETCHALL();

		if(is_array($data))
		{
			foreach($data as $custArr)
			{
				$cust_id = $custArr["CUST_ID"];
				$Customer = new Customer($cust_id);
				$userList = $Customer->getEmailRepairerList();
			
				if($userList)
				{
					foreach($userList as $isi)
					{
/*						$param = array(
												'paymentType' => $paymentType,
												'transferType' => $transferType
												);
					
						$payment = new Payment();
						$query = $payment->getPaymentList($param);
						$query ->JOINLEFT(array('TEN'=>'T_EMAIL_NOTIFICATION'),'P.PS_NUMBER = TEN.PS_NUMBER',array(''))
									-> WHERE('PS_ISREPAIREMAILED <> 1 or PS_ISREPAIREMAILED is NULL')
									->WHERE('UPPER(CUST_ID) = ?',$cust_user['CUST_ID'])
									->WHERE('UPPER(PS_CREATEDBY) = ?',$cust_user['USER_ID'])
									->WHERE('P.PS_STATUS = ?', (string)$paymentStatus["code"]["requestrepair"]);
						$paymentList = $db->FETCHALL($query);
	*/
		
						$CustomerUser = new CustomerUser($cust_id,$isi['USER_ID']);						
						
						$listAccount  = $CustomerUser->getAccounts();
						$accountList  = Application_Helper_Array::simpleArray($listAccount,'ACCT_NO');
						$paramPayment = array("WA"     => false,
											  "ACCOUNT_LIST"  => $accountList,
											  "_beneLinkage"  => $isi['IS_BENELINKAGE'],
											);	
						$query = $CustomerUser->getPayment($paramPayment);	
						$query ->JOINLEFT(array('TEN'=>'T_EMAIL_NOTIFICATION'),'P.PS_NUMBER = TEN.PS_NUMBER',array(''))
									-> WHERE('PS_ISREPAIREMAILED <> 1 or PS_ISREPAIREMAILED is NULL')
									->WHERE('P.PS_STATUS = ?', (string)$paymentStatus["code"]["requestrepair"]);
						$paymentList = $db->FETCHALL($query);
						if($paymentList)
						{
							$content = '';
							$counter = 0;
							foreach($paymentList as $paymentdata)
							{
								$caseBeneBank = "CASE P.PS_CATEGORY WHEN 'BULK PAYMENT' THEN CASE P.PS_TYPE WHEN '5' THEN '".$config['app']['bankname']."' ELSE '-' END ELSE CASE P.PS_TYPE WHEN '2' THEN T.BENEFICIARY_BANK_NAME ELSE '".$config['app']['bankname']."' END END";
								$caseMessage = "CASE P.PS_CATEGORY WHEN 'SINGLE PAYMENT' THEN T.TRA_MESSAGE ELSE '-' END";
								
								$info = $db->select()->distinct()
											->from(array('P' => 'T_PSLIP'),array(	'benebank' 	=> new Zend_Db_Expr($caseBeneBank),
																					'message' 	=> new Zend_Db_Expr($caseMessage)))
											->join(array('T' => 'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',array())
											->where('P.PS_NUMBER LIKE '.$db->quote($paymentdata['payReff']));
								//echo $info;die;
								$inforesult = $db->fetchRow($info);
								$payReffArr[] = $paymentdata['payReff'];
								$content .= '
								<tr>
									<td><font>'.++$counter.'</font></td>
									<td><font>'.$paymentdata['payReff'].'</font></td>
									<td><font>'.$paymentdata['paySubj'].'</font></td>
									';
								if($paymentdata['accsrc'] == '-')
								{
									$content .= '	<td><font>'.$paymentdata['accsrc'].'</font></td>
									';
								}
								else{
								$content .= '	<td><font>'.$paymentdata['accsrc'].'( '.$paymentdata['accsrc_ccy'].' ) - '.$paymentdata['accsrc_name'].'</font></td>
								';
								}
								if($paymentdata['acbenef'] == '-')
								{
									$content .= '	<td><font>'.$paymentdata['acbenef'].'</font></td>
									';
								}
								else{
								$content .= '	<td><font>'.$paymentdata['acbenef'].'( '.$paymentdata['acbenef_ccy'].' ) - '.$paymentdata['acbenef_name'].'</font></td>
								';
								}
								$content .= '	<td><font>'.$paymentdata['ccy'].' '.Application_Helper_General::displayMoney($paymentdata['amount']).'</font></td>
									<td align="right"><font>'.$inforesult['message'].'</font></td>
									<td align="center"><font>'.Application_Helper_General::convertDate($paymentdata['efdate'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
									<td align="right"><font>'.$inforesult['benebank'].'</font></td>
									<td align="center"><font>'.$paymentdata['PS_CREATEDBY'].'</font></td>								
									<td align="center"><font>'.Application_Helper_General::convertDate($paymentdata['created'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
									<td align="right"><font>'.$paymentdata['payType'].'</font></td>
									<td align="right"><font>'.$paymentdata['payStatus'].'</font></td>
								</tr>';	
								$translate = array( '[[TITLE]]' => 'Repair Payment Notification',
													'[[USER_FULLNAME]]' => $isi['USER_FULLNAME'],
													'[[USER_ID]]' => $isi['USER_ID'],
													'[[CUST_ID]]' => $custArr['CUST_ID'],
													'[[FOOTER_ID]]' 		=> $FOOTER_ID,
													'[[FOOTER_EN]]' 		=> $FOOTER_EN,
													'[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
													'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
													'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl,
													'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
													'[[master_bank_fax]]' 	=> $templateEmailMasterBankFax,
													'[[master_bank_address]]' 	=> $templateEmailMasterBankAddress,
													'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,		
								
										);			
								$mailContent = strtr($template,$translate);										
							}														
							
							if($content != ''){
								$subject = 'Repair Email Notification';				
								$translate = array( '[[DATA_CONTENT]]' => $content);
								$mailContent = strtr($mailContent,$translate);				
								$status = Application_Helper_Email::sendEmail($isi['USER_EMAIL'],$subject,$mailContent);	
								if($status == 'success')
								{
									$emailSent++;
								}		
								else
								{
									$failSent++;
								}
							}
						}
					}
				}				
			}
			Application_Helper_General::setPaymentEmailNotification($type, $payReffArr);
			$msg = '';
			if($emailSent)
			{$msg .= $emailSent.' Mail Sent'.PHP_EOL;}
			if($failSent)
			{$msg .= $failSent.' Mail Failed';}
			echo $msg;
			Application_Helper_General::cronLog(basename(__FILE__),$msg,'1');
		}

?>
