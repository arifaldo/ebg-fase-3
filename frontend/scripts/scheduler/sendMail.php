<?php
require_once (realpath(dirname(__FILE__) . '/../frontend/scripts/scheduler/zf-cli.php'));

Class mailTester
{
	public $_fulldesc = 'sendMail';
	public $systemLogger = null;
	public $_mailContent = 'This is Content.';
	public $_transport = null;
	public $_fromName = null;
	public $_username = null;
	
	public function __construct(){
		$logName = null;
		$pathFile = null;
		$printToDisplay = true;
		$logFileType = "ALL";

		$loggerService = SGO_Extendedmodule_Loggerservice_Service::getInstance();
		$this->systemLogger = $loggerService->getLogger($logName, $pathFile, $printToDisplay, $logFileType);

$this->systemLogger->addInfo('Load SMTP configuration');
		$config = Zend_Registry::get('config');
		$smtpServer = $config['email']['smtpserver'];
		$smtpPort	= $config['email']['smtpport'];
		$this->_username = $username = @$config['email']['username'];
		$password = @$config['email']['password'];
		$this->_fromName = $config['email']['fromName'];
		$subject = '=== Testing Sending Email ===';

		$configEmailAuth = (isset($config['email']['auth']) ? $config['email']['auth'] : NULL);

		$config = array(
			'port' 	  	=> $smtpPort,
// 				'auth'	  	=> $configEmailAuth, //'crammd5', //'login'
			// 'username'  => $username,
			// 'password'  => $password,
		);
		
		if (isset($configEmailAuth)){
			$config['auth'] = $configEmailAuth;
			if (isset($username))
				$config['username'] = $username;
			if (isset($password))
				$config['password'] = $password;
		}

// 				$config['auth'] = 'login';
		// else:
			// $config['auth'] = '';
		// endif;					

		if ($smtpPort !== '25')
			$config['ssl'] = 'tls';

		$this->_transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);
	}
	
	public function send($arg)
	{
$this->systemLogger->addInfo('--- Starting Process ---');
        $recipient = @$arg[1];
		
		$validator = new Zend_Validate_EmailAddress();
		if ($validator->isValid($recipient)) {
$this->systemLogger->addInfo('Recipient is valid : '.$recipient);

$this->systemLogger->addInfo('Sending Email');
			$sendEmailStatus = $this->sendMail('EMAIL TESTER', $this->_mailContent, array($recipient),'');
$this->systemLogger->addInfo('++ Generating SMTP LOG ++');
			$emailSendProcessRemark = $this->_transport->getConnection()->getLog();
$this->systemLogger->addInfo($emailSendProcessRemark);	
echo $emailSendProcessRemark;		
$this->systemLogger->addInfo('++ END OF SMTP LOG ++');
		} else {
$this->systemLogger->addInfo('Recipient is invalid : '.$recipient);
		}
$this->systemLogger->addInfo('--- End Process ---');
	}
	
	public function sendMail($subject,$body,$recipient)
	{
		$emailStatus = 0;

		$emailLog = "";

			$isEmptyRecipient = false;
			if(!empty($recipient)){
				try {
					$mail = new Zend_Mail();
					$mail->setFrom($this->_username,$this->_fromName);
					$mail->setSubject($subject);
					$mail->setBodyHtml($body);
					$mail->addTo($recipient,'');
					$mail->send($this->_transport);
					$emailStatus = 1;
				} catch(Exception $e) {
					$emailStatus = 0;
					$emailLog = $e;
				}	
			} else {
				$emailStatus = 0;
				$emailLog = "Empty recipient.";
				$isEmptyRecipient = true;
			}
$this->systemLogger->addInfo('++ emailLog ++');
$this->systemLogger->addInfo($emailLog);
$this->systemLogger->addInfo('++ emailLog ++');
$this->systemLogger->addInfo('MAIL STATUS : '.$emailStatus);
	}
	
}


$mail = new mailTester();
$mail->send($argv);