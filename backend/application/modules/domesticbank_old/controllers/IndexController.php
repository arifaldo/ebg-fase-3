<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Domesticbank_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');


	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

	    $fields = array(
						'clearing_code'  => array('field' => 'CLR_CODE',
											      'label' => $this->language->_('Clearing Code'),
											      'sortable' => true),
						'swift_code'     => array('field' => 'SWIFT_CODE',
											      'label' => $this->language->_('RTGS Code'),
											      'sortable' => true),
						'bank_name'      => array('field' => 'BANK_NAME',
											      'label' => $this->language->_('Bank Name'),
											      'sortable' => true),
						/*'Workfield_code'           => array('field' => 'WORKFIELD_CODE',
											      'label' => 'Workfield Code',
											      'sortable' => true),
						 'bank_office_name'      => array('field' => 'BANK_OFFICE_NAME',
											      'label' => 'Bank Office Name',
											      'sortable' => true),
						'bank_address'      => array('field' => 'BANK_ADDRESS',
											      'label' => 'Bank Address',
											      'sortable' => true),
						// 'city'           => array('field' => 'CITY',
											      // 'label' => 'City',
											      // 'sortable' => true),
						 'CITY'           => array('field' => 'CITY_CODE',
											      'label' => 'City Code',
											      'sortable' => true),
						'province_code'           => array('field' => 'PROVINCE_CODE',
											      'label' => 'Province Code',
											      'sortable' => true),
						'branch_code'           => array('field' => 'BRANCH_CODE',
											      'label' => 'Branch Code',
											      'sortable' => true),
						'branch_name'           => array('field' => 'BRANCH_NAME',
											      'label' => 'Branch Name',
											      'sortable' => true),
												  /*
						'kbi_code'           => array('field' => 'KBI_CODE',
											      'label' => 'KBI Code',
											      'sortable' => true),
						
						'pop_status_code'           => array('field' => 'POP_STATUS_CODE',
											      'label' => 'Population Status',
											      'sortable' => true),
						'res_status_code'           => array('field' => 'RES_STATUS_CODE',
											      'label' => 'Resident Status',
											      'sortable' => true), */
					/* 	'clr_status_code'           => array('field' => 'CLR_STATUS',
											      'label' => 'Clearing Status',
											      'sortable' => true),
						'cord_status_code'           => array('field' => 'CORD_STATUS_CODE',
											      'label' => 'Cord Status',
											      'sortable' => true),
						'bank_inst_code'           => array('field' => 'BANK_INST_CODE',
											      'label' => 'Bank Inst Code',
											      'sortable' => true),
						'active_date'           => array('field' => 'ACTIVE_DATE',
											      'label' => 'Active Date',
											      'sortable' => true),
						'bi_account'           => array('field' => 'BI_ACCOUNT',
											      'label' => 'BI Account',
											      'sortable' => true), */
				      );


	    $filterlist = array("CLR_CODE","SWIFT_CODE","BANK_NAME");
		
		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','BANK_NAME');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'CLR_CODE'  => array('StringTrim','StripTags'),
							'SWIFT_CODE'      => array('StringTrim','StripTags'),
							'BANK_NAME'      => array('StringTrim','StripTags')
							// 'swift_code'     => array('StringTrim','StripTags')
		);

		$dataParam = array("CLR_CODE","SWIFT_CODE","BANK_NAME");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
		
		$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_DOMESTIC_BANK_TABLE'),array_merge($getData,array('BANK_ID')))
							->where('BANK_ISMASTER <> 1');

		if($filter == TRUE || $csv || $pdf || $this->_request->getParam('print'))
		{  
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fclearing_code = $zf_filter->getEscaped('CLR_CODE');
			$fbank_name     = $zf_filter->getEscaped('BANK_NAME');
			$fswift_code    = $zf_filter->getEscaped('SWIFT_CODE');
			$fcity_code     = $zf_filter->getEscaped('CITY');

	        if($fclearing_code) $select->where('UPPER(CLR_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fclearing_code).'%'));
	        if($fbank_name)     $select->where('BANK_NAME LIKE '.$this->_db->quote('%'.strtoupper($fbank_name).'%'));
	        if($fswift_code)    $select->where('UPPER(SWIFT_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fswift_code).'%'));
	        if($fcity_code)          $select->where('UPPER(CITY_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fcity_code).'%'));
			
			$this->view->clearing_code = $fclearing_code;
			$this->view->bank_name     = $fbank_name;
			$this->view->swift_code    = $fswift_code;
			$this->view->CITY     = $fcity_code;
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);  
		
		$select = $this->_db->fetchall($select);
		
		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['BANK_ID']);
		}
		

		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
				$this->_helper->download->csv($header,$selectPdfCsv,null,'domestic_bank_list');
				Application_Helper_General::writeLog('DBLS','Download CSV Domestic Bank');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$selectPdfCsv,null,'domestic_bank_list');
				Application_Helper_General::writeLog('DBLS','Download PDF Domestic Bank');
		}
		else if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $selectPdfCsv, 'data_caption' => 'Domestic Bank', 'data_header' => $fields));
		}
		else
		{		
				Application_Helper_General::writeLog('DBLS','View Domestic Bank');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }

	}

}