<?php

class group_IndexController extends group_Model_Group{
	
   
   //constructor utk class child
	public function initController()
	{  
	  
    }
	
    public function indexAction() 
	{
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
	   
	   if(count($temp)>1)
	   {
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
    	
    	// konfigurasi master status
    	$code[] = $this->_masterStatus['code']['active'];
        $code[] = $this->_masterStatus['code']['inactive'];
        $desc[] = $this->_masterStatus['desc']['active'];
        $desc[] = $this->_masterStatus['desc']['inactive'];
        $this->view->groupStatus = array_combine($code,$desc);
    	//End konfigurasi master status
    	
        
    	$fields = array('group_id'  => array('field'    => 'BGROUP_ID',
                                             'label'    => 'Group ID',
                                             'sortable' => true),
                        'group_name'  => array('field'    => 'BGROUP_DESC',
                                               'label'    => 'Group Name',
                                               'sortable' => true),
                        'status'   => array('field'    => 'BGROUP_STATUS',
                                            'label'    => 'Status',
                                            'sortable' =>  true),
            		   );
            		
         //get page, sortby, sortdir
         $page = $this->_getParam('page');
         
         //validate parameters before passing to view and query
         $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
                    
         // mengambil nilai dari parameter sortby, jika tidak ada nilainya, nilai defaultnya adalah group_id
         $sortBy = $this->_getParam('sortby','group_id');
         $sortDir = $this->_getParam('sortdir','asc');
					
         // validasi jika nilainya tidak ada di list fields
         $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields)))) ? 
                   $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

         $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc')))) ? 
                    $sortDir : 'asc';
     
         //get filtering param
            $allNum = new Zend_Filter_Alnum(true);

            $filterArr = array('filter' 	 => array('StringTrim','StripTags'),
                               'group_id' 	 => array('StringTrim','StripTags','StringToUpper'),
            				   'group_name'	 => array('StringTrim','StripTags','StringToUpper'),
                               'status'      => array('StringTrim','StripTags','StringToUpper')
                              );
            
            $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
                
            $filter = $zf_filter->getEscaped('filter');

            $this->view->currentPage = $page;
            $this->view->sortBy = $sortBy;
            $this->view->sortDir = $sortDir;
            
            $select = array();
            
            if($filter == 'Filter')
            {
                    $groupID = html_entity_decode($zf_filter->getEscaped('group_id'));
                    $groupName = html_entity_decode($zf_filter->getEscaped('group_name'));
                    $status = $zf_filter->getEscaped('status');

					$this->view->group_name = $groupName;
                    $this->view->group_id   = $groupID;
                    $this->view->status     = $status;
            }
           
            if($filter !='')
            {
				$select = $this->_db->select()
							        ->from(array('M_BGROUP'),
							               array('BGROUP_ID', 'BGROUP_DESC', 'BGROUP_STATUS'))
                                    ->order(array($sortBy.' '.$sortDir));
            }
			
			if($filter == 'Filter')
            {
                   //where clauses
                    if($groupID)
                    	$select->where("UPPER(BGROUP_ID) LIKE ".$this->_db->quote('%'.$groupID.'%'));
					if($groupName)
                    	$select->where("UPPER(BGROUP_DESC) LIKE ".$this->_db->quote('%'.$groupName.'%'));
                    if($status)
                    	$select->where("BGROUP_STATUS = ?", (string)$status);
            }
           
            $this->paging($select);
            
            $this->view->fields = $fields;
            $this->view->filter = $filter;
            
            
            $this->backendLog('V',$this->_moduleDB,null,null,null);
			            

	}
	
	
	
}
