<?php
// rsync
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'Crypt/AESMYSQL.php';
//require_once 'General/Account.php';
require_once 'General/Settings.php';
require_once("Service/Account.php");

class echanrequest_DetailController extends customer_Model_Customer
{

	public function initController()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->button = true;
	}
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		/*$AESMYSQL = new Crypt_AESMYSQL();
		$cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
		$this->view->cust_id = $cust_id;
		*/
		$regNumber = $this->_getParam('regNumber');
		$changes_id = $this->_getParam('changes_id');

		$conf 				= Zend_Registry::get('config');
		$custmodelCode 		= $conf["cust"]["model"]["code"];
		$custmodelDesc 		= $conf["cust"]["model"]["desc"];
		$arrcustmodel 		= array_combine(array_values($custmodelCode), array_values($custmodelDesc));

		// echo '<pre>';
		// print_r($arrcustmodel);
		$customerstatus = $this->_customerstatus;
		$options = array_combine(array_values($customerstatus['code']), array_values($customerstatus['desc']));

		$setting = new Settings();
		$system_type1 = $setting->getSetting('system_type');

		// echo '<pre>';
		// print_r($system_type1);
		// echo "<BR><code>system_type1 = $system_type1</code><BR>";
		$this->view->system_type1 = $system_type1;

		$tokenType = $setting->getSetting('tokentype');

		$tokenTypeCode = array_flip($this->_tokenType['code']);
		$tokenTypeDesc = $this->_tokenType['desc'];

		$this->view->tokenTypeText = $tokenTypeDesc[$tokenTypeCode[$tokenType]];

		// ----------------------------------------------------COMPANY DETAIL----------------------------------------------------------------------------------

		$listYesNo = array(0 => 'No', 1 => 'Yes');

		$caseCUST_MONTHLYFEE_STATUS = "(CASE CUST_MONTHLYFEE_STATUS ";
		foreach ($listYesNo as $key => $val) {
			$caseCUST_MONTHLYFEE_STATUS .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseCUST_MONTHLYFEE_STATUS .= " END)";

		$caseCUST_CHARGES_STATUS = "(CASE CUST_CHARGES_STATUS ";
		foreach ($listYesNo as $key => $val) {
			$caseCUST_CHARGES_STATUS .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseCUST_CHARGES_STATUS .= " END)";


		$caseCUST_SECURITIES = "(CASE CUST_SECURITIES ";
		foreach ($listYesNo as $key => $val) {
			$caseCUST_SECURITIES .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseCUST_SECURITIES .= " END)";


		$caseModel = "(CASE CUST_MODEL ";
		foreach ($arrcustmodel as $key => $val) {
			$caseModel .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseModel .= " END)";

		$caseStatus = "(CASE CUST_STATUS ";
		foreach ($options as $key => $val) {
			$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseStatus .= " END)";

		$caseWhenRequestChangeList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN '" . $this->language->_('Request Repaired') . "'
			 WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Unread Suggestion') . "'
			 WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Read Suggestion') . "'
			 WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Repaired Suggestion') . "'
			 END
			";

		$caseWhenRegisStatusList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN '" . $this->language->_('Request Repaired') . "'
			 WHEN (G.CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Waiting for approve') . "'
			 WHEN (G.CHANGES_STATUS = 'AP') THEN '" . $this->language->_('Approved') . "'
			 WHEN (G.CHANGES_STATUS = 'RJ') THEN '" . $this->language->_('Reject') . "'
			 END
			";

		$select	= $this->_db->select()
			->FROM(
				array('CUST' => 'TEMP_ONLINECUST_BIS'),
				array(
					'CustomerID' => 'CUST.CUST_ID',
					'CUST_MODEL' => $caseModel,
					'CUST_STATUS' => $caseStatus,
					'CUST_SECURITIES' => $caseCUST_SECURITIES,
					'CUST_SECURITIES1' => 'CUST_SECURITIES',
					'CUST_REG_NUMBER',
					'CUST_NAME',
					'CUST_CIF',
					'CUST_TYPE',
					'CUST_WORKFIELD',
					'CUST_CITY',
					'CUST_ADDRESS',
					'CUST_ZIP',
					'CUST_PROVINCE',
					'CUST_CONTACT_PHONE',
					'CUST_PHONE',
					'CUST_EXT',
					'CUST_FAX',
					'CUST_EMAIL',
					'CUST_WEBSITE',
					'CUST_CONTACT',
					'CUST_MONTHLYFEE_STATUS' => $caseCUST_MONTHLYFEE_STATUS,
					'CUST_CHARGES_STATUS' => $caseCUST_CHARGES_STATUS,
					'CUST_LIMIT_IDR',
					'CUST_LIMIT_USD',
					'CUST_FINANCE',
					'CUST_CREATED',
					'CUST_CREATEDBY' => 'CUST_UPDATEDBY',
					'CUST_UPDATED',
					'CUST_UPDATEDBY',
					'CUST_SUGGESTED',
					'CUST_SUGGESTEDBY',
					'CUST_CODE',
					'CUST_REVIEW',
					'CUST_APPROVER',
					'CUST_SAME_USER',
					'CUST_APP_TOKEN',
					'CUST_MODEL_VAL' => 'CUST_MODEL',
					'CUST_NPWP',
					'CUST_VILLAGE',
					'CUST_DISTRICT',
					'BUSINESS_TYPE',
					'GO_PUBLIC',
					'GRUP_BUMN',
					'DEBITUR_CODE',
					'COLLECTIBILITY_CODE',
					'suggestion_status' => new Zend_Db_Expr($caseWhenRequestChangeList),
					'registration_status' => new Zend_Db_Expr($caseWhenRegisStatusList),
					'G.CHANGES_REASON'
					// 'CUST.*', 
				)
			)

			->joinleft(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = CUST.CHANGES_ID', array('*'))
			->joinleft(array('COUNTRY' => 'M_COUNTRY'), 'CUST.COUNTRY_CODE = COUNTRY.COUNTRY_CODE', array('COUNTRY_NAME'))
			->joinleft(array('P' => 'M_PRELIMINARY'), 'P.CUST_ID = CUST.CUST_ID', array("*"))
			->joinleft(array('PM' => 'M_PRELIMINARY_MEMBER'), 'PM.CUST_ID = CUST.CUST_ID', array("*"))
			->joinleft(array('C' => 'M_CITYLIST'), 'C.CITY_CODE = CUST.CUST_CITY', array("*"))
			->WHERE('CUST.CUST_REG_NUMBER = ?', $regNumber)
			->WHERE('CUST.CHANGES_ID = ?', $changes_id);
		//die();
		//echo "<BR><code>select = $select</code><BR>";   
		$resultcompanydetail = $this->_db->fetchRow($select);

		$this->view->companydetail = $resultcompanydetail;




		$this->view->cust_rls_token  = 1;

		$cust_id =  $resultcompanydetail['CustomerID'];

		$select2 = $this->_db->select()
			->FROM(
				array('CUST' => 'T_ONLINECUST_BIS_USER')
			)
			->WHERE('CUST.CUST_ID = ?', $cust_id);

		$resultcompanydetailadmin = $this->_db->fetchRow($select2);
		$this->view->companydetailadmin = $resultcompanydetailadmin;


		if ($cust_id) {

			$this->_setReadSuggestion($changes_id);
			if ($resultcompanydetail['CUST_MODEL_VAL'] == '1') {
				$model = new linefacility_Model_Linefacility();
				$data = $model->getDataByCustId($cust_id);
				$this->view->data = $data;

				$segmentationArr = [
					1 => 'CORPORATE',
					2 => 'COMMERCIAL',
					3 => 'SME',
					4 => 'FINANCIAL INSTITUTION'
				];
				$this->view->segmentationArr = $segmentationArr;
			}
			if ($resultcompanydetail['CUST_MODEL_VAL'] == '3') {
				$model                              	= new specialobligee_Model_Specialobligee();
				$filterParam = array();
				$filterParam['fCompanyCode'] 			= $cust_id;
				$dataSp                               	= $model->getData($filterParam, true);

				$this->view->dataSp                   	= $dataSp['0'];

				$select = $this->_db->select()
					->from(
						array('A' => 'M_PRELIMINARY'),
						array('*')
					)
					->where('A.CUST_ID = ?', $cust_id);
				$datapre = $this->_db->fetchRow($select);

				$datapre['CUST_NAME'] = $resultcompanydetail['CUST_NAME'];
				$this->view->datapre = $datapre;


				$select = $this->_db->select()
					->from(
						array('A' => 'M_PRELIMINARY_MEMBER'),
						array('*')
					)
					->where('A.CUST_ID = ?', $cust_id);
				$dataMember = $this->_db->fetchAll($select);
				$this->view->dataMember = $dataMember;

				$getCustAcctById                    = $model->getCustAcctById($cust_id);
				$debitedAcctArr                     = [];
				foreach ($getCustAcctById as $row) {
					$debitedAcctArr[$row['ACCT_NO']]   = $row['ACCT_NO'] . ' (' . $row['CCY_ID'] . ')' . ' / ' . $row['ACCT_NAME'] . ' / ' . $row['ACCT_DESC'];
				}
				$this->view->debitedAcctArr         = $debitedAcctArr;
			}


			$this->view->businessEntityArr = Application_Helper_Array::listArray($this->getBusinessEntity(), 'BUSINESS_ENTITY_CODE', 'BUSINESS_ENTITY_DESC');
			$this->view->companyTypeArr = Application_Helper_Array::listArray($this->getCompanyType(), 'COMPANY_TYPE_CODE', 'COMPANY_TYPE_DESC');

			$this->view->debiturCodeArr = Application_Helper_Array::listArray($this->getDebitur(), 'DEBITUR_CODE', 'DEBITUR_DESC');
			//$this->view->collectCodeArr = Application_Helper_Array::listArray($this->getCreditQuality(),'CODE','DESCRIPTION');

			$entityCode = $conf["bg"]["entity"]["code"];
			$entityDesc = $conf["bg"]["entity"]["desc"];
			$entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));
			$this->view->entityArr = $entityArr;

			$statusCode = $conf["bg"]["statusowner"]["code"];
			$statusDesc = $conf["bg"]["statusowner"]["desc"];
			$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));
			$this->view->statusArr = $statusArr;

			$select = $this->_db->select()
				->from(
					array('A' => 'M_POSITION'),
					array('*')
				);
			$position    = $this->_db->fetchAll($select);
			$positionArr = Application_Helper_Array::listArray($position, 'CODE', 'POSITION');
			$this->view->positionArr = $positionArr;

			$M_PRELIMINARY_MEMBER = $this->_db->select()
				->from(
					array('A' => 'M_PRELIMINARY_MEMBER'),
					array('*')
				)
				->where('A.CUST_ID = ?', $cust_id);
			$M_PRELIMINARY_MEMBER = $this->_db->fetchAll($M_PRELIMINARY_MEMBER);
			$this->view->M_PRELIMINARY_MEMBER = $M_PRELIMINARY_MEMBER;

			// echo '<pre>';
			// print_r($M_PRELIMINARY_MEMBER);
			//get adapter profile data
			$select = $this->_db->select()
				->from(array('H' => 'M_CUST_ADAPTER_PROFILE'), array('*'))
				->where('H.CUST_ID = ' . $this->_db->quote($cust_id));

			$profileData = $this->_db->fetchAll($select);

			foreach ($profileData as $key => $value) {
				if ($value['ADAPTER_PROFILE_ID'] !== 'defaults') {
					//get adapter profile data
					$selectAdapterName = $this->_db->select()
						->from(array('M_ADAPTER_PROFILE'), array('PROFILE_NAME'))
						->where('PROFILE_ID = ' . $this->_db->quote($value['ADAPTER_PROFILE_ID']));

					$profileName = $this->_db->fetchAll($selectAdapterName);

					$profileData[$key]['PROFILE_NAME'] = $profileName[0]['PROFILE_NAME'];
				} else {
					$profileData[$key]['PROFILE_NAME'] = 'default';
				}
			}

			foreach ($profileData as $key => $value) {
				if ($value['TRF_TYPE'] == 'payroll') {
					$this->view->payrollAdapter = $value['PROFILE_NAME'];
				}

				if ($value['TRF_TYPE'] == 'manytomany') {
					$this->view->manytomanyAdapter = $value['PROFILE_NAME'];
				}

				if ($value['TRF_TYPE'] == 'emoney') {
					$this->view->emoneyAdapter = $value['PROFILE_NAME'];
				}

				if ($value['TRF_TYPE'] == 'onetomany') {
					$this->view->onetomanyAdapter = $value['PROFILE_NAME'];
				}

				if ($value['TRF_TYPE'] == 'manytoone') {
					$this->view->manytooneAdapter = $value['PROFILE_NAME'];
				}
			}

			$selectMaker = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where("(FPRIVI_ID = 'CRSP' OR FPRIVI_ID = 'CRVA' OR FPRIVI_ID = 'CDFT') AND FUSER_ID LIKE ?", "%" . (string)$resultcompanydetail['CUST_ID'] . "%")
				->group('FUSER_ID');

			$userMaker = $this->_db->fetchAll($selectMaker);

			foreach ($userMaker as $row) {
				$userIdMaker = explode($resultcompanydetail['CUST_ID'], $row['FUSER_ID']);

				//get user name
				$selectMakerName = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", (string)$userIdMaker[1]);

				$userMakerName = $this->_db->fetchAll($selectMakerName);

				$MakerList .= $userMakerName[0]['USER_FULLNAME'] . '<br>';
			}

			$imgClass = '';
			if ($resultcompanydetail['CUST_FINANCE'] == '3') {
				$imgClass = "imgSepia";
			}

			// var_dump($MakerList);exit();
			/*$MakerNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="'.$imgClass.'" src="/assets/themes/assets/newlayout/img/maker.PNG">
                  <span class="hovertextcontent" style="left: -70px;">'.$MakerList.'</span></a>';*/
			$MakerNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/maker.PNG">
                  </a>';

			$this->view->makerNameCircle = $MakerNameCircle;

			$selectReviewer = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where("FPRIVI_ID = 'RVPV' AND FUSER_ID LIKE ?", "%" . (string)$resultcompanydetail['CUST_ID'] . "%");

			$userReviewer = $this->_db->fetchAll($selectReviewer);

			foreach ($userReviewer as $row) {
				$userIdReviewer = explode($resultcompanydetail['CUST_ID'], $row['FUSER_ID']);

				//get user name
				$selectReviewerName = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", (string)$userIdReviewer[1]);

				$userReviewerName = $this->_db->fetchAll($selectReviewerName);

				$ReviewerList .= $userReviewerName[0]['USER_FULLNAME'] . '<br>';
			}

			// var_dump($ReviewerList);exit();
			/*$ReviewerNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="'.$imgClass.'" src="/assets/themes/assets/newlayout/img/reviewer.png">
                  <span class="hovertextcontent" style="left: -100px;">'.$ReviewerList.'</span></a>';*/
			$ReviewerNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/reviewer.png">
                  </a>';

			$this->view->reviewerNameCircle = $ReviewerNameCircle;

			$selectApprover = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where("FPRIVI_ID = 'PAPV' AND FUSER_ID LIKE ?", "%" . (string)$resultcompanydetail['CUST_ID'] . "%");

			$userApprover = $this->_db->fetchAll($selectApprover);

			foreach ($userApprover as $row) {
				$userIdApprover = explode($resultcompanydetail['CUST_ID'], $row['FUSER_ID']);

				//get user name
				$selectApproverName = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", (string)$userIdApprover[1]);

				$userApproverName = $this->_db->fetchAll($selectApproverName);

				$ApproverList .= $userApproverName[0]['USER_FULLNAME'] . '<br>';
			}

			// var_dump($ApproverList);exit();
			/*$ApproverNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="'.$imgClass.'" src="/assets/themes/assets/newlayout/img/approver.png">
                  <span class="hovertextcontent" style="left: -100px;">'.$ApproverList.'</span></a>';
				  */
			$ApproverNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/approver.png">
                  </a>';

			$this->view->approverNameCircle = $ApproverNameCircle;

			$selectReleaser = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where("FPRIVI_ID = 'PRLP' AND FUSER_ID LIKE ?", "%" . (string)$resultcompanydetail['CUST_ID'] . "%");

			$userReleaser = $this->_db->fetchAll($selectReleaser);

			foreach ($userReleaser as $row) {
				$userIdReleaser = explode($resultcompanydetail['CUST_ID'], $row['FUSER_ID']);

				//get user name
				$selectReleaserName = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", (string)$userIdReleaser[1]);

				$userReleaserName = $this->_db->fetchAll($selectReleaserName);

				$releaserList .= $userReleaserName[0]['USER_FULLNAME'] . '<br>';
			}

			// var_dump($releaserList);exit();
			/*$releaserNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="'.$imgClass.'" src="/assets/themes/assets/newlayout/img/releaser.png">
                  <span class="hovertextcontent" style="left: -100px;">'.$releaserList.'</span></a>';*/
			$releaserNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/releaser.png">
                  </a>';

			$this->view->releaserNameCircle = $releaserNameCircle;

			// ----------------------------------------------------END COMPANY DETAIL----------------------------------------------------------------------------------

			// --------------------------------------------------------BANK ACCOUNT----------------------------------------------------------------------------------------

			$bankacc = $this->getCustomerAcct($cust_id);

			$this->view->bankacc = $bankacc;

			// --------------------------------------------------END BANK ACCOUNT-----------------------------------------------------------------------------------

			// ------------------------------------------------------USER ACCOUNT---------------------------------------------------------------------------------------

			//$useracc = $this->getUserAcct($cust_id);
			//Zend_Debug::dump($selectuseracc);Zend_Debug::dump($useracc);die;
			//$this->view->useracc = $useracc;

			// --------------------------------------------------END USER ACCOUNT-----------------------------------------------------------------------------------

			// ------------------------------------------------------------USER LIMIT---------------------------------------------------------------------------------------

			//$userlimit = $this->getUserLimit($cust_id);
			//$this->view->userlimit = $userlimit;

			// ---------------------------------------------------------END USER LIMIT-----------------------------------------------------------------------------------

			// ------------------------------------------------------USER DAILY LIMIT-----------------------------------------------------------------------------------

			//$userdailylimit = $this->getUserDailyLimit($cust_id);
			//$this->view->userdailylimit = $userdailylimit;

			// --------------------------------------------------END USER DAILY LIMIT--------------------------------------------------------------------------------	

			// ------------------------------------------------------USER BENEF BANK ACC-----------------------------------------------------------------------------------
			//$this->view->benefBankAcc    = $this->getBenefBankAcc($cust_id);
			// --------------------------------------------------END USER BENEF BANK ACC--------------------------------------------------------------------------------	

			// ------------------------------------------------------USER BENEF USER-----------------------------------------------------------------------------------
			//$this->view->benefUser  		  = $this->getBenefUser($cust_id);

			//$this->view->benefAccount   	= $this->getBenefAccount($cust_id);

			$colomn = array('account_number', 'account_name', 'account_currency');
			$acctlist = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('A' => 'M_APIKEY'))
					->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
					->join(array('B' => 'M_BANK_TABLE'), 'A.BANK_CODE = B.BANK_CODE', array('bankname' => 'B.BANK_NAME'))
					//    ->group('A.APIKEY_ID')
					->where('UPPER(A.CUST_ID)=' . $this->_db->quote((string)$cust_id))
					->where('A.`FIELD` IN (?)', $colomn)
					->order('A.APIKEY_ID ASC')

			);

			$account = array();
			foreach ($acctlist as $key => $value) {
				$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
				$account[$value['ID']]['bankname'] = $value['bankname'];
				$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			}

			$datapers = array();

			$totalonline = 0;

			foreach ($account as $key => $request) {

				$datapers[$key - 1]['bank_name']  = $request['bankname'];
				$datapers[$key - 1]['bank_code']  = $request['BANK_CODE'];
				$datapers[$key - 1]['account_number']  = $request['account_number'];
				$datapers[$key - 1]['account_name']  = $request['account_name'];
				if (isset($request['account_alias'])) {
					$datapers[$key - 1]['account_alias']  = $request['account_alias'];
				} else {
					$datapers[$key - 1]['account_alias']  = '-';
				}
				$datapers[$key - 1]['account_currency']  = 'IDR';
				$datapers[$key - 1]['account_status'] = '1';
			}

			$total = count($datapers);

			$acctmanual = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('A' => 'T_BALANCE'))
					->join(array('C' => 'M_BANK_TABLE'), 'A.BANK_CODE = C.BANK_CODE', array('bankname' => 'C.BANK_NAME'))
					->where('A.ACCT_STATUS = ?', '5')
					->where('UPPER(A.CUST_ID)=' . $this->_db->quote((string)$cust_id))
					->group("A.ACCT_NO")
					->group("A.BANK_CODE")
					->limit(100)
			);
			// echo $acctmanual;die;
			$totalmanual = 0;
			foreach ($acctmanual as $key => $value) {
				$datapersmanual[$total]['bank_code']  = $value['BANK_CODE'];
				$datapersmanual[$total]['bank_name']  = $value['bankname'];
				$datapersmanual[$total]['account_number']  = $value['ACCT_NO'];
				$datapersmanual[$total]['account_currency']  = $value['CCY_ID'];
				$datapersmanual[$total]['account_alias']  = $value['ACCT_ALIAS'];
				$datapersmanual[$total]['account_name']  = $value['ACCT_ALIAS'];
				$datapersmanual[$total]['account_status'] = $value['ACCT_STATUS'];
				$total++;
			}

			if (count($datapersmanual) > 0) {
				$datapersonal = array_merge($datapers, $datapersmanual);
			} else {
				$datapersonal = $datapers;
			}

			$this->view->datapersonal = $datapersonal;

			// --------------------------------------------------END BENEF USER--------------------------------------------------------------------------------	


			// --------------------------------------------------APPROVAL MATRIX--------------------------------------------------------------------------------	
			$selectUsergroup = $this->_db->select()
				->from(array('M_APP_GROUP_USER'), array('*'))
				->where('CUST_ID = ?', $cust_id)
				->group('GROUP_USER_ID')
				// echo $selectUser;die;
				->query()->fetchall();

			$selectUsers = $this->_db->select()
				->from(array('M_APP_GROUP_USER'), array('*'))
				->where('CUST_ID = ?', $cust_id)
				// echo $selectUser;die;
				->query()->fetchall();
			// print_r($selectUser);die;

			$userlists = '';
			foreach ($selectUsergroup as $key => $value) {
				foreach ($selectUsers as $no => $val) {
					if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
						if (empty($userlists)) {
							$userlists .= $val['USER_ID'];
						} else {
							$userlists .= ', ' . $val['USER_ID'];
						}
					}
				}
				$selectUsergroup[$key]['USER'] .= $userlists;
				$userlists = '';
				$spesials = 'S_' . $cust_id;
				if ($value['GROUP_USER_ID'] == $spesials) {
					$selectUsergroup[$key]['GID'] .= 'SG';
				} else {
					$groups = explode('_', $value['GROUP_USER_ID']);
					$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
					// $cust = explode('_', $value['GROUP_USER_ID'])
					$selectUsergroup[$key]['GID'] .= $alphabets[(int)$groups[2]];
				}
			}
			$this->view->selectUsergroup = $selectUsergroup;


			$select = $this->_db->select()
				->from(array('MAB' => 'M_APP_BOUNDARY'))
				->join(array('MABG' => 'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
				->where('MAB.CUST_ID = ?', (string)$cust_id)
				->group('BOUNDARY_ID');
			// echo $select;
			$result = $this->_db->fetchAll($select);

			$dataArr = array();
			// $alf = 'A';
			// $alf++;
			// ++$alf;
			// print_r((int)$alf);
			// $nogroup = sprintf("%02d", 1);
			// print_r($result);die;

			foreach ($result as $row) {
				list($grouptype, $groupname, $groupnum) = explode("_", $row['GROUP_USER_ID']);
				if ($grouptype == 'N') {
					$name = 'Group ' . trim($groupnum, '0');
				} else {
					$name = 'Special Group';
				}

				$selectUser = $this->_db->select()
					->from(array('M_APP_GROUP_USER'), array('USER_ID'))
					->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
					// echo $selectUser;die;
					->query()->fetchall();

				$userlist = '';

				$policythen = explode(' THEN ', $row['POLICY']);

				foreach ($policythen as $keythen => $valuethen) {
					$policy = explode(' AND ', $valuethen);

					foreach ($policy as $key => $value) {
						$replaceVal = str_replace('(', '', $value);
						$replaceVal = str_replace(')', '', $replaceVal);
						$policy[$key] = $replaceVal;
					}

					foreach ($policy as $key => $value) {
						if ($value == 'SG') {
							$group = 'S_' . $cust_id;
							$selectUser = $this->_db->select()
								->from(array('M_APP_GROUP_USER'), array('USER_ID'))
								->where('GROUP_USER_ID = ?', $group)
								// echo $selectUser;die;
								->query()->fetchall();
							foreach ($selectUser as $val) {
								if (empty($userlist)) {
									$userlist .= $val['USER_ID'];
								} else {
									$userlist .= ', ' . $val['USER_ID'];
								}
							}
						} else {
							$alphabet = array("A" => 1, "B" => 2, "C" => 3, "D" => 4, "E" => 5, "F" => 6, "G" => 7, "H" => 8, "I" => 9, "J" => 10, "K" => 11, "L" => 12, "M" => 13, "N" => 14, "O" => 15, "P" => 16, "Q" => 17, "R" => 18, "S" => 19, "T" => 20, "U" => 21, "V" => 22, "W" => 23, "X" => 24, "Y" => 25, "Z" => 26,);

							$policyor = explode(' OR ', $value);

							// print_r($policyor);die;
							foreach ($policyor as $numb => $valpol) {
								if ($valpol == 'SG') {
									$group = 'S_' . $cust_id;
									$selectUser = $this->_db->select()
										->from(array('M_APP_GROUP_USER'), array('USER_ID'))
										->where('GROUP_USER_ID = ?', $group)
										// echo $selectUser;die;
										->query()->fetchall();
									foreach ($selectUser as $val) {
										if (empty($userlist)) {
											$userlist .= $val['USER_ID'];
										} else {
											$userlist .= ', ' . $val['USER_ID'];
										}
									}
								} else {
									$nogroup = sprintf("%02d", $alphabet[$valpol]);
									// print_r($valpol);
									$group = 'N_' . $cust_id . '_' . $nogroup;
									$selectUser = $this->_db->select()
										->from(array('M_APP_GROUP_USER'), array('USER_ID'))
										->where('GROUP_USER_ID = ?', $group)
										// echo $selectUser;die;
										->query()->fetchall();
									// print_r($selectUser);  
									foreach ($selectUser as $val) {
										if (empty($userlist)) {
											$userlist .= $val['USER_ID'];
										} else {
											$userlist .= ', ' . $val['USER_ID'];
										}
									}
								}
							}
						}
					}
				}

				$arrTraType     = array_combine($this->_paymenttype["code"], $this->_paymenttype["desc"]);
				$arrTraType['19'] = 'Cash Pooling Same Bank';
				$arrTraType['20'] = 'Cash Pooling Same Bank';
				$arrTraType['23'] = 'Cash Pooling Other Bank';

				$userl = explode(', ', $userlist);
				$userlist = array_unique($userl);
				$userlistnew = '';

				foreach ($userlist as $ky  => $val) {
					//var_dump($ky);
					if ($ky == 0) {
						$userlistnew .= $val;
						//var_dump($userlistnew);
					} else {
						$userlistnew .= ', ' . $val;
					}
				}

				$changepolicy = explode(" ", $row['POLICY']);
				$data = array();
				foreach ($changepolicy as $keypolicy => $valpolicy) {
					if ($valpolicy == 'AND') {
						$valpolicy = '<span style="color: blue;">and</span>';
					} elseif ($valpolicy == 'OR') {
						$valpolicy = '<span style="color: blue;">or</span>';
					} elseif ($valpolicy == 'THEN') {
						$valpolicy = '<span style="color: blue;">then</span>';
					}

					$replacepolicy1 = str_replace('(', '<span style="color: blue;">(</span>', $valpolicy);
					$replacepolicy2 = str_replace(')', '<span style="color: blue;">)</span>', $replacepolicy1);

					$data[$keypolicy] = $this->language->_($replacepolicy2);
				}

				$valpolicy2 = implode(" ", $data);

				$dataArr[] = array(
					'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
					'CURRENCY' => $row['CCY_BOUNDARY'],
					'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN']) . " - " . Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
					'GROUP_NAME' => $valpolicy2,
					'USERS' => $userlistnew
				);
			}

			$this->view->dataBoundary = $dataArr;


			// -------------------------------------------------- END APPROVAL MATRIX--------------------------------------------------------------------------------

			// --------------------------------------------------------VERIFIER GROUP-----------------------------------------------------------------------------------

			/*$verifiergroup = $this->getAppGroup($cust_id);
			$verGroup = '';
			foreach($verifiergroup as $param)
			{
				if(isset($verGroup[$param['GROUP_USER_ID']])) 
				{
					$verGroup[$param['GROUP_USER_ID']] = $verGroup[$param['GROUP_USER_ID']].' , '.$param['USER_ID'];
				}
				else
				{
					$verGroup[$param['GROUP_USER_ID']] = $param['USER_ID'];
				}
			}
			$this->view->verifiergroup = $verGroup;*/

			// ----------------------------------------------------END VERIFIER GROUP--------------------------------------------------------------------------------

			// --------------------------------------------VERIFIER GROUP BOUNDARY-----------------------------------------------------------------------------

			$getAppBoundaryGroup =	$this->_db->SELECT()
				->FROM(array('MAB' => 'M_APP_BOUNDARY'), array('*'))
				->JOINLEFT(array('MABG' => 'M_APP_BOUNDARY_GROUP'), 'MABG.BOUNDARY_ID=MAB.BOUNDARY_ID', array('*'))
				->WHERE('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
				->WHERE('BOUNDARY_ISUSED = 1')
				->ORDER('BOUNDARY_MIN ASC')
				// ->GROUP('CCY_BOUNDARY')
				// ->GROUP('BOUNDARY_MIN')
				// ->GROUP('BOUNDARY_MAX')
				->QUERY()->FETCHALL();
			// echo '<pre>';
			// print_r($getAppBoundaryGroup);die;
			$verifiergroupbound = array();
			foreach ($getAppBoundaryGroup as $row) {
				$group_desc = null;
				//example : N_CUSTCHRIS_01 & S_CUSTCHRIS
				$explodeGroup = explode('_', $row['GROUP_USER_ID']);

				if ($explodeGroup[0] == 'N')      $group_desc = 'Group ' . (int)$explodeGroup[2];
				else if ($explodeGroup[0] == 'S') $group_desc = 'Special Group';

				$key = $row['BOUNDARY_ID'];
				$verifiergroupbound[$key]['GROUP_USER_ID'][] 	= $group_desc;
				$verifiergroupbound[$key]['BOUNDARY_MIN']    	= $row['BOUNDARY_MIN'];
				$verifiergroupbound[$key]['BOUNDARY_MAX']    = $row['BOUNDARY_MAX'];
				$verifiergroupbound[$key]['CCY_BOUNDARY']    	= $row['CCY_BOUNDARY'];
			}
			$this->view->verifiergroupbound = $verifiergroupbound;

			$this->view->status_type = $this->_customerstatus;
			$pdf = $this->_getParam('pdf');

			// if(!empty($pdf))
			if ($pdf || $this->_request->getParam('print')) {
				//DIE('A');
				$this->view->button = false;
				$MakerNameCirclePDF = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/maker.PNG">
					<span class="hovertextcontent" style="left: -70px;"></span></a>';

				$this->view->MakerNameCirclePDF = $MakerNameCirclePDF;
				$ReviewerNamePDF = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/reviewer.png">
					<span class="hovertextcontent" style="left: -100px;"></span></a>';
				$this->view->ReviewerNamePDF = $ReviewerNamePDF;
				$ApproverNameCirclePDF = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/approver.png">
				<span class="hovertextcontent" style="left: -100px;"></span></a>';
				$this->view->ApproverNameCirclePDF = $ApproverNameCirclePDF;
				$releaserNameCirclePDF = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/releaser.png">
					<span class="hovertextcontent" style="left: -100px;"></span></a>';
				$this->view->releaserNameCirclePDF = $releaserNameCirclePDF;
				if ($pdf) {
					//die('a');
					$viewHtml = $this->view->render('detail/index.phtml');


					$viewHtml 	= str_ireplace($MakerNameCircle, $MakerNameCirclePDF, $viewHtml);
					$viewHtml 	= str_ireplace($ReviewerName, $ReviewerNamePDF, $viewHtml);
					$viewHtml 	= str_ireplace($ApproverNameCircle, $ApproverNameCirclePDF, $viewHtml);
					$viewHtml 	= str_ireplace($releaserNameCircle, $releaserNameCirclePDF, $viewHtml);
					// echo "<BR><code>viewHtml = $viewHtml</code><BR>"; die;	
					$viewHtml 	= str_ireplace('table-fixed', '', $viewHtml);
					$outputHTML = "<tr><td>" . $viewHtml . "</td></tr>";
					// if(strlen($outputHTML) < 22000)
					{
						Application_Helper_General::writeLog('APOR', 'Download customer register online . Register Number : ( ' . $regNumber . ' )');
						$this->_helper->download->pdfModif(null, null, null, 'Customer Detail ( ' . $cust_id . ' )', $outputHTML);
					}
					// else{
					// $this->view->button = true;
					// $this->view->errmsg ="PDF too large";
					// }

				} else if ($this->_request->getParam('print') == 1) {
					$this->_forward('printcustomerdetailall', 'index', 'widget');
				}
			}
			// -----------------------------------------END VERIFIER GROUP BOUNDARY-------------------------------------------------------------------------

			$submit_approve = $this->_getParam('submit_approve');


			// if(!empty($pdf))
			if ($submit_approve) {

				$select = $this->_db->select()
					->from(array('A' => 'T_GLOBAL_CHANGES'))
					->where('A.CHANGES_ID = ?', $changes_id);
				$result = $this->_db->fetchRow($select);

				//echo "<pre>";
				//var_dump($result);die;
				try {
					if ($result['CHANGES_TYPE'] == 'N') {

						// INSERT M_USER
						$user = $this->_db->select()
							->from(array('A' => 'T_ONLINECUST_BIS_USER'))
							->where('A.CUST_ID = ?', $cust_id);
						$user = $this->_db->fetchRow($user);

						if ($user) {

							$this->_db->beginTransaction();
							try {
								$str = rand();
								$rand = md5($str);
								$insertArr2['CUST_ID'] = $user["CUST_ID"];
								$insertArr2['USER_ID'] = $user["USER_ID"];
								$insertArr2['USER_EMAIL'] = $user["USER_EMAIL"];
								$insertArr2['USER_FULLNAME'] = $user["USER_FULLNAME"];
								$insertArr2['USER_PHONE'] = $user["USER_PHONE"];
								$insertArr2['USER_EXT'] = $user["USER_EXT"];
								$insertArr2['USER_STATUS'] = 2;

								$insertArr2['USER_CREATED']     = new Zend_Db_Expr('now()');
								$insertArr2['USER_CREATEDBY']   =  $this->_userIdLogin;
								$insertArr2['USER_UPDATED']     = new Zend_Db_Expr('now()');
								$insertArr2['USER_UPDATEDBY']   =  $this->_userIdLogin;
								$insertArr2['USER_ISNEW']       = 1;
								$insertArr2['USER_DATEPASS']     = new Zend_Db_Expr('now() + INTERVAL 1 DAY');
								$insertArr2['USER_CODE']       	= $rand;

								$this->_db->insert('M_USER', $insertArr2);
								$this->_db->commit();

								// send mail
								$subject = "New User Account Information";
								$setting = new Settings();
								$templateEmailMasterBankName = $setting->getSetting('master_bank_name');
								$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
								$templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
								$templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
								$templateEmailMasterBankWapp = $setting->getSetting('master_bank_wapp');
								$template = $setting->getSetting('femailtemplate_newuser');
								$url_fo = $setting->getSetting('url_fo');

								$date = date('Y-m-d h:i:s', strtotime("+1 days"));
								$template = str_ireplace('[[user_fullname]]', $user['USER_FULLNAME'], $template);
								$template = str_ireplace('[[comp_accid]]', $user['CUST_ID'], $template);

								// $template = str_ireplace('[[buser_name]]',$user['USER_FULLNAME'],$template);
								$template = str_ireplace('[[user_login]]', $user['USER_ID'], $template);
								// $template = str_ireplace('[[user_email]]',$user['USER_EMAIL'],$template);
								$template = str_ireplace('[[user_status]]', 'Approved', $template);
								$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
								$template = str_ireplace('[[exp_date]]', $datenow, $template);


								$template = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankEmail, $template);
								$template = str_ireplace('[[master_bank_telp]]', $templateEmailMasterBankTelp, $template);
								$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
								$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
								$template = str_ireplace('[[master_bank_wapp]]', $templateEmailMasterBankWapp, $template);
								// define("ENCRYPTION_KEY", "!@#$%^&*");
								// $string = "This is the original data string!";

								$cust = ($this->sslEnc($user['CUST_ID']));
								$userid = ($this->sslEnc($user['USER_ID']));

								$newPassword = $url_fo . '/pass/index?safetycheck=&code=' . urldecode($rand) . '&cust_id=' . urlencode($cust) . '&user_id=' . urlencode($userid) . '&code=' . urlencode($rand);

								$template = str_ireplace('[[confirm_link]]', $newPassword, $template);

								//echo $template;
								//die();

								$email = $user['USER_EMAIL'];

								if ($email != '') {
									$result = Application_Helper_Email::sendEmail($email, $subject, $template);
								}


								//$sendMailNotif = $this->sendMailNotif($user);

							} catch (Exception $error) {
								$this->_db->rollBack();
								var_dump($error);
								$this->view->error = true;
								$this->view->report_msg = 'Error : Failed Insert Data M_USER';
							}
						}

						//

						// INSERT M_CUSTOMER_ACCT

						$acctArray	= $this->_getParam('acct');
						$ORDER_NO = 0;
						foreach ($acctArray as $key => $value) {
							$svcAccount = new Service_Account($value, null);
							$resultAcctInfo = $svcAccount->inquiryAccontInfo('AI', TRUE);
							if ($resultAcctInfo['response_desc'] == 'Success') //00 = success
							{
								$svcAccountCIF = new Service_Account($resultAcctInfo['account_number'], null, null, null, null, $resultAcctInfo['cif']);
								$resultCIF = $svcAccountCIF->inquiryCIFAccount();

								$filterBy = $resultAcctInfo['account_number']; // or Finance etc.
								$new = array_filter($resultCIF['accounts'], function ($var) use ($filterBy) {
									return ($var['account_number'] == $filterBy);
								});

								$singleArr = array();
								foreach ($new as $key => $val) {
									$singleArr = $val;
								}
							}
							$returnCore  = array_merge($resultAcctInfo, $singleArr);

							if ($returnCore['status'] == '1' || $singleArr['status'] == '4') {
								$custAcct = $this->_db->select()
									->from(array('A' => 'T_ONLINECUST_BIS_ACC'))
									->where('A.CUST_ID = ?', $cust_id)
									->where('A.ACCT_NO = ?', $returnCore['account_number']);

								$custAcct = $this->_db->fetchRow($custAcct);

								if ($custAcct) {
									$this->_db->beginTransaction();
									try {
										$acct_data['CUST_ID']      = $cust_id;
										$acct_data['ACCT_NO']      = $custAcct['ACCT_NO'];
										$acct_data['ACCT_STATUS']      = $returnCore['status'];
										$acct_data['ACCT_SUGGESTED']   = new Zend_Db_Expr('now()');
										$acct_data['ACCT_SUGGESTEDBY'] = $this->_userIdLogin;
										$acct_data['ACCT_NAME']   	= $returnCore['account_name'];
										$acct_data['ACCT_ALIAS_NAME'] = $custAcct['ACCT_NAME'];
										$acct_data['ACCT_TYPE']   	= $returnCore['type'];
										$acct_data['ACCT_DESC']   	= $returnCore['type_desc'];
										$acct_data['ORDER_NO']   	= $ORDER_NO;
										$acct_data['CCY_ID']   	= $returnCore['currency'];
										$acct_data['ACCT_CIF']   	= $returnCore['cif'];

										//echo "<pre>";
										//var_dump($acct_data);die;

										$this->_db->insert('M_CUSTOMER_ACCT', $acct_data);
										$this->_db->commit();
										$ORDER_NO = $ORDER_NO + 1;
									} catch (Exception $error) {
										$this->_db->rollBack();
										var_dump($error);
										$this->view->error = true;
										$this->view->report_msg = 'Error : Failed Insert Data M_CUSTOMER_ACCT';
									}

									$this->_db->beginTransaction();
									try {
										$whereCust = ['CUST_ID = ?' => $cust_id];
										$updateCust['USER_UPDATED'] =  new Zend_Db_Expr('now()');
										$updateCust['USER_UPDATEDBY'] = $this->_userIdLogin;
										$updateCust['CUST_CIF'] = $returnCore['cif'];
										$this->_db->update('M_CUSTOMER', $updateCust, $whereCust);
										$this->_db->commit();
									} catch (Exception $error) {
										$this->_db->rollBack();
										var_dump($error);
										$this->view->error = true;
										$this->view->report_msg = 'Error : Failed Insert Data M_CUSTOMER';
									}
								}
							}
						}

						//

						// INSERT CUSTOMER

						$customer = $this->_db->select()
							->from(array('A' => 'TEMP_ONLINECUST_BIS'))
							->where('A.CHANGES_ID = ?', $changes_id)
							->where('A.CUST_REG_NUMBER = ?', $regNumber);
						$customer = $this->_db->fetchRow($customer);
						if ($customer) {

							// INSERT M_FPRIVI_USER

							$priviUser = ['ABGC', 'APDC', 'UAUR', 'RBGC', 'RPDC', 'VPDC', 'CNBG', 'COSP', 'USPD', 'SBRP', 'VSPD', 'CCBG', 'UARP', 'VLBG', 'UFLG', 'CSRL', 'UVEP', 'UABG', 'DAGL', 'ULST', 'DELI', 'DSDB', 'VDEL', 'RLBG', 'RPBG'];

							if($customer['CUST_APPROVER']){
								$priviUser = array_merge($priviUser, ['APBG']);
							}

							foreach ($priviUser as $value) {
								# code...
								$this->_db->beginTransaction();
								try {
									//code...
									$this->_db->insert('M_FPRIVI_USER', [
										'FPRIVI_ID' => trim($value),
										'FUSER_ID' => $user['CUST_ID'] . $user["USER_ID"]
									]);
									$this->_db->commit();
								} catch (\Throwable $th) {
									//throw $th;
								}
							}

							// 

							$this->_db->beginTransaction();
							try {
								$app = Zend_Registry::get('config');
								$app = $app['app']['bankcode'];
								$insertArr = array_diff_key($customer, array('TEMP_ID' => '', 'CHANGES_ID' => '', 'CUST_REG_NUMBER' => ''));
								$insertArr['CUST_CREATEDBY'] = $this->_userIdLogin;
								$insertArr['CUST_STATUS'] = '1';
								$insertArr['CUST_SUGGESTED'] =  new Zend_Db_Expr('now()');
								$insertArr['CUST_SUGGESTEDBY'] = $this->_userIdLogin;
								$insertArr['CUST_UPDATED'] =  new Zend_Db_Expr('now()');
								$insertArr['CUST_UPDATEDBY'] = $this->_userIdLogin;
								$insertArr['CUST_CIF'] = empty($returnCore['cif']) ? NULL : $returnCore['cif'];

								$this->_db->insert('M_CUSTOMER', $insertArr);
								$this->_db->commit();
								//die();

							} catch (Exception $error) {
								$this->_db->rollBack();
								var_dump($error);
								$this->view->error = true;
								$this->view->report_msg = 'Error : Failed Insert Data M_CUSTOMER';
							}
						}



						//

						// UPDATE STATUS

						$this->_db->beginTransaction();
						try {
							$data2  = [
								'CHANGES_STATUS'	=> 'AP',
								'LASTUPDATED'		=> new Zend_Db_Expr('now()')
							];
							$where2 = ['CHANGES_ID = ?' => $changes_id];

							$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);
							$this->_db->commit();
						} catch (Exception $error) {
							$this->_db->rollBack();
							var_dump($error);
							$this->view->error = true;
							$this->view->report_msg = 'Error : Failed Update Data T_GLOBAL_CHANGES';
						}

						//

					}

					Application_Helper_General::writeLog('APOR', 'Approve customer register online . Register Number : ( ' . $regNumber . ' )');
					$this->setbackURL('/' . $this->_request->getModuleName() . '/index');
					$this->_redirect('/notification/success/index');
				} catch (Exception $error) {
					$this->view->error = true;
					$this->view->report_msg = 'Error : Failed Approval';
				}
			}


			$submit_reject = $this->_getParam('submit_reject');

			// if(!empty($pdf))
			if ($submit_reject) {

				try {
					$this->_db->beginTransaction();

					$data2  = [
						'CHANGES_STATUS'	=> 'RJ',
						'LASTUPDATED'		=> new Zend_Db_Expr('now()')
					];
					$where2 = ['CHANGES_ID = ?' => $changes_id];
					$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

					$this->_db->commit();
					Application_Helper_General::writeLog('APOR', 'Reject customer register online . Register Number : ( ' . $regNumber . ' )');
					$this->setbackURL('/' . $this->_request->getModuleName() . '/index');
					$this->_redirect('/notification/success/index');
				} catch (Exception $error) {
					$this->view->error = true;
					$this->view->report_msg = 'Error : Failed Reject';
				}
			}

			$submit_request_repair = $this->_getParam('repair');

			// if(!empty($pdf))
			if ($submit_request_repair) {

				try {
					$this->_db->beginTransaction();
					$notes = $this->_getParam('PS_REASON_REPAIR');
					$data2  = [
						'CHANGES_STATUS'	=> 'RR',
						'CHANGES_REASON'    => $notes,
						'LASTUPDATED'		=> new Zend_Db_Expr('now()')
					];
					$where2 = ['CHANGES_ID = ?' => $changes_id];
					$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

					$this->_db->commit();

					// send email
					$set = new Settings();
					$url_fo = $set->getSetting('url_fo');
					$templateEmailMasterBankAddress = $set->getSetting('master_bank_address');
					$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
					$templateEmailMasterBankAppUrl = $set->getSetting('master_bank_app_url');
					$templateEmailMasterBankCity = $set->getSetting('master_bank_city');
					$templateEmailMasterBankCountry = $set->getSetting('master_bank_country');
					$templateEmailMasterBankEmail = $set->getSetting('master_bank_email');
					$templateEmailMasterBankEmail1 = $set->getSetting('master_bank_email1');
					$templateEmailMasterBankFax = $set->getSetting('master_bank_fax');
					$templateEmailMasterBankName = $set->getSetting('master_bank_name');
					$templateEmailMasterBankProvince = $set->getSetting('master_bank_province');
					$templateEmailMasterBankTelp = $set->getSetting('master_bank_telp');
					$templateEmailMasterBankWebsite = $set->getSetting('master_bank_website');
					$templateEmailMasterBankWapp = $set->getSetting('master_bank_wapp');
					$FEmailReset = $set->getSetting('bemailtemplate_repaircustomer');

					$pcust_id = $this->sslEnc($cust_id);
					$confirm_link = $url_fo . '/registerechannel/index?cust_id=' . urlencode($pcust_id);

					$customer = $this->_db->select()
						->from(array('A' => 'TEMP_ONLINECUST_BIS'))
						->where('A.CHANGES_ID = ?', $changes_id)
						->where('A.CUST_REG_NUMBER = ?', $regNumber);
					$customer = $this->_db->fetchRow($customer);

					$data = array(
						'[[cust_name]]' => $customer['CUST_NAME'],
						'[[user_email]]' => $customer['CUST_EMAIL'],
						'[[confirm_link]]' => $confirm_link,
						'[[master_bank_name]]' => $templateEmailMasterBankName,
						'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
						'[[master_bank_wapp]]' => $templateEmailMasterBankWapp,
						'[[master_bank_email]]' => $templateEmailMasterBankEmail,
						'[[master_bank_app_name]]'		=> $templateEmailMasterBankAppName,
						'[[notes_repair]]'		=> $notes,
					);

					$FEmailReset = strtr($FEmailReset, $data);

					try {
						$mainResponse = Application_Helper_Email::sendEmail($customer['CUST_EMAIL'], 'Reset Customer Information', $FEmailReset);

						Application_Helper_General::writeLog('APOR', 'Request Repair customer register online . Register Number : ( ' . $regNumber . ' )');
						$this->setbackURL('/' . $this->_request->getModuleName() . '/index');
						$this->_redirect('/notification/success/index');
					} catch (Exception $error) {
						$this->view->error = true;
						$this->view->report_msg = 'Error : Failed Send Mail';
					}
				} catch (Exception $error) {
					$this->view->error = true;
					$this->view->report_msg = 'Error : Failed request repair';
					//die($error);
				}
			}
		}
		Application_Helper_General::writeLog('APOR', 'View detail customer register online . Register Number : ( ' . $regNumber . ' )');
	}

	private function _setReadSuggestion($changesId)
	{
		$data = $this->_db->update('T_GLOBAL_CHANGES', array('READ_STATUS' => 1), $this->_db->quoteInto('CHANGES_ID = ?', $changesId));
	}

	public function sslPrm()
	{
		return array("6a1f325be4c0492063e83a8cb2cb9ae7", "IV (optional)", "aes-128-cbc");
	}

	public function sslEnc($msg)
	{
		list($pass, $iv, $method) = $this->sslPrm();
		return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}

	public function getCustomerAcct($cust_id)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'T_ONLINECUST_BIS_ACC'),
				array(
					'A.CUST_ID',
					'A.ACCT_NO',
					'A.ACCT_NAME',
					'CCY_ID' => 'A.ACCT_CCY',
					'A.ACCT_OWNERSHIP'
				)
			)
			->where('A.CUST_ID = ?', $cust_id);
		return $this->_db->fetchAll($select);
	}

	public function getPriviTemplate($FTEMPLATE_ID)
	{
		$select = $this->_db->select()
			->from(array('A' => 'M_FPRIVILEGE_TEMPLATE'), array('*'))
			->where('A.FTEMPLATE_ID = ?', $FTEMPLATE_ID);
		return $this->_db->fetchAll($select);
	}


	public function inquiryaccountinfoAction()
	{

		//integrate ke core
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		if ($result['response_desc'] == 'Success') //00 = success
		{
			$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
			$result2 = $svcAccountCIF->inquiryCIFAccount();

			$filterBy = $result['account_number']; // or Finance etc.
			$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
				return ($var['account_number'] == $filterBy);
			});

			$singleArr = array();
			foreach ($new as $key => $val) {
				$singleArr = $val;
			}
		}
		$result = array_merge($result, $singleArr);
		if ($result['type_desc'] == 'Deposito') {
			$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountDeposito->inquiryDeposito();
			if ($result3['account_type'] == "ARO" && $result3['rate'] == "0.00") {
				$flagError = false;
			} else {
				$flagError = true;
				$errorMsgListener = "Error : variant rate tidak = 0 dan tipe tidak = ARO";
			}
		} else {

			if (empty($result)) {
				$flagError = true;
			} else {
				$flagError = false;
			}
		}

		$return = array('accountList' => $result, 'flagError' => $flagError, 'errorMessage' => $errorMsgListener);

		echo json_encode($return);
	}
}
