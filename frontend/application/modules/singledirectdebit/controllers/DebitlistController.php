<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'CMD/Payment.php';

class singledirectdebit_DebitlistController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        // $select = $this->_db->select()
        //                      ->from(array('A' => 'T_DIRECTDEBIT'),array('*'))
        //                      ->where('A.COMP_ID = '.$this->_db->quote((string)$this->_custIdLogin));
                             
		// $select = $this->_db->select()
		// 				->from(array('P' => 'T_PSLIP'), array('*'))
		// 				->join(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array('SOURCE_ACCOUNT','SOURCE_ACCOUNT_CCY','SOURCE_ACCOUNT_NAME','UUID','TRA_AMOUNT'))
		// 				->where('P.CUST_ID = '.$this->_db->quote((string)$this->_custIdLogin))
		// 				->where('P.PS_STATUS in (2,17)')
		// 				->where('P.PS_TYPE in (27,28)')
		// 				->order('P.PS_CREATED DESC')
		// 				->query()->fetchAll();

		$Payment = new Payment();
		$select  = $Payment->getPaymentList();
		// $select->where('P.CUST_ID 	 = ?', (string) $this->_custId);
		$select->where('P.PS_STATUS = 17');
		$select->where('P.PS_TYPE in (27,28)');
        
        $alldata1 = $select->query()->fetchAll();
        // $this->paging($select);
		// echo "<pre>";
		// var_dump($select);die;

		$this->view->selectreview = $alldata1;

		$Payment = new Payment();
		$select2  = $Payment->getPaymentList();
		// $select->where('P.CUST_ID 	 = ?', (string) $this->_custId);
		$select2->where('P.PS_STATUS = 2');
		$select2->where('P.PS_TYPE in (27,28)');
        
        $alldata2 = $select2->query()->fetchAll();

		$this->view->selectrelease = $alldata2;

		// var_dump($alldata2);

        $conf = Zend_Registry::get('config');
        $this->view->bankname = $conf['app']['bankname'];

		$releaseTab = $this->_getParam('release');
		$reviewTab = $this->_getParam('review');

		if($releaseTab){
			$this->view->releaseTab = $releaseTab;
		}

		if($reviewTab){
			$this->view->reviewTab = $reviewTab;
		}

    }
}