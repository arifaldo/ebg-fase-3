<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_ReviewController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                   'label' => $this->language->_('Alias Name'),
                                   'sortable' => true),*/
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Register Number'),
      ),
      'type' => array(
        'field' => 'TIME_PERIOD_START',
        'label' => $this->language->_('Type'),
      ),

      'subject'     => array(
        'field'    => 'BG_SUBJECT',
        'label'    => $this->language->_('Subject'),
      ),
      'startdate' => array(
        'field' => 'TIME_PERIOD_START',
        'label' => $this->language->_('Date From'),
      ),
      'enddate'   => array(
        'field'    => 'TIME_PERIOD_END',
        'label'    => $this->language->_('Date To'),
      ),
      'created'   => array(
        'field'    => 'TIME_PERIOD_END',
        'label'    => $this->language->_('Created Date'),
      ),

      'createdby'   => array(
        'field'    => 'BG_FORMAT',
        'label'    => $this->language->_('Created By'),
      )
      //'bgamount'  => array('field'    => 'BG_AMOUNT',
      //                    'label'    => $this->language->_('Amount')
      //                 )
    );

    $selectbg = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE'), array(
        'REG_NUMBER' => 'BG_REG_NUMBER',
        'SUBJECT' => 'BG_SUBJECT',
        'CREATED' => 'BG_CREATED',
        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_START',
        'TIME_PERIOD_END',
        'CREATEDBY' => 'BG_CREATEDBY',
        'AMOUNT' => 'BG_AMOUNT',
        'FULLNAME' => 'T.USER_FULLNAME'

      ))
      ->join(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID')
      ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_STATUS = 1')
      ->order('A.BG_CREATED DESC')
      ->query()->fetchAll();

    $selectlc = $this->_db->select()
      ->from(array('A' => 'T_LC'), array(
        'REG_NUMBER' => 'LC_REG_NUMBER',
        'SUBJECT' => 'LC_CREDIT_TYPE',
        'CREATED' => 'LC_CREATED',
        'CCYID' => 'LC_CCY',
        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_END' => 'LC_EXPDATE',
        'CREATEDBY' => 'LC_CREATEDBY',
        'AMOUNT' => 'LC_AMOUNT',
        'FULLNAME' => 'T.USER_FULLNAME'
      ))
      ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
      ->where('A.LC_CUST =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.LC_STATUS = 1')
      ->order('A.LC_CREATED DESC')
      ->query()->fetchAll();


    $result = array_merge($selectbg, $selectlc);

    $setting = new Settings();
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $pw_hash = md5($enc_salt . $enc_pass);
    $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
    $sessionNamespace->token   = $rand;
    $this->view->token = $sessionNamespace->token;

    foreach ($result as $key => $value) {
      $get_reg_number = $value["REG_NUMBER"];

      $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
      $encpayreff = urlencode($encrypted_payreff);

      $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
    }

    $this->paging($result);

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );


    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;
  }
}
