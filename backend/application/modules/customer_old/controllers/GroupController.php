<?php

require_once 'Zend/Controller/Action.php';

class customer_GroupController extends customer_Model_Customer
{

   public function indexAction() 
   {
      $cust_id = strtoupper($this->_getParam('cust_id'));
      $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
      $error_remark = null;
    
      if($cust_id)
      {
        $customer = $this->getCustomer($cust_id);
        if(!$customer['CUST_ID']) $cust_id = null;
      }
    
      if($cust_id)
      {
	    $fields = array('group_name'   => array('field'    => 'FGROUP_NAME',
                                              'label'    => 'Group Name',
                                              'sortable' => true),
	  
                      'group_desc'    => array('field'    => 'FGROUP_DESC',
                                              'label'    => 'Group Description',
                                              'sortable' => true),
	  
	                  'group_status'  => array('field'    => 'FGROUP_STATUS',
                                              'label'    => 'Status',
                                              'sortable' => true)
            		  );
                    
      $page   = $this->_getParam('page');
      $page   = (Zend_Validate::is($page,'Digits'))? $page : 1;
      $sortBy = $this->_getParam('sortby');
      $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
      $sortDir = $this->_getParam('sortdir');
      $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
      
      $this->view->currentPage = $page;
      $this->view->sortBy = $sortBy;
      $this->view->sortDir = $sortDir;
      
      $select = $this->_db->select()
						     ->from(array('M_FGROUP'),array('FGROUP_NAME','FGROUP_DESC','FGROUP_STATUS'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' OR UPPER(CUST_ID)='.$this->_db->quote('BANK'))
                             ->order($sortBy.' '.$sortDir);
                             
      $this->paging($select);
      $this->view->fields = $fields;
      $this->view->cust_id = $cust_id;
      $this->view->cust_name = $customer['CUST_NAME'];
      $this->view->status_type = $this->_masterStatus;
       
        //insert log
        try 
        {
	      $this->_db->beginTransaction();
		  $this->backendLog($this->_actionID['view'],$this->_moduleID['cust'],$cust_id,null,null);
          $this->_db->commit();
	    }
	    catch(Exception $e) 
	    {
		  $this->_db->rollBack();
  		  Application_Log_GeneralLog::technicalLog($e);
	    }
    }
    else
    {            
        $error_remark = $this->getErrorRemark('22','Customer ID');
      
        //insert log
        try 
        {
	       $this->_db->beginTransaction();
	       $this->backendLog(strtoupper($this->_actionID['view']),$this->_moduleID['cust'],null,null,$error_remark);
	       $this->_db->commit();
	    }
	    catch(Exception $e)
	    {
	       $this->_db->rollBack();
  	       Application_Log_GeneralLog::technicalLog($e);
	    }
    
        $this->_helper->getHelper('FlashMessenger')->addMessage('F');
        $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
        $this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));
    }
    
    
  }
  
  
}




