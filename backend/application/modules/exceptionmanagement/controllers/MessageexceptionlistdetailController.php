<?php
require_once 'Zend/Controller/Action.php';
require_once 'Service/ResendEFT.php';

class Exceptionmanagement_MessageexceptionlistdetailController extends Application_Main
{
	public function indexAction()
	{	 
		$typearr = array_combine(array_values($this->_transfertype['code']),array_values($this->_transfertype['desc']));
		$citizenarr = array_combine(array_values($this->_citizenship['code']),array_values($this->_citizenship['desc']));
		
		$psnumb = $this->_getParam('psnumb');
		$transid = $this->_getParam('transid');
		$this->view->psnumb = $psnumb;
		$this->view->transid = $transid;
		$resend = $this->_getParam('resend');
		$updatefailed = $this->_getParam('updatefailed');
		$updatecomplete = $this->_getParam('updatecomplete');
		
		if($updatefailed && $this->view->hasPrivilege('EFUD'))
		{
			if($this->view->hasPrivilege('EFUD'))
			{
				$updateArr['EFT_STATUS'] = 0;
			}
			Application_Helper_General::writeLog('EFUD','Update Message Exception to Failed ('.$psnumb.')');
		}
		
		if($updatecomplete)
		{
			if($this->view->hasPrivilege('EFUD'))
			{
				$updateArr['EFT_STATUS'] = 1;
			}
			Application_Helper_General::writeLog('EFUD','Update Message Exception to Success ('.$psnumb.')');
		}
		
		if($resend)
		{
			Application_Helper_General::writeLog('EFUD','Resend EFT ('.$psnumb.')');
		}
		
		if(!$resend && !$updatefailed && $updatecomplete)
		{
			Application_Helper_General::writeLog('EFLS','Viewing Message Exception List Detail ('.$psnumb.')');
		}
		
		$caseType = "(CASE B.TRANSFER_TYPE ";
  		foreach($typearr as $key=>$val)
  		{
   			$caseType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseType .= " END)";
  			
  		$caseCitizen = "(CASE B.BENEFICIARY_CITIZENSHIP ";
  		foreach($citizenarr as $key=>$val)
  		{
   			$caseCitizen .= " WHEN '".$key."' THEN '".$val."'";
  		}
  			$caseCitizen .= " END)";

		$select = $this->_db->select()
						->from(array('A' => 'T_PSLIP'),array())
						->join(array('B' => 'T_TRANSACTION'), 'A.PS_NUMBER = B.PS_NUMBER',array('transid'		=>	'B.TRANSACTION_ID',
																								'transtype'		=>	$caseType,
																								'payref'		=>	'A.PS_NUMBER',
																								'transamt'		=>	'B.TRA_AMOUNT',
																								'transccy'		=>	'B.SOURCE_ACCOUNT_CCY',
																								'sourceacct'	=>	'B.SOURCE_ACCOUNT',
																								'sourcename'	=>	'B.SOURCE_ACCOUNT_NAME',
																								'sourcealname'	=>	'B.SOURCE_ACCOUNT_ALIAS_NAME',
																								'beneacct'		=>	'B.BENEFICIARY_ACCOUNT',
																								'benename'		=>	'B.BENEFICIARY_ACCOUNT_NAME',
																								'citizen'		=>	$caseCitizen,
																								'clrcode'		=>	'B.CLR_CODE',
																								'bankname'		=>	'B.BENEFICIARY_BANK_NAME',
																								'swfcode'		=>	'B.SWIFT_CODE',
																								'rawrequest'	=>	'B.RAW_REQUEST',
																								'type'			=>	'A.PS_CATEGORY'));
		$select -> where("B.TRANSACTION_ID LIKE ".$this->_db->quote($transid));
		$select -> where("A.PS_NUMBER LIKE ".$this->_db->quote($psnumb));
		$result = $this->_db->fetchRow($select);
			
		if($updatefailed || $updatecomplete)
		{
			if($this->view->hasPrivilege('EFUD'))
			{
				$whereArr = array('TRANSACTION_ID = ?'=>$transid);
				$updateTransaction = $this->_db->UPDATE('T_TRANSACTION',$updateArr,$whereArr);
				$this->_redirect('/popup/successpopup/index');
			}
		}

		if($resend && $this->view->hasPrivilege('EFUD'))
		{
			if($result['type'] == 'SINGLE PAYMENT')
			{
				$type = 'single';
				$send = $transid;
			}
			
			if($result['type'] == 'MULTI PAYMENT' || $result['type'] == 'BULK PAYMENT')
			{
				$type = 'multi';
				$send = $psnumb;
			}

			$params = array();
			$params['TYPE']    = $type; // single/multi
			$params['PS_NUMBER']  = $send;
			$params['RAW_REQUEST']  = $result['rawrequest'];
			$data    = new Service_ResendEFT($params);
			$sendTransfer  = $data->sendTransfer();
			$this->view->response = $sendTransfer['ResponseCode'];
		}
		$this->view->result=$result;
	}
}
?>
