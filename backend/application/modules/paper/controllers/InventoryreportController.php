<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class paper_InventoryreportController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                 'label' => $this->language->_('Alias Name'),
                                 'sortable' => true),*/
      'paperno'     => array(
        'field'    => '#',
        'label'    => $this->language->_('Paper Number'),
      ),
      'status' => array(
        'field' => '#',
        'label' => $this->language->_('Status'),
      ),
      'description' => array(
        'field' => '#',
        'label' => $this->language->_('Description'),
      ),

      'bankbranch'     => array(
        'field'    => '#',
        'label'    => $this->language->_('Bank Branch'),
      ),
      'lastupdate'     => array(
        'field'    => '#',
        'label'    => $this->language->_('Last Update'),
      ),
      //'bgamount'  => array('field'    => 'BG_AMOUNT',
      //                    'label'    => $this->language->_('Amount')
      //                 )
    );

    $filterlist = array('PAPER_NUMBER', 'STATUS', 'BANK_BRANCH', 'PAPER_UPDATED', 'PAPER_UPDATEDBY');

    $this->view->filterlist = $filterlist;

    $get_all_branch = $this->_db->select()
      ->from(array('A' => 'M_BRANCH'), array("BRANCH_NAME"))
      ->query()->fetchAll();

    $this->view->all_branch = $get_all_branch;

    $paperdate = $this->_request->getParam('paperdate');

    $sqlpaperreport = $this->_db->select()
      ->from(array('A' => 'M_PAPER_GROUP'), array("*"))
      // ->joinleft(array('B' => 'M_PAPER_GROUP'), 'A.PAPER_GROUP = B.PAPER_GROUP', array("B.PAPER_BRANCH", "B.PAPER_KEY"))
      ->joinleft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.PAPER_BRANCH', array("B.BRANCH_NAME"));
    //->where("DATE(A.PAPER_UPDATED) = ?",'2022-03-09');
    $wherecol = $this->_request->getParam('wherecol');

    if ($wherecol[0] == "PAPER_UPDATED") {

      $paperdate = $this->_request->getParam('paperdate');

      if (!empty($paperdate[1])) {
        $FormatDate   = new Zend_Date($paperdate[1], $this->_dateDisplayFormat);
        $dateto      = $FormatDate->toString($this->_dateDBFormat);
        $sqlpaperreport->where("DATE(A.PAPER_UPDATED) <= ?", $dateto);
      }


      if (!empty($paperdate[0])) {
        $FormatDate   = new Zend_Date($paperdate[0], $this->_dateDisplayFormat);
        $datefrom2    = $FormatDate->toString($this->_dateDBFormat);
        $sqlpaperreport->where("DATE(A.PAPER_UPDATED) >= ?", $datefrom2);
      }
    }

    $paper_report = $sqlpaperreport
      // ->order('A.PAPER_ID ASC')
      ->query()->fetchAll();



    $generate_report = [];
    $save_temp = [];
    foreach ($paper_report as $key => $value) {
      $start_number = $value["PAPER_FROM"] + 0;
      $save_length = strlen($value["PAPER_FROM"]);
      for ($i = 0; $i < $value["PAPER_QTY"]; $i++) {
        $temp_id = str_pad($start_number, $save_length, "0", STR_PAD_LEFT);
        $temp_id = $value["PAPER_KEY"] . "$temp_id";
        $temp_paper_report = [];

        $sqlcekpaperreport = $this->_db->select()
          ->from(array('A' => 'M_PAPER'), array("*"))
          ->where("A.PAPER_ID = '" . $temp_id . "'");


        $paperdate = $this->_request->getParam('paperdate');

        if (!empty($paperdate[1])) {
          $FormatDate   = new Zend_Date($paperdate[1], $this->_dateDisplayFormat);
          $dateto      = $FormatDate->toString($this->_dateDBFormat);
          $sqlcekpaperreport->where("DATE(A.PAPER_UPDATED) <= ?", $dateto);
        }


        if (!empty($paperdate[0])) {
          $FormatDate   = new Zend_Date($paperdate[0], $this->_dateDisplayFormat);
          $datefrom2    = $FormatDate->toString($this->_dateDBFormat);
          $sqlcekpaperreport->where("DATE(A.PAPER_UPDATED) >= ?", $datefrom2);
        }

        //echo $sqlcekpaperreport;*/
        $check_paper_report = $sqlcekpaperreport
          ->query()->fetchAll();

        if (count($check_paper_report) == 0) {
          $temp_paper_report["PAPER_NUMBER"] = $temp_id;
          $temp_paper_report["STATUS"] = "Available";
          $temp_paper_report["DESCRIPTION"] = "-";
          $temp_paper_report["BANK_BRANCH"] = $value["BRANCH_NAME"];
          $temp_paper_report["LAST_UPDATED"] = date("d M Y", strtotime($value["PAPER_UPDATED"])) . " (" . $value["PAPER_UPDATEDBY"] . ")";
          //$temp_paper_report["PAPER_UPDATED"] = date("Y-m-d", strtotime($value["PAPER_UPDATED"]));
          //$temp_paper_report["PAPER_UPDATEDBY"] = $value["PAPER_UPDATEDBY"];
        } else {
          switch ($check_paper_report[0]["STATUS"]) {
            case '1':
              $status = "Printed";
              break;
            case '2':
              $status = "Damaged";
              break;
          }

          $temp_paper_report["PAPER_NUMBER"] = $temp_id;
          $temp_paper_report["STATUS"] = $status;
          $temp_paper_report["DESCRIPTION"] = $check_paper_report[0]["NOTES"];
          $temp_paper_report["BANK_BRANCH"] = $value["BRANCH_NAME"];
          $temp_paper_report["LAST_UPDATED"] = date("d M Y", strtotime($check_paper_report[0]["PAPER_UPDATED"])) . " (" . $check_paper_report[0]["PAPER_UPDATEDBY"] . ")";
          //$temp_paper_report["PAPER_UPDATED"] = date("Y-m-d", strtotime($check_paper_report[0]["PAPER_UPDATED"]));
          //$temp_paper_report["PAPER_UPDATEDBY"] = $check_paper_report[0]["PAPER_UPDATEDBY"];
        }
        array_push($generate_report, $temp_paper_report);

        $start_number += 1;
      }
    }

    if (!empty($this->_request->getParam('wherecol'))) {
      $save_wherecol = $this->_request->getParam('wherecol');
      $save_value = $this->_request->getParam('whereval');
      $save_key = [];
      $key_search_temp = $generate_report;
      $counter = 1;
      foreach ($save_wherecol as $key => $value) {
        if (count($save_wherecol) == 1) {

          $key_search = preg_grep('~' . preg_quote(strtolower($save_value[$key]), '~') . '~', array_map('strtolower', array_column($generate_report, $value)));

          foreach ($key_search as $key => $value) {
            if (!in_array($key, $save_key)) {
              array_push($save_key, $key);
            }
          }

          foreach ($save_key as $value) {
            array_push($save_temp, $generate_report[$value]);
          }

          $generate_report = $save_temp;
        } else {
          $key_search = preg_grep('~' . preg_quote(strtolower($save_value[$key]), '~') . '~', array_map('strtolower', array_column($generate_report, $value)));

          $key_search_temp = array_intersect_key($key_search_temp, $key_search);

          $key_search = $key_search_temp;
        }

        if ($counter == count($save_wherecol) && count($save_wherecol) != 1) {
          $generate_report = $key_search_temp;
        }
        $counter += 1;
      }
    }

    $csv    = $this->_getParam('csv');
    if ($csv) {
      //Application_Helper_General::writeLog('ADLS','Download CSV Backend User List');
      $header = array('Paper Number', 'Status', 'Description', 'Cabang Bank', 'Last Update');
      $listable = array_merge_recursive(array($header), $generate_report);
      $this->_helper->download->csv(null, $listable, null, $this->language->_('Paper Inventory Report List'));
    }

    $this->paging($generate_report);

    $this->view->fields = $fields;
    Application_Helper_General::writeLog('VPIR', 'View Paper Inventory Report');
  }

  public function detailAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $paper_number = $this->_getParam("paper-number");

    $get_paper_number = substr($paper_number, -7);
    $get_paper_key = explode($get_paper_number, $paper_number)[0];



    if ($paper_number == "") $this->_redirect('/paper/inventoryreport/');

    $select_paper = $this->_db->select()
      ->from(array('A' => 'M_PAPER'), array("*"))
      ->joinleft(array('B' => 'M_PAPER_GROUP'), 'A.PAPER_GROUP = B.PAPER_GROUP', array("B.PAPER_BRANCH"))
      ->joinleft(array('C' => 'M_BRANCH'), 'B.PAPER_BRANCH = C.BRANCH_CODE', array("C.BRANCH_NAME"))
      ->where("A.PAPER_ID = '" . $paper_number . "'")
      ->query()->fetchAll();

    if (count($select_paper) == 0) {

      $select = $this->_db->select()
        ->from(array('A' => 'M_PAPER_GROUP'), array("*"))
        ->joinleft(array('B' => 'M_BRANCH'), 'A.PAPER_BRANCH = B.BRANCH_CODE', array("B.BRANCH_NAME"))
        ->where("'$get_paper_number' < PAPER_TO AND '$get_paper_number' >= PAPER_FROM AND PAPER_KEY = '$get_paper_key'")
        ->query()->fetchAll();

      if (count($select) == 0) $this->_redirect('/paper/inventoryreport/');

      $select[0]["NOTES"] = "-";
      $select[0]["STATUS"] = "Available";
      $select[0]["PAPER_NUMBER"] = $get_paper_number;
    } else {

      $select = $select_paper;
      $select[0]["PAPER_NUMBER"] = $get_paper_number;
      $select[0]["PAPER_KEY"] = $get_paper_key;
      switch ($select[0]["STATUS"]) {
        case '1':
          $select[0]["STATUS"] = "Printed";
          break;

        case '2':
          $select[0]["STATUS"] = "Damaged";
          break;
      }
    }

    // $select = $this->_db->select()
    //   ->from(array('A' => 'M_PAPER'), array("*"))
    //   ->joinleft(array('B' => 'M_PAPER_GROUP'), 'A.PAPER_GROUP = B.PAPER_GROUP', array("B.PAPER_BRANCH", "B.PAPER_KEY"))
    //   ->joinleft(array('C' => 'M_BRANCH'), 'B.PAPER_BRANCH = C.BRANCH_CODE', array("C.BRANCH_NAME"))
    //   ->where("A.PAPER_ID = '" . $paper_number . "'")
    //   ->query()->fetchAll();

    // if (count($select) == 0) return $this->_redirect('/paper/inventoryreport/'); //redirect when data is null

    $this->view->paperdata = $select[0];
  }

  public function damageAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $paper_number = $this->_getParam("paper-number");

    $get_paper_number = substr($paper_number, -6);
    $get_paper_key = explode($get_paper_number, $paper_number)[0];

    if ($this->_request->isPost()) {
      $filters = array(
        'paper_key'        => array('StripTags', 'StringTrim'),
        'paper_number'        => array('StripTags', 'StringTrim'),
        'notes'        => array('StripTags', 'StringTrim'),
      );

      if ($this->_request->getPost("updateStatus") != 2) {
        $validators =  array(
          'paper_key'    => array(
            'NotEmpty',
            'messages' => array($this->language->_('Can not be empty'))
          ),
          'paper_number'    => array(
            'NotEmpty',
            'messages' => array($this->language->_('Can not be empty'))
          ),
          'notes'    => array(
            'NotEmpty',
            'messages' => array($this->language->_('Can not be empty'))
          ),
        );
      } else {
        $validators =  array(
          'paper_key'    => array(
            'NotEmpty',
            'messages' => array($this->language->_('Can not be empty'))
          ),
          'paper_number'    => array(
            'NotEmpty',
            'messages' => array($this->language->_('Can not be empty'))
          ),
        );
      }

      $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

      if ($zf_filter_input->isValid()) {

        $select = $this->_db->select()
          ->from(array('A' => 'M_PAPER_GROUP'), array("*"))
          ->joinleft(array('B' => 'M_BRANCH'), 'A.PAPER_BRANCH = B.BRANCH_CODE', array("B.BRANCH_NAME"))
          ->where("'$zf_filter_input->paper_number' < PAPER_TO AND '$zf_filter_input->paper_number' >= PAPER_FROM AND PAPER_KEY = '$zf_filter_input->paper_key'")
          ->query()->fetchAll();


        $paper_data['PAPER_ID'] = $zf_filter_input->paper_key . $zf_filter_input->paper_number;
        $paper_data['STATUS'] = ($this->_request->getPost("updateStatus") == 1) ?: "2";
        $paper_data['PAPER_GROUP']     = $select[0]["PAPER_GROUP"];
        $paper_data['PAPER_UPDATED']   = new Zend_Db_Expr('now()');
        $paper_data['PAPER_UPDATEDBY']     = $this->_userIdLogin;
        $paper_data['NOTES'] = $zf_filter_input->notes;

        $this->_db->delete("M_PAPER", [
          "PAPER_ID = ?" => $paper_data["PAPER_ID"]
        ]);

        if ($this->_request->getPost("updateStatus") != 2) {
          try {
            $this->_db->beginTransaction();
            $this->_db->insert('M_PAPER', $paper_data);
            $this->_db->commit();

            switch ($this->_request->getPost("updateStatus")) {
              case 1:
                $status = "Printed";
                # code...
                break;
              case 3:
                $status = "Damaged";
                # code...
                break;
              default:
                $status = "Null";
                # code...
                break;
            }

            Application_Helper_General::writeLog('DPIR', 'Update Status Paper menjadi ' . $status . ' | Paper Number : ' . $paper_data["PAPER_ID"]);

            $this->setbackURL('/paper/inventoryreport');
            $this->_redirect('/notification/submited/paper');
          } catch (Exception $e) {
            $this->_db->rollBack();
            SGO_Helper_GeneralLog::technicalLog($e);

            $this->view->error_remark = 'Gagal menambahkan inventory report ke database. Silahkan coba lagi.';
          }
        }

        Application_Helper_General::writeLog('DPIR', 'Update Status Paper menjadi Available | Paper Number : ' . $paper_data["PAPER_ID"]);

        $this->setbackURL('/paper/inventoryreport');
        $this->_redirect('/notification/submited/paper');
      }
    }
    // else {
    //   var_dump("test");
    //   die();
    //   if ($paper_number == "") return $this->_redirect('/paper/inventoryreport/');
    // }

    $select_paper = $this->_db->select()
      ->from(array('A' => 'M_PAPER'), array("*"))
      ->joinleft(array('B' => 'M_PAPER_GROUP'), 'A.PAPER_GROUP = B.PAPER_GROUP', array("B.PAPER_BRANCH"))
      ->joinleft(array('C' => 'M_BRANCH'), 'B.PAPER_BRANCH = C.BRANCH_CODE', array("C.BRANCH_NAME"))
      ->where("A.PAPER_ID = '" . $paper_number . "'")
      ->query()->fetchAll();

    if (count($select_paper) == 0) {

      $select = $this->_db->select()
        ->from(array('A' => 'M_PAPER_GROUP'), array("*"))
        ->joinleft(array('B' => 'M_BRANCH'), 'A.PAPER_BRANCH = B.BRANCH_CODE', array("B.BRANCH_NAME"))
        ->where("'$get_paper_number' < PAPER_TO AND '$get_paper_number' >= PAPER_FROM AND PAPER_KEY = '$get_paper_key'")
        ->query()->fetchAll();

      if (count($select) == 0) return $this->_redirect('/paper/inventoryreport/');

      $select[0]["NOTES"] = "-";
      $select[0]["STATUS"] = "Available";
      $select[0]["PAPER_NUMBER"] = $get_paper_number;
    } else {

      // if ($select_paper[0]["STATUS"] == "2") return $this->_redirect('/paper/inventoryreport/');

      $select = $select_paper;
      $select[0]["PAPER_NUMBER"] = $get_paper_number;
      $select[0]["PAPER_KEY"] = $get_paper_key;
      switch ($select[0]["STATUS"]) {
        case '1':
          $select[0]["STATUS"] = "Printed";
          break;

        case '2':
          $select[0]["STATUS"] = "Damaged";
          break;
      }
    }

    // $select = $this->_db->select()
    //   ->from(array('A' => 'M_PAPER'), array("*"))
    //   ->joinleft(array('B' => 'M_PAPER_GROUP'), 'A.PAPER_GROUP = B.PAPER_GROUP', array("B.PAPER_BRANCH", "B.PAPER_KEY"))
    //   ->joinleft(array('C' => 'M_BRANCH'), 'B.PAPER_BRANCH = C.BRANCH_CODE', array("C.BRANCH_NAME"))
    //   ->where("A.PAPER_ID = " . $paper_number)
    //   ->query()->fetchAll();

    $this->view->paperdata = $select[0];
  }
}
