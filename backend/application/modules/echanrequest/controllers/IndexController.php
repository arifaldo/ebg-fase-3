<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class echanrequest_IndexController extends Application_Main
{

	private $_suggestionStatusCP = array();
	private $_userAuth = array();
	private $_typetitle = array();


	protected $_moduleDB  = 'WFA';

	public function initController()
	{
		$this->_suggestionStatusCP = array(
			"" => '--' . $this->language->_('Any Value') . "--",
			"UR" => $this->language->_('Unread Suggestion'),
			"RS" => $this->language->_('Read Suggestion'),
			"RR" => $this->language->_('Request Repaired'),
			"RP" => $this->language->_('Repaired Suggestion'),
		);
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$this->_userAuth = Zend_Auth::getInstance()->getIdentity();
		if (empty($this->_userAuth->branchCode))
			$this->_userAuth->branchCode = 'ERROR_BRANCH_CODE_IS_EMPTY';

		$mode = Zend_Registry::get('config');
		$mode = $mode['bo']['create']['user']['branch'];
		if (empty($mode))
			$mode = '0';

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$echannelArr = array('Permata Online (Bisnis)' => 'Permata Online (Bisnis)');

		$this->view->echannelArr = array_merge(array('' => '--- ' . $this->language->_('Any Value') . ' ---'), $echannelArr);

		$select = array();

		$privi = $this->_priviId;


		$this->setbackURL();

		$displayDateFormat  = $this->_dateDisplayFormat;
		$databaseSaveFormat = $this->_dateDBFormat;


		$fields = array(
			'reqNumber'    => array(
				'field' => 'CUST_REG_NUMBER',
				'label' => $this->language->_('Registration Number'),
				'sortable' => true
			),
			'companyName'    => array(
				'field' => 'CUST_NAME',
				'label' => $this->language->_('Company Name'),
				'sortable' => true
			),
			'contactPerson'    => array(
				'field' => 'CUST_CONTACT_PHONE',
				'label' => $this->language->_('Contact Person'),
				'sortable' => true
			),
			'contactNumber'    => array(
				'field' => 'CUST_PHONE',
				'label' => $this->language->_('Contact Number'),
				'sortable' => true
			),
			'lastUpdate' => array(
				'field' => 'LASTUPDATED',
				'label' => $this->language->_('Last Update'),
				'sortable' => true
			),
			'suggestionStatus' => array(
				'field' => 'READ_STATUS',
				'label' => $this->language->_('Suggestion Status'),
				'sortable' => true
			),
			'registrationStatus' => array(
				'field' => 'registration_status',
				'label' => $this->language->_('Registration Status'),
				'sortable' => true
			),
		);

		// $filterlist = ["Registration Number" => "CUST_REG_NUMBER", "Company Name" => "CUST_NAME", "Contact Person" => "CUST_CONTACT_PHONE", "Contact Number" => "CUST_PHONE", "Last Update" => "LASTUPDATED_DATE", "Suggestion Status" => "READ_STATUS", "Registration Status" => "registration_status"];
		$filterlist = ["Registration Number" => "CUST_REG_NUMBER", "Company Name" => "CUST_NAME", "Contact Person" => "CUST_CONTACT_PHONE", "Contact Number" => "CUST_PHONE", "Last Update" => "LASTUPDATED_DATE", "Registration Status" => "registration_status"];

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$csv 	 = $this->_getParam('csv');
		$sortBy  = $this->_getParam('sortby', 'suggestionDate');
		$sortDir = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;


		//get filtering param
		$filterArr = array(
			'filter'			=> array('StringTrim', 'StripTags', 'HtmlEntities'),
			'CUST_REG_NUMBER' 	=> array('StripTags', 'StringTrim', 'HtmlEntities'),
			'CUST_NAME' 	=> array('StripTags', 'StringTrim', 'HtmlEntities'),
			'CUST_CONTACT_PHONE' 	=> array('StripTags', 'StringTrim', 'HtmlEntities'),
			'CUST_PHONE' 	=> array('StripTags', 'StringTrim', 'HtmlEntities'),
			'LASTUPDATED_DATE' 	=> array('StripTags', 'StringTrim', 'HtmlEntities'),
			'LASTUPDATED_DATE_END' 	=> array('StripTags', 'StringTrim', 'HtmlEntities'),
			'READ_STATUS' 	=> array('StripTags', 'StringTrim', 'HtmlEntities'),
			'registration_status' => array('StringTrim', 'StripTags', 'HtmlEntities'),
		);

		$validator = array(
			'filter'			 	=> array(),
			'CUST_REG_NUMBER' 	=> array(),
			'CUST_NAME' 	=> array(),
			'CUST_CONTACT_PHONE' 	=> array(),
			'CUST_PHONE' 	=> array(),
			'LASTUPDATED_DATE' 	=> array(),
			'LASTUPDATED_DATE_END' 	=> array(),
			'READ_STATUS' 	=> array(),
			'registration_status' => array(),
		);


		$dataParam = array('CUST_REG_NUMBER', 'CUST_NAME', 'CUST_CONTACT_PHONE', 'CUST_PHONE', 'LASTUPDATED_DATE', 'READ_STATUS', 'registration_status');
		$dataParamValue = array();
		if ($this->_request->getParam('wherecol')) {


			$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
			$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
			// print_r($dataParam);die;

			// print_r($output);die;
			// print_r($this->_request->getParam('wherecol'));
			foreach ($dataParam as $no => $dtParam) {

				if (!empty($this->_request->getParam('wherecol'))) {
					$dataval = $this->_request->getParam('whereval');
					// print_r($dataval);
					$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if ($value == "LASTUPDATED_DATE") {
							$order--;
						}
						if ($dtParam == $value) {
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				}
			}
		}
		// print_r($dataParamValue);
		// die;	
		// print_r($this->_request->getParam('whereval'));die;
		if (!empty($this->_request->getParam('suggestdate'))) {
			$sgarr = $this->_request->getParam('suggestdate');
			$dataParamValue['LASTUPDATED_DATE'] = $sgarr[0];
			$dataParamValue['LASTUPDATED_DATE_END'] = $sgarr[1];
		}

		/*var_dump($dataParamValue);*/



		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

		$filter 		= $this->_getParam('filter');


		if ($filter == true) {
			$reqNumber 	= html_entity_decode($zf_filter->getEscaped('CUST_REG_NUMBER'));
			$companyName 	= html_entity_decode($zf_filter->getEscaped('CUST_NAME'));
			$contactPerson 	= html_entity_decode($zf_filter->getEscaped('CUST_CONTACT_PHONE'));
			$contactNumber 	= html_entity_decode($zf_filter->getEscaped('CUST_PHONE'));
			$suggestionStatus 	= html_entity_decode($zf_filter->getEscaped('READ_STATUS'));
			$registrationStatus 	= html_entity_decode($zf_filter->getEscaped('registration_status'));

			$changesCreatedFrom = (Zend_Date::isDate($zf_filter->getEscaped('LASTUPDATED_DATE'), $displayDateFormat)) ?
				new Zend_Date($zf_filter->getEscaped('LASTUPDATED_DATE_END'), $displayDateFormat) :
				false;
			$changesCreatedTo 	= (Zend_Date::isDate($zf_filter->getEscaped('LASTUPDATED_DATE'), $displayDateFormat)) ?
				new Zend_Date($zf_filter->getEscaped('LASTUPDATED_DATE_END'), $displayDateFormat) :
				false;

			$this->view->echannel_type 	= $echanneltype;

			if ($changesCreatedFrom)
				$this->view->changesCreatedFrom = $changesCreatedFrom->toString($displayDateFormat);
			if ($changesCreatedTo)
				$this->view->changesCreatedTo   = $changesCreatedTo->toString($displayDateFormat);
		}

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  	= new Echanrequest_Model_Privilege();
			$listAutModuleArr 			= $changeModulePrivilegeObj->getAuthorizeModule();
			$whPriviID 					= "'" . implode("','", $listAutModuleArr) . "'";

			$caseWhenRequestChangeList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN '" . $this->language->_('Request Repaired') . "'
			 WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Unread Suggestion') . "'
			 WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Read Suggestion') . "'
			 WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Repaired Suggestion') . "'
			 END
			";

			$caseWhenRegisStatusList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN '" . $this->language->_('Request Repaired') . "'
			 WHEN (G.CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Waiting for approve') . "'
			 WHEN (G.CHANGES_STATUS = 'AP') THEN '" . $this->language->_('Approved') . "'
			 WHEN (G.CHANGES_STATUS = 'RJ') THEN '" . $this->language->_('Reject') . "'
			 END
			";

			//compose query (use filtering and sorting)
			$select = $this->_db->select()
				->from(
					array('D' => 'TEMP_ONLINECUST_BIS'),
					array(
						'D.*'
					)
				)
				->joinleft(array('G' => 'T_GLOBAL_CHANGES'), 'D.CHANGES_ID = G.CHANGES_ID', array(
					'G.*', 'suggestion_status' => new Zend_Db_Expr($caseWhenRequestChangeList),
					'registration_status' => new Zend_Db_Expr($caseWhenRegisStatusList)
				))
				// ->joinleft(array('B' => 'M_BUSER'), 'G.CREATED_BY = B.BUSER_ID', array('B.BUSER_BRANCH'))
				->joinleft(array('C' => 'M_BRANCH'), 'C.ID = D.BANK_BRANCH', array('C.BRANCH_NAME', 'C.STATUS'))
				// ->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID', array('BG.BGROUP_CALL'))
				->where('G.CHANGES_FLAG = ' . $this->_db->quote('B'))
				->where("G.MODULE IN (" . $whPriviID . ")");

			$auth = Zend_Auth::getInstance()->getIdentity();
			if ($auth->userHeadQuarter == "NO" || $auth->userHeadQuarter == "") {
				$select->where('D.BANK_BRANCH = ?', $auth->userBranchId); // Add Bahri
				// $select->where('C.ID = ?', $auth->userBranchId); // Add Bahri
			}



			// var_dump($auth->userBranchId);


			// if($mode == '1' && $this->_userAuth->headQuarter !== 'Y'){
			// 	if(!(empty($this->_userAuth->branchCode))){
			// 		$select->joinLeft(array('MB' => 'M_BUSER'), 'MB.BUSER_ID = G.CREATED_BY', array())
			// 		->where('MB.BRANCH_CODE = ?', $this->_userAuth->branchCode);	
			// 	}
			// }
		}

		if ($filter == TRUE) {


			$caseWhenRequestChangeList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN '" . $this->language->_('Request Repaired') . "'
			 WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Unread Suggestion') . "'
			 WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Read Suggestion') . "'
			 WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Repaired Suggestion') . "'
			 END
			";

			$caseWhenRegisStatusList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN '" . $this->language->_('Request Repaired') . "'
			 WHEN (G.CHANGES_STATUS = 'WA') THEN '" . $this->language->_('Waiting for approve') . "'
			 WHEN (G.CHANGES_STATUS = 'AP') THEN '" . $this->language->_('Approved') . "'
			 WHEN (G.CHANGES_STATUS = 'RJ') THEN '" . $this->language->_('Reject') . "'
			 END
			";
			// if ($registrationStatus == 'Request Repaired') $registrationStatusVal = 'RR';
			// if ($registrationStatus == 'Waiting for approve') $registrationStatusVal = 'WA';
			// if ($registrationStatus == 'Approved') $registrationStatusVal = 'AP';
			// if ($registrationStatus == 'Reject') $registrationStatusVal = 'RJ';

			if ($suggestionStatus == 'Unread Suggestion') $suggestionStatusVal = '0';
			if ($suggestionStatus == 'Read Suggestion') $suggestionStatusVal = '1';
			if ($suggestionStatus == 'Repaired Suggestion') $suggestionStatusVal = '2';



			//where clauses
			if ($reqNumber)
				$select->where("UPPER(CUST_REG_NUMBER) LIKE " . $this->_db->quote('%' . $reqNumber . '%'));

			if ($companyName)
				$select->where("UPPER(CUST_NAME) LIKE " . $this->_db->quote('%' . $companyName . '%'));
			if ($contactPerson) {
				// $select->where("UPPER(D.CUST_CONTACT_PHONE) LIKE " . $this->_db->quote('%' . $contactPerson . '%'));
				$select->where("UPPER(D.CUST_CONTACT) LIKE " . $this->_db->quote('%' . $contactPerson . '%'));
			}
			if ($contactNumber)
				// $select->where("UPPER(CUST_PHONE) LIKE " . $this->_db->quote('%' . $contactNumber . '%'));
				$select->where("UPPER(CUST_CONTACT_PHONE) LIKE " . $this->_db->quote('%' . $contactNumber . '%'));
			if ($suggestionStatusVal)
				$select->where("UPPER(READ_STATUS) LIKE " . $this->_db->quote('%' . $suggestionStatusVal . '%'));
			if ($registrationStatus) {
				$select->where("UPPER(CHANGES_STATUS) LIKE " . $this->_db->quote('%' . $registrationStatus . '%'));
			}

			if ($changesCreatedFrom)
				$select->where("CONVERTSGO('DATE',G.LASTUPDATED) >= CONVERTSGO('DATE'," . $this->_db->quote($changesCreatedFrom->toString($databaseSaveFormat)) . ")");
			if ($changesCreatedTo)
				$select->where("CONVERTSGO('DATE',G.LASTUPDATED) <= CONVERTSGO('DATE'," . $this->_db->quote($changesCreatedTo->toString($databaseSaveFormat)) . ")");
		}

		//$select->where('(CHANGES_STATUS = '.$this->_db->quote('WA').')');

		// $select->order(array($sortBy . ' ' . $sortDir));
		$select->order('G.CHANGES_STATUS DESC');
		$select->order('G.LASTUPDATED DESC');
		$dataSelectedChanges 		= $this->_db->fetchAll($select);
		//echo count($dataSelectedChanges);
		//die();
		foreach ($dataSelectedChanges as $key => $value) {
			$get_changes_id = $value["CHANGES_ID"];

			$AESMYSQL = new Crypt_AESMYSQL();
			$rand = $this->token;

			$encrypted_payreff = $AESMYSQL->encrypt($get_changes_id, $rand);
			$encpayreff = urlencode($encrypted_payreff);

			$dataSelectedChanges[$key]["CHANGES_ID_ENCRYPTED"] = $encpayreff;
		}

		$this->view->paginator 		= $dataSelectedChanges;


		$result = $select->query()->fetchall();

		$this->view->fields 		= $fields;
		$this->view->paginatorParam = $result;


		$this->paging($result);


		$select2 = $this->_db->select()
			->from(
				array('G' => 'T_GLOBAL_CHANGES'),
				array(
					'D.CUST_REG_NUMBER',
					'D.CUST_NAME',
					'D.CUST_CONTACT_PHONE',
					'D.CUST_PHONE',
					'G.LASTUPDATED',
					'suggestion_status' => new Zend_Db_Expr($caseWhenRequestChangeList),
					'registration_status' => new Zend_Db_Expr($caseWhenRegisStatusList)
				)
			)
			->joinleft(array('D' => 'TEMP_ONLINECUST_BIS'), 'D.CHANGES_ID = G.CHANGES_ID', array())
			->joinleft(array('B' => 'M_BUSER'), 'G.CREATED_BY = B.BUSER_ID', array('B.BUSER_BRANCH'))
			->joinleft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH', array('C.BRANCH_NAME', 'C.STATUS'))
			// ->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID', array('BG.BGROUP_CALL'))
			->where('G.CHANGES_FLAG = ' . $this->_db->quote('B'))
			->where("G.MODULE IN (" . $whPriviID . ")");
		if ($auth->userHeadQuarter == "NO") {
			// $select2->where('C.ID = ?', $auth->userBranchId); // Add Bahri
			$select->where('D.BANK_BRANCH = ?', $auth->userBranchId); // Add Bahri

		}
		$select2->order(array($sortBy . ' ' . $sortDir));


		if ($csv || $this->_request->getParam('print')) {
			$result = $select2->query()->FetchAll();

			foreach ($result as $key => $value) {

				$result[$key]["lastupdated"] = Application_Helper_General::convertDate($value["LASTUPDATED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}

			if ($csv) {
				Application_Helper_General::writeLog('ADLS', 'Download CSV Backend User List');
				$header = array('Registration Number', 'Company Name', 'Contact Person', 'Contact Number', 'Last Update', 'Suggestion Status', 'Registration Status');
				$listable = array_merge_recursive(array($header), $result);
				$this->_helper->download->csv($filterlistdata, $listable, null, $this->language->_('Approval Request - Registration Online'));
			}

			if ($this->_request->getParam('print')) {
				unset($fields['action']);
				$filterlistdatax = $this->_request->getParam('data_filter');
				$this->_forward('print', 'index', 'widget', array('data_content' => $result, 'data_caption' => 'Backend User List', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
			}
		}

		if (!empty($dataParamValue)) {
			$this->view->sgdateStart = $dataParamValue['LASTUPDATED_DATE'];
			$this->view->sgdateEnd = $dataParamValue['LASTUPDATED_DATE_END'];

			unset($dataParamValue['LASTUPDATED_DATE_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}

			// print_r($whereval);die;
		} else {
			$wherecol = array();
			$whereval = array();
		}

		$this->view->wherecol     = $wherecol;
		$this->view->whereval     = $whereval;
		Application_Helper_General::writeLog('APOR', 'View customer register online');
	}
}
