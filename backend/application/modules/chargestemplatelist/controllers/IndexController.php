<?php


require_once 'Zend/Controller/Action.php';


class Chargestemplatelist_IndexController extends Application_Main
{
	public function indexAction() 
	{
		/*$list = array(	''		=>	'--- Any Value---',
						'1'		=>	'Service Charge (SC)',
						'2' 	=> 	'Community Charge (CC)');
		
		$this->view->list = $list;*/

		$this->_helper->layout()->setLayout('newlayout');
		$templateName = $this->language->_('Template Name');
		$createDate = $this->language->_('Created Date');
		$creator = $this->language->_('Creator');
		$latesSuggestion = $this->language->_('Latest Suggestion');
		$latestSuggester = $this->language->_('Latest Suggester');
		$typeEmail = $this->language->_('Type');
		
		$fields = array	(
		                    'TemplateName'         => array     ( 'field' => 'TEMPLATE_NAME',
							                                       'label' => $templateName,
																   'sortable' => true
							                                      ),
							'CreateDate'  			=> array	(
																	'field' => 'CREATED',
																	'label' => $createDate,
																	'sortable' => true
																),
							'Creator'  			=> array	(
																	'field' => 'CREATEDBY',
																	'label' => $creator,
																	'sortable' => true
																),
							'LatestSuggestion'  			=> array	(
																	'field' => 'SUGGESTED',
																	'label' => $latesSuggestion,
																	'sortable' => true
																),
							'LatestSuggestor'  			=> array	(
																	'field' => 'SUGGESTEDBY',
																	'label' => $latestSuggester,
																	'sortable' => true
																),
							'Type'  			=> array	(
																	'field' => '',
																	'label' => $typeEmail,
																	'sortable' => false
																),
						);
						


		$filterlist = array("TEMPLATE");
		
		$this->view->filterlist = $filterlist;
				

		//get page, sortby, sortdir
        $page    = $this->_getParam('page');		
        $sortBy  = $this->_getParam('sortby');
        $sortDir = $this->_getParam('sortdir');
		$service = $this->_getParam('service');
	    if ($service)
		{
		    //echo "redirect";
			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/new');
			die();
		}
		
		//validate parameters before passing to view and query
        $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
        	array(array_keys($fields))
            ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
			
		$sortDir = (Zend_Validate::is($sortDir,'InArray',
        	array('haystack'=>array('asc','desc'))
            ))? $sortDir : 'asc';
			
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;	
		
		$this->view->fields = $fields;
		
		//get filtering param
        $filters = array('filter' 	   		=>  array('StringTrim', 'StripTags'),
                         'TEMPLATE'    =>  array('StripTags', 'StringToUpper'),
        				 //'Type'     		=>  array('StringTrim', 'StripTags', 'StringToUpper'),

                        );
        
        $validators = array(
											'filter'	  => array('allowEmpty' => true),
											'TEMPLATE'	  => array('allowEmpty' => true),
											//'Type'		  => array('allowEmpty' => true), 
										);


        $dataParam = array("TEMPLATE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		$zf_filter = new Zend_Filter_Input($filters, $validators, $dataParamValue, $this->_optionsValidator);
        // $filter 	= $zf_filter->getEscaped('filter');
        $filter 		= $this->_getParam('filter');
        //$type 	= $zf_filter->getEscaped('Type');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		$templateName2  = html_entity_decode(($zf_filter->TemplateName2)  ? $zf_filter->TemplateName2   : strtoupper($zf_filter->getEscaped('TEMPLATE')));
		
		
		
		//data
		$mData = $this->_db->select()
					->from('M_TEMPLATE_CHARGES_OTHER', array(	'TEMPLATE_ID',
																'TEMPLATE_NAME', 
																'CREATED', //'CREATED'=>'CONVERT(VARCHAR,CREATED,113)', 
																'CREATEDBY', 
																'SUGGESTED',//'SUGGESTED'=>'CONVERT(VARCHAR,SUGGESTED,113)', 
																'SUGGESTEDBY'));

		
		if($filter == true)
		{
			//echo $templateName2;die;
		    if($templateName2)
	        {
	        	$this->view->tempname = $templateName2;
			    $mData->where("UPPER(TEMPLATE_NAME) LIKE ".$this->_db->quote('%'.$templateName2.'%'));
		    }
			//if($type)
	        //{
	        	//$this->view->Type = $type;
	        	//$mData->where("CHARGE LIKE ".$this->_db->quote($cekchargeStatus));
			    //$mData->where("UPPER(TEMPLATE_NAME) LIKE ".$this->_db->quote('%'.$templateName2.'%'));
		    //}
		}
		$mData->order($sortBy.' '.$sortDir); 
		//$mData->order('TEMPLATE_ID ASC');
		$this->paging($mData);


		if($csv || $pdf)
		{
			$arr = $this->_db->fetchAll($mData);
			foreach ($arr as $key => $value)
			{
				Array_push($arr[$key],'SC');
				unset($arr[$key]["TEMPLATE_ID"]);
			}
			
			if($csv)
			{	
				Application_Helper_General::writeLog('CTLS','Download CSV Charge Template List');
				$this->_helper->download->csv(array($this->language->_('Template Name'),$this->language->_('Created Date'),$this->language->_('Creator'),$this->language->_('Latest Suggestion'),$this->language->_('Latest Suggestor'), $this->language->_('Type')),$arr,null,$this->language->_('Charges Template List'));  
			}
			else if($pdf)
			{
				Application_Helper_General::writeLog('CTLS','Download PDF Charge Template List');
				$this->_helper->download->pdf(array($this->language->_('Template Name'),$this->language->_('Created Date'),$this->language->_('Creator'),$this->language->_('Latest Suggestion'),$this->language->_('Latest Suggestor'), $this->language->_('Type')),$arr,null,$this->language->_('Charges Template List'));  
			}
		}
		else
		{
			Application_Helper_General::writeLog('CTLS','View Charge Template List');
		}

		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
		
			
		//$res =	$this->_db  ->fetchAll($mData);
		//$this->view->mData = $res;
	}
}
