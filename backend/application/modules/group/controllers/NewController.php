<?php

require_once 'Zend/Controller/Action.php';

class group_NewController extends group_Model_Group
{
	//constructor utk class child
	public function initController()
	{
	   
	}
	
	public function indexAction() 
	{ 
	    $this->view->schemeTemplate = $this->getTemplate();
	    $this->view->privilege      = $this->getPrivilege();
		
		
		if($this->_request->isPost())
		{   
			$this->view->inputParam = $this->getRequest()->getParams(); 
			
			//build filter chains
			$filters = array('group_code' => array('StringTrim'), //trim whitespaces				
							 'group_name' => array('StringTrim'),
							 'validate_privilege' => array('StringTrim'),
							 'role_conflict' => array('StringTrim'),
							 );
							 
			//build validator chains
			$validators = array('group_code' => array(
						                              'NotEmpty',
								                      'Alnum',
								                      //new Zend_Validate_Regex('/^[a-zA-Z]{3,3}\d{3,3}$/'), //group code may not be empty
								                      new Zend_Validate_StringLength(array('min'=>1,'max'=>6)),
								                      array('Db_NoRecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_ID')),
								                      array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_ID')), 
								                      'messages' => array(
													   					  $this->getErrorRemark('01','GROUP ID'), //set error message for group_code
													   					  $this->getErrorRemark('04','GROUP ID'),
													   					  $this->getErrorRemark('04','GROUP ID'),
													   					  $this->getErrorRemark('02','GROUP ID'),
													   					  $this->getErrorRemark('03','GROUP ID'),
													   					  ) //set error message for 2nd validator (Db_NoRecordExists)													   					   )
													   ),
						         'group_name' => array(
													   'NotEmpty',
												       new Zend_Validate_StringLength(array('min'=>1,'max'=>32)), //group name may not Be empty
													   array('Db_NoRecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_DESC')),
													   array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_DESC')), 
													   'messages' => array(
													   					   $this->getErrorRemark('01','GROUP NAME'), //set error message for group_code
													   					   $this->getErrorRemark('04','GROUP NAME'),
													   					   $this->getErrorRemark('02','GROUP NAME'),
													   					   $this->getErrorRemark('03','GROUP NAME'),
													                       )
													  ),
														
						          'validate_privilege' => array(
						                                        'NotEmpty',
											                    'messages' => array($this->getErrorRemark('30','Role List')) 
											                    ),
						          'role_conflict' => array(
											               new Zend_Validate_Identical('valid'),
											               'messages' => array($this->getErrorRemark('26','Privilege'))
														  ) 
						        );
						
	       
			$_POST['validate_privilege'] = array_key_exists('1',array_flip($_POST['privilege']));
			
			$_POST['role_conflict'] = $this->roleIsConflict($_POST['privilege']);
		  
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getPost(),$this->_optionsValidator);
			
			$zf_filter_input->setDefaultEscapeFilter(new Zend_Filter_StripTags());
			
			if($zf_filter_input->isValid()) 
			{     
			
			    try 
				{
				  $this->_db->beginTransaction();	
				  	
				  $groupCode  = ($zf_filter_input->group_code)? $zf_filter_input->group_code : $this->_request->getParam('group_code'); 
				  $groupName  = ($zf_filter_input->group_name)? $zf_filter_input->group_name : $this->_request->getParam('group_name');
				  $privilege  = ($zf_filter_input->privilege)?  $zf_filter_input->privilege  : $this->_request->getParam('privilege');
				  $groupCode  = strtoupper($groupCode);
						
				  $changeType = $this->_changeType['code']['new'];
				  $status     = $this->_masterStatus['code']['active'];
				  
				  // insert ke T_GLOBAL_CHANGES
				  $changesId = $this->suggestionWaitingApproval('Backend Group',"Group ID = $groupCode, Group Name = $groupName",$changeType,'','M_BGROUP,M_BPRIVI_GROUP','TEMP_BGROUP,TEMP_BPRIVI_GROUP','BGROUP_ID',$groupCode);
				 
				  // insert TEMP BGROUP
				  $this->insertTempGroup($groupCode,$groupName,$changesId,$status);

				  if(is_array($privilege))
				  {
					foreach ($privilege as $privi_id => $insert)
				    {
					  if($insert)
					  {
						$this->insertTempPriviGroup($groupCode,$privi_id,$changesId);
					   }
					 }
					}

					$this->backendLog('N',$this->_moduleDB,$groupCode,$this->_getAllParams(),null);
					
					// pengaturan error
					$msg_success = $this->getErrorRemark('00','Group ID',$groupCode);
                    $this->view->msg_success = ($msg_success !="")? $msg_success : null;
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					Application_Log_GeneralLog::technicalLog($e);
					$this->backendLog('N', $this->_moduleDB, null,$this->_getAllParams(),$this->getErrorRemark('82'));
					//rethrow exception
				}
				
				//if we got here, this means no problem during try block, commit transaction
				$this->_db->commit();
				$this->view->success = true;
				
			} //End IS VALID
			else
			{
				// pengaturan error
				$this->view->error = true;
				$this->view->group_code  = ($zf_filter_input->isValid('group_code'))? $zf_filter_input->group_code : $this->_request->getParam('group_code'); 
				$this->view->group_name  = ($zf_filter_input->isValid('group_name'))? $zf_filter_input->group_name : $this->_request->getParam('group_name'); 
				$errors = $zf_filter_input->getMessages();
				
				$this->backendLog('N',$this->_moduleDB,null,null,$this->displayErrorRemark($errors));
				
				//assign error messages
				$this->view->groupCodeErr = (isset($errors['group_code']))? $errors['group_code'] : null;
				$this->view->groupNameErr = (isset($errors['group_name']))? $errors['group_name'] : null;
				$this->view->privilegeErr = (isset($errors['validate_privilege']))? $errors['validate_privilege'] : null;
				
				if(is_null($this->view->privilegeErr))
				{  
					$this->view->privilegeErr = (isset($errors['role_conflict']))? $errors['role_conflict'] : null;
			
				}
		    }
           
		  
		    
	   } //End REQUEST IS POST
	   
   }
	
	
 	
  private function roleIsConflict($privilege)
  {
     // utk mengatur struktur array
	$selectedArray = array();
	if(is_array($privilege))
	{
	   foreach($privilege as $keyPrivilege => $value) 
	   {
		 if($value=="1") $selectedArray[] = $keyPrivilege;
	   }
	}
	
	// utk mengatur struktur array
	if(is_array($selectedArray))
	{
	   if(count($selectedArray)> 0)
	   {
		  foreach($selectedArray as $key => $value )
		  {
             $selectedArray[$key] = "'".$value."'";
		  }
		  $groupArr = implode(',',$selectedArray);
		}
		else return 'invalid';
	 }
	    
	 $listPrivilege = $this->getTemplateId($groupArr);
	 
	 
	 if(is_array($listPrivilege))
	 {
	   $privilegeConflict = array_flip(Application_Helper_Array::simpleArray($listPrivilege,'PARENT_BTEMPLATE_ID'));
     }
	 
	 if(array_key_exists("1", $privilegeConflict) && array_key_exists("2", $privilegeConflict))
	 {
		return 'invalid';
	 }
	 else return 'valid';
			
  }
   
   
   
 }


