<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'CMD/Payment.php';

class Reruntransaction_DetailController extends Application_Main
{

  public function initController()
  {
    $conf = Zend_Registry::get('config');

    $this->_bankName = $conf['app']['bankname'];


    $frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
    //var_dump($select_int);
        if(empty($bankNameArr)){
          $selectBank = $this->_db->select()
      ->from(
        array('B' => 'M_BANK_TABLE'),
        array(
          'BANK_CODE'     => 'B.BANK_CODE',
          'BANK_NAME'     => 'B.BANK_NAME'
        )
      );

      $bankList = $this->_db->fetchAll($selectBank);

      foreach ($bankList as $key => $value) {
        $bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
      }

      $cache->save($bankNameArr,$cacheID);
        }


    

    $this->bankNameList = $bankNameArr;
  } 
  
  public function indexAction() 
  { 
    $this->_helper->layout()->setLayout('newpopup');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;

  $this->view->slipnumb = $this->_getParam('slipnumb');
    $AESMYSQL = new Crypt_AESMYSQL();
    $slipnumb = $AESMYSQL->decrypt($this->_getParam('slipnumb'), $password);

    $select = $this->_db->select()
                  ->from(array('B' => 'T_PSLIP'),array('*'))
                  ->joinleft(array('C' => 'T_TRANSACTION'), 'B.PS_NUMBER = C.PS_NUMBER',array('*'))
                  ->joinleft(array('D' => 'M_CUSTOMER'), 'B.CUST_ID = D.CUST_ID',array('*'));
  $select -> where("B.PS_NUMBER = ".$this->_db->quote($slipnumb));
  $result = $select->query()->FetchAll();
  
   $conf = Zend_Registry::get('config');
    $bank = $conf['app']['bankname'];
    if(!empty($result)){
      foreach($result as $key => $val){
        
        if(!empty($val['SOURCE_ACCT_BANK_CODE'])){
          $bank = $this->bankNameList[$val['SOURCE_ACCT_BANK_CODE']];
        }
        $result[$key]['BANK_NAME'] = $bank;
      }
    }
  
  $this->view->result=$result;
  //echo $select;
  //Zend_Debug::dump($select); die;
  $pdf      = $this->_getParam('pdf');
  $this->view->pdf    = false;
  $this->view->profileid    = $slipnumb;


  $release      = $this->_getParam('release');
  if($release){
    $errorcode = array('60');
    $date = (date("Y-m-d"));
    $qrelease = $this->_db->select()
              ->from(array('A' => 'T_PSLIP'),array())
        ->join(array('D' => 'T_TRANSACTION'), 'A.PS_NUMBER = D.PS_NUMBER',array('A.PS_NUMBER','A.CUST_ID','A.PS_CATEGORY', 'A.PS_TXCOUNT'))
              ->where("D.ERRORCODE = ?",$errorcode) 
              ->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
              ->query()->fetchAll();
    $profileid = $this->_getParam('profileid');
  $data = $this->_request->getParams();
  
    //$profileid = $AESMYSQL->decrypt($this->_getParam('profileid'), $password);
    //var_dump(profileid);
//var_dump($data);die;
    foreach($profileid as $psnumber)
    {
    
    //var_dump($psnumber);die;
      // $AESMYSQL = new Crypt_AESMYSQL();
          // $releaseid = $AESMYSQL->decrypt($this->_getParam($releaselist['PS_NUMBER']), $password);
          
      // if($releaseid == '1')
      // {
       
      
      $releaselist = array();
          //foreach ($qrelease as $key => $value) {
            $releaselist = $this->_db->select()
              ->from(array('A' => 'T_PSLIP'),array('A.PS_NUMBER','A.CUST_ID','A.PS_CATEGORY', 'PS_TXCOUNT'))
              ->where("A.PS_NUMBER = ?",$psnumber)
              //->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
              ->query()->fetchAll();
        // $AESMYSQL = new Crypt_AESMYSQL();
            // $releaseid = $AESMYSQL->decrypt($this->_getParam($releaselist['PS_NUMBER']), $password);
            
        // if($releaseid == '1')
        // {
          $payslip++;
      //var_dump($psnumber);
          //var_dump($releaselist);die;
          $Payment = new Payment($psnumber, $releaselist[0]['CUST_ID'], $this->_userIdLogin,null,1);
          
  //        $cekpayment = $this->_db->select()
  //                    ->from(array('A' => 'T_PSLIP'),array())
  //                    ->join(array('B' => 'T_TRANSACTION'), 'A.PS_NUMBER = B.PS_NUMBER',array('A.PS_NUMBER','tracount' => "COUNT('B.TRANSACTION_ID')"))
  //                    ->where("PS_STATUS = '7'")
  //                    ->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
  //                    ->where("A.PS_NUMBER = '".($releaselist['PS_NUMBER']."'"))
  //                    ->group('A.PS_NUMBER');
  //        $cekpayment = $this->_db->fetchRow($cekpayment);
          
          $traslip = $traslip + $releaselist[0]['PS_TXCOUNT'];
          
  //        if(!$cekpayment)
  //        {
  //          $invalidpayslip++;
  //        }
  //        else
  //        {
    //die('here');
  
      $content = array(
                'PS_STATUS'    => 2
                
                 ); 
        
      $whereArr  = array('PS_NUMBER = ?'=>$psnumber);
      $this->_db->update('T_PSLIP',$content,$whereArr);
    
            $resultRelease = $Payment->releasePayment();
            
            $cektrans = $this->_db->select()
                      ->from(array('t' => 'T_TRANSACTION'),array( "trasuccess"   => "sum(if(t.TRA_STATUS = 3, 1, 0))", 
                                            "trafailed"    => "sum(if(t.TRA_STATUS = 4, 1, 0))",
                                            ))
                      ->where("t.PS_NUMBER = '".($psnumber."'"));
            $cektrans = $this->_db->fetchRow($cektrans);
            
            if(!empty($resultRelease['status'])){
               $this->view->success = 'success';
            }
               
            if ($resultRelease['status'] == '00')
            {
              $paysuccess++;
              $trasuccess = $trasuccess + $cektrans['trasuccess'];
              $trafailed  = $trafailed  + $cektrans['trafailed'];
            }
            if ($resultRelease['status'] != '00' && $resultRelease['status'] != 'XT')
            {
              $payfailed++;
              $trasuccess = $trasuccess + $cektrans['trasuccess'];
              $trafailed  = $trafailed  + $cektrans['trafailed'];
            }
            if ($resultRelease['status'] == 'XT')
            {
              $failedattempt++;
            }

           
      //$this->_redirect('/notification/success/index');
//      var_dump($trasuccess);
//      var_dump($failedattempt);die;
  //        }
          
        // }
      }

  }


  }
}