<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';
	require_once 'CMD/SinglePayment.php';
	require_once 'CMD/Validate/ValidatePaymentSingle.php';
	require_once 'CMD/SweepPayment.php';
	$db = Zend_Db_Table::getDefaultAdapter();
	
	$truecounter = 0;
	$falsecounter = 0;
	$cronResult = "";

	// INNER JOIN KE T_TRANSACTION
	// PS PERIODIC_STATUS  2
	// PERIODIC_NEXTDATE (today)
	
	$data = $db->select	()
				->FROM	(array('P' => 'T_PERIODIC'),array('*'))
				->JOIN	(array('D' => 'T_PERIODIC_DETAIL'),'P.PS_PERIODIC = D.PS_PERIODIC',array('*'))
				->JOIN	(array('PS' => 'T_PSLIP'),'P.PS_PERIODIC = PS.PS_PERIODIC ',array('*')) 
				// ->JOIN	(array('T' => 'T_TRANSACTION'),'PS.PS_NUMBER = T.PS_NUMBER',array('*'))
				->WHERE('P.PS_PERIODIC_STATUS = 2')		
				->WHERE('PS.PS_STATUS IN (7)')		
				->WHERE('PS.PS_TYPE IN (19,20)')				
				->WHERE('DATE(P.PS_PERIODIC_NEXTDATE) = '."'".date('Y-m-d')."'".'')
				->WHERE('DATE(PS.PS_EFDATE) = '."'".date('Y-m-d')."'".'')
				->WHERE('DATE(P.PS_PERIODIC_ENDDATE) >= '."'".date('Y-m-d')."'".'')		
				// ->where("PS.PS_EFTIME between SUBDATE(TIME(NOW()), INTERVAL 1 MINUTE) AND SUBDATE(TIME(NOW()), INTERVAL -16 MINUTE)")		
				->GROUP("PS.PS_PERIODIC")
				->query()->fetchAll()
				;
	//echo $data->__toString();die;
	// echo $data;die;
	//Zend_Debug::dump($data, 'Test'); die;	
		
	if($data)
	{
		// var_dump($data);
		//Zend_Debug::dump($data); die;
		foreach($data as $row)
		{
			$PS_EVERY_PERIODIC_UOM 					= $row['PS_EVERY_PERIODIC_UOM'];
			$PERIODIC_EVERY							= $row['PS_EVERY_PERIODIC'];
			$END_DATE								= $row['PS_PERIODIC_ENDDATE'];
			$PS_TYPE								= $row['PS_TYPE'];
			$PS_NUMBER								= $row['PS_NUMBER'];
			
			$dataday = $db->select	()
				->FROM	(array('P' => 'T_PERIODIC_DAY'),array('P.*'))
				->JOIN	(array('PS' => 'T_PSLIP'),'P.PERIODIC_ID = PS.PS_PERIODIC',array()) 
				->WHERE('PS.PS_NUMBER = ?',$PS_NUMBER)
				->group('P.DAY_ID')				
				->query()->fetchAll()
				;
				// var_dump($PS_EVERY_PERIODIC_UOM);die;
			// NextDate														
			if ($PS_EVERY_PERIODIC_UOM ==1){ //every day

				$date = new DateTime();
				$arrday = array(
							'0' => 'sunday',
							'1' => 'monday',
							'2' => 'tuesday',
							'3' => 'wednesday',
							'4' => 'thursday',
							'5' => 'friday',
							'6' => 'saturday'

						);
				// Modify the date it contains
				$datenumb = date('w');
				if($datenumb==6){
					$datename = 0;
				}else{
					$datename = $datenumb+1;
				}
				// var_dump($datename);
				if(!empty($dataday)){
					$resutnext = 0;
					// echo 'here';
					foreach ($dataday as $key => $value) {
						if($datename == $value['DAY_ID']){
							$string = 'next '.$arrday[$datename];
							// var_dump($string);
							$date->modify($string);

							$NEXT_DATE = $date->format('Y-m-d');
							
							$resutnext = 1;
						}
					}
					// var_dump($NEXT_DATE);echo 'ge';
					if(!$resutnext){
						// ;

						$string = 'next '.$arrday[$dataday['0']['DAY_ID']];
						var_dump($string);
						$date->modify($string);
						$NEXT_DATE = $date->format('Y-m-d');
					}

				}else{
					$NEXT_DATE =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				}				
				
				// $nextDateVal = strtotime($nextDate);
				
				
				// var_dump($NEXT_DATE);
				//echo $NEXT_DATE; die;
				
			}else if($PS_EVERY_PERIODIC_UOM ==2){

				$nextDate =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				$nextDateVal = strtotime($nextDate);
				$NEXT_DATE = date("Y-m-d",strtotime("+7 day", $nextDateVal));

			}elseif ($PS_EVERY_PERIODIC_UOM ==3){ //every date
			
				$nextDate =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				$nextDateVal = strtotime($nextDate);
				$dateNextMonth = date("Y-m-d",strtotime("+29 days", $nextDateVal));
				$nextMontheVal = strtotime($dateNextMonth);				
				$maxDays=date('t', $nextMontheVal); // hari terakhir bulan berikutnya
				$every = date('j', $nextDateVal);	
					
				if ($maxDays >=  $every){
					$every = $every;
				}else{
					$every = $maxDays;
				}
				$nextDate = mktime(0,0,0,date("n", strtotime($dateNextMonth)),$every,date("Y", strtotime($dateNextMonth)));
				$NEXT_DATE = date("Y-m-d",$nextDate);
				
			}
			
			// foreach ($variable as $key => $value) {
				# code...
			// }

			$param = array();			
			$param['PS_PERIODIC'] 			= $row['PS_PERIODIC'];
			$param['PS_SUBJECT'] 			= $row['PS_SUBJECT'];
			$param['PS_EFDATE']  			= $NEXT_DATE;
			$param['PS_EFDATE']  			= $NEXT_DATE;
			$param['PS_EFTIME']  			= $row['PS_EFTIME'];
			
			$param['PS_TYPE'] 				= $row['PS_TYPE'];
			
			$param['PS_CCY']  				= 'IDR';
			$param['REF_ID']  				= $row['REF_ID'];
			
			$param['SWEEP'] 				= "SI";

			// $param['REMAIN_BALANCE_TYPE']	= $row['REMAIN_BALANCE_TYPE'];
			// $param['SWEEP_TYPE'] 				= $row['SWEEP_TYPE'];
			// $param['BENE_DATA']				= $row['REF_ID'];

			
			$PS_NUMBER								= "";
			
			//$db->beginTransaction();
			// var_dump($END_DATE);
			// var_dump($NEXT_DATE);
			// die;

			$cekdata = $db->select	()
				->FROM	(array('P' => 'T_PSLIP'),array('P.*'))
				->WHERE('P.PS_PERIODIC = ?',$row['PS_PERIODIC'])
				->WHERE('P.PS_EFDATE = ?', $NEXT_DATE)
				->query()->fetchAll()
				;

			if(empty($cekdata)){	

			if (strtotime($END_DATE) >= strtotime($NEXT_DATE)){
				
				// create T_PSLIP
				$param['_addBeneficiary'] = 'BADA';
				$param['_beneLinkage'] = 'BLBU';
				$param['_priviCreate'] = 'IPMO';

				$transdata = $db->select	()
				->FROM	(array('P' => 'T_TRANSACTION'),array('P.*'))
				// ->JOIN	(array('PS' => 'T_PSLIP'),'P.PERIODIC_ID = PS.PS_PERIODIC',array()) 
				->WHERE('P.PS_NUMBER = ?',$row['PS_NUMBER'])				
				->query()->fetchAll()
				;

				$param['CROND'] = true;
				foreach ($transdata as $key => $value) {
										$param['TRANSACTION_DATA'][] = array(
										'SOURCE_ACCOUNT' 			=> $value['SOURCE_ACCOUNT'],
										'SOURCE_ACCOUNT_NAME' 		=> $value['SOURCE_ACCOUNT_NAME'],
								      	'SOURCE_ACCOUNT_TYPE' 		=> $value['SOURCE_ACCOUNT_TYPE'], //new
								      	'SOURCE_ACCOUNT_BANK_CODE' 	=> $value['SOURCE_ACCOUNT_BANK_CODE'],
								      	'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
								      	'BENEFICIARY_ID' 			=> $value['BENEFICIARY_ID'],
										'BENEFICIARY_ACCOUNT' 		=> $value['BENEFICIARY_ACCOUNT'],
										'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
										'BENEFICIARY_ACCOUNT_NAME' 	=> $value['BENEFICIARY_ACCOUNT_NAME'],
										'BENEFICIARY_ALIAS_NAME' 	=> '',
										'SWIFT_CODE'				=> $value['SWIFT_CODE'],
										'CLR_CODE'					=> $value['CLR_CODE'],
										'BANK_CODE'					=> $value['BANK_CODE'],
										'TRANSFER_TYPE' 			=> 'PB',
										'TRA_AMOUNT' 				=> $value['TRA_AMOUNT'],
										'TRA_MESSAGE' 				=> $value['TRA_MESSAGE'],
										'TRA_REFNO'					=> $value['TRA_REFNO'],
										'TRA_ADDITIONAL_MESSAGE' 	=> '',
										'TRA_REMAIN'				=> $value['TRA_REMAIN'],
										'TRA_NOTIF'				=> $value['TRA_NOTIF'],
										'TRA_EMAIL'				=> $value['NOTIF_EMAIL'],
										'TRA_SMS'				=> $value['NOTIF_SMS']
					);
				}

							
				$SweepPayment = new SweepPayment(null, $row['CUST_ID'], $row['PS_CREATEDBY']);
				// echo "<pre>";
				// var_dump($param);die;
				$result = $SweepPayment->createOpenSweep($param);

				// $SinglePayment = new SinglePayment($PS_NUMBER, $row['USER_ID']);
				// if ($PS_TYPE == 19){ // within					
				// 	$param['_priviCreate'] 					= 'CRSP';
				// 	$result = $SinglePayment->createPaymentWithin($param);
				// }elseif ($PS_TYPE == 20){ //domesitic					
				// 	$param['_priviCreate'] 					= 'CDFT';
				// 	$result = $SinglePayment->createPaymentDomestic($param);
				// }
							
				$db->beginTransaction();
				// update NEXTDATE PERIOIDC				
				$data = array ('PS_PERIODIC_NEXTDATE' => $NEXT_DATE, 'PS_PERIODIC_CURRENTDATE' => date('Y-m-d'));
				$where['PS_PERIODIC = ?'] = $row['PS_PERIODIC'];
				$db->update('T_PERIODIC',$data,$where);
				
				$db->commit();
								
			}else{
				$db->beginTransaction();
				// update STATUS PERIODIC to be COMPLETED
				$data = array ('PS_PERIODIC_STATUS' => 1, 'PS_PERIODIC_CURRENTDATE' => date('Y-m-d'));
				$where['PS_PERIODIC = ?'] = $row['PS_PERIODIC'];
				$db->update('T_PERIODIC',$data,$where);
				
				$db->commit();
			}
			}
			
			//$db->commit();
						
			
			/*$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], $row['USER_ID']);
			$resultRelease = $Payment->releasePayment();
					
			if ($resultRelease['status'] == '00')
			{
				$truecounter++;
			}
			else
			{
				$falsecounter++;
			}*/
		}
		
		/*if($truecounter>0)
		{
			$cronResult .= " ".$truecounter." payment(s) success";
		}
		if($falsecounter>0)
		{
			$cronResult .= " ".$falsecounter." payment(s) failed";
		}*/		
	}
	else
	{
		$cronResult .= "No data released";
	}

	$filename = basename(__FILE__);
	Application_Helper_General::cronLog($filename, $cronResult, 1);
?>