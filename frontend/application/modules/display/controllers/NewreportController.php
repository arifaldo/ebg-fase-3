<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

class display_NewreportController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	protected $_filterProp = array();

	public function initController(){

		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		$opt[""] = "-- " .$this->language->_('Please Select'). " --";
		
		$payType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$arrPayStatus	= array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
		//$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		$arrAccount 	= $CustomerUser->getAccounts();

		$arrCcy = array("IDR" => "IDR", "USD" => "USD");
		
		foreach($payType as $key => $value){ 
			 $optpaytypeRaw[$key] = $this->language->_($value); 
		}

		//foreach($trfType as $key => $value){ $filterTrfType[$key] = $this->language->_($value); }
		foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		
		if(is_array($arrAccount) && count($arrAccount) > 0){
			foreach($arrAccount as $key => $value){
				$val 		= $arrAccount[$key]["ACCT_NO"];
				$ccy 		= $arrAccount[$key]["CCY_ID"];
				$acctname 	= $arrAccount[$key]["ACCT_NAME"];
				$accttype 	= ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';	// 10 : saving, 20 : giro;
				
				$arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';
			}
		}
		else { $arrAccountRaw = array();}
		
		$optPayType = $opt + $optpaytypeRaw;
		$optPayStatus = $opt + $optpayStatusRaw;
		unset($optPayStatus[10]);
		unset($optPayStatus[15]);
		unset($optPayStatus[16]);
		
		$optarrAccount 	= $opt + $arrAccountRaw;
		$optarrCcy = $opt + $arrCcy;

		$ACBENEFArr = array();
		$benefRaw = array();

        $select = $this->_db->select()
                            ->from(array('B'            => 'M_BENEFICIARY_USER'),
                                   array('ACBENEF_ID'   => 'B.BENEFICIARY_ID')
                                   )
                            ->join(array('A'=>'M_BENEFICIARY'), 'A.BENEFICIARY_ID = B.BENEFICIARY_ID',array('A.BENEFICIARY_ACCOUNT'))
                            ->where("B.CUST_ID  = ?" , (string)$this->_custIdLogin)
                            ->where("B.USER_ID  = ?" , (string)$this->_userIdLogin);

        $ACBENEFArr = $this->_db->fetchAll($select);

        // bila empty, user bene linkage cannot see all payment
        if (empty($ACBENEFArr))
        {   $ACBENEFArr[] = "0";        }

                    
        foreach ($ACBENEFArr as $key => $val){
        	$benefRaw[$val['ACBENEF_ID']] = $val['BENEFICIARY_ACCOUNT'];
        }

        $optarrBenef = $opt + $benefRaw;

		$filterProp = array(
						"payReff" => array(
									"label"=>$this->language->_('Payment Ref'),
									"type"=>"text",
									"col"=>"P.PS_NUMBER",
									"data"=>""
								),
						"created" => array(
									"label"=>$this->language->_('Created Date'),
									"type"=>"date",
									"col"=>"PS_CREATED",
									"data"=>""
								),
						"updated" => array(
									"label"=>$this->language->_('Updated Date'),
									"type"=>"date",
									"col"=>"PS_UPDATED",
									"data"=>""
								),
						"accsrc" => array(
									"label"=>$this->language->_('Source Account'),
									"type"=>"select",
									"col"=>"",
									"data"=>$optarrAccount
								),
						"accsrc_name" => array(
									"label"=>$this->language->_('Source Name'),
									"type"=>"text",
									"col"=>"SOURCE_ACCOUNT_NAME",
									"data"=>""
								),
						"acbenef" => array(
									"label"=>$this->language->_('Beneficiary Account'),
									"type"=>"select",
									"col"=>"BENEFICIARY_ID",
									"data"=>$optarrBenef
								),
						"acbenef_name" => array(
									"label"=>$this->language->_('Beneficiary Name'),
									"type"=>"text",
									"col"=>"BENEFICIARY_ACCOUNT_NAME",
									"data"=>""
								),
						"ccy" => array(
									"label"=>$this->language->_('Currency'),
									"type"=>"select",
									"col"=>"PS_CCY",
									"data"=>$optarrCcy
								),
						"traamt" => array(
									"label"=>$this->language->_('Amount'),
									"type"=>"amount",
									"col"=>"TRA_AMOUNT",
									"data"=>""
								),
						"efdate" => array(
									"label"=>$this->language->_('Payment Date'),
									"type"=>"date",
									"col"=>"PS_EFDATE",
									"data"=>""
								),
						"psstatus" => array(
									"label"=>$this->language->_('Payment Status'),
									"type"=>"select",
									"col"=>"PS_STATUS",
									"data"=>$optPayStatus
								),
						"pstype" => array(
									"label"=>$this->language->_('Payment Type'),
									"type"=>"select",
									"col"=>"PS_TYPE",
									"data"=>$optPayType
								),
					);

		$this->_filterProp = $filterProp;
	}
		
	public function indexAction()
	{	
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$filterProp = $this->_filterProp;
		
		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		
		$fields = array	(	'payReff'  					=> array	(
																		'field' => 'payReff',
																		'label' => $this->language->_('Payment Ref').'#',
																		'sortable' => true
																),
							'created'  					=> array	(
																		'field' => 'created',
																		'label' => $this->language->_('Created Date'),
																		'sortable' => true
																	),
							'paySubj'  					=> array	(
																		'field' => 'paySubj',
																		'label' => $this->language->_('Subject'),
																		'sortable' => true
																	),	
							'accsrc'  					=> array	(
																		'field' => 'accsrc',
																		'label' => $this->language->_('Source Account'),
																		'sortable' => true
																	),
							'accsrc_name' 				=> array	(
																		'field' => 'accsrc_name',
																		'label' => $this->language->_('Source Account Name'),
																		'sortable' => true
																	),
							'acbenef'  					=> array	(
																		'field' => 'acbenef',
																		'label' => $this->language->_('Beneficiary Account'),
																		'sortable' => true
																	),
							'acbenef_name'  			=> array	(
																		'field' => 'acbenef_name',
																		'label' => $this->language->_('Beneficiary Name'),
																		'sortable' => true
																	),
							'numtrx' 					 => array	(
																		'field' => 'numtrx',
																		'label' => '#'.$this->language->_('Trans'),
																		'sortable' => true
																	),
							'ccy' 						 => array	(
																		'field' => 'ccy',
																		'label' => $this->language->_('CCY'),
																		'sortable' => false
																	),
							'amount'  					=> array	(
																		'field' => 'amount',
																		'label' => $this->language->_('Amount'),
																		'sortable' => true
																	),
							'efdate'  					=> array	(
																		'field' => 'efdate',
																		'label' => $this->language->_('Payment Date'),
																		'sortable' => true
																	),
							'updated'  					=> array	(
																		'field' => 'updated',
																		'label' => $this->language->_('Last Updated'),
																		'sortable' => true
																	),
							'payStatus'    				=> array	(
																		'field' => 'payStatus',
																		'label'  => $this->language->_('Payment Status'),
																		'sortable' => true
																	),
							'payType'  					=> array	(
																		'field'	=> 'payType',
																		'label' 	=> $this->language->_('Payment Type'),
																		'sortable' => true
																	),
						);

		//for filter
		$this->view->filterProp = $filterProp;
		
		//get page, sortby, sortdir
		$page    		= $this->_getParam('page');
		$sortBy  		= ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updated');
		$sortDir 		= $this->_getParam('sortdir');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfltr');
		
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		
		$this->view->sortBy			= $sortBy;
		$this->view->sortDir 		= $sortDir;
		
		//validate parameters before passing to view and query
		$page 		= (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		$sortBy 	= (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';	
		
		$this->view->currentPage = $page;

		$paramPayment = array("WA" 				=> false,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							);
		
		// get payment query
		$select   = $CustomerUser->getPaymentNew($paramPayment);
		

		$filters = array(	'wherecol' => array('StringTrim','StripTags','HtmlEntities'),
                            'whereopt' => array('StringTrim','StripTags','HtmlEntities'),
                            'whereval' => array('StringTrim','StripTags','HtmlEntities'),
		);
		
		
		$zf_filter = new Zend_Filter_Input($filters,array(),$this->_request->getParams(),$this->_optionsValidator);

			
        $whereCols = $zf_filter->wherecol;
        $whereOpts = $zf_filter->whereopt;
        $whereVals = $zf_filter->whereval;

        $optsArr = array("EQUAL" => "=",
                        "NOT EQUAL" => "<>",
                        "LESS THAN" => "<",
                        "GREATER THAN" => ">",
                        "LESS THAN OR EQUAL TO" => "<=",
                        "GREATER THAN OR EQUAL TO" => ">=",
                        "LIKE" => "LIKE"
        );

        if(!empty($clearfilter)){
			$rmvfilter = $clearfilter;
			$setfilter = null;
		}
		else{
			$rmvfilter = $this->_getParam('rmvfilter');
			if($csv || $pdf){
				$setfilter = null;
			}
			else{
				$setfilter = $filter;
				$rmvfilter = null;
			}
		}

		$this->view->rmvfilter = $rmvfilter;

        if($setfilter == null && $rmvfilter != 1){
        	$whereCols[0] = "updated-date";
        	$whereOpts[0] = "EQUAL";
        	$whereVals[0] = date("d/m/Y");
        }
        elseif($rmvfilter == 1){
        	$whereCols[0] = "updated-date";
        	$whereOpts[0] = "LESS THAN OR EQUAL TO";
        	$whereVals[0] = date("d/m/Y");
        }

        if(!empty($whereCols)){
        	foreach ($whereCols as $key => $value) {
        		$tempCol = explode("-", $value);
        		$fname = $tempCol[0];
        		$ftype = $tempCol[1];
        		$fcolumn = $filterProp[$fname]['col'];

        		$opt = $optsArr[$whereOpts[$key]];
        		if($whereOpts[$key] == "LIKE")
        			$formatVal = "%".$whereVals[$key]."%";
        		else
        			$formatVal = $whereVals[$key];

        		if($fname == "payReff" || $fname == "ccy"){
        			$select->where('UPPER('.$fcolumn.') '.$opt.' "'.$formatVal.'"');
        		}
        		elseif($fname == "accsrc"){
        			$select->where('(SELECT COUNT(PS_NUMBER) FROM T_TRANSACTION WHERE PS_NUMBER = P.PS_NUMBER and SOURCE_ACCOUNT '.$opt.' "'.$formatVal.'") > 0');
        		}
        		elseif($fname == "pstype"){
        			$tempArr = explode(",", $formatVal);
        			if($whereOpts[$key] == "EQUAL")
        				$opt = "IN";
        			else
        				$opt = "NOT IN";

        			$select->where($fcolumn.' '.$opt.' (?)', $tempArr);
        		}
        		elseif($ftype == "amount"){
        			$select->where($fcolumn.' '.$opt.' '.Application_Helper_General::convertDisplayMoney($formatVal));
        		}
        		elseif($ftype == "date"){
        			$FormatDate 	= new Zend_Date($formatVal, $this->_dateDisplayFormat);
					$dbDate   	= $FormatDate->toString($this->_dateDBFormat);	
					$select->where('DATE('.$fcolumn.') '.$opt.' "'.$dbDate.'"');
        		}
        		else{
        			$select->where($fcolumn.' '.$opt.' "'.$formatVal.'"');
        		}
        	}
        }

        $select->order($sortBy.' '.$sortDir);

        //echo $select;
        //die;		
		
		//jika tombol csv dan pdf ditekan
		// if($pdf || $csv)
		// {
		//     $fUpdatedStart 	= $zf_filter->getEscaped('updatedStart');
		//     $fUpdatedEnd 	= $zf_filter->getEscaped('updatedEnd');
		// }

		if ($csv || $pdf)
		{	
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			$dataSQL = $this->_db->fetchAll($select);
		}
		else
		{
			$this->paging($select);
		}
		
		$data = array();

		if($csv || $pdf){
			foreach ($dataSQL as $d => $dt)
			{
				$persenLabel = $dt["BALANCE_TYPE"] == '2' ? ' %' : '';
				foreach ($fields as $key => $field)
				{
					$value 	   = $dt[$key];
					$PSSTATUS  = $dt["PS_STATUS"];
					$PSNUMBER  = $dt["payReff"];
					$payStatus = $dt["payStatus"];
					
					if($key == 'acbenef' && $dt['PS_TYPE'] == '12'){
						$value = '-';	
					}

					if ($key == "payStatus"){
						if($PSSTATUS == 5){
							
							// COMPLETED WITH (_) TRANSACTION (S) FAILED 
							// TRA_STATUS FAILED (4)
							
							$select = $this->_db->select()
												->from('T_TRANSACTION',array('countfailed'=>'count(TRANSACTION_ID)'))
												->where("TRA_STATUS = '4' AND PS_NUMBER = ?",$PSNUMBER);
							$countFailed = $this->_db->fetchOne($select);
							
							if($countFailed == 0) 	$value = $payStatus;
							else 					$value = 'Completed with '.$countFailed.' Failed Transaction(s)';
							
						}
						else $value = $payStatus;
					}

					if($key == "amount" && !$csv){
						if(!empty($persenLabel)){
							if(($dt['PS_TYPE'] == '14' || $dt['PS_TYPE'] == '15')){
								if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
									$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';
								}else{
									$value = '-';
								}
							}else{
								$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';
							}
						}elseif(($dt['PS_TYPE'] == '14' || $dt['PS_TYPE'] == '15')){
								if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
									$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
								}else{
									$value = '-';
								}
						}else{
							$value = Application_Helper_General::displayMoney($value);
						}
					}
					elseif ($key == "amount" && $csv){
						if(!empty($persenLabel)){
							if(($dt['PS_TYPE'] == '14' || $dt['PS_TYPE'] == '15')){
								if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
									$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';
								}else{
									$value = '-';
								}
							}else{
								$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';
							}
						}elseif(($dt['PS_TYPE'] == '14' || $dt['PS_TYPE'] == '15')){
							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
								$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
							}else{
								$value = '-';
							}
						}else{
							$value = Application_Helper_General::displayMoney($value);
						}
					}			
					elseif($key == "created" || $key == "updated")	{ $value = Application_Helper_General::convertDate($value,$this->view->displayDateTimeFormat); }
					elseif($key == "efdate") { $value = Application_Helper_General::convertDate($value,$this->_dateViewFormat,$this->view->defaultDateFormat); }
					
					$value = ($value == "" && !$csv)? "&nbsp;": $value;
					$data[$d][$key] = $this->language->_($value);
				}
			}
		}
		
		if($csv)
		{
			$this->_helper->download->csv($header,$data,null,'List View Payment');  
			Application_Helper_General::writeLog('DARC','Export CSV View Payment');
		}
		elseif($pdf)
		{
			$this->_helper->download->pdf($header,$data,null,'List View Payment');  
			Application_Helper_General::writeLog('DARC','Export PDF View Payment');
		}
		else
		{	
			//$this->view->data 				= $data;
			
			Application_Helper_General::writeLog('DARC','View Payment');
		}

		if($clearfilter == 1){
			$this->view->filter = NULL;
		}
		
		$this->view->wherectr = count($whereCols) + 1;

		$this->view->wherecol = $whereCols;
		$this->view->whereopt = $whereOpts;
		$this->view->whereval = $whereVals;

		$this->view->fields = $fields;
	}

	public function psstatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $arrPayStatus   = array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
		foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($optpayStatusRaw as $key => $row){
            $optHtml.="<option value='".$key."'>".$row."</option>";
        }

        echo $optHtml;
    }

    public function pstypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $payType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		
		foreach($payType as $key => $value){ 
			 $optpaytypeRaw[$key] = $this->language->_($value); 
		}

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($optpaytypeRaw as $key => $row){
            $optHtml.="<option value='".$key."'>".$row."</option>";
        }

        echo $optHtml;
    }

    public function accsrcAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        $arrAccount     = $CustomerUser->getAccounts();
		
		if(is_array($arrAccount) && count($arrAccount) > 0){
			foreach($arrAccount as $key => $value){
				
				$val 		= $arrAccount[$key]["ACCT_NO"];
				$ccy 		= $arrAccount[$key]["CCY_ID"];
				$acctname 	= $arrAccount[$key]["ACCT_NAME"];
				$accttype 	= ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';	// 10 : saving, 20 : giro;
				
				$arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';
				
			}
		}
		else { $arrAccountRaw = array();}

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($arrAccountRaw as $key => $row){
            $optHtml.="<option value='".$key."'>".$row."</option>";
        }

        echo $optHtml;
    }

    public function ccyAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
       
        $optHtml.="<option value='IDR'>IDR</option>";
        $optHtml.="<option value='USD'>USD</option>";
       

        echo $optHtml;
    }


    public function acbenefAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();


        $ACBENEFArr = array();

        $select = $this->_db->select()
                            ->from(array('B'            => 'M_BENEFICIARY_USER'),
                                   array('ACBENEF_ID'   => 'B.BENEFICIARY_ID')
                                   )
                            ->join(array('A'=>'M_BENEFICIARY'), 'A.BENEFICIARY_ID = B.BENEFICIARY_ID',array('A.BENEFICIARY_ACCOUNT'))
                            ->where("B.CUST_ID  = ?" , (string)$this->_custIdLogin)
                            ->where("B.USER_ID  = ?" , (string)$this->_userIdLogin);

        $ACBENEFArr = $this->_db->fetchAll($select);

        // bila empty, user bene linkage cannot see all payment
        if (empty($ACBENEFArr))
        {   $ACBENEFArr[] = "0";        }
                    
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            $optHtml.="<option value='".$value['ACBENEF_ID']."'>".$value['BENEFICIARY_ACCOUNT']."</option>";
        }

        echo $optHtml;
    }

    public function countfailedAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $id = $this->_getParam('id');
        $stat = $this->_getParam('stat');

        $select = $this->_db->select()
							->from('T_TRANSACTION',array('countfailed'=>'count(TRANSACTION_ID)'))
							->where("TRA_STATUS = '4' AND PS_NUMBER = ?",$id);
		$countFailed = $this->_db->fetchOne($select);
        

        if($countFailed == 0) 	
        	echo $this->language->_($stat);
		else 
			echo $this->language->_('Completed with '.$countFailed.' Failed Transaction(s)');
    }

    public function fieldlistAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $filterProp = $this->_filterProp;
		$fieldlist = '<option value="">-- '.$this->language->_('Please Select').' --</option>';
		foreach($filterProp as $key => $val){
			$fieldlist.='<option value="'.$key.'-'.$val['type'].'">'.$val['label'].'</option>';
		}

        echo $fieldlist;
    }
}