<?php
class Cmdreport_beneficiarylistController extends Application_Main
{	
	public function indexAction()
	{
		$getBeneficiary = new cmdreport_Model_Cmdreport();
		
		
		$filterSend = array();
		
		$listUser = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$listUser = array_merge($listUser,Application_Helper_Array::listArray($this->getUserID(),'USER_ID','USER_ID'));
		$this->view->listUser = $listUser;
		Application_Helper_General::writeLog('RPBU','View Destination List Report');
		$listBeneTypea = array(''=>'--- '.$this->language->_('Any Value').' ---');
    	//$listBeneTypeb = array_combine(array_values($this->_beneftype['code']),array_values($this->_beneftype['desc']));
		$listBeneTypeb	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		
		foreach($listBeneTypeb as $key => $value)
		{ 
			$optpayStatusRaw[$key] = $this->language->_($value); 
		}
		//Zend_Debug::dump($optpayStatusRaw);
		//unset($listBeneTypeb['16']);
    	//unset($listBeneTypeb['17']);
    	
    	unset($optpayStatusRaw['16']);
    	unset($optpayStatusRaw['17']);
    	
    	//$listBeneType = $listBeneTypea+$listBeneTypeb;
    	$listBeneType = $listBeneTypea+$optpayStatusRaw;
    	
		$this->view->listBeneType = $listBeneType;
		
	
		
		
		$userid = $this->language->_('User ID'); 
		$username = $this->language->_('User Name'); 
		$beneficiaryaccountnumber = $this->language->_('Beneficiary Account Number'); 
		$beneficiaryname = $this->language->_('Beneficiary Name'); 
		$beneficiaryalias = $this->language->_('Beneficiary Alias'); 
		$currency = $this->language->_('Currency'); 
		$createby = $this->language->_('Created By');  
		$createddate = $this->language->_('Created Date');
		$type = $this->language->_('Type');
		$approvestatus = $this->language->_('Approved Status');
		$requesttodelete = $this->language->_('Request to Delete');
		
		$fields = array(
						'user_id'  		=> array('field' => 'B.USER_ID',
											   	'label' => $userid,
											   	'sortable' => true),
						'user_name'  	=> array('field' => 'USER_FULLNAME', 
											   	'label' => $username,
											   	'sortable' => true),
						'benef_acct'  	=> array('field' => 'BENEFICIARY_ACCOUNT',
											   	'label' => $beneficiaryaccountnumber,
											   	'sortable' => true),
						'benef_name'  	=> array('field' => 'BENEFICIARY_NAME',
											   	'label' => $beneficiaryname,
											   	'sortable' => true),
						/*'benef_alias'  	=> array('field' => 'BENEFICIARY_ALIAS',
											   	'label' => $beneficiaryalias,
											   	'sortable' => true),*/
						'ccy'   		=> array('field'   => 'CURR_CODE',
											  	'label'    => $currency,
											  	'sortable' => true),
						'created_by'  	=> array('field' => 'BENEFICIARY_CREATEDBY',
											   'label' => $createby,
											   'sortable' => true),
						'created_date'  => array('field' => 'BENEFICIARY_CREATED',
											   	'label' => $createddate,
											   	'sortable' => true),
						'type'  		=> array('field' => 'BENEFICIARY_TYPE',
												'label' => $type,
												'sortable' => true),
						'approve'  		=> array('field' => '',
												'label' => 'Approved Status',
												'sortable' => true),
//						'isapprove'  		=> array('field' => 'BENEFICIARY_ISAPPROVE',
//												'label' => $approvestatus,
//												'sortable' => true),
						/*'requestdelete'  		=> array('field' => 'BENEFICIARY_ISREQUEST_DETELE',
												'label' => $requesttodelete,
												'sortable' => true),*/
				);
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','user_id');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'userid' 		=> array('StringTrim','StripTags','StringToUpper'),
							'username'    	=> array('StringTrim','StripTags','StringToUpper'),
							'beneacct'    	=> array('StringTrim','StripTags','StringToUpper'),
							'beneacctname'	=> array('StringTrim','StripTags','StringToUpper'),
							'beneaccttype'	=> array('StringTrim','StripTags','StringToUpper'),
		);
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		//if($filter == 'Set Filter')
		if($filter == true)
		{
			$userid = $zf_filter->getEscaped('userid');
			$username = $zf_filter->getEscaped('username');
			$beneacct = $zf_filter->getEscaped('beneacct');
			$beneacctname = $zf_filter->getEscaped('beneacctname');
			$beneaccttype = $zf_filter->getEscaped('beneaccttype');
			
			if($userid)
			{
				$filterSend['userid'] = $userid;
			}
			
			if($username)
			{
				$filterSend['username'] = $username;
			}
			
			if($beneacct)
			{
				$filterSend['beneacct'] = $beneacct;
			}
			
			if($beneacctname)
			{
				$filterSend['beneacctname'] = $beneacctname;
			}
			
			if($beneaccttype)
			{
				$filterSend['beneaccttype'] = $beneaccttype;
			}
			
			$this->view->userid = $userid;
			$this->view->username = $username;
			$this->view->beneacct = $beneacct;
			$this->view->beneacctname = $beneacctname;
			$this->view->beneaccttype = $beneaccttype;
		}
		
		$sorting = $sortBy.' '.$sortDir;
		$result = $getBeneficiary -> getBeneficiary('list',$filterSend,$sorting);
		$arr = $result;
		foreach($result as $key => $val)
		{
			unset($arr[$key]['BENEFICIARY_ISAPPROVE']);
			unset($arr[$key]['BENEFICIARY_ALIAS']);
		}
		
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			$headerNew  = array($this->language->_('User ID'),$this->language->_('User Name'),$this->language->_('Beneficiary Account Number'),$this->language->_('Beneficiary Name'),$this->language->_('Currency'),$this->language->_('Created By'),$this->language->_('Created By'),$this->language->_('Type'));
							
			if($csv)
			{
				$this->_helper->download->csv($header,$arr,null,'Beneficiary User List');  
				// $this->backendLog('CSV', $this->_moduleDB, null,null,null);  
			}
			else if($pdf)
			{
				$this->_helper->download->pdf($headerNew,$arr,null,'Beneficiary User List');    
				// $this->backendLog('PDF', $this->_moduleDB, null,null,null);  
			}
			else if($this->_request->getParam('print') == 1){
	            $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => $this->language->_('Beneficiary User List'), 'data_header' => $fields));
	       	}
		}
		
		$this->paging($result);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}
}
