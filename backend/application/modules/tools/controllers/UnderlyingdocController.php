<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
class tools_UnderlyingdocController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->params = $this->_request->getParams();
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1)
		{
      		if($temp[0]=='F' || $temp[0]=='S')
			{
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		
		$arr = null;
		$viewFilter = null;

		$select  = $this->_db->select()
	      ->from(array('B' => 'M_CUSTOMER'), array(
	        'CUST_ID',
	        'CUST_NAME' => new Zend_Db_Expr("CONCAT(CUST_NAME , ' (' , CUST_ID , ')  ' )")
	      ));
	    $select->where("B.CUST_STATUS not in (3, 2)");
	    // echo $select;
	    $datacust =  $select->query()->fetchAll();

	    $custArr  = Application_Helper_Array::listArray($datacust, 'CUST_ID', 'CUST_NAME');
	    $custArr  = array_merge(array('' => '-- ' . $this->language->_('Any Value') . ' --'), $custArr);
	    $this->view->custArr = $custArr;
		
		$select = $this->_db->select()->distinct()
			->from(array('A' => 'M_USER'),
				array('USER_ID', 'USER_FULLNAME', 'CUST_ID'))
				->where("CUST_ID LIKE ".$this->_db->quote($this->_custIdLogin))
				->order('USER_FULLNAME ASC')
				 ->query() ->fetchAll();
				 
       	$userlist = array(""=>'-- '.$this->language->_('Any Value') .'--');
		$userlist +=Application_Helper_Array::listArray($select,'USER_ID','USER_FULLNAME');
		$this->view->listCustId = $userlist;
		
		$fields = array	(
							'module'  			=> array	(
																	'field' => 'FILE_NAME',
																	'label' => $this->language->_('Module'),
																	'sortable' => true
																),
							'compName'  			=> array	(
																	'field' => 'COMPANY',
																	'label' => $this->language->_('Company'),
																	'sortable' => true
																),
																
							'DocumentNumber'  		=> array	(
																	'field' => 'DOCUMENT_NUMBER',
																	'label' => $this->language->_('Doucment Reff'),
																	'sortable' => true
																),
							'FileDescription'  		=> array	(
																	'field' => 'FILE_DESCRIPTION',
																	'label' => $this->language->_('Document Description'),
																	'sortable' => true
																),
							//'FileName'  			=> array	(
							//										'field' => 'FILE_NAME',
							//										'label' => $this->language->_('File Name'),
							//										'sortable' => true
							//									),
							
							
							
							'Upload Date and Time'  => array	(
																	'field' => 'FILE_UPLOADED_TIME',
																	'label' => $this->language->_('Last Uploaded by'),
																	'sortable' => true
																)
						);
		$this->view->fields = $fields;

   		$filterlist = array('SEARCH_TEXT','UPLOADED_BY','UPDATED');
		
		$this->view->filterlist = $filterlist;

		
		$page    = $this->_getParam('page');		
        $sortBy  = $this->_getParam('sortby','');
        $sortDir = $this->_getParam('sortdir','asc');
		
        $page 		= (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
        $sortBy 	= (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
				
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array(	
							'SEARCH_TEXT'   	=> array('StringTrim','StripTags','StringToUpper'),
							'UPLOADED_BY' 	  	=> array('StringTrim','StripTags'),
							'UPDATED' 	  		=> array('StringTrim','StripTags'),
							'UPDATED_END' 	  	=> array('StringTrim','StripTags'),
		);

		$dataParam = array("SEARCH_TEXT","UPLOADED_BY");
		$dataParamValue = array();
		 foreach ($dataParam as $dtParam)
		 {			  
			  
			 if(!empty($this->_request->getParam('whereco'))){
				 $dataval = $this->_request->getParam('whereval');
					 foreach ($this->_request->getParam('whereco') as $key => $value) {
						 if($dtParam==$value){
							 if(!empty($dataParamValue[$dtParam])){
								 $dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							 }
							 
							 $dataParamValue[$dtParam] = $dataval[$key];
						 }
					 }
 
			 }			 
 
			 // $dataPost = $this->_request->getPost($dtParam);
			 // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
	 }

		if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['UPDATED'] = $createarr[0];
					$dataParamValue['UPDATED_END'] = $createarr[1];
			}
		
		$options = array('allowEmpty' => true);
		$validators = array(
							'SEARCH_TEXT'   	=> array(),
							'UPLOADED_BY'   	=> array(),
							'UPDATED' 	  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
							'UPDATED_END' 	  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		
		);
		$zf_filter 	= new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
		
		// $filter 	= $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$select = $this->_db->select()
							->distinct()
							->from(	
									array('TFS'=>'T_FILE_SUBMIT'),
									array(
											'FILE_ID'			=>'TFS.FILE_ID',
											'FILE_NAME'			=>'TFS.FILE_NAME',
											'FILE_DESCRIPTION'	=>'TFS.FILE_DESCRIPTION',
											'DOCUMENT_NUMBER'	=>'TFS.DOCUMENT_NUMBER',											
											'FILE_UPLOADED_TIME'=>'TFS.FILE_UPLOADED_TIME',
											'FILE_SYSNAME'		=>'TFS.FILE_SYSNAME',
											'FILE_UPLOADEDBY'	=>'TFS.FILE_UPLOADEDBY',
											'BUSER_NAME'		=>'MU.BUSER_NAME',
											'COMPANY' 			=> new Zend_Db_Expr("CONCAT(MC.CUST_NAME , ' (' , MC.CUST_ID , ')  ' )"),
											'FILE_DOWNLOADED' 	=>'TFS.FILE_DOWNLOADED',
											'FILE_MODULE' 	=>'TFS.FILE_MODULE',
										))
							->joinLeft(array('MU'=>'M_BUSER'),'MU.BUSER_ID=TFS.FILE_UPLOADEDBY',array())
							->joinLeft(array('MC'=>'M_CUSTOMER'),'MC.CUST_ID=TFS.CUST_ID',array())
							 ->where('TFS.FILE_DELETED =?','0')
							// ->where('TFS.CUST_ID =?',$this->_custIdLogin)
							->order('TFS.FILE_UPLOADED_TIME DESC');

			 $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;  

	    $AESMYSQL = new Crypt_AESMYSQL();
	    $PS_NUMBER      = urldecode($this->_getParam('FILE_ID'));
	    $FILE_ID = $AESMYSQL->decrypt($PS_NUMBER, $password);

    // $user_id = strtoupper($user_id);
		// echo $select; die();
		// $FILE_ID   	= $this->_getParam('FILE_ID');
		if($FILE_ID)
		{	


			$select->where('FILE_ID =?',$FILE_ID);
			$data = $this->_db->fetchRow($select);
			
			$attahmentDestination = UPLOAD_PATH.'/document/temp/';
			//echo  $attahmentDestination.$data['FILE_SYSNAME'];die;
			$this->_helper->download->file($data['FILE_NAME'],$attahmentDestination.$data['FILE_SYSNAME']);
			
			$updateArr = array();
			$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED']+1;
			$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;
			
			$whereArr = array('FILE_ID = ?'=>$FILE_ID);
			
			$fileupdate = $this->_db->update('T_FILE_SUBMIT',$updateArr,$whereArr);
			//echo "file downloaded: ".$data['FILE_DOWNLOADED'];
			Application_Helper_General::writeLog('VDEL','Download File Underlying Document '.$data['FILE_NAME']);
		}
		else{
			Application_Helper_General::writeLog('VDEL','View File Underlying Document');
		}
		
		if($filter == null)
		{	
			$DATE_START = NULL;//(date("d/m/Y"));
			$DATE_END = NULL;//(date("d/m/Y"));
			$this->view->DATE_START  = $DATE_START;
			$this->view->DATE_END  = $DATE_END;
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}
		// print_r($zf_filter->getEscaped('SEARCH_TEXT'));die;
		// if($filter == TRUE)
		// {
		$SEARCH_TEXT   	= $zf_filter->getEscaped('SEARCH_TEXT');
		$UPLOADED_BY    = $zf_filter->getEscaped('UPLOADED_BY');
		$DATE_START    	= $zf_filter->getEscaped('UPDATED');
		$DATE_END		= $zf_filter->getEscaped('UPDATED_END');

		$this->view->createdStart 	= $DATE_START;
		$this->view->createdEnd 	= $DATE_END;
		
		if($SEARCH_TEXT)
		{
			// $select->where("UPPER(FILE_NAME) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%'));
			$this->view->SEARCH_TEXT = $SEARCH_TEXT;
			$SEARCH_TEXTarr = explode(',', $SEARCH_TEXT);
			$select->where("UPPER(FILE_NAME)  in (?)", $SEARCH_TEXTarr );
		}
		
		if($UPLOADED_BY)
		{
			// $select->where("UPPER(MU.USER_ID) = ".$this->_db->quote($UPLOADED_BY));
			$this->view->UPLOADED_BY = $UPLOADED_BY;
			$UPLOADED_BYarr = explode(',', $UPLOADED_BY);
			$select->where("UPPER(MU.USER_FULLNAME)  in (?)", $UPLOADED_BYarr );
		}
		// }
		
		if($filter == null || $filter == TRUE)
		{
			if($DATE_START)
			{
				$FormatDate 	= new Zend_Date($DATE_START, $this->_dateDisplayFormat);
				$from   	= $FormatDate->toString($this->_dateDBFormat);	
				if($from)
				{
					$select->where('DATE(FILE_UPLOADED_TIME) >= '.$this->_db->quote($from));
					
				}
				$this->view->DATE_START = $DATE_START;
			}
			
			if($DATE_END)
			{
				$FormatDate 	= new Zend_Date($DATE_END, $this->_dateDisplayFormat);
				$to   	= $FormatDate->toString($this->_dateDBFormat);
				if($to)
				{
					$select->where('DATE(FILE_UPLOADED_TIME) <= '.$this->_db->quote($to));
					
				}
				$this->view->DATE_END = $DATE_END;
			}
		}
//		if(!$this->_request->isPost()){
//		Application_Helper_General::writeLog('VDEL','View File Sharing report');
//		}
		$select->where("FILE_TYPE = ?",'2');
		$select->order($sortBy.' '.$sortDir);
		// echo $select;die;
		//DIE($select);
		//$arr = $this->_db->fetchAll($select);
		$this->paging($select);
		if (!empty($dataParamValue)) {
			// $this->view->createdStart = $dataParamValue['PS_CREATED'];
			// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
		//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
		//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

			// unset($dataParamValue['PS_CREATED_END']);
			// unset($dataParamValue['PS_EFDATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',',$value);
							if(!empty($duparr)){
								
								foreach($duparr as $ss => $vs){
									$wherecol[]	= $key;
									$whereval[] = $vs;
								}
							}else{
									$wherecol[]	= $key;
									$whereval[] = $value;
							}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
				
    	}
	// 	if(!empty($dataParamValue)){
	// 		foreach ($dataParamValue as $key => $value) {
	// 			$wherecol[]	= $key;
	// 			$whereval[] = $value;
	// 		}
    //     $this->view->wherecol     = $wherecol;
    //     $this->view->whereval     = $whereval;
     
    //   }
      	$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash; 
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
		
		$params = $this->_request->getParams();
		//echo '<pre>';
		
		
		$upload = $this->_getParam('upload');
		$maxFileSize = "1024"; //"1024000"; //1024
		//~ $fileExt = "csv,dbf";
		$fileExt = "csv,dbf,pdf";
		
		$delete = $this->_getParam('multiReject');
		
		//var_dump($delete)
		//var_dump($params);die;
		if($this->_request->isPost() && $upload)
		{
			
			$attahmentDestination 	= UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			// print_r($attahmentDestination);die;
			$params = $this->_request->getParams();

			$sourceFileName = trim($adapter->getFileName());
			// print_r($sourceFileName);die;
			if($sourceFileName == null){

				$sourceFileName = null;
				$fileType = null;
			}
			else
			{
			//	var_dump($_FILES);
				$sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
				if($_FILES["document"]["type"])
				{
					$adapter->setDestination($attahmentDestination);
					$fileType 				= $adapter->getMimeType();
					$size 					= $_FILES["document"]["size"];
				}
				else{
					$fileType = null;
					$size = null;
				}
			}
			$paramsName['sourceFileName'] 	= $sourceFileName;

			$cid 						= $this->_getParam('cid');
			$description 				= $this->_getParam('description');
			$doc_number 				= $this->_getParam('doc_number');
			$paramsName['description']  = $description;

			$filtersName = array(
									'cid'				=> array('StringTrim'),									
									'sourceFileName'	=> array('StringTrim'),
									'doc_number'		=> array('StringTrim'),
									'description'		=> array('StringTrim'),
								);

			$validatorsName = array(
									'cid' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error : Please Choose Company')
																				)
														),
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error : File cannot be left blank')
																				)
														),
									'doc_number' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error: Document Number cannot be left blank')
																				)
														),
									'description' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error: Description cannot be left blank')
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
		
			if($sourceFileName == NULL){
	//	die('here');
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName']))? $errors['sourceFileName'] : null;

			}
			elseif($doc_number == NULL){
//die('here1');

				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->doc_numberErr 	= (isset($errors['doc_number']))? $errors['doc_number'] : null;
			}
			elseif($description == NULL){
//die('here1');

				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->descriptionErr 	= (isset($errors['description']))? $errors['description'] : null;

			}
			elseif($cid == NULL){
//die('here1');

				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->descriptionErr 	= (isset($errors['cid']))? $errors['cid'] : null;

			}
			else{

				if($zf_filter_input_name->isValid())
				{
					//var_dump($fileType);
					$fileTypeMessage = explode('/',$fileType);
					if (isset($fileTypeMessage[1])){
						$fileType =  $fileTypeMessage[1];
						//var_dump($fileExt);
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
						$extensionValidator->setMessage("Extension file must be *.csv /.dbf / .pdf");

						$size 			= number_format($size);
						$sizeTampil 	= $size/1000;
						//$sizeValidator 	= new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
						// $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");

						$adapter->setValidators(array($extensionValidator));

						if($adapter->isValid())
						{
							//die('her');
							$date = date("dmy");
							$time = date("his");

							$newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
							$adapter->addFilter('Rename', $newFileName);

							if($adapter->receive())
							{


								$filedata = $attahmentDestination.$newFileName;
								// var_dump($filedata);
								if (file_exists($filedata)) {
								exec("clamdscan --fdpass ".$filedata, $out ,$int);
									
								// if()
									// var_dump($int);
									// var_dump($out);
									// die;
								if($int == 1){

									unlink($filedata);
									//die('here');
									$this->view->error = true;
									$errors = array(array('FileType'=>'Error: An infected file. Please upload other file'));
									$this->view->errorMsg = $errors;


								}else{

									try
									{
										$this->_db->beginTransaction();
										$insertArr = array(
												'CUST_ID'				=> $cid,
												'FILE_TYPE'				=> '2',
												'FILE_UPLOADEDBY'		=> $this->_userIdLogin,
												'FILE_NAME' 			=> $sourceFileName,
												'FILE_DESCRIPTION'		=> $description,
												'DOCUMENT_NUMBER'		=> $doc_number,
												'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
												'FILE_DOWNLOADED'		=> '0',
												'FILE_DELETED'			=> '0',
												'FILE_DOWNLOADEDBY'		=> '',
												'FILE_DELETEDBY'		=> '',
												'FILE_ISEMAILED'		=> '0',
												'FILE_SYSNAME'			=> $newFileName,
										);

										$this->_db->insert('T_FILE_SUBMIT', $insertArr);
										$this->_db->commit();

										Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);

										$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
										$this->_redirect('/notification/success');
									}
									catch(Exception $e)
									{										
										$this->_db->rollBack();
										Application_Log_GeneralLog::technicalLog($e);
									}


								}
							}

							}
						}
						else
						{
							
							$this->view->error = true;
							$errors = array($adapter->getMessages());
							//var_dump($errors);
							//die('here3');
							$this->view->errorMsg = $errors;
						}
					}else{
						$this->view->error = true;
						$errors = array(array('FileType'=>'Error: Unknown File Type'));
						$this->view->errorMsg = $errors;
					}
				}else{
					$this->view->error = true;
					$errors 						= $zf_filter_input_name->getMessages();
					$this->view->errorMsg = $errors;
					//var_dump($errors);die('here');
				}
			}
		}else if($this->_request->isPost() && $delete){
			//die('here'); 
				try
				{
					$postreq_id	= $this->_request->getParam('mPayReff');
					
					foreach ($postreq_id as  $key =>$value)
					{
						
						$AESMYSQL = new Crypt_AESMYSQL();
						$FILEENC      = urldecode($value);
						$FILE_ID_DELETE = $AESMYSQL->decrypt($FILEENC, $password);						
						//$FILE_ID_DELETE =  $postreq_id[$key];

						$this->_db->beginTransaction();

						$param = array();
						$param['FILE_DELETED'] = '1';
						$param['FILE_DELETEDBY'] = $this->_userIdLogin;

						$where = array('FILE_ID = ?' => $FILE_ID_DELETE);
						$query = $this->_db->update( "T_FILE_SUBMIT", $param, $where );
						$stringlog = 'Delete Document Underlying'.$FILE_ID_DELETE;
						Application_Helper_General::writeLog('DELI',$stringlog);
						$this->_db->commit();
					}
				}
				catch(Exception $e)
				{
					// var_dump($e);die;
					$this->_db->rollBack();
				}
				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
				$this->_redirect('/notification/success/index');
		}
		//var_dump($errors);
		if(!empty($errors)){
			
			foreach($errors as $keyRoot => $rowError)
			{
			   foreach($rowError as $errorString)
			   {
				  $errorArray[$keyRoot] = $errorString;
			   }
			}
			if(!empty($errorArray['sourceFileName'])){
				$error = $errorArray['sourceFileName'];
			}else if(!empty($errorArray['FileType'])){
				$error = $errorArray['FileType'];
			}else if(!empty($errorArray['description'])){
				$error = $errorArray['description'];
			}else if(!empty($errorArray['doc_number'])){
				$error = $errorArray['doc_number'];
			}
			// var_dump($errorArray);
			$this->view->error = true;
			$this->view->errormsg = $error;
		}
		
		$FILE_DESCRIPTION_len = (isset($description))? strlen($description): 0;
		$FILE_DESCRIPTION_len = 150 - $FILE_DESCRIPTION_len;

		//$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;

		$this->view->FILE_DESCRIPTION_len		= $FILE_DESCRIPTION_len;


		$this->view->max_file_size = $maxFileSize; //'1024';
		$this->view->file_extension = $fileExt;
		Application_Helper_General::writeLog('DELI',"Viewing Document Underlying");

	}


	public function addAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->params = $this->_request->getParams();
		
		$selectcomp = $this->_db->select()
                            ->from('M_CUSTOMER', array('value' => new Zend_Db_Expr("CONCAT(CUST_NAME , ' (' , CUST_ID , ')  ' )"),
														 'CUST_NAME','CUST_ID','CUST_TYPE'))
                            
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);

        $this->view->custarr = json_encode($tempColumn);
		
		$arrModule = array();
		$arrModule['1'] = 'Notional Pooling';
		$arrModule['2'] = 'Auto Debit';
		$arrModule['3'] = 'Bank Guarantee';
		$this->view->arrModule = $arrModule;
		
		      $submit = $this->_getParam('submit');
      if($submit)
      {
        $data = $this->_request->getParams();
		
			$attahmentDestination 	= UPLOAD_PATH.'/document/temp/';		
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			$adapter->setDestination ($attahmentDestination);
			$ext = array('txt','png', 'jpg', 'bmp', 'pdf','xls','xlsx','csv');
		//	$extensionValidator = new Zend_Validate_File_Extension(array(false, $ext));
		//	$extensionValidator->setMessage(
		//										$this->language->_('Error: Extension file must be *.txt')
			//								);
											
			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
										$this->language->_('Error: File size must not more than ').$this->getSetting('Fe_attachment_maxbyte')
										);

			$adapter->setValidators(array($sizeValidator));	
			$files = $adapter->getFileInfo();
			$data = $this->_request->getParams();
			
			$error = false;
			if(empty($data['COMPANY_ID']) || empty($data['COMPANY_TEXT'])){
				//echo 'here';die;
				$error = true;
				$errormsg = $this->language->_('Mandatory field must be filled');
			}
			
			if(count($files) != count($data['docid'])){
				$error = true;
				$errormsg = $this->language->_('Mandatory field must be filled');
			}
			//echo '<pre>';
			//var_dump(count($files));
			//var_dump(count($data['docid']));
			//var_dump($data);die;
			 //
			if($adapter->isValid() && !$error)
			{
				//echo '<pre>';
				//var_dump($files);
				$i = 0;	
				foreach($files as $key => $val){
				
				//echo '<pre>';
				//var_dump($val);
				$sourceFileName = $val['name'];
				$newFileName = $val['name'] . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				$adapter->addFilter ( 'Rename',$newFileName);
				$adapter->receive($val);
				//Zend_Debug::dump($val);
				//$myFile = file_get_contents($newFileName);
				//$arry_myFile = explode("\n", $myFile);
				//$adapter->addFilter('Rename', $newFileName);
				//@unlink($newFileName);
				//Zend_Debug::dump($myFile);
				$namaFile = $val['name'];
				$namaSementara = $val['tmp_name'];
				//try{
				$terupload = move_uploaded_file($namaSementara, $attahmentDestination.$newFileName);
				//}catch(Exception $e)
					//							{
						//							var_dump($e);die;
							//					}
				//var_dump($terupload);
				if($terupload)
				{
					//die('here');
					$filedata = $attahmentDestination.$newFileName;
					//var_dump($filedata);
					if (file_exists($filedata)) {
								exec("clamdscan --fdpass ".$filedata, $out ,$int);
					}
					//var_dump($i);
							//var_dump($data['docdesc']);
					//var_dump($int);
							//	if($int == 1){
							//echo 'here';die;
							//		unlink($filedata);
							//		//die('here');
							//		$this->view->error = true;
							//		$errors = array(array('FileType'=>'Error: An infected file. Please upload other file'));
							//		$this->view->errorMsg = $errors;


							//	}else{
							
								$insertArr = array(
															'CUST_ID'				=> $data['COMPANY_ID'],
															'FILE_UPLOADEDBY'		=> $this->_userIdLogin,
															'FILE_TYPE'				=> '2',
															'FILE_NAME' 			=> $sourceFileName,
															'FILE_DESCRIPTION'		=> $data['docdesc'][$i],
															'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
															'FILE_DOWNLOADED'		=> '0',
															'FILE_DELETED'			=> '0',
															'FILE_DOWNLOADEDBY'		=> '',
															'FILE_DELETEDBY'		=> '',
															'FILE_ISEMAILED'		=> '0',
															'FILE_SYSNAME'			=> $newFileName,
															'FILE_MODULE'			=> $data['filemodule'],
															'DOCUMENT_NUMBER'			=> $this->generateDocID()
											);
											Zend_Debug::dump($insertArr);
										try{
												$this->_db->insert('T_FILE_SUBMIT', $insertArr);
										}
												catch(Exception $e)
												{
													var_dump($e);die;
												}
									
								//}
				}else{
					//Zend_Debug::dump($adapter->getMessages());die;
				}
			//	echo '<pre>';
			//var_dump($files);die;
				$i++;
				}
				//die;
				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
						$this->_redirect('/notification/success');
			}else{
				if(!empty($adapter->getMessages())){
					$error = true;
					if(empty($adapter->getMessages()['fileUploadErrorNoFile'])){
						$errormsg = $adapter->getMessages();
					}
					if(empty($errormsg)){
						$errormsg = $this->language->_('Mandatory field must be filled');
					}
				}
				$data = $this->_request->getParams();
				//echo '<pre>';
				//var_dump($data);
				//die;
				$this->view->COMPANY_TEXT = $data['COMPANY_TEXT'];
				$this->view->COMPANY_ID = $data['COMPANY_ID'];
				$this->view->COMPANY_TEXT = $data['COMPANY_TEXT'];
				$this->view->docdesc	= $data['docdesc'];
				$this->view->errormsg = $errormsg;
				$this->view->error = $error;
				
				//Zend_Debug::dump($adapter->getMessages());die;
			}
		
		
		
		
		
		/*$insertArr = array(
												'CUST_ID'				=> $data['COMPANY_ID'],
												'FILE_UPLOADEDBY'		=> $this->_userIdLogin,
												'FILE_TYPE'				=> '1',
												'FILE_NAME' 			=> $sourceFileName,
												'FILE_DESCRIPTION'		=> $description,
												'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
												'FILE_DOWNLOADED'		=> '0',
												'FILE_DELETED'			=> '0',
												'FILE_DOWNLOADEDBY'		=> '',
												'FILE_DELETEDBY'		=> '',
												'FILE_ISEMAILED'		=> '0',
												'FILE_SYSNAME'			=> $newFileName,
								);

							$this->_db->insert('T_FILE_SUBMIT', $insertArr);
	   */
	  }
	
	}	
	
	public function generateDocID(){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(2));
		$trxId = 'DOC'.$currentDate.$seqNumber;

		return $trxId;
	}
	
	
	public function custAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

//      if ($priviBeneLinkage == true)
//      {
            
			$select = $this->_db->select()
                                ->from(array('B'            => 'M_CUSTOMER'),
                                       array('CUST_ID'   => 'B.CUST_ID')
                                       );		
			if(!empty($tblName)){
				$select->where('B.CUST_NAME LIKE ?','%'.$tblName.'%');
			}
			
            $ACBENEFArr = $this->_db->fetchAll($select);
            
                    
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['CUST_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['CUST_ID']."' ".$select.">".$value['CUST_ID']."</option>";
        }

        echo $optHtml;
    }
}
