<?php

class bgclosingworkflow_Model_Repairclosing
{
    protected $_db;

    public function __construct()
    {
        $this->_db = Zend_Db_Table::getDefaultAdapter();
    }

    /**
     * @param array|null $filter
     * @param int|null $status
     * @param string $custIdlogin
     * 
     * @return array
     */
    public function getAllClosingBg(array $filter = null, int $status = null, string $custIdLogin = null): array
    {
        $select = $this->_db->select()
            ->from(['TBGC' => 'TEMP_BANK_GUARANTEE_CLOSE'], array('*','CLOSEBG_TYPE'=>'TBGC.CHANGE_TYPE'))
            ->joinLeft(['TBG' => 'T_BANK_GUARANTEE'], 'TBGC.BG_NUMBER = TBG.BG_NUMBER', [
                '*',
                'BRANCH_NAME' => new Zend_Db_Expr('(SELECT MB.BRANCH_NAME FROM M_BRANCH MB WHERE MB.BRANCH_CODE = TBG.BG_BRANCH LIMIT 1)'),
                'CURRENCY_BG' => new Zend_Db_Expr('(SELECT TBGD.PS_FIELDVALUE FROM T_BANK_GUARANTEE_DETAIL TBGD WHERE LOWER(TBGD.PS_FIELDNAME) = \'currency\' LIMIT 1)')
            ])
            ->joinLeft(['MB' => 'M_BRANCH'], 'TBG.BG_BRANCH = MB.BRANCH_CODE', []);

        $select->where('TBG.CUST_ID = ?', $custIdLogin);

        // cek status
        if ($status) $select->where('TBGC.SUGGESTION_STATUS = ?', $status);
        
        // jika tidak ada filter
        if (!$filter) return $this->_db->fetchAll($select);

        $specialCondition = ['BG_AMOUNT', 'LASTUPDATEDBY'];
        foreach ($filter as $key => $value) {
            # code...
            if (!in_array($key, $specialCondition)) {
                $select->where('LOWER(TBG.' . $key . ') LIKE ?', '%' . $value . '%');
            } else {
                // special condition if needed 
                if (strtolower($key) == 'bg_amount') $select->where('TBG.BG_AMOUNT >= ?', str_replace([',', ''], '', $value));
                if (strtolower($key) == 'lastupdatedby') $select->where('TBGC.LASTUPDATEDBY LIKE ?', '%' . $value . '%');
            }
        }

        return $this->_db->fetchAll($select);
    }
}
