<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class paymentworkflow_WaitingreleaseController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$payType = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$trfType = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		foreach ($payType as $key => $value) {
			$filterPayType[$key] = $value;
		}
		foreach ($trfType as $key => $value) {
			if ($key != 3 && $key != 4) $filterTrfType[$key] = $value;
		}

		$fields = array(
			'payReff'  		=> array(
				'field' 	=> 'payReff',
				'label' 	=> $this->language->_('Payment Ref') . '#',
				'sortable' => true
			),
			'paySubj'  		=> array(
				'field' 	=> 'paySubj',
				'label' 	=> $this->language->_('Subject'),
				'sortable' => true
			),
			'created'  		=> array(
				'field' 	=> 'created',
				'label' 	=> $this->language->_('Created Date'),
				'sortable' => true
			),
			'efdate'  		=> array(
				'field' 	=> 'efdate',
				'label' 	=> $this->language->_('Payment Date'),
				'sortable' => true
			),
			'updated'  		=> array(
				'field' 	=> 'updated',
				'label' 	=> $this->language->_('Last Updated'),
				'sortable' => true
			),
			'accsrc'  		=> array(
				'field' 	=> 'accsrc',
				'label' 	=> $this->language->_('Source Account'),
				'sortable' => true
			),
			/*'accsrc_name'  	=> array('field' 	=> 'accsrc_name',
												 'label' 	=> $this->language->_('Source Account Name'),
												 'sortable' => true),*/
			'acbenef'  		=> array(
				'field' 	=> 'acbenef',
				'label' 	=> $this->language->_('Beneficiary Account'),
				'sortable' => true
			),
			/*'acbenef_name'  => array('field' 	=> 'acbenef_name',
												 'label'	=> $this->language->_('Beneficiary Name'),
												 'sortable' => true),*/
			'numtrx'  		=> array(
				'field' 	=> 'numtrx',
				'label' 	=> '#' . $this->language->_('Trans'),
				'sortable' => true
			),
			'amount'  			=> array(
				'field' 	=> 'amount',
				'label' 	=> $this->language->_('Amount'),
				'sortable' => true
			),
			/*'amount'  		=> array('field' 	=> 'amount',
												 'label' 	=> $this->language->_('Amount'),
												 'sortable' => true),*/
			'payType'  		=> array(
				'field'	=> 'payType',
				'label' 	=> $this->language->_('Payment Type'),
				'sortable' => true
			),
		);

		$filterlist = array('PS_SUBJECT','SOURCE_ACCOUNT','BENEFICIARY_ACCOUNT', 'PS_NUMBER', 'PS_EFDATE', 'PS_TYPE', 'TRANSFER_TYPE');

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'created');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		$filterArr = array(
			'PS_NUMBER'  		=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'PS_TYPE'  		=> array('StringTrim', 'StripTags'),
			'TRANSFER_TYPE'  		=> array('StringTrim', 'StripTags'),
			'PS_SUBJECT'   => array('StringTrim', 'StripTags'),
			'SOURCE_ACCOUNT'     => array('StringTrim', 'StripTags'),
			'BENEFICIARY_ACCOUNT'     => array('StringTrim', 'StripTags'),
			'PS_EFDATE'   => array('StringTrim', 'StripTags'),
			'PS_EFDATE_END'     => array('StringTrim', 'StripTags'),
		);


		// print_r($this->_getParams());die;
		// if POST value not null, get post, else get param
		$dataParam = array("PS_NUMBER", "PS_TYPE", "TRANSFER_TYPE","PS_SUBJECT","SOURCE_ACCOUNT","BENEFICIARY_ACCOUNT");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		// if (!empty($this->_request->getParam('createdate'))) {
		// 	$createarr = $this->_request->getParam('createdate');
		// 	$dataParamValue['PS_CREATED'] = $createarr[0];
		// 	$dataParamValue['PS_CREATED_END'] = $createarr[1];
		// }
 

		if (!empty($this->_request->getParam('efdate'))) {
			$efdatearr = $this->_request->getParam('efdate');
			$dataParamValue['PS_EFDATE'] = $efdatearr[0];
			$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
		}

		// The default is set so all fields allow an empty string
		$options = array('allowEmpty' => true);
		$validators = array(
			'PS_NUMBER'		=> array(),
			'PS_TYPE' 		=> array(),
			'TRANSFER_TYPE' 		=> array(array('InArray', array('haystack' => array_keys($filterTrfType)))),
			'PS_SUBJECT'   	=> array(),
			'SOURCE_ACCOUNT'     	=> array(),
			'BENEFICIARY_ACCOUNT'     	=> array(),
			'PS_EFDATE'   	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_EFDATE_END'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter 	= $this->_getParam('filter');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');
		$fromMenu	= $this->_getParam('m');

		$fPaymentReff = $zf_filter->getEscaped('PS_NUMBER');
		$fPaymentType = $zf_filter->getEscaped('PS_TYPE');
		$fTrfType 	  = $zf_filter->getEscaped('TRANSFER_TYPE');
		$fSubject = $zf_filter->getEscaped('PS_SUBJECT');
		$fSource   = $zf_filter->getEscaped('SOURCE_ACCOUNT');
		$fBenef   = $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');
		$fPayDateFrom = $zf_filter->getEscaped('PS_EFDATE');
		$fPayDateTo   = $zf_filter->getEscaped('PS_EFDATE_END');
		$fPaymentStart 	= $zf_filter->getEscaped('PS_EFDATE');
		$fPaymentEnd 	= $zf_filter->getEscaped('PS_EFDATE_END');

		$paramPayment = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);

		$select->where('P.PS_STATUS = ?', (string) $this->_paymentstatus["code"]["waitingtorelease"]);

		// 		echo $select;die;
		// 		Zend_Debug::dump($this->_userIdLogin);
		// 		Zend_Debug::dump($this->_custIdLogin);
		// 		Zend_Debug::dump($paramPayment);

		if ($fromMenu == 1) {
			$defaultFromDate = Application_Helper_General::addDate(date('Y-m-d'), 0, -1);	// 1 month
			$fCreatedFrom 	 = ($fCreatedFrom) ? $fCreatedFrom : Application_Helper_General::convertDate($defaultFromDate);
			$fCreatedTo   	 = ($fCreatedTo) ? $fCreatedTo  : Application_Helper_General::convertDate(date('Y-m-d'));
		}

		// Filter Data
		if ($fSubject) {
			$fSubjectArr = explode(',', $fSubject);
			$select->where("UPPER(P.PS_SUBJECT)  in (?)",$fSubjectArr );
		}

		if ($fSource) {
			$fSourceArr = explode(',', $fSource);
			$select->where("T.SOURCE_ACCOUNT  in (?)",$fSourceArr );
		}

		if ($fBenef) {
			$fBenefArr = explode(',', $fBenef);
			$select->where("T.BENEFICIARY_ACCOUNT  in (?)",$fBenefArr );
		}

		if ($fPayDateFrom) {
			$FormatDate 	= new Zend_Date($fPayDateFrom, $this->_dateDisplayFormat);
			$payDateFrom   	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) >= ?', $payDateFrom);
		}

		if ($fPayDateTo) {
			$FormatDate 	= new Zend_Date($fPayDateTo, $this->_dateDisplayFormat);
			$payDateTo  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) <= ?', $payDateTo);
		}

		if ($fPaymentReff) {
			$fPaymentReffArr = explode(',', $fPaymentReff);
			$select->where("UPPER(P.PS_NUMBER)  in (?)",$fPaymentReffArr );
		}

		if ($fPaymentType) {
			$fPayType = explode(',', $fPaymentType);
			$select->where("P.PS_TYPE in (?) ", $fPayType);
		}

		if ($fTrfType != "") {
			$select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and TRANSFER_TYPE = ?) > 0", (string) $fTrfType);
		}
		//		{	$select->where("T.TRANSFER_TYPE = ? ", (string)$fTrfType);		}

		// $select->order($sortBy . ' ' . $sortDir);
		$dirdeb = array(27,28);
		$select->where("P.PS_TYPE NOT IN (?) ", $dirdeb);
		$select->orWhere("P.PS_STATUS = '2' AND P.PS_TYPE IN (38) AND P.CUST_ID = ? ",$this->_custIdLogin);
		$select->group("T.PS_NUMBER");
		// 		echo '<br />';
		// echo $select;die;
		//		echo $select->__toString();die;
		$dataSQL = $this->_db->fetchAll($select);

		if ($csv || $pdf || $this->_request->getParam('print')) {
			$header  = Application_Helper_Array::simpleArray($fields, "label");
		} else {
			$this->paging($dataSQL);
			$dataSQL = $this->view->paginator;
		}


		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $newDataBank = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($newDataBank)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$newDataBank[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($newDataBank,$cacheID);
        }


		$app = Zend_Registry::get('config');
		$app = $app['app']['bankname'];

		// arrange data for viewing in html, csv, pdf
		$data = array();
		//echo '<pre>';
		 		//print_r($dataSQL);die;
		foreach ($dataSQL as $d => $dt) {
			if (!$csv && !$pdf && !$this->_request->getParam('print')) {
				$data[$d]["allowMultiAction"] = ($dt["PS_CATEGORY"] == "SINGLE PAYMENT" || $dt["PS_CATEGORY"] == "OPEN TRANSFER") ? true : false;
			}

			$persenLabel = $dt["BALANCE_TYPE"] == '2' ? ' %' : '';

			foreach ($fields as $key => $field) {
				$value = $dt[$key];
				if ($key == 'acbenef' && ($dt['PS_TYPE'] == '12' || $dt['PS_TYPE'] == '16' || $dt['PS_TYPE'] == '17')) {
					$value = '-';
				} elseif ($key == "acbenef" && $value != "-") {
					if (!empty($dt['BANK_CODE'])) {
						$benefBankName = $newDataBank[$dt['BANK_CODE']];
					} else if (!empty($dt['BENEF_ACCT_BANK_CODE'])) {
						$benefBankName = $newDataBank[$dt['BENEF_ACCT_BANK_CODE']];
					}else if (!empty($dt['BENEFICIARY_BANK_NAME'])) {
						$benefBankName = $dt['BENEFICIARY_BANK_NAME'];
					}else if (!empty($dt['CLR_CODE'])) {
						$benefBankName = $newDataBank[$dt['CLR_CODE']];
					} else {
						$benefBankName = $app;
					}


					if($dt['PS_TYPE'] == '4' || $dt['PS_TYPE'] == '25' || $dt['PS_TYPE'] == '26' || $dt['PS_TYPE'] == '38'){
						$value = '-';
					}else{
						if (empty($dt["acbenef_alias"]) || $dt["acbenef_alias"] == '-') {
							$value = $dt['acbenef'] . ' (' . $dt['acbenef_ccy'] . ')' .  " / ".$benefBankName ." / " . $dt['acbenef_name'] . " (S)";
						} else {
							$value = $dt['acbenef'] . ' (' . $dt['acbenef_ccy'] . ')' .  " / ".$benefBankName ." / " . $dt['acbenef_alias'] . " (A)";
						}
					}
				}
				/*if ($key == "amount" && !$csv)
				{	
					if( $dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15' ){

						$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
					}else{
						$value = Application_Helper_General::displayMoney($value);
					}
				}
				elseif ($key == "amount" && $csv)
				{	
					if( $dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15' ){

						$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
					}else{
						$value = Application_Helper_General::displayMoney($value);
					}
				}*/
				if ($key == 'amount' && ($dt['PS_TYPE'] == '19' || $dt['PS_TYPE'] == '20' || $dt['PS_TYPE'] == '23')) {
					$value = '-';
				} elseif ($key == "created") {
					$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);
				} elseif ($key == "updated") {
					$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);
				} elseif ($key == "efdate") {
					$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);
				} elseif (($key == "accsrc") && $value != "-") {
					/*$value = $value."[".$dt[$key.'_ccy']."]";*/
					if (empty($dt['SOURCE_ACCT_BANK_CODE'])) {
						$sourceBankName = $app;
					} else {
						$sourceBankName = $newDataBank[$dt['SOURCE_ACCT_BANK_CODE']];
					}

					if (empty($dt["accsrc_alias"]) || $dt["accsrc_alias"] == '-') {
						$value = $dt['accsrc'] . ' (' .$dt['accsrc_ccy']  . ')' . " / ".$sourceBankName." / " . $dt['accsrc_name'] . " (S)";
					} else {

						$value = $dt['accsrc'] . ' (' .$dt['accsrc_ccy']  . ')' . " / ".$sourceBankName." / " . $dt['accsrc_alias'] . " (A)";
					}
				} elseif ($key == "amount" && $value != "-") {
					//var_dump($dt['PS_TYPE']);
					if($dt['PS_TYPE'] == '23' || $dt['PS_TYPE'] == '19' || $dt['PS_TYPE'] == '20'  || $dt['PS_TYPE'] == '15' || $dt['PS_TYPE'] == '14'){
						$value = '-';
					}else if($dt['PS_TYPE'] == '4' || $dt['PS_TYPE'] == '11' || $dt['PS_TYPE'] == '25' ){
						$value = $dt['ccy'] . " " . Application_Helper_General::displayMoney($dt['totalamount']);
					} else {
						$value = $dt['ccy'] . " " . Application_Helper_General::displayMoney($dt['TRA_AMOUNT']);
					}
				} else if ($key == 'payType' && $value != '-') {
					// if ($dt['TRANSFER_TYPE'] == 0) {
					// 	if ($dt['SOURCE_ACCT_BANK_CODE'] == $dt['BENEF_ACCT_BANK_CODE']) {
					// 		//$value = $dt['payType'] . ' - ' . 'PB';
					// 		$value = $dt['payType'];
					// 	} else {
					// 		//$value = $dt['payType'] . ' - ' . 'ONLINE';
					// 		$value = $dt['payType'];
					// 	}
					// } else if ($dt['TRANSFER_TYPE'] == 1) {
					// 	// $value = $dt['payType'] . ' - ' . 'SKN';
					// 	$value = $dt['payType'];
					// } else if ($dt['TRANSFER_TYPE'] == 2) {
					// 	// $value = $dt['payType'] . ' - ' . 'RTGS';
					// 	$value = $dt['payType'];
					// }

					$dt['TRANSFER_TYPE1'] = $trfType[$dt['TRANSFER_TYPE']];

                    $tra_type	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");

                    $tra_type1 = $tra_type[$dt['TRANSFER_TYPE']];
                    
                    if ($dt['PS_TYPE'] == '19') {
                         $payType = 'CP Same Bank Remains';
                    }else if ($dt['PS_TYPE'] == '20') {
                         $payType = 'CP Same Bank Maintains';
                    }else if ($dt['PS_TYPE'] == '23') {
                         $payType = 'CP Others Remains - '.$tra_type1;
                    }else if ($dt['PS_TYPE'] == '21') {
                         $payType = 'MM - '.$tra_type1;
                    }else if($dt['PS_TYPE'] == '16' || $dt['PS_TYPE'] == '17'){
						$type = $this->_db->fetchRow(
							$this->_db->select()
								->from(array('T_PSLIP_DETAIL'), array('PS_FIELDVALUE'))
								->where('PS_NUMBER = ?' , $dt['PS_NUMBER'])
								->where('PS_FIELDNAME = ?' , 'Type Of Transaction ')
						);
						if(!empty($type)){
							$typeArr = explode(' ',$type['PS_FIELDVALUE']);
							$payType = $typeArr['1'].' - '.$typeArr['0'];
						}else{
						//var_dump($type);die;
						$payType = $dt['payType']; 
						}
					}else if($dt['PS_TYPE'] == '25' || $dt['PS_TYPE'] == '4' || $dt['PS_TYPE'] == '26' || $dt['PS_TYPE'] == '28' || $dt['PS_TYPE'] == '3' || $dt['PS_TYPE'] == '27'){
						$payType = $dt['payType'];
					}elseif($dt['PS_TYPE'] == '38'){
						$payType = 'Inhouse - BG';
					}else {
                        $payType = $dt['payType'] . ' - ' . $dt['TRANSFER_TYPE1'];
                    }

                    $value = $payType;

				}

				$value = ($value == "" && !$csv) ? "&nbsp;" : $value;
				// 				if($dt['TRANSFER_TYPE']=='8'){
				// // 					print_r('here');die;payType
				// 					$data[$d][$key] = $this->language->_($value);
				// 					if($key=='payType')
				// 					$data[$d]['payType'] = $this->language->_('Mayapada(Buy)');
				// 				}else if($dt['TRANSFER_TYPE']=='7'){
				// // 					$data[$d]['payType'] = $this->language->_('Mayapada(Sell)');
				// 					$data[$d][$key] = $this->language->_($value);
				// 					if($key=='payType')
				// 						$data[$d]['payType'] = $this->language->_('Mayapada(Buy)');
				// 				}else{
				$data[$d][$key] = $this->language->_($value);
				// 				}
			}
			// 			die;
		}
		// 		print_r($data);die;
		$filterlistdata = array("Filter");
			foreach($dataParamValue as $fil => $val){
					$paramTrx = $fil . " - " . $val;
					array_push($filterlistdata, $paramTrx);
		}

		if ($csv) {
			$filterlistdatax = $this->_request->getParam('data_filter');
			$listable = array_merge_recursive(array($header), $data);
			$this->_helper->download->csv($filterlistdatax, $listable, null, 'List Waiting Release');
			Application_Helper_General::writeLog('PRLP', 'Export CSV Waiting Release');
		} elseif ($pdf) {
			$this->_helper->download->pdf($header, $data, null, 'List Waiting Release');
			Application_Helper_General::writeLog('PRLP', 'Export PDF Waiting Release');
		} elseif ($this->_request->getParam('print')) {
			$filterlistdatax = $this->_request->getParam('data_filter');
			$this->_forward('printtable', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'List Waiting Release', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
		} else {
			$stringParam = array(
				'payReff'		=> $fPaymentReff,
				'payType'		=> $fPaymentType,
				'trfType'		=> $fTrfType,
				'PS_SUBJECT'	=> $fSubject,
				'SOURCE_ACCOUNT'	=> $fSource,
				'BENEFICIARY_ACCOUNT'	=> $fBenef,
				'payDateFrom'	=> $fPayDateFrom,
				'payDateTo'	=> $fPayDateTo,
			);

			$this->view->filterPayType 	= $filterPayType;
			$this->view->filterTrfType 	= $filterTrfType;
			$this->view->payReff 		= $fPaymentReff;
			$this->view->payType 		= $fPaymentType;
			$this->view->trfType 		= $fTrfType;
			$this->view->createdFrom 	= $fCreatedFrom;
			$this->view->createdTo 		= $fCreatedTo;
			$this->view->payDateFrom 	= $fPayDateFrom;
			$this->view->payDateTo 		= $fPayDateTo;
			$this->view->query_string_params = $stringParam;
			$this->updateQstring();
			if (!empty($this->_request->getParam('wherecol'))) {
				$this->view->wherecol			= $this->_request->getParam('wherecol');
			}

			if (!empty($this->_request->getParam('whereopt'))) {
				$this->view->whereopt			= $this->_request->getParam('whereopt');
			}

			if (!empty($this->_request->getParam('whereval'))) {
				$this->view->whereval			= $this->_request->getParam('whereval');
			}


			$this->view->data 				= $data;
			$this->view->fields 			= $fields;
			$this->view->filter 			= $filter;
			$this->view->paymentStart 	= $fPaymentStart;
			$this->view->paymentEnd 	= $fPaymentEnd;
			$this->view->sortBy 			= $sortBy;
			$this->view->sortDir 			= $sortDir;
			$this->view->allowMultiRelease 	= ($this->view->hasPrivilege('PRLP') && count($data) > 0);
			$this->view->allowMultiReject  	= ($this->view->hasPrivilege('PRJT') && count($data) > 0);

			// set URL
			$URL = $this->view->url(array(
				'module'	 => $this->view->modulename,
				'controller' => $this->view->controllername,
				'action'	 => 'index',
				'page'		 => $page,
				'sortBy' 	 => $this->view->sortBy,
				'sortDir' 	 => $this->view->sortDir
			), null, true) . $this->view->qstring;

			$sessionNamespace = new Zend_Session_Namespace('URL_CP_WR');
			$sessionNamespace->URL = $URL;
			if (!empty($dataParamValue)) {
				// $this->view->createdStart = $dataParamValue['PS_CREATED'];
				// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
				$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
				$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

				unset($dataParamValue['PS_CREATED_END']);
				unset($dataParamValue['PS_EFDATE_END']);

				foreach ($dataParamValue as $key => $value) {
					$duparr = explode(',',$value);
								if(!empty($duparr)){
									
									foreach($duparr as $ss => $vs){
										$wherecol[]	= $key;
										$whereval[] = $vs;
									}
								}else{
										$wherecol[]	= $key;
										$whereval[] = $value;
								} 
				}
				$this->view->wherecol     = $wherecol;
				$this->view->whereval     = $whereval;
				// print_r($whereval);die;
			}
			$this->view->filterlistdata = $filterlistdata;

			Application_Helper_General::writeLog('PRLP', 'Viewing Waiting Release');
		}
	}
}
