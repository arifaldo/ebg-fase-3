<?php

/**
 * Globalchanges model for BACKEND
 * 
 * @author 
 * @version 
 */

require_once 'Zend/Db/Table/Abstract.php';

class Echanrequest_Model_Globalchanges extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'T_GLOBAL_CHANGES';
	protected $_primary = 'CHANGES_ID';
							
	protected $dbObj;
	protected $_id;
	protected $_row;
	protected $_errorMsg;
	protected $_errorCode;
	protected $_moduleId;
	protected $_suggestData;
	
	//model mapping,
	//array key = obtained from field MODULE, TABLE T_GLOBAL_CHANGES								
	//array value = model name in this module
	//mapping for backend
	protected $_detailModelMap = array(
										'registerechannel' => 'Echanrequest_Model_Tempregisterechannel'
									  );

	protected $_detailModelIdMap = array(
										'registerechannel' => 'REC'
									  );							  
	/**
     * Constructor
     * @param  String $id Change Id
     */
	public function __construct($id) {
	
	
		parent::__construct();
		$this->dbObj = $this->getDefaultAdapter();
		$this->setId($id);
		
			
		$this->dbObj->getProfiler()->setEnabled(true);	
	}
	
	/**
     * Destructor
     */
	public function __destruct() {
		$this->dbObj->getProfiler()->clear();	
	}
	
	/**
     * set Id for this object
     * @param  String $id Change Id
     * @return boolean 
     */
	public function setId($id) {
		
	    $privi = array();
	    
	    if(Zend_Auth::getInstance()->hasIdentity())
    	{
    	  $auth  = Zend_Auth::getInstance()->getIdentity();
    	  $privi = $auth->priviIdLogin;
    	}
		
		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{	
			$row = $this->dbObj
				->select()
				->from($this->_name)
				->where($this->dbObj->quoteInto($this->_primary.' = ? ',$id))
				->where($this->dbObj->quoteInto("CHANGES_FLAG = ?",'B'))
				->query()
			   	->fetch(Zend_Db::FETCH_ASSOC);
		}

		
		
    	if($row) {
    		$this->_id = $id;
			$this->_row = $row;
			$this->_moduleId = $this->_detailModelIdMap[$row['MODULE']];
			$this->_suggestData = $row['DISPLAY_TABLENAME'];
			return true;
    	}
    	else {
    		$this->_errorCode = '82';
    		$this->_errorMsg = 'Record with given id not found';
    		throw new Exception($this->_errorMsg);
    	}
    		
	}
	
	/**
     * Reject changes
     * Update status in changes table to "request repair"
     *
     * @param  mixed $actor User name/id who request changes repair,for use in logging into database
     * @param  String $note OPTIONAL Approval note to set in database
     * @return boolean indicating operation success/failure
     */
	public function reject($actor,$note = null) {
		$changeModulePrivilegeObj  = new Echanrequest_Model_Privilege();
		$isAuthorizeReject = $changeModulePrivilegeObj->isAuthorizeReject($this->_row['MODULE']);
		if(!$isAuthorizeReject){
			$this->_errorCode = '82';
			$this->_errorMsg = 'You have no privilege for reject specified changes';
			return false;
		}
		$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($this->_row['MODULE'], 'Reject');
		$fullDesc = "Reject Changes Id : {$this->_row['CHANGES_ID']} Suggest Data : {$this->_row['DISPLAY_TABLENAME']} Data Id : {$this->_row['KEY_FIELD']}  Data Name : {$this->_row['KEY_VALUE']} ";
		Application_Helper_General::writeLog($privilgeDualControl,$fullDesc);
		
		// $this->dbObj->beginTransaction();

		try {
			//check whether $this->_row['MODULE'] value exists in $this->_detailModelMap
			if(in_array($this->_row['MODULE'],array_keys($this->_detailModelMap))) {
				//create detail model
				$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id,$this->_row);

				if($this->_row['CHANGES_TYPE'] == 'N'){
					$updated = $detailModel->deleteNew(); //request utk retain temp data
					//$updated = true;
					$this->_errorCode = $detailModel->getErrorCode();
					$this->_errorMsg = $detailModel->getErrorMessage();
				}
				else{
					$updated = false;
					$this->_errorCode = '22';
					$this->_errorMsg = 'Unknown changes type';
				}
				
				if($updated) {		
					$rowUpdated = $this->dbObj->update($this->_name, array('CHANGES_STATUS' => 'RJ',
																		   'CHANGES_REASON' => $note,
																		   'LASTUPDATED' => new Zend_Db_Expr("GETDATE()"),
																		   ),
													   $this->dbObj->quoteInto('CHANGES_ID = ?',$this->_id)
													  );
					if(!(boolean)$rowUpdated) {								  
						$this->_errorCode = '82';
						$this->_errorMsg = $this->getErrorRemark('82');
						throw new Exception($this->_errorMsg);
					}							  
				}
				else
					throw new Exception($this->_errorMsg);
			}
			else {
				$this->_errorCode = '22';
				$this->_errorMsg = 'Unknown Module';
				throw new Exception($this->_errorMsg);
			}
		}
		catch(Exception $e) {
			$this->dbObj->rollBack();
			$this->_errorMsg = $e;
			throw $e;
			return false;
		}
		
		// $this->dbObj->commit();
		return true;
	}
	
	/**
     * Approve changes
     *
     * @param  mixed $actor User name/id who approved changes,for use in logging into database
     * @param  String $note OPTIONAL Approval note to set in database
     * @return boolean indicating operation success/failure
     */
	public function approve($actor = null,$additional = null) {
		$changeModulePrivilegeObj  = new Echanrequest_Model_Privilege();
		$isAuthorizeAppove = $changeModulePrivilegeObj->isAuthorizeApprove($this->_row['MODULE']);
		if(!$isAuthorizeAppove){
			$this->_errorCode = '82';
			$this->_errorMsg = 'You have no privilege for approve specified changes';
			return false;
		}
		
		$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($this->_row['MODULE'], 'Approve');
		$fullDesc = "Approve Changes Id : {$this->_row['CHANGES_ID']} Suggest Data : {$this->_row['DISPLAY_TABLENAME']} Data Id : {$this->_row['KEY_FIELD']}  Data Name : {$this->_row['KEY_VALUE']} ";
		Application_Helper_General::writeLog($privilgeDualControl,$fullDesc);

		// $this->dbObj->beginTransaction();

		try 
		{  
			//check whether $this->_row['MODULE'] value exists in $this->_detailModelMap
			if(in_array($this->_row['MODULE'],array_keys($this->_detailModelMap))) 
			{
				//create detail model
				$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id,$this->_row);
				
				if($this->_row['CHANGES_TYPE'] == 'N'){
					$updated = $detailModel->approveNew($actor,$additional);
					$this->_errorCode = $detailModel->getErrorCode();
					$this->_errorMsg  = $detailModel->getErrorMessage();
				}
				else{
					$updated = false;
					$this->_errorCode = '82';
					$this->_errorMsg = 'Unknown changes type';
				}
				
				if($updated) 
				{  
					
					$rowUpdated = $this->dbObj->update($this->_name, array('CHANGES_STATUS' => 'AP',
																		   'CHANGES_REASON' => $note,
																		   'LASTUPDATED' => new Zend_Db_Expr("GETDATE()"),
																		   ),
													   $this->dbObj->quoteInto('CHANGES_ID = ?',$this->_id)
												  );
													  
													  
					if(!(boolean)$rowUpdated) 
					{								  
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query Failed (Global Changes)';
					}					  
				}
				else
				{
				    return false;
				}
			}
			else {
				$this->_errorCode = '82';
				$this->_errorMsg = 'Unknown Module';
			}
		}
		catch(Exception $e) 
		{
			
			$this->_errorMsg = $e;
			$this->dbObj->rollBack();
			return false;
		}
		
		// $this->dbObj->commit();
		return true;
	}
	
	/**
     * Get Error Message
     *
     * @return String Error Message
     */
	public function getErrorMessage() {
		return $this->_errorMsg;
	}
	
	public function getErrorCode() {
		return $this->_errorCode;
	}
	
	public function getModuleId() {
		return $this->_moduleId;
	}
	public function getSuggestData() {
		return $this->_suggestData;
	}
	public function getChangesInfo(){
		return $this->_row;
	}
}