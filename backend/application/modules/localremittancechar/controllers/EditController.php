<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';

class Localremittancechar_EditController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{	
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		$charge_type 	= $this->_getParam('id');
		$cust_id = $this->_getParam('custid');
		$cust_name = $this->_custNameLogin;
		
			$this->view->title = 'Charges';
			
		
		$model = new remittancechar_Model_Remittancechar();
		
		$this->view->ccyArr = ( array(''=>'-')+Application_Helper_Array::listArray($model->getCCYList(),'CCY_ID','CCY_ID'));
// 		$this->view->paginator = $model->getCCYList();
		$buser_id = $this->_userIdLogin;
		$this->view->id 	= $charge_type;
		$this->view->custid = $cust_id;
		$ccy =
		array('0' => array('CCY_ID'=> 'IDR'),'1'=> array('CCY_ID'=> 'USD'));
		$this->view->curencylist = $ccy;
		
// 		echo ' <pre>';print_r($ccy);die;
		$this->view->paginator = $model->getRemittanceTypeLocal($charge_type,$cust_id);
		
		$settings =  new Settings();

		
		

		if($this->_request->isPost())
		{
			$errDesc = array();
			$ccylist = $model->getCCYList();
			$params = $this->getRequest()->getParams();

			//$params_ccy = $this->getRequest()->getParams('ccy');
// 			print_r($params_ccy);
			if($charge_type=='6'){
				foreach ($params['ccy'] as $value){
// 					print_r($value);
					$amount_data = 'amount'.$value;  
					$amount_data = $params[$amount_data];
					
					$ccy_amount = 'ccy'.$value;
					$ccy_amount_data = $params[$ccy_amount];
					
					if($params[$ccy_amount]=='-'){
						$checkdata = true;
					}else{
						$checkdata = $model->getCheckRemittanceType($value,$value,$charge_type,$cust_id);
					}
					if(empty($checkdata) || $checkdata==true){
						
						
						
					}else{
						$msg = 'Error: Same currency are not allowed in full amount fee';
						break;	
					}
					
				}
			}
			
// 			print_r($msg);die;
			$err = false;

			if(empty($msg))
			{
					if($charge_type=='6'){
						$info = 'Local Remittance Charges';
						$info2 = 'Set Edit New Charges';
						
						$change = 'true';
// 						print_r($params);die;
						foreach ($params['ccy'] as $value){
							$amount_data = 'amount'.$value;
							$ccy_amount = 'ccy'.$value;
							$charge_amt =	Application_Helper_General::convertDisplayMoney($params[$amount_data]);
// 							print_r($charge_amt);die;
// 							if( $charge_amt > 0){
// 								$change = 'err';

// 							}
						
						}
// 												print_r($change);die;
						// 						print_r('here');
						if($change != 'err'){
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Remittance Charges Fee',$cust_id,$cust_name);
						foreach ($params['ccy'] as $value){
							$amount_data = 'amount'.$value;  
// 							$amount_data = $this->getRequest()->getParams($amount_data);
							
							$ccy_amount = 'ccy'.$value;
// 							$ccy_amount_data = $this->getRequest()->getParams($ccy_amount);
							
							$charge_amt =	Application_Helper_General::convertDisplayMoney($params[$amount_data]);
// 							$this->_setParam('min_amount_skn',$min_amount_skn_val);

							
								
							
							
							
								$params_insert = array(
										'CHANGES_ID' => $change_id,
										'CUST_ID' => $cust_id,
										'CHARGE_TYPE' => $charge_type,
										'CHARGE_CCY' => $value,
										'CHARGE_AMOUNT_CCY' => $value,
										'CHARGE_AMT' => $charge_amt
										
								);
// 								print_r($params_insert);die;
								if($params[$ccy_amount]=='-'){
								
								}else{
										$result = $model->insertTemp($params_insert);
								}
								
						}
						}
					}
					
					$this->setbackUrl('/remittancechanges');
					if(!$err && $change != 'err'){
						// 						echo 'here';
						
						$this->_redirect('/notification/submited/index');
					}else{
					
						$msg = 'Error: Currency cannot be left blank.';
						$this->view->error 	= true;
						$this->view->msg_failed = $msg;
						$this->view->errDesc = $errDesc;
						// 						echo 'here2';
					}
					
					
					
					
			}
			else
			{
				$this->view->error 	= true;
				$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';
				$this->view->errDesc = $errDesc;
			}
		}
		Application_Helper_General::writeLog('CHOP','Set Edit Remittance Charges');
	}

}

