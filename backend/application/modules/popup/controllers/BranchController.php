<?php

require_once 'Zend/Controller/Action.php';

//NOTE:
//Watch the modulename, filename and classname carefully
class popup_BranchController extends Application_Main 
{
	public function initController()
	{       
	
		$this->_helper->layout()->setLayout('popup');
	}


	public function indexAction() 
	{
	
	    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
	
	
	    $fields = array(
						'branch_code'      => array('field' => 'BRANCH_CODE',
											      'label' => 'Branch Code',
											      'sortable' => true),
						'branch_name'           => array('field' => 'BRANCH_NAME',
											      'label' => 'Branch Name',
											      'sortable' => true),
						'bank_address'  => array('field' => 'BANK_ADDRESS',
											      'label' => 'Address',
											      'sortable' => false),
						'contact'  => array('field' => 'CONTACT',
											      'label' => 'Contact',
											      'sortable' => false),
						/*'swift_code'     => array('field' => 'SWIFT_CODE',
											      'label' => 'Swift Code',
											      'sortable' => true)*/
	                  );
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$branch_code    = $this->_getParam('branch_code');
		$branch_name_coverage    = $this->_getParam('branch_name_coverage');

		$selectold = $this->_db->select()
		->from(array('M_BRANCH_COVERAGE'))
		->where('BRANCH_CODE = ?',$branch_code)
		->query()->fetchAll();

		// var_dump($selectold);die;

		$this->view->selectold = $selectold;

		$select = $this->_db->select()
					        ->from(array('A' => 'M_BRANCH'));
		
		$whereHead  = $this->_db->fetchAll($this->_db->select()
		->from(array('A' => 'M_BRANCH'),array('A.BRANCH_CODE'))
		->where("A.HEADQUARTER = ?","YES"));
		
		$whereBrach  = $this->_db->fetchAll($this->_db->select()
		->from(array('A' => 'M_BRANCH_COVERAGE'),array('A.BRANCH_CODE')));
		//->where("A.HEADQUARTER = ?","YES"));
		
		$whereBrachCoverage  = $this->_db->fetchAll($this->_db->select()
		->from(array('A' => 'M_BRANCH_COVERAGE'),array('A.BRANCH_COVERAGE_CODE')));
	
						
		
		if(!empty($whereHead))
	    $select->where("A.BRANCH_CODE not in (?)",$whereHead);
		
		if(!empty($whereBrach))
		$select->where("A.BRANCH_CODE not in (?)",$whereBrach);
	
		if(!empty($whereBrachCoverage))
		$select->where("A.BRANCH_CODE not in (?)",$whereBrachCoverage);
		
		//echo $select;die;
		
	    if(!empty($branch_code)){
			$select->where("A.BRANCH_CODE != ?",$branch_code);	
		}
		
		
		
		$resultdata  = $this->_db->fetchAll($this->_db->select()
		->from(array('B' => 'M_BRANCH_COVERAGE'),array('B.BRANCH_COVERAGE_CODE'))
		->where("B.BRANCH_CODE != ?",$branch_code));

		if(!empty($resultdata)){
			$select->where("A.BRANCH_CODE NOT IN (?)",$resultdata);	
		}

		
		
		
		//die();

		$resultdatatemp  = $this->_db->fetchAll($this->_db->select()
		->from(array('B' => 'TEMP_BRANCH_COVERAGE'),array('B.BRANCH_COVERAGE_NAME'))
		->where("B.BRANCH_CODE = ?",$branch_code));
		$arr = array();
		foreach($resultdatatemp as $key=>$value){
			$arr[$value["BRANCH_COVERAGE_NAME"]]= $value["BRANCH_COVERAGE_NAME"];
		}
	
		$this->view->branch_name_coverage = $arr;
		
		//$this->view->success = true;
		
	    $select->order($sortBy.' '.$sortDir);   
		//echo $select;die;
		$this->paging($select);
		$this->frontendLog('V',$this->_moduleDB,null);
		$this->view->fields = $fields;
		//$this->view->filter = $filter;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}

}
