<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class binsetup_DeactivateController extends binsetup_Model_Binsetup 
{

   public function indexAction() 
   {   
       $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;


       $AESMYSQL = new Crypt_AESMYSQL();
       $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
       $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
       $cust_bin = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_bin'), $password));

       $error_remark = null;
    
       // jika customer id valid
       if($cust_id)
       {   
           $cust_data = $this->getCustomerActIn($cust_id,$cust_bin,1);
           
           if(!empty($cust_data))
           {   
      	        $tempCustomerId = $this->getTempCustomerId($cust_id,$cust_bin,TRUE);

		        if(!$tempCustomerId)
		        {
//                   $info = 'Customer ID = '.$cust_data['CUST_ID'].', Customer Name = '.$cust_data['CUST_NAME'];
                   $info = 'BIN';
                   //$cust_data['CUST_STATUS'] = $this->_masteruserStatus['code']['inactive'];

                   try 
                   { 
			          $this->_db->beginTransaction();
			          
			          $cust_data['CUST_BIN_STATUS'] = 2;
			          $cust_data['BIN_SUGGESTED']    = new Zend_Db_Expr('now()');
		              $cust_data['BIN_SUGGESTEDBY']  = $this->_userIdLogin;
			          
			          // insert ke T_GLOBAL_CHANGES
			          $change_id = $this->suggestionWaitingApproval('BIN',$info,$this->_changeType['code']['suspend'],null,'M_CUSTOMER_BIN','TEMP_CUSTOMER_BIN','Bin',$cust_bin,$cust_id);

			          $this->insertTempCustomer($change_id,$cust_data);
			          
			          //log CRUD
			          Application_Helper_General::writeLog('BNSP','customer BIN has been suspended, Cust ID : '.$cust_id. ' ,BIN : '.$cust_bin.' ,Change id : '.$change_id);
			          
			          $this->_db->commit();
			          
			          $ns = new Zend_Session_Namespace('FVC');
					  $ns->backURL = '/binsetup';
			          
			          $this->_redirect('/notification/submited/index');
			          //$this->_redirect('/notification/success/index');
		           }
		           catch(Exception $e) 
		           { 
			         $this->_db->rollBack();
			         $error_remark = $this->getErrorRemark('82');
			         Application_Log_GeneralLog::technicalLog($e);
			       }
                }
                else $error_remark = 'Customer BIN is already suggested'; 
            
          }
          else  $cust_id = null; 
       }// END IF CUST_ID
    
       
       if(!$cust_id)  $error_remark = 'cust id is not found';
      
    
       //insert log
       try
       {
	       $this->_db->beginTransaction();
	       
	       Application_Helper_General::writeLog('BNSP','view suspend customer BIN');
	       
           $this->_db->commit();
	   }
	   catch(Exception $e) 
	   {
	       $this->_db->rollBack();
  	       Application_Log_GeneralLog::technicalLog($e);
	   }
	
       $this->_helper->getHelper('FlashMessenger')->addMessage($class);
       $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
//       $this->_redirect($this->_backURL);

       
       
  }
  
}



