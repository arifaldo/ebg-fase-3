<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php'; 

$db = Zend_Db_Table::getDefaultAdapter();
$config    = Zend_Registry::get('config');

	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	
	
$template = file_get_contents(LIBRARY_PATH.'/email template/Source Account Email Notification.html');
$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');

$transFT   = array( '[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
					'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
					'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl													
				  );			
$FOOTER_ID = strtr($FOOTER_ID, $transFT);
$FOOTER_EN = strtr($FOOTER_EN, $transFT);

	$select = $db->select()->distinct()
					->from(array('A' => 'T_TRANSACTION'),array())
					->joinleft(array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER',array())
					->joinLeft(array('E' => 'T_EMAIL_NOTIFICATION_TRX'), 'A.TRANSACTION_ID = E.TRANSACTION_ID', array(	'A.SOURCE_ACCOUNT',
																														'A.SOURCE_ACCOUNT_CCY',
																														'A.SOURCE_ACCOUNT_NAME',
																														'A.SOURCE_ACCOUNT_ALIAS_NAME'))
					->where('B.PS_STATUS = 5')
					->where('A.TRA_STATUS = 3')
					->where("E.PS_ISSOURCEEMAILED NOT LIKE '1' OR E.PS_ISSOURCEEMAILED IS NULL")
					->query() ->fetchAll()
;
// die($select);
					
if($select) 
{
	foreach($select as $result)
	{
		$send = false;
		//select data from database where is uploaded today and not email yet and not deleted yet
		/* $caseBeneBank = "CASE A.TRANSFER_TYPE WHEN '0' THEN '".$config['app']['bankname']."' 
										ELSE CASE B.PS_CATEGORY WHEN 'BULK PAYMENT' 
													THEN '-' ELSE A.BENEFICIARY_BANK_NAME END 
										END"; */
		$caseBeneBank = "CASE A.TRANSFER_TYPE WHEN '0' THEN '".$config['app']['bankname']."' 
										ELSE A.BENEFICIARY_BANK_NAME END";
		$caseTransferType = "CASE A.TRANSFER_TYPE
																						WHEN '0' THEN 'In House' 
																						WHEN '1' THEN 'RTGS'
																						WHEN '2' THEN 'SKN' END";
		$select2 = $db->select()//->distinct()
						->from(array('A' => 'T_TRANSACTION'),array('TRANSACTION_ID'))
						->joinleft(array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER',array('PS_NUMBER'))
						->joinLeft(array('E' => 'T_EMAIL_NOTIFICATION_TRX'), 'A.TRANSACTION_ID = E.TRANSACTION_ID', array(	'A.*',
																															'B.*',
																															'benebank' => new Zend_Db_Expr($caseBeneBank),
																															'transferType' => new Zend_Db_Expr($caseTransferType),
																															'E.PS_ISBENEEMAILED', 
																															'E.PS_ISSOURCEEMAILED',) )
						->where('B.PS_STATUS = 5')
						->where('A.TRA_STATUS = 3')
						->where('A.SOURCE_ACCOUNT LIKE '.$db->quote($result['SOURCE_ACCOUNT']))
						->where("E.PS_ISSOURCEEMAILED NOT LIKE '1' OR E.PS_ISSOURCEEMAILED IS NULL")
						->where("DATE(B.PS_EFDATE) <= DATE(ADDDATE(now(), 1))")
						->query() ->fetchAll()
						;
// 			Zend_Debug::dump($select2);
// 			die($select2);			
		$sourcename = $result['SOURCE_ACCOUNT_NAME'];
		$sourcealname = $result['SOURCE_ACCOUNT_ALIAS_NAME'];
		$sourceccy = $result['SOURCE_ACCOUNT_CCY'];
		$sourceacc = $result['SOURCE_ACCOUNT'];

		try
		{
			$db->beginTransaction();			
			$content = '';
			$counter = 0;
			foreach($select2 as $result2)
			{
				$beneficiary = $result2['BENEFICIARY_ACCOUNT'].' ('.$result2['BENEFICIARY_ACCOUNT_CCY'].') '.$result2['BENEFICIARY_ACCOUNT_NAME'].' / '.$result2['BENEFICIARY_ALIAS_NAME'];
				$amount = $result2['BENEFICIARY_ACCOUNT_CCY'].' '.Application_Helper_General::displayMoney($result2['TRA_AMOUNT']);
				$content .= '
				<tr>
					<td><font>'.++$counter.'</font></td>
					<td><font>'.$result2['PS_NUMBER'].'</font></td>
					<td><font>'.$result2['TRANSACTION_ID'].'</font></td>
					<td><font>'.$result2['PS_SUBJECT'].'</font></td>
					<td><font>'.$beneficiary.'</font></td>
					<td><font>'.$amount.'</font></td>
					<td><font>'.$result2['TRA_MESSAGE'].'</font></td>
					<td><font>'.Application_Helper_General::convertDate($result2['PS_EFDATE'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
					<td><font>'.$result2['benebank'].'</font></td>
					<td><font>'.$result2['transferType'].'</font></td>
					';
				
				if($result2['PS_ISBENEEMAILED'] == null && $result2['PS_ISSOURCEEMAILED'] == null)
				{
					$send = true;
					$insertArr = 
						array(
							'TRANSACTION_ID' 		=> $result2['TRANSACTION_ID'], 
							'PS_NUMBER'				=> $result2['PS_NUMBER'], 
							'PS_ISSOURCEEMAILED' 		=> '1', 
							'PS_SOURCEEMAILDATETIME' 	=> new Zend_Db_Expr("now()")
							);			
					$db->insert('T_EMAIL_NOTIFICATION_TRX', $insertArr);
				}
				
				elseif($result2['PS_ISBENEEMAILED'] == 1 && $result2['PS_ISSOURCEEMAILED'] == null)
				{
					$send = true;
					$updateArr = 
						array( 
							'PS_ISSOURCEEMAILED' 		=> '1',
							'PS_SOURCEEMAILDATETIME' 	=> new Zend_Db_Expr("now()")
							);
					$whereArr = array('TRANSACTION_ID =?'	=> $result2['TRANSACTION_ID']);			
					$db->update('T_EMAIL_NOTIFICATION_TRX', $updateArr, $whereArr);
				}
			}
// 			Zend_Debug::dump($send);
			if($send == true)
			{
				$template = str_ireplace('[[TITLE]]','Outgoing Transfer Notification',$template);
				$template = str_ireplace('[[count]]',$counter,$template);
				$template = str_ireplace('[[SOURCE_ACCOUNT_NAME]]',$sourcename,$template);
				$template = str_ireplace('[[SOURCE_ACCOUNT_ALIAS_NAME]]',$sourcealname,$template);
				$template = str_ireplace('[[SOURCE_ACCOUNT]]',$sourceacc,$template);
				$template = str_ireplace('[[SOURCE_ACCOUNT_CCY]]',$sourceccy,$template);
				$template = str_ireplace('[[SOURCE_DATA]]',$content,$template);
				$template = str_ireplace('[[FOOTER_ID]]',$FOOTER_ID,$template);
				$template = str_ireplace('[[FOOTER_EN]]',$FOOTER_EN,$template);
				$template = str_ireplace('[[APP_NAME]]',$templateEmailMasterBankAppName,$template);
				$template = str_ireplace('[[BANK_NAME]]',$templateEmailMasterBankName,$template);
				$template = str_ireplace('[[APP_URL]]',$templateEmailMasterBankAppUrl,$template);
				
				$template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);
				$template = str_ireplace('[[master_bank_fax]]',$templateEmailMasterBankFax,$template);
				$template = str_ireplace('[[master_bank_address]]',$templateEmailMasterBankAddress,$template);
				$template = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$template);

				$select3 = $db->select()//->distinct()
					->from(array('M_CUSTOMER_ACCT'),array('ACCT_EMAIL'))
					->where('ACCT_NO LIKE '.$db->quote($sourceacc))
					->query() ->fetchAll()
				;
				
// 				Zend_Debug::dump($select3);
// 				die;
// 				die($select3);
					
				$email = $select3[0]['ACCT_EMAIL'];
				
				$subject = 'Outgoing Transfer Notification';
				//echo $template;die;
				$resultemail = Application_Helper_Email::sendEmail($email,$subject,$template);
				//echo $template;
// 				Zend_Debug::dump($sourcename);
// 				Zend_Debug::dump($email);
// 				Zend_Debug::dump($resultemail);
	
				$db->commit();
				$string  = "Sending Email For Source";
				Application_Helper_General::cronLog(basename(__FILE__),$string,1);
			}		
		}
		catch(Exception $e) 
		{
			$string  = "Sending Email For Source Failed";
			Application_Helper_General::cronLog(basename(__FILE__),$string,0);
			$db->rollBack();
		}
	}
}
//Application_Helper_General::cronLog($filename, $cronResult, $isSuccess);
?>
