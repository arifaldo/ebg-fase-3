<?php
require_once 'Zend/Controller/Action.php';

class user_UseractivityreportsController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()->distinct()
			->from(array('A' => 'M_USER'), array('USER_ID', 'USER_FULLNAME', 'CUST_ID'))
			->where("CUST_ID LIKE " . $this->_db->quote($this->_custIdLogin))
			->order('USER_FULLNAME ASC')
			->query()->fetchAll();
		$this->view->var = $select;



		$frontendOptions = array(
			'lifetime' => 259200,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/acctbalance/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		$cacheID = 'FACTIVITY';

		$activity = $cache->load($cacheID);
		//var_dump($select_int);
		if (empty($activity)) {

			$activity = $this->_db->select()->distinct()
				->from(array('A' => 'M_FPRIVILEGE'), array('FPRIVI_ID', 'FPRIVI_DESC'))
				->order('FPRIVI_DESC ASC')
				->query()->fetchAll();
			$cache->save($activity, $cacheID);
		}

		$login = array('FPRIVI_ID' => 'FLGN', 'FPRIVI_DESC' => 'Login');
		$logout = array('FPRIVI_ID' => 'FLGT', 'FPRIVI_DESC' => 'Logout');
		$changepass = array('FPRIVI_ID' => 'CHMP', 'FPRIVI_DESC' => 'Change My Password');
		$resetpass = array('FPRIVI_ID' => 'RFPW', 'FPRIVI_DESC' => 'Reset Forgot Password');
		array_unshift($activity, $changepass);
		array_unshift($activity, $logout);
		array_unshift($activity, $login);
		array_unshift($activity, $resetpass);

		$widget = array('FPRIVI_ID' => 'MWDG', 'FPRIVI_DESC' => 'Widget');
		array_unshift($activity, $widget);
		$account = array('FPRIVI_ID' => 'MACC', 'FPRIVI_DESC' => 'Akun Saya');
		array_unshift($activity, $account);

		$activityarr = Application_Helper_Array::listArray($activity, 'FPRIVI_ID', 'FPRIVI_DESC');
		asort($activityarr);
		//Zend_Debug::dump($activityarr);die;
		$this->view->activity = $activityarr;

		$userlist = Application_Helper_Array::listArray($select, 'USER_ID', 'USER_FULLNAME');
		$this->view->userlist = $userlist;

		$fields = array(
			'LOG_DATE'      	=> array(
				'field' => 'LOG_DATE',
				'label' => $this->language->_('Date/Time'),
				'sortable' => true
			),
			'USER_ID'           => array(
				'field' => 'USER_ID',
				'label' => $this->language->_('User ID'),
				'sortable' => true
			),
			'USER_FULLNAME'           => array(
				'field' => 'USER_FULLNAME',
				'label' => $this->language->_('User Name'),
				'sortable' => true
			),
			'FPRIVI_DESC'  	=> array(
				'field' => 'FPRIVI_DESC',
				'label' => $this->language->_('Activity'),
				'sortable' => true
			),
			'ACTION_FULLDESC'     => array(
				'field' => 'ACTION_FULLDESC',
				'label' => $this->language->_('Description'),
				'sortable' => true
			)
		);

		$filterlist = array('LOG_DATE', 'USER_ID', 'SEARCH_ACTIVITY');

		$this->view->filterlist = $filterlist;


		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'USER_ID');
		$sortDir = $this->_getParam('sortdir', 'asc');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$filterArr = array(
			'USER_ID'    	=> array('StripTags'),
			'SEARCH_ACTIVITY'    => array('StripTags'),
			'LOG_DATE' 			=> array('StripTags', 'StringTrim'),
			'LOG_DATE_END' 			=> array('StripTags', 'StringTrim'),
		);

		$validator = array(
			'USER_ID'    	=> array(),
			'SEARCH_ACTIVITY'    => array(),
			'LOG_DATE' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'LOG_DATE_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		$dataParam = array("USER_ID", "SEARCH_ACTIVITY", "LOG_DATE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {



			if (!empty($this->_request->getParam('whereco'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('whereco') as $key => $value) {
					if ($dtParam == $value) {
						if (!empty($dataParamValue[$dtParam])) {
							$dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
						}

						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}



			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		if (!empty($this->_request->getParam('logdate'))) {

			// echo "masuk";die();

			$createarr = $this->_request->getParam('logdate');
			$dataParamValue['LOG_DATE'] = $createarr[0];
			$dataParamValue['LOG_DATE_END'] = $createarr[1];
		}

		$options = array('allowEmpty' => true);
		$zf_filter 	= new Zend_Filter_Input($filterArr, $validator, $dataParamValue, $options);
		// $filter 	= $zf_filter->getEscaped('filter');
		$filter     = $this->_getParam('filter');
		$clearfilter	= $this->_getParam('filter_clear');


		$user 		= html_entity_decode($zf_filter->getEscaped('USER_ID'));
		$active 	= html_entity_decode($zf_filter->getEscaped('SEARCH_ACTIVITY'));
		$datefrom 	= html_entity_decode($zf_filter->getEscaped('LOG_DATE'));
		$dateto 	= html_entity_decode($zf_filter->getEscaped('LOG_DATE_END'));
		// print_r($active);die;
		$select2 = $this->_db->select()
			->from(array('B' => 'T_FACTIVITY'), array())
			->joinleft(array('C' => 'M_FPRIVILEGE'), 'B.ACTION_DESC = C.FPRIVI_ID', array(
				'B.LOG_DATE',
				'B.USER_ID',
				'FPRIVI_DESC' => new Zend_Db_Expr("(CASE B.ACTION_DESC 
								  								   	WHEN 'FLGN' THEN 'Login' 
								  									WHEN 'FLGT' THEN 'Logout' 
								  									WHEN 'CHMP' THEN 'Change My Password' 
								  									WHEN 'RFPW' THEN 'Reset Forgot Password'
																	WHEN 'MWDG' THEN 'Widget' 
																	WHEN 'MACC' THEN 'Akun Saya'  
								  									ELSE C.FPRIVI_DESC 
								  									END)"),
				'B.ACTION_FULLDESC'
			))
			->joinLeft(array('D' => 'M_USER'), 'D.USER_ID = B.USER_ID AND D.CUST_ID = B.CUST_ID', array('USER_FULLNAME'))
			->order('B.LOG_DATE DESC');
		$select2->where('B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin));

		if ($filter == null) {
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
			//Zend_Debug::dump($this->view->fDateFrom); die;
		}

		if ($filter == null) {
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
			//Zend_Debug::dump($this->view->fDateFrom); die;
		}
		if (!empty($clearfilter)) {
			$datefrom = '';
			$dateto = '';
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';
		}
		if ($filter == TRUE) {
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;
			if (!empty($datefrom)) {
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  = $FormatDate->toString($this->_dateDBFormat);
			}
			if (!empty($datefrom)) {
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($dateto)) {
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto    = $FormatDate->toString($this->_dateDBFormat);
				//Zend_Debug::dump($dateto); die;	
			}

			if (!empty($datefrom) && empty($dateto))
				$select2->where("DATE(B.LOG_DATE) >= " . $this->_db->quote($datefrom));

			if (empty($datefrom) && !empty($dateto))
				$select2->where("DATE(B.LOG_DATE) <= " . $this->_db->quote($dateto));

			if (!empty($datefrom) && !empty($dateto))
				$select2->where("DATE(B.LOG_DATE) between " . $this->_db->quote($datefrom) . " and " . $this->_db->quote($dateto));

			if (empty($datefrom) && empty($dateto)) {
				$datefrom = (date("d/m/Y", strtotime("-7 days")));
				$dateto = (date("d/m/Y"));

				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  = $FormatDate->toString($this->_dateDBFormat);

				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto    = $FormatDate->toString($this->_dateDBFormat);

				$select2->where("DATE(B.LOG_DATE) between " . $this->_db->quote($datefrom) . " and " . $this->_db->quote($dateto));
			}
		}

		// if($filter ==TRUE)
		// {
		if ($active) {
			// var_dump($active);die();
			$select2->where("B.ACTION_DESC LIKE " . $this->_db->quote($active));
			$this->view->active = $active;
		}

		//if($user) $select2->where("USER_ID LIKE ".$user);
		if ($user) {
			// $userid=explode('-',$user);
			// $cekuserid = $userid[0];
			// $cekusername = $userid[1];
			// $this->view->cekuserid = $cekuserid;
			// $this->view->cekusername = $cekusername;
			$userid = explode(',', $user);
			$select2->where("B.USER_ID  in (?)", $userid);
			// $select2->where("B.USER_ID LIKE ".$this->_db->quote('%'.$cekuserid.'%'));
			// var_dump($userid);
		}
		//Zend_Debug::dump($cekuserid); die;
		//Zend_Debug::dump($select2); die;
		// }

		$select2->order($sortBy . ' ' . $sortDir);
		// die($select2);

		//Zend_Debug::dump($arr); die;

		if ($csv || $pdf || $this->_request->getParam('print')) {
			$arr = $this->_db->fetchAll($select2);

			$data_val = array();
			$i = 0;
			foreach ($arr as $data) {
				$data['FPRIVI_DESC'] = $this->language->_($data['FPRIVI_DESC']);
				array_push($data_val, $data);
				//$data_val[$i++] = $data;
			}

			//Zend_Debug::dump($data_val);die;

			foreach ($arr as $key => $value) {
				//echo $key; 
				$arr[$key]["LOG_DATE"] = Application_Helper_General::convertDate($value["LOG_DATE"], 'dd/MM/yyyy HH:mm:ss', 'yyyy-MM-dd HH:mm:ss');
			}

			if ($csv) {
				Application_Helper_General::writeLog('UARP', 'Download PDF User Activity Report');

				$selectcomp = $this->_db->select()
					->from('M_CUSTOMER', array('value' => 'CUST_ID', 'CUST_NAME'))
					->where('CUST_ID = ?', $this->_custIdLogin);

				$selectcomp = $this->_db->fetchAll($selectcomp);
				$headerData[] = $selectcomp[0]['CUST_NAME'];
				$newData[] = array('User Activity Report');

				if ($filter == TRUE) {
					if (!empty($datefrom) && empty($dateto)) {
						$newData[] = array('From : ' . Application_Helper_General::convertDate($datefrom, 'dd/MM/yyyy'));
					}

					if (empty($datefrom) && !empty($dateto)) {
						$newData[] = array(' to : ' . Application_Helper_General::convertDate($dateto, 'dd/MM/yyyy'));
					}

					if (!empty($datefrom) && !empty($dateto)) {
						$newData[] = array('From : ' . Application_Helper_General::convertDate($datefrom, 'dd/MM/yyyy') . ' to : ' . Application_Helper_General::convertDate($dateto, 'dd/MM/yyyy'));
					}

					if (!empty($user)) {
						$newData[] = array('User ID : ' . $user);
					}

					if (!empty($active)) {
						$newData[] = array('Activity Type : ' . $arr[0]['FPRIVI_DESC']);
					}
				}

				if ($filter == null) {
					$newData[] = array('From : ' . Application_Helper_General::convertDate($datefrom, 'dd/MM/yyyy') . ' to : ' . Application_Helper_General::convertDate($dateto, 'dd/MM/yyyy'));
				}

				$newData[] = array(' ');
				$newData[] = array($this->language->_('Date/Time'), $this->language->_('User ID'), $this->language->_('User Name'), $this->language->_('Activity'), $this->language->_('Description'));

				foreach ($data_val as $p => $pTrx) {
					$paramTrx = array(
						"LOG_DATE"  			=> Application_Helper_General::convertDate($pTrx['LOG_DATE'], $this->displayDateTimeFormat, $this->defaultDateFormat),
						"USER_ID"  				=> $pTrx['USER_ID'],
						"USER_NAME"  			=> strtoupper($pTrx['USER_FULLNAME']),
						"ACTION_DESC"  			=> $pTrx['FPRIVI_DESC'],
						"ACTION_FULLDESC"  		=> $pTrx['ACTION_FULLDESC'],
					);
					$newData[] = $paramTrx;
				}

				// $filename = "User Activity Report_[".$this->_custIdLogin."]_[".$this->language->_('Date/Time')."]";
				$date = date("Y-m-d");
				$this->_helper->download->csv($headerData, $newData, null, "User Activity Report_[" . $selectcomp[0]['CUST_NAME'] . "]_[" . $date . "]");
			}

			if ($pdf) {
				Application_Helper_General::writeLog('UARP', 'Download CSV User Activity Report');
				$this->_helper->download->pdf(array($this->language->_('Date/Time'), $this->language->_('User ID'), $this->language->_('Activity'), $this->language->_('Description')), $data_val, null, 'My Activity');
			}
			if ($this->_request->getParam('print')) {
				// $data = $arr;//$this->_dbObj->fetchAll($select);
				// $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Activity', 'data_header' => $fields));

				$setting = new Settings();
				$master_bank_app_name = $setting->getSetting('master_bank_app_name');
				$master_bank_name = $setting->getSetting('master_bank_name');
				$masterbank = $master_bank_app_name . ' - ' . $master_bank_name;

				$selectcomp = $this->_db->select()
					->from('M_CUSTOMER', array('value' => 'CUST_ID', 'CUST_NAME'))
					->where('CUST_ID = ?', $this->_custIdLogin);

				$selectcomp = $this->_db->fetchAll($selectcomp);
				$newData[] = $selectcomp[0]['CUST_NAME'];
				$newData[] = 'User Activity Report';

				if ($filter == TRUE) {
					if (!empty($datefrom) && empty($dateto)) {
						$newData[] = 'From : ' . Application_Helper_General::convertDate($datefrom, 'dd/MM/yyyy');
					}

					if (empty($datefrom) && !empty($dateto)) {
						$newData[] = ' to : ' . Application_Helper_General::convertDate($dateto, 'dd/MM/yyyy');
					}

					if (!empty($datefrom) && !empty($dateto)) {
						$newData[] = 'From : ' . Application_Helper_General::convertDate($datefrom, 'dd/MM/yyyy') . ' to : ' . Application_Helper_General::convertDate($dateto, 'dd/MM/yyyy');
					}

					if (!empty($user)) {
						$newData[] = 'User ID : ' . $user;
					}

					if (!empty($active)) {
						$newData[] = 'Activity Type : ' . $arr[0]['FPRIVI_DESC'];
					}
				}

				if ($filter == null) {
					$newData[] = 'From : ' . Application_Helper_General::convertDate($datefrom, 'dd/MM/yyyy') . ' to : ' . Application_Helper_General::convertDate($dateto, 'dd/MM/yyyy');
				}

				$tempfilter = 1;

				$this->_forward('print', 'index', 'widget', array('data_content' => $data_val, 'data_caption' => $masterbank, 'data_header' => $fields, 'data_filter' => $newData, 'filter' => $tempfilter));
			}
		} else {
			Application_Helper_General::writeLog('UARP', 'View User Activity Report');
		}

		$this->paging($select2);
		if (!empty($dataParamValue)) {
			// $this->view->createdStart = $dataParamValue['PS_CREATED'];
			// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
			//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
			//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

			// unset($dataParamValue['PS_CREATED_END']);
			unset($dataParamValue['LOG_DATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',', $value);
				if (!empty($duparr)) {

					foreach ($duparr as $ss => $vs) {
						$wherecol[]	= $key;
						$whereval[] = $vs;
					}
				} else {
					$wherecol[]	= $key;
					$whereval[] = $value;
				}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
		// if(!empty($this->_request->getParam('wherecol'))){
		//   $this->view->wherecol     = $this->_request->getParam('wherecol');
		// }

		// if(!empty($this->_request->getParam('whereopt'))){
		//   $this->view->whereopt     = $this->_request->getParam('whereopt');
		// }

		// if(!empty($this->_request->getParam('whereval'))){
		//   $this->view->whereval     = $this->_request->getParam('whereval');
		// }
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}
}
