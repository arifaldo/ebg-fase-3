<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class charges_chargestemplatelistdetailController extends customer_Model_Customer 
{
	
	public function initController(){
			  $this->_helper->layout()->setLayout('popup');
	}	
	
	public function indexAction()
	{ 
		$template_id = $this->_getParam('template_id');
		$mData = $this->_db->select()
			->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'), array('*'))
			->join(array('TCO' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = TCO.TEMPLATE_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
			->where("A.TEMPLATE_ID = ".$this->_db->quote($template_id));
		$arr = $this->_db->fetchAll($mData);
		
		$this->view->templatedetail = $arr;
	
			
	}
}