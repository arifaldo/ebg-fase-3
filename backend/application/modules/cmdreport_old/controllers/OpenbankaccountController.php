<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class  cmdreport_OpenbankaccountController extends Application_Main
{
	protected $_moduleDB = 'RTF';


	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$time_start = microtime(true); 
		
		$colomn = array('account_number','account_name','account_currency');
		$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANK_TABLE'),'A.BANK_CODE = B.BANK_CODE',array('bankname' => 'B.BANK_NAME'))
					//	  ->group('A.APIKEY_ID')
						  ->where('A.`FIELD` IN (?)',$colomn)
						 ->order('A.APIKEY_ID ASC')
						 
				);
		//echo 'Total execution time in seconds: ' . (microtime(true) - $time_start);die; 
					//	 echo $acctlist;die;
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['bankname'] = $value['bankname'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['CREATED'] = $value['CREATED'];
			$account[$value['ID']]['CREATEDBY'] = $value['CREATEDBY'];
			
		}

		$datapers = array();

		$totalonline = 0;

		foreach ($account as $key => $request) {
			
				$datapers[$key-1]['bank_name']  = $request['bankname'];
				$datapers[$key-1]['bank_code']  = $request['BANK_CODE'];
				$datapers[$key-1]['account_number']  = $request['account_number'];
				$datapers[$key-1]['account_name']  = $request['account_name'];
				if (isset($request['account_alias'])) {
					$datapers[$key-1]['account_alias']  = $request['account_alias'];
				}else{
					$datapers[$key-1]['account_alias']  = '-';
				}
				$datapers[$key-1]['account_currency']  = 'IDR';
				$datapers[$key-1]['account_status'] = '1';
				$datapers[$key-1]['created'] = $request['CREATED'];
				$datapers[$key-1]['createdby'] = $request['CREATEDBY'];
		}

		// echo "<pre>";
		// var_dump($datapers);
		// die();

		$total = count($datapers);

		$acctmanual = $this->_db->fetchAll(
				$this->_db->select()
					 ->from(array('A' => 'T_BALANCE'))
					 ->join(array('C' => 'M_BANK_TABLE'),'A.BANK_CODE = C.BANK_CODE',array('bankname' => 'C.BANK_NAME'))
					 ->where('A.ACCT_STATUS = ?','5')
					 ->group("A.ACCT_NO")
					 ->group("A.BANK_CODE")
					 ->limit(100)
			);
		// echo $acctmanual;die;
		$totalmanual = 0;
		foreach ($acctmanual as $key => $value) {
			$datapersmanual[$total]['bank_code']  = $value['BANK_CODE'];
			$datapersmanual[$total]['bank_name']  = $value['bankname'];
			$datapersmanual[$total]['account_number']  = $value['ACCT_NO'];
			$datapersmanual[$total]['account_currency']  = $value['CCY_ID'];
			$datapersmanual[$total]['account_alias']  = $value['ACCT_ALIAS'];
			$datapersmanual[$total]['account_name']  = $value['ACCT_ALIAS'];
			$datapersmanual[$total]['account_status'] = $value['ACCT_STATUS'];
			$total++;
		}

		if (count($datapersmanual) > 0) {
			$datapersonal =	array_merge($datapers, $datapersmanual);
		}
		else{
			$datapersonal = $datapers;
		}

		$this->view->datapersonal = $datapersonal;

		$status = $this->_customerstatus;
		$options = array_combine(array_values($status['code']),array_values($status['desc']));
		$this->view->listStatus = array('' =>'-- '.$this->language->_('Any Value').' --');
		$this->view->listStatus += $options;

		$fields = array	(
							'NO'  			=> array	(
																	'field' => 'NO',
																	'label' => $this->language->_('NO'),
																	'sortable' => true
																),
							'bank_name'  			=> array	(
																	'field' => 'bank_name',
																	'label' => $this->language->_('Bank Name'),
																	'sortable' => true
																),
							'account_number'  			=> array	(
																	'field' => 'account_number',
																	'label' => $this->language->_('Account'),
																	'sortable' => true
																),
							'account_name'  					=> array	(
																		'field' => 'account_name',
																		'label' => $this->language->_('Account Name'),
																		'sortable' => true
																	),
							'account_currency'  					=> array	(
																		'field' => 'account_currency',
																		'label' => $this->language->_('CCY'),
																		'sortable' => true
																	),
							'created'  => array	(
																		'field' => 'created',
																		'label' => $this->language->_('Registered Date'),
																		'sortable' => true
																	),
							'createdby'  => array	(
																		'field' => 'createdby',
																		'label' => $this->language->_('Registered By'),
																		'sortable' => true
																	),
						);

		$filterlist = array("COMP_CODE","COMP_NAME","ACCT_NO","ACCT_NAME","STATUS","CCY_ID","ACCT_DESC","ACCT_UPDATED");

		$this->view->filterlist = $filterlist;
		//get page, sortby, sortdir
        $page    = $this->_getParam('page');
        $sortBy  = $this->_getParam('sortby','C.CUST_ID');
        $sortDir = $this->_getParam('sortdir','asc');

        $csv 		= $this->_request->getParam('csv');
        $print 		= $this->_request->getParam('print');

        //validate parameters before passing to view and query
        $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

        $sortBy = (Zend_Validate::is($sortBy,'InArray',
        	array(array_keys($fields))
            ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

        $sortDir = (Zend_Validate::is($sortDir,'InArray',
        	array('haystack'=>array('asc','desc'))
            ))? $sortDir : 'asc';

		$filterArr = array('filter' => array('StripTags','StringTrim'),
			   'COMP_CODE'    => array('StripTags','StringTrim','StringToUpper'),
			   'COMP_NAME'  => array('StripTags','StringTrim','StringToUpper'),
			   'ACCT_NO'    => array('StripTags','StringTrim','StringToUpper'),
			   'ACCT_NAME'  => array('StripTags','StringTrim','StringToUpper'),
			   'CCY_ID'  => array('StripTags','StringTrim','StringToUpper'),
			   'ACCT_DESC'  => array('StripTags','StringTrim','StringToUpper'),
			   'ACCT_UPDATED'  => array('StripTags','StringTrim','StringToUpper'),
			   'STATUS' => array('StripTags','StringToUpper'),
			   'GROUP_NAME' => array('StripTags','Alnum','StringToUpper')
			  );

		  $dataParam = array("COMP_CODE","COMP_NAME","ACCT_NO","ACCT_NAME","STATUS","CCY_ID","ACCT_DESC");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		//$arrACCT_STATUS = array(0=>"Unapproved",1=>"Approved",2=>"Suspended",3=>"Deleted",4=>"Request Edit",5=>"Request  Delete");

		
		
		
			$this->view->fields = $fields;
			$this->view->filter = $filter;
			$this->view->modulename = $this->_request->getModuleName();
			if(!$this->_request->isPost()){
			Application_Helper_General::writeLog('RPAC','View Bank Account List Report');
			}

			if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
					$whereval[] = $value;
				}
	        $this->view->wherecol     = $wherecol;
	        $this->view->whereval     = $whereval;

	      }

	    if($csv || $print)
		{	
			
			$i=0;
			foreach($datapersonal as $row)
			{
				
				$data[$i]['NO'] 				= $i+1;
				$data[$i]['bank_name'] 			= $row['bank_name'];
				$data[$i]['account_number'] 	= $row['account_number'];
				$data[$i]['account_name'] 		= $row['account_name'];
				$data[$i]['account_currency'] 	= $row['account_currency'];
				$data[$i]['created'] 			= Application_Helper_General::convertDate($row['created'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$data[$i]['createdby'] 			= $row['createdby'];
				$i++;
			}

			if($csv)
			{
				
				unset($fields['NO']);

				$header = array ('0'=>'No');
				$header = array_merge( $header,Application_Helper_Array::simpleArray($fields, "label"));
			
				$this->_helper->download->csv($header,$data,null,$this->language->_('360 Open Bank Account'));  
				Application_Helper_General::writeLog('SPSB','Download CSV 360 Open Bank Account');
			}
			elseif($this->_request->getParam('print') == 1){
				
				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => '360 Open Bank Account', 'data_header' => $fields));
			}
			else
			{
				//Application_Helper_General::writeLog('SPSB','View Submission Report');
			}
		}

      
	}
}
?>
