<?php

Class customeraccount_Model_Customeraccount extends Application_Main
{

    protected $_acctData = array('CUST_ID'     => null,
                                 'ACCT_NO'     => null,
  	                             'CCY_ID'      => null,
		                         'ACCT_EMAIL'  => null,
                                 'ACCT_STATUS' => null
        				         );

   
    public function insertTempCustomerAcct($change_id,$acct_data) 
    {  
       $content = array('CHANGES_ID'       => $change_id,
									'CUST_ID'          => $acct_data['CUST_ID'],
									'ACCT_NO'          => $acct_data['ACCT_NO'],
									'CCY_ID'           => isset($acct_data['CCY_ID']) ? $acct_data['CCY_ID'] : null,
									'ACCT_EMAIL'       => isset($acct_data['ACCT_EMAIL']) ? $acct_data['ACCT_EMAIL'] : null,
									'ACCT_STATUS'      => isset($acct_data['ACCT_STATUS']) ? $acct_data['ACCT_STATUS'] : null,
									'ACCT_SUGGESTED'   => isset($acct_data['ACCT_SUGGESTED']) ? $acct_data['ACCT_SUGGESTED'] : null,
									'ACCT_SUGGESTEDBY' => isset($acct_data['ACCT_SUGGESTEDBY']) ? $acct_data['ACCT_SUGGESTEDBY'] : null,
									'PLAFOND'          => isset($acct_data['PLAFOND']) ? $acct_data['PLAFOND'] : null,
									'ACCT_SOURCE'      => isset($acct_data['ACCT_SOURCE']) ? $acct_data['ACCT_SOURCE'] : null,
									'ACCT_NAME'        => isset($acct_data['ACCT_NAME']) ? $acct_data['ACCT_NAME'] : null,
									'ACCT_TYPE'        => isset($acct_data['ACCT_TYPE']) ? $acct_data['ACCT_TYPE'] : null,
									'ACCT_DESC'        => isset($acct_data['ACCT_DESC']) ? $acct_data['ACCT_DESC'] : null,
								//	'ACCT_ALIAS_NAME'  => isset($acct_data['ACCT_ALIAS_NAME']) ? $acct_data['ACCT_ALIAS_NAME'] : null,
									'ACCT_ALIAS_NAME'  => isset($acct_data['ACCT_ALIAS_NAME']) ? $acct_data['ACCT_ALIAS_NAME'] : null,
		                            'ORDER_NO'         => isset($acct_data['ORDER_NO']) ? $acct_data['ORDER_NO'] : null,
									'ACCT_NAME'        => isset($acct_data['ACCT_NAME']) ? $acct_data['ACCT_NAME'] : null,
									'GROUP_ID'         => isset($acct_data['GROUP_ID']) ? $acct_data['GROUP_ID'] : null,
									'ACCT_RESIDENT'         => isset($acct_data['ACCT_RESIDENT']) ? $acct_data['ACCT_RESIDENT'] : null,
						       		'ACCT_CITIZENSHIP'         => isset($acct_data['ACCT_CITIZENSHIP']) ? $acct_data['ACCT_CITIZENSHIP'] : null,
						       		'ACCT_CATEGORY'         => isset($acct_data['ACCT_CATEGORY']) ? $acct_data['ACCT_CATEGORY'] : null,
						       		'ACCT_ID_TYPE'         => isset($acct_data['ACCT_ID_TYPE']) ? $acct_data['ACCT_ID_TYPE'] : null,
						       		'ACCT_ID_NUMBER'         => isset($acct_data['ACCT_ID_NUMBER']) ? $acct_data['ACCT_ID_NUMBER'] : null,
								   );
    
		//$acct_data['CHANGES_ID'] = $change_id;
		//$content = $acct_data;
		
		//Zend_Debug::dump($acct_data); 
        
	    $insert = $this->_db->insert('TEMP_CUSTOMER_ACCT',$content);

  }
  
  public function updateTempCustomerAcct($changes_id,$acct_data)
  {
       $content = array(
						//'CUST_ID'          => $acct_data['CUST_ID'],
  	                 	//'ACCT_NO'          => $acct_data['ACCT_NO'],
		             	//'CCY_ID'           => $acct_data['CCY_ID'],
                        'ACCT_EMAIL'       => $acct_data['ACCT_EMAIL'],
                        'ACCT_ALIAS_NAME'  => isset($acct_data['ACCT_ALIAS_NAME']) ? $acct_data['ACCT_ALIAS_NAME'] : null,
                        //'ACCT_STATUS'      => $acct_data['ACCT_STATUS'],
                        //'ACCT_SUGGESTED'   => $acct_data['ACCT_SUGGESTED'],
                       // 'ACCT_SUGGESTEDBY' => $acct_data['ACCT_SUGGESTEDBY'],
                       );
  
       $whereArr = array('CHANGES_ID = ?'=>$changes_id);
	   $customerupdate = $this->_db->update('TEMP_CUSTOMER_ACCT',$content,$whereArr);
  }
  
  
  public function getAccStatus($acct_no)
  {
      $select = $this->_db->select()
                           ->from('M_CUSTOMER_ACCT')
                           ->where('ACCT_NO='.$this->_db->quote((string)$acct_no))
                           ->query()->fetch();
                           
	  return $select;
  }
  
  public function getCustData($cust_id)
  {
      $select = $this->_db->select()
                           ->from('M_CUSTOMER',array('CUST_STATUS','CUST_ID','CUST_NAME'))
                           ->where('CUST_ID='.$this->_db->quote((string)$cust_id))
                           ->query()->fetch();
                           
	  return $select;
  }
  
  public function getTempAcct($acct_no)
  {
      $select = $this->_db->select()
                           ->from('M_CUSTOMER_ACCT')
                           ->where('ACCT_NO='.$this->_db->quote((string)$acct_no))
                           ->query()->fetch();
                           
	  return $select;
  }
  
	public function getCustomerAcct($cust_id)
    { 
        $select = $this->_db->select()
	 				         ->from(array('CA'=>'M_CUSTOMER_ACCT'),array('*'))
		  				     ->joinleft(array('g'=>'M_GROUPING'),'g.GROUP_ID=CA.GROUP_ID',array('GROUP_NAME'))
		  				     ->where('UPPER(CA.ACCT_NO)='.$this->_db->quote((string)$cust_id))
		  				     //->where('ACCT_STATUS!=3')
		  				     ->order(array('ORDER_NO ASC'))		  				     
		  				     ->query()->fetchAll();
		 
		return $select;  				     
    }
    
  
}




