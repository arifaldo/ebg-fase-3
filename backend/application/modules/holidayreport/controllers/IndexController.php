<?php

class holidayreport_IndexController extends Application_Main {
	
	protected $_moduleDB  = 'HTB';
	
	public function indexAction(){
		$this->_helper->layout()->setLayout('newlayout');
		
        $getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;

        $fields = array('datetime' 	   => array('field'    => 'HOLIDAY_DATE',
                                        	    'label'    => $this->language->_('Date'),
                                        	    'sortable' => true),
                        'ccy'       => array('field'    => 'HOLIDAY_CCY',
                                        	    'label'    => $this->language->_('CCY '),
                                        	    'sortable' => true),
                        'notes' 	   => array('field'    => 'HOLIDAY_DESCRIPTION',
                                        	    'label'    => $this->language->_('Notes'),
                                        	    'sortable' => true),
						'suggested'		=> array('field'    => 'SUGGESTED',
											    'label'    => $this->language->_('Suggested'),
                                        	    'sortable' => true),
						'approved'     => array('field'    => 'APPROVED',
											    'label'    => $this->language->_('Approved'),
                                        	    'sortable' => true),
            );
    
        $sqlQueryHoliday= $this->_db->select()
                                ->from(array('B' => 'M_HOLIDAY'),array(
                                    'day' => 'DAY(B.HOLIDAY_DATE)',
                                    'month' => 'MONTH(B.HOLIDAY_DATE)',
                                    'year' => 'YEAR(B.HOLIDAY_DATE)',
                                    'HOLIDAY_YEAR' => 'B.HOLIDAY_YEAR',
                                    'HOLIDAY_DATE' => 'B.HOLIDAY_DATE',
                                    'HOLIDAY_DESCRIPTION' => 'B.HOLIDAY_DESCRIPTION',
                                    'HOLIDAY_CCY' => 'B.HOLIDAY_CCY'
                                ))
                                ->join(array('A' => 'M_BUSER'), 'A.BUSER_ID = B.SUGGESTEDBY',array('suggestedby'=>'A.BUSER_NAME'))
                                ->join(array('C' => 'M_BUSER'), 'C.BUSER_ID = B.APPROVEDBY',array('approvedby'=>'A.BUSER_NAME'))
                                ->order('B.HOLIDAY_DATE ASC');
                                
		$listHoliday = $this->_db->fetchAll($sqlQueryHoliday);
        // echo "<pre>";
        // var_dump($listHoliday);


        $this->view->listHoliday = $listHoliday;
        $this->view->filter = "0";

        $filter = $this->_getParam("filter");
        $pdf = $this->_getParam("pdf");
        $print = $this->_getParam("print");
        $year = $this->_getParam("selectyear");

        if($filter){
            if($filter == '0'){
                $select2 = $listHoliday;
            }else{
                $select2 = $this->_db->select()
                                ->from(array('B' => 'M_HOLIDAY'),array(
                                    'day' => 'DAY(B.HOLIDAY_DATE)',
                                    'month' => 'MONTH(B.HOLIDAY_DATE)',
                                    'year' => 'YEAR(B.HOLIDAY_DATE)',
                                    'HOLIDAY_YEAR' => 'B.HOLIDAY_YEAR',
                                    'HOLIDAY_DATE' => 'B.HOLIDAY_DATE',
                                    'HOLIDAY_DESCRIPTION' => 'B.HOLIDAY_DESCRIPTION',
                                    'HOLIDAY_CCY' => 'B.HOLIDAY_CCY'
                                ))
                                ->where('B.HOLIDAY_CCY =?', $filter)
                                ->order('B.HOLIDAY_DATE ASC')
                                ->query()->fetchAll();
                
                // echo "<pre>";
                // var_dump($select2);die;
            }
            $this->view->listHoliday = $select2;
            $this->view->filter = $filter;
        }

        if($pdf){
            
            $select2 = $this->_db->select()
                                ->from(array('B' => 'M_HOLIDAY'),array(
                                    'day' => 'DAY(B.HOLIDAY_DATE)',
                                    'month' => 'MONTH(B.HOLIDAY_DATE)',
                                    'year' => 'YEAR(B.HOLIDAY_DATE)',
                                    'HOLIDAY_YEAR' => 'B.HOLIDAY_YEAR',
                                    'HOLIDAY_DATE' => 'B.HOLIDAY_DATE',
                                    'HOLIDAY_DESCRIPTION' => 'B.HOLIDAY_DESCRIPTION',
                                    'HOLIDAY_CCY' => 'B.HOLIDAY_CCY',
                                    'APPROVED' => 'B.APPROVED',
                                    'APPROVEDBY' => 'B.APPROVEDBY',
                                    'SUGGESTED' => 'B.SUGGESTED',
                                    'SUGGESTEDBY' => 'B.SUGGESTEDBY'
                                ))
                                ->where('B.HOLIDAY_YEAR =?', $year)
                                ->order('B.HOLIDAY_DATE ASC')
                                ->query()->fetchAll();

            $this->view->content = $select2;
            $this->view->year = $year;

            $outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/report_pdf.phtml')."</td></tr>";
			// echo $outputHTML;die;
			$this->_helper->download->pdf(null,null,null,'Holiday Report '.$year,$outputHTML);
        }

        if($print){
            if(empty($filter) || $filter == '0'){
                $filter = 'All CCY';
            }

            $dfilter = array('Year : '.$year, 'CCY : '.$filter);
            
            $data= $this->_db->select()
                            ->from(array('B' => 'M_HOLIDAY'),array(
                                'HOLIDAY_DATE' => 'B.HOLIDAY_DATE',
                                'HOLIDAY_CCY' => 'B.HOLIDAY_CCY',
                                'HOLIDAY_DESCRIPTION' => 'B.HOLIDAY_DESCRIPTION',
                                'SUGGESTED' => new Zend_Db_Expr("CONCAT(B.SUGGESTEDBY , ' (' , B.SUGGESTED , ')  ' )"),
                                'APPROVED' => new Zend_Db_Expr("CONCAT(B.APPROVEDBY , ' (' , B.APPROVED , ')  ' )")
                            ))
                            ->where('B.HOLIDAY_YEAR =?', $year)
                            ->order('B.HOLIDAY_DATE ASC');
            
            if($filter != 'All CCY'){
                $data->where('B.HOLIDAY_CCY =?', $filter);
            }
		    $newdata = $this->_db->fetchAll($data);
            $this->_forward('print', 'index', 'widget', array('data_content' => $newdata, 'data_filter' => $dfilter, 'data_caption' => 'Holiday Report '.$year, 'data_header' => $fields));
        }

        
    }

    public function detaileventAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
    
        $date = $this->_getParam('date');
    
        $select = $this->_db->select()
                           ->from(array('A' => 'M_HOLIDAY'),array('*'))
                           ->join(array('B' => 'M_BUSER'), 'B.BUSER_ID = A.SUGGESTEDBY',array('suggestby'=>'B.BUSER_NAME'))
                           ->join(array('C' => 'M_BUSER'), 'C.BUSER_ID = A.APPROVEDBY',array('approveby'=>'C.BUSER_NAME'))
                           ->where('A.HOLIDAY_DATE = ?', $date);
    
        $data = $this->_db->fetchRow($select);
        
        // $data = $date.''.$desc.''.$ccy;
        $result = array();
        if (empty($data)) {
          $result['status'] = '0';
        }else{
          $result['status'] = '1';
          $result['suggested'] = $data['SUGGESTED'];
          $result['suggestedby'] = $data['suggestby'].' ('.$data['SUGGESTEDBY'].')';
          $result['approved'] = $data['APPROVED'];
          $result['approvedby'] = $data['approveby'].' ('.$data['APPROVEDBY'].')';
        }
    
        print_r(json_encode($result));
    }

    public function updateinfoAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
    
        $year = $this->_getParam('year');
    
        $select = $this->_db->select()
                           ->from(array('A' => 'M_HOLIDAY'),array('*'))
                           ->join(array('B' => 'M_BUSER'), 'B.BUSER_ID = A.SUGGESTEDBY',array('suggestby'=>'B.BUSER_NAME'))
                           ->join(array('C' => 'M_BUSER'), 'C.BUSER_ID = A.APPROVEDBY',array('approveby'=>'C.BUSER_NAME'))
                           ->where('A.HOLIDAY_YEAR = ?', $year)
                           ->order('A.HOLIDAY_DATE DESC');
    
        $data = $this->_db->fetchAll($select);
        
        // $data = $date.''.$desc.''.$ccy;
        $result = array();
        if (empty($data)) {
          $result['status'] = '0';
        }else{
          $result['status'] = '1';
          $result['suggested'] = $data[0]['SUGGESTED'];
          $result['suggestedby'] = $data[0]['suggestby'].' ('.$data[0]['SUGGESTEDBY'].')';
          $result['approved'] = $data[0]['APPROVED'];
          $result['approvedby'] = $data[0]['approveby'].' ('.$data[0]['APPROVEDBY'].')';
        }
    
        print_r(json_encode($result));
    }

}