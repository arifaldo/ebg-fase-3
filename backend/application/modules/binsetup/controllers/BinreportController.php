<?php

require_once 'Zend/Controller/Action.php';

class binsetup_BinreportController extends binsetup_Model_Binsetup
{
	
	public function initController()
	{       
		//$statusArr = Application_Helper_Array::globalvarArray($this->_masteruserStatus);
		$statusArr = array(''=>'-- '.$this->language->_('Any Value') .' --','1'=>$this->language->_('Approved'), '2'=>$this->language->_('Suspended'), '3'=>$this->language->_('Deleted'));
	    $this->view->statusArr = $statusArr;
		
		$custArr  = Application_Helper_Array::listArray($this->getAllCustomer(),'CUST_ID','CUST_ID');
		$custArr  = array_merge(array(''=>'-- '.$this->language->_('Any Value') .' --'),$custArr);
		$this->view->custArr = $custArr;
		/*
		Zend_Debug::dump($countryArr);
		die;*/
	
		$this->view->signArr = array('EQ'=>'=', 'NE'=>'!=', 'LT'=>'<', 'GT'=>'>', 'LE'=>'<=', 'GE'=>'>=');
		//format display date
		$this->view->dateDisplayFormat = $this->_dateDisplayFormat;
    }
 
  public function indexAction() 
  {  
     $fields = array('custid'   => array('field'    => 'CUST_ID',
                                        'label'    => $this->language->_('Company Code'),
                                        'sortable' => true),
    
                    'custname' => array('field'    => 'CUST_NAME',
                                        'label'    => $this->language->_('Company Name'),
                                        'sortable' => true),
    
                    'bin'     => array('field'    => 'CUST_CITY',
                                        'label'    => $this->language->_('BIN'),
                                        'sortable' => true),
    
                    'status'   => array('field'    => 'CUST_STATUS',
                                        'label'    => $this->language->_('Status'),
                                        'sortable' => true),
    
                    'latestSuggestion'     => array('field'    => 'CUST_SUGGESTED',
                                               'label'    => $this->language->_('Latest Suggestion'),
                                               'sortable' => true),
    
                    'latestSuggestor'   => array('field'  => 'CUST_SUGGESTEDBY',
                                               'label'    => $this->language->_('Latest Suggester'),
                                               'sortable' => true),
    
                    'latestApproval'    => array('field'  => 'CUST_UPDATED',
                                               'label'    => $this->language->_('Latest Approval'),
                                               'sortable' => true),
    
                    'latestApprover'  => array('field'   => 'CUST_UPDATEDBY',
                                               'label'    => $this->language->_('Latest Approver'),
                                               'sortable' => true)
                   );
    
    //validasi page, jika input page bukan angka               
    $page = $this->_getParam('page');
    
    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
	$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
	$csv = $this->_getParam('csv');
	$pdf = $this->_getParam('pdf');
	
    $filterArr = array('filter'  => array('StripTags','StringTrim'),
                       'cid'     => array('StripTags','StringTrim','StringToUpper'),
                       'cname'   => array('StripTags','StringTrim','StringToUpper'),
                       'status'  => array('StripTags','StringTrim'),
                       'bin'    => array('StripTags','StringTrim'),
    
                       'latestSuggestionSign' => array('StripTags','StringTrim','StringToUpper'),
    				   'latestSuggestion'     => array('StripTags','StringTrim'),
                       'latestSuggestor'      => array('StripTags','StringTrim','StringToUpper'),
                       'latestApprovalSign'   => array('StripTags','StringTrim','StringToUpper'),
     				   'latestApproval'       => array('StripTags','StringTrim'),
                       'latestApprover'       => array('StripTags','StringTrim','StringToUpper'),
                      );
                      
    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
    $filter = $zf_filter->getEscaped('filter');
    
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;
    
  	$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
	if(count($temp)>1){
      	if($temp[0]=='F' || $temp[0]=='S'){
      		if($temp[0]=='F')
        		$this->view->error = 1;
        	else
        		$this->view->success = 1;
      		$msg = ''; unset($temp[0]);
      		foreach($temp as $value)
      		{
      			if(!is_array($value))
      				$value = array($value);
      			$msg .= $this->view->formErrors($value);
      		}
        	$this->view->customer_msg = $msg;
     	}	
    }
    
    
     // proses pengambilan data filter,display all,sorting
        $select = array(); 
    
//        $select = $this->_db->select()
//						    ->from('M_CUSTOMER_BIN',array('CUST_ID','CUST_BIN','CUST_BIN_STATUS','BIN_SUGGESTED','BIN_SUGGESTEDBY','BIN_UPDATED','BIN_UPDATEDBY'));
						       //->where('CUST_STATUS != 3');
						       //->where('CUST_STATUS <> 3');
						     //->where("CONVERT(VARCHAR(20), CUST_SUGGESTED, 111) = '2012/04/12'")
						       // ->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($cid)));
						       //  CONVERT(VARCHAR(8), now(), 112) AS [YYYYMMDD]
						     // ->query()->fetchAll();


	  	 $select = $this->_db->select()
  	                         ->from(array('A' => 'M_CUSTOMER_BIN'),array())
//							 ->joinLeft(array('B' => 'M_CUSTOMER'),'A.CUST_ID = B.CUST_ID AND A.CUST_ID = B.CUST_ID',array('*'));
							 ->joinLeft(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('A.CUST_BIN_ID', 'A.CUST_BIN_TMP',
							 																	 'A.CUST_ID', 'C.CUST_NAME',
							 																	 'A.CUST_BIN', 'A.CUST_BIN_STATUS',
							 																	 'A.BIN_SUGGESTED', 'A.BIN_SUGGESTEDBY',
							 																	 'A.BIN_UPDATED', 'A.BIN_UPDATEDBY',
							 																	 ));
	  			                     
	  			                     
	  			                     
	  			                     
	  			                     
	  			                     
	  			                     
	  			                     
		  				     
						    
		//saat pertama kali klik, lgsg filter by date today				        
		if($filter == '')
		{
		        /*$today = date('d/m/Y');
		        
		        $this->view->latestSuggestionFrom = $today;
			    $this->view->latestSuggestionTo   = $today;
		        
			    //konversi date agar dapat dibandingkan
			    $latestSuggestionFrom   = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
								 	       new Zend_Date($today,$this->_dateDisplayFormat):
								 	       false;
			
			    $latestSuggestionTo     = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
								 	       new Zend_Date($today,$this->_dateDisplayFormat):
								 	       false;
	           
	           if($latestSuggestionFrom)  $select->where("CONVERT(date,CUST_SUGGESTED) >= CONVERT(DATE,".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
			   if($latestSuggestionTo)    $select->where("CONVERT(date,CUST_SUGGESTED) <= CONVERT(DATE,".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");*/
		}			    
    
		
        if($filter == TRUE)
        {
			$cid       = html_entity_decode($zf_filter->getEscaped('cid'));
			$cname     = html_entity_decode($zf_filter->getEscaped('cname'));
			$bin      = html_entity_decode($zf_filter->getEscaped('bin'));
			$status    = html_entity_decode($zf_filter->getEscaped('status'));
			$latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('latestSuggestionFrom'));
			$latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('latestSuggestionTo'));
			$latestSuggestor        = html_entity_decode($zf_filter->getEscaped('latestSuggestor'));
			$latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('latestApprovalFrom'));
			$latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('latestApprovalTo'));
			$latestApprover         = html_entity_decode($zf_filter->getEscaped('latestApprover'));
			
			//konversi date agar dapat dibandingkan
			$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom,$this->_dateDisplayFormat))?
								 	   new Zend_Date($latestSuggestionFrom,$this->_dateDisplayFormat):
								 	   false;
			
			$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo,$this->_dateDisplayFormat))?
								 	   new Zend_Date($latestSuggestionTo,$this->_dateDisplayFormat):
								 	   false;
								 	   					 	   
			$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom,$this->_dateDisplayFormat))?
								 	   new Zend_Date($latestApprovalFrom,$this->_dateDisplayFormat):
								 	   false;

			$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo,$this->_dateDisplayFormat))?
								 	   new Zend_Date($latestApprovalTo,$this->_dateDisplayFormat):
								 	   false;					 	   
			
	        //if($cid)            $select->where('UPPER(CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cid).'%'));
			if($cid)              $select->where('UPPER(A.CUST_ID)='.$this->_db->quote(strtoupper($cid)));
			if($bin)              $select->where('UPPER(CUST_BIN) LIKE '.$this->_db->quote('%'.strtoupper($bin).'%'));
			if($cname)            $select->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($cname).'%'));
			if($status)           $select->where('CUST_BIN_STATUS=?',$status);
			if($latestSuggestor)  $select->where('UPPER(BIN_SUGGESTEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestSuggestor).'%'));
			if($latestApprover)   $select->where('UPPER(BIN_UPDATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestApprover).'%'));

			if($latestSuggestionFrom)  $select->where("DATE(BIN_SUGGESTED) >= DATE(".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
			if($latestSuggestionTo)    $select->where("DATE(BIN_SUGGESTED) <= DATE(".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");
			if($latestApprovalFrom)    $select->where("DATE(BIN_UPDATED) >= DATE(".$this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)).")");
			if($latestApprovalTo)      $select->where("DATE(BIN_UPDATED) <= DATE(".$this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)).")");
			
			$this->view->cid     = $cid;
			$this->view->cname   = $cname;
			$this->view->bin    = $bin;
			$this->view->status  = $status;
		    $this->view->latestSuggestor  = $latestSuggestor;
			$this->view->latestApprover   = $latestApprover;
			
			if($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
			if($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
			if($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
			if($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
        }
        //utk sorting 
        $select->order($sortBy.' '.$sortDir);
       
    // END proses pengambilan data filter,display all,sorting

    $data = $this->_db->fetchAll($select);
    
    $this->paging($select);
    
    if($pdf || $csv)
	{
		
		$arr = $data;
		$statusCode = array_flip($this->_masterglobalstatus['code']);
    	$statusDesc = $this->_masterglobalstatus['desc'];
		foreach($arr as $key=>$value)
		{
			unset($arr[$key]["CUST_BIN_ID"]);
			unset($arr[$key]["CUST_BIN_TMP"]);
			unset($arr[$key]["BIN_CREATED"]);
			unset($arr[$key]["BIN_CREATEDBY"]);
			$arr[$key]["CUST_BIN_STATUS"] = $this->language->_($statusDesc[$statusCode[$value['CUST_BIN_STATUS']]]);
			$arr[$key]["BIN_SUGGESTED"] = Application_Helper_General::convertDate($value["BIN_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			$arr[$key]["BIN_UPDATED"] = Application_Helper_General::convertDate($value["BIN_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			
		}
		
		if($csv)
		{
			Application_Helper_General::writeLog('BNDL','Download CSV BIN Report');				
			$this->_helper->download->csv(array('Company Code', 'Company Name', 'BIN', 'Status', 'Latest Suggestion', 'Latest Suggester', 'Latest Approval', 'Latest Approver'),$arr,null,'BIN Report');
		}
		
		if($pdf)
		{
			Application_Helper_General::writeLog('BNDL','Download PDF BIN Report');
			$this->_helper->download->pdf(array('Company Code', 'Company Name', 'BIN', 'Status', 'Latest Suggestion', 'Latest Suggester', 'Latest Approval', 'Latest Approver'),$arr,null,'BIN Report');
		}
		
	}else{
		//insert log
		 try
		 {
		   $this->_db->beginTransaction();
		   
		   Application_Helper_General::writeLog('BNLS','View Customer BIN Setup Customer BIN List');
		   
		   $this->_db->commit();
		 }
	     catch(Exception $e)
	     {
	 	    $this->_db->rollBack();
		 }
	}
    
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    $this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
    $this->view->statusDesc = $this->_masterglobalstatus['desc'];
    $this->view->modulename = $this->_request->getModuleName();
    
    
     
	
  }
  
  
}


