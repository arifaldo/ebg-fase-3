<?php
Class ofacbank_Model_Ofacbank {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getCountry()
    { 
       $select = $this->_db->select()
                           ->from('M_COUNTRY')
                           ->query()->fetchAll();
        return $select;
    }

  	public function getCountryName($countryCode){
  		$select = $this->_db->select()
  							->from('M_COUNTRY')
  							->where('COUNTRY_CODE=?',$countryCode);
  		$result = $this->_db->fetchrow($select);
  		return $result;
  	}

    public function insertData($content)
    {
		$this->_db->insert("M_OFAC",$content);
    }
  
    public function deleteData($countryCode)
    {
		$this->_db->delete('M_OFAC','country_code = ?', $countryCode);
    }
}