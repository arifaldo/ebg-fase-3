<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class Appgroup_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{       
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$cust_id = $this->_custIdLogin;
		$selectGroup = $this->_db->select()
								 ->from(array('AG'=>'M_APP_GROUP_USER'))
								 ->join(array('U'=>'M_USER'), 'AG.CUST_ID = U.CUST_ID AND AG.USER_ID = U.USER_ID', array('USER_FULLNAME'))
								 ->where('UPPER(AG.CUST_ID) = ?', $cust_id)
								 ->query()->fetchAll();

		if(count($selectGroup) > 0){
			$dataGroup = array();
			for($i=0; $i < 10; $i++){
				$dataGroup[$i]['GROUP_NAME'] = 'Group '.($i+1);
				$dataGroup[$i]['GROUP_MEMBER'] = 'N/A';
			}
			foreach($selectGroup as $row){
				list($grouptype,$groupname,$groupnum) = explode("_",$row['GROUP_USER_ID']);
				$groupnum = (int)(ltrim($groupnum,"0"));
				if($grouptype == "S"){
					if($specialGroupMember) $specialGroupMember .= ', '.$row['USER_FULLNAME'].' ('.$row['USER_ID'].')';
					else $specialGroupMember = $row['USER_FULLNAME'].' ('.$row['USER_ID'].')';
				}
				else{
					// $ctr = count($dataGroup);
					// if($tempname == $dataGroup[$ctr-1]['GROUP_NAME'])
					// 	$dataGroup[$ctr-1]['GROUP_MEMBER'] .= ', '.$row['USER_FULLNAME'].' ('.$row['USER_ID'].')';
					// else{
					// 	$dataGroup[$ctr+1]['GROUP_NAME'] = $tempname;
					// 	$dataGroup[$ctr+1]['GROUP_MEMBER'] = $row['USER_FULLNAME'].' ('.$row['USER_ID'].')';
					// }

					if($dataGroup[$groupnum-1]['GROUP_MEMBER'] == "N/A") $dataGroup[$groupnum-1]['GROUP_MEMBER'] = $row['USER_FULLNAME'].' ('.$row['USER_ID'].')';
					else $dataGroup[$groupnum-1]['GROUP_MEMBER'] .= ', '.$row['USER_FULLNAME'].' ('.$row['USER_ID'].')';

				}
			}

			if($specialGroupMember){
				$temparr = array("GROUP_NAME"=>"Special Group","GROUP_MEMBER"=>$specialGroupMember);
				array_push($dataGroup,$temparr);
			}
		}


		$selectBoundary = $this->_db->select()
									->from(array('A'=>'M_APP_BOUNDARY'))
									->join(array('B'=>'M_APP_BOUNDARY_GROUP'),'A.BOUNDARY_ID = B.BOUNDARY_ID',array('GROUP_USER_ID'))
									->where('UPPER(CUST_ID) = ?', $cust_id)
									->query()->fetchAll();

		if(count($selectBoundary) > 0){
			$dataBoundary = array();
			foreach ($selectBoundary as $row) {
				list($grouptype,$groupname,$groupnum) = explode("_", $row['GROUP_USER_ID']);
				$groupnum = ltrim($groupnum);
				$ctr = count($dataBoundary);

				if($row['ROW_INDEX'] == $dataBoundary[$ctr-1]['INDEX']){
					if($grouptype != "S")
						$dataBoundary[$ctr-1]['NORMAL_GROUP'] .= ", Group ".$groupnum;
					else
						$dataBoundary[$ctr-1]['SPECIAL_GROUP'] = 1;
				}
				else{
					$dataBoundary[$ctr]['INDEX'] = $row['ROW_INDEX'];
					$dataBoundary[$ctr]['BOUNDARY'] = $row['CCY_BOUNDARY'].' '.Application_Helper_General::displayMoney($row['BOUNDARY_MIN']).' - '.$row['CCY_BOUNDARY'].' '.Application_Helper_General::displayMoney($row['BOUNDARY_MAX']);
					if($grouptype == "S"){
						$dataBoundary[$ctr]['SPECIAL_GROUP'] = 1;
						$dataBoundary[$ctr]['NORMAL_GROUP'] = null;
					}
					else{
						$dataBoundary[$ctr]['NORMAL_GROUP'] = "Group ".$groupnum;
						$dataBoundary[$ctr]['SPECIAL_GROUP'] = null;
					}
				}
			}
		}

		$this->view->cust_id = $cust_id;
		$this->view->groupdata = $dataGroup;
		$this->view->boundarydata = $dataBoundary;

		Application_Helper_General::writeLog('VAGL','View Approver Group List');
	}
}
