<?php

require_once 'Zend/Controller/Action.php';

class Activationcard_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
	   $this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
		
    	$countrycode = $this->language->_('Country Code');
		$activateddate = $this->language->_('Activate Date');
		$debitcard = $this->language->_('Debit Card Account');
		$registerto = $this->language->_('Register to');
		$sugest = $this->language->_('Last Suggested');
		$approved = $this->language->_('Last Approved');
		$status = $this->language->_('Status');
		//$head = $this->language->_('Headquarter');


	    $fields = array(
						'DEBIT_NUMBER'  => array('field' => 'DEBIT_NUMBER',
											      'label' => $debitcard,
											      'sortable' => true),
						'DEBIT_ACTIVATED'  => array('field' => 'DEBIT_ACTIVATED',
											      'label' => $activateddate,
											      'sortable' => true),
						'REG_NUMBER'     => array('field' => 'REG_NUMBER',
											      'label' => $registerto,
											      'sortable' => true),
            	        'DEBIT_SUGGESTED'     => array('field' => 'DEBIT_SUGGESTED',
                                    	            'label' => $sugest,
                                    	            'sortable' => true),
            	        'DEBIT_APPROVED'     => array('field' => 'DEBIT_APPROVED',
                                    	            'label' => $approved,
                                    	            'sortable' => true)

				      );

   		$filterlist = array("BRANCH_CODE","BRANCH_NAME","CITY_NAME","REGION");

		$this->view->filterlist = $filterlist;


		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','BRANCH_NAME');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'BRANCH_CODE'  => array('StringTrim','StripTags'),
							'BRANCH_NAME'  => array('StringTrim','StripTags'),
                		    'CITY_NAME'  => array('StringTrim','StripTags'),
                		    'REGION'  => array('StringTrim','StripTags'),
		);

		$dataParam = array("BRANCH_CODE","BRANCH_NAME","CITY_NAME","REGION");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($this->_request->getParam('wherecol'));die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		//$filter_lg = $this->_getParam('filter');

		$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'T_DEBITCARD'))
							->joinleft(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'));

		

		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf)
		if($filter == true || $csv || $pdf || $this->_request->getParam('print'))
		{
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fcountry_name = $zf_filter->getEscaped('BRANCH_NAME');
			$fcountry_code = $zf_filter->getEscaped('BRANCH_CODE');
			$fregion_name = $zf_filter->getEscaped('REGION');
			$fcity_name = $zf_filter->getEscaped('CITY_NAME');
			if($fcountry_code) $select->where('UPPER(BRANCH_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fcountry_code).'%'));
	        if($fcountry_name) $select->where('UPPER(BRANCH_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fcountry_name).'%'));
	        if($fregion_name) $select->where('UPPER(REGION_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fregion_name).'%'));
	        if($fcity_name) $select->where('UPPER(CITY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fcity_name).'%'));

			$this->view->branch_name = $fcountry_name;
			$this->view->city_name = $fregion_name;
			$this->view->region_name = $fcity_name;

	        if($fheadquarter == 'Y'){
	        	$this->view->yhead = 'selected';
	        }else if($fheadquarter == 'N'){
	        	$this->view->yhead = 'selected';
	        }else{
	        	$this->view->nn = 'selected';
	        }
		}
		$select->where('A.IS_ACTIVATED = ? ','1');

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);

		$select = $this->_db->fetchall($select);

		if($csv || $pdf){
			foreach ($select as $key => $value) {
				unset($select[$key]['ID']);
			}
		}
//         print_r($select);die;
		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {
				$this->_helper->download->csv($header,$select,null,'Branch Bank List');
				Application_Helper_General::writeLog('COLS','Download CSV Branch Bank List');
		}
		else if($pdf)
		{

				$this->_helper->download->pdf($header,$select,null,'Branch Bank List');
				Application_Helper_General::writeLog('COLS','Download PDF Branch Bank List');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Branch Bank List', 'data_header' => $fields));
       	}
		else
		{
				Application_Helper_General::writeLog('COLS','View Branch Bank List');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}

        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }

	}

}
