<?php
class Practice_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initView()
    {
        $router = new Zend_Controller_Router_Rewrite();
        $request =  new Zend_Controller_Request_Http();
        $request->setParam('test', 'coba');
        $router->route($request);

        Zend_Registry::set('paramsBoot', $request->getParams());

        $module = $request->getModuleName();
        $contoller = $request->getControllerName();
        $action = $request->getActionName();

        // $module = ($module == 'default') ? 'home' : $module;

        $view = new Zend_View();

        $scandir = scandir(LIBRARY_PATH . "/configs/");

        $allconfig = [];
        foreach ($scandir as $key => $value) {
            if (pathinfo($value, PATHINFO_EXTENSION) != 'ini') continue;
            $cfgCoba = new Zend_Config_Ini(LIBRARY_PATH . "/configs/" . $value, APPLICATION_ENV, ['allowModifications' => false]);
            $cfgAcl = $cfgCoba->toArray();
            $allconfig = array_merge($allconfig, $cfgAcl);
        }

        Zend_Registry::set('wew', $allconfig);
        // $view->headTitle(' : ' . $module);

        return $view;
    }
}
