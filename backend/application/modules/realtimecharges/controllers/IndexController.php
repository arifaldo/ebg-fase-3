<?php


require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';


class realtimecharges_IndexController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $custid = $AESMYSQL->decrypt($this->_getParam('custid'), $password);
	    $custid = (Zend_Validate::is($custid,'Alnum') && Zend_Validate::is($custid,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $custid : null;
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));	
		$select -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $select->query()->FetchAll();
		$this->view->result = $result;
		
		$chargestatus = $result[0]["CUST_CHARGES_STATUS"];
		
		if($chargestatus == 0)
		{
			$this->view->chargestatus = 'Disabled';
		}
		if($chargestatus == 1)
		{
			$this->view->chargestatus = 'Enabled';
		}
		
		$custname = $result[0]['CUST_NAME'];
		
		$selectacc = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectacc -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectacc -> where("A.ACCT_SOURCE NOT LIKE '1'");
		$selectacc -> where("A.ACCT_SOURCE IS NOT NULL");
		
		$resultacc = $selectacc->query()->FetchAll();
		$this->view->resultaccount = $resultacc;
		
		foreach($resultacc as $row)
		{	
			$acctno = $row['ACCT_NO'];
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_WITHIN'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
			$select4 -> where("BUSINESS_TYPE = '1'");
			$result4 = $select4->query()->FetchAll();

			if($select4)
			{
				foreach($result4 as $row2)
				{	
   					$idamt = $row2['ACCT_NO'].'amount';
					$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
					if($amt == null)
					{
						$amt = '0.00';
					}
					$this->view->$idamt = $amt;
				}
			}
		}
		
		Application_Helper_General::writeLog('CHUD','Update company charges');
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		//Zend_Debug::dump($result); die;		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
		$select2 = $this->_db->select()
							->from('T_GLOBAL_CHANGES');
		$select2 -> where("KEY_FIELD LIKE ".$this->_db->quote($custid));
		$select2 -> where("KEY_VALUE LIKE ".$this->_db->quote($custname));
		$select2 -> where("DISPLAY_TABLENAME = 'Set Realtime Charges'");
		$select2 -> where("CHANGES_STATUS = 'WA'");
		$cek = $select2->query()->FetchAll();
		//Zend_Debug::dump($cek); die;
			
		if(!$cek)
		{
			if($submit)
			{
					$error = 0;
					foreach($resultacc as $row)
					{
						$idamt = $row['ACCT_NO'].'amount';
						
   						//Zend_Debug::dump($idamt); die;
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						$temp_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
						$this->view->$idamt = $cekamt;
						
   						//Zend_Debug::dump($cekfrom); die;
   						
						if($cek_angka == false && $cekamt != null)
							{
								$errid = 'err'.$idamt;
								$err = 'Amount value has to be numeric';
								$this->view->$errid = $err;
								$error++;
								//die;
							}	
					}
					//Zend_Debug::dump($custid); die;
					
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
					$info = 'Charges';
					$info2 = 'Set Realtime Charges';
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_WITHIN','TEMP_CHARGES_WITHIN',$custid,$custname,$custid);
					
					foreach($resultacc as $row)
					{
						$idamt = $row['ACCT_NO'].'amount';
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
						if($cekamt == null)
						{
							$amt = '0.00';
						}
							$data= array(
											'CHANGES_ID' 		=> $change_id,
											'CUST_ID' 			=> $custid,
											'ACCT_NO' 			=> $row['ACCT_NO'],
											'AMOUNT' 			=> $amt,
											'BUSINESS_TYPE' 	=> '1',
											'CHARGES_ACCT_NO'	=> $row['ACCT_NO']
										);
							$this->_db->insert('TEMP_CHARGES_WITHIN',$data);										
							//echo 'aaa';die;
							//Zend_Debug::dump($data);
					}
					//die;
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/setcompanycharges/detail/index/custid/'.$this->_getParam('custid'));
						$this->_redirect('/notification/submited/index');
					}
				}
				
				catch(Exception $e)
				{
					Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}		
		}
		}
		if($cek)
		{			
			$docErr = "*No changes allowed for this record while awaiting approval for previous change";	
			$this->view->error = $docErr;
			$this->view->changestatus = "disabled";
		}	
	}
}
