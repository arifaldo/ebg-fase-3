<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';
require_once 'Service/TransferWithin.php';
require_once 'CMD/Payment.php';

function saveLog($logMessage) {
	// $fp = fopen($logFilePath, "a+");
	$message = PHP_EOL.date("Y-m-d H:i:s")." - ".$logMessage;
	echo $message."\n";
	// fwrite($fp, $message);
	// fclose($fp);
}

$flag = realpath(dirname(__FILE__)).'/inprogressBulkMultiCredit'; //path to flag file

$res = 'Execute Multi Credit In Progress';
$x = 0;

echo 'Bulk Multi Credit Executor'.PHP_EOL;

// if (file_exists($flag)) {
// 	echo 'Queue inprogress at '.date('Y-m-d H:i:s').PHP_EOL;
// }else{
	try{
		$fp = fopen($flag, 'w');
		@fclose($fp);
		echo 'Create flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
	}catch(Exception $e){
		$stringFailed = 'Can\'t create flag at: '.date('Y-m-d H:i:s');
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		echo $stringFailed.PHP_EOL;

		die();
	}

	$uid = shell_exec('id -u benny');
	$gid = shell_exec('id -g benny');

	$uid = intval($uid);
	$gid = intval($gid);

	//set user id and group id for running process
	//NOTE: posix_setuid and posix_setgid only works on POSIX compliant OS (does not work on windows)
	if(false === posix_setgid($gid)) {
	    echo "Error. Unable to set gid to ".$gid."!".PHP_EOL; 
	    exit();
	}
	if(false === posix_setuid($uid)) {
	    echo "Error. Unable to set uid to ".$uid."!".PHP_EOL;
	    exit();
	}

	declare(ticks = 1);

	$openProcesses = 0; 
	$procs = array();
	$maxProcs = 2;

	pcntl_signal(SIGCHLD, "childFinished");



	$limit = 2;
	$childCounter = $i = 0;

$continuePslip = true;
while($continuePslip){
		$dbObj = Zend_Db_Table::getDefaultAdapter();
			$listpayroll = array('11','25','31','32');

			$pslipData = $dbObj->select()
				->from	(
					array('P' => 'T_PSLIP'),
					array('*')
				)
				->where('P.PS_STATUS = 8')
				->where('P.SENDFILE_STATUS = 0')
				->where('DATE(P.PS_EFDATE) = DATE(now())')
				->where('P.PS_TYPE NOT IN (?)', $listpayroll) //do not execute payroll
				->order('P.PS_UPDATED ASC')
				// echo $pslipData;die;
				->query()->fetchAll();
		$dbObj->closeConnection();
		unset($dbObj);
		if(!empty($pslipData))
		{
			foreach($pslipData as $pslip){
				$dbObj = Zend_Db_Table::getDefaultAdapter();
				$query = $dbObj->select()
								->from(array('TT' => 'T_TRANSACTION'), array('*'))
								->joinLeft(array('TP'=>'T_PSLIP'), 'TP.PS_NUMBER = TT.PS_NUMBER', array('CUST_ID','USER_ID' => 'PS_CREATEDBY'))
								->joinLeft(array('MB'=>'M_BENEFICIARY'), 'MB.BENEFICIARY_ID = TT.BENEFICIARY_ID', array('BENEFICIARY_ADDRESS'))
								->where ('TT.PS_NUMBER = ?', $pslip['PS_NUMBER'])
								->where ('TT.ISTRX != ?', '1')		
								->where('RIGHT(TT.TRANSACTION_ID, 1) = ?', '2')
								->limit($limit);
					// echo $query;
				$x = 0;
				$result = array();
				$start = microtime(true);
				
				$setFlag= updateFlagData($query);
				
				$trx = getData($query);
				saveLog("START LOOP $trx");
				while($trx){
					foreach ($trx as $key => $val){
						$pid = pcntl_fork();
						if($pid == -1) {
							//fork failed, no child is created
							$message = $childCounter.'Fork Failed : '.$functionCode;
							throw new Exception($message);
						} elseif($pid == 0) {
							saveLog("Start Child Process ");
							$x ++;
							if (!(empty($val['SOURCE_ACCOUNT'])) && !(empty($val['BENEFICIARY_ACCOUNT']))){
								saveLog("Create Payment OBJ");
								$paymentObj = new Payment($val['PS_NUMBER'], $val['CUST_ID'], $val['USER_ID']);
								
								if ($val['TRANSFER_TYPE'] == "0"){
									// echo '<pre>'
									// var_dump($val);die;
									$return = $paymentObj->sendTransferSingleWithin($pslip, $val);
								}

								if ($val['TRANSFER_TYPE'] == "1" || $val['TRANSFER_TYPE'] == "2")
									$return = $paymentObj->sendTransferSingleDomestic($pslip, $val);

								saveLog("Merging $result");
								$result = array_merge_recursive($result, array('TRX' => $return['TRX']));
									// var_dump($result);
							}
						}else {
							$procs[] = $pid; // add the PID to the list
							++$openProcesses;
							saveLog('Current process(es) : '.$openProcesses);
							if ($openProcesses >= $maxProcs) {
								pcntl_wait($status);     
							}                           
						}
					}
					$trx = getData($query);
				}

				saveLog("END LOOP $trx");
				saveLog("END $x :".$x);
				var_dump($result);
				if ($result && is_array($result) && count($result)){
					saveLog("Update Trx to Completed");
					$paymentObj->setPaymentCompleted($trx, $result);
				}
			}
		}else{
			$continuePslip = false;
			saveLog("No Data to be Process");
		}

		$time_elapsed_secs = microtime(true) - $start;
		echo 'Time elapsed : '.$time_elapsed_secs.PHP_EOL ;
}

	try{
		unlink($flag);
		echo 'Delete flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
	}catch(Exception $e){
		$stringFailed = 'Can\'t delete flag at: '.date('Y-m-d H:i:s');
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		echo $stringFailed.PHP_EOL;

		die();
	}

function getData($query){
	$trx = false;
	try {
		$dbObj = Zend_Db_Table::getDefaultAdapter();
			$trx = $query->query()->fetchAll();
		$dbObj->closeConnection();
		unset($dbObj);
	} catch (Exception $e) {
		saveLog("Error : Load Transaction Data");
		die;
	}
	
	return $trx;
}

function updateFlagData($query){
	$trx = false;
	$succes = false;
	try {
		$dbObj = Zend_Db_Table::getDefaultAdapter();
			$trx = $query->query()->fetchAll();
			
			if(!empty($trx))
			{
				foreach ($trx as $key => $val){
					$datatrx = array ('ISTRX' => '1');
					$wheretrx['TRANSACTION_ID = ?'] = $val['TRANSACTION_ID'];
					$db->update('T_TRANSACTION',$datatrx,$wheretrx);
				}
			}

			$succes = TRUE;
			
		$dbObj->closeConnection();
		unset($dbObj);
	} catch (Exception $e) {
		saveLog("Error : Flag Transaction Data");
		die;
	}
	
	return $succes;;
}

function childFinished($signo) {

    global $openProcesses, $procs;

    // Check each process to see if it's still running
    // If not, remove it and decrement the count
    foreach ($procs as $key => $pid) if (posix_getpgid($pid) === false) {
        unset($procs[$key]);
        $openProcesses--;
    }

}

function signalHandler($signal) {
    global $killed;
    
	$message = "Got signal ".$signal;
	saveLog($message);
    switch($signal) {
       case SIGTERM:
            //just update flag, so loop will terminate
            $killed = true;
            return;
			break;
	   case SIGINT:
            //just update flag, so loop will terminate
            $killed = true;
            return;
			break;	
	   case SIGHUP:
			//do nothing
			return;
			break;
	   case SIGQUIT:
			//do nothing
			return;
			break;
	   //default:
    }
}
// }

echo 'AFFECTED ROW(S): '.$res.'('.$x.')'.PHP_EOL;
Application_Helper_General::cronLog(basename(__FILE__), $res, '1');

?>