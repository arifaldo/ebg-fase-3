<?php
class display_Model_Paymentreport
{
	protected $_db;
	
    // constructor
	public function __construct()
	{	
		$config = Zend_Registry::get('config');
		$this->_masterglobalstatus 	= $config['master']['globalstatus'];
		$this->_beneftype 			= $config['account']['beneftype'];
		$this->_paymentStatus 		= $config["payment"]["status"]; 
		$this->_paymentType 		= $config["payment"]["type"]; 
		$this->_transferType 		= $config["transfer"]["type"]; 
		$this->_transferStatus 		= $config["transfer"]["status"]; 
		$this->_historystatus 		= $config["history"]["status"]; 
		$this->_db 					= Zend_Db_Table::getDefaultAdapter();
	}
	
	
	public function getPaymentDetail($psNumber)
	{
		$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
		$transtypesarr = array_combine(array_values($this->_transferType['code']),array_values($this->_transferType['desc']));
		$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
	
		$casePayType = "(CASE P.PS_TYPE ";
		foreach($paytypesarr as $key=>$val)
		{
			if($key==18){
				$val='Disbursement';
			}
			$casePayType .= " WHEN ".$key." THEN '".$val."'";
		}
		$casePayType .= " WHEN '110' OR '120' THEN PS_CATEGORY";
		$casePayType .= " ELSE 'N/A' END)";
			
		$casePayStatus = "(CASE P.PS_STATUS ";
		foreach($paystatusarr as $key=>$val)
		{
			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
		}
		$casePayStatus .= " ELSE 'N/A' END)";
			
		$caseTransType = "(CASE T.TRANSFER_TYPE ";
		foreach($transtypesarr as $key=>$val)
		{
			$caseTransType .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseTransType .= " ELSE 'N/A' END)";
			
		$caseTransType = "(CASE T.TRANSFER_TYPE ";
		foreach($transtypesarr as $key=>$val)
		{
			$caseTransType .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseTransType .= " ELSE 'N/A' END)";
			
		$caseTransStatus = "(CASE T.TRA_STATUS ";
		foreach($transstatusarr as $key=>$val)
		{
			$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseTransStatus .= " ELSE 'N/A' END)";
			
		$select	= $this->_db->select()
		->from(	array('P' => 'T_PSLIP'),array())
		->join(	array('U' => 'M_CUSTOMER'), 'P.CUST_ID = U.CUST_ID', array())
		->joinleft(	array('S' => 'M_CUSTOMER_ACCT'), 'S.CUST_ID = U.CUST_ID', array())
		->joinleft(	array('B' => 'T_PERIODIC'), 'P.PS_PERIODIC = B.PS_PERIODIC', array('PS_PERIODIC_NUMBER'))
		->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'P.*','U.*','T.*','S.*',
				'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," (",T.SOURCE_ACCOUNT_CCY,") ",T.SOURCE_ACCOUNT_NAME)'),
				'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," (",T.BENEFICIARY_ACCOUNT_CCY,") ",T.BENEFICIARY_ACCOUNT_NAME)'),
				'PS_STATUS'	=> $casePayStatus,
				'PS_TYPE' => $casePayType,
				'TRANSFER_TYPE' => $caseTransType,
				'TRA_TYPE' => 'T.TRANSFER_TYPE',
				'CEK_PS_TYPE' => 'P.PS_TYPE',
				'TRANSFER_STATUS' => $caseTransStatus,
				'BALANCE_TYPE'=>new Zend_Db_Expr("(SELECT BALANCE_TYPE FROM T_PERIODIC_DETAIL WHERE PS_PERIODIC = P.PS_PERIODIC limit 1)")
		))
				->where("T.TRANSACTION_ID = ? ", $psNumber)
				->order("T.REEXECUTE_INDEX DESC")
				->limit(1);
				// ->group('P.PS_NUMBER');
				// echo $select;die;
	// print_r($select);die;
		return $this->_db->fetchAll($select);
	}
	
	
	public function getHistory($psNumber)
	{
		$historyarr = array_combine(array_values($this->_historystatus['code']),array_values($this->_historystatus['desc']));
		
		$caseHistory = "(CASE P.HISTORY_STATUS ";
		foreach($historyarr as $key=>$val)
		{
			$caseHistory .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseHistory .= " ELSE 'N/A' END)";
		 
		$select	= $this->_db->select()
		->from(	array('P' => 'T_PSLIP_HISTORY'),array('*','HISTORY_STATUS' => $caseHistory))
		->where("P.PS_NUMBER = ? ", $psNumber);
		$select -> order('DATE_TIME DESC');
		return $this->_db->fetchAll($select);
	}
	
	
	public function getHistoryAprove($psNumber)
	{
		$historyarr = array_combine(array_values($this->_historystatus['code']),array_values($this->_historystatus['desc']));
			
		$caseHistory = "(CASE P.HISTORY_STATUS ";
		foreach($historyarr as $key=>$val)
		{
			$caseHistory .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseHistory .= " ELSE 'N/A' END)";
			
		$select	= $this->_db->select()
		->from(	array('P' => 'T_PSLIP_HISTORY'),array('*','HISTORY_STATUS' => $caseHistory))
		->where("P.PS_NUMBER = ? ", $psNumber)
		
		->where("P.HISTORY_STATUS = ? ", '2');
		
		$select -> order('DATE_TIME DESC');
// 		print_r($select->query());die;
		return $this->_db->fetchAll($select);
	}
	
	public function getCountry($cCode)
	{
		$select	= $this->_db->select()
		->from(	array('P' => 'M_COUNTRY'),array('COUNTRY_NAME'))
		->where("P.COUNTRY_CODE = ? ", $cCode);
		return $this->_db->fetchOne($select);
	}
 
    
	public function getPslipDetail($psNumber)
    {
    	$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP_DETAIL'),array('*'))
						->where("P.PS_NUMBER = ? ", $psNumber);
		return $this->_db->fetchAll($select);
    }
    
 	
}