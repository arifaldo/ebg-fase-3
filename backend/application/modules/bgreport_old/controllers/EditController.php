<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgreport_EditController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        // $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $bgnumb = $this->_getParam('bgnumb');


        if (!empty($bgnumb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->where('A.BG_REG_NUMBER = ?', $bgnumb)
                ->query()->fetchAll();

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->where('A.BG_NUMBER = ?', $bgnumb)
                ->query()->fetchAll();
            //$datas = $this->_request->getParams();
            // var_dump($datas);

            $dataselect = $bgdata['0'];
            $selectcomp = $this->_db->select()
                ->from(array('A' => 'M_CUSTOMER'), array('*'))
                ->where('A.CUST_ID = ?', $dataselect['CUST_ID'])
                ->query()->fetchAll();

            $this->view->compinfo = $selectcomp[0];


            if (!empty($bgdata)) {
                // var_dump($bgdata);
                $data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = $data['TIME_PERIOD_START']; // Application_Helper_General::convertDate($data['TIME_PERIOD_START'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
                $this->view->updateEnd = $data['TIME_PERIOD_END']; //Application_Helper_General::convertDate($data['TIME_PERIOD_END'],$this->view->viewDateFormat,$this->view->defaultDateFormat);

                /*$arrStatus = array('1' => 'Waiting for review',
                            '2' => 'Waiting for approve',
                            '3' => 'Waiting to release',
                            '4' => 'Waiting for bank approval',
                            '5' => 'Issued',
                            '6' => 'Expired',
                            '7' => 'Canceled',
                            '8' => 'Claimed by applicant',
                            '9' => 'Claimed by recipient', 
                            '10' => 'Request Repair',
                            '11' => 'Reject',
                            '12' => 'Suspend'
                          );
         
                        $this->view->arrStatus = $arrStatus;*/

                $config            = Zend_Registry::get('config');
                $BgType     = $config["bg"]["status"]["desc"];
                $BgCode     = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));
                $this->view->arrStatus = $arrStatus;


                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                $this->view->bankFormatVal = $data['BG_FORMAT'];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];
                $this->view->languageVal = $data['BG_LANGUAGE'];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->CUST_ID = $selectcomp[0]['CUST_ID'];
                $this->view->ACCT_NO = $data['ACCT_NO'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];

                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];


                $CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);
                $m_param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                // $param = array('CCY_IN' => 'IDR');
                $m_AccArr = $CustomerUser->getAccounts($m_param);
                // var_dump($AccArr);

                if (!empty($m_AccArr)) {
                    $this->view->src_name = $m_AccArr['0']['ACCT_NAME'];

                    foreach ($m_AccArr as $i => $value) {

                        $m_AccArr[$i]['ACCT_BANK'] = $this->_bankName;
                    }
                }
                $this->view->m_AccArr = $m_AccArr;

                $param = array('CCY_IN' => 'IDR');
                $AccArr = $CustomerUser->getAccounts($param);

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];

                    foreach ($AccArr as $i => $value) {

                        $AccArr[$i]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArr = $AccArr;


                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Date') {
                                $this->view->paDate =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);


                $back = $this->_getParam('back');
                if ($back) {
                    $this->_redirect('/bgreport');
                }
                //echo '<pre>';
                //var_dump($back);die;

            }
        }

        $back = $this->_getParam('back');
        if ($back) {
            $this->_redirect('/bgreport');
        }
        //echo '<pre>';
        //var_dump($back);die;

        $maxFileSize = "1024"; //"1024000"; //1024
        //~ $fileExt = "csv,dbf";
        $fileExt = "jpeg,jpg,pdf";
        if ($this->_request->isPost()) {
            $attahmentDestination   = UPLOAD_PATH . '/document/submit/';
            $errorRemark            = null;
            $adapter                = new Zend_File_Transfer_Adapter_Http();
            $data = $this->_request->getParams();

            $sourceFileName = trim($adapter->getFileName());

            if ($sourceFileName == null) {
                //die('here');
                $sourceFileName = null;
                $fileType = null;

                // ---------- update -----------------------------------------------------------
                $usage = '';
                foreach ($data['chkpurpose'] as $val) {
                    $usage .= $val . ',';
                }
                $usage = substr($usage, 0, -1);

                try {
                    if ($data['bank_amount'] == '') {
                        $data['bank_amount'] = 0;
                    }

                    if ($data['amount'] == '') {
                        $data['amount'] = 0;
                    }
                    $this->_db->beginTransaction();
                    $insertArr = array(
                        'CUST_ID'               => $data['custid'],
                        'BG_STATUS'             => 10,
                        'BG_SUBJECT'            => $data['subject'],
                        'GUARANTEE_TRANSACTION'         => $data['comment'],
                        'BG_AMOUNT'           => Application_Helper_General::convertDisplayMoney($data['bank_amount']),
                        'TIME_PERIOD_START'         => $data['updatedate'][0],
                        'TIME_PERIOD_END'           => $data['updatedate'][1],
                        'COUNTER_WARRANTY_TYPE'         => $data['warranty_type'],
                        'COUNTER_WARRANTY_ACCT_NO'          => $data['account_number'],
                        'COUNTER_WARRANTY_ACCT_NAME'            => $data['account_name'],
                        'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney($data['amount']),
                        'FEE_CHARGE_TO'         => $data['acct'],
                        'BG_FORMAT'         => $data['bankFormat'],
                        'BG_LANGUAGE'           => $data['language'],
                        'USAGE_PURPOSE'         => $usage,
                        'BG_CREATED'    => new Zend_Db_Expr('now()'),
                        'BG_CREATEDBY'      => $this->_userIdLogin
                    );


                    $where = array('BG_NUMBER = ?' => $bgnumb);
                    $this->_db->update('T_BANK_GUARANTEE', $insertArr, $where);

                    if ($data['warranty_type'] == '2') {
                        if (!empty($data['owner1'])) {
                            $arrDetail = array(
                                'PS_FIELDNAME' => 'Plafond Owner 1',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['owner1']
                            );

                            $PS_FIELDNAME = "Plafond Owner 1";
                            $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);
                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);

                            $arrDetail = array(
                                'PS_FIELDNAME' => 'Amount Owner 1',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['amountowner1']
                            );

                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);
                        }

                        if (!empty($data['owner2'])) {
                            $arrDetail = array(
                                'PS_FIELDNAME' => 'Plafond Owner 2',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['owner2']
                            );



                            $PS_FIELDNAME = "Plafond Owner 2";
                            $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);
                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);


                            $arrDetail = array(
                                'PS_FIELDNAME' => 'Amount Owner 2',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['amountowner2']
                            );
                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);
                        }

                        if (!empty($data['owner3'])) {
                            $arrDetail = array(
                                'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                'CUST_ID' => $insertArr['CUST_ID'],
                                'USER_ID' =>  $this->_userIdLogin,
                                'PS_FIELDNAME' => 'Plafond Owner 3',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['owner3']
                            );
                            $PS_FIELDNAME = "Plafond Owner 3";
                            $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);

                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);



                            $arrDetail = array(
                                'PS_FIELDNAME' => 'Amount Owner 3',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['amountowner3']
                            );
                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);
                        }
                    }

                    if ($data['warranty_type'] == '3') {
                        $arrDetail = array(
                            'PS_FIELDNAME' => 'Insurance Name',
                            'PS_FIELDTYPE' => 1,
                            'PS_FIELDVALUE' => $data['insuranceName']
                        );

                        $PS_FIELDNAME = "Insurance Name";
                        $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);

                        $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);



                        $arrDetail = array(
                            'PS_FIELDNAME' => 'Principal Agreement',
                            'PS_FIELDTYPE' => 1,
                            'PS_FIELDVALUE' => $data['PrincipalAgreement']
                        );

                        $PS_FIELDNAME = "Principal Agreement";
                        $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);

                        $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);

                        $arrDetail = array(
                            'PS_FIELDNAME' => 'Principal Agreement Date',
                            'PS_FIELDTYPE' => 1,
                            'PS_FIELDVALUE' => $data['paDate']
                        );

                        $PS_FIELDNAME = "Principal Agreement Date";
                        $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);

                        $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);




                        $arrDetail = array(
                            'PS_FIELDNAME' => 'Amount',
                            'PS_FIELDTYPE' => 1,
                            'PS_FIELDVALUE' => $data['insurance_amount']
                        );

                        $PS_FIELDNAME = "Amount";
                        $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);
                        $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);
                    }



                    $historyInsert = array(
                        'DATE_TIME'         => new Zend_Db_Expr("now()"),
                        'BG_REG_NUMBER'         => $bgnumb,
                        'CUST_ID'           => $data['custid'],
                        'USER_LOGIN'        => $this->_userIdLogin,
                        'HISTORY_STATUS'    => 10
                    );

                    $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                    $this->_db->commit();

                    //Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);
                    $this->setbackURL('/' . $this->_request->getModuleName() . '/');
                    $this->_redirect('/notification/success');
                } catch (Exception $e) {
                    var_dump($e);
                    die;
                    $this->_db->rollBack();
                    //Application_Log_GeneralLog::technicalLog($e);
                }
                //------------ end update --------
            } else {

                $sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
                if ($_FILES["uploadSource"]["type"]) {
                    $adapter->setDestination($attahmentDestination);
                    $fileType               = $adapter->getMimeType();
                    $size                   = $_FILES["document"]["size"];
                } else {
                    $fileType = null;
                    $size = null;
                }
            }
            $paramsName['sourceFileName']   = $sourceFileName;
            $fileTypeMessage = explode('/', $fileType);
            if (isset($fileTypeMessage[1])) {
                $fileType =  $fileTypeMessage[1];
                $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                $extensionValidator->setMessage("Extension file must be *.jpg /.jpeg / .pdf");

                $size           = number_format($size);
                $sizeTampil     = $size / 1000;
                //$sizeValidator    = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                // $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");

                $adapter->setValidators(array($extensionValidator));

                if ($adapter->isValid()) {
                    $date = date("dmy");
                    $time = date("his");

                    $newFileName = $date . "_" . $time . "_" . $data['custid'] . "_" . $sourceFileName;
                    $adapter->addFilter('Rename', $newFileName);

                    if ($adapter->receive()) {


                        $filedata = $attahmentDestination . $newFileName;
                        // var_dump($filedata);
                        if (file_exists($filedata)) {
                            exec("clamdscan --fdpass " . $filedata, $out, $int);

                            // if()
                            // var_dump($int);
                            // var_dump($out);
                            // die;
                            if ($int == 1) {

                                unlink($filedata);
                                //die('here');
                                $this->view->error = true;
                                $errors = array(array('FileType' => 'Error: An infected file. Please upload other file'));
                                $this->view->errorMsg = $errors;
                            } else {



                                // ---------- update -----------------------------------------------------------
                                $usage = '';
                                foreach ($data['chkpurpose'] as $val) {
                                    $usage .= $val . ',';
                                }
                                $usage = substr($usage, 0, -1);

                                try {
                                    if ($data['bank_amount'] == '') {
                                        $data['bank_amount'] = 0;
                                    }

                                    if ($data['amount'] == '') {
                                        $data['amount'] = 0;
                                    }
                                    $this->_db->beginTransaction();
                                    $insertArr = array(
                                        'CUST_ID'               => $data['custid'],
                                        'BG_STATUS'             => 10,
                                        'BG_SUBJECT'            => $data['subject'],
                                        'GUARANTEE_TRANSACTION'         => $data['comment'],
                                        'FILE'          => $newFileName,
                                        'BG_AMOUNT'           => Application_Helper_General::convertDisplayMoney($data['bank_amount']),
                                        'TIME_PERIOD_START'         => $data['updatedate'][0],
                                        'TIME_PERIOD_END'           => $data['updatedate'][1],
                                        'COUNTER_WARRANTY_TYPE'         => $data['warranty_type'],
                                        'COUNTER_WARRANTY_ACCT_NO'          => $data['account_number'],
                                        'COUNTER_WARRANTY_ACCT_NAME'            => $data['account_name'],
                                        'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney($data['amount']),
                                        'FEE_CHARGE_TO'         => $data['acct'],
                                        'BG_FORMAT'         => $data['bankFormat'],
                                        'BG_LANGUAGE'           => $data['language'],
                                        'USAGE_PURPOSE'         => $usage,
                                        'BG_CREATED'    => new Zend_Db_Expr('now()'),
                                        'BG_CREATEDBY'      => $this->_userIdLogin
                                    );


                                    $where = array('BG_REG_NUMBER = ?' => $bgnumb);
                                    $this->_db->update('T_BANK_GUARANTEE', $insertArr, $where);

                                    if ($data['warranty_type'] == '2') {
                                        if (!empty($data['owner1'])) {
                                            $arrDetail = array(
                                                'PS_FIELDNAME' => 'Plafond Owner 1',
                                                'PS_FIELDTYPE' => 1,
                                                'PS_FIELDVALUE' => $data['owner1']
                                            );

                                            $PS_FIELDNAME = "Plafond Owner 1";
                                            $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);
                                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);

                                            $arrDetail = array(
                                                'PS_FIELDNAME' => 'Amount Owner 1',
                                                'PS_FIELDTYPE' => 1,
                                                'PS_FIELDVALUE' => $data['amountowner1']
                                            );

                                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);
                                        }

                                        if (!empty($data['owner2'])) {
                                            $arrDetail = array(
                                                'PS_FIELDNAME' => 'Plafond Owner 2',
                                                'PS_FIELDTYPE' => 1,
                                                'PS_FIELDVALUE' => $data['owner2']
                                            );



                                            $PS_FIELDNAME = "Plafond Owner 2";
                                            $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);
                                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);


                                            $arrDetail = array(
                                                'PS_FIELDNAME' => 'Amount Owner 2',
                                                'PS_FIELDTYPE' => 1,
                                                'PS_FIELDVALUE' => $data['amountowner2']
                                            );
                                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);
                                        }

                                        if (!empty($data['owner3'])) {
                                            $arrDetail = array(
                                                'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                                'CUST_ID' => $insertArr['CUST_ID'],
                                                'USER_ID' =>  $this->_userIdLogin,
                                                'PS_FIELDNAME' => 'Plafond Owner 3',
                                                'PS_FIELDTYPE' => 1,
                                                'PS_FIELDVALUE' => $data['owner3']
                                            );
                                            $PS_FIELDNAME = "Plafond Owner 3";
                                            $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);

                                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);



                                            $arrDetail = array(
                                                'PS_FIELDNAME' => 'Amount Owner 3',
                                                'PS_FIELDTYPE' => 1,
                                                'PS_FIELDVALUE' => $data['amountowner3']
                                            );
                                            $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);
                                        }
                                    }

                                    if ($data['warranty_type'] == '3') {
                                        $arrDetail = array(
                                            'PS_FIELDNAME' => 'Insurance Name',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $data['insuranceName']
                                        );

                                        $PS_FIELDNAME = "Insurance Name";
                                        $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);

                                        $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);



                                        $arrDetail = array(
                                            'PS_FIELDNAME' => 'Principal Agreement',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $data['PrincipalAgreement']
                                        );

                                        $PS_FIELDNAME = "Principal Agreement";
                                        $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);

                                        $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);

                                        $arrDetail = array(
                                            'PS_FIELDNAME' => 'Principal Agreement Date',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $data['paDate']
                                        );

                                        $PS_FIELDNAME = "Principal Agreement Date";
                                        $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);

                                        $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);




                                        $arrDetail = array(
                                            'PS_FIELDNAME' => 'Amount',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $data['insurance_amount']
                                        );

                                        $PS_FIELDNAME = "Amount";
                                        $where = array('BG_NUMBER = ?' => $bgnumb, 'PS_FIELDNAME = ?' => $PS_FIELDNAME);
                                        $this->_db->update('T_BANK_GUARANTEE_DETAIL', $arrDetail, $where);
                                    }



                                    $historyInsert = array(
                                        'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                        'BG_REG_NUMBER'         => $bgnumb,
                                        'CUST_ID'           => $data['custid'],
                                        'USER_LOGIN'        => $this->_userIdLogin,
                                        'HISTORY_STATUS'    => 10
                                    );

                                    $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                                    $this->_db->commit();

                                    //Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);
                                    $this->setbackURL('/' . $this->_request->getModuleName() . '/');
                                    $this->_redirect('/notification/success');
                                } catch (Exception $e) {
                                    var_dump($e);
                                    die;
                                    $this->_db->rollBack();
                                    //Application_Log_GeneralLog::technicalLog($e);
                                }
                                //------------ end update --------


                            }
                        }
                    }
                } else {
                    $this->view->error = true;
                    $errors = array($adapter->getMessages());
                    $this->view->errorMsg = $errors;
                }
            } else {
                /*$sessionNamespace = new Zend_Session_Namespace('eform');
                                    $sessionNamespace->content = $datas;
                                    $this->_redirect('/bgreport/edit/confirm');*/
            }
        }
    }
}
