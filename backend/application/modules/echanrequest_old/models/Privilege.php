<?php

/**
 * privilege mapping for dual control
 * 
 * @author 
 * @version 
 */
class Echanrequest_Model_Privilege  {
	
	protected $_privilegeMap = array(
									'APOR' => 'registerechannel'
						);
						
						
	protected $_privilegeApproveMap = array(
									'APOR' => 'registerechannel'
						);
						
	protected $_privilegeRepairMap = array();									
	protected $_privilegeRequestRepairMap = array();
	protected $_privilegeRejectMap = array();
						
	public function __construct(){
		$this->_privilegeRepairMap = $this->_privilegeRepairMap;
		$this->_privilegeRequestRepairMap = $this->_privilegeApproveMap;
		
		unset($this->_privilegeRequestRepairMap['ACCA']);
		$this->_privilegeRejectMap = $this->_privilegeApproveMap;
	}					

	public function getAuthorizeModule() {
			$myPrivilege =  array_flip(Zend_Registry::get('privilege')); 
			$authorizeModuleArr = array_intersect_key($this->_privilegeMap, $myPrivilege);
			return $authorizeModuleArr;
	}
	
	
	public function isAuthorizeApprove($moduleName) {
			$myPrivilege =  array_flip(Zend_Registry::get('privilege')); 
			$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
			$returnValue = false;
			if(is_array($authorizeModuleArr)){
				$newArr = array_flip($authorizeModuleArr);

				if (array_key_exists('CHCA', $authorizeModuleArr)){
					$newArr['adminfeeaccount'] = 'CHCA';
					$newArr['adminfeecompany'] = 'CHCA';
					$newArr['chargesdomestic'] = 'CHCA';
					$newArr['disbursementcharges'] = 'CHCA';
					$newArr['monthlyaccount'] = 'CHCA';
					$newArr['monthlycompany'] = 'CHCA';
					$newArr['realtimecharges'] = 'CHCA';
					$newArr['settlementcharges'] = 'CHCA';
				}

				if(array_key_exists('ALFC', $authorizeModuleArr)){
					$newArr['trxcommfee'] = 'ALFC';
					$newArr['setbillerfee'] = 'ALFC';
				}
				
				
				if(array_key_exists($moduleName, $newArr)){
					$returnValue =  true;
				}
			}
			return $returnValue;
	}

	public function isAuthorizeReject($moduleName) {
			$myPrivilege =  array_flip(Zend_Registry::get('privilege')); 
			$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
			$returnValue = false;
			if(is_array($authorizeModuleArr)){
				$newArr = array_flip($authorizeModuleArr);
				if (array_key_exists('CHCA', $authorizeModuleArr)){
					$newArr['adminfeeaccount'] = 'CHCA';
					$newArr['adminfeecompany'] = 'CHCA';
					$newArr['chargesdomestic'] = 'CHCA';
					$newArr['disbursementcharges'] = 'CHCA';
					$newArr['monthlyaccount'] = 'CHCA';
					$newArr['monthlycompany'] = 'CHCA';
					$newArr['realtimecharges'] = 'CHCA';
					$newArr['settlementcharges'] = 'CHCA';
				}

				if(array_key_exists('ALFC', $authorizeModuleArr)){
					$newArr['trxcommfee'] = 'ALFC';
					$newArr['setbillerfee'] = 'ALFC';
				}

				if(array_key_exists($moduleName, $newArr)){
					$returnValue =  true;
				}
			}
			return $returnValue;
	}
	
	public function getPrivilege($moduleName,$action){
		
			$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
			if($action=='Approve'){ 
				$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
			}elseif ($action=='Reject'){
				$authorizeModuleArr = array_intersect_key($this->_privilegeRejectMap, $myPrivilege);
			}
			
			$returnValue = "REQC";
			if(is_array($authorizeModuleArr)){
				$newArr = array_flip($authorizeModuleArr);
				if (array_key_exists('CHCA', $authorizeModuleArr)){
					$newArr['adminfeeaccount'] = 'CHCR';
					$newArr['adminfeecompany'] = 'CHCR';
					$newArr['chargesdomestic'] = 'CHCR';
					$newArr['disbursementcharges'] = 'CHCR';
					$newArr['monthlyaccount'] = 'CHCR';
					$newArr['monthlycompany'] = 'CHCR';
					$newArr['realtimecharges'] = 'CHCR';
					$newArr['settlementcharges'] = 'CHCR';;
				}

				if(array_key_exists('ALFC', $authorizeModuleArr)){
					$newArr['trxcommfee'] = 'ALFC';
					$newArr['setbillerfee'] = 'ALFC';
				}
				
				if(array_key_exists($moduleName, $newArr)){
					$returnValue =  $newArr[$moduleName];
				}
			}
			return $returnValue;
	
	}
	
}
