<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class registerechannel_IndividualController extends Application_Main
{	
	protected $_country;

	public function initController(){
		$model = new registerechannel_Model_Registerechannel();
		$this->_country = Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');
	}

	public function indexAction()
	{
		$model = new registerechannel_Model_Registerechannel();

		$this->view->countryArr = array_merge(array(''=>'--- '.$this->language->_('Any Value').' ---'),$this->_country);	

		if($this->_request->isPost())
		{
			
			$filters = 	array( 
								'user_fullname'		=> 	array('StringTrim','StripTags','HtmlEntities'),
								'user_email'		=> 	array('StringTrim','StripTags','HtmlEntities'),
								'user_mobile_phone'	=> 	array('StringTrim','StripTags','HtmlEntities'),
								'user_address'		=> 	array('StringTrim','StripTags','HtmlEntities'),
								'user_city'			=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
								'user_zip'			=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
								'user_province'		=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
								'user_country'		=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
							);

			$validators = array	(
							'user_fullname' 	=> 	array	(	
																'NotEmpty',
																array('StringLength',array('min'=>1,'max'=>50)),
																'messages' => array	(
																						$this->language->_("Can not be empty"),
																						$this->language->_("Name maximum length is")." 50",
																					)
															),
							'user_email' 		=> 	array	(	
																'NotEmpty',
																 array('StringLength',array('min'=>1,'max'=>128)),
																'messages' => array	(
																						$this->language->_("Can not be empty"),
																						$this->language->_("Email maximum length is")." 128",
																					)
															),
							
							'user_mobile_phone' => 	array	(	
																'NotEmpty',
																'Digits',
																array('StringLength',array('min'=>1,'max'=>20)),
																'messages' => array	(
																						$this->language->_("Can not be empty"),
																						$this->language->_("Invalid format"),
																						$this->language->_("Phone number maximum length is")." 20"
																					)
															),
							'user_address' 		=> 	array	(	
																'NotEmpty',
																array('StringLength',array('min'=>1,'max'=>200)),
																'messages' => array	(
																						$this->language->_("Can not be empty"),
																						$this->language->_("Address maximum length is")." 200",
																					)
														),
							'user_city' 		=> 	array	(	
																'NotEmpty',
																array('StringLength',array('min'=>1,'max'=>50)),
																'messages' => array	(
																						$this->language->_("Can not be empty"),
																						$this->language->_("City maximum length is")." 50",
																					)
															),
							'user_zip' 			=> 	array	(	
																'NotEmpty',
																array('StringLength',array('min'=>1,'max'=>10)),
																'messages' => array	(
																						$this->language->_("Can not be empty"),
																						$this->language->_("Zip code maximum length is")." 10",
																					)
															),
							'user_province' 	=> 	array	(	
																'NotEmpty',
																array('StringLength',array('min'=>1,'max'=>50)),
																'messages' => array	(
																						$this->language->_("Can not be empty"),
																						$this->language->_("State/Province maximum length is")." 50",
																					)
															),
							'user_country' 		=> 	array	(	
																'NotEmpty',
																'messages' => array	(
																						$this->language->_("Please Choose One"),
																					)
															),
			);
			

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

			if($this->_getParam('user_email'))
		    {
				$validate = new validate;
		      	$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('user_email'));
		    }
		    else
		    {
		      	$cek_multiple_email = true;
		    }
			
			if($zf_filter_input->isValid() && $cek_multiple_email == true)
			{
				$param = array();

				foreach($validators as $key => $value){
					$param[$key] = $zf_filter_input->getEscaped($key);
				}

				$sessionNamespace = new Zend_Session_Namespace('firstPage');
				$sessionNamespace->param = $param;
				
				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index');
				$this->_redirect('/registerechannel/individual/account');
			}
			else
			{
				foreach($validators as $key=>$value)
				{
					$this->view->$key = $this->_getParam($key);
				}
				
				$error = $zf_filter_input->getMessages();
										
				foreach($error as $keyRoot => $rowError)
				{
				   foreach($rowError as $errorString)
				   {
				   	  $keyname = "error_" . $keyRoot;
					  $this->view->$keyname = $this->language->_($errorString);
				   }
				}

				if(isSet($cek_multiple_email) && $cek_multiple_email == false) $this->view->error_user_email = $this->language->_('Invalid email format');
			}
		}
		else{
			$sessionNamespace = new Zend_Session_Namespace('firstPage');
			if(isset($sessionNamespace->param)){
				$param = $sessionNamespace->param;
				foreach($param as $key => $val){
					$this->view->$key = $val;
				}
			}
		}
	}

	public function accountAction()
	{
		$model = new registerechannel_Model_Registerechannel();
		$ccyArr = Application_Helper_Array::listArray($model->getCCY(),'CCY_ID','CCY_ID');
		$this->view->ccyArr = array_merge(array(''=>'--- '.$this->language->_('Any Value').' ---'),$ccyArr);

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

		$addParam = $this->_getParam('add');
		$submitParam = $this->_getParam('submit');

		$sessionNamespace = new Zend_Session_Namespace('secondPage');
		if(isset($sessionNamespace->acclist)){
			$acclist = $sessionNamespace->acclist;
		}
		else{
			$acclist = array();
		}

		if($this->_request->isPost()){
			if($addParam){

				$filters = 	array( 
									'acct_no'			=> 	array('StringTrim','StripTags','HtmlEntities'),
									'acct_name'			=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
									'acct_ccy'			=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
								);

				$validators = array	( 
										'acct_no' 		=> 	array	(	
																		'NotEmpty',
																		array('StringLength',array('min'=>1,'max'=>50)),
																		'messages' => array	(
																								$this->language->_("Can not be empty"),
																								$this->language->_("Account number maximum length is")." 50",
																							)
																	),
										'acct_name' 	=> 	array	(	
																		'NotEmpty',
																		array('StringLength',array('min'=>1,'max'=>50)),
																		'messages' => array	(
																								$this->language->_("Can not be empty"),
																								$this->language->_("Name maximum length is")." 50",
																							)
																	),
										'acct_ccy' 		=> 	array	(	
																		'NotEmpty',
																		'messages' => array	(
																								$this->language->_("Please Choose One"),
																							)
															),
									);

				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

				if($zf_filter_input->isValid()){
					if(empty($acclist)){
						$acclist[0]['ACCT_NO'] = $zf_filter_input->getEscaped('acct_no');
						$acclist[0]['ACCT_NAME'] = $zf_filter_input->getEscaped('acct_name');
						$acclist[0]['ACCT_CCY'] = $zf_filter_input->getEscaped('acct_ccy');
					}
					else{
						$acct_no = $zf_filter_input->getEscaped('acct_no');
						foreach($acclist as $key => $val){
							if($val['ACCT_NO'] == $acct_no)
								$exist = 1;
						}

						if(!$exist){
							$ctr = count($acclist);
							$acclist[$ctr]['ACCT_NO'] = $acct_no;
							$acclist[$ctr]['ACCT_NAME'] = $zf_filter_input->getEscaped('acct_name');
							$acclist[$ctr]['ACCT_CCY'] = $zf_filter_input->getEscaped('acct_ccy');
						}
						else{
							$this->view->error = 1;
							$this->view->report_msg = "Account number already added";
						}
					}

					if(!$this->view->error){
						$sessionNamespace = new Zend_Session_Namespace('secondPage');
						$sessionNamespace->acclist = $acclist;
					}
				}
				else{
					foreach($validators as $key=>$value)
					{
						$this->view->$key = $this->_getParam($key);
					}
					
					$error = $zf_filter_input->getMessages();
											
					foreach($error as $keyRoot => $rowError)
					{
					   foreach($rowError as $errorString)
					   {
					   	  $keyname = "error_" . $keyRoot;
						  $this->view->$keyname = $this->language->_($errorString);
					   }
					}
				}
			}
			elseif($submitParam){
				$this->_redirect('/registerechannel/individual/confirm');
			}
		}
		else{
			$delParam = $this->_getParam('delete');

			if($delParam){
				if(empty($acclist)){
					$errMsg = 'There is no account in the list';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errMsg);
				}
				else{
					$filter = new Zend_Filter_StringTrim();
					$filter2 = new Zend_Filter_StripTags();
					$filter3 = new Zend_Filter_HtmlEntities();

					$delParam = $filter->filter($delParam);
					$delParam = $filter2->filter($delParam);
					$delParam = $filter3->filter($delParam);

					
					$acct_no = $delParam;
					$exist = 0;

					foreach($acclist as $key => $val){
						if($val['ACCT_NO'] == $acct_no){
							$exist = 1;
							$delkey = $key;
						}
					}

					if($exist){
						unset($acclist[$key]);

						$errMsg = "Account deleted from the list";

						$sessionNamespace = new Zend_Session_Namespace('secondPage');
						$sessionNamespace->acclist = $acclist;

						$this->_helper->getHelper('FlashMessenger')->addMessage('S');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errMsg);
					}
					else{
						$errMsg = "Account doesn't exist in the list";

						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errMsg);
					}
				}

				$this->_redirect('/registerechannel/individual/account');
			}
		}

		$sessionNamespace = new Zend_Session_Namespace('secondPage');
		if(isset($sessionNamespace->acclist)){
			$acclist = $sessionNamespace->acclist;
			$this->view->acclist = $acclist;
		}

	}

	public function confirmAction()
	{
		$model = new registerechannel_Model_Registerechannel();

		$this->view->countryArr = $this->_country;

		$sessionNamespace = new Zend_Session_Namespace('firstPage');
		if(isset($sessionNamespace->param)){
			$usrdata = $sessionNamespace->param;
			foreach($usrdata as $key => $value){
				$this->view->$key = $value;
			}

			$sessionNamespace2 = new Zend_Session_Namespace('secondPage');
			if(isset($sessionNamespace2->acclist)){
				$acclist = $sessionNamespace2->acclist;
				$this->view->acclist = $acclist;
			}
		}
		else{
			Zend_Session::namespaceUnset('firstPage');
			Zend_Session::namespaceUnset('secondPage');

			$this->view->errnotfound = 1;
			$this->view->errMsg = "Data not found";
		}


		if($this->_request->isPost()){
			$agree = $this->_getParam('agreement');

			if($agree){
				try{
					$insertArr = array();
					foreach($usrdata as $key => $val){
						$insertArr[strtoupper($key)] = $val;
					}

					$typeTitle = "Permata Online (Individual)";
					$changeType = "N";
					$change_id = $this->suggestionWaitingApproval($typeTitle,null,$changeType,null,null,'TEMP_MYONLINE_IND','IND',$insertArr['USER_FULLNAME']);

					$insertArr['CREATED'] = date('Y-m-d H:i:s');
					$insertArr['CREATEDBY'] = 'SYSTEM';
					$insertArr['CHANGES_ID'] = $change_id;

					$id = $model->insertMyOnlineReg('ind',$insertArr);

					if(!empty($acclist)){
						foreach($acclist as $row){
							$insertAcc = array();

							$insertAcc['REG_ID'] = $id;
							$insertAcc['ACCT_NO'] = $row['ACCT_NO'];
							$insertAcc['ACCT_NAME'] = $row['ACCT_NAME'];
							$insertAcc['ACCT_CCY'] = $row['ACCT_CCY'];

							$model->insertMyOnlineIndAcc($insertAcc);
						}
					}

					$this->_redirect('/registerechannel/individual/success');
				}
				catch(Exception $e){
					$this->view->error = 1;
					$this->view->errMsg = "Failed to add data";
				}
			}
			else{
				$this->view->error = 1;
				$this->view->errMsg = "Please check agreement";
			}
		}
	}

	public function successAction()
	{
		Zend_Session::namespaceUnset('firstPage');
		Zend_Session::namespaceUnset('secondPage');
	}
}
