<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class Administrator_SendemailController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('popup');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$userid = $AESMYSQL->decrypt($this->_getParam('userid'), $password);

		$select = $this->_db->select()
			->from(array('A' => 'M_BUSER'), array('BUSER_NAME', 'BUSER_ID', 'BUSER_CLEARTEXT_PASSWORD', 'BUSER_EMAIL', 'BUSER_ISNEW'));
		//$select ->where('BUSER_ID = '.$userid);
		$select->where("BUSER_ID LIKE " . $this->_db->quote($userid));
		$result = $this->_db->fetchRow($select);
		$email = $result['BUSER_EMAIL'];

		// 	$password = strtoupper(substr(base64_decode($result['BUSER_CLEARTEXT_PASSWORD']),4, -4));
		$password = substr(base64_decode($result['BUSER_CLEARTEXT_PASSWORD']), 4, -4);

		//Zend_Debug::dump($result); die;

		$setting = new Settings();

		$templateEmailMasterBankAddress = $setting->getSetting('master_bank_address');
		$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
		$templateEmailMasterBankAppUrl = $setting->getSetting('master_bank_app_url');
		$templateEmailMasterBankCity = $setting->getSetting('master_bank_city');
		$templateEmailMasterBankCountry = $setting->getSetting('master_bank_country');
		$templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
		$templateEmailMasterBankEmail1 = $setting->getSetting('master_bank_email1');
		$templateEmailMasterBankFax = $setting->getSetting('master_bank_fax');
		$templateEmailMasterBankName = $setting->getSetting('master_bank_name');
		$templateEmailMasterBankProvince = $setting->getSetting('master_bank_province');
		$templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
		$templateEmailMasterBankWebsite = $setting->getSetting('master_bank_website');

		if ($result['BUSER_ISNEW'] == 1) {
			$FEmailResetPass = $setting->getSetting('bemailtemplate_newuser');
			$subject = "CMD - New User";
		} else {
			$FEmailResetPass = $setting->getSetting('bemailtemplate_boresetpwd');
			$subject = "Reset Password Information";
		}

		// $template = str_ireplace('[[USER_FULLNAME]]',$result['BUSER_NAME'],$template);
		// 	$template = str_ireplace('[[buser_name]]',$result['BUSER_NAME'],$template);
		// 	$template = str_ireplace('[[buser_login]]',$result['BUSER_ID'],$template);
		// 	$template = str_ireplace('[[buser_cleartext_password]]',$password,$template);
		// 	$template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);

		// $result = Application_Helper_Email::sendEmail($email,$subject,$template);



		$datepass = date('Y-m-d h:i:s', strtotime("+1 days"));
		$str = rand();
		$rand = md5($str);

		$data = array(
			// 'BUSER_PASSWORD' => $encryptedNewPassword,
			'UPDATED' => new Zend_Db_Expr("now()"),
			// 'BUSER_LASTCHANGEPASSWORD' => new Zend_Db_Expr("now()"),
			'BUSER_FAILEDATTEMPT' => 0,
			'BUSER_ISLOGIN' => 0,
			'BUSER_ISLOCKED' => 0,
			'BUSER_LOCKREASON' => '',
			'BUSER_RRESET' => 0,
			'BUSER_RCHANGE' => 1,
			'BUSER_ISREQUIRE_CHANGEPWD' => 0,
			'BUSER_ISNEW' => 0,
			'BUSER_DATEPASS' => $datepass,
			'BUSER_CODE' => $rand
		);

		$where =  array();
		$where['BUSER_ID = ?'] = $userid;
		$this->_db->update('M_BUSER', $data, $where);
		// print_r($data);die;
		$set = new Settings();
		$templateEmailMasterBankAddress = $set->getSetting('master_bank_address');
		$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
		$templateEmailMasterBankAppUrl = $set->getSetting('master_bank_app_url');
		$templateEmailMasterBankCity = $set->getSetting('master_bank_city');
		$templateEmailMasterBankCountry = $set->getSetting('master_bank_country');
		$templateEmailMasterBankEmail = $set->getSetting('master_bank_email');
		$templateEmailMasterBankEmail1 = $set->getSetting('master_bank_email1');
		$templateEmailMasterBankFax = $set->getSetting('master_bank_fax');
		$templateEmailMasterBankName = $set->getSetting('master_bank_name');
		$templateEmailMasterBankProvince = $set->getSetting('master_bank_province');
		$templateEmailMasterBankTelp = $set->getSetting('master_bank_telp');
		$templateEmailMasterBankWapp = $set->getSetting('master_bank_wapp');
		$templateEmailMasterBankWebsite = $set->getSetting('master_bank_website');
		$url_bo = $set->getSetting('url_bo');

		// $user_isnew = $this->_getParam('user_isnew');

		//		$FEmailResetPass = $set->getSetting('femailtemplate_resetpwd');

		// if($user_isnew == 1){
		// 	$FEmailResetPass = $set->getSetting('femailtemplate_newuser');
		// }
		// else{

		// }
		// $FEmailResetPass = $set->getSetting('bemailtemplate_resetpwd');



		$isi = $this->_db->SELECT()
			->FROM('M_BUSER', array('BUSER_ID', 'BUSER_EMAIL', 'BUSER_NAME', 'BUSER_CLEARTEXT_PASSWORD'))
			->WHERE('BUSER_ID = ? ', $userid);
		//->WHERE ('CUST_ID = ? ',$cust_id);
		// echo $isi;die;
		$isi = $this->_db->fetchrow($isi);
		// print_r($isi);die;
		$actual_link = $_SERVER['SERVER_NAME'];
		$key = md5('permataNet92');
		$encrypt = new Crypt_AES();
		// $user = ( $encrypt->encrypt ( $this->_userId ) );
		$user = ($this->sslEnc($userid));
		// $cust = ( $encrypt->encrypt ( $this->_custId ) );
		// print_r($user);die;
		$newPassword = $url_bo . '/pass/index?safetycheck=&code=' . urlencode($rand) . '&user_id=' . urlencode($user);
		// print_r($newPassword);die;
		$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
		$data = array(
			'[[buser_fullname]]' => $isi['BUSER_NAME'],
			'[[buser_name]]' => $isi['BUSER_NAME'],
			'[[comp_accid]]' => 'Backend',
			'[[user_login]]' => $isi['BUSER_ID'],
			'[[buser_email]]' => $isi['BUSER_EMAIL'],
			'[[confirm_link]]' => $newPassword,
			'[[master_bank_name]]' => $templateEmailMasterBankName,
			'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
			'[[master_bank_email]]' => $templateEmailMasterBankEmail,
			'[[exp_date]]'					=> $datenow,
			// '[[user_cleartext_password]]' 	=> $newPassword,
			'[[master_bank_app_name]]'		=> $templateEmailMasterBankAppName,
			'[[master_bank_wapp]]' 		=> $templateEmailMasterBankWapp
		);

		$FEmailResetPass = strtr($FEmailResetPass, $data);

		// echo $FEmailResetPass;die;
		$mainResponse = Application_Helper_Email::sendEmail($isi['BUSER_EMAIL'], $subject, $FEmailResetPass);

		//$msg = "Success";

		$updatesugStatus['BUSER_RPASSWORD_ISEMAILED'] = 1;
		$wheresugStatus['BUSER_ID = ?'] = $userid;
		$query = $this->_db->update("M_BUSER", $updatesugStatus, $wheresugStatus);

		// if($mainResponse == 1)
		// {
		$msg = 'Emailed sent success';
		// }

		//  	if($remainResponsesult == 0)
		// {
		// 	$msg = 'Emailed sent failed';
		// }
		$this->view->msg = $msg;
		//Zend_Debug::dump($template); die;
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('ADRL', 'Email Password Backend User Account (' . $userid . ')');
		}
	}


	public function sslPrm()
	{
		return array("6a1f325be4c0492063e83a8cb2cb9ae7", "IV (optional)", "aes-128-cbc");
	}

	public function sslEnc($msg)
	{
		list($pass, $iv, $method) = $this->sslPrm();
		return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}
}
