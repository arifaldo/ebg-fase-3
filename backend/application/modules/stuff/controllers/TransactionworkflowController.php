<?php

class stuff_TransactionworkflowController extends Application_Main
{
    public function indexAction() 
    { 

		$listActor = array(  'S' => 'Supplier',
										  'B' => 'Buyer',
										  'O' => 'Supplier Or Buyer'
										 );
		
		$listInvoicetype = array('1' => 'Item',
												   '2' => 'Unit',
												   '3' => 'Bulk'
												  );

		$listYesNo = array('N' => 'No',
										'Y' => 'Yes',
										);	

		$listOnOff = array('0' => 'OFF',
									    '1' => 'ON'
									  );	
		
		$listSequence = array(
													'0' => 'After Document Verify',
													'1' => 'Before Document Verify',
												);
		
		$this->view->listSequence = $listSequence;
		$this->view->listActor = $listActor;
		$this->view->listInvoicetype = $listInvoicetype;			
		$this->view->listYesNo = $listYesNo;			
		$this->view->listOnOff = $listOnOff;		
    }
}