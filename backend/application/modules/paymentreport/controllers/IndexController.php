<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class paymentreport_IndexController extends Application_Main 
{
	protected $_moduleDB = 'RTF';
	
	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		
		$conf = Zend_Registry::get('config');
		$this->view->bankName = $conf['app']['bankname'];

		$this->_helper->layout()->setLayout('newlayout');
		//$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$arrTraTypeRaw 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		$arrCompany 	= $this->getCompanyCode();
		
		unset($arrPayStatus[10]);
		unset($arrPayStatus[15]);
		unset($arrPayStatus[16]);
		//print_r($arrPayType);die;		
		//Zend_Debug::dump($arrPayType);
		foreach($arrPayType as $key => $value){
			//if($key != 3){
				$optpaytypeRaw[$key] = $value;
			//}
		}

		$this->view->arrPayType 	= $optpaytypeRaw;
		$this->view->arrPayStatus 	= $arrPayStatus;
		$this->view->arrTraType 	= $arrTraTypeRaw;
		$this->view->arrCompany 	= $arrCompany;
		
		$fields = array	(	
							'companycode'  		=> array(
															'field' => 'companycode',
															'label' => $this->language->_('Company'),
															'sortable' => true
														),
							'payref'  			=> array(
															'field' => 'payref',
															'label' => $this->language->_('Payment Ref').'#',
															'sortable' => true
														),
							'acctsource'  		=> array(
															'field' => 'acctsource',
															'label' => $this->language->_('Source Account'),
															'sortable' => true
														),
							'beneaccount'  		=> array(
															'field' => 'beneaccount',
															'label' => $this->language->_('Beneficiary Account'),
															'sortable' => true
														),
							'numtrx'  			=> array(
															'field' => 'numtrx',
															'label' => $this->language->_('#Trans'),
															'sortable' => true
														),
							'amount'  			=> array(   'field' 	=> 'amount',
															'label' 	=> $this->language->_('Amount'),
															'sortable' => true
														),
							'transferdate'  	=> array(
															'field' => 'transferdate',
															'label' => $this->language->_('Payment Date'),
															'sortable' => true
														),
							'updateddate'  		=> array(
															'field' => 'updateddate',
															'label' => $this->language->_('Updated Date'),
															'sortable' => true
														),
							// 'traceno'  			=> array(
							// 								'field' => 'traceno',
							// 								'label' => 'Trace No',
							// 								'sortable' => true
							// 							),
							
							/*'bookrate'  			=> array(
															'field' => 'bookrate',
															'label' => $this->language->_('Book Rate'),
															'sortable' => true
														),*/

							'paytypes'  			=> array(
															'field' => 'paytypes',
															'label' => $this->language->_('Payment - Trf Type'),
															'sortable' => true
														),
							'paystatus'  		=> array(
															'field' => 'paystatus',
															'label' => $this->language->_('Payment - Trans Status'),
															'sortable' => true
														),
						);

		$filterlist = array('PS_CREATED','PS_UPDATED','PS_TRFDATE','COMP_ID','PAYTYPE','TRFTYPE','PAYSTAT','COMP_NAME','PAY_REF','SOURCE_ACCOUNT','BENEFICIARY_ACCOUNT');
		
		$this->view->filterlist = $filterlist;
		
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');
		
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;
			
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$data = array();
		
		$filterArr = array('COMP_ID'    	=> array('StripTags'),
	                       'COMP_NAME'  		=> array('StripTags','StringTrim','StringToUpper'),
						   
						   'PAY_REF' 			=> array('StripTags','StringTrim','StringToUpper'),
						   						   
						   'PS_TRFDATE' 		=> array('StripTags','StringTrim'),
						   'PS_TRFDATE_END' 		=> array('StripTags','StringTrim'),
						   'PS_CREATED' 		=> array('StripTags','StringTrim'),
						   'PS_CREATED_END' 			=> array('StripTags','StringTrim'),
						   'PS_UPDATED' 		=> array('StripTags','StringTrim'),
						   'PS_UPDATED_END' 			=> array('StripTags','StringTrim'),
						   
						   'SOURCE_ACCOUNT' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'BENEFICIARY_ACCOUNT' 		=> array('StripTags','StringTrim','StringToUpper'),
						  						   
						   'PAYSTAT' 		=> array('StripTags'),
						   'PAYTYPE' 		=> array('StripTags'),
						   'TRFTYPE' 		=> array('StripTags'),
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','COMP_NAME','PAY_REF','PAYSTAT','PAYTYPE','TRFTYPE','SOURCE_ACCOUNT','BENEFICIARY_ACCOUNT');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_CREATED"){
								$order--;
							}
						if($value == "PS_UPDATED"){
								$order--;
							}
						if($value == "PS_TRFDATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}

		if(!empty($this->_request->getParam('updatedate'))){
				$updatearr = $this->_request->getParam('updatedate');
					$dataParamValue['PS_UPDATED'] = $updatearr[0];
					$dataParamValue['PS_UPDATED_END'] = $updatearr[1];
			}

		if(!empty($this->_request->getParam('trfdate'))){
				$transferarr = $this->_request->getParam('trfdate');
					$dataParamValue['PS_TRFDATE'] = $transferarr[0];
					$dataParamValue['PS_TRFDATE_END'] = $transferarr[1];
			}

	/*		var_dump($this->_request->getParam('updateddate'));
			var_dump($dataParamValue);*/
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
		
						'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($arrCompany)))),
						'COMP_NAME' 	=> array(),	
						
						'PAY_REF' 	=> array(),	
												
						'PS_TRFDATE' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_TRFDATE_END'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_CREATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_CREATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_UPDATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_UPDATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'SOURCE_ACCOUNT' 	=> array(),	
						'BENEFICIARY_ACCOUNT' 	=> array(),	
						
						'PAYSTAT' => array(array('InArray', array('haystack' => array_keys($arrPayStatus)))),							
						'PAYTYPE' 	=> array(),	
						'TRFTYPE' 	=> array(array('InArray', array('haystack' => array_keys($arrTraTypeRaw)))),	
						
						);

		
		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$fcompanycode 		= html_entity_decode($zf_filter->getEscaped('COMP_ID'));
		$fcompanyname 		= html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
		$fpaymentreff 		= html_entity_decode($zf_filter->getEscaped('PAY_REF'));
		
		$fcreatedfrom 		= html_entity_decode($zf_filter->getEscaped('PS_CREATED'));
		$fcreatedto 		= html_entity_decode($zf_filter->getEscaped('PS_CREATED_END'));
		
		$ftransferfrom 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE'));
		$ftransferto 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE_END'));
		
		$facctsource 		= html_entity_decode($zf_filter->getEscaped('SOURCE_ACCOUNT'));
		$fbeneaccount 		= html_entity_decode($zf_filter->getEscaped('BENEFICIARY_ACCOUNT'));
		$fpaymentstatus 	= html_entity_decode($zf_filter->getEscaped('PAYSTAT'));
		$fpaymenttype 		= html_entity_decode($zf_filter->getEscaped('PAYTYPE'));
		$ftransfertype 		= html_entity_decode($zf_filter->getEscaped('TRFTYPE'));
		//var_dump($fpaymenttype);
		//var_dump($dataParamValue);
		/*
			fcompanycode, fcompanyname, fpaymentreff, ftransferfrom, 
			ftransferto, fcreatedfrom, fcreatedto, fupdatedfrom, fupdatedto
			facctsource, fpaymentstatus, fpaymenttype, ftransfertype
		
		*/
		
			
		
		if($filter == NULL && $clearfilter != 1){
			$fupdatedfrom 	= date("01/m/Y");
			$fupdatedto 	= date("d/m/Y");
		}
		else{
		
			if($filter != NULL){
				$fupdatedfrom 		= html_entity_decode($zf_filter->getEscaped('PS_UPDATED'));
				$fupdatedto 		= html_entity_decode($zf_filter->getEscaped('PS_UPDATED_END'));
			}
			else if($clearfilter == 1){
				$fupdatedfrom 	= "";
				$fupdatedto 	= "";
			}
		}
		//Zend_Debug::dump($optpaytypeRaw);
		$casePayType = "(CASE TP.PS_TYPE ";
  		foreach($optpaytypeRaw as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";
		
		$casePayStatus = "(CASE TP.PS_STATUS ";		
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";
		
		$config    		= Zend_Registry::get('config');
		$paymentStatus 	= $config["payment"]["status"]; 
		$paymentType 	= $config["payment"]["type"]; 
		$transferType 	= $config["transfer"]["type"]; 
		
		// case when string		
		$caseArr 		 = Application_Helper_General::casePaymentType($paymentType, $transferType);
		// Zend_Debug::dump($caseArr);die;
		$casePaymentType = $caseArr["PS_TYPE"];
		$caseACCTSRC 	 = $caseArr["ACCTSRC"];
		$caseACBENEF 	 = $caseArr["ACBENEF"];
		//echo $caseACBENEF;
		$casePaymentStatus = Application_Helper_General::casePaymentStatus($paymentStatus);
		
		// build the query
		try{
		$select	= $this->_db->select()
							->distinct()
							->from(	array(	'P' => 'T_PSLIP'),
									array(	'companycode'	=>'C.CUST_ID',
											'companyname'	=>'C.CUST_NAME',
											'company'	=> new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )"),
											'payref'		=> 'P.PS_NUMBER',
										  	'TRA_TYPE'		=> 'T.TRANSFER_TYPE',
										  	'PSTYPE'		=> 'P.PS_TYPE',
											'createddate'	=> 'P.PS_CREATED',
											'updateddate'	=> 'P.PS_UPDATED',
											'transferdate'	=> 'P.PS_EFDATE',
											// 'traceno'		=> 'T.TRACE_NO',
											
											
											// 'acctsource'	=> new Zend_Db_Expr("	CASE P.PS_TYPE $caseACCTSRC 
																					// ELSE 
																						// T.SOURCE_ACCOUNT , ' (' , T.SOURCE_ACCOUNT_CCY , ')  ' , T.SOURCE_ACCOUNT_NAME , ' / ' , T.SOURCE_ACCOUNT_ALIAS_NAME
																					// END"),
											'acctsource'		=> new Zend_Db_Expr("CASE P.PS_TYPE $caseACCTSRC ELSE T.SOURCE_ACCOUNT END"),
											'beneaccount'		=> new Zend_Db_Expr("CASE P.PS_TYPE $caseACBENEF ELSE T.BENEFICIARY_ACCOUNT END"),
											'amount'		=> 'T.TRA_AMOUNT',										
											'ccy'		=> 'P.PS_CCY',										
											'paystatus'		=> new Zend_Db_Expr("CASE P.PS_STATUS $casePaymentStatus ELSE 'N/A' END"),
											'paytypes'		=> new Zend_Db_Expr("CASE $casePaymentType ELSE 'N/A' END"),
											'numtrx'		=> 'P.PS_TXCOUNT',
											'accsrc_ccy'	=> 'T.SOURCE_ACCOUNT_CCY',
											'accsrc_name'	=> new Zend_Db_Expr("CASE P.PS_TYPE $caseACCTSRC ELSE CONCAT(T.SOURCE_ACCOUNT_NAME) END"),
											'accsrc_alias'		=> 'T.SOURCE_ACCOUNT_ALIAS_NAME',
											'acbenef_name'	=> new Zend_Db_Expr("CASE P.PS_TYPE $caseACBENEF ELSE CONCAT(T.BENEFICIARY_ACCOUNT_NAME) END"),
											'acbenef_bankname'	=> 'T.BENEFICIARY_ACCOUNT_NAME',
											'acbenef_alias'	=> 'T.BENEFICIARY_ALIAS_NAME',
											'acbenef_ccy'	=> 'T.BENEFICIARY_ACCOUNT_CCY',
											'PS_STATUS'		=> 'P.PS_STATUS',
											
											
										 )
									)
							->joinLeft(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array() )
							->joinLeft(array('F' => 'M_BANK_TABLE' ), 'T.SOURCE_ACCT_BANK_CODE = F.BANK_CODE',array())
							->joinLeft(array('E' => 'M_BANK_TABLE' ), 'T.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							// ->joinLeft(	array('V' => 'V_PSLIP_TRA')  , 'P.PS_NUMBER = V.PS_NUMBER', array() )
							->joinLeft(
										array(	'C' => 'M_CUSTOMER'),
												'C.CUST_ID = P.CUST_ID',
												array()
										)
							->where('T.DISPLAY_FLAG=1')
							->group("T.PS_NUMBER")
							->order('P.PS_UPDATED DESC');
							//->where("T.TRANSFER_TYPE <= 4");
							
							/*
								transfer type:
									0: PB 
									1: RTGS
									2: SKN
							*/
		} catch (Exception $e) {
			Zend_Debug::Dump($e->getMessages());
		}
		
		
		/*
			fcompanycode, fcompanyname, fpaymentreff, ftransferfrom, 
			ftransferto, fcreatedfrom, fcreatedto, fupdatedfrom, fupdatedto
			facctsource, fpaymentstatus, fpaymenttype, ftransfertype
		
		*/
		
		// echo "<pre>";
		// echo $select;die;
		// echo $select->__toString();

		$selectBank	= $this->_db->select()
								->from(
									array('B' => 'M_BANK_TABLE'),
									array(
										'BANK_CODE'			=> 'B.BANK_CODE',
										'BANK_NAME'			=> 'B.BANK_NAME'
									)
								);

		$bankList = $this->_db->fetchAll($selectBank);

		foreach ($bankList as $key => $value) {
			$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		$this->view->bankNameList = $bankNameArr;
		
		if($fcompanycode){ $select->where("UPPER(C.CUST_ID) = ".$this->_db->quote($fcompanycode)); }
		if($fcompanyname){ $select->where("UPPER(C.CUST_NAME) LIKE ".$this->_db->quote('%'.$fcompanyname.'%')); }
		if($fpaymentreff){ $select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fpaymentreff.'%')); }
		
		if($ftransferfrom){
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) >= ?', $transferfrom);
		}
		if($ftransferto){
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) <= ?', $transferto);
		}
		
		if($fcreatedfrom){
			$FormatDate 	= new Zend_Date($fcreatedfrom, $this->_dateDisplayFormat);
			$createdfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) >= ?', $createdfrom);
		}
		// echo $select;die;
		if($fcreatedto){
			$FormatDate 	= new Zend_Date($fcreatedto, $this->_dateDisplayFormat);
			$createdto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) <= ?', $createdto);
		}
		
		if($fupdatedfrom){
			$FormatDate 	= new Zend_Date($fupdatedfrom, $this->_dateDisplayFormat);
			$updatedfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) >= ?', $updatedfrom);
		}
		// echo $select;die;
		if($fupdatedto){
			$FormatDate 	= new Zend_Date($fupdatedto, $this->_dateDisplayFormat);
			$updatedto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) <= ?', $updatedto);
		}
		
		if($facctsource)	{ $select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$facctsource.'%')); }
		if($fbeneaccount)	{ $select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fbeneaccount.'%')); }
		if($fpaymentstatus)	{ $select->where("P.PS_STATUS = ? ", $fpaymentstatus); }
		if($fpaymenttype){ 
			$fPayType 	 	= explode(",", $fpaymenttype);
			//print_r($fPayType);die;
			$select->where("P.PS_TYPE in (?) ", $fPayType);
			// $select->where("P.PS_TYPE = ? ", $fpaymenttype);		
					// print_r($fpaymenttype);die;
		}
		if($ftransfertype != null)
		{
			$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($ftransfertype));
		}
		else if($this->_getParam('TRANSFERTYPE') != null)
		{ 
			if($this->_getParam('TRANSFERTYPE') == 0)
			{
				$ftransfertype = 0;
				$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($ftransfertype));
			}
		}
		$excpstype = array(27,28);
		$select->where("P.PS_TYPE NOT IN (?) ", $excpstype);
		// echo "<pre>";
		// echo $select->__toString();
		// die;
		// $select->group('P.PS_NUMBER');
		// $select->order($sortBy.' '.$sortDir);
		
		$dataSQL = $this->_db->fetchAll($select);

		// var_dump($dataSQL);die;

		//Zend_Debug::dump($this->_db->fetchAll($select));
		$dataCount 	= count($dataSQL); // utk validasi max row csv
		
		if ($csv || $pdf || $this->_request->getParam('print')) {
			
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			//Zend_Debug::dump($dataSQL);die;
		}
		else {
		
			//$this->paging($dataSQL);
			//$dataSQL = $this->view->paginator;
			$this->paging($select);
			
		}
		
		
		
		if(!empty($dataSQL) && count($dataSQL) > 0){
			
			
			
			foreach($dataSQL as $dt => $val){
				
				foreach($val as $key => $val2){
					
					$value = $val[$key];
					
					if($key == "createddate" || $key == "updateddate"){
						$value = Application_Helper_General::convertDate($value,$this->view->displayDateTimeFormat,$this->view->defaultDateFormat); 
					}
					
					else if($key == "transferdate") { 
						$value = Application_Helper_General::convertDate($value,$this->_dateViewFormat); 
					}else if($key == "bookrate") { 
						$value = 'IDR '.Application_Helper_General::displayMoney($value); 
					}
					
					$data[$dt][$key] = $value;
				}
			}
				
		}
		else	$data = array();
	
		if($csv)
		{
			$this->_helper->download->csv($header,$data,null,$this->language->_('Payment Report'));  
			Application_Helper_General::writeLog('RPPY','Download CSV payment report');
		}
		elseif($pdf)
		{
			$this->_helper->download->pdf($header,$data,null,$this->language->_('Payment Report'));  
			Application_Helper_General::writeLog('RPPY','Download PDF payment report');
		}
		else if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Payment Report', 'data_header' => $fields));
		}
		else
		{			
			$stringParam = array(
									'COMP_ID'	=> $fcompanycode,
									'COMP_NAME'	=> $fcompanyname,
									'PAY_REF'	=> $fpaymentreff,
									'PS_TRFDATE'	=> $ftransferfrom,
									'PS_TRFDATE_END'	=> $ftransferto,
									'PS_CREATED'	=> $fcreatedfrom,
									'PS_CREATED_END'		=> $fcreatedto,
									'PS_UPDATED'	=> $fupdatedfrom,
									'PS_UPDATED_END'		=> $fupdatedto,
									'SOURCE_ACCOUNT'	=> $facctsource,
									'BENEFICIARY_ACCOUNT'	=> $fbeneaccount,
									'PAYSTAT'	=> $fpaymentstatus,
									'PAYTYPE'	=> $fpaymenttype,
									'TRANSFER_TYPE'	=> $ftransfertype,
									'clearfilter'	=> $clearfilter,
									'filter'		=> $filter,
								
								);
			
			$this->view->companycode 	= $fcompanycode;
			$this->view->companyname 	= $fcompanyname;
			$this->view->paymentreff 	= $fpaymentreff;
			
			$this->view->transferfrom 	= $ftransferfrom;
			$this->view->transferto 	= $ftransferto;
			
			$this->view->createdfrom 	= $fcreatedfrom;
			$this->view->createdto 		= $fcreatedto;
			
			$this->view->updatedfrom 	= $fupdatedfrom;
			$this->view->updatedto 		= $fupdatedto;
			
			$this->view->acctsource 	= $facctsource;
			$this->view->beneaccount 	= $fbeneaccount;
			$this->view->paymentstatus 	= $fpaymentstatus;
			$this->view->paymenttype 	= $fpaymenttype;
			$this->view->transfertype 	= $ftransfertype;
			//print_r($data);die;
			$this->view->fields      	= $fields;
			$this->view->sortBy      	= $sortBy;
			$this->view->sortDir    	= $sortDir;
			$this->view->data    		= $data;
			$this->view->dataCount 		= $dataCount;
			
			$this->view->arrPayType 	= $optpaytypeRaw;
			$this->view->arrPayStatus 	= $arrPayStatus;
			$this->view->arrTraType 	= $arrTraTypeRaw;
			$this->view->arrCompany 	= $arrCompany;
			
			// set URL
			$URL = $this->view->url(array(	'module'	 => $this->view->modulename,
											'controller' => $this->view->controllername,
											'action'	 => 'index',
											'page'		 => $page,
											'sortBy' 	 => $this->view->sortBy,
											'sortDir' 	 => $this->view->sortDir),null,true).$this->view->qstring;
			
			$sessionNamespace = new Zend_Session_Namespace('URL_CP_PR');
			$sessionNamespace->URL = $URL;
			
			Application_Helper_General::writeLog('RPPY','View Payment Report');

			if(!empty($dataParamValue)){
	    		$this->view->createdStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
	    		$this->view->updatedStart = $dataParamValue['PS_UPDATED'];
	    		$this->view->updateEnd = $dataParamValue['PS_UPDATED_END'];
	    		$this->view->trfdateStart = $dataParamValue['PS_TRFDATE'];
	    		$this->view->trfdateEnd = $dataParamValue['PS_TRFDATE_END'];

	    	  	unset($dataParamValue['PS_CREATED_END']);
			    unset($dataParamValue['PS_TRFDATE_END']);
			    unset($dataParamValue['PS_UPDATED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
		}
	}
	
	public function getCompanyCode(){
		
       $select = $this->_db->select()
      	                   //->from(array('M'=>'M_ANCHOR'),array())
						   ->from(array('CT'=>'M_CUSTOMER'),array('companycustid'=>'CUST_ID','companyname'=>'CUST_NAME'))
      	                   ->query()->fetchAll();
						   
		if(is_array($select) && count($select) > 0){	   
			foreach($select as $key => $val){
				
				$a = $select[$key]["companycustid"];
				$b = (isset($select[$key]["membername"])) ? $select[$key]["membername"] : '';
				$result[$a] = $a;
				
			}
		}
		else $result = array();
		return $result;
	  
    }
		
		
		
		
}
