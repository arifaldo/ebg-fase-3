<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/CustomerUser.php';
	require_once 'General/Settings.php';
	require_once 'SGO/Soap/ClientUser.php';
	$db = Zend_Db_Table::getDefaultAdapter();

	$curTime = date('Y-m-d H:i');

	$cronResult = $curTime.' | ';

	$data = $db->select()
			->FROM	(array('A' => 'M_AIRLINES_SETTINGS'),array('*'))
			->WHERE('A.EF_DATE >= ?', $curTime.':00')
			->WHERE('A.EF_DATE <= ?', $curTime.':59')
			->WHERE('A.STATUS = 1')
			->query()->fetchAll();

	
	if (!empty($data)) {

		$clientUser  =  new SGO_Soap_ClientUser();
		foreach ($data as $key => $value) {

			$transaction_info = json_decode($value['TRANSACTION_INFO'], 1);

			//----------------------------------------kalau travelagentnya bs lebih dari satu dalam suatu settingan-------------------------------------
			$currentBalance = 0;
			$successCheckBalance = 0;
			$travelagentArr = array();
			$requestArr = array();
			$responseArr = array();
			foreach ($transaction_info as $row => $rowVal) {
				if (!in_array($rowVal['travelagentid'], $travelagentArr)) {
					$paramObj = array(
						'airline_code' => $value['AIRLINE_CODE'],
						'AUTH_USER' => 'travel',
						'AUTH_PASS' => 'test',
						'TRAVELAGENT_ID' => $rowVal['travelagentid']
					);

					//check balance diulang 4x sampai dapat sukses
					
					for($i = 0; $i < 4; $i++){
						$success = $clientUser->callapi('airlinesDepositInquiry',$paramObj,'airlines/agent/info');

						$result  = $clientUser->getResult();

						//utk log request and response
						array_push($requestArr, $paramObj);
						array_push($responseArr, $result);
				
						if($clientUser->isTimedOut()){
							$returnStruct = array(
									'ResponseCode'	=>'XT',
									'ResponseDesc'	=>'Service Timeout',					
									'Cif'			=>'',
									'AccountList'	=> '',
							);
						}else{

							if($result->error_code == '0000'){
								$currentBalance += $result->account_info->available_credit;
								$successCheckBalance++;
								break;
							}
						}
					}
					array_push($travelagentArr, $rowVal['travelagentid']);
				}
			}

			if ($successCheckBalance == count($travelagentArr)) {

				if ($currentBalance < $value['MIN_BALANCE']) {

					$dividedby = $transaction_info[0]['type'];

					$balanceAfter = $currentBalance;

					foreach ($transaction_info as $key2 => $value2) {

						$amount = '';
						if ($dividedby == 'p') {
							$amount = (($value['AMOUNT_TOPUP'] * $value2['value']) / 100);
						}
						else{
							$amount = $value2['value'];
						}

						$description = 'Auto Topup from Source Account: '.$value2['sourceaccount'].' to Travel agent id: '.$value2['travelagentid'].' of IDR '.Application_Helper_General::displayMoney($amount);

						$balanceAfter += $amount;

						$transactionId = generateTransactionID($value);

						$data = array(
							'TRANSACTION_ID'		=> $transactionId,
							'CUST_ID'				=> $value['CUST_ID'],
							'AIRLINE_CODE'			=> $value['AIRLINE_CODE'],
							'BANK_CODE'				=> $value2['bankcode'],
							'SOURCE_ACCOUNT'		=> $value2['sourceaccount'],
							'TRAVELAGENT_ID' 		=> $value2['travelagentid'],
							'BALANCE_PREV' 			=> $currentBalance,
							'CURRENT_BALANCE' 		=> $balanceAfter,
							'TOPUP_AMOUNT' 			=> $amount,
							'DESCRIPTION' 			=> $description,
							'DATE_TIME' 			=> date('Y-m-d H:i:s'),
							'STATUS'	 			=> 2, // 1=checking balance, 2=autotopup, 3=failed check balance, 4=failed autotopup, 5=suspect topup, 6=reach warning level 1, 7=reach warning level 2
							'BALANCE_CYCLE'			=> $value['BALANCE_CYCLE'],
							'WARN1_FLAG'			=> 0,
							'WARN2_FLAG'			=> 0
						);	



						$paramObj = array(
							'airline_code' => $value['AIRLINE_CODE'],
							// 'AUTH_USER' => $transaction_info[0]['travelagentid'],
							'AUTH_USER' => 'travel',
							'AUTH_PASS' => 'test',
							'TRAVELAGENT_ID' => $value2['travelagentid'],
							'TRANSACTION_ID' => $transactionId,
							'AMOUNT' => $amount,
							'ccy' => 'IDR',
						);

						$successTopup = false;
						$status = 'failed';
						$success = $clientUser->callapi('airlinesDepositTopup',$paramObj,'airlines/topup/commit');

						$result  = $clientUser->getResult();
				
						if($clientUser->isTimedOut()){
							$returnStruct = array(
									'ResponseCode'	=>'XT',
									'ResponseDesc'	=>'Service Timeout',					
									'Cif'			=>'',
									'AccountList'	=> '',
							);
						}else{

							if($result->error_code == '0000'){
								$successTopup = true;
								$status = 'success';
							}
							else if($result->error_code == '9999'){
								$successTopup = false;
								$status = 'suspect';
							}
						}

						if ($successTopup) {

							$data['REQUEST'] = json_encode($paramObj);
							$data['RESPONSE'] = json_encode($result);

							insertAutotopupLog($data);
							$currentBalance = $balanceAfter;
						}
						else{
							$data['CURRENT_BALANCE'] = $currentBalance;
							$data['TOPUP_AMOUNT'] = null;
							$data['REQUEST'] = json_encode($paramObj);
							$data['RESPONSE'] = json_encode($result);

							$description = 'Failed Auto Topup from Source Account: '.$value2['sourceaccount'].' to Travel agent id: '.$value2['travelagentid'].' of IDR '.Application_Helper_General::displayMoney($amount).' | '.$result->response_desc;
							$data['DESCRIPTION'] = $description;

							$data['STATUS'] = 4; // 1=checking balance, 2=autotopup, 3=failed check balance, 4=failed autotopup, 5=suspect topup, 6=reach warning level 1, 7=reach warning level 2

							if($status == 'suspect'){
								$data['STATUS'] = 5;
							}

							insertAutotopupLog($data);
						}

						//send email
						sendEmail($value['CUST_ID'], $value['CREATED_BY'], $data);

						$cronResult .= $description.'<br>';
					}

					updateTimeAutotopupLog($data);
				}
				else{

					//kalau travel agent id bisa banyak dlm suatu setting
					$travelagentid = implode(',', $travelagentArr); 

					$data = array(
						'TRANSACTION_ID'		=> null,
						'CUST_ID'				=> $value['CUST_ID'],
						'AIRLINE_CODE'			=> $value['AIRLINE_CODE'],
						'BANK_CODE'				=> null,
						'SOURCE_ACCOUNT'		=> null,
						'TRAVELAGENT_ID' 		=> $travelagentid,
						'BALANCE_PREV' 			=> null,
						'CURRENT_BALANCE' 		=> $currentBalance,
						'TOPUP_AMOUNT' 			=> null,
						'DATE_TIME' 			=> date('Y-m-d H:i:s'),
						'BALANCE_CYCLE'			=> $value['BALANCE_CYCLE'],
						'REQUEST'				=> json_encode($requestArr),
						'RESPONSE'				=> json_encode($responseArr),
						'WARN1_FLAG'			=> 0,
						'WARN2_FLAG'			=> 0

					);

					if($currentBalance <= $value['WARN_LEVEL2']){

						$description = 'Checking balance. Balance: IDR '.Application_Helper_General::displayMoney($currentBalance).'. Balance is exceed warning level 2 ('.$value['WARN_LEVEL2'].')';

						$data['DESCRIPTION'] = $description;
						$data['STATUS'] = 7;
						$data['WARN2_FLAG'] = 1;
					}
					else if($currentBalance <= $value['WARN_LEVEL1']){

						$description = 'Checking balance. Balance: IDR '.Application_Helper_General::displayMoney($currentBalance).'. Balance is exceed warning level 1 ('.$value['WARN_LEVEL1'].')';

						$data['DESCRIPTION'] = $description;
						$data['STATUS'] = 6;
						$data['WARN1_FLAG'] = 1;
					}
					else{

						$description = 'Checking balance. Balance: IDR '.Application_Helper_General::displayMoney($currentBalance);

						$data['DESCRIPTION'] = $description;
						$data['STATUS'] = 1;
					}

					insertAutotopupLog($data);

					updateTimeAutotopupLog($data);	
 	 
					//send email
					sendEmail($value['CUST_ID'], $value['CREATED_BY'], $data);

					$cronResult .= $description.'<br>';
				}	
			}
			else{

				$description = 'Failed check balance airlines:  '.$value['AIRLINE_CODE'].' of customer id: '.$value['CUST_ID'];

				//kalau travel agent id bisa banyak dlm suatu setting
				$travelagentid = implode(',', $travelagentArr); 

				$data = array(
					'TRANSACTION_ID'		=> null,
					'CUST_ID'				=> $value['CUST_ID'],
					'AIRLINE_CODE'			=> $value['AIRLINE_CODE'],
					'TRAVELAGENT_ID' 		=> $travelagentid,
					'DESCRIPTION' 			=> $description,
					'DATE_TIME' 			=> date('Y-m-d H:i:s'),
					'STATUS'	 			=> 3, // 1=checking balance, 2=autotopup, 3=failed check balance, 4=failed autotopup, 5=suspect topup, 6=reach warning level 1, 7=reach warning level 2
					'BALANCE_CYCLE'			=> $value['BALANCE_CYCLE'],
					'REQUEST'				=> json_encode($requestArr),
					'RESPONSE'				=> json_encode($responseArr),
					'WARN1_FLAG'			=> 0,
					'WARN2_FLAG'			=> 0
				);

				insertAutotopupLog($data);

				updateTimeAutotopupLog($data);	

				//send email
				sendEmail($value['CUST_ID'], $value['CREATED_BY'], $data);

				$cronResult .= $description.'<br>'; 
			}
		}
	}
	else
	{
		$cronResult .= "No data";
	}

	$filename = basename(__FILE__);
	Application_Helper_General::cronLog($filename, $cronResult, 1);


	function generateTransactionID($data){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(9));
		$trxId = 'AT'.$currentDate.$seqNumber;

		return $trxId;
	}

	function insertAutotopupLog($data){

		$db = Zend_Db_Table::getDefaultAdapter();

		unset($data['BALANCE_CYCLE']);
		unset($data['WARN1_FLAG']);
		unset($data['WARN2_FLAG']);

		try {
			//insert T_AIRLINES_LOG
			$db->insert('T_AIRLINES_LOG',$data);

			// Application_Helper_General::cronLog($filename, 'Success insert to T_AIRLINES_LOG', 1);
		} catch (Exception $e) {
			die($e);
			// Application_Helper_General::cronLog($filename, 'Failed insert to T_AIRLINES_LOG', 0);
		}
	}

	function updateTimeAutotopupLog($data){

		$db = Zend_Db_Table::getDefaultAdapter();

		if ($data['BALANCE_CYCLE'] == '30m') {
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+30 minutes"));
		}
		else if($data['BALANCE_CYCLE'] == '1h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+1 hour"));
		}
		else if($data['BALANCE_CYCLE'] == '2h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+2 hour"));
		}
		else if($data['BALANCE_CYCLE'] == '3h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+3 hour"));
		}

		$updateData = array(
			'EF_DATE' => $EF_DATE,
		);
		$where['CUST_ID = ?'] = $data['CUST_ID'];
		$where['AIRLINE_CODE = ?'] = $data['AIRLINE_CODE'];

		try {
			//update efdate M_AIRLINES_SETTINGS
			$db->update('M_AIRLINES_SETTINGS', $updateData, $where);
			// Application_Helper_General::cronLog($filename, 'Success insert to T_AIRLINES_LOG', 1);
		} catch (Exception $e) {
			die($e);
			// Application_Helper_General::cronLog($filename, 'Failed insert to T_AIRLINES_LOG', 0);
		}
	}

	function updateWarnFlag($data){

		$db = Zend_Db_Table::getDefaultAdapter();

		$updateData = array(
			'WARN1_FLAG' => $data['WARN1_FLAG'],
			'WARN2_FLAG' => $data['WARN2_FLAG']
		);
		$where['CUST_ID = ?'] = $data['CUST_ID'];
		$where['AIRLINE_CODE = ?'] = $data['AIRLINE_CODE'];

		try {
			//update efdate M_AIRLINES_SETTINGS
			$db->update('M_AIRLINES_SETTINGS', $updateData, $where);
			// Application_Helper_General::cronLog($filename, 'Success insert to T_AIRLINES_LOG', 1);
		} catch (Exception $e) {
			die($e);
			// Application_Helper_General::cronLog($filename, 'Failed insert to T_AIRLINES_LOG', 0);
		}
	}


	/*
		 1=checking balance, just check not send email
		 2=autotopup
		 3=failed check balance
		 4=failed autotopup
		 5=suspect topup
		 6=reach warning level 1
		 7=reach warning level 2
	*/
	function sendEmail($custId, $userId, $data){

		$db = Zend_Db_Table::getDefaultAdapter();

		$cu = new CustomerUser($custId, $userId);

		$select	= $db->select()
			->from(array('M_EMAIL_SETTING'), array('*'))
			->where('CUST_ID = ?', $custId)
			->where('PS_TYPE = ?', '99')
			->where('STATUS = ?', '1');

		$notifSetting = $db->fetchRow($select);


		$select	= $db->select()
			->from(array('M_AIRLINES_SETTINGS'), array('WARN1_FLAG', 'WARN2_FLAG'))
			->where('CUST_ID = ?', $custId)
			->where('AIRLINE_CODE = ?', $data['AIRLINE_CODE']);

		$warnFlag = $db->fetchRow($select);

		$warn1Flag = $warnFlag['WARN1_FLAG'];
		$warn2Flag = $warnFlag['WARN2_FLAG'];

		//chcek if warn flag is on
		$checkWarnFlag = true;
		if($data['STATUS'] == '6' && $warn1Flag == 1){
			$checkWarnFlag = false;
		}
		else if($data['STATUS'] == '7' && $warn2Flag == 1){
			$checkWarnFlag = false;
		}


		//if notif setting auto topup on, and status != 1 (just check balance not exceed any warn or min balance)
		if (!empty($notifSetting) && $data['STATUS'] != 1 && $checkWarnFlag) {

			$select	= $db->select()
				->from(array('A' => 'M_AIRLINES'), array('AIRLINE_NAME'))
				->join(array('B' => 'M_AIRLINES_SETTINGS'), 'A.AIRLINE_CODE = B.AIRLINE_CODE', array('*'))
				->where('A.AIRLINE_CODE = ?', $data['AIRLINE_CODE'])
				->where('B.CUST_ID = ?', $data['CUST_ID']);

			$airline = $db->fetchRow($select);

			$data['AIRLINE_DATA'] = $airline;

			$select	= $db->select()
				->from(array('A' => 'M_CUSTOMER'), array('CUST_NAME'))
				->where('A.CUST_ID = ?', $data['CUST_ID']);

			$custName = $db->fetchRow($select);

			$data['CUST_NAME'] = $custName['CUST_NAME'];

			//if berhub sm topup
			if($data['STATUS'] == '2' || $data['STATUS'] == '4' || $data['STATUS'] == '5'){

				$select	= $db->select()
					->from(array('A' => 'M_APIKEY'), array('*'))
					->join(array('B' => 'M_BANK_TABLE'), 'A.BANK_CODE = B.BANK_CODE', array('BANK_NAME'))
					->where('A.FIELD = ?', 'account_number')
					->where('A.VALUE = ?', $data['SOURCE_ACCOUNT']);

				$acctData = $db->fetchRow($select);

				$select	= $db->select()
					->from(array('A' => 'M_APIKEY'), array('*'))
					->where('A.APIKEY_ID = ?', $acctData['APIKEY_ID'])
					->where('A.FIELD = ?', 'account_name');

				$acctAllData = $db->fetchRow($select);

				$data['BANK_NAME'] = $acctData['BANK_NAME'];
				$data['ACCOUNT_NAME'] = $acctAllData['VALUE'];
			}

			$email = $notifSetting['EMAIL'];
			$emailList = explode(';', $email); 

			if (count($emailList) > 1) {
				foreach ($emailList as $key => $value) {
					$newEmailList[]['USER_EMAIL'] = $value;
				}
			}
			else{
				$newEmailList[]['USER_EMAIL'] = $email;
			}

			//cuma data dummy utk dioper ke function
			$transactionData[0]['PS_NUMBER'] = 'autotopup'; 
			$transactionData[0]['BENEFICIARY_ACCOUNT'] = $data['TRAVELAGENT_ID']; 
			$transactionData[0]['BENEFICIARY_ACCOUNT_NAME'] = '-'; 
			$transactionData[0]['TRA_MESSAGE'] = $data['DESCRIPTION']; 
			$transactionData[0]['SOURCE_ACCOUNT_CCY'] = 'IDR'; 
			$transactionData[0]['TRA_AMOUNT'] = '-'; 
			$sendTransfer['ResponseCode'] = null;

			//data log
			$transactionData[0]['DATA'] = $data; 
			
			$cu->sendEmailPaymentNotification($transactionData,$sendTransfer,$newEmailList); //send email payment notification		

			//updatewarnflag
			updateWarnFlag($data);
			
		}
	}

?>