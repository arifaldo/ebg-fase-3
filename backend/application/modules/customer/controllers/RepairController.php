<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class customer_RepairController extends customer_Model_Customer
{

	public function initController()
	{

		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction()
	{
		//pengaturan url untuk button back
		$this->_helper->layout()->setLayout('newpopup');
		$this->setbackURL('/' . $this->_request->getModuleName() . '/index');

		$cust_id = strtoupper($this->_getParam('changes_id'));
		$this->view->changes_id = $cust_id;
		$change_id = strtoupper($this->_getParam('changes_id'));
		$cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;
		$cust_view = 1;
		$error_remark = null;
		$flag = 0;

		$this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(), 'COUNTRY_CODE', 'COUNTRY_NAME');
		$this->view->changes_id = $cust_id;
		//data customer
		// var_dump($cust_id);die;
		$resultdata = $this->getTempCustomer($cust_id);
		// print_r($resultdata);die;

		if ($cust_id) {
			$tempCustomerId = $this->getTempCustomerId($cust_id);
			if ($tempCustomerId) $error_remark = 'invalid format';
			else $flag = 1;
		} else {
			$error_remark = 'invalid format';
		}

		$arrSecure = array(0 => "No", 1 => "Yes");

		$this->view->secure = $arrSecure;

		//convert limit idr & usd agar bisa masuk ke database
		$limitidr = $this->_getParam('cust_limit_idr');
		$limitidr = Application_Helper_General::convertDisplayMoney($limitidr);
		$this->_setParam('cust_limit_idr', $limitidr);

		$limitusd = $this->_getParam('cust_limit_usd');
		$limitusd = Application_Helper_General::convertDisplayMoney($limitusd);
		$this->_setParam('cust_limit_usd', $limitusd);
		//END convert limit idr & usd agar bisa masuk ke database 


		if ($this->_request->isPost()) {
			$params = $this->_request->getParams();

			$filters = array('*' => array('StringTrim', 'StripTags'));

			if ($params['cust_model'] == "Pemohon") {
				$params['cust_model'] = 1;
			} elseif ($params['cust_model'] == "Asuransi") {
				$params['cust_model'] = 2;
			} else {
				$params['cust_model'] = 3;
			}

			$customer_filter = array();

			$validators = array(
				'cust_type'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Company Type cannot be empty'))
				),
				'business_type'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Business Type cannot be empty'))
				),
				'go_public'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Go Public cannot be empty'))
				),
				'grup_bumn'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Grup Bumn cannot be empty'))
				),
				'cust_address'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Address cannot be empty'))
				),
				'cust_village'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kelurahan cannot be empty'))
				),
				'cust_district'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kecamatan cannot be empty'))
				),
				'cust_city'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kabupaten/City cannot be empty'))
				),
				'cust_zip'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Zip cannot be empty'))
				),
				'country_code'    => array(
					'NotEmpty',
					'messages' => array($this->language->_('Country cannot be empty'))
				),
				'cust_email' => array(
					'NotEmpty',
					'messages' => array($this->language->_('Contact Email cannot be empty'))
				),
				'cust_website'  => array(
					'allowEmpty' => true
				),
				'req_id'	 => array(
					'allowEmpty' => true
				),
				'cust_same_user'  => array(
					'allowEmpty' => true
				),
				'cust_approver'  => array(
					'allowEmpty' => true
				),
			);

			if ($params['cust_model'] == 1) {

				$validators += array(
					'debitur_code'   => array(
						'NotEmpty',
						'messages' => array($this->language->_('Golongan Debitur Code cannot be empty'))
					),
					'cust_contact'	 => array('allowEmpty' => true),
					'cust_contact_phone'     => array(
						'allowEmpty' => true,
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
					'cust_phone'       => array(
						'allowEmpty' => true,
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
				);
			} else {
				$validators += array(
					'cust_phone' => array(
						'NotEmpty',
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Company Phone cannot be empty'),
							$this->language->_('Company Phone maximum characters allowed is') . ' 128'
						)
					),
				);
			}

			if ($params['cust_model'] == 2) {
				$validators += array(
					'cust_contact'	 => array(
						'NotEmpty',
						'messages' => array($this->language->_('Contact Person cannot be empty'))
					),
					'cust_contact_phone' => array(
						'NotEmpty',
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number cannot be empty'),
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
				);
			}

			if ($params['cust_model'] == 3) {
				$validators += array(
					'cust_contact'	 => array('allowEmpty' => true),
					'cust_contact_phone'     => array(
						'allowEmpty' => true,
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
				);
			}

			if ($params['cust_model'] != 3) {
				$validators += array(
					'collectibility_code'	 => array(
						'allowEmpty' => true,
						// 'messages' => array($this->language->_('Collectibility Code cannot be empty'))
					),
				);
			}

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

			//validasi multiple email
			if ($this->_getParam('cust_email')) {
				$validate = new validate;
				$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('cust_email'));
			} else {
				$cek_multiple_email = true;
			}

			$flag_cust_phone = 'T';


			if ($zf_filter_input->isValid() && $flag_cust_phone == 'T' && $cek_multiple_email == true) {
				$info = 'Customer ID = ' . $zf_filter_input->cust_id . ', Customer Name = ' . $zf_filter_input->cust_name;
				$cust_data = $this->_custData;
				foreach ($validators as $key => $value) {
					if ($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;
				}

				try {
					$this->_db->beginTransaction();

					if (is_null($cust_data['CUST_CHARGES_STATUS']) || $cust_data['CUST_CHARGES_STATUS'] == '') {
						$cust_data['CUST_CHARGES_STATUS'] = 0;
					}
					if (is_null($cust_data['CUST_MONTHLYFEE_STATUS']) || $cust_data['CUST_MONTHLYFEE_STATUS'] == '') {
						$cust_data['CUST_MONTHLYFEE_STATUS'] = 0;
					}
					if (is_null($cust_data['CUST_TOKEN_AUTH']) || $cust_data['CUST_TOKEN_AUTH'] == '') {
						$cust_data['CUST_TOKEN_AUTH'] = 0;
					}
					if (is_null($cust_data['CUST_SPECIAL_RATE']) || $cust_data['CUST_SPECIAL_RATE'] == '') {
						$cust_data['CUST_SPECIAL_RATE'] = 0;
					}
					if (is_null($cust_data['CUST_SPECIAL']) || $cust_data['CUST_SPECIAL'] == '') {
						$cust_data['CUST_SPECIAL'] = 'N';
					}
					if (is_null($cust_data['CUST_LIMIT_IDR']) || $cust_data['CUST_LIMIT_IDR'] == '') {
						$cust_data['CUST_LIMIT_IDR'] = 0;
					}
					if (is_null($cust_data['CUST_LIMIT_USD']) || $cust_data['CUST_LIMIT_USD'] == '') {
						$cust_data['CUST_LIMIT_USD'] = 0;
					}

					if ($cust_data['GRUP_BUMN'] == "Y") {
						$cust_data['GRUP_BUMN'] = 1;
					} else {
						$cust_data['GRUP_BUMN'] = 0;
					}

					$cust_data['CUST_STATUS']		= 1;
					$cust_data['CUST_CREATED']		= date('Y-m-d H:i:s');
					$cust_data['CUST_CREATEDBY']	= $this->_userIdLogin;
					$cust_data['CUST_UPDATED']		= null;
					$cust_data['CUST_UPDATEDBY']	= null;
					$cust_data['CUST_SUGGESTED']	= date('Y-m-d H:i:s');
					$cust_data['CUST_SUGGESTEDBY']	= $this->_userIdLogin;

					if ($cust_data['CUST_APP_TOKEN'] == 'on') {
						$cust_data['CUST_APP_TOKEN'] = '1';
					}
					if ($cust_data['CUST_RLS_TOKEN'] == 'on') {
						$cust_data['CUST_RLS_TOKEN'] = '1';
					}
					if ($zf_filter_input->cust_role_adm == 'on') {
						$cust_data['CUST_ADM_ROLE'] = '1';
					}
					if ($cust_data['CUST_FINANCE'] == '3') {
						$cust_data['CUST_LIMIT_IDR'] = 1;
						$cust_data['CUST_LIMIT_USD'] = 1;
					}

					if (!empty($zf_filter_input->same_release)) {
						$cust_data['CUST_SAME_USER'] = $zf_filter_input->same_release;
					}

					$getTemp = $this->_db->select()
						->from("TEMP_CUSTOMER")
						->where("CHANGES_ID = ?", $change_id)
						->query()->fetchAll();

					$cust_data['CUST_ID'] = $getTemp[0]['CUST_ID'];
					$cust_data['CUST_MODEL'] = $getTemp[0]['CUST_MODEL'];
					$cust_data['CUST_CIF'] = $getTemp[0]['CUST_CIF'];
					$cust_data['CUST_NAME'] = $getTemp[0]['CUST_NAME'];
					$cust_data['CUST_NPWP'] = $getTemp[0]['CUST_NPWP'];
					$cust_data['DEBITUR_CODE'] = $params['debitur_code']; // new

					try {
						$this->updateTempCustomer($change_id, $cust_data);

						$this->_db->delete('TEMP_CUSTOMER_ACCT', 'CHANGES_ID = ' . $this->_db->quote($change_id));

						foreach ($zf_filter_input->req_id as $key => $value) {
							$content = [
								'CHANGES_ID'		=> $change_id,
								'CUST_ID'			=> $cust_data['CUST_ID'],
								'ACCT_NO'			=> $value,
								'CCY_ID'			=> Application_Helper_General::getCurrCode($this->_getParam('ccy' . $value)),
								'ACCT_EMAIL'		=> null,
								'ACCT_STATUS'		=> 1,
								'ACCT_SUGGESTED'	=> date('Y-m-d H:i:s'),
								'ACCT_SUGGESTEDBY'	=> $this->_userIdLogin,
								'PLAFOND'			=> null,
								'ACCT_SOURCE'		=> null,
								'ACCT_NAME'			=> $this->_getParam('accountName' . $value),
								'ACCT_TYPE'			=> $this->_getParam('productType' . $value),
								'ACCT_DESC'			=> $this->_getParam('acct_desc' . $value),
								// 'ACCT_ALIAS_NAME'	=> isset($acct_data['ACCT_ALIAS_NAME']) ? $acct_data['ACCT_ALIAS_NAME'] : null,
								'ACCT_ALIAS_NAME'  => $this->_getParam('accountName' . $value),
								'ORDER_NO'         => $key,
								// 'ACCT_NAME'			=> isset($acct_data['ACCT_NAME']) ? $acct_data['ACCT_NAME'] : null,
								'GROUP_ID'			=> null,
								'ACCT_RESIDENT'		=> null,
								'ACCT_CITIZENSHIP'	=> null,
								'ACCT_CATEGORY'		=> null,
								'ACCT_ID_TYPE'		=> null,
								'ACCT_ID_NUMBER'	=> null
							];
							$insert = $this->_db->insert('TEMP_CUSTOMER_ACCT', $content);
						}
					} catch (Exception $e) {
						$this->print($e);
						die('catch');
					}


					$this->updateGlobalChanges($change_id);

					Application_Helper_General::writeLog('CCUD', 'Customer has been updated (edit), Cust ID : ' . $zf_filter_input->cust_id . ' Cust Name : ' . $zf_filter_input->cust_name . ' Change id : ' . $change_id);

					$this->_db->commit();

					$this->_redirect('/popup/submited/index');
				} catch (Exception $e) {
					$this->_db->rollBack();
					$error_remark = $this->language->_('Database Error');
				}

				if (isset($error_remark)) {
					$msg = $error_remark;
					$class = 'F';
				} else {
					$msg = 'Success';
					$class = 'S';
				}

				$this->_helper->getHelper('FlashMessenger')->addMessage($class);
				$this->_helper->getHelper('FlashMessenger')->addMessage($msg);
				$this->_redirect($this->_backURL);
			} //END IF is VALID
			else {
				$this->view->cust_cif = ($zf_filter_input->isValid('cust_cif')) ? $zf_filter_input->cust_cif : $this->_getParam('cust_cif');
				$this->view->cust_id   = ($zf_filter_input->isValid('cust_id')) ? $zf_filter_input->cust_id : $this->_getParam('cust_id');
				$this->view->cust_name = ($zf_filter_input->isValid('cust_name')) ? $zf_filter_input->cust_name : $this->_getParam('cust_name');




				$this->view->cust_code = ($zf_filter_input->isValid('cust_code')) ? $zf_filter_input->cust_code : $this->_getParam('cust_code');
				$this->view->cust_special = ($zf_filter_input->isValid('cust_special')) ? $zf_filter_input->cust_special : $this->_getParam('cust_special');

				$this->view->cust_type = ($zf_filter_input->isValid('cust_type')) ? $zf_filter_input->cust_type : $this->_getParam('cust_type');
				$this->view->cust_address = ($zf_filter_input->isValid('cust_address')) ? $zf_filter_input->cust_address : $this->_getParam('cust_address');
				$this->view->cust_city = ($zf_filter_input->isValid('cust_city')) ? $zf_filter_input->cust_city : $this->_getParam('cust_city');
				$this->view->cust_zip  = ($zf_filter_input->isValid('cust_zip')) ? $zf_filter_input->cust_zip : $this->_getParam('cust_zip');
				$this->view->cust_province = ($zf_filter_input->isValid('cust_province')) ? $zf_filter_input->cust_province : $this->_getParam('cust_province');
				$this->view->cust_workfield = ($zf_filter_input->isValid('cust_workfield')) ? $zf_filter_input->cust_workfield : $this->_getParam('cust_workfield');
				$this->view->cust_contact  = ($zf_filter_input->isValid('cust_contact')) ? $zf_filter_input->cust_contact : $this->_getParam('cust_contact');
				$this->view->cust_phone    = ($zf_filter_input->isValid('cust_phone')) ? $zf_filter_input->cust_phone : $this->_getParam('cust_phone');
				$this->view->cust_ext      = ($zf_filter_input->isValid('cust_ext')) ? $zf_filter_input->cust_ext : $this->_getParam('cust_ext');
				$this->view->cust_fax      = ($zf_filter_input->isValid('cust_fax')) ? $zf_filter_input->cust_fax : $this->_getParam('cust_fax');
				$this->view->cust_email    = ($zf_filter_input->isValid('cust_email')) ? $zf_filter_input->cust_email : $this->_getParam('cust_email');
				$this->view->cust_website  = ($zf_filter_input->isValid('cust_website')) ? $zf_filter_input->cust_website : $this->_getParam('cust_website');
				$this->view->country_code  = ($zf_filter_input->isValid('country_code')) ? $zf_filter_input->country_code : $this->_getParam('country_code');
				$this->view->cust_limit_idr  = ($zf_filter_input->isValid('cust_limit_idr')) ? $zf_filter_input->cust_limit_idr : $this->_getParam('cust_limit_idr');
				$this->view->cust_limit_usd  = ($zf_filter_input->isValid('cust_limit_usd')) ? $zf_filter_input->cust_limit_usd : $this->_getParam('cust_limit_usd');

				$this->view->same_release  = $this->_getParam('same_release');

				$this->view->cust_review  = ($zf_filter_input->isValid('cust_review')) ? $zf_filter_input->cust_review : $this->_getParam('cust_review');
				$this->view->cust_approver  = ($zf_filter_input->isValid('cust_approver')) ? $zf_filter_input->cust_approver : $this->_getParam('cust_approver');
				$this->view->cust_app_token  = ($zf_filter_input->isValid('cust_app_token')) ? $zf_filter_input->cust_app_token : $this->_getParam('cust_app_token');
				$this->view->cust_rls_token  = ($zf_filter_input->isValid('cust_rls_token')) ? $zf_filter_input->cust_rls_token : $this->_getParam('cust_rls_token');
				$this->view->cust_finance  = ($zf_filter_input->isValid('cust_finance')) ? $zf_filter_input->cust_finance : $this->_getParam('cust_finance');
				$this->view->admin1  = ($zf_filter_input->isValid('admin1')) ? $zf_filter_input->admin1 : $this->_getParam('admin1');
				$this->view->emailadmin1  = ($zf_filter_input->isValid('emailadmin1')) ? $zf_filter_input->emailadmin1 : $this->_getParam('emailadmin1');
				$this->view->phoneadmin1  = ($zf_filter_input->isValid('phoneadmin1')) ? $zf_filter_input->phoneadmin1 : $this->_getParam('phoneadmin1');
				$this->view->tokenadmin1  = ($zf_filter_input->isValid('tokenadmin1')) ? $zf_filter_input->tokenadmin1 : $this->_getParam('tokenadmin1');
				$this->view->admin2  = ($zf_filter_input->isValid('admin2')) ? $zf_filter_input->admin2 : $this->_getParam('admin2');
				$this->view->emailadmin2  = ($zf_filter_input->isValid('emailadmin2')) ? $zf_filter_input->emailadmin2 : $this->_getParam('emailadmin2');
				$this->view->phoneadmin2  = ($zf_filter_input->isValid('phoneadmin2')) ? $zf_filter_input->phoneadmin2 : $this->_getParam('phoneadmin2');
				$this->view->tokenadmin2  = ($zf_filter_input->isValid('tokenadmin2')) ? $zf_filter_input->tokenadmin2 : $this->_getParam('tokenadmin2');

				$this->view->cust_rdn_key  = ($zf_filter_input->isValid('cust_rdn_key')) ? $zf_filter_input->cust_rdn_key : $this->_getParam('cust_rdn_key');
				$this->view->cust_role_adm = ($zf_filter_input->isValid('cust_role_adm')) ? $zf_filter_input->cust_role_adm : $this->_getParam('cust_role_adm');

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html 
				$errorArray = null;
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$errorArray[$keyRoot] = $errorString;
					}
				}

				//pengaturan error untuk cust emobile, phone dan fax
				if ($flag_cust_phone == 'F')    $errorArray['cust_phone'] = $this->language->_('Invalid phone number format');
				if ($flag_cust_fax == 'F')      $errorArray['cust_fax'] = $this->language->_('Invalid fax number format');
				//END pengaturan error untuk cust emobile, phone dan fax

				if (isset($cek_multiple_email) && $cek_multiple_email == false) $errorArray['cust_email'] = $this->language->_('Invalid format');

				$this->view->customer_msg  = $errorArray;

				if (!empty($this->_getParam('cust_securities'))) {
					$this->view->cust_securities = 	$this->_getParam('cust_securities');
				} else {
					$this->view->cust_securities = '0';
				}
			}
		} // END if($this->_request->isPost())
		else {
			//$resultdata = $this->getCustomer($cust_id);  
			$this->view->business_typeArr 		= $this->getBusinessEntity();
			$this->view->cust_typeArr 			= $this->getCompanyType();
			$this->view->debitur_codeArr 		= $this->getDebitur();
			$this->view->cust_cityArr 			= $this->getCity();
			$this->view->country_codeArr 		= $this->getCountry();
			$this->view->collectibility_codeArr = $this->getCreditQuality();

			$this->view->go_publicArr = [['VALUE' => "Y", "DESC" => "Ya"], ["VALUE" => "N", "DESC" => "Tidak"]];
			$this->view->bumn_grupArr = [['VALUE' => "Y", "DESC" => "Ya"], ["VALUE" => "N", "DESC" => "Tidak"]];

			$this->view->temp_cust_model = $resultdata['CUST_MODEL'];

			if ($resultdata['CUST_MODEL'] == 1) {
				$resultdata['CUST_MODEL'] = "Pemohon";
			} elseif ($resultdata['CUST_MODEL'] == 2) {
				$resultdata['CUST_MODEL'] = "Asuransi";
			} else {
				$resultdata['CUST_MODEL'] = "Special Obligee";
			}

			$this->view->cust_model     = $resultdata['CUST_MODEL'];
			$this->view->cust_id        = strtoupper($resultdata['CUST_ID']);
			$this->view->cust_cif       = $resultdata['CUST_CIF'];
			$this->view->cust_name      = $resultdata['CUST_NAME'];
			$this->view->cust_npwp      = $resultdata['CUST_NPWP'];
			$this->view->cust_type      = $resultdata['CUST_TYPE'];
			$this->view->business_type      = $resultdata['BUSINESS_TYPE'];
			$this->view->cust_code 		= $resultdata['CUST_CODE'];
			$this->view->go_public = $resultdata['GO_PUBLIC'];
			$this->view->grup_bumn = $resultdata['GRUP_BUMN'] == 1 ? "Y" : "N";
			$this->view->debitur_code = $resultdata['DEBITUR_CODE'];
			$this->view->collectibility_code = $resultdata['COLLECTIBILITY_CODE'];
			$this->view->cust_district = $resultdata['CUST_DISTRICT'];
			$this->view->cust_village = $resultdata['CUST_VILLAGE'];
			$this->view->cust_zip       = $resultdata['CUST_ZIP'];
			$this->view->cust_address   = $resultdata['CUST_ADDRESS'];
			$this->view->cust_city      = $resultdata['CUST_CITY'];
			$this->view->cust_contact   = $resultdata['CUST_CONTACT'];
			$this->view->cust_contact_phone   = $resultdata['CUST_CONTACT_PHONE'];
			$this->view->cust_email     = $resultdata['CUST_EMAIL'];
			$this->view->cust_phone     = $resultdata['CUST_PHONE'];
			$this->view->cust_website   = $resultdata['CUST_WEBSITE'];
			$this->view->country_code   = $resultdata['COUNTRY_CODE'];
			$this->view->approver   = $resultdata['CUST_APPROVER'];
		}

		$this->view->modulename    = $this->_request->getModuleName();
		$this->view->customer_type   =  $this->_custType;
		$this->view->lengthCustId  = $this->_custIdLength;
		$this->view->approve_type  =  $this->_masterhasStatus;



		//insert log
		try {
			$this->_db->beginTransaction();

			Application_Helper_General::writeLog('CCUD', 'Edit Customer [' . $this->view->cust_id . ']');

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollBack();
			Application_Log_GeneralLog::technicalLog($e);
		}
	}


	// public function cifaccountAction()
	// {
	// 	$this->_helper->viewRenderer->setNoRender();
	// 	$this->_helper->layout()->disableLayout();

	// 	$cust_cif   = $this->_getParam('cust_cif');
	// 	$changes_id = $this->_getParam('changes_id');

	// 	$app = Zend_Registry::get('config');
	// 	$app = $app['app']['bankcode'];

	// 	$core = array();
	// 	//var_dump($cust_cif);

	// 	$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $cust_cif);
	// 	$result = $svcAccountCIF->inquiryCIFAccount();
	// 	$core =  $result['accounts'];
	// 	//var_dump($result);
	// 	//die;
	// 	// DATA TEMP CUSTOMER ACCOUNT
	// 	$select = $this->_db->select()
	// 		->from('TEMP_CUSTOMER_ACCT')
	// 		->where('CHANGES_ID = ?', $changes_id);
	// 	$accountInfo = $this->_db->fetchAll($select);

	// 	$accountInfoArr = [];
	// 	if ($accountInfo) {
	// 		foreach ($accountInfo as $row) {
	// 			$accountInfoArr[] = $row['ACCT_NO'];
	// 		}
	// 	}

	// 	if ($result['response_code'] == '0000') {
	// 		$arrResult = array();
	// 		$err = array();

	// 		if (count($core) > 1) {
	// 			foreach ($core as $key => $val) {
	// 				//if($val->status == '0'){
	// 				$arrResult = array_merge($arrResult, array($val['account_number'] => $val));
	// 				array_push($err, $val['account_number']);
	// 				//}
	// 			}
	// 		} else {
	// 			foreach ($core as $data) {
	// 				//if($data->status == '0'){
	// 				$arrResult = array_merge($arrResult, array($data['account_number'] => $data));
	// 				array_push($err, $data['account_number']);
	// 				//}
	// 			}
	// 		}
	// 		echo '<pre>';
	// 		//var_dump($arrResult);die;
	// 		$final = $arrResult;
	// 		if (count($final) < 1) {
	// 			$this->view->data1 = '0';
	// 		} else {
	// 			$this->view->data = $arrResult;
	// 		}

	// 		$dataProPlan = array('xxxxx');
	// 		foreach ($final as $dataProductPlan) {
	// 			// print_r($dataProductPlan);die;
	// 			//if($dataProductPlan->status == '0'){
	// 			if (isset($dataProductPlan['type'])) {
	// 				$select_product_type  = $this->_db->select()
	// 					->from(array('M_PRODUCT_TYPE'), array('PRODUCT_NAME'))
	// 					->where("PRODUCT_CODE = ?", $dataProductPlan['type']);

	// 				$userData_product_type = $this->_db->fetchAll($select_product_type);

	// 				if (!empty($userData_product_type)) {
	// 					$dataProductPlan['type_desc'] = $userData_product_type[0]['PRODUCT_NAME'];
	// 					$dataUser[] = $dataProductPlan;
	// 				}
	// 			}


	// 			//}
	// 		}
	// 	}

	// 	$optHtml = '<table cellpadding="3" cellspacing="0" border="0" class="table table-sm table-striped">
	// 				<thead>
	// 				<tr class="headercolor">
	// 				<th width="3%" valign="top" class="tablehead fth-header"></th>
	// 				<th  valign="top" class="tablehead fth-header">' . $this->language->_('Account Number') . '</th>
	// 				<th  valign="top" class="tablehead fth-header">' . $this->language->_('Currency') . '</th>
	// 				<th  valign="top" class="tablehead fth-header">' . $this->language->_('Type') . '</th>
	// 				</tr>
	// 				</thead>
	// 				<tbody id="accountBody">';


	// 	if (!empty($dataUser)) {
	// 		foreach ($dataUser as $key => $row) {
	// 			$i = $i + 1;
	// 			$acct_source = 3;
	// 			$acct_desc   = $row['type_desc'];

	// 			if ($row['currency'] == '000') {
	// 				$cekIDR = 'IDR';
	// 			}

	// 			if (in_array($row['account_number'], $accountInfoArr)) {
	// 				$checked = 'checked';
	// 			} else {
	// 				$checked = '';
	// 			}

	// 			$button_checkbox = "<input type='checkbox' name='req_id[]' id='" . $row['account_number'] . "' value='" . $row['account_number'] . "'>";
	// 			$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
	// 			if ($row['account_number'] != '') {
	// 				$optHtml .= '<tr>
	// 			<td class="' . $td_css . '"><input type="checkbox" name="req_id[]" id="' . $row['account_number'] . '" class="cekbok" value="' . $row['account_number'] . '" ' . $checked . '></td>
	// 			<td class="' . $td_css . '"><input type="hidden" name="accountName' . $row['account_number'] . '" id="accountName' . $row['account_number'] . '" value="' . $row['account_number'] . '">' . $row['account_number'] . '</td>
	// 			<td class="' . $td_css . '"><input type="hidden" name="ccy' . $row['account_number'] . '" id="ccy' . $row['account_number'] . '" value="' . $cekIDR . '">' . $row['currency'] . '</td>
	// 			<td class="' . $td_css . '">' . $acct_desc . '<input type="hidden" name="acct_desc' . $row['account_number'] . '" id="acct_desc' . $row['account_number'] . '" value="' . $acct_desc . '"><input type="hidden" name="productType' . $row['account_number'] . '" value="' . $row['type'] . '"><input type="hidden" name="planCode' . $row['account_number'] . '" value="' . $row['type'] . '"><input type="hidden" name="ccy' . $row['account_number'] . '" value="' . $row['currency'] . '"><input type="hidden" name="productName' . $row['account_number'] . '" id="productName' . $row['account_number'] . '" value="' . $acct_desc . '">
	// 			</td>
	// 			</tr>';
	// 			}
	// 		}
	// 	} else {
	// 		$optHtml .= '<tr><td colspan="5" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
	// 	}

	// 	$optHtml .= '</tbody></table>';
	// 	echo $optHtml;
	// }

	public function cifaccountAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$cust_cif   = $this->_getParam('cust_cif');
		$changes_id = $this->_getParam('changes_id');

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$core = array();
		//var_dump($cust_cif);

		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $cust_cif);
		$result = $svcAccountCIF->inquiryCIFAccount();


		$core =  $result['accounts'];
		//var_dump($result);
		//die;
		// DATA TEMP CUSTOMER ACCOUNT
		$select = $this->_db->select()
			->from('TEMP_CUSTOMER_ACCT')
			->where('CHANGES_ID = ?', $changes_id);
		$accountInfo = $this->_db->fetchAll($select);

		$accountInfoArr = [];
		if ($accountInfo) {
			foreach ($accountInfo as $row) {
				$accountInfoArr[] = $row['ACCT_NO'];
			}
		}


		if ($result['response_code'] == '0000') {
			$arrResult = array();
			$err = array();

			if (count($core) > 1) {
				foreach ($core as $key => $val) {
					//if($val->status == '0'){
					$arrResult = array_merge($arrResult, array($val['account_number'] => $val));
					array_push($err, $val['account_number']);
					//}
				}
			} else {
				foreach ($core as $data) {
					//if($data->status == '0'){
					$arrResult = array_merge($arrResult, array($data['account_number'] => $data));
					array_push($err, $data['account_number']);
					//}
				}
			}

			$final = $arrResult;
			if (count($final) < 1) {
				$this->view->data1 = '0';
			} else {
				$this->view->data = $arrResult;
			}

			$dataProPlan = array('xxxxx');

			$getAllProduct = $this->_db->select()
				->from("M_PRODUCT")
				->query()->fetchAll();

			foreach ($final as $dataProductPlan) {

				$svcAccount = new Service_Account($dataProductPlan['account_number'], null);

				$aro = true;
				$specialRate = false;
				$status = true;
				$hold = false;

				if ($dataProductPlan['type'] == 'T') {
					$result = $svcAccount->inquiryDeposito();
					$aro = ($result['aro_status'] == 'Y') ? true : false;
					$specialRate = ($result['varian_rate'] != '0') ? true : false;
					$hold = (strtolower($result['hold_description']) == 'y') ? true : false;
				} else {
					$result = $svcAccount->inquiryAccountBalance();
				}

				$status = ($result['status'] != 1 && $result['status'] != 4) ? false : true;

				$accountTypeCheck = false;
				$productTypeCheck = false;

				foreach ($getAllProduct as $key => $value) {
					# code...
					if ($value['PRODUCT_CODE'] == $dataProductPlan['type']) {
						$accountTypeCheck = true;
					};

					if ($value['PRODUCT_PLAN'] == $dataProductPlan['product_type']) {
						$productTypeCheck = true;
					};
				}

				$dataProductPlan['special_rate'] = $specialRate;

				// print_r($dataProductPlan);die;
				//if($dataProductPlan->status == '0'){
				if (isset($dataProductPlan['type'])) {

					if ($result['response_code'] == '0000' && $aro && $status && $accountTypeCheck && $productTypeCheck && !$hold) {
						$dataUser[] = $dataProductPlan;
					}

				}
			}
		}

		$optHtml = '<table cellpadding="3" cellspacing="0" border="0" class="table table-sm table-striped" id="myTable">
		<thead>
		<tr class="headercolor">
		<th width="3%" valign="top" class="tablehead fth-header"></th>
		<th  valign="top" class="tablehead fth-header" style="width: 25%">' . $this->language->_('Account Number') . '</th>
		<th  valign="top" class="tablehead fth-header" style="width: 25%">' . $this->language->_('Currency') . '</th>
		<th  valign="top" class="tablehead fth-header" style="width: 25%">' . $this->language->_('Tipe Akun') . '</th>
		<th  valign="top" class="tablehead fth-header" style="width: 25%">' . $this->language->_('Tipe Produk') . '</th>
		</tr>
		</thead>
		<tbody id="accountBody">';


		if (!empty($dataUser)) {
			foreach ($dataUser as $key => $row) {
				$specialRateText = "";

				if ($row['special_rate']) {
					$specialRateText = ' <div class="text-danger ml-2 special_rate" style="border: 1px solid red;border-radius: 100%;display: inline-block;width: 20px;height: 20px;cursor: default;"><center>i</center></div><div style="display:none; position: relative" class="ml-2">&nbsp; <div style="background-color: white;border: 1px solid red;position: absolute;width: 300px;top:0" class="text-danger">Special Rate</div> </div>';
				}
				$i = $i + 1;
				$acct_source = 3;
				$acct_desc   = $row['type_desc'];

				if ($row['currency'] == '000') {
					$cekIDR = 'IDR';
				}

				if (in_array($row['account_number'], $accountInfoArr)) {
					$checked = 'checked';
				} else {
					$checked = '';
				}

				$button_checkbox = "<input type='checkbox' name='req_id[]' id='" . $row['account_number'] . "' value='" . $row['account_number'] . "'>";
				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
				if ($row['account_number'] != '') {
					$optHtml .= '<tr>
					<td class="' . $td_css . '"><input type="checkbox" name="req_id[]" id="' . $row['account_number'] . '" class="cekbok" value="' . $row['account_number'] . '" ' . $checked . '></td>
					<td class="' . $td_css . '"><input type="hidden" name="accountName' . $row['account_number'] . '" id="accountName' . $row['account_number'] . '" value="' . $row['account_number'] . '">' . $row['account_number'] . '</td>
					<td class="' . $td_css . '"><input type="hidden" name="ccy' . $row['account_number'] . '" id="ccy' . $row['account_number'] . '" value="' . $cekIDR . '">' . $row['currency'] . '</td>
					<td class="' . $td_css . '">' . $acct_desc . '' . $specialRateText . '<input type="hidden" name="acct_desc' . $row['account_number'] . '" id="acct_desc' . $row['account_number'] . '" value="' . $acct_desc . '"><input type="hidden" name="productType' . $row['account_number'] . '" value="' . $row['type'] . '"><input type="hidden" name="planCode' . $row['account_number'] . '" value="' . $row['type'] . '"><input type="hidden" name="ccy' . $row['account_number'] . '" value="' . $row['currency'] . '"><input type="hidden" name="productName' . $row['account_number'] . '" id="productName' . $row['account_number'] . '" value="' . $acct_desc . '">
					<td class="' . $td_css . '">' . $row['product_type'] . '</td>
					</td>
					</tr>';
				}
			}
		} else {
			$optHtml .= '<tr><td colspan="5" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}

		$optHtml .= '</tbody></table>';
		echo $optHtml;
	}


}
