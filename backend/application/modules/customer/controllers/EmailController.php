<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once ('Crypt/AES.php');

class customer_EmailController extends Application_Main
{
	const actual_link = 'https://360.espay.id';
	public function initController(){
		  $this->_helper->layout()->setLayout('popup');
	 }
	public function indexAction()
	{
		
		
		$set = new Settings();
		$templateEmailMasterBankAddress = $set->getSetting('master_bank_address');
		$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
		$templateEmailMasterBankAppUrl = $set->getSetting('master_bank_app_url');
		$templateEmailMasterBankCity = $set->getSetting('master_bank_city');
		$templateEmailMasterBankCountry = $set->getSetting('master_bank_country');
		$templateEmailMasterBankEmail = $set->getSetting('master_bank_email');
		$templateEmailMasterBankEmail1 = $set->getSetting('master_bank_email1');
		$templateEmailMasterBankFax = $set->getSetting('master_bank_fax');
		$templateEmailMasterBankName = $set->getSetting('master_bank_name');
		$templateEmailMasterBankProvince = $set->getSetting('master_bank_province');
		$templateEmailMasterBankTelp = $set->getSetting('master_bank_telp');
		$templateEmailMasterBankWapp = $set->getSetting('master_bank_wapp');
		$templateEmailMasterBankWebsite = $set->getSetting('master_bank_website');
		
		$user_isnew = $this->_getParam('user_isnew');
		
//		$FEmailResetPass = $set->getSetting('femailtemplate_resetpwd');
		
		if($user_isnew == 1){
			$FEmailResetPass = $set->getSetting('femailtemplate_newuser');
		}
		else{
			$FEmailResetPass = $set->getSetting('femailtemplate_resetpwd');
		}

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $user_id = $AESMYSQL->decrypt($this->_getParam('user_id'), $password);
		
		$cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
		//echo $user_id;
		//echo $cust_id;die;
		// echo $FEmailResetPass;die;
		
		
		$isi = $this->_db->SELECT ()
						->FROM ('M_USER', array('USER_ID','CUST_ID','USER_EMAIL','USER_FULLNAME','USER_CLEARTEXT_PWD'))
						->WHERE ('USER_ID = ? ', $user_id)
						->WHERE ('CUST_ID = ? ',$cust_id);
		//echo $isi;die;
		$isi = $this->_db->fetchrow($isi);
		$actual_link = $_SERVER['SERVER_NAME'];
		$url_fo = $set->getSetting('url_fo');
		$key = md5 ('permataNet92');
		$encrypt = new Crypt_AES ();
		// $user = ( $encrypt->encrypt ( $user_id ) );
		//$user = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $user_id, MCRYPT_MODE_ECB);
		$user = ( $this->sslEnc ( $user_id ) );
		$cust = ( $this->sslEnc ( $isi['CUST_ID'] ) );
		

		$str=rand(); 
		$rand = md5($str); 
			// echo $result; die;
	 	$newPassword = $url_fo.'/pass/index?safetycheck=&code='.urldecode($rand).'&cust_id='.urlencode($cust).'&user_id='.urlencode($user);
 		// $newPassword = $actual_link.':2019/pass/index?safetycheck=&cust_id='.urlencode($cust).'&user_id='.urlencode($user);
 		// print_r($newPassword);die;
 		$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
		$data = array( '[[user_fullname]]' => $isi['USER_FULLNAME'],
								'[[comp_accid]]' => $isi['CUST_ID'],
								'[[user_login]]' => $isi['USER_ID'],
								'[[exp_date]]'					=> $datenow,
								'[[user_email]]' => $isi['USER_EMAIL'],
								'[[confirm_link]]' => $newPassword,
								'[[master_bank_name]]' => $templateEmailMasterBankName,
								'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
								'[[master_bank_email]]' => $templateEmailMasterBankEmail,
								'[[master_bank_app_name]]'		=> $templateEmailMasterBankAppName,
								'[[master_bank_wapp]]' => $templateEmailMasterBankWapp
		
								);
		
		$FEmailResetPass = strtr($FEmailResetPass,$data);
		$date = date('Y-m-d h:i:s', strtotime("+1 days"));

		$data = array(
									'USER_RPWD_ISEMAILED' 			=> 1,
									'USER_DATEPASS'					=> $date,
									// 'USER_RRESET'					=> 1,
								);
		// print_r($data);die;
		$where =  array();
		$where['CUST_ID 		 = ?'] 	= $isi['CUST_ID'];
		$where['USER_ID 		 = ?'] 	= $user_id;
		$this->_db->update('M_USER',$data,$where);
		
		// echo $FEmailResetPass;die;
		 //echo $isi['USER_EMAIL'];die;
		$status = Application_Helper_Email::sendEmail($isi['USER_EMAIL'],'Reset Password Information',$FEmailResetPass);
		$this->view->status = $status;
		$statusDesc = ($status == 1) ? 'Success' : 'Failed';
		Application_Helper_General::writeLog('CSRL',$statusDesc.' Emailing FO User Password : Cust Id ( '.$this->_custIdLogin.' ) ,User Id ( '.$user_id.' )');

		
	}
	
	public function sslPrm()
	{
	return array("6a1f325be4c0492063e83a8cb2cb9ae7","IV (optional)","aes-128-cbc");
	}

	public function sslEnc($msg)
	{
	  list ($pass, $iv, $method)=$this->sslPrm();
		 return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}

	// function encrypt($pure_string) {
 //    $dirty = array("+", "/", "=");
 //    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
 //    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
 //    // $_SESSION['iv'] = mcrypt_create_iv($iv_size, MCRYPT_RAND);
 //    // print_r($_SESSION['iv']);die;
 //    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, 'permataNet92', utf8_encode($pure_string), MCRYPT_MODE_ECB, 'permataNet92');
 //    $encrypted_string = base64_encode($encrypted_string);
 //    return str_replace($dirty, $clean, $encrypted_string);
	// }
}
?>