<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';
class tools_PaymentongoinghistoryController extends Application_Main {
	
	public function initController(){
	}
	
	public function indexAction() 
	{
		$paymentRef = $this->_getParam('paymentref');
		$periodic = $this->_getParam('periodic'); 

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 

		// echo 'asdasdsa';die();
 
		$select = $this->_db->select()
      		->from(array('A' => 'T_PSLIP'), array('A.PS_PERIODIC','A.PS_TYPE'))
      		//->where('A.PS_PERIODIC = ?', $periodic)
      		->where('A.PS_NUMBER = ?', $paymentRef);
		
		//echo $select;die;
		
      	$data = $this->_db->fetchAll($select); 
		
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$this->view->arrPayStatus 	= $arrPayStatus;
		
		$this->view->pstype = $data['0']['PS_TYPE'];
		
		$psnumbArr = array();
		if(!empty($data['0']['PS_PERIODIC'])){

			

			$selectnumber	= $this->_db->select()
				->from(
					array('A'	 		=> 'T_PSLIP'),
					array(
						'PS_NUMBER' 	=> 'A.PS_NUMBER'
					)
				)
			->where("A.PS_PERIODIC	= ?", $data['0']['PS_PERIODIC']);
			//->where("A.ACCT_STATUS 	= 1");
			
			$data = $this->_db->fetchAll($selectnumber);

			//var_dump($data);die;
			foreach ($data as $key => $value) {
				$psnumbArr[] = $value['PS_NUMBER'];
			}

		}else{
			$psnumbArr[] = $paymentRef;
		}
		//var_dump($psnumbArr);
		$listHistory = array();
		foreach($psnumbArr as $k => $valnumb){
			//var_dump($valnumb);
				$Payment = new 	Payment($valnumb);
				$lastHistory =  $Payment->getHistory();
				$listHistory = array_merge($listHistory, $lastHistory);
		}
		//echo '<pre>';
		//var_dump($listHistory);die;
		/*
      	//pertama
		$Payment = new 	Payment($paymentRef);
		$listHistory[] =  $Payment->getHistory();

		//kedua dst
		if (!empty($paymentRefArr)) {
			foreach ($paymentRefArr as $key => $value) {
				$Payment = new 	Payment($value['PS_NUMBER']);
				$listHistory[] =  $Payment->getHistory();
			}
			
			foreach ($listHistory as $key => $value) {
				if ($key > 0) {
					$count = count($listHistory[0]) + 1;
					$listHistory[0][$count] = $listHistory[$key][0];  
					unset($listHistory[$key]);
				}
			}
		}

		$this->view->paymentHistory =  $listHistory[0]; */
		$this->view->paymentHistory =  $listHistory;
		//$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;
		
	}
}

