<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class bintemplate_DeleteController extends bintemplate_Model_Bintemplate
{
  public function indexAction()
  {
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $temp_code = $AESMYSQL->decrypt($this->_getParam('temp_code'), $password);

    if ($temp_code) {

      $temp_data = $this->getBinTemplate($temp_code);
      $info = 'BIN';
      try {
        $this->_db->beginTransaction();

        $temp_data['BIN_SUGGESTED']   = new Zend_Db_Expr('now()');
        $temp_data['BIN_SUGGESTEDBY'] = $this->_userIdLogin;

        //insert ke T_GLOBAL_CHANGES
        $change_id = $this->suggestionWaitingApproval('Bin Template', $info, $this->_changeType['code']['delete'], null, 'BIN_TEMPLATE', 'TEMP_BIN_TEMPLATE', 'BIN TEMPLATE', $zf_filter_input->template_name);
        
        $this->insertTempBinTemplateUpdate($change_id, $temp_data, $temp_code);

        //log CRUD
        Application_Helper_General::writeLog('BNUD', 'BIN Template has been updated (delete), Template Code : B' . str_pad($temp_code, 3, '0', STR_PAD_LEFT).' ,Change id : ' . $change_id);

        $this->_db->commit();

        $ns = new Zend_Session_Namespace('FVC');
        $ns->backURL = '/bintemplate';
        $this->_redirect('/notification/submited/index');
        //$this->_redirect('/notification/success/index');
      } catch (Exception $e) {
        $this->_db->rollBack();
        // $error_remark = $this->getErrorRemark('82');
        // Application_Log_GeneralLog::technicalLog($e);
      }
    }


    if (!$cust_id) $error_remark = $this->getErrorRemark('22', 'Customer ID');
    if ($error_remark) {
      $msg = $error_remark;
      $class = 'F';
    } else {
      $class = 'S';
      $msg = $this->getErrorRemark('00', 'Customer ID', $cust_id);
    }

    //insert log
    try {
      $this->_db->beginTransaction();

      Application_Helper_General::writeLog('BNUD', 'view delete');

      $this->_db->commit();
    } catch (Exception $e) {
      $this->_db->rollBack();
      Application_Log_GeneralLog::technicalLog($e);
    }

    $this->_helper->getHelper('FlashMessenger')->addMessage($class);
    $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
  }
  public function getBinTemplate($temp_code)
  {
    $select = $this->_db->select()
      ->from(array('A' => 'BIN_TEMPLATE'), array('*'))
      ->where('UPPER(A.TEMP_CODE)=' . $this->_db->quote((string) $temp_code))
      ->query()->fetch();
    return $select;
  }
}
