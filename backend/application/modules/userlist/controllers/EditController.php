<?php

require_once 'Zend/Controller/Action.php';

class userlist_EditController extends Application_Main 
{
	public function indexAction() 
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		 $user_id = strtoupper($this->_getParam('user_id'));
   
       if($user_id)
       {
		
		$select	= $this->_db->select()
								->distinct()
								->from	(
											array('MB'=>'M_BUSER'),
											array(
													'BUSER_ID'=>'MB.BUSER_ID',
													'BUSER_STATUS'=>'MB.BUSER_STATUS',
													'BUSER_NAME'=>'MB.BUSER_NAME',
													'BUSER_EMAIL'=>'MB.BUSER_EMAIL',
													'BGROUP_ID'=>'MB.BGROUP_ID',
													'BUSER_ISLOCKED'=>'MB.BUSER_ISLOCKED',
													'BUSER_LASTLOGIN'=>'MB.BUSER_LASTLOGIN',
													'BGROUP_DESC'=>'MG.BGROUP_DESC'
												)
										)
								->joinLeft	(
												array('MG' => 'M_BGROUP'),
												'MB.BGROUP_ID = MG.BGROUP_ID',
												array()
											)
								->where('MB.BUSER_ID =?',$user_id);
						
		$resultdata = $this->_db->fetchAll($select);
				
				if($resultdata[0]['BUSER_ID'])
				  {
						$this->view->userId      = strtoupper($resultdata[0]['BUSER_ID']);
						$this->view->groupId     = $resultdata[0]['BGROUP_ID'];
						$this->view->status    = $resultdata[0]['BUSER_STATUS'];
						$this->view->name    = $resultdata[0]['BUSER_NAME'];
						$this->view->email = $resultdata[0]['BUSER_EMAIL'];
						$this->view->description    = "-";
						$this->view->failed     = "-";
						$this->view->userislogoin = $resultdata[0]['BUSER_LASTLOGIN'];
						$this->view->userislocked  = $resultdata[0]['BUSER_ISLOCKED'];
						$this->view->createdate    = "-";
						$this->view->creator      = "-";
						$this->view->last_suggestion      = "-";
						$this->view->last_suggestor    = "-";
						$this->view->last_approval  = "-";
						$this->view->last_approver  = "-";
							
					 
				   }
				   else $user_id = null; 
		}
		
		
		
		
		
		$selectGroup =	$this->_db->select()
									->from(	'M_BGROUP',
										array(
												'BGROUP_ID','BGROUP_DESC'));
									//->where('USER_ID = \''.$user_id.'\'')		
									//->where('PASSWORD = \''.$v_password_old.'\'');
				
				// echo $v_password;
		$group = $this->_db->fetchAll($selectGroup);  
		$arrgroup = Application_Helper_Array::listArray($group,'BGROUP_ID','BGROUP_DESC');  
		$this->view->arrGroup = $arrgroup;
		$flip = Application_Helper_Array::listArray($group,'BGROUP_DESC','BGROUP_ID');  
//Zend_Debug::dump($arrgroup);die;
		if($this->_request->isPost())
		{

			$filters = array(
								'userId'		=> array('StripTags', 'StringTrim'),
								'name'	=> array('StripTags', 'StringTrim'),
								'email' => array('StripTags', 'StringTrim'),
								//'description' => array('StripTags', 'StringTrim'),
								'groupId'  => array('StripTags', 'StringTrim')
							);

			$validators = array(
									'userId' => array(	
															'NotEmpty',
															'Alnum',
															array('StringLength', array('min' => 5)),
															'messages' => array(
																					$this->getErrorRemark('01', 'User ID'),
																					$this->getErrorRemark('04', 'User ID'),
																					$this->getErrorRemark('04', 'User ID'),
																				) 													   					   
														),
									'name' => array(
															'NotEmpty',
															array('StringLength', array('max' => 20)),
															'messages' => array(
																					$this->getErrorRemark('01', 'Name'),
																					$this->getErrorRemark('04', 'Name'),
																				)
														),														
									'email' => array(
															new SGO_Validate_EmailAddress(),
															'allowEmpty' => true,
															'messages' => array(																																								
																					$this->getErrorRemark('04', 'Email'),
																				)
														),	
									'groupId' => array(
																array('InArray',$flip),
                                                                'messages' => array(																																								
																					$this->getErrorRemark('01','Please Select Group'),
																				)
														),																																					
								);
			$params = $this->_request->getParams();
			$zf_filter_input = new Zend_Filter_Input($filters, $validators,$params, $this->_optionsValidator);
			
			$groupid = $this->_request->getParam('groupId');
			$valid = new Zend_Validate_Identical(array($groupid => '0'));
//			Zend_Debug::dump($arrgroup);die;
//			CEK HURUF DAN ANGKA DI USERID			
			$strtemp = 0;
			$digittemp = 0;					
			$user_id = $zf_filter_input->userId;
			$str = str_split($user_id);			
			foreach($str as $string){
				if((Zend_Validate::is($string,'Digits')) ? true : false)
				{$digittemp = 1;}
				if((Zend_Validate::is($string,'Alpha')) ? true : false)
				{$strtemp = 1;}
			}
			
//			echo $strtemp.' '.$digittemp;die;
//			CEK GROUPID
//			$group = (($this->_getParam('groupId')==0) ? false : true);
						
			if($zf_filter_input->isValid() && $digittemp == 1 && $strtemp == 1)
			{						
				
				$email = $zf_filter_input->email;
				
				$selectmaster =	$this->_db->select()
									->from('M_BUSER',array('BUSER_ID'))
									->where('BUSER_ID = ?',$user_id)
									->query()->fetchAll();
				$selecttemp = $this->_db->select()
									->from('TEMP_BUSER')
									->where('USER_ID = ?',$user_id)
									->query()->fetchAll();
				
				if(count($selectmaster)==0 && count($selecttemp) == 0) 
				{
					$info = 'User ID = '.$params['userId'].', User Name = '.$params['name'];
					$change_id = $this->suggestionWaitingApproval('User Backend',$info,'E',null,'M_BUSER','TEMP_BUSER','USER_ID',$params['userId']);
					
					$this->_db->beginTransaction();	
						try
						{							
							$insertArr = array(
								'CHANGES_ID' 		=> $change_id, 
								'USER_ID'			=> $zf_filter_input->userId, 
								'USER_NAME' 		=> $zf_filter_input->name, 
								'BGROUP_ID' 		=> $zf_filter_input->groupId,								
								'USER_EMAIL'		=> $zf_filter_input->email
							);			
							
							$this->_db->insert('TEMP_BUSER', $insertArr);
							$this->_db->commit();
							//$this->_redirect('/notification/success/index');
							$this->view->success 	= true;
							$this->view->successMsg = $this->view->formErrors(array($this->getErrorRemark('00', 'Add New Backend User')));
						}
						catch(Exception $e)
						{
							$this->_db->rollBack();
							$errorRemark = $this->getErrorRemark('82');
							$this->view->errorMsg = $errorRemark;
						//	SGO_Helper_GeneralLog::technicalLog($e);
						}
					
				}
				else if(count($selectmaster)>0)
				{		
					$this->view->error 			= true;		
					$errors 					= "$user_id is already taken ";
					$this->view->errorMsg 		= $errors;
				}
				else if(count($selecttemp)>0)
							{		
					$this->view->error 			= true;		
					$errors 					= "$user_id is already registered";
					$this->view->errorMsg 		= $errors;
							}
			}
			else
			{
				$this->view->error 			= true;	
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->errorMsg = $docErr;
				$this->view->userId 	= ($zf_filter_input->isValid('userId'))? $zf_filter_input->userId : $params['userId'];
				$this->view->name	 	= ($zf_filter_input->isValid('name'))? $zf_filter_input->name : $params['name'];
				$this->view->description = ($zf_filter_input->isValid('description'))? $zf_filter_input->description : $params['description'];
				$this->view->email	 	= ($zf_filter_input->isValid('email'))? $zf_filter_input->email : $params['email'];
				$this->view->groupId	 	= ($zf_filter_input->isValid('groupId'))? $zf_filter_input->groupId : $params['groupId'];
//				echo $docErr,die;
			}							
			
		}
		
		
		
	}
}
