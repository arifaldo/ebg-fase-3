<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class predefinedbeneficiary_ApprovedomesticacctController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{		
		$fields = array(
						'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => 'Alias Name',
											   'sortable' => true),
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => 'Beneficiary Account',
											   'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => 'Beneficiary Account Name',
											   'sortable' => true),
						'address'   => array('field'     => 'BENEFICIARY_ADDRESS',
											  'label'    => 'Address',
											  'sortable' => true),
						'citizenship'   => array('field' => 'BENEFICIARY_CITIZENSHIP',
											  'label'    => 'Citizenship',
											  'sortable' => true),
						'bankname'   => array('field'    => 'BANK_NAME',
											  'label'    => 'Bank Name',
											  'sortable' => true),
						'act'   => array('field'    	 => 'ACTION',
											  'label'    => 'Action',
											  'sortable' => true)
				);
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$select = $this->_db->select()
					   ->from('M_BENEFICIARY',array('BENEFICIARY_ID','BENEFICIARY_ALIAS','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BENEFICIARY_ADDRESS','BENEFICIARY_CITIZENSHIP' => '(CASE BENEFICIARY_CITIZENSHIP WHEN \'R\' THEN \'Resident\' WHEN \'NR\' THEN \'Non Resident\' END)','BANK_NAME','BENEFICIARY_ISREQUEST_DELETE','BENEFICIARY_ISAPPROVE','ACTION' => '(CASE WHEN BENEFICIARY_ISREQUEST_DELETE = 1 THEN \'Delete\' WHEN BENEFICIARY_ISAPPROVE = 0 THEN \'Add\' END)')); 
		$select->where("BENEFICIARY_TYPE = 2");
		$select->where("BENEFICIARY_ISREQUEST_DELETE = 1 OR BENEFICIARY_ISAPPROVE = 0");
		$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
	    $select->order($sortBy.' '.$sortDir);   
		
		$this->paging($select);
		$this->view->fields = $fields;
		
		if($this->_request->isPost() )
		{
			//Zend_Debug::Dump($this->getRequest()->getParams());die;
			$Beneficiary = new Beneficiary();
			$param = $this->getRequest()->getParams();
			$benef_id = $this->getRequest()->getParam('benef_id');
			$act = $this->getRequest()->getParam('act');
			$submit = $this->getRequest()->getParam('submit');
			if(!array_key_exists('1',array_flip($benef_id)))
			{				
				$errorMsg = 'Error: Please checked selection.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
			}
			else
			{
				foreach($benef_id as $key => $value)
				{
					if($value=='0') unset($benef_id[$key]);
				}
				//Zend_Debug::Dump($benef_id);die;
				if($submit=='Approve')
				{
					foreach($benef_id as $key => $value)
					{
						if($act[$key]=='Delete') $Beneficiary->delete($key);
						else if($act[$key]=='Add') $Beneficiary->approve($key);
					}
				}
				else if($submit=='Reject')
				{
					foreach($benef_id as $key => $value)
					{
						if($act[$key]=='Delete') $Beneficiary->rejectDelete($key);
						else if($act[$key]=='Add') $Beneficiary->delete($key);
					}
				}
				//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
				$this->_redirect('/notification/success');
			}
			
			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}	
			}
		}
	}
}
