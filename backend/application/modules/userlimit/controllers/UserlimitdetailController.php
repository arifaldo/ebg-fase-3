<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'Crypt/AESMYSQL.php';

class userlimit_userlimitdetailController extends Application_Main
{

	public function initController(){
			  //$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
   		$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
	    $acct_no = $AESMYSQL->decrypt($this->_getParam('acct_no'), $password);
	    $user_login = $AESMYSQL->decrypt($this->_getParam('user_login'), $password);

		$this->view->acct_no = $acct_no;
		$type = $this->_getParam('type');
		$this->view->type = $type;

		// ----------------------------------------------------USER LIMIT DETAIL----------------------------------------------------------------------------------
		if($acct_no && $user_login)
		{
			$arrStatus = array(-1 => '--- Any Value ---',
							 1 => 'Approved',
							 0 => 'Deleted');

			$caseStatus = "(CASE MAKERLIMIT_STATUS ";
			foreach($arrStatus as $key=>$val)
			{
				$caseStatus .= " WHEN ".$key." THEN '".$val."'";
			}
			$caseStatus .= " END)";

			$select = $this->_db->select()
	 				         ->FROM (array('MK' => 'M_MAKERLIMIT'),array('MK.ACCT_NO','MK.USER_LOGIN','MK.MAXLIMIT','STATUS'=>$caseStatus,'CREATED','CREATEDY','UPDATED','UPDATEDBY','SUGGESTED','SUGGESTEDBY'))
	 				         ->JOINLEFT (array('C' => 'M_CUSTOMER'),'C.CUST_ID = MK.CUST_ID',array('C.CUST_ID','C.CUST_NAME'))
							 ->JOINLEFT (array('MA' => 'M_CUSTOMER_ACCT'),'MK.ACCT_NO = MA.ACCT_NO',array('MA.ACCT_NAME','MA.CCY_ID'))
							 ->JOINLEFT (array('MC' => 'M_USER'),'MC.USER_ID =MK.USER_LOGIN' , array('MC.USER_FULLNAME'))
							 ->WHERE('MK.ACCT_NO = ?',$acct_no)
							 ->WHERE('MK.USER_LOGIN = ?',$user_login);

			$resultuserlimitdetail = $this->_db->fetchRow($select);

			$selectCustomer = $this->_db->select()
								->from('M_CUSTOMER', array('CUST_NAME'))
								->where('CUST_ID = ?', $cust_id);

			$customerName = $this->_db->fetchRow($selectCustomer);

			$resultuserlimitdetail['CUST_ID'] = $cust_id;
			$resultuserlimitdetail['CUST_NAME'] = $customerName['CUST_NAME'];

			$this->view->userlimitdetail = $resultuserlimitdetail;
		}
		// ----------------------------------------------------END USER LIMIT DETAIL----------------------------------------------------------------------------------
		$this->view->modulename = $this->_request->getModuleName();
		Application_Helper_General::writeLog('MLLS','Viewing User Limit Detail, Customer ID : '.$resultuserlimitdetail['CUST_ID'].', User ID : '.$user_login);
	}
}
