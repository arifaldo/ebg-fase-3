<?php

require_once 'Zend/Controller/Action.php';

class group_DetailController extends group_Model_Group 
{
	public function initController()
	{
	 
    }

	public function indexAction() 
	{
	    $this->view->groupStatus = $this->_masterStatus;
		
		$this->view->schemeTemplate = $this->getTemplate();
		$this->view->privilege = $this->getPrivilege();

	    $groupCode = $this->getRequest()->getParam('group_code');	
	    
	    if($groupCode)
	    {
           $this->getRequest()->setParam('privilege',Application_Helper_Array::listArray($this->getPriviGroup($groupCode),'BPRIVI_ID','BG_ID'));
	    }
	    
       	$this->view->inputParam = $this->getRequest()->getParams(); 
			
		//build filter chains
		$filters = array('group_code' => array('StringTrim'));
							 
	    //build validator chains
		$validators =  array(
						'group_code' => array('NotEmpty', //tdk berfungsi
													   array('Db_RecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_ID')),
													   'messages' => array(
													   					   $this->getErrorRemark('01','GROUP ID'), //set error message for group_code
													   					   $this->getErrorRemark('22','GROUP ID'),
													   					   ) //set error message for 2nd validator (Db_NoRecordExists)													   					   )
											 ),
						    );
			
		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());
		
	// utk mengecek apakah parameter yg dikirim adalah group_code, jika tdk maka proses tdk dilakukan
	// pengganti validator NotEmpty, krn tdk berfungsi	
    if(array_key_exists('group_code',$this->_request->getParams()))
	{
		if($zf_filter_input->isValid())
		{  
			$group = $this->getGroup($groupCode);
                   			
            if(is_array($group))
			{
				$this->view->group_code =  $groupCode;           			
				$this->view->group_name =  $group['BGROUP_DESC'];
						        	
				$code = $this->_masterStatus['code'];
				$code = array_flip($code);
				
				$this->view->group_status =  $this->_masterStatus['desc'][$code[$group['BGROUP_STATUS']]];
				
			    $this->view->code_group_status =  $group['BGROUP_STATUS'];
			}
						        
		    $alreadySuggest = $this->_db->fetchOne("SELECT 1 FROM TEMP_BGROUP WHERE BGROUP_ID = '{$groupCode}'");  
            if($alreadySuggest) $this->view->alreadySuggest =  true;
        }
		else
		{   
		    $this->view->errors = $zf_filter_input->getMessages();
		}
   }   
   else
   {   
      $this->getRequest()->setParam('privilege','');
      $this->view->inputParam = $this->getRequest()->getParams();
   }
  
        
 }
 
}
