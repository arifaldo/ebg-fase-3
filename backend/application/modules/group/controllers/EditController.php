<?php

require_once 'Zend/Controller/Action.php';

class group_EditController extends group_Model_Group
{
	protected $_moduleAction  = 'BGR';
	
	public function indexAction() 
	{
		$this->view->schemeTemplate = $this->getTemplate();
		$this->view->privilege = $this->getPrivilege();
                    					
        //build filter chains
		$filters = array('group_code' => array('StringTrim'));
							 
		//build validator chains
		$validators =  array(
						  'group_code' => array('NotEmpty', // tidak berfungsi, entah mengapa
		                                       'Alnum',
											    array('Db_RecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_ID','exclude'=>"BGROUP_STATUS = 'A'")),
												array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_ID')),
											    'messages' => array(
													   				$this->getErrorRemark('01','GROUP ID'), //set error message for group_code
													   				$this->getErrorRemark('04','GROUP ID'),
													   				$this->getErrorRemark('22','GROUP ID'),
													   				$this->getErrorRemark('03','GROUP ID')
													   			   ) //set error message for 2nd validator (Db_NoRecordExists)													   					   )
											    ),
						   );
			
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());
			
			if($zf_filter_input->isValid()) 
			{    	
			    $groupCode  = $this->getRequest()->getParam('group_code');
			    $group      = $this->getGroup($groupCode);
			    $priviGroup = $this->getPriviGroup($groupCode); 
			
				if(is_array($group))
                {  
				  $this->view->group_code  = ($zf_filter_input->isValid('group_code'))? $zf_filter_input->group_code : $groupCode;            			
				  $this->view->group_name  =  $group['BGROUP_DESC'];
						        	
				  $code = $this->_masterStatus['code'];
				  $code = array_flip($code);
				  $this->view->group_status = $this->_masterStatus['desc'][$code[$group['BGROUP_STATUS']]];

				  if($group['BGROUP_STATUS'] == $this->_masterStatus['code']['inactive'])
				  {
				  	$errorMsg = $this->getErrorRemark('81');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
							         
					$this->backendLog('V', $this->_moduleAction, $this->getRequest()->getParam('group_code'), null, $errorMsg);
							       
					$this->_redirect($this->_backURL);
				  }
			    }

			 
		   if($this->_request->isPost())
		   {
			  $this->view->inputParam = $this->getRequest()->getParams(); 
			  //build filter chains
			  $filters = array('group_code'         => array('StringTrim'), //trim whitespaces				
							   'group_name'         => array('StringTrim'),
							   'validate_privilege' => array('StringTrim'),
							   'role_conflict'      => array('StringTrim')
							  );
							 
			  //build validator chains
			  $clause     = $this->_db->quoteInto('BGROUP_DESC != ?', $group['BGROUP_DESC']);
			  
			  $validators =  array( 'group_code' => array(),
			  
						            'group_name' => array('NotEmpty',new Zend_Validate_StringLength(array('min'=>1,'max'=>32)), //group name may not Be empty
												           array('Db_NoRecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_DESC','exclude'=>$clause)),
													       array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_DESC','exclude'=>$clause)), 
													       'messages' => array(
													   					      $this->getErrorRemark('01','GROUP NAME'), //set error message for group_code
													   					      $this->getErrorRemark('04','GROUP NAME'),
													   					      $this->getErrorRemark('02','GROUP NAME'),
													   					      $this->getErrorRemark('03','GROUP NAME')
													                          )
													      ),
														
						           'validate_privilege' => array('NotEmpty',
                                                                 'messages' => array($this->getErrorRemark('30','Role List')),  
													  		      //set error message for role list validator
									                             ),
						          'role_conflict' => array(new Zend_Validate_Identical('valid'),
											               'messages' => array($this->getErrorRemark('26','Privilege'))
									                        //set error message for role list validator 
										                  ),
						         );
						         
			$_POST['validate_privilege'] = array_key_exists('1',array_flip($_POST['privilege']));
			$_POST['role_conflict']      = $this->roleIsConflict($_POST['privilege']);
			
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getPost());
			$zf_filter_input->setDefaultEscapeFilter(new Zend_Filter_StripTags());
			if($zf_filter_input->isValid()) 
			{
                try 
                {
				   $this->_db->beginTransaction();
						
				   $groupCode  = ($zf_filter_input->group_code)? $zf_filter_input->group_code : $this->_request->getParam('group_code'); 
				   $groupName  = ($zf_filter_input->group_name)? $zf_filter_input->group_name : $this->_request->getParam('group_name');
				   $privilege  = ($zf_filter_input->privilege)? $zf_filter_input->privilege : $this->_request->getParam('privilege');
				   $groupCode  = strtoupper($groupCode);
						
					
				   $changeType = $this->_changeType['code']['edit'];
				   $status     = $this->_masterStatus['code']['active'];
				   
				   // insert ke T_GLOBAL_CHANGE
				   $changesId  = $this->suggestionWaitingApproval('Backend Group',"Group ID = $groupCode, Group Name = $groupName",$changeType,'','M_BGROUP','TEMP_BGROUP','BGROUP_ID',$groupCode);
				   $this->insertTempGroup($groupCode,$groupName,$changesId,$status);
				   
                   if(is_array($privilege))
                   {
					  foreach($privilege as $privi_id => $insert) 
					  {
					    if($insert) $this->insertTempPriviGroup($groupCode,$privi_id,$changesId);
					  }
				   }

                  $this->_helper->getHelper('FlashMessenger')->addMessage('S');
				  $this->_helper->getHelper('FlashMessenger')
								->addMessage($this->getErrorRemark('00','Group ID',$groupCode));
								
				  $this->_db->commit();
                  $this->_redirect($this->_backURL);
						
			    }
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					Application_Log_GeneralLog::technicalLog($e);
					//rethrow exception
				}
				//if we got here, this means no problem during try block, commit transaction
				$this->_db->commit();
				$this->view->success = true;
				
			} // End IF IS VALID - SECOND
			else
			{
			    $this->view->error = true;
				$this->view->group_code  = ($zf_filter_input->isValid('group_code'))? $zf_filter_input->group_code : $this->_request->getParam('group_code'); 
				$this->view->group_name  = ($zf_filter_input->isValid('group_name'))? $zf_filter_input->group_name : $this->_request->getParam('group_name'); 
				$errors = $zf_filter_input->getMessages();
				
				$this->backendLog('E',$this->_moduleDB,$this->_request->getParam('group_code'),null,$this->displayErrorRemark($errors));
				
				//assign error messages
				$this->view->groupCodeErr = (isset($errors['group_code']))? $errors['group_code'] : null;
				$this->view->groupNameErr = (isset($errors['group_name']))? $errors['group_name'] : null;
				$this->view->privilegeErr = (isset($errors['validate_privilege']))? $errors['validate_privilege'] : null;
				if(is_null($this->view->privilegeErr))
				{
					$this->view->privilegeErr = (isset($errors['role_conflict']))? $errors['role_conflict'] : null;
				}				
			}

		 }// END IF REQUEST IS POST
		 // view pertama kali
		 else
		 {  
			$this->backendLog('V', $this->_moduleAction, null,null,null);
			$this->getRequest()->setParam('privilege',Application_Helper_Array::listArray($priviGroup,'BPRIVI_ID','BG_ID'));
		 }
					
     }//END IF IS VALID - FIRST
	 else
	 { 
	    $this->getRequest()->setParam('privilege','');
	    $this->view->error = true;
		$errors = $zf_filter_input->getMessages();
			
		//assign error messages
		$this->view->groupCodeErr = (isset($errors['group_code']))? $errors['group_code'] : null;
								
	  }

     $this->view->inputParam = $this->getRequest()->getParams(); 
 }
	
	
	private function roleIsConflict($privilege)
	{
		$selectedArray = array();
		if(is_array($privilege))
		{
			foreach ($privilege as $keyPrivilege => $value) 
			{
				if($value=="1") $selectedArray[] = $keyPrivilege;
	        }
	    }
	    
		if(is_array($selectedArray))
		{
			if(count($selectedArray)> 0 )
			{
			   	foreach ($selectedArray as $key => $value )
			   	{
			   		$selectedArray[$key] = "'".$value."'";
			   	}
		   		$groupArr = implode(',',$selectedArray);
			}
			else return 'invalid';
		}
		
		$listPrivilege = $this->getTemplateId($groupArr);
		
	   if(is_array($listPrivilege))
		{
			$privilegeConflict = array_flip(Application_Helper_Array::simpleArray($listPrivilege,'PARENT_BTEMPLATE_ID'));
		}
			
		if(array_key_exists("1", $privilegeConflict) && array_key_exists("2", $privilegeConflict))
		{
			return 'invalid';
		}
		else return 'valid';
		
	}
	
	
}

