<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class corporatedcard_ActivateController extends Application_Main 
{

   public function indexAction() 
   {   
       $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;
        $AESMYSQL = new Crypt_AESMYSQL();
        //$cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password));
        //$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
        
        $error_remark = null;
		$regid = $this->_getParam('regid');
       // jika customer id valid
       if($regid)
       {   
           //$id  = $zf_filter_input->getEscaped('regid'); 
					$this->view->regid = $regid;
					$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							//->where('A.CUST_ID = ?',$cust_id)
							->where('A.REG_NUMBER = ?',$regid);
							
					$data = $this->_db->fetchRow($select);	
           
           if(!empty($data))
           {   
      	      //  $tempCustomerId = $this->getTempCustomerId($cust_id);
		      //  if(!$tempCustomerId)
		     //   {
                   $info = 'Customer ID = '.$data['CUST_ID'].', Customer Name = '.$data['CUST_NAME'].', Reguster Number = '.$data['REG_NUMBER'];
                   //$cust_data['CUST_STATUS'] = $this->_masteruserStatus['code']['inactive'];
                   $key_value = $data['CUST_NAME'];
                   try 
                   { 
			          $this->_db->beginTransaction();
			          
			          $data['DEBIT_STATUS'] = 1;
			          $data['DEBIT_SUGGESTED']    = new Zend_Db_Expr('now()');
		              $data['DEBIT_SUGGESTEDBY']  = $this->_userIdLogin;
			          unset($data['CUST_NAME']);			 
			          // insert ke T_GLOBAL_CHANGES
					  $change_id = $this->suggestionWaitingApproval('Corporate Debit Card',$info,$this->_changeType['code']['unsuspend'],null,'T_CUST_DEBIT','TEMP_CUST_DEBIT',$data['CUST_ID'],$key_value,$data['CUST_ID']);
					  //zend_debug::dump($data); die;
			          $data['CHANGES_ID'] = $change_id;
					  $this->_db->insert('TEMP_CUST_DEBIT', $data);
			          
			          //log CRUD
			          Application_Helper_General::writeLog('CCSP','Corporate Card has been Unsuspended, Cust ID : '.$data['CUST_ID']. ' Cust Name : '.$data['CUST_NAME'].' Change id : '.$change_id);
			          
			          $this->_db->commit();
			          
					  $this->setbackURL('/'.$this->_request->getModuleName().'/index');  
			          $this->_redirect('/notification/submited/index');
			          //$this->_redirect('/notification/success/index');
		           }
		           catch(Exception $e) 
		           { 
			         $this->_db->rollBack();
			         $error_remark = $this->getErrorRemark('82');
			         Application_Log_GeneralLog::technicalLog($e);
			       }
               // }
              //  else $error_remark = 'Corporate Card is already suggested'; 
            
          }
          else  $regid = null; 
       }// END IF CUST_ID
    
       
       if(!$regid)  $error_remark = 'Register Number is not found';
      
    
       //insert log
       try
       {
	       $this->_db->beginTransaction();
	       
	       Application_Helper_General::writeLog('CCSP','view Unsuspended Corporate Card');
	       
           $this->_db->commit();
	   }
	   catch(Exception $e) 
	   {
	       $this->_db->rollBack();
  	       Application_Log_GeneralLog::technicalLog($e);
	   }
	
       $this->_helper->getHelper('FlashMessenger')->addMessage($class);
       $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
       $this->_redirect($this->_backURL);

       
       
  }
  
}



