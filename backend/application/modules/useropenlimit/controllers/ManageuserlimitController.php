<?php

require_once 'Zend/Controller/Action.php';

class useropenlimit_ManageuserlimitController extends Application_Main 
{
	
  public function indexAction() 
  {

  	$setting = new Settings();			  	
	$enc_pass = $setting->getSetting('enc_pass');
	$enc_salt = $setting->getSetting('enc_salt');
	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
	$pw_hash = md5($enc_salt.$enc_pass);
	$rand = $this->_userIdLogin.date('dHis').$pw_hash;
	$sessionNamespace->token 	= $rand;
	$this->view->token = $sessionNamespace->token; 

	$this->_helper->layout()->setLayout('newlayout');
        //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/manageuserlimit/'); 
	
  
		$displayDateFormat  = $this->_dateDisplayFormat;
		$databaseSaveFormat = $this->_dateDBFormat;
		
		$listId = $this->_db->select()->distinct()
						->from(array('M_CUSTOMER_ACCT'),
							   array('CUST_ID'))
						->order('CUST_ID ASC')
						->query()->fetchAll();     	
		$this->view->listCustId = Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');
		
		$listCcy = $this->_db->select()
								->from('M_MINAMT_CCY',array('CCY_ID'))
								->order('CCY_ID')
								->query()->fetchAll();
		$this->view->listCcyId = Application_Helper_Array::listArray($listCcy,'CCY_ID','CCY_ID');
		
		$arrStatus = array( -1 => '--- '.$this->language->_('Any Value').' ---',
									 1 => $this->language->_('Approved'),
									 2 => $this->language->_('Suspended'),
									 3 => $this->language->_('Deleted'));
		$this->view->listStatus = $arrStatus;

		/*------------------------------------------------------------FIELDS------------------------------------------------------------------*/
		{
			$fields = array	(
							// 'companycode'  			=> array	(
							// 										'field' => 'C.CUST_ID',
							// 										'label' => $this->language->_('Company Code'),
							// 										'sortable' => true
							// 									),
							'companyname'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => $this->language->_('Company'),
																	'sortable' => true
																),
							'userid'  					=> array	(
																		'field' => 'USER_LOGIN',
																		'label' => $this->language->_('User'),
																		'sortable' => true
																	),
							'accnumber'  			=> array	(
																	'field' => 'MA.ACCT_NO',
																	'label' => $this->language->_('Account Number'),
																	'sortable' => true
																),
							// 'accname'  			=> array	(
							// 										'field' => 'ACCT_NAME',
							// 										'label' => $this->language->_('Account Name'),
							// 										'sortable' => true
							// 									),																	
							'currency' 					=> array	(
																		'field' => 'CCY_ID',
																		'label' => $this->language->_('CCY'),
																		'sortable' => true
																	),
							'maxlimit' 					=> array	(
																		'field' => 'MAXLIMIT',
																		'label' => $this->language->_('Max Limit'),
																		'sortable' => true
																	),											
							'status'  					=> array	(
																		'field' => 'STATUS',
																		'label' => $this->language->_('Status'),
																		'sortable' => true
																	),	
							
							'latestsuggestion' 					=> array	(
																		'field' => 'SUGGESTED',
																		'label' => $this->language->_('Last Suggested'),
																		'sortable' => true
																	),	
							// 'latestsuggestor'  					=> array	(
							// 											'field' => 'SUGGESTEDBY',
							// 											'label' => $this->language->_('Last Suggester'),
							// 											'sortable' => true
							// 										),	
							'latestsuggestionapp'		  		=> array	(
																		'field' => 'UPDATED',
																		'label' => $this->language->_('Last Approved'),
																		'sortable' => true
																	),
							// 'latestapp'		  		=> array	(
							// 											'field' => 'UPDATEDBY',
							// 											'label' => $this->language->_('Latest Approver'),
							// 											'sortable' => true
							// 										),													  
	                       );


			$filterlist = array('PS_LATEST_SUGGESTION','PS_LATEST_APPROVAL','LATEST_SUGGESTER','LATEST_APPROVER','COMP_ID','COMP_NAME','CCY','ACC_NUMBER','ACC_NAME','USER_ID','STATUS');
			
			$this->view->filterlist = $filterlist;
						
			$page = $this->_getParam('page');
			$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
			$sortBy = $this->_getParam('sortby');
			$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
			$sortDir = $this->_getParam('sortdir');
			$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		}
		/*-------------------------------------------------------END FIELDS------------------------------------------------------------------*/
		
		/*-------------------------------------------------------------VALIDATION------------------------------------------------------------------------*/
		{
			 $filterArr = array('filter'	 	=> array('StripTags','StringTrim'),
	                       'COMP_ID'    => array('StripTags','StringTrim','StringToUpper'),
							'COMP_NAME'  => array('StripTags','StringTrim','StringToUpper'),
							'CCY'  => array('StripTags','StringTrim','StringToUpper'),
							'ACC_NUMBER'    => array('StripTags','StringTrim','StringToUpper'),
							'ACC_NAME'  => array('StripTags','StringTrim','StringToUpper'),
							'STATUS' => array('StripTags','StringToUpper'),
							'USER_ID' => array('StripTags','Alpha','StringToUpper'),
							'PS_LATEST_SUGGESTION' => array('StripTags','StringTrim','StringToUpper'),
				    		'PS_LATEST_SUGGESTION_END'     => array('StripTags','StringTrim'),
				            'LATEST_SUGGESTER'      => array('StripTags','StringTrim','StringToUpper'),
				            'PS_LATEST_APPROVAL'   => array('StripTags','StringTrim','StringToUpper'),
				     		'PS_LATEST_APPROVAL_END'       => array('StripTags','StringTrim'),
				             'LATEST_APPROVER'       => array('StripTags','StringTrim','StringToUpper'),					   
	                      );

			 $validators = array(
										'filter' => array(),
										  'COMP_ID'    => array(),
										   'COMP_NAME'  => array(),
										   'CCY'  => array(),
										   'ACC_NUMBER'    => array(),
										   'ACC_NAME'  => array(),
										   'STATUS' => array(),
										   'USER_ID' => array(),			  
					                       'PS_LATEST_SUGGESTION' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
					    				   'PS_LATEST_SUGGESTION_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
					                       'LATEST_SUGGESTER'      => array(),
					                       'PS_LATEST_APPROVAL'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
					     				   'PS_LATEST_APPROVAL_END'       => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
					                       'LATEST_APPROVER'       => array(),
										);

						  
			$dataParam = array('PS_LATEST_SUGGESTION','PS_LATEST_APPROVAL','LATEST_SUGGESTER','LATEST_APPROVER','COMP_ID','COMP_NAME','CCY','ACC_NUMBER','ACC_NAME','USER_ID','STATUS');
			$dataParamValue = array();
				
			$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
			$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

			// print_r($output);die;
			// print_r($this->_request->getParam('wherecol'));
			foreach ($dataParam as $no => $dtParam)
			{
			
				if(!empty($this->_request->getParam('wherecol'))){
					$dataval = $this->_request->getParam('whereval');
					// print_r($dataval);
					$order = 0;
						foreach ($this->_request->getParam('wherecol') as $key => $value) {
							if($value == "PS_LATEST_APPROVAL" || $value == "PS_LATEST_SUGGESTION"){
									$order--;
								}
							if($dtParam==$value){
								$dataParamValue[$dtParam] = $dataval[$order];
							}
							$order++;
						}
					
				}
			}
			// print_r($dataParamValue);
			// die;	
			// print_r($this->_request->getParam('whereval'));die;
				if(!empty($this->_request->getParam('lsdate'))){
					$lsarr = $this->_request->getParam('lsdate');
						$dataParamValue['PS_LATEST_SUGGESTION'] = $lsarr[0];
						$dataParamValue['PS_LATEST_SUGGESTION_END'] = $lsarr[1];
				}
				if(!empty($this->_request->getParam('ladate'))){
					$laarr = $this->_request->getParam('ladate');
						$dataParamValue['PS_LATEST_APPROVAL'] = $laarr[0];
						$dataParamValue['PS_LATEST_APPROVAL_END'] = $laarr[1];
				}

		    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	 		$filter 		= $this->_getParam('filter');

			$this->view->currentPage = $page;
			$this->view->sortBy = $sortBy;
			$this->view->sortDir = $sortDir;
			
			$acctStatusArr = array(
					'1' =>'Approved',
					'2' =>'Suspended',
					'3' =>'Deleted',
			);
			
			$caseStatus = "(CASE MA.ACCT_STATUS ";
			foreach($acctStatusArr as $key=>$val)
			{
				$caseStatus .= " WHEN ".$key." THEN '".$val."'";
			}
			$caseStatus .= " END)";
			
			$select = $this->_db->select()
	 				         ->FROM (array('MK' => 'M_MAKERLIMIT'),array('MK.USER_LOGIN','MK.MAXLIMIT','STATUS'=>$caseStatus,'MK.UPDATED','MK.UPDATEDBY','MK.SUGGESTED','MK.SUGGESTEDBY'))
	 				         ->WHERE ('MK.MAXLIMIT > 0')
	 				         // ->WHERE ('MK.MAKERLIMIT_STATUS <> 3')
							 ->JOIN (array('C' => 'M_CUSTOMER'),'C.CUST_ID = MK.CUST_ID',array('C.CUST_ID','C.CUST_NAME'))
							 ->JOIN (array('MU' => 'M_USER'),'MU.USER_ID = MK.USER_LOGIN',array('MU.USER_FULLNAME','MU.USER_ID'))
							 ->JOIN (array('MA' => 'M_CUSTOMER_ACCT'),'MK.ACCT_NO = MA.ACCT_NO',array('company'	=> new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )"),
								 		'user'	=> new Zend_Db_Expr("CONCAT(MU.USER_FULLNAME , ' (' , MU.USER_ID , ')  ' )"),'MA.ACCT_NO','MA.ACCT_NAME','MA.CCY_ID','MA.ACCT_STATUS','companyID' => 'MA.CUST_ID'));

			if($filter==$this->language->_('Set Filter'))
			{
					$companyid = html_entity_decode($zf_filter->getEscaped('COMP_ID'));
					$companyname = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
					$userid = html_entity_decode($zf_filter->getEscaped('USER_ID'));
					$accnumber = html_entity_decode($zf_filter->getEscaped('ACC_NUMBER'));
					$accname = html_entity_decode($zf_filter->getEscaped('ACC_NAME'));
					$ccyid = $zf_filter->getEscaped('CCY');	
					$status = $zf_filter->getEscaped('STATUS');				
					$suggestor = $zf_filter->getEscaped('LATEST_SUGGESTER');
					$approver = $zf_filter->getEscaped('LATEST_APPROVER');
				
					$suggestionFrom = (Zend_Date::isDate($zf_filter->getEscaped('PS_LATEST_SUGGESTION'),$displayDateFormat))?
										new Zend_Date($zf_filter->getEscaped('PS_LATEST_SUGGESTION'),$displayDateFormat):
										false;
					$suggestionTo = (Zend_Date::isDate($zf_filter->getEscaped('PS_LATEST_SUGGESTION_END'),$displayDateFormat))?
										new Zend_Date($zf_filter->getEscaped('PS_LATEST_SUGGESTION_END'),$displayDateFormat):
										false;		
						
					$approvalFrom = (Zend_Date::isDate($zf_filter->getEscaped('PS_LATEST_APPROVAL'),$displayDateFormat))?
										new Zend_Date($zf_filter->getEscaped('PS_LATEST_APPROVAL'),$displayDateFormat):
										false;
					$approvalTo = (Zend_Date::isDate($zf_filter->getEscaped('PS_LATEST_APPROVAL_END'),$displayDateFormat))?
										new Zend_Date($zf_filter->getEscaped('PS_LATEST_APPROVAL_END'),$displayDateFormat):
										false;
					// Zend_Debug::dump($zf_filter->getEscaped('approvalFrom'));
					// Zend_Debug::dump($zf_filter->getEscaped('approvalTo'));die;

					$this->view->companyid   = $companyid;
				    $this->view->companyname = $companyname;
				    $this->view->userid   = $userid;
				    $this->view->accnumber    = $accnumber;
				    $this->view->accname     = $accname;
				    $this->view->ccyid     = $ccyid;
				    $this->view->status     = $status;
				    $this->view->suggestor     = $suggestor;
				    $this->view->approver     = $approver;

					if($companyid)
					{
						$this->view->companyid = $companyid;
						$select->where("UPPER(C.CUST_ID) LIKE ".$this->_db->quote('%'.$companyid.'%'));	
					}
					
					if($companyname)
					{
						$this->view->companyname = $companyname;
						$select->where("UPPER(C.CUST_NAME) LIKE ".$this->_db->quote('%'.$companyname.'%'));
					}   
					
					if($userid)
					{
						$this->view->userid = $userid;
						$select->where("UPPER(MK.USER_LOGIN) LIKE ".$this->_db->quote('%'.$userid.'%'));
					}									
					
					if($accnumber)
					{
						$this->view->accnumber = $accnumber;
						$select->where("MA.ACCT_NO LIKE ".$this->_db->quote('%'.$accnumber.'%'));
					}
					
					if($accname)
					{
						$this->view->accname = $accname;
						$select->where("UPPER(MA.ACCT_NAME) LIKE ".$this->_db->quote('%'.$accname.'%'));
					}
					
					if($ccyid)
					{
						$this->view->ccyid = $ccyid;
						$select->where("MA.CCY_ID LIKE ".$this->_db->quote($ccyid));
					}
					
					// if($status!=-1)
					// {
					// 	$this->view->status = $status;
					// 	$select->where("MA.ACCT_STATUS = ".$this->_db->quote($status));
					// } 
					
					if($suggestor)
					{
						$this->view->suggestor = $suggestor;
						$select->where("UPPER(MK.SUGGESTEDBY) LIKE '%".$suggestor."%'");
					} 					
 						
					if($approver)
					{
						$this->view->approver = $approver;
						$select->where("UPPER(MK.UPDATEDBY) LIKE '%".$approver."%'");
					} 					
					if($suggestionFrom)
					{
						$this->view->suggestionFrom = $suggestionFrom->toString($displayDateFormat);
						$select->where("DATE(MK.SUGGESTED) >= DATE(".$this->_db->quote($suggestionFrom->toString($this->_dateDBFormat)).")");
					}

					if($suggestionTo)
					{
						$this->view->suggestionTo = $suggestionTo->toString($displayDateFormat);
						$select->where("DATE(MK.SUGGESTED) <= DATE(".$this->_db->quote($suggestionTo->toString($this->_dateDBFormat)).")");
					}		

					if($approvalFrom)
					{
						$this->view->approvalFrom = $approvalFrom->toString($displayDateFormat);
						$select->where("DATE(MK.UPDATED) >= DATE(".$this->_db->quote($approvalFrom->toString($this->_dateDBFormat)).")");
					}

					if($approvalTo)
					{
						$this->view->approvalTo = $approvalTo->toString($displayDateFormat);
						$select->where("DATE(MK.UPDATED) <= DATE(".$this->_db->quote($approvalTo->toString($this->_dateDBFormat)).")");
					}


			}					 
		}
		/*---------------------------------------------------------END VALIDATION------------------------------------------------------------------------*/
		
        $select->order($sortBy.' '.$sortDir);   
        // echo $select;
        $select = $select->query()->fetchAll();
		// die($select);		
		
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->modulename = $this->_request->getModuleName(); 
		Application_Helper_General::writeLog('MLLS','View User Limit List');

		if(!empty($dataParamValue)){
	    		$this->view->lsdateStart = $dataParamValue['PS_LATEST_SUGGESTION'];
	    		$this->view->lsdateEnd = $dataParamValue['PS_LATEST_SUGGESTION_END'];
	    		$this->view->ladateStart = $dataParamValue['PS_LATEST_APPROVAL'];
	    		$this->view->ladateEnd = $dataParamValue['PS_LATEST_APPROVAL_END'];

	    	  	unset($dataParamValue['PS_LATEST_SUGGESTION_END']);
			    unset($dataParamValue['PS_LATEST_APPROVAL_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
  }
}