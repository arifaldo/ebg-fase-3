<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class Sweep_DetailsweepController extends Application_Main {
	
	
	public function indexAction(){
		
		
		
		$payreff = $this->_getParam('payreef');
		
		/*select tabel T_TEMP_SWEEP*/
		$sweep = $this->_db->select()
							->from(array('T_TEMP_SWEEP'),
									array(
											'Teamplate Reff' 	=> 'teamplate_reff',
											'Payment Subject'	=> 'payment_subject',
											'Beneficiary'		=> 'beneficiari_acc',
											'Source Account'	=> 'source_acc',
											'Day Name'			=> 'dayname',
											'Currency'			=> 'currency',
											'Day'				=> 'day',
											'Sesion'			=> 'sesion',
											'End Date'			=> 'end_date',
											'Payment Message'	=> 'payment_message',
											'Payment Add Message' => 'payment_add_message',
											'Remain Balance'	=> 'remain_balance', 
											'Category'			=> 'category'
									))
							->where('teamplate_reff LIKE '.$this->_db->quote($payreff));
		//echo $sweep->__toString();
		
		$sweep = $this->_db->fetchAll($sweep);
		$sweeplenght = count($sweep);
		
		//["Teamplate Reff"]
		$teamplatereff = array();
		
		//["Payment Subject"]
		$paymentsubject = array();
		
		//["Beneficiary"]
		$beneficiary = array();
		
		//["Source Account"]
		$sourceacc = array();
		
		//["Day Name"]
		$dayname = array();
		
		//["Day"]
		$day = array();
		
		//["Sesion"]
		$sesion = array();
		
		//["End Date"]
		$enddate = array();
		
		//["Category"]
		$category = array();
		
		//["Currency"]
		$curr = array();
		
		//["Payment Message"]
		$pay_messg = array();
		
		//["Payment Add Message"]
		$pay_add_messg = array();
		
		//["Remain Balance"]
		$remain_balance = array();
		
		for($i=0; $i<$sweeplenght; $i++){
			array_push($teamplatereff, $sweep[$i]["Teamplate Reff"]);
			array_push($paymentsubject, $sweep[$i]["Payment Subject"]);
			array_push($beneficiary, $sweep[$i]["Beneficiary"]);
			array_push($sourceacc, $sweep[$i]["Source Account"]);
			array_push($dayname, $sweep[$i]["Day Name"]);
			array_push($day, $sweep[$i]["Day"]);
			array_push($sesion, $sweep[$i]["Sesion"]);
			array_push($enddate, $sweep[$i]["End Date"]);
			array_push($category, $sweep[$i]["Category"]);
			array_push($pay_messg, $sweep[$i]["Payment Message"]);
			array_push($curr, $sweep[$i]["Currency"]);
			array_push($pay_add_messg, $sweep[$i]["Payment Add Message"]);
			array_push($remain_balance, $sweep[$i]["Remain Balance"]);
		}
		//["Teamplate Reff"]
		$this->view->teamplatereff = $teamplatereff;
		
		//["Payment Subject"]
		$this->view->paymentsubject = $paymentsubject;
		
		//["Beneficiary"]
		$this->view->beneficiary = $beneficiary;
		
		//["Source Account"]
		$this->view->sourceacc = $sourceacc;
		
		//["Day Name"]
		$this->view->dayname = $dayname;
		
		//["Day"]
		$this->view->day = $day;
		
		//["Sesion"]
		$this->view->sesion = $sesion;
		
		//["End Date"]
		$this->view->enddate = $enddate;
		
		//["Category"]
		$this->view->category = $category;
		
		//["Currency"]
		$this->view->curr = $curr;
		
		//["Payment Message"]
		$this->view->pay_messg = $pay_messg;
		
		//$sweeplenght
		$this->view->sweeplenght = $sweeplenght;
		
		//["Payment Add Message"]
		$this->view->pay_add_messg = $pay_add_messg;
		
		//["Remain Balance"]
		$this->view->remain_balance = $remain_balance;
		
// 		Zend_Debug::dump($sweep);
		
	}

}