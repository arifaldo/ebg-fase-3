<?php

/**
 * IndexController
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

//NOTE:
//Watch the modulename, filename and classname carefully
class Authorizationacl_IndexController extends Application_Main
{
	/**
	 * The default action - show the home page
	 */
	public function indexAction()
	{
		// $this->_helper->layout()->setLayout('popup');
		$this->_helper->layout()->disableLayout();
	}
	public function forbiddenAction()
	{
		// TODO Auto-generated IndexController::indexAction() default action
		$this->_helper->layout()->setLayout('newlayout');
	}
	public function nodataAction()
	{
		// TODO Auto-generated IndexController::indexAction() default action
	}
	public function disableAction()
	{
		//$this->_helper->layout()->setLayout('popup');
		// $this->_helper->layout()->setLayout('newlayout');
		$this->_helper->layout()->disableLayout();

		require_once 'General/Settings.php';
		$setting = new Settings();
		$this->view->note = nl2br($setting->getSetting('disable_note'));

		$select = $this->_db->select()
			->from(array('M_SETTING'), array('*'))
			->query()->fetchAll();
		$setting = Application_Helper_Array::listArray($select, 'SETTING_ID', 'SETTING_VALUE');
		// echo '<pre>';print_r($setting);
		$this->view->masterTitle = $setting['master_bank_app_name'];
		$this->view->masterEmail = $setting['master_bank_email'];
		$this->view->masterFax   = $setting['master_bank_fax'];
		$this->view->masterBankTelp   = $setting['master_bank_telp'];
	}

	public function disableuserAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	}
}
