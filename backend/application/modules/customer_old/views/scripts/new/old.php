<?php
if($this->cust_hidden)
{
  $option_max15 = array('disabled'=>true);
  $option_max40 = array('disabled'=>true);
  $option_max20 = array('disabled'=>true);
}
else
{
  $option_max15 = array('maxlength'=>15);
  $option_max40 = array('maxlength'=>40);
  $option_max20 = array('maxlength'=>20);
}

$option_max40['size'] = 43;
$option_max20['size'] = 23;
$option_max40_open = array('size'=>60,'maxlength'=>40);
?>



<script type="text/javascript">

//akan di onload pertama kali
$(document).ready(function()
{
	 $('#cust_charges_status_<?php echo $this->cust_charges_status; ?>').attr('checked','checked');
	 $('#cust_monthlyfee_status_<?php echo $this->cust_monthlyfee_status; ?>').attr('checked','checked');
	 $('#cust_token_auth_<?php echo $this->cust_token_auth; ?>').attr('checked','checked');

	 //pengaturan error messages
	 <?php
	   if(!is_null($this->customer_msg) || $this->customer_msg != '')
	   {
	       foreach($this->customer_msg as $key => $error)
	       {
	   ?>
	           $('#error_<?php echo $key; ?>').html('<?php echo $error; ?>');
	 <?php
	       }
	   }
	   ?>


});


function resetData()
{
	 $('#cust_id').val('<?php echo $this->cust_id; ?>');
	 $('#cust_name').val('<?php echo $this->cust_name; ?>');
	 $('#cust_type').val('<?php echo $this->cust_type; ?>');
	 $('#cust_workfield').val('<?php echo $this->cust_workfield; ?>');
	 $('#cust_address').val('<?php echo $this->cust_address; ?>');
	 $('#cust_city').val('<?php echo $this->cust_city; ?>');
	 $('#cust_zip').val('<?php echo $this->cust_zip; ?>');
	 $('#cust_province').val('<?php echo $this->cust_province; ?>');
	 $('#country').val('<?php echo $this->country; ?>');
	 $('#cust_contact').val('<?php echo $this->cust_contact; ?>');
	 $('#cust_phone').val('<?php echo $this->cust_phone; ?>');
	 $('#cust_ext').val('<?php echo $this->cust_ext; ?>');
	 $('#cust_fax').val('<?php echo $this->cust_fax; ?>');
	 $('#cust_email').val('<?php echo $this->cust_email; ?>');
	 $('#cust_website').val('<?php echo $this->cust_website; ?>');
	 $('#country_code').val('<?php echo $this->country_code; ?>');
   $('#cust_limit_idr').val('<?php echo $this->cust_limit_idr; ?>');
   $('#cust_limit_usd').val('<?php echo $this->cust_limit_usd; ?>');

	 $('#cust_charges_status_<?php echo $this->cust_charges_status; ?>').attr('checked','checked');
	 $('#cust_monthlyfee_status_<?php echo $this->cust_monthlyfee_status; ?>').attr('checked','checked');
	 $('#cust_token_auth_<?php echo $this->cust_token_auth; ?>').attr('checked','checked');
}



</script>
<div class="row">
      	<div class="col-md-12">
        	<div class="page-bar">
            	<ul class="page-breadcrumb">
	                <li>
	                    <span><?php echo $this->language->_('Customer Setup'); ?></span>
	                    <i class="fa fa-angle-right pl-1 pr-1"></i>
	                </li>
	                <li>
                      <?php echo $this->language->_('Customer'); ?>
                      <i class="fa fa-angle-right pl-1 pr-1"></i>
                  </li>
                  <li>
	                    <?php echo $this->language->_('Add'); ?>
	                </li>
            	</ul>
        	</div>
      	</div>
</div>


<?php if($this->success){ ?>
<div class="successmsg"><?=$this->customer_msg?></div><br />
<?php } ?>

<?php if(count($this->customer_msg) > 0){ ?>
<div class="errmsg"><?php echo $this->language->_('Error in processing form values. Please correct values and re-submit'); ?>.</div><br />
<?php } ?>


<h5><?=$this->language->_('Add New Customer')?></h5>
<h6 class="greyFont"><?=$this->language->_('Add new customer information')?></h6>

<br>

<div class="row content-box">
  <div class= "portlet-body form col-md-12">
    <div style="text-align: center">
      <button class="custindicator">New Customer</button>
      <button class="custindicator">Contact Person</button>
      <button class="custindicator">Company Limit</button>
      <button class="custindicator">Transaction</button>
      <button class="custindicator">Company Admin</button>
    </div>

    <br>
    <br>

    <form id="regForm" name="Form" method="post" action="<?=$this->url(array('module'=>$this->modulename,'controller'=>'new','action'=>'index'),null,true)?>">
     <table align="center" width="70%" cellpadding="3" cellspacing="0" border="0" class="tableform custtab">
       <tr>
            <td class="tbl-evencontent"><?php echo $this->formLabel('cust_id',$this->language->_('Company Code')); ?>&nbsp;<span class="errmsg">*</span></td>
            <td class="tbl-evencontent">:</td>
            <td class="tbl-evencontent" colspan="8">
               <?php
               echo $this->formText('cust_id',$this->cust_id,array('class' => 'form-control col-md-12','maxlength'=>16,'onfocus'=>'AlnumOnly(this.value,this.id)','onblur'=>'AlnumOnly(this.value,this.id)','onkeypress'=>'return forceAlnumOCBC(event)','onkeyup'=>'javascript:this.value = this.value.toUpperCase();','oninput' => 'this.className="form-control"'));
               ?>
               <div id="error_cust_id" class="errmsg"></div>
            </td>
        </tr>
        <tr>
            <td class="tbl-evencontent"></td>
              <td class="tbl-evencontent"></td>
              <td class="tbl-evencontent">
             <ul>
                <em>
                <?php echo $this->language->_('Format: 5-12 chars, alphanumeric, no special chars or white spaces'); ?> :
                </em>
             </ul>
            </td>
        </tr>
       <!--  <tr>
            <td class="tbl-evencontent"><?php echo $this->formLabel('cust_name',$this->language->_('Company CIF')); ?>&nbsp;<span class="errmsg">*</span></td>
            <td class="tbl-evencontent">:</td>
            <td class="tbl-evencontent" colspan="3">
                <?php echo $this->formText('cust_cif',$this->cust_cif,array('maxlength'=>20,'class' => 'form-control col-md-12','readonly'=>true));?>

            </td>
            <td>
            <input class="btngreen hov" value="<?php echo $this->language->_('Populate Data'); ?>" onclick="showPopUpWindow('<?=$this->url(array('module'=>'popup','controller'=>'populatedata','action'=>'index'),null,true)?>', 750)" type="button">
                <div id="error_cust_cif" class="errmsg"></div>
            </td>
        </tr> -->
        <tr>
          <td class="tbl-evencontent"><?php echo $this->formLabel('cust_name',$this->language->_('Company Name')); ?>&nbsp;<span class="errmsg">*</span></td>
          <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
              <?php echo $this->formText('cust_name',$this->cust_name,array('maxlength'=>80,'class' => 'form-control col-md-12','oninput' => 'this.className="form-control"'));?>
              <div id="error_cust_name" class="errmsg"></div>
          </td>
        </tr>
        <tr>
          <td class="tbl-evencontent"><?php echo $this->formLabel('cust_type',$this->language->_('Company Type')); ?></td>
          <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
           <?php echo $this->formText('cust_type',$this->cust_type,array('maxlength'=>16,'class' => 'form-control col-md-12','oninput' => 'this.className="form-control"'));?>
           <div id="error_cust_type" class="errmsg"></div>
          </td>
        </tr>
        <tr>
          <td class="tbl-evencontent"><?php echo $this->formLabel('cust_workfield',$this->language->_('Business Type')); ?></td>
          <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
            <?php  echo $this->formText('cust_workfield',$this->cust_workfield,array('maxlength'=>80,'class' => 'form-control col-md-12','oninput' => 'this.className="form-control"'));?>
            <div id="error_cust_workfield" class="errmsg"></div>
          </td>
        </tr>

        <tr>
          <td class="tbl-evencontent"><?php echo $this->formLabel('cust_address',$this->language->_('Address')); ?></td>
          <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
            <?php echo $this->formText('cust_address',$this->cust_address,array('maxlength'=>200,'class' => 'form-control col-md-12','oninput' => 'this.className="form-control"'));?>
            <div id="error_cust_address" class="errmsg"></div>
          </td>
        </tr>
        <tr>
          <td class="tbl-evencontent"><?php echo $this->formLabel('cust_city',$this->language->_('City')); ?></td>
          <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
              <?php echo $this->formText('cust_city',$this->cust_city,array('class' => 'form-control col-md-12','oninput' => 'this.className="form-control"'),$option_max40_open);?>
               <div id="error_cust_city" class="errmsg"></div>
          </td>
        </tr>
        <tr>
          <td class="tbl-evencontent"><?php echo $this->formLabel('cust_zip','Zip'); ?></td>
          <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
              <?php echo $this->formText('cust_zip',$this->cust_zip,array('maxlength'=>10,'class' => 'form-control col-md-12','onfocus'=>'return forceNumberOCBC(event)','onblur'=>'return forceNumberOCBC(event)','onkeypress'=>'return forceNumberOCBC(event)','oninput' => 'this.className="form-control"'));?>
              <div id="error_cust_zip" class="errmsg"></div>
          </td>
        </tr>
        <tr>
          <td class="tbl-evencontent"><?php echo $this->formLabel('cust_province',$this->language->_('State/Province')); ?></td>
          <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
              <?php echo $this->formText('cust_province',$this->cust_province,array('maxlength'=>128,'class' => 'form-control col-md-12','oninput' => 'this.className="form-control"'));?>
              <div id="error_cust_province" class="errmsg"></div>
          </td>
        </tr>
        <tr>
          <td class="tbl-evencontent"><?php echo $this->formLabel('country_code',$this->language->_('Country')); ?>&nbsp;<span class="errmsg">*</span></td>
          <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
        <?php
          if($this->button)
          {
            echo $this->formSelect('country_code',$this->country_code,array('class' => 'form-control col-md-12','oninput' => 'this.className="form-control"'),$this->countryArr);
          }
          else
          {
            $cc = (isset($this->countryArr[$this->country_code])) ? $this->countryArr[$this->country_code] : '';
            echo $this->formText('country_code',$cc,array('class' => 'form-control col-md-12','oninput' => 'this.className="form-control"'));
          }
        ?>
             <div id="error_country_code" class="errmsg"></div>
          </td>
        </tr>
      </table>

      <table align="center" width="70%" cellpadding="3" cellspacing="0" border="0" class="tableform custtab">
          <tr>
            <td class="tbl-evencontent"><?php echo $this->formLabel('cust_contact',$this->language->_('Contact Person')); ?></td>
            <td class="tbl-evencontent">:</td>
            <td class="tbl-evencontent" colspan="8">
               <?php echo $this->formText('cust_contact',$this->cust_contact,array('maxlength'=>128,'class' => 'form-control col-md-12','oninput' => 'this.className="form-control"')); ?>
               <div id="error_cust_contact" class="errmsg"></div>
            </td>
          </tr>

          <tr>
            <td class="tbl-evencontent"><?php echo $this->formLabel('cust_phone',$this->language->_('Contact Number')); ?></td>
            <td class="tbl-evencontent">:</td>
            <td class="tbl-evencontent" colspan="8">
               <?php echo $this->formText('cust_phone',$this->cust_phone,array('maxlength'=>128,'class' => 'form-control col-md-12','onfocus'=>'return forceNumberOCBC(event)','onblur'=>'return forceNumberOCBC(event)','onkeypress'=>'return forceNumberOCBC(event)','oninput' => 'this.className="form-control"'));?>
               <div id="error_cust_phone" class="errmsg"></div>
            </td>
          </tr>

           <tr>
              <td class="tbl-evencontent"></td>
              <td class="tbl-evencontent"></td>
              <td class="tbl-evencontent" colspan="8">
                <ul>
                  <em>
                  <?php echo $this->language->_('format'); ?>: <?php echo $this->language->_('country code-city code-your number'); ?> (<?php echo $this->language->_('eg');?> : 622199999);
                  </em>
                </ul>
              </td>
          </tr>

          <tr>
            <td class="tbl-evencontent"><?php echo $this->formLabel('cust_ext',$this->language->_('Extension')); ?></td>
            <td class="tbl-evencontent">:</td>
            <td class="tbl-evencontent" colspan="8">
                <?php echo $this->formText('cust_ext',$this->cust_ext,array('maxlength'=>128,'class' => 'form-control col-md-12','onfocus'=>'return forceNumberOCBC(event)','onblur'=>'return forceNumberOCBC(event)','onkeypress'=>'return forceNumberOCBC(event)','oninput' => 'this.className="form-control"'));?>
                <div id="error_cust_ext" class="errmsg"></div>
            </td>
          </tr>

           <tr>
            <td class="tbl-evencontent"><?php echo $this->formLabel('cust_fax',$this->language->_('Fax Number')); ?></td>
            <td class="tbl-evencontent">:</td>
            <td class="tbl-evencontent" colspan="8">
                <?php echo $this->formText('cust_fax',$this->cust_fax,$option_max40_open+array('class' => 'form-control col-md-12' , 'onfocus'=>'return forceNumberOCBC(event)','onblur'=>'return forceNumberOCBC(event)','onkeypress'=>'return forceNumberOCBC(event)','oninput' => 'this.className="form-control"'));?>
                <div id="error_cust_fax" class="errmsg"></div>
            </td>
          </tr>
          <tr>
              <td class="tbl-evencontent"></td>
              <td class="tbl-evencontent"></td>
              <td class="tbl-evencontent" colspan="8">
                 <ul>
                  <em>
                   <?php echo $this->language->_('format'); ?>: <?php echo $this->language->_('country code-city code-your number'); ?> (<?php echo $this->language->_('eg');?> : 622199999)
                  </em>
                </ul>
             </td>
          </tr>
          <tr>
            <td class="tbl-evencontent"><?php echo $this->formLabel('cust_email',$this->language->_('Contact Email')); ?></td>
            <td class="tbl-evencontent">:</td>
            <td class="tbl-evencontent" colspan="8">
                <?php echo $this->formText('cust_email',$this->cust_email,array('maxlength'=>128,'class' => 'form-control col-md-12','oninput' => 'this.className="form-control"')); ?>
                <div id="error_cust_email" class="errmsg"></div>
            </td>
          </tr>
          <tr>
            <td class="tbl-evencontent"><?php echo $this->formLabel('cust_website',$this->language->_('Company Website')); ?></td>
            <td class="tbl-evencontent">:</td>
          <td class="tbl-evencontent" colspan="8">
              <?php echo $this->formText('cust_website',$this->cust_website,array('maxlength'=>100,'class' => 'form-control col-md-12','oninput' => 'this.className="form-control"')); ?>
              <div id="error_cust_website" class="errmsg"></div>
          </td>
          </tr>
    </table>

    <table align="center" width="70%" cellpadding="3" cellspacing="0" border="0" class="tableform custtab">
      <tr>
        <td class="tbl-evencontent"><?php echo $this->formLabel('cust_limit_idr',$this->language->_('Company Limit (IDR)')); ?>&nbsp;<span class="errmsg">*</span></td>
        <td class="tbl-evencontent">:</td>
        <td class="tbl-evencontent" colspan="8">
          <?php echo $this->formText('cust_limit_idr',Application_Helper_General::displayMoney($this->cust_limit_idr), array('class' => 'form-control col-md-12'),$optionCurrency); ?>
          <div id="error_cust_limit_idr" class="errmsg"></div>
        </td>
      </tr>
      <tr>
        <td class="tbl-evencontent"><?php echo $this->formLabel('cust_limit_usd',$this->language->_('Company Limit (USD)')); ?>&nbsp;<span class="errmsg">*</span></td>
        <td class="tbl-evencontent">:</td>
        <td class="tbl-evencontent" colspan="8">
          <?php echo $this->formText('cust_limit_usd',Application_Helper_General::displayMoney($this->cust_limit_usd),array('class' => 'form-control col-md-12') ,$optionCurrency); ?>
          <div id="error_cust_limit_usd" class="errmsg"></div>
        </td>
      </tr>
    </table>

     <table align="center" width="70%" cellpadding="3" cellspacing="0" border="0" class="tableform custtab">
      <tr>
        <td class="tbl-evencontent"><?php echo $this->formLabel('service_type',$this->language->_('Service Type')); ?>&nbsp;<span class="errmsg">*</span></td>
        <td class="tbl-evencontent">:</td>
        <td class="tbl-evencontent" colspan="8">
            <input class="" type="radio" value="0" checked="checked" id="service_type0" name="service_type"/> <?php echo $this->language->_('Financial'); ?>
            <input class="" type="radio" value="1"  id="service_type1" name="service_type"/>  <?php echo $this->language->_('Non Financial'); ?> <br/>
        </td>
      <tr>
        <h5>Transaction Workflow</h5>
      </tr>
      <tr>
        <h5 class="greyFont">Transaction Workflow Process</h5>
      </tr>

     </table>

    <br>

      <div style="overflow:auto;">
          <div style="float:right; margin-right: 10%;">
              <button type="button" class="btnwhite hov" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
              <button type="button" class="btngreen hov" id="nextBtn" onclick="nextPrev(1)">Next</button>
            </div>
        </div>
    </form>
  </div>
</div>

<script type="text/javascript">

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("custtab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("custtab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("custtab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("custindicator")[currentTab].className += " inactive";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("custindicator");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
    x[i].className = x[i].className.replace(" inactive", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}

	function changeDateEstablish(){
		if(document.getElementById('cust_type').value=='E0')
			document.getElementById('dateEstablish').innerHTML='&nbsp;<span class="errmsg">*</span>';
		else
			document.getElementById('dateEstablish').innerHTML='';
	}

	<?php if($this->cust_type=='E0'){?>
	changeDateEstablish();
	<?php } ?>
</script>
