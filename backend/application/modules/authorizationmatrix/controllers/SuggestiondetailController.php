<?php

require_once 'Zend/Controller/Action.php';

class authorizationmatrix_SuggestiondetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$changeid = $this->_getParam('changes_id');

  	$select = $this->_db->select()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
	$select -> where("A.CHANGES_ID = ?",$changeid);
	$select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
	$result = $select->query()->FetchAll();
	$this->view->result=$result;
	
  	if(!$result)
		$this->_redirect('/notification/invalid/index');
  	
	$suggested = $this->_db->select()
					       	->from(array('A' => 'TEMP_APP_BOUNDARY'),array('CCY_BOUNDARY','BOUNDARY_MIN','BOUNDARY_MAX','ROW_INDEX','CUST_ID'));
	$suggested -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
	$suggested -> where("A.BOUNDARY_ISUSED LIKE '1'");
	$rsuggested = $suggested->query()->FetchAll();
	$this->view->suglist = $rsuggested;
	$custid = '';
	if(!empty($rsuggested))
		$custid = $rsuggested[0]['CUST_ID'];
	$this->view->custid = $custid;
	if(!$this->_request->isPost()){
	Application_Helper_General::writeLog('BMCL','View Boundary Member ('.$custid.') changes detail page');
	}
	$selectspecialid = 'S_'.$custid;
	
	$i = 1;
	foreach($rsuggested as $listsuggested)
	{	
    	$specialid = 'sug'.'special'.$i;
    	
		$selectspecial = $this->_db->select()
					       		->from(array('A' => 'TEMP_APP_BOUNDARY_GROUP'),array('A.GROUP_USER_ID'));
		$selectspecial -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$selectspecial -> where("A.ROW_INDEX LIKE ".$this->_db->quote($listsuggested['ROW_INDEX']));
		$selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($selectspecialid));
		$resultspecial = $this->_db->fetchRow($selectspecial);
    	
    	if($resultspecial)
    	{
    		$this->view->$specialid = "X";
    	}
    	
    	$group = $this->_db->select()
				       	->from(array('A' => 'TEMP_APP_BOUNDARY_GROUP'),array('*'));
		$group -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$group -> where("A.ROW_INDEX LIKE ".$this->_db->quote($listsuggested['ROW_INDEX']));
		$group -> where("A.GROUP_USER_ID NOT LIKE ".$this->_db->quote($selectspecialid));
		$rgroup = $group->query()->FetchAll();
		//Zend_Debug::dump($listsuggested['ROW_INDEX']);die;
		foreach($rgroup as $grouplist)
		{
			$groupid = 'sug'.$grouplist['GROUP_USER_ID'].'-'.$i;
			$this->view->$groupid = 'X';
			//Zend_Debug::dump($groupid);
		}
    	
    	$i++;	
	}
//die;
  	$current = $this->_db->select()
					       	->from(array('A' => 'M_APP_BOUNDARY'),array('CCY_BOUNDARY','BOUNDARY_MIN','BOUNDARY_MAX','BOUNDARY_ID','CUST_ID'));
	$current -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
	$current -> where("A.BOUNDARY_ISUSED LIKE '1'");
	$rcurrent = $current->query()->FetchAll();
	$this->view->curlist = $rcurrent;
	//Zend_Debug::dump($rcurrent);die;
  	$i = 1;
	foreach($rcurrent as $listcurrent)
	{	
    	$specialid = 'cur'.'special'.$i;
    	
		$selectspecial = $this->_db->select()
					       		->from(array('A' => 'M_APP_BOUNDARY_GROUP'),array('A.GROUP_USER_ID'));
		$selectspecial -> where("A.BOUNDARY_ID LIKE ".$this->_db->quote($listcurrent['BOUNDARY_ID']));
		$selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($selectspecialid));
		$resultspecial = $this->_db->fetchRow($selectspecial);
    	//Zend_Debug::dump($resultspecial);
    	if($resultspecial)
    		$this->view->$specialid = "X";
    	
    	$group = $this->_db->select()
				       	->from(array('A' => 'M_APP_BOUNDARY_GROUP'),array('*'));
		$group -> where("A.BOUNDARY_ID LIKE ".$this->_db->quote($listcurrent['BOUNDARY_ID']));
		$group -> where("A.GROUP_USER_ID NOT LIKE ".$this->_db->quote($selectspecialid));
		$rgroup = $group->query()->FetchAll();
		
		foreach($rgroup as $grouplist)
		{
			$groupid = 'cur'.$grouplist['GROUP_USER_ID'].'-'.$i;
			$this->view->$groupid = 'X';
			//Zend_Debug::dump($groupid);die;
		}
    	
    	$i++;	
	}
	//die;
	if(count($rcurrent))
		$this->view->enable = true;
	
	$select = $this->_db->select()
					       	->from(array('A' => 'T_GLOBAL_CHANGES'),array('CREATED_BY','CREATED','CHANGES_TYPE'));
	$select -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
	$result = $this->_db->fetchRow($select);
	$this->view->result = $result;
	
	$cust = $this->_db->select()
					       	->from(array('A' => 'M_CUSTOMER'),array('CUST_NAME'));
	$cust -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
	$resultcust = $this->_db->fetchRow($cust);
	$this->view->custname = $resultcust;
	
	//echo $select2; die;
	$this->view->typeCode = array_flip($this->_changeType['code']);
    $this->view->typeDesc = $this->_changeType['desc'];
	$this->view->changes_id = $changeid;
  }
}