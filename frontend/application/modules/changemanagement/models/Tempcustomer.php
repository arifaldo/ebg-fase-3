<?php
/**
 * Tempcustomer model
 * 
 * @author 
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';

class Changemanagement_Model_Tempcustomer extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'CST';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) 
	{
		$set = new Settings();
		//ambil dari m_setting SKN RTGS
		$global_charges_skn_ori = 0; // $set->getSettingNew('global_charges_skn');
		$global_charges_rtgs_ori = 0; // $set->getSettingNew('global_charges_rtgs');
		
		//ambil dari m_setting FEE CHARGE
		$admin_fee_account_ori = 0; // $set->getSettingNew('admin_fee_account');
		$admin_fee_company_ori = 0; //$set->getSettingNew('admin_fee_company');
		
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
        $cust_id 	= $customer['CUST_ID'];
        
		//query from TEMP_USER
		/*$users = $this->dbObj->select()
						  			->from('TEMP_USER')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetchAll(Zend_Db::FETCH_ASSOC);*/
	
		//query from TEMP_CUSTOMER_ACCT
		/*$accts = $this->dbObj->select()
						  			->from('TEMP_CUSTOMER_ACCT')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetchAll(Zend_Db::FETCH_ASSOC);*/
        /*if(!count($accts))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Bank Accounts)';
			return false;
        }*/
	
		//query from TEMP_CUSTOMER_LIMIT
		/*$limits = $this->dbObj->select()
						  			->from('TEMP_CUSTOMER_LIMIT')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetchAll(Zend_Db::FETCH_ASSOC);
	*/
		//query from TEMP_FGROUP (NO RECORDS INSERTED TO TEMP_FGROUP ANYMORE)
		/*$fgroups = $this->dbObj->select()
							   ->from('TEMP_FGROUP')
						  	   ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  	   ->query()
						  	   ->fetchAll(Zend_Db::FETCH_ASSOC);
		*/
		//==========================================================================================================
		//insert to master table M_CUSTOMER
		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['CUST_CREATED']     = new Zend_Db_Expr('now()');
		$insertArr['CUST_CREATEDBY']   = $actor;
		$insertArr['CUST_UPDATED']     = new Zend_Db_Expr('now()');
		$insertArr['CUST_UPDATEDBY']   = $actor;
		
	try
		{
			$customerins = $this->dbObj->insert('M_CUSTOMER',$insertArr);
		}
	catch(exception $e)	
		{
			Zend_Debug::dump($e->getMessage());
		}
		if(!(boolean)$customerins) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
	
		
		//insert CHARGE SKN - M_CHARGES_OTHER
	 	$insertChargeSkn['CUST_ID']     	= $cust_id;
 		$insertChargeSkn['CHARGES_TYPE']	= '2';
 		$insertChargeSkn['CHARGES_NO']  	= '1';
 		$insertChargeSkn['CHARGES_CCY'] 	= 'IDR';
 		$insertChargeSkn['CHARGES_AMT'] 	= $global_charges_skn_ori;

 		$ChargeSknInsert = $this->dbObj->insert('M_CHARGES_OTHER',$insertChargeSkn);

		if(!(boolean)$ChargeSknInsert){
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer Limit)';
			return false;
		}
		
		//insert CHARGE SKN - M_CHARGES_OTHER
	 	$insertChargeRtgs['CUST_ID']     	= $cust_id;
 		$insertChargeRtgs['CHARGES_TYPE']	= '1';
 		$insertChargeRtgs['CHARGES_NO']  	= '1';
 		$insertChargeRtgs['CHARGES_CCY'] 	= 'IDR';
 		$insertChargeRtgs['CHARGES_AMT'] 	= $global_charges_rtgs_ori;

 		$ChargeRtgsInsert = $this->dbObj->insert('M_CHARGES_OTHER',$insertChargeRtgs);
 		
		if(!(boolean)$ChargeRtgsInsert){
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer Limit)';
			return false;
		}
		
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Added, Cust Id : '.$insertArr['CUST_ID']. ' Cust Name : '.$insertArr['CUST_NAME'].' Change id : '.$this->_changeId);
		
				
		//insert to master table M_USER sementara
		/*if(is_array($users) && count($users)) 
		{
			foreach($users as $key=>$val) 
			{
				$val['USER_ISLOGIN'] = 'N';
				$val['USER_LASTLOGIN'] = null;
				$val['USER_LASTACTIVITY'] = null;
				$val['USER_FAILEDATTEMPT'] = null;
				$val['CREATED'] = new Zend_Db_Expr('now()');
				$val['LASTUPDATED'] = new Zend_Db_Expr('now()');
				$val['CREATEDBY'] = $actor;
				$val['USER_HASH'] = null;
				$val['FORCE_CHANGE_PWD'] = null;
				$val['USER_FAILEDTOKEN'] = 0;
				$insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>'','USER_PWD'=>''));
				$userins = $this->dbObj->insert('M_USER',$insertArr);
				if(!(boolean)$userins) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(User)';
					return false;
				}
			}
		}*/
		
		//insert to master table M_CUSTOMER_ACCT
		/*if(is_array($accts) && count($accts)) 
		{
			foreach($accts as $key=>$val) 
			{
				$insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$acctins = $this->dbObj->insert('M_CUSTOMER_ACCT',$insertArr);
				if(!(boolean)$acctins) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Customer Acoounts)';
					return false;
				}
			}
		}*/
		
		//insert to master table M_CUSTOMER_LIMIT
		/*if(is_array($limits) && count($limits)) 
		{
			foreach($limits as $key=>$val) 
			{
				$insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$limitins = $this->dbObj->insert('M_CUSTOMER_LIMIT',$insertArr);
				if(!(boolean)$limitins) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Customer Limit)';
					return false;
				}
			}
		}*/
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		
		$transFailed = false;
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
		else
		{
			$custId = $customer['CUST_ID'];			
		}
		
		//query from TEMP_CUSTOMER_ACCT
		/*$accts = $this->dbObj->select()
							->from('TEMP_CUSTOMER_ACCT')
							->where('CHANGES_ID = ?',$this->_changeId)
							->query()
							->fetchAll(Zend_Db::FETCH_ASSOC);*/
		
		//query from TEMP_CUSTOMER_LIMIT
		/*$limits = $this->dbObj->select()
							->from('TEMP_CUSTOMER_LIMIT')
							->where('CHANGES_ID = ?',$this->_changeId)
							->query()
							->fetchAll(Zend_Db::FETCH_ASSOC);*/
	
		//update record customer
		$updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','CUST_CREATED'=>'','CUST_CREATEDBY'=>'','CUST_STATUS'=>'','CUST_MONTHLYFEE_TYPE'=>''));
		$updateArr['CUST_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['CUST_UPDATEDBY']   = $actor;
		
		$whereArr = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$customerupdate = $this->dbObj->update('M_CUSTOMER',$updateArr,$whereArr);
		
		//update record account
		/*if(is_array($accts) && count($accts)) 
		{
			$acctdel= $this->dbObj->delete('M_CUSTOMER_ACCT',$this->dbObj->quoteInto('CUST_ID = ?',$custId));
			foreach($accts as $key=>$val) 
			{
				$insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$acctins = $this->dbObj->insert('M_CUSTOMER_ACCT',$insertArr);
				if(!(boolean)$acctins) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Customer Acoounts)';
					return false;
				}
			}
		}*/
		
		//update record limit
		/*if(is_array($limits) && count($limits)) 
		{
			$limitdel = $this->dbObj->delete('M_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CUST_ID = ?',$custId));
			foreach($limits as $key=>$val) 
			{
				$insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$limitins = $this->dbObj->insert('M_CUSTOMER_LIMIT',$insertArr);
				if(!(boolean)$limitins) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Customer Limit)';
					return false;
				}
			}
		}*/
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Edited, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$updateArr['CUST_NAME'].' Change id : '.$this->_changeId);
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
						  
		
		$updateArr['CUST_STATUS']      = 1;   //1 = AKTIF
		$updateArr['CUST_SUGGESTED']   = $customer['CUST_SUGGESTED'];
		$updateArr['CUST_SUGGESTEDBY'] = $customer['CUST_SUGGESTEDBY'];
		$updateArr['CUST_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['CUST_UPDATEDBY']   = $actor;
		
		
		
		$whereArr = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$customerupdate = $this->dbObj->update('M_CUSTOMER',$updateArr,$whereArr);
		if(!(boolean)$customerupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Unsuspended, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$customer['CUST_NAME'].' Change id : '.$this->_changeId);

		/*
		//update all users status
		$updateArr = array('USER_STATUS' => 'A');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		*/
		
		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
		
		//=====================================================================
		//select all anchors made from this customer
		/*$anchors = $this->dbObj->select()
							   ->from('M_ANCHOR')
							   ->where('ANCHOR_CUST = ?',(string)$customer['CUST_ID'])
							   ->query()
							   ->fetchAll(Zend_Db::FETCH_ASSOC);
		//select all member ids of this customer
		$members = $this->dbObj->select()
							   ->from('M_MEMBER')
							   ->where('MEMBER_CUST = ?',(string)$customer['CUST_ID'])
							   ->query()
							   ->fetchAll(Zend_Db::FETCH_ASSOC);	*/				   
							   
		//======================================================================
		//START VALIDATION
		//Proses me �nonaktifkan Customer� hanya bisa dilakukan jika 
		//Customer tersebut sudah tidak mempunyai kewajiban apapun 
		//(data Loan Main Account sudah dalam status �Closed�).
		//Khusus untuk Customer yang berfungsi sebagai anchor,
		//maka fungsi ini hanya berlaku jika 
		//seluruh membernya sudah tidak mempunyai kewajiban apapun dengan Bank 
		//(data Loan Main Account seluruh member harus dengan status �Closed�)

		//find this customer's member Ids					   
		//see $members above					   
		//if(is_array($members) && count($members)) 
		//{
			/*$validation_member_where ='MEMBER_ID IN (';
			foreach($members as $val)
				$validation_member_where .= $this->dbObj->quoteInto('?,',$val['MEMBER_ID']);
			$validation_member_where = substr($validation_member_where,0,-1);
			$validation_member_where .= ')';*/
			
			/*
			$activeLoan = $this->dbObj->select()
									  ->from('T_LOAN')
									  ->where($validation_member_where)
									  ->where("LOAN_STATUS IN ('C','O')")
									  ->query()
									  ->fetchAll(Zend_Db::FETCH_ASSOC);
			if(count($activeLoan)>0) {
				$this->_errorMsg = 'Cannot approve. Unsettled loan';
				return false;
			}
			*/
									  
			/*$activeTransaction = $this->dbObj->select()
											 ->from('T_TX')
											 ->where($validation_member_where)
											 //->where("TX_STATUS IN ('WA','WH','WV','WR','BA','WE',RT')")
											 ->where("TX_STATUS NOT IN ('RJ','S','F','SP')")
											 ->query()
											 ->fetchAll(Zend_Db::FETCH_ASSOC);
			if(is_array($activeTransaction) && count($activeTransaction)) {
				$this->_errorCode = '80';
				$this->_errorMsg = 'Cannot approve. Unsettled transaction';
				return false;
			}*/
		//}					   
							   
						   	 
		
		//END OF VALIDATION					   
		//======================================================================

		//update customer status		  
		//$updateArr = array('CUST_STATUS' => 'I');
		
		$updateArr['CUST_STATUS']      = 2;
		$updateArr['CUST_SUGGESTED']   = $customer['CUST_SUGGESTED'];
		$updateArr['CUST_SUGGESTEDBY'] = $customer['CUST_SUGGESTEDBY'];
		$updateArr['CUST_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['CUST_UPDATEDBY']   = $actor;
		
		
		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$customerupdate = $this->dbObj->update('M_CUSTOMER',$updateArr,$whereArr);
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Suspended, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$customer['CUST_NAME'].' Change id : '.$this->_changeId);
		
	    /*//update all this customer's users status
		$updateArr = array('USER_STATUS' => 'I');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		
		//update member where cust_id = this customer id
		$whereArr = array('MEMBER_CUST = ?'=>(string)$customer['CUST_ID']);
		$updateArr = array('MEMBER_STATUS' => 'I');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$memberupdate = $this->dbObj->update('M_MEMBER',$updateArr,$whereArr);*/
		
		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	
	/*
	 if delete customer :
	 - delete user
	 - delete bank account
	 - delete user limit
	 - delete daily limit
	*/
	public function approveDelete($actor = null) 
	{  
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
						  
		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
        
        $suspect_query =  $this->dbObj->select()
        ->from(array('T'=>'T_TRANSACTION'),array('T.TRA_STATUS'))
        ->join(	array('P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER', array('P.*')
        );
        
        $suspect_query->where('T.TRA_STATUS = 1');
        $suspect_query->where('CUST_ID = (?)', (string)$customer['CUST_ID']);
        $suspect = $this->dbObj->fetchAll($suspect_query);
        
        
        
        if(empty($suspect)){
        //update status customer
	    $updateArr = array('CUST_STATUS' => 3);
	    $updateArr['CUST_SUGGESTED']   = $customer['CUST_SUGGESTED'];
		$updateArr['CUST_SUGGESTEDBY'] = $customer['CUST_SUGGESTEDBY'];
		$updateArr['CUST_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['CUST_UPDATEDBY']   = $actor;
	    
		
		$whereArr = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$customerupdate = $this->dbObj->update('M_CUSTOMER',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
        //END update status customer
		
		
		//update status user
	    $updateArr   = array('USER_STATUS' => 3);
	   
	    $whereArr    = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$userupdated = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		
		/*if(!(boolean)$userupdated) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Update failed(User)';
			return false;
		}*/
		//END update status user
		
		
		//update status maker limit
	    $updateArr   = array('MAKERLIMIT_STATUS' => 3);
	   
	    $whereArr    = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$makerlimitupdated = $this->dbObj->update('M_MAKERLIMIT',$updateArr,$whereArr);
		
		/*if(!(boolean)$makerlimitupdated) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Update failed(Maker Limit)';
			return false;
		}*/
		//END update status maker limit
		
		
		//update status daily limit
	    $updateArr = array('DAILYLIMIT_STATUS' => 3);
	   
	    $whereArr  = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$dailylimitupdated = $this->dbObj->update('M_DAILYLIMIT',$updateArr,$whereArr);
		
		/*if(!(boolean)$dailylimitupdated) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Update failed(Daily Limit)';
			return false;
		}*/
		//END update status daily limit
		
		
		//update status bank account
	    $updateArr   = array('ACCT_STATUS' => 3);
	   
	    $whereArr    = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$acctupdated = $this->dbObj->update('M_CUSTOMER_ACCT',$updateArr,$whereArr);
		
		/*if(!(boolean)$acctupdated) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Update failed(Bank Account)';
			return false;
		}*/
		//END update status bank account
		
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Deleted, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$customer['CUST_NAME'].' Change id : '.$this->_changeId);
		
		
		//delete token semua user di customer tersebut
		//query from M_USER
		$m_user = $this->dbObj->select()
						  	  ->from('M_USER')
						  	  ->where('CUST_ID = ?',(string)$customer['CUST_ID'])
						  	  ->query()->fetchAll();
		/*if(empty($m_user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }		*/

        //delete token kirim perintah ke host utk didelete
        foreach($m_user as $row)
        {
	        //integrate ke core (TOKEN), delete token lama dan add token baru	
		    //diremove jika hanya token di M_USER ada isinya, jika tidak ada maka tidak usah dihapus
		    if($row['TOKEN_ID'])
		    {
		        //REMOVE TOKEN
				$data   = new Service_Token($customer['CUST_ID'],$row['USER_ID']);
				$result = $data->removePairToken();
		        //$result['ResponseCode'] = '00';
				
				if($result['ResponseCode'] != '00')    //00 = 'success'
				{
			        $this->_errorMsg = $result['ResponseDesc'];
		            return false;
				}
		    }
        }
        //END delete token semua user di customer tersebut
		
		
		//delete temp customer
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			      return false;
		
		return true;
		}
		else{
		
			$this->_errorMsg = "Gagal menghapus nasabah. Nasabah masih memiliki transaksi yang berstatus suspect.";
			return false;
		}
	}
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() 
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		//delete from TEMP_CUSTOMER_ACCT
		$acctdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$acctdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Account)';
//			return false;
//		}

		//delete from TEMP_CUSTOMER_LIMIT
		$limitdelete = $this->dbObj->delete('TEMP_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$limitdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Limit)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		//delete from TEMP_CUSTOMER_ACCT
		$acctdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$acctdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Account)';
//			return false;
//		}

		//delete from TEMP_CUSTOMER_LIMIT
		$limitdelete = $this->dbObj->delete('TEMP_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$limitdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Limit)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteDelete() 
	{
	
	
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
		
		
		
	}
	
	
    public function approveDeactivate(){

	}

	public function approveActivate(){

	}
	
    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}
	
	
	
	
	
}