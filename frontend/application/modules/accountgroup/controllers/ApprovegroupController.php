<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Grouping.php';
class Accountgroup_approvegroupController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{		
		$this->_helper->layout()->setLayout('newlayout');
		$fields = array(
						'group_name'  => array('field' => 'GROUP_NAME',
											   'label' => $this->language->_('Group Name'),
											   'sortable' => true),
						'updated_by'  => array('field' => 'CREATEDBY',
											   'label' => $this->language->_('Updated By'),
											   'sortable' => true),
						'updated_date'  => array('field' => 'CREATE_DATETIME',
											   'label' => $this->language->_('Updated Date and Time'),
											   'sortable' => true),
						'act'   	  => array('field'    => 'ACTION',
											  'label'    => $this->language->_('Action'),
											  'sortable' => true)
				);
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$select = $this->_db->select()
					   ->from(array('M_GROUPING'),array('GROUP_ID','GROUP_NAME','CREATEDBY','CREATE_DATETIME','ACTION' => '(CASE WHEN GROUP_ISREQUESTDELETE = 1 THEN \'Delete\' WHEN GROUP_ISAPPROVED = 0 THEN \'Add\' END)')); 
		$select->where("GROUP_ISREQUESTDELETE = 1 OR GROUP_ISAPPROVED = 0");
		$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
	    $select->order($sortBy.' '.$sortDir);   
		
		$this->paging($select);
		
		$this->view->fields = $fields;
		
		$arr = $this->_db->fetchAll($select);
			
//		$data_val = array();
//		$i=0;
//		foreach ($arr as $data) {
//			$data['FPRIVI_DESC'] = $this->language->_($data['FPRIVI_DESC']);
//			array_push($data_val, $data);
//			//$data_val[$i++] = $data;
//		}
		
		if($this->_request->getParam('print') == 1){
                $data = $arr;//$this->_dbObj->fetchAll($select);
                $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Approve Group', 'data_header' => $fields));
        }
		
		if($this->_request->isPost() )
		{
			$Grouping = new Grouping();
			$param = $this->getRequest()->getParams();
			$group_id = $this->getRequest()->getParam('group_id');
			$act = $this->getRequest()->getParam('act');
			//$submit = $this->getRequest()->getParam('submit');
			$submit_approve = $this->getRequest()->getParam('submit_approve');
			$submit_reject = $this->getRequest()->getParam('submit_reject');
			
			
			if(!array_key_exists('1',array_flip($group_id)))
			{				
				$errorMsg = $this->language->_('Error: Please checked selection').'.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
			}
			else
			{
				foreach($group_id as $key => $value)
				{
					if($value=='0') unset($group_id[$key]);
				}
				//if($submit=='Approve')
				if($submit_approve==TRUE)
				{
					foreach($group_id as $key => $value)
					{
						if($act[$key]=='Delete') $Grouping->delete($key);
						else if($act[$key]=='Add') $Grouping->approve($key);
					}
				}
				//else if($submit=='Reject')
				else if($submit_reject==TRUE)
				{
					foreach($group_id as $key => $value)
					{
						if($act[$key]=='Delete') $Grouping->rejectDelete($key);
						else if($act[$key]=='Add') $Grouping->delete($key);
					}
				}
				Application_Helper_General::writeLog('GRAP','Approving Group');
				
				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
				$this->_redirect('/notification/success');
			}
			
			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}	
			}
		}
		else
		{
			Application_Helper_General::writeLog('GRAP','Viewing Approve Group');
		}
	}
}
