<?php

require_once 'Zend/Controller/Action.php';

class Memberbank_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$ccy = 'CCY';
    	$country = $this->language->_('Country');
		$benefbank = $this->language->_('Beneficiary Bank Name');
		$benefbankcode = $this->language->_('Beneficiary Bank Code');
		$nostrobank = $this->language->_('Nostro Bank Name');
		$nostrobankcode = $this->language->_('Nostro Swift Code');
		$nostroswift = $this->language->_('Nostro Swift Code');
		
		$model = new nostrobank_Model_Nostrobank();
	    $this->view->ccyArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCCYList(),'CCY_ID','CCY_ID'));
	    $this->view->nostroArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getNostroNames(),'NOSTRO_NAME','NOSTRO_NAME'));
		
	    $fields = array(
						'ccy'  => array('field' =>'CCY',
											      'label' => $ccy,
											      'sortable' => true),
						'country_name'     => array('field' => 'COUNTRY',
											      'label' => $country,
											      'sortable' => true),
	    				'bank_code'      => array('field' => 'BANK_CODE',
								    				'label' => $benefbankcode,
								    				'sortable' => true),
						'bank_name'      => array('field' => 'BANK_NAME',
											      'label' => $benefbank,
											      'sortable' => true),
						'nostro_name'      => array('field' => 'NOSTRO_NAME',
											      'label' => $nostrobank,
											      'sortable' => true),
	    				'nostro_code'      => array('field' => 'NOSTRO_SWIFTCODE',
								    				'label' => $nostrobankcode,
								    				'sortable' => true),
						'created'      => array('field' => 'CREATED',
											      'label' => 'Created Date',
											      'sortable' => true),
			    		'createdby'      => array('field' => 'CREATEDBY',
								    				'label' => 'Created By',
								    				'sortable' => true),
			    		'updated'      => array('field' => 'UPDATED',
								    				'label' => 'Updated Date',
								    				'sortable' => true),
			    		'updatedby'      => array('field' => 'UPDATEDBY',
								    				'label' => 'Updated By',
								    				'sortable' => true)
	    		
	    		
				      );


	    $filterlist = array('CCY','BENEF_BANK','NOSTRO_BANK');
		
		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
	    $page = $this->_getParam('page');
    
    
    
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
    //validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	    	$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'CCY'  => array('StringTrim','StripTags'),
							'BENEF_BANK'      => array('StringTrim','StripTags'),
							'NOSTRO_BANK'      => array('StringTrim','StripTags')
		);

		 $dataParam = array("CCY","BENEF_BANK","NOSTRO_BANK");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
		//$filter_lg = $this->_getParam('filter');	

		//$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_MEMBER_BANK'),array('ID','CCY','COUNTRY'	=> new Zend_Db_Expr(" CONCAT( COUNTRY_CODE , ' - ' , COUNTRY_NAME  ) "),'BANK_CODE','BANK_NAME','NOSTRO_NAME','NOSTRO_SWIFTCODE','COUNTRY_CODE','COUNTRY_NAME','CREATED','CREATEDBY','UPDATED','UPDATEDBY'));

		// echo $select;
		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf) 
		if($filter == true || $csv || $pdf || $this->_request->getParam('print')) 
		{  
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fccy = $zf_filter->getEscaped('CCY');
			$fbenef_bank     = $zf_filter->getEscaped('BENEF_BANK');
			$fnostro_name    = $zf_filter->getEscaped('NOSTRO_BANK');

	        if($fccy) $select->where('UPPER(CCY) LIKE '.$this->_db->quote('%'.strtoupper($fccy).'%'));
	        if($fbenef_bank)     $select->where('UPPER(BANK_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fbenef_bank).'%'));
	        if($fnostro_name)    $select->where('UPPER(NOSTRO_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fnostro_name).'%'));
			
			$this->view->ccy = $fccy;
			$this->view->benef_bank     = $fbenef_bank;
			$this->view->nostro_name    = $fnostro_name;
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);  
		
		$select = $this->_db->fetchall($select);

		//menghilangkan index/key BANK_CODE utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		//echo '<pre>';
		//print_r($selectPdfCsv);die;
		foreach($selectPdfCsv as $key => $row)
		{

			unset($selectPdfCsv[$key]['COUNTRY_CODE']);
			unset($selectPdfCsv[$key]['COUNTRY_NAME']);
			unset($selectPdfCsv[$key]['ID']);
//		    unset($selectPdfCsv[$key]['BANK_CODE']);
		}
		

		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
				$this->_helper->download->csv($header,$selectPdfCsv,null,'MEMBER BANK');
				Application_Helper_General::writeLog('NBLS','Download CSV Member Bank');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$selectPdfCsv,null,'MEMBER BANK');
				Application_Helper_General::writeLog('NBLS','Download PDF Member Bank');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Member Bank', 'data_header' => $fields));
       	}
		else
		{		
				Application_Helper_General::writeLog('NBLS','View Member Bank');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}

}