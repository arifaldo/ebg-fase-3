<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class bgapprovalmatrix_RepairapprovalgroupController extends customer_Model_Customer
{
  protected $_moduleDB = 'RTF'; //masih harus diganti

  public function indexAction()
  {
    if (!$this->view->hasPrivilege("RAMC")) {
      return $this->_redirect("/bgapprovalmatrix");
    }

    $this->view->changes_id = $this->_getParam("changes_id");
    $this->_helper->layout()->setLayout('popup');
    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if (count($temp) > 1) {
      if ($temp[0] == 'F' || $temp[0] == 'S') {
        if ($temp[0] == 'F')
          $this->view->error = 1;
        else
          $this->view->success = 1;
        $msg = '';
        unset($temp[0]);
        foreach ($temp as $value) {
          if (!is_array($value))
            $value = array($value);
          $msg .= $this->view->formErrors($value);
        }
        $this->view->report_msg = $msg;
      }
    }



    $this->setbackURL('/' . $this->_request->getModuleName());


    $select = $this->_db->select()
      ->from(array('MAB' => 'M_APP_GROUP_BUSER'));


    $result = $this->_db->fetchAll($select);

    $listgroup = $this->_db->select()
      ->from(array('MAB' => 'M_APP_GROUP_BUSER'))
      ->group('MAB.GROUP_BUSER_ID');
    $resultlist = $this->_db->fetchAll($listgroup);

    $this->view->error = 0;
    $userlist = "";

    foreach ($resultlist as $key => $value) {
      $selectUser = $this->_db->select()
        ->from(array('M_APP_GROUP_BUSER'), array('BUSER_ID'))
        ->where('GROUP_BUSER_ID = ?', $value['GROUP_BUSER_ID'])
        ->query()->fetchall();
      foreach ($selectUser as $val) {
        if (empty($userlist))
          $userlist .= $val['BUSER_ID'];
        else
          $userlist .= ',' . $val['BUSER_ID'];
      }

      $resultlist[$key]['BUSERLIST'] = $userlist;
      $userlist = '';
    }

    $this->view->listgroup1  = $resultlist;

    if ($result) {
      $this->view->data = $result;
      foreach ($result as $key => $value) {
        $special = 'S_' . $cust_id;
        if ($value['GROUP_BUSER_ID'] == $special) {
          $this->view->spname = $value['GROUP_NAME'];
          $selectUser = $this->_db->select()
            ->from(array('M_APP_GROUP_BUSER'), array('BUSER_ID'))
            ->where('GROUP_BUSER_ID = ?', $special)
            // echo $selectUser;die;
            ->query()->fetchall();
          $userlist = '';
          foreach ($selectUser as $val) {
            if (empty($userlist))
              $userlist .= $val['BUSER_ID'];
            else
              $userlist .= ',' . $val['BUSER_ID'];
          }
          $this->view->splist1 = $userlist;
        }
      }
    }






    $cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;
    // print_r($cust_id);die;
    $selectuser = $this->_db->select()
      ->from(array('A' => 'M_BUSER'))
      ->where('A.BUSER_STATUS != ?', 3);
    //var_dump($selectuser->query()->fetchAll())
    $this->view->dataact = $selectuser->query()->fetchAll();

    $submit = $this->_getParam('submit');
    $reset = $this->_getParam('reset');
    $data = $this->_request->getParams();

    if ($submit) {

      if ($data['sg_user'] != NUll) {
        $sg_user = $data['sg_user'];
      } else {
        $sg_user = $data['sg_user1'];
      }

      $content = array();
      $content['sg_name'] = $data['sg_group'];
      $content['sg_user'] = $sg_user;
      $content['cust_id'] = $data['cust_id'];
      $content['g_list'] = $data['group'];
      $content['g_id'] = $data['groupid'];
      $content['g_user'] = $data['user'];
      $sessionNamespace = new Zend_Session_Namespace('approvalgroupbackend');
      // $sessionNamespace->mode = 'Add';
      $sessionNamespace->content = $content;
      // echo "<pre>";
      // print_r($content);die;
      // $this->_redirect('/directdebit/index/confirm');
      $url = '/bgapprovalmatrix/repairapprovalpolicy/index/changes_id/' . $this->_getParam("changes_id");
      $this->_redirect($url);
      // Application_Helper_General::writeLog('BMUD','Update Boundary Member ('.$custid.')');
    }

    $check_global_changes = $this->_db->select()
      ->from(["T_GLOBAL_CHANGES"], ["CHANGES_STATUS"])
      ->where("CHANGES_ID = ? ", $this->_getParam("changes_id"))
      ->query()->fetchAll();

    if (count($check_global_changes) == 0) {
      return $this->_redirect("/home/dashboard");
    }

    if ($check_global_changes[0]["CHANGES_STATUS"] != "RR") {
      return $this->_redirect("/home/dashboard");
    }

    // print_r($resultdata);die;
    $listGroup = $this->_db->fetchAll(
      $this->_db->select()
        ->from('M_APP_GROUP_BUSER', array('BUSER_ID', 'GROUP_NAME'))

    );
    // print_r($listGroup);die;
    $listuser = '';
    if (!empty($listGroup)) {
      foreach ($listGroup as $key => $value) {
        $listuser .= $value['BUSER_ID'] . ' , ';
      }
      // print_r($listGroup);die;
      $this->view->splist = $listuser;
      $this->view->spname = $listGroup['0']['GROUP_NAME'];
    }

    Zend_Session::namespaceUnset('approvalgroup' . $cust_id);
    $sessionNamespace   = new Zend_Session_Namespace('approvalgroup' . $cust_id);
    $content    = $sessionNamespace->content;
    if (!empty($content['sg_user'])) {
      $this->view->splist = $content['sg_user'];
    }
    //echo '<pre>';
    //    var_dump($content);die;
    if (!empty($content['g_list'])) {
      $datanew = array();
      $alphabet = array('A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'E' => 5, 'F' => 6, 'G' => 7, 'H' => 8);

      foreach ($content['g_id'] as $no => $value) {
        //var_dump($value);
        $datanew[$no]['GROUP_NAME'] = $content['g_list'][$no];
        $datanew[$no]['USERLIST'] = $content['g_user'][$no];
        $datanew[$no]['GROUP_USER_ID'] = 'N_' . $cust_id . '_' . $alphabet[$value];
      }
      //echo '<pre>';
      //var_dump($datanew);die;
      $this->view->listgroup = $datanew;
    }

    // print_r($listGroup);die;



    $getAppGroup = $this->getAppGroup($cust_id);
    $getAppGroupArray = null;
    foreach ($getAppGroup as $row) {
      $getAppGroupArray[$row['GROUP_USER_ID']][] = $row['USER_ID'];
    }
    $this->view->appGroup = $getAppGroupArray;


    $getAppBoundaryGroup = $this->getAppBoundary($cust_id);
    $boundaryGroup = null;
    foreach ($getAppBoundaryGroup as $row) {
      //example : N_CUSTCHRIS_01 & S_CUSTCHRIS
      $explodeGroup = explode('_', $row['GROUP_BUSER_ID']);

      if ($explodeGroup[0] == 'N')      $group_desc = 'Group ' . (int)$explodeGroup[2];
      else if ($explodeGroup[0] == 'S') $group_desc = 'Special Group';


      $boundaryGroup[$row['BOUNDARY_ID']]['GROUP_BUSER_ID'][] = $group_desc;
      $boundaryGroup[$row['BOUNDARY_ID']]['BOUNDARY_MIN']    = $row['BOUNDARY_MIN'];
      $boundaryGroup[$row['BOUNDARY_ID']]['BOUNDARY_MAX']    = $row['BOUNDARY_MAX'];
      $boundaryGroup[$row['BOUNDARY_ID']]['CCY_BOUNDARY']    = $row['CCY_BOUNDARY'];
    }
    $this->view->appBoundary = $boundaryGroup;

    $this->view->benefUser   = $this->getBenefUser($cust_id);

    //----------------------------------------------- END Bank Account------------------------------------------










    $this->view->status_type = $this->_masterglobalstatus;
    $this->view->modulename = $this->_request->getModuleName();

    //insert log
    try {
      $this->_db->beginTransaction();

      Application_Helper_General::writeLog('CCLS', 'View Customer Detail [' . $cust_id . ']');

      $this->_db->commit();
    } catch (Exception $e) {
      var_dump($e);
      die;
      $this->_db->rollBack();
    }

    /*
        $cektrx = $this->_db->select()
                ->from('T_PSLIP');
          $cektrx -> where("CUST_ID = ? ",$cust_id);
          $cektrx -> where("PS_STATUS = ? ",'1');
          // echo $select2;die;
          $cektrx = $cektrx->query()->FetchAll(); 

        //   $select2 = $this->_db->select()
        //         ->from('TEMP_APP_BOUNDARY');
        // $select2 -> where("CUST_ID LIKE ".$this->_db->quote($cust_id));
        // // echo $select2;die;
        // $cek = $select2->query()->FetchAll();  

        if(!empty($cektrx)){
          $this->view->error = true;
          $this->view->error_msg  = 'Not allowed to make changes. There is a transaction on payment workflow awaiting for approval.';
        }
*/
    $selectdatachange = $this->_db->select()
      ->from('T_GLOBAL_CHANGES');
    $selectdatachange->where("CUST_ID = " . $this->_db->quote($cust_id) . " OR CUST_ID ='BANK'");
    $selectdatachange->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS ='RR'");

    $selectdatachange->where("DISPLAY_TABLENAME = ? ", 'Backend Approver Matrix');

    // echo $selectdatachange;die();
    $selectdatachange1 = $selectdatachange->query()->FetchAll();

    if (!empty($selectdatachange1)) {
      $this->view->error2 = true;
      $this->view->error_msg2  = 'Not allowed to make changes. There is suggestion awaiting for approval on data changes requests.';
    }
  }
}
