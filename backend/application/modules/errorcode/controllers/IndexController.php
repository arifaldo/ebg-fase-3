<?php

require_once 'Zend/Controller/Action.php';

class Errorcode_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{ 
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$countrycode = $this->language->_('Code');
		$countryname = $this->language->_('Desc');
		$rerun = $this->language->_('Is Re-Run');



	    $fields = array(
						'code'  => array('field' => 'CODE',
											      'label' => $countrycode,
											      'sortable' => true),
						'desc'     => array('field' => 'DESC',
											      'label' => $countryname,
											      'sortable' => true),
						'isrerun'     => array('field' => 'IS_RERUN',
											      'label' => $rerun,
											      'sortable' => true)
				      );

	    $filterlist = array("CODE");

		$this->view->filterlist = $filterlist;


		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','CODE');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'CODE'  => array('StringTrim','StripTags'),
		);

		$dataParam = array("CODE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		//$filter_lg = $this->_getParam('filter');

		$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_ERRORCODE'),$getData);

		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf)
		if($filter == true || $csv || $pdf || $this->_request->getParam('print'))
		{
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fcountry_name = $zf_filter->getEscaped('DESC');

	        if($fcountry_name) $select->where('UPPER(DESC) LIKE '.$this->_db->quote('%'.strtoupper($fcountry_name).'%'));

			$this->view->country_name = $fcountry_name;
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);

		$select = $this->_db->fetchall($select);

	  $this->view->countryData = $select;

		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {
				$this->_helper->download->csv($header,$select,null,'Code List');
				Application_Helper_General::writeLog('COLS','Download CSV Code List');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$select,null,'Code List');
				Application_Helper_General::writeLog('COLS','Download PDF Code List');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Code List', 'data_header' => $fields));
       	}
		else
		{
				Application_Helper_General::writeLog('COLS','View Code List');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }

	}

}
