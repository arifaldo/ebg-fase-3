<?php

require_once 'Zend/Controller/Action.php';

//NOTE:
//Watch the modulename, filename and classname carefully
class popup_BranchcoverageController extends Application_Main 
{
	public function initController()
	{       
	
		$this->_helper->layout()->setLayout('popup');
	}


	public function indexAction() 
	{
	
	    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
	
	
	    $fields = array(
						'branch_code'      => array('field' => 'BRANCH_CODE',
											      'label' => 'Branch Code',
											      'sortable' => true),
						'branch_name'           => array('field' => 'BRANCH_NAME',
											      'label' => 'Branch Name',
											      'sortable' => true),
						'bank_address'  => array('field' => 'BANK_ADDRESS',
											      'label' => 'Address',
											      'sortable' => false),
						'contact'  => array('field' => 'CONTACT',
											      'label' => 'Contact',
											      'sortable' => false),
						/*'swift_code'     => array('field' => 'SWIFT_CODE',
											      'label' => 'Swift Code',
											      'sortable' => true)*/
	                  );
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$branch_code    = $this->_getParam('branch_code');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_BRANCH_COVERAGE')); 
		$select->where("A.BRANCH_CODE = ?",$branch_code);
		
		$select2 = $this->_db->select()
					        ->from(array('A' => 'M_BRANCH')); 
		$select2->where("A.BRANCH_CODE = ?",$branch_code);	
		$arr = $this->_db->fetchRow($select2);
		
		$this->view->branchName = $arr['BRANCH_NAME'];

		
					        
		
		//$this->view->success = true;
		
	    $select->order($sortBy.' '.$sortDir);   
		$this->paging($select);
		$this->frontendLog('V',$this->_moduleDB,null);
		$this->view->fields = $fields;
		//$this->view->filter = $filter;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}
}
