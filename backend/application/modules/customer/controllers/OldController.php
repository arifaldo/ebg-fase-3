<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class customer_NewController extends customer_Model_Customer
 {
 
    public function oldAction() 
    {
		$this->_helper->layout()->setLayout('newlayout');
        //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');    
   
        $this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');
    
        $error_remark = null; $key_value = null;

        //convert limit idr & usd agar bisa masuk ke database
	    $limitidr = $this->_getParam('cust_limit_idr');
	    $limitidr = Application_Helper_General::convertDisplayMoney($limitidr);
	    $this->_setParam('cust_limit_idr',$limitidr); 

	    $limitusd = $this->_getParam('cust_limit_usd');
	    $limitusd = Application_Helper_General::convertDisplayMoney($limitusd);
	    $this->_setParam('cust_limit_usd',$limitusd); 
	    //END convert limit idr & usd agar bisa masuk ke database
      
  	
		if($this->_request->isPost() && $this->_getParam('submit') == $this->language->_('Submit'))
		{
			$this->view->inputParam = $this->getRequest()->getParams();
			$cust_id = strtoupper($this->_getParam('cust_id'));
			$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>12)))? $cust_id : null;
			
		    /*echo strtoupper($this->_getParam('cust_id'));
			die;*/
			
			$customer_filter = array();
			$haystack_cust_type = array($this->_custType['code']['company'],$this->_custType['code']['individual']);
			$haystack_step_approve = array($this->_masterhasStatus['code']['yes'],$this->_masterhasStatus['code']['no']);
			$cust_acc = array();
			$cust_limit = array();
  	   		$tempacc = array('idr','usd');
      	  
			$filters = array('cust_id'           => array('StripTags','StringTrim','StringToUpper'),
			                 'cust_cif'          => array('StripTags','StringTrim'),
			                 'cust_name'         => array('StripTags','StringTrim'),
			                 'cust_type'         => array('StripTags','StringTrim','StringToUpper'),
			                 'cust_workfield'    => array('StripTags','StringTrim'),
						     'cust_address'      => array('StripTags','StringTrim'),
			                 'cust_city'         => array('StripTags','StringTrim'),
			                 'cust_zip'          => array('StripTags','StringTrim'),
			                 'country_code'      => array('StripTags','StringTrim'),
							 'cust_province'     => array('StripTags','StringTrim'),
			                 'cust_contact'      => array('StripTags','StringTrim'),
			                 'cust_phone'        => array('StripTags','StringTrim'),
						     'cust_ext'          => array('StripTags','StringTrim'),
			                 'cust_fax'          => array('StripTags','StringTrim'),
						     'cust_email'        => array('StripTags','StringTrim'),
			                 'cust_website'      => array('StripTags','StringTrim'),
			                 'cust_limit_idr'      => array('StripTags','StringTrim'),
			                 'cust_limit_usd'      => array('StripTags','StringTrim'),
			                //'cust_charges_status' => array('StripTags','StringTrim'),
			                 'cust_special_rate'   => array('StripTags','StringTrim'),
			                 //'cust_monthlyfee_status'  => array('StripTags','StringTrim'),
			                 //'cust_token_auth'  => array('StripTags','StringTrim'),
							 /* 'cust_emobile'        => array('StripTags','StringTrim'),
			                 'cust_isemobile'      => array('StripTags','StringTrim'), */
			                );

			$validators =  array('cust_id'        => array('NotEmpty',
      	                                                   'Alnum',
      	                                                    array('StringLength',array('min'=>6,'max'=>16)),
      	                                                       array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_ID')),
      	                                                       array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_ID')),
      	                                                       'messages' => array($this->language->_('Can not be empty'),
      	                                                                           $this->language->_('Invalid Company Code Format'),
//      	                                                                           $this->language->_('Invalid Company Code Format'),
																						$this->language->_('Company Code length min').' 6 '.$this->language->_('max') . '16',
      	                                                                           $this->language->_('Company code already in use. Please use another'),
      	                                                                           $this->language->_('Company code already suggested. Please use another'))
      	                                                      ),
          
      	                     'cust_cif'                 => array(
      	                                                      //array('StringLength',array('min'=>1,'max'=>19)),
												               array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_CIF')),
                                                               array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_CIF')),
												               'allowEmpty' => true,
												               'messages' => array(//$this->getErrorRemark('04','Company CIF'),
                                                                                   $this->language->_('CIF is already registered. Please use another'),
                                                                                   $this->language->_('CIF is already suggested. Please use another')
                                                                                   )
												              ),                            

							 'cust_name'                => array('NotEmpty',
												                //array('StringLength',array('min'=>1,'max'=>80)),
												               'messages' => array($this->language->_('Can not be empty'),
								 					  	      	         		   //$this->getErrorRemark('04','Company Name')
								 					  	      	         		   )
												              ),					              
												              
                             'cust_type'                => array('allowEmpty' => true,
												              	 array('StringLength',array('max'=>16)),
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												              	 				   $this->language->_('The maximum characters allowed is').' 16',
												                                   //$this->getErrorRemark('22','Company Type')
												                                   )
											                  ),

                            					              
                             'cust_workfield'            => array('allowEmpty' => true,
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												                                   //$this->getErrorRemark('22','Company Type')
												                                   )
											                  ),
											                  
											                  
						     'cust_address'            => array('allowEmpty' => true,
												                array('StringLength',array('max'=>200)),
												               'messages' => array(
												                					$this->language->_('The maximum characters allowed is').' 200'
												                                  // $this->getErrorRemark('04','Company Address')
												                                   )
												              ),
												  
						  
												  
						     'cust_city'               => array(array('StringLength',array('min'=>1,'max'=>20)),
												               'allowEmpty' => true,
												               'messages' => array(
												                                   $this->language->_('The maximum characters allowed is').' 20'
												                                  )
												              ),
												  
						     'cust_zip'           => array(//array('StringLength',array('min'=>1,'max'=>10)),
												               'allowEmpty' => true,
						     									'Digits',
												               'messages' => array(
												                                // $this->getErrorRemark('04','Zip')
												                                  )
												              ),
												              
						     'country_code'           => array(//array('StringLength',array('min'=>1,'max'=>10)),
												               'allowEmpty' => true,
												               'messages' => array(
												                              // $this->getErrorRemark('04','Country')
												                                  )
												              ),				              
												  
                           'cust_province'      => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
												               'messages' => array($this->language->_('The maximum characters allowed is').' 128')
												              ),

                           'cust_contact'       => array('allowEmpty' => true,
												         array('StringLength',array('min'=>1,'max'=>128)),
												         'messages' => array(
												                             $this->language->_('The maximum characters allowed is').' 128')
												         ),

						    'cust_phone'        => array('Digits',
												         array('StringLength',array('min'=>1,'max'=>20)),
												         'allowEmpty' => true,
												         'messages' => array($this->language->_('Invalid phone number format'),
												                             $this->language->_('The maximum characters allowed is').' 20'
												                            )
												              ),					         
												         
                            'cust_ext'          => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
                            									'Digits',
												               'messages' => array(
												                                  $this->language->_('The maximum characters allowed is').' 128'
												                                   )
												              ),
												  
                            'cust_fax'          => array('Digits',
												         'allowEmpty' => true,
												         'messages' => array(
												                            $this->language->_('Invalid fax number format')
												                            )
												              ),
												  
                            'cust_email'        => array( //new Application_Validate_EmailAddress(),
												          array('StringLength',array('min'=>1,'max'=>128)),
												          'allowEmpty' => true,
												          'messages' => array(
												                             //$this->language->_('Invalid email format'),
												                             $this->language->_('Email lenght cannot be more than 128'),
												                             )
                                                        ),
												  
                            'cust_website'      => array(new Application_Validate_Hostname(),
                                                               array('StringLength',array('min'=>1,'max'=>100)),
												               'allowEmpty' => true,
                                                               'messages' => array($this->language->_('Invalid URL format'),
                                                                                   $this->language->_('invalid format'))
                                                              ),

                           /* 'cust_charges_status'      => array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                    $this->language->_('invalid format')
                                                                                          )
                                                              ),                                    

                             'cust_monthlyfee_status'   => array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                   $this->language->_('invalid format')
                                                                                          )
                                                              ),        
                             'cust_token_auth'      	=> array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                   $this->language->_('invalid format')
                                                                                          )
                                                              ),*/        
                                                                                          

                             'cust_special_rate'      	=> array(array('StringLength',array('min'=>1,'max'=>100)),
												                      'allowEmpty' => true,
                                                                      'messages' => array(
                                                                                       $this->language->_('invalid format')
                                                                                          )
                                                              ),
                            'cust_limit_idr' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 1 - 9999999999.99',
															'Must be numeric values',
															)),
							'cust_limit_usd' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 1 - 9999999999.99',
															'Must be numeric values',
															)),                                  

                            /*  'cust_emobile'      => array(
                                                          //array('StringLength',array('min'=>1,'max'=>100)),
                                                          array('Db_NoRecordExists',array('table'=>'M_CUSTOMER','field'=>'CUST_EMOBILE')),
      	                                                  array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER','field'=>'CUST_EMOBILE')),
                                                          'allowEmpty' => true,
                                                          'messages' => array(
                                                                            'Emobile No. already assigned in other company.',
      	                                                                    'Emobile No already suggested. Please use another'
                                                                             )
                                                          ),  
                                                              
                            'cust_isemobile'      => array(//array('StringLength',array('min'=>1,'max'=>100)),
                                                           'Digits',
												           'allowEmpty' => true,
                                                           'messages' => array(
                                                                               'Invalid emobile number format',
                                                                              )
                                                           ),  */                                                                     
							);

							
     
       $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
       
		//validasi user_id : paling sedikit harus punya 1 huruf dan 1 angka
	      if($zf_filter_input->cust_id)
	      {
	         $user_id =  $zf_filter_input->cust_id;
	         $check_alpha  = 0;
	         $check_digits = 0;
	         $strlen_user_id = strlen($user_id);
	
	         for($i=0; $i<$strlen_user_id; $i++)
	         {
	            if(Zend_Validate::is($user_id[$i],'Alpha'))  $check_alpha  = 1;
	            if(Zend_Validate::is($user_id[$i],'Digits')) $check_digits = 1;
	         }
	
	         if($check_alpha == 1 && $check_digits == 1) $flag_user_id = 'T';
	         else $flag_user_id = 'F';
	      }
	      else
	      {
	         $flag_user_id = 'T';
	      }
       
		//validasi multiple email
	      if($this->_getParam('cust_email'))
	      {
			$validate = new validate;
	      	$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('cust_email'));
	      }
	      else
	      {
	      	$cek_multiple_email = true;
	      }
       
       //validasi emobile, cust phone dan cust fax = harus dimulai dengan '62'
	   $flag_cust_emobile = 'T';
       /* $flag_cust_isemobile_error = false;
	   if($zf_filter_input->cust_emobile) 
       {
           $cust_emobile_62 = substr($zf_filter_input->cust_emobile,0,2);
         
           if($cust_emobile_62 != '62') $flag_cust_emobile = 'F';
           else $flag_cust_emobile = 'T';
       }
       else 
       {
           if($zf_filter_input->cust_isemobile == '' || is_null($zf_filter_input->cust_isemobile) || $zf_filter_input->cust_isemobile == 0)
           { 
              $flag_cust_emobile = 'T';
           }
           else
           { 
              $flag_cust_emobile = 'F';
              $flag_cust_isemobile_error = true;
           }
        } */
       
       
       $flag_cust_phone = 'T';
      /* if($zf_filter_input->cust_phone)
       {
          $cust_phone_62   = substr($zf_filter_input->cust_phone,0,2);
          
          if($cust_phone_62 != '62') $flag_cust_phone = 'F';
          else $flag_cust_phone = 'T';
       }
       else $flag_cust_phone = 'T';*/
       
       $flag_cust_fax = 'T';
       /*if($zf_filter_input->cust_fax)
       {
          $cust_fax_62     = substr($zf_filter_input->cust_fax,0,2);
     
          if($cust_fax_62 != '62') $flag_cust_fax = 'F';
          else $flag_cust_fax = 'T';
       }
       else $flag_cust_fax = 'T';*/
      //END validasi emobile = harus dimulai dengan '62'
      
       
   
       
       
       
       if($zf_filter_input->isValid() && $flag_user_id == 'T' && $flag_cust_emobile == 'T' && $flag_cust_phone == 'T' && $flag_cust_fax == 'T' && $cek_multiple_email == true)
       {  
      	  $info = 'Customer ID = '.$zf_filter_input->cust_id.', Customer Name = '.$zf_filter_input->cust_name;
          $cust_data = $this->_custData;
          
	  	  foreach($validators as $key=>$value)
	  	  {
	  	    if($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;	
	  	  }
	  	  
	  	  
	  	  try 
	      {
		     $this->_db->beginTransaction();
		     
		     if(is_null($cust_data['CUST_CHARGES_STATUS']) || $cust_data['CUST_CHARGES_STATUS'] == '') $cust_data['CUST_CHARGES_STATUS'] = 0;
		     if(is_null($cust_data['CUST_MONTHLYFEE_STATUS']) || $cust_data['CUST_MONTHLYFEE_STATUS'] == '')   $cust_data['CUST_MONTHLYFEE_STATUS'] = 0;
		     if(is_null($cust_data['CUST_TOKEN_AUTH']) || $cust_data['CUST_TOKEN_AUTH'] == '')   $cust_data['CUST_TOKEN_AUTH'] = 0;
		     if(is_null($cust_data['CUST_SPECIAL_RATE']) || $cust_data['CUST_SPECIAL_RATE'] == '')     $cust_data['CUST_SPECIAL_RATE'] = 0;
		      if(is_null($cust_data['CUST_LIMIT_IDR']) || $cust_data['CUST_LIMIT_IDR'] == '')     $cust_data['CUST_LIMIT_IDR'] = 0;
		      if(is_null($cust_data['CUST_LIMIT_USD']) || $cust_data['CUST_LIMIT_USD'] == '')     $cust_data['CUST_LIMIT_USD'] = 0;
		     // if(is_null($cust_data['CUST_ISEMOBILE']) || $cust_data['CUST_ISEMOBILE'] == '')           $cust_data['CUST_ISEMOBILE'] = 0;
		          
		    
		     
		     $cust_data['CUST_STATUS'] = 1;
		     $cust_data['CUST_CREATED']     = null;
		     $cust_data['CUST_CREATEDBY']   = null;
		     $cust_data['CUST_UPDATED']     = null;
		     $cust_data['CUST_UPDATEDBY']   = null;
		     
		     
		     $cust_data['CUST_SUGGESTED']    = new Zend_Db_Expr('now()');
		     $cust_data['CUST_SUGGESTEDBY']  = $this->_userIdLogin;
		     
		     
		     /* Zend_Debug::dump( $cust_data );
	          die;*/
		     
		     // insert ke T_GLOBAL_CHANGES
		     $change_id = $this->suggestionWaitingApproval('Customer',$info,$this->_changeType['code']['new'],null,'M_CUSTOMER','TEMP_CUSTOMER',$zf_filter_input->cust_id,$zf_filter_input->cust_name,$zf_filter_input->cust_id,$zf_filter_input->cust_name);
		     $this->insertTempCustomer($change_id,$cust_data);
		     
		    
		     //if(count($customer_filter)) $this->setUserData($customer_filter,$zf_filter_input,$change_id);
		  	 //if($countacc) $this->insertTempAcc($countacc,$change_id,$cust_acc);
		  	 //if(count($cust_limit)) $this->insertTempLimit($change_id,$cust_limit);
		  	 
		     //log CRUD
			 Application_Helper_General::writeLog('CCAD','Customer has been added, Cust ID : '.$zf_filter_input->cust_id. ' Cust Name : '.$zf_filter_input->cust_name.' Change id : '.$change_id);
		     
		     $this->_db->commit();
		     
		     $this->_redirect('/notification/submited/index');
		     //$this->_redirect('/notification/success/index');
	  	  }
		  catch(Exception $e)
		  {
		    $this->_db->rollBack();
		    $error_remark = 'exception';
	      }
		
	      
		  if(isset($error_remark))
		  {
		  }
		  else
		  {
		    $this->view->success = 1; 
		    $msg   = 'Record added sucessfully';
		    $key_value = $zf_filter_input->cust_id;
		    $this->view->customer_msg = $msg;
		   //insert log
		   try 
		   {
			  $this->_db->beginTransaction();
			  
			  Application_Helper_General::writeLog('CCAD',' Create Customer ['.$this->view->cust_id.']');
			  
			  $this->_db->commit();
		   }
		   catch(Exception $e)
		   {
			  $this->_db->rollBack();
			  Application_Log_GeneralLog::technicalLog($e);
		   }
		  } 
	  }// END IF IS VALID
	  else
	  { 
	    $this->view->error = 1;
	  	$this->view->cust_id   = ($zf_filter_input->isValid('cust_id'))? $zf_filter_input->cust_id : $this->_getParam('cust_id');
	  	$this->view->cust_name = ($zf_filter_input->isValid('cust_name'))? $zf_filter_input->cust_name : $this->_getParam('cust_name');
	    $this->view->cust_type = ($zf_filter_input->isValid('cust_type'))? $zf_filter_input->cust_type : $this->_getParam('cust_type'); 
	  	$this->view->cust_workfield = ($zf_filter_input->isValid('cust_workfield'))? $zf_filter_input->cust_workfield : $this->_getParam('cust_workfield'); 
		$this->view->cust_address = ($zf_filter_input->isValid('cust_address'))? $zf_filter_input->cust_address : $this->_getParam('cust_address');
		$this->view->cust_city = ($zf_filter_input->isValid('cust_city'))? $zf_filter_input->cust_city : $this->_getParam('cust_city');
		$this->view->cust_zip  = ($zf_filter_input->isValid('cust_zip'))? $zf_filter_input->cust_zip : $this->_getParam('cust_zip');
		$this->view->cust_province = ($zf_filter_input->isValid('cust_province'))? $zf_filter_input->cust_province : $this->_getParam('cust_province');
		$this->view->country = ($zf_filter_input->isValid('country'))? $zf_filter_input->country : $this->_getParam('country');
		$this->view->cust_contact  = ($zf_filter_input->isValid('cust_contact'))? $zf_filter_input->cust_contact : $this->_getParam('cust_contact');
		$this->view->cust_phone    = ($zf_filter_input->isValid('cust_phone'))? $zf_filter_input->cust_phone : $this->_getParam('cust_phone');
		$this->view->cust_ext      = ($zf_filter_input->isValid('cust_ext'))? $zf_filter_input->cust_ext : $this->_getParam('cust_ext');
		$this->view->cust_fax      = ($zf_filter_input->isValid('cust_fax'))? $zf_filter_input->cust_fax : $this->_getParam('cust_fax');
        $this->view->cust_email    = ($zf_filter_input->isValid('cust_email'))? $zf_filter_input->cust_email : $this->_getParam('cust_email');
        $this->view->cust_website  = ($zf_filter_input->isValid('cust_website'))? $zf_filter_input->cust_website : $this->_getParam('cust_website');
        $this->view->country_code  = ($zf_filter_input->isValid('country_code'))? $zf_filter_input->country_code : $this->_getParam('country_code');
         $this->view->cust_limit_idr  = ($zf_filter_input->isValid('cust_limit_idr'))? $zf_filter_input->cust_limit_idr : $this->_getParam('cust_limit_idr');
         $this->view->cust_limit_usd  = ($zf_filter_input->isValid('cust_limit_usd'))? $zf_filter_input->cust_limit_usd : $this->_getParam('cust_limit_usd');

        //$this->view->cust_charges_status     = ($zf_filter_input->isValid('cust_charges_status'))? $zf_filter_input->cust_charges_status : $this->_getParam('cust_charges_status');
        //$this->view->cust_monthlyfee_status   = ($zf_filter_input->isValid('cust_monthlyfee_status'))? $zf_filter_input->cust_monthlyfee_status : $this->_getParam('cust_monthlyfee_status');
        //$this->view->cust_token_auth   = ($zf_filter_input->isValid('cust_token_auth'))? $zf_filter_input->cust_token_auth : $this->_getParam('cust_token_auth');
        
        $error = $zf_filter_input->getMessages();
        
        //format error utk ditampilkan di view html 
        $errorArray = null;
        foreach($error as $keyRoot => $rowError)
        {
           foreach($rowError as $errorString)
           {
              $errorArray[$keyRoot] = $errorString;
           }
        }
        
        if($flag_user_id == 'F')  $errorArray['cust_id'] = $this->language->_('Invalid Company Code format');
        
        if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['cust_email'] = $this->language->_('Invalid format');

        $this->view->customer_msg  = $errorArray;
        
		
	  }
    }//END if($this->_request->isPost())
	else if($this->_getParam('pdf') == 'PDF'){
		Application_Helper_General::writeLog('CSAD','Download PDF Create Customer User [BO]');
		$this->view->button = false;
		$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/index.phtml')."</td></tr>";
		$this->_helper->download->pdf(null,null,null,'Create Customer User',$outputHTML);
		/*$this->view->cust_charges_status  = '';
        $this->view->cust_monthlyfee_status = '';
        $this->view->cust_token_auth = '';*/
        
        $this->view->country_code = '';
	}
    else
    {
        /*$this->view->cust_charges_status  = 0;
        $this->view->cust_monthlyfee_status   = 0;
        $this->view->cust_token_auth   = 0;*/
        $this->view->country_code         = 'ID';
    }
		$this->view->button = true;
        
	
   
    $this->view->modulename      =  $this->_request->getModuleName();
    $this->view->customer_type   =  $this->_custType;
    $this->view->approve_type    =  $this->_masterhasStatus;
    $this->view->lengthCustId    =  $this->_custIdLength;
   


	   
  }
  
   
}


