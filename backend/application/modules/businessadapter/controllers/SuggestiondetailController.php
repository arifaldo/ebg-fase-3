<?php

require_once 'Zend/Controller/Action.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class businessadapter_SuggestiondetailController extends bintemplate_Model_Bintemplate
{
	protected $_moduleDB = 'CST';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('popup');

		$params = $this->_request->getParams();
		$keyValue = null;
		$excludeChangeId  = 'CHANGES_STATUS IN (' . $this->_db->quote($this->_changeStatus['code']['waitingApproval']) . ',' . $this->_db->quote($this->_changeStatus['code']['repairRequested']) . ')';
		$excludeChangeId .= ' AND ' . $this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

		if (is_array($params) && count($params)) $fullDesc = SGO_Helper_GeneralFunction::displayFullDesc($params);
		else $fullDesc = null;

		$this->view->suggestionType = $this->_suggestType;



		//cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
		$changes_id = strip_tags(trim($this->_getParam('changes_id')));

		$select = $this->_db->select()
			->from(array('A' => 'T_GLOBAL_CHANGES'), array('*'))
			->where("A.CHANGES_ID = ?", $changes_id)
			->where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		// echo $select;die;
		$global_changes = $this->_db->fetchRow($select);

		if (empty($global_changes))  $this->_redirect('/notification/invalid/index');
		//END cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid

		
		if (array_key_exists('changes_id', $params)) {
			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
			$validators = array(
				'changes_id' => array(
					'NotEmpty',
					'Digits',
					// array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
					array('Db_RecordExists', array('table' => 'TEMP_ADAPTER_PROFILE', 'field' => 'CHANGES_ID')),
					'messages' => array(
						'Can not be empty',
						'Invalid format',
						// 'Change id is not already exists in Master Global Changes',
						'Change id is not already exists in Temp Global Changes',
					),
				),
			);


			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
			
			if ($zf_filter_input->isValid()) {
				
				$this->view->status_type = $this->_masterglobalstatus;

				$binTypeArr = array(
		            '1' => 'eCollection',
		            '2' => 'VA Debit'
	            );

	           $optStatus = array(
	            '1' => $this->language->_('No'),
	            '0' => $this->language->_('Yes')
	           );

	           $optH2H = array(
	 	
	            '0' => $this->language->_('No'),
	            '1' => $this->language->_('Yes')
	           );

	           $optSkip = array(
	            '0' => $this->language->_('Yes'),
	            '1' => $this->language->_('No')
	           );

	          //------------------------ecollection opt--------------------------------
	           $optVA = array(
	            '0' => $this->language->_('Dynamic Close'),
	            '1' => $this->language->_('Static Open'),
	            '2' => $this->language->_('Static Close')
	           );

	           $optExpTime = array(
	            '0' => $this->language->_('Input Later'),
	            '1' => $this->language->_('Predefined')
	           );

	           $optExTimeType = array(
	            '1' => $this->language->_('Hour(s)'),
	            '2' => $this->language->_('Day(s)')
	           );

	           $optPayment = array(
	            '0' => $this->language->_('Yes'),
	            '1' => $this->language->_('No'),
	            '2' => $this->language->_('Over'),
	           );

	           $optBill = array(
	            '0' => $this->language->_('Total'),
	            '3' => $this->language->_('Selected'),
	            '1' => $this->language->_('FIFO'),
	            '2' => $this->language->_('LIFO')
	           );

	          //----------------------Va Debit opt-------------------------------

	           $optRemainingCredit = array(
	            '0' => $this->language->_('Accumulate'),
	            '1' => $this->language->_('Auto Reset'),
	           );

	           $dayArr = array(
	            '0' => 'Monday',
	            '1' => 'Tuesday',
	            '2' => 'Wednesday',
	            '3' => 'Thursday',
	            '4' => 'Friday',
	            '5' => 'Saturday',
	            '6' => 'Sunday',
	           );

	           $monthArr = array(
	            '1' => 'January',
	            '2' => 'February',
	            '3' => 'March',
	            '4' => 'April',
	            '5' => 'May',
	            '6' => 'June',
	            '7' => 'July',
	            '8' => 'August',
	            '9' => 'September',
	            '10' => 'October',
	            '11' => 'November',
	            '12' => 'December'
	           );

				$arrStatus = array(
                    '1' => 'Active', 
                    '2' => 'Suspended',
                    '3' => 'Deleted'
			);
				$this->view->statusArr = $arrStatus;
				
				$changeId  = $zf_filter_input->changes_id;
				
				$selectData = $this->_db->select()
                     ->from(array('H' => 'TEMP_ADAPTER_PROFILE'), '*')
                     ->join(array('D' => 'TEMP_ADAPTER_PROFILE_DETAIL'), 'H.PROFILE_ID = D.PROFILE_ID', '*')
                     ->where('H.CHANGES_ID = '.$this->_db->quote($changeId));
				//echo $selectData;
				$dataAdapter = $this->_db->fetchAll($selectData);
				$this->view->adapter = $dataAdapter;
				
				$xselectData = $this->_db->select()
                     ->from(array('H' => 'M_ADAPTER_PROFILE'), '*')
                     ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'H.PROFILE_ID = D.PROFILE_ID', '*')
                     ->where('H.PROFILE_ID = '.$this->_db->quote($dataAdapter[0]['PROFILE_ID']));

				$xdataAdapter = $this->_db->fetchAll($xselectData);
				$this->view->xadapter = $xdataAdapter;
				//echo '<pre>';
				//var_dump($xdataAdapter);die;
				//$this->view->datatype = $dataAdapter[0]['']
				$fileName = $dataAdapter[0]['FILE_PATH'];
				$fixLength = $dataAdapter[0]['FIXLENGTH'];
				$fixLengthType = $dataAdapter[0]['FIXLENGTH_TYPE'];
				$fixLengthHeader = $dataAdapter[0]['FIXLENGTH_HEADER_ORDER'];
				$fixLengthHeaderName = $dataAdapter[0]['FIXLENGTH_HEADER_NAME'];
				$fixLengthContent = $dataAdapter[0]['FIXLENGTH_CONTENT_ORDER'];
				$delimitedWith = $dataAdapter[0]['DELIMITED_WITH'];

				$file_contents = file_get_contents($fileName);
				
				$delete     = $this->_getParam('delete');
				$params = $this->_request->getParams();
				
				$select = $this->_db->select()
											->from(array('H' => 'M_CUST_ADAPTER_PROFILE'),array('*'));

				$select->where('H.ADAPTER_PROFILE_ID = '. $this->_db->quote($profileid));
    
				$profileCust = $this->_db->fetchAll($select);
				$this->view->custlist = $profileCust;
				
				//$resultdata = $this->getTempBinTemplate($changeId);
				
				 //var_dump($fileName);die;
				
				
				
				if (strpos($fileName, '.csv') !== false) {
            $extensionName = 'csv';
        }
        else if (strpos($fileName, '.json') !== false) {
            $extensionName = 'json';
        }
        else if (strpos($fileName, '.xml') !== false) {
            $extensionName = 'xml';
        }
        else if (strpos($fileName, '.txt') !== false) {
            $extensionName = 'txt';
        }
        else if (strpos($fileName, '.xls') !== false) {
            $extensionName = 'xls';
        }
        else if (strpos($fileName, '.xlsx') !== false) {
            $extensionName = 'xlsx';
        }
        
        //if csv occured
        if ($extensionName === 'csv') {

            if (!empty($delimitedWith)) {
                $data = $this->_helper->parser->parseCSV($fileName, $delimitedWith);
            }
            //if fix length
            else if ($fixLength == 1) {

                $data = file($fileName); 
            }
            else{
                $data = $this->_helper->parser->parseCSV($fileName);
            }
        }
        //if txt occured
        else if ($extensionName === 'txt') {

           $lines = file($fileName);

           $checkMt940 = false;
           $checkMt101 = false;
           foreach ($lines as $line) {
               if (strpos($line, '{1:') !== false) {
                   $checkMt940 = true;
               }
               else if (strpos($line, ':20:') !== false) {
                   $checkMt101 = true;
               }
           }

           //if mt940 format
           if ($checkMt940) {
               
                $data = $this->_helper->parser->mt940($fileName);
                $mtFile = true;
           }
           else if($checkMt101){
                $data = $this->_helper->parser->mt101($fileName);
                $mtFile = true;
           }
           else{

                if (!empty($delimitedWith)) {
                    //parse csv jg bisa utk txt
                    $data = $this->_helper->parser->parseCSV($fileName, $delimitedWith);
                }
                //if fix length
                else if ($fixLength == 1) {

                    $data = file($fileName);                   
                }
                else{
                    //parse csv jg bisa utk txt
                    $data = $this->_helper->parser->parseCSV($fileName, ',');
                }
           }
        }
        //if json occured
        else if ($extensionName === 'json') {

            $datajson = json_decode($file_contents, 1);
            $i = 0;
            foreach ($datajson as $key => $value) {
                if ($i == 0) {
                    $data[$i] = array_keys($value);
                    $data[$i + 1] = array_values($value);
                }
                else{
                    $data[$i+1] = array_values($value);
                }

                $i++;
            }
        }
        //if xml occured
        else if($extensionName === 'xml'){

            $xml = (array) simplexml_load_string($file_contents);

             $i = 0;
            foreach ($xml as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if ($i == 0) {
                        $data[$i] = array_keys((array)$value2);
                        $data[$i + 1] = array_values((array)$value2);
                    }
                    else{
                        $data[$i+1] = array_values((array)$value2);
                    }

                    $i++;
                }
            }

            for($i=0; $i<count($data); $i++){
                if ($i > 0) {
                    foreach ($data[$i] as $key => $value) {
                        if (empty($value)) {
                            $data[$i][$key] = null;
                        }
                    }
                }
            }
        }
        else if($extensionName === 'xls' || $extensionName === 'xlsx'){
            try {
                $inputFileType = IOFactory::identify($fileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($fileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($fileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 1; $row <= $highestRow; $row++){                        
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

                $data[$row - 1] = $rowData[0];
            }
        }

        if(!empty($data))
        {    

            if ($fixLength == 1) {   

                $fixLengthHeaderNameArr = explode(',', $fixLengthHeaderName);

                if (count($fixLengthHeaderNameArr) != 0) {

                    foreach ($fixLengthHeaderNameArr as $key => $value) {
                        //first order, startIndex from 0
                       $newdata[0][] = $value;
                    }
                }
                else{
                    $newdata[0][] = $fixLengthHeaderName;
                }

                $startArrIndex = 1;

                 // if with header
                if ($fixLengthType == 1) {
                    $surplusIndex = 0;
                }
                else if ($fixLengthType == 3) {
                    $surplusIndex = 1;
                }

                $contentOrderArr = explode(',', $fixLengthContent);

                if (count($contentOrderArr) > 1) {
                    foreach ($data as $key => $value) {
                        if ($key >= $startArrIndex) {
                            foreach ($contentOrderArr as $key2 => $value2) {
                                //first order, startIndex from 0
                                if ($key2 == 0) {
                                    $startIndex = 0;
                                    $endIndex = (int) $value2 + 1;

                                    $newdata[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                                else{
                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                    $endIndex = (int) ($value2 - ($startIndex - 1));

                                    $newdata[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                            }       
                        }   
                    }
                }

                $this->view->headerData = $newdata[0];
                unset($newdata[0]);
                $this->view->valueData  = $newdata;
            }
            else{
 
                //---------get rid of empty spaces---------
                $emptySpaces = array();
                foreach ($data[0] as $key => $value) {
                    if (empty($value)) {
                        array_push($emptySpaces, $key);
                        unset($data[0][$key]);
                    }
                }
                foreach ($data as $key => $value) {
                    foreach ($emptySpaces as $spaces) {
                        unset($data[$key][$spaces]);
                    }
                }

                $this->view->headerData = $data[0];

                unset($data[0]);
                $this->view->valueData  = $data;
            }

            $this->view->dataAdapter = $dataAdapter;
        }
        else //kalo total record = 0
        {
            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
            $this->view->error      = true;
            $this->view->report_msg = $this->displayError($error_msg);
        }
				
				
				
				
				//var_dump($global_changes);
				//suggest data
				$this->view->changes_id     = $changes_id;
				$this->view->changes_type   = $global_changes['CHANGES_TYPE'];
				$this->view->changes_status = $global_changes['CHANGES_STATUS'];
				$this->view->read_status    = $global_changes['READ_STATUS'];
				$this->view->created        = $global_changes['CREATED'];
				$this->view->created_by     = $global_changes['CREATED_BY'];


				//temp bin template
				$this->view->temp_name      = $resultdata['TEMP_NAME'];
				$this->view->temp_desc      = $resultdata['TEMP_DESC'];

				$this->view->cust_created     = $resultdata['TEMP_CREATED'];
				$this->view->cust_createdby   = $resultdata['TEMP_CREATEDBY'];
				$this->view->cust_suggested   = $resultdata['TEMP_SUGGESTED'];
				$this->view->cust_suggestedby = $resultdata['TEMP_SUGGESTEDBY'];
				$this->view->cust_updated     = $resultdata['TEMP_UPDATED'];
				$this->view->cust_updatedby   = $resultdata['TEMP_UPDATEDBY'];
		 
		          $this->view->bintype        = $resultdata['BIN_TYPE'] != '' ? $binTypeArr[$resultdata['BIN_TYPE']] : '-';
		          $this->view->allowskip        = $resultdata['SKIP_ERROR'] != '' ? $optSkip[$resultdata['SKIP_ERROR']] : '-';

		          //ecollection
		          $this->view->va_type   = $resultdata['VA_TYPE'] != '' ? $optVA[$resultdata['VA_TYPE']] : '-';
		          $this->view->exp_time   = $resultdata['EXP_TIME'] != '' ? $optExpTime[$resultdata['EXP_TIME']] : '-';

		          if ($resultdata['EXP_TIME'] == '1') {
		            $this->view->exp_time .= ' ('.$resultdata['EX_TIME'].' '.$optExTimeType[$resultdata['EX_TIME_TYPE']].')';
		          }

		          $this->view->partial_type   = $resultdata['PARTIAL_TYPE'] != '' ? $optPayment[$resultdata['PARTIAL_TYPE']] : '-';
		          $this->view->bill_method   =  $resultdata['BILLING_TYPE'] != '' ? $optBill[$resultdata['BILLING_TYPE']] : '-';
		          $this->view->vp_max   = $resultdata['VP_MAX'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['VP_MAX']) : '-';
		          $this->view->vp_min   = $resultdata['VP_MIN'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['VP_MIN']) : '-';
		          $this->view->seller_fee   = $resultdata['SELLER_FEE'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['SELLER_FEE']) : '-';
		          $this->view->buyer_fee   = $resultdata['BUYER_FEE'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['BUYER_FEE']) : '-';

		          //Va Debit
		          $this->view->va_credit_max   = $resultdata['VA_CREDIT_MAX'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['VA_CREDIT_MAX']) : '-';
		          $this->view->va_daily_limit   = $resultdata['VA_DAILY_LIMIT'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['VA_DAILY_LIMIT']) : '-';
		          $this->view->remainingCredit   = $resultdata['REMAINING_AVAILABLE_CREDIT'] != '' ? $optRemainingCredit[$resultdata['REMAINING_AVAILABLE_CREDIT']] : '-';

		          if ($resultdata['BIN_TYPE'] == '2' && ($resultdata['HOSTTOHOST'] == '0' || $resultdata['HOSTTOHOST'] == '')) {
		            if ($resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
		              if ($resultdata['EX_TIME_TYPE'] == '2') {
		                $exTime = 'Weekly every '. $dayArr[$resultdata['EX_TIME']];
		              }
		              else if ($resultdata['EX_TIME_TYPE'] == '3') {
		                $exTime = 'Monthly on '. $resultdata['EX_TIME'];
		              }
		              else if ($resultdata['EX_TIME_TYPE'] == '4') {
		                $explodeTime = explode('_', $resultdata['EX_TIME']);

		                $exTime = 'Yearly every '. $explodeTime[0].' '.$monthArr[$explodeTime[1]];
		              }

		              if ($resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
		                $this->view->remainingCredit .= ' ('.$exTime.')';
		              }
		            }
		          }

				//get current bin template
				$m_resultdata = $this->getBinTemplate($resultdata['TEMP_CODE']);
				// var_dump($m_resultdata);die;

				$this->view->m_temp_name      = $m_resultdata['TEMP_NAME'];
				$this->view->m_temp_desc      = $m_resultdata['TEMP_DESC'];

				$this->view->m_bintype        = $m_resultdata['BIN_TYPE'] != '' ? $binTypeArr[$m_resultdata['BIN_TYPE']] : '-';
		          $this->view->m_allowskip        = $m_resultdata['SKIP_ERROR'] != '' ? $optSkip[$m_resultdata['SKIP_ERROR']] : '-';

		          //ecollection
		          $this->view->m_va_type   = $m_resultdata['VA_TYPE'] != '' ? $optVA[$m_resultdata['VA_TYPE']] : '-';
		          $this->view->m_exp_time   = $m_resultdata['EXP_TIME'] != '' ? $optExpTime[$m_resultdata['EXP_TIME']] : '-';

		          if ($m_resultdata['EXP_TIME'] == '1') {
		            $this->view->m_exp_time .= ' ('.$m_resultdata['EX_TIME'].' '.$optExTimeType[$m_resultdata['EX_TIME_TYPE']].')';
		          }

		          $this->view->m_partial_type   = $m_resultdata['PARTIAL_TYPE'] != '' ? $optPayment[$m_resultdata['PARTIAL_TYPE']] : '-';
		          $this->view->m_bill_method   =  $m_resultdata['BILLING_TYPE'] != '' ? $optBill[$m_resultdata['BILLING_TYPE']] : '-';
		          $this->view->m_vp_max   = $m_resultdata['VP_MAX'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['VP_MAX']) : '-';
		          $this->view->m_vp_min   = $m_resultdata['VP_MIN'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['VP_MIN']) : '-';
		          $this->view->m_seller_fee   = $m_resultdata['SELLER_FEE'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['SELLER_FEE']) : '-';
		          $this->view->m_buyer_fee   = $m_resultdata['BUYER_FEE'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['BUYER_FEE']) : '-';

		          //Va Debit
		          $this->view->m_va_credit_max   = $m_resultdata['VA_CREDIT_MAX'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['VA_CREDIT_MAX']) : '-';
		          $this->view->m_va_daily_limit   = $m_resultdata['VA_DAILY_LIMIT'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['VA_DAILY_LIMIT']) : '-';
		          $this->view->m_remainingCredit   = $m_resultdata['REMAINING_AVAILABLE_CREDIT'] != '' ? $optRemainingCredit[$m_resultdata['REMAINING_AVAILABLE_CREDIT']] : '-';

		          if ($m_resultdata['BIN_TYPE'] == '2' && ($m_resultdata['HOSTTOHOST'] == '0' || $m_resultdata['HOSTTOHOST'] == '')) {
		            if ($m_resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
		              if ($m_resultdata['EX_TIME_TYPE'] == '2') {
		                $exTime = 'Weekly every '. $dayArr[$m_resultdata['EX_TIME']];
		              }
		              else if ($m_resultdata['EX_TIME_TYPE'] == '3') {
		                $exTime = 'Monthly on '. $m_resultdata['EX_TIME'];
		              }
		              else if ($m_resultdata['EX_TIME_TYPE'] == '4') {
		                $explodeTime = explode('_', $m_resultdata['EX_TIME']);

		                $exTime = 'Yearly every '. $explodeTime[0].' '.$monthArr[$explodeTime[1]];
		              }

		              $this->view->m_remainingCredit .= ' ('.$exTime.')';
		            }
		          }

			} else {
				$errors = $zf_filter_input->getMessages();
				$errorRemark = SGO_Helper_GeneralFunction::displayErrorRemark($errors);
				Application_Helper_General::writeLog('BNLS', 'View BIN Template changes list');

				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
				$this->_redirect('/popuperror/index/index');
			}
		} else {
			$errorRemark = 'Changes Id not found';
			//Application_Helper_General::writeLog('CCCL','');

			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
			$this->_redirect('/popuperror/index/index');
		}
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('BNLS', 'View BIN Template changes list');
		}
	}
}
