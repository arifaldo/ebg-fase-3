<?php
class requestbook_Model_Requestbook
{
	protected $_db;
	
    // constructor
	public function __construct()
	{	
		$config = Zend_Registry::get('config');
		$this->_masterglobalstatus 	= $config['master']['globalstatus'];
		$this->_beneftype 			= $config['account']['beneftype'];
		$this->_paymentStatus 		= $config["payment"]["status"]; 
		$this->_paymentType 		= $config["payment"]["type"]; 
		$this->_transferType 		= $config["transfer"]["type"]; 
		$this->_transferStatus 		= $config["transfer"]["status"]; 
		$this->_historystatus 		= $config["history"]["status"]; 
		$this->_db 					= Zend_Db_Table::getDefaultAdapter();
	}
	
	public function generatePaymentID()
	{
		$currentDate = date("Ymd");
		// $seqNumber	 = $this->getPaymentCounter($forTransaction);
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(9));
		$checkDigit  = '';
		//$checkDigit  = strtoupper(substr(MD5($currentDate.$seqNumber),-3));
		// $checkDigit  = strtoupper(substr(preg_replace("/[^a-zA-Z0-9]/", "", uniqid()),-3));
		//$paymentID   = $currentDate.$seqNumber.$checkDigit;
		$paymentID   = "C".$currentDate.$seqNumber.$checkDigit;
			
		return $paymentID;
	}
	
	public function getDataAcct($ACCTSRC,$CUST_ID)
    {
		$select	= $this->_db->select()
						->from(	array('P' => 'M_CUSTOMER_ACCT'),array('*'))
						>where('ACCT_NO = ?', $ACCTSRC)
			       		->where('CUST_ID = ?', (string)$CUST_ID);
		//echo $select; die;
		return $this->_db->fetchAll($select);
    }
	
	public function add(array $param) {

		$priviBeneLinkage			= $param['_beneLinkage'];			// true/false		
		$PS_NUMBER = $this->generatePaymentID();
		$ADMIN_FEE = str_replace('.','', $param['ADMIN_FEE']);
		
		$paymentInsert = array(
					'PS_NUMBER' 					=> $PS_NUMBER,
					'CUST_ID' 						=> isset($param['CUST_ID_LOGIN']) 	? $param['CUST_ID_LOGIN'] 	: "",					
					'PS_REQUESTED'	 				=> new Zend_Db_Expr("now()"),
					'PS_SUBJECT' 					=> isset($param['SUBJECT']) 	? $param['SUBJECT'] 	: "",
					'SOURCE_ACCOUNT' 				=> isset($param['ACCTSRC']) 	? $param['ACCTSRC'] 	: "", 
					'SOURCE_ACCOUNT_NAME' 			=> isset($param['ACCTSRC_NAME']) 	? $param['ACCTSRC_NAME'] 	: "",
					'SOURCE_ACCOUNT_CCY' 			=> isset($param['ACCTSRC_CCY']) 	? $param['ACCTSRC_CCY'] 	: "",
					'PRODUCT_TYPE' 					=> isset($param['PRODUCT_TYPE']) 	? $param['PRODUCT_TYPE'] 	: "",
					'NO_OF_SHEETS' 					=> isset($param['NUMBEROF_SHEET']) 	? $param['NUMBEROF_SHEET'] 	: "",
					'PS_REQUESTBY' 					=> $param['USER_ID_LOGIN'],
					'ADMIN_FEE' 					=> isset($ADMIN_FEE) 	? $ADMIN_FEE 	: "",
					'SUGGEST_STATUS'				=> 'WA',					
		);

		//Zend_Debug::dump($paymentInsert); die;
		$result = $this->_db->insert('T_REQ_BOOK',$paymentInsert);

		//$LASTPS_NUMBER = $this->_db->lastInsertId();

		/*if($priviBeneLinkage === true)
		{
			$linkage = array(	'CUST_ID' 			=> $param['CUST_ID_LOGIN'],
								'USER_ID' 			=> $param['USER_ID_LOGIN'],
								'BENEFICIARY_ID' 	=> $ACBENEF_ID,
							 );

			$this->_db->insert('M_BENEFICIARY_USER', $linkage);
		}*/

		return $result;
	}

	public function update(array $param) {
		$data = array(
								'PS_APPROVEBY' => $param['USER_ID_LOGIN'],
								'SUGGEST_STATUS' => 'AP'		
		);
		$where = array('BENEFICIARY_ID = ?' => $param['BENEFICIARY_ID']);
		$this->_db->update('M_BENEFICIARY',$data,$where);

	}

	/**
	 *
	 * Approve Beneficiary
	 * @param int $beneficiaryId
	 */
	public function approve($param) {
		$data = array(
								'PS_APPROVEBY' => $param['USER_ID_LOGIN'],
								'SUGGEST_STATUS' => 'AP'		
		);
		$where = array('PS_NUMBER = ?' => $param['PS_NUMBER']);
		$result = $this->_db->update('T_REQ_BOOK',$data,$where);

		return $result;
	}
	
	public function reject($param) {
		$data = array(
								'PS_APPROVEBY' => $param['USER_ID_LOGIN'],
								'SUGGEST_STATUS' => 'RJ'		
		);
		$where = array('PS_NUMBER = ?' => $param['PS_NUMBER']);
		$result = $this->_db->update('T_REQ_BOOK',$data,$where);

		return $result;
	}
	
}