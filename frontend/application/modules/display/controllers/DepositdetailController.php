<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';
class display_DepositdetailController extends Application_Main
{
	public function indexAction(){

		$this->_helper->layout()->setLayout('newpopup');
		// $deposit_id = $this->_getParam('deposit_id'); 

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	     $password = $sessionNamespace->token; 
	     $this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($this->_getParam('deposit_id'));
		$deposit_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

		$selectDeposit = $this->_db->select()
			       ->from(array('D' => 'T_DEPOSIT'))
					        ->where('D.ID = ?', $deposit_id)
					        ->join(array('C' => 'M_CUSTOMER'), 'D.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'));

		$depositData = $this->_db->fetchRow($selectDeposit);

		$this->view->depositData = $depositData;


		$custInfo = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('C' => 'M_CUSTOMER'))
						->where("CUST_ID = ".$this->_db->quote($depositData['CUST_ID']))
				);
		$this->view->custname	= $custInfo['CUST_NAME'];
		$this->view->custadd	= $custInfo['CUST_ADDRESS'];
		$this->view->custcity	= $custInfo['CUST_CITY'];
		$this->view->custprov	= $custInfo['CUST_PROVINCE'];
		$this->view->custzip	= $custInfo['CUST_ZIP'];
		$this->view->custphone	= $custInfo['CUST_PHONE'];
		$this->view->custfax	= $custInfo['CUST_FAX'];
		$this->view->finance	= $custInfo['CUST_FINANCE'];
		$this->view->custemail	= $custInfo['CUST_EMAIL'];

	}
}