<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class cmdreport_EftreconciliationreportController extends Application_Main
{
	public function indexAction()
	{	
		// $arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		// foreach($arrPayType as $key => $value){
			// if($key != 3){
				// $optpaytypeRaw[$key] = $value;
			// }
		// }
		
		// 6,4 THEN 'Multi Credit'
		// 7,5 THEN 'Multi Debet'
		$paytypeall		= $optpaytypeAll = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		$this->view->paymenttype 		= $paytypeall;
		// foreach($paytypeall as $key => $value){
			
			// if($key != 3 && $key != 8 && $key != 9 && $key != 10){
			
				// if($key == 4 
				// OR 
				// $key == 6
				// ) {
					// $value = $paytypeall[4];
					// $optpaytypeAll[$key] = $value;
				// }
				// else if($key == 5 
				// OR $key == 7
				// ) {
					// $value = $paytypeall[5];
					// $optpaytypeAll[$key] = $value;
				// }
				// else $optpaytypeAll[$key] = $value;
			// }
			// $optpaytypeAll[$key] = $value;
		// }
		$transfertype 				= $this->_transfertype;
		$transfertypedesc 			= $transfertype['desc'];
		$transfertypecode 			= array_flip($transfertype['code']);
		$typearr 					= array_combine(array_values($this->_transfertype['code']),array_values($this->_transfertype['desc']));
		$this->view->transfertype 	= $transfertype;
		
		$transferstatus 			= $this->_transferstatus;
		$transferstatusdesc 		= $transferstatus['desc'];
		$transferstatuscode 		= array_flip($transferstatus['code']);
		$statusarr 					= array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
		$this->view->transferstatus = $transferstatus;
		
		$mstatusarr = array_combine(array_values($this->_eftstatus['code']),array_values($this->_eftstatus['desc']));
		unset($mstatusarr["2"]);
		unset($mstatusarr["9"]);
		$this->view->messtatus = $mstatusarr;
		
		$monthlist = array(
						'1'		=>	'January',
						'2' 	=> 	'February',
						'3'		=>	'March',
						'4'		=>	'April',
						'5'		=>	'May',
						'6'		=>	'June',
						'7'		=>	'July',
						'8'		=>	'August',
						'9'		=>	'September',
						'10'	=>	'October',
						'11'	=>	'November',
						'12'	=>	'December');
		$this->view->monthlist = $monthlist;
		
		$thisyear = date("Y");
		$yearlist = array(
						$thisyear-3	=> 	$thisyear-3,
						$thisyear-2	=> 	$thisyear-2,
						$thisyear-1	=> 	$thisyear-1,
						$thisyear	=> 	$thisyear);
		$this->view->yearlist = $yearlist;
		
		$datetype 	= $this->_getParam('datetype');
		$month 		= $this->_getParam('month');
		$year 		= $this->_getParam('year');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');
		
		$caseType = "(CASE B.PS_TYPE ";
  		foreach($optpaytypeAll as $key=>$val)
  		{
   			$caseType .= " WHEN ".$key." THEN '".$val."'";
  		}
  		$caseType .= " END)";
  			
		/* $caseTypeT = "(CASE T.TX_TYPE ";
  		foreach($paytypearr as $key=>$val)
  		{
   			$caseTypeT .= " WHEN ".$key." THEN '".$val."'";
  		}
  		$caseTypeT .= " END)"; */
  			
  		$caseType2 = "(CASE C.TRANSFER_TYPE ";
  		foreach($typearr as $key=>$val)
  		{
   			$caseType2 .= " WHEN ".$key." THEN '".$val."'";
  		}
  		$caseType2 .= " END)";
  			
  		/* $caseTypeT2 = "(CASE TD.TRANSFER_TYPE ";
  		foreach($typearr as $key=>$val)
  		{
   			$caseTypeT2 .= " WHEN ".$key." THEN '".$val."'";
  		}
  		$caseTypeT2 .= " END)"; */
  		
  		$caseStatus = "(CASE C.TRA_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  		$caseStatus .= " END)";
  		
  		/* $caseStatusT = "(CASE TD.TRA_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatusT .= " WHEN ".$key." THEN '".$val."'";
  		}
  		$caseStatusT .= " END)"; */
  		
  		$caseStatus2 = "(CASE C.EFT_STATUS ";
  		foreach($mstatusarr as $key=>$val)
  		{
   			$caseStatus2 .= " WHEN ".$key." THEN '".$val."'";
  		}
  		$caseStatus2 .= " END)";
		
  		/* $caseStatusT2 = "(CASE TD.EFT_STATUS ";
  		foreach($mstatusarr as $key=>$val)
  		{
   			$caseStatusT2 .= " WHEN ".$key." THEN '".$val."'";
  		}
  		$caseStatusT2 .= " END)"; */
		
		/*$select1 = $this->_db->select()->distinct()
							->from(array('T' => 'T_TX'),array())
							->join(array('TD' => 'T_TX_DETAIL'),
									'T.PS_NUMBER = TD.PS_NUMBER',
									array('PS_TYPE'=>$caseTypeT, 
											'TRANSFER_TYPE'=>$caseTypeT2, 
											'TRA_STATUS'=>$caseStatusT, 
											'EFT_STATUS'=>$caseStatusT2, 
											'SOURCE_ACCOUNT_CCY', 
											'TRANS_COUNT' => 'COUNT(*)', 
											'TRANS_SUM' => 'SUM(TD.TRA_AMOUNT)'
										)
									)
					        ->group(array('PS_TYPE'=>'T.TX_TYPE' ,
											'TRANSFER_TYPE'=>'TD.TRANSFER_TYPE',
											'TRA_STATUS'=>'TD.TRA_STATUS',
											'EFT_STATUS'=>'TD.EFT_STATUS',
											'SOURCE_ACCOUNT_CCY'=>'TD.SOURCE_ACCOUNT_CCY'
										)
									);
		$select1->where("T.TX_STATUS IN (5,6)");*/
		
		$select2 = $this->_db->select()->distinct()
					        ->from(array('B' => 'T_PSLIP'),array())
					        ->join(array('C' => 'T_TRANSACTION'), 
									'B.PS_NUMBER = C.PS_NUMBER',
									array('PS_TYPE'=>$caseType, 
											'TRANSFER_TYPE'=>$caseType2, 
											'TRA_STATUS'=>$caseStatus, 
//											'EFT_STATUS'=>$caseStatus2, 
											'SOURCE_ACCOUNT_CCY', 
											'TRANS_COUNT' => 'COUNT(*)', 
											'TRANS_SUM' => 'SUM(C.TRA_AMOUNT)'
										)
									)
					        ->group(array('PS_TYPE'=>'B.PS_TYPE' ,
											'TRANSFER_TYPE'=>'C.TRANSFER_TYPE',
											'TRA_STATUS'=>'C.TRA_STATUS',
//											'EFT_STATUS'=>'C.EFT_STATUS',
											'SOURCE_ACCOUNT_CCY'=>'C.SOURCE_ACCOUNT_CCY'
										)
									);
		$select2 -> where("B.PS_STATUS IN (5,6)");
					        
		$filterArr = array (
							'filter'		=> array('StripTags'),
							'transtype'		=> array('StripTags','StringTrim'),
						   	'transstatus' 	=> array('StripTags','StringTrim'),
							'mstatus'		=> array('StripTags','StringTrim'),
							'paytype' 		=> array('StripTags','StringTrim'),
							'datetype' 		=> array('StripTags','StringTrim'),
							'date' 			=> array('StripTags','StringTrim')
							);
		
		$validator = array (
							'filter'		=> array(),
							'transtype'		=> array(),
						   	'transstatus' 	=> array(),
							'mstatus'		=> array(),
							'paytype' 		=> array(),
							'datetype' 		=> array(),
							'date' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat))
							);
		
	    $zf_filter 		= new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter 		= $zf_filter->getEscaped('filter');
	    $transtype 		= html_entity_decode($zf_filter->getEscaped('transtype'));
	    $transstatus 	= html_entity_decode($zf_filter->getEscaped('transstatus'));
	    $mstatus 		= html_entity_decode($zf_filter->getEscaped('mstatus'));
	    $paytype 		= html_entity_decode($zf_filter->getEscaped('paytype'));
	    $date 			= html_entity_decode($zf_filter->getEscaped('date'));
	    $datetype 		= html_entity_decode($zf_filter->getEscaped('datetype'));

		/*
		if($filter == null || $filter == "Clear Filter")
		{	
			$date = (date("d/m/Y"));
			// Kata Cik Erica Noviana bukan Payment message tapi Transfer Message (Suhandi)
			// $this->view->paymsg = "Inhouse, Domestic, Multi Credit, Multi Debit, Disbursement, Settlement, Payment Allocation";
			//$result = $select2->query()->FetchAll();
		}
		*/
		//
		if($datetype == null || $filter == null || $filter == "Clear Filter" || empty($date))
		{
			$date = (date("d/m/Y"));
			$this->view->datemsg = Application_Helper_General::convertDate($date,"dd MMM yyyy",$this->view->defaultDateFormat);
			$this->view->date = $date;
			if(!empty($date))
			{
				$FormatDate = new Zend_Date($date, $this->_dateDisplayFormat);
				$date  = $FormatDate->toString($this->_dateDBFormat);	
			}
	
			if(!empty($date))
			$select2->where("DATE(B.PS_UPDATED) = ".$this->_db->quote($date));
			//$select1->where("CONVERT(DATE, T.UPDATED) = ".$this->_db->quote($date));
		}
		else if($datetype == 'date2' && $filter == TRUE)
		{
			$this->view->datemsg = $monthlist[$month].' '.$year;
			//Zend_Debug::dump($month);die;
			$this->view->month = $month;
			$this->view->year = $year;
			$select2->where("MONTH(B.PS_EFDATE) = ".$this->_db->quote($month));
			$select2->where("YEAR(B.PS_EFDATE) = ".$this->_db->quote($year));
			
			//$select1->where("MONTH(CONVERT(DATE, T.UPDATED)) = ".$this->_db->quote($month));
			//$select1->where("YEAR(CONVERT(DATE, T.UPDATED)) = ".$this->_db->quote($year));
		} 
		else if($datetype == 'date1' && $filter == TRUE)
		{		
			$date   = 	$zf_filter->getEscaped('date');
			$this->view->date = $date;
			$this->view->datemsg = Application_Helper_General::convertDate($date,"dd MMM yyyy",$this->view->defaultDateFormat);
			if(!empty($date))
			{
				$FormatDate = new Zend_Date($date, $this->_dateDisplayFormat);
				$date  = $FormatDate->toString($this->_dateDBFormat);	
			}
			$select2->where("DATE(B.PS_UPDATED) = ".$this->_db->quote($date));
			//$select1->where("CONVERT(DATE, T.UPDATED) = ".$this->_db->quote($date));
		}			
		//if($active) $select2->where("ACTION_DESC LIKE ".$active);
	    if($filter == TRUE)
	    {
	    	$this->view->datetype = $datetype;
	    	
	    	if($transtype!=='')
	    	{
	       		$this->view->transtype = $transtype;
	       		$select2->where("C.TRANSFER_TYPE = ".$this->_db->quote($transtype));
				//$select1->where("TD.TRANSFER_TYPE = ".$this->_db->quote($transtype));
	    	}
	    	
	    	if($paytype!=='')
	    	{
	    		$paymsg = $optpaytypeAll[$paytype];
	       		$this->view->paymsg = $paymsg;
	    		$this->view->paytype = $paytype;
	    		$select2->where("B.PS_TYPE = ".$this->_db->quote($paytype));
	    		//$select1->where("T.TX_TYPE = ".$this->_db->quote($paytype));
	    	}
	    	
	    	if($transstatus!=='')
	    	{
	       		$this->view->transstatus = $transstatus;
	       		$select2->where("C.TRA_STATUS = ".$this->_db->quote($transstatus));
	       		//$select1->where("TD.TRA_STATUS = ".$this->_db->quote($transstatus));
	    	}
	    	
//	    	if($mstatus!=='')
//	    	{
//	       		$this->view->mstatus = $mstatus;
//	       		$select2->where("C.EFT_STATUS = ".$this->_db->quote($mstatus));
//	       		//$select1->where("TD.EFT_STATUS = ".$this->_db->quote($mstatus));
//	    	}
		}

		//$select = $this->_db->select()->union(array($select1, $select2));
		// die($select2);
		$arr = $this->_db->fetchAll($select2);
		   
		$this->view->result = $arr;
		
		$no=0;
		$export = array();
		foreach($arr as $row)
        {
			$export[$no]['PS_TYPE'] 			= $row['PS_TYPE'];
			$export[$no]['TRANSFER_TYPE'] 		= $row['TRANSFER_TYPE'];
			$export[$no]['TRA_STATUS'] 			= $row['TRA_STATUS'];
//			$export[$no]['EFT_STATUS'] 			= $row['EFT_STATUS'];
			$export[$no]['SOURCE_ACCOUNT_CCY'] 	= $row['SOURCE_ACCOUNT_CCY'];
			$export[$no]['TRANS_COUNT'] 		= $row['TRANS_COUNT'];
			$export[$no]['TRANS_SUM'] 			= Application_Helper_General::displayMoney( $row['TRANS_SUM'] );
			$no++;
		}
		
		$templateExport = array('Payment Type','Transfer Type','Transaction Status','Currency','Number of Transactions','Value of Transactions');
		
		if($csv)
		{
			$this->_helper->download->csv(array($this->language->_('Payment Type'),$this->language->_('Transfer Type'),$this->language->_('Transaction Status'),$this->language->_('Currency'),$this->language->_('Number of Transaction'),$this->language->_('Value of Transaction')),$export,null,$this->language->_('Reconciliation Report'));
			Application_Helper_General::writeLog('SBRC',' Download CSV Reconciliation Report');
		}
		
		if($pdf)
		{
			$this->_helper->download->pdf(array($this->language->_('Payment Type'),$this->language->_('Transfer Type'),$this->language->_('Transaction Status'),$this->language->_('Currency'),$this->language->_('Number of Transaction'),$this->language->_('Value of Transaction')),$export,null,$this->language->_('Reconciliation Report'));
			Application_Helper_General::writeLog('SBRC',' Download PDF Reconciliation Report');
		}
		if(!$csv || !$pdf)
			Application_Helper_General::writeLog('SBRC','View Reconciliation Report');
		if(!empty($arr))
		{	
			$listTransMsg = array();
			// $result = $select2->query()->FetchAll();
			foreach($arr as $row)
			{
				$listTransMsg[$row['TRANSFER_TYPE']] = $row['TRANSFER_TYPE'];
			}
			$transMsg= implode(",", $listTransMsg);

			// $i = 0;
			// foreach($list as $listview)
			// {
				// if($i == 0)
				// {
					// $paymsg = $listview;
				// }
				// else
				// {
					// $paymsg .= ', '.$listview;
				// }
				// Zend_Debug::dump($i);
				// $i++;
			// }
			$this->view->transMsg = $transMsg;
		}//die;
		//Zend_Debug::dump($datetype);die;
		$this->view->filter = $filter;
	}
}
?>