<?php

require_once 'Zend/Controller/Action.php';

//NOTE:
//Watch the modulename, filename and classname carefully
class dropbox_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$arr = null;
		$viewFilter = null;

		$companyCode = $this->language->_('Company');
		$companyName = $this->language->_('Company Name');
		$fileName = $this->language->_('File');
		$fileDescription = $this->language->_('File Description');
		$uploadedBy = $this->language->_('Uploaded By');
		$uploadDateTime = $this->language->_('Uploaded Date and Time');

		$fields = array(
			'Company Code'  			=> array(
				'field' => 'CUST_ID',
				'label' => $companyCode,
				'sortable' => true
			),
			// 'Company Name'  			=> array	(
			// 										'field' => 'MC.CUST_NAME',
			// 										'label' => $companyName,
			// 										'sortable' => true
			// 									),
			'FileName'  			=> array(
				'field' => 'FILE_NAME',
				'label' => $fileName,
				'sortable' => true
			),
			// 'FileDescription'  					=> array	(
			// 											'field' => 'FILE_DESCRIPTION',
			// 											'label' => $fileDescription,
			// 											'sortable' => true
			// 										),
			'Uploaded By'  					=> array(
				'field' => 'FILE_UPLOADEDBY',
				'label' => $uploadedBy,
				'sortable' => true
			),
			'Upload Date and Time'  					=> array(
				'field' => 'FILE_UPLOADED_TIME',
				'label' => $uploadDateTime,
				'sortable' => true
			)
		);
		$this->view->fields = $fields;

		$filterArr = array(
			'filter' 	  	=> array('StringTrim', 'StripTags'),
			'NAME' 	  	=> array('StringTrim', 'StripTags'),
			'CODE'   	=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'UPDATED_BY' 	  	=> array('StringTrim', 'StripTags'),
			'QUEST_DATE' 	  	=> array('StringTrim', 'StripTags'),
			'QUEST_DATE_END' 	  	=> array('StringTrim', 'StripTags'),
		);


		$filterlist = array("CODE" => "CODE", "NAME" => "NAME", "UPLOAD BY" => "UPLOADED_BY", "Upload Date" => "QUEST_DATE");
		$this->view->filterlist = $filterlist;

		$dataParam = array("CODE", "NAME", "UPLOADED_BY");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}
		}
		if (!empty($this->_request->getParam('questdate'))) {
			$createarr = $this->_request->getParam('questdate');
			$dataParamValue['QUEST_DATE'] = $createarr[0];
			$dataParamValue['QUEST_DATE_END'] = $createarr[1];
		}

		$zf_filter 	= new Zend_Filter_Input($filterArr, array(), $dataParamValue);
		$filter 		= $this->_getParam('filter');
		$delete		 	= $this->_getParam('delete');

		if ($delete) {
			$postreq_id	= $this->_request->getParam('req_id');
			if ($postreq_id) {

				foreach ($postreq_id as $key => $value) {
					if ($postreq_id[$key] == 0) {
						unset($postreq_id[$key]);
					}
				}
			}

			if ($postreq_id == null) $params['req_id'] = null;
			else $params['req_id'] = 1;

			$validators = array(
				'req_id' => array(
					'NotEmpty',
					'messages' => array(
						'Error File ID Submitted',
					)
				),
			);

			$filtersVal = array('req_id' => array('StringTrim', 'StripTags'));

			$zf_filter_input = new Zend_Filter_Input($filtersVal, $validators, $params, $this->_optionsValidator);
			$success = false;
			foreach ($postreq_id as $key => $value) {
				if (!empty($value)) {
					$success = true;
				}
			}

			if ($zf_filter_input->isValid() && $success) {
				try {
					foreach ($postreq_id as  $key => $value) {
						$FILE_ID_DELETE =  $postreq_id[$key];

						$this->_db->beginTransaction();

						$param = array();
						$param['FILE_DELETED'] = '1';
						$param['FILE_DELETEDBY'] = $this->_userIdLogin;

						$where = array('FILE_ID = ?' => $FILE_ID_DELETE);
						$query = $this->_db->update("T_FILE_SUBMIT", $param, $where);
						Application_Helper_General::writeLog('DSUD', 'Delete File Sharing');
						$this->_db->commit();
					}
				} catch (Exception $e) {
					$this->_db->rollBack();
				}
				$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
				$this->_redirect('/notification/success/index');
			} else {
				$error 			= true;
				$errors 		= $zf_filter_input->getMessages();
				$req_idErr 		= (isset($errors['req_id'])) ? $errors['req_id'] : null;
			}
		}

		if (isset($error)) {
			$this->view->error 			= $errors;
			$this->view->req_idErr 		= $req_idErr;
			$filter = true;
		}

		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$select =	$this->_db->select()
			->from(
				array('TFS' => 'T_FILE_SUBMIT'),
				array(
					'FILE_ID' => 'TFS.FILE_ID',
					'FILE_NAME' => 'TFS.FILE_NAME',
					'FILE_DESCRIPTION' => 'TFS.FILE_DESCRIPTION',
					'FILE_UPLOADED_TIME' => 'TFS.FILE_UPLOADED_TIME',
					'FILE_SYSNAME' => 'TFS.FILE_SYSNAME',
					'FILE_UPLOADEDBY' => 'TFS.FILE_UPLOADEDBY',
					'CUST_NAME' => 'MC.CUST_NAME',
					'FILE_DOWNLOADED' => 'TFS.FILE_DOWNLOADED',
				)
			)
			->where("FILE_DELETED != 1")
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'TFS.CUST_ID = MC.CUST_ID'
			);

		$FILE_ID   	= $this->_getParam('FILE_ID');
		if ($FILE_ID && $this->view->hasPrivilege('DSDL')) {
			$select->where('FILE_ID =?', $FILE_ID);
			$data = $this->_db->fetchRow($select);
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['FILE_NAME'], $attahmentDestination . $data['FILE_SYSNAME']);
			Application_Helper_General::writeLog('DSDL', 'Download File Sharing');
			$updateArr = array();
			$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED'] + 1;
			$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;

			$whereArr = array('FILE_ID = ?' => $FILE_ID);

			$fileupdate = $this->_db->update('T_FILE_SUBMIT', $updateArr, $whereArr);
		}

		if ($filter == TRUE) {
			$SEARCH_TEXT   	= $zf_filter->getEscaped('SEARCH_TEXT');
			$CODE   		= $zf_filter->getEscaped('CODE');
			$UPLOADED_BY    = $zf_filter->getEscaped('UPLOADED_BY');
			$DATE_START    	= $zf_filter->getEscaped('QUEST_DATE');
			$DATE_END		= $zf_filter->getEscaped('QUEST_DATE_END');
			$NAME			= $zf_filter->getEscaped('NAME');
			$this->view->DATE_START 	= $DATE_START;
			$this->view->DATE_END 		= $DATE_END;
			$this->view->SEARCH_TEXT 	= $SEARCH_TEXT;
			$this->view->CODE 			= $CODE;
			$this->view->NAME 			= $NAME;
			$this->view->UPLOADED_BY 	= $UPLOADED_BY;


			if ($SEARCH_TEXT) {
				$select->where("UPPER(FILE_NAME) LIKE " . $this->_db->quote('%' . $SEARCH_TEXT . '%'));
			}

			if ($CODE) {
				$select->where("UPPER(TFS.CUST_ID) LIKE " . $this->_db->quote('%' . $CODE . '%'));
			}

			if ($NAME) {
				$select->where("UPPER(FILE_NAME) LIKE " . $this->_db->quote('%' . $NAME . '%'));
			}

			if ($UPLOADED_BY) {
				$select->where("UPPER(FILE_UPLOADEDBY) like " . $this->_db->quote('%' . $UPLOADED_BY . '%'));
			}

			if ($DATE_START) {
				$FormatDate 	= new Zend_Date($DATE_START, $this->_dateDisplayFormat);
				$DATE_START  	= $FormatDate->toString($this->_dateDBFormat);
				$select->where("DATE(FILE_UPLOADED_TIME) >= DATE(" . $this->_db->quote($DATE_START) . ")");
			}

			if ($DATE_END) {
				$FormatDate 	= new Zend_Date($DATE_END, $this->_dateDisplayFormat);
				$DATE_END  	= $FormatDate->toString($this->_dateDBFormat);
				$select->where("DATE(FILE_UPLOADED_TIME) <= DATE(" . $this->_db->quote($DATE_END) . ")");
			}
		}
		if (!$FILE_ID && $this->view->hasPrivilege('DSDL')) {
			Application_Helper_General::writeLog('DSLS', 'View File Sharing List');
		}
		$select->where("FILE_TYPE = ? ", '1');
		$select->order($sortBy . ' ' . $sortDir);
		$arr = $this->_db->fetchAll($select);
		$this->paging($arr);
		unset($dataParamValue['QUEST_DATE_END']);
		$size = file($data['FILE_NAME'], $attahmentDestination . $data['FILE_SYSNAME']);
		$this->view->size = $size;
		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
	}
}
