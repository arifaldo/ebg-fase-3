<?php

require_once 'Zend/Controller/Action.php';

class stock_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');    
	    $this->setbackURL('/stock/index');    

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$this->view->report_msg = array();

    	
	    
		if($this->_request->isPost() && $this->_getParam('submit'))
		{

			$filters = array(
			                 'stock_code' => array('StringTrim','StripTags','StringToUpper'),
							 'stock_name' => array('StringTrim','StripTags','StringToUpper')
							);

			$validators = array(
			                    'stock_code' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>10)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 12 chars)'),
			                                                             )
													),
								'stock_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 105 chars)'),
																         )
														)
							   );
		
							   // print_r($this->_request->getParams());die;
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				// die;
				//$countrycode = substr($zf_filter_input->bank_code,4,2);
				$content = array(
								'STOCK_CODE' 	 => $zf_filter_input->stock_code,
								'STOCK_NAME' 	 => $zf_filter_input->stock_name,
								'CUST_ID'		=>	$this->_custIdLogin

						       );
						   	// print_r($content);die;    
				try 
				{
					
					//-------- insert --------------
					$this->_db->beginTransaction();
					
					$this->_db->insert('M_STOCK',$content);
					
					$this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('RBAD','Add Stock. Stock Code : ['.$zf_filter_input->stock_code.'], Stock Name : ['.$zf_filter_input->stock_name.']');
					$this->view->success = true;
					$this->view->report_msg = array();
					
					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e) 
				{
					// print_r($e);die;
					//rollback changes
					$this->_db->rollBack();
					
					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				// die;
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();
				// print_r($error);die;
				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
		
		Application_Helper_General::writeLog('RBAD','Add Stock List');
	}

}