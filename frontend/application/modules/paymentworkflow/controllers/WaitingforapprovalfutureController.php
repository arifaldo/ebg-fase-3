<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class paymentworkflow_WaitingforapprovalfutureController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}	
    	
    	$payType = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
    	$trfType = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		
		foreach($payType as $key => $value){  $filterPayType[$key] = $this->language->_($value); }
		foreach($trfType as $key => $value){ if($key != 3 && $key != 4) $filterTrfType[$key] = $this->language->_($value); }
		
		
		$fields = array('payReff'  		=> array('field' 	=> 'payReff',
												 'label' 	=> $this->language->_('Payment Ref').'#',
												 'sortable' => true),
						'paySubj'  		=> array('field' 	=> 'paySubj',
												 'label' 	=> $this->language->_('Subject'),
												 'sortable' => true),
						'created'  		=> array('field' 	=> 'created',
												 'label' 	=> $this->language->_('Created Date'),
												 'sortable' => true),	
						'efdate'  		=> array('field' 	=> 'efdate',
												 'label' 	=> $this->language->_('Payment Date'),
												 'sortable' => true),
						'accsrc'  		=> array('field' 	=> 'accsrc',
												 'label' 	=> $this->language->_('Source Account'),
												 'sortable' => true),
						'accsrc_name'  	=> array('field' 	=> 'accsrc_name',
												 'label' 	=> $this->language->_('Source Account Name'),
												 'sortable' => true),
						'acbenef'  		=> array('field' 	=> 'acbenef',
												 'label' 	=> $this->language->_('Beneficiary Account'),
												 'sortable' => true),
						'acbenef_name'  => array('field' 	=> 'acbenef_name',
												 'label'	=> $this->language->_('Beneficiary Name'),
												 'sortable' => true),
						'numtrx'  		=> array('field' 	=> 'numtrx',
												 'label' 	=> '#'.$this->language->_('Trans'),
												 'sortable' => true),
						'ccy'  			=> array('field' 	=> 'ccy',
												 'label' 	=> $this->language->_('CCY'),
												 'sortable' => true),
						'amount'  		=> array('field' 	=> 'amount',
												 'label' 	=> $this->language->_('Amount'),
												 'sortable' => true),
						'payType'  		=> array('field'	=> 'payType',
												 'label' 	=> $this->language->_('Payment Type'),
												 'sortable' => true),
		);

		$filterlist = array('Created Date','Payment Date','Payment Ref#','Payment Type','Transfer Type');
		
		$this->view->filterlist = $filterlist;

	

		
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','created');
		$sortDir = $this->_getParam('sortdir','desc');
//		echo "<pre>";
//		print_r($this->_request->getParams());
//		echo $sortBy."<br>";
//		echo $sortDir."<br>";
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';				
						
		$filterArr = array(	'payReff'  		=> array('StringTrim','StripTags','StringToUpper'),
							'payType'  		=> array('StringTrim','StripTags'),
							'trfType'  		=> array('StringTrim','StripTags'),
							'createdFrom'   => array('StringTrim','StripTags'),
							'createdTo'     => array('StringTrim','StripTags'),
							'payDateFrom'   => array('StringTrim','StripTags'),
							'payDateTo'     => array('StringTrim','StripTags'),
		);
		
		// if POST value not null, get post, else get param
		$dataParam = array("payReff", "payType", "trfType", "createdFrom", "createdTo", "payDateFrom", "payDateTo");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
		
		// The default is set so all fields allow an empty string
		$options = array('allowEmpty' => true);
		$validators = array(
						'payReff'		=> array(),
        				'payType' 		=> array(array('InArray', array('haystack' => array_keys($filterPayType)))),
        				'trfType' 		=> array(array('InArray', array('haystack' => array_keys($filterTrfType)))),
						'createdFrom'   	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'createdTo'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'payDateFrom'   	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'payDateTo'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);
		$dataParam = array('Created Date','Payment Date','Payment Ref#','Payment Type','Transfer Type');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
											 
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
	
		foreach ($this->_request->getParams() as $key => $value)
		{
			//print_r($key);
			if($key == 'module' || $key == 'controller' || $key == 'action' || $key == 'safetycheck' ){

			}else{
				
				$printvar[$key] = str_replace('/','-',$value);
			}
			
			
		}
		//print_r($printvar);die;
		$printvar['print'] = '1';
		$this->view->qfilter = $printvar;

		$filter 	= $this->_getParam('filter');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');
		$fromMenu	= $this->_getParam('m');
		
		$fPaymentReff = $zf_filter->getEscaped('payReff');
		$fPaymentType = $zf_filter->getEscaped('payType');
		$fTrfType 	  = $zf_filter->getEscaped('trfType');
		$fCreatedFrom = $zf_filter->getEscaped('createdFrom');
		$fCreatedTo   = $zf_filter->getEscaped('createdTo');
		$fPayDateFrom = $zf_filter->getEscaped('payDateFrom');
		$fPayDateTo   = $zf_filter->getEscaped('payDateTo');

		$paramPayment = array("WA" 				=> true,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
		
		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		
		$selectuser	= $this->_db->select()
								->from(array('A'	 		=> 'M_APP_GROUP_USER'),
								   array('GROUP_USER_ID' 	=> 'A.GROUP_USER_ID',
								   		 'USER_ID' => 'A.USER_ID',
								   		 'CUST_ID' => 'A.CUST_ID'
								   		)
								   )
							->join(array('B' => 'M_APP_BOUNDARY_GROUP'), 'A.GROUP_USER_ID = B.GROUP_USER_ID', array())
							->join(array('C' => 'M_APP_BOUNDARY'), 'C.BOUNDARY_ID = B.BOUNDARY_ID', array('BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
								   		 'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
								   		 'CCY_BOUNDARY' => 'C.CCY_BOUNDARY'
								   		))
		->where("A.USER_ID 	= ?" , (string)$this->_userIdLogin);
		
		$datauser = $this->_db->fetchAll($selectuser);
		
// 		echo "<pre>";
// 		print_r($datauser);die;
		
		$rangesql = '';
		foreach ($datauser as $key => $value){
			$rangesql .= " (P.PS_TOTAL_AMOUNT BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
			if(!empty($datauser[$key+1])){
				$rangesql .= " OR ";
			}
		}
$rangesql .= " OR ";
		foreach ($datauser as $key => $value){
			$rangesql .= " (P.PS_TYPE = '14' AND P.PS_REMAIN BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
			if(!empty($datauser[$key+1])){
				$rangesql .= " OR ";
			}
		}
$rangesql .= " OR ";
		foreach ($datauser as $key => $value){
			$rangesql .= " (P.PS_TYPE = '15' AND P.PS_REMAIN BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
			if(!empty($datauser[$key+1])){
				$rangesql .= " OR ";
			}
		}
//		print_r($rangesql);die;
		
		$select   = $CustUser->getPayment($paramPayment);
		
		$select->where('P.PS_STATUS = ?', '20');	
		$select->where('P.PS_PERIODIC = ?', '');	
		// `P`.`PS_PERIODIC`
//$select->joinInner(array('PR' => 'T_PERIODIC'), 'P.PS_PERIODIC = PR.PS_PERIODIC', array() )
//echo $select;die;
//		$select->where("P.PS_PERIODIC in ('0') ");

		if ($fromMenu == 1)
		{
			$defaultFromDate = Application_Helper_General::convertDate(date('Y-m-d'));
					//Application_Helper_General::addDate(date('Y-m-d'), 0, -1);	// 1 month
			$fCreatedFrom 	 = ($fCreatedFrom)? $fCreatedFrom: Application_Helper_General::convertDate($defaultFromDate);
			$fCreatedTo   	 = ($fCreatedTo  )? $fCreatedTo  : Application_Helper_General::convertDate(date('Y-m-d'));
		}
								
		// Filter Data
		if($fCreatedFrom)
		{
			$FormatDate 	= new Zend_Date($fCreatedFrom, $this->_dateDisplayFormat);
			$createdFrom   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) >= ?', $createdFrom);
		}
		
		if($fCreatedTo)
		{
			$FormatDate 	= new Zend_Date($fCreatedTo, $this->_dateDisplayFormat);
			$createdTo  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) <= ?', $createdTo);
		}
		
		if($fPayDateFrom)
		{
			$FormatDate 	= new Zend_Date($fPayDateFrom, $this->_dateDisplayFormat);
			$payDateFrom   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) >= ?', $payDateFrom);
		}
		
		if($fPayDateTo)
		{
			$FormatDate 	= new Zend_Date($fPayDateTo, $this->_dateDisplayFormat);
			$payDateTo  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) <= ?', $payDateTo);
		}
		
		if($fPaymentReff)
		{	$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPaymentReff.'%'));	}	
		
		if($fPaymentType)
		{	
			$fPayType 	 	= explode(",", $fPaymentType);
			$select->where("P.PS_TYPE in (?) ", $fPayType);					
		}
// 		print_r($rangesql);
		if(!empty($rangesql))
		{
		    if(!empty($datauser)){
			$select->where($rangesql);
		    }
			//$select->where("P.PS_NUMBER NOT IN ('P20160921NIG0UTYHC','P20160920PMQ6Q1CEC') ");
			
		}
		
		if ($fTrfType != "")
		{	$select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and TRANSFER_TYPE = ?) > 0", (string)$fTrfType);		}
//		{	$select->where("T.TRANSFER_TYPE = ? ", (string)$fTrfType);		}
		
		$select->order($sortBy.' '.$sortDir);
		// echo "<pre>";
		// echo $select;die;
// 		Zend_Debug::dump($this->_request->getParams());
// 		die;
// 		
		$dataSQL = $this->_db->fetchAll($select);
		
		if ($csv || $pdf || $this->_request->getParam('print'))
		{	$header  = Application_Helper_Array::simpleArray($fields, "label");		}
		else
		{
			$this->paging($dataSQL);	
			$dataSQL = $this->view->paginator;
		}
		
		// arrange data for viewing in html, csv, pdf
		$data = array();
 		//echo '<pre>';
 		//print_r($dataSQL);die;
		foreach ($dataSQL as $d => $dt)
		{
			if (!$csv && !$pdf && !$this->_request->getParam('print'))		
			{	
				$data[$d]["balanceType"] 		= $dt["BALANCE_TYPE"];
				//$data[$d]["allowMultiAction"] = ($dt["PS_CATEGORY"] == "SINGLE PAYMENT")? true: false;	
				$data[$d]["allowMultiAction"] = true;	

			}
			
			
			$persenLabel = $dt["BALANCE_TYPE"] == '2' ? ' %' : '';
				
			foreach ($fields as $key => $field)
			{
				$value = $dt[$key];
				
				if($key == 'accsrc_name'){
					if($dt['PS_TYPE'] == '5'){
						$value = "-";
					}else{
						$value = $value.' / '.$dt['accsrc_alias'];
					}
				}
				if($key == 'acbenef_name'){
					//$value = $value.' / '.$dt['acbenef_alias'];
					if($dt['PS_TYPE'] == '17' || $dt['PS_TYPE'] == '16' || $dt['PS_TYPE'] == '4' || $dt['PS_TYPE'] == '11'){
						$value = "-";
					}else{
						$value = $value.' / '.$dt['acbenef_alias'];
					}
				}
				if ($key == "amount" && $csv)
				{	
					
					if( $dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15'){

						$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
					}else{
						$value = Application_Helper_General::displayMoney($dt['TRA_AMOUNT']);
					}
				}
				elseif ($key == "amount" && !$csv)
				{
					if( $dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15' ){

						$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
					}else{
						$value = Application_Helper_General::displayMoney($dt['TRA_AMOUNT']);
					}	
				}
				elseif ($key == "created")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);			}
				elseif ($key == "efdate")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);					}
				elseif (($key == "accsrc" || $key == "acbenef") && $value != "-")
				{	
					if($dt['PS_TYPE'] == '17' || $dt['PS_TYPE'] == '16'){
						if($key == "acbenef"){
							$value = "-";		
						}
						if($key == "accsrc"){
							$value = $value." [".$dt[$key.'_ccy']."]";		
						}
					}else{
						$value = $value." [".$dt[$key.'_ccy']."]";		
					}
					

				}
				
				$value = ($value == "" && !$csv)? "&nbsp;": $value;
				
				$data[$d][$key] = $this->language->_($value);
			}
		}
		//print_r($data);die;
		if($csv)
		{
			$this->_helper->download->csv($header,$data,null,'List Waiting Approval');  
			Application_Helper_General::writeLog('PAPV','Export CSV Waiting Approval');
		}
		elseif($pdf)
		{
			$this->_helper->download->pdf($header,$data,null,'List Waiting Approval');  
			Application_Helper_General::writeLog('PAPV','Export PDF Waiting Approval'); 
		}
		elseif($this->_request->getParam('print') == 1){
                $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'List Waiting Approval', 'data_header' => $fields));
        }
		else
		{	
			$stringParam = array('payReff'		=> $fPaymentReff,
								 'payType'		=> $fPaymentType,
								 'trfType'		=> $fTrfType,
								 'createdFrom'	=> $fCreatedFrom,
								 'createdTo'	=> $fCreatedTo,
								 'payDateFrom'	=> $fPayDateFrom,
								 'payDateTo'	=> $fPayDateTo,
							    );	
							    
			$this->view->filterPayType 	= $filterPayType;
			$this->view->filterTrfType 	= $filterTrfType;
			$this->view->payReff 		= $fPaymentReff;
			$this->view->payType 		= $fPaymentType;
			$this->view->trfType 		= $fTrfType;
			$this->view->createdFrom 	= $fCreatedFrom;
			$this->view->createdTo 		= $fCreatedTo;
			$this->view->payDateFrom 	= $fPayDateFrom;
			$this->view->payDateTo 		= $fPayDateTo;
			$this->view->query_string_params = $stringParam;
			$this->updateQstring();
 			//print_r($data);die;
			$this->view->data 				= $data;
			$this->view->fields 			= $fields;
			$this->view->filter 			= $filter;
			$this->view->sortBy 			= $sortBy;
	        $this->view->sortDir 			= $sortDir;
			$this->view->allowMultiApprove 	= ($this->view->hasPrivilege('PAPV') && count($data) > 0);
			$this->view->allowMultiReject  	= ($this->view->hasPrivilege('PRJT') && count($data) > 0);
			
			// set URL
			$URL = $this->view->url(array(	'module'	 => $this->view->modulename,
											'controller' => $this->view->controllername,
											'action'	 => 'index',
											'page'		 => $page,
											'sortBy' 	 => $this->view->sortBy,
											'sortDir' 	 => $this->view->sortDir),null,true).$this->view->qstring;
			
			$sessionNamespace = new Zend_Session_Namespace('URL_CP_WA');
			$sessionNamespace->URL = $URL;
			
			Application_Helper_General::writeLog('PAPV','Viewing Waiting Approval');
		}		
	}
}
