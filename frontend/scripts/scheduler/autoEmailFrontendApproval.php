<?php

	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	
	require_once 'General/Settings.php';  
	
	// $emailSent = 0;
	/*
		multi error: Host Timeout
		
	*/
	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	$templateEmailMasterBankWapp = $Settings->getSetting('master_bank_wapp');
	
	$url_fo = $Settings->getSetting('url_fo');
	$template = $Settings->getSetting('femailtemplate_customerpending');
	

	$db 		= Zend_Db_Table::getDefaultAdapter();

	$efdate = date('Y-m-d');
	$isHoliday = true;

	$weekday   = date("l",strtotime($efdate));				// Sunday, ...
	$weekdigit = date("w",strtotime($efdate));				// (0...6)

	if ($weekdigit == "0" || $weekdigit == "6")	// Sunday OR Saturday
	{
			//$isHoliday = "Payment Date is on $weekday.";
			// $day = $language->_($weekday);
			$isHoliday = false;
	}
	else
	{
			//$efdateDB = $efdate->toString($dateDBFormat);

			$select	= $db->select()
								->from(array('A'	 		=> 'M_HOLIDAY'),
									   array('HOLIDAY' 		=> 'A.HOLIDAY_DESCRIPTION')
									   )
								->where("DATE(A.HOLIDAY_DATE) = DATE(?)", $efdate);
			$data = $db->fetchOne($select);

			if (!empty($data))
			{	$isHoliday = false;			}
	}

	
	if($isHoliday){
	
	$selectBuser = $db->select()
					 ->from(array('U' => 'M_USER'), array('U.*'))
					 ->join(array('BG' => 'M_CUSTOMER'), 'BG.CUST_ID = U.CUST_ID', array('BG.CUST_NAME'))
					 ->where('U.USER_PENDING_NOTIF = ?', 1);
					 //->where('USER_ID = ?','PTFE_BENY014');
					  


	$dataBuser 	= $db->fetchAll($selectBuser);	
	foreach ($dataBuser as $key => $value) {
		
		// $groupArr = array ($value ['BGROUP_ID'] );

		// $statusPrivilegeGroup = $db->fetchOne(
		// 							$db->SELECT()
		// 								->FROM('M_BGROUP',array('BGROUP_STATUS'))
		// 								->WHERE('BGROUP_ID = ?',$value['BGROUP_ID'])
		// 																									);
							// mengambil privilege dari group
		$privgroup = $db->select()
						     ->from(array('BPG' => 'M_FPRIVI_USER'), array('BPG.FPRIVI_ID'))
							 // ->join(array('BG' => 'M_BGROUP'), 'BPG.BGROUP_ID = BG.BGROUP_ID', array())
							 // ->joinLeft(array('BP' => 'M_BPRIVILEGE'), 'BPG.BPRIVI_ID = BP.BPRIVI_ID',array())
							 ->where("BPG.FUSER_ID = ?",$value['CUST_ID'].''.$value['USER_ID'])
							 ->query()->fetchAll();

		$privi = ($statusPrivilegeGroup == 1) ? Application_Helper_Array::simpleArray ( $privgroup, 'FPRIVI_ID' ) : array();
		$privi [] = 'systemPrivilege';

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			// $changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
			// $listAutModuleArr = $changeModulePrivilegeObj->getAuthorizeModule() ;

			// $whPriviID = "'".implode("','", $listAutModuleArr)."'";

			$select = $db->SELECT()
							->FROM(array('G' => 'T_GLOBAL_CHANGES'),array('DISPLAY_TABLENAME'))
							// ->joinleft(array('B' => 'M_BUSER'), 'G.CREATED_BY = B.BUSER_ID',array('B.BUSER_BRANCH'))
							// ->joinleft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH',array('C.BRANCH_NAME','C.STATUS'))
							// ->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID',array('BG.BGROUP_CALL'))
							->where('CHANGES_FLAG = '.$db->quote('F'))
							->where('CHANGES_STATUS = '.$db->quote('WA'))
							->where('G.CUST_ID="'.$value['CUST_ID'].'"');

			$result = $db->fetchAll($select);
			// $result = $modelHome->getChangesData($whPriviID);

			$waitingAutorization = array();
			foreach($result as $row)
			{
				if(isset($waitingAutorization[$row['DISPLAY_TABLENAME']]))
				{
					$waitingAutorization[$row['DISPLAY_TABLENAME']] += 1;
				}
				else
				{
					$waitingAutorization[$row['DISPLAY_TABLENAME']] = 1;
				}
			}
			$stringheader = '';
			$stringtable = '';
			// $stringtable = '<table class="table table-sm" style="border-collapse: collapse;width: 100%;"><tbody><tr><th class="tablehead" style="text-align: left;background-color: #003f6e;color: white;" valign="top">No</th><th class="tablehead" style="text-align: left;background-color: #003f6e;color: white;" valign="top">Description</th><th style="text-align: left;background-color: #003f6e;color: white;" class="tablehead" valign="top">Request</th></tr>';

			$stringheader = '';
			$stringtable = '';
		//	$stringtable = '<div class="grid" style="display: flex;flex-wrap: wrap;display: grid;grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));grid-auto-rows: minmax(150px, auto);grid-gap: 1em;">';
			$no = 1;
			foreach ($waitingAutorization as $key => $totalRequest) {
				$stringheader .= '/'.$key;
				if ($no == 1) {$stringtable .= '<table style="width:100%"><tr>';}
				$stringtable .= '<td style="width:30%">';
				$urbimg = "https://360.espay.id/images/icon/setting.png";

				
				if (strpos($key,'Holiday') !== false) {
				    $urbimg = "https://360.espay.id/images/icon/holiday.png";	
				}
				
				if (strpos($key,'Biller') !== false) {
				    $urbimg = "https://360.espay.id/images/icon/biller.png";
				}
				

				if (strpos($key,'Backend') !== false) {
					    $urbimg = "https://360.espay.id/images/customeruser.png";
				}else if (strpos($key,'User Account') !== false) {
					    $urbimg = "https://360.espay.id/images/icon/bankaccount.png";
				}else if (strpos($key,'Customer') !== false) {
					    $urbimg = "https://360.espay.id/images/customeruser.png";
				}else if (strpos($key,'Company') !== false) {
					    $urbimg = "https://360.espay.id/images/icon/newuser.png";
				}else if (strpos($key,'User Account') !== false) {
					    $urbimg = "https://360.espay.id/images/icon/bankaccount.png";
				}else if (strpos($key,'Backend User') !== false) {
					    $urbimg = "https://360.espay.id/images/icon/newuser.png";
				}else if (strpos($key,'Remittance Charges') !== false) {
					    $urbimg = "https://360.espay.id/images/remittance.png";
				}else if (strpos($key,'Local Remittance') !== false) {
					    $urbimg = "https://360.espay.id/images/localremittance.png";
				}else if (strpos($key,'API') !== false) {
					    $urbimg = "https://360.espay.id/images/api.png";
				}else if (strpos($key,'Template Setting') !== false) {
					    $urbimg = "https://360.espay.id/images/icon/template.png";
				}else if (strpos($key,'Minimum Amount') !== false) {
					    $urbimg = "https://360.espay.id/images/icon/remittance.png";
				}else if (strpos($key,'Approver Matrix') !== false) {
					    $urbimg = "https://360.espay.id/images/icon/matrix.png";
				}else if (strpos($key,'Charges') !== false) {
					    $urbimg = "https://360.espay.id/images/charges.png";
				}
				
				
				

				
				$stringtable .= '<div style="width:100%;display:inline-block;vertical-align:top;height: 200px;"><a href="'.$url_fo.'" style="font-weight:bold;color:#0377bc;text-decoration:none" target="_blank" data-saferedirecturl="__web-inspector-hide-shortcut__"><div style="border-radius:8px;margin-bottom:8px;padding:12px 12px 10px;height: 100%;box-sizing:border-box;border:thin solid #e5e7e9"><h5 style="font-size:20px;text-align:left;margin:0;color:rgba(49,53,59,0.96);font-weight:bold;margin:8px 0 3px"><center>'.$totalRequest.'</center></h5><center><img style="height:50px" src="'.$urbimg.'" class="CToWUd"></center><h5 style="font-size:16px;text-align:left;margin:0;color:rgba(49,53,59,0.96);font-weight:bold;margin:8px 0 16px"><center>'.ucfirst($key).'</center></h5></div></a></div>';	
				//$stringtable .= '<div style="border-radius: 15px;background-image: url('.$urbimg.');background-position: center;background-position-y: 15px;background-size: 70px 70px;background-repeat: no-repeat;border: 1px solid black;background-color: #FFFFFF;display: flex;height: 170px;margin-left: 5px;margin-right: 5px;flex: 1 1 200px;">';
				//$stringtable .= '<div class="module"><div style="float:left;margin-top: 70px;"><h3 style="font-size: 25px;color: #000000 !important;margin-bottom:2px;margin-left:10px">'.$totalRequest.'</h3><small style="font-weight: 600;font-size:14px;color:#000000;margin-left:5px;"><strong>'.ucfirst($key).' Changes List </strong></small></div>';
				$stringtable .= '</td>';
				// $stringtable .= '<tr><td style="border-top: 1px solid #dee2e6;">'.$no.'</td><td style="border-top: 1px solid #dee2e6;"><strong>'.ucfirst($key).' Changes List </strong></td><td style="border-top: 1px solid #dee2e6;">'.$totalRequest.'</td></tr>';
				if($no != 1){
				if ($no % 3 == 0) {$stringtable .= '</tr></table><table style="width:100%"><tr>';}
				}
				$no++;
			}
		//	$stringtable .= '</div>';
			// $stringtable .= '</table>';

			

			$translate = array( 
				'[[pending_task_header]]' => $stringheader,
				'[[pending_task_list]]'   => $stringtable,
				'[[user_fullname]]' 		=> $value['USER_FULLNAME'],
				'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
				'[[master_bank_fax]]' 	=> $templateEmailMasterBankFax,
				'[[master_bank_address]]' 	=> $templateEmailMasterBankAddress,
				'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,
				'[[master_bank_app_name]]' 	=> $templateEmailMasterBankApp,
				'[[master_bank_wapp]]' 	=> $templateEmailMasterBankWapp,	
				'[[buser_fullname]]'	=> $value['USER_FULLNAME'],
				'[[comp_name]]'	=> $value['CUST_NAME']
			);
			
			$mailContent = strtr($template,$translate);
			$subject = 'Pending Approval Notification';
			// var_dump($value['USER_EMAIL']);
			// echo $mailContent;
			if(!empty($result)){
			$result = Application_Helper_Email::sendEmail($value['USER_EMAIL'],$subject,$mailContent);
			}

		}


	}

}

	
?>
