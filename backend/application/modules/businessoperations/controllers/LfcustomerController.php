<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class businessoperations_LfcustomerController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new businessoperations_Model_Businessoperations();

		$setting 	= new Settings();			  	
		$enc_pass 	= $setting->getSetting('enc_pass');
		$enc_salt 	= $setting->getSetting('enc_salt');

		$sessToken 			= new Zend_Session_Namespace('Tokenenc');			 
		$password_hash 		= md5($enc_salt.$enc_pass);
		$rand 				= $this->_userIdLogin.date('YmdHis').$password_hash;
		$sessToken->token 	= $rand;
		$this->view->token 	= $sessToken->token; 

		$filter 		= $this->_getParam('filter');
		$filter_clear 	= $this->_getParam('filter_clear');

		//get filtering param
		$filters = array('*' => array('StringTrim', 'StripTags'));

		$validators = array('*' => array('allowEmpty' => true));

		$optionValidators = array('breakChainOnFailure' => false);

		$zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optionValidators);

		$filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');

		if($filter == TRUE && $filter_clear == NULL){
			$fCompanyCode 	= $filterParam['fCompanyCode'] = $this->_request->getParam('fCompanyCode');
			$fCompanyName 	= $filterParam['fCompanyName'] = $this->_request->getParam('fCompanyName');
			$fCollectCode 	= $filterParam['fCollectCode'] = $this->_request->getParam('fCollectCode');
			// $fUploadDateFrom 	= html_entity_decode($zf_filter->getEscaped('fUploadDateFrom'));
			// $fUploadDateTo 		= html_entity_decode($zf_filter->getEscaped('fUploadDateTo'));
		}
			
		// if($fUploadDateFrom){
		// 	$FormatDate = new Zend_Date($fUploadDateFrom, $this->_dateDisplayFormat);
		// 	$fUploadDateFrom = $FormatDate->toString($this->_dateDBFormat);	
		// }
			
		// if($fUploadDateTo){
		// 	$FormatDate = new Zend_Date($fUploadDateTo, $this->_dateDisplayFormat);
		// 	$fUploadDateTo = $FormatDate->toString($this->_dateDBFormat);	
		// }
		$data = $model->getData($filterParam,$filter);
		$this->paging($data);
		// echo '<pre>';
		// print_r($data);
		// echo '</pre><br>';
		// die;

		$this->view->filter 			= $filter;
		$this->view->collectCodeArr 	= $model->getCreditQuality();
		$this->view->fCompanyCode 		= $fCompanyCode;
		$this->view->fCompanyName 		= $fCompanyName;
		$this->view->fCollectCode 		= $fCollectCode;
		// $this->view->fUploadDateFrom 	= $fUploadDateFrom;
		// $this->view->fUploadDateTo 		= $fUploadDateTo;

		Application_Helper_General::writeLog('VFBO','View Line Facility Customer List');
	}

	public function onboardingAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new businessoperations_Model_Businessoperations();

		$getCustomer = $model->getCustomer();
		$custIdArr = Application_Helper_Array::listArray($getCustomer,'CUST_ID','CUST_NAME');
		$this->view->custIdArr = $custIdArr;

		if($this->_request->isPost()){
			$filters = array('*' => array('StringTrim', 'StripTags'));

			$zf_filter_input = new Zend_Filter_Input($filters, null, $this->_request->getPost());

			$cust_id 		= $zf_filter_input->cust_id;
			$pks_number 	= $zf_filter_input->pks_number;
			$pks_file 		= $_FILES['pks_file'];
			$pks_start_date = $zf_filter_input->pks_start_date;
			$pks_exp_date 	= $zf_filter_input->pks_exp_date;
			$plafond_limit 	= Application_Helper_General::convertDisplayMoney($zf_filter_input->plafond_limit);

			if(empty($cust_id)){
				$errDesc['cust_id'] = $this->language->_('Customer cannot be empty');
			}
			if(empty($pks_number)){
				$errDesc['pks_number'] = $this->language->_('Underlying Document cannot be empty');
			}
			if(empty($pks_file['name'])){
				$errDesc['pks_file'] = $this->language->_('Underlying Document cannot be empty');
			}
			if(empty($pks_start_date)){
				$errDesc['pks_start_date'] = $this->language->_('Facility Start Date cannot be empty');
			}
			if(empty($pks_exp_date)){
				$errDesc['pks_exp_date'] = $this->language->_('Facility Expired Date cannot be empty');
			}
			if(empty($plafond_limit) || $plafond_limit == 0){
				$errDesc['plafond_limit'] = $this->language->_('Facility Plafond Limit cannot be empty');
			}

			$this->view->errDesc 		= $errDesc;
			$this->view->cust_id 		= $cust_id;
			$this->view->pks_number 	= $pks_number;
			$this->view->pks_start_date = $pks_start_date;
			$this->view->pks_exp_date 	= $pks_exp_date;
			$this->view->plafond_limit 	= $plafond_limit;

			if(empty($errDesc)){
				$path = UPLOAD_PATH.'/document/submit/';

				$filename_pks = strtolower(uniqid()).'_'.strtolower(str_replace(' ', '_', $pks_file['name']));
				$path_old_pks = $pks_file['tmp_name'];
				$path_new_pks = $path.$filename_pks;
				move_uploaded_file($path_old_pks, $path_new_pks);

				$dataInsert = [
					'CUST_ID'		=> $cust_id,
					'PKS_NUMBER'	=> $pks_number,
					'PKS_FILE'		=> $filename_pks,
					'PKS_START_DATE'=> $pks_start_date,
					'PKS_EXP_DATE'	=> $pks_exp_date,
					'PLAFOND_LIMIT'	=> $plafond_limit
				];
				$this->_db->insert('M_CUST_LINEFACILITY', $dataInsert);

				Application_Helper_General::writeLog('OFBO','Customer Line Facility Onboarding CUST_ID: '.$cust_id);
				$this->setbackURL('/businessoperations/lfcustomer');
				$this->_redirect('/notification/success');
			}
		}else{
			Application_Helper_General::writeLog('UFBO','View Customer Line Facility Onboarding');
		}
	}

	public function detailAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new businessoperations_Model_Businessoperations();

		$id = $this->_getParam('id');
		$download = $this->_getParam('download');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$decryption_id = $AESMYSQL->decrypt($decryption, $password);

		$data = $model->getDataById($decryption_id);
		$this->view->data = $data;

		$currentUrl = $this->getRequest()->getRequestUri();
		$this->view->currentUrl = $currentUrl;

		$params = $this->getRequest()->getParams();
		$this->view->params = $params;

		if($download){
			$path = UPLOAD_PATH.'/document/submit/';
			$file = $data['PKS_FILE'];
			$this->_helper->download->file($file, $path.$file);
		}
	}

	public function updateAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new businessoperations_Model_Businessoperations();

		$id = $this->_getParam('id');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$decryption_id = $AESMYSQL->decrypt($decryption, $password);

		$data = $model->getDataById($decryption_id);
		$this->view->data = $data;

		$currentUrl = $this->getRequest()->getRequestUri();
		$this->view->currentUrl = $currentUrl;

		$params = $this->getRequest()->getParams();
		$this->view->params = $params;

		$custId = $data['CUST_ID'];

		if($this->_request->isPost()){
			$filters = array('*' => array('StringTrim', 'StripTags'));

			$zf_filter_input = new Zend_Filter_Input($filters, null, $this->_request->getPost());

			$pks_number 	= $zf_filter_input->pks_number;
			$pks_file 		= $_FILES['pks_file'];
			$pks_start_date = $zf_filter_input->pks_start_date;
			$pks_exp_date 	= $zf_filter_input->pks_exp_date;
			$plafond_limit 	= Application_Helper_General::convertDisplayMoney($zf_filter_input->plafond_limit);

			if(empty($pks_number)){
				$errDesc['pks_number'] = $this->language->_('Underlying Document cannot be empty');
			}
			if(empty($pks_file['name'])){
				$errDesc['pks_file'] = $this->language->_('Underlying Document cannot be empty');
			}
			if(empty($pks_start_date)){
				$errDesc['pks_start_date'] = $this->language->_('Facility Start Date cannot be empty');
			}
			if(empty($pks_exp_date)){
				$errDesc['pks_exp_date'] = $this->language->_('Facility Expired Date cannot be empty');
			}
			if(empty($plafond_limit) || $plafond_limit == 0){
				$errDesc['plafond_limit'] = $this->language->_('Facility Plafond Limit cannot be empty');
			}

			$this->view->errDesc 		= $errDesc;
			$this->view->pks_number 	= $pks_number;
			$this->view->pks_start_date = $pks_start_date;
			$this->view->pks_exp_date 	= $pks_exp_date;
			$this->view->plafond_limit 	= $plafond_limit;

			if(empty($errDesc)){
				$path = UPLOAD_PATH.'/document/submit/';

				$filename_pks = strtolower(uniqid()).'_'.strtolower(str_replace(' ', '_', $pks_file['name']));
				$path_old_pks = $pks_file['tmp_name'];
				$path_new_pks = $path.$filename_pks;
				move_uploaded_file($path_old_pks, $path_new_pks);

				$dir = $path.$data['PKS_FILE'];
				if(is_file($dir)){
					unlink($dir);
				}

				$dataUpdate = [
					'PKS_NUMBER'	=> $pks_number,
					'PKS_FILE'		=> $filename_pks,
					'PKS_START_DATE'=> $pks_start_date,
					'PKS_EXP_DATE'	=> $pks_exp_date,
					'PLAFOND_LIMIT'	=> $plafond_limit
				];
				$where = ['ID = ?'=>$decryption_id];
				$this->_db->update('M_CUST_LINEFACILITY', $dataUpdate, $where);

				Application_Helper_General::writeLog('UFBO','Update Customer Line Facility CUST_ID: '.$custId);
				$this->setbackURL('/businessoperations/lfcustomer');
				$this->_redirect('/notification/success');
			}
		}else{
			Application_Helper_General::writeLog('UFBO','View Update Customer Line Facility');
		}
	}
}