<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class Administrator_ReleasepaymentslipdetailController extends Application_Main
{
  public function indexAction() 
  { 
    $this->_helper->layout()->setLayout('newpopup');

  	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $slipnumb = $AESMYSQL->decrypt($this->_getParam('slipnumb'), $password);

  	$select = $this->_db->select()
					        ->from(array('B' => 'T_PSLIP'),array('*'))
					        ->joinleft(array('C' => 'T_TRANSACTION'), 'B.PS_NUMBER = C.PS_NUMBER',array('*'))
					        ->joinleft(array('D' => 'M_CUSTOMER'), 'B.CUST_ID = D.CUST_ID',array('*'));
	$select -> where("B.PS_NUMBER = ".$this->_db->quote($slipnumb));
	$result = $select->query()->FetchAll();
	$this->view->result=$result;
	//echo $select;
	//Zend_Debug::dump($select); die;
  $pdf      = $this->_getParam('pdf');
  $this->view->pdf    = false;
  }
}