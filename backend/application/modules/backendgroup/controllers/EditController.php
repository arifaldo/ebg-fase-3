<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/Settings.php';

class backendgroup_EditController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();
        $groupid = $AESMYSQL->decrypt($this->_getParam('groupid'), $password);
        
		$this->view->groupid = $groupid;
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));
		$privil = $this->_db->select()
							 ->FROM ('M_BPRIVILEGE');
							 
		
		$setting = new Settings();
  
  		$type = $setting->getSetting('system_type');
		 if($type=='1'){
	        $privil->where('BPRIVI_MODE IN (0,1)');
	      }else if($type=='2'){
	        $privil->where('BPRIVI_MODE IN (0,2)');
	      }
	      			$privil->ORDER('BPRIVI_DESC ASC');

	    // echo $privil;die();

		$privil = $this->_db->fetchAll($privil);

						     // ->query()->fetchall();
		$this->view->privil = $privil;

		$select = $this->_db->select()
					        ->from(array('TEMP_BGROUP'));
		$select -> where("BGROUP_ID LIKE ".$this->_db->quote($groupid));
		$result = $select->query()->FetchAll();

		if($result)
		{
			$change = "disabled";
			$this->view->change = $change;
		}

		$filterArr = array('groupid'    => array('StripTags','StringTrim','StringToUpper'),
						   'groupname'  => array('StripTags','StringTrim'),
								  );

		$caseStatus = "(CASE BGROUP_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";

			$cek = $this->_db->select()
					        ->from(array('M_BGROUP'),
					        array('BGROUP_ID','BGROUP_DESC','BGROUP_CALL','UPDATED','CREATED','CREATEDBY','BGROUP_STATUS'=>$caseStatus));
		$cek -> where("BGROUP_ID LIKE ".$this->_db->quote($groupid));
		$cekresult = $cek->query()->FetchAll();
		//die;
		$expid = "BGROUP_ID != ".$this->_db->quote($cekresult[0]['BGROUP_ID']);
		$expname = "BGROUP_DESC != ".$this->_db->quote($cekresult[0]['BGROUP_DESC']);
		$status = $cekresult[0]['BGROUP_STATUS'];
		//die;
		$validators = array(
							'groupid' => array(),
							'groupname' => array('NotEmpty',
															array('StringLength',array('min'=>1,'max'=>32)),
															array('Db_NoRecordExists',array('table'=>'M_BGROUP','field'=>'BGROUP_DESC','exclude'=>$expname)),
															array('Db_NoRecordExists',array('table'=>'TEMP_BGROUP','field'=>'BGROUP_DESC','exclude'=>$expname)),
															'messages' => array(
																	$this->language->_('Can not be empty'),
																	$this->language->_('Template Code length must not be more than 32'),
																	$this->language->_('Group Name already in use. Please use another'),
																	$this->language->_('Group Name already suggested. Please use another')
																	)
												)
						);

    	$zf_filter = new Zend_Filter_Input($filterArr,$validators,$this->_request->getParams());
    	$submit = $this->_getParam('submit');
    	$reset = $this->_getParam('reset');
    	$groupname = $this->_getParam('groupname');
    	// $callcenter 	= $this->_getParam('callcenter');
    	$re = '%^[a-zA-Z0-9+\- ]*$%';
		$cekspecialchar = preg_match($re, $zf_filter->groupname)? 1 : 0;
		//Zend_Debug::dump($validators);die;
		if(!$submit || $reset)
		{
			$select4 = $this->_db->select()
					        ->from(array('M_BPRIVI_GROUP'));
			$select4 -> where("BGROUP_ID LIKE ".$this->_db->quote($groupid));
			$result4 = $select4->query()->FetchAll();

			foreach($privil as $row)
			{
				foreach($result4 as $row2)
				{
					if($row2['BPRIVI_ID'] == $row['BPRIVI_ID'])
					{
						$this->view->$row['BPRIVI_ID'] = '1';
						break;
					}
				}
			}

			$this->view->groupid = $cekresult[0]['BGROUP_ID'];
			$this->view->groupname = $cekresult[0]['BGROUP_DESC'];
			// $this->view->callcenter = $cekresult[0]['BGROUP_CALL'];
			$this->view->prev = $result4;
		}


		if($submit && $this->view->hasPrivilege('BGED'))
		{
			$this->view->groupid=$groupid;
			$this->view->groupname=$groupname;
			// $this->view->callcenter	= $callcenter;
			$cekcheckbox = 0;
			foreach($privil as $row)
			{
				if($this->_getParam($row['BPRIVI_ID']) == 1)
				{
					$cekcheckbox++;
					$this->view->$row['BPRIVI_ID'] = $this->_getParam($row['BPRIVI_ID']);
				}
			}
			//Zend_Debug::dump($cekcheckbox);die;
			if($cekcheckbox == 0)
			{
				$this->view->errcheckbox = $this->language->_('Please Select at least 1 Privilege');
				//die;
			}

			$originalgrouparr = array('BAAPPV','BAMAKR');
			$originalgroup = in_array($groupid,$originalgrouparr);

			if($zf_filter->isValid() && $cekcheckbox!=0 && $groupname != $groupid && $cekspecialchar && !$originalgroup)
			{
				$cekcheckbox = 0;
				Application_Helper_General::writeLog('BGED','Editing Backend group ('.$groupid.')');
				$this->_db->beginTransaction();
				try
				{
					$info = "Backend Group";
					$change_id = $this->suggestionWaitingApproval('Backend Group',$info,$this->_changeType['code']['edit'],null,'M_BGROUP','TEMP_BGROUP',$groupid,$groupname);

					$data = array(
									'CHANGES_ID' => $change_id,
									'BGROUP_ID' => $groupid,
									'BGROUP_DESC' => $groupname,
									'BGROUP_CALL' 		=> '0',
									'BGROUP_STATUS' => '1',
									'CREATED' => new Zend_Db_Expr('now()'),
									'CREATEDBY' => $this->_userIdLogin,
									);

					$this->_db->insert('TEMP_BGROUP',$data);


					foreach($privil as $row)
					{
						if($this->_getParam($row['BPRIVI_ID']) == 1)
						{
							$cekcheckbox++;
							$data2 = array(
											'CHANGES_ID' => $change_id,
											'BPRIVI_ID' => $row['BPRIVI_ID'],
											'BGROUP_ID' => $groupid
												);
							$this->_db->insert('TEMP_BPRIVI_GROUP',$data2);
						}
					}

					if($cekcheckbox == 0)
					{
						$this->_db->rollBack();
					}
					else
					{
						$this->_db->commit();
						$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
						$this->_redirect('/notification/submited/index');
					}
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
			else
			{
				if($groupname && $groupid && $groupname == $groupid)
				{
					$this->view->xgroupname = $this->language->_("Group ID and Group name must not to be same");
				}
				else if (!$cekspecialchar)
				{
					$this->view->xgroupname = $this->language->_('Must not contain special characters');
					$zf_filter->groupname = '';
				}
				else
				{
					foreach($zf_filter->getMessages() as $key=>$err)
					{
						$xxx = 'x'.$key;
						$this->view->$xxx = $this->displayError($err);
					}
				}
			}
		}
		else
		{
			Application_Helper_General::writeLog('BGED','Viewing Edit Backend group ('.$groupid.') page');
		}

		if($result)
		{
			$change = "disabled";
			$this->view->change = $change;
		}
	}
}
