<?php


require_once 'Zend/Controller/Action.php';


class Setbillercatalog_SetController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model 			 = new biller_Model_Biller();
		$billerCharges = new setbillercharges_Model_Setbillercharges();
		$providerArr 	 = $billerCharges->getServiceProvider();
    	$options = array_combine(array_values($this->_masterStatus['code']),array_values($this->_masterStatus['desc']));
		unset($options[3]);
		array_unshift($options,'---Any Value---');
		$this->view->options = $options;
		$this->view->var = $providerArr;
		$userid = $this->_userIdLogin;

		$fields = array	(
							'sgo_product_code'  			=> array	(
																	'field' => 'SGO_PRODUCT_CODE',
																	'label' => $this->language->_('Product Code'),
																	'sortable' => true
																),

							'biller_code'  			=> array	(
																	'field' => 'PRODUCT_CODE',
																	'label' => $this->language->_('Product Name'),
																	'sortable' => true
																),
							'product_alias_name'  			=> array	(
																	'field' => 'PRODUCT_ALIAS_NAME',
																	'label' => $this->language->_('Product Alias Name'),
																	'sortable' => true
																),
							'status'  		=> array	(
																	'field' => 'CATALOG_STATUS',
																	'label' => 'Status',
																	'sortable' => true
																),
						);

		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'billerCode'    	=> array('StripTags','StringTrim'),
	                       'billerName'  	=> array('StripTags','StringTrim','StringToUpper'),
	                       'billerStatus'  	=> array('StripTags','StringTrim'),
						   'suggestor'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'approver'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'fDateFrom'  	=> array('StripTags','StringTrim'),
						   'ufDateFrom'  	=> array('StripTags','StringTrim'),
						   'fDateTo'  		=> array('StripTags','StringTrim'),
						   'ufDateTo'  		=> array('StripTags','StringTrim'),
	                      );

	    $validator = array('filter' 		=> array(),
	                       'billerCode'    	=> array(),
	                       'billerName'  	=> array(),
	                       'billerStatus'  	=> array(),
						   'suggestor'  	=> array(),
						   'approver'  	=> array(),
						   'fDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'ufDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'ufDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );

		$fParam = array();
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $billerCode = $fParam['billerCode'] = html_entity_decode($zf_filter->getEscaped('billerCode'));
		$billerName = $fParam['billerName'] = html_entity_decode($zf_filter->getEscaped('billerName'));
		$billerStatus = $fParam['billerStatus'] = html_entity_decode($zf_filter->getEscaped('billerStatus'));
		$suggestor 	= $fParam['suggestor'] 	= html_entity_decode($zf_filter->getEscaped('suggestor'));
		$approver 	= $fParam['approver'] 	= html_entity_decode($zf_filter->getEscaped('approver'));
		$datefrom 	= $fParam['datefrom'] 	= html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$udatefrom 	= $fParam['udatefrom'] 	= html_entity_decode($zf_filter->getEscaped('ufDateFrom'));
		$dateto 	= $fParam['dateto'] 	= html_entity_decode($zf_filter->getEscaped('fDateTo'));
		$udateto 	= $fParam['udateto'] 	= html_entity_decode($zf_filter->getEscaped('ufDateTo'));

		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');
		$set  = $this->_getParam('set');
		$fParam['provid'] = $this->_getParam('provid');
		$this->view->provid = $fParam['provid'] = $this->_getParam('provid');


		$page = (Zend_Validate::is($page,'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';


	if($set)
		{
			Application_Helper_General::writeLog('UBCT','Update Biller Catalog');

			$active = $this->_request->getParam('req_id');

			$flag = false;
			foreach($active as $val)
			{
				if($val == A)
				{
					$flag = true;
					break;
				}
			}

			foreach($active as $key=>$value)
			{

				$model_biller = new biller_Model_Biller();

				$model_biller->setBillerCatalog($key,$value,$userid);
			}
			//$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
			$urlback = '/setbillercatalog/detail/index/provid/'.$this->_getParam('provid');
			$this->setbackURL($urlback);
			$this->_redirect('/notification/success');



		}

		$data = $model->getBillerDetailCatalog($fParam,$sortBy,$sortDir,$filter);


		$this->paging($data);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;


	}
}
