<?php

require_once 'Zend/Controller/Action.php';


class User_ViewController extends Application_Main{
	
	protected $_txStatus = array();
	

  	public function indexAction() 
  	{
  		$actionId = 'V';
  		$moduleDB = 'USF';
	    $cust_id = $this->_custIdLogin;
	    //$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? strtoupper($cust_id) : '';
	    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>25)))? strtoupper($cust_id) : '';
	    
	    if($cust_id)
	    {
	  	  $select = $this->_db->select()
	  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME','CUST_STATUS'))
	  	                         ->where('CUST_ID='.$this->_db->quote((string)$cust_id));
	      $result = $this->_db->fetchRow($select);
	      
	      if($result['CUST_ID'])
	      {
	      	$this->view->cust_name = $result['CUST_NAME'];
	      	$this->view->cust_status = $result['CUST_STATUS'];
	      }else{ $cust_id = ''; }
	  	}
	  	
	    $user_id = $this->_getParam('user_id');
	    
	    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? strtoupper($user_id) : '';
	    if($cust_id && $user_id)
	    {
	      $select = $this->_db->select()
	                             ->from(array('u'=>'M_USER'))
	                             ->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
	                             ->where('u.CUST_ID='.$this->_db->quote((string)$cust_id))
	                             ->where('USER_ID='.$this->_db->quote((string)$user_id));
	                             
	      $resultdata = $this->_db->fetchRow($select);
	      if($resultdata['USER_ID'])
	      {
	        $this->view->user_data = $resultdata;
	        if($resultdata['USER_HASTOKEN'] == 'Y')
	        {
	        	  $request = new stdClass();
		       	  $request->CustomerId = $cust_id;
		       	  $request->UserId = $user_id;
		       	  
	        }
	        
	        $select = $this->_db->select()
	                               ->from('TEMP_USER',array('TEMP_ID'))
	                               ->where('CUST_ID='.$this->_db->quote((string)$cust_id))
	                               ->where('USER_ID='.$this->_db->quote((string)$user_id));
	        $result = $this->_db->fetchOne($select);    
	        if($result){ $temp = 0; }else{ $temp = 1; }
	        $this->view->user_temp = $temp; 
	      }
	      else
	      { 
	      	$user_id = '';
	      	$errorMsg = $this->getErrorRemark('22','User ID');
	    	$this->_helper->getHelper('FlashMessenger')
					->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')
					->addMessage("$errorMsg");
//			$this->frontendLog(strtoupper($actionId),strtoupper($this->_moduleDB),null,null,$errorMsg);
			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed'); 
	      }
	      
	      $selectLimit = $this->_db->select()
	                             ->from(array('u'=>'M_USER_LIMIT'),array('LIMIT_CCY','TXN_LIMIT','DAILY_LIMIT'))
	                             ->where('CUST_ID='.$this->_db->quote((string)$cust_id))
	                             ->where('USER_ID='.$this->_db->quote((string)$user_id));
	                            
	      $this->view->limitData = $this->_db->fetchAll($selectLimit);
	      
	      $ccyArr = $this->_db->select()
								   ->from(array('M_MINAMT_CCY'), array('CCY_ID'))
								   ->where('M_MINAMT_CCY.CCY_STATUS='.$this->_db->quote((string)$this->_masterStatus['code']['active']))
								   ->order('CCY_ID ASC')
								   ->query()->fetchAll();
								   
		  $this->view->ccyArr  = Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		  $this->view->cust_id = $cust_id;
    	  $this->view->change_type = $this->_changeType;
	      $this->view->status_type = $this->_masteruserStatus;
	      $this->view->ynstatus_type = $this->_masterhasStatus;
	      $this->view->user_id = $user_id;
	      $this->view->btn_back = ('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/success');
	      $this->view->btn_logout = ('/user/logout/index');
	      $this->view->btn_active = ('/user/active/index');
	      $this->view->btn_edit = ('/user/edit/index');
	      $this->view->btn_resetpass = ('/user/resetpassword/index');
//	      $this->frontendLog('V',strtoupper($moduleDB),null,null,null);
	    }
		else
		{
	    	$errorMsg = $this->getErrorRemark('22','User ID');
	    	$this->_helper->getHelper('FlashMessenger')
					->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')
					->addMessage("$errorMsg");
//			$this->frontendLog(strtoupper($actionId),strtoupper($this->_moduleDB),null,null,$errorMsg);
			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
	    }
	    
	    
  	}
  	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}
	

}

