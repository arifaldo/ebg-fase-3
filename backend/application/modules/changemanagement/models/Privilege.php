<?php

/**
 * privilege mapping for dual control
 *
 * @author
 * @version
 */
class Changemanagement_Model_Privilege
{

	protected $_privilegeMap = array(
		// 'ATCC' => 'bgreport',
		// 'ATNC' => 'bgreport',
		'VMPC' => 'manageproductaccount',
		'DLCL' => 'userdailylimit',
		'AGCL' => 'appgroup',
		'VUCL' => 'assignuserscommunity',
		'VBGC' => 'backendgroup',
		'ADCL' => 'backenduser',
		'BMCL' => "authorizationmatrix','boundary",
		'CTCL' => 'chargestemplatelist',
		'VCCL' => 'community',
		'CHCL' => "adminfeeaccount','adminfeecompany','companycharges','chargesdomestic','disbursementcharges','monthlyaccount','monthlycompany','realtimecharges','settlementcharges','specialcharges','trafficapicharges", //BE AWARE dengan petiknya karena digunakan untuk SQL
		'CCCL' => "customer','bglimit",
		'CSCL' => 'user',
		'STCL' => "generalsetting','transactiondailylimit",
		'VGSC' => 'globalscheme',
		'HDCL' => 'holiday',
		'HDUD' => 'holidaysetting',
		'VMCL' => 'member',
		'CRCL' => 'minamountccy',
		'VDCL' => 'physicaldocumentconfig',
		'VPCL' => 'principal',
		'VRCL' => 'rootcommunity',
		'VSCL' => 'scheme',
		'ACCL' => 'customeraccount',
		'BLCL' => 'systembalance',
		'MLCA' => "userlimit','useropenlimit",
		'ECCL' => 'escrow',
		'VWBC' => 'setbillercharges',
		'VLBI' => 'biller',
		'VBAC' => 'billeraccount',
		'SBOC' => 'setbilleronboardcharges',
		'VBAC' => 'billeronboard',
		'VLBO' => 'remittancechar',
		'AVPA' => 'businessadapter',
		'APRE' => 'registerechannel',
		//'VLBO' => 'specialcharges',
		'VLBO' => 'localremittancechar',
		'VAMC' => 'bgapprovalmatrix',
		'VMST' => 'manualupdate',
		'VBOC' => "billeronboardaccount','agent','binsetup','bintemplate','directdebit','boundary','corporatedcard','corporatedebitcard','activationcard','bgreport','accountaggregation','nationalpoolinggroup"

		// ''
		// 'VBOC' => "billeronboard','remittancechar','localremittancechar','agent','binsetup','directdebit",
	);


	protected $_privilegeApproveMap = array(
		'AMPC' => 'manageproductaccount',
		'AGCA' => 'appgroup',
		'AGSP' => 'globalscheme',
		'VUCT' => 'assignuserscommunity',
		'BGAP' => 'backendgroup',
		'ADCA' => 'backenduser',
		'BMCA' => "authorizationmatrix','boundary",
		'CTCA' => 'chargestemplatelist',
		'VACT' => 'community',
		'CBCA' => 'member',
		'CHCA' => "adminfeeaccount','adminfeecompany','companycharges','chargesdomestic','disbursementcharges','monthlyaccount','monthlycompany','realtimecharges','specialcharges','settlementcharges','trafficapicharges", //BE AWARE dengan petiknya karena digunakan untuk SQL
		'CCCA' => "customer','bglimit",
		'CSCA' => 'user',
		'STCA' => 'generalsetting',
		'STTR' => 'transactiondailylimit',
		'HDCA' => 'holiday',
		'HDUD' => 'holidaysetting',
		'VAMB' => 'member',
		'CRCA' => 'minamountccy',
		'VPDC' => 'physicaldocumentconfig',
		'VAPC' => 'principal',
		'VARC' => 'rootcommunity',
		'VASH' => 'scheme',
		'ACCA' => 'customeraccount',
		'BLCA' => 'systembalance',
		'DLCA' => 'userdailylimit',
		'MLCA' => "userlimit','useropenlimit",
		'ECCA' => 'escrow',
		'APBC' => 'setbillercharges',
		'APBL' => 'biller',
		'ABAC' => 'billeraccount',
		'APPA' => 'businessadapter',
		'ASBC' => 'setbilleronboardcharges',
		'APBO' => "billeronboard','remittancechar','localremittancechar",
		'AAMC' => 'bgapprovalmatrix',
		'APRE' => 'registerechannel',

		'ABAO' => "billeronboardaccount','binsetup','bintemplate','boundary','corporatedcard','corporatedebitcard','activationcard','bgreport','accountaggregation','directdebit','agent','nationalpoolinggroup",


		// 'RBIO' => "billeronboard','remittancechar','localremittancechar','agent','binsetup','directdebit",
	);

	protected $_privilegeRepairMap = array(
		'AGCR' => 'appgroup',
		'VRAU' => 'assignuserscommunity',
		'BGRP' => 'backendgroup',
		'ADCR' => 'backenduser',
		'BMCR' => "authorizationmatrix','boundary",
		'CTCR' => 'chargestemplatelist',
		'VRCM' => 'community',
		'CBCR' => 'member',
		'CHCR' => "adminfeeaccount','adminfeecompany','companycharges','chargesdomestic','disbursementcharges','monthlyaccount','monthlycompany','specialcharges','realtimecharges','settlementcharges','trafficapicharges", //BE AWARE dengan petiknya karena digunakan untuk SQL
		'CCCR' => "customer','bglimit",
		'CSCR' => 'user',
		'STCR' => 'generalsetting',
		'STTR' => 'transactiondailylimit',
		'VRGP' => 'globalscheme',
		'HDCR' => 'holiday',
		'HDUD' => 'holidaysetting',
		'VRLT' => 'member',
		'CRCR' => 'minamountccy',
		'RPDC' => 'physicaldocumentconfig',
		'VRPC' => 'principal',
		'VRRT' => 'rootcommunity',
		'VRSH' => 'scheme',
		'BLCR' => 'systembalance',
		'DLCR' => 'userdailylimit',
		'MLCR' => "userlimit','useropenlimit",
		'RPBC' => 'setbillercharges',
		'APPA' => 'businessadapter',
		'RBIL' => 'biller',
		'RAMC' => 'bgapprovalmatrix',
		'RBIO' => "billeronboard','remittancechar','localremittancechar','agent','binsetup','bintemplate','directdebit','corporatedebitcard','activationcard','accountaggregation','boundary','corporatedcard','nationalpoolinggroup",
		// 'RBIO' => 'billeronboard',
		// 'RBIO' => 'remittancechar',
		//                               'RBIO' => 'localremittancechar',
		//                               'RBIO' => 'agent',
		// 'RBIO' => 'binsetup'
		// 'RBIO' => 'directdebit'

	);

	protected $_privilegeRequestRepairMap = array();
	protected $_privilegeRejectMap = array();

	public function __construct()
	{
		$this->_privilegeRepairMap = $this->_privilegeRepairMap;
		$this->_privilegeRequestRepairMap = $this->_privilegeApproveMap;
		$this->_privilegeRejectMap = $this->_privilegeApproveMap;
	}

	public function getAuthorizeModule()
	{

		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		// var_dump($myPrivilege);die();
		$authorizeModuleArr = array_intersect_key($this->_privilegeMap, $myPrivilege);
		// echo '<pre>';
		// var_dump($this->_privilegeMap);
		// var_dump($myPrivilege);
		// print_r($authorizeModuleArr );die;
		return $authorizeModuleArr;
	}


	public function isAuthorizeApprove($moduleName)
	{
		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
		$returnValue = false;
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);

			if (array_key_exists('CHCA', $authorizeModuleArr)) {
				$newArr['adminfeeaccount'] = 'CHCA';
				$newArr['adminfeecompany'] = 'CHCA';

				$newArr['companycharges'] = 'CHCA';
				$newArr['boundary'] = 'CHCA';
				$newArr['nationalpoolinggroup'] = 'CHCA';

				// adminfeecompany
				$newArr['chargesdomestic'] = 'CHCA';
				$newArr['disbursementcharges'] = 'CHCA';
				$newArr['monthlyaccount'] = 'CHCA';
				$newArr['monthlycompany'] = 'CHCA';
				$newArr['realtimecharges'] = 'CHCA';
				$newArr['settlementcharges'] = 'CHCA';
				$newArr['directdebit'] = 'CHCA';
				$newArr['specialcharges'] = 'CHCA';

				$newArr['trafficapicharges'] = 'CHCA';
			}

			if (array_key_exists('CCCA', $authorizeModuleArr)) {
				$newArr['customer'] = 'CCCA';
				$newArr['bglimit'] = 'CCCA';
			}

			if (array_key_exists('APBO', $authorizeModuleArr)) {
				$newArr['billeronboard'] = 'APBO';
				$newArr['remittancechar'] = 'APBO';
				//$newArr['specialcharges'] = 'APBO';
				$newArr['localremittancechar'] = 'APBO';
			}

			if (array_key_exists('ABAO', $authorizeModuleArr)) {
				$newArr['binsetup'] = 'ABAO';
				$newArr['bintemplate'] = 'ABAO';
				$newArr['corporatedcard'] = 'ABAO';
				$newArr['corporatedebitcard'] = 'ABAO';
				$newArr['activationcard'] = 'ABAO';
				$newArr['bgreport'] = 'ABAO';
			}

			if (array_key_exists('BMCA', $authorizeModuleArr)) {
				$newArr['boundary'] = 'BMCA';
				$newArr['authorizationmatrix'] = 'BMCA';
			}

			if (array_key_exists('MLCA', $authorizeModuleArr)) {
				$newArr['userlimit'] = 'MLCA';
				$newArr['useropenlimit'] = 'MLCA';
			}

			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  true;
			}
		}
		return $returnValue;
	}
	public function isAuthorizeReject($moduleName)
	{
		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
		$returnValue = false;
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);
			if (array_key_exists('CHCA', $authorizeModuleArr)) {
				$newArr['adminfeeaccount'] = 'CHCA';
				$newArr['adminfeecompany'] = 'CHCA';
				$newArr['companycharges'] = 'CHCA';
				$newArr['chargesdomestic'] = 'CHCA';
				$newArr['disbursementcharges'] = 'CHCA';
				$newArr['nationalpoolinggroup'] = 'CHCA';
				$newArr['monthlyaccount'] = 'CHCA';
				$newArr['monthlycompany'] = 'CHCA';
				$newArr['realtimecharges'] = 'CHCA';
				$newArr['settlementcharges'] = 'CHCA';
				$newArr['directdebit'] = 'CHCA';
				$newArr['specialcharges'] = 'CHCA';
				$newArr['trafficapicharges'] = 'CHCA';
			}

			if (array_key_exists('APBO', $authorizeModuleArr)) {
				$newArr['billeronboard'] = 'APBO';
				$newArr['remittancechar'] = 'APBO';
				$newArr['specialcharges'] = 'APBO';
				$newArr['localremittancechar'] = 'APBO';
			}

			if (array_key_exists('CCCA', $authorizeModuleArr)) {
				$newArr['customer'] = 'CCCA';
				$newArr['bglimit'] = 'CCCA';
			}

			if (array_key_exists('ABAO', $authorizeModuleArr)) {
				$newArr['binsetup'] = 'ABAO';
				$newArr['bintemplate'] = 'ABAO';
				$newArr['corporatedcard'] = 'ABAO';
				$newArr['corporatedebitcard'] = 'ABAO';
				$newArr['activationcard'] = 'ABAO';
				$newArr['bgreport'] = 'ABAO';
			}

			if (array_key_exists('BMCA', $authorizeModuleArr)) {
				$newArr['boundary'] = 'BMCA';
				$newArr['authorizationmatrix'] = 'BMCA';
			}

			if (array_key_exists('MLCA', $authorizeModuleArr)) {
				$newArr['userlimit'] = 'MLCA';
				$newArr['useropenlimit'] = 'MLCA';
			}



			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  true;
			}
		}
		return $returnValue;
	}
	public function isAuthorizeRequestRepair($moduleName)
	{
		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeRequestRepairMap, $myPrivilege);
		$returnValue = false;
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);
			if (array_key_exists('CHCA', $authorizeModuleArr)) {
				$newArr['adminfeeaccount'] = 'CHCA';
				$newArr['adminfeecompany'] = 'CHCA';
				$newArr['companycharges'] = 'CHCA';
				$newArr['chargesdomestic'] = 'CHCA';
				$newArr['disbursementcharges'] = 'CHCA';
				$newArr['nationalpoolinggroup'] = 'CHCA';
				$newArr['monthlyaccount'] = 'CHCA';
				$newArr['monthlycompany'] = 'CHCA';
				$newArr['realtimecharges'] = 'CHCA';
				$newArr['settlementcharges'] = 'CHCA';
				$newArr['directdebit'] = 'CHCA';
				$newArr['specialcharges'] = 'CHCA';
				$newArr['trafficapicharges'] = 'CHCA';
			}

			if (array_key_exists('ABAO', $authorizeModuleArr)) {
				$newArr['binsetup'] = 'ABAO';
				$newArr['bintemplate'] = 'ABAO';
				$newArr['corporatedcard'] = 'ABAO';
				$newArr['corporatedebitcard'] = 'ABAO';
				$newArr['activationcard'] = 'ABAO';
				$newArr['bgreport'] = 'ABAO';
			}

			if (array_key_exists('CCCA', $authorizeModuleArr)) {
				$newArr['customer'] = 'CCCA';
				$newArr['bglimit'] = 'CCCA';
			}

			if (array_key_exists('APBO', $authorizeModuleArr)) {
				$newArr['billeronboard'] = 'APBO';
				$newArr['remittancechar'] = 'APBO';
				$newArr['specialcharges'] = 'APBO';
				$newArr['localremittancechar'] = 'APBO';
			}

			if (array_key_exists('BMCA', $authorizeModuleArr)) {
				$newArr['boundary'] = 'BMCA';
				$newArr['authorizationmatrix'] = 'BMCA';
			}

			if (array_key_exists('MLCA', $authorizeModuleArr)) {
				$newArr['userlimit'] = 'MLCA';
				$newArr['useropenlimit'] = 'MLCA';
			}



			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  true;
			}
		}
		return $returnValue;
	}

	public function getPrivilege($moduleName, $action)
	{

		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		if ($action == 'Approve') {
			$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
		} elseif ($action == 'Reject') {
			$authorizeModuleArr = array_intersect_key($this->_privilegeRejectMap, $myPrivilege);
		} elseif ($action == 'Request Repair') {
			$authorizeModuleArr = array_intersect_key($this->_privilegeRequestRepairMap, $myPrivilege);
		}

		$returnValue = "REQC";
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);
			if (array_key_exists('CHCA', $authorizeModuleArr)) {
				$newArr['adminfeeaccount'] = 'CHCR';
				$newArr['adminfeecompany'] = 'CHCR';
				$newArr['companycharges'] = 'CHCR';
				$newArr['chargesdomestic'] = 'CHCR';
				$newArr['disbursementcharges'] = 'CHCR';
				$newArr['nationalpoolinggroup'] = 'CHCR';
				$newArr['monthlyaccount'] = 'CHCR';
				$newArr['monthlycompany'] = 'CHCR';
				$newArr['realtimecharges'] = 'CHCR';
				$newArr['settlementcharges'] = 'CHCR';
				$newArr['directdebit'] = 'CHCR';
				$newArr['specialcharges'] = 'CHCR';
				$newArr['trafficapicharges'] = 'CHCR';
			}

			if (array_key_exists('ABAO', $authorizeModuleArr)) {
				$newArr['binsetup'] = 'ABAO';
				$newArr['bintemplate'] = 'ABAO';
				$newArr['corporatedcard'] = 'ABAO';
				$newArr['corporatedebitcard'] = 'ABAO';
				$newArr['activationcard'] = 'ABAO';
				$newArr['bgreport'] = 'ABAO';
			}

			if (array_key_exists('CCCA', $authorizeModuleArr)) {
				$newArr['customer'] = 'CCCA';
				$newArr['bglimit'] = 'CCCA';
			}

			if (array_key_exists('BMCA', $authorizeModuleArr)) {
				$newArr['boundary'] = 'BMCA';
				$newArr['authorizationmatrix'] = 'BMCA';
			}

			if (array_key_exists('MLCA', $authorizeModuleArr)) {
				$newArr['userlimit'] = 'MLCA';
				$newArr['useropenlimit'] = 'MLCA';
			}



			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  $newArr[$moduleName];
			}
		}
		return $returnValue;
	}


	public function isAuthorizeRepair($moduleName)
	{
		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeRepairMap, $myPrivilege);
		$returnValue = false;
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);
			if (array_key_exists('CHCR', $authorizeModuleArr)) {
				$newArr['adminfeeaccount'] = 'CHCR';
				$newArr['adminfeecompany'] = 'CHCR';
				$newArr['companycharges'] = 'CHCR';
				$newArr['chargesdomestic'] = 'CHCR';
				$newArr['disbursementcharges'] = 'CHCR';
				$newArr['nationalpoolinggroup'] = 'CHCR';
				$newArr['monthlyaccount'] = 'CHCR';
				$newArr['monthlycompany'] = 'CHCR';
				$newArr['realtimecharges'] = 'CHCR';
				$newArr['settlementcharges'] = 'CHCR';
				$newArr['directdebit'] = 'CHCR';
				$newArr['specialcharges'] = 'CHCR';
				$newArr['trafficapicharges'] = 'CHCR';
			}

			if (array_key_exists('ABAO', $authorizeModuleArr)) {
				$newArr['binsetup'] = 'ABAO';
				$newArr['bintemplate'] = 'ABAO';
				$newArr['corporatedcard'] = 'ABAO';
				$newArr['corporatedebitcard'] = 'ABAO';
				$newArr['activationcard'] = 'ABAO';
				$newArr['bgreport'] = 'ABAO';
			}

			if (array_key_exists('CCCR', $authorizeModuleArr)) {
				$newArr['customer'] = 'CCCR';
				$newArr['bglimit'] = 'CCCR';
			}

			if (array_key_exists('BMCR', $authorizeModuleArr)) {
				$newArr['boundary'] = 'BMCR';
				$newArr['authorizationmatrix'] = 'BMCR';
			}

			if (array_key_exists('MLCR', $authorizeModuleArr)) {
				$newArr['userlimit'] = 'MLCR';
				$newArr['useropenlimit'] = 'MLCR';
			}




			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  true;
			}
		}
		return $returnValue;
	}
}
