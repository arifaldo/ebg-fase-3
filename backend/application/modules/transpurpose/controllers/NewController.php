<?php

require_once 'Zend/Controller/Action.php';

class Transpurpose_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');    
	    $this->setbackURL('/transpurpose/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$this->view->report_msg = array();

    	$model = new transpurpose_Model_Transpurpose();
	    $this->view->categoryArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCategory(),'CATEGORY','CATEGORY'));

		if($this->_request->isPost() )
		{
			$filters = array(
			                 	'trans_code'     => array('StringTrim','StripTags'),
								'trans_category'     => array('StringTrim','StripTags'),
								'trans_descript'     => array('StringTrim','StripTags'),
							);

			$validators = array(
								'trans_code'    => array(
															'NotEmpty',
															array('Db_NoRecordExists', array('table' => 'M_TRANSACTION_PURPOSE', 'field' => 'CODE')),
															'messages' => array(
																              $this->language->_('Can not be empty'),
																              $this->language->_('Code already exist'),
														                      )
														   ),
								'trans_category'    => array(
															'NotEmpty',
															
															'messages' => array(
																	$this->language->_('Can not be empty'),
																	
															)
								),
								'trans_descript'    => array(
															'NotEmpty',
															
															'messages' => array(
																	$this->language->_('Can not be empty'),
															
															)
								),
							   );
		
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
// 				print_r($this->_request->getParams());
				$created = date('Y-m-d H:i:s');

				$content = array(
								'CODE' 	 		=> $zf_filter_input->getEscaped('trans_code'),
								'CATEGORY' 	 	 => $zf_filter_input->getEscaped('trans_category'),
								'DESCRIPTION'	 => $zf_filter_input->getEscaped('trans_descript')
						       );
// 					print_r($content);	       die;
				try 
				{
					
					//-------- insert --------------
					$this->_db->beginTransaction();
					
					$this->_db->insert('M_TRANSACTION_PURPOSE',$content);
					
					// $this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('TPAD','Add Transaction Purpose. Code : ['.$zf_filter_input->getEscaped("trans_code").'], Country Name : ['.$zf_filter_input->getEscaped('trans_category').']');
					$this->view->success = true;
					$this->view->report_msg = array();
					
					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					foreach(array_keys($filters) as $field)
						$this->view->$field = $this->_getParam($field);

					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = $this->_getParam($field);

				$error = $zf_filter_input->getMessages();
				
				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
		
		Application_Helper_General::writeLog('TPAD','Add Transaction Purpose');
	}

}