<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class manageproductaccount_DeleteController extends Application_Main
{

	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$model = new manageproductaccount_Model_Manageproductaccount();

		//pengaturan url untuk button back
		$this->setbackURL('/' . $this->view->modulename);

		$filters = array('product_id' => array('StringTrim', 'StripTags'));

		$validators =  array(
			'product_id' => array(
				'NotEmpty',
				array('Db_RecordExists', array('table' => 'M_PRODUCT', 'field' => 'PRODUCT_ID')),
				'messages' => array(
					$this->language->_('Cannt be empty'),
					$this->language->_('Product ID is not founded'),
				)
			)
		);

		if (array_key_exists('product_id', $this->_request->getParams())) {
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
			if ($zf_filter_input->isValid()) {


				try {
					// Application_Helper_General::writeLog('AALS','Delete Product Account Type,Product ID : '.$this->_getParam('product_id'));
					$this->_db->beginTransaction();


					$productId  = $zf_filter_input->getEscaped('product_id');

					$select = $this->_db->select()->from(array('M_PRODUCT'), array('*'));
					$select->where("PRODUCT_ID LIKE " . $productId);
					$result = $select->query()->FetchAll();

					Application_Helper_General::writeLog('PAIM', 'Add Temp Data Delete Product Account Type. Account Type : [' . $result[0]['PRODUCT_CODE'] . '], Product Type : [' . $result[0]['PRODUCT_PLAN'] . '], Product Name : [' . $result[0]['PRODUCT_NAME'] . ']');

					$info = "Product Account";
					$change_id = $this->suggestionWaitingApproval('Product Type', $info, $this->_changeType['code']['delete'], null, 'M_PRODUCT', 'TEMP_PRODUCT_TYPE', $result[0]['PRODUCT_CODE'], $result[0]['PRODUCT_NAME']);

					$content = array(
						'CHANGES_ID' => $change_id,
						'PRODUCT_ID' => $result[0]['PRODUCT_ID'],
						'PRODUCT_CODE' 	=> $result[0]['PRODUCT_CODE'],
						'PRODUCT_PLAN' 	=> $result[0]['PRODUCT_PLAN'],
						'PRODUCT_NAME'  => $result[0]['PRODUCT_NAME'],
						'ACCT_TYPE'  => $result[0]['ACCT_TYPE'],
						'LAST_SUGGESTED' => new Zend_Db_Expr('now()'),
						'LAST_SUGGESTEDBY' => $this->_userIdLogin,
					);

					$model->insertTempData($content);

					$this->_db->commit();
					$this->setbackURL('/' . $this->_request->getModuleName() . '/index/');
					$this->_redirect('/notification/submited/index');


					//$this->_db->commit();
					//$this->_redirect('/notification/success/index'); 

				} catch (Exception $e) {
					//rollback changes
					$this->_db->rollBack();
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/failed');
				}
			} else {
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();

				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach ($errors as $key => $error) {
					foreach ($error as $key2 => $errorMsg) {
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						Application_Helper_General::writeLog('AALS', 'Update Product Type');

						$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/failed');
					}
				}
			}
		} else {
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

			Application_Helper_General::writeLog('OBDE', 'Delete others bank');

			$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/failed');
		}
	}


	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}

	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}
}
