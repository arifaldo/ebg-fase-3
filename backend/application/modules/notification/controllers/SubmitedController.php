<?php
require_once 'Zend/Controller/Action.php';

class notification_SubmitedController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ns = new Zend_Session_Namespace('FVC');
		$ns->backURL = '';

		$this->view->cust_id = strip_tags(trim($this->_getParam('cust_id')));
	}

	public function paperAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ns = new Zend_Session_Namespace('FVC');
		$ns->backURL = '';


		$this->view->cust_id = strip_tags(trim($this->_getParam('cust_id')));
	}
}
