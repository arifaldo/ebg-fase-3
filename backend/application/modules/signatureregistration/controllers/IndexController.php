<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';

class Signatureregistration_IndexController extends Application_Main
{
	/**
	 * The default action - show the home page
	 */

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();

		Application_Helper_General::writeLog('SBGC', 'Signing Bank Guarantee Certificate');

		$select2 = $this->_db->select()
			->from(array('A' => 'M_BUSER'), array('*'))
			->where("BUSER_ID = ?", $this->_userIdLogin);
		$buserdata = $select2->query()->fetchAll();

		$requestcheck['nik'] = $buserdata['0']['BUSER_NIK'];
		$clientUser  =  new SGO_Soap_ClientUser();
		//var_dump($requestcheck);die;			
		$success = $clientUser->callapi('checkDS', $requestcheck);

		$result  = $clientUser->getResult();

		if ($result->data == null) {
			//echo 'here';die;
			//var_dump($result);
			$this->view->message = $result->message;
			// $this->view->message  = (array) $result->message; 
			$this->view->unregist = true;
		}

		if ($this->_request->isPost()) {
			if ($this->_getParam('Submit')) {

				$request['email'] = $buserdata['0']['BUSER_EMAIL'];
				$clientUser  =  new SGO_Soap_ClientUser();

				$success = $clientUser->callapi('registerDS', $request);
				//var_dump($request);die;
				$result  = $clientUser->getResult();
				$this->setbackURL('/signatureregistration');
				$this->_redirect('/notification/submited/index');
			}
		}
	}


	public function registerAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$select2 = $this->_db->select()
			->from(array('A' => 'M_BUSER'), array('*'))
			->where("BUSER_ID = ?", $this->_userIdLogin);
		$buserdata = $select2->query()->fetchAll();

		$request['email'] = $buserdata['0']['BUSER_EMAIL'];
		$clientUser  =  new SGO_Soap_ClientUser();

		$success = $clientUser->callapi('registerDS', $request);
		$result  = $clientUser->getResult();

		header('Content-Type: application/json');
		echo json_encode($result);
	}
}
