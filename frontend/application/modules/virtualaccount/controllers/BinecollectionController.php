<?php

require_once 'Zend/Controller/Action.php';

class virtualaccount_BinecollectionController extends Application_Main
{

  public function indexAction()
  {

    $setting = new Settings();          
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
    $pw_hash = md5($enc_salt.$enc_pass);
    $rand = $this->_userIdLogin.date('dHis').$pw_hash;
    $sessionNamespace->token  = $rand;
    $this->view->token = $sessionNamespace->token;

    $this->_helper->layout()->setLayout('newlayout');

    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    // echo "<pre>";
    // print_r($temp);die();
    if (count($temp) > 1) {
      if ($temp[0] == 'F' || $temp[0] == 'S') {
        if ($temp[0] == 'F')
          $this->view->error = 1;
        else
          $this->view->success = 1;
        $msg = '';
        unset($temp[0]);
        foreach ($temp as $value) {
          if (!is_array($value))
            $value = array($value);
          $msg .= $this->view->formErrors($value);
        }
        $this->view->customer_msg = $msg;
      }
    }


    $delete     = $this->_getParam('delete');
    $open     = $this->_getParam('open');
    $close     = $this->_getParam('close');


    //suspend
    if ($close || $open || $delete) {
        $binId = $this->_request->getParam('binid');

        $validators = array (
                                'binid' => array   (
                                                        'NotEmpty',
                                                        'messages' => array (
                                                                                'No Selected bin',
                                                                            )
                                                    ),
                            );

        $filtersVal = array ( 'binid' => array('StringTrim','StripTags'));

        $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
            $success = false;
        foreach ($binId as $key => $value) {
            if(!empty($value)){
                $success = true;
            }
        }


        if($zf_filter_input->isValid() && $success)
        {
            try
            {
                $this->_db->beginTransaction();
                foreach ($binId as $key => $value)
                {
                  $explodeVal = explode('-', $value);
                  $cust_id = $explodeVal[0];
                  $cust_bin = $explodeVal[1];
                  $vaNumber = $explodeVal[2];

                  $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
                  $error_remark = null;
                  // jika customer id valid
                  if($cust_id)
                  {   

                     if ($open) {
                       $va_status = 1;
                     }
                     else if($close){
                      $va_status = 2;
                     }
                     else if($delete){
                      $va_status = 3;
                     }

                      $where = array(
                            'CUST_ID = ?' => $cust_id,
                            'CUST_BIN = ?' => $cust_bin,
                            'VA_NUMBER = ?' => $vaNumber
                      );

                      $param['UPDATED_DATE'] = new Zend_Db_Expr("now()");
                      $param['UPDATED_BY'] = $this->_userIdLogin;
                      $param['VA_STATUS'] = $va_status;

                      try{
                        $this->_db->update('T_VA_TRANSACTION', $param, $where);
                      }catch(Exception $e){
                        print_r($e->getMessage());
                        die;
                      }
                    
                   }// END IF CUST_ID
                   else{
                    $error_remark[] = 'Invalid Customer ID';
                   }
                }

                if (empty($error_remark)) {
                  $this->_db->commit();
                  $ns = new Zend_Session_Namespace('FVC');
                  $ns->backURL = '/virtualaccount/binecollection';
          
                  $this->_redirect('/notification/success/index');
                }
            }
            catch(Exception $e)
            {
                $this->_db->rollBack();
            }
        }
        else
        {
            $error          = true;
            $errors         = $zf_filter_input->getMessages();
            $biniderr   = (isset($errors['biniderr']))? $errors['biniderr'] : null;
        }
    }

    //bin list
    $select = $this->_db->select()
        ->from(array('B' => 'M_CUSTOMER_BIN'),array('*'))
        ->where('B.CUST_ID = ?', (string)$this->_custIdLogin)
        ->where('B.BIN_TYPE = 1') //ecollection
        ->query()->fetchAll();

    $optBin = '<option value="" >-- ' . $this->language->_('Any Value') . ' --</option>';
    foreach ($select as $key => $value) {
      $optBin .= '<option value="'.$value['CUST_BIN'].'">'.$value['CUST_BIN'].'</option>';
    }

    $this->view->optBin = $optBin;


    $filter     = $this->_getParam('filter');
    $bin     = $this->_getParam('BIN');

           
    if ($filter == '') {
      $data = array();
    }

    $select = array();
    $select = $this->_db->select()
      ->from(array('T' => 'T_VA_TRANSACTION'), array(         
                                  'CUST_BIN',
                                  'CUST_ID',
                                  'VANUMBER'           => 'VA_NUMBER',
                                  'VA_NAME',
                                  'CCY',
                                  'BILL_AMOUNT'         => 'INVOICE_AMOUNT',
                                  'PAID_AMOUNT'         => 'SETTLED_AMOUNT',
                                  'EXP_DATE',
                                  'UPDATED_DATE',
                                  'UPDATED_BY',
                                  'VASTATUS' => 'VA_STATUS',
                                  'VA_STATUS'           => new Zend_Db_Expr('CASE
                                                                              WHEN VA_STATUS = 1 THEN "Open"
                                                                              WHEN VA_STATUS = 2 THEN "Closed"
                                                                              WHEN VA_STATUS = 3 THEN "Deleted"
                                                                            END
                                                                           '),
                                  'DOC_STATUS'           => new Zend_Db_Expr('CASE
                                                                              WHEN DOC_STATUS = 0 THEN "In Billing"
                                                                              WHEN DOC_STATUS = 1 THEN "Pending Future Date"
                                                                              WHEN DOC_STATUS = 2 THEN "Settled"
                                                                              WHEN DOC_STATUS = 3 THEN "Unsettled"
                                                                              WHEN DOC_STATUS = 4 THEN "Cancelled"
                                                                              WHEN EXP_DATE < "'.date('Y-m-d H:i:s').'" THEN "Unsettled"
                                                                              WHEN VA_STATUS = 3 THEN "-"
                                                                            END
                                                                           ')
                               ))
      ->where('CUST_ID = ?', (string)$this->_custIdLogin)
      ->order('CREATED_DATE DESC');

    $fields = array(
          'va_number'     => array(
            'field'    => 'VA_NUMBER',
            'label'    => $this->language->_('VA_NUMBER'),
            'sortable' => true
          ),
          'va_name'   => array(
            'field'    => 'VA_NUMBER',
            'label'    => $this->language->_('VA_NAME'),
            'sortable' => true
          ),
          'ccy' => array(
            'field'    => 'ccy',
            'label'    => $this->language->_('CCY'),
            'sortable' => true
          ),
          'bill_amount' => array(
            'field'    => 'BILL_AMOUNT',
            'label'    => $this->language->_('Bill Amount'),
            'sortable' => true
          ),
          'paid_amount'     => array(
            'field'    => 'PAID_AMOUNT',
            'label'    => $this->language->_('Paid Amount'),
            'sortable' => true
          ),
          'expired_date'    => array(
            'field'  => 'EXPIRED_DATE',
            'label'    => $this->language->_('Expired Date'),
            'sortable' => true
          ),
          'last_updated'   => array(
            'field'    => 'LAST_UPDATED',
            'label'    => $this->language->_('Last Updated'),
            'sortable' => true
          ),
          'status'   => array(
            'field'    => 'STATUS',
            'label'    => $this->language->_('Status'),
            'sortable' => true
          ),
          'desc'   => array(
            'field'    => 'DESC',
            'label'    => $this->language->_('Desc'),
            'sortable' => true
          )
        );       
        $filterlist = array('VA_NUMBER', 'VA_NAME', 'CCY', 'PAID_AMOUNT', 'STATUS', 'DESC', 'LAST_UPDATED', 'EXPIRED_DATE');

        $filterArr = array(
          'filter'  => array('StripTags', 'StringTrim'),
          'BIN'  => array('StripTags', 'StringTrim'),
          'VA_NUMBER'  => array('StripTags', 'StringTrim'),
          'VA_NAME'  => array('StripTags', 'StringTrim'),
          'CCY'  => array('StripTags', 'StringTrim'),
          'PAID_AMOUNT'  => array('StripTags', 'StringTrim'),
          'LAST_UPDATED'   => array('StripTags', 'StringTrim', 'StringToUpper'),
          'LAST_UPDATED_END'       => array('StripTags', 'StringTrim'),
          'EXPIRED_DATE'   => array('StripTags', 'StringTrim', 'StringToUpper'),
          'EXPIRED_DATE_END'       => array('StripTags', 'StringTrim'),
          'STATUS'  => array('StripTags', 'StringTrim'),
          'DESC'  => array('StripTags', 'StringTrim'),
        );

        $validators = array(
          'filter' => array(),
          'BIN'    => array(),
          'VA_NUMBER'    => array(),
          'VA_NAME'    => array(),
          'CCY'  => array(),
          'PAID_AMOUNT'  => array(),
          'LAST_UPDATED' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'LAST_UPDATED_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'EXPIRED_DATE' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'EXPIRED_DATE_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'STATUS' => array(),
          'DESC' => array(),
        );

        $dataParam = array('VA_NUMBER', 'VA_NAME', 'CCY', 'PAID_AMOUNT', 'STATUS', 'DESC', 'LAST_UPDATED', 'EXPIRED_DATE');

    if ($filter == TRUE && !empty($bin)) {

      $vaType = $this->_getParam('vatype');

      //if va type static
      if ($vaType != '0') {

        $select = array();
        $select = $this->_db->select()
          ->from(array('T' => 'T_VA_TRANSACTION'), array(         
                                                    'T.CUST_BIN',
                                                    'T.CUST_ID',
                                                    'T.VA_ID',
                                                    'VANUMBER'           => 'T.VA_NUMBER',
                                                    'VA_NUMBER'           => new Zend_Db_Expr('CONCAT(T.CUST_BIN, "-", T.VA_NUMBER)'),
                                                    'VA_NAME',
                                                    'DOC'                 => new Zend_Db_Expr('COUNT(T.VA_ID)'),
                                                    'CCY',
                                                    'TOTAL_BILL_AMOUNT'   => new Zend_Db_Expr('SUM(T.INVOICE_AMOUNT)'),
                                                    'PAID_AMOUNT'         => new Zend_Db_Expr('SUM(T.SETTLED_AMOUNT)'),
                                                    'SETTLED_WITH_REMARK' => new Zend_Db_Expr('CASE
                                                                                                WHEN T.DOC_STATUS = 2 AND T.REMARKS IS NOT NULL THEN SUM(T.SETTLED_AMOUNT)
                                                                                                ELSE "0"
                                                                                              END'),
                                                    'UNSETTLED_AMOUNT'    => new Zend_Db_Expr('SUM(T.UNSETTLED_AMOUNT)'),
                                                    'IN_BILLING'          => new Zend_Db_Expr('SUM(T.IN_BILLING)'),
                                                    'T.UPDATED_DATE',
                                                    'T.UPDATED_BY',
                                                    'VASTATUS'            => 'T.VA_STATUS',
                                                    'VA_STATUS'           => new Zend_Db_Expr('CASE
                                                                                                WHEN T.VA_STATUS = 1 THEN "Open"
                                                                                                WHEN T.VA_STATUS = 2 THEN "Closed"
                                                                                                WHEN T.VA_STATUS = 3 THEN "Deleted"
                                                                                              END
                                                                                             ')
                                                 ))
          ->join(array('C' => 'T_VA_TRANSACTION_CREDIT'), 'T.VA_NUMBER = C.VA_NUMBER', array('C.AVAILABLE_CREDIT'))
          ->where('T.CUST_ID = ?', (string)$this->_custIdLogin)
          ->where('C.CUST_ID = ?', (string)$this->_custIdLogin)
          ->group('VA_NUMBER')
          ->order('T.CREATED_DATE DESC');


         $fields = array(
          'va_number'     => array(
            'field'    => 'VA_NUMBER',
            'label'    => $this->language->_('VA_NUMBER'),
            'sortable' => true
          ),
          'doc'   => array(
            'field'    => 'DOC',
            'label'    => $this->language->_('Doc#'),
            'sortable' => true
          ),
          'ccy' => array(
            'field'    => 'ccy',
            'label'    => $this->language->_('CCY'),
            'sortable' => true
          ),

          'total_bill_amount' => array(
            'field'    => 'TOTAL_BILL_AMOUNT',
            'label'    => $this->language->_('Total Bill Amount'),
            'sortable' => true
          ),
          'paid_amount'     => array(
            'field'    => 'PAID_AMOUNT',
            'label'    => $this->language->_('Paid Amount'),
            'sortable' => true
          ),
          'settled_w_remark'    => array(
            'field'  => 'SETTLED_WITH_REMARK',
            'label'    => $this->language->_('Settled With Remark'),
            'sortable' => true
          ),
          'unsettled_amount'   => array(
            'field'    => 'UNSETTLED_AMOUNT',
            'label'    => $this->language->_('Unsettled Amount'),
            'sortable' => true
          ),
          'available_credit'   => array(
            'field'    => 'AVAILABLE_CREDIT',
            'label'    => $this->language->_('Available Credit'),
            'sortable' => true
          ),
          'in_billing'   => array(
            'field'    => 'IN_BILLING',
            'label'    => $this->language->_('In Billing'),
            'sortable' => true
          ),
          'last_updated'   => array(
            'field'    => 'LAST_UPDATED',
            'label'    => $this->language->_('Last Updated'),
            'sortable' => true
          ),
          'status'   => array(
            'field'    => 'STATUS',
            'label'    => $this->language->_('Status'),
            'sortable' => true
          )
        );
        $fieldprint = array('VA NUMBER', 'CCY','TOTAL BILL AMOUNT', 'PAID AMOUNT', 'SETTLED WITH REMARK', 'UNSETTLED AMOUNT', 'AVAILABLE CREDIT', 'IN BILLING', 'STATUS', 'LAST UPDATED'); 
        $filterlist = array('VA_NUMBER', 'CCY','TOTAL_BILL_AMOUNT', 'PAID_AMOUNT', 'SETTLED_WITH_REMARK', 'UNSETTLED_AMOUNT', 'AVAILABLE_CREDIT', 'IN_BILLING', 'STATUS', 'LAST_UPDATED');

        $filterArr = array(
          'filter'  => array('StripTags', 'StringTrim'),
          'BIN'  => array('StripTags', 'StringTrim'),
          'VA_NUMBER'  => array('StripTags', 'StringTrim'),
          'CCY'  => array('StripTags', 'StringTrim'),
          'TOTAL_BILL_AMOUNT'  => array('StripTags', 'StringTrim'),
          'PAID_AMOUNT'  => array('StripTags', 'StringTrim'),
          'SETTLED_WITH_REMARK'  => array('StripTags', 'StringTrim'),
          'UNSETTLED_AMOUNT'  => array('StripTags', 'StringTrim'),
          'AVAILABLE_CREDIT'  => array('StripTags', 'StringTrim'),
          'IN_BILLING'  => array('StripTags', 'StringTrim'),
          'LAST_UPDATED'   => array('StripTags', 'StringTrim', 'StringToUpper'),
          'LAST_UPDATED_END'       => array('StripTags', 'StringTrim'),
          'STATUS'  => array('StripTags', 'StringTrim'),
        );

        $validators = array(
          'filter' => array(),
          'BIN' => array(),
          'VA_NUMBER'    => array(),
          'CCY'  => array(),
          'TOTAL_BILL_AMOUNT'  => array(),
          'PAID_AMOUNT'  => array(),
          'SETTLED_WITH_REMARK'  => array(),
          'UNSETTLED_AMOUNT'  => array(),
          'AVAILABLE_CREDIT'  => array(),
          'IN_BILLING'  => array(),
          'LAST_UPDATED' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'LAST_UPDATED_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'STATUS' => array(),
        );

        $dataParam = array('VA_NUMBER', 'CCY', 'TOTAL_BILL_AMOUNT', 'PAID_AMOUNT', 'SETTLED_WITH_REMARK', 'UNSETTLED_AMOUNT', 'AVAILABLE_CREDIT', 'IN_BILLING', 'STATUS', 'LAST_UPDATED');
      }

    //validasi page, jika input page bukan angka               
    $page = $this->_getParam('page');

    $page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;

    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';



    $dataParamValue = array();
    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }
    }

    $dataParamValue['BIN'] = $this->_getParam('BIN');


    if (!empty($this->_request->getParam('updatedate'))) {
      $datearr = $this->_request->getParam('updatedate');
      $dataParamValue['LAST_UPDATED'] = $datearr[0];
      $dataParamValue['LAST_UPDATED_END'] = $datearr[1];
    }

    if (!empty($this->_request->getParam('expdate'))) {
      $datearr = $this->_request->getParam('expdate');
      $dataParamValue['EXPIRED_DATE'] = $datearr[0];
      $dataParamValue['EXPIRED_DATE_END'] = $datearr[1];
    }

    $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;


//tinggal ubah sesuai filter

      $bin      = html_entity_decode($zf_filter->getEscaped('BIN'));

      //static
      if ($vatype != '0') {
        $vaNumber = html_entity_decode($zf_filter->getEscaped('VA_NUMBER'));
        $ccy = html_entity_decode($zf_filter->getEscaped('CCY'));
        $totalBillAmount = html_entity_decode($zf_filter->getEscaped('TOTAL_BILL_AMOUNT'));
        $paidAmount = html_entity_decode($zf_filter->getEscaped('PAID_AMOUNT'));
        $settledWremark = html_entity_decode($zf_filter->getEscaped('SETTLED_WITH_REMARK'));
        $unsettledAmount = html_entity_decode($zf_filter->getEscaped('UNSETTLED_AMOUNT'));
        $availableCredit = html_entity_decode($zf_filter->getEscaped('AVAILABLE_CREDIT'));
        $inBilling = html_entity_decode($zf_filter->getEscaped('IN_BILLING'));
        $status = html_entity_decode($zf_filter->getEscaped('STATUS'));

        $lastUpdated     = html_entity_decode($zf_filter->getEscaped('LAST_UPDATED'));
        $lastUpdatedEnd  = html_entity_decode($zf_filter->getEscaped('LAST_UPDATED_END'));

        if ($vaNumber) $select->where('CONCAT(T.CUST_BIN, "-", T.VA_NUMBER) LIKE ? ', '%'.$vaNumber.'%');
        if ($totalBillAmount) $select->having('SUM(T.INVOICE_AMOUNT) = ? ', (string)Application_Helper_General::convertDisplayMoney($totalBillAmount));


      }

      //konversi date agar dapat dibandingkan
      $lastUpdated   = (Zend_Date::isDate($lastUpdated, $this->_dateDisplayFormat)) ?
        new Zend_Date($lastUpdated, $this->_dateDisplayFormat) :
        false;

      $lastUpdatedEnd     = (Zend_Date::isDate($lastUpdatedEnd, $this->_dateDisplayFormat)) ?
        new Zend_Date($lastUpdatedEnd, $this->_dateDisplayFormat) :
        false;

      if ($bin)              $select->where('T.CUST_BIN = ? ', (string)$bin);
      if ($status)           $select->where('T.VA_STATUS = ?', (string)$status);
      if ($latestCreatedFrom)    $select->where("DATE(T.UPDATED_DATE) >= DATE(" . $this->_db->quote($lastUpdated->toString($this->_dateDBFormat)) . ")");
      if ($latestCreatedTo)      $select->where("DATE(T.UPDATED_DATE) <= DATE(" . $this->_db->quote($lastUpdatedEnd->toString($this->_dateDBFormat)) . ")");

      $this->view->bin    = $bin;
      $this->view->vatype    = $vatype;
      $this->view->status  = $status;
      $this->view->latestSuggestor  = $latestSuggestor;
      $this->view->latestApprover   = $latestApprover;

      if ($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
      if ($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
      if ($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
      if ($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);



      //utk sorting 
      $select->order($sortBy . ' ' . $sortDir);
      $data = $this->_db->fetchAll($select);
    }
    else{
      $data = array();
    }

    $this->paging($data);
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    $this->view->bin    = $this->_getParam('BIN');
    $this->view->vatype = $vaType;
    $this->view->filterlist = $filterlist;
    $this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
    $this->view->statusDesc = $this->_masterglobalstatus['desc'];
    $this->view->modulename = $this->_request->getModuleName();


    $arr = $data;


    if($this->_request->getParam('print') == 1){

      foreach($arr as $key=>$row)
        {
          
          $statusCode = array_flip($this->_masterglobalstatus['code']);
          $statusDesc = $this->_masterglobalstatus['desc'];

          $arr[$key]['BIN_UPDATED']     = Application_Helper_General::convertDate($row['BIN_UPDATED'],"dd MMM yyyy HH:mm:ss ") . ' (' . $row['BIN_UPDATEDBY'] . ')';
          $arr[$key]['BIN_SUGGESTED']   = Application_Helper_General::convertDate($row['BIN_SUGGESTED'],"dd MMM yyyy HH:mm:ss ") . ' (' . $row['BIN_SUGGESTEDBY'] . ')';
          $arr[$key]['MASTER_ACCOUNT']  = $row['ACCT_NO'].' ('.$row['CCY'].') -'.$row['ACCT_NAME'];
          $arr[$key]['CUST_CITY']       = $row['CUST_BIN'];
          $binType                      = array('1' => 'Cash In', '2' => 'Cash Out');
          $arr[$key]['BIN_TYPE']        = (empty($row['BIN_TYPE']) ? '-' : $binType[$row['BIN_TYPE']] );
          $arr[$key]['CUST_STATUS']     = $statusDesc[$statusCode[$row['CUST_BIN_STATUS']]];
          
        }
        var_dump($data);
        echo "checkprint";
        die;

      $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Bin Setup', 'data_header' => $fields));
    }

    $filterlistdata = array("Filter");
			foreach($dataParamValue as $fil => $val){
					$paramTrx = $fil . " - " . $val;
					array_push($filterlistdata, $paramTrx);
		}

    if($this->_request->getParam('csv')){
      if ($vaType == '0') {
        $listdata = array();
        foreach($data as $key => $val){
            $list = array($val['VA_NUMBER'], $val['VA_NAME'], $val['CCY'], $val['BILL_AMOUNT'], $val['PAID_AMOUNT'], $val['LAST_UPDATED'], $val['EXPIRED_DATE'], $val['STATUS'], $val['DESC'],
            );
            array_push($listdata, $list);
        }
        $fieldprint = array('VA NUMBER', 'VA NAME', 'CCY', 'BILL AMOUNT', 'PAID AMOUNT', 'LAST UPDATED', 'EXPIRED DATE', 'STATUS', 'DESC');
      }
      $filterlistdatax = $this->_request->getParam('data_filter');
			$listable = array_merge_recursive(array($fieldprint), $listdata);
			$this->_helper->download->csv($filterlistdatax, $listable, null, 'eCollection List');
    }

    //insert log
    try {
      $this->_db->beginTransaction();

      Application_Helper_General::writeLog('BNLS', 'View Customer BIN Setup Customer BIN List');

      $this->_db->commit();
    } catch (Exception $e) {
      $this->_db->rollBack();
    }

    if (!empty($dataParamValue)) {
      $this->view->updateStart = $dataParamValue['LAST_UPDATED'];
      $this->view->updateEnd = $dataParamValue['LAST_UPDATED_END'];
      $this->view->expiredStart = $dataParamValue['EXPIRED_DATE'];
      $this->view->expiredEnd = $dataParamValue['EXPIRED_DATE_END'];

      unset($dataParamValue['LAST_UPDATED_END']);
      unset($dataParamValue['EXPIRED_DATE_END']);
      unset($dataParamValue['BIN']);

      foreach ($dataParamValue as $key => $value) {
        $wherecol[] = $key;
        $whereval[] = $value;
      }

      // print_r($whereval);die;
    } else {
      $wherecol = array();
      $whereval = array();
    }

    $this->view->wherecol     = $wherecol;
    $this->view->whereval     = $whereval;
  }

  public function bincheckAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $bin = $this->_getParam('bin');

    $select = $this->_db->select()
        ->from(array('B' => 'M_CUSTOMER_BIN'),array('*'))
        ->where('B.CUST_ID = ?', (string)$this->_custIdLogin)
        ->where('B.CUST_BIN = ?', (string)$bin)
        ->query()->fetchAll();

    $type = $select[0]['VA_TYPE'];

    echo trim($type);

  }
}
