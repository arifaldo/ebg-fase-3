<?php

require_once 'Zend/Controller/Action.php';

class Country_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');
	    $this->setbackURL('/country/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$this->view->report_msg = array();
		if($this->_request->isPost() )
		{
			$filters = array(
			                 'country_code' => array('StringTrim','StripTags','StringToUpper'),
							 'country_name' => array('StringTrim','StripTags','StringToUpper')
							);

			$validators = array(
			                    'country_code' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>2)),
			                                         array('Db_NoRecordExists', array('table' => 'M_COUNTRY', 'field' => 'COUNTRY_CODE')),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 2 chars)'),
																         $this->language->_('Country already existed')
			                                                             )
													),
								'country_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>128)),
													 array('Db_NoRecordExists', array('table' => 'M_COUNTRY', 'field' => 'COUNTRY_NAME')),
													 'messages' => array(
																		 $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 128 chars)'),
																         $this->language->_('Country already existed')
																         )
														)
							   );


			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				$content = array(
								'COUNTRY_CODE' 	 => $zf_filter_input->country_code,
								'COUNTRY_NAME' 	 => $zf_filter_input->country_name
						       );

				try
				{

					//-------- insert --------------
					$this->_db->beginTransaction();

					$this->_db->insert('M_COUNTRY', $content);

					$this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('COAD','Add New Country. Country Code : ['.$zf_filter_input->country_code.'], Country Name : ['.$zf_filter_input->country_name.']');
					$this->view->success = true;
					$this->view->report_msg = array();

					// $this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}

		Application_Helper_General::writeLog('COAD','Add New Country');
	}

}
