<?php

require_once 'Zend/Controller/Action.php';

class Atm_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
	  
	   
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$countrycode = $this->language->_('Country Code');
		$branchname = $this->language->_('Branch Name');
		$branchcode = $this->language->_('ATM Code');
		$branchaddress = $this->language->_('ATM Address');
		$cityname = $this->language->_('City Name');
		$religionname = $this->language->_('Religion Name');
		$contact = $this->language->_('Contact');
		
		
		
	    $fields = array(
						'atm_code'  => array('field' => 'ATM_CODE',
											      'label' => $branchcode,
											      'sortable' => true),
						'atm_address'     => array('field' => 'ATM_ADDRESS',
											      'label' => $branchaddress,
											      'sortable' => true),
            	        'city_name'     => array('field' => 'ATM_CITY',
                                    	            'label' => $cityname,
                                    	            'sortable' => true),
            	        

				      );

   		$filterlist = array("ATM_CODE","ATM_ADDRESS","CITY_NAME");
		
		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','ATM_CODE');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'ATM_CODE'  => array('StringTrim','StripTags'),
                		    'CITY_NAME'  => array('StringTrim','StripTags'),
                		    'ATM_ADDRESS'  => array('StringTrim','StripTags'),
		);

		$dataParam = array("ATM_CODE","CITY_NAME","ATM_ADDRESS");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
		//$filter_lg = $this->_getParam('filter');	
		
		$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_ATM'));


		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf) 
		if($filter == true || $csv || $pdf || $this->_request->getParam('print')) 
		{  
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fatmcode = $zf_filter->getEscaped('ATM_CODE');
			$fatmadd = $zf_filter->getEscaped('ATM_ADDRESS');
			$fatmcity = $zf_filter->getEscaped('CITY_NAME');

	        if($fatmcode) $select->where('UPPER(ATM_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fatmcode).'%'));
	        if($fatmadd) $select->where('UPPER(ATM_ADDRESS) LIKE '.$this->_db->quote('%'.strtoupper($fatmadd).'%'));
	        if($fatmcity) $select->where('UPPER(ATM_CITY) LIKE '.$this->_db->quote('%'.strtoupper($fatmcity).'%'));

			$this->view->fatmcode = $fatmcode;
			$this->view->fatmadd = $fatmadd;
			$this->view->fatmcity = $fatmcity;
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);  
		
		$select = $this->_db->fetchall($select);

		if($csv || $pdf){
			foreach ($select as $key => $value) {
				unset($select[$key]['ATM_ID']);
			}
		}

//         print_r($select);die;
		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
				$this->_helper->download->csv($header,$select,null,'ATM List');
				Application_Helper_General::writeLog('COLS','Download CSV ATM List');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$select,null,'ATM List');
				Application_Helper_General::writeLog('COLS','Download PDF ATM List');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'ATM List', 'data_header' => $fields));
       	}
		else
		{		
				Application_Helper_General::writeLog('COLS','View ATM List');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }

	}

}