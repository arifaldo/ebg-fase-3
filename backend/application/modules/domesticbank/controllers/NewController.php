<?php

require_once 'Zend/Controller/Action.php';
// require_once 'CMD/Beneficiary.php';

class Domesticbank_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');
	    $this->setbackURL('/domesticbank/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}


    	$this->view->report_msg = array();
		if($this->_request->isPost() )
		{
			$filters = array(


							 'clearing_code' => array('StringTrim','StripTags'),
							 'swift_code'    => array('StringTrim','StripTags'),
							 'bank_name'     => array('StringTrim','StripTags'),

							);

			$validators = array(

													'clearing_code'    => array(
																				'NotEmpty',
																					//'Digits',
																					new Zend_Validate_StringLength(array('max'=>7)),
																				// array('Db_NoRecordExists', array('table' => 'M_DOMESTIC_BANK_TABLE', 'field' => 'CLR_CODE')),
																				'messages' => array(
																												$this->language->_('Can not be empty'),
																												$this->language->_('Data too long (max 7 chars)'),
																												// 'Clearing Code Already Exist'
																														)
																				 ),

													'swift_code'      => array(
																				 // 'allowEmpty' => true,
																				 'NotEmpty',
																				new Zend_Validate_StringLength(array('max'=>15)),
																				//array('Db_RecordExists', array('table' => 'M_DOC_PHYSICAL', 'field' => 'FIELD3_VALUE')),
																				'messages' => array(
																										$this->language->_('Can not be empty'),
																													 $this->language->_('Data too long (max 15 chars)'),
																													 )
																				),
			                 			'bank_name' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																         $this->language->_($this->language->_('Can not be empty')),
																         $this->language->_($this->language->_('Data too long (max 35 chars)')),
			                                                             )
													),
								/*'BANK_OFFICE_NAME' => array('allowEmpty' => true,
			                                         new Zend_Validate_StringLength(array('max'=>20)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 20 chars)',
			                                                             )
													),
								'BANK_ADDRESS' => array('allowEmpty' => true,
			                                         new Zend_Validate_StringLength(array('max'=>50)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 50 chars)',
			                                                             )
													),
													 */
								/* 'city'      => array(//'NotEmpty',
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>15)),
													 'messages' => array(
																         'Data too long (max 15 chars)',
																         )
														),
								'CITY'      => array(
													'NotEmpty',
													 // 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>4)),
													 'messages' => array(
																		'Can not be empty',
																         'Data too long (max 4 chars)',
																         )
														),
								'WORKFIELD_CODE'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																		'Can not be empty',
																         'Data too long (max 1 chars)',
																         )
														),
								'PROVINCE_CODE'      => array(
													 // 'allowEmpty' => true,
													 'NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>4)),
													 'messages' => array(
																		'Can not be empty',
																         'Data too long (max 4 chars)',
																         )
														),
								/* 'KBI_CODE'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>3)),
													 'messages' => array(
																         'Data too long (max 3 chars)',
																         )
														),

								'BRANCH_CODE'      => array(
													'NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>4)),
													 'messages' => array(
																			'Can not be empty',
																         'Data too long (max 4 chars)',
																         )
														),
								'BRANCH_NAME' => array(
													'NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>30)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 30 chars)',
			                                                             )
													),
							/* 	'BI_ACCOUNT'      => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>12)),
													 'messages' => array(
																         'Data too long (max 12 chars)',
																         )
														),
								'POP_STATUS_CODE' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														),
								'RES_STATUS_CODE' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														),
								'CLR_STATUS' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														),
								'CORD_STATUS_CODE' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														),
								'BANK_INST_CODE' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														), */

								/* 'ACTIVE_DATE'      => array(
														   'allowEmpty' => true,
														  ) */
							   );


			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				$content = array(


								'CLR_CODE' 	 	 => $zf_filter_input->clearing_code,
								'SWIFT_CODE'	 => $zf_filter_input->swift_code,
								'BANK_NAME' 	 => $zf_filter_input->bank_name
// 								'CITY_CODE' 	 => $zf_filter_input->CITY,
// 								'WORKFIELD_CODE' => $zf_filter_input->WORKFIELD_CODE,
// 								'PROVINCE_CODE'  => $zf_filter_input->PROVINCE_CODE,
								// 'KBI_CODE' 		 => $zf_filter_input->KBI_CODE,
// 								'BRANCH_CODE' 	 => $zf_filter_input->BRANCH_CODE,
// 								'BRANCH_NAME' 	 => $zf_filter_input->BRANCH_NAME,
								/* 'BI_ACCOUNT' 	 => $zf_filter_input->BI_ACCOUNT,
								'CLR_STATUS' 	 => $zf_filter_input->CLR_STATUS,
								'POP_STATUS_CODE'=> $zf_filter_input->POP_STATUS_CODE,
								'RES_STATUS_CODE'=> $zf_filter_input->RES_STATUS_CODE,
								'CORD_STATUS_CODE'=> $zf_filter_input->CORD_STATUS_CODE,
								'BANK_INST_CODE' => $zf_filter_input->BANK_INST_CODE,
								'ACTIVE_DATE'	 => Application_Helper_General::convertDate($zf_filter_input->ACTIVE_DATE, $this->_dateDBFormat, $this->_dateDisplayFormat), */
								//'BANK_ISMASTER' => 0,
						       );
				//$content['BANK_ISDISPLAYED'] = (!empty($zf_filter_input->swift_code)) ? 1 : 0;


				try
				{

					//-------- insert --------------
					$this->_db->beginTransaction();

					$this->_db->insert('M_DOMESTIC_BANK_TABLE',$content);

					$this->_db->commit();
					// $id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('DBAD','Add Domestic Bank. Bank ID : ['.$zf_filter_input->swift_code.'], Bank Name : ['.$zf_filter_input->bank_name.']');
					$this->view->success = true;
					$this->view->report_msg = array();

					// $this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					// var_dump($e);
					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}

		Application_Helper_General::writeLog('DBAD','Viewing Add Domestic Bank');
	}

}
