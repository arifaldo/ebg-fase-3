<?php

require_once 'Zend/Controller/Action.php';

class echanrequest_ApprovalonlineregController extends Application_Main
{
    
    private $_suggestionStatusCP = array();
    private $_userAuth = array();
    private $_typetitle = array();
    
    
    protected $_moduleDB  = 'WFA';
    
    public function initController(){
        $this->_suggestionStatusCP = array(	"" => '--'.$this->language->_('Any Value')."--",
        "UR"=> $this->language->_('Unread Suggestion') ,
        "RS"=> $this->language->_('Read Suggestion'),
        "RR"=> $this->language->_('Request Repaired'),
        "RP"=> $this->language->_('Repaired Suggestion'),
    );
}

public function indexAction()
{
    $this->_helper->layout()->setLayout('newlayout');
    
    $fields = array(
        
        'regno' => array(
            'field'    => '#',
            'label'    => $this->language->_('Registration Number'),
        ),
        'companyName' => array(
            'field' => '#',
            'label' => $this->language->_('Company Name'),
        ),
        'contactPerson' => array(
            'field' => '#',
            'label' => $this->language->_('Contact Person'),
        ),
        
        'contactNumber' => array(
            'field' => '#',
            'label' => $this->language->_('Contact Number'),
        ),
        'lastUpdate' => array(
            'field' => '#',
            'label' => $this->language->_('Last Update'),
        ),
        'suggestionStatus' => array(
            'field' => '#',
            'label' => $this->language->_('Suggestion Status'),
        ),
        'regStatus' => array(
            'field' => '#',
            'label' => $this->language->_('Registration Status'),
            )
        );

        $this->view->fields = $fields;
        
        $filterlist = array(
            "CHANGES_ID" => 'Registration Number',
            'CUST_NAME' => "Customer Name",
            'LAST_UPDATED' => "Last Update",
            "SUGGEST_STATUS" => "Suggestion Status",
            "REGIS_STATUS" => "Registration Status",
        );
        
        $this->view->filterlist = $filterlist;
        
        $page    = $this->_getParam('page');
        
        $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
        $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        
        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';
        
        $this->view->currentPage = $page;
        $this->view->sortBy      = $sortBy;
        $this->view->sortDir     = $sortDir;
        
        $this->paging([]);
        
    }

    public function detailAction(){
        $this->_helper->layout()->setLayout('newlayout');
    }
}