<?php

require_once 'Zend/Controller/Action.php';

class notificationsetting_IndexController extends Application_Main
{

	//===============================================================================================================
	protected $_moduleDB  = 'WFA';

	public function indexAction()
	{
		//notificationsetting.index.index = "TXNT"
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege('TXNT')) { 
			$this->_redirect('/authorizationacl/index/index');
		}

		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);

		$setting = new Settings();  
		$system_type = $setting->getSetting('system_type');

		$arrTraType     = array_combine($this->_paymenttype["code"], $this->_paymenttype["desc"]);

		if ($system_type == 1) {
			
			unset($arrTraType['21']);
			unset($arrTraType['19']);
	        unset($arrTraType['23']);

		}else if ($system_type == 2) {
			unset($arrTraType['1']);
			unset($arrTraType['2']);
			unset($arrTraType['3']);
			unset($arrTraType['4']);
			unset($arrTraType['5']);
			unset($arrTraType['6']);
			unset($arrTraType['7']);
			unset($arrTraType['8']);
			unset($arrTraType['11']);
			unset($arrTraType['12']);
			unset($arrTraType['14']);
			unset($arrTraType['15']);
			unset($arrTraType['16']);
			unset($arrTraType['17']);
			unset($arrTraType['18']);
			unset($arrTraType['20']);
			unset($arrTraType['22']);
			unset($arrTraType['99']);
			// $arrTraType['15'] = 'Sweep Transfer';
			$arrTraType['19'] = 'Cash Pooling Same Bank';
	        $arrTraType['23'] = 'Cash Pooling Other Bank';
			// $arrTraType['99'] = 'Suspect Transaction';
			// $arrTraType['99'] = 'Auto Top Up';
		}
		//var_dump($arrTraType);die;
		$arrTraTypenew['38'] = 'BG';
		$this->view->optpaytypeRaw = $arrTraTypenew;

		$selectTrx	= $this->_db->select()
			->from(array('M_EMAIL_SETTING'), array('*'))
			->where('CUST_ID = ?', $this->_custIdLogin);

		$pslipTrxData = $this->_db->fetchAll($selectTrx);

		foreach ($pslipTrxData as $row) {
			$email[$row['PS_TYPE']] = $row['EMAIL'];
			$status[$row['PS_TYPE']] = $row['STATUS'];
		}

		$this->view->email  = $email;
		$this->view->status = $status;

		$selectemail	= $this->_db->select()
			->from(array('M_EMAIL_SETTING'), array('*'));

		$conf = Zend_Registry::get('config');
		$paymentType = $conf['payment']['type'];
		$paymentTypeFlip = array_flip($paymentType['code']);

		$this->view->paymentType = $paymentType;
		$this->view->paymentTypeFlip = $paymentTypeFlip;

		$submit = $this->_getParam('submit');
    	$reset  = $this->_getParam('reset');

		if($submit){

			foreach ($arrTraType as $key => $value) {
				$listemail  = $this->_request->getParam('email'.$key);
				$listnotif  = $this->_request->getParam('notif'.$key);
				$listpstype = $this->_request->getParam('ps_type'.$key);

				if (!empty($listemail)) {

					// if(empty($listemail)){
					// $error_msg[] = $this->language->_('Error').': '.$this->language->_('Email cannot be left blank.').'';
					// $this->view->error 		= true;
					// $this->view->report_msg	= $this->displayError($error_msg);
					// }else if (empty($listnotif)) {
					// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Notification cannot be left blank.').'';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

					$select	= $this->_db->select()
					->from(array('M_EMAIL'), array('*'))
					->where('CUST_ID = ?', $this->_custIdLogin);

					$pslipTrxData1 = $this->_db->fetchAll($select);

					if (empty($pslipTrxData1)) {
						$data = array(
							'CUST_ID' 		=> $this->_custIdLogin,
							'UPDATED' 		=> new Zend_Db_Expr("now()"),
							'UPDATEDBY' 	=> $this->_userIdLogin
						);

						$this->_db->insert('M_EMAIL', $data);
					} else {

						$data = array(
							'UPDATED' 		=> new Zend_Db_Expr("now()"),
							'UPDATEDBY' 	=> $this->_userIdLogin,
						);

						$where = array('CUST_ID = ?' => $this->_custIdLogin);
						$this->_db->update('M_EMAIL', $data, $where);
					}


					if ($listnotif == 0) {
						$status = 0;
					}else{
						$status = 1;
					}

					$count_data	= $this->_db->select()
					->from(array('M_EMAIL_SETTING'), array('count(*) AS count'))
					->where('PS_TYPE = ?', $listpstype)
					->where('CUST_ID = ?', $this->_custIdLogin);

					$count_data = $this->_db->fetchRow($count_data);

					if($count_data['count']>0){

						$data1 = array(
								'CUST_ID' 		=> $this->_custIdLogin,
								'EMAIL' 		=> $listemail,
								'STATUS'		=> $status
						);

						$where = array(
										'PS_TYPE = ?' => $listpstype,
										'CUST_ID = ?' => $this->_custIdLogin);
						$this->_db->update('M_EMAIL_SETTING', $data1, $where);

					}else{

						$data1 = array(
							'CUST_ID' 		=> $this->_custIdLogin,
							'PS_TYPE' 		=> $listpstype,
							'EMAIL' 		=> $listemail,
							'STATUS'		=> $status
						);

						$this->_db->insert('M_EMAIL_SETTING', $data1);

					}

				}else if(empty($listemail)){

					$where = array(
							'PS_TYPE = ?' => $listpstype,
							'CUST_ID = ?' => $this->_custIdLogin);
  					$this->_db->delete('M_EMAIL_SETTING',$where);

				}

			}

			$this->_redirect('/notificationsetting/index');
			
		}

	}

	public function changenotifAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

		$value = $this->_getParam('value');
		$type = $this->_getParam('type');

        try{

			$data1 = array(
				'STATUS'		=> $value
			);

        	$where = array(
				'PS_TYPE = ?' => $type,
				'CUST_ID = ?' => $this->_custIdLogin
			);

			// $updateArr = array('USER_PENDING_NOTIF' => $value);
			// $this->_db->update('M_USER',$updateArr,$where);
			$this->_db->update('M_EMAIL_SETTING', $data1, $where);
			echo 'success';
        }
        catch(Exception $e){
        	echo 'failed';
        }
	}
}