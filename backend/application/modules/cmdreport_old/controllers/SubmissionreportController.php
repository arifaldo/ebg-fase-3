<?php


require_once 'Zend/Controller/Action.php';


class cmdreport_SubmissionreportController extends Application_Main 
{
	
	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		$select = $this->_db->select()
							->distinct()
							->from(
										array('A' => 'M_CUSTOMER'),
										array('CUST_ID','CUST_NAME'))
							->order('CUST_ID ASC')
							->query() 
							->fetchAll();
// 		Zend_Debug::dump($select); die;
		
		//array File is Deleted
		$arrIsDeleted = array( '0'=>$this->language->_('No'), '1'=>$this->language->_('Yes'));
		
		$this->view->companycodeArr = $select;
		$this->view->arrIsDeleted 	= $arrIsDeleted;
		
		$arr = null;
		$viewFilter = null;
		
		$fields = array	(
							'Customer ID'  			=> array	(
								'field' => 'CUST_ID',
								'label' => $this->language->_('Company'),
								'sortable' => true
							),
// 							'CustomerName'  			=> array	(
// 								'field' => 'CUST_NAME',
// 								'label' => 'Company Name',
// 								'sortable' => true
// 							),
							'FileName'  			=> array	(
								'field' => 'FILE_NAME',
								'label' => $this->language->_('File Name - Description'),
								'sortable' => true
							),
// 							'FileDesc'  			=> array	(
// 								'field' => 'FILE_NAME',
// 								'label' => 'File Description',
// 								'sortable' => true
// 							),
							'Uploaded By'  			=> array	(
								'field' => 'USER_LOGIN',
								'label' => $this->language->_('Uploaded By'),
								'sortable' => true
							),	
							'Downloaded Time'  			=> array	(
								'field' => 'FILE_DOWNLOADED',
								'label' => $this->language->_('Downloaded Time(s)'),
								'sortable' => true
							),	
							'Last Downloaded'  			=> array	(
								'field' => 'FILE_DOWNLOADEDBY',
								'label' => $this->language->_('Last Downloaded By'),
								'sortable' => true
							),	
							// 'Upload Date and Time'  => array	(
							// 	'field' => 'FILE_UPLOADED_TIME',
							// 	'label' => $this->language->_('Uploaded Date'),
							// 	'sortable' => true
							// 									),	
							'File Is Deleted'  			=> array	(
								'field' => 'FILE_DELETED',
								'label' => $this->language->_('File Deleted'),
								'sortable' => true
							),
						);
		$this->view->fields = $fields;

		$filterlist = array('File Deleted','PS_UPLOADED','COMP_ID','COMP_NAME','UPLOADED_BY');
		
		$this->view->filterlist = $filterlist;
		
		$page    = $this->_getParam('page');		
        $sortBy  = $this->_getParam('sortby','');
        $sortDir = $this->_getParam('sortdir','asc');
		
       	$page 		= (Zend_Validate::is($page,'Digits'))? $page : 1;
        $sortBy 	= (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
				
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'UPLOADED_BY' 	=> array('StringTrim','StripTags'),
							'COMP_ID'  	=> array('StringTrim','StripTags','StringToUpper'),
							'COMP_NAME'  	=> array('StringTrim','StripTags','StringToUpper'),
							'PS_UPLOADED'  	=> array('StringTrim','StripTags'),
							'PS_UPLOADED_END'  	=> array('StringTrim','StripTags'),
							'File Deleted'   	=> array('StringTrim','StripTags'),
							'pdf' 	  		=> array('StringTrim','StripTags'),
							'csv' 	  		=> array('StringTrim','StripTags'),
		);
		
			// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','File Deleted','COMP_NAME','UPLOADED_BY');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_UPLOADED"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('updatedate'))){
				$updatearr = $this->_request->getParam('updatedate');
					$dataParamValue['PS_UPLOADED'] = $updatearr[0];
					$dataParamValue['PS_UPLOADED_END'] = $updatearr[1];
			}
		
		// The default is set so all fields allow an empty string
		$options = array('allowEmpty' => true);
		$validators = array(
								'filter'		=> array(),
								'UPLOADED_BY'		=> array(),
								'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($select)))),
								'COMP_NAME' 	=> array(array('InArray', array('haystack' => array_keys($select)))),
								'PS_UPLOADED'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
								'PS_UPLOADED_END'    	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
								'File Deleted' 	=> array(array('InArray', array('haystack' => array_keys($arrIsDeleted)))),
								'pdf'			=> array(),
								'csv'			=> array(),
						
							);
											 
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		//$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$filter 	= $this->_request->getParam('filter');
		$filter_clear 	= $this->_getParam('clearfilter');
		
		$csv 		= $this->_request->getParam('csv');
		$pdf 		= $zf_filter->getEscaped('pdf');

		$select =	$this->_db->select()
								->from	(
											array('TFS'=>'T_FILE_SUBMIT'),
											array(
													'FILE_ID'			=>'TFS.FILE_ID',
													'FILE_NAME'			=>'TFS.FILE_NAME',
													'FILE_DESCRIPTION'	=>'TFS.FILE_DESCRIPTION',
													'USER_LOGIN'		=>'TFS.FILE_UPLOADEDBY',
													'CUST_ID'			=>'TFS.CUST_ID',
													'FILE_UPLOADED_TIME'=>'TFS.FILE_UPLOADED_TIME',
													'FILE_DOWNLOADED'	=>'TFS.FILE_DOWNLOADED',
													'FILE_DOWNLOADEDBY'	=>'TFS.FILE_DOWNLOADEDBY',
													'FILE_DELETED'		=>'TFS.FILE_DELETED',
													'FILE_DELETEDBY'	=>'TFS.FILE_DELETEDBY',
												)
										)
								->joinLeft	(
												array('MC' => 'M_CUSTOMER'),
												'TFS.CUST_ID = MC.CUST_ID',
												array('CUST_NAME')
											);

		if($filter == null)
		{
			$now = Zend_Date::now()->toString("dd/MM/yyyy");

			$DATE_START = $this->view->DATE_START = $now;
			$DATE_END  = $this->view->DATE_END = $now;
		}
		
		if($filter_clear == TRUE)
		{
			$now = '';

			$DATE_START = $this->view->DATE_START = $now;
			$DATE_END  = $this->view->DATE_END = $now;
		}
		
		if($filter == TRUE)
		{
			if($this->_getParam('PS_UPLOADED'))
			{
				
				$DATE_START    	= $this->_getParam('PS_UPLOADED');
				$this->view->DATE_START = $DATE_START;
			}
			if($this->_getParam('PS_UPLOADED_END'))
			{
				$DATE_END		= $this->_getParam('PS_UPLOADED_END');
				$this->view->DATE_END = $DATE_END;
			}
			
//			if(!empty($DATE_START))
//			{
//				$this->view->DATE_START = $DATE_START;		
//				$FormatDate = new Zend_Date($DATE_START, $this->_dateDisplayFormat);
//				$DATE_START  = $FormatDate->toString($this->_dateDBFormat);
//			}
//			
//			if(!empty($DATE_END))
//			{
//				$this->view->DATE_END = $DATE_END;				
//				$FormatDate = new Zend_Date($DATE_END, $this->_dateDisplayFormat);
//				$DATE_END  = $FormatDate->toString($this->_dateDBFormat);
//			}
			
			$SEARCH_TEXT   	= $zf_filter->getEscaped('SEARCH_TEXT'); //$this->_getParam('SEARCH_TEXT');
			$uplodedBy   	= $zf_filter->getEscaped('UPLOADED_BY'); //$this->_getParam('uplodedBy');
			$companycode    = $zf_filter->getEscaped('COMP_ID'); //$this->_getParam('companycode');
			$companyname    = $zf_filter->getEscaped('COMP_NAME'); //$this->_getParam('companyname');
			$fISDELETED		= $zf_filter->getEscaped('File Deleted'); //$this->_getParam('ISDELETED');
			
			if($SEARCH_TEXT)
			{
				$select->where("UPPER(TFS.FILE_NAME) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%'));
				$this->view->SEARCH_TEXT = $SEARCH_TEXT;
			}
			
			if($uplodedBy)
			{
				$select->where("UPPER(TFS.FILE_UPLOADEDBY) LIKE ".$this->_db->quote('%'.$uplodedBy.'%'));
				$this->view->uplodedBy = $uplodedBy;
			}
			
			if($companycode)
			{
				$select->where("UPPER(TFS.CUST_ID) = ".$this->_db->quote($companycode));
				$this->view->companycode = $companycode;
			}
			
			if($companyname)
			{
				$select->where("UPPER(MC.CUST_NAME) = ".$this->_db->quote($companyname));
				$this->view->companyname = $companyname;
			}
			
			if($fISDELETED != "")
			{
				$select->where('TFS.FILE_DELETED = '.$this->_db->quote($fISDELETED));
				$this->view->ISDELETED 	= $fISDELETED;
			}
		}

		//if($filter === '' || $filter == 'Set Filter')
		if($filter === '')
		{
			if($zf_filter->getEscaped('PS_UPLOADED'))
			{
				$DATE_START    	= $zf_filter->getEscaped('PS_UPLOADED'); //$this->_getParam('DATE_START');
			}
			if($zf_filter->getEscaped('PS_UPLOADED_END'))
			{	
				$DATE_END		= $zf_filter->getEscaped('PS_UPLOADED_END'); //$this->_getParam('DATE_END');
			}
			
			if(!empty($DATE_START))
			{
				$FormatDate = new Zend_Date($DATE_START, $this->_dateDisplayFormat);
				$DATE_START  = $FormatDate->toString($this->_dateDBFormat);	
				$select->where('DATE(TFS.FILE_UPLOADED_TIME) >= '.$this->_db->quote($DATE_START));
			}
			
			if(!empty($DATE_END))
			{
				$FormatDate = new Zend_Date($DATE_END, $this->_dateDisplayFormat);
				$DATE_END  = $FormatDate->toString($this->_dateDBFormat);	
				$select->where('DATE(TFS.FILE_UPLOADED_TIME) <= '.$this->_db->quote($DATE_END));
			}
		}
		
		$select->order($sortBy.' '.$sortDir);
		$this->paging($select);
		
		if($csv || $pdf || $this->_request->getParam('print'))
		{	
			$arr = $this->_db->fetchAll($select);
			$i=0;
			foreach($arr as $row)
			{
				// if($csv)				
				// {
				$data[$i]['NO'] = $i+1;

				// }
				$data[$i]['CUST_ID'] = $row['CUST_NAME'].' ('.$row['CUST_ID'].')';
				$data[$i]['FILE_NAME'] = $row['FILE_NAME'].' - '.$row['FILE_DESCRIPTION'];
				$data[$i]['USER_LOGIN'] = $row['USER_LOGIN'].' '.Application_Helper_General::convertDate($row['FILE_UPLOADED_TIME'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$data[$i]['FILE_DOWNLOADED'] = $row['FILE_DOWNLOADED'];
				$data[$i]['FILE_DOWNLOADEDBY'] = $row['FILE_DOWNLOADEDBY'];
			
				$status = "No";
				if($row['FILE_DELETED']==1)
				{
					$status = "Yes (by ".$row['FILE_DELETEDBY'].")";
				}

				$data[$i]['FILE_DELETED'] = $status;
				$i++;
			}

			if($csv)
			{
				
				$header = array ('0'=>'No');
			
				$header = array_merge( $header,Application_Helper_Array::simpleArray($fields, "label"));
			
				$this->_helper->download->csv($header,$data,null,$this->language->_('Submission Report'));  
				Application_Helper_General::writeLog('SPSB','Download CSV File Sharing Report');
			}
			else if($pdf)
			{
				$header = Application_Helper_Array::simpleArray($fields, "label");
				$this->_helper->download->pdf($header,$data,null,$this->language->_('Submission Report'));  
				Application_Helper_General::writeLog('SPSB','Download PDF File Sharing Report');
			}
			elseif($this->_request->getParam('print') == 1){
				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Submission Report', 'data_header' => $fields));
			}
			else
			{
				//Application_Helper_General::writeLog('SPSB','View Submission Report');
			}
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('SPSB','View File Sharing Report');

			if(!empty($dataParamValue)){
	    		$this->view->updatedStart = $dataParamValue['PS_UPLOADED'];
	    		$this->view->updateEnd = $dataParamValue['PS_UPLOADED_END'];

			    unset($dataParamValue['PS_UPLOADED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }

		}
	}	
}
