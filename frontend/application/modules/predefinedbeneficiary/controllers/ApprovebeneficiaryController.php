<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';

class predefinedbeneficiary_ApprovebeneficiaryController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{	
		$this->_helper->layout()->setLayout('newlayout');

		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => $this->language->_('Beneficiary Name'),
											   'sortable' => true),
						'bank_name'  => array('field' => 'BANK_NAME',
											   'label' => $this->language->_('Bank Name'),
											   'sortable' => true),
						// 'email'  => array('field' => 'BENEFICIARY_EMAIL',
						// 					   'label' => $this->language->_('Email Address'),
						// 					   'sortable' => true),
						// 'ccy'   => array('field'    => 'CURR_CODE',
						// 					  'label'    => $this->language->_('CCY'),
						// 					  'sortable' => true),
						// 'checked'   => array('field'    => 'BENEFICIARY_BANKSTATUS_disp',
						// 					  'label'    => $this->language->_('Checked by Bank'),
						// 					  'sortable' => true),
						'type'   => array('field'    => 'BENEFICIARY_TYPE_disp',
											  'label'    => $this->language->_('Beneficiary Type'),
											  'sortable' => true),
						'act'   => array('field'    => 'ACTION_disp',
											  'label'    => $this->language->_('Sugestion Type'),
											  'sortable' => true),
						'name_cek'  => array('field' => 'BENEFICIARY_NAME_CEK',
											   'label' => $this->language->_('Name Checking*'),
											   'sortable' => true),
				);

		$filterlist = array('BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BENEFICIARY_TYPE');
		
		$this->view->filterlist = $filterlist;


		$filterArr = array(
								'filter' 				=> array('StripTags','StringTrim'),
								'BENEFICIARY_ACCOUNT' 		=> array('StripTags','StringTrim','StringToUpper'),
								'BENEFICIARY_NAME'  	=> array('StripTags','StringTrim','StringToUpper'),
								'BENEFICIARY_TYPE' 		=> array('StripTags'),
							);

		$dataParam = array('BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BENEFICIARY_TYPE');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			 
			 
			if(!empty($this->_request->getParam('whereco'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('whereco') as $key => $value) {
						if($dtParam==$value){
							if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
							
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		//var_dump($dataParamValue);

		$options = array('allowEmpty' => true);
		$validators = array(
			'BENEFICIARY_ACCOUNT'		=> array(),
			'BENEFICIARY_NAME' 		=> array(),
			'BENEFICIARY_TYPE' 		=> array(),
		);

		$zf_filter = new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
		// $filter = $zf_filter->getEscaped('filter');
		$filter = $this->_request->getParam('filter');
		
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$optoverride = array(
							'1' => 'Y', 
							'2' => 'N');
        $this->view->optoverride = $optoverride;
		
		/*$select = $this->_db->select()
					   ->from('M_BENEFICIARY',array('BENEFICIARY_ID','BENEFICIARY_ALIAS','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BANK_NAME','BENEFICIARY_EMAIL','CURR_CODE','BENEFICIARY_BANKSTATUS' => '(CASE BENEFICIARY_BANKSTATUS WHEN 0 THEN \'No\' WHEN 1 THEN \'Yes\' WHEN 2 THEN \'Failed\' END)','BENEFICIARY_ISREQUEST_DELETE','BENEFICIARY_ISAPPROVE','ACTION' => '(CASE WHEN BENEFICIARY_ISREQUEST_DELETE = 1 THEN \'Delete\' WHEN BENEFICIARY_ISAPPROVE = 0 THEN \'Add\' END)')); 
		$select->where("BENEFICIARY_ISREQUEST_DELETE = 1 OR BENEFICIARY_ISAPPROVE = 0");
		$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));*/
		
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$select->where("BENEFICIARY_ISREQUEST_DELETE = 1 OR BENEFICIARY_ISAPPROVE = 0");
	    
		// if($filter==TRUE)
		// {
		if($zf_filter->getEscaped('BENEFICIARY_ACCOUNT'))
		{
			$fAcct = $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');
			// $select->where('B.BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.$beneficiaryAcct.'%'));
			$fAcctArr = explode(',', $fAcct);
			$select->where("B.BENEFICIARY_ACCOUNT  in (?)",$fAcctArr );
			$this->view->beneficiaryAcct = $beneficiaryAcct;
		}
		if($zf_filter->getEscaped('BENEFICIARY_NAME'))
		{
			$fName = $zf_filter->getEscaped('BENEFICIARY_NAME');
			// $select->where('B.BENEFICIARY_NAME LIKE '.$this->_db->quote('%'.$beneficiaryAcctName.'%'));
			$fNameArr = explode(',', $fName);
			$select->where("B.BENEFICIARY_NAME  in (?)",$fNameArr );
			$this->view->beneficiaryAcctName = $beneficiaryAcctName;
		}
		if($zf_filter->getEscaped('BENEFICIARY_TYPE'))
		{
			$fType = $zf_filter->getEscaped('BENEFICIARY_TYPE');
			// $select->where('B.BENEFICIARY_TYPE = '.$this->_db->quote($beneficiarytype));
			$fTypearr = explode(',', $fType);
			$select->where("B.BENEFICIARY_TYPE  in (?)",$fTypearr );
			$this->view->beneficiarytype = $beneficiarytype;
			
		}
		// }
		
		$select->order($sortBy.' '.$sortDir);   
		// die($select);
		$this->paging($select);
		$this->view->fields = $fields;
		
		if($this->_request->getParam('print') == 1){
			$dataSQL = $this->_db->fetchAll($select);
			$i = 0; 
			foreach ($dataSQL as $row){
				$dataSQL[$i++]['BANK_NAME'] = $row['BANK_NAME']?$row['BANK_NAME']:'In House';
			}
			unset($fields['action']);
                $this->_forward('print', 'index', 'widget', array('data_content' => $dataSQL, 'data_caption' => 'Approve Destination Account', 'data_header' => $fields));
        }
		



		if($this->_request->isPost() && !$filter)
		{
			
			//Zend_Debug::Dump($this->getRequest()->getParams()); //die;
			$Beneficiary = new Beneficiary();
			$param = $this->getRequest()->getParams();
			$benef_id = $this->getRequest()->getParam('benef_id');
			$override = $this->getRequest()->getParam('override');
			$act = $this->getRequest()->getParam('act');
			//$submit = $this->getRequest()->getParam('submit');
			$submit_reject = $this->getRequest()->getParam('submit_reject');
			$submit_approve = $this->getRequest()->getParam('submit_approve');

			
			
			if(!array_key_exists('1',array_flip($benef_id)))
			{				
				$this->view->error = true;
				$this->view->report_msg = 'Error: Please checked selection.';
			}
			else
			{
				foreach($benef_id as $key => $value)
				{
					if($value=='0'){
						unset($benef_id[$key]);
						unset($override[$key]);
					}
				}
				//Zend_Debug::Dump($benef_id);die;
				//if($submit=='Approve')
				if($submit_approve==TRUE)
				{					
					//Zend_Debug::Dump($benef_id);die;
					/*$action = "approve";
					$params['benef_id'] = $benef_id;
					$params['varApp'] = 'pageindex';*/					
					//$this->_forward($action, $this->_request->getControllerName(), $this->_request->getModuleName(), $params);
					
					$content['benef_id'] = $benef_id;
					$content['override'] = $override;
					$content['varApp'] = 'pageindex';
					$content['act'] = $act;
					
					//echo $content['benef_id']; die;
					// Zend_Debug::dump($content,"content");
					// die;


					
					$sessionNamespace = new Zend_Session_Namespace('confirmBenef');
					$sessionNamespace->content = $content;
					
					$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/approve/');
					
															
					/*foreach($benef_id as $key => $value)
					{
						if($act[$key]=='Delete') $Beneficiary->delete($key);
						else if($act[$key]=='Add') $Beneficiary->approve($key);
					}*/
				}
				//else if($submit=='Reject')
				else if($submit_reject==TRUE)
				{
					foreach($benef_id as $key => $value)
					{
						if($act[$key]=='Delete') $Beneficiary->rejectDelete($key);
						else if($act[$key]=='Add') $Beneficiary->delete($key);
					}
					//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/approve/');
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				
				/*$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
				$this->_redirect('/notification/success');*/
			}
		}
		
		if (!empty($dataParamValue)) {
			// $this->view->createdStart = $dataParamValue['PS_CREATED'];
			// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
		//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
		//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

			// unset($dataParamValue['PS_CREATED_END']);
			// unset($dataParamValue['PS_EFDATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',',$value);
							if(!empty($duparr)){
								
								foreach($duparr as $ss => $vs){
									$wherecol[]	= $key;
									$whereval[] = $vs;
								}
							}else{
									$wherecol[]	= $key;
									$whereval[] = $value;
							}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
			 
		}
		// if(!empty($this->_request->getParam('wherecol'))){
		// 		$this->view->wherecol			= $this->_request->getParam('wherecol');
		// 	}

		// 	if(!empty($this->_request->getParam('whereopt'))){
		// 		$this->view->whereopt			= $this->_request->getParam('whereopt');
		// 	}

		// 	if(!empty($this->_request->getParam('whereval'))){
		// 		$this->view->whereval			= $this->_request->getParam('whereval');
		// 	}
		Application_Helper_General::writeLog('BAPA','Viewing Approve Beneficiary');
	}
	
	public function approveAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$Settings = new Settings();
		
		$this->view->backURL = (!empty($sessionURL->URL)) ?
									   $sessionURL->URL : '/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index';
		
		$sessionNamespace = new Zend_Session_Namespace('confirmBenef');
		$data = $sessionNamespace->content;
		//Zend_Debug::dump($data['act']); die;	
		$benefIdarr = $data['benef_id'];
		$act = $data['act'];
		$override = $data['override'];
		$this->view->overide = $data['override'];
		 //Zend_Debug::dump($override); 
		
		$av 		= $this->_getAllParams();
		//echo '<pre>';
		//var_dump($data);//die;
		//$benefIdarr = $this->_getParam('benef_id');	
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		 			
		$benefIdarr = $data['benef_id'];// 			
		$process 	= $this->_getParam('process');
		
		
		$benefna = array();
		foreach($benefIdarr as $key => $value)
		{
			if($value=='0') unset($benefIdarr[$key]);			
		}
		
		
		
		
		//validasi token -- begin
		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
		
		$data2 					= $this->_db->fetchRow($select3);
		$tokenIdUser 			= $data2['TOKEN_ID'];
		$tokenType 				= $data2['TOKEN_TYPE'];
		$usermobilephone 		= $data2['USER_MOBILE_PHONE'];
		$this->view->tokentype 	= $data2['TOKEN_TYPE'];
		
		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();
		$this->view->challengeCode		= $challengeCode;
		
		$challengeCodeSub = substr($challengeCode, 0,2);
		$this->view->challengeCodeReq = $challengeCodeSub;
		//validasi token -- end
		
		
		$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];
			// var_dump($tokenIdUser);
			// var_dump($tokendata);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			// var_dump($tokendata);
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
				
				$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
			// echo $selectQuery;
			$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

			// var_dump($usergoogleAuth);die; 
			if (!empty($usergoogleAuth)) {
				$this->view->googleauth = true;
				//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
				$settingObj = new Settings();
				$maxtoken = $settingObj->getSetting("max_failed_token");
				$this->view->tokenfail = (int)$maxtoken-1;
				if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
					//die;  
					$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
					
					
					
					$tokenfail = (int)$maxtoken - ((int)$usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
					$this->view->tokenfail = $tokenfail;
				}
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}
			else{
				$this->view->nogoauth = '1';
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}
			}
		
		//$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
		//$challengeCode 	= $Token->generateChallengeCode();
		
		$hasPriviApprovePayment = $this->view->hasPrivilege('PAPV');
		$hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');

		/*if ($hasPriviApprovePayment == false)
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to approve payment.");
			$this->_redirect('/paymentworkflow/waitingforapproval/index');
		}*/
		
		if (empty($benefIdarr))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error: Please select payment ref#').".");
			$this->_redirect('/predefinedbeneficiary/approvebeneficiary/index');
		}

		////////////////////////////////////////
		
		
		
				
		$headers = array("acbenef" 		=> $this->language->_('Beneficiary Account'),
						 "acbenef_name" => $this->language->_('Beneficiary Name'),
						 "bank_name" 	=> $this->language->_('Bank Name'),
						 "acbenefType"		=> $this->language->_('Beneficiary Type'),						 
						 "type" 			=> $this->language->_('Sugestion Type'),
						 "name_cek" 		=> $this->language->_('Name Checking*'),
						 "overide" 		=> $this->language->_('Override'),						 
						);		
						
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','BENEFICIARY_ACCOUNT');
		$sortDir = $this->_getParam('sortdir','asc');
		
		foreach($benefIdarr as $k => $v)
		{
			array_push($benefna, $k);
		}
		
		
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$select->where("BENEFICIARY_ISREQUEST_DELETE = 1 OR BENEFICIARY_ISAPPROVE = 0");
		$select->where("B.BENEFICIARY_ID IN (?)", $benefna);
		$select->order($sortBy.' '.$sortDir);  
		
		$this->view->headers 		= $headers;
		$this->paging($select);
		//$this->view->fields = $fields;
		
		///////////////////////////////////////
		//$varApp = "pageconfirm";
		if ($this->_request->isPost())
		{
			//validasi token -- begin
			$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2')	, "challengeCodeReq2");
			$challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq')	, "challengeCodeReq");

			$inputtoken1 		= $this->_getParam('inputtoken1');
			$inputtoken2 		= $this->_getParam('inputtoken2');
			$inputtoken3 		= $this->_getParam('inputtoken3');
			$inputtoken4 		= $this->_getParam('inputtoken4');
			$inputtoken5 		= $this->_getParam('inputtoken5');
			$inputtoken6 		= $this->_getParam('inputtoken6');

			$responseCodeReq	= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;
			
			$responseCodeReq 	= $filter->filter($responseCodeReq	, "responseCodeReq");
			//$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);
			$this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);
			//validasi token -- end
			
			//validasi hard token page 1 -- begin
			$message = $Settings->getSetting('token_confirm_message');
			$trans = array("[Response_Code]" => $responseCodeReq);
			$message = strtr($message, $trans);
			
		/*	if($tokenType == '1'){ //sms token
				if(empty($responseCodeReq)){
					$resultToken = FALSE;
					$this->view->error = true;
					$errMessage = 'Error : Response Token cannot be left blank.';
					$this->view->report_msg = $errMessage;
				}
				else
				{
					$token = new Service_Token(NULL,$this->_userIdLogin);
					$token->setMsisdn($usermobilephone);
					$token->setMessage($message);

					$res = $token->confirm();
					$resultToken = $res['ResponseCode'] == '0000';
				}
			}
			elseif($tokenType == '2'){ //hard token
				if(empty($responseCodeReq)){
					$resultToken = FALSE;
					$this->view->error = true;
					$errMessage = 'Error : Response Token cannot be left blank.';
					$this->view->report_msg = $errMessage;
				}
				else
				{
					
					$resHard = $HardToken->verifyHardToken($challengeCodeReq, $responseCodeReq);
					$resultToken = $resHard['ResponseCode'] == '0000';

					//set user lock token gagal
					$CustUser = new CustomerUser($this->_custIdLogin,$this->_userIdLogin);
					if ($resHard['ResponseCode'] != '0000'){
						$tokenFailed = $CustUser->setFailedTokenMustLogout();
						if ($tokenFailed)
							$this->_forward('home');
					}
				}
			}
			elseif($tokenType == '3'){ //mobile token
				$resultToken = TRUE;
			}
			else{$resultToken = TRUE;} */
			//validasi hard token page 1 -- end
			
			if (!empty($tokenGoogle)) {
				$this->view->googleauth = true;
				$pga = new PHPGangsta_GoogleAuthenticator();
				// var_dump($data2['GOOGLE_CODE']);
				$setting 		= new Settings();
				$google_duration 	= $setting->getSetting('google_duration');
				$resultcapca = $pga->verifyCode($tokenGoogle, $responseCodeReq, $google_duration);
				 //var_dump($resultcapca);
				//var_dump($tokenGoogle);
				//var_dump($responseCodeReq);die;
				// var_dump($resultcapca);
				//  	var_dump($responseCode);
				//  	var_dump($tokenGoogle);die;
				if ($resultcapca) {
					$resultToken = true;
					$datatoken = array(
									'USER_FAILEDTOKEN' => 0
								);

								$wheretoken =  array();
								$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
								$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
								$data = $this->_db->update('M_USER',$datatoken,$wheretoken);
					//echo '--';
				} else {  
					$tokenFailed = $CustUser->setLogToken(); //log token activity
					//var_dump($resultToken);echo 'ge';
					$error = true; 
					$this->view->popauth = true;
					$this->view->error = true;
					$errorMsg[] = $this->language->_('Invalid Google Auth Code');	//$verToken['ResponseDesc'];
					$this->view->ERROR_MSG =  $this->language->_('Invalid Google Auth Code');
					if ($tokenFailed === true) {
				 		$this->_redirect('/default/index/logout');
				 	}

					// if ($tokenFailed === true)
					// {
					// 	$this->_redirect('/default/index/logout');
					// }
				}
			} else {
				$resultToken = TRUE;	
			}
			//var_dump($resultToken);die;
							
			if($resultToken == TRUE){
				$Beneficiary = new Beneficiary();			
				foreach($benefna as $key => $value)
				{	
					// Zend_Debug::dump($override[$value]); die;
					if($act[$value]=='Delete'){
						$Beneficiary->delete($value);
					} 
					else if($act[$value]=='Add')
					{
						if ($override[$value]==1){
							$Beneficiary->approveOverride($value);
						}else{
							$Beneficiary->approve($value);	
						}						
					}
					//$Beneficiary->approve($value);
				}
				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
				$this->_redirect('/notification/success');
			}else{
				if($resultToken == FALSE){
					if($res['ResponseCode'] == 'XT'){
						$errMessage = 'Error : Service Rejected';
						$this->view->resulttoken = $errMessage;
					}
					else{
						$errMessage = 'Error : Invalid Token';
						$this->view->resulttoken = $errMessage;
					}
				}
			}
			
			
		}
		

	}
	
}
