<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
class popup_PrincipalpopupController extends Application_Main
{
	 protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
	
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction() 
	{
	
	    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
	    $fields = array(
						'companycode'      => array('field' => 'CUST_ID',
											      'label' => 'Company Code',
											      'sortable' => true),
						'Company Name'           => array('field' => 'CUST_NAME',
											      'label' => 'Company Name',
											      'sortable' => true),
	                  );
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	
							'filter' 	  	=> array('StringTrim','StripTags'),
							'companyCode'   => array('StringTrim','StripTags','StringToUpper'),
							'companyName'   => array('StringTrim','StripTags'),
						);
		
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$select = $this->_db->select()
					        ->from(array('MA' => 'M_CUSTOMER'),
									array(
											'CUST_ID'=>'MA.CUST_ID',
											'CUST_NAME'=>'MA.CUST_NAME',
											'DISPLAY'=>new zend_db_expr("MA.CUST_ID + ' - ' + MA.CUST_NAME")
										)
									)
							->where("MA.CUST_STATUS = '1'"); 
				        
		if($filter == 'Set Filter')
		{
			$companyCode	= $zf_filter->getEscaped('companyCode');
			$companyName	= $zf_filter->getEscaped('companyName');
			
	        if($companyCode)$select->where('UPPER(MA.CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($companyCode).'%'));
	        if($companyName)$select->where('UPPER(MA.CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($companyName).'%'));
			
			$this->view->companyCode	= $companyCode;
			$this->view->companyName	= $companyName;
			//die($select);
		}
	
		//$this->view->success = true;
		
		
	    $select->order($sortBy.' '.$sortDir);   
		
		$this->paging($select);
		$this->frontendLog('V',$this->_moduleDB,null);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}

}
