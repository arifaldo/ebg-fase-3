<?php

require_once 'Zend/Controller/Action.php';

class important_AddController extends Application_Main 
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		if($this->_request->isPost())
		{


			$params = $this->_request->getParams();
		
			// print_r($params);die;
			$paramsName['sourceFileName'] 	= $sourceFileName;
			
			$helpTopic 			= $this->_getParam('helpTopic');
			if($helpTopic == null)
			{
				$helpTopic = null;
			}
			$paramsName['helpTopic']  = $helpTopic;

			$description = $this->_getParam('news');
			if($description == null)
			{
				$description = null;
			}

			$paramsName['description'] = $description;


			$filtersName = array(
									
									'helpTopic'	=> array('StripTags', 'StringTrim'),
									'ckeditor'	=> array('StripTags', 'StringTrim'),
									'to_date'	=> array('StripTags', 'StringTrim'),
									'to_time'	=> array('StripTags', 'StringTrim'),
								);
			
			$validatorsName = array(
									'helpTopic' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Can not be empty'),
																				)
														),
									'ckeditor' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Can not be empty'),
																				)
														),
									'to_date' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Can not be empty'),
																				)
														),
									'to_time' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Can not be empty'),
																				)
														),
									// 'helpTopic' => array(),
									// 'ckeditor' => array(),

									);
			// print_r($paramsName);die;
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $this->_request->getParams(), $this->_optionsValidator);

			if($zf_filter_input_name->isValid())
			{
				
						try
						{
							$this->_db->beginTransaction();
							// print_r($params);die;
							// print_r($this->_dateDisplayFormat);
							$insertArr = array(
								'CREATEDBY'			=> $this->_userIdLogin, 
								'CREATED' 		=> new Zend_Db_Expr('now()'), 
								'SUBJECT' 			=> $params['helpTopic'],
								'EXP_DATE'		=> Application_Helper_General::convertDate($params['to_date'], 'yyyy-MM-dd'),
								'EXP_TIME' 		=> $params['to_time'],
								'CONTENT'		=> $params['ckeditor'],
								'TARGET'		=> $params['target_type'],
								'SEND_TO'		=> $params['sendto'],
								'RECEIVER'		=> $params['receiver_name'],
								'FORCE_READ'		=> $params['force_read'],
								'DATE_TYPE'		=> $params['postdatetype'],
								
							);			

							if($params['postdatetype'] == '1'){
								// die;
								$insertArr['STATUS'] = '1';
								$insertArr['EFDATE'] = new Zend_Db_Expr('DATE(now())');
								$insertArr['EFTIME'] = new Zend_Db_Expr('TIME(now())');
							}else{
								$insertArr['STATUS'] = '3';
								$insertArr['EFDATE'] = Application_Helper_General::convertDate($params['from_date'], 'yyyy-MM-dd');
								$insertArr['EFTIME'] = $params['from_time'];
							}

							// print_r($insertArr);die;

							$select = $this->_db->insert('M_NOTIFICATION', $insertArr);
							//die($select);
							$this->_db->commit();
							$id = $this->_db->lastInsertId();
							Application_Helper_General::writeLog('HLAD','Add News. ID: ['.$id.'], File name : ['.$sourceFileName.'], File Topic :['.$params['helpTopic'].']');
							$this->setbackURL('/important');
							$this->_redirect('/notification/success');

						}
						catch(Exception $e)
						{
							// print_r($e);die;
							$this->_db->rollBack();
							$this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
						}
										
				
			}
			else
			{
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				// print_r($errors);die;
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName']))? $errors['sourceFileName'] : null;
				$this->view->helpTopicErr 		= (isset($errors['helpTopic']))? $errors['helpTopic'] : null;
				$this->view->helpTopic 			= $params['helpTopic'];
				$this->view->news 			= $params['news'];
			}
		}
		
		
		//$this->view->max_file_size = $maxFileSize;
		//$this->view->file_extension = $fileExt;
		
		//$disable_note_len 	 = (isset($params['description']))  ? strlen($params['description'])  : 0;	
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
		
		$disable_note_len 	 = 100; //- $disable_note_len;		
		$note_len 	 = 100 - $note_len;	

		$this->view->disable_note_len	= $disable_note_len;
		$this->view->note_len			= $note_len;
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('HLAD','Viewing Add Help');
		}
	}
}
