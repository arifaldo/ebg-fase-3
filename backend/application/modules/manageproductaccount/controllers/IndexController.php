<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Manageproductaccount_IndexController extends Application_Main
{
	
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
	public function indexAction()
	{
	   $this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$acctTypeList = $this->_db->select()
		  ->from(array('A' => 'M_PRODUCT_TYPE'), array("ACCT_TYPE","PRODUCT_NAME","PRODUCT_CODE"))
		  ->query()->fetchAll();

		$this->view->acctTypeList = $acctTypeList;
	
		$model = new manageproductaccount_Model_Manageproductaccount();
// 		$conf 		= Zend_Registry::get('config');
		$account_typeArr 	= array_combine($this->_productaccount["code"],$this->_productaccount["desc"]);
      	
    	$this->view->account_typeArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+$account_typeArr);
	
	    $fields = array(
						'acct_type'           => array('field' => 'ACCT_TYPE',
											      'label' => $this->language->_('Tipe Akun'),
											      'sortable' => true),
						'plan_code'      => array('field' => 'PRODUCT_PLAN',
											      'label' => $this->language->_('Tipe Produk'),
											      'sortable' => true),
						'product_name'           => array('field' => 'PRODUCT_NAME',
											      'label' => $this->language->_('Deskripsi Produk'),
											      'sortable' => true),
						'suggested'      => array('field' => 'LAST_SUGGESTED',
											      'label' => $this->language->_('Terakhir Disarankan'),
											      'sortable' => true),
	    				'approved'      => array('field' => 'LAST_APPROVED',
											      'label' => $this->language->_('Terakhir Disetujui'),
											      'sortable' => true),
						'action'           => array('field' => '',
											      'label' => $this->language->_('Tindakan'),
											      'sortable' => false),
				      );
		
   		$filterlist = array("TIPE AKUN"=>"PRODUCT_CODE","TIPE PRODUK" => "PRODUCT_PLAN","DESKRIPSI PRODUK"=>"PRODUCT_NAME","LAST SUGGESTED"=>"LAST_SUGGESTED","LAST SUGGESTED BY"=>"LAST_SUGGESTEDBY","LAST APPROVED"=>"LAST_APPROVED","LAST APPROVED BY"=>"LAST_APPROVEDBY");
		
		$this->view->filterlist = $filterlist;
	
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','product_code');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'PRODUCT_CODE'  => array('StringTrim','StripTags'),
							'PRODUCT_PLAN'  => array('StringTrim','StripTags'),
							'PRODUCT_NAME'      => array('StringTrim','StripTags','StringToUpper'),
							'ACCT_TYPE'      => array('StringTrim','StripTags','StringToUpper'),
							'LAST_SUGGESTED'      => array('StringTrim','StripTags','StringToUpper'),
							'LAST_SUGGESTED_END'      => array('StringTrim','StripTags','StringToUpper'),
							'LAST_SUGGESTEDBY'      => array('StringTrim','StripTags','StringToUpper'),
							'LAST_APPROVED'      => array('StringTrim','StripTags','StringToUpper'),
							'LAST_APPROVED_END'      => array('StringTrim','StripTags','StringToUpper'),
							'LAST_APPROVEDBY'      => array('StringTrim','StripTags','StringToUpper'),
		);
		

		$dataParam = array("PRODUCT_CODE","PRODUCT_PLAN","PRODUCT_NAME","ACCT_TYPE","LAST_SUGGESTED","LAST_SUGGESTEDBY","LAST_APPROVED","LAST_APPROVEDBY");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "LAST_SUGGESTED" || $value == "LAST_APPROVED") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}
		
		if (!empty($this->_request->getParam('suggestdate'))) {
			$suggestdate = $this->_request->getParam('suggestdate');
			$dataParamValue['LAST_SUGGESTED'] = $suggestdate[0];
			$dataParamValue['LAST_SUGGESTED_END'] = $suggestdate[1];
		}
		if (!empty($this->_request->getParam('approveddate'))) {
			$approveddate = $this->_request->getParam('approveddate');
			$dataParamValue['LAST_APPROVED'] = $approveddate[0];
			$dataParamValue['LAST_APPROVED_END'] = $approveddate[1];
		}
		
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		
		// print_r($dataParamValue);die;
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	

		$fParam = array();
		if($filter == TRUE)
		{
			// die;
			
			$fParam['ftipeakun'] 		= $zf_filter->getEscaped('PRODUCT_CODE');
			$fParam['ftipeproduk'] 		= $zf_filter->getEscaped('PRODUCT_PLAN');
			$fParam['fdeskripsiproduk']     	= $zf_filter->getEscaped('PRODUCT_NAME');
			$fParam['fsuggested']     	= $zf_filter->getEscaped('LAST_SUGGESTED');
			$fParam['fsuggested_end']     	= $zf_filter->getEscaped('LAST_SUGGESTED_END');
			$fParam['fsuggestedby']     	= $zf_filter->getEscaped('LAST_SUGGESTEDBY');
			$fParam['fapproved']     	= $zf_filter->getEscaped('LAST_APPROVED');
			$fParam['fapproved_end']     	= $zf_filter->getEscaped('LAST_APPROVED_END');
			$fParam['fapprovedby']     	= $zf_filter->getEscaped('LAST_APPROVEDBY');
		}
		
// 		die('here');
		$select = $model->getData($fParam,$sortBy,$sortDir,$filter);	
		
		foreach ($select as  $key => $value){
			$arrTemp[$value['PRODUCT_ID']] = $model->getTempProductId($value['PRODUCT_ID']);
		}
		$this->view->getTempProductId = $arrTemp; 
		
		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		/*foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['PRODUCT_ID']);
		}*/
		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
			$save_temp = [];
			foreach ($selectPdfCsv as $key => $value) {
				array_push($save_temp, [
					$key + 1,
					$account_typeArr[$value["ACCT_TYPE"]] . " (" . $value["PRODUCT_CODE"] . ")",
					$value["PRODUCT_PLAN"],
					$value["PRODUCT_NAME"],
					date("d M Y", strtotime($value["LAST_SUGGESTED"])) . " (" . $value["LAST_SUGGESTEDBY"].")",
					date("d M Y", strtotime($value["LAST_APPROVED"])) . " (" . $value["LAST_APPROVEDBY"].")",

				]);
			}

			$head = [
				"No",
				$this->language->_('Tipe Akun'),
				$this->language->_('Tipe Produk'),
				$this->language->_('Deskripsi Produk'),
				$this->language->_('Terakhir Disarankan'),
				$this->language->_('Terakhir Disetujui')
			];
			return $this->_helper->download->csv($head, $save_temp, null, $this->language->_('Produk Rekening List - ' . date("Ymd-His")));
			Application_Helper_General::writeLog('PALS','Export to CSV');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf(array($this->language->_('Product Code'),$this->language->_('Plan Code'),$this->language->_('Product Name')),$selectPdfCsv,null,'product_account');   
				Application_Helper_General::writeLog('PALS','Export to PDF');
		}
		elseif($this->_request->getParam('print') == 1){
				unset($fields['action']);
	            $this->_forward('print', 'index', 'widget', array('data_content' => $selectPdfCsv, 'data_caption' => 'product_account', 'data_header' => $fields));
	       		Application_Helper_General::writeLog('PALS','Export to PDF');
		}
		else
		{		
				Application_Helper_General::writeLog('PALS','View Product Account List');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		
		if (!empty($dataParamValue)) {
			$this->view->suggestStart = $dataParamValue['LAST_SUGGESTED'];
			$this->view->suggestEnd = $dataParamValue['LAST_SUGGESTED_END'];
			$this->view->approvedStart = $dataParamValue['LAST_APPROVED'];
			$this->view->approvedEnd = $dataParamValue['LAST_APPROVED_END'];

			unset($dataParamValue['LAST_SUGGESTED_END']);
			unset($dataParamValue['LAST_APPROVED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
			// print_r($whereval);die;
		}
		
	}

	
	
	public function accttypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $selectGroup =  $this->_db->select()
            ->from('M_PRODUCT_TYPE', array('PRODUCT_CODE', 'PRODUCT_NAME'));
        // ->where("BGROUP_STATUS = '1'");         
        $arrGroup = $this->_db->fetchAll($selectGroup);
        $optHtml = "<option value=''>-- " . $this->language->_('Silahkan Pilih') . " --</option>";
        foreach ($arrGroup as $key => $value) {
            if ($tblName == $value['PRODUCT_CODE']) {
                $select = 'selected';
            } else {
                $select = '';
            }
            $optHtml .= "<option value='" . $value['PRODUCT_CODE'] . "' " . $select . ">" . $value['PRODUCT_NAME'] . "</option>";
        }

        echo $optHtml;
    }
}