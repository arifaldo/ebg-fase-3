<?php


require_once 'Zend/Controller/Action.php';


class chargesdomestic_RepairController extends Application_Main
{
	public function indexAction() 
	{
		$changeid = $this->_getParam('changes_id');
		
		$select2 = $this->_db->select()->distinct()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
		$select2 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$result2 = $this->_db->fetchRow($select2);

		$custid = $result2['KEY_FIELD'];
		
		$selectcur = $this->_db->select()
			->from(array('A' => 'M_MINAMT_CCY'),
				array('CCY_ID'))
				->order('CCY_ID ASC')
				 -> query() ->fetchAll();
		//Zend_Debug::dump($select); die;
		$this->view->cur=$selectcur;
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$select -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $this->_db->fetchRow($select);
		$this->view->result = $result;		
		
		$custname = $result['CUST_NAME'];
		
		$select2 = $this->_db->select()
							->from('TEMP_CHARGES_OTHER');
		$select2 -> where("CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$select2 -> where("CHARGES_TYPE = '1'");
		$result2 = $this->_db->fetchRow($select2);
		if($result2)
		{
			$this->view->rtgsamt = $result2['CHARGES_AMT'];
			$this->view->rtgsccy = $result2['CHARGES_CCY'];
		}
	
		$select3 = $this->_db->select()
							->from('TEMP_CHARGES_OTHER');
		$select3 -> where("CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$select3 -> where("CHARGES_TYPE = '2'");
		$result3 = $this->_db->fetchRow($select3);
		if($result3)
		{
			$this->view->sknamt = $result3['CHARGES_AMT'];
			$this->view->sknccy = $result3['CHARGES_CCY'];
		}
		
		$select4 = $this->_db->select()
							->from('M_CHARGES_OTHER');
		$select4 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select4 -> where("CHARGES_TYPE = '1'");
		$result4 = $this->_db->fetchRow($select4);
		if($result4)
		{
			$this->view->rtgsamtdefault = $result4['CHARGES_AMT'];
			$this->view->rtgsccydefault = $result4['CHARGES_CCY'];
		}
	
		$select5 = $this->_db->select()
							->from('M_CHARGES_OTHER');
		$select5 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select5 -> where("CHARGES_TYPE = '2'");
		$result5 = $this->_db->fetchRow($select5);
		if($result5)
		{
			$this->view->sknamtdefault = $result5['CHARGES_AMT'];
			$this->view->sknccydefault = $result5['CHARGES_CCY'];
		}
		
		if($result4 && $result5)
		{
			$this->view->default = true;
		}
		
		$template = $this->_db->select()->distinct()
						->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),array('TEMPLATE_NAME'));
		$template = $this->_db->fetchAll($template);
		$i = 0;
		//die;
		//Zend_Debug::dump($template);die;
		foreach($template as $list)
		{
			$templatelist = $this->_db->select()->distinct()
						->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'),array())
						->join(array('B' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = B.TEMPLATE_ID' , array('TEMPLATE_NAME'))
						->where("A.STATUS NOT LIKE '3'");
			$templatelist = $this->_db->fetchAll($templatelist);
			$tempnameid = 'temp'.$i;
			$this->view->$tempnameid = $list['TEMPLATE_NAME'];
			foreach($templatelist as $list2)
			{
				if(isset($list2['CHARGES_TYPE']) && !(empty($list2['CHARGES_TYPE']))){
					if($list2['CHARGES_TYPE'] == '1')
					{
						$rtgsamt = 'rtgsamt'.$i;
						$rtgsccy = 'rtgsccy'.$i;
						$this->view->$rtgsamt = $list2['CHARGES_AMT'];
						$this->view->$rtgsccy = $list2['CHARGES_CCY'];
					}
					if($list2['CHARGES_TYPE'] == '2')
					{
						$sknamt = 'sknamt'.$i;
						$sknccy = 'sknccy'.$i;
						$this->view->$sknamt = $list2['CHARGES_AMT'];
						$this->view->$sknccy = $list2['CHARGES_CCY'];
					}
				}
			}
			$i++;
		}
		if($i > 0)
		{
			$this->view->countlist = $i;
		}

/////////////////////////////////////////////////////////////////////////////////////////////
		
		$submit = $this->_getParam('submit');
			if($submit)
			{
				Application_Helper_General::writeLog('CHCR','Repairing Domestic charges ('.$changeid.')');
				$this->_db->beginTransaction();
				try
				{
					$info = 'Charges';
					$info2 = 'Domestic Charges';
					$error = 0;
					$this->updateGlobalChanges($changeid,$info);
					
					$where = array('CHANGES_ID = ?' => $changeid);
					$this->_db->delete('TEMP_CHARGES_OTHER',$where);
					
					$rtgsamtinsert = $this->_getParam('rtgs');
					$rtgsccyinsert = $this->_getParam('rtgsccy');
					$sknamtinsert = $this->_getParam('skn');
					$sknccyinsert = $this->_getParam('sknccy');
					
					$this->view->rtgsamt = $rtgsamtinsert;
					$this->view->rtgsccy = $rtgsccyinsert;
					$this->view->sknamt = $sknamtinsert;
					$this->view->sknccy = $sknccyinsert;
					
					$rtgsamt = Application_Helper_General::convertDisplayMoney($rtgsamtinsert);
					$temp_rtgsamt = str_replace('.','',$rtgsamt);
					$cek_angka = (Zend_Validate::is($temp_rtgsamt,'Digits')) ? true : false;
					
					$sknamt = Application_Helper_General::convertDisplayMoney($sknamtinsert);
					$temp_sknamt = str_replace('.','',$sknamt);
					$cek_angka2 = (Zend_Validate::is($temp_sknamt,'Digits')) ? true : false;
					
					$select5 = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('*'))
							->where("CCY_ID LIKE ".$this->_db->quote($rtgsccyinsert))
				 			-> query()->fetchAll();
				 			
				 	$select6 = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('*'))
							->where("CCY_ID LIKE ".$this->_db->quote($sknccyinsert))
				 			-> query()->fetchAll(); 
					
					if($rtgsamtinsert != null && !$cek_angka)
					{
						$this->view->errrtgsamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}
					
					if($sknamtinsert != null && !$cek_angka2)
					{
						$this->view->errsknamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}
					
					if($rtgsamtinsert == null)
					{
						$this->view->errrtgsamt = $this->language->_('Value must be filled');
						$error++;
					}
					
					if($sknamtinsert == null)
					{
						$this->view->errsknamt = $this->language->_('Value must be filled');
						$error++;
					}
					
					if(!$select5)
					{
						$this->view->errrtgsccy = $this->language->_('Incorrect currency');
						$error++;
					}
					
					if(!$select6)
					{
						$this->view->errsknccy = $this->language->_('Incorrect currency');
						$error++;
					}

					if($error == 0)
					{
							$data1 = array(
											'CHANGES_ID'	=> $changeid,
											'CUST_ID' 		=> $custid,
											'CHARGES_TYPE'	=> '1',
											'CHARGES_NO'	=> '1',
											'CHARGES_CCY' 	=> $rtgsccyinsert,
											'CHARGES_AMT' 	=> $rtgsamt,
											);

							$this->_db->insert('TEMP_CHARGES_OTHER',$data1);
							//echo 'data1';die;
							$data2 = array(
											'CHANGES_ID'	=> $changeid,
											'CUST_ID' 		=> $custid,
											'CHARGES_TYPE'	=> '2',
											'CHARGES_NO'	=> '1',
											'CHARGES_CCY' 	=> $sknccyinsert,
											'CHARGES_AMT' 	=> $sknamt,
											);
							$this->_db->insert('TEMP_CHARGES_OTHER',$data2);
							//die;
							$commit = true;
					}
					
					if($error == 0 && $commit)
					{	
						//echo 'commit';die;
						$this->_db->commit();						    
						$this->_redirect('/popup/successpopup/index');
					}
				}
				catch(Exception $e)
				{
					//echo 'rollback';die;
					$this->_db->rollBack();
				}
		}
		else
		{
			Application_Helper_General::writeLog('CHCR','View Repair Domestic charges page ('.$changeid.')');
		}
	}
}
