<?php
class registerechannel_Model_Registerechannel
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getTempBis($change_id)
	{
		$result = $this->_db->select()
							->from('TEMP_MYONLINE_BIS')
							->where('CHANGES_ID = ?', $change_id);

		return $this->_db->fetchRow($result);
	}

	public function getTempInd($change_id)
	{
		$result = $this->_db->select()
							->from('TEMP_MYONLINE_IND')
							->where('CHANGES_ID = ?', $change_id);

		return $this->_db->fetchRow($result);
	}

	public function getBisUser($custid)
	{
		$result = $this->_db->select()
							->from('T_MYONLINE_BIS_USER')
							->where('CUST_ID = ?', $custid);

		return $this->_db->fetchAll($result);
	}

	public function getBisUserDet($id)
	{
		$result = $this->_db->select()
							->from('T_MYONLINE_BIS_USER')
							->where('ID = ?', $id);

		return $this->_db->fetchRow($result);
	}

	public function getBisAcct($custid)
	{
		$result = $this->_db->select()
							->from('T_MYONLINE_BIS_ACC')
							->where('CUST_ID = ?', $custid);

		return $this->_db->fetchAll($result);
	}

	public function getBisAcctDet($id)
	{
		$result = $this->_db->select()
							->from('T_MYONLINE_BIS_ACC')
							->where('ID = ?', $id);

		return $this->_db->fetchRow($result);
	}

	public function getIndAcct($tempid)
	{
		$result = $this->_db->select()
							->from('T_MYONLINE_IND_ACC')
							->where('REG_ID = ?', $tempid);

		return $this->_db->fetchAll($result);
	}

	public function getIndAcctDet($id)
	{
		$result = $this->_db->select()
							->from('T_MYONLINE_IND_ACC')
							->where('REG_ID = ?', $id);

		return $this->_db->fetchRow($result);
	}

	public function getCountry()
	{
		$result = $this->_db->select()
							->from('M_COUNTRY')
							->query()
							->fetchAll();

		return $result;
	}

	public function getBranch()
	{
		$result = $this->_db->select()
							->from('M_BANK_BRANCH')
							->query()
							->fetchAll();

		return $result;
	}

	public function getFileName($custID,$col)
	{
		$result = $this->_db->select()
							->from('TEMP_MYONLINE_BIS',array($col))
							->where('CUST_ID = ?', $custID);

		return $this->_db->fetchOne($result);
	}
}