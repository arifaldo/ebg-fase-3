<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/Account.php';
	require_once 'General/Settings.php';
	
	$setting = new Settings();
	$starttime = $setting->getSetting('start_running_balance');
	$endtime = $setting->getSetting('cut_off_running_balance');	
	$begin = new DateTime($starttime);
	$end = new DateTime($endtime);
	$now = new DateTime();
		
			  	


	// if ($now >= $begin && $now <= $end){
		// die;
	
	// die('here');
	$db = Zend_Db_Table::getDefaultAdapter();
	$actionid = 'scheduler';
	$moduleid = 'reject payment';
	$interval = 2;
	$truecounter = 0;
	$falsecounter = 0;
	
	$dataCM = $db->select()
						->FROM('M_CUSTOMER_ACCT',array('ACCT_NO','CUST_ID','CCY_ID','ACCT_TYPE'))
						->WHERE('ACCT_STATUS = 1 OR ACCT_STATUS = 3 ')
						->WHERE('ACCT_NO = ?','20181011541')
						->QUERY()->FETCHALL();
	
	if($dataCM)
	{
		foreach($dataCM as $row)
		{
			$ccy = Application_Helper_General::getCurrNum($row['CCY_ID']);
			$account 	  = new Account($row['ACCT_NO'], $ccy);
			$accountCheck = $account->checkBalance();	// return array('checkBalanceStatus'=>$resultCheckBalance,'errorMessage'=>$errorMsgListener);
			$accountInfo  = $account->getCoreAccountInfo();
			



			
				
				$ccy 		= $row['CCY_ID'];
				$ACCT_TYPE 	= $row['ACCT_TYPE'];
				if(!empty($ccy)){
				$currCode 	= Application_Helper_General::getCurrNum($ccy);
				}
				if (!empty($row['ACCT_NO']))	
				{						   

					$data =  new Service_Account($row['ACCT_NO'],$currCode);
					$dataInquiry = $data->accountInquiry('AB',FALSE); 
					$dataAccountType = $dataInquiry['AccountType'];

					$dateFrom = date('Y-m-1');
					$dateTo = date('Y-m-30');

					// $dateFromDB = Application_Helper_General::convertDate($dateFrom, $this->_dateDBFormat, $this->_dateDisplayFormat);
					// $dateToDB   = Application_Helper_General::convertDate($dateTo, $this->_dateDBFormat, $this->_dateDisplayFormat);
					
					$dataStatement = $data->accountStatementHistory($dateFromDB, $dateToDB, $isMobile = FALSE, $dataAccountType); 
						
					print_r($dataStatement);die;	
					// echo '<pre>';
					// print_r($dataStatement);die;

					if($dataInquiry['ResponseCode'] == '00' && $dataStatement['ResponseCode'] == '00'){
						$this->view->messageResponse = '1';
					}
					elseif ($dataStatement['ResponseCode'] =='45'){
						$this->view->messageError = $this->language->_('No Transaction History');
						$this->view->messageResponse = '0';
					}
					else{
						$this->view->messageError = $this->language->_('Error : Cant Continue the Transaction, Please Retry in a few moment');
						$this->view->messageResponse = '0';
					}	
						
					$sessionData = new Zend_Session_Namespace('TRX_INQ');
					$sessionData->Detail 			= $dataStatement['Detail'];	
					$sessionData->OpeningBalance 	= $dataStatement['OpeningBalance'];	
					$sessionData->ClosingBalance 	= $dataStatement['ClosingBalance'];	
					$sessionData->HoldAmount 		= $dataInquiry['HoldAmount'];	
				}
				else
				{
					$sessionData = new Zend_Session_Namespace('TRX_INQ');
					
					$dataStatement['Detail'] 			= $sessionData->Detail;
					$dataStatement['OpeningBalance'] 	= $sessionData->OpeningBalance; 
					$dataStatement['ClosingBalance'] 	= $sessionData->ClosingBalance;
					$dataInquiry['HoldAmount'] 			= $sessionData->HoldAmount;
				}
				//echo "<pre>"; print_r($dataStatement);	die();
				
				$dataDetails = array();
				$totalDebet = "";
				$totalKredit = "";
								
				foreach($dataStatement['Detail'] as $dt => $datadtl)
				{

					if($acct_stat_layout == '1'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
							if( $dt == 0 )
							{
//								$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
							}
							else
							{
								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//								$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							}
							
							
							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}
					elseif($acct_stat_layout == '2'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
							if( $dt == 0 )
							{
//								$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
							}
							else
							{
								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//								$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							}
							
							
							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}
					elseif($acct_stat_layout == '1R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
//						$balance = $datadtl->Balance;
//						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);

						if( $dt == 0 )
						{
//							$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
							$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
						}
						else
						{
							$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//							$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
						}
						
						
						$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;

					}
					elseif($acct_stat_layout == '2R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance - $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance + $datadtl->Amount;
						}
						
						
//						$typeTransaction = $datadtl->DBCR;
//						$dataAmount = $datadtl->Amount;
//						
//						
//						if($typeTransaction == 'D'){
//							$debetAmount = $dataAmount;
//							
//
//						}
//						elseif($typeTransaction == 'C'){
//							$creditAmount = $dataAmount;
//						}
						
//						$totBalance = ($dataStatement['ClosingBalance']-$totalKredit+$totalDebet);
//						$tot = $totBalance + $debetAmount - $creditAmount;
						
						if( $dt == 0 )
						{
//							$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
							$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
						}
						else
						{
							$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//							$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
						}
						
						
						$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
						
						
//						$dataStatement['Detail'][$dt]->Balance = $tot;
						//$runningBalance = ($totBalance-$debetAmount)+$creditAmount;
						//$balance = $datadtl->Balance;
//						$balance = $tot;
						
//						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);
						
					}
				
					
					if ($period == 'history')
					{	
//						$balanceSign = substr($datadtl->Balance, 0, 1);
//						$balance	 = substr($datadtl->Balance, 1);
//						if ($balanceSign != '+')
//			   			{	$balance = (-1) * $balance;		}
			   			
			   			$balance = $datadtl->Balance;
	   			
						//$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);		
					}
		
				}
				
				$dataStatement['Detail'] = array_reverse($dataStatement['Detail'], true); //di sorting
				












			if($accountInfo['availableBalance']=='N/A'){
				$accountInfo['availableBalance'] = 0;
			}
			if($accountCheck['checkBalanceStatus']!=''){

				$cekdata = $db->select()
						->FROM('T_BALANCE',array('*'))
						->WHERE('ACCT_NO = ?',$row['ACCT_NO'])
						->WHERE('DATE(PLAFOND_END) = DATE(NOW())')
						 // echo $cekdata;die;
						->QUERY()->FETCHALL();

				if(!empty($cekdata)){
					$logData = array(	
										'CUST_ID' 			=> $row['CUST_ID'],
										'CCY_ID' 			=> $row['CCY_ID'],
										'ACCT_NAME'			=> $accountInfo['accountName'],
										'PLAFOND' 			=> $accountInfo['availableBalance'],
										'ACCT_STATUS' 				=> 'A'
									);
					$where = array(
										'ACCT_NO'	=> $row['ACCT_NO'],
										'PLAFOND_END' 		=> new Zend_Db_Expr("now()")
					);
					// $db->insert('T_BALANCE', $logData);
					$db->update('T_BALANCE', $logData, $where); 

				}else{
					$logData = array(	'PLAFOND_END' 		=> new Zend_Db_Expr("now()"),
										'CUST_ID' 			=> $row['CUST_ID'],
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'CCY_ID' 			=> $row['CCY_ID'],
										'ACCT_NAME'			=> $accountInfo['accountName'],
										'PLAFOND' 			=> $accountInfo['availableBalance'],
										'ACCT_STATUS' 				=> 'A'
									);
					$db->insert('T_BALANCE', $logData);

				}

				
			// 	print_r($accountInfo);
			// print_r($accountCheck);die;	
			}
		}
		
				
	}

// }

	
?>