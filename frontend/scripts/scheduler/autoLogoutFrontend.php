<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

$db = Zend_Db_Table::getDefaultAdapter();
//$db = Zend_Registry::set('db',$db);

function userIdle($diff)
{
	$db = Zend_Db_Table::getDefaultAdapter();	
	
	$getUserIdle = $db->select()
				  ->from('TEMP_SESSION_USER')
				  ->where('TIMESTAMPDIFF(second, USER_LASTACTIVITY, now()) > ?', $diff)
				  ->query()
				  ->fetchall()
	;

	forceLogout($getUserIdle);
}

function forceLogout($userIdle)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	try{
		$db->beginTransaction();
		
		$res = null;
		$string = '';
		foreach($userIdle as $row)
		{
			unset($where);
			$custid = $row['CUST_ID'];
			$userid = $row['USER_ID'];	
			
			$logout = array(    'USER_ISLOGIN'      => '0' );		
			$where[] = "CUST_ID = '".$custid."'";
			$where[] = "USER_ID = '".$userid."'";
			$res += $db->update('M_USER', $logout, $where);
			$resDelete = $db->delete('TEMP_SESSION_USER', $where);
			
			$string .="Force Logout Cust ID {$custid} User ID {$userid}";
		}	
		Application_Helper_General::cronLog('autoLogoutFrontend', $res, '1');
		$db->commit();		   
	}
	catch(Exception $e) 
	{
		$db->rollBack();
	}
}

function autoLogout()
{

	$set = new Settings();
	$minDiff = $set->getSetting('timeoutafter');
	userIdle($minDiff);
}

autoLogout();

?>