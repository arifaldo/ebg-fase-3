<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class vendorbglist_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$page 			= $this->_getParam('page');
		$filter_clear 	= $this->_getParam('filter_clear');
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;


		$Settings = new Settings();
		$claim_period = $Settings->getSetting('max_claim_period');
		$this->view->BG_CLAIM_PERIOD = $claim_period;

		//get filtering param
		$filterlist = array('BG_NUMBER', 'BG_SUBJECT', 'BG_STATUS');

		$this->view->filterlist = $filterlist;

		$filterArr = array(
			'BG_NUMBER'  => array('StringTrim', 'StripTags'),
			'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
			'BG_STATUS'   => array('StringTrim', 'StripTags')
		);

		$dataParam = array('BG_NUMBER', 'BG_SUBJECT', 'BG_STATUS');
		$dataParamValue = array();

		foreach ($dataParam as $dtParam) {
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						if (!empty($dataParamValue[$dtParam])) {
							$dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
						}
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}
		}

		$options = array('allowEmpty' => true);
		$validators = array(
			'BG_NUMBER' => array(),
			'BG_SUBJECT' => array(),
			'BG_STATUS' => array()
		);

		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

		$fbgnumb    = $zf_filter->getEscaped('BG_NUMBER');
		$subject   = $zf_filter->getEscaped('BG_SUBJECT');
		$fstatus   = $zf_filter->getEscaped('BG_STATUS');

		//

		//$whereIn = [7, 20, 21, 22, 23];
		$whereIn = [15, 16];
		$select = $this->_db->select()
			->from(
				array('A' => 'T_BANK_GUARANTEE'),
				array(
					'BG_REG_NUMBER'			=> 'A.BG_REG_NUMBER',
					'BG_CLAIM_DATE'			=> 'A.BG_CLAIM_DATE',
					'RECIPIENT_NAME'		=> 'A.RECIPIENT_NAME',
					'BG_NUMBER'				=> 'A.BG_NUMBER',
					'BG_SUBJECT'			=> 'A.BG_SUBJECT',
					'BG_AMOUNT'				=> 'A.BG_AMOUNT',
					'BRANCH_NAME'			=> 'B.BRANCH_NAME',
					'COUNTER_WARRANTY_TYPE'	=> new Zend_Db_Expr("(
					CASE A.COUNTER_WARRANTY_TYPE
					WHEN '1' THEN 'Full Cover'
					WHEN '2' THEN 'Line Facility'
					WHEN '3' THEN 'Insurance'
					ELSE '-'
					END)"),
					'BG_INSURANCE_CODE'		=> 'A.BG_INSURANCE_CODE',
					'TIME_PERIOD_START'		=> 'A.TIME_PERIOD_START',
					'TIME_PERIOD_END'		=> 'A.TIME_PERIOD_END',
					'STATUS'				=> new Zend_Db_Expr("(
					CASE A.BG_STATUS
					WHEN '1' THEN 'Waiting for review'
					WHEN '2' THEN 'Waiting for approve'
					WHEN '3' THEN 'Waiting to release'
					WHEN '4' THEN 'Waiting for bank approval'
					WHEN '5' THEN 'Issued'
					WHEN '6' THEN 'Expired'
					WHEN '7' THEN 'Canceled'
					WHEN '8' THEN 'Claimed by applicant'
					WHEN '9' THEN 'Claimed by recipient'
					WHEN '10' THEN 'Request Repair'
					WHEN '11' THEN 'Reject'
					ELSE '-'
					END)"),
					"BG_STATUS",
					'BG_ISSUED'
				)
			)
			->joinLeft(
				array('B' => 'M_BRANCH'),
				'B.BRANCH_CODE = A.BG_BRANCH',
				array()
			)
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = A.CUST_ID',
				array(
					"CUST_NAME",
				)
			)
			->joinLeft(
				array('INSURANCE' => 'M_CUSTOMER'),
				'INSURANCE.CUST_ID = A.BG_INSURANCE_CODE',
				array(
					"INSURANCE_NAME" => "INSURANCE.CUST_NAME",
				)
			)
			->where('A.SP_OBLIGEE_CODE = ?', $this->_custIdLogin)
			->where('A.BG_STATUS IN (?)', $whereIn);

		if ($fbgnumb) {
			$select->where("A.BG_NUMBER LIKE " . $this->_db->quote('%' . $fbgnumb . '%'));
		}
		if ($subject) {
			$select->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
		}

		if ($fstatus) {
			$select->where("A.BG_STATUS = ?", $fstatus);
		}

		$select->order('A.BG_CREATED DESC');
		$data = $this->_db->fetchAll($select);
		$this->paging($data);

		$config = Zend_Registry::get('config');
		//$config    		= Zend_Registry::get('config');
		$BgType     = $config["bg"]["status"]["desc"];
		$BgCode     = $config["bg"]["status"]["code"];

		$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

		$this->view->arrStatus = $arrStatus;

		$this->view->currentPage	= $page;
		$this->view->filter 		= $filter;

		if (!empty($dataParamValue)) {

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',', $value);
				if (!empty($duparr)) {

					foreach ($duparr as $ss => $vs) {
						$wherecol[] = $key;
						$whereval[] = $vs;
					}
				} else {
					$wherecol[] = $key;
					$whereval[] = $value;
				}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}


		if ($data) {

			$dataAll = [];

			if (!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1) {
				foreach ($data as $key => $row) {
					$subData = [];
					$subData['BG_REG_NUMBER'] = $row['BG_REG_NUMBER'] . ' / ' . $row['CUST_NAME'];
					$subData['BG_NUMBER'] = $row['BG_NUMBER'] . ' / ' . strtoupper($row['BG_SUBJECT']);
					$subData['BG_AMOUNT'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['BG_AMOUNT']);
					$subData['BRANCH_NAME'] = $row['BRANCH_NAME'];

					if (empty($row['BG_INSURANCE_CODE'])) {
						$subData['COUNTER_WARRANTY_TYPE'] = $row['COUNTER_WARRANTY_TYPE'];
					} else {
						$subData['COUNTER_WARRANTY_TYPE'] = $row['COUNTER_WARRANTY_TYPE'] . ' (' . $row['BG_INSURANCE_CODE'] . ')';
					}

					$subData['TIME_PERIOD_START'] = date('d M Y', strtotime($row['TIME_PERIOD_START']));
					$subData['TIME_PERIOD_END'] = date('d M Y', strtotime($row['TIME_PERIOD_END']));
					$subData['BG_STATUS'] = $arrStatus[$row['BG_STATUS']];
					$dataAll[] = $subData;
				}
			}


			if ($this->_getParam('csv')) {

				$this->_helper->download->csv(array($this->language->_('	Reg No# / Applicant'), $this->language->_('BG Number / Subjek Transaksi'), $this->language->_('BG Amount'), $this->language->_('Branch'), $this->language->_('Counter Type'), $this->language->_('Start Date'), $this->language->_('End Date'), $this->language->_('Status')), $dataAll, null, 'Vendor Bank Guarantee List');
			} else if ($this->_request->getParam('print') == 1) {

				$fields = array(
					'regno'     => array(
						'field'    => 'BG_REG_NUMBER',
						'label'    => $this->language->_('Reg No# / Applicant'),
					),
					'number'     => array(
						'field'    => 'BG_NUMBER',
						'label'    => $this->language->_('BG Number / Subjek Transaksi'),
					),
					'bgamount'  => array(
						'field'    => 'BG_AMOUNT',
						'label'    => $this->language->_('BG Amount'),
					),
					'branch'  => array(
						'field'    => 'BRANCH_NAME',
						'label'    => $this->language->_('Branch'),
					),
					'countertype'  => array(
						'field'    => 'COUNTER_WARRANTY_TYPE',
						'label'    => $this->language->_('Counter Type'),
					),
					'startdate' => array(
						'field' => 'TIME_PERIOD_START',
						'label' => $this->language->_('Date From'),
					),
					'enddate'   => array(
						'field'    => 'TIME_PERIOD_END',
						'label'    => $this->language->_('Date To'),
					),
					'status'   => array(
						'field'    => 'BG_STATUS',
						'label'    => $this->language->_('Status'),
					)
				);

				$this->_forward('print', 'index', 'widget', array('data_content' => $dataAll, 'data_caption' => 'Vendor Bank Guarantee List', 'data_header' => $fields));
			}

			Application_Helper_General::writeLog('OVBG', $this->language->_('Lihat Daftar Bank Garansi Vendor'));
		}
	}

	public function detailAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$bg = $this->_getParam('bgnumb');

		$sessToken  = new Zend_Session_Namespace('Tokenenc');
		$password   = $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bg);
		$decryption_bg = $AESMYSQL->decrypt($decryption, $password);

		$conf = Zend_Registry::get('config');

		$whereIn = [15, 16];

		$select = $this->_db->select()
			->from(array('TBG' => 'T_BANK_GUARANTEE'), array('*'))
			->joinLeft(
				array('MB' => 'M_BRANCH'),
				'MB.BRANCH_CODE = TBG.BG_BRANCH',
				array('BRANCH_NAME')
			)
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = TBG.CUST_ID',
				array(
					'CUST_ID',
					'CUST_NAME',
					'CUST_NPWP',
					'CUST_ADDRESS',
					'CUST_CITY',
					'CUST_FAX',
					'CUST_CONTACT',
					'CUST_PHONE',
				)
			)
			->joinLeft(
				array('MCST' => 'M_CUSTOMER'),
				'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
				array(
					'SP_OBLIGEE_NAME' => "MCST.CUST_NAME",
				)
			)
			->joinLeft(
				array('MCL' => 'M_CITYLIST'),
				'MCL.CITY_CODE = MC.CUST_CITY',
				array('CITY_NAME')
			)
			->joinLeft(
				array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
				'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
				array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
			)
			->joinLeft(
				array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
				'TBGS.BG_NUMBER = TBG.BG_REG_NUMBER',
				[
					"ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
				]
			)
			->where('TBG.BG_REG_NUMBER = ?', $decryption_bg)
			->where('TBG.SP_OBLIGEE_CODE = ?', $this->_custIdLogin)
			->where("TBG.BG_STATUS IN (?)", $whereIn);
		$data = $this->_db->fetchRow($select);

		if ($this->_request->isPost()) {
			$download = $this->_getParam('certfinal');
			if ($download == 1) {
				$attahmentDestination = UPLOAD_PATH . '/document/submit/';
				// $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				return $this->_helper->download->file("BG_NO_" . $data["BG_NUMBER"] . ".pdf", $attahmentDestination . $data['DOCUMENT_ID'] . ".pdf");
			}
		}

		if ($data['BG_OLD']) {
			$bgOld = $this->_db->select()
				->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
				->where('BG_NUMBER = ?', $data['BG_OLD'])
				->query()->fetch();

			$this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
			$this->view->prevProv = $bgOld['PROVISION_FEE'];
		}

		$config        = Zend_Registry::get('config');
		$BgcomplationType     = $config["bgclosing"]["changetype"]["desc"];
		$BgcomplationCode     = $config["bgclosing"]["changetype"]["code"];

		$arrStatusSettlement = array_combine(array_values($BgcomplationCode), array_values($BgcomplationType));

		$this->view->arrStatusSettlement = $arrStatusSettlement;

		// GET PAPER USED
		$getPaperUsed = $this->_db->select()
			->from('M_PAPER')
			->where('NOTES = ?', $data['BG_NUMBER'])
			->query()->fetchAll();

		$paperUsed = array_column($getPaperUsed, 'PAPER_ID');
		$this->view->paperUsed = $paperUsed;

		if ($data['BG_NUMBER_NEW']) {
			$bgNew = $this->_db->select()
				->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
				->where('BG_NUMBER = ?', $data['BG_NUMBER_NEW'])
				->query()->fetch();

			$this->view->bgRegNumberNew = $bgNew['BG_REG_NUMBER'];
		}

		switch ($data["CHANGE_TYPE"]) {
			case '0':
				$this->view->suggestion_type = "New";
				break;
			case '1':
				$this->view->suggestion_type = "Amendment Changes";
				break;
			case '2':
				$this->view->suggestion_type = "Amendment Draft";
				break;
		}

		//echo '<pre>';print_r($data);
		$conf = Zend_Registry::get('config');
		// BG TYPE
		$bgType         = $conf["bg"]["type"]["desc"];
		$bgCode         = $conf["bg"]["type"]["code"];

		$arrbgType = array_combine(array_values($bgCode), array_values($bgType));

		$this->view->arrbgType = $arrbgType;

		//BG Counter Guarantee Type
		$bgcgType         = $conf["bgcg"]["type"]["desc"];
		$bgcgCode         = $conf["bgcg"]["type"]["code"];

		$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

		$this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];


		if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

			$bgdatasplit = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $decryption_bg)
				->query()->fetchAll();

			$this->view->fullmember = $bgdatasplit;
		}

		// if (!empty($getDataBg['BG_OLD'])) {
		//   $tbgdata = $this->_db->select()
		//     ->from(["A" => "T_BANK_GUARANTEE"], ["*"])
		//     ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
		//     ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
		//     ->where('A.BG_NUMBER = ?', $getDataBg['BG_OLD'])
		//     ->query()->fetch();
		//   $this->view->tbgdata = $tbgdata;
		// }

		// if (!empty($tbgdata['BG_REG_NUMBER'])) {
		//   $tbgdatadetail = $this->_db->select()
		//     ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
		//     ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
		//     ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
		//     ->query()->fetchAll();
		// }

		// if (!empty($tbgdatadetail)) {
		//   foreach ($tbgdatadetail as $key => $value) {

		//     if ($getDataBg['COUNTER_WARRANTY_TYPE'] == 3) {
		//       if ($value['PS_FIELDNAME'] == 'Insurance Name') {
		//         $this->view->tinsuranceName =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
		//         $this->view->tPrincipalAgreement =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Amount') {
		//         $this->view->tinsurance_amount =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
		//         $this->view->tpaDateStart =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
		//         $this->view->tpaDateEnd =   $value['PS_FIELDVALUE'];
		//       }
		//     } else {

		//       if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
		//         $this->view->towner1 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Amount Owner') {
		//         $this->view->tamountowner1 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
		//         $this->view->towner2 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
		//         $this->view->tamountowner2 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
		//         $this->view->towner3 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
		//         $this->view->tamountowner3 =   $value['PS_FIELDVALUE'];
		//       }
		//     }
		//   }
		// }

		// $getGuarantedTransanctions = $this->_db->select()
		//   ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
		//   ->where('BG_REG_NUMBER = ?', $bgRegNumberDecrypt)
		//   ->query()->fetchAll();

		// $this->view->guarantedTransanctions = $getGuarantedTransanctions;

		// if (!empty($tbgdata['BG_REG_NUMBER'])) {
		//   $getGuarantedTransanctionsT = $this->_db->select()
		//     ->from('T_BANK_GUARANTEE_UNDERLYING')
		//     ->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
		//     ->query()->fetchAll();

		//   $this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;
		// }

		// if (!empty($tbgdata['BG_REG_NUMBER'])) {
		//   $checkOthersAttachmentT = $this->_db->select()
		//     ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
		//     ->where("BG_REG_NUMBER = ?", $tbgdata['BG_REG_NUMBER'])
		//     ->order('A.INDEX ASC')
		//     ->query()->fetchAll();

		//   $this->view->othersAttachmentT = $checkOthersAttachmentT;
		// }

		// if ($getDataBg['COUNTER_WARRANTY_TYPE'] == '1') {
		//   $bgdatasplit = $this->_db->select()
		//     ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
		//     ->where('A.BG_REG_NUMBER = ?', $bgRegNumberDecrypt)
		//     ->query()->fetchAll();

		//   foreach ($bgdatasplit as $key => $value) {
		//     $temp_save = $this->_db->select()
		//       ->from("M_CUSTOMER_ACCT")
		//       ->where("ACCT_NO = ?", $value["ACCT"])
		//       ->query()->fetchAll();

		//     $bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
		//     $bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
		//   }

		//   $bgdatasplitT = $this->_db->select()
		//     ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
		//     ->where('A.BG_NUMBER = ?', $getDataBg['BG_OLD'] ? $getDataBg['BG_OLD'] : '')
		//     ->query()->fetchAll();

		//   foreach ($bgdatasplitT as $key => $value) {
		//     $temp_save = $this->_db->select()
		//       ->from("M_CUSTOMER_ACCT")
		//       ->where("ACCT_NO = ?", $value["ACCT"])
		//       ->query()->fetchAll();

		//     $bgdatasplitT[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
		//     $bgdatasplitT[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
		//   }

		//   $this->view->fullmemberA = $bgdatasplit[0];
		//   $this->view->fullmemberT = $bgdatasplitT[0];
		// }

		$bgdatadetail = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('A.BG_REG_NUMBER = ?', $decryption_bg)
			->query()->fetchAll();

		if (!empty($bgdatadetail)) {
			foreach ($bgdatadetail as $key => $value) {

				if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
					if ($value['PS_FIELDNAME'] == 'Insurance Name') {
						$this->view->insuranceName =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
						$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount') {
						$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
						$this->view->paDateStart =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
						$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
					}
				} else {

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
						$this->view->owner1 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
						$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
						$this->view->owner2 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
						$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
						$this->view->owner3 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
						$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
					}
				}
			}
		}

		$principleData = [];
		if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
			foreach ($bgdatadetail as $key => $value) {
				$principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
			}

			$this->view->principleData = $principleData;
		}

		$get_linefacility = $this->_db->select()
			->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
			->where("CUST_ID = ?", $data["CUST_ID"])
			->query()->fetchAll();

		$this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
		$this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

		$this->view->linefacility = $get_linefacility[0];

		$this->view->acct = $data['FEE_CHARGE_TO'];

		$conf = Zend_Registry::get('config');
		$this->view->bankname = $conf['app']['bankname'];

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
		$AccArr = $CustomerUser->getAccountsBG($param);
		//var_dump($AccArr);die;

		if (!empty($AccArr)) {
			$this->view->src_name = $AccArr['0']['ACCT_NAME'];
		}

		$get_cash_collateral = $this->_db->select()
			->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
			->where("CUST_ID = ?", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$this->view->cash_collateral = $get_cash_collateral[0];

		$bgpublishType     = $conf["bgpublish"]["type"]["desc"];
		$bgpublishCode     = $conf["bgpublish"]["type"]["code"];

		$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

		$arrBankFormat = array(
			1 => 'Bank Standard',
			2 => 'Special Format (with bank approval)'
		);

		$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
		$this->view->bankFormatNumber = $data['BG_FORMAT'];

		$arrLang = array(
			1 => 'Indonesian',
			2 => 'English',
			3 => 'Billingual',
		);

		$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

		$checkOthersAttachment = $this->_db->select()
			->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
			->where("BG_REG_NUMBER = '268312345601F4201'")
			->order('A.INDEX ASC')
			->query()->fetchAll();

		if (count($checkOthersAttachment) > 0) {
			$this->view->othersAttachment = $checkOthersAttachment;
		}

		// Get data T_BANK_GUARANTEE_HISTORY
		$select = $this->_db->select()
			->from(
				array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
				array('*')
			)
			->where('TBGH.BG_REG_NUMBER = ?', $decryption_bg)
			->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
		$dataHistory = $this->_db->fetchAll($select);

		// Get data TEMP_BANK_GUARANTEE_SPLIT
		$select = $this->_db->select()
			->from(
				array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
				array('*')
			)
			->where('TBGS.BG_REG_NUMBER = ?', $decryption_bg);
		$dataAccSplit = $this->_db->fetchAll($select);

		$config = Zend_Registry::get('config');

		$docTypeCode = $config["bgdoc"]["type"]["code"];
		$docTypeDesc = $config["bgdoc"]["type"]["desc"];
		$docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

		$statusCode = $config["bg"]["status"]["code"];
		$statusDesc = $config["bg"]["status"]["desc"];
		$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

		$historyStatusCode = $config["history"]["status"]["code"];
		$historyStatusDesc = $config["history"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

		$counterTypeCode = $config["bgcg"]["type"]["code"];
		$counterTypeDesc = $config["bgcg"]["type"]["desc"];
		$counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

		//$config    		= Zend_Registry::get('config');
		$BgType     = $config["bg"]["status"]["desc"];
		$BgCode     = $config["bg"]["status"]["code"];

		$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

		$this->view->arrStatus = $arrStatus;


		$this->view->data               = $data;
		//Zend_Debug::dump($data);
		$this->view->dataHistory        = $dataHistory;
		$this->view->dataAccSplit       = $dataAccSplit;
		$this->view->docTypeArr         = $docTypeArr;
		$this->view->statusArr          = $statusArr;
		$this->view->historyStatusArr   = $historyStatusArr;
		$this->view->counterTypeArr     = $counterTypeArr;

		$download = $this->_getParam('download');
		if ($download == 1) {
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
		}

		Application_Helper_General::writeLog('OVBG', $this->language->_('Lihat Detail Bank Garansi Vendor BG No') . " : " . $data['BG_NUMBER']);
	}

	public function getstatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$tblName = $this->_getParam('id');

		$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

		$config        = Zend_Registry::get('config');
		$BgType     = $config["bg"]["status"]["desc"];
		$BgCode     = $config["bg"]["status"]["code"];

		$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

		foreach ($arrStatus as $key => $row) {
			if ($key == 16 || $key == 15) {
				if ($tblName == $key) {
					$select = 'selected';
				} else {
					$select = '';
				}
				$optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
			}
		}

		echo $optHtml;
	}
}
