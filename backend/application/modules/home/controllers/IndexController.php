<?php

class Home_IndexController extends Application_Main {

	public function initController()
	{
		$this->_helper->layout->setLayout('newlayout');
	}

	public function indexAction()
    { 
    	$privi = $this->_priviId;

    	$set = new Settings();

		$timeoutpwd = $set->getSetting('pwd_expired_day');
		$daybefore = 5;

		$modelHome = new home_Model_Home();
		$result = $modelHome->checkIsRequireChangePwd($this->_userIdLogin,$timeoutpwd,$daybefore);

		if(is_array($result) && count($result) > 0){
			$datetime 	= $result["datetime"];
			$dateadd 	= Application_Helper_General::convertDate($result["dateadd"],$this->view->displayDateFormat,$this->view->defaultDateFormat);

			$this->view->MSGBOX = 1;
			$this->view->dateadd 		= $dateadd;
		}else $this->view->MSGBOX = 0;


    	$modelHome = new home_Model_Home();

    	$select = $this->_db->select()
								->from(	'M_NOTIFICATION',
										array('*'));
		$select->where("DATE (EFDATE) <= DATE(NOW())");
        #$select->where("TIME (EFDATE) <= TIME(NOW())");
        $select->where("DATE (EXP_DATE) >= DATE(NOW())");
		$select->where("TARGET IN ('2','3')");
		#$select->where("TIME (EXP_TIME) >= TIME(NOW())");
		// echo $select;die;
		$notif = $this->_db->fetchAll($select);
		foreach ($notif as $key => $value) {
            $not[] = $value['ID'];
        }

        // print_r($not);die;


        $selectforce = $this->_db->select()
                                ->from( 'M_BFORCE_NOTIF',
                                        array('NOTIF_ID'));
        // $select->where("DATE (EFDATE) <= DATE(NOW())");
        #$select->where("TIME (EFDATE) <= TIME(NOW())");
        $selectforce->where("USER_ID = ? ",$this->_userIdLogin);
        $selectforce->where("NOTIF_ID IN (?)",$not);
        // $select->where("TARGET IN ('1','3')");
        #$select->where("TIME (EXP_TIME) >= TIME(NOW())");
       // echo $selectforce;die;
        $forcenotif = $this->_db->fetchAll($selectforce);
		// print_r($notif);die;
		if(!empty($notif)){
			$this->view->notif = $notif;
			$this->view->forcenotif = $forcenotif;
		}


    	if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
			$listAutModuleArr = $changeModulePrivilegeObj->getAuthorizeModule() ;

			$whPriviID = "'".implode("','", $listAutModuleArr)."'";

			$result = $modelHome->getChangesData($whPriviID);

			$waitingAutorization = array();
			foreach($result as $row)
			{
				if(isset($waitingAutorization[$row['DISPLAY_TABLENAME']]))
				{
					$waitingAutorization[$row['DISPLAY_TABLENAME']] += 1;
				}
				else
				{
					$waitingAutorization[$row['DISPLAY_TABLENAME']] = 1;
				}
			}
			$this->view->waitingAutorization = $waitingAutorization;
		}
    }


        public function savenotifAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $param = array(
                'USER_ID' => $this->_userIdLogin,
                'NOTIF_ID' => $tblName,
                'CREATED' => new Zend_Db_Expr("now()")
            );
        // try {
        	$this->_db->insert('M_BFORCE_NOTIF',$param);
        // } catch (Exception $e) {
        	// return $e;
        // }


        // echo $optHtml;
    }
}
