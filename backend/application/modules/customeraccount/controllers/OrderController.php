<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class customeraccount_OrderController extends customeraccount_Model_Customeraccount{ 


  public function indexAction() 
  {
    $this->_helper->layout()->setLayout('newlayout');
    
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password));
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
    $error_msg = null;
    

    if($cust_id)
    {
      //customer 
  	  $select = $this->_db->select()
  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'))
  	                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $result = $this->_db->fetchRow($select);
      if(!$result['CUST_ID']){ $cust_id = null; }
      else{ $this->view->cust_name = $result['CUST_NAME']; }
      
    }


    if(!$cust_id)
    {
      $error_remark = $this->getErrorRemark('22','Customer ID');
      //insert log
      try 
      {
	    $this->_db->beginTransaction();
        $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
        $this->_db->commit();
	  }
	  catch(Exception $e) 
      {
	    $this->_db->rollBack();
//   	    SGO_Helper_GeneralLog::technicalLog($e);
	  }
        
	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
    }
    
     
  	  
    
    
     if($this->_request->isPost())
     {
        $count = $this->_getParam('count');
        
        
        for($i=1; $i<=$count; $i++)
        {
           $acct_order  = $this->_getParam('acct_order'.$i);
           $explode_acct_order = explode('_',$acct_order);
           $acct    = $explode_acct_order[0];
           $orderNo = $explode_acct_order[1];
           
          //update record account order
		  $updateArr['ORDER_NO'] = $orderNo;
		  
		  $whereArr = array('CUST_ID = ?' => (string)$cust_id,
		                    'ACCT_NO = ?' => (string)$acct);
		  
		  $customerupdate = $this->_db->update('M_CUSTOMER_ACCT',$updateArr,$whereArr);
		  
		  //log CRUD
		  Application_Helper_General::writeLog('ACOR','Bank Account has been Ordered, Cust ID : '.$cust_id. ' Cust Name : '.$result['CUST_NAME']);
		  
        }
        
        $error_msg = 'Account Order Updated';
      }
    
      //QUERY customer acct
  	  $select = $this->_db->select()
  	                         ->from('M_CUSTOMER_ACCT',array('ACCT_NO','ACCT_NAME'))
  	                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
  	                         ->order(array('ORDER_NO ASC'));
      $result = $this->_db->fetchAll($select);
     
     $this->view->cust_acct = $result; 
     
     
     $this->view->error_msg  = $error_msg;
     $this->view->cust_id    = $cust_id;
     $this->view->modulename = $this->_request->getModuleName();
    
     
     
     
    //insert log
	try 
	{
	  $this->_db->beginTransaction();
	  Application_Helper_General::writeLog('ACOR','');
	  $this->_db->commit();
	}
    catch(Exception $e){
 	  $this->_db->rollBack();
	  SGO_Helper_GeneralLog::technicalLog($e);
	}
  }
}