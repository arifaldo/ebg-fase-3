<?php

require_once 'Zend/Controller/Action.php';

class Atm_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}


    	$this->view->report_msg = array();

		if(!$this->_request->isPost())
		{
			$id = $this->_getParam('id');
			//$bank_id = (Zend_Validate::is($bank_id,'Digits'))? $bank_id : null;

			if($id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_ATM'))
									 ->where("ATM_ID=?", $id)
							                   );
			  if($resultdata)
			  {
			        $this->view->atm_code        = $resultdata['ATM_CODE'];
					$this->view->atm_address      = $resultdata['ATM_ADDRESS'];
					$this->view->city_name      = $resultdata['ATM_CITY'];
			  }
			}
			else
			{
			   $error_remark = 'ATM not found';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   //$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			   $this->_redirect('/'.$this->_request->getModuleName().'/index');
			}
		}
		else
		{
			$filters = array(
							 'atm_code' => array('StringTrim','StripTags'),

							 'atm_address' => array('StringTrim','StripTags'),
            			    'city_name' => array('StringTrim','StripTags'),

							);

			$validators = array(

								'atm_code'      => array('NotEmpty',
													 'messages' => array(
																		 $this->language->_('Can not be empty')
																         )
														),
								'atm_address'      => array('NotEmpty',
													 'messages' => array(
																		 $this->language->_('Can not be empty')
																         )
														),
                			    'city_name'      => array('NotEmpty',
                                    			        'messages' => array(
                                    			            $this->language->_('Can not be empty')
                                    			        )
                                    			    ),
							   );

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//Zend_Debug::Dump($this->_request->getParams());die;

			if($zf_filter_input->isValid())
			{
				$content = array(
								'ATM_ADDRESS' 	 => $zf_filter_input->atm_address,
            				    'ATM_CITY' 	 => $zf_filter_input->city_name,
						       );

				try
				{
				    //-----insert--------------
					$this->_db->beginTransaction();

					$whereArr  = array('ATM_CODE = ?'=>$zf_filter_input->atm_code);
					$this->_db->update('M_ATM',$content,$whereArr);

					$this->_db->commit();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					Application_Helper_General::writeLog('COUD','Edit ATM. ATM Code : ['.$zf_filter_input->atm_code.']');
					$this->view->success = true;
					$this->view->report_msg = array();

					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

				    foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);
					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
	}

}
