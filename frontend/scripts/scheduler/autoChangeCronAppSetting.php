<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
$db = Zend_Db_Table::getDefaultAdapter();

echo "<pre>";

$newAppID     = (array_key_exists(1, $argv)) ? $argv[1] : "";
echo "newAppID: $newAppID. ";

if (empty($newAppID))
{	echo "  newAppID is empty";	die();		}

$update = $settingAPP->setSetting('cron_app', $newAppID);  

$filename = basename(__FILE__);
$cronResult = "$update row.";
Application_Helper_General::cronLog($filename, $cronResult);

echo "<br>".$cronResult;
?>