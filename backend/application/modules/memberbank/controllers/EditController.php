<?php

require_once 'Zend/Controller/Action.php';

class Memberbank_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
	    //pengaturan url untuk button back
			$this->_helper->layout()->setLayout('newlayout');
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

		$this->view->report_msg = array();

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}


		if(!$this->_request->isPost())
		{
			$id = $this->_getParam('id');
			$ccy = $this->_getParam('bccy');
			$bankcode = $this->_getParam('bbank_code');
			$benefbankcode = $this->_getParam('benefbank_code');


			if(!empty($ccy) && !empty($bankcode) && !empty($benefbankcode))
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('A'=>'M_MEMBER_BANK'),array('ID','CCY','BANK_CODE','COUNTRY_NAME','NOSTRO_NAME','NOSTRO_SWIFTCODE','BANK_NAME','COUNTRY_CODE'))

									 // ->where("CCY = ?", $ccy)
									 // ->where("NOSTRO_SWIFTCODE = ?", $bankcode)
									 // ->where("BANK_CODE = ?", $benefbankcode)
									->where("ID = ?", $id)
							);

			  if($resultdata)
			  {
			  		$this->view->id = $resultdata['ID'];
			        $this->view->ccy  = $resultdata['CCY'];
			        //$this->view->old_ccy  = $resultdata['CCY'];
					$this->view->country_name      = $resultdata['COUNTRY_NAME'];
					$this->view->country_code      = $resultdata['COUNTRY_CODE'];
					$this->view->benef_bank  = $resultdata['BANK_CODE'];
					$this->view->benef_bank_name  = $resultdata['BANK_NAME'];
					$this->view->bank_code   	= $resultdata['BANK_CODE'];
					//$this->view->old_bank_code   	= $resultdata['BANK_CODE'];
					$this->view->nostro_name   	= $resultdata['NOSTRO_NAME'];
					$this->view->nostro_swiftcode   	= $resultdata['NOSTRO_SWIFTCODE'];
					//$this->view->old_nostro_swiftcode   	= $resultdata['NOSTRO_SWIFTCODE'];
			  }
			}
			else
			{
			   $error_remark = 'CCY & Bank Code not found';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/index');
			}
		}
		else
		{
			$filters = array(
							 'id'     => array('StringTrim','StripTags','StringToUpper'),
							 'ccy'     => array('StringTrim','StripTags','StringToUpper'),
							 'country_name' => array('StringTrim','StripTags','StringToUpper'),
							 'country_code' => array('StringTrim','StripTags','StringToUpper'),
							 'benef_bank_name'    => array('StringTrim','StripTags','StringToUpper'),
							 'bank_code'    => array('StringTrim','StripTags','StringToUpper'),
							 'nostro_name'     => array('StringTrim','StripTags','StringToUpper'),
							 'nostro_swiftcode'=> array('StringTrim','StripTags','StringToUpper'),
							);

			$validators = array(
								'id' => array('NotEmpty',
												'messages' => array(
														$this->language->_('Can not be empty'),
												)
								),
								'ccy' => array('NotEmpty',
													 'messages' => array(
																         $this->language->_('Can not be empty'),
			                                                             )
													),
								'country_code' => array('NotEmpty',
														'messages' => array(
																$this->language->_('Can not be empty'),
														)
								),
								'country_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>128)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Data too long (max 128 chars)')
																         )
														),
								'benef_bank_name'      => array('NotEmpty',
													 //new Zend_Validate_StringLength(array('max'=>8)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		//$this->language->_( 'Data too long (max 8 chars)')
																         )
														),
								'bank_code'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>8)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Bank Code too long (max 8 chars)')
																         )
														),
								'nostro_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>50)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Data too long (max 50 chars)')
																         )
														),
								'nostro_swiftcode' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>11)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																		 $this->language->_( 'Data too long (max 11 chars)')
			                                                             )
													),
							   );

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				$id = $zf_filter_input->id;
				$newccy = $zf_filter_input->ccy;
				$newswift = $zf_filter_input->nostro_swiftcode;
				$newbankcode = $zf_filter_input->bank_code;

				$check = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('A'=>'M_MEMBER_BANK'))
									 ->where("CCY = ?", $newccy)
									 ->where("NOSTRO_SWIFTCODE = ?", $newswift)
									 ->where("BANK_CODE = ?", $newbankcode)
									 ->where("ID != ?", $id)
				);

				if(empty($check))
					$checkval = true;
				else
					$checkval = false;

				// $oldccy = $this->_getParam('old_ccy');
				// $newccy = $zf_filter_input->ccy;

				// $oldswift = $this->_getParam('old_nostro_swiftcode');
				// $newswift = $zf_filter_input->nostro_swiftcode;

				// $oldbankcode = $this->_getParam('old_bank_code');
				// $newbankcode = $zf_filter_input->bank_code;

				// if(($oldccy !== $newccy) || ($oldswift !== $newswift) || ($oldbankcode !== $newbankcode)){
				// 	$check = $this->_db->fetchRow(
				// 				$this->_db->select()
				// 					 ->from(array('A'=>'M_MEMBER_BANK'),array('CCY','BANK_CODE','COUNTRY_NAME','NOSTRO_NAME','NOSTRO_SWIFTCODE','BANK_NAME','COUNTRY_CODE'))

				// 					 ->where("CCY = ?", $newccy)
				// 					 ->where("NOSTRO_SWIFTCODE = ?", $newswift)
				// 					 ->where("BANK_CODE = ?", $newbankcode)
				// 	);

				// 	if(empty($check))
				// 		$checkval = true;
				// 	else
				// 		$checkval = false;
				// }
				// else{
				// 	$checkval = true;
				// }


				if($checkval){
					$content = array(
									'CCY' => $zf_filter_input->ccy,
									'NOSTRO_NAME' => $zf_filter_input->nostro_name,
									'NOSTRO_SWIFTCODE' => $zf_filter_input->nostro_swiftcode,
									'COUNTRY_CODE' => $zf_filter_input->country_code,
									'COUNTRY_NAME'	 => $zf_filter_input->country_name,
									'BANK_CODE' => $zf_filter_input->bank_code,
									'BANK_NAME' => $zf_filter_input->benef_bank_name,
									'UPDATED'	=> date('Y-m-d H:i:s'),
									'UPDATEDBY'	=> $this->_userIdLogin
							       );

					try
					{
					    //-----insert--------------
						$this->_db->beginTransaction();

						$whereArr  = array('ID = ?'=>$id);
						$this->_db->update('M_MEMBER_BANK',$content,$whereArr);

						$this->_db->commit();

						foreach(array_keys($filters) as $field)
							$this->view->$field = $zf_filter_input->getEscaped($field);

						Application_Helper_General::writeLog('NBUD','Edit Memberbank Bank. CCY : ['.$zf_filter_input->ccy.'] , Beneficiary Bank : ['.$zf_filter_input->benef_bank_name.']');
						$this->view->success = true;
						$this->view->report_msg = array();

						$this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();

					    foreach(array_keys($filters) as $field)
							$this->view->$field = $zf_filter_input->getEscaped($field);

						// $this->view->old_ccy = $this->_getParam('old_ccy');
						// $this->view->old_bank_code = $this->_getParam('old_bank_code');
						// $this->view->old_nostro_swiftcode = $this->_getParam('old_nostro_swiftcode');

						$benefbankname = $zf_filter_input->getEscaped('benef_bank_name');
						$benefbankcode = $zf_filter_input->getEscaped('bank_code');
						$this->view->benef_bank = $benefbankname." - ".$benefbankcode;

						$errorMsg = 'exception';
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					}
				}
				else{
					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					// $this->view->old_ccy = $this->_getParam('old_ccy');
					// $this->view->old_bank_code = $this->_getParam('old_bank_code');
					// $this->view->old_nostro_swiftcode = $this->_getParam('old_nostro_swiftcode');

					$benefbankname = $zf_filter_input->getEscaped('benef_bank_name');
					$benefbankcode = $zf_filter_input->getEscaped('bank_code');
					$this->view->benef_bank = $benefbankname." - ".$benefbankcode;

					$errorMsg = 'Error : Member bank has been registered';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();

				$benefbankname = ($zf_filter_input->isValid('benef_bank_name'))? $zf_filter_input->getEscaped('benef_bank_name') : $this->_getParam('benef_bank_name');
				$benefbankcode = ($zf_filter_input->isValid('bank_code'))? $zf_filter_input->getEscaped('bank_code') : $this->_getParam('bank_code');
				$this->view->benef_bank = $benefbankname." - ".$benefbankcode;

				// $this->view->old_ccy = $this->_getParam('old_ccy');
				// $this->view->old_bank_code = $this->_getParam('old_bank_code');
				// $this->view->old_nostro_swiftcode = $this->_getParam('old_nostro_swiftcode');

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->success = false;
                $this->view->report_msg = $errorArray;
			}
		}
	}

}
