<?php

class holidaysetting_IndexController extends Application_Main
{

    protected $_moduleDB  = 'HTB';

    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newlayout');

        $getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;

        $sqlQueryHoliday = $this->_db->select()
            ->from(array('B' => 'M_HOLIDAY'), array(
                'HOLIDAY_ID',
                'day' => new Zend_Db_Expr("DATE_FORMAT(B.HOLIDAY_DATE, '%d')"),
                'month' => new Zend_Db_Expr("DATE_FORMAT(B.HOLIDAY_DATE, '%m')"),
                'year' => 'YEAR(B.HOLIDAY_DATE)',
                'HOLIDAY_YEAR' => 'B.HOLIDAY_YEAR',
                'HOLIDAY_DATE' => 'B.HOLIDAY_DATE',
                'HOLIDAY_DESCRIPTION' => 'B.HOLIDAY_DESCRIPTION',
                'HOLIDAY_CCY' => 'B.HOLIDAY_CCY'
            ))
            ->order('B.HOLIDAY_DATE ASC');

        $listHoliday = $this->_db->fetchAll($sqlQueryHoliday);
        // echo "<pre>";
        // var_dump($listHoliday);die;


        $this->view->listHoliday = $listHoliday;
        $this->view->filter = "0";

        $filter = $this->_getParam("filter");
        $err = $this->_getParam("error");
        $currentyear = $this->_getParam("cy");

        $this->view->err = $err;


        if ($filter) {
            if ($filter == '0') {
                $select2 = $listHoliday;
            } else {
                $select2 = $this->_db->select()
                    ->from(array('B' => 'M_HOLIDAY'), array(
                        'day' => new Zend_Db_Expr("DATE_FORMAT(B.HOLIDAY_DATE, '%d')"),
                        'month' => new Zend_Db_Expr("DATE_FORMAT(B.HOLIDAY_DATE, '%m')"),
                        'year' => 'YEAR(B.HOLIDAY_DATE)',
                        'HOLIDAY_YEAR' => 'B.HOLIDAY_YEAR',
                        'HOLIDAY_DATE' => 'B.HOLIDAY_DATE',
                        'HOLIDAY_DESCRIPTION' => 'B.HOLIDAY_DESCRIPTION',
                        'HOLIDAY_CCY' => 'B.HOLIDAY_CCY'
                    ))
                    ->where('B.HOLIDAY_CCY =?', $filter)
                    ->order('B.HOLIDAY_DATE ASC')
                    ->query()->fetchAll();

                // echo "<pre>";
                // var_dump($select2);die;
            }
            $this->view->listHoliday = $select2;
            $this->view->filter = $filter;
        }

        $this->view->currentyear = $currentyear;

        // if($this->_request->isPost() && $this->view->hasPrivilege('HDUD')){
        if ($this->_request->isPost()) {

            $this->_db->beginTransaction();

            $holiday_date_arr = explode('~|~', $this->_getParam("dates"));
            $holiday_desc_arr = explode('~|~', $this->_getParam("datesdes"));
            $holiday_date_id = explode('~|~', $this->_getParam("datesid"));
            $holidayccy_ccyid_arr = explode('~|~', $this->_getParam("ccyid"));
            $year = $this->_getParam("selectyear");
            $remdates = explode('~|~', $this->_getParam("remdates"));
            // var_dump($year);
            // var_dump($remdates);

            // filter by year
            $i = 0;
            foreach ($holiday_date_arr as $c) {
                $datafilter[] = array(
                    'HOLIDAY_DATE' => $c,
                    'HOLIDAY_DESCRIPTION' => $holiday_desc_arr[$i],
                    'HOLIDAY_CCY' => $holidayccy_ccyid_arr[$i],
                    'HOLIDAY_ID' => $holiday_date_id[$i]
                );
                $i++;
            }

            // echo "<pre>";
            // print_r(array_filter($datafilter, function ($var) use ($year) { return (stripos($var['HOLIDAY_DATE'], $year) !== false); }));

            $newarray = array_filter($datafilter, function ($var) use ($year) {
                return (stripos($var['HOLIDAY_DATE'], $year) !== false);
            });
            // $newarray = $datafilter;

            // echo "<pre>";
            // var_dump($newarray);die();

            // filter by remove date
            if ($remdates) {
                // $newarray = array_filter($newarray, function ($dates) use ($remdates) {
                $newarray = array_filter($newarray, function ($dates) use ($remdates) {
                    // return !in_array($dates['HOLIDAY_DATE'], $remdates);
                    return !in_array($dates['HOLIDAY_ID'], $remdates);
                });
            }

            //var_dump($newarray);die;
            // check same value in db
            $sCurrent = $this->_db->select()
                ->from(array('B' => 'M_HOLIDAY'), array('HOLIDAY_DATE', 'HOLIDAY_DESCRIPTION', 'HOLIDAY_CCY'))
                ->where('B.HOLIDAY_YEAR =?', $year)
                ->query()->fetchAll();


            if (count($sCurrent) == count($newarray)) {

                // $duplicateList1 = array();
                // foreach($sCurrent as $c => $v){
                //     foreach($newarray as $k => $value){
                //         if($v['HOLIDAY_DATE'] == $value['HOLIDAY_DATE'] && $v['HOLIDAY_DESCRIPTION'] == $value['HOLIDAY_DESCRIPTION'] && $v['HOLIDAY_CCY'] == $value['HOLIDAY_CCY']){
                //             $duplicate1 = array(
                //                 'date' => $v['HOLIDAY_DATE'],
                //                 'desc' => $v['HOLIDAY_DESCRIPTION'],
                //                 'ccy' => $v['HOLIDAY_CCY']
                //             );
                //             array_push($duplicateList1, $duplicate1);
                //         }
                //     }
                // }
                // if(count($duplicateList1) == count($sCurrent)){
                //     $this->_redirect('/holidaysetting/index/index/error/2');
                // }
                $checkArray = $newarray;
                foreach ($sCurrent as $k => $vv) {
                    $ll = array(
                        'HOLIDAY_DATE' => $vv['HOLIDAY_DATE'],
                        'HOLIDAY_DESCRIPTION' => $vv['HOLIDAY_DESCRIPTION'],
                        'HOLIDAY_CCY' => $vv['HOLIDAY_CCY']
                    );
                    array_push($checkArray, $ll);
                }

                $unique = array_unique($checkArray, SORT_REGULAR);

                $diffCellUniq = array_diff_key($checkArray, $unique);

                //if(count($sCurrent) == count($diffCellUniq)){
                //   $this->_redirect('/holidaysetting/index/index/error/2');
                //}

                // echo "<pre>";
                // var_dump($diffCellUniq);
                // echo "###";
                // var_dump($checkArray);die;
            }


            $list = array();
            $duplicateList = array();
            foreach ($newarray as $key => $value) {
                foreach ($list as $ll => $vv) {
                    if ($vv['HOLIDAY_DATE'] == $value['HOLIDAY_DATE'] && $vv['HOLIDAY_DESCRIPTION'] == $value['HOLIDAY_DESCRIPTION'] && $vv['HOLIDAY_CCY'] == $value['HOLIDAY_CCY']) {
                        $duplicate = array(
                            'date' => $vv['HOLIDAY_DATE'],
                            'desc' => $vv['HOLIDAY_DESCRIPTION'],
                            'ccy' => $vv['HOLIDAY_CCY']
                        );
                        array_push($duplicateList, $duplicate);
                    }
                }
                $listArray = array(
                    'HOLIDAY_DATE' => $value['HOLIDAY_DATE'],
                    'HOLIDAY_DESCRIPTION' => $value['HOLIDAY_DESCRIPTION'],
                    'HOLIDAY_CCY' => $value['HOLIDAY_CCY']
                );

                array_push($list, $listArray);
            }
            // var_dump($duplicateList);die;
            // var_dump($newarray);

            if (count($duplicateList) > 0) {
                $this->_redirect('/holidaysetting/index/index/error/1');
            } else {
                // insert ke T_GLOBAL_CHANGES
                if (is_array($newarray)) {
                    try {
                        // validate
                        // var_dump($newarray);die;
                        // foreach($newarray as $rows => $val){
                        // $currentD = $this->_db->select()
                        // 		->from(array('C' => 'M_HOLIDAY'));
                        //         ->where("HOLIDAY_DATE = ?",$val['HOLIDAY_DATE']);
                        //         ->where("HOLIDAY_CCY = ?",$val['HOLIDAY_DATE']);
                        // $currentD = $this->_db->fetchAll($currentD);
                        // foreach($currentD as $row => $curr){
                        //     if($curr['HOLIDAY_DESCRIPTION'] == $val['HOLIDAY_DESCRIPTION']){
                        //         echo "same data ";die;
                        //     }
                        // }
                        // $dataValidate = array($val['HOLIDAY_DATE'],)
                        // }
                        $info = "";
                        $change_id = $this->suggestionWaitingApproval('Holiday Setting', $info, $this->_changeType['code']['edit'], null, 'M_HOLIDAY', 'TEMP_HOLIDAY', '', $year);

                        $j = 0;

                        foreach ($newarray as $key => $value) {

                            $data = array(
                                'CHANGES_ID' => $change_id,
                                'HOLIDAY_YEAR' => $year,
                                'HOLIDAY_DATE' => $value['HOLIDAY_DATE'],
                                'HOLIDAY_DESCRIPTION' => $value['HOLIDAY_DESCRIPTION'],
                                'HOLIDAY_CCY' => $value['HOLIDAY_CCY'],
                                'SUGGESTEDBY' => $this->_userIdLogin,
                                'SUGGESTED' => new Zend_Db_Expr('now()'),
                            );
                            // var_dump($data);die;
                            $this->_db->insert('TEMP_HOLIDAY', $data);
                            $j++;
                        }

                        $rowUpdated = $this->_db->update(
                            'T_GLOBAL_CHANGES',
                            array(
                                'KEY_FIELD' => $year,
                                'KEY_VALUE' => '',
                            ),
                            $this->_db->quoteInto('CHANGES_ID = ?', $change_id)
                        );
                        Application_Helper_General::writeLog('HDUD', 'Update Holiday Setting Data: ' . $year);
                        $this->_db->commit();
                        $this->view->success = true;
                        $msg = 'Holiday changes saved';
                        $this->setbackURL('/holidaysetting');
                        $this->_redirect('/notification/submited/index');
                        $this->view->report_msg = $msg;
                    } catch (Exception $e) {
                        $this->_db->rollBack();
                        var_dump($e);
                        die;
                    }
                }
            }
        }

        $sqlCheckTemp = $this->_db->select()
            ->from(array('B' => 'TEMP_HOLIDAY'), array('*'));
        $listTemp = $this->_db->fetchAll($sqlCheckTemp);

        $sqlCheckTempChange = $this->_db->select()
            ->from(array('B' => 'T_GLOBAL_CHANGES'), array('KEY_FIELD'))
            ->where('DISPLAY_TABLENAME =?', 'Holiday Setting')
            ->where('CHANGES_STATUS =?', 'WA');
        $listTempChange = $this->_db->fetchAll($sqlCheckTempChange);

        if (!empty($listTemp) || !empty($listTempChange)) {
            $tempcheck = true;
        } else {
            $tempcheck = false;
        }

        // $this->view->tempcek = $tempcheck;
        $this->view->listTempChange = $listTempChange;
        // var_dump($listTempChange);

        Application_Helper_General::writeLog('HDLS', 'View Holiday Setting');
    }

    public function checkduplicateAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $date = $this->_getParam('date');
        $desc = $this->_getParam('desc');
        $ccy = $this->_getParam('ccy');

        $select = $this->_db->select()
            ->from(array('B' => 'M_HOLIDAY'), array('*'))
            ->where('DATE(HOLIDAY_DATE) = ?', $date)
            ->where('HOLIDAY_DESCRIPTION = ?', $desc)
            ->where('HOLIDAY_CCY = ?', $ccy);

        $data = $this->_db->fetchRow($select);

        // $data = $date.''.$desc.''.$ccy;
        if (empty($data)) {
            $result = '0';
        } else {
            $result = '1';
        }

        echo $result;
    }
}
