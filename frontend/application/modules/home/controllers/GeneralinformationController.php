<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';

class home_GeneralinformationController extends Application_Main
{



	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$app_kurs = Zend_Registry::get('config');
		$app_kurs = $app_kurs['app']['bankcode'];

		$userId_kurs = $this->_userIdLogin;
		$custId_kurs = $this->_custIdLogin;

		$param_kurs = array('USER_ID' => $userId_kurs, 'CUST_ID' => $custId_kurs);

		$model_kurs = new accountstatement_Model_Accountstatement();

		$userAcctInfo_kurs = $model_kurs->getCustomerAccountKurs($param_kurs);
		//die;		

		$accountNo_kurs 	= $userAcctInfo_kurs[0]['ACCT_NO'];
		$accountType_kurs = $userAcctInfo_kurs[0]['ACCT_TYPE'];
		$accountCcy_kurs = $userAcctInfo_kurs[0]['CCY_ID'];
		//print_r($userAcctInfo_kurs );die;				
		// $svcInquiry_kurs = new Service_Inquiry($userId_kurs, $accountNo_kurs, $accountType_kurs, $accountCcy_kurs);
		// $result_kurs		= $svcInquiry_kurs->rateInquiry();

		$fields_kurs = array(
			'ccy'  => array(
				'field' => '',
				'label' => $this->language->_('CCY'),
				'sortable' => false
			),
			'sell'  => array(
				'field' => '',
				'label' => $this->language->_('Sell'),
				'sortable' => false
			),
			'buy'   => array(
				'field'    => '',
				'label'    => $this->language->_('Buy'),
				'sortable' => false
			),
		);

		// $data_kurs = array();

		// $data_kurs = $result_kurs['DataList'];

		//print_r($result_kurs);die;
		Application_Helper_General::writeLog('ERIQ', $logDesc);

		$this->view->userId_kurs = $this->_userIdLogin;
		// $this->view->resultdata_kurs = $data_kurs;
		$this->view->fields_kurs = $fields_kurs;




		// bank table
		$fields_bank = array(
			'bank_name'      => array(
				'field' => 'BANK_NAME',
				'label' => $this->language->_('Bank Name'),
				'sortable' => true
			),
			/* 	'city'           => array('field' => 'CITY_CODE',
'label' => 'City',
'sortable' => true), */
			'clearing_code'  => array(
				'field' => 'CLR_CODE',
				'label' => $this->language->_('Clearing Code'),
				'sortable' => true
			),
			/*'swift_code'     => array('field' => 'SWIFT_CODE',
'label' => 'Swift Code',
'sortable' => true)*/
		);


		//get page, sortby, sortdir
		$page_bank    = $this->_getParam('page');
		$sortBy_bank  = $this->_getParam('sortby', 'doc_no');
		$sortDir_bank = $this->_getParam('sortdir', 'asc');

		$csv_bank = $this->_getParam('csv');

		//validate parameters before passing to view and query
		$page_bank = (Zend_Validate::is($page_bank, 'Digits') && ($page_bank > 0)) ? $page_bank : 1;

		$sortBy_bank = (Zend_Validate::is(
			$sortBy_bank,
			'InArray',
			array(array_keys($fields_bank))
		)) ? $fields_bank[$sortBy_bank]['field'] : $fields_bank[key($fields_bank)]['field'];

		$sortDir_bank = (Zend_Validate::is(
			$sortDir_bank,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir_bank : 'asc';


		//get filtering param
		$allNum_bank = new Zend_Filter_Alnum(true);

		$filterArr_bank = array(
			'filter' 	  	 => array('StringTrim', 'StripTags'),
			//'clearing_code'  => array('StringTrim','StripTags'),
			'city'           => array('StringTrim', 'StripTags', 'StringToUpper'),
			'bank_name'      => array('StringTrim', 'StripTags'),
			//'swift_code'     => array('StringTrim','StripTags')
		);


		$zf_filter_bank = new Zend_Filter_Input($filterArr_bank, array(), $this->_request->getParams());
		$filter_bank = $zf_filter_bank->getEscaped('filter');

		$this->view->currentPage_bank = $page_bank;
		$this->view->sortBy_bank      = $sortBy_bank;
		$this->view->sortDir_bank     = $sortDir_bank;

		// $this->view->cityArr = array_merge(array(''=>'--- select city ---'),$this->getCity());

		$frontendOptions = array(
			'lifetime' => 259200,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/acctbalance/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

		$cacheID = 'DOM_BANK';

		$select_bank = $cache->load($cacheID);

		//var_dump($select_bank);
		if (empty($select_bank) || $select_bank == NULL) {
			//die('gere');
			$select_bank = $this->_db->select()
				->from(array('A' => 'M_DOMESTIC_BANK_TABLE'), array(
					'BANK_NAME',
					// 					        		'CITY_CODE',
					'CLR_CODE'
				));

			//$select_bank->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));


			if ($filter_bank == TRUE) {
				//$fclearing_code = $zf_filter_bank->getEscaped('clearing_code');
				$fbank_name_bank     = $zf_filter_bank->getEscaped('bank_name');
				//$fswift_code  = $zf_filter_bank->getEscaped('swift_code');
				// $fcity          = $zf_filter_bank->getEscaped('city');



				//if($fclearing_code) $select_bank->where('UPPER(CLR_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fclearing_code).'%'));
				if ($fbank_name_bank)     $select_bank->where('BANK_NAME LIKE ' . $this->_db->quote('%' . $fbank_name_bank . '%'));
				//if($fswift_code)    $select_bank->where('UPPER(SWIFT_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fswift_code).'%'));
				// if($fcity)          $select_bank->where('UPPER(CITY) LIKE '.$this->_db->quote('%'.strtoupper($fcity).'%'));
				// if($fcity)          $select_bank->where('CITY_CODE LIKE '.$this->_db->quote('%'.$fcity.'%'));



				//$this->view->clearing_code = $fclearing_code;
				$this->view->bank_name     = $fbank_name_bank;
				//$this->view->swift_code    = $fswift_code;
				// $this->view->city          = $fcity;
			}

			//$this->view->success = true;
			$select_bank->order($sortBy_bank . ' ' . $sortDir_bank);

			$select_bank = $this->_db->fetchall($select_bank);
			//var_dump($select_int);
			$cache->save($select_bank, $cacheID);
		}

		//$select_bank = $cache->load($cacheID);
		//	var_dump($select_int);die('sini');
		//$select_bank = array();
		$header_bank = Application_Helper_Array::simpleArray($fields_bank, 'label');

		//--------konfigurasicsv dan pdf---------
		if ($csv_bank) {
			$this->_helper->download->csv($header_bank, $select_bank, null, 'bank_table');  //array('Bank Name','Location','Clearing Code')
			Application_Helper_General::writeLog('VDBT', 'Export CSV Domestic Bank Table');
		} else {
			Application_Helper_General::writeLog('VDBT', 'Viewing Domestic Bank Table');
		}
		//-------END konfigurasicsv dan pdf------------



		$this->paging($select_bank);
		$this->view->fields_bank = $fields_bank;
		$this->view->filter_bank = $filter_bank;


		// International Bank
		$bankcode_int = $this->language->_('Bank Code');
		$bankname_int = $this->language->_('Bank Name');
		$bankaddress1_int = $this->language->_('Bank Address') . "1";
		$bankaddress2_int = $this->language->_('Bank Address') . "2";
		$cityname_int = $this->language->_('City Name');
		$country_int = $this->language->_('Country');
		$pobnumber_int = $this->language->_('POB Number');
		$created_int = $this->language->_('Created Date');
		$created_intby = $this->language->_('Created By');
		$updated_int = $this->language->_('Updated Date');
		$updated_intby = $this->language->_('Updated By');

		// 		$model = new customer_Model_Customer();
		// 		echo 'here';die;
		//$selectcountry_int = $this->_db->select()
		//->from('M_COUNTRY')
		//->query()->fetchAll();
		// 	    $this->view->countryArr_int = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME'));


		$frontendOptions = array(
			'lifetime' => 259200,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/acctbalance/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		$cacheID = 'COUNTRY';

		$selectcountry_int = $cache->load($cacheID);
		//var_dump($selectcountry_int);die;
		if (empty($selectcountry_int)) {
			//die;
			$selectcountry_int = $this->_db->select()
				->from('M_COUNTRY')
				->query()->fetchAll();
			$cache->save($selectcountry_int, $cacheID);
		}

		$this->view->countryArr_int = $selectcountry_int;


		$fields_int = array(

			'bank_code'  => array(
				'field' => 'bank_code',
				'label' => $bankcode_int,
				'sortable' => true
			),
			'bank_name'     => array(
				'field' => 'bank_name',
				'label' => $bankname_int,
				'sortable' => true
			),
			'bank_address1'      => array(
				'field' => 'bank_address1',
				'label' => $bankaddress1_int,
				'sortable' => true
			),
			'bank_address2'      => array(
				'field' => 'bank_address2',
				'label' => $bankaddress2_int,
				'sortable' => true
			),
			'city_name'      => array(
				'field' => 'city_name',
				'label' => $cityname_int,
				'sortable' => true
			),
			'country_name'      => array(
				'field' => 'country_name',
				'label' => $country_int,
				'sortable' => true
			),
			'pob_number'      => array(
				'field' => 'pob_number',
				'label' => $pobnumber_int,
				'sortable' => true
			),
			// 						'created'      => array('field' => 'created',
			// 												      'label' => $created_int,
			// 												      'sortable' => true),
			// 						'createdby'      => array('field' => 'createdby',
			// 												      'label' => $created_intby,
			// 												      'sortable' => true),
			// 						'updated'      => array('field' => 'updated',
			// 												      'label' => $updated_int,
			// 												      'sortable' => true),
			// 						'updatedby'      => array('field' => 'updatedby',
			// 												      'label' => $updated_intby,
			// 												      'sortable' => true),
		);

		//get page, sortby, sortdir
		$page_int    = $this->_getParam('page');
		$sortBy_int  = $this->_getParam('sortby', 'bank_code');
		$sortDir_int = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page_int = (Zend_Validate::is($page_int, 'Digits') && ($page_int > 0)) ? $page_int : 1;

		$sortBy_int = (Zend_Validate::is(
			$sortBy_int,
			'InArray',
			array(array_keys($fields_int))
		)) ? $fields_int[$sortBy_int]['field'] : $fields_int[key($fields_int)]['field'];

		$sortDir_int = (Zend_Validate::is(
			$sortDir_int,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir_int : 'asc';

		//get filtering param
		$allNum_int = new Zend_Filter_Alnum(true);

		$filterArr_int = array(
			'filter' 	  	 => array('StringTrim', 'StripTags'),
			'bank_code'  => array('StringTrim', 'StripTags'),
			'bank_name'      => array('StringTrim', 'StripTags'),
			'country'     => array('StringTrim', 'StripTags')
		);

		$zf_filter_int = new Zend_Filter_Input($filterArr_int, array(), $this->_request->getParams());
		$filter = $zf_filter_int->getEscaped('filter');

		$this->view->currentPage_int = $page_int;
		$this->view->sortBy_int      = $sortBy_int;
		$this->view->sortDir_int     = $sortDir_int;

		$csv_int = $this->_getParam('csv');
		$pdf_int = $this->_getParam('pdf');
		//$filter_lg = $this->_getParam('filter');

		//$tempfields = $fields_int;
		//unset($tempfields['country_name']);
		//$getData = Application_Helper_Array::SimpleArray($tempfields,'field');






		$frontendOptions = array(
			'lifetime' => 259200,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/acctbalance/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		$cacheID = 'REMITTANCE_BANK';

		$select_int = $cache->load($cacheID);
		//var_dump($select_int);die;
		if (empty($select_int)) {
			//die;
			$select_int = $this->_db->select()
				->from(array('A' => 'M_BANK_REMITTANCE'), array('bank_code', 'bank_name', 'bank_address1', 'bank_address2', 'city_name', 'country_name' => 'B.COUNTRY_NAME', 'pob_number', 'country_code'))
				->join(array('B' => 'M_COUNTRY'), 'A.country_code = B.COUNTRY_CODE', array())
				->limit(100);




			//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv_int || $pdf_int)
			if ($filter_int == true || $csv_int || $pdf_int || $this->_request->getParam('print')) {
				//$tempfields['country_name'] = $fields_int['country_name'];
				$header_int = Application_Helper_Array::simpleArray($fields_int, 'label');

				$fbank_code_int = $zf_filter_int->getEscaped('bank_code');
				$fbank_name_int     = $zf_filter_int->getEscaped('bank_name');
				$fcountry_int    = $zf_filter_int->getEscaped('country');

				if ($fbank_code_int) $select_int->where('UPPER(bank_code) LIKE ' . $this->_db->quote('%' . strtoupper($fbank_code_int) . '%'));
				if ($fbank_name_int)     $select_int->where('UPPER(bank_name) LIKE ' . $this->_db->quote('%' . strtoupper($fbank_name_int) . '%'));
				if ($fcountry_int)    $select_int->where('UPPER(A.country_code) LIKE ' . $this->_db->quote('%' . strtoupper($fcountry_int) . '%'));

				$this->view->bank_code = $fbank_code_int;
				$this->view->bank_name  = $fbank_name_int;
				$this->view->country    = $fcountry_int;
			}

			//$this->view->success = true;
			$select_int->order($sortBy_int . ' ' . $sortDir_int);
			$select_int = $this->_db->fetchall($select_int);
			$cache->save($select_int, $cacheID);
		}

		//$this->view->countryArr_int = $selectcountry_int;


		//--------konfigurasicsv dan pdf---------
		if ($csv_int) {
			foreach ($select_int as $key => $val) {
				unset($select_int[$key]['country_code']);
			}
			$this->_helper->download->csv($header_int, $select_int, null, 'REMITTANCE BANK');
			Application_Helper_General::writeLog('RBLS', 'Download CSV Remittance Bank');
		} else if ($pdf_int) {
			$this->_helper->download->pdf($header_int, $select_int, null, 'REMITTANCE BANK');
			Application_Helper_General::writeLog('RBLS', 'Download PDF Remittance Bank');
		} else if ($this->_request->getParam('print') == 1) {
			$this->_forward('print', 'index', 'widget', array('data_content' => $select_int, 'data_caption' => 'Remittance Bank', 'data_header' => $fields_int));
		} else {
			Application_Helper_General::writeLog('RBLS', 'View Remittance Bank');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->view->select_int = $select_int;
		$this->view->fields_int = $fields_int;
		$this->view->filter_int = $filter_int;




		//Nearby ATM
		$temp_branch = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp_branch) > 1) {
			if ($temp_branch[0] == 'F' || $temp_branch[0] == 'S') {
				if ($temp_branch[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp_branch[0]);
				foreach ($temp_branch as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}
		$bankbranch_branch = $this->language->_('Branch Bank');
		$bankaddress1_branch = $this->language->_('Branch Address');

		$region_branch = $this->language->_('Region');
		$contact_branch = $this->language->_('Contact');

		$cityname_branch = $this->language->_('City Name');


		$fields_branch = array(
			'branch_name'      => array(
				'field' => 'BRANCH_NAME',
				'label' => $bankbranch_branch,
				'sortable' => true
			),
			'branch_address'      => array(
				'field' => 'BANK_ADDRESS',
				'label' => $bankaddress1_branch,
				'sortable' => true
			),
			'city_name'      => array(
				'field' => 'CITY_NAME',
				'label' => $cityname_branch,
				'sortable' => true
			),
			'region_name'      => array(
				'field' => 'REGION_NAME',
				'label' => $region_branch,
				'sortable' => true
			),
			'contact'      => array(
				'field' => 'CONTACT',
				'label' => $contact_branch,
				'sortable' => true
			)
		);

		$filterlist_branch = array('BRANCH_NAME', 'BANK_ADDRESS', 'CITY_NAME', 'REGION_NAME');

		$this->view->filterlist_branch = $filterlist_branch;


		//get page, sortby, sortdir
		$page_branch    = $this->_getParam('page_branch');
		$sortBy_branch  = $this->_getParam('sortby', 'BRANCH_NAME');
		$sortDir_branch = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page_branch = (Zend_Validate::is($page_branch, 'Digits') && ($page_branch > 0)) ? $page_branch : 1;

		$sortBy_branch = (Zend_Validate::is(
			$sortBy_branch,
			'InArray',
			array(array_keys($fields_branch))
		)) ? $fields_branch[$sortBy_branch]['field'] : $fields_branch[key($fields_branch)]['field'];

		$sortDir_branch = (Zend_Validate::is(
			$sortDir_branch,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir_branch : 'asc';

		//get filtering param
		$allNum_branch = new Zend_Filter_Alnum(true);

		$filterArr_branch = array(
			'filter' 	  	 => array('StringTrim', 'StripTags'),
			'BRANCH_NAME'  => array('StringTrim', 'StripTags'),
			'BANK_ADDRESS'  => array('StringTrim', 'StripTags'),
			'CITY_NAME'      => array('StringTrim', 'StripTags'),
			'REGION_NAME'      => array('StringTrim', 'StripTags'),
		);

		$dataParam_branch = array('BRANCH_NAME', 'BANK_ADDRESS', 'CITY_NAME', 'REGION_NAME');
		$dataParam_branchValue = array();
		foreach ($dataParam_branch as $dtParam_branch) {

			// print_r($dtParam_branch);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam_branch == $value) {
						$dataParam_branchValue[$dtParam_branch] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam_branch);
			// $dataParam_branchValue[$dtParam_branch] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam_branch);
		}

		$zf_filter_branch = new Zend_Filter_Input($filterArr_branch, array(), $dataParam_branchValue);
		// $filter = $zf_filter_branch->getEscaped('filter');
		$filter_branch 		= $this->_getParam('filter');
		$this->view->currentPage_branch = $page_branch;
		$this->view->sortBy_branch      = $sortBy_branch;
		$this->view->sortDir_branch     = $sortDir_branch;

		$csv_branch = $this->_getParam('csv');
		$pdf_branch = $this->_getParam('pdf');
		//$filter_branch_lg = $this->_getParam('filter');	

		//$temp_branchfields_branch = $fields_branch;
		//unset($temp_branchfields_branch['country_name']);
		//$getData = Application_Helper_Array::SimpleArray($temp_branchfields_branch,'field');





		$frontendOptions = array(
			'lifetime' => 259200,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/acctbalance/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		$cacheID = 'BRANCH';

		$select_branch = $cache->load($cacheID);

		if (empty($select_branch)) {
			$select_branch = $this->_db->select()
				->from(array('A' => 'M_BRANCH'), array('BRANCH_NAME', 'BANK_ADDRESS', 'CONTACT', 'REGION_NAME', 'CITY_NAME'));
			// ->join(array('B' => 'M_COUNTRY'),'A.country_code = B.COUNTRY_CODE', array());


			//if($filter_branch == 'Set Filter' || $filter_branch == 'Mengatur Filter' || $csv_branch || $pdf_branch) 
			if ($filter_branch == true || $csv_branch || $pdf_branch || $this->_request->getParam('print')) {
				//$temp_branchfields_branch['country_name'] = $fields_branch['country_name'];
				$header_branch = Application_Helper_Array::simpleArray($fields_branch, 'label');


				$fcity_branch     = $zf_filter_branch->getEscaped('CITY_NAME');
				$faddress_branch     = $zf_filter_branch->getEscaped('BANK_ADDRESS');
				$fname_branch    = $zf_filter_branch->getEscaped('BRANCH_NAME');
				$fregion_branch    = $zf_filter_branch->getEscaped('REGION_NAME');

				if ($fregion_branch)     $select_branch->where('UPPER(REGION_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fregion_branch) . '%'));
				if ($faddress_branch)     $select_branch->where('UPPER(BANK_ADDRESS) LIKE ' . $this->_db->quote('%' . strtoupper($faddress_branch) . '%'));
				if ($fname_branch)     $select_branch->where('UPPER(BRANCH_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fname_branch) . '%'));
				if ($fcity_branch)    $select_branch->where('UPPER(CITY_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fcity_branch) . '%'));

				$this->view->region_name  = $fregion_branch;
				$this->view->city_name    = $fcity_branch;
				$this->view->branch_name    = $fname_branch;
				$this->view->branch_address    = $faddress_branch;
			}

			//$this->view->success = true;
			$select_branch->order($sortBy_branch . ' ' . $sortDir_branch);
			$select_branch = $this->_db->fetchall($select_branch);
			$cache->save($select_branch, $cacheID);
		}

		//--------konfigurasicsv dan pdf---------
		if ($csv_branch) {
			foreach ($select_branch as $key => $val) {
				// unset($select_branch[$key]['country_code']);
			}
			$this->_helper->download->csv($header_branch, $select_branch, null, 'Branch BANK');
			Application_Helper_General::writeLog('RBLS', 'Download CSV Branch Bank');
		} else if ($pdf_branch) {
			$this->_helper->download->pdf($header_branch, $select_branch, null, 'Branch BANK');
			Application_Helper_General::writeLog('RBLS', 'Download PDF Branch Bank');
		} else if ($this->_request->getParam('print') == 1) {
			$this->_forward('print', 'index', 'widget', array('data_content' => $select_branch, 'data_caption' => 'Branch Bank', 'data_header' => $fields_branch));
		} else {
			Application_Helper_General::writeLog('RBLS', 'View Branch Bank');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->view->select_branch = $select_branch;
		if (!empty($dataParam_branchValue)) {
			foreach ($dataParam_branchValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
		$this->view->fields_branch = $fields_branch;
		$this->view->filter_branch = $filter_branch;





		//nearby_atm
		$temp_atm = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp_atm) > 1) {
			if ($temp_atm[0] == 'F' || $temp_atm[0] == 'S') {
				if ($temp_atm[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp_atm[0]);
				foreach ($temp_atm as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$bankaddress1_atm = $this->language->_('ATM Address');

		$cityname_atm = $this->language->_('City Name');


		$fields_atm = array(

			'atm_address'      => array(
				'field' => 'ATM_ADDRESS',
				'label' => $bankaddress1_atm,
				'sortable' => true
			),
			'city_name'      => array(
				'field' => 'ATM_CITY',
				'label' => $cityname_atm,
				'sortable' => true
			)
		);

		$filterlist_atm = array('ATM_ADDRESS', 'ATM_CITY');

		$this->view->filterlist_atm = $filterlist_atm;

		//get page, sortby, sortdir
		$page_atm    = $this->_getParam('page');
		$sortBy_atm  = $this->_getParam('sortby', 'ATM_ADDRESS');
		$sortDir_atm = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page_atm = (Zend_Validate::is($page_atm, 'Digits') && ($page_atm > 0)) ? $page_atm : 1;

		$sortBy_atm = (Zend_Validate::is(
			$sortBy_atm,
			'InArray',
			array(array_keys($fields_atm))
		)) ? $fields_atm[$sortBy_atm]['field'] : $fields_atm[key($fields_atm)]['field'];

		$sortDir_atm = (Zend_Validate::is(
			$sortDir_atm,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir_atm : 'asc';

		//get filtering param
		$allNum_atm = new Zend_Filter_Alnum(true);

		$filterArr_atm = array(
			'filter' 	  	 => array('StringTrim', 'StripTags'),
			'ATM_ADDRESS'  => array('StringTrim', 'StripTags'),
			'ATM_CITY'      => array('StringTrim', 'StripTags'),

		);

		$dataParam_atm = array("ATM_ADDRESS", "ATM_CITY");
		$dataParam_atmValue = array();
		foreach ($dataParam_atm as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParam_atmValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParam_atmValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		// print_r($dataParam_atmValue);die;
		$zf_filter_atm = new Zend_Filter_Input($filterArr_atm, array(), $dataParam_atmValue);
		// $filter = $zf_filter_atm->getEscaped('filter');
		$filter_atm 		= $this->_getParam('filter');

		$this->view->currentPage_atm = $page_atm;
		$this->view->sortBy_atm      = $sortBy_atm;
		$this->view->sortDir_atm     = $sortDir_atm;

		$csv_atm = $this->_getParam('csv');
		$pdf_atm = $this->_getParam('pdf');
		//$filter_atm_lg = $this->_getParam('filter');	

		//$temp_atmfields_atm = $fields_atm;
		//unset($temp_atmfields_atm['country_name']);
		//$getData = Application_Helper_Array::SimpleArray($temp_atmfields_atm,'field');



		$frontendOptions = array(
			'lifetime' => 259200,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/acctbalance/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		$cacheID = 'ATMLIST';

		$select_atm = $cache->load($cacheID);
		if (empty($select_atm)) {
			$select_atm = $this->_db->select()
				->from(array('A' => 'M_ATM'), array('ATM_ADDRESS', 'ATM_CITY'));
			// ->join(array('B' => 'M_COUNTRY'),'A.country_code = B.COUNTRY_CODE', array());


			//if($filter_atm == 'Set Filter' || $filter_atm == 'Mengatur Filter' || $csv_atm || $pdf_atm) 
			if ($filter_atm == true || $csv_atm || $pdf_atm || $this->_request->getParam('print')) {
				//$temp_atmfields_atm['country_name'] = $fields_atm['country_name'];
				$header_atm = Application_Helper_Array::simpleArray($fields_atm, 'label');


				$fatm_address_atm     = $zf_filter_atm->getEscaped('ATM_ADDRESS');
				$fcity_atm    = $zf_filter_atm->getEscaped('ATM_CITY');


				if ($fatm_address_atm)     $select_atm->where('UPPER(ATM_ADDRESS) LIKE ' . $this->_db->quote('%' . strtoupper($fatm_address_atm) . '%'));
				if ($fcity_atm)    $select_atm->where('UPPER(ATM_CITY) LIKE ' . $this->_db->quote('%' . strtoupper($fcity_atm) . '%'));

				$this->view->fatm_address  = $fatm_address_atm;
				$this->view->fcity    = $fcity_atm;
				// unset($_GET['wherecol']);
				// unset($_GET['whereval']);


			}

			//$this->view->success = true;
			$select_atm->order($sortBy_atm . ' ' . $sortDir_atm);
			$select_atm = $this->_db->fetchall($select_atm);
			$cache->save($select_atm, $cacheID);
		}

		//$select_atm = $this->_db->fetchall($select_atm);

		//--------konfigurasicsv dan pdf---------
		if ($csv_atm) {
			foreach ($select_atm as $key => $val) {
				// unset($select_atm[$key]['country_code']);
			}
			$this->_helper->download->csv($header_atm, $select_atm, null, 'ATM BANK');
			Application_Helper_General::writeLog('RBLS', 'Download CSV ATM Bank');
		} else if ($pdf_atm) {
			$this->_helper->download->pdf($header_atm, $select_atm, null, 'ATM BANK');
			Application_Helper_General::writeLog('RBLS', 'Download PDF ATM Bank');
		} else if ($this->_request->getParam('print') == 1) {
			$this->_forward('print', 'index', 'widget', array('data_content' => $select_atm, 'data_caption' => 'ATM Bank', 'data_header' => $fields_atm));
		} else {
			Application_Helper_General::writeLog('RBLS', 'View ATM Bank');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->view->select_atm = $select_atm;
		if (!empty($dataParam_atmValue)) {
			foreach ($dataParam_atmValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}

		$this->view->fields_atm = $fields_atm;
		$this->view->filter_atm = $filter_atm;





		//country table
		$temp_country = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp_country) > 1) {
			if ($temp_country[0] == 'F' || $temp_country[0] == 'S') {
				if ($temp_country[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp_country[0]);
				foreach ($temp_country as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$countrycode = $this->language->_('Country Code');
		$countryname = $this->language->_('Country Name');



		$fields_country = array(
			'country_code'  => array(
				'field' => 'COUNTRY_CODE',
				'label' => $countrycode,
				'sortable' => true
			),
			'country_name'     => array(
				'field' => 'COUNTRY_NAME',
				'label' => $countryname,
				'sortable' => true
			),
		);

		$filterlist_country = array('COUNTRY_NAME');

		$this->view->filterlist_country = $filterlist_country;

		//get page, sortby, sortdir
		$page_country    = $this->_getParam('page');
		$sortBy_country  = $this->_getParam('sortby', 'COUNTRY_CODE');
		$sortDir = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page_country = (Zend_Validate::is($page_country, 'Digits') && ($page_country > 0)) ? $page_country : 1;

		$sortBy_country = (Zend_Validate::is(
			$sortBy_country,
			'InArray',
			array(array_keys($fields_country))
		)) ? $fields_country[$sortBy_country]['field'] : $fields_country[key($fields_country)]['field'];

		$sortDir_country = (Zend_Validate::is(
			$sortDir_country,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir_country : 'asc';

		//get filtering param

		$filterArr_country = array(
			'filter' 	  	 => array('StringTrim', 'StripTags'),
			'COUNTRY_NAME'  => array('StringTrim', 'StripTags'),
		);

		$dataParam_country = array("COUNTRY_NAME");
		$dataParam_countryValue = array();
		foreach ($dataParam_country as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParam_countryValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParam_countryValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter_country = new Zend_Filter_Input($filterArr_country, array(), $dataParam_countryValue);
		// $filter = $zf_filter_country->getEscaped('filter');
		$filter_country 		= $this->_getParam('filter');

		$this->view->currentPage_country = $page_country;
		$this->view->sortBy_country      = $sortBy_country;
		$this->view->sortDir_country     = $sortDir_country;

		$csv_country = $this->_getParam('csv');
		$pdf_country = $this->_getParam('pdf');
		//$filter_country_lg = $this->_getParam('filter');	

		$getData_country = Application_Helper_Array::SimpleArray($fields_country, 'field');




		$frontendOptions = array(
			'lifetime' => 259200,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/acctbalance/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		$cacheID = 'COUNTRY';

		$select_country = $cache->load($cacheID);
		if (empty($select_country)) {
			$select_country = $this->_db->select()
				->from(array('A' => 'M_COUNTRY'), $getData_country);


			//if($filter_country == 'Set Filter' || $filter_country == 'Mengatur Filter' || $csv_country || $pdf_country) 
			if ($filter_country == true || $csv_country || $pdf_country || $this->_request->getParam('print')) {
				$header = Application_Helper_Array::simpleArray($fields_country, 'label');

				$fcountry_name = $zf_filter_country->getEscaped('COUNTRY_NAME');

				if ($fcountry_name) $select_country->where('UPPER(COUNTRY_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fcountry_name) . '%'));

				$this->view->country_name = $fcountry_name;
			}
			$select_country = $this->_db->select()
				->from('M_COUNTRY')
				->query()->fetchAll();
			$cache->save($select_country, $cacheID);
		}

		//$this->view->countryArr_int = $selectcountry_int;

		//$this->view->success = true;
		//$select_country->order($sortBy_country.' '.$sortDir_country);  

		//		$select_country = $this->_db->fetchall($select_country);

		//--------konfigurasicsv dan pdf---------
		if ($csv_country) {
			$this->_helper->download->csv($header, $select_country, null, 'Country List');
			Application_Helper_General::writeLog('COLS', 'Download CSV Country List');
		} else if ($pdf_country) {
			$this->_helper->download->pdf($header, $select_country, null, 'Country List');
			Application_Helper_General::writeLog('COLS', 'Download PDF Country List');
		} else if ($this->_request->getParam('print') == 1) {
			$this->_forward('print', 'index', 'widget', array('data_content' => $select_country, 'data_caption' => 'Country List', 'data_header' => $fields_country));
		} else {
			Application_Helper_General::writeLog('COLS', 'View Country List');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->view->select_country = $select_country;
		if (!empty($this->_request->getParam('wherecol'))) {
			$this->view->wherecol     = $this->_request->getParam('wherecol');
		}

		if (!empty($this->_request->getParam('whereopt'))) {
			$this->view->whereopt     = $this->_request->getParam('whereopt');
		}

		if (!empty($this->_request->getParam('whereval'))) {
			$this->view->whereval     = $this->_request->getParam('whereval');
		}
		$this->view->fields_country = $fields_country;
		$this->view->filter_country = $filter_country;
	}
}
