<?php

Class billeronboard_Model_Billeronboard
{
	protected $_db;
	protected $_providerType;
	protected $_masterglobalstatus;
	protected $_chargesTo;
	protected $_caseStatus;
	protected $_caseProvider;
	protected $_casechargesTo;
	//added agung
	protected $_catalogStatus;
	protected $_masterStatus;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$config = Zend_Registry::get('config');
		$this->_providerType = $config['provider']['type'];
		$this->_masterglobalstatus = $config['master']['globalstatus'];
		$this->_chargesTo = $config['charges']['to'];
		//added agung
		$this->_masterStatus = $config['master']['status'];
		

		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));
		$this->_caseStatus = "(CASE A.PROVIDER_STATUS ";
		foreach($statusarr as $key=>$val)
		{
		  $this->_caseStatus .= " WHEN ".$key." THEN '".$val."'";
		}
		$this->_caseStatus .= " END)";

		$providerType = array_combine(array_values($this->_providerType['code']),array_values($this->_providerType['desc']));
		
		unset($providerType["2"]); //only payment
		
		$this->_caseProvider = "(CASE A.PROVIDER_TYPE ";
		foreach($providerType as $key=>$val)
		{
		  $this->_caseProvider .= " WHEN ".$key." THEN '".$val."'";
		}
		$this->_caseProvider .= " END)";
		
		$chargesTo = array_combine(array_values($this->_chargesTo['code']),array_values($this->_chargesTo['desc']));
		$this->_casechargesTo = "(CASE A.CHARGES_TO ";
		foreach($chargesTo as $key=>$val)
		{
		  $this->_casechargesTo .= " WHEN ".$key." THEN '".$val."'";
		}
		$this->_casechargesTo .= " END)";
		
		$this->_caseStatusacct = "(CASE A.ACCT_STATUS ";
		foreach($statusarr as $key=>$val)
		{
		  $this->_caseStatusacct .= " WHEN ".$key." THEN '".$val."'";
		}
		$this->_caseStatusacct .= " END)";
		
		//added agung
		$catalogstatusarr = array_combine(array_values($this->_masterStatus['code']),array_values($this->_masterStatus['desc']));
		$this->_catalogStatus = "(CASE A.CATALOG_STATUS ";
		foreach($catalogstatusarr as $key=>$val)
		{
		  $this->_catalogStatus .= " WHEN ".$key." THEN '".$val."'";
		}
		$this->_catalogStatus .= " END)";

	}

    public function getProvider($fParam,$sortBy,$sortDir,$filter)
    {
		if (Zend_Registry::isRegistered('language')){
			$language = Zend_Registry::get('language');
		}
		$select2 = $this->_db->select()
						->FROM(array('A' => 'M_SERVICE_PROVIDER'),
						array('A.PROVIDER_ID','A.PROVIDER_CODE','A.PROVIDER_NAME','A.PROVIDER_DOMAIN','A.PROVIDER_KEY_ID',
						'SERVICE_TYPE' => 'B.SERVICE_NAME',
						'PROVIDER_TYPE' => $this->_caseProvider,
						'PROVIDER_STATUS' => $this->_caseStatus,
						'A.PROVIDER_SUGGESTED',
						'A.PROVIDER_SUGGESTEDBY',
						'A.PROVIDER_UPDATED',
						'A.PROVIDER_UPDATEDBY',
						)
						)
						->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array())
						->WHERE('B.SERVICE_ID = ? ', 18); //E-Commerce
		if($filter == $language->_('Set Filter'))
		{
			if(!empty($fParam['datefrom']) && empty($fParam['dateto']))
					$select2->where("DATE(A.PROVIDER_SUGGESTED) >= ".$this->_db->quote($fParam['datefrom']));
					
			if(empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(A.PROVIDER_SUGGESTED) <= ".$this->_db->quote($fParam['dateto']));
					
			if(!empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(A.PROVIDER_SUGGESTED) BETWEEN ".$this->_db->quote($fParam['datefrom'])." AND ".$this->_db->quote($fParam['dateto']));

			if(!empty($fParam['udatefrom']) && empty($fParam['udateto']))
					$select2->where("DATE(A.PROVIDER_UPDATED) >= ".$this->_db->quote($fParam['udatefrom']));
					
			if(empty($fParam['udatefrom']) && !empty($fParam['udateto']))
					$select2->where("DATE(A.PROVIDER_UPDATED) <= ".$this->_db->quote($fParam['udateto']));
					
			if(!empty($fParam['udatefrom']) && !empty($fParam['udateto']))
					$select2->where("DATE(A.PROVIDER_UPDATED) BETWEEN ".$this->_db->quote($fParam['udatefrom'])." AND ".$this->_db->quote($fParam['udateto']));


		    if($fParam['billerCode'])
		    {
	       		$select2->where("A.PROVIDER_CODE = ".$this->_db->quote($fParam['billerCode']));
		    }
			if($fParam['serviceCategory'])
		    {
	       		$select2->where("SERVICE_ID = ".$this->_db->quote($fParam['serviceCategory']));
		    }
		    
		    
			if($fParam['billerName'])
			{
	       		$select2->where("A.PROVIDER_NAME LIKE ".$this->_db->quote('%'.$fParam['billerName'].'%'));
			}
			
			if($fParam['billerStatus'])
			{
	       		$select2->where("A.PROVIDER_STATUS =  ".$this->_db->quote($fParam['billerStatus']));
			}
			
			if($fParam['suggestor'])
		    {
	       		$select2->where("A.PROVIDER_SUGGESTEDBY LIKE '%".$fParam['suggestor']."%'");
		    }
			if($fParam['approver'])
		    {
	       		$select2->where("A.PROVIDER_UPDATEDBY LIKE '%".$fParam['approver']."%'");
		    }
		}
		$select2->order($sortBy.' '.$sortDir);       
		//echo $select2;
       return $this->_db->fetchAll($select2);
    }
	
	public function getDetailProvider($providerid,$ccy=null)
	{
		$result = false;
		$sqlProvider = $this->_db->select()
						->from(array('A' => 'M_SERVICE_PROVIDER'),
						array(
						'A.PROVIDER_ID','A.PROVIDER_CODE','A.PROVIDER_NAME','A.PROVIDER_DOMAIN','A.PROVIDER_KEY_ID',
						'PROVIDER_TYPE' => $this->_caseProvider,
						'SERVICE_TYPE' => 'B.SERVICE_NAME',
						'PROVIDER_STATUS' => $this->_caseStatus,))
						->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array())
						->WHERE("A.PROVIDER_ID = ".$this->_db->quote($providerid));
						
		$result = $this->_db->fetchRow($sqlProvider);
		if($result)
		{
			$sqlProviderCharges = $this->_db->SELECT()
																	->FROM(array('A'=>'M_CHARGES_PROVIDER'),array('A.CHARGES_CCY','A.CHARGES_AMT',
																							'CHARGES_TO' => $this->_casechargesTo,
																							'CHARGES_TO_ID' => 'A.CHARGES_TO',
																	'A.CHARGES_ACCT'))
																	->WHERE("A.PROVIDER_ID = ".$this->_db->quote($providerid));
			if($ccy)
				$sqlProviderCharges->where("A.CHARGES_CCY = ".$this->_db->quote($ccy));

			$resultProviderCharges = $this->_db->fetchAll($sqlProviderCharges);
			
			if($resultProviderCharges)
			{
				foreach($resultProviderCharges as $key => $value)
				{
					$CHARGES[$value['CHARGES_CCY']] = $resultProviderCharges[$key];
				}
				$result += array('CHARGES'=>$CHARGES);
			}
		}
		
		return 	$result;
	}
	
	public function getDetailProviderTemp($changes_id)
	{
		$result = false;
		$sqlProvider = $this->_db->select()
						->from(array('C' => 'TEMP_CHARGES_PROVIDER'),
						array(
						'C.PROVIDER_ID','A.PROVIDER_CODE','A.PROVIDER_NAME',
						'PROVIDER_TYPE' => $this->_caseProvider,
						'SERVICE_TYPE' => 'B.SERVICE_NAME',
						'PROVIDER_STATUS' => $this->_caseStatus,))
						->join(array('A' => 'M_SERVICE_PROVIDER'), 'A.PROVIDER_ID = C.PROVIDER_ID', array())
						->join(array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array())
						->where("C.CHANGES_ID = ?", $changes_id);
						
		$result = $this->_db->fetchRow($sqlProvider);
		if($result)
		{
			$sqlProviderCharges = $this->_db->select()
											->from(array('A'=>'TEMP_CHARGES_PROVIDER'),array('A.CHARGES_CCY','A.CHARGES_AMT',
																	'CHARGES_TO' => $this->_casechargesTo,
																	'CHARGES_TO_ID' => 'A.CHARGES_TO',
											'A.CHARGES_ACCT'))
											->where("A.CHANGES_ID = ?", $changes_id);

			$resultProviderCharges = $this->_db->fetchAll($sqlProviderCharges);
			
			if($resultProviderCharges)
			{
				foreach($resultProviderCharges as $key => $value)
				{
					$CHARGES[$value['CHARGES_CCY']] = $resultProviderCharges[$key];
				}
				$result += array('CHARGES'=>$CHARGES);
			}
		}
		
		return 	$result;
	}
	
    public function getServiceType($serviceType)
    {
		$select2 = $this->_db->select()->distinct()
					->from(array('M_SERVICES_TYPE'))
					->WHERE("SERVICE_ID = 18") 
					;
		if (!empty($serviceType)){
			$select2->WHERE("SERVICE_OF = ".$this->_db->quote($serviceType));
		}		
		$select2->order('SERVICE_OF ASC');
		$select2->order('SERVICE_NAME ASC');
				 	//->query()->fetchAll();
       //return $data;
       //echo $select2;
       return $this->_db->fetchAll($select2);
    }
	
	public function isRequestChange($providerID)
	{
		$cek = $this->_db->fetchRow(
				$this->_db->select()
						->FROM('TEMP_SERVICE_PROVIDER')
						//->WHERE("PROVIDER_ID = ?",$this->_db->quote($providerID))
						->WHERE("PROVIDER_ID = ".$this->_db->quote($providerID))
					);
		return $cek;
	}

	public function getTemp($changes_id)
	{
		return $this->_db->fetchRow(
									$this->_db->select()
									->from(array('A' => 'TEMP_SERVICE_PROVIDER'))
									->where('A.CHANGES_ID = ?', $changes_id)
								);
	}
	
	public function insertTemp($param)
	{
		$this->_db->insert('TEMP_SERVICE_PROVIDER',$param);
	}
	
	public function updateTemp($change_id,$param)
	{
		$whereArr = array(
						'CHANGES_ID = ?'     => $change_id
					);		
		$this->_db->update('TEMP_SERVICE_PROVIDER',$param,$whereArr);
	}

	public function deleteTemp($change_id)
	{
		$where = array('CHANGES_ID = ?' => $change_id);
		$this->_db->delete('TEMP_SERVICE_PROVIDER', $where);
	}

    public function setBillerCatalog($SET_BILLER_CATALOG,$active,$userid)
    {
    	//Zend_Debug::dump($active);
    	if($active == '1'){
    		$catalogStatus = 'A';
    	}
    	else{
    		$catalogStatus = 'I';
    	}
    	//die;
		$param = array('CATALOG_STATUS'=> $catalogStatus,
					   //'PROVIDER_SUGGESTED' => date('Y-m-d H:i:s'),	
					   //'PROVIDER_SUGGESTEDBY'=> $userid,	
					   'PROVIDER_UPDATED' => date('Y-m-d H:i:s'),	
					   //'PROVIDER_UPDATEDBY' => $userid,
		);	
		$where = array('CATALOG_ID = ?' => $SET_BILLER_CATALOG	
					);
		$query = $this->_db->update ( "M_PROVIDER_CATALOG", $param, $where );
    }
 	public function getBillerDetailCatalog($fParam,$sortBy,$sortDir,$filter){
		$select2 = $this->_db->select()
						->FROM(array('A' => 'M_PROVIDER_CATALOG'),
						array('*'))
						//array('CATALOG_STATUS' => $this->_catalogStatus,'A.NOMINAL','A.CATALOG_ID'))
						->joinLeft(array('MU'=>'M_SERVICE_PROVIDER'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('MU.PROVIDER_NAME','MU.PROVIDER_ID'))
						->WHERE('A.PROVIDER_ID = ? ', $fParam['provid']);		
		$select2->order($sortBy.' '.$sortDir);       
        return $this->_db->fetchAll($select2);
 	}	
	public function getBillerCatalog($fParam,$sortBy,$sortDir,$filter){
		$select2 = $this->_db->select()
						->FROM(array('A' => 'M_SERVICE_PROVIDER'),array('*'))
						//->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array());
						->WHERE('A.SERVICE_TYPE = ? ', 14);
		if($filter == 'Set Filter')
		{	
			if($fParam['billerName'])
			{
	       		$select2->where("A.PROVIDER_NAME LIKE ".$this->_db->quote('%'.$fParam['billerName'].'%'));
			}
		}
		$select2->order($sortBy.' '.$sortDir);       
		
        return $this->_db->fetchAll($select2);	
	}
	
	public function getBillerAcc($param)
	{
		$arrSelect = array(
										'PROVIDER_CODE' => 'B.PROVIDER_CODE',
										'PROVIDER_NAME' => 'B.PROVIDER_NAME',
										'ACCT_NO' => 'A.ACCT_NO',
										'ACCT_NAME' => 'A.ACCT_NAME',
										'ACCT_CIF' => 'A.ACCT_CIF',
										'CCY_ID' => 'A.CCY_ID',
										'ACCT_TYPE' => 'A.ACCT_TYPE',
										'ACCT_DESC' => 'A.ACCT_DESC',
										'ACCT_STATUS' => $this->_caseStatusacct,
										'ACCT_SUGGESTED' => 'A.ACCT_SUGGESTED',   
										'ACCT_SUGGESTEDBY' => 'A.ACCT_SUGGESTEDBY',
										'ACCT_UPDATED' => 'A.ACCT_UPDATED',
										'ACCT_UPDATEDBY' => 'A.ACCT_UPDATEDBY',
									);
		if(isset($param['detail']))
			$arrSelect += array('ACCT_CREATED' => 'A.ACCT_CREATED','ACCT_CREATEDBY' => 'A.ACCT_CREATEDBY','PROVIDER_ID' => 'A.PROVIDER_ID');
		$sql = $this->_db->SELECT()
									->FROM(array('A'=>'M_PROVIDER_ACCT'),$arrSelect)
									->JOIN (array('B' => 'M_SERVICE_PROVIDER'), 'B.PROVIDER_ID = A.PROVIDER_ID', array());
		$sql->WHERE('B.SERVICE_TYPE = 18');
		
		if(isset($param['PROVIDER_ID']))
			$sql->WHERE('A.PROVIDER_ID = ?',$param['PROVIDER_ID']);
		if(isset($param['ACCT_NO']))
			$sql->WHERE('A.ACCT_NO = ?',$param['ACCT_NO']);

		if(!empty($param['datefrom']) && empty($param['dateto']))
				$sql->where("DATE(B.PROVIDER_SUGGESTED) >= ".$this->_db->quote($param['datefrom']));
				
		if(empty($param['datefrom']) && !empty($param['dateto']))
				$sql->where("DATE(B.PROVIDER_SUGGESTED) <= ".$this->_db->quote($param['dateto']));
				
		if(!empty($param['datefrom']) && !empty($param['dateto']))
				$sql->where("DATE(B.PROVIDER_SUGGESTED) BETWEEN ".$this->_db->quote($param['datefrom'])." AND ".$this->_db->quote($param['dateto']));

		if(!empty($param['udatefrom']) && empty($param['udateto']))
				$sql->where("DATE(B.PROVIDER_UPDATED) >= ".$this->_db->quote($param['udatefrom']));
				
		if(empty($param['udatefrom']) && !empty($param['udateto']))
				$sql->where("DATE(B.PROVIDER_UPDATED) <= ".$this->_db->quote($param['udateto']));
				
		if(!empty($param['udatefrom']) && !empty($param['udateto']))
				$sql->where("DATE(B.PROVIDER_UPDATED) BETWEEN ".$this->_db->quote($param['udatefrom'])." AND ".$this->_db->quote($param['udateto']));


		if(!empty($param['billerCode']))
		{
			$sql->where("B.PROVIDER_CODE = ".$this->_db->quote($param['billerCode']));
		}
		
		if(!empty($param['billerName']))
		{
			$sql->where("B.PROVIDER_NAME LIKE ".$this->_db->quote('%'.$param['billerName'].'%'));
		}
		
		if(!empty($param['status']))
		{
			$sql->where("A.ACCT_STATUS =  ".$this->_db->quote($param['status']));
		}
		
		if(!empty($param['suggestor']))
		{
			$sql->where("B.PROVIDER_SUGGESTEDBY LIKE '%".$param['suggestor']."%'");
		}
		if(!empty($param['approver']))
		{
			$sql->where("B.PROVIDER_UPDATEDBY LIKE '%".$param['approver']."%'");
		}

		if(!empty($param['sortBy']) && !empty($param['sortDir']))
			$sql->order($param['sortBy'].' '.$param['sortDir']); 

		return $this->_db->fetchAll($sql);
	}

	public function acctIsRequestChange($acct)
	{
		$cek = $this->_db->fetchRow(
				$this->_db->select()
						->FROM('TEMP_PROVIDER_ACCT')
						->WHERE("ACCT_NO = ?",$acct)
					);
		return $cek;
	}

	public function getTempacct($changes_id)
	{
		return $this->_db->fetchRow(
									$this->_db->select()
									->from(array('A' => 'TEMP_PROVIDER_ACCT',array('*')))
									->JOIN (array('B' => 'M_SERVICE_PROVIDER'), 'B.PROVIDER_ID = A.PROVIDER_ID', array('PROVIDER_NAME'))
									->where('A.CHANGES_ID = ?', $changes_id)
								);
	}
	
	public function insertTempacct($param)
	{
		$this->_db->insert('TEMP_PROVIDER_ACCT',$param);
	}
	
	public function updateTempacct($change_id,$param)
	{
		$whereArr = array(
						'CHANGES_ID = ?'     => $change_id
					);
					
		$this->_db->update('TEMP_PROVIDER_ACCT',$param,$whereArr);
	}

	public function deleteTempacct($change_id)
	{
		$where = array('CHANGES_ID = ?' => $change_id);
		$this->_db->delete('TEMP_PROVIDER_ACCT', $where);
	}

}