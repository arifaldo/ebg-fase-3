<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class Corporatedebitcard_DebitaddController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$attahmentDestination 	= UPLOAD_PATH.'/document/temp';
		$adapter 				= new Zend_File_Transfer_Adapter_Http();
		
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 

		$field = array(
			'BRANCH_CODE' 		=> 6,
			'BRANCH_NAME' 		=> 50,
			'BANK_ADDRESS' 		=> 140,
		    'CITY_NAME' 		=> 50,
		    'REGION_NAME' 		=> 50,
		    'CONTACT' 		=> 50,
		);
		
		$cust_id = $this->_getParam('cust');
		$REG_NUMBER = $this->_getParam('regid');
		$this->view->regid = $REG_NUMBER;
		$this->view->cust = $cust_id;
		$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							->where('A.CUST_ID = ?',$cust_id)
							->where('A.REG_NUMBER = ?',$REG_NUMBER);
							
		$data = $this->_db->fetchRow($select);			
		if($data['DEBIT_TYPE'] == '1'){
			$data['DEBIT_TYPE'] = 'Corporate';
		}else{
			$data['DEBIT_TYPE'] = 'Card Holder';
		}
		
		if($data['DEBIT_ATM'] == '1'){
			$data['DEBIT_ATM'] = 'On';
		}else{
			$data['DEBIT_ATM'] = 'Off';
		}
		
		if($data['DEBIT_EDC'] == '1'){
			$data['DEBIT_EDC'] = 'On';
		}else{
			$data['DEBIT_EDC'] = 'Off';
		}
		
		
		$this->view->data = $data;

		$this->view->field = $field;
		if($this->_request->isPost())
		{
			$params = $this->_request->getParams();
			//var_dump($attahmentDestination);die;
			$adapter->setDestination ($attahmentDestination);

			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
			$extensionValidator->setMessage(
				'Error: Extension file must be *.txt'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
			);
			
			
			
			//var_dump($params);
			$adapter->setValidators(array($extensionValidator,$sizeValidator));

			if($adapter->isValid())
			{

				$success = 1;
				$sourceFileName = $adapter->getFileName();
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				$adapter->addFilter ( 'Rename',$newFileName);
				$adapter->receive();
				//Zend_Debug::dump($newFileName);die;
				$myFile = file_get_contents($newFileName);
				//var_dump(htmlspecialchars($myFile));
				$arry_myFile = explode("\n", $myFile);
				@unlink($newFileName);
				//Zend_Debug::dump($arry_myFile);die;
				if($adapter->receive())
				{
					$this->_db->beginTransaction();
					/*$master = $this->_db->FetchRow(
						$this->_db->select()
							->from('M_DOMESTIC_BANK_TABLE')
							->where('BANK_ISMASTER = 1')
					);*/

					//$this->_db->query('TRUNCATE TABLE M_BRANCH');

					/*if(!empty($master))
					{
						unset($master['BANK_ID']);
						$this->_db->insert("M_DOMESTIC_BANK_TABLE",$master);
					}*/
					//var_dump($arry_myFile);die;
					unset($arry_myFile[0]);
					
					foreach ($arry_myFile as $ky => $vl)
					{
						$master = $this->_db->FetchRow(
						$this->_db->select()
							->from('T_DEBITCARD')
							->where('IS_ACTIVATED = ?','1')
							->where('REG_NUMBER IS NULL')
							->where('DEBIT_NUMBER = ?',trim($vl))
							
						);
						//var_dump($master);
						if(empty($master)){
							$success = 0;
							$err = true;
							$errmsg = 'Debit Card not found';
						}
						$arry_myFile[$ky] = trim($vl);
					}
					
				//	var_dump($err);die;
					if($err){
						$err = true;
						$errmsg = 'Debit Card not found';
						$success = 0;
						$this->view->err = $err;
						$this->view->errmsg = $errmsg;
						//die;
						//$this->_redirect('/corporatedebitcard/debitadd/index/cust/'.$params['cust'].'/regid/'.$params['regid']);
					}else if(count(array_unique($arry_myFile))<count($arry_myFile))
						{
							$errmsg = 'Duplicate Debit Card';
							$err = true;
							$success = 0;
							$this->view->err = $err;
						$this->view->errmsg = $errmsg;
						//die;
						//	$this->_redirect('/corporatedebitcard/debitadd/index/cust/'.$params['cust'].'/regid/'.$params['regid']);
							
							// Array has duplicates
						}else{
						
					
					$change_id = $this->suggestionWaitingApproval('Debit Card',$info,$this->_changeType['code']['new'],null,'T_DEBITCARD','TEMP_DEBITCARD',$cust_id,$data['CUST_NAME'],$cust_id,$data['CUST_NAME']);
					$success = 1;
					foreach ($arry_myFile as $row)
					{

						

						//$var['created'] = date('Y-m-d H:i:s');
						//$var['createdby'] = $this->_userIdLogin;

						try
						{
							$var = array();
							if( !empty($row) )
							{
								$master = $this->_db->FetchRow(
								$this->_db->select()
									->from('T_DEBITCARD')
									->where('IS_ACTIVATED = ?','1')
									->where('REG_NUMBER IS NULL')
									->where('DEBIT_NUMBER = ?',trim($vl))
									
								);
								
								$var['IS_ACTIVATED']	= $master['IS_ACTIVATED'];
								$var['DEBIT_ACTIVATED']	= $master['DEBIT_ACTIVATED'];
								$var['DEBIT_ACTIVATEDBY']	= $master['DEBIT_ACTIVATEDBY'];
								$var['ACCT_NO']	= $master['ACCT_NO'];
								$var['CHANGES_ID'] = $change_id;
								$var['CUST_ID'] = $cust_id;
								$var['REG_NUMBER'] = $REG_NUMBER;
								$var['DEBIT_NUMBER'] = trim($row);
								$var['DEBIT_STATUS'] = '3';
								
								$var['DEBIT_SUGGESTEDBY'] = $this->_userIdLogin;
								$var['DEBIT_SUGGESTED'] = new Zend_Db_Expr('now()');
								
 								 //echo "<pre>";
								 //print_r($var);die;
								$this->_db->insert("TEMP_DEBITCARD",$var);
							}
						}
						catch(Exception $e)
						{
							//var_dump($e);die;
							$success = 0;
						}
					}
					
						}
				}
				if($success == 1)
				{
					$this->_db->commit();
					Application_Helper_General::writeLog('COIM','Import Debit Card List');
					//$this->setbackURL('/corporatedebitcard/');
					  $AESMYSQL = new Crypt_AESMYSQL();
					  $rand = $this->token;
					  $encrypted_trxid = $AESMYSQL->encrypt($REG_NUMBER, $rand);
         			  $enctrxid = urlencode($encrypted_trxid); 
			          $urlBack = '/corporatedcard/view/index/id/'.$enctrxid;
               		  $this->setbackURL($urlBack);

					$this->_redirect('/notification/submited/index');
				}else{
					$this->_db->rollBack();
				}
			}
			else
			{
				$this->view->errorMsg = $adapter->getMessages();
				Zend_Debug::dump($adapter->getMessages());die;
			}
		}
		Application_Helper_General::writeLog('COIM','Import Debit Card List');
	}
	
	 public function downloadtrx2Action()
    {
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
		
      
      $changeId = $this->_getParam('reg_id');
      
      $datakey = array(
                  "0" => 'Debit Card Number'
                );
      $headerData[] = $datakey;
      //var_dump($changeId);
     $resultdata  = $this->_db->fetchAll(
                     $this->_db->select()
                               ->from(array('T' => 'TEMP_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('REG_NUMBER'))
                               ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
                               ->where('T.CHANGES_ID = ?', $changeId)
                               );

     
      foreach ($resultdata as $p => $pTrx)
      {

        

        $paramTrx = array(  
                  "0" => $pTrx['DEBIT_NUMBER']
                );

        $newData[] = $paramTrx;
        
      }
	//var_dump($newData);die;
      $this->_helper->download->txtBatch($headerData,$newData,null,$resultdata['0']['REG_NUMBER']);
      Application_Helper_General::writeLog('RPPY','Download TXT Document Upload Detail');  
      
    }
	
}