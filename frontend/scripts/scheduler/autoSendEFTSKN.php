<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/SKN.php';

	$db 	  = Zend_Db_Table::getDefaultAdapter();
	$config   = Zend_Registry::get('config');
	$actionId = 'scheduler';
	$moduleId = 'auto Generate SKN';

	$now			= date('_Ymd_His');
	$filename 		= "CIBSKN".$now.".DKO";
	$filepath		= LIBRARY_PATH.'data/file/SKN/';

	$typeCode = $config['transfer']['type']['code']['SKN'];

	$select = $db->select()
				 ->from(array('P' => 'T_PSLIP'),array('T.*', 'P.PS_EFDATE'))
				 ->joinLeft(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array() )
				 ->where('P.PS_STATUS 		= 5')						
				 ->where('T.TRA_STATUS 		= 3')
				 ->where('T.TRANSFER_TYPE	= ?', $typeCode)
				 ->where('T.EFT_STATUS		= 3')	// 1 =  Success, 0 =  Failed, 2 =  Not Delivered, 3 = waiting to send EFT, 4 = in progress sending EFT
				 ->where('DATE(P.PS_EFDATE) = DATE(now())')				
				 ->order('P.PS_UPDATED ASC');
	$data   = $db->fetchAll($select);

	if($data)
	{
		// locked trx
		foreach($data as $trx)
		{
			$paymentData = array('EFT_STATUS' 			=> 4);
			$whereData   = array('TRANSACTION_ID = ?'	=> (string) $trx['TRANSACTION_ID']);
			$db->update('T_TRANSACTION', $paymentData, $whereData);
		}

		$SKN = new SKN();

		foreach($data as $trx)
		{
			$contentEFT 	= $SKN->setEFTDetail($trx);
		}

		$dataEFT = $SKN->getEFT();

		$isSuccessEFT = true;

		if (!empty($isSuccessEFT))
		{
			$eftfile = file_put_contents($filepath."/".$filename, $dataEFT);
			
			if($eftfile !== false){
				$sendEFTCode = "00";	
				$sendEFTDesc = "SKN Success write file";	
			}else{
				$sendEFTCode = "11";	
				$sendEFTDesc = "SKN Failed write file";	
				$isSuccessEFT = false;
			}
		}

		if ($isSuccessEFT == true) {
			$eftStatus 		= 1; // 1 =  Success, 0 =  Failed, 2 =  Not Delivered, 3 = waiting to send EFT, 4 = in progress sending EFT, 9 = Exception 
			$eftBankCode 	= 0; // 0 = all success, 99 = not yet sent 
		} else {
			$eftStatus 		= 3;
			$eftBankCode 	= 99;
		}

		foreach($data as $trx)
		{
			$paymentData = array('EFT_STATUS' 			=> $eftStatus,
								 'EFT_BANKCODE' 		=> $eftBankCode,
								 'EFT_BANKRESPONSE' 	=> $sendEFTCode.":".$sendEFTDesc,
								);
			$whereData   = array('TRANSACTION_ID = ?'	=> (string) $trx['TRANSACTION_ID']);
			$db->update('T_TRANSACTION', $paymentData, $whereData);
		}
	}
	else
	{
		echo " No SKN In Progress";
	}

?>