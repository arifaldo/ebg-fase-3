<?php
/**
 * Tempcustomer model
 *
 * @author
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';
//require_once 'General/Setting.php';

class Changemanagement_Model_Tempbgreport extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = '';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_BANK_GUARANTEE')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetchAll(Zend_Db::FETCH_ASSOC);
			
        if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Bank Guarantee)';
			return false;
        }
		
		$request = array();
					
					/*if($insertArr['DEBIT_TYPE'] == '1'){
						$cust_type = 'CORPORATE';	
					}else{
						$cust_type = 'CARDHOLDER';	
					}*/
					
					
					$selectcomp = $this->dbObj->select()
                            ->from('M_CUSTOMER', array('*'))
                             ->where('CUST_ID = ? ',$customer['0']['CUST_ID'])
                            ->where('CUST_STATUS = 1 ');
					//echo $selectcomp;die;
					//$datacomp = $this->dbObj->fetchAll($selectcomp);
		
		foreach($customer as $data){
			/*
					$request['header']['sender_id'] = 'D360';
					$request['header']['signature'] = '';
					$request['data']['customer_type'] = $cust_type;
					$request['data']['bank_code'] = '999';
					$request['data']['cif'] = $datacomp['0']['CUST_CIF'];
					$request['data']['account_type'] = '2';
					$request['data']['account_number'] = $data['DEBIT_NUMBER'];
					$request['data']['account_currency'] = '000';
					$request['data']['account_name'] = $datacomp['0']['CUST_NAME'];
					$request['data']['email'] = $datacomp['0']['CUST_EMAIL'];
					$request['data']['phone_number'] = $datacomp['0']['CUST_PHONE'];
					$request['data']['birthdate'] = $datacomp['0']['CUST_CREATED'];
					$request['data']['address'] = $datacomp['0']['CUST_ADDRESS'];
					$request['data']['city'] = $datacomp['0']['CUST_CITY'];
					$request['data']['state'] = $datacomp['0']['CUST_PROVINCE'];
					$clientUser  =  new SGO_Soap_ClientUser();
					
					$success = $clientUser->callapi('registeraccount',$request);
					$result  = $clientUser->getResult();
			*/
			$updateArr = array_diff_key($data,array('TEMP_ID'=>'','CHANGES_ID'=>''));
			$updateArr['DEBIT_STATUS']   = 1;
			$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
			$updateArr['DEBIT_APPROVEDBY']   = $actor;
			
			$whereArr = array(
				'DEBIT_NUMBER = ?' => $updateArr['DEBIT_NUMBER']
			);
		
		try
			{
				$customerins = $this->dbObj->update('T_DEBITCARD',$updateArr,$whereArr);
			}
		catch(exception $e)
			{
				var_dump($e);die;
				Zend_Debug::dump($e->getMessage());
			}
			if(!(boolean)$customerins)
			{
				$this->_errorCode = '82';
				$this->_errorMsg = 'Query failed(Bank Guarantee)';
				return false;
			}
		}

		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null)
	{
		$transFailed = false;
		//query from TEMP_CUSTOMER
		$bankguarantee = $this->dbObj->select()
						  ->from('TEMP_BANK_GUARANTEE')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($bankguarantee))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Bank Guarantee)';
			return false;
        }

		

        $updateArr = array_diff_key($bankguarantee,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['BG_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BG_UPDATEDBY']   = $actor;

		$whereArr = array(
							'BG_REG_NUMBER = ?'=>(string)$bankguarantee['BG_REG_NUMBER'],
							'BG_NUMBER = ?'=>(string)$bankguarantee['BG_NUMBER']
						);
						
		$bankguaranteeupdate = $this->dbObj->update('T_BANK_GUARANTEE',$updateArr,$whereArr);

		
	    // insert T_BANK_GUARANTEE_HISTORY
		$insertArr['DATE_TIME'] = new Zend_Db_Expr('now()');
		$insertArr['BG_REG_NUMBER']   = $bankguarantee['BG_REG_NUMBER'];
		$insertArr['USER_LOGIN'] =  $actor;
		$insertArr['CUST_ID']   = $bankguarantee['CUST_ID'];
		$insertArr['BG_REASON']   = $bankguarantee['BG_STATUS'] == 9 ? 'WithDrawal' : 'Edit';
		$insertArr['HISTORY_STATUS']   = $bankguarantee['BG_STATUS'];
		
		$bankguaranteeInsert = $this->dbObj->insert('T_BANK_GUARANTEE_HISTORY',$insertArr);
	
		
		if(!(boolean)$bankguaranteeupdate)
		{
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(T_BANK_GUARANTEE)';
			return false;
		}

		if(!(boolean)$bankguaranteeInsert)
		{
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(T_BANK_GUARANTEE_HISTORY)';
			return false;
		}
		$customer = $this->dbObj->select()
						  ->from('M_CUSTOMER')
						  ->where('CUST_ID = ?',$bankguarantee['CUST_ID'])
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Edited, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$updateArr['CUST_NAME'].' Change id : '.$this->_changeId);
        if ($bankguarantee['BG_STATUS'] == 9){
			// ---------------------------------- EMAIL NOTIF -----------------------------------------------
		$setting 	= new Settings();
			$template						= $setting->getSetting('femailtemplate_bg','');
			$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name','');
			$templateEmailMasterBankAppName 	= $setting->getSetting('master_bank_app_name','');
			$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp','');
			$templateEmailMasterBankWapp 	= $setting->getSetting('master_bank_wapp','');
			$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email','');
			$templateEmailMasterBankEmail1 	= $setting->getSetting('master_bank_email1','');
			$date 							= date("d M Y H:i:s");
			$config    		= Zend_Registry::get('config');
			$bgstatus 	= $config["bg"]["status"]["desc"];
			$bgstatusCode 	= $config["bg"]["status"]["code"];
			$bgstatusarr = array_combine(array_values($bgstatusCode),array_values($bgstatus));
			//var_dump($paytypesarr);die;
		    $config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];
		
				$arrStatus = array_combine(array_values($BgCode),array_values($BgType));
			$amount = number_format($bankguarantee['BG_AMOUNT'],2,',','.');
		$translate = array(
				'[[title]]'                     => $bankguarantee['BG_STATUS'] == 9 ? 'BG WithDrawal' : '',
				'[[user_name]]'                 => $customer['CUST_NAME'],		
				'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
				'[[master_bank_app_name]]' 		=> $templateEmailMasterBankAppName,
				'[[MASTER_BANK_TELP]]' 			=> $templateEmailMasterBankTelp,
				'[[MASTER_BANK_EMAIL]]' 		=> $templateEmailMasterBankEmail,
				'[[MASTER_BANK_WAPP]]' 			=> $templateEmailMasterBankWapp,
				'[[MASTER_BANK_EMAIL1]]' 		=> $templateEmailMasterBankEmail1,
				'[[cust_name]]'					=> $customer['CUST_NAME'],
				'[[bg_number]]'					=> $bankguarantee['BG_REG_NUMBER'],
				'[[account_no]]'	 			=> $bankguarantee['ACCT_NO'],
				'[[subject]]' 					=> $bankguarantee['BG_SUBJECT'],
				'[[amount]]' 					=> 'IDR '.$amount,
				'[[status]]' 					=> $arrStatus[$bankguarantee['BG_STATUS']],
				'[[notes]]' 					=> $bankguarantee['BG_CLAIM_NOTES'],
				//'[[duedate]]' 					=> $resultQuery['0']['TIME_PERIOD_END'],
			);

		
		$subject = 'BG WithDrawal';
		$useremail = $customer['CUST_EMAIL'];
		$mailContent = strtr($template,$translate); 
		//var_dump($useremail);
		//var_dump($mailContent);die;
		Application_Helper_Email::sendEmail($useremail,$subject,$mailContent);
		// ---------------------------------- END EMAIL NOTIF -------------------------------------------
		}
		

		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP
		$bankguarantee = $this->dbObj->select()
						  ->from('TEMP_BANK_GUARANTEE')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
        //var_dump($bankguarantee);
		//die();
		if(!count($bankguarantee))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Bank Guarantee)';
			return false;
        }

		

        $updateArr = array_diff_key($bankguarantee,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['BG_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BG_UPDATEDBY']   = $actor;

		$whereArr = array(
							'BG_REG_NUMBER = ?'=>(string)$bankguarantee['BG_REG_NUMBER'],
							'BG_NUMBER = ?'=>(string)$bankguarantee['BG_NUMBER']
						);
						
		$bankguaranteeupdate = $this->dbObj->update('T_BANK_GUARANTEE',$updateArr,$whereArr);

		
	    // insert T_BANK_GUARANTEE_HISTORY
		$insertArr['DATE_TIME'] = new Zend_Db_Expr('now()');
		$insertArr['BG_REG_NUMBER']   = $bankguarantee['BG_REG_NUMBER'];
		$insertArr['USER_LOGIN'] =  $actor;
		$insertArr['CUST_ID']   = $bankguarantee['CUST_ID'];
		$insertArr['BG_REASON']   = 'UnSuspend';
		$insertArr['HISTORY_STATUS']   = $bankguarantee['BG_STATUS'];
		
		$bankguaranteeInsert = $this->dbObj->insert('T_BANK_GUARANTEE_HISTORY',$insertArr);
	
		
		if(!(boolean)$bankguaranteeupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(T_BANK_GUARANTEE)';
			return false;
		}

		if(!(boolean)$bankguaranteeInsert)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(T_BANK_GUARANTEE_HISTORY)';
			return false;
		}

	
		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP
		$bankguarantee = $this->dbObj->select()
						  ->from('TEMP_BANK_GUARANTEE')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
        //var_dump($bankguarantee);
		//die();
		if(!count($bankguarantee))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Bank Guarantee)';
			return false;
        }

		

        $updateArr = array_diff_key($bankguarantee,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['BG_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BG_UPDATEDBY']   = $actor;

		$whereArr = array(
							'BG_REG_NUMBER = ?'=>(string)$bankguarantee['BG_REG_NUMBER'],
							'BG_NUMBER = ?'=>(string)$bankguarantee['BG_NUMBER']
						);
						
		$bankguaranteeupdate = $this->dbObj->update('T_BANK_GUARANTEE',$updateArr,$whereArr);

		
	    // insert T_BANK_GUARANTEE_HISTORY
		$insertArr['DATE_TIME'] = new Zend_Db_Expr('now()');
		$insertArr['BG_REG_NUMBER']   = $bankguarantee['BG_REG_NUMBER'];
		$insertArr['USER_LOGIN'] =  $actor;
		$insertArr['CUST_ID']   = $bankguarantee['CUST_ID'];
		$insertArr['BG_REASON']   = 'Suspend';
		$insertArr['HISTORY_STATUS']   = $bankguarantee['BG_STATUS'];
		
		$bankguaranteeInsert = $this->dbObj->insert('T_BANK_GUARANTEE_HISTORY',$insertArr);
	
		
		if(!(boolean)$bankguaranteeupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(T_BANK_GUARANTEE)';
			return false;
		}

		if(!(boolean)$bankguaranteeInsert)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(T_BANK_GUARANTEE_HISTORY)';
			return false;
		}

		$customer = $this->dbObj->select()
						  ->from('M_CUSTOMER')
						  ->where('CUST_ID = ?',$bankguarantee['CUST_ID'])
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		// ---------------------------------- EMAIL NOTIF -----------------------------------------------
		    $setting 	= new Settings();
			$template						= $setting->getSetting('femailtemplate_bg','');
			$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name','');
			$templateEmailMasterBankAppName 	= $setting->getSetting('master_bank_app_name','');
			$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp','');
			$templateEmailMasterBankWapp 	= $setting->getSetting('master_bank_wapp','');
			$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email','');
			$templateEmailMasterBankEmail1 	= $setting->getSetting('master_bank_email1','');
			$date 							= date("d M Y H:i:s");
			$config    		= Zend_Registry::get('config');
			$bgstatus 	= $config["bg"]["status"]["desc"];
			$bgstatusCode 	= $config["bg"]["status"]["code"];
			$bgstatusarr = array_combine(array_values($bgstatusCode),array_values($bgstatus));
			
			$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];
		
				$arrStatus = array_combine(array_values($BgCode),array_values($BgType));

			//var_dump($template);die('heere');
					
			$amount = number_format($bankguarantee['BG_AMOUNT'],2,',','.');
		$translate = array(
				'[[title]]'                     => 'BG Suspend',
				'[[user_name]]'                 => $customer['CUST_NAME'],		
				'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
				'[[master_bank_app_name]]' 		=> $templateEmailMasterBankAppName,
				'[[MASTER_BANK_TELP]]' 			=> $templateEmailMasterBankTelp,
				'[[MASTER_BANK_EMAIL]]' 		=> $templateEmailMasterBankEmail,
				'[[MASTER_BANK_WAPP]]' 			=> $templateEmailMasterBankWapp,
				'[[MASTER_BANK_EMAIL1]]' 		=> $templateEmailMasterBankEmail1,
				'[[cust_name]]'					=> $customer['CUST_NAME'],
				'[[bg_number]]'					=> $bankguarantee['BG_REG_NUMBER'],
				'[[account_no]]'	 			=> $bankguarantee['ACCT_NO'],
				'[[subject]]' 					=> $bankguarantee['BG_SUBJECT'],
				'[[amount]]' 					=> 'IDR '.$amount,
				'[[status]]' 					=> $arrStatus[$bankguarantee['BG_STATUS']],
				'[[notes]]' 					=> $bankguarantee['REASON'],
				//'[[duedate]]' 					=> $resultQuery['0']['TIME_PERIOD_END'],
			);
    
		$subject = 'BG Suspend Notification';
		$useremail = $customer['CUST_EMAIL'];
		$mailContent = strtr($template,$translate); 
		//var_dump($useremail);
		//var_dump($mailContent);die;
		Application_Helper_Email::sendEmail($useremail,$subject,$mailContent);
		// ---------------------------------- END EMAIL NOTIF -------------------------------------------
		$deleteChanges  = $this->deleteSuspend();
		
		if(!$deleteChanges)
			return false;

		return true;
	}


	/*
	 if delete customer :
	 - delete user
	 - delete bank account
	 - delete user limit
	 - delete daily limit
	*/
	/*public function approveDelete($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER_BIN')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Bank Guarantee)';
			return false;
        }

        //update status customer
	    $updateArr = array('CUST_BIN_STATUS' => 3);
	    $updateArr['BIN_SUGGESTED']   = $customer['BIN_SUGGESTED'];
		$updateArr['BIN_SUGGESTEDBY'] = $customer['BIN_SUGGESTEDBY'];
		$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BIN_UPDATEDBY']   = $actor;
		
		/////////////////
		$updateArrDataTrans = array('PAID_STATUS' => 3);
		$whereArrDataTrans = array(
			'CUST_BIN = ?'=>(string)$customer['CUST_BIN_TMP']
		);
		$customerupdateDataTrans = $this->dbObj->update('T_VP_TRANSACTION',$updateArrDataTrans,$whereArrDataTrans);
		/////////////////
		
		$whereArr = array(
			'CUST_ID = ?'=>(string)$customer['CUST_ID'],
			'CUST_BIN = ?'=>(string)$customer['CUST_BIN_TMP']
		);
		$customerupdate = $this->dbObj->update('M_CUSTOMER_BIN',$updateArr,$whereArr);

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Bank Guarantee)';
			return false;
		}
        //END update status customer


		
		//delete temp customer
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			      return false;

		return true;
	}*/

	public function approveDelete($actor = null)
	{
		//query from TEMP_CUSTOMER
		$bankguarantee = $this->dbObj->select()
						  ->from('TEMP_BANK_GUARANTEE')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($bankguarantee))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Bank Guarantee)';
			return false;
        }

		

        $updateArr = array_diff_key($bankguarantee,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['BG_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BG_UPDATEDBY']   = $actor;

		$whereArr = array(
							'BG_REG_NUMBER = ?'=>(string)$bankguarantee['BG_REG_NUMBER'],
							'BG_NUMBER = ?'=>(string)$bankguarantee['BG_NUMBER']
						);
						
		$bankguaranteeupdate = $this->dbObj->update('T_BANK_GUARANTEE',$updateArr,$whereArr);

		
	    // insert T_BANK_GUARANTEE_HISTORY
		$insertArr['DATE_TIME'] = new Zend_Db_Expr('now()');
		$insertArr['BG_REG_NUMBER']   = $bankguarantee['BG_REG_NUMBER'];
		$insertArr['USER_LOGIN'] =  $actor;
		$insertArr['CUST_ID']   = $bankguarantee['CUST_ID'];
		$insertArr['BG_REASON']   = 'Close';
		$insertArr['HISTORY_STATUS']   = $bankguarantee['BG_STATUS'];
		
		$bankguaranteeInsert = $this->dbObj->insert('T_BANK_GUARANTEE_HISTORY',$insertArr);
	
		
		if(!(boolean)$bankguaranteeupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(T_BANK_GUARANTEE)';
			return false;
		}

		if(!(boolean)$bankguaranteeInsert)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(T_BANK_GUARANTEE_HISTORY)';
			return false;
		}
        //END update status customer
		$customer = $this->dbObj->select()
						  ->from('M_CUSTOMER')
						  ->where('CUST_ID = ?',$bankguarantee['CUST_ID'])
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		// ---------------------------------- EMAIL NOTIF -----------------------------------------------
		$setting 	= new Settings();
			$template						= $setting->getSetting('femailtemplate_bg','');
			$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name','');
			$templateEmailMasterBankAppName 	= $setting->getSetting('master_bank_app_name','');
			$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp','');
			$templateEmailMasterBankWapp 	= $setting->getSetting('master_bank_wapp','');
			$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email','');
			$templateEmailMasterBankEmail1 	= $setting->getSetting('master_bank_email1','');
			$date 							= date("d M Y H:i:s");
			$config    		= Zend_Registry::get('config');
			$bgstatus 	= $config["bg"]["status"]["desc"];
			$bgstatusCode 	= $config["bg"]["status"]["code"];
			$bgstatusarr = array_combine(array_values($bgstatusCode),array_values($bgstatus));
			//var_dump($paytypesarr);die;
			$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];
		
				$arrStatus = array_combine(array_values($BgCode),array_values($BgType));	
			$amount = number_format($bankguarantee['BG_AMOUNT'],2,',','.');
		$translate = array(
				'[[title]]'                     => 'BG Close dari permintaan',
				'[[user_name]]'                 => $customer['CUST_NAME'],		
				'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
				'[[master_bank_app_name]]' 		=> $templateEmailMasterBankAppName,
				'[[MASTER_BANK_TELP]]' 			=> $templateEmailMasterBankTelp,
				'[[MASTER_BANK_EMAIL]]' 		=> $templateEmailMasterBankEmail,
				'[[MASTER_BANK_WAPP]]' 			=> $templateEmailMasterBankWapp,
				'[[MASTER_BANK_EMAIL1]]' 		=> $templateEmailMasterBankEmail1,
				'[[cust_name]]'					=> $customer['CUST_NAME'],
				'[[bg_number]]'					=> $bankguarantee['BG_REG_NUMBER'],
				'[[account_no]]'	 			=> $bankguarantee['ACCT_NO'],
				'[[subject]]' 					=> $bankguarantee['BG_SUBJECT'],
				'[[amount]]' 					=> 'IDR '.$amount,
				'[[status]]' 					=> $arrStatus[$bankguarantee['BG_STATUS']],
				'[[notes]]' 					=> $bankguarantee['BG_CANCEL_NOTES'],
				//'[[duedate]]' 					=> $resultQuery['0']['TIME_PERIOD_END'],
			);

		
		$subject = 'BG Close Notification';
		$useremail = $customer['CUST_EMAIL'];
		$mailContent = strtr($template,$translate); 
		//var_dump($useremail);
		//var_dump($mailContent);die;
		Application_Helper_Email::sendEmail($useremail,$subject,$mailContent);
		// ---------------------------------- END EMAIL NOTIF -------------------------------------------

		//delete temp customer
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			      return false;

		return true;
	}


	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BANK_GUARANTEE',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Bank Guarantee)';
//			return false;
//		}


		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BANK_GUARANTEE',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Bank Guarantee)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BANK_GUARANTEE',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Bank Guarantee)';
//			return false;
//		}

		return true;
	}

	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BANK_GUARANTEE',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Bank Guarantee)';
//			return false;
//		}

		return true;
	}

	public function deleteDelete()
	{


		$customerdelete = $this->dbObj->delete('TEMP_BANK_GUARANTEE',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;



	}


    public function approveDeactivate(){

	}

	public function approveActivate(){

	}

    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}





}