<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class registerechannel_SuggestiondetailController extends Application_Main{

  	public function indexAction()
  	{
		$this->_helper->layout()->setLayout('newpopup');

	    $model = new registerechannel_Model_Registerechannel();
	    $changes_id = $this->_getParam('changes_id');
	    $changes_id = (Zend_Validate::is($changes_id,'Digits'))? $changes_id : 0;

	    if($changes_id)
	    {
			$result = $this->getGlobalChanges($changes_id);
			if($result){
				if(!in_array($result['CHANGES_STATUS'],array('WA'))){
					$this->_redirect('/notification/invalid/index');
				}
				else
				{
					//content send to view
					$reg_type = $result['KEY_FIELD'];

					if($reg_type == 'BIS')
						$resultdata = $model->getTempBis($changes_id);
					else
						$resultdata = $model->getTempInd($changes_id);


					if($resultdata['TEMP_ID'])
					{
						foreach($resultdata as $key => $val){
							$id = strtolower($key);
							if($val == '' || $val == NULL)
								$val = "-";

							$this->view->$id = $val;
						}


						$isAuthorizeAppove =  false;
						$isAuthorizeReject =  false;


						if (!empty($this->_priviId)){
							$changeModulePrivilegeObj  = new Echanrequest_Model_Privilege();
							$isAuthorizeAppove = $changeModulePrivilegeObj->isAuthorizeApprove($result['MODULE']);
							$isAuthorizeReject = $changeModulePrivilegeObj->isAuthorizeReject($result['MODULE']);
						}

						$this->view->visibleButtonApprove = $isAuthorizeAppove;
						$this->view->visibleButtonReject = $isAuthorizeReject;

						$this->view->countryArr = Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');

						if($reg_type == 'BIS'){
							$userList = $model->getBisUser($resultdata['CUST_ID']);
							$acctList = $model->getBisAcct($resultdata['CUST_ID']);

							$this->view->userList = $userList;
							$this->view->acctList = $acctList;

							$name = $resultdata['CUST_NAME'];
						}
						else{
							$acctList = $model->getIndAcct($resultdata['TEMP_ID']);
							$this->view->acctList = $acctList;

							$name = $resultdata['USER_FULLNAME'];
						}


						$this->view->typeTitle = $result['DISPLAY_TABLENAME'];
						$this->view->reg_type = $result['KEY_FIELD'];
					}
					else{ $changes_id = 0; }
				}
			}
		}

	    if(!$changes_id)
	    {
	      	$error_remark = $this->language->_('Changes Id is not found');
		    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
		    $this->_redirect('/popuperror/index/index');
	    }

	    $this->view->changes_id = $changes_id;

	    if(!$this->_request->isPost()){
	    	Application_Helper_General::writeLog('VLRE','View E-Channel Registration Request List : '. $name);
	    }

	    if($this->_request->isPost()){
	    	$submit = $this->_getParam('submit');

	    	$gcmodel = new Echanrequest_Model_Globalchanges($changes_id);

	    	if($submit == "Approve"){
	    		$message = null;

	    		if($reg_type == "BIS"){
		    		$selUser = $this->_getParam('user_id');
		    		if(empty($selUser)){
		    			$message = $this->language->_('Please select user');
		    		}
		    	}

		    	$selAcc = $this->_getParam('acct_no');

	    		if(empty($selAcc)){
	    			if(!empty($message))
	    				$message.="<br/>".$this->language->_('Please select account');
	    			else
	    				$message = $this->language->_('Please select account');
	    		}

	    		if(empty($message)){
	    			$addData = array(
	    						'reg_type' => $result['KEY_FIELD'],
	    						'acctlist' => $selAcc
	    					);

	    			if($reg_type == "BIS")
	    				$addData['userlist'] = $selUser;

	    			$updated = $gcmodel->approve($this->_userIdLogin, $addData);
	    			$message = $this->language->_('Suggestion Granted');

	    			if($updated){
	    				$success = true;
	    				$error = false;
	    			}
	    			else{
	    				$message = $gcmodel->getErrorMessage();
	    				$success = false;
	    				$error = true;
	    			}
	    		}
	    		else{
	    			$error = true;
	    			$success = false;
	    		}
	    	}
	    	elseif($submit == 'Reject'){
	    		$updated = $gcmodel->reject($this->_userIdLogin);
				$message = $this->language->_('Suggestion Rejected');

				if($updated){
					$msg = "Maaf, pendaftaran e-Channel ".$result['DISPLAY_TABLENAME']." Anda ditolak.";

					$setting = new Settings();
					$template = $setting->getSetting('bemailtemplate_eregis');
					$templateEmailMasterBankName = $setting->getSetting('master_bank_name');
					$greeting = "Yth ".$resultdata['CUST_NAME'];

					$template = str_ireplace('[[greeting]]',$greeting,$template);
					$template = str_ireplace('[[eregistration_message]]',$msg,$template);
				 	$template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);

					$email = $resultdata['CUST_EMAIL'];
					$subject = "Pendaftaran E-Channel ".$result['DISPLAY_TABLENAME'];
					Application_Helper_Email::sendEmail($email,$subject,$template);

					$tempPhone = explode(";",$resultdata['CUST_PHONE']);
					$smsmsg = $greeting.",\n".$msg;

					if(count($tempPhone) > 1){
						for($i=0;$i<count($tempPhone);$i++){
							$sms = new Service_SMS($tempPhone[$i],$smsmsg);
							$sms->OutgoingSMS();
						}
					}
					else{
						$sms = new Service_SMS($resultdata['CUST_PHONE'],$smsmsg);
						$sms->OutgoingSMS();
					}

					$success = true;
					$error = false;
				}
				else{
					$message = $gcmodel->getErrorMessage();
					$success = false;
					$error = true;
				}
	    	}

			$this->view->message = $message;
			$this->view->success = $success;
			$this->view->error = $error;

			if($success)
				$refreshPage = true;
			else
				$refreshPage = false;

			$this->view->refreshPage = $refreshPage;
	    }
	    else{
	    	$this->_setReadSuggestion($changes_id);
	    	$this->view->message = '';
	    	$this->view->refreshPage = true;
	    }
  	}

  	public function viewdocAction()
  	{
	    $model = new registerechannel_Model_Registerechannel();

	    $cust_id = strtoupper($this->_getParam('cust_id'));
	    $type = $this->_getParam('type');

	    switch ($type) {
	    	case 1:
	    		$col = "AKTA_PP";
	    		break;
	    	case 2:
	    		$col = "AKTA_PT";
	    		break;
	    	case 3:
	    		$col = "SIUP_TDP";
	    		break;
	    	case 4:
	    		$col = "ID_PIHAK";
	    		break;
	    	case 5:
	    		$col = "OTHER_DOC";
	    		break;
	    	default:
	    		break;
	    }

	    $fileName = $model->getFileName($cust_id,$col);

	    if($fileName)
	    {
	        $this->_helper->viewRenderer->setNoRender();
	        $this->_helper->layout()->disableLayout();

	        $tempFile = explode(".", $fileName);
	        $ext = strtolower($tempFile[1]);
	        $loc = UPLOAD_PATH.'/document/temp/'.$fileName;

	        if($ext == "jpg" || $ext == "jpeg"){
	          	$this->getResponse()
	            	->setHeader('Content-Type', 'image/jpeg');
	         	readfile($loc);
	        }
	        else{
	          	$this->getResponse()
	                ->setHeader('Content-Disposition', 'inline; filename='.$fileName)
	                ->setHeader('Content-Type', 'application/pdf');
	         	$pdf = Zend_Pdf::load($loc);
	          	echo $pdf->render();
	        }

	    }else{
	    	$fileName = null;
	    }

	    $this->view->fileName = $fileName;

	    if(empty($fileName))
	    	$this->_helper->layout()->setLayout('popup');
 	}

  	private function _setReadSuggestion($changesId){
		$data = $this->_db->update('T_GLOBAL_CHANGES', array('READ_STATUS'=>1),$this->_db->quoteInto('CHANGES_ID = ?',$changesId));
	}
}
