<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';


class user_ForcelogController extends Application_Main{

  public function indexAction() 
  {
   
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));
    // user/view/index/cust_id/FIF00A1/user_id/PTFE_KELVIN013
    $this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$this->_getParam('cust_id').'/user_id'.$this->_getParam('user_id')); 
//    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>30)))? $user_id : null;
    $error_remark = null;
    
    if($cust_id)
    {
      $select = $this->_db->select()
  	                         ->from('M_CUSTOMER',array('CUST_ID'))
  	                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
  	                        // ->where('UPPER(CUST_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
      $result = $this->_db->fetchOne($select);
      if(!$result) $cust_id = null;
    }
    
    if(!$cust_id)
    {
      // $error_remark = $this->getErrorRemark('22','Customer ID');
       $error_remark = 'no customer';
      //insert log
      try {
	    $this->_db->beginTransaction();
	    $this->backendLog(strtoupper($this->_actionID['flog']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
        $this->_db->commit();
	  }
	  catch(Exception $e) {
	    $this->_db->rollBack();
  	    SGO_Helper_GeneralLog::technicalLog($e);
	  }
	    
	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
    }
    
    
    if($user_id)
    {
  	  $select = $this->_db->select()
  	                         ->from('M_USER',array('USER_ID','USER_ISLOGIN'))
  	                         ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
  	                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $result = $this->_db->fetchRow($select);
      
      if($result['USER_ID'])
      {
          if($result['USER_ISLOGIN'] == 1)
          {
            try 
            {
            	Application_Helper_General::writeLog('CSFL','Force Frontend User Logout [' .$user_id . ']');
		        $this->_db->beginTransaction();
		        $where  = 'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id);
		        $where .= ' AND UPPER(USER_ID)='.$this->_db->quote((string)$user_id);
		        $this->_db->update('M_USER',array('USER_ISLOGIN'=>0),$where);
		        $this->_db->commit();
		        
            $encrypted_custid = $AESMYSQL->encrypt($cust_id);
            $enccustid = urlencode($encrypted_custid);

            $encrypted_userid = $AESMYSQL->encrypt($user_id);
            $encuserid = urlencode($encrypted_userid);
		        $this->_redirect('/user/view/index/cust_id/'.$enccustid.'/user_id/'.$encuserid.'/resetMsgSucces/success/');
	  	    }
		    catch(Exception $e)
		    {
		       $this->_db->rollBack();
           $error_remark = 'failed query';
		       // $error_remark = $this->getErrorRemark('82');
		       SGO_Helper_GeneralLog::technicalLog($e);
		    }
          }
      }
      else{ $user_id = null; }
  	}

  	
  	if(!$user_id)
  	{
      // $error_remark = $this->getErrorRemark('22','User ID');
      $error_remark = 'empty user';
      $fulldesc = 'CUST_ID:'.$cust_id;
      $key_value = null;
  	}
  	else
  	{
  	  $fulldesc = null;
  	  $key_value = $cust_id.','.$user_id;
  	}
    
  	if($error_remark){ $msg = $error_remark; $class = 'F'; }
  	else
  	{
      $error_remark = 'empty user';
  	  // $msg = $this->getErrorRemark('00','User ID',$user_id);
  	  $class = 'S';
  	}

  	
  	
    //insert log
    try 
    {
	  $this->_db->beginTransaction();
	  $this->backendLog(strtoupper($this->_actionID['flog']),strtoupper($this->_moduleID['user']),$key_value,$fulldesc,$error_remark);
      $this->_db->commit();
	}
	catch(Exception $e) 
	{
	  $this->_db->rollBack();
  	  SGO_Helper_GeneralLog::technicalLog($e);
	}
	
	$this->_helper->getHelper('FlashMessenger')->addMessage($class);
	$this->_helper->getHelper('FlashMessenger')->addMessage($msg);
    $this->_redirect($this->_backURL);
  }
  
}

