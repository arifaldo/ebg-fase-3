<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
class binsetup_SuggestiondetailController extends binsetup_Model_Binsetup
{
  	protected $_moduleDB = 'CST';

	public function indexAction()
  	{
  		$this->_helper->layout()->setLayout('popup');
  		
  		$params = $this->_request->getParams();
  		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params)) $fullDesc = SGO_Helper_GeneralFunction::displayFullDesc($params);
  		else $fullDesc = null;

  	    $this->view->suggestionType = $this->_suggestType;
  		
  	    
  	    
  	    //cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
  	    // $changes_id = strip_tags( trim($this->_getParam('changes_id')) );

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;  


    $AESMYSQL = new Crypt_AESMYSQL();
    $PS_NUMBER      = urldecode($this->_getParam('changes_id'));
    $changes_id = $AESMYSQL->decrypt($PS_NUMBER, $password); 
  	      
	    $select = $this->_db->select()
		                     ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'))
		                     ->where("A.CHANGES_ID = ?",$changes_id)
		                     ->where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
                         // echo $select;die;
		$global_changes = $this->_db->fetchRow($select);
		 var_dump($global_changes);
		if(empty($global_changes))  $this->_redirect('/notification/invalid/index');
		//END cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
  	    
  	    
  		if(array_key_exists('changes_id', $params))
  		{  
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
  					// array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
					array('Db_RecordExists', array('table' => 'TEMP_CUSTOMER_BIN', 'field' => 'CHANGES_ID')),
					'messages' => array(
						'Can not be empty',
						'Invalid format',
						// 'Change id is not already exists in Master Global Changes',
						'Change id is not already exists in Temp Global Changes',
					                   ),
  				                    ),
  			                     );

  			
  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
  			
	  		if($zf_filter_input->isValid())
	  		{
	  		    $this->view->status_type = $this->_masterglobalstatus;
	  		    die('gere');
	  			$changeId  = $zf_filter_input->changes_id;
	  			$resultdata = $this->getTempCustomer($changeId);
	  			$keyValue  = $resultdata['CUST_ID'];
	  			
	  			//suggest data
	  			$this->view->changes_id     = $resultdata['CHANGES_ID'];
	  			$this->view->changes_type   = $resultdata['CHANGES_TYPE'];
	  			$this->view->changes_status = $resultdata['CHANGES_STATUS'];
	  			$this->view->read_status    = $resultdata['READ_STATUS'];
	  			$this->view->created        = $resultdata['CREATED'];
	  			$this->view->created_by     = $resultdata['CREATED_BY'];
	  			
	  			
 				//temp customer data
	  			$this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
      		    $this->view->cust_bin      = strtoupper($resultdata['CUST_BIN']);
      		    $this->view->cust_bin_status      = strtoupper($resultdata['CUST_BIN_STATUS']);
      		   
      		    $this->view->cust_created     = $resultdata['BIN_CREATED'];
      		    $this->view->cust_createdby   = $resultdata['BIN_CREATEDBY'];
      		    $this->view->cust_suggested   = $resultdata['BIN_SUGGESTED'];
      		    $this->view->cust_suggestedby = $resultdata['BIN_SUGGESTEDBY'];
      		    $this->view->cust_updated     = $resultdata['BIN_UPDATED'];
      		    $this->view->cust_updatedby   = $resultdata['BIN_UPDATEDBY'];
              $this->view->cust_bin_name   = $resultdata['BIN_NAME'];
      		    
      		    //master customer data
      		    $m_resultdata = $this->getCustomer(strtoupper($resultdata['CUST_ID']),$resultdata['CUST_BIN_TMP']);
      		    // print_r($m_resultdata);die;
      		    $this->view->m_cust_id      	= strtoupper($m_resultdata['CUST_ID']);
       			$this->view->m_cust_name    	= $m_resultdata['CUST_NAME'];
      		    $this->view->m_cust_bin    		= $m_resultdata['CUST_BIN'];
      		    $this->view->m_cust_status   	= $m_resultdata['CUST_BIN_STATUS'];
      		    
      		    $this->view->m_cust_created     = $m_resultdata['BIN_CREATED'];
      		    $this->view->m_cust_createdby   = $m_resultdata['BIN_CREATEDBY'];
      		    $this->view->m_cust_suggested   = $m_resultdata['BIN_SUGGESTED'];
      		    $this->view->m_cust_suggestedby = $m_resultdata['BIN_SUGGESTEDBY'];
      		    $this->view->m_cust_updated     = $m_resultdata['BIN_UPDATED'];
      		    $this->view->m_cust_updatedby   = $m_resultdata['BIN_UPDATEDBY'];
              $this->view->m_cust_bin_name   = $m_resultdata['BIN_NAME'];
	  		}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			$errorRemark = SGO_Helper_GeneralFunction::displayErrorRemark($errors);
	  			Application_Helper_General::writeLog('BNLS','View customer BIN changes list');

	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
      			$this->_redirect('/popuperror/index/index');
	  		}
  		}
  		else
  		{
  			$errorRemark = 'Changes Id not found';
			//Application_Helper_General::writeLog('CCCL','');

	  		$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
      		$this->_redirect('/popuperror/index/index');
  		}
  		if(!$this->_request->isPost()){
  		Application_Helper_General::writeLog('BNLS','View customer BIN changes list');
  		}
  		
  		
	}
}



