<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class backenduser_EditController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));

		$setting = new Settings();
		$minBuserID = $setting->getSetting('min_length_buserid');
		$maxBuserID = $setting->getSetting('max_length_buserid');
		$this->view->minBuserID = $minBuserID;
		$this->view->maxBuserID = $maxBuserID;

		if ($user_id) {
			$select	= $this->_db->select()
				->distinct()
				->from(
					array('MB' => 'M_BUSER'),
					array(
						'BUSER_ID' => 'MB.BUSER_ID',
						'BUSER_STATUS' => 'MB.BUSER_STATUS',
						'BUSER_NAME' => 'MB.BUSER_NAME',
						'BUSER_BRANCH' => 'MB.BUSER_BRANCH',
						'BUSER_EMAIL' => 'MB.BUSER_EMAIL',
						'BUSER_NIK' => 'MB.BUSER_NIK',
						'BGROUP_ID' => 'MB.BGROUP_ID',
						'BUSER_ISLOCKED' => 'MB.BUSER_ISLOCKED',
						'BUSER_LASTLOGIN' => 'MB.BUSER_LASTLOGIN',
						'BUSER_DESC' => 'MB.BUSER_DESC',
						"BUSER_ISNEW" => "MB.BUSER_ISNEW"
					)
				)
				->where('MB.BUSER_ID =?', $user_id);

			$resultdata = $this->_db->fetchRow($select);

			if ($resultdata['BUSER_ID']) {
				$this->view->userId     		= strtoupper($resultdata['BUSER_ID']);
				$this->view->groupId    		= $resultdata['BGROUP_ID'];
				$this->view->branch    			= $resultdata['BUSER_BRANCH'];
				$this->view->name				= $resultdata['BUSER_NAME'];
				$this->view->email				= $resultdata['BUSER_EMAIL'];
				$this->view->nik				= $resultdata['BUSER_NIK'];
				$this->view->desc   	 		= $resultdata['BUSER_DESC'];
				$this->view->newbuser   	 	= $resultdata['BUSER_ISNEW'];
			} else {
				$user_id = null;
			}
		}



		$selectBranch =	$this->_db->select()
			->from('M_BRANCH', array('ID', 'BRANCH_NAME'));
		//->where('PASSWORD = \''.$v_password_old.'\'');

		// echo $v_password;
		$branch = $this->_db->fetchAll($selectBranch);
		$arrbranch = Application_Helper_Array::listArray($branch, 'ID', 'BRANCH_NAME');

		$this->view->arrBranch = $arrbranch;

		$flbranch = Application_Helper_Array::listArray($branch, 'BRANCH_NAME', 'ID');
		$flip = Application_Helper_Array::listArray($group, 'BGROUP_DESC', 'BGROUP_ID');


		$selectGroup =	$this->_db->select()
			->from('M_BGROUP', array('BGROUP_ID', 'BGROUP_DESC'))
			->where("BGROUP_STATUS = '1'");
		//->where('PASSWORD = \''.$v_password_old.'\'');

		// echo $v_password;
		$group = $this->_db->fetchAll($selectGroup);
		$arrgroup = Application_Helper_Array::listArray($group, 'BGROUP_ID', 'BGROUP_DESC');
		$arrgroup = array('' => '-- ' . $this->language->_('Any Value') . ' --') + $arrgroup;
		$this->view->arrGroup = $arrgroup;
		$flip = Application_Helper_Array::listArray($group, 'BGROUP_DESC', 'BGROUP_ID');


		//Zend_Debug::dump($arrgroup);die;
		if ($this->_request->isPost()) {

			$filters = array(
				'userId'		=> array('StripTags', 'StringTrim'),
				'name'			=> array('StripTags', 'StringTrim'),
				'email' 		=> array('StripTags', 'StringTrim'),
				'nik' 			=> array('StripTags', 'StringTrim'),
				'description' 	=> array('StripTags'),
				'groupId'  		=> array('StripTags', 'StringTrim'),
				'branch'  		=> array('StripTags', 'StringTrim')
			);

			$validators = array(
				'userId' => array(
					'NotEmpty',
					//array('StringLength', array('min' => 5, 'max' => 16)),
					array('StringLength', array('min' => $minBuserID, 'max' => $maxBuserID)),

					'messages' => array(
						$this->language->_('Can not be empty, Invalid user Id format'),
						// $this->language->_('Invalid user Id format'),
						$this->language->_('Minimal ' . $minBuserID . ' karakter dan maksimal ' . $maxBuserID . ' karakter'),
						//						$this->language->_('Invalid user Id format'),
					)
				),
				'name' => array(
					'NotEmpty',
					// array('StringLength', array('max' => 20)),
					'messages' => array(
						$this->language->_('Can not be empty'),
						// $this->language->_('Invalid user name format'),
					)
				),
				'email' => array(
					'NotEmpty',
					//new SGO_Validate_EmailAddress(),
					array('StringLength', array('min' => 1, 'max' => 128)),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Email lenght cannot be more than 128'),
					)
				),
				'email' => array(
					'NotEmpty',
					//new SGO_Validate_EmailAddress(),
					array('StringLength', array('min' => 1, 'max' => 128)),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Email lenght cannot be more than 128'),
					)
				),
				'nik' => array(
					'NotEmpty',
					//new SGO_Validate_EmailAddress(),
					array('StringLength', array('min' => 16, 'max' => 16)),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Minimal 16 digit'),
					)
				),
				'groupId' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Please Select Group'),
					)
				),
				'branch' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Please Select Branch'),
					)
				),
			);
			$params = $this->_request->getParams();
			// $params["email"] = $params["email"] . "@btn.co.id";
			$params["email"] = explode("@", $params["email"])[0] . "@btn.co.id";
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

			//validasi multiple email
			if ($this->_getParam('email')) {
				$validate = new validate;
				// $cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('email'));
				$cek_multiple_email = $validate->isValidEmailMultiple($params["email"]);
			} else {
				$cek_multiple_email = true;
			}

			//$groupid = $this->_request->getParam('groupId');
			//$valid = new Zend_Validate_Identical(array($groupid => '0'));

			//			CEK HURUF DAN ANGKA DI USERID
			$strtemp = 0;
			$digittemp = 0;
			$user_id = $zf_filter_input->userId;
			$str = str_split($user_id);
			// foreach ($str as $string) {
			// 	if ((Zend_Validate::is($string, 'Digits')) ? true : false) {
			// 		$digittemp = 1;
			// 	}
			// 	if ((Zend_Validate::is($string, 'Alpha')) ? true : false) {
			// 		$strtemp = 1;
			// 	}
			// }

			//			echo $strtemp.' '.$digittemp;die;
			//			CEK GROUPID
			//			$group = (($this->_getParam('groupId')==0) ? false : true);
			//Zend_Debug::dump($this->_getParam('groupId'));die;

			if ($zf_filter_input->isValid() && $this->view->hasPrivilege('ADUD') && $cek_multiple_email == true) {
				//Zend_Debug::dump($zf_filter_input->description);die;

				$email = $zf_filter_input->email;

				$selecttemp = $this->_db->select()
					->from('TEMP_BUSER')
					->where('BUSER_ID = ?', $user_id)
					->query()->fetchAll();

				if (count($selecttemp) == 0) {
					Application_Helper_General::writeLog('ADUD', 'Editing Backend User (' . $user_id . ')');
					$info = 'Edit Backend User';
					$change_id = $this->suggestionWaitingApproval('Backend User', $info, 'E', null, 'M_BUSER', 'TEMP_BUSER', $params['userId'], $params['name']);

					$this->_db->beginTransaction();
					try {

						$insertArr = array(
							'CHANGES_ID' 		=> $change_id,
							'BUSER_ID'			=> $zf_filter_input->userId,
							'BUSER_NAME' 		=> $zf_filter_input->name,
							'BGROUP_ID' 		=> $zf_filter_input->groupId,
							'BUSER_BRANCH' 		=> $zf_filter_input->branch,
							'BUSER_DESC' 		=> $params['desc'],
							'BUSER_EMAIL'		=> $zf_filter_input->email,
							'BUSER_NIK'			=> $zf_filter_input->nik
						);

						// 						Zend_Debug::dump($insertArr);die;
						$this->_db->insert('TEMP_BUSER', $insertArr);
						$this->_db->commit();
						$this->setbackURL('/' . $this->_request->getModuleName() . '/index/index/');
						$this->_redirect('/notification/submited/index');
						$this->view->success 	= true;
						$this->view->successMsg = $this->language->_('Record Saved');
						// $insertArr = array(
						// 	'CHANGES_ID' 		=> $change_id,
						// 	'BUSER_ID'			=> $zf_filter_input->userId,
						// 	'BUSER_NAME' 		=> $zf_filter_input->name,
						// 	'BGROUP_ID' 		=> $zf_filter_input->groupId,
						// 	'BUSER_ BRANCH' 	=> $zf_filter_input->branch,
						// 	'BUSER_DESC' 		=> $params['desc'],
						// 	'BUSER_EMAIL'		=> $zf_filter_input->email
						// 	// 'BUSER_STATUS'		=> '1'
						// );
						// // echo '<pre>';
						// // print_r($insertArr);die;

						// $this->_db->getProfiler()->setEnabled(true);
						// $this->_db->insert('TEMP_BUSER', $insertArr);
						// Zend_Debug::dump($this->_db->getProfiler()->getLastQueryProfile()->getQuery());
						// Zend_Debug::dump($this->_db->getProfiler()->getLastQueryProfile()->getQueryParams());
						// $this->_db->getProfiler()->setEnabled(false);
						// $this->_db->commit();
						// die('here');
						// $this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
						// $this->_redirect('/notification/submited/index');
					} catch (Exception $e) {
						$this->_db->rollBack();
					}
				} else if (count($selecttemp) > 0) {
					$this->view->error 			= true;
					$errors 					= "$user_id " . $this->language->_('is already on approval process') . "";
					$this->view->errorMsg 		= $errors;
				}
			} else {

				//var_dump($zf_filter_input->getMessages());
				//die('here');
				if (isset($cek_multiple_email) && $cek_multiple_email == false) {
					$errr[] = array($this->language->_('Invalid email format'));
					$this->view->xemail = $this->displayError($errr);
				}

				//$docErr = $this->displayError($zf_filter_input->getMessages());
				foreach ($zf_filter_input->getMessages() as $key => $err) {
					$xxx = 'x' . $key;
					$this->view->$xxx = $this->displayError($err);
				}
				//$this->view->error 			= true;
				//$this->view->errorMsg 		= $docErr;
				$this->view->userId 		= ($zf_filter_input->isValid('userId')) ? $zf_filter_input->userId : $params['userId'];
				$this->view->name	 		= ($zf_filter_input->isValid('name')) ? $zf_filter_input->name : $params['name'];
				$this->view->branch	 		= ($zf_filter_input->isValid('branch')) ? $zf_filter_input->branch : $params['branch'];
				$this->view->desc 	= ($zf_filter_input->isValid('desc')) ? $zf_filter_input->desc : $params['desc'];
				$this->view->email	 		= ($zf_filter_input->isValid('email')) ? $zf_filter_input->email : $params['email'];
				$this->view->nik	 		= ($zf_filter_input->isValid('nik')) ? $zf_filter_input->nik : $params['nik'];
				$this->view->groupId	 	= ($zf_filter_input->isValid('groupId')) ? $zf_filter_input->groupId : $params['groupId'];
				//				echo $docErr,die;
			}
			Application_Helper_General::writeLog('ADUD', 'Edit Backend User (' . $user_id . ')');
		} else {
			Application_Helper_General::writeLog('ADUD', 'Viewing Edit Backend User (' . $user_id . ') page');
		}

		if (!empty($resultdata['BUSER_DESC'])) {
			$desc 		 = $resultdata['BUSER_DESC'];
		} else {
			$desc 		 = $this->_request->getParam('desc');
		}
		$desc_len 	 = (isset($desc))  ? strlen($desc)  : 0;
		$desc_len 	 = 200 - $desc_len;

		$this->view->desc_len		= $desc_len;
	}
}
