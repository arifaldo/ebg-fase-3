<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'Crypt/AESMYSQL.php';

class payment_NewtaxController extends Application_Main
{
	public function initModel()
	{
		$this->model 					= new payment_Model_Payment();
		$this->param['SERVICE_TYPE'] 	= 6;
		$this->param['PROVIDER_TYPE'] 	= 1;
		$this->param['fetch'] 			= 'fetchAll';
		$this->param['USER_ID'] 		= $this->_userIdLogin;
		// 		$this->CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$this->CustomerUser 			= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace 			= new Zend_Session_Namespace('newtax');
		$paramSession 				= $sessionNamespace->paramSession;
		$this->view->radioCheck		= 1;
		$paramac = array(
			'CCY_IN' => 'IDR'
		);
		$AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
		$this->view->AccArr 		= $AccArr;

		$anyValue = '-- ' . $this->language->_('Any Value') . ' --';

		$arr 						= $this->model->getProviderId($this->param['SERVICE_TYPE'], $this->param['PROVIDER_TYPE'], $this->_userIdLogin);
		// 		print_r($arr);die;
		$providerArr 				= array('' => $anyValue);
		$providerArr 				+= Application_Helper_Array::listArray($arr, 'PROVIDER_ID', 'PROVIDER_NAME');
		$this->view->taxproviderArr 	= $providerArr;




		//select m_user - begin
		$userData = $this->CustomerUser->getUser($this->_userIdLogin);

		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->amount 		= '5000';
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];
		$tokenIdUser 				= $userData['TOKEN_ID'];
		$tokenType 					= $userData['TOKEN_TYPE'];
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);

		//added new hard token
		$HardToken 						= new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode 					= $HardToken->generateChallengeCode();
		$this->view->challengeCode 		= $challengeCode;
		$this->view->challengeCodeReq 	= $challengeCode;
		//select m_user - end

		$list 							= $this->model->cekList($this->param);
		$this->view->paymentList 		= $list;

		$inArrayTahun 					= array(
			'2010' => '2010',
			'2011' => '2011',
			'2012' => '2012',
			'2013' => '2013',
			'2014' => '2014',
			'2015' => '2015'
		);
		//$tahunArr 				= array(''=>$anyValue);
		$tahunArr 						= $inArrayTahun;
		$this->view->tahunArr 			= $tahunArr;
		$this->view->token = false;
		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);

			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}
		//Zend_Debug::dump($this->_getAllParams());

		//added cms purchase payment get repair - begin
		$PS_NUMBER 						= $this->_getParam('PS_NUMBER');
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';

		if (!empty($PS_NUMBER)) {
			$paramList = array(
				"WA" 			=> false,
				"ACCOUNT_LIST" 	=> $this->_accountList,
				"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
			);
			$select   = $this->CustomerUser->getPayment($paramList, false);
			$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

			$select->columns(
				array(
					"tra_message"		=> "T.TRA_MESSAGE",
					"tra_refno"			=> "T.TRA_REFNO",
					"acbenef_email"		=> "T.BENEFICIARY_EMAIL",
					"provider_id"			=> "P.PS_BILLER_ID",
					"order_id"	=> "T.BILLER_ORDER_ID",
					//"acbenef_bankname"	=> "T.BENEFICIARY_ACCOUNT_NAME",
					//"acbenef_alias"		=> "T.BENEFICIARY_ALIAS_NAME",
				)
			);

			$pslipData 	= $this->_db->fetchRow($select);
			//Zend_Debug::dump($pslipData);

			if (!empty($pslipData)) {
				// $PS_EFDATE  		= $pslipData['efdate'];  
				$PS_EFDATE  		= date("d/m/Y", strtotime($pslipData['efdate']));
				$sourceAcct = $pslipData['accsrc'];
				$prevoperator = $pslipData['provider_id'];

				//check if existed in paylist
				$cParam = array();
				$cParam['PROVIDER_ID'] = $pslipData['provider_id'];
				$cParam['USER_ID'] = $this->_userIdLogin;
				$cParam['CUST_ID']	= $this->_custIdLogin;
				$cParam['REF_NO']	= $pslipData['order_id'];
				$cParam['fetch']	= 'fetchRow';

				$cekPay = $this->model->cekList($cParam);

				if (!empty($cekPay))
					$typeoforderid = "2";
				else
					$typeoforderid = "1";

				$prevorderid = $pslipData['order_id'];

				// $param['ACCTSRC'] 	= $pslipData['accsrc']; 
				// $logObj 			= json_decode($pslipData['LOG']); 
				// $param 				= (array) $logObj;
				// $operator 			= $param['operator'];
				// $sgoProductCode 	= $param['sgoProductCode'];

				// $model 				= new purchasing_Model_Purchasing();
				// $listvoucherNew 	= $model->getProviderId($operator);
				// $providerIdArrtest 	= array('empty' => ' --- Please Select ---');
				// $providerIdArrtest 	+= Application_Helper_Array::listArray($listvoucherNew,'NOMINAL','NOMINAL');
				// $cekAmountList 		= $this->view->amountList 		= $listvoucherNew;

				// $userDataServiceProvider 	= $this->CustomerUser->getServiceProvider($operator);
				// $provInquiry 				= $this->view->provInquiry = $userDataServiceProvider['PROVIDER_INQUIRY_SERVICE_STATUS'];

				// $getAmount 			= $model->getProductCode($sgoProductCode);
				// $sgoProductCodeRep 	= $getAmount['0']['SGO_PRODUCT_CODE'];
				//Zend_Debug::dump($getAmount);
			}
		}
		//added cms purchase payment get repair - end

		if ($this->_getParam('operator')) {
			//select m_service_provider - begin
			$userDataServiceProvider 	= $this->CustomerUser->getServiceProvider($this->_getParam('operator'));
			$provInquiry 	= $this->view->provInquiry = $userDataServiceProvider['PROVIDER_INQUIRY_SERVICE_STATUS'];
			$provCode 		= $this->view->provCode 	= $userDataServiceProvider['PROVIDER_CODE'];

			// 			print_r($provCode)

			$msg = array();
			$param = array();

			$param['SERVICE_TYPE'] 		= 6;
			$param['PROVIDER_TYPE'] 	= 1; //1
			$param['operator']			= $this->_getParam('operator');
			$param['ACCTSRC']			= $this->_getParam('ACCTSRC');
			$param['cek']				= $this->_getParam('cek');
			$param['radioCheck']		= $this->_getParam('radioCheck');
			$param['payment']			= $this->_getParam('payment');
			$param['USER_ID'] 			= $this->_userIdLogin;
			$param['challengeCodeReq'] 	= $this->_getParam('challengeCodeReq');
			$param['responseCodeReq'] 	= $this->_getParam('responseCodeReq');
			$param['TOKEN_TYPE'] 		= $tokenType;
			$param['TOKEN_ID'] 			= $tokenIdUser;
			$param['CUST_ID'] 			= $this->_custIdLogin;
			$param['ACCT_NO'] 			= $this->_getParam('ACCTSRC');
			$param['year'] 				= $this->_getParam('tahun');
			$param['message']			= $this->_getParam('TRA_MESSAGE');

			// 			$arr 						= $this->model->getProviderIdTaxOp($this->param['SERVICE_TYPE'],$this->param['PROVIDER_TYPE'],$this->_userIdLogin,$param['operator']);
			// 		
			$arr = array();
			$providerArr 				= array('' => $anyValue);
			$providerArr 				+= Application_Helper_Array::listArray($arr, 'REF_NO', 'REF_NO');
			// 			print_r($arr);
			// 			print_r($providerArr);die;
			$this->view->providerArr 	= $providerArr;

			if ($provCode == 'PKB') { //PAJAK KENDARAAN BERMOTOR
				$policeNumber  = strtoupper(str_replace(' ', '', $this->_getParam('orderId')));
				$jum 		= strlen($policeNumber);
				$sub 		= substr($policeNumber, 0, 1);
				$sub1 		= substr($policeNumber, 1, 1);

				if ($sub == 'B') {
					if (is_numeric($sub1) == true) {
						//$policeNumberNew = str_replace('B', '', $policeNumber);
						$policeNumberNew = substr($policeNumber, 1, $jum);
						//echo'jakarta';
					} else {
						$policeNumberNew = $policeNumber;
						//echo'lainnya';	
					}
				} else {
					$policeNumberNew = $policeNumber;
					//echo'lainnya';
				}

				if ($policeNumber == '') {
					$policeNo = '';
				} else {
					$policeNo = $policeNumberNew;
				}
				$convertData = $this->model->getConvertHuruf($policeNo);

				$orderid 			= $convertData;
			} else {
				$orderid 			= $this->_getParam('orderId');
			}

			if ($provInquiry == 'N') {
				$param['provInquiry'] 	= $provInquiry;
				$param['amount'] 		= Application_Helper_General::convertDisplayMoney($this->_getParam('amount'));
			} else {
				$param['provInquiry'] 	= $provInquiry;
			}

			$param['USER_MOBILE_PHONE'] = $userMobilePhone;

			if ($param['radioCheck'] == 1) {
				if ($provCode == 'PKB') {
					$param['orderId'] = (empty($orderid) ? '' : $orderid);
				} else {
					//$param['orderId'] = (empty($orderid) ? '' : $orderid.$param['year']);
					$param['orderId'] = (empty($orderid) ? '' : $orderid);
				}
			} else if ($param['radioCheck'] == 2) {
				$param['orderId'] = $this->_getParam('payment');
			}

			//tax type
			$prefixtaxType = substr($param['orderId'], 0, 1);

			if ($prefixtaxType == 0 || $prefixtaxType == 1 || $prefixtaxType == 2 || $prefixtaxType == 3) {
				$taxType = "DJP";
			} elseif ($prefixtaxType == 4 || $prefixtaxType == 5 || $prefixtaxType == 6) {
				$taxType = "DJBC";
			} elseif ($prefixtaxType == 7 || $prefixtaxType == 8 || $prefixtaxType == 9) {
				$taxType = "DJA";
			}
			//end tax type

			$sgo_product_code_data	= $this->model->getSgoProductCode($param['operator']);
			$sgo_product_code 		= $sgo_product_code_data['0']['SGO_PRODUCT_CODE'];

			$param['productCode'] 	= $sgo_product_code; //STCKAI

			$sendProvider	 = new SinglePayment(null, $this->_userIdLogin);

			$operator_data = $this->_getParam('operator');
			$this->view->operator = $operator_data;
			if (!empty($operator_data)) {

				$this->view->pkb_select = "selected";
				if ($param['operator'] == '1006') {
					$param['amount'] = '240000';
				}
				if ($this->_custSameUser) {
					if (!$this->view->hasPrivilege('PRLP')) {
						// die('here');
						$msg = $this->language->_("Error: You don't have privilege to release payment");
						$this->view->error = true;
						$resProvider = false;
						$this->view->ERROR_MSG_N = $this->language->_("Error: You don't have privilege to release payment");
					} else {
						$challengeCode		= $this->_getParam('challengeCode');
						$inputtoken1 		= $this->_getParam('inputtoken1');
						$inputtoken2 		= $this->_getParam('inputtoken2');
						$inputtoken3 		= $this->_getParam('inputtoken3');
						$inputtoken4 		= $this->_getParam('inputtoken4');
						$inputtoken5 		= $this->_getParam('inputtoken5');
						$inputtoken6 		= $this->_getParam('inputtoken6');

						$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

						$userOnBehalf = $this->_userIdLogin;
						$HardToken 	= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
						$resHard = $HardToken->verifyHardToken($challengeCode, $responseCode);
						$resultToken = $resHard['ResponseCode'] == '0000';

						if ($resHard['ResponseCode'] != '0000') {
							$tokenFailed = $CustUser->setLogToken(); //log token activity

							$this->view->error = true;
							$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

							if ($tokenFailed === true) {
								$this->_redirect('/default/index/logout');
							}
						} else {
							/*$paymentData = array('PS_RELEASER_CHALLENGE' => $challengeCode);

									$whereData 	 = array('PS_NUMBER = ?' => (string) $PS_NUMBER,
														 'CUST_ID = ?'	 => (string) $this->_custIdLogin);

									$this->_db->update('T_PSLIP', $paymentData, $whereData);*/
							$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
							$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
						}
					}
				} else {
					$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
					$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
				}
				// $resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
				// print_r($msg);die;
				// if($msg['responseCodeReq']['0'] == 'force_logout'){
				// 	$this->_forward('home'); //harus ketendan kareng salah token sebanyak
				// 	//$this->_redirect('/home');
				// }
				// 			print_r($resProvider);die;
				if ($resProvider == TRUE) {
					// 			
					// 				print_r($msg);die;	
					$paramSession['operator']			= $param['operator']; //1168 = Repirnt, 1167 = pembayaran
					$paramSession['ACCTSRC']			= $param['ACCTSRC'];
					$paramSession['ccy']				= $msg['detailCustomer']['0']['CCY_ID'];
					$paramSession['ACCT_NAME']			= $msg['responseData']['0']['customer_name'];
					$paramSession['ACCT_NAME_DATA']		= $msg['detailCustomer']['0']['ACCT_NAME'];
					$paramSession['ACCT_ALIAS_NAME']	= $msg['detailCustomer']['0']['ACCT_NAME'];
					$paramSession['ACCT_TYPE']			= $msg['detailCustomer']['0']['ACCT_TYPE'];
					$paramSession['ACCT_DESC']			= $msg['detailCustomer']['0']['ACCT_DESC'];
					$paramSession['FREEZE_STATUS']		= $msg['detailCustomer']['0']['FREEZE_STATUS'];
					$paramSession['MAXLIMIT']			= $msg['detailCustomer']['0']['MAXLIMIT'];
					$paramSession['amount']				= $msg['responseData']['0']['amount'];
					$paramSession['fee']				= $msg['fee']['0'];
					$paramSession['total']				= $msg['responseData']['0']['amount'] + $msg['fee']['0'];
					$paramSession['sgoProductCode']		= $msg['param']['0']['productCode'];
					$paramSession['operatorNama']		= $msg['provider']['operatorName'];
					$paramSession['operatorAliasName']	= $msg['provider']['operatorAliasName'];
					$paramSession['CUST_ID']			= $this->_custIdLogin;
					$paramSession['USER_ID']			= $this->_userIdLogin;
					$paramSession['TOKEN_TYPE']			= $tokenType;
					$paramSession['USER_MOBILE_PHONE']	= $userMobilePhone;

					$paramSession['message']			= $msg['param']['0']['Message'];
					$paramSession['PS_CATEGORY']		= $msg['param']['0']['PS_CATEGORY'];
					$paramSession['TypeOfTrans']		= $msg['param']['0']['TypeOfTrans'];
					$paramSession['PS_CATEGORY_EMAIL']	= $msg['param']['0']['PS_CATEGORY_EMAIL'];
					$paramSession['PS_TYPE']			= $msg['param']['0']['PS_TYPE'];
					$paramSession['Module']				= $msg['param']['0']['Module'];

					//khusus pbb
					$paramSession['name']				= $msg['responseData']['0']['name'];
					$paramSession['nop']				= $msg['responseData']['0']['nop']; //orderid
					$paramSession['address']			= $msg['responseData']['0']['address'];
					$paramSession['sub_district']		= $msg['responseData']['0']['sub_district'];
					$paramSession['regency']			= $msg['responseData']['0']['regency'];
					$paramSession['province']			= $msg['responseData']['0']['province'];
					$paramSession['year']				= $param['year'];

					$paramSession['cek']				= $param['cek'];
					$paramSession['dataUi']				= $msg['responseData']['0']['dataUi'];

					if ($param['radioCheck'] == 1) {
						$paramSession['orderId']		= $param['orderId'];
					} else if ($param['radioCheck'] == 2) {
						$paramSession['orderId']		= $param['payment'];
					}
					$paramSession['taxType']			= $taxType;


					if ($param['operator'] == 1167) { // penerimaan/pembayaran
						$redirect = '/payment/newtax/next';
						$paramSession['reprint'] = 0;
					} else if ($param['operator'] == 1158) {
						$paramSession['dataUi']['NOP'] = '10.123.1234.2345';
						$paramSession['dataUi']['customer_name'] = 'Julian Tandjung';
						$paramSession['dataUi']['customer_address'] = 'Jl. Purwosari';

						$paramSession['sub_district'] = 'MIJEN';
						$paramSession['district'] = 'POLAMAN';
						$paramSession['regency'] = 'KOTA SEMARANG';
						$paramSession['province'] = 'JAWA TENGAH';
						$paramSession['total'] = 245000;
						// 					$paramSession[''] = '10.123.1234.2345';
						$redirect = '/payment/newtax/next';
						$paramSession['reprint'] = 0;
					} elseif ($param['operator'] == 1157) { // reprint					
						$redirect = '/payment/newtax/confirm';
						$paramSession['reprint'] = 1;
					}

					$paramSession['PS_NUMBER']			= $PS_NUMBER;
					// print_r($redirect);
					// print_r($paramSession);die;
					$sessionNamespace->paramSession = $paramSession;

					//$this->_redirect('/payment/tax/confirm');
					$this->_redirect($redirect);
				} else {
					$errors 	= $msg;
					$error		= true;
				}
			} else {

				// 				$this->view->cetak_select = "selected";
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				if ($operator_data == '1167')
					$this->view->negara_select = "selected";
				if ($operator_data == '1006')
					$this->view->pbb_select = "selected";
				if ($operator_data == '1145')
					$this->view->pkb_select = "selected";
				if ($operator_data == '1155')
					$this->view->cetak_select = "selected";

				$paramSession = array();
				$this->view->operator = true;
				$paramSession['operator']			= $paramSession['operator'];
				$paramSession['ACCTSRC']			= $param['ACCTSRC'];
				$paramSession['ccy']				= $paramSession['ccy'];
				$paramSession['ACCT_NAME']			= $paramSession['ACCT_NAME'];
				$paramSession['ACCT_NAME_DATA']		= $paramSession['ACCT_NAME_DATA'];
				$paramSession['ACCT_ALIAS_NAME']	= $paramSession['ACCT_ALIAS_NAME'];
				$paramSession['amount']				= $paramSession['amount'];
				$paramSession['fee']				= $paramSession['fee'];
				$paramSession['total']				= $paramSession['total'];
				$paramSession['orderId']			= $paramSession['orderId'];
				$paramSession['sgoProductCode']		= $paramSession['sgoProductCode'];
				$paramSession['operatorNama']		= $paramSession['operatorNama'];
				$paramSession['CUST_ID']			= $paramSession['CUST_ID'];
				$paramSession['USER_ID']			= $paramSession['USER_ID'];
				$paramSession['PSNumber']			= $msg['PS_NUMBER']['0'];
				$paramSession['date']				= $paramSession['date'];
				$paramSession['paymentMessage']		= $msg['paymentMessage']['0'];
				$paramSession['dataUi']				= $paramSession['dataUi'];

				$sessionNamespace->paramSession 	= $paramSession;

				if ($this->_getParam('orderId') != '' || $this->_getParam('payment') != '') {
					$this->_redirect('/payment/newtax/confirm');
				}
			}

			if ($error) {
				if ($this->_getParam('next') == $this->language->_('Next')) {
					$this->view->error 					= $error;
					$this->view->operatorErr 			= (isset($errors['operator'])) ? $errors['operator'] : null;
					$this->view->orderIdErr 			= (isset($errors['orderId'])) ? $errors['orderId'] : null;
					$this->view->paymentErr 			= (isset($errors['payment'])) ? $errors['payment'] : null;
					$this->view->ACCTSRCErr 			= (isset($errors['ACCTSRC'])) ? $errors['ACCTSRC'] : null;
					$this->view->amountErr 				= (isset($errors['amount'])) ? $errors['amount'] : null;
					$this->view->responseCodeReqErr 	= (isset($errors['responseCodeReq'])) ? $errors['responseCodeReq'] : null;
					$this->view->xACCTSRC 				= (isset($errMsg)) ? $errMsg : null;
				}

				$this->view->operator				= $param['operator'];
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				$this->view->cek					= $param['cek'];
				$this->view->radioCheck				= $param['radioCheck'];
				$this->view->themes					= $this->_getParam('themes');
				$this->view->tahun					= $this->_getParam('tahun');
				if ($provInquiry == 'N') {
					$this->view->amount				= $this->_getParam('amount');
				}

				if ($param['radioCheck'] == 1) {
					if ($provCode == 'PKB') {
						$this->view->policeNumber		= $this->_getParam('orderId');
					} else {
						$this->view->orderId			= $this->_getParam('orderId');
					}
				} else if ($param['radioCheck'] == 2) {
					$this->view->payment			= $param['payment'];
				}

				if (count($msg) < 1) {
					$this->view->ERROR_MSG_N 		= 'Error : Invalid Token';
				}
			}
		} else {
			unset($_SESSION['newtax']);
		}
		Application_Helper_General::writeLog('MTAX', 'Tax Payment');

		///added cms purchase payment get repair - begin
		if (!empty($PS_NUMBER) && !$this->_getParam('operator')) {
			$this->view->ACCTSRC		= $sourceAcct;
			$this->view->operator		= $prevoperator;

			if ($typeoforderid == 1) {
				$this->view->radioCheck = 1;
				$this->view->orderId = $prevorderid;
			} else {
				$this->view->radioCheck = 2;
				$this->view->payment = $prevorderid;
			}
		}


		if ($this->_request->isPost() && $this->_getParam('next')) {
			// $params 					= $this->_request->getParams();
			// echo "<pre>";
			// var_dump($params);die;
			$filters = array(
				'ACCTSRC'         => array('StripTags', 'StringTrim'),
				'paymentSubject'        => array('StripTags', 'StringTrim'),
				'radioCheck'        => array('StripTags', 'StringTrim'),
				'idNumber'        => array('StripTags', 'StringTrim'),
			);


			if ($this->_getParam('radioCheck') == 1) {
				$validators =  array(
					'ACCTSRC'        => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
					'paymentSubject'        => array(
						'allowEmpty' => true,
						'messages' => array()
					),
					'radioCheck'        => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
				);
				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

				if ($zf_filter_input->isValid()) {
					$sessionNamespace->billingIdType = 1;
					$paramSession['paymentSubject'] = $this->_getParam('paymentSubject');
					$paramSession['ACCTSRC']			= $this->_getParam('ACCTSRC');
					$sessionNamespace->paramSession = $paramSession;
					$this->_redirect('/payment/newtax/next');
				} else {
					$this->view->ACCTSRC   = ($zf_filter_input->isValid('ACCTSRC')) ? $zf_filter_input->ACCTSRC : $this->_getParam('ACCTSRC');
					$this->view->radioCheck   = ($zf_filter_input->isValid('radioCheck')) ? $zf_filter_input->radioCheck : $this->_getParam('radioCheck');
					$error = $zf_filter_input->getMessages();

					//format error utk ditampilkan di view html 
					$errorArray = null;
					foreach ($error as $keyRoot => $rowError) {
						foreach ($rowError as $errorString) {
							$errorArray[$keyRoot] = $errorString;
						}
					}
					foreach ($error as $keyRoot => $rowError) {
						foreach ($rowError as $errorString) {
							$keyname = $keyRoot . "Err";
							// print_r($keyname);die;
							$this->view->$keyname = $this->language->_($errorString);
						}
					}

					$this->view->customer_msg  = $errorArray;
				}
			} else {
				$validators =  array(
					'ACCTSRC'        => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
					'idNumber'        => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
					'paymentSubject'        => array(
						'allowEmpty' => true,
						'messages' => array()
					),
				);
				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

				if ($zf_filter_input->isValid()) {
					$sessionNamespace->billingIdType = 2;
					$paramSession['idnum'] = $this->_getParam('idNumber');
					$paramSession['paymentSubject'] = $this->_getParam('paymentSubject');
					$paramSession['ACCTSRC']			= $this->_getParam('ACCTSRC');
					$sessionNamespace->paramSession = $paramSession;
					$this->_redirect('/payment/tax/nexttax');
				} else {
					$this->view->ACCTSRC   = ($zf_filter_input->isValid('ACCTSRC')) ? $zf_filter_input->ACCTSRC : $this->_getParam('ACCTSRC');
					$this->view->idNumber   = ($zf_filter_input->isValid('idNumber')) ? $zf_filter_input->idNumber : $this->_getParam('idNumber');
					$this->view->radioCheck   = ($zf_filter_input->isValid('radioCheck')) ? $zf_filter_input->radioCheck : $this->_getParam('radioCheck');
					$error = $zf_filter_input->getMessages();

					//format error utk ditampilkan di view html 
					$errorArray = null;
					foreach ($error as $keyRoot => $rowError) {
						foreach ($rowError as $errorString) {
							$errorArray[$keyRoot] = $errorString;
						}
					}
					foreach ($error as $keyRoot => $rowError) {
						foreach ($rowError as $errorString) {
							$keyname = $keyRoot . "Err";
							// print_r($keyname);die;
							$this->view->$keyname = $this->language->_($errorString);
						}
					}

					$this->view->customer_msg  = $errorArray;
				}
			}
		}

		//added cms purchase payment get repair - end
	}

	public function nextAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->userId	= $this->_userIdLogin;

		$SinglePayment 		= new SinglePayment(null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('newtax');
		$paramSession 		= $sessionNamespace->paramSession;

		$paramac = array(
			'CCY_IN' => 'IDR'
		);
		$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 // ->where('A.ACCT_STATUS = ?','5')
						 ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->order('A.APIKEY_ID ASC')
						 // echo $acctlist;
				);
		$masterData = $this->_db->fetchAll(
					$this->_db->select()
					  ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
					  ->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
					  ->where('A.CUST_ID = ?', $this->_custIdLogin)
					  ->where('A.USER_LOGIN = ?', $this->_userIdLogin)
					  ->where('A.MAXLIMIT > 0')
					  ->where('A.MAXLIMIT_OPEN = 1')
				  );

				  $account = array();
				  foreach ($acctlist as $key => $value) {
					  
					  if($value['FIELD'] == 'account_number'){
						  foreach ($masterData as $newkey => $valuemaster) {
							if($value['VALUE'] == $valuemaster['ACCT_NO']){
							  $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
							  $account[$value['ID']]['MAXLIMIT'] = $valuemaster['MAXLIMIT'];
							  $account[$value['ID']]['APIKEY_ID'] = $value['APIKEY_ID'];
							  $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
							  $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
							  $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
							  $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
							  $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
							  $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
							}
						  }
					  }
				  }
		 
				  $i = 0;
				  foreach ($account as $key => $value) {
					  
					  foreach ($acctlist as $kl => $vl) {
						   
						  if($vl['APIKEY_ID'] == $value['APIKEY_ID']){
							  
								$acct[$i][$vl['FIELD']] = $vl['VALUE'];
						  }
					  }
		  
					  $acct[$i]['ACCT_NO'] = $value['account_number'];
					  $acct[$i]['ACCT_ALIAS'] = $value['account_alias'];
					  $acct[$i]['ACCT_CCY'] = $value['account_currency'];
					  $acct[$i]['MAXLIMIT'] = $value['MAXLIMIT'];
					  $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
					  $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
					  $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
					  $acct[$i]['ACCT_NAME'] = $value['account_name'];
		  
					  $acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
					  $acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
					  $acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
					  $acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
					  $i++;
				  }
		
		
		$AccArr = $acct;
		if(!empty($AccArr)){
			$no = 0;
			foreach($AccArr as $val){
				$dataacct[$no]['ACCT_NO'] = $val['account_number'];
				$dataacct[$no]['BANK_NAME'] = $val['BANK_NAME'];
				$dataacct[$no]['BANK_CODE'] = $val['BANK_CODE'];
				$dataacct[$no]['CCY_ID'] = $val['account_currency'];
				$dataacct[$no]['ACCT_NAME'] = $val['account_name'];
				$dataacct[$no]['ACCT_ALIAS_NAME'] = $val['account_alias'];
				$dataacct[$no]['ACCT_TYPE'] = '';
				$dataacct[$no]['FREEZE_STATUS'] = '0';
				$dataacct[$no]['MAXLIMIT'] = $val['MAXLIMIT'];
				$dataacct[$no]['DESC'] = '0';
				$dataacct[$no]['EMAIL'] = '0';
				$no++;
			}
		}
		//}
		//echo '<pre>';
		//var_dump($dataacct);
		//$this->view->AccArr 		= $AccArr;
		
		$AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
		if(!empty($AccArr)){
			foreach($AccArr as $ky => $val){
				$conf = Zend_Registry::get('config');
				$AccArr[$ky]['BANK_NAME'] = $conf['app']['bankname'];
				$AccArr[$ky]['BANK_CODE'] = ''; 
			}
		}
		$resultsrc = array_merge($AccArr, $dataacct);
		$this->view->AccArr 		= $resultsrc;
		
		//echo '<pre>';
		//var_dump($resultsrc);die;
		
		//if back form confirm page
		if ($paramSession['CONFIRM_PAGE']) {
			// var_dump($paramSession);die;
			$payerNpwp = (explode(' - ', $paramSession['payerNpwp']));
			$this->view->payerNpwp1   = $payerNpwp[0];
			$this->view->payerNpwp2   = $payerNpwp[1];
			$this->view->payerNpwp3   = $payerNpwp[2];
			$this->view->payerNpwp4   = $payerNpwp[3];
			$this->view->payerNpwp5   = $payerNpwp[4];
			$this->view->payerNpwp6   = $payerNpwp[5];

			$asessableNpwp = (explode(' - ', $paramSession['asessableNpwp']));
			$this->view->asessableNpwp1   = $asessableNpwp[0];
			$this->view->asessableNpwp2   = $asessableNpwp[1];
			$this->view->asessableNpwp3   = $asessableNpwp[2];
			$this->view->asessableNpwp4   = $asessableNpwp[3];
			$this->view->asessableNpwp5   = $asessableNpwp[4];
			$this->view->asessableNpwp6   = $asessableNpwp[5];

			$this->view->ACCTSRC   = $paramSession['ACCTSRC'];
			$this->view->paymentSubject   = $paramSession['paymentSubject'];

			$this->view->asessablename   = $paramSession['asessablename'];
			$this->view->asessableaddress   = $paramSession['asessableaddress'];
			$this->view->asessablecity   = $paramSession['asessablecity'];
			$this->view->asessableidentity1   = $paramSession['asessableidentity1'];
			$this->view->asessableidentity2   = $paramSession['asessableidentity2'];

			$this->view->payername   = $paramSession['payername'];
			$this->view->chargetype   = $paramSession['chargetype'];
			$this->view->chargeid   = $paramSession['chargeid'];
			$this->view->taxtype   = $paramSession['taxtype'];
			$this->view->akuncode   = $paramSession['akuncode'];
			$this->view->deposittype   = $paramSession['deposittype'];
			$this->view->remark   = $paramSession['remark'];

			$taxobjectnumber   = preg_split("/[.-]/", $paramSession['taxobjectnumber']);
			$this->view->taxobjectnumber1   = trim($taxobjectnumber[0]);
			$this->view->taxobjectnumber2   = trim($taxobjectnumber[1]);
			$this->view->taxobjectnumber3   = trim($taxobjectnumber[2]);
			$this->view->taxobjectnumber4   = trim($taxobjectnumber[3]);
			$this->view->taxobjectnumber5   = trim($taxobjectnumber[4]);
			$this->view->taxobjectnumber6   = trim($taxobjectnumber[5]);
			$this->view->taxobjectnumber7   = trim($taxobjectnumber[6]);

			$this->view->amount   = $paramSession['amount'];

			$skNumber = (explode(' / ', $paramSession['skNumber']));
			$this->view->skNumber1   = $skNumber[0];
			$this->view->skNumber2   = $skNumber[1];
			$this->view->skNumber3   = $skNumber[2];
			$this->view->skNumber4   = $skNumber[3];
			$this->view->skNumber5   = $skNumber[4];


			$this->view->periodic   = $paramSession['periodic'];

			$this->view->NOP_isMandatory   = $paramSession['NOP_isMandatory'];
			$this->view->SK_isMandatory   = $paramSession['SK_isMandatory'];

			$this->view->month1   = $paramSession['month1'];
			$this->view->month2   = $paramSession['month2'];
			$param = $paramSession;
			$param['CONFIRM_PAGE'] = false;
			$sessionNamespace->paramSession	= $param;
		}
		// $sessionNamespace 	= new Zend_Session_Namespace('newtax');
		// $billingIdType 		= $sessionNamespace->billingIdType;

		// $this->view->billingIdType = $billingIdType;

		//added repair purchase payment - begin
		$PS_NUMBER 			= $paramSession["PS_NUMBER"];
		// 		print_r($paramSession);die;
		$this->view->paramSession	= $paramSession;
		$PS_EFDATE		= date('Y-m-d');
		$date			= $this->model->date();
		// 		print_r($paramSession);die;
		// 		operator
		$this->model 					= new payment_Model_Payment();

		$map_code = array();
		$select = $this->_db->select()
			->from(array('M_MAP_CODE'), array('MAP_CODE', 'MAP_NAME'))
			->query()->fetchAll();

		foreach ($select as $key) {
			$map_code[$key['MAP_CODE']] = $key['MAP_NAME'];
		}
		$this->view->map_code = $map_code;

		// $data = $this->model->getProviderName($paramSession['operator'],1);
		// 		print_r($data);die;
		// $paramSession['operatorNama'] = $data['PROVIDER_NAME'];
		// die('here');
		$this->view->token = false;
		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);

			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}
		//templatedetail - begin
		$htmldataDetailDetail = '';
		foreach ($paramSession['dataUi'] as $datadetail => $key) {
			//Zend_Debug::dump($key);

			if ($datadetail == 'total_amount') {
			} else {

				$htmldataDetailDetail .= '
			<tr>
				<td class="tbl-evencontent">' . $this->language->_($datadetail) . '</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">' . $key . '</td>
			</tr>
			';
			}
		}

		if (isset($htmldataDetailDetail)) $this->view->templateDetail = $htmldataDetailDetail;
		//templatedetail - end

		//select m_user - begin
		$userData = $this->CustomerUser->getUser($this->_userIdLogin);

		//added token type
		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];
		$tokenIdUser 				= $userData['TOKEN_ID'];
		$tokenType 					= $userData['TOKEN_TYPE'];
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);

		$paramSession['TOKEN_ID'] 	= $tokenIdUser;
		$paramSession['PS_EFDATE'] 	= $PS_EFDATE;
		$paramSession['date'] 		= $date;

		$paramSession['dateDisplayFormat'] 	= $this->_dateDisplayFormat;
		$paramSession['dateDBFormat'] 		= $this->_dateDBFormat;

		//ADDED PRIV
		$paramSession['priv1'] 	= $this->view->hasPrivilege('BADA');
		$paramSession['priv2'] 	= $this->view->hasPrivilege('BLBU');
		$paramSession['priv3']	= "MTAX";
		$paramSession['confirmPage']	= 1; //harus ada kalau tidak $this->_isConfirm false & cek benef

		//added cms purchase payment get repair - begin
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER)) ? $PS_NUMBER : '';

		if (!empty($PS_NUMBER)) {
			$paramSession['isRepair'] = true;
		} else {
			$paramSession['isRepair'] = false;
		}
		//added cms purchase payment get repair - end


		if ($this->_getParam('submit1') == TRUE) {

			$inputtoken1 		= $this->_getParam('inputtoken1');
			$inputtoken2 		= $this->_getParam('inputtoken2');
			$inputtoken3 		= $this->_getParam('inputtoken3');
			$inputtoken4 		= $this->_getParam('inputtoken4');
			$inputtoken5 		= $this->_getParam('inputtoken5');
			$inputtoken6 		= $this->_getParam('inputtoken6');

			$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;
			$filters = array(
				'payerNpwp1'           => array('StripTags', 'StringTrim', 'StringToUpper'),
				'payerNpwp2'         => array('StripTags', 'StringTrim'),
				'payerNpwp3'        => array('StripTags', 'StringTrim'),
				'payerNpwp4'        => array('StripTags', 'StringTrim'),
				'payerNpwp5'        => array('StripTags', 'StringTrim'),
				'payerNpwp6'        => array('StripTags', 'StringTrim'),

				'asessableNpwp1'        => array('StripTags', 'StringTrim'),
				'asessableNpwp2'        => array('StripTags', 'StringTrim'),
				'asessableNpwp3'        => array('StripTags', 'StringTrim'),
				'asessableNpwp4'        => array('StripTags', 'StringTrim'),
				'asessableNpwp5'        => array('StripTags', 'StringTrim'),
				'asessableNpwp6'        => array('StripTags', 'StringTrim'),

				'payername'        => array('StripTags', 'StringTrim'),
				'chargetype'        => array('StripTags', 'StringTrim'),
				'chargeid'        => array('StripTags', 'StringTrim'),
				'taxtype'        => array('StripTags', 'StringTrim'),

				'akuncode'        => array('StripTags', 'StringTrim'),
				'deposittype'        => array('StripTags', 'StringTrim'),

				'NOP_isMandatory'        => array('StripTags', 'StringTrim'),
				'SK_isMandatory'        => array('StripTags', 'StringTrim'),

				'ACCTSRC'         => array('StripTags', 'StringTrim'),
				'paymentSubject'        => array('StripTags', 'StringTrim'),
			);

			$validators =  array(
				'ACCTSRC'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),

				'payerNpwp1'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'payerNpwp2'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'payerNpwp3'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'payerNpwp4'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'payerNpwp5'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'payerNpwp6'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),

				'asessableNpwp1'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'asessableNpwp2'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'asessableNpwp3'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'asessableNpwp4'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'asessableNpwp5'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'asessableNpwp6'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),

				'payername'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'chargetype'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),

				'taxtype'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),

				'akuncode'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'deposittype'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'amount'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
			);

			$NOP_isMandatory 		= $this->_getParam('NOP_isMandatory');
			$SK_isMandatory 		= $this->_getParam('SK_isMandatory');
			$chargetype 		= $this->_getParam('chargetype');
			$taxtype 		= $this->_getParam('taxtype');

			if ($taxtype == "1") {
				$taxtype_validator = array(
					'asessablename' => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'asessableaddress' => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'asessablecity' => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'asessableidentity1' => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'asessableidentity2' => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				);
				$validators = array_merge($validators, $taxtype_validator);
			}

			if ($chargetype == "1") {
				$chargetype_validator = array('chargeid' => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))));
				$validators = array_merge($validators, $chargetype_validator);
			}

			if ($NOP_isMandatory == "1" || $NOP_isMandatory == "2") {
				$nop_validator =  array(
					'taxobjectnumber1'  => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'taxobjectnumber2'  => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'taxobjectnumber3'  => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'taxobjectnumber4'  => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'taxobjectnumber5'  => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'taxobjectnumber6'  => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'taxobjectnumber7'  => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),

				);
				$validators = array_merge($validators, $nop_validator);
			}

			if ($SK_isMandatory == "1") {
				$sk_validator =  array(
					'skNumber1'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'skNumber2'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'skNumber3'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'skNumber4'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'skNumber5'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')))
				);
				$validators = array_merge($validators, $sk_validator);
			}

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

			//Validate max month selected
			$validateMonth = true;
			$clickedSearch = true;
			$dateNow = date("Y-m");

			$selected_year = $this->_getParam('periodic');
			$selected_month1 = 1 + (int) $this->_getParam('month1');
			$selected_month2 = 1 + (int) $this->_getParam('month2');
			$month1 = date("Y-m", strtotime($selected_year . '-' . $selected_month1));
			$month2 = date("Y-m", strtotime($selected_year . '-' . $selected_month2));
			if ($month1 > $dateNow || $month2 > $dateNow) {
				$validateMonth = false;
			}

			if ($this->_getParam('clickedsearch') != '1' && $this->_getParam('taxtype') != '1') {
				$clickedSearch = false;
			}

			//check makerlimit, dailylimit
			if ($this->_getParam('amount') !== '' && $this->_getParam('ACCTSRC') !== '') {
				$amount = preg_replace("/([^0-9\\.])/i", "", $this->_getParam('amount'));
				$createLimit 	= $this->validateLimit($amount, 'IDR', $this->_getParam('ACCTSRC'), $msgg);
			}

			//check privilege relase
			$this->view->error = false;
			if ($this->_custSameUser) {
				if (!$this->view->hasPrivilege('PRLP')) {
					// die('here');
					$errMessage = $this->language->_("Error: You don't have privilege to release payment");
					$this->view->error = true;
					$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
				}
			}

			if ($zf_filter_input->isValid() && $validateMonth && $createLimit === true && $clickedSearch && $this->view->error==false) {

				// echo '<pre>';
				// var_dump($this->_request->getParams());die;
				// added by rocki
				$param['paymentSubject'] = $this->_getParam('paymentSubject');
				$param['ACCTSRC']			= $this->_getParam('ACCTSRC');
				$payerNpwp1 = $this->_getParam('payerNpwp1');
				$payerNpwp2 = $this->_getParam('payerNpwp2');
				$payerNpwp3 = $this->_getParam('payerNpwp3');
				$payerNpwp4 = $this->_getParam('payerNpwp4');
				$payerNpwp5 = $this->_getParam('payerNpwp5');
				$payerNpwp6 = $this->_getParam('payerNpwp6');
				$param['payerNpwp']		= $payerNpwp1 . ' - ' . $payerNpwp2 . ' - ' . $payerNpwp3 . ' - ' . $payerNpwp4 . ' - ' . $payerNpwp5 . ' - ' . $payerNpwp6;
				$param['payername'] = $this->_getParam('payername');

				$asessableNpwp1 = $this->_getParam('asessableNpwp1');
				$asessableNpwp2 = $this->_getParam('asessableNpwp2');
				$asessableNpwp3 = $this->_getParam('asessableNpwp3');
				$asessableNpwp4 = $this->_getParam('asessableNpwp4');
				$asessableNpwp5 = $this->_getParam('asessableNpwp5');
				$asessableNpwp6 = $this->_getParam('asessableNpwp6');

				$param['asessableNpwp']		= $asessableNpwp1 . ' - ' . $asessableNpwp2 . ' - ' . $asessableNpwp3 . ' - ' . $asessableNpwp4 . ' - ' . $asessableNpwp5 . ' - ' . $asessableNpwp6;
				$param['asessablename'] = $this->_getParam('asessablename');
				$param['asessableaddress'] = $this->_getParam('asessableaddress');
				$param['asessablecity'] = $this->_getParam('asessablecity');
				$param['asessableidentity1'] = $this->_getParam('asessableidentity1');
				$param['asessableidentity2'] = $this->_getParam('asessableidentity2');


				$param['chargetype'] = $this->_getParam('chargetype');
				$param['chargeid'] = $this->_getParam('chargeid');
				$param['taxtype'] = $this->_getParam('taxtype');
				$param['akuncode'] = $this->_getParam('akuncode');
				$param['deposittype'] = $this->_getParam('deposittype');
				$param['message'] = $this->_getParam('TRA_MESSAGE');
				$param['NOP_isMandatory'] = $this->_getParam('NOP_isMandatory');
				$param['SK_isMandatory'] = $this->_getParam('SK_isMandatory');

				$param['tranferdatetype'] = $this->_getParam('tranferdatetype');
				if ($this->_getParam('tranferdatetype') == "1") {
					$param['date'] = $this->_getParam('immediateTransfer');
				} else {
					$param['date'] = $this->_getParam('PS_FUTUREDATE');
				}

				$param['notif'] = $this->_getParam('notif');
				if ($this->_getParam('notif') == "2") {
					$param['email'] = $this->_getParam('email');
					$param['sms'] = $this->_getParam('sms');
				}

				$param['amount'] = $this->_getParam('amount');

				$taxobjectnumber1 = $this->_getParam('taxobjectnumber1');
				$taxobjectnumber2 = $this->_getParam('taxobjectnumber2');
				$taxobjectnumber3 = $this->_getParam('taxobjectnumber3');
				$taxobjectnumber4 = $this->_getParam('taxobjectnumber4');
				$taxobjectnumber5 = $this->_getParam('taxobjectnumber5');
				$taxobjectnumber6 = $this->_getParam('taxobjectnumber6');
				$taxobjectnumber7 = $this->_getParam('taxobjectnumber7');

				$param['taxobjectnumber'] = $taxobjectnumber1 . ' . ' . $taxobjectnumber2 . ' . ' . $taxobjectnumber3 . ' . ' . $taxobjectnumber4 . ' . ' . $taxobjectnumber5 . ' - ' . $taxobjectnumber6 . ' . ' . $taxobjectnumber7;

				$skNumber1 = $this->_getParam('skNumber1');
				$skNumber2 = $this->_getParam('skNumber2');
				$skNumber3 = $this->_getParam('skNumber3');
				$skNumber4 = $this->_getParam('skNumber4');
				$skNumber5 = $this->_getParam('skNumber5');

				$param['skNumber']		= $skNumber1 . ' / ' . $skNumber2 . ' / ' . $skNumber3 . ' / ' . $skNumber4 . ' / ' . $skNumber5;

				$param['remark'] = $this->_getParam('remark');

				$param['month1'] = $this->_getParam('month1');
				$param['month2'] = $this->_getParam('month2');
				$param['periodic'] = $this->_getParam('periodic');
				$param['SERVICE_TYPE']	= '6';
				$param['operator']	= '1158';
				$param['provInquiry']	= 'N';
				$param['ccy']	= 'IDR';
				$param['CUST_ID']	= $this->_custIdLogin;
				$param['USER_ID']	= $this->_userIdLogin;
				$param['PS_TYPE']	= '17';
				$param['PS_CATEGORY'] = 'PAYMENT';
				$param['operatorNama'] = 'MPN';
				$param['dateDisplayFormat'] 	= $this->_dateDisplayFormat;
				$param['dateDBFormat'] 		= $this->_dateDBFormat;

				$currentDate = date("Ymd");
				$randomdigit = str_pad(mt_rand(0, 99999), 8, '0', STR_PAD_LEFT);
				// $rand = rand(10,100);
				$param['orderId'] = $currentDate . $randomdigit;
				// echo "<pre>";
				// var_dump($this->_request->getParams());die;

				$sgo_product_code_data	= $this->model->getSgoProductCode($param['operator']);
				$sgo_product_code 		= $sgo_product_code_data['0']['SGO_PRODUCT_CODE'];

				$param['sgoProductCode'] 	= $sgo_product_code; //STCKAI

				$param['DETAIL'] = array(
					'0' => array(
						'PS_FIELDNAME' 	=> 'asessablename',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('asessablename')
					),
					'1' => array(
						'PS_FIELDNAME' 	=> 'asessableaddress',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('asessableaddress')
					),
					'2' => array(
						'PS_FIELDNAME' 	=> "asessablecity",
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('asessablecity')
					),
					'3' => array(
						'PS_FIELDNAME' 	=> 'taxobjectnumber',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('taxobjectnumber1') . $this->_getParam('taxobjectnumber2') . $this->_getParam('taxobjectnumber3') . $this->_getParam('taxobjectnumber4') . $this->_getParam('taxobjectnumber5') . $this->_getParam('taxobjectnumber6') . $this->_getParam('taxobjectnumber7')
					),
					'4' => array(
						'PS_FIELDNAME' 	=> 'skNumber',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('skNumber1') . $this->_getParam('skNumber2') . $this->_getParam('skNumber3') . $this->_getParam('skNumber4') . $this->_getParam('skNumber5')
					),
					'5' => array(
						'PS_FIELDNAME' 	=> 'NOP_isMandatory',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('NOP_isMandatory')
					),
					'6' => array(
						'PS_FIELDNAME' 	=> 'SK_isMandatory',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('SK_isMandatory')
					),
					'7' => array(
						'PS_FIELDNAME' 	=> 'remark',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('remark')
					),
					'8' => array(
						'PS_FIELDNAME' 	=> 'deposittype',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('deposittype')
					),
					'9' => array(
						'PS_FIELDNAME' 	=> 'payerNpwp',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('payerNpwp1') . $this->_getParam('payerNpwp2') . $this->_getParam('payerNpwp3') . $this->_getParam('payerNpwp4') . $this->_getParam('payerNpwp5') . $this->_getParam('payerNpwp6')
					),
					'10' => array(
						'PS_FIELDNAME' 	=> 'payername',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('payername')
					),
					'11' => array(
						'PS_FIELDNAME' 	=> 'chargetype',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('chargetype')
					),
					'12' => array(
						'PS_FIELDNAME' 	=> 'chargeid',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('chargeid')
					),
					'13' => array(
						'PS_FIELDNAME' 	=> 'taxtype',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('taxtype')
					),
					'14' => array(
						'PS_FIELDNAME' 	=> 'month1',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('month1')
					),
					'15' => array(
						'PS_FIELDNAME' 	=> 'month2',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('month2')
					),
					// '5' => array(
					// 						'PS_FIELDNAME' 	=> 'Total Payment',
					// 						'PS_FIELDTYPE'	=> '3',
					// 						'PS_FIELDVALUE'	=> $this->_getParam('periodic')
					// 					),
					'16' => array(
						'PS_FIELDNAME' 	=> 'amount',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('amount')
					),
					'17' => array(
						'PS_FIELDNAME' 	=> 'asessableidentity1',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('asessableidentity1')
					),
					'18' => array(
						'PS_FIELDNAME' 	=> 'asessableidentity2',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('asessableidentity2')
					),
					'19' => array(
						'PS_FIELDNAME' 	=> 'akuncode',
						'PS_FIELDTYPE'	=> '3',
						'PS_FIELDVALUE'	=> $this->_getParam('akuncode')
					),

					'20' => array(
						'PS_FIELDNAME' 	=> 'Type Of Transaction',
						'PS_FIELDTYPE'	=> '1',
						'PS_FIELDVALUE'	=> 'Tax Payment'
					),
				);
				// $param['ACCT_NAME'] = '';

				$sessionNamespace->paramSession 	= $param;
				$this->_redirect('/payment/newtax/confirm');
				
				
			} else {
				$this->view->ACCTSRC   = ($zf_filter_input->isValid('ACCTSRC')) ? $zf_filter_input->ACCTSRC : $this->_getParam('ACCTSRC');
				$this->view->paymentSubject   = ($zf_filter_input->isValid('paymentSubject')) ? $zf_filter_input->paymentSubject : $this->_getParam('paymentSubject');

				$this->view->payerNpwp1   = ($zf_filter_input->isValid('payerNpwp1')) ? $zf_filter_input->payerNpwp1 : $this->_getParam('payerNpwp1');
				$this->view->payerNpwp2  = ($zf_filter_input->isValid('payerNpwp2')) ? $zf_filter_input->payerNpwp2 : $this->_getParam('payerNpwp2');
				$this->view->payerNpwp3   = ($zf_filter_input->isValid('payerNpwp3')) ? $zf_filter_input->payerNpwp3 : $this->_getParam('payerNpwp3');
				$this->view->payerNpwp4   = ($zf_filter_input->isValid('payerNpwp4')) ? $zf_filter_input->payerNpwp4 : $this->_getParam('payerNpwp4');
				$this->view->payerNpwp5   = ($zf_filter_input->isValid('payerNpwp5')) ? $zf_filter_input->payerNpwp5 : $this->_getParam('payerNpwp5');
				$this->view->payerNpwp6   = ($zf_filter_input->isValid('payerNpwp6')) ? $zf_filter_input->payerNpwp6 : $this->_getParam('payerNpwp6');

				$this->view->asessableNpwp1   = ($zf_filter_input->isValid('asessableNpwp1')) ? $zf_filter_input->asessableNpwp1 : $this->_getParam('asessableNpwp1');
				$this->view->asessableNpwp2   = ($zf_filter_input->isValid('asessableNpwp2')) ? $zf_filter_input->asessableNpwp2 : $this->_getParam('asessableNpwp2');
				$this->view->asessableNpwp3   = ($zf_filter_input->isValid('asessableNpwp3')) ? $zf_filter_input->asessableNpwp3 : $this->_getParam('asessableNpwp3');
				$this->view->asessableNpwp4   = ($zf_filter_input->isValid('asessableNpwp4')) ? $zf_filter_input->asessableNpwp4 : $this->_getParam('asessableNpwp4');
				$this->view->asessableNpwp5   = ($zf_filter_input->isValid('asessableNpwp5')) ? $zf_filter_input->asessableNpwp5 : $this->_getParam('asessableNpwp5');
				$this->view->asessableNpwp6   = ($zf_filter_input->isValid('asessableNpwp6')) ? $zf_filter_input->asessableNpwp6 : $this->_getParam('asessableNpwp6');

				$this->view->asessablename   = ($zf_filter_input->isValid('asessablename')) ? $zf_filter_input->asessablename : $this->_getParam('asessablename');
				$this->view->asessableaddress   = ($zf_filter_input->isValid('asessableaddress')) ? $zf_filter_input->asessableaddress : $this->_getParam('asessableaddress');
				$this->view->asessablecity   = ($zf_filter_input->isValid('asessablecity')) ? $zf_filter_input->asessablecity : $this->_getParam('asessablecity');
				$this->view->asessableidentity1   = ($zf_filter_input->isValid('asessableidentity1')) ? $zf_filter_input->asessableidentity1 : $this->_getParam('asessableidentity1');
				$this->view->asessableidentity2   = ($zf_filter_input->isValid('asessableidentity2')) ? $zf_filter_input->asessableidentity2 : $this->_getParam('asessableidentity2');

				$this->view->payername   = ($zf_filter_input->isValid('payername')) ? $zf_filter_input->payername : $this->_getParam('payername');
				$this->view->chargetype   = ($zf_filter_input->isValid('chargetype')) ? $zf_filter_input->chargetype : $this->_getParam('chargetype');
				$this->view->chargeid   = ($zf_filter_input->isValid('chargeid')) ? $zf_filter_input->chargeid : $this->_getParam('chargeid');
				$this->view->taxtype   = ($zf_filter_input->isValid('taxtype')) ? $zf_filter_input->taxtype : $this->_getParam('taxtype');
				$this->view->akuncode   = ($zf_filter_input->isValid('akuncode')) ? $zf_filter_input->akuncode : $this->_getParam('akuncode');
				$this->view->deposittype   = ($zf_filter_input->isValid('deposittype')) ? $zf_filter_input->deposittype : $this->_getParam('deposittype');

				$this->view->remark   = ($zf_filter_input->isValid('remark')) ? $zf_filter_input->remark : $this->_getParam('remark');

				$this->view->taxobjectnumber1   = ($zf_filter_input->isValid('taxobjectnumber1')) ? $zf_filter_input->taxobjectnumber1 : $this->_getParam('taxobjectnumber1');
				$this->view->taxobjectnumber2   = ($zf_filter_input->isValid('taxobjectnumber2')) ? $zf_filter_input->taxobjectnumber2 : $this->_getParam('taxobjectnumber2');
				$this->view->taxobjectnumber3   = ($zf_filter_input->isValid('taxobjectnumber3')) ? $zf_filter_input->taxobjectnumber3 : $this->_getParam('taxobjectnumber3');
				$this->view->taxobjectnumber4   = ($zf_filter_input->isValid('taxobjectnumber4')) ? $zf_filter_input->taxobjectnumber4 : $this->_getParam('taxobjectnumber4');
				$this->view->taxobjectnumber5   = ($zf_filter_input->isValid('taxobjectnumber5')) ? $zf_filter_input->taxobjectnumber5 : $this->_getParam('taxobjectnumber5');
				$this->view->taxobjectnumber6   = ($zf_filter_input->isValid('taxobjectnumber6')) ? $zf_filter_input->taxobjectnumber6 : $this->_getParam('taxobjectnumber6');
				$this->view->taxobjectnumber7   = ($zf_filter_input->isValid('taxobjectnumber7')) ? $zf_filter_input->taxobjectnumber7 : $this->_getParam('taxobjectnumber7');


				$this->view->amount   = ($zf_filter_input->isValid('amount')) ? $zf_filter_input->amount : $this->_getParam('amount');

				$this->view->skNumber1   = ($zf_filter_input->isValid('skNumber1')) ? $zf_filter_input->skNumber1 : $this->_getParam('skNumber1');
				$this->view->skNumber2   = ($zf_filter_input->isValid('skNumber2')) ? $zf_filter_input->skNumber2 : $this->_getParam('skNumber2');
				$this->view->skNumber3   = ($zf_filter_input->isValid('skNumber3')) ? $zf_filter_input->skNumber3 : $this->_getParam('skNumber3');
				$this->view->skNumber4   = ($zf_filter_input->isValid('skNumber4')) ? $zf_filter_input->skNumber4 : $this->_getParam('skNumber4');
				$this->view->skNumber5   = ($zf_filter_input->isValid('skNumber5')) ? $zf_filter_input->skNumber5 : $this->_getParam('skNumber5');


				$this->view->periodic   = ($zf_filter_input->isValid('periodic')) ? $zf_filter_input->periodic : $this->_getParam('periodic');


				$this->view->NOP_isMandatory   = ($zf_filter_input->isValid('NOP_isMandatory')) ? $zf_filter_input->NOP_isMandatory : $this->_getParam('NOP_isMandatory');
				$this->view->SK_isMandatory   = ($zf_filter_input->isValid('SK_isMandatory')) ? $zf_filter_input->SK_isMandatory : $this->_getParam('SK_isMandatory');
				$this->view->TRA_MESSAGE   = $this->_getParam('TRA_MESSAGE');
				if (!$validateMonth) {
					$this->view->error_maxMonth = 'Can not greater than current month';
					$this->view->month1   = $this->_getParam('month1');
					$this->view->month2   = $this->_getParam('month2');
				} else {
					$this->view->month1   = ($zf_filter_input->isValid('month1')) ? $zf_filter_input->month1 : $this->_getParam('month1');
					$this->view->month2   = ($zf_filter_input->isValid('month2')) ? $zf_filter_input->month2 : $this->_getParam('month2');
				}

				if (!$clickedSearch) {
					$this->view->error_clickedsearch = 'Please click the search button';
				}
				$this->view->clickedsearch   = ($zf_filter_input->isValid('clickedsearch')) ? $zf_filter_input->clickedsearch : $this->_getParam('clickedsearch');
				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html 
				$errorArray = null;
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$errorArray[$keyRoot] = $errorString;
					}
				}
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$keyname = "error_" . $keyRoot;
						// print_r($keyname);die;
						$this->view->$keyname = $this->language->_($errorString);
					}
				}

				if ($createLimit === true) {
				} else {
					if ($createLimit !== null) {
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $createLimit;
					}
				}
			}
		} else if ($this->_getParam('submit') == true) {
			$this->_redirect('/payment/newtax/index');
		}
		// die('here');
		Application_Helper_General::writeLog('MTAX', 'Tax Payment');
	}

	public function confirmAction()
	{
		$pdf = $this->_getParam('pdf');
		$sessionNamespace 					= new Zend_Session_Namespace('newtax');
		$paramSession 						= $sessionNamespace->paramSession;
		$this->view->ERROR_MSG_N			= $paramSession['paymentMessage'];
		$PS_NUMBER							= $paramSession['PS_NUMBER'];

		$this->view->token = false;
		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];
			// var_dump($tokenIdUser);
			// var_dump($tokendata);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			// var_dump($tokendata);
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		if ($this->_request->isPost()) {
			// echo "<pre>";
			// var_dump($paramSession);die;
			// $sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
			// $resProvider	 = $sendProvider->createPaymentPurchase($paramSession, $msg);
			// $paramSession['TRA_AMOUNT'] = Application_Helper_General::convertDisplayMoney($paramSession['TRA_AMOUNT']);

			if ($this->_getParam('submit1') == TRUE) {
				//back button
				$param = $paramSession;
				$param['CONFIRM_PAGE'] = true;
				$sessionNamespace->paramSession 	= $param;
				$this->_redirect('/payment/newtax/next');
			}

			if ($this->_custSameUser) {
				// var_dump($tokenGoogle);
				// die('here');

				$inputtoken1 		= $this->_getParam('inputtoken1');
				$inputtoken2 		= $this->_getParam('inputtoken2');
				$inputtoken3 		= $this->_getParam('inputtoken3');
				$inputtoken4 		= $this->_getParam('inputtoken4');
				$inputtoken5 		= $this->_getParam('inputtoken5');
				$inputtoken6 		= $this->_getParam('inputtoken6');

				$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;
				
				if (!empty($tokenGoogle)) {

					$pga = new PHPGangsta_GoogleAuthenticator();
					// var_dump($data2['GOOGLE_CODE']);
					$setting 		= new Settings();
					$google_duration 	= $setting->getSetting('google_duration');
					$resultcapca = $pga->verifyCode($tokenGoogle, $responseCode, $google_duration);
					// var_dump($resultcapca);
					//  	var_dump($responseCode);
					//  	var_dump($tokenGoogle);die;
					if ($resultcapca) {
						$resultToken = $resHard['ResponseCode'] == '0000';
					} else {
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$errorToken = true;
						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Google Auth Code');	//$verToken['ResponseDesc'];
						$this->view->ERROR_MSG =  $this->language->_('Invalid Google Auth Code');

						// if ($tokenFailed === true)
						// {
						// 	$this->_redirect('/default/index/logout');
						// }
					}
				} else {

					$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$verToken 	= $Token->verify($challengeCode, $responseCode);

					if ($verToken['ResponseCode'] != '00') {
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$errorToken = true;
						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
						$this->view->ERROR_MSG =  $this->language->_('Invalid Response Code');
						echo $this->view->ERROR_MSG;

						if ($tokenFailed === true) {
							$this->_redirect('/default/index/logout');
						}
					}
				}
			}
			// if($msg == 'force_logout'){
			// 	$this->_forward('home'); //harus ketendan kareng salah token sebanyak
			// 	//$this->_redirect('/home');
			// }

			if ($errorToken==false) {
				$sendProvider 		= new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
				$resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg);
				// var_dump($resProvider);die;

				if ($resProvider == TRUE) {

					$paramSession['operator']			= $paramSession['operator'];
					$paramSession['ACCTSRC']			= $paramSession['ACCTSRC'];
					$paramSession['ccy']				= $paramSession['ccy'];
					$paramSession['ACCT_NAME']			= $paramSession['ACCT_NAME'];
					$paramSession['ACCT_NAME_DATA']		= $paramSession['ACCT_NAME_DATA'];
					$paramSession['ACCT_ALIAS_NAME']	= $paramSession['ACCT_ALIAS_NAME'];
					$paramSession['amount']				= $paramSession['amount'];
					$paramSession['fee']				= $paramSession['fee'];
					$paramSession['total']				= $paramSession['total'];
					$paramSession['orderId']			= $paramSession['orderId'];
					$paramSession['sgoProductCode']		= $paramSession['sgoProductCode'];
					$paramSession['operatorNama']		= $paramSession['operatorNama'];
					$paramSession['CUST_ID']			= $paramSession['CUST_ID'];
					$paramSession['USER_ID']			= $paramSession['USER_ID'];
					$paramSession['PSNumber']			= $msg['PS_NUMBER']['0'];
					$paramSession['date']				= $paramSession['date'];
					$paramSession['paymentMessage']		= $msg['paymentMessage']['0'];
					$paramSession['dataUi']				= $resWS['ResponseData']['dataUi'];
					$paramSession['taxType']			= $paramSession['taxType'];	
	
					$sessionNamespace->paramSession 	= $paramSession;
					$this->backURL = '/' . $this->view->modulename . '/' . $this->view->controllername . '/index';

					if ($this->_custSameUser) {
						//		die('sini');
						$paramSQL = array(
							"WA" 				=> false,
							"ACCOUNT_LIST" 	=> $this->_accountList,
							"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
						);
	
						// get payment query
						$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
						$select   = $CustUser->getPayment($paramSQL);
						$select->where('P.PS_NUMBER = ?', (string) $msg['PS_NUMBER']['0']);
						// echo $select;
						$pslip = $this->_db->fetchRow($select);
						$settingObj = new Settings();
						$setting = array(
							"COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
							"COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
							"COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
							"COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
							"COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
							'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
							"range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
							"auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
							"_dateFormat" 			=> $this->_dateDisplayFormat,
							"_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
							"_transfertype" 		=> array_flip($this->_transfertype["code"]),
						);
	
						$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
						$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));
	
						$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
						$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);
	
						$app = Zend_Registry::get('config');
						$appBankname = $app['app']['bankname'];
	
						$selectTrx = $this->_db->select()
							->from(
								array('TT' => 'T_TRANSACTION'),
								array(
									'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
									'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
									'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
									//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
									'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
									'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
									'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
									'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
									'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
									'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
									'TRA_REFNO'				=> 'TT.TRA_REFNO',
									'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
									'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
									'TRA_STATUS'			=> 'TT.TRA_STATUS',
									'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
									'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
									'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
									'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
									'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
									'CLR_CODE'				=> 'TT.CLR_CODE',
									'TT.RATE',
									'TT.PROVISION_FEE',
									'TT.NOSTRO_NAME',
									'TT.FULL_AMOUNT_FEE',
									'C.PS_CCY', 'C.CUST_ID',
									'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
									'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
									'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
									'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
									'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
																							FROM T_PERIODIC_DETAIL Y
																							inner join T_PSLIP Z
																							on Y.PS_PERIODIC = Z.PS_PERIODIC
																							where
																							Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
								)
							)
							->joinLeft(array('C' => 'T_PSLIP'), 'C.PS_NUMBER = TT.PS_NUMBER', array())
							->where('TT.PS_NUMBER = ?', $msg['PS_NUMBER']['0']);
						// var_dump($msg);
						// echo $selectTrx;die;
						$paramTrxArr = $this->_db->fetchAll($selectTrx);
	
						$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $msg['PS_NUMBER']['0']);
						$paramPayment = array_merge($pslip, $setting);
						// echo '<pre>';
						// print_r($paramPayment);
						// print_r($paramTrxArr);
						// die;
						$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
						$infoWarnOri = (!empty($check['infoWarning']) ? '*) ' . $check['infoWarning'] : '');
						$sessionNameConfrim->infoWarnOri = $infoWarnOri;
	
						if ($validate->isError() === true) {
							$error = true;
							$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
						}
	
						$Payment = new Payment($msg['PS_NUMBER']['0'], $this->_custIdLogin, $this->_userIdLogin);
						// if ($this->_hasPriviReleasePayment){
						$resultRelease = $Payment->releasePayment();
						// print_r($msg);
						// print_r($resultRelease);die;
						$this->view->ps_numb = $msg['PS_NUMBER'][0];
						$this->view->hidetoken = true;
						if ($resultRelease['status'] == '00') {
							$ns = new Zend_Session_Namespace('FVC');
							$ns->backURL = $this->view->backURL;
							$this->view->releaseresult = true;
							// $this->_redirect('/notification/success/index');
						} else {
							// die('here');
							$this->view->releaseresult = false;
							$this->_helper->getHelper('FlashMessenger')->addMessage($result);
							//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
							//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
							$this->_redirect('/notification/index/release');
						}
						// }
					// } else {
					// 	//die('cek');
					// 	$this->_redirect('/notification/success/index');
					}
					//$this->_redirect('/payment/tax/confirm');
				} else {
					$errors 	= $msg;
					$error		= true;
				}
	
				if ($error) {
					$this->view->error 		= $error;
					$this->view->ERROR_MSG 	= (isset($errors)) ? $errors : null;
				}
				
				$ns = new Zend_Session_Namespace('FVC');
				$ns->backURL = '/payment/newtax/next';
				$this->_redirect('/notification/success/index');
			}
		}
		//roki
		
		if ($paramSession['reprint'] == 1) { //if reprint
			$select	= $this->_db->select()
				->from(
					array('A'	 		=> 'T_TRANSACTION'),
					array('PS_NUMBER' 	=> 'A.PS_NUMBER',)
				)
				->joinLeft(array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER', array())
				//->where("A.USER_ID 			 = ?", $paramSession['USER_ID'])
				//->where("A.SOURCE_ACCOUNT 	 = ?", $paramSession['ACCTSRC'])
				->where("A.BILLER_ORDER_ID 	 = ?", $paramSession['orderId'])
				->order('B.PS_CREATED DESC')
				->limit(1);
			$psnumber = $this->_db->fetchRow($select);
			$this->view->psnumber = $psnumber['PS_NUMBER'];
		} else { // if payment tax
			$this->view->psnumber = $paramSession['PSNumber'];
		}
		// roki
		$month = array(
			$this->language->_('January'),
			$this->language->_('February'),
			$this->language->_('March'),
			$this->language->_('April'),
			$this->language->_('May'),
			$this->language->_('June'),
			$this->language->_('July'),
			$this->language->_('August'),
			$this->language->_('September'),
			$this->language->_('October'),
			$this->language->_('November'),
			$this->language->_('December')
		);

		$this->view->month_map = $month;

		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO' => $paramSession['ACCTSRC']));
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;

		$map_code = array();
		$select = $this->_db->select()
			->from(array('M_MAP_CODE'), array('MAP_CODE', 'MAP_NAME'))
			->query()->fetchAll();

		foreach ($select as $key) {
			$map_code[$key['MAP_CODE']] = $key['MAP_NAME'] . " (" . $key['MAP_CODE'] . ")";
		}
		$this->view->map_code = $map_code;

		$select = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('M_MAP_CODE'), array('DEPOSIT_CODE', 'DEPOSIT_NAME'))
				->where('MAP_CODE = ' . $paramSession['akuncode'])
		);
		foreach ($select as $key) {
			$depositType[$key['DEPOSIT_CODE']] = $key['DEPOSIT_NAME'] . " (" . $key['DEPOSIT_CODE'] . ")";
		}
		$this->view->depositType = $depositType;
		///

		//templatedetail - begin
		$htmldataDetailDetail = '';
		foreach ($paramSession['dataUi'] as $datadetail => $key) {
			//Zend_Debug::dump($key);

			if ($datadetail == 'total_amount') {
			} else {

				$htmldataDetailDetail .= '
			<tr>
				<td class="tbl-evencontent">' . $this->language->_($datadetail) . '</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">' . $key . '</td>
			</tr>
			';
			}
		}
		if (isset($htmldataDetailDetail)) $this->view->templateDetail = $htmldataDetailDetail;
		//templatedetail - end

		if (!$paramSession) {
			$this->cancel();
		}

		if ($this->_getParam('submit') == TRUE) {
			$this->_redirect('/payment/newtax/index');
		}

		$sessionNamespace->paramSession 	= $paramSession;
		$this->view->paramSession			= $paramSession;

		if ($pdf) {
			$htmldataDetail .=
				'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="550px">
				<tr>
					<td class="tbl-evencontent">&nbsp; Payment Ref</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $this->view->psnumber . '</td>
				</tr>
			
				<tr>
					<td class="tbl-evencontent">&nbsp; Source Account</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['ACCTSRC'] . '[' . $paramSession['ccy'] . '] - ' . $paramSession['ACCT_NAME_DATA'] . ' (' . $paramSession['ACCT_DESC'] . ') </td>
				</tr>
    			<tr>
					<td class="tbl-evencontent">&nbsp; Type Of Tax</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['operatorNama'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Billing ID</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['orderId'] . '</td>
				</tr>';

			if ($paramSession['taxType'] == "DJP") { //0,1,2,3
				$htmldataDetail .=
					'<tr>
					<td class="tbl-evencontent">&nbsp; NPWP</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['npwp'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Customer Name</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['customer_name'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Customer Address</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['customer_address'] . '</td>
				</tr>
		    	<tr>
					<td class="tbl-evencontent">&nbsp; Tax Object Number</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['NOP'] . '</td>
				</tr>
		        <tr>
					<td class="tbl-evencontent">&nbsp; Account Code</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['account_map'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Payment Type</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['type'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Tax Period</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['period'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; SK Number</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['sk_number'] . '</td>
				</tr>';
			} elseif ($paramSession['taxType'] == "DJA") { //7,8,9
				$htmldataDetail .=
					'<tr>
					<td class="tbl-evencontent">&nbsp; Customer Name</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['customer_name'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; K/L</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['k_l'] . '</td>
				</tr>
		    	<tr>
					<td class="tbl-evencontent">&nbsp; Echelon Unit 1</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['eselon_unit'] . '</td>
				</tr>
		        <tr>
					<td class="tbl-evencontent">&nbsp; Code Unit</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['work_unit'] . '</td>
				</tr>';
			} elseif ($paramSession['taxType'] == "DJBC") { //4,5,6
				$htmldataDetail .=
					'<tr>
					<td class="tbl-evencontent">&nbsp; Customer Name</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['customer_name'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; ID Type Customer</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['customer_id'] . '</td>
				</tr>
		    	<tr>
					<td class="tbl-evencontent">&nbsp; Document Type</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['document_type'] . '</td>
				</tr>
		        <tr>
					<td class="tbl-evencontent">&nbsp; Document Number</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['document_number'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Document Date</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['document_date'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; KPBC Code</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['kppbc_code'] . '</td>
				</tr>';
			}

			$amount = $paramSession['ccy'] . " " . Application_Helper_General::displayMoney($paramSession['amount']);
			$htmldataDetail .=
				'<tr>
					<td class="tbl-evencontent">&nbsp; Amount</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $amount . '</td>
				</tr>
		        <tr>
					<td class="tbl-evencontent">&nbsp; NTB/NTP</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['ntb'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; NTPN</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['ntpn'] . '</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; STAN</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $paramSession['dataUi']['stan'] . '</td>
				</tr>
			
				<tr>
					<td class="tbl-evencontent">&nbsp; Status</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">' . $this->language->_('Success') . '</td>
				</tr>
			</table>';

			//$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($this->view->psnumber, $paramSession);
			$this->_helper->download->pdf(null, null, null, 'Payment Tax Report Detail', $htmldataDetail);
		}
		if ($this->_request->getParam('printtrxtax') == 1) {
			// 			echo  "<pre>";
			// 			print_r($paramSession);die;
			// 			$paramSessionData = array();
			// 			$paramSessionData[''] = ;
			$this->_forward('printtrxtax', 'index', 'widget', array('psnumber' => $this->view->psnumber, 'param' => $paramSession));
		}
		Application_Helper_General::writeLog('MTAX', 'Tax Payment');
	}

	public function bulkAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
		$this->view->AccArr 		= $AccArr;
	}

	public function cancel()
	{
		unset($_SESSION['newtax']);
		$this->_redirect("/home/index");
	}



	
	public function npwpdataAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$id_code = $this->_getParam('id_code');

		$postField = array();
        //$tmpfile = $_FILES['file']['tmp_name'];
        //$filename = basename($_FILES['file']['name']);
        //$postField['data'] =  curl_file_create($tmpfile, $_FILES['file']['type'], $filename);
        //$headers = array("Content-Type" => "multipart/form-data");
		//echo json_encode($_FILES); 
		$ch = curl_init();
		//curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL, '192.168.86.26:360/mpng2/inquiry/npwp');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		
		$date = date('Y-m-d H:m:s');
		$req_id = sprintf('%06x-%06x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));

		$postFields = array(
			'uuid'	=> 'CMS.INTEROP'.$req_id,
			'datetime' => $date,
			'sender_id' => 'SGO360',
			'map_code' => '411125',
			'deposit_type' => '100',
			'npwp'	=> $id_code
	//		$uploadFieldName => realpath($tmp)
		);
		//echo json_encode($postFields);die; 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

		$response = curl_exec($ch);
		$result = json_decode($response);
		
		
		
		

		if(curl_errno($ch)){
			
			echo curl_error($ch);die;
		} 
	//die('here');
		echo json_encode($result);die;

		//echo $optHtml;
	}

	public function getdepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$id_code = $this->_getParam('id_code');

		$depositType = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('M_MAP_CODE'), array('DEPOSIT_CODE', 'DEPOSIT_NAME', 'NOP_ISMANDATORY', 'NOSK_ISMANDATORY'))
				->where('MAP_CODE = ' . $id_code)
		);

		$optHtml = '';
		if (!empty($depositType)) {
			$optHtml = '<option selected="true" disabled value="">' . $this->language->_("--Deposit Type--") . '</option>';
			foreach ($depositType as $key) {
				$optHtml .= '<option data-nop="' . $key['NOP_ISMANDATORY'] . '" data-nosk="' . $key['NOSK_ISMANDATORY'] . '" value="' . $key['DEPOSIT_CODE'] . '">' . $key['DEPOSIT_CODE'] . ' - ' . $key['DEPOSIT_NAME'] . '</option>';
			}
		}

		echo $optHtml;
	}

	public function validateLimit($amount, $ccy, $account, &$msg1)
	{
		$validate   = new ValidatePayment($this->_custIdLogin, $this->_userIdLogin);
		$resultValMakerlimit	= $validate->checkAmountACCSRC($amount, $ccy, $account);
		$resultValDailylimit	= $validate->checkAmountCCY($amount, $ccy);
		$minAmt 				= $this->validateMinAmt($ccy, $amount);

		if ($resultValDailylimit === TRUE) {
			if ($resultValMakerlimit === TRUE) {
				if ($minAmt === TRUE) {
					return TRUE;
				} else {
					$msg1 = $minAmt;
					return $msg1;
				}
			} else {
				$msg1 = $resultValMakerlimit;
				return $msg1;
			}
		} else {
			$msg1 = $resultValDailylimit;
			return $msg1;
		}
	}
	public function validateMinAmt($ccy, $amount = null)
	{
		if (Zend_Registry::isRegistered('language')) {
			$language = Zend_Registry::get('language');
		}
		$select = $this->_db->select()
			->from(array('M_MINAMT_CCY'), array('CCY_ID', 'MIN_TX_AMT'))
			->where('CCY_ID = ?', $ccy);

		$select = $this->_db->fetchRow($select);

		$minAmt = $select['MIN_TX_AMT'];
		$checked = true;

		// var_dump($amount);
		// var_dump($minAmt);
		// die;
		if ($amount < $minAmt) {
			$checked = 	$language->_('Amount cannot be lower than') . " " . $select['CCY_ID'] . " " . Application_Helper_General::displayMoney($minAmt) . ".";
		}

		return $checked;
	}
}
