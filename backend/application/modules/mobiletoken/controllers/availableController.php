<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class token_availableController extends Application_Main
{
	public function indexAction() 
	{ 
		$this->view->statusCode = $this->_masterglobalstatus['code'];
    	$this->view->statusDesc = $this->_masterglobalstatus['desc'];
    	$this->view->modulename = $this->_request->getModuleName();
    	$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

	  	$select = $this->_db->select()->distinct()
				->from(array('A' => 'M_CUSTOMER'),
					array('CUST_ID'))
					->order('CUST_ID ASC')
					 -> query() ->fetchAll();
			$this->view->var=$select;
		 
	    $fields = array('C.CUST_ID'   		=> array('field'    => 'C.CUST_ID',
	                                        		 'label'    => $this->language->_('Company Code'),
	                                        		 'sortable' => true),
	    
	                    'CUST_NAME' 		=> array('field'    => 'CUST_NAME',
	                                        		 'label'    => $this->language->_('Company Name'),
	                                        		 'sortable' => true),
	    
	                    'USER_ID'     		=> array('field'    => 'USER_ID',
	                                        		 'label'    => $this->language->_('User ID'),
	                                       		  	 'sortable' => true),
	    
	    				'USER_FULLNAME'     => array('field'    => 'USER_FULLNAME',
	                                        		 'label'    => $this->language->_('User Name'),
	                                       		  	 'sortable' => true),
	    
	    				'TOKEN_ID'     		=> array('field'    => 'TOKEN_ID',
	                                        		 'label'    => $this->language->_('Token ID'),
	                                       		  	 'sortable' => true),
	    
	    				'USER_EMAIL'    	=> array('field'  	=> 'USER_EMAIL',
	                                        		 'label'    => $this->language->_('Email'),
	                                        		 'sortable' => true),
	    
	    				'USER_PHONE'    	=> array('field'  	=> 'USER_PHONE',
	                                        		 'label'    => $this->language->_('Phone Number'),
	                                        		 'sortable' => true),
	    
	                    'USER_CREATED'		=> array('field' 	=> 'USER_CREATED',
	                                        		 'label'    => $this->language->_('Created Date'),
	                                        		 'sortable' => true),
	    
	    				'USER_SUGGESTED'     => array('field'  	=> 'USER_SUGGESTED',
	                                        		 'label'    => $this->language->_('Suggested Date'),
	                                        		 'sortable' => true),
	    
	    				'USER_UPDATED'     	=> array('field'  	=> 'USER_UPDATED',
	                                        		 'label'    => $this->language->_('Approved Date'),
	                                        		 'sortable' => true),
	    
	    				'USER_LOCKREASON'   => array('field'  	=> 'USER_LOCKREASON',
	                                        		 'label'    => $this->language->_('Lock Reason'),
	                                        		 'sortable' => true),
	    
	                    'USER_STATUS'   	=> array('field'    => 'USER_STATUS',
	                                        		 'label'    => $this->language->_('Status'),
	                                        		 'sortable' => true),
	                   );
	    
	    //validasi page, jika input page bukan angka               
	    $page = $this->_getParam('page');
	    $csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
    
    
	    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
	    
	    //validasi sort, jika input sort bukan ASC atau DESC
	    $sortBy  = $this->_getParam('sortby');
	    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	    $sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
	    $filterArr = array(	'filter' => array('StripTags','StringTrim'),
	                       	'custid'    => array('StripTags','StringTrim','StringToUpper'),
	                       	'custname'  => array('StripTags','StringTrim','StringToUpper'),
	    					'username'  => array('StripTags','StringTrim','StringToUpper'),
	    					'userid'  => array('StripTags','StringTrim','StringToUpper'),
	                       	'status' => array('StripTags','StringTrim')
	                      );
	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    
	    $this->view->currentPage = $page;
	    $this->view->sortBy = $sortBy;
	    $this->view->sortDir = $sortDir;
	    
    
    // proses pengambilan data filter,display all,sorting
    
      	$caseStatus = "(CASE B.USER_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";
  			
        $select2 = $this->_db->select()
					        ->from(array('B' => 'M_USER'),array())
					        ->join(array('C' => 'M_CUSTOMER'), 'B.CUST_ID = C.CUST_ID',array(	'C.CUST_ID',
				        																	 	'C.CUST_NAME',
				        																	 	'B.USER_ID',
				        																	 	'B.USER_FULLNAME',
				        																		'B.TOKEN_ID',
				        																	 	'B.USER_EMAIL',
				        																	 	'B.USER_PHONE',
				        																	 	'B.USER_CREATED',
				        																	 	'B.USER_SUGGESTED',
				        																	 	'B.USER_UPDATED',
				        																	 	'B.USER_LOCKREASON',
				        																	 	'USER_STATUS'=>$caseStatus)); 
		
        if($filter==TRUE)
        {
			$custid     = html_entity_decode($zf_filter->getEscaped('custid'));
			$custname   = html_entity_decode($zf_filter->getEscaped('custname'));
			$userid     = html_entity_decode($zf_filter->getEscaped('userid'));
			$username   = html_entity_decode($zf_filter->getEscaped('username'));
			$status  = $zf_filter->getEscaped('status');
			if($custid)$select2->where('UPPER(C.CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($custid).'%'));
			if($custname)$select2->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($custname).'%'));
			if($userid)$select2->where('UPPER(USER_ID) LIKE '.$this->_db->quote('%'.strtoupper($userid).'%'));
			if($username)$select2->where('UPPER(USER_FULLNAME) LIKE '.$this->_db->quote('%'.strtoupper($username).'%'));
			if($status)$select2->where('USER_STATUS=?',$status);
			//echo $status; die;
			
			$this->view->custid = $custid;
			$this->view->custname = $custname;
			$this->view->userid = $userid;
			$this->view->username = $username;
			$this->view->status = $status;
        }
        $select2->order($sortBy.' '.$sortDir);
 
    	// END proses pengambilan data filter,display all,sorting
		//echo $select2; die;
    
        if($csv || $pdf)
        {
        	$result = $select2->query()->FetchAll();
        	
        	foreach($result as $key=>$value)
			{
				$result[$key]["USER_CREATED"] = Application_Helper_General::convertDate($value["USER_CREATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$result[$key]["USER_SUGGESTED"] = Application_Helper_General::convertDate($value["USER_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$result[$key]["USER_UPDATED"] = Application_Helper_General::convertDate($value["USER_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
			
        	$header = Application_Helper_Array::simpleArray($fields,'label');

	        if($csv)
			{
				Application_Helper_General::writeLog('RPCU','Download CSV Customer User List');
				$this->_helper->download->csv($header,$result,null,'Customer User List');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('RPCU','Download PDF Customer User List');
				$this->_helper->download->pdf($header,$result,null,'Customer User List');
			}
        }
        else
        {
        	Application_Helper_General::writeLog('RPCU','View Customer User List');
        }
        
	    $this->paging($select2);
	    //die;
	    $this->view->fields = $fields;
	    $this->view->filter = $filter;
     //insert log
	} 
}