<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentOpen.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';

class opensweep_OthersController extends Application_Main
{

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
	}


	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$custID = $this->_custIdLogin;
		$userID = $this->_userIdLogin;
		$this->view->ccyArr = $this->getCcy();
		$CustomerUser = new CustomerUser($custID, $userID);

		$settings 			= new Application_Settings();
		$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$this->view->lldIdenticalArr 	= $lldIdenticalArr;
		$this->view->lldRelationshipArr = $lldRelationshipArr;
		$this->view->lldPurposeArr 		= $lldPurposeArr;
		$ccyList  			= $settings->setCurrencyRegistered();	

		$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;








		$selects = $this->_db->select()
			->from(array('MAB' => 'M_APP_BOUNDARY'))
			->join(array('MABG' => 'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
			->where('MAB.CUST_ID = ?', (string) $cust_id)
			->group('BOUNDARY_ID');
		// echo $select;
		$result = $this->_db->fetchAll($selects);

		$dataArr = array();
		// $alf = 'A';
		// $alf++;
		// ++$alf;
		// print_r((int)$alf);
		// $nogroup = sprintf("%02d", 1);
		// print_r($result);die;
		foreach ($result as $row) {
			list($grouptype, $groupname, $groupnum) = explode("_", $row['GROUP_USER_ID']);
			if ($grouptype == 'N')
				$name = 'Group ' . trim($groupnum, '0');
			else
				$name = 'Special Group';

			$selectUser = $this->_db->select()
				->from(array('M_APP_GROUP_USER'), array('USER_ID'))
				->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
				// echo $selectUser;die;
				->query()->fetchall();

			$userlist = '';

			$policy = explode(' AND ', $row['POLICY']);


			foreach ($policy as $key => $value) {
				if ($value == 'SG') {
					$group = 'S_' . $cust_id;
					$selectUser = $this->_db->select()
						->from(array('M_APP_GROUP_USER'), array('USER_ID'))
						->where('GROUP_USER_ID = ?', $group)
						// echo $selectUser;die;
						->query()->fetchall();
					foreach ($selectUser as $val) {
						if (empty($userlist))
							$userlist .= $val['USER_ID'];
						else
							$userlist .= ', ' . $val['USER_ID'];
					}
				} else {



					$alphabet = array("A" => 1, "B" => 2, "C" => 3, "D" => 4, "E" => 5, "F" => 6, "G" => 7, "H" => 8, "I" => 9, "J" => 10, "K" => 11, "L" => 12, "M" => 13, "N" => 14, "O" => 15, "P" => 16, "Q" => 17, "R" => 18, "S" => 19, "T" => 20, "U" => 21, "V" => 22, "W" => 23, "X" => 24, "Y" => 25, "Z" => 26,);

					$policyor = explode(' OR ', $value);
					// print_r($policyor);die;
					foreach ($policyor as $numb => $valpol) {
						if ($valpol == 'SG') {
							$group = 'S_' . $cust_id;
							$selectUser = $this->_db->select()
								->from(array('M_APP_GROUP_USER'), array('USER_ID'))
								->where('GROUP_USER_ID = ?', $group)
								// echo $selectUser;die;
								->query()->fetchall();
							foreach ($selectUser as $val) {
								if (empty($userlist)) {
									$userlist .= $val['USER_ID'];
								} else {
									$userlist .= ', ' . $val['USER_ID'];
								}
							}
						} else {
							$nogroup = sprintf("%02d", $alphabet[$valpol]);
							// print_r($valpol);
							$group = 'N_' . $cust_id . '_' . $nogroup;
							$selectUser = $this->_db->select()
								->from(array('M_APP_GROUP_USER'), array('USER_ID'))
								->where('GROUP_USER_ID = ?', $group)
								// echo $selectUser;die;
								->query()->fetchall();
							// print_r($selectUser);
							foreach ($selectUser as $val) {
								if (empty($userlist))
									$userlist .= $val['USER_ID'];
								else
									$userlist .= ', ' . $val['USER_ID'];
							}
						}
					}
				}
			}

			$changepolicy = explode(" ", $row['POLICY']);
			$data = array();
			foreach ($changepolicy as $keypolicy => $valpolicy) {

			if ($valpolicy == 'AND') {
			  $valpolicy = '<span style="color: blue;">and</span>';
			}elseif ($valpolicy == 'OR') {
			  $valpolicy = '<span style="color: blue;">or</span>';
			}elseif ($valpolicy == 'THEN') {
			  $valpolicy = '<span style="color: blue;">then</span>';
			}

			$replacepolicy1 = str_replace('(', '<span style="color: blue;">(</span>', $valpolicy);
			$replacepolicy2 = str_replace(')', '<span style="color: blue;">)</span>', $replacepolicy1);

			$data[$keypolicy] = $this->language->_($replacepolicy2);
			}

			$valpolicy2 = implode(" ", $data);
			
			$arrTraType     = array_combine($this->_paymenttype["code"], $this->_paymenttype["desc"]);

			$dataArr[] = array(
				'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
				'CURRENCY' => $row['CCY_BOUNDARY'],
				'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN']) . " - " . Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
				'GROUP_NAME' => $valpolicy2,
				'USERS' => $userlist
			);
		}
		// print_r($dataArr);die;
		/* print_r($row);die();*/

		$this->view->dataBoundary = $dataArr;

		//general settings, utk ambil parameter skn rtgs
		$Settings = new Settings();
		$this->view->min_amount_skn = $Settings->getSetting('min_amount_skn');			
		$this->view->max_amount_skn = $Settings->getSetting('max_amount_skn');		

		$this->view->min_amount_rtgs = $Settings->getSetting('min_amount_rtgs');			
		$this->view->max_amount_rtgs = $Settings->getSetting('max_amount_rtgs');	

		$this->view->contactemail = $Settings->getSetting('master_bank_email');	
		$this->view->contactphone = $Settings->getSetting('master_bank_telp');	
		 
		//general settings, utk ambil bank support list
		$this->view->bank_support_api = $Settings->getSetting('bank_support_api');		
		$this->view->bank_support_inhouse_transfer = $Settings->getSetting('bank_support_inhouse_transfer');			
		$this->view->bank_support_online_transfer = $Settings->getSetting('bank_support_online_transfer');			
		$this->view->bank_support_domestic = $Settings->getSetting('bank_support_domestic');			
		$this->view->bank_support_inhouse_inquiry = $Settings->getSetting('bank_support_inhouse_inquiry');			
		$this->view->bank_support_interbank_inquiry = $Settings->getSetting('bank_support_interbank_inquiry');	
		$residentArr 		= array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
		$citizenshipArr		= array("W" => "WNI", "N" => "WNA");
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();

		if(!$this->view->hasPrivilege('BLBU')){
			$this->view->hidesave = 'none';
		}
		
		$this->view->citizenshipArr 	= $citizenshipArr;
		$this->view->residentArr 		= $residentArr;
		$this->view->lldCategoryArr 	= $lldCategoryArr;
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		/*
		 * generate payment ref
		 */
		 
		 $TRA_MIN_AMOUNT = $this->_getParam('TRA_MIN_AMOUNT');
					
		//if (empty($TRA_MIN_AMOUNT)) {
			$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				foreach($ccyList as $k => $valccy){
					if($valccy['CCY_ID'] == 'IDR'){
						$minamt = str_replace('.00', '', Application_Helper_General::displayMoney($valccy['MIN_AMT']));
					//	var_dump($minamt);
						$this->view->TRA_MIN_AMOUNT = $minamt;
					}
				}
			//var_dump($ccyList);die;
				
		//		}

		$submit = $this->_getParam('submit');

		//if ($submit) {
	//		$this->_redirect('/opensweep/others/confirm');
//		}


		$banklist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'), array('A.BANK_CODE'))
				// ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
				//  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
				//  // ->join(array('D' => 'M_DOMESTIC_BANK_TABLE'),'D.BANK_NAME = B.BANK_NAME',array('D.SWIFT_CODE'))
				->where('A.CUST_ID = ?', $this->_custIdLogin)
				->group('A.BANK_CODE')
			// ->where("A.BANK_CODE IN ('008','014','013','009','032','031','153','002') ")
			// ->order('A.APIKEY_ID ASC')
		);
		// var_dump($banklist);
		$par = array();
		if (!empty($banklist)) {
			foreach ($banklist as $key => $value) {
				$par[] = $value['BANK_CODE'];
			}
		}
		// var_dump($par);
		// var_dump($banklist);die;
		if (empty($par)) {
			$par = array('000');
		}

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'))
			->where('C.BANK_CODE IN (?)', $par);
		 //echo $selectbank;die;
		$databank 					= $this->_db->fetchAll($selectbank);

		$this->view->BankArr = $databank;


		$this->view->paymentreff = $this->generatePaymentReff(1); //send payment ref to view

		$this->view->sourceType = 2; //source type, 1 = single select, 2 = upload

		$complist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_USER'), array('CUST_ID'))

				->where("A.USER_ID = ? ", $this->_userIdLogin)
		);
		// echo $complist;;die;
		// var_dump($complist);die;
		$comp = "'";
		// print_r($complist);die;
		foreach ($complist as $key => $value) {
			$comp .= "','" . $value['CUST_ID'] . "','";
		}
		$comp .= "'";


		$acctlist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'))
				->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
				->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
				// ->where('A.ACCT_STATUS = ?','5')
				->where("A.CUST_ID IN (" . $comp . ")")
				->order('A.APIKEY_ID ASC')
			// echo $acctlist;
		);
		// echo $acctlist;die;
		// echo '<pre>';
		// echo $acctlist;
		// print_r($acctlist);die;
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}

		$acct = array();
		foreach ($account as $key => $value) {
			$acct[$key - 1]['ACCT_NO'] = $value['account_number'];
			$acct[$key - 1]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$key - 1]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$key - 1]['ACCT_NAME'] = $value['account_name'];
		}
		// var_dump($acct);
		$AccArr = $acct;


		$paramscr = array('CCY_IN' => 'IDR');
		$this->view->sourceAcc = $AccArr;
		//		$this->view->sourceAcc = $CustomerUser->getAccounts();
		// 		print_r($this->view->sourceAcc);die;
		//$sourceAcc =  $CustomerUser->getAccounts();

		$select   = $CustomerUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		$select->where("B.BENEFICIARY_ISAPPROVE = ?", 1);

		// $resultBeneficiary = $this->_db->fetchAll($select);
		// $this->view->listBeneficiary = $resultBeneficiary;
		// $this->view->listBeneficiary = $AccArr;
		$this->view->listBeneficiary = array('' => ' -- Please Bank -- ');

		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->hidetoken = false;
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];
			 //var_dump($tokenIdUser);
			 
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			// var_dump($tokendata);
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		/*
		 * Verify form
		*/
		
		$anyValue = '-- '.$this->language->_('Select City'). ' --';
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
		$select ->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);
		
		$cityCodeArr 			= array(''=> $anyValue);
		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		$this->view->cityCodeArr 	= $cityCodeArr;
		
	//	$sessionNameRand = new Zend_Session_Namespace('openSweep');

		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		if(empty($tranferdatetype)){$tranferdatetype=1;}
		$this->view->tranferdatetype = $tranferdatetype;

		$filter 		= new Application_Filtering();
		$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
		$PS_EFDATE 		= date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));//Application_Helper_General::convertDate($this->getCurrentDate());
		$isConfirmPage 	= $this->_getParam('confirmPage');


		if($this->_getParam('process') == 'back'){
			Zend_Session::namespaceUnset('TW');
		}

		$sessionNamespace = new Zend_Session_Namespace('TW');


		$process 	= $this->_getParam('process');
		$submitBtn 	= ($this->_request->isPost())? true: false;

		// print_r($this->_request->getParams());
		// get ps_number data for repair payment
		
		//tambahn pentest
		$date_val	= date('d/m/Y');
		$PS_EFDATE_VAL = $this->_request->getParam('PS_EFDATE');
		$this->view->paymentDate = $PS_EFDATE_VAL;
		if($PS_EFDATE_VAL > $date_val){
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}
		elseif ($PS_EFDATE_VAL == $date_val){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}
		else{
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}
		
		
		$PS_PERIODIC_EVERY = $this->_request->getParam('PERIODIC_EVERY');
		$PS_PERIODIC_EVERY_DATE = $this->_request->getParam('PERIODIC_EVERYDATE');
		$PS_ENDDATE_VAL = ($this->_request->getParam('PS_ENDDATEPERIODIC'))?$this->_request->getParam('PS_ENDDATEPERIODIC'):date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
		$this->view->endDatePeriodic = $PS_ENDDATE_VAL;		
				
		$jenisTransfer =  $this->_request->getParam('tranferdatetype');
		$jenisPeriodic =  $this->_request->getParam('tranferdateperiodictype');
		
		if ($jenisTransfer == '1'){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}elseif ($jenisTransfer == '2'){
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}elseif ($jenisTransfer == '3'){
			$periodictranfer = $this->language->_('Periodic Transfer');
			$this->view->paymentType = $periodictranfer;
			
			if ($jenisPeriodic == '5'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY;
			}elseif ($jenisPeriodic == '6'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY_DATE;
			}
		}
		// End


		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		if(empty($tranferdatetype)){$tranferdatetype=1;}
		$this->view->tranferdatetype = $tranferdatetype;

		
		if (!empty($pslipData))	
		{
			if($TransferDate == '1'){
				$this->view->PS_EFDATEFUTURE = $futureDate;
			}
			elseif ($tranferdatetype =='3'){
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII;//						
			}
			else{
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			}
		}
		else{
			$this->view->PS_EFDATEFUTURE = $futureDate;
		}
	//	var_dump($PS_EFDATE);
		//var_dump($this->view->PS_EFDATEFUTURE);
		//$TRANSFER_TYPE ='SKN';
		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');

		if ($this->_request->isPost()) {
//echo '<pre>';
	//									 var_dump($this->_request->getParams());
		//								 print_r($param);die;

			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
				
			if($this->_getParam('randomTransact') == $sessionNameRand->randomTransact){

				try {
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					$blocksize = 256;  // can be 128, 192 or 256
					$ACBENEF =  SGO_Helper_AES::decrypt( $this->_request->getParam('ACBENEF'), $passwordRand, $blocksize );
					$ACCTSRC =  SGO_Helper_AES::decrypt( $this->_request->getParam('ACCTSRC'), $passwordRand, $blocksize );
					$TRA_AMOUNT =  SGO_Helper_AES::decrypt( $this->_request->getParam('TRA_AMOUNT'), $passwordRand, $blocksize );
					$TRA_MIN_AMOUNT =  SGO_Helper_AES::decrypt( $this->_request->getParam('TRA_MIN_AMOUNT'), $passwordRand, $blocksize );
				} 
				catch (Exception $e) {
					$ACBENEF = '';
					$ACCTSRC = '';
					$TRA_AMOUNT = '';
					$TRA_MIN_AMOUNT = '';
				}

				$SWIFT_CODE 		= $this->_getParam('SWIFT_CODE');
				$SAVEBENE 			= $this->_getParam('save_bene');
				$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PS_SUBJECT')		, "PS_SUBJECT");
				$PS_EFDATE 			= $filter->filter($this->_request->getParam('PS_EFDATE')		, "PS_DATE");

				if ($isConfirmPage == 1) {
					//$TRA_AMOUNT 		= $filter->filter($TRA_AMOUNT		, "AMOUNT");
					//$TRA_MIN_AMOUNT 		= $filter->filter($TRA_MIN_AMOUNT		, "AMOUNT");
					$TRA_AMOUNT 		= $filter->filter($this->_request->getParam('TRA_AMOUNT')		, "AMOUNT");
					$TRA_MIN_AMOUNT 		= $filter->filter($this->_request->getParam('TRA_MIN_AMOUNT')		, "AMOUNT");
				}else{
					$TRA_AMOUNT 		= $filter->filter($this->_request->getParam('TRA_AMOUNT')		, "AMOUNT");
					$TRA_MIN_AMOUNT 		= $filter->filter($this->_request->getParam('TRA_MIN_AMOUNT')		, "AMOUNT");
				}
				

				$TRA_MESSAGE 		= $filter->filter($this->_request->getParam('TRA_MESSAGE')		, "TRA_MESSAGE");

				if ($isConfirmPage == 1) {
					$ACCTSRC 			= $filter->filter($ACCTSRC	, "ACCOUNT_NO");
				}else{
					$ACCTSRC 			= $this->_request->getParam('ACCTSRC');
				}
				
				//$SWEEP_SPLIT = $this->_request->getParam('sweep_split');

				$ACCTSRC_BANKCODE			= $filter->filter($this->_request->getParam('sourceBankCode')		, "BANK_CODE");
				$ACBENEF 			= $this->_request->getParam('ACBENEF');//$filter->filter($ACBENEF			, "ACCOUNT_NO");
				$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME')	, "ACCOUNT_NAME");
				$ACCTSRCTEXT 	= $filter->filter($this->_request->getParam('ACCTSRCTEXT')	, "ACCOUNT_NAME");
				
				
				$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS')	, "ACCOUNT_ALIAS");
				$ACBENEF_EMAIL 		= $filter->filter($this->_request->getParam('ACBENEF_EMAIL')	, "EMAIL");
				$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE')		, "SELECTION");

				$ACBENEF_CITIZEN 		= $filter->filter($this->_request->getParam('ACBENEF_CITIZENSHIP_SELECT')		, "SELECTION");
				$ACBENEF_RESIDENT 		= $filter->filter($this->_request->getParam('ACBENEF_RESIDENT_SELECT')		, "SELECTION");
				$ACBENEF_CATEGORY 		= $filter->filter($this->_request->getParam('LLD_CATEGORY_SELECT')		, "SELECTION");
				$ACBENEF_IDTYPE 		= $filter->filter($this->_request->getParam('LLD_BENEIDENTIF_SELECT')		, "SELECTION"); 
 
				$ACBENEF_CITY 		= $filter->filter($this->_request->getParam('CITY_CODE_SELECT')		, "SELECTION");
				$ACBENEF_PHONE 		= $this->_getParam('ACBENEF_PHONE');


				$BANK_NAME			= $filter->filter($this->_request->getParam('BANK_NAME')		, "BANK_NAME");

				$TRANSFER_TYPE 		= $filter->filter($this->_request->getParam('TRANSFER_TYPE')	, "SELECTION");

				// if($TRANSFER_TYPE == 'ONLINE'){
					$BANK_CODE			= $filter->filter($this->_request->getParam('ACCT_BANK')		, "BANK_CODE");
				// }
				// else{
					$TRANSFER_FEE 		= $filter->filter($this->_request->getParam('TRANSFER_FEE')		, "TRANSFER_FEE");
					$TRA_REFNO 			= $filter->filter($this->_request->getParam('TRA_REFNO')		, "TRA_REFNO");

					$ACBENEF_ADDRESS	= $filter->filter($this->_request->getParam('ACBENEF_ADDRESS')	, "ADDRESS");
					$ACBENEF_ADDRESS2	= $filter->filter($this->_request->getParam('ACBENEF_ADDRESS2')	, "ADDRESS");
				
					$ACBENEF_RESIDENT= $filter->filter($this->_request->getParam('ACBENEF_RESIDENT'), "SELECTION");
					$this->view->ACBENEF_RESIDENT= (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT	: 'R'; 
					
					$ACBENEF_CITIZENSHIP= $filter->filter($this->_request->getParam('ACBENEF_CITIZENSHIP'), "SELECTION");
					$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: 'W';
					
					$CITY_CODE= $filter->filter($this->_request->getParam('CITY_CODE'), "SELECTION");
					$CITY = $filter->filter($this->_request->getParam('CITY'), "SELECTION");
					$this->view->CITY_CODE= (isset($CITY_CODE))	? $CITY_CODE	: ''; 
					
					
					$CLR_CODE			= $filter->filter($this->_request->getParam('CLR_CODE')			, "BANK_CODE");
					$LLD_CATEGORY 		= $filter->filter($this->_request->getParam('LLD_CATEGORY')		, "LLD_CODE");
					$LLD_IDENTICAL 		= $this->_request->getParam('LLD_IDENTICAL');
					$LLD_RELATIONSHIP 	= $this->_request->getParam('LLD_RELATIONSHIP');
					$LLD_PURPOSE 		= $this->_request->getParam('LLD_PURPOSE');
					$LLD_DESCRIPTION 	= $this->_request->getParam('LLD_DESCRIPTION');
					$LLD_BENEIDENTIF 	= $filter->filter($this->_request->getParam('LLD_BENEIDENTIF')	, "LLD_CODE");
					$LLD_BENENUMBER 	= $filter->filter($this->_request->getParam('LLD_BENENUMBER')	, "LLD_CODE");
					$LLD_SENDERIDENTIF 	= $filter->filter($this->_request->getParam('LLD_SENDERIDENTIF'), "LLD_CODE");
					$LLD_SENDERNUMBER 	= $filter->filter($this->_request->getParam('LLD_SENDERNUMBER')	, "LLD_CODE");

					$TRANSFER_FEE_num 	= Application_Helper_General::convertDisplayMoney($TRANSFER_FEE);
				// }
					
	
				$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
				$TRA_MIN_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT);
				
				$PERIODIC_EVERY 	= $filter->filter($this->_request->getParam('PERIODIC_EVERY')	, "SELECTION");
				$PERIODIC_EVERYDATE = $filter->filter($this->_request->getParam('PERIODIC_EVERYDATE')	, "SELECTION");
				$TrfDateType 		= $filter->filter($this->_request->getParam('tranferdatetype')	, "SELECTION");				
				$TrfPeriodicType 	= $filter->filter($this->_request->getParam('tranferdateperiodictype')	, "SELECTION");
				$PS_ENDDATE			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC')		, "PS_DATEPERIODIC");
				
				$PS_EVERY_PERIODIC_UOM 	= $TrfPeriodicType;
				$START_DATE 			= $filter->filter(date('d/m/Y')	, "PS_DATE");
				$EXPIRY_DATE 			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC')	, "PS_DATE");

				$TRA_NOTIF		= $this->_request->getParam('notif');
					$TRA_EMAIL		= $this->_request->getParam('email_notif');
					$TRA_SMS		= $this->_request->getParam('sms_notif');
					
					$SWEEP_SPLIT		= $this->_request->getParam('sweep_split');
					$SWEEP_TYPE		= $this->_request->getParam('sweep_type');
	
				$filter->__destruct();
				unset($filter);

				$TRANSFER_TYPE = $this->_request->getParam('TRANSFER_TYPE');
				
				if (empty($TRANSFER_TYPE)) {
					$TRANSFER_TYPE = 'ONLINE';
				}

				if ($isConfirmPage == 1) {
					$TRANSFER_TYPE = $this->_request->getParam('TRANSFER_TYPE');
				}

				if ($ACCTSRC_BANKCODE == $BANK_CODE) {
					$this->view->TRANSFER_TYPE_view = 'Cash Pooling - PB';
				}
				else{
					$this->view->TRANSFER_TYPE_view = 'Cash Pooling - '.$TRANSFER_TYPE;
				}
	


			if ($submitBtn)
				{
					
					

					// die('here');

					//cek periodik				
					if ($TrfDateType=='3'){
						//var_dump($PS_EVERY_PERIODIC_UOM);						
						// NextDate					
							//echo '<pre>';
							//var_dump($this->_request->getParams());						
							
						if ($PS_EVERY_PERIODIC_UOM ==5){ //every day
							
							$dateNow = mktime(0,0,0,date("n"),date("j"),date("Y"));
							$every = (int)$PERIODIC_EVERY;
							$d = date("w",$dateNow);
							
							$addDay = $every;
							if ($every > $d){
								$addDay = $addDay;
							}else{
								$addDay = $addDay + 7;
							}
							$nextDate  =  (int)$addDay - (int)$d;
							//var_dump($nextDate);
							$NEXT_DATE 			= $this->_request->getParam('PS_EFDATE');
							
							$NEXT_DATE = date("Y-m-d",strtotime($NEXT_DATE));
							//var_dump($NEXT_DATE);
							$PERIODIC_EVERY_VAL = $PERIODIC_EVERY;
							
						}elseif ($PS_EVERY_PERIODIC_UOM ==6){ //every date
							
							$dateNextMonth = mktime(0,0,0,date("n"),date("j")+1,date("Y"));
							
							$dateNow = date("j");
							$maxDays=date('t', $dateNextMonth);
							//echo $maxDays; die;
							$every = (int)$PERIODIC_EVERYDATE;
	
							if ($every > $dateNow){
								$addMonth = 0;
							}else{
								$addMonth = 1; 
							}
							
							if ($maxDays >=  $every){
								$every = $every;
							}else{
								$every = $maxDays;
							}
							
							$nextDate = mktime(0,0,0,date("n")+$addMonth,$every,date("Y"));
							$NEXT_DATE = date("Y-m-d",$nextDate);
							$PERIODIC_EVERY_VAL = $PERIODIC_EVERYDATE;
						}
						//$NEXT_DATE 			= $this->_request->getParam('PS_EFDATE');
							
						//$NEXT_DATE = date("Y-m-d",strtotime($NEXT_DATE));
						//echo $NEXT_DATE."<br>";die('here'); 
						$END_DATE = join('-',array_reverse(explode('/',$EXPIRY_DATE))); 
					//	$PS_EFDATE = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
						$PS_EFDATE = $this->_request->getParam('PS_EFDATE');
						
						$PS_EFDATE_ORII = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
						//echo $END_DATE;
						 
						/*if ($TrfPeriodicType==5 && empty($PERIODIC_EVERY) ){
							$errorMsg		 		= $this->language->_('Period can not be left blank.');
							$this->view->error 	= true;
							$this->view->ERROR_MSG	= $errorMsg;
						}elseif ($TrfPeriodicType==6 && empty($PERIODIC_EVERYDATE)){							
							$errorMsg		 		= $this->language->_('Period can not be left blank.');
							$this->view->error 	= true;		
							$this->view->ERROR_MSG	= $errorMsg;				
						}elseif (strtotime($NEXT_DATE) > strtotime($END_DATE)){						
							$errorMsg		 	= $this->language->_('End Date must grather than'). " ". $NEXT_DATE;
							$this->view->error 	= true;		
							$this->view->ERROR_MSG	= $errorMsg;					
						}*/					
					}else if($TrfDateType=='2'){
						$PS_EFDATE 			= $this->_request->getParam('PS_EFDATE');
						//var_dump($PS_EFDATE);
						$PS_EFDATE = $PS_EFDATE;
					}else{
						$PS_EFDATE = $PS_EFDATE;
					}
					
					$ACCTSRC = $this->_getParam('ACCTSRC');

					if (empty($ACCTSRC)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Source Account cannot be left blank';
					}
					
					$ACCT_BANK = $this->_getParam('sourceBankCode');

					if (empty($ACCT_BANK)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Beneficiary Bank Must Be Selected';
					}
					
					$CITY_CODE = $this->_getParam('CITY_CODE');

					if (empty($CITY_CODE)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'City cannot be left blank';
					}
					
					$ACBENEF = $this->_getParam('ACBENEF');
					
					if (empty($ACBENEF)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Beneficiaty Account cannot be left blank';
					}
					
					$ACBENEF_ADDRESS = $this->_getParam('ACBENEF_ADDRESS');
					
					if (empty($ACBENEF_ADDRESS)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Beneficiaty Address cannot be left blank';
					}
					
					$ACBENEF_ADDRESS2 = $this->_getParam('ACBENEF_ADDRESS2');
					
					if (empty($ACBENEF_ADDRESS2)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Beneficiaty Address 2 cannot be left blank';
					}
					
					$ACBENEF_EMAIL = $this->_getParam('ACBENEF_EMAIL');
					
					if (empty($ACBENEF_EMAIL)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Beneficiaty Email cannot be left blank';
					}
					
					$LLD_BENEIDENTIF_SELECT = $this->_getParam('LLD_BENEIDENTIF');
				/*	
					if (empty($LLD_BENEIDENTIF_SELECT)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Beneficiaty ID Type cannot be left blank';
					}
					
					$LLD_BENENUMBER = $this->_getParam('LLD_BENENUMBER');
					
					if (empty($LLD_BENENUMBER)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Beneficiaty ID Number cannot be left blank';
					}
					*/

					$TRA_MIN_AMOUNT = $this->_getParam('TRA_MIN_AMOUNT');
					
					
					

					if (empty($TRA_MIN_AMOUNT)) {
												$this->view->error = true;
												$confirmPage = false;
												$error_msg[] = 'Minimum Amount cannot be left blank';
					}else{
						$minamount = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($minamt));
						$tra_min_amount = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT));
						if((int)$minamount>(int)$tra_min_amount){
												$this->view->error = true;
												$confirmPage = false;
												$error_msg[] = 'Minimum Amount cannot be lower than IDR '.$minamt;
						}
					}
					
					
					
					$settingObj = new Settings();
					$COT_SKN			= $settingObj->getSetting("cut_off_time_skn"	, "00:00:00");
					$COT_RTGS			= $settingObj->getSetting("cut_off_time_rtgs", "00:00:00");
					
//					$this->view->error = true;
					//var_dump($TrfDateType);die;
					if ($TrfDateType == 2) {
					$efDate = $this->_getParam('PS_FUTUREDATE');
					$session = $this->_getParam('PS_EFTIMEF');
					$this->view->PS_EFTIMEF = $session;
					if (empty($efDate)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Future Date Must Be Selected';
					}
					if (empty($session)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Session Must Be Selected';
					} 
					$sessioncon = $session.':00';
					//var_dump($session);
					//var_dump($COT_SKN);  
					//var_dump(strtotime($session.':00'));
					//var_dump(strtotime($COT_SKN));
					if (strtotime($session.':00') > strtotime($COT_SKN)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Time greater than Cut Off Time SKN';
					}
					
				} else if ($TrfDateType == 3) {
					$repetition = $this->_getParam('repetition');
					$this->view->repetition = $repetition;
					$efDate = $this->_getParam('PS_EFDATE');
					$PS_EFDATE = $this->_getParam('PS_EFDATE');
					$this->view->efDate = $efDate;
					//daily
					if ($repetition == 1) {
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
						$repeatOn = $this->_getParam('report_day');
						
						$report_day = $this->_getParam('report_day');

							if (!empty($report_day)) {
								foreach ($report_day as $key => $value) {
									if($value != '0' || $value != '6'){
									$this->view->{'check' . $value} = 'checked';
									}
								}
							}
					}
					//weekly
					else if ($repetition == 2) {
						$repeatEvery = 2;
						$repeatOn = $this->_getParam('report_day');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
						
						$report_day = $this->_getParam('report_day');

						if (!empty($report_day)) {
							foreach ($report_day as $key => $value) {
								if($value != '0' || $value != '6'){
									$this->view->{'check' . $value} = 'checked';
								}
							}
						}

						if (empty($repeatEvery)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat Every Must Be Selected';
						}
						if (empty($repeatOn)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat On Be Selected';
						}
					}
					//monthly
					else if ($repetition == 3) {
						$repeatEvery = 3;
						// $repeatOn = $this->_getParam('PS_REPEATON');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

						if (empty($repeatEvery)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat Every Must Be Selected';
						}
						// if (empty($repeatOn)) {
						// 	$this->view->error = true;
						// 	$confirmPage = false;
						// 	$error_msg[] = 'Repeat On Be Selected';
						// }
					}
					$TrfDateType = $this->_getParam('tranferdatetype');
					if ($TrfDateType == '3') {
						$session = $this->_getParam('PS_EFTIMEP');
					} else {
						$session = $this->_getParam('PS_EFTIMEF');
					}
					
					$this->view->PS_EFTIMEP = $session;
						//var_dump($TRANSFER_TYPE);
						//var_dump($COT_SKN);
						//var_dump($COT_RTGS);
						//var_dump($session);
					if($TRANSFER_TYPE == 'SKN'){

						if (strtotime($session.':00') > strtotime($COT_SKN)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Time greater than Cut Off Time SKN';
						}
					}else{
						if (strtotime($session.':00') > strtotime($COT_RTGS)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Time greater than Cut Off Time RTGS';
						}
					}
					// var_dump($session);

					if (empty($endDate)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'End Date Must Be Selected';
					}
					if (empty($session)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Session Time Must Be Selected';
					}
					
					$this->view->repeat_day = $repeatOn;
				}
					
				//	var_dump($ACBENEF_CITY);die;
					if($TRANSFER_TYPE == 'SKN'){

						if($ACBENEF_ADDRESS == '' || $ACBENEF_ADDRESS2 == ''){
							$error_msg		 		= 'Beneficiary city can not be left blank.';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
						}
					}
					
					
					if(!$PS_EFDATE){
	
						$error_msg		 		= 'Payment Date can not be left blank.';
						$this->view->error 		= true;
						$this->view->error_msg	= $error_msg;
						
					}elseif($TrfDateType=='3' && $TrfPeriodicType==5 && empty($PERIODIC_EVERY)){
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->error_msg	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					
					}elseif($TrfDateType=='3' && $TrfPeriodicType==6 && empty($PERIODIC_EVERYDATE)){
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->error_msg	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					
					}elseif($TrfDateType=='3' && strtotime($NEXT_DATE) > strtotime($END_DATE)){
						$errorMsg		 		= $this->language->_('End Date must be greater than'). " ". $NEXT_DATE;
						$this->view->error 		= true;
						$this->view->error_msg	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
	
					}elseif(empty($ACBENEF_EMAIL)){
						//var_dump($ACCTSRC);
						//var_dump($ACBENEF);die;
						if($ACCTSRC == "" || $ACBENEF == "" || $TRA_AMOUNT_num == ""){ 
							//die('get');
							$error_msg		 		= 'Mandatory field cannot be empty.';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
						}else{
							$error_msg		 		= 'Email can not be left blank.';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
						}
					}elseif(!filter_var($ACBENEF_EMAIL, FILTER_VALIDATE_EMAIL)){
						$error_msg		 		= 'Invalid email format';
						$this->view->error 		= true;
						$this->view->error_msg	= $error_msg;
					}
					else{
						
						
						//die('gere');
						$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
						if(!$validateDateFormat->isValid($PS_EFDATE))
						{
							$error_msg = 'Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
	
						}
						else{
							$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
							$sessionNameConfrim->benefAcct = $ACBENEF;
							$sessionNameConfrim->sourceAcct = $ACCTSRC;
							$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;		
							$sessionNameConfrim->traMinAmount = $TRA_MIN_AMOUNT_num;

							$paramPayment = array(	"CATEGORY" 					=> "OPEN SWEEP",
													"FROM" 						=> "F",				// F: Form, I: Import
													"PS_NUMBER"					=> $PS_NUMBER,
													"PS_SUBJECT"				=> $PS_SUBJECT,
													"PS_EFDATE"					=> $PS_EFDATE,
													"_dateFormat"				=> $this->_dateDisplayFormat,
													"_dateDBFormat"				=> $this->_dateDBFormat,
													"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
													"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
													"_createPB"					=> false,								// cannot create PB trx
													"_createDOM"				=> $this->view->hasPrivilege('CDFT'),	// privi CDFT (Create Domestic Fund Transfer)
													"_createREM"				=> false,								// cannot create REM trx
												 );
												

												if(empty($CITY_CODE)){
													$CITY_CODE = $ACBENEF_CITY;
												}

												

												if(empty($LLD_BENEIDENTIF)){
													$LLD_BENEIDENTIF = $ACBENEF_IDTYPE;
												}


												if(empty($LLD_CATEGORY)){
													$LLD_CATEGORY = $ACBENEF_CATEGORY;
												}

												if(empty($ACBENEF_CITIZENSHIP)){
													$ACBENEF_CITIZENSHIP = $ACBENEF_CITIZEN;
												}
												
							$paramTrxArr[0] = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
													"TRA_AMOUNT" 				=> $sessionNameConfrim->traAmount,
													"TRA_MIN_AMOUNT" 			=> $sessionNameConfrim->traMinAmount,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_REFNO" 				=> $TRA_REFNO,
													"ACCTSRC" 					=> $sessionNameConfrim->sourceAcct,
													"ACBENEF" 					=> $sessionNameConfrim->benefAcct,
													"ACBENEF_CCY" 				=> $ACBENEF_CCY,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
													"ACCTSRC_BANKCODE"			=> $ACCTSRC_BANKCODE,
												// for Beneficiary data, except (bene CCY and email), must be passed by reference
													"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
													"ReffId" 					=> &$ReffId,
													"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
	 												"ACBENEF_CITIZENSHIP" 		=> &$ACBENEF_CITIZENSHIP,		// W:WNI, N: WNA
													"ACBENEF_RESIDENT" 			=> &$ACBENEF_RESIDENT,			// 1/R: RESIDENT, 2/NR: NON-RESIDENT
													"ACBENEF_ADDRESS1" 			=> &$ACBENEF_ADDRESS,
													"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,
	
												//	"ORG_DIR" 					=> $ORG_DIR,
													"BANK_CODE" 				=> $BANK_CODE,
												//	"BANK_NAME" 				=> $BANK_NAME,
												//	"BANK_BRANCH" 				=> $BANK_BRANCH,
												//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
												//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
												//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
	
													"LLD_IDENTICAL" 			=> $LLD_IDENTICAL,
												//	"LLD_CITIZENSHIP"			=> $LLD_CITIZENSHIP,
													"LLD_CATEGORY" 				=> $LLD_CATEGORY,
													"LLD_RELATIONSHIP" 			=> $LLD_RELATIONSHIP,
													"LLD_PURPOSE" 				=> $LLD_PURPOSE,
													"LLD_DESCRIPTION" 			=> $LLD_DESCRIPTION,
													"LLD_BENEIDENTIF" 			=> $LLD_BENEIDENTIF,
													"LLD_BENENUMBER" 			=> $LLD_BENENUMBER,
													"LLD_SENDERIDENTIF" 		=> $LLD_SENDERIDENTIF,
													"LLD_SENDERNUMBER" 			=> $LLD_SENDERNUMBER,
													"CITY"						=> $CITY,
													"CITY_CODE" 				=> $CITY_CODE, //yang akan dikirimkan ke vlink field 3
													"SWEEP_TYPE"				=> $SWEEP_TYPE,
													"SWEEP_SPLIT"				=> $SWEEP_SPLIT
													//"PS_EMAIL"					=> $TRA_EMAIL,
													//"PS_NOTIF"					=> $TRA_NOTIF,
													//"PS_SMS"					=> $TRA_SMS,
												 );

							
	
	
							if($isConfirmPage != 1){
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME = $paramTrxArr[0]['ACBENEF_BANKNAME'];
							}
							else{
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME;		
							}

							if(empty($sessionNameConfrim->SAVEBENE)){
								$sessionNameConfrim->SAVEBENE = $SAVEBENE;
							}
							//echo '<pre>';
							//var_dump($this->_request->getParams());
							 //var_dump($paramPayment);
							 //var_dump($paramTrxArr);die;
							$resWs = array();
							$validate   = new ValidatePaymentOpen($this->_custIdLogin, $this->_userIdLogin);

							if(empty($TRANSFER_TYPE) && $this->view->error == false)
								$validate->setFlagConfirmPage(($isConfirmPage == 1)?TRUE:FALSE); //tujuan untuk set supaya jangan manggil inquiry lg di page ke 2

							//c check account inquiry
							// echo "<pre>";
							// var_dump($account);die;

							//define clientuser object
							$clientUser  =  new SGO_Soap_ClientUser();
							// echo "<pre>";
							// ;
							// if(empty($this->_request->getParam('TRANSFER_TYPE')))
							//supaya di confirm page tidak terpanggil lagi
							if ($isConfirmPage != 1 && !empty($this->_request->getParam('TRANSFER_TYPE'))) {
								if ($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS') {
									//var_dump($BANK_CODE)
									foreach ($databank as $key => $value) {
										if($value['BANK_CODE'] == $BANK_CODE){
											$BANK_NAME = $value['BANK_NAME'];
										}
									}
									$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
									$sessionNameConfrim->swift_code = $SWIFT_CODE;
									$sessionNameConfrim->ACCTSRCTEXT = $ACCTSRCTEXT;
									$sessionNameConfrim->traAmount	   = $TRA_AMOUNT_num;
									$sessionNameConfrim->traMinAmount	   = $TRA_MIN_AMOUNT_num;
									$sessionNameConfrim->acct_numb = $ACCTSRC;
									$sessionNameConfrim->acct_bank = $ACCTSRC_BANKCODE;
									$sessionNameConfrim->bene_numb = $ACBENEF;
									$sessionNameConfrim->bene_bank = $ACBENEF_BANKNAME;
									$sessionNameConfrim->bene_address = $ACBENEF_ADDRESS;
									$sessionNameConfrim->bene_address2 = $ACBENEF_ADDRESS2;
									$sessionNameConfrim->citizenship = $ACBENEF_RESIDENT;
									$sessionNameConfrim->nationality = $ACBENEF_CITIZENSHIP;
									$sessionNameConfrim->bene_category = $LLD_CATEGORY;
									$sessionNameConfrim->bene_idtype = $LLD_BENEIDENTIF;
									$sessionNameConfrim->bene_idnum = $LLD_BENENUMBER;
									$sessionNameConfrim->bene_city = $CITY_CODE;
									$sessionNameConfrim->bene_phone = $ACBENEF_PHONE;
									$sessionNameConfrim->bank_code = $BANK_CODE;
									$sessionNameConfrim->desc	   = $TRA_MESSAGE;
									$sessionNameConfrim->bene_name = $ACBENEF_BANKNAME;
									$sessionNameConfrim->bank_name = $BANK_NAME;

									$sessionNameConfrim->bene_email = $ACBENEF_EMAIL;

									$sessionNameConfrim->lld_identical = $LLD_IDENTICAL;
									$sessionNameConfrim->lld_relation	   = $LLD_RELATIONSHIP;
									$sessionNameConfrim->lld_purpose = $LLD_PURPOSE;
									$sessionNameConfrim->lld_desc = $LLD_DESCRIPTION;
									
									
									//$isConfirmPage = true;
								}

								//validate
								$resWs = array();
								//echo '<pre>';
								//var_dump($paramPayment);
								//var_dump($paramTrxArr);die;
								$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);
								//echo '<pre>';
								//var_dump($resultVal);
								//var_dump($resWs);
								//var_dump($paramPayment);
								//var_dump($paramTrxArr);die; 
								if (!$resultVal) {
									//die('asf');
									$isConfirmPage = false;
									//$this->view->error = true;
									$errorMsg 		= $validate->getErrorMsg();								
									$this->view->error_msg = $errorMsg;
								}
								else{
									//$isConfirmPage = true;
								}


								if($this->view->error){
									$isConfirmPage = false;
								}
							}
						
							//simpan session utk dipakai saat back
							$data['PS_SUBJECT'] = $PS_SUBJECT;
							$data['ACCTSRC'] = $ACCTSRC;
							$data['ACCTSRC_BANKCODE'] = $ACCTSRC_BANKCODE;
							$data['TRA_MIN_AMOUNT'] = $TRA_MIN_AMOUNT_num;
							$data['TRA_AMOUNT'] = $TRA_AMOUNT_num;
							$data['BANK_CODE'] = $BANK_CODE;
							
							$data['ACCTSRCTEXT'] = $ACCTSRCTEXT;
							$data['ACCT_BANK'] = $ACCTSRC_BANKCODE;
							$data['BENEFICIARY_BANK_CODE'] = $BANK_CODE;
							$data['BANK_CITY'] = $CITY_CODE;
							
							$data['CITY'] = $CITY;
							$data['PS_EFTIMEF'] = $session;  
							$data['PS_EFTIMEP'] = $session;
							$data['sweep_split'] = $SWEEP_SPLIT;
							
							$data['BANK_NAME'] = $BANK_NAME; 
							$data['CLR_CODE'] = $CLR_CODE;
							$data['SWIFT_CODE'] = $SWIFT_CODE;
							$data['CITY_CODE'] = $CITY_CODE;
							$data['ACBENEF'] = $ACBENEF;
							$data['ACBENEF_BANKNAME'] = $ACBENEF_BANKNAME;
							$data['ACBENEF_ADDRESS'] = $ACBENEF_ADDRESS;
							$data['ACBENEF_ADDRESS2'] = $ACBENEF_ADDRESS2;
							$data['ACBENEF_RESIDENT'] = $ACBENEF_RESIDENT;
							$data['ACBENEF_CITIZEN'] = $ACBENEF_CITIZEN;
							$data['ACBENEF_CATEGORY'] = $ACBENEF_CATEGORY;
							$data['ACBENEF_IDTYPE'] = $ACBENEF_IDTYPE;
							$data['LLD_BENENUMBER'] = $LLD_BENENUMBER;
							$data['ACBENEF_PHONE'] = $ACBENEF_PHONE;
							$data['ACBENEF_EMAIL'] = $ACBENEF_EMAIL;
							$data['TRANSFER_TYPE'] = $TRANSFER_TYPE;
							$data['TRA_MESSAGE'] = $TRA_MESSAGE;
							$data['PS_EFDATEFUTURE'] = $PS_EFDATE ;
							$data['TrfDateType'] = $tranferdatetype;
							

							$data['LLD_IDENTICAL'] = $LLD_IDENTICAL;
							$data['LLD_RELATIONSHIP'] = $LLD_RELATIONSHIP ;
							$data['LLD_PURPOSE'] = $LLD_PURPOSE;
							$data['LLD_DESCRIPTION'] = $LLD_DESCRIPTION;
							
							
							//$sessionNameConfrim->tranferdatetype = $tranferdatetype;
								//	$sessionNameConfrim->ps_efdate = $PS_EFDATE ;
							$sessionNamespace = new Zend_Session_Namespace('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
							$sessionNamespace->data = $data;

							/////////

							// $resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);

							// if($TRANSFER_TYPE != 'ONLINE' && $this->view->error == false){
							// 	$sourceAccountType = $resWs['accountType'];
							// 	$infoWarning = $resWs['infoHoliday']['infoWarning']; //permintaan mayapada hari libur dikasih warning saja
							// 	$reffId = $paramTrxArr[0]['ReffId'];
							// 	if($isConfirmPage != 1){
							// 		$this->view->reffIdSend = $reffId;
							// 		$this->view->sourceAcctTypeGet = $sourceAccountType;
							// 	}
							// 	else{
							// 		$reffIdGet 		= $this->_getParam('reffId');
							// 		$this->view->reffIdSend = $reffIdGet;
									
							// 		$sourceAcctTypeGet 		= $this->_getParam('sourceAcctType');
							// 		$this->view->sourceAcctTypeGet = $sourceAcctTypeGet;
							// 	}
							// }
							// var_dump($this->_request->getParam('TRANSFER_TYPE'));die;

							//if($this->_request->getParam('ERROR_AUTH')){
							//	$this->view->error == true;
						//		$isConfirmPage = false;
						//		$this->view->inquiry_error = 'Invalid Data';							
						//	} 

							$confirmBtn = $this->_getParam('submit');
							//echo '<pre>';
							//var_dump($error_msg);
							//var_dump($this->_request->getParams());
							
							//var_dump($this->view->error);echo '<-';
							//var_dump($confirmBtn);
							if($this->view->error == false && ($confirmBtn == 'Submit' || $confirmBtn == 'Confirm'))	// payment data is valid
							{
								$confirmPage = $this->_getParam('confirmPage');
								//var_dump($confirmPage);
								//die('here');
								
								$payment 		= $validate->getPaymentInfo();
								
								//echo '<pre>';
							//var_dump($this->_request->getParams());
							//var_dump($this->view->error);
								if($confirmPage == '0'){
									$confirmPage = false;
								}
								if($isConfirmPage == '0'){
									$isConfirmPage = false;
								}
								//echo 'confirm :';
								//var_dump($isConfirmPage);
								//var_dump($USE_CONFIRM_PAGE);
								//var_dump($confirmPage); 
								//die('here'); 
								
								//if($confirmPage)
								if ( $isConfirmPage == false && $confirmPage == false)
								{
									// echo "<pre>";
									 //var_dump($sessionNameConfrim);die;
									// foreach ($AccArr as $key => $value) {
										// 
									// }
									$isConfirmPage 	= true;
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
									$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
									
									if($TRANSFER_TYPE == 'ONLINE')
										$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$ACCTSRC]["ACCT_ALIAS"];
									else
										$ACCTSRC_ALIAS = "";

									$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
									$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);
	
									if (!$resultVal) {
									//die('asf');
										$isConfirmPage = false;
										$this->view->error = true;
										$errorMsg 		= $validate->getErrorMsg();								
										$this->view->error_msg = $errorMsg;
									}
	
									$validate->__destruct();
									unset($validate);
	
									require_once 'General/Charges.php';
									$trfType		= $TRANSFER_TYPE;
									$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
									$paramCharges 	= array("accsrc" => $sessionNameConfrim->sourceAcct, "transferType" => $trfType);

									if($TRANSFER_TYPE == "ONLINE")
										$chargesAMT = 0;
									else
										$chargesAMT 	= $chargesObj->getCharges($paramCharges);

									$chargesCCY 	= $ACCTSRC_CCY;
								}
								else
								{
									
									$validate->__destruct();
									unset($validate);
	
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
	
									$param = array();
									$param['PS_SUBJECT'] 					= $this->_getParam('PS_SUBJECT');
									$param['PS_CCY'] 						= 'IDR';
									$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
									$param['TRA_AMOUNT'] 					= Application_Helper_General::convertDisplayMoney($this->_getParam('TRA_AMOUNT'));
									$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;
									$param['PS_EFTIME']  					= $session;
									

									$acct_name = '';
									$acct_ccy = '';
									$acct_bankcode = '';
									foreach ($account as $key => $value) {
										if($value['account_number'] == $this->_getParam('ACCTSRC')){
											$acct_name = $value['account_name'];
											$acct_ccy = (!empty($value['account_currency']) ? $value['account_currency'] : 'IDR');
											$acct_bankcode = $value['BANK_CODE'];
										}
									}
									
									$param['SOURCE_ACCOUNT'] 				= $this->_getParam('ACCTSRC');
									$param['SOURCE_ACCOUNT_NAME']			= $acct_name;
									$param['SOURCE_ALIAS_NAME']				= '-';
									$param['SOURCE_ACCOUNT_CCY']			= $acct_ccy;
									$param['SOURCE_ACCT_BANK_CODE']			= $this->_getParam('sourceBankCode');
									$param['BENEFICIARY_ACCOUNT'] 			= $sessionNameConfrim->benefAcct;
									$param['BENEF_ACCT_BANK_CODE'] 			= $sessionNameConfrim->bank_code;
									$param['BENEFICIARY_ACCOUNT_CCY'] 		= $ACBENEF_CCY;
									$param['BENEFICIARY_ACCOUNT_NAME'] 		= $ACBENEF_BANKNAME;
									$param['BENEFICIARY_ALIAS_NAME'] 		= '-';
									$param['BENEFICIARY_BANK_NAME'] 		= $BANK_NAME;
									$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;
									$param['PHONE_NUMBER'] 					= $ACBENEF_PHONE;
									//tiara
									$param['BENEFICIARY_BANK_CITY'] 		= $sessionNameConfrim->bene_city;
									$param['BENEFICIARY_ADDRESS'] 			= $ACBENEF_ADDRESS;
									$param['BENEFICIARY_ADDRESS2'] 			= $ACBENEF_ADDRESS2;


									$param['ACBENEF_CITIZEN'] 				= $sessionNameConfrim->nationality; //nationality
									$param['ACBENEF_RESIDENT'] 				= $sessionNameConfrim->citizenship; //citizenship
									$param['ACBENEF_CATEGORY'] 				= $sessionNameConfrim->bene_category;
									$param['ACBENEF_IDTYPE'] 				= $sessionNameConfrim->bene_idtype;
									$param['ACBENEF_IDNUM'] 				= $sessionNameConfrim->bene_idnum;

									$param['LLD_DESCRIPTION'] 				= $sessionNameConfrim->lld_desc;
									$param['LLD_IDENTICAL'] 				= $sessionNameConfrim->lld_identical;
									$param['LLD_RELATIONSHIP'] 				= $sessionNameConfrim->lld_relation;
									$param['LLD_PURPOSE'] 					= $sessionNameConfrim->lld_purpose;

									$param['PS_CATEGORY']					= 'SWEEP PAYMENT';
									$param['PS_TYPE']						= '23';

						

									$param['TRANSFER_TYPE']		 			= $TRANSFER_TYPE;
									$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
									$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
									
									$param['TRANS_TYPE']					= $TRANSFER_TYPE;
									
									$param['TRA_MIN_AMOUNT']				= Application_Helper_General::convertDisplayMoney($this->_getParam('TRA_MIN_AMOUNT'));
									$param['SWEEP_SPLIT']					= $SWEEP_SPLIT;


									$param['SWIFT_CODE']					= $SWIFT_CODE;
									$param['CLR_CODE']						= $CLR_CODE;
				
								//var_dump($param);die;
									// foreach ($account as $key => $value) {
									// 			if($value['account_number'] == $sessionNameConfrim->acct_numb){
									// 				$acct_name = $value['account_name'];
									// 			}
									// 		}
									// 		if(empty($acct_name)){
									// 			$acct_name = '';
									// 		}

									 //var_dump($this->_custSameUser);die;
									if($this->_custSameUser){


										if(!$this->view->hasPrivilege('PRLP')){
											// die('here');
											
											$errMessage = $this->language->_("Error: You don't have privilege to release payment");
											$this->view->error = true;
											$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
										}else{
											
											///google auth
											$challengeCode		= $this->_getParam('challengeCode');

											$inputtoken1 		= $this->_getParam('inputtoken1');
											$inputtoken2 		= $this->_getParam('inputtoken2');
											$inputtoken3 		= $this->_getParam('inputtoken3');
											$inputtoken4 		= $this->_getParam('inputtoken4');
											$inputtoken5 		= $this->_getParam('inputtoken5');
											$inputtoken6 		= $this->_getParam('inputtoken6');

											$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;


											$select3 = $this->_db->select()
												 ->from(array('C' => 'M_USER'));
											$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
											// $select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
											$data2 = $this->_db->fetchRow($select3);

											// $code = $param['googleauth'];
		

											$pga = new PHPGangsta_GoogleAuthenticator();
									    	 //var_dump($data2['GOOGLE_CODE']);
									    	 //var_dump($code);
									    	 //print_r($responseCode);die();
											
											$setting 		= new Settings();
											$google_duration 	= $setting->getSetting('google_duration');
									        if($pga->verifyCode($data2['GOOGLE_CODE'], $responseCode, $google_duration))
									        {
									        	$resultToken = $resHard['ResponseCode'] == '0000';
									        	$tokenAuth = true;
									        }else{
									        	$tokenAuth = false;	
									        }
											
											if($tokenAuth){
												$param['HISTORY_STATUS'] = 1;
												$param['PS_STATUS'] = 7;
											
											  if($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS'){
													 //echo '<pre>';
													 //var_dump($this->_request->getParams());
													 //print_r($param);die;

												//	if ($sourceType == 1) {
														$TrfDateType = $this->_getParam('tranferdatetype');
														//var_dump($TrfDateType);
															if ($TrfDateType == 2) {
																$efDate = $this->_getParam('PS_FUTUREDATE');
																//var_dump($efDate);
																$session = $this->_getParam('session_inp');

																$nextDate = $efDate;
																$repetition = 0;
															}

															//periodic
															else if ($TrfDateType == 3) {
																$repetition = $this->_getParam('repetition');
																//var_dump($repetition);die;
																$this->view->repetition = $repetition;
																//daily
																if ($repetition == 1) {
														// var_dump($TrfDateType);die('he');
														$repeatOn = $this->_getParam('report_day');
														$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
														
														$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																	$nextDate = $nextDate->format('Y-m-d');

																	$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
//var_dump($datenumb);die;												
																		foreach ($dataday as $key => $value) {
																			if($value== '0' || $value == '6'){
																					unset($dataday[$key]);
																			}
																		}
																			//var_dump($efDate);
																			//var_dump($dataday);
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value];
																				//var_dump($string);die;
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																				
																				$nextDatenow = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDatenow = $nextDatenow->format('Y-m-d');
																				$datenumb = date("w", strtotime($nextDatenow));
																				//var_dump($nextDatenow);
																				//var_dump($datenumb);
																				//var_dump($value);
																				//echo '-';
																				if ($datenumb == $value) {
																					$nextDateArr[] = $nextDatenow;
																				//	var_dump($nextDate);die('1');
																					break;

																				}
																				$nextDateArr[] = $nextDate;
																				//var_dump($nextDate);die('2');
																			//	break;
																		//	}
																		//	}
																			
																			
																			
																		}
																		//var_dump($nextDateArr);die;
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																//			if(!empty($nextDateArr['1'])){
																	//			$nextDate = $nextDateArr['1'];
																		//	}else{
																				$nextDate = $nextDateArr['0'];
																			//}
																			
																			$param['PS_EFDATE'] = $nextDateArr['0'];
																	}
																	
																			
																	
																
													}
																//weekly
																else if ($repetition == 2) {

																	$repeatEvery = $this->_getParam('repeat_every');
																	$repeatOn = $this->_getParam('report_day');
																	$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);
																		$string = 'next ' . $arrday[$repeatOn];
																				//var_dump($efDate);
																				//var_dump($string);die;
																				if(empty($efDate)){
																					$efDate = $this->_getParam('PS_STARTDATEPERIODIC');
																				}
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																}
																//monthly
																else if ($repetition == 3) {
																	$repeatEvery = $this->_getParam('repeat_every');
																	$repeatOn = $this->_getParam('PS_REPEATON');
																	$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

																	$nextDate = date("Y-m-d", strtotime("+1 month"));
																}
																$session = $this->_getParam('session_inp');
															}
			 
														$START_DATE = join('-', array_reverse(explode('/', date('d/m/Y'))));

													if ($TrfDateType == 2) {
				//										var_dump($efDate);
														$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $efDate);
			//											var_dump($EXPIRY_DATE);die;
														$expDate = $EXPIRY_DATE->format('Y-m-d');

														$NEXT_DATE = DateTime::createFromFormat('d/m/Y', $nextDate);
														$nextDate = $NEXT_DATE->format('Y-m-d');
														// var_dump($nextDate);die;
													} else if ($TrfDateType == 3) {
														// echo 'here';
														$start = $this->_getParam('PS_EFDATE');
														$STR_DATE = DateTime::createFromFormat('d/m/Y', $start);
														$START_DATE = $STR_DATE->format('Y-m-d');


														// $START_DATE = join('-',array_reverse(explode('/',date('d/m/Y'))));

														$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
														$expDate = $EXPIRY_DATE->format('Y-m-d');
													}
													// var_dump($TrfDateType);
													// 	var_dump($START_DATE);die;
													// var_dump($TrfDateType);

													if (!empty($TrfDateType == 3 && !empty($endDate))) {
														$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
														$endDate = $EF_DATE->format('Y-m-d');
														// var_dump($endDate);die;
													}

													if (empty($endDate)) {
														if ($TrfDateType == 2) {

															// $endDate = join('-',array_reverse(explode('/',date('d/m/Y'))));
															$EF_DATE = DateTime::createFromFormat('d/m/Y', $efDate);
															$endDate = $EF_DATE->format('Y-m-d');
														} else {
															$endDate = join('-', array_reverse(explode('/', date('d/m/Y'))));
															$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
															$endDate = $EF_DATE->format('Y-m-d');
														}
													}

													
													// var_dump($START_DATE);
													// var_dump($endDate);die;
													if ($TrfDateType == 3) {
													$insertPeriodic = array(
														'PS_EVERY_PERIODIC' 	=> (int) $repeatEvery,
														'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
														'PS_EVERY_PERIODIC_UOM' => $repetition, 	// 5: every day of, 6: every date of
														'PS_PERIODIC_STARTDATE' => $START_DATE,
														'PS_PERIODIC_ENDDATE'	=> $endDate,
														// 'PS_PERIODIC_NEXTDATE'	=> $nextDate,
														'PS_PERIODIC_NEXTDATE'	=> $param['PS_EFDATE'],
														'PS_PERIODIC_STATUS' 	=> 2,					// 2: INPROGRESS KALO BELUM BERAKHIR, 1: COMPLETE KALO SUDAH HABIS END DATE, 0: CANCEL
														'USER_ID' 				=> $this->_userIdLogin,
														'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
														'SESSION_TYPE'			=> 0,
													);
													

													// echo '<pre>';
													// print_r($insertPeriodic);die();

													$this->_db->insert('T_PERIODIC', $insertPeriodic);
													$psPeriodicID =  $this->_db->lastInsertId();
													}

														//select data source account dari m_apikey
														$sourceList = $this->_db->fetchAll(
															$this->_db->select()
																->from(array('A' => 'M_APIKEY'), array('*'))
																->where("A.VALUE = ? ", $param['SOURCE_ACCOUNT'])
																->where("A.CUST_ID = ? ", $this->_custIdLogin)
																->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
																->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
														);

														foreach ($sourceList as $key => $value) {
															$newSourceList[$value['FIELD']] = $value['VALUE'];
															$newSourceList['bank_code'] = $value['BANK_CODE'];
															$newSourceList['bank_name'] = $value['BANK_NAME'];
														}
														
														if ($TrfDateType == 3) {
														
														//T_PERIODIC_DETAIL
														$insertPeriodicDetail = array(
															'PS_PERIODIC' 				=> $psPeriodicID,
															'SOURCE_ACCOUNT' 			=> $acSource,
															'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
															'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
															'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
															'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
															'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
															'BENEFICIARY_ACCOUNT' 		=> $acBenef,
															'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
															'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
															// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
															'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
															'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
															// 'TRA_AMOUNT' 				=> $remainsBalance,
															'TRA_MESSAGE' 				=> $message,
															'TRA_MESSAGE_ADD'			=> '',
															'PAYMENT_TYPE'				=> 23,
															'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
															'BALANCE_TYPE'				=> 1,  // 1=Fixed Amount;2=Percentage
															'TRA_REMAIN'				=> $remainsBalance
														);
														$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);
			 
														$report_day = $this->_getParam('report_day');
														//var_dump($data);die;
														if (!empty($report_day)) {

															$filter_day = array_unique($report_day);
															foreach ($filter_day as $keyday => $valday) {
																if($valday != '0' && $valday != '6'){
																if($data['POOLING'][$valday] == '' ){
																	$data['POOLING'][$valday] = 0;
																}
																$insertPeriodicday = array(
																	'PERIODIC_ID' => $psPeriodicID,
																	'DAY_ID'		=> $valday,
																	'LIMIT_AMOUNT'		=> $data['POOLING'][$valday]
																);

																$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
																}
															}
														}
														 
														}
													//}
													if ($TrfDateType == 3) {
														$param['PS_PERIODIC'] 			= $psPeriodicID;
													}

													try {
														$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
														//die('here');
														//echo '<pre>';
														//var_dump($param);die;
														$resProvider	 = $sendProvider->createPaymentSknRtgsSweep($param, $msg);
														$success = true;
													} catch (Exception $e) {
														$success = false;
														var_dump($e);die;
													}
												}
												
												if($resProvider){
														$this->view->result = $resProvider;
														Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
												}
											
												
												//$this->setbackURL('/opensweep/others');
												//$this->_redirect('/notification/success/index');
											}
											
											
											
										}

									//if token valid
									
									
									$isConfirmPage = true;	
									$resultPage = (!empty($resultPage) ? false : true);			
									$this->view->success = $success;
									$this->view->success_msg = 'Payment Success';
									$this->view->error_msg = $errorMsg;

								}else{
									// die('gere');

									$param['HISTORY_STATUS'] = 1;
									$param['PS_STATUS'] = 17;
								
								  if($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS'){
										 //echo '<pre>';
										 //var_dump($this->_request->getParams());
										 //print_r($param);die;

								  	//	if ($sourceType == 1) {
											$TrfDateType = $this->_getParam('tranferdatetype');
											//var_dump($TrfDateType);
								  				if ($TrfDateType == 2) {
													$efDate = $this->_getParam('PS_FUTUREDATE');
													//var_dump($efDate);
													$session = $this->_getParam('session_inp');

													$nextDate = $efDate;
													$repetition = 0;
												}

												//periodic
												else if ($TrfDateType == 3) {
													$repetition = $this->_getParam('repetition');
													//var_dump($repetition);die;
													$this->view->repetition = $repetition;
													//daily
													if ($repetition == 1) {
														// var_dump($TrfDateType);die('he');
														$repeatOn = $this->_getParam('report_day');
														$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
														
														$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																	$nextDate = $nextDate->format('Y-m-d');

																	$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
//var_dump($datenumb);die;												
																		foreach ($dataday as $key => $value) {
																			if($value== '0' || $value == '6'){
																					unset($dataday[$key]);
																			}
																		}
																			//var_dump($efDate);
																			//var_dump($dataday);
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value];
																				//var_dump($string);die;
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																				
																				$nextDatenow = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDatenow = $nextDatenow->format('Y-m-d');
																				$datenumb = date("w", strtotime($nextDatenow));
																				//var_dump($nextDatenow);
																				//var_dump($datenumb);
																				//var_dump($value);
																				//echo '-';
																				if ($datenumb == $value) {
																					$nextDateArr[] = $nextDatenow;
																				//	var_dump($nextDate);die('1');
																					break;

																				}
																				$nextDateArr[] = $nextDate;
																				//var_dump($nextDate);die('2');
																			//	break;
																		//	}
																		//	}
																			
																			
																			
																		}
																		//var_dump($nextDateArr);die;
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																			// if(!empty($nextDateArr['1'])){
																				// $nextDate = $nextDateArr['1'];
																			// }else{
																				$nextDate = $nextDateArr['0'];
																			// }
																			
																			$param['PS_EFDATE'] = $nextDateArr['0'];
																	}
																	
																	
																
													}
													//weekly
													else if ($repetition == 2) {

														$repeatEvery = $this->_getParam('repeat_every');
														$repeatOn = $this->_getParam('report_day');
														$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
														$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);
														$string = 'next ' . $arrday[$repeatOn];
																				//var_dump($efDate);
																				//var_dump($string);die;
																				if(empty($efDate)){
																					$efDate = $this->_getParam('PS_STARTDATEPERIODIC');
																				}
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
													}
													//monthly
													else if ($repetition == 3) {
														$repeatEvery = $this->_getParam('repeat_every');
														$repeatOn = $this->_getParam('PS_REPEATON');
														$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

														$nextDate = date("Y-m-d", strtotime("+1 month"));
													}
													$session = $this->_getParam('session_inp');
												}
 
								  			$START_DATE = join('-', array_reverse(explode('/', date('d/m/Y'))));

										if ($TrfDateType == 2) {
	//										var_dump($efDate);
											$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $efDate);
//											var_dump($EXPIRY_DATE);die;
											$expDate = $EXPIRY_DATE->format('Y-m-d');

											$NEXT_DATE = DateTime::createFromFormat('d/m/Y', $nextDate);
											$nextDate = $NEXT_DATE->format('Y-m-d');
											// var_dump($nextDate);die;
										} else if ($TrfDateType == 3) {
											// echo 'here';
											$start = $this->_getParam('PS_EFDATE');
											$STR_DATE = DateTime::createFromFormat('d/m/Y', $start);
											$START_DATE = $STR_DATE->format('Y-m-d');


											// $START_DATE = join('-',array_reverse(explode('/',date('d/m/Y'))));

											$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
											$expDate = $EXPIRY_DATE->format('Y-m-d');
										}
										// var_dump($TrfDateType);
										// 	var_dump($START_DATE);die;
										// var_dump($TrfDateType);

										if (!empty($TrfDateType == 3 && !empty($endDate))) {
											$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
											$endDate = $EF_DATE->format('Y-m-d');
											// var_dump($endDate);die;
										}

										if (empty($endDate)) {
											if ($TrfDateType == 2) {

												// $endDate = join('-',array_reverse(explode('/',date('d/m/Y'))));
												$EF_DATE = DateTime::createFromFormat('d/m/Y', $efDate);
												$endDate = $EF_DATE->format('Y-m-d');
											} else {
												$endDate = join('-', array_reverse(explode('/', date('d/m/Y'))));
												$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
												$endDate = $EF_DATE->format('Y-m-d');
											}
										}

										
										// var_dump($START_DATE);
										// var_dump($endDate);die;
										if ($TrfDateType == 3) {
										$insertPeriodic = array(
											'PS_EVERY_PERIODIC' 	=> (int) $repeatEvery,
											'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
											'PS_EVERY_PERIODIC_UOM' => $repetition, 	// 5: every day of, 6: every date of
											'PS_PERIODIC_STARTDATE' => $START_DATE,
											'PS_PERIODIC_ENDDATE'	=> $endDate,
											// 'PS_PERIODIC_NEXTDATE'	=> $nextDate,
											'PS_PERIODIC_NEXTDATE'	=> $param['PS_EFDATE'],
											'PS_PERIODIC_STATUS' 	=> 2,					// 2: INPROGRESS KALO BELUM BERAKHIR, 1: COMPLETE KALO SUDAH HABIS END DATE, 0: CANCEL
											'USER_ID' 				=> $this->_userIdLogin,
											'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
											'SESSION_TYPE'			=> 0,
										);
										

										// echo '<pre>';
										// print_r($insertPeriodic);die();

										$this->_db->insert('T_PERIODIC', $insertPeriodic);
										$psPeriodicID =  $this->_db->lastInsertId();
										}

											//select data source account dari m_apikey
											$sourceList = $this->_db->fetchAll(
												$this->_db->select()
													->from(array('A' => 'M_APIKEY'), array('*'))
													->where("A.VALUE = ? ", $param['SOURCE_ACCOUNT'])
													->where("A.CUST_ID = ? ", $this->_custIdLogin)
													->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
													->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
											);

											foreach ($sourceList as $key => $value) {
												$newSourceList[$value['FIELD']] = $value['VALUE'];
												$newSourceList['bank_code'] = $value['BANK_CODE'];
												$newSourceList['bank_name'] = $value['BANK_NAME'];
											}
											
											if ($TrfDateType == 3) {
											
											//T_PERIODIC_DETAIL
											$insertPeriodicDetail = array(
												'PS_PERIODIC' 				=> $psPeriodicID,
												'SOURCE_ACCOUNT' 			=> $acSource,
												'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
												'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
												'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
												'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
												'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
												'BENEFICIARY_ACCOUNT' 		=> $acBenef,
												'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
												'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
												// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
												'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
												'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
												// 'TRA_AMOUNT' 				=> $remainsBalance,
												'TRA_MESSAGE' 				=> $message,
												'TRA_MESSAGE_ADD'			=> '',
												'PAYMENT_TYPE'				=> 23,
												'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
												'BALANCE_TYPE'				=> 1,  // 1=Fixed Amount;2=Percentage
												'TRA_REMAIN'				=> $remainsBalance
											);
											$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);
 
											$report_day = $this->_getParam('report_day');
											//var_dump($data);die;
											if (!empty($report_day)) {

												$filter_day = array_unique($report_day);
												foreach ($filter_day as $keyday => $valday) {
													if($valday != '0' && $valday != '6'){
													if($data['POOLING'][$valday] == '' ){
														$data['POOLING'][$valday] = 0;
													}
													$insertPeriodicday = array(
														'PERIODIC_ID' => $psPeriodicID,
														'DAY_ID'		=> $valday,
														'LIMIT_AMOUNT'		=> $data['POOLING'][$valday]
													);

													$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
													}
												}
											}
											 
											}
										//}
										if ($TrfDateType == 3) {
											$param['PS_PERIODIC'] 			= $psPeriodicID;
										}

										try {
											$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
											//die('here');
										//	echo '<pre>';
										//	var_dump($param);die;
											$resProvider	 = $sendProvider->createPaymentSknRtgsSweep($param, $msg);
											$success = true;
										} catch (Exception $e) {
											$success = false;
											var_dump($e);die;
										}
									}

									Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
									$this->setbackURL('/opensweep/others');
									$this->_redirect('/notification/success/index');
								}

								}
							}
							else
							{
								$errorMsg 		= $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
							//	var_dump($errorMsg);
							//	var_dump($errorTrxMsg);
								$validate->__destruct();
								unset($validate);
	
								if(empty($errMessage)){
									$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));	
								}
								$this->view->error_msg  = $error_msg;
								$this->view->error 		= true;
								$this->view->ERROR_MSG	= $errMessage;
							}
						}
					}
				}
				else{
					//tambahn pentest
					$TRA_AMOUNT  		= $this->_getParam('TRA_AMOUNT');
					$TRA_MIN_AMOUNT		= $this->_getParam('TRA_MIN_AMOUNT');
					$PS_EFDATE_ORI_now  		= $this->_getParam('PS_EFDATE');
					if($PS_EFDATE_ORI_now > date('d/m/Y')){
						$TransferDate 	=  "2";
					}else{
						$TransferDate	=  "1";
						$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
					}
					
					$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
					$isConfirmPage 	= false;
				}

			}
			else{
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/opensweep/others');
			}

			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;	
			
			//tambahn pentest
			
			if($tranferdatetype =='1'){
				$this->view->PS_EFDATE 			= $PS_EFDATE;
			}
			elseif ($tranferdatetype =='3'){
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				
				$PS_EFDATE = $PS_EFDATE;//						
				//var_dump($PS_EFDATE);die;
			}
			else{
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;						
			}

		}
		else
		{
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;
		}


		$this->view->infoWarning = $infoWarning;

		$TRA_MESSAGE_len = (isset($TRA_MESSAGE))? strlen($TRA_MESSAGE): 0;
		$TRA_REFNO_len 	 = (isset($TRA_REFNO))  ? strlen($TRA_REFNO)  : 0;

		$TRA_MESSAGE_len = 120 - $TRA_MESSAGE_len;
		$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;

		$settingObj = new Settings();
		$this->view->COT_SKN			= $settingObj->getSetting("cut_off_time_skn"	, "00:00:00");
		$this->view->COT_RTGS			= $settingObj->getSetting("cut_off_time_rtgs", "00:00:00");
		$this->view->COT_BI				= $settingObj->getSetting("cut_off_time_bi", "00:00:00");
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$this->view->AccArr 			= $AccArr;
		$this->view->transferTypeArr 	= array($this->_transfertype["desc"]["SKN"]  => $this->_transfertype["desc"]["SKN"],
												$this->_transfertype["desc"]["RTGS"] => $this->_transfertype["desc"]["RTGS"],
												'ONLINE' => 'ONLINE');
 		$this->view->citizenshipArr 	= $citizenshipArr;
		$this->view->residentArr 		= $residentArr;
		$this->view->lldCategoryArr 	= $lldCategoryArr;
		$this->view->lldIdenticalArr 	= $lldIdenticalArr;
		$this->view->lldRelationshipArr = $lldRelationshipArr;
		$this->view->lldPurposeArr 		= $lldPurposeArr;
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		$this->view->lldSenderIdentifArr  = $lldSenderIdentifArr;
		//$this->view->repetition 		= $repetition;

		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		$this->view->PS_EFDATE 			= $PS_EFDATE;

		$this->view->TransferDate		= (isset($TransferDate))		? $TransferDate			: '1';

		if($PS_NUMBER){
			$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT_num))			? Application_Helper_General::displayMoneyplain($TRA_AMOUNT_num)			: '';
			$this->view->TRA_MIN_AMOUNT 		= (isset($TRA_MIN_AMOUNT_num))			? Application_Helper_General::displayMoneyplain($TRA_MIN_AMOUNT_num)			: $minamt;
		}
		else{
			$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT_num))			? Application_Helper_General::displayMoneyplain($TRA_AMOUNT_num): '';
			$this->view->TRA_MIN_AMOUNT 		= (isset($TRA_MIN_AMOUNT_num))			? Application_Helper_General::displayMoneyplain($TRA_MIN_AMOUNT_num)			: $minamt;
		}


		$this->view->TRA_MESSAGE 		= (isset($TRA_MESSAGE))			? $TRA_MESSAGE			: '';
		$this->view->TRA_REFNO 			= (isset($TRA_REFNO))			? $TRA_REFNO			: '';
		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;
		$this->view->TRA_REFNO_len		= $TRA_REFNO_len;

		$BANKNAME = '';
		$ACCT_NAME = '';
		if(!empty($ACCTSRC)){
			foreach ($AccArr as $key => $value) {
				
				if($value['ACCT_NO'] == $ACCTSRC){
					//var_dump($value);
					$BANKNAME = $value['ACCT_BANK'];
					$ACCT_NAME = $value['ACCT_NAME'];
				}
			}
		} 
		//var_dump($databank);
		
		//foreach ($databank as $key => $value) {
		//							if ($ACCTSRC_BANKCODE == $value['BANK_CODE']) {
										//var_dump($value);
										//$this->CLR_CODE = $value['CLR_CODE'];
										//$this->SWIFT_CODE = $value['SWIFT_CODE'];
			//							$BANK_NAME = $value['BANK_CODE'];
			//						}
			//					}
		

		$this->view->ACCTSRC_view		= (isset($ACCTSRC))		? $ACCTSRC.' ['.$BANKNAME.'] '.$ACCT_NAME			: '';
		
		if (!empty($pslipData))	
		{
			$this->view->ACCTSRC 			= (isset($ACCTSRC))				? $ACCTSRC				: '';
			$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
		}
		else{
			$this->view->ACCTSRC 			= (isset($sessionNameConfrim->sourceAcct))				? $sessionNameConfrim->sourceAcct				: '';
			$this->view->ACBENEF 			= (isset($sessionNameConfrim->benefAcct))				? $sessionNameConfrim->benefAcct				: '';
		}
		
		
		if (!empty($pslipData))	
		{
			$this->view->ACBENEF_BANKNAME	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
		}
		else{
			
			if($tranferdatetype == 3){
				$this->view->ACBENEF_BANKNAME	= $this->_getParam('ACBENEF_BANKNAME');
			}
			else{
				$this->view->ACBENEF_BANKNAME	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
			}
		}

		$this->view->ACCTSRC_BANKCODE 			= (isset($ACCTSRC_BANKCODE))			? $ACCTSRC_BANKCODE			: "";

		$this->view->notif 		= (isset($TRA_NOTIF))			? $TRA_NOTIF			: '';
		$this->view->sms_notif 		= (isset($TRA_SMS))			? $TRA_SMS			: '';
		$this->view->email_notif 		= (isset($TRA_EMAIL))			? $TRA_EMAIL			: '';
		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->ACBENEF_EMAIL 		= (isset($ACBENEF_EMAIL))		? $ACBENEF_EMAIL		: '';
		$this->view->CURR_CODE 			= (isset($ACBENEF_CCY))			? $ACBENEF_CCY			: '';
		$this->view->ACBENEF_ADDRESS 	= (isset($ACBENEF_ADDRESS))		? $ACBENEF_ADDRESS		: '';
		$this->view->ACBENEF_ADDRESS2 	= (isset($ACBENEF_ADDRESS2))		? $ACBENEF_ADDRESS2		: '';
 		$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: '';
 		$this->view->ACBENEF_CITIZEN    = (isset($ACBENEF_CITIZEN))		? $ACBENEF_CITIZEN		: '';
 		$this->view->CITY_CODE			= (isset($CITY_CODE))			? $CITY_CODE	: ''; 
		$this->view->CITY			= (isset($CITY))			? $CITY	: ''; 
 		$this->view->ACBENEF_PHONE			= (isset($ACBENEF_PHONE))			? $ACBENEF_PHONE	: ''; 
			 
		$this->view->ACBENEF_RESIDENT	= (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT		: '';
		$this->view->BANK_NAME			= (isset($BANK_NAME))			? $BANK_NAME			: '';
		$this->view->BANK_CODE 			= (isset($sessionNameConfrim->bank_code))			? $sessionNameConfrim->bank_code			: $BANK_CODE;
		$this->view->SWIFT_CODE 		= (isset($SWIFT_CODE))			? $SWIFT_CODE			: "";

		$this->view->TRANSFER_TYPE 		= (isset($TRANSFER_TYPE))		? $TRANSFER_TYPE		: "SKN";
		$this->view->CLR_CODE 			= (isset($CLR_CODE))			? $CLR_CODE				: "";   // "0140601";  // 0000000
		$this->view->LLD_CATEGORY 		= (isset($LLD_CATEGORY))		? $LLD_CATEGORY			: "";
		$this->view->LLD_IDENTICAL 		= (isset($LLD_IDENTICAL))		? $LLD_IDENTICAL		: "";
		$this->view->LLD_RELATIONSHIP 	= (isset($LLD_RELATIONSHIP))	? $LLD_RELATIONSHIP		: "";
		$this->view->LLD_PURPOSE 		= (isset($LLD_PURPOSE))			? $LLD_PURPOSE			: "";
		$this->view->LLD_DESCRIPTION 	= (isset($LLD_DESCRIPTION))		? $LLD_DESCRIPTION		: "";
		$this->view->LLD_BENEIDENTIF 	= (isset($LLD_BENEIDENTIF))		? $LLD_BENEIDENTIF		: "KTP";
		$this->view->LLD_BENENUMBER 	= (isset($LLD_BENENUMBER))		? $LLD_BENENUMBER		: "";
		$this->view->LLD_SENDERIDENTIF 	= (isset($LLD_SENDERIDENTIF))	? $LLD_SENDERIDENTIF	: "";
		$this->view->LLD_SENDERNUMBER 	= (isset($LLD_SENDERNUMBER))	? $LLD_SENDERNUMBER		: "";
		$this->view->CHARGES_AMT 		= (isset($chargesAMT))			? Application_Helper_General::displayMoney($chargesAMT)	: '';
		$this->view->CHARGES_CCY 		= (isset($chargesCCY))			? $chargesCCY			: '';
			if($isConfirmPage != ''){
					$this->view->confirmPage		= $isConfirmPage;
			}else{
				$this->view->confirmPage		= '0';
			}
			
		$SWEEP_TY = $this->_request->getParam('sweep_split');
		$SWEEP_SPLIT  = (isset($SWEEP_TY))		? $SWEEP_TY		: '1';	
			
		if($SWEEP_SPLIT == '1'){
				$splittext = 'Split the sweep';
			}else{
				$splittext = 'Cancel the sweep';
			}
		$this->view->SWEEP_SPLIT_TEXT 	= $splittext;
		
		$this->view->sweep_split = $SWEEP_SPLIT;
		
		$this->view->resultPage			= $resultPage;
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;
		$this->view->repeatOn			= $repeatOn;
		$this->view->PERIODIC_EVERY 	= (isset($PERIODIC_EVERY))		? $PERIODIC_EVERY		: '0';
		$this->view->PERIODIC_EVERYDATE = (isset($PERIODIC_EVERYDATE))	? $PERIODIC_EVERYDATE	: '0';
		$this->view->TrfDateType 		= (isset($TrfDateType))			? $TrfDateType			: '1';
		$this->view->TrfPeriodicType 	= (isset($TrfPeriodicType))		? $TrfPeriodicType		: ''; //$TrfPeriodicType;
		
		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;

		if($TRANSFER_TYPE != 'ONLINE' && $ACBENEF_RESIDENT != 'R'){
			$this->view->LLD_HI = '-';
		}
		else{
			$this->view->LLD_HI = 'none';
		}
		$ACCT_BANK = $this->_getParam('ACCT_BANK');
		//var_dump($ACCT_BANK);
	 	$this->view->BENEFICIARY_BANK_CODE = $ACCT_BANK;

		if(!$this->_request->isPost()) {
			//data saat confirm
			$sessionNamespace = new Zend_Session_Namespace('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
			$data = $sessionNamespace->data;
			
			//echo '<pre>';
			//var_dump($data); 
			if (!empty($data)) {
				foreach ($data as $key => $value) {
					if($key == 'TRA_AMOUNT'){
							$this->view->$key = Application_Helper_General::displayMoneyplain($value);
					}else if($key == 'ACCTSRCTEXT'){
						$this->view->ACCTSRC_view = $value;
					}else{
							$this->view->$key = $value;
					}
					
				}
				Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
			}
		} 
		

		if ($isConfirmPage == '1')
		{
		}
		else{
			Application_Helper_General::writeLog('CDFT','Viewing Create Single Payment Domestic');
		}








			// $isConfirmPage = $this->_getParam('confirmPage');
			// $confirmPage = true;

			// //if hal pertama di hit, akan msk ke confirm  page
			// if (!$isConfirmPage) {

			// 	$sourceType = $this->_getParam('sourceType');
			// 	$acctBank = $this->_getParam('ACCT_BANK');
			// 	$beneBank = $this->_getParam('BENEF_BANK');

			// 	if (empty($acctBank)) {
			// 		$this->view->error = true;
			// 		$confirmPage = false;
			// 		$error_msg[] = 'Bank cannot be left blank. Please correct it';
			// 		$this->view->error_msg = $error_msg;
			// 	}


			// 	if ($sourceType == 1) { //single source
			// 		$acSource = $this->_getParam('ACCTSRC');
			// 	} else { //upload file
			// 		// echo "<pre>";
			// 		// var_dump($this->_request->getParams());
			// 		$adapter = new Zend_File_Transfer_Adapter_Http();

			// 		$adapter->setDestination($this->_destinationUploadDir);
			// 		$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
			// 		$extensionValidator->setMessage(
			// 			$this->language->_('Error') . ': ' . $this->language->_('Extension file must be') . ' *.txt'
			// 		);

			// 		$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			// 		$sizeValidator->setMessage(
			// 			'Error: File exceeds maximum size'
			// 		);

			// 		$adapter->setValidators(array(
			// 			$extensionValidator,
			// 			$sizeValidator,
			// 		));

			// 		$sourceFileName = substr_replace(basename($adapter->getFileName()), '', 100);

			// 		$extension = explode('.', $sourceFileName);

			// 		$extensionName = $extension[1];

			// 		$newFileName = $adapter->getFileName() . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

			// 		$adapter->addFilter('Rename', $newFileName);
			// 		$error = false;
			// 		// var_dump($newFileName);
			// 		if ($adapter->isValid())
			// 		// if (true)
			// 		{

			// 			if ($adapter->receive()) {

			// 				//               	$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
			// 				// $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

			// 				$adapter->addFilter('Rename', $newFileName);
			// 				// $adapter->receive();
			// 				$myFile = file_get_contents($newFileName);
			// 				$arry_myFile = explode("\n", $myFile);
			// 				@unlink($newFileName);
			// 				// echo "<pre>";

			// 				$datasource = array();
			// 				$sourceno = 0;
			// 				if (!empty($arry_myFile)) {
			// 					// var_dump($arry_myFile);die;
			// 					foreach ($arry_myFile as $key => $value) {
			// 						if ($key == 0) {
			// 							$datafile = explode('|', $value);
			// 							$file_id = $datafile['1'];
			// 							$file_date = $datafile['4'] . '-' . sprintf("%02d", $datafile['3']) . '-' . sprintf("%02d", $datafile['2']);
			// 							$total_trx = $datafile['5'];
			// 							$ccy_trx = $datafile['6'];
			// 						}
			// 						$dataex = explode('|', $value);
			// 						if ($dataex['0'] == '1') {
			// 							$datasource[$sourceno]['ACCT_NO'] = $dataex['1'];
			// 							$datasource[$sourceno]['REMAIN_BALANCE'] = $dataex['2'];
			// 							$datasource[$sourceno]['DESC'] = $dataex['3'];
			// 							$datasource[$sourceno]['CUST_REF'] = $dataex['4'];
			// 							$datasource[$sourceno]['NOTIF_SMS'] = '';
			// 							$datasource[$sourceno]['NOTIF_EMAIL'] = '';
			// 							$sourceno++;
			// 						}
			// 					}

			// 					foreach ($datasource as $key => $value) {
			// 						$checkdata = $this->_db->fetchAll(
			// 							$this->_db->select()
			// 								->from(array('A' => 'M_APIKEY'), array('*'))
			// 								->where("A.VALUE = ? ", $value['ACCT_NO'])
			// 								->where("A.CUST_ID = ? ", $this->_custIdLogin)
			// 							);
			// 						if(empty($checkdata)){
			// 							$error = true;
			// 							$confirmPage = false;
			// 							$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Account is not registered') . '.';
			// 							$this->view->error      = true;	
			// 						}
			// 					}

			// 					if (count($datasource) >= 50) {
			// 						$error = true;
			// 						$confirmPage = false;
			// 						$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Max 50 data') . '.';
			// 						$this->view->error      = true;
			// 					}

			// 					if (count($datasource) != $total_trx) {
			// 						$error = true;
			// 						$confirmPage = false;
			// 						$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('total data file invalid') . '.';
			// 						$this->view->error      = true;
			// 					}
			// 				}

			// 				if ($this->_getParam('accteopen') == 1) {
			// 					$input_file_id = $this->_getParam('FILE_IDBENE');
			// 				} else {
			// 					$input_file_id = $this->_getParam('FILE_ID');
			// 				}

			// 				if (strtoupper($input_file_id) != 'BYPASSIDFU') {
			// 					if (strtoupper($input_file_id) != strtoupper($file_id)) {
			// 						$error = true;
			// 						$confirmPage = false;
			// 						$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File ID or Uploaded Date not matched. Please check your uploaded file') . '.';
			// 						$this->view->error      = true;
			// 					}
			// 				}


			// 				if ($file_date != date('Y-m-d')) {
			// 					$error = true;
			// 					$confirmPage = false;
			// 					$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File ID or Uploaded Date not matched. Please check your uploaded file') . '.';
			// 					$this->view->error      = true;
			// 				}

			// 				// var_dump($arry_myFile);die;
			// 				// $getTypeArr = explode(".", $sourceFileName);
			// 				// $getType = $getTypeArr[1];

			// 				// var_dump($newFileName);

			// 				// $data = $this->_helper->parser->parseCSV($newFileName);

			// 				// @unlink($newFileName);

			// 				$acSource = $datasource;
			// 				$acBenef = $this->_getParam('ACBENEF');
			// 				if (!empty($datasource)) {
			// 					foreach ($datasource as $key => $value) {
			// 						if ($value['ACCT_NO'] == $acBenef) {
			// 							$error = true;
			// 							$this->view->error = true;
			// 							$confirmPage = false;
			// 							$error_msg[] = 'Source Account cannot be same with Beneficiary Account';
			// 						}
			// 					}
			// 				} else //kalo total record = 0
			// 				{
			// 					$error = true;
			// 					$confirmPage = false;
			// 					$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Wrong File Formats or Delimited String') . '.';
			// 					$this->view->error      = true;
			// 				}
			// 			}
			// 		} else {

			// 			$error = true;
			// 			$this->view->error = true;
			// 			$confirmPage = false;
			// 			foreach ($adapter->getMessages() as $key => $val) {
			// 				// var_dump($val);
			// 				if ($key == 'fileUploadErrorNoFile')
			// 					$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File cannot be left blank. Please correct it') . '.';
			// 				else
			// 					$error_msg[] = $val;
			// 				break;
			// 			}
			// 			// die('here');
			// 		}
			// 	}

				// $acBenef = $this->_getParam('ACBENEF');
				// // die('here1');
				// //validasi source account tidak boleh sama dengan beneficiary account
				// //if single source
				// if ($sourceType == 1) {
				// 	if ($acSource == $acBenef) {
				// 		$this->view->error = true;
				// 		$confirmPage = true;
				// 		$error_msg[] = 'Source Account cannot be same with Beneficiary Account';
				// 	}
				// }
				// //if upload
				// else {

				// 	//validasi source account
				// 	$source_error = array();
				// 	// if (!$error) {
				// 	// 	foreach ($acSource as $key => $value) {
				// 	// 		$error_array = $this->validateOpenSourceAccount($value, $acBenef);
				// 	// 		if (!empty($error_array)) {
				// 	// 			array_push($source_error, $error_array);		
				// 	// 		}
				// 	// 	}
				// 	// }
				// }

				// if (!empty($source_error)) {
				// 	$this->view->error = true;
				// 	$confirmPage = true;
				// 	$error_msg[] = 'Source Account Error';
				// 	$this->view->source_error = $source_error;
				// }

				// $paymentReff = $this->_getParam('teamplate_reff');
				// $paymentSubject = $this->_getParam('paymentsubject');
				// $sourceName = $this->_getParam('sourceName');
				// $benefName = $this->_getParam('benefName');
				// $message = $this->_getParam('TRA_MESSAGE');
				// $sweepOption = $this->_getParam('ta_sel');
				// $TrfDateType = $this->_getParam('tranferdatetype');
				// if ($TrfDateType == '2') {
				// 	$trfTime = $this->_getParam('PS_EFTIMEF');
				// } else {
				// 	$trfTime = $this->_getParam('PS_EFTIMEP');
				// }
				// $jam = preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $trfTime);
				// if(!$jam){
				// 		$this->view->error = true;
				// 		$confirmPage = true;
				// 		$error_msg[] = 'Invalid Time Format';
				// }

				// //var_dump($sweepOption);die; 
				
				// //remains balance
				// if ($sweepOption == 1) {
				// 	// die('vas');
				// 	$remainBalanceType = $this->_getParam('rem_per');
				// 	$remainsBalance = $this->_getParam('remainbalance');
				// }
				// //maintain balance
				// else {
				// 	// die('here');
				// 	$maintainbalance =  $this->_getParam('maintainbalance');
					
				// 	$report_day = $this->_getParam('report_day');
					
				// 	$arrday = array(
				// 		'0' => 'sun',
				// 		'1' => 'mon',
				// 		'2' => 'tue',
				// 		'3' => 'wed',
				// 		'4' => 'thu',
				// 		'5' => 'fry',
				// 		'6' => 'sat'

				// 	);
				// 	foreach ($report_day as $key => $value) {
				// 		// var_dump($arrday[$key]);
				// 		$tag = 'pooling_' . $arrday[$key];
				// 		$valueday = $this->_getParam($tag);
				// 		if($valueday == ''){
				// 			$this->view->error = true;
				// 			$confirmPage = false;
				// 			$error_msg[] = 'Maintain balance amount cannot be blank';
				// 			break;
				// 		}
				// 	}
					
					
				// 	$pooling_sun = $this->_getParam('pooling_sun');
				// 	$pooling_mon = $this->_getParam('pooling_mon');
				// 	$pooling_tue = $this->_getParam('pooling_tue');
				// 	$pooling_wed = $this->_getParam('pooling_wed');
				// 	$pooling_thu = $this->_getParam('pooling_thu');
				// 	$pooling_fry = $this->_getParam('pooling_fry');
				// 	$pooling_sat = $this->_getParam('pooling_sat');

				// 	$fore_sun = $this->_getParam('forecast_sun');
				// 	$fore_mon = $this->_getParam('forecast_mon');
				// 	$fore_tue = $this->_getParam('forecast_tue');
				// 	$fore_wed = $this->_getParam('forecast_wed');
				// 	$fore_thu = $this->_getParam('forecast_thu');
				// 	$fore_fry = $this->_getParam('forecast_fry');
				// 	$fore_sat = $this->_getParam('forecast_sat');
				// }

				// //if immediate just pass it, bcs immediate will get current date and time when insert
				// //futuredate
				// if ($TrfDateType == 2) {
				// 	$efDate = $this->_getParam('PS_FUTUREDATE');
				// 	$session = $this->_getParam('PS_EFTIMEF');

				// 	if (empty($efDate)) {
				// 		$this->view->error = true;
				// 		$confirmPage = false;
				// 		$error_msg[] = 'Future Date Must Be Selected';
				// 	}
				// 	if (empty($session)) {
				// 		$this->view->error = true;
				// 		$confirmPage = false;
				// 		$error_msg[] = 'Session Must Be Selected';
				// 	}
				// } else if ($TrfDateType == 3) {
				// 	$repetition = $this->_getParam('repetition');
				// 	$efDate = $this->_getParam('PS_EFDATE');
				// 	//daily
				// 	if ($repetition == 1) {
				// 		$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
				// 	}
				// 	//weekly
				// 	else if ($repetition == 2) {
				// 		$repeatEvery = $this->_getParam('selectrepeat');
				// 		$repeatOn = $this->_getParam('report_day');
				// 		$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

				// 		if (empty($repeatEvery)) {
				// 			$this->view->error = true;
				// 			$confirmPage = false;
				// 			$error_msg[] = 'Repeat Every Must Be Selected';
				// 		}
				// 		if (empty($repeatOn)) {
				// 			$this->view->error = true;
				// 			$confirmPage = false;
				// 			$error_msg[] = 'Repeat On Be Selected';
				// 		}
				// 	}
				// 	//monthly
				// 	else if ($repetition == 3) {
				// 		$repeatEvery = $this->_getParam('selectrepeat');
				// 		// $repeatOn = $this->_getParam('PS_REPEATON');
				// 		$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

				// 		if (empty($repeatEvery)) {
				// 			$this->view->error = true;
				// 			$confirmPage = false;
				// 			$error_msg[] = 'Repeat Every Must Be Selected';
				// 		}
				// 		// if (empty($repeatOn)) {
				// 		// 	$this->view->error = true;
				// 		// 	$confirmPage = false;
				// 		// 	$error_msg[] = 'Repeat On Be Selected';
				// 		// }
				// 	}
				// 	$TrfDateType = $this->_getParam('tranferdatetype');
				// 	if ($TrfDateType == '3') {
				// 		$session = $this->_getParam('PS_EFTIMEP');
				// 	} else {
				// 		$session = $this->_getParam('PS_EFTIMEF');
				// 	}
				// 	// var_dump($session);

				// 	if (empty($endDate)) {
				// 		$this->view->error = true;
				// 		$confirmPage = false;
				// 		$error_msg[] = 'End Date Must Be Selected';
				// 	}
				// 	if (empty($session)) {
				// 		$this->view->error = true;
				// 		$confirmPage = false;
				// 		$error_msg[] = 'Session Time Must Be Selected';
				// 	}
				// }

				// $notif = $this->_getParam('notif');

				// if ($notif == 2) {
				// 	$email_notif = $this->_getParam('email_notif');
				// 	$sms_notif = $this->_getParam('sms_notif');
				// }


				// if (empty($error_msg)) {
				// 	// var_dump($fore_sun);
				// 	// var_dump($pooling_sun);
				// 	// echo "<pre>";
				// 	// var_dump($this->_request->getParams());die;
				// 	$poolling = array(
				// 		'0' => (isset($pooling_sun))			? $pooling_sun			: $fore_sun,
				// 		'1' => (isset($pooling_mon))			? $pooling_mon			: $fore_mon,
				// 		'2' => (isset($pooling_tue))			? $pooling_tue			: $fore_tue,
				// 		'3' => (isset($pooling_wed))			? $pooling_wed			: $fore_wed,
				// 		'4' => (isset($pooling_thu))			? $pooling_thu			: $fore_thu,
				// 		'5' => (isset($pooling_fry))			? $pooling_fry			: $fore_fry,
				// 		'6' => (isset($pooling_sat))			? $pooling_sat			: $fore_sat
				// 	);
				// 	// var_dump($poolling);die;
				// 	$accteopen = $this->_getParam('accteopen');

				// 	$databank = $this->_db->fetchAll(
				// 		$this->_db->select()
				// 			->from(array('A' => 'M_BANK_TABLE'), array('*'))
				// 			->where("A.BANK_CODE = ? ", $acctBank)
				// 	);
				// 	// ->where("A.CUST_ID = ? ", $this->_custIdLogin));

				// 	// acctBank$se

				// 	$report_day = $this->_getParam('report_day');
				// 	$toConfirm = false;
				// 	// $repetition = $this->_getParam('repetition');
				// 	if ($accteopen != '1') {
				// 		// die('here');
				// 		$paramdata = array(
				// 			'SOURCE_TYPE' => $sourceType,
				// 			'ACCT_NO'	=> $acSource,
				// 			'ACCT_NAME'	=> $sourceName,
				// 			'BENEF_NO' 	=> $acBenef,
				// 			'BENEF_NAME' => $benefName,
				// 			'DESC' 		=> $message,
				// 			'DATEE_TYPE' 	=> $TrfDateType,
				// 			'SUBJECT' 		=> $paymentSubject,
				// 			'BALANCE_TYPE' 		=> $remainBalanceType,
				// 			'SWEEP_TYPE'	=> $sweepOption,
				// 			'REMAIN_BALANCE' 		=> $remainsBalance,
				// 			'EFDATE' 		=> $efDate,
				// 			'ENDDATE' 		=> $endDate,
				// 			'REPEAT_DAY'	=> $repeatOn,
				// 			'REPEAT_EVERY'	=> $repeatEvery,
				// 			'REPEAT_EVERY'	=> $repeatEvery,
				// 			'NOTIF' 		=> $notif,
				// 			'EMAIL_NOTIF' 		=> $email_notif,
				// 			'SMS_NOTIF' 		=> $sms_notif,
				// 			'SMS_NOTIF' 		=> $sms_notif,
				// 			'SMS_NOTIF' 		=> $sms_notif,
				// 			'POOLING'		=> $poolling,
				// 			'REPETITION'	=> $repetition,
				// 			'EFTIME'		=> $trfTime,
				// 			'SWEEPIN'		=> $accteopen,
				// 			'BANK_CODE'		=> $acctBank,
				// 			'SWIFT_CODE'	=> $databank['0']['SWIFT_CODE'],
				// 			'CLR_CODE'		=> $databank['0']['CLR_CODE'],
				// 			'REPORT_DAY'	=> $report_day,
				// 			'REPETITION'	=> $repetition,
				// 			'MAINTAIN_BALANCE' => $maintainbalance,
				// 		);
				// 	} else {
				// 		$acSources = $this->_getParam('ACCTSRC');
				// 		$paramdata = array(
				// 			'SOURCE_TYPE' => $sourceType,
				// 			'ACCT_NO'	=> $acSources,
				// 			'ACCT_NAME'	=> $sourceName,
				// 			'BENEF_NO' 	=> $acSource,
				// 			'BENEF_NAME' => $benefName,
				// 			'DESC' 		=> $message,
				// 			'DATEE_TYPE' 	=> $TrfDateType,
				// 			'SUBJECT' 		=> $paymentSubject,
				// 			'BALANCE_TYPE' 		=> $remainBalanceType,
				// 			'SWEEP_TYPE'	=> $sweepOption,
				// 			'REMAIN_BALANCE' 		=> $remainsBalance,
				// 			'EFDATE' 		=> $efDate,
				// 			'ENDDATE' 		=> $endDate,
				// 			'REPEAT_DAY'	=> $repeatOn,
				// 			'REPEAT_EVERY'	=> $repeatEvery,
				// 			'REPEAT_EVERY'	=> $repeatEvery,
				// 			'NOTIF' 		=> $notif,
				// 			'EMAIL_NOTIF' 		=> $email_notif,
				// 			'SMS_NOTIF' 		=> $sms_notif,
				// 			'SMS_NOTIF' 		=> $sms_notif,
				// 			'SMS_NOTIF' 		=> $sms_notif,
				// 			'POOLING'		=> $poolling,
				// 			'REPETITION'	=> $repetition,
				// 			'EFTIME'		=> $trfTime,
				// 			'SWEEPIN'		=> $accteopen,
				// 			'BANK_CODE'		=> $acctBank,
				// 			'SWIFT_CODE'	=> $databank['0']['SWIFT_CODE'],
				// 			'CLR_CODE'		=> $databank['0']['CLR_CODE'],
				// 			'REPORT_DAY'	=> $report_day
				// 		);
				// 		// echo "<pre>";
				// 		// var_dump($paramdata);die;
				// 	}

				// 	$paramdata['databank'] = $databank;
				// 	$paramdata['dataacct'] = $acct;
				// 	// echo "<pre>";
				// 	// var_dump($error_msg);
				// 	// var_dump($this->_request->getParams());

				// 	// var_dump($paramdata);die;
				// 	$sessionNamespace = new Zend_Session_Namespace('confirmSweep');
				// 	$sessionNamespace->content = $paramdata;

				// 	$toConfirm = true;
				// }
	

				// 	$paymentReff = $this->_getParam('teamplate_reff');
				// 	$paymentSubject = $this->_getParam('paymentsubject');
				// 	$sourceName = $this->_getParam('sourceName');
				// 	$benefName = $this->_getParam('benefName');
				// 	$message = $this->_getParam('TRA_MESSAGE');
				// 	$sweepOption = $this->_getParam('ta_sel');
				// 	$TrfDateType = $this->_getParam('tranferdatetype');
				// 	if ($TrfDateType == '2') {
				// 		$trfTime = $this->_getParam('PS_EFTIMEF');
				// 		$this->view->PS_EFTIMEF = $trfTime;
				// 	} else {
				// 		$trfTime = $this->_getParam('PS_EFTIMEP');
				// 		$this->view->PS_EFTIMEP = $trfTime;
				// 	}

				// 	$remainBalanceType = $this->_getParam('rem_per');

				// 	$ACCT_BANK = $this->_getParam('ACCT_BANK');
				// 	$ACCTSRC = $this->_getParam('ACCTSRC');
				// 	$ACBENEF = $this->_getParam('ACBENEF');
				// 	$FILE_ID = $this->_getParam('FILE_ID');
				// 	$TRA_MESSAGE = $this->_getParam('TRA_MESSAGE');
				// 	$PS_EFDATEFUTURE = $this->_getParam('PS_FUTUREDATE');


				// 	$PS_EFDATE = $this->_getParam('PS_EFDATE');

				// 	$repetition = $this->_getParam('repetition');
				// 	if (!empty($repetition)) {
				// 		$repeatstr = 'repetition' . $repetition;
				// 		// var_dump($repeatstr);
				// 		$this->view->repetition = $repetition;
				// 		$this->view->$repeatstr = 'selected';
				// 	}

				// 	$PS_ENDDATEPERIODIC = $this->_getParam('PS_ENDDATEPERIODIC');

				// 	$selectrepeat = $this->_getParam('selectrepeat');
				// 	if (!empty($selectrepeat)) {
				// 		$repeat = 'repeat' . $selectrepeat;
				// 		$this->view->$repeat = 'selected';
				// 	}



				// 	$this->view->PS_EFDATE = $PS_EFDATE;
				// 	$this->view->endDatePeriodic = $PS_ENDDATEPERIODIC;

				// 	$report_day = $this->_getParam('report_day');

				// 	if (!empty($report_day)) {
				// 		foreach ($report_day as $key => $value) {
				// 			$this->view->{'check' . $value} = 'checked';
				// 		}
				// 	}


				// 	// var_dump($PS_EFDATEFUTURE);die;
				// 	// var_dump($this->_getParam('paymentsubject'));
				// 	// var_dump($paymentsubject);die;
				// 	$this->view->FILE_ID = $FILE_ID;
				// 	$this->view->ACCTSRC = $ACCTSRC;
				// 	$this->view->ACBENEF = $ACBENEF;
				// 	$this->view->TRA_MESSAGE = $TRA_MESSAGE;
				// 	$this->view->PS_EFDATEFUTURE = $PS_EFDATEFUTURE;

				// 	$aliassource = '';
				// 	$aliasbenef = '';
				// 	foreach ($acct as $key => $value) {
				// 		if ($value['ACCT_NO'] == $sourceName) {
				// 			$aliassource = $value['ACCT_ALIAS'];
				// 		}

				// 		if ($value['ACCT_NO'] == $benefName) {
				// 			$aliasbenef = $value['ACCT_ALIAS'];
				// 		}
				// 	}
				// 	$bank_name = '';
				// 	foreach ($databank as $key => $value) {
				// 		if ($value['BANK_CODE'] == $ACCT_BANK) {
				// 			$bank_name = $value['BANK_NAME'];
				// 		}
				// 	}
				// 	// var_dump($TrfDateType);
				// 	// $this->view->remainbalance = 
				// 	$this->view->paymentReff = $paymentReff;
				// 	$this->view->paymentsubject = $this->_getParam('paymentsubject');
				// 	$this->view->acSource = $acSource;
				// 	$this->view->sourceName = $sourceName;
				// 	$this->view->acBenef = $acBenef;
				// 	$this->view->benefName = $benefName;
				// 	$this->view->message = $message;
				// 	$this->view->sweepOption = $sweepOption;
				// 	$this->view->TrfDateType = $TrfDateType;
				// 	$this->view->sourceType = $sourceType;
				// 	$this->view->ACCT_BANK = $ACCT_BANK;

				// 	$remainsBalance = $this->_getParam('remainbalance');
				// 	$maintainbalance = $this->_getParam('maintainbalance');
				// 	//remains balance
				// 	$this->view->remainBalanceType = $remainBalanceType;
				// 	// var_dump($remainsBalance);die;
				// 	$this->view->remainsBalance = $remainsBalance;
				// 	$this->view->maintainbalance = $maintainbalance;

				// 	//maintain balance
				// 	$this->view->pooling_sun = $pooling_sun;
				// 	$this->view->pooling_mon = $pooling_mon;
				// 	$this->view->pooling_tue = $pooling_tue;
				// 	$this->view->pooling_wed = $pooling_wed;
				// 	$this->view->pooling_thu = $pooling_thu;
				// 	$this->view->pooling_fry = $pooling_fry;
				// 	$this->view->pooling_sat = $pooling_sat;

				// 	$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
				// 	$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
				// 	$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
				// 	$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
				// 	$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
				// 	$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
				// 	$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);

				// 	//trf date type
				// 	//future date
				// 	$this->view->efDate = $efDate;
				// 	//periodic
				// 	$this->view->repetition = $repetition;
				// 	$this->view->endDate = $endDate;
				// 	$this->view->repeatEvery = $repeatEvery;
				// 	$this->view->repeatOn = $repeatOn;
				// 	$this->view->endDate = $endDate;

				// 	//notif
				// 	$this->view->notif = $notif;
				// 	$this->view->email_notif = $email_notif;
				// 	$this->view->sms_notif = $sms_notif;

				// 	$this->view->session = $session;

				// 	$this->view->TrfDateType 		= (isset($TrfDateType))			? $TrfDateType			: '2';

				// 	$paramBack = array(
				// 		'paymentReff' => $paymentReff,
				// 		'paymentSubject' => $paymentSubject,
				// 		'sourceName'	=> $sourceName,
				// 		'benefName'	=> $benefName,
				// 		'message'	=> $message,
				// 		'sweepOption'	=> $sweepOption,
				// 		'sourceType'	=> $sourceType,
				// 		'TrfDateType'	=> $TrfDateType,
				// 		'trfTime'	=> $trfTime,
				// 		'remainBalanceType'	=> $remainBalanceType,
				// 		'ACCT_BANK'	=> $ACCT_BANK,
				// 		'ACCTSRC'	=> $ACCTSRC,
				// 		'ACBENEF'	=> $ACBENEF,
				// 		'FILE_ID'	=> $FILE_ID,
				// 		'TRA_MESSAGE'	=> $TRA_MESSAGE,
				// 		'PS_EFDATE'	=> $PS_EFDATE,
				// 		'PS_EFDATEFUTURE'	=> $PS_EFDATEFUTURE,
				// 		'repetition'	=> $repetition,
				// 		'PS_ENDDATEPERIODIC'	=> $PS_ENDDATEPERIODIC,
				// 		'selectrepeat'	=> $selectrepeat,
				// 		'report_day'	=> $report_day,
				// 		'FILE_ID'	=> $FILE_ID,
				// 		'remainsBalance'	=> $remainsBalance,
				// 		'maintainbalance'	=> $maintainbalance,
				// 		'pooling_sun'	=> $pooling_sun,
				// 		'pooling_mon'	=> $pooling_mon,
				// 		'pooling_tue'	=> $pooling_tue,
				// 		'pooling_wed'	=> $pooling_wed,
				// 		'pooling_thu'	=> $pooling_thu,
				// 		'pooling_fry'	=> $pooling_fry,
				// 		'pooling_sat'	=> $pooling_sat,
				// 		'efDate'	=> $efDate,
				// 		'endDate'	=> $endDate,
				// 		'repeatEvery'	=> $repeatEvery,
				// 		'repeatOn'	=> $repeatOn,
				// 		'notif'			=> $notif,
				// 		'email_notif'	=> $email_notif,
				// 		'sms_notif'		=> $sms_notif,
				// 		'session'		=> $session
				// 	);

				// 	$sessionNamespace->paramBack = $paramBack;
				// }

				// $this->view->error_msg = $error_msg;

				// //page indicator
				// $this->view->confirmPage = $confirmPage;

				// if ($toConfirm) {
				// 	$this->_redirect('/opensweep/index/confirm');
				// }
			//if submit di confirmpage
	//	}

		// $sessionNamespace = new Zend_Session_Namespace('confirmSweep');
		// $paramBack = $sessionNamespace->paramBack;
		// $back = $sessionNamespace->back;

		// if ($back) {
			
		// 	if ($paramBack['TrfDateType'] == '2') {
		// 		$this->view->PS_EFTIMEF = $paramBack['trfTime'];
		// 	} else {
		// 		$this->view->PS_EFTIMEP = $paramBack['trfTime'];
		// 	}

		// 	if (!empty($paramBack['repetition'])) {
		// 		$repeatstr = 'repetition' . $paramBack['repetition'];
		// 		// var_dump($repeatstr);
		// 		$this->view->repetition = $paramBack['repetition'];
		// 		$this->view->$repeatstr = 'selected';
		// 	}

		// 	if (!empty($paramBack['selectrepeat'])) {
		// 		$repeat = 'repeat' . $paramBack['selectrepeat'];
		// 		$this->view->$repeat = 'selected';
		// 	}

		// 	$this->view->PS_EFDATE = $paramBack['PS_EFDATE'];
		// 	$this->view->endDatePeriodic =  $paramBack['PS_ENDDATEPERIODIC'];

		// 	if (!empty($paramBack['report_day'])) {
		// 		foreach ($paramBack['report_day'] as $key => $value) {
		// 			$this->view->{'check' . $value} = 'checked';
		// 		}
		// 	}

		// 	$this->view->FILE_ID = $paramBack['FILE_ID'];
		// 	$this->view->ACCTSRC = $paramBack['ACCTSRC'];
		// 	$this->view->ACBENEF = $paramBack['ACBENEF'];
		// 	$this->view->TRA_MESSAGE = $paramBack['TRA_MESSAGE'];
		// 	$this->view->PS_EFDATEFUTURE = $paramBack['PS_EFDATEFUTURE'];

		// 	$aliassource = '';
		// 	$aliasbenef = '';
		// 	foreach ($acct as $key => $value) {
		// 		if ($value['ACCT_NO'] == $sourceName) {
		// 			$aliassource = $value['ACCT_ALIAS'];
		// 		}

		// 		if ($value['ACCT_NO'] == $benefName) {
		// 			$aliasbenef = $value['ACCT_ALIAS'];
		// 		}
		// 	}
		// 	$bank_name = '';
		// 	foreach ($databank as $key => $value) {
		// 		if ($value['BANK_CODE'] == $ACCT_BANK) {
		// 			$bank_name = $value['BANK_NAME'];
		// 		}
		// 	}
		// 	// var_dump($TrfDateType);
		// 	// $this->view->remainbalance = 
		// 	$this->view->paymentReff = $paramBack['paymentReff'];
		// 	$this->view->paymentsubject = $paramBack['paymentSubject'];
		// 	$this->view->acSource = $paramBack['ACCTSRC'];
		// 	$this->view->sourceName = $paramBack['sourceName'];
		// 	$this->view->acBenef = $paramBack['ACBENEF'];
		// 	$this->view->benefName = $paramBack['benefName'];
		// 	$this->view->message = $paramBack['message'];
		// 	$this->view->sweepOption = $paramBack['sweepOption'];
		// 	$this->view->TrfDateType = $paramBack['TrfDateType'];
		// 	$this->view->sourceType = $paramBack['sourceType'];
		// 	$this->view->ACCT_BANK = $paramBack['ACCT_BANK'];

		// 	//remains balance
		// 	$this->view->remainBalanceType = $paramBack['remainBalanceType'];
		// 	// var_dump($remainsBalance);die;
		// 	$this->view->remainsBalance = $paramBack['remainsBalance'];
		// 	$this->view->maintainbalance = $paramBack['maintainbalance'];

		// 	//maintain balance
		// 	$this->view->pooling_sun = $paramBack['pooling_sun'];
		// 	$this->view->pooling_mon = $paramBack['pooling_mon'];
		// 	$this->view->pooling_tue = $paramBack['pooling_tue'];
		// 	$this->view->pooling_wed = $paramBack['pooling_wed'];
		// 	$this->view->pooling_thu = $paramBack['pooling_thu'];
		// 	$this->view->pooling_fry = $paramBack['pooling_fry'];
		// 	$this->view->pooling_sat = $paramBack['pooling_sat'];

		// 	$this->view->pooling_sun_view = $this->moneyAliasFormatter($paramBack['pooling_sun']);
		// 	$this->view->pooling_mon_view = $this->moneyAliasFormatter($paramBack['pooling_mon']);
		// 	$this->view->pooling_tue_view = $this->moneyAliasFormatter($paramBack['pooling_tue']);
		// 	$this->view->pooling_wed_view = $this->moneyAliasFormatter($paramBack['pooling_wed']);
		// 	$this->view->pooling_thu_view = $this->moneyAliasFormatter($paramBack['pooling_thu']);
		// 	$this->view->pooling_fry_view = $this->moneyAliasFormatter($paramBack['pooling_fry']);
		// 	$this->view->pooling_sat_view = $this->moneyAliasFormatter($paramBack['pooling_sat']);

		// 	//trf date type
		// 	//future date
		// 	$this->view->efDate = $paramBack['efDate'];
		// 	//periodic
		// 	$this->view->repetition = $paramBack['repetition'];
		// 	$this->view->endDate = $paramBack['endDate'];
		// 	$this->view->repeatEvery = $paramBack['repeatEvery'];
		// 	$this->view->repeatOn = $paramBack['repeatOn'];

		// 	//notif
		// 	$this->view->notif = $paramBack['notif'];
		// 	$this->view->email_notif = $paramBack['email_notif'];
		// 	$this->view->sms_notif = $paramBack['sms_notif'];
		// 	$this->view->session = $paramBack['session'];

		// 	$sessionNamespace->back = false;
		// }
		// else{
		// 	unset($_SESSION['confirmSweep']);
		// }
				
		// //Recrate Ongoing
		// if ($this->_request->getParam('recreate')) {
		// 	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		// 	$filter       = new Application_Filtering();
		// 	$AESMYSQL = new Crypt_AESMYSQL();
		// 	$password = $sessionNamespace->token;
		// 	$PS_NUMBER = urldecode($filter->filter($this->_getParam('recreate'), "recrate"));
		// 	$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

		// 	$recreate  = $this->_db->select()
		// 		->from(array('P' => 'T_PSLIP'))
		// 		->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER')
		// 		->where('P.PS_NUMBER = ?', $PS_NUMBER);
		// 	$recreate = $this->_db->fetchRow($recreate);

		// 	$this->view->paymentsubject = $recreate['PS_SUBJECT'];
		// 	$this->view->ACCT_BANK = $recreate['BENEF_ACCT_BANK_CODE'];
		// 	$this->view->ACBENEF = $recreate['BENEFICIARY_ACCOUNT'];
		// 	$this->view->ACCTSRC = $recreate['SOURCE_ACCOUNT'];
		// 	$this->view->TRA_MESSAGE = $recreate['TRA_MESSAGE'];
			
		// }
	}


	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('confirmSweep');
		$data = $sessionNamespace->content;
		// echo "<pre>";
		// var_dump($this->_request->getParams());
		// var_dump($data);
		// die();
		// every atau every date
		if($this->_custSameUser){
			$this->view->token = true;
		}


		if ($this->_request->isPost()) {

			if ($this->_getParam('back') == true)
			{
				$sessionNamespace->back 	= true;
				$this->_redirect('/opensweep/others');
			}


			$filter = new Application_Filtering();
			$RECURING 				= $filter->filter($this->_request->getParam('tranferdatetype'), "PERIODIC_TYPE"); //trf date type

			/* filter */
			$filterArr = array(
				// 'paymentsubject'    => array('StripTags','StringTrim'),
				'ACCTSRC'    => array('StripTags', 'StringTrim'),
				'ACBENEF'  => array('StripTags', 'StringTrim'),
				'ta_sel'  => array('StripTags', 'StringTrim') // sweep option
			);

			$validators = array(
				// 'paymentsubject' => array('NotEmpty',
				// 				'messages' => array(
				//                                               				$this->language->_('Payment Subject cannot be left blank.')
				// 									)
				// 			),
				'ACCTSRC' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Source Account cannot be left blank.')
					)
				),
				'ACBENEF' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Beneficiary Account cannot be left blank.')
					)
				),
				'ta_sel' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Sweep Option cannot be left blank.')
					)
				),
			);



			$zf_filter = new Zend_Filter_Input($filterArr, $validators, $this->_request->getParams());

			// $param = $this->_getAllParams();
			if ($zf_filter->isValid()) {
				// die('here');
				$paymentReff = $this->_getParam('teamplate_reff');
				$paymentSubject = $this->_getParam('paymentsubject');
				$sourceType = $this->_getParam('sourceType');
				$acSource = $this->_getParam2('ACCTSRC');
				$acBenef = $this->_getParam2('ACBENEF');
				$message = $this->_getParam('TRA_MESSAGE');
				$sweepOption = $this->_getParam('ta_sel');
				$TrfDateType = $this->_getParam('tranferdatetype');

				$accteopen = $this->_getParam('accteopen');


				//remains balance
				if ($sweepOption == 1) {
					$remainBalanceType = $this->_getParam('rem_per');
					$remainsBalance = $this->_getParam('remainbalance');

					$remainsBalanceArr = explode('.', $remainsBalance);
					$remainsBalance = str_replace(',', '', $remainsBalanceArr[0]);
				}
				//maintain balance
				else {
					$pooling_sun = $this->_getParam('pooling_sun');
					$pooling_mon = $this->_getParam('pooling_mon');
					$pooling_tue = $this->_getParam('pooling_tue');
					$pooling_wed = $this->_getParam('pooling_wed');
					$pooling_thu = $this->_getParam('pooling_thu');
					$pooling_fry = $this->_getParam('pooling_fry');
					$pooling_sat = $this->_getParam('pooling_sat');
				}


				//futuredate
				if ($TrfDateType == 2) {
					$efDate = $this->_getParam('PS_FUTUREDATE');
					$session = $this->_getParam('session_inp');

					$nextDate = $efDate;
					$repetition = 0;
				}

				//periodic
				else if ($TrfDateType == 3) {
					$repetition = $this->_getParam('repetition');

					//daily
					if ($repetition == 1) {
						// var_dump($TrfDateType);die('he');
						$repeatOn = $this->_getParam('report_day');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
						$nextDate = date("Y-m-d", strtotime("+1 day"));
						// echo "<pre>";
						// var_dump($repeatOn);die;
						$dataday = $repeatOn;

						$date = new DateTime();
						$arrday = array(
							'0' => 'sunday',
							'1' => 'monday',
							'2' => 'tuesday',
							'3' => 'wednesday',
							'4' => 'thursday',
							'5' => 'friday',
							'6' => 'saturday'

						);
						// Modify the date it contains
						$datenumb = date('w');
						if ($datenumb == 6) {
							$datename = 0;
						} else {
							$datename = $datenumb + 1;
						}
						// var_dump($datename);
						if (!empty($dataday)) {
							$resutnext = 0;
							// echo 'here';
							foreach ($dataday as $key => $value) {
								if ($datename == $value['DAY_ID']) {
									$string = 'next ' . $arrday[$datename];
									// var_dump($string);
									$date->modify($string);

									$NEXT_DATE = $date->format('Y-m-d');

									$resutnext = 1;
								}
							}
							// var_dump($NEXT_DATE);echo 'ge';
							if (!$resutnext) {
								// ;

								$string = 'next ' . $arrday[$dataday['0']['DAY_ID']];
							//	var_dump($string);
								$date->modify($string);
								$nextDate = $date->format('Y-m-d');
							}
						}
						// var_dump($NEXT_DATE);die;
					}
					//weekly
					else if ($repetition == 2) {

						$repeatEvery = $this->_getParam('repeat_every');
						$repeatOn = $this->_getParam('report_day');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

						$nextDate = date("Y-m-d", strtotime("+1 week"));
					}
					//monthly
					else if ($repetition == 3) {
						$repeatEvery = $this->_getParam('repeat_every');
						$repeatOn = $this->_getParam('PS_REPEATON');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

						$nextDate = date("Y-m-d", strtotime("+1 month"));
					}
					$session = $this->_getParam('session_inp');
				}

				$notif = $this->_getParam('notif');

				if ($notif == 2) {
					$email_notif = $this->_getParam('email_notif');
					$sms_notif = $this->_getParam('sms_notif');
				}


				// cek value of remain balance percentage
				$RBVal = 0;
				if ($remainBalanceType == 2) { // jika percentage

					$valx = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($remainsBalance));

					if ($valx > 100) {	// jika lebih dari 100%
						$RBVal = $RBVal + 1;
					} else {
						$RBVal = $RBVal + 0;
					}
				} else {
					$RBVal = 0;
				}

				if ($RBVal > 0) { //jika remain balance percentage lebih dari 100%					
					$errorMsg		 		= $this->language->_('Remain Balance must not grather than 100');
					$this->view->error 		= true;
					$this->view->error_msg	= $errorMsg;
				}

				$this->_db->beginTransaction();



				try {
					//echo '<pre>';
					//var_dump($data);die;
					//periodic
					//T_PERIODIC
					$START_DATE = join('-', array_reverse(explode('/', date('d/m/Y'))));

					if ($data['DATEE_TYPE'] == 2) {
						$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $efDate);
						$expDate = $EXPIRY_DATE->format('Y-m-d');

						$NEXT_DATE = DateTime::createFromFormat('d/m/Y', $nextDate);
						$nextDate = $NEXT_DATE->format('Y-m-d');
						// var_dump($nextDate);die;
					} else if ($data['DATEE_TYPE'] == 3) {
						// echo 'here';
						$start = $this->_getParam('PS_STARTDATEPERIODIC');
						$STR_DATE = DateTime::createFromFormat('d/m/Y', $start);
						$START_DATE = $STR_DATE->format('Y-m-d');


						// $START_DATE = join('-',array_reverse(explode('/',date('d/m/Y'))));

						$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
						$expDate = $EXPIRY_DATE->format('Y-m-d');
					}
					// var_dump($TrfDateType);
					// 	var_dump($START_DATE);die;
					// var_dump($TrfDateType);

					if (!empty($data['DATEE_TYPE'] == 3 && !empty($endDate))) {
						$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
						$endDate = $EF_DATE->format('Y-m-d');
						// var_dump($endDate);die;
					}

					if (empty($endDate)) {
						if ($data['DATEE_TYPE'] == 2) {

							// $endDate = join('-',array_reverse(explode('/',date('d/m/Y'))));
							$EF_DATE = DateTime::createFromFormat('d/m/Y', $efDate);
							$endDate = $EF_DATE->format('Y-m-d');
						} else {
							$endDate = join('-', array_reverse(explode('/', date('d/m/Y'))));
							$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
							$endDate = $EF_DATE->format('Y-m-d');
						}
					}

					// var_dump($START_DATE);
					// var_dump($endDate);die;
					if ($data['DATEE_TYPE'] == 3) {
					$insertPeriodic = array(
						'PS_EVERY_PERIODIC' 	=> (int) $repeatEvery,
						'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
						'PS_EVERY_PERIODIC_UOM' => $repetition, 	// 5: every day of, 6: every date of
						'PS_PERIODIC_STARTDATE' => $START_DATE,
						'PS_PERIODIC_ENDDATE'	=> $endDate,
						'PS_PERIODIC_NEXTDATE'	=> $nextDate,
						'PS_PERIODIC_STATUS' 	=> 2,					// 2: INPROGRESS KALO BELUM BERAKHIR, 1: COMPLETE KALO SUDAH HABIS END DATE, 0: CANCEL
						'USER_ID' 				=> $this->_userIdLogin,
						'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
						'SESSION_TYPE'			=> 0,
					);
					

					// echo '<pre>';
					// print_r($insertPeriodic);die();

					$this->_db->insert('T_PERIODIC', $insertPeriodic);
					$psPeriodicID =  $this->_db->lastInsertId();
					}

					//select data beneficiary dari m_apikey
					$beneList = $this->_db->fetchAll(
						$this->_db->select()
							->from(array('A' => 'M_APIKEY'), array('*'))
							->where("A.VALUE = ? ", $acBenef)
							->where("A.CUST_ID = ? ", $this->_custIdLogin)
							->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
							->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
					);

					foreach ($beneList as $key => $value) {
						$newBeneList[$value['FIELD']] = $value['VALUE'];
						$newBeneList['bank_code'] = $value['BANK_CODE'];
						$newBeneList['bank_name'] = $value['BANK_NAME'];
						$newBeneList['id'] = $value['APIKEY_ID'];
					}


					//if upload then decode json

					if ($sourceType == 1) {
						$acSource = $acSource;
					} else {
						$acSource = json_decode($acSource, 1);
					}

					if ($accteopen == 1) {
						$acctBenef = json_decode($acBenef, 1);
					}
					// var_dump($sourceType);die;
					//if single select
					if ($sourceType == 1) {

						//select data source account dari m_apikey
						$sourceList = $this->_db->fetchAll(
							$this->_db->select()
								->from(array('A' => 'M_APIKEY'), array('*'))
								->where("A.VALUE = ? ", $acSource)
								->where("A.CUST_ID = ? ", $this->_custIdLogin)
								->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
								->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
						);

						foreach ($sourceList as $key => $value) {
							$newSourceList[$value['FIELD']] = $value['VALUE'];
							$newSourceList['bank_code'] = $value['BANK_CODE'];
							$newSourceList['bank_name'] = $value['BANK_NAME'];
						}
						
						if ($data['DATEE_TYPE'] == 3) {
						
						//T_PERIODIC_DETAIL
						$insertPeriodicDetail = array(
							'PS_PERIODIC' 				=> $psPeriodicID,
							'SOURCE_ACCOUNT' 			=> $acSource,
							'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
							'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
							'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
							'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
							'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
							'BENEFICIARY_ACCOUNT' 		=> $acBenef,
							'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
							'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
							// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
							'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
							'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
							// 'TRA_AMOUNT' 				=> $remainsBalance,
							'TRA_MESSAGE' 				=> $message,
							'TRA_MESSAGE_ADD'			=> '',
							'PAYMENT_TYPE'				=> 23,
							'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
							'BALANCE_TYPE'				=> 1,  // 1=Fixed Amount;2=Percentage
							'TRA_REMAIN'				=> $remainsBalance
						);
						$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

						$report_day = $this->_getParam('report_day');
						//var_dump($data);die;
						if (!empty($report_day)) {

							$filter_day = array_unique($report_day);
							foreach ($filter_day as $keyday => $valday) {
								if($data['POOLING'][$valday] == '' ){
									$data['POOLING'][$valday] = 0;
								}
								$insertPeriodicday = array(
									'PERIODIC_ID' => $psPeriodicID,
									'DAY_ID'		=> $valday,
									'LIMIT_AMOUNT'		=> $data['POOLING'][$valday]
								);

								$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
							}
						}
						
						}
					}

					//if data upload
					else {

						//loop data upload source dan select dri db m_apikey
						// echo "<pre>";
						// var_dump($acSource);die;
						//        	echo "<pre>";
						// var_dump($acSource);
						// var_dump($this->_request->getParams());die;

						// var_dump($accteopen);die;
						if ($accteopen == 1) {

							foreach ($acctBenef as $row => $source) {

								$sourceList = $this->_db->fetchAll(
									$this->_db->select()
										->from(array('A' => 'M_APIKEY'), array('*'))
										->where("A.VALUE = ? ", $source['ACCT_NO'])
										->where("A.CUST_ID = ? ", $this->_custIdLogin)
										->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
										->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
								);

								foreach ($sourceList as $key => $value) {
									$newSourceList[$value['FIELD']] = $value['VALUE'];
									$newSourceList['bank_code'] = $value['BANK_CODE'];
									$newSourceList['bank_name'] = $value['BANK_NAME'];
								}
								if ($data['DATEE_TYPE'] == 3) {
								//T_PERIODIC_DETAIL
								$insertPeriodicDetail = array(
									'PS_PERIODIC' 				=> $psPeriodicID,
									'SOURCE_ACCOUNT' 			=> $acSource,
									'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
									'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
									'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
									'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
									'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
									'BENEFICIARY_ACCOUNT' 		=> $source['ACCT_NO'],
									'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
									'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
									// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
									'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
									'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
									// 'TRA_AMOUNT' 				=> $remainsBalance,
									'TRA_MESSAGE' 				=> $message,
									'TRA_MESSAGE_ADD'			=> '',
									'PAYMENT_TYPE'				=> 23,
									'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
									'BALANCE_TYPE'				=> 1,  // 1=Fixed Amount;2=Percentage
									'TRA_REMAIN'				=> $source['REMAIN_BALANCE']
								);
								$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

								$report_day = $this->_getParam('report_day');

								if (!empty($report_day)) {
									$filter_day = array_unique($report_day);
									foreach ($report_day as $keyday => $valday) {
										
										if($valday != '0' || $valday != '6'){
									
										$insertPeriodicday = array(
											'PERIODIC_ID' => $psPeriodicID,
											'DAY_ID'		=> $valday,
											'LIMIT_AMOUNT'		=> $data['POOLING'][$valday]
										);

										$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
										}
									}
								}
								
								}
								//reset array
								unset($newSourceList);
							}
						} else {

							foreach ($acSource as $row => $source) {

								$sourceList = $this->_db->fetchAll(
									$this->_db->select()
										->from(array('A' => 'M_APIKEY'), array('*'))
										->where("A.VALUE = ? ", $source['ACCT_NO'])
										->where("A.CUST_ID = ? ", $this->_custIdLogin)
										->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
										->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
								);

								foreach ($sourceList as $key => $value) {
									$newSourceList[$value['FIELD']] = $value['VALUE'];
									$newSourceList['bank_code'] = $value['BANK_CODE'];
									$newSourceList['bank_name'] = $value['BANK_NAME'];
								}
			
								if ($data['DATEE_TYPE'] == 3) {
								//T_PERIODIC_DETAIL
								$insertPeriodicDetail = array(
									'PS_PERIODIC' 				=> $psPeriodicID,
									'SOURCE_ACCOUNT' 			=> $source['ACCT_NO'],
									'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
									'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
									'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
									'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
									'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
									'BENEFICIARY_ACCOUNT' 		=> $acBenef,
									'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
									'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
									// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
									'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
									'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
									// 'TRA_AMOUNT' 				=> $remainsBalance,
									'TRA_MESSAGE' 				=> $message,
									'TRA_MESSAGE_ADD'			=> '',
									'PAYMENT_TYPE'				=> 23,
									'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
									'BALANCE_TYPE'				=> 1,  // 1=Fixed Amount;2=Percentage
									'TRA_REMAIN'				=> $source['REMAIN_BALANCE']
								);
								$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

								$report_day = $this->_getParam('report_day');

								if (!empty($report_day)) {

									$filter_day = array_unique($report_day);
									foreach ($filter_day as $keyday => $valday) {
										
										if($valday != '0' || $valday != '6'){
										$insertPeriodicday = array(
											'PERIODIC_ID' => $psPeriodicID,
											'DAY_ID'		=> $valday,
											'LIMIT_AMOUNT'		=> $data['POOLING'][$valday]
										);

										$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
										}
									}
								}
								
								
								// echo '<pre>';
								// var_dump($data);

								$pooling_data = $data['POOLING'];
								$poolingamount = Application_Helper_General::convertDisplayMoney($data['MAINTAIN_BALANCE']);
								if (empty($poolingamount)) {
									$poolingamount = 0;
								}
								if (!empty($pooling_data)) {

									foreach ($pooling_data as $keyday => $valday) {
										if ($valday != NULL) {
											$insertPoolingcday = array(
												'PERIODIC_ID' => $psPeriodicID,
												'DAY_ID'		=> $keyday,
												'AMOUNT'		=> $valday
											);
											// var_dump($insertPoolingcday);die;
											$this->_db->insert('T_PERIODIC_POOLING', $insertPoolingcday);
										}
										// else{
										// 	$insertPoolingcday = array(
										// 		'PERIODIC_ID' => $psPeriodicID,
										// 		'DAY_ID'		=> $keyday,
										// 		'AMOUNT'		=> $poolingamount
										// 	);

										// 	$this->_db->insert('T_PERIODIC_POOLING', $insertPoolingcday);

										// }

									}
								} else {

									foreach ($report_day as $keyday => $valday) {
										$insertPoolingcday = array(
											'PERIODIC_ID' => $psPeriodicID,
											'DAY_ID'		=> $keyday,
											'AMOUNT'		=> $poolingamount
										);
										// var_dump($insertPoolingcday);die;
										$this->_db->insert('T_PERIODIC_POOLING', $insertPoolingcday);
									}
								}

								}
								//reset array
								unset($newSourceList);
							}
						}
					}

					$this->_db->commit();



					$date = new DateTime();
					$arrday = array(
						'0' => 'sunday',
						'1' => 'monday',
						'2' => 'tuesday',
						'3' => 'wednesday',
						'4' => 'thursday',
						'5' => 'friday',
						'6' => 'saturday'

					);
					// Modify the date it contains
					$datenumb = date('w');
					$datenumb = $datenumb + 1;
					if ($datenumb == 6) {
						$datename = 0;
					} else {
						$datename = $datenumb;
					}
					// var_dump($this->_request->getParams());die;
					// var_dump($efDate);
					// var_dump($START_DATE);
					// var_dump($endDate);die;
					$daterange = $this->GetDays($START_DATE, $endDate);
					// var_dump($daterange);die;

					// if(!empty($repeatOn)){
					// $resutnext = 0;
					// // echo 'here';
					foreach ($daterange as $no => $values) {

						$day = date('w', strtotime($values));
						foreach ($repeatOn as $key => $value) {
							if ($day == $value) {
								$arrdate[] = $values;
							}
						}
					}
					// var_dump(min($arrdate));
					// die;



					// echo "string";
					if (!empty($arrdate)) {
						// ;

						$efDate = min($arrdate);
					}

					// }

					// echo "<pre>";
					// 		var_dump($repetition);
					// 		var_dump($this->_request->getParams());die;

					//param utk create payment sweep
					if ($data['DATEE_TYPE'] == 3) {
					$param['PS_PERIODIC'] 			= $psPeriodicID;
					}
					$param['PS_SUBJECT'] 			= $paymentSubject;
					if ($TrfDateType == 2) {
						$param['PS_EFDATE']  			= $expDate;
						// new Zend_Db_Expr("GETDATE()");	
					} else if ($TrfDateType == 3) {
						if ($repetition == 1) {
							$param['PS_EFDATE']  			= $nextDate;
						}
					}
	
					$param['PS_EFTIME']  			= $data['EFTIME'];
					// if ($sweepOption == 2) {
					$param['PS_TYPE'] 				= 23;
					// } else {
					// 	$param['PS_TYPE'] 				= 19;
					// }
					$param['PS_CCY']  				= 'IDR';
					$param['REF_ID']  				= $paymentReff;
					$param['REMAIN_BALANCE_TYPE']	= $remainBalanceType;
					$param['SWEEP'] 				= "SI";
					$param['SWEEP_TYPE'] 				= $sweepOption;
					$param['BENE_DATA']				= $newBeneList;


					 //var_dump($this->_request->getParams());die;
					//if single select
					if ($sourceType == 1) {
						// var_dump($sweepOption);die;
						if ($sweepOption == 1) {
							$traamount = $remainsBalance;
						} else {
							$traamount = Application_Helper_General::convertDisplayMoney($data['MAINTAIN_BALANCE']);
						}

						//data transaction single
						$param['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 			=> $acSource,
							'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
							'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
							'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
							'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
							'BENEFICIARY_ID' 			=> $newBeneList['id'],
							'BENEFICIARY_ACCOUNT' 		=> $acBenef,
							'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
							'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
							'BENEFICIARY_ALIAS_NAME' 	=> '',
							'TRANSFER_TYPE' 			=> 'PB',
							'TRA_AMOUNT' 				=> $traamount,
							'TRA_MESSAGE' 				=> $message,
							'TRA_REFNO'					=> '',
							'TRA_ADDITIONAL_MESSAGE' 	=> '',
							'TRA_REMAIN'				=> $traamount
						);
					} else {

						//data transaction upload file


						// echo "<pre>";
						// var_dump($acSource);
						// var_dump($this->_request->getParams());die;


						if ($accteopen == 1) {

							foreach ($acctBenef as $row => $source) {

								// var_dump($source);
								$sourceList = $this->_db->fetchAll(
									$this->_db->select()
										->from(array('A' => 'M_APIKEY'), array('*'))
										->where("A.VALUE = ? ", $source['ACCT_NO'])
										->where("A.CUST_ID = ? ", $this->_custIdLogin)
										->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
										->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
								);
								// var_dump($source['NOTIF_EMAIL']);
								foreach ($sourceList as $key => $value) {
									$newSourceList[$value['FIELD']] = $value['VALUE'];
									$newSourceList['bank_code'] = $value['BANK_CODE'];
									$newSourceList['bank_name'] = $value['BANK_NAME'];
								}

								if (!empty($source['NOTIF_EMAIL']) || !empty($source['NOTIF_SMS'])) {
									$TRA_NOTIF = '1';
								} else {
									$TRA_NOTIF = '0';
								}
								$param['TRANSACTION_DATA'][] = array(
									'SOURCE_ACCOUNT' 			=> $acSource,
									'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
									'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
									'SOURCE_ACCOUNT_BANK_CODE' 	=> $data['BANK_CODE'],
									'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
									'BENEFICIARY_ID' 			=> $newBeneList['id'],
									'BENEFICIARY_ACCOUNT' 		=> $source['ACCT_NO'],
									'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
									'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
									'BENEFICIARY_ALIAS_NAME' 	=> '',
									'SWIFT_CODE'				=> $data['SWIFT_CODE'],
									'CLR_CODE'					=> $data['CLR_CODE'],
									'BANK_CODE'					=> $data['BANK_CODE'],
									'TRANSFER_TYPE' 			=> 'PB',
									'TRA_AMOUNT' 				=> $source['REMAIN_BALANCE'],
									'TRA_MESSAGE' 				=> $message,
									'TRA_REFNO'					=> $source['CUST_REF'],
									'TRA_ADDITIONAL_MESSAGE' 	=> '',
									'TRA_REMAIN'				=> $source['REMAIN_BALANCE'],
									'TRA_NOTIF'				=> $TRA_NOTIF,
									'TRA_EMAIL'				=> $source['NOTIF_EMAIL'],
									'TRA_SMS'				=> $source['NOTIF_SMS']
								);

								//reset array
								unset($newSourceList);
							}
						} else {

							foreach ($acSource as $row => $source) {

								// var_dump($source);die;
								$sourceList = $this->_db->fetchAll(
									$this->_db->select()
										->from(array('A' => 'M_APIKEY'), array('*'))
										->where("A.VALUE = ? ", $source['ACCT_NO'])
										->where("A.CUST_ID = ? ", $this->_custIdLogin)
										->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
										->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
								);
								// var_dump($source['NOTIF_EMAIL']);
								foreach ($sourceList as $key => $value) {
									$newSourceList[$value['FIELD']] = $value['VALUE'];
									$newSourceList['bank_code'] = $value['BANK_CODE'];
									$newSourceList['bank_name'] = $value['BANK_NAME'];
								}

								if (!empty($source['NOTIF_EMAIL']) || !empty($source['NOTIF_SMS'])) {
									$TRA_NOTIF = '1';
								} else {
									$TRA_NOTIF = '0';
								}
								$param['TRANSACTION_DATA'][] = array(
									'SOURCE_ACCOUNT' 			=> $source['ACCT_NO'],
									'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
									'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
									'SOURCE_ACCOUNT_BANK_CODE' 	=> $data['BANK_CODE'],
									'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
									'BENEFICIARY_ID' 			=> $newBeneList['id'],
									'BENEFICIARY_ACCOUNT' 		=> $acBenef,
									'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
									'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
									'SWIFT_CODE'				=> $data['SWIFT_CODE'],
									'CLR_CODE'					=> $data['CLR_CODE'],
									'BANK_CODE'					=> $data['BANK_CODE'],
									'BENEFICIARY_ALIAS_NAME' 	=> '',
									'TRANSFER_TYPE' 			=> 'PB',
									'TRA_AMOUNT' 				=> $source['REMAIN_BALANCE'],
									'TRA_MESSAGE' 				=> $message,
									'TRA_REFNO'					=> $source['CUST_REF'],
									'TRA_ADDITIONAL_MESSAGE' 	=> '',
									'TRA_REMAIN'				=> $source['REMAIN_BALANCE'],
									'TRA_NOTIF'				=> $TRA_NOTIF,
									'TRA_EMAIL'				=> $source['NOTIF_EMAIL'],
									'TRA_SMS'				=> $source['NOTIF_SMS']
								);

								//reset array
								unset($newSourceList);
							}
						}
					}

					$param['_addBeneficiary'] = $this->view->hasPrivilege('BADA');
					$param['_beneLinkage'] = $this->view->hasPrivilege('BLBU');
					$param['_priviCreate'] = 'IPMO';

					$SweepPayment = new SweepPayment(null, $this->_custIdLogin, $this->_userIdLogin);

					// echo '<pre>';
					// print_r($param);die();

					if($this->_custSameUser){
										if(!$this->view->hasPrivilege('PRLP')){
											// die('here');
											
											$errMessage = $this->language->_("Error: You don't have privilege to release payment");
											$this->view->error = true;
											$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
										}else{
											
											///google auth
											$challengeCode		= $this->_getParam('challengeCode');

											$inputtoken1 		= $this->_getParam('inputtoken1');
											$inputtoken2 		= $this->_getParam('inputtoken2');
											$inputtoken3 		= $this->_getParam('inputtoken3');
											$inputtoken4 		= $this->_getParam('inputtoken4');
											$inputtoken5 		= $this->_getParam('inputtoken5');
											$inputtoken6 		= $this->_getParam('inputtoken6');

											$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;


											$select3 = $this->_db->select()
												 ->from(array('C' => 'M_USER'));
											$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
											// $select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
											$data2 = $this->_db->fetchRow($select3);

											// $code = $param['googleauth'];
		

											$pga = new PHPGangsta_GoogleAuthenticator();
									    	 //var_dump($data2['GOOGLE_CODE']);
									    	 //var_dump($code);
									    	 //print_r($responseCode);die();
											$setting 		= new Settings();
											$google_duration 	= $setting->getSetting('google_duration');
									        if($pga->verifyCode($data2['GOOGLE_CODE'], $responseCode, $google_duration))
									        {
									        	$resultToken = $resHard['ResponseCode'] == '0000';
									        	$tokenAuth = true;
									        }else{
									        	$tokenAuth = false;	
									        }
										}
					if($tokenAuth){
						$param['sameuser'] = 1;
						$result = $SweepPayment->createOpenSweep($param);
					}else{
						$this->view->error = true;
						// $docErr = $this->displayError($zf_filter->getMessages());
						// print_r($docErr);die;
						$docErr = array('0'=> 'Invalid Token');
						$this->view->report_msg = $docErr;
					}					
										
				}else{
					$result = $SweepPayment->createOpenSweep($param);
				}

					

					$this->setbackURL('/opensweep');
					$this->_redirect('/notification/success/index');
				} catch (Exception $e) {
					// $this->_db->rollBack();
					 //print_r($e);
					 //die();
				}
			} else {
				$this->view->error = true;
				$docErr = $this->displayError($zf_filter->getMessages());
				// print_r($docErr);die;
				
				$this->view->report_msg = $docErr;
			}
		}
		// echo "<pre>";
		// var_dump($data);die;


		//maintain balance
		if (!empty($data['POOLING'])) {
			$arrday = array(
				'0' => 'sun',
				'1' => 'mon',
				'2' => 'tue',
				'3' => 'wed',
				'4' => 'thu',
				'5' => 'fry',
				'6' => 'sat'

			);
			foreach ($data['POOLING'] as $key => $value) {
				// var_dump($arrday[$key]);
				${'pooling_' . $arrday[$key]} = $value;
				// var_dump($pooling_sun);
			}
			// var_dump($pooling_sun);die;
			// var_dump( $data['POOLING']);die;


			$this->view->pooling_sun = $pooling_sun;
			$this->view->pooling_mon = $pooling_mon;
			$this->view->pooling_tue = $pooling_tue;
			$this->view->pooling_wed = $pooling_wed;
			$this->view->pooling_thu = $pooling_thu;
			$this->view->pooling_fry = $pooling_fry;
			$this->view->pooling_sat = $pooling_sat;

			$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
			$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
			$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
			$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
			$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
			$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
			$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);
			// var_dump($data);die;
			if (!empty($data['MAINTAIN_BALANCE'])) {

				$this->view->mainbalance = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($data['MAINTAIN_BALANCE']));
			}
		}

		$this->view->paymentSubject = $data['SUBJECT'];
		// if($data['BALANCE_TYPE'])
		$this->view->sweepbene = $data['SWEEPIN'];
		$this->view->sourceType = $data['SOURCE_TYPE'];
		$this->view->acSource = $data['ACCT_NO'];
		$this->view->acBenef = $data['BENEF_NO'];

		// $paramdata['databank'] = $databank;
		// $paramdata['dataacct'] = $acct;

		$aliasbenef = '';
		foreach ($data['dataacct'] as $key => $value) {
			if ($value['ACCT_NO'] == $data['ACCT_NO']) {
				// var_dump($value);die;
				$aliassource = $value['ACCT_ALIAS'];
			}

			if ($value['ACCT_NO'] == $data['BENEF_NO']) {
				$aliasbenef = $value['ACCT_ALIAS'];
			}
		}

		// var_dump(expression)
		$bank_name = '';
		foreach ($data['databank'] as $key => $value) {
			if ($value['BANK_CODE'] == $data['BANK_CODE']) {
				$bank_name = $value['BANK_NAME'];
			}
		}

		$this->view->benefName = Application_Helper_General::viewAccountOpen($data['BENEF_NO'], 'IDR', $bank_name, $aliasbenef);;
		$this->view->sourceName = Application_Helper_General::viewAccountOpen($data['ACCT_NO'], 'IDR', $bank_name, $aliassource);;
		// if($data['SWEEPIN'] == '1'){
		$this->view->beneSource = $data['BENEF_NO'];
		// 	$this->view->sourceType = '1';
		// 	$this->view->acSource = $data['BENEF_NO'];
		// 	$this->view->sourceName = $data['BENEF_NAME'];
		// }


		$this->view->sweepOption = $data['SWEEP_TYPE'];


		$this->view->remainsBalance = $data['REMAIN_BALANCE'];
		$this->view->remainBalanceType = $data['BALANCE_TYPE'];

		$this->view->TrfDateType = $data['DATEE_TYPE'];
		$this->view->efDate = $data['EFDATE'];
		$this->view->endDate = $data['ENDDATE'];
		$this->view->repetition = $data['REPETITION'];
		$this->view->repeatEvery = $data['REPEAT_EVERY'];
		$this->view->repeatOn = $data['REPEAT_DAY'];

		$this->view->notif = $data['NOTIF'];
		$this->view->email_notif = $data['EMAIL_NOTIF'];
		$this->view->sms_notif = $data['SMS_NOTIF'];
		$this->view->message = $data['DESC'];
		$this->view->PS_EFTIMEP = $data['EFTIME'];
		$this->view->PS_EFTIMEF = $data['EFTIME'];

		$this->view->repeat_day = $data['REPORT_DAY'];
	}

	function GetDays($sStartDate, $sEndDate)
	{
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
		$sEndDate = gmdate("Y-m-d", strtotime($sEndDate));

		// Start the variable off with the start date  
		$aDays[] = $sStartDate;

		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;

		// While the current date is less than the end date  
		// var_dump($sCurrentDate);
		// var_dump($sEndDate);
		// $sCurrentDate = gmdate("Y-m-d", strtotime("+2 day", strtotime($sCurrentDate)));  
		// var_dump($sCurrentDate);die;
		while ($sCurrentDate < $sEndDate) {
			// Add a day to the current date
			$sCurrentDate = date('Y-m-d', strtotime($sCurrentDate . "+1 days"));
			// strtotime($sCurrentDate . "+1 days");  
			// $sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// if($sCurrentDate == '2020-01-30'){
			// var_dump($sCurrentDate);die;
			// }

			// Add this new day to the aDays array  
			$aDays[] = $sCurrentDate;
		}

		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;
	}

	function moneyAliasFormatter($n)
	{
		// first strip any formatting;
		return str_replace('.00', '', Application_Helper_General::displayMoney($n));
		// $n = (0+str_replace(",", "", $n));

		// // is this a number?
		// if (!is_numeric($n)) return false;

		// // now filter it;
		// if ($n > 1000000000000) return round(($n/1000000000000), 2).' T';
		// elseif ($n > 1000000000) return round(($n/1000000000), 2).' B';
		// elseif ($n > 1000000) return round(($n/1000000), 2).' M';
		// elseif ($n > 1000) return $n;

		// return number_format($n);
	}

	function validateOpenSourceAccount($accsrc, $benef)
	{

		$error_array = array();
		$error_msg = array();
		$error = false;
		if ($accsrc == $benef) {
			$error = true;
			array_push($error_msg, 'Source Account Cannot Be Same As Beneficiary Account');
		}

		$sourceList = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'), array('*'))
				->where("A.VALUE = ? ", $accsrc)
				->where("A.CUST_ID = ? ", $this->_custIdLogin)
		);

		if (empty($sourceList)) {
			$error = true;
			array_push($error_msg, 'Source Account is Not Registered');
		}

		if ($error) {
			$error_array[$accsrc] = $error_msg;
		}

		return $error_array;
	}

	public function generatePaymentReff($forTransaction)
	{

		/*$paymentreff = "SI".date("Y").date("m").date("d").strtoupper(uniqid());
		
		return $paymentreff;*/

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(8));
		$checkDigit  = '';

		$paymentreff   = "SI" . $currentDate . $seqNumber . $checkDigit;

		return $paymentreff;
	}

	private function fillParams($param)
	{
		$this->view->paymentsubject			= $param["paymentsubject"];
		$this->view->ACBENEF				= $param["ACBENEF"];
		$this->view->ACBENEF_BANKNAME		= $param["ACBENEF_BANKNAME"];
		$this->view->ACBENEF_ALIAS			= $param["ACBENEF_ALIAS"];
		$this->view->CURR_CODE				= $param["CURR_CODE"];
		$this->view->datetime				= $param["end_date"];
		$this->view->end_date				= $param["end_date"];
		$this->view->REMAIN_BALANCE_TYPE	= $param["REMAIN_BALANCE_TYPE"];
		$this->view->TrfPeriodicType		= $param["tranferdateperiodictype"];
		$this->view->dayname 				= $param["dayname"];
		$this->view->day					= $param["day"];
		$this->view->sesion_sweep 			= $param["sesion_sweep"];
		$this->view->ACCTSRC				= $param["sesion_sweep"];
	}


	public function accountlistAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$bankCode = $this->_getParam('bankCode');

		$complist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_USER'), array('CUST_ID'))

				->where("A.USER_ID = ? ", $this->_userIdLogin)
		);

		$comp = "'";
		foreach ($complist as $key => $value) {
			$comp .= "','" . $value['CUST_ID'] . "','";
		}
		$comp .= "'";


		$acctlist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'))
				->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
				->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
				// ->where('A.ACCT_STATUS = ?','5')
				->where("A.CUST_ID IN (" . $comp . ")")
				->where("A.BANK_CODE = ?", $bankCode)
				->order('A.APIKEY_ID ASC')
			// echo $acctlist;
		);

		$account = array();
		//echo '<pre>';
		//var_dump($acctlist);die;
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			if(empty($value['account_alias'])){
				
					$alias = $account[$value['ID']]['account_name'];
			}else{
				$alias = $account[$value['ID']]['account_alias'];
			}
			
			$account[$value['ID']]['account_alias'] = $alias;
		}
		
	//	echo '<pre>';
	//	var_dump($account);die;
		$acct = array();
		foreach ($account as $key => $value) {
			$acct[$key - 1]['ACCT_NO'] = $value['account_number'];
			$acct[$key - 1]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$key - 1]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$key - 1]['ACCT_NAME'] = $value['account_name'];
		}

			// $html = "<option value=''>-- Select Source Account --</option>";
			// foreach ($acct as $key => $value) {
			// 	$html .= "<option value = '" . $value['ACCT_NO'] . "'>";

			// 	if (!empty($value['ACCT_ALIAS'])) {
			// 		// $html .= $value['ACCT_NO']." [".$value['ACCT_BANK']."] - ".$value['ACCT_ALIAS']."</option>";
			// 		$html .= $value['ACCT_NO'] . " (IDR) - " . $value['ACCT_ALIAS'] . "</option>";
			// 	} else {
			// 		// $html .= $value['ACCT_NO']." [".$value['ACCT_BANK']."] </option>";
			// 		$html .= $value['ACCT_NO'] . " (IDR) </option>";
			// 	}
			// }

		echo json_encode($acct);
	}

	public function generateTransactionID(){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(5));
		$trxId = 'PR'.$currentDate.$this->_custIdLogin.$seqNumber;

		return $trxId;
	}
	
	 public function bankcityAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('bankcode');
        
		$selectbank = $this->_db->select()
							->from(array('A' => 'M_BANK_TABLE'),array('*'));
					$selectbank->where('A.BANK_CODE = ?',$tblName);
					$arr = $this->_db->fetchall($selectbank);
					//var_dump($arr);
		if(!empty($arr)){
        $select = $this->_db->select()
							->from(array('A' => 'M_CITY'),array('*'));
					$select->where('A.SWIFT_CODE = ?',trim($arr['0']['SWIFT_CODE']));
					//echo $select;
					$arr = $this->_db->fetchall($select);
		}

        // $tempColumn = $this->_db->fetchAll($select); 
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
 
        foreach($arr as $key => $row){
           // if($tblName==$value['CITY_CODE']){
            //    $select = 'selected'; 
            //}else{
                $select = '';
            //} 
			
            $optHtml.="<option value='".$row['CITY_CODE']."' ".$select.">".$row['CITY_NAME']."</option>";
        }

        echo $optHtml;
    }
	
	
	public function resetbankAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

		$banklist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'), array('A.BANK_CODE'))
				// ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
				//  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
				//  // ->join(array('D' => 'M_DOMESTIC_BANK_TABLE'),'D.BANK_NAME = B.BANK_NAME',array('D.SWIFT_CODE'))
				->where('A.CUST_ID = ?', $this->_custIdLogin)
				->group('A.BANK_CODE')
			// ->where("A.BANK_CODE IN ('008','014','013','009','032','031','153','002') ")
			// ->order('A.APIKEY_ID ASC')
		);
		// var_dump($banklist);
		$par = array();
		if (!empty($banklist)) {
			foreach ($banklist as $key => $value) {
				$par[] = $value['BANK_CODE'];
			}
		}
		// var_dump($par);
		// var_dump($banklist);die;
		if (empty($par)) {
			$par = array('000');
		}

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'))
			->where('C.BANK_CODE IN (?)', $par);
		 //echo $selectbank;die;
		$databank 					= $this->_db->fetchAll($selectbank);
		
		
		$optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
 
        foreach($databank as $key => $row){
           // if($tblName==$value['CITY_CODE']){
            //    $select = 'selected';
            //}else{
                $select = '';
            //} 
			
            $optHtml.="<option value='".$row['BANK_CODE']."' ".$select.">".$row['BANK_NAME']."</option>";
        }

        echo $optHtml;
	}
}
