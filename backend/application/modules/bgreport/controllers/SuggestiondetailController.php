<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgreport_SuggestiondetailController extends customer_Model_Customer
{
	protected $_moduleDB = 'CST';

	public function indexbakAction()
	{

		$this->_helper->layout()->setLayout('popup');

		$params = $this->_request->getParams();
		$keyValue = null;
		$excludeChangeId  = 'CHANGES_STATUS IN (' . $this->_db->quote($this->_changeStatus['code']['waitingApproval']) . ',' . $this->_db->quote($this->_changeStatus['code']['repairRequested']) . ')';
		$excludeChangeId .= ' AND ' . $this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

		if (is_array($params) && count($params)) $fullDesc = SGO_Helper_GeneralFunction::displayFullDesc($params);
		else $fullDesc = null;

		$this->view->suggestionType = $this->_suggestType;



		//cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
		$changes_id = strip_tags(trim($this->_getParam('changes_id')));

		$select = $this->_db->select()
			->from(array('A' => 'T_GLOBAL_CHANGES'), array('*'))
			->where("A.CHANGES_ID = ?", $changes_id)
			->where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$global_changes = $this->_db->fetchRow($select);

		if (empty($global_changes))  $this->_redirect('/notification/invalid/index');
		//END cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid


		if (array_key_exists('changes_id', $params)) {
			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
			$validators = array(
				'changes_id' => array(
					'NotEmpty',
					'Digits',
					array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
					array('Db_RecordExists', array('table' => 'TEMP_BANK_GUARANTEE', 'field' => 'CHANGES_ID')),
					'messages' => array(
						'Can not be empty',
						'Invalid format',
						'Change id is not already exists in Master Global Changes',
						'Change id is not already exists in Temp Global Changes',
					),
				),
			);


			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
			if ($zf_filter_input->isValid()) {
				//$this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');
				$this->view->status_type = $this->_masterglobalstatus;

				$changeId  = $zf_filter_input->changes_id;
				//$resultdata = $this->getTempCustomer($changeId);
				$resultdata  = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('T' => 'TEMP_BANK_GUARANTEE'))
						->joinleft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID=T.CUST_ID', array('CUST_NAME'))
						->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS'))
						->where('T.CHANGES_ID = ?', $changeId)
				);

				$keyValue  = $resultdata['CUST_ID'];


				//$this->view->date_establish = (($custData['DATE_ESTABLISH'])? SGO_Helper_GeneralFunction::convertDate($custData['DATE_ESTABLISH']) : null);

				//Zend_Debug::dump($resultdata);
				//die;

				$selectbank = $this->_db->select()
					->from('M_BANK_TABLE', array('BANK_NAME', 'BANK_CODE'));
				//->from(array('A' => 'M_BANK_TABLE'));		

				$tempBank = $this->_db->fetchAll($selectbank);
				$bankarr = array();
				foreach ($tempBank as $dt) {
					$bankarr[$dt['BANK_CODE']] = $dt['BANK_NAME'];
				}
				$this->view->bankarr = $bankarr;

				//var_dump($resultdata);die;
				//suggest data
				$this->view->changes_id     = $resultdata['CHANGES_ID'];
				$this->view->changes_type   = $resultdata['CHANGES_TYPE'];
				$this->view->changes_status = $resultdata['CHANGES_STATUS'];
				$this->view->read_status    = $resultdata['READ_STATUS'];
				$this->view->created        = $resultdata['CREATED'];
				$this->view->created_by     = $resultdata['CREATED_BY'];

				if ($resultdata['CHANGES_TYPE'] == 'S') {
					$this->view->changes_name  = 'Suspend';
				} elseif ($resultdata['CHANGES_TYPE'] == 'U') {
					$this->view->changes_name  = 'Unsuspend';
				} elseif ($resultdata['CHANGES_TYPE'] == 'L') {
					$this->view->changes_name  = 'Delete';
				} elseif ($resultdata['CHANGES_TYPE'] == 'E') {
					$this->view->changes_name  = 'Edit';
				}

				//temp bank garante data
				$this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
				$this->view->cust_name    = $resultdata['CUST_NAME'];
				$this->view->reg_ref     = $resultdata['BG_REG_NUMBER'];
				$this->view->bg_no     = $resultdata['BG_NUMBER'];
				$this->view->acct_no     = $resultdata['ACCT_NO'];
				$this->view->bg_subject     = $resultdata['BG_SUBJECT'];

				$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

				$this->view->status  = $arrStatus[$resultdata['BG_STATUS']];

				$data = $resultdata;

				$this->view->data = $data;


				//$resultdata['CUST_FINANCE'];

				$this->view->country_code  = $resultdata['COUNTRY_CODE'];
				$this->view->cust_contact  = $resultdata['CUST_CONTACT'];
				$this->view->cust_phone    = $resultdata['CUST_PHONE'];
				$this->view->cust_ext      = $resultdata['CUST_EXT'];
				$this->view->cust_fax      = $resultdata['CUST_FAX'];
				$this->view->cust_email    = $resultdata['CUST_EMAIL'];
				$this->view->cust_website  = $resultdata['CUST_WEBSITE'];
				$this->view->cust_status   = $resultdata['CUST_STATUS'];

				$this->view->cust_created     = $resultdata['CUST_CREATED'];
				$this->view->cust_createdby   = $resultdata['CUST_CREATEDBY'];
				$this->view->cust_suggested   = $resultdata['CUST_SUGGESTED'];
				$this->view->cust_suggestedby = $resultdata['CUST_SUGGESTEDBY'];
				$this->view->cust_updated     = $resultdata['CUST_UPDATED'];
				$this->view->cust_updatedby   = $resultdata['CUST_UPDATEDBY'];


				//echo $selectglobal;die;



				$setting = new Settings();
				$system_type = $setting->getSetting('system_type');
				$this->view->system_type = $system_type;

				$tblName = $resultdata['CUST_CIF'];
				$app = Zend_Registry::get('config');
				$app = $app['app']['bankcode'];
				// return $app;


				$m_resultdata  = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('T' => 'T_BANK_GUARANTEE'))
						->joinleft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID=T.CUST_ID', array('CUST_NAME'))
						//->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
						->where('T.BG_REG_NUMBER = ?', $resultdata['BG_REG_NUMBER'])
				);

				$keyValue  = $m_resultdata['CUST_ID'];


				//$this->view->cust_id      = strtoupper($m_resultdata['CUST_ID']);
				$this->view->m_cust_name   = $m_resultdata['CUST_NAME'];
				$this->view->m_reg_ref     = $m_resultdata['BG_REG_NUMBER'];

				$this->view->m_bg_no     = $m_resultdata['BG_NUMBER'];
				$this->view->m_acct_no     = $m_resultdata['ACCT_NO'];
				$this->view->m_bg_subject     = $m_resultdata['BG_SUBJECT'];

				$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$m_arrStatus = array_combine(array_values($BgCode), array_values($BgType));



				$this->view->m_status  = $m_arrStatus[$m_resultdata['BG_STATUS']];


				//master customer data
				$m_resultdata = $this->getCustomer(strtoupper($resultdata['CUST_ID']));

				$this->view->m_cust_id      = strtoupper($m_resultdata['CUST_ID']);
				$this->view->m_cust_name    = $m_resultdata['CUST_NAME'];
				$this->view->m_cust_cif     = $m_resultdata['CUST_CIF'];
				$this->view->m_cust_type    = $m_resultdata['CUST_TYPE'];
				$this->view->m_cust_securities    = $m_resultdata['CUST_SECURITIES'];
				$this->view->m_cust_code    = $m_resultdata['CUST_CODE'];
				$this->view->m_cust_workfield  = $m_resultdata['CUST_WORKFIELD'];
				if ($m_resultdata['CUST_FINANCE'] == '1') {
					$this->view->m_cust_finance  = 'Finance';
				} else {
					$this->view->m_cust_finance  = 'Non Finance';
				}

				if (empty($m_resultdata['CUST_SECURITIES'])) {
					$this->view->m_cust_securities    = 'No';
				} else {
					$this->view->m_cust_securities    = 'Yes';
				}
				$this->view->m_cust_address = $m_resultdata['CUST_ADDRESS'];
				$this->view->m_cust_city    = $m_resultdata['CUST_CITY'];
				$this->view->m_cust_zip     = $m_resultdata['CUST_ZIP'];
				$this->view->m_cust_province = $m_resultdata['CUST_PROVINCE'];
				$this->view->m_country_code  = $m_resultdata['COUNTRY_CODE'];
				$this->view->m_cust_contact  = $m_resultdata['CUST_CONTACT'];
				$this->view->m_cust_phone    = $m_resultdata['CUST_PHONE'];
				$this->view->m_cust_ext      = $m_resultdata['CUST_EXT'];

				$this->view->m_cust_created     = $m_resultdata['CUST_CREATED'];
				$this->view->m_cust_createdby   = $m_resultdata['CUST_CREATEDBY'];
				$this->view->m_cust_suggested   = $m_resultdata['CUST_SUGGESTED'];
				$this->view->m_cust_suggestedby = $m_resultdata['CUST_SUGGESTEDBY'];
				$this->view->m_cust_updated     = $m_resultdata['CUST_UPDATED'];
				$this->view->m_cust_updatedby   = $m_resultdata['CUST_UPDATEDBY'];

				// Application_Helper_General::writeLog('CCCL','View customer changes list');
			} else {
				$errors = $zf_filter_input->getMessages();
				$errorRemark = SGO_Helper_GeneralFunction::displayErrorRemark($errors);
				Application_Helper_General::writeLog('CCCL', 'View customer changes list');

				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
				$this->_redirect('/popuperror/index/index');
			}
		} else {
			$errorRemark = 'Changes Id not found';
			//Application_Helper_General::writeLog('CCCL','');

			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
			$this->_redirect('/popuperror/index/index');
		}
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('CCCL', 'View customer changes list');
		}
	}

	public function indexAction()
	{
		$change_id = $this->_getParam('changes_id');
		$change_id = (Zend_Validate::is($change_id, 'Digits')) ? $change_id : 0;
		$this->view->changes_id = $change_id;
		$this->_helper->layout()->setLayout('newpopup');

		$getTempData = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE')
			->where('CHANGES_ID = ?', $change_id)
			->query()->fetch();

		$this->view->tempData = $getTempData;

		$getBgData = $this->_db->select()
			->from(['A' => 'T_BANK_GUARANTEE'])
			->joinLeft(['B' => 'M_CUSTOMER'], 'A.CUST_ID = B.CUST_ID', ['B.CUST_NAME'])
			->where('BG_REG_NUMBER = ?', $getTempData['BG_REG_NUMBER'])
			->query()->fetch();

		$getTempCloseData = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE')
			->where('CHANGES_ID = ?', $change_id)
			->query()->fetch();

		$config    		= Zend_Registry::get('config');
		$BgcomplationType     = $config["bgclosing"]["changetype"]["desc"];
		$BgcomplationCode     = $config["bgclosing"]["changetype"]["code"];

		$arrStatusSettlement = array_combine(array_values($BgcomplationCode), array_values($BgcomplationType));

		$this->view->arrStatusSettlement = $arrStatusSettlement;

		$this->view->data = $getBgData;

		$AESMYSQL = new Crypt_AESMYSQL();
		$rand = $this->token;
		$encrypted = $AESMYSQL->encrypt($getBgData['BG_REG_NUMBER'], $rand);
		$encrypted_id = urlencode($encrypted);
		$link = $this->view->url(array('module' => 'bgreport', 'controller' => 'detail', 'action' => 'index', 'bg' => $encrypted_id), null, true);

		$this->view->link = $link;

		$getGlobalChanges = $this->_db->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $change_id)
			->query()->fetch();

		$this->view->needApprove = ($getGlobalChanges['CHANGES_STATUS'] == 'WA') ? true : false;

		if ($this->_request->isPost()) {
			// approve
			if ($this->_getParam('approve')) {
				$this->_db->update('T_GLOBAL_CHANGES', [
					'CHANGES_STATUS' => 'AP'
				], [
					'CHANGES_ID = ?' => $change_id
				]);

				$this->_db->update('T_BANK_GUARANTEE', [
					'CLOSING_TYPE' => $getTempData['CLOSING_TYPE'],
					'CLOSING_FILE' => $getTempData['CLOSING_FILE'],
					'BG_STATUS' => '16'
				], [
					'BG_REG_NUMBER = ?' => $getTempData['BG_REG_NUMBER']
				]);

				$this->_db->delete('TEMP_BANK_GUARANTEE_CLOSE', [
					'CHANGES_ID = ?' => $change_id
				]);

				$penutupan = $arrStatusSettlement[$getTempData['CLOSING_TYPE']];

				$actionDesc = ($getBgData['COUNTER_WARRANTY_TYPE'] == 1) ? 'ATCC' : 'ATNC';
				$tipeWarranty = ($getBgData['COUNTER_WARRANTY_TYPE'] == 1) ? 'cash collateral' : 'non cash collateral';
				Application_Helper_General::writeLog($actionDesc, 'Menyetujui Usulan Penutupan Bank Garansi No. ' . $getBgData['BG_NUMBER'] . ' (' . $tipeWarranty . '). Jenis Penutupan: ' . $penutupan . '');
			}

			// reject
			if ($this->_getParam('reject')) {
				$this->_db->update('T_GLOBAL_CHANGES', [
					'CHANGES_STATUS' => 'RJ'
				], [
					'CHANGES_ID = ?' => $change_id
				]);
				$this->_db->delete('TEMP_BANK_GUARANTEE_CLOSE', [
					'CHANGES_ID = ?' => $change_id
				]);

				switch ($getTempData['CLOSING_TYPE']) {
					case 1:
						$penutupan = 'Permintaan Principal';
						break;

					case 2:
						$penutupan = 'Habis Masa';
						break;

					case 3:
						$penutupan = 'Klaim Obligee';
						break;
				}

				$actionDesc = ($getBgData['COUNTER_WARRANTY_TYPE'] == 1) ? 'ATCC' : 'ATNC';
				$tipeWarranty = ($getBgData['COUNTER_WARRANTY_TYPE'] == 1) ? 'cash collateral' : 'non cash collateral';
				Application_Helper_General::writeLog($actionDesc, 'Menolak Usulan Penutupan Bank Garansi No. ' . $getBgData['BG_NUMBER'] . ' (' . $tipeWarranty . '). Jenis Penutupan: ' . $penutupan . '');
			}

			if ($this->_getParam('download') === '0' || $this->_getParam('download') === '1') {
				$attahmentDestination = UPLOAD_PATH . '/document/submit/';
				$this->_helper->download->file(explode('~|~', $getTempCloseData['CLOSING_FILE'])[$this->_request->getParam('download')], $attahmentDestination . explode('~|~', $getTempCloseData['CLOSING_FILE'])[$this->_request->getParam('download')]);
			}

			$this->view->refresh = true;
		}
	}
}
