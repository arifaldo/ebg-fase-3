<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgclosingworkflow_approvedetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$allSetting = $settings->getAllSetting();

		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');

		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = $this->combineCodeAndDesc($bgpublishCode, $bgpublishType);

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);
		$this->view->arrbgcg = $arrbgcg;

		$bgType   = $config["bg"]["type"]["desc"];
		$bgCode   = $config["bg"]["type"]["code"];
		$arrbg 	= $this->combineCodeAndDesc($bgCode, $bgType);
		$this->view->arrbg = $arrbg;

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);
		$this->view->arrbgclosingType = $arrbgclosingType;

		$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
		$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);
		$this->view->arrbgclosingStatus = $arrbgclosingStatus;
		// END GLOBAL VARIABEL

		// load model
		$model = new bgclosingworkflow_Model_Approvedetailclosing($this->_userIdLogin);

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI

		$bgparam = $this->_getParam('bgnumb');

		$this->view->bgnumb_enc = $bgparam;
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, uniqid());

		// JIKA BG NUMBER ADA
		if (!empty($numb)) {

			// GET BG DATA
			$bgdata = $model->getDetailBg($numb);

			// JIKA DATA ADA
			if (!empty($bgdata) && !empty($bgdata['CLOSE_REF_NUMBER'])) {
				$this->view->bgdata = $bgdata;

				// GET PAPER USED
				$getPaperUsed = $model->getPaperUsed($numb);
				$paperUsed = array_column($getPaperUsed, 'PAPER_ID');
				$this->view->paperUsed = $paperUsed;

				// GET HISTORY CLOSE
				$getLastHistoryClose = $model->getLastHistoryClose($bgdata['CLOSE_REF_NUMBER']);
				$this->view->getLastHistoryClose = $getLastHistoryClose;

				// GET ALL HISTORY CLOSE
				$getAllHistoryClose = $model->getHistoryClose($bgdata['CLOSE_REF_NUMBER'], $bgdata['CUST_ID']);

				$getAllHistoryClose = array_filter($getAllHistoryClose, function($item) use($bgdata){
					return ($item['CUST_ID'] == $bgdata['CUST_ID'] || $item['CUST_ID'] == '-');
					// return true;
				});

				$this->view->getAllHistoryClose = $getAllHistoryClose;

				$historyStatusCode = $config["bgclosinghistory"]["status"]["code"];
				$historyStatusDesc = $config["bgclosinghistory"]["status"]["desc"];
				$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));
				$this->view->historyStatusArr   = $historyStatusArr;

				// PRINCIPAL DETAIL
				$principalDetail = $model->principalDetail($bgdata['CUST_ID']);
				$this->view->principalDetail = $principalDetail;

				// TBG DETAIL
				$getTbgDetail = $model->getTbgDetail($bgdata['BG_REG_NUMBER']);
				$this->view->getTbgDetail = $getTbgDetail;

				// KONTRA GARANSI DETAIL
				if (!empty($getTbgDetail)) {
					foreach ($getTbgDetail as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($bgdata['COUNTER_WARRANTY_TYPE'] == '3') {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$insuranceName = $this->_db->select()
									->from("M_CUSTOMER")
									->where("CUST_ID = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuraceName =  $insuranceName[0]['CUST_NAME'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				// GET SPLIT IF EXIST
				$getSplit = $model->getBgSplit($bgdata['BG_REG_NUMBER']);
				$this->view->getSplit = $getSplit;

				// LAMPIRAN DOKUMEN PENUTUPAN
				$tempBgCloseFile = $model->getBgCloseFile($bgdata['CLOSE_REF_NUMBER']);
				$newTempBgCloseFile = array_combine(array_map(function ($item) {
					return implode('_', explode(' ', strtolower($item)));
				}, array_column($tempBgCloseFile, 'FILE_NOTE')), array_map(function ($item) {
					return trim(str_replace([explode('_', $item)[0], explode('_', $item)[1], explode('_', $item)[2], explode('_', $item)[3]], '', $item), '_');
				}, array_column($tempBgCloseFile, 'BG_FILE')));

				$this->view->tempBgCloseFile = $newTempBgCloseFile;

				// VALIDATE BUTTON
				if ($bgdata['COUNTER_WARRANTY_TYPE'] == '1') {
					$boundary = $this->validatebtn('50', $bgdata['BG_AMOUNT'], 'IDR', $bgdata['CLOSE_REF_NUMBER']);
				} else {
					$boundary = $this->validatebtn('51', $bgdata['BG_AMOUNT'], 'IDR', $bgdata['CLOSE_REF_NUMBER']);
				}
				$this->view->validbtn = $boundary;

				//  ----- ???
				if ($bgdata['COUNTER_WARRANTY_TYPE'] == '1') {
					$policyBoundary = $this->findPolicyBoundary(50, $bgdata['BG_AMOUNT']);
					$approverUserList = $this->findUserBoundary(50, $bgdata['BG_AMOUNT']);
				} else {
					$policyBoundary = $this->findPolicyBoundary(51, $bgdata['BG_AMOUNT']);
					$approverUserList = $this->findUserBoundary(51, $bgdata['BG_AMOUNT']);
				}

				// POLICY MATRIKS
				$this->view->policyBoundary = strtr($policyBoundary, $approverUserList['GROUP_NAME']);

				// GET HISTORY APPROVAL
				$this->view->getHistoryApproval = $model->getHistoryApproval($bgdata['CLOSE_REF_NUMBER']);

				if ($bgdata['CHANGE_TYPE_CLOSE'] == '3') {
					$getIndexClaimAcctNumber = $this->_db->select()
						->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
						->where('CLOSE_REF_NUMBER = ?', $bgdata['CLOSE_REF_NUMBER'])
						->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
						->query()->fetch();

					$getAcctClaimDetail = $this->_db->select()
						->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
						->where('CLOSE_REF_NUMBER = ?', $bgdata['CLOSE_REF_NUMBER'])
						// ->where('ACCT_INDEX = ?', $getIndexClaimAcctNumber['ACCT_INDEX'])
						->query()->fetchAll();

					$getAcctClaimDetail = array_combine(array_map('strtolower', array_column($getAcctClaimDetail, 'PS_FIELDNAME')), array_column($getAcctClaimDetail, 'PS_FIELDVALUE'));

					$this->view->getAcctClaimDetail = $getAcctClaimDetail;

					$this->view->swiftCodeParam = $getAcctClaimDetail['swift code'];
					$this->view->nomorRekening = $getAcctClaimDetail['beneficiary account number'];
					$this->view->namaRekening = $getAcctClaimDetail['beneficiary account name'];
					$this->view->alamatRekening = $getAcctClaimDetail['beneficiary address'];

					$getInfoBank = $this->_db->select()
						->from('M_DOMESTIC_BANK_TABLE')
						->where('SWIFT_CODE = ?', $getAcctClaimDetail['swift code'])
						->query()->fetch();

					$this->view->getInfoBank = $getInfoBank;
				}

				$tempBgClose = $this->_db->select()
					->from(['TBGC' => 'TEMP_BANK_GUARANTEE_CLOSE'])
					->where('TBGC.BG_REG_NUMBER = ?', $numb)
					->orWhere('TBGC.BG_NUMBER = ?', $bgdata['BG_NUMBER'])
					->where('TBGC.SUGGESTION_STATUS = 6')
					->query()->fetchAll();

				$this->view->tempBgClose = $tempBgClose;

				// POST DATA
				if ($this->_request->isPost()) {

					$reason = $this->_request->getParam('reason');

					// PENOLAKAN DATA
					if ($this->_request->getParam('penolakan')) {
						$model->updateStatusClose($bgdata['CLOSE_REF_NUMBER'], '7');

						// LOG
						$logText = ($bgdata['CHANGE_TYPE_CLOSE'] == 1) ? $this->language->_('Penolakan Pengajuan Penutupan untuk BG No : ' . $bgdata['BG_NUMBER'] . ', Principal : ' . $bgdata['CUST_ID'] . ', NoRef Pengajuan : ' . $bgdata['CLOSE_REF_NUMBER'] . ', Catatan : ' . $reason) : ($bgdata['CHANGE_TYPE_CLOSE'] == 3 ? $this->language->_('Penolakan Pengajuan Klaim untuk BG No : ' . $bgdata['BG_NUMBER'] . ', Principal : ' . $bgdata['CUST_ID'] . ', NoRef Pengajuan : ' . $bgdata['CLOSE_REF_NUMBER'] . ', Catatan : ' . $reason) : '');
						$privLog = $bgdata['COUNTER_WARRANTY_TYPE'] == '1' ? 'ACCS' : 'ANCS';
						Application_Helper_General::writeLog($privLog, $logText);

						// HISTORY CLOSE
						$inserHistoryCloseData = array_intersect_key($bgdata, ['CLOSE_REF_NUMBER' => '', 'BG_NUMBER' => '', 'CUST_ID' => '-']);
						$inserHistoryCloseData['REASON_POST'] = $reason;
						$inserHistoryCloseData['HISTORY_STATUS'] = '7';
						$model->insertTbgHistoryClose($inserHistoryCloseData);

						// SEND EMAIL
						if ($bgdata['CHANGE_TYPE_CLOSE'] == 1) {
							$titleChangeType = 'PENUTUPAN';
						} else if ($bgdata['CHANGE_TYPE_CLOSE'] == 3) {
							$titleChangeType = 'KLAIM';
						} else {
							$titleChangeType = 'PENUTUPAN / KLAIM';
						}

						$contractName = $this->handleDokumenUnderlying($bgdata['BG_REG_NUMBER']);

						$dataEmail = [
							'[[title_change_type]]' => $titleChangeType,
							'[[close_ref_number]]' => $bgdata['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $bgdata['BG_NUMBER'],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $bgdata["USAGE_PURPOSE_DESC"],
							'[[cust_name]]' => $bgdata["PRINCIPLE_NAME"],
							'[[recipient_name]]' => $bgdata['RECIPIENT_NAME'],
							'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($bgdata["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' => $arrbgcg[$bgdata['COUNTER_WARRANTY_TYPE']],
							'[[change_type]]' => $arrbgclosingType[$bgdata['CHANGE_TYPE_CLOSE']],
							'[[suggestion_status]]' => $arrbgclosingStatus[7],
							'[[repair_notes]]' => $reason ?: '-',
							'[[cust_cp]]' => $bgdata['CUST_CP'],
							'[[recipient_cp]]' => $bgdata['RECIPIENT_CP'],
							'[[master_bank_name]]' => $allSetting["master_bank_name"],
							'[[master_bank_email]]' => $allSetting["master_bank_email"],
							'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
							'[[master_app_name]]' => $allSetting["master_bank_app_name"],
						];

						if ($bgdata['CHANGE_TYPE_CLOSE'] == 3) {
							$getEmailTemplate = $allSetting['bemailtemplate_7A'];
							$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);
	
							$privi = $bgdata['COUNTER_WARRANTY_TYPE'] == '1' ? ['VPCC'] : ['VPNC', 'PADK'];
							$emailAddresses = $model->getBuserEmailWithPrivi($privi, $bgdata['BG_BRANCH']);
	
							$emailSubject = 'PENOLAKAN PENGAJUAN ' . $titleChangeType . ' BG NO. ' . $bgdata['BG_NUMBER'];
	
							// SEND EMAIL TO KANTOR CABANG PENERBIT
							if ($bgdata['BRANCH_EMAIL']) $this->sendEmail($bgdata['BRANCH_EMAIL'], $emailSubject, $getEmailTemplate);
	
							// SEND EMAIL TO GROUP KONTRA
							switch (true) {
								case $bgdata['COUNTER_WARRANTY_TYPE'] == '1':
									// SEND EMAIL TO GROUP CASH COL
									if ($allSetting['email_group_cashcoll']) $this->sendEmail($allSetting['email_group_cashcoll'], $emailSubject, $getEmailTemplate);
									break;
	
								case $bgdata['COUNTER_WARRANTY_TYPE'] == '2':
									// SEND EMAIL TO GROUP LINE FACILITY
									if ($allSetting['email_group_linefacility']) $this->sendEmail($allSetting['email_group_linefacility'], $emailSubject, $getEmailTemplate);
									break;
	
								case $bgdata['COUNTER_WARRANTY_TYPE'] == '3':
									// SEND EMAIL TO GROUP ASURANSI
									if ($allSetting['email_group_insurance']) $this->sendEmail($allSetting['email_group_insurance'], $emailSubject, $getEmailTemplate);
									break;
	
								default:
									# code...
									break;
							}

							// SEND EMAIL TO ASURANSI
							if ($bgdata['COUNTER_WARRANTY_TYPE'] == '3') {
								$branchFullName = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $insuranceBranch[0]["CUST_ID"])
									->query()->fetchAll();
		
								$dataInsurance = array_merge($dataEmail, [
									'[[insurance_name]]' => $branchFullName[0]['CUST_NAME'],
									'[[insurance_branch_name]]' => $insuranceBranch[0]["INS_BRANCH_NAME"],
								]);
	
								$getEmailTemplateInsurance = $allSetting['bemailtemplate_7D'];
								$getEmailTemplateInsurance = strtr($getEmailTemplateInsurance, $dataInsurance);
		
								$this->sendEmail($insuranceBranch[0]["INS_BRANCH_EMAIL"], 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN KLAIM BG NO. ' . $bgdata["BG_NUMBER"], $getEmailTemplateInsurance);
							}

						}

						// SEND EMAIL KE NASABAH / PRINSIPAL
						$getEmailTemplatePrincipal = $allSetting['bemailtemplate_7B'];
						$getEmailTemplatePrincipal = strtr($getEmailTemplatePrincipal, $dataEmail);

						if ($bgdata['CUST_EMAIL'] != '') $this->sendEmail($bgdata['CUST_EMAIL'], 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN ' . $titleChangeType . ' BG NO. ' . $bgdata["BG_NUMBER"], $getEmailTemplatePrincipal);

						// SEND EMAIL KE OBLIGEE
						$getEmailTemplateObligee = $allSetting['bemailtemplate_7C'];
						$getEmailTemplateObligee = strtr($getEmailTemplateObligee, $dataEmail);

						if ($bgdata['RECIPIENT_EMAIL'] != '') $this->sendEmail($bgdata['RECIPIENT_EMAIL'], 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN ' . $titleChangeType . ' BG NO. ' . $bgdata["BG_NUMBER"], $getEmailTemplateObligee);

						$this->setbackURL('/' . $this->_request->getModuleName() . '/approve/');
						return $this->_redirect('/notification/success');
					}

					// VERIFIKASI ULANG DATA
					if ($this->_request->getParam('reverikasi')) {
						$model->updateStatusClose($bgdata['CLOSE_REF_NUMBER'], '9');

						// LOG
						$logText = $this->language->_('Permintaan Verifikasi Ulang Pengajuan Penutupan untuk BG No : ' . $bgdata['BG_NUMBER'] . ', Principal : ' . $bgdata['CUST_ID'] . ', NoRef Pengajuan : ' . $bgdata['CLOSE_REF_NUMBER'] . ', Catatan : ' . $reason);
						$privLog = $bgdata['COUNTER_WARRANTY_TYPE'] == '1' ? 'RCCS' : 'RNCS';
						Application_Helper_General::writeLog($privLog, $logText);

						// HISTORY CLOSE
						$inserHistoryCloseData = array_intersect_key($bgdata, ['CLOSE_REF_NUMBER' => '', 'BG_NUMBER' => '', 'CUST_ID' => '-']);
						$inserHistoryCloseData['REASON_POST'] = $reason;
						$inserHistoryCloseData['HISTORY_STATUS'] = '9';
						$model->insertTbgHistoryClose($inserHistoryCloseData);

						// SEND EMAIL
						$getEmailTemplate = $allSetting['bemailtemplate_6A'];
						$contractName = $this->handleDokumenUnderlying($bgdata['BG_REG_NUMBER']);
						$titleChangeType = 'PENUTUPAN';

						$data = [
							'[[title_change_type]]' => $titleChangeType,
							'[[close_ref_number]]' => $bgdata['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $bgdata['BG_NUMBER'],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $bgdata["USAGE_PURPOSE_DESC"],
							'[[cust_name]]' => $bgdata["PRINCIPLE_NAME"],
							'[[recipient_name]]' => $bgdata['RECIPIENT_NAME'],
							'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($bgdata["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' => $arrbgcg[$bgdata['COUNTER_WARRANTY_TYPE']],
							'[[change_type]]' => $arrbgclosingType[$bgdata['CHANGE_TYPE_CLOSE']],
							'[[suggestion_status]]' => $arrbgclosingStatus[9],
							'[[repair_notes]]' => $reason,
						];

						$getEmailTemplate = strtr($getEmailTemplate, $data);

						$privi = $bgdata['COUNTER_WARRANTY_TYPE'] == '1' ? ['VVCC'] : ['VVNC', 'PADK'];
						$emailAddresses = $model->getBuserEmailWithPrivi($privi, $bgdata['BG_BRANCH']);

						foreach ($emailAddresses as $email) {
							$this->sendEmail($email, $arrbgclosingStatus[9] . ' BG NO. ' . $bgdata['BG_NUMBER'], $getEmailTemplate);
						}

						$this->setbackURL('/' . $this->_request->getModuleName() . '/approve/');
						return $this->_redirect('/notification/success');
					}

					// PERMINTAAN PERBAIKAN DATA
					if ($this->_request->getParam('perbaikan') && $bgdata['CHANGE_TYPE_CLOSE'] == '3') {
						$model->updateStatusClose($bgdata['CLOSE_REF_NUMBER'], '10');

						// LOG
						$logText = $this->language->_('Permintaan Perbaikan Pengajuan Klaim untuk BG No : ' . $bgdata['BG_NUMBER'] . ', Principal : ' . $bgdata['CUST_ID'] . ', NoRef Pengajuan : ' . $bgdata['CLOSE_REF_NUMBER'] . ', Catatan : ' . $reason);
						$privLog = $bgdata['COUNTER_WARRANTY_TYPE'] == '1' ? 'RCCS' : 'RNCS';
						Application_Helper_General::writeLog($privLog, $logText);

						// HISTORY CLOSE
						$inserHistoryCloseData = array_intersect_key($bgdata, ['CLOSE_REF_NUMBER' => '', 'BG_NUMBER' => '', 'CUST_ID' => '-']);
						$inserHistoryCloseData['REASON_POST'] = $reason;
						$inserHistoryCloseData['HISTORY_STATUS'] = '6';
						$model->insertTbgHistoryClose($inserHistoryCloseData);

						// SEND EMAIL
						$getEmailTemplate = $allSetting['bemailtemplate_6A'];
						$contractName = $this->handleDokumenUnderlying($bgdata['BG_REG_NUMBER']);
						$titleChangeType = 'KLAIM';

						$data = [
							'[[title_change_type]]' => $titleChangeType,
							'[[close_ref_number]]' => $bgdata['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $bgdata['BG_NUMBER'],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $bgdata["USAGE_PURPOSE_DESC"],
							'[[cust_name]]' => $bgdata["PRINCIPLE_NAME"],
							'[[recipient_name]]' => $bgdata['RECIPIENT_NAME'],
							'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($bgdata["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' => $arrbgcg[$bgdata['COUNTER_WARRANTY_TYPE']],
							'[[change_type]]' => $arrbgclosingType[$bgdata['CHANGE_TYPE_CLOSE']],
							'[[suggestion_status]]' => $arrbgclosingStatus[10],
							'[[repair_notes]]' => $reason,
						];

						$getEmailTemplate = strtr($getEmailTemplate, $data);

						$privi = $bgdata['COUNTER_WARRANTY_TYPE'] == '1' ? ['VPCC'] : ['VPNC', 'PADK'];
						// $emailAddresses2 = $model->getBuserEmailWithPrivi($privi, $bgdata['BG_BRANCH']);
						$emailAddresses =  $this->buserEmail($privi, $bgdata['BG_BRANCH']);

						foreach ($emailAddresses as $email) {
							$this->sendEmail($email['BUSER_EMAIL'], $arrbgclosingStatus[10] . ' BG NO. ' . $bgdata['BG_NUMBER'], $getEmailTemplate);
						}


						$this->setbackURL('/' . $this->_request->getModuleName() . '/approve/');
						return $this->_redirect('/notification/success');
					}

					// PERSETUJUAN DATA
					if ($this->_request->getParam('persetujuan')) {

						// CEK MATRIKS

						// SELECT USER GROUP
						$selectusergroup	= $this->_db->select()
							->from(array('C' => 'M_APP_GROUP_BUSER'), array(
								'*'
							))
							->where("C.BUSER_ID 	= ?", (string) $this->_userIdLogin);
						$usergroup = $this->_db->fetchAll($selectusergroup);

						$TRANSFER_TYPE =  $bgdata['COUNTER_WARRANTY_TYPE'] == 1 ? '50' : '51';
						$selectuser	= $this->_db->select()
							->from(array('C' => 'M_APP_BGBOUNDARY'), array(
								'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
								'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
								'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
								'C.TRANSFER_TYPE',
								'C.POLICY'
							))
							->where("C.TRANSFER_TYPE 	= ?", (string) $TRANSFER_TYPE)
							->where("C.BOUNDARY_MIN 	<= ?", $bgdata['BG_AMOUNT'])
							->where("C.BOUNDARY_MAX 	>= ?", $bgdata['BG_AMOUNT']);
						$datauser = $this->_db->fetchAll($selectuser);

						if (!empty($usergroup)) {

							$cek = false;
							foreach ($usergroup as $key => $value) {
								$group = explode('_', $value['GROUP_BUSER_ID']);
								$alphabet = array_combine(range(1, count(range('A', 'Z'))), array_values(range('A', 'Z')));
								$groupalfa = $alphabet[intval($group[2])];
								$usergroup[$key]['GROUP'] = $groupalfa;

								foreach ($datauser as $nub => $val) {
									$command = str_replace('(', '', $val['POLICY']);
									$command = str_replace(')', '', $command);
									$list = explode(' ', $command);

									foreach ($list as $row => $data) {
										if ($data == $groupalfa) {
											$groupuser = $data;
											$cek = true;
											break;
										}
									}
								}
							}

							if ($group[0] == 'S') {
								$cek = true;
								$groupuser = 'SG';
							}
							if (empty($datauser)) {
								$groupuser = '-';
							} else if (!$cek) {
								// return false;
							}

							$verifierInsert = array(
								'REG_NUMBER'  			=> $bgdata['CLOSE_REF_NUMBER'],
								'CUST_ID' 				=> $this->_custIdLogin,
								'USER_ID' 				=> $this->_userIdLogin,
								'GROUP' 				=> $groupuser,
								'CREATED' 				=> new Zend_Db_Expr("now()")
							);

							$this->_db->insert('T_BGAPPROVAL', $verifierInsert);

							if (!empty($datauser)) {
								$approve = $this->validateapproval($datauser['0']['POLICY'], $list, $bgdata['CLOSE_REF_NUMBER']);
							} else {
								$approve = true;
							}
						}

						if ($group[0] == 'S') {
							$approve = true;
						}

						// INSERT HISTORY
						$historyInsert = array(
							'CLOSE_REF_NUMBER'	=> $bgdata['CLOSE_REF_NUMBER'],
							'BG_NUMBER'   		=> $bgdata['BG_NUMBER'],
							'CUST_ID'           => $bgdata['CUST_ID'],
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'USER_LOGIN'        => $this->_userIdLogin,
							'USER_FROM'         => 'BANK',
							'BG_REASON'			=> ($approve) ? $this->language->_('NoRef Penyelesaian : ') . $bgdata['CLOSE_REF_NUMBER'] : $this->language->_('NoRef Pengajuan : ') . $bgdata['CLOSE_REF_NUMBER'],
							'HISTORY_STATUS'    => ($bgdata['CHANGE_TYPE_CLOSE'] == '1') ? 11 : ($bgdata['CHANGE_TYPE_CLOSE'] == '3' ? 12 : '')
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						// JIKA MATRIKS SUDAH TERPENUHI
						if ($approve) {
							// EXECUTE CODE TO APPROVE

							$getCloseData = $this->_db->select()
								->from('TEMP_BANK_GUARANTEE_CLOSE')
								->where('CLOSE_REF_NUMBER = ?', $bgdata['CLOSE_REF_NUMBER'])
								->query()->fetch();

							// HAPUS DATA DARI TEMP DAN UPDATE DATA KE TABEL T_BANK_GUARANTEE

							$updateData = $bgdata['CHANGE_TYPE_CLOSE'] == '1' ? [
								'BG_STATUS' => '16',
								'CLOSING_TYPE' => $bgdata['CHANGE_TYPE_CLOSE'],
								'CLOSE_REF_NUMBER' => $bgdata['CLOSE_REF_NUMBER'],
								'BG_OFFRISK_DATE' => new Zend_Db_Expr('now()'),
								'BG_UPDATED' => new Zend_Db_Expr('now()'),
								'BG_UPDATEDBY' => 'SYSTEM'
							] : ($bgdata['CHANGE_TYPE_CLOSE'] == '3' ? [
								'BG_STATUS' => '16',
								'CLOSING_TYPE' => $bgdata['CHANGE_TYPE_CLOSE'],
								'CLOSE_REF_NUMBER' => $bgdata['CLOSE_REF_NUMBER'],
								'CLAIM_FIRST_VERIFIED' => $getCloseData['CLAIM_FIRST_VERIFIED'],
								'BG_OFFRISK_DATE' => new Zend_Db_Expr('now()'),
								'BG_UPDATED' => new Zend_Db_Expr('now()'),
								'BG_UPDATEDBY' => 'SYSTEM'
							] : '');

							$this->_db->update('T_BANK_GUARANTEE', $updateData, [
								'BG_NUMBER = ?' => $bgdata['BG_NUMBER']
							]);

							// HAPUS DATA TEMP
							$this->_db->delete('TEMP_BANK_GUARANTEE_CLOSE', [
								'BG_NUMBER = ?' => $bgdata['BG_NUMBER']
							]);

							// CEK REKENING TUJUAN PELEPASAN MD
							// GET ESCROW RELEASE
							$getEscrowRelease = $this->_db->select()
								->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
								->where('CLOSE_REF_NUMBER = ?', $bgdata['CLOSE_REF_NUMBER'])
								->query()->fetchAll();

							// SERVICE COUNTER FOR T_BG_TRANSACTION
							$serviceCount = 1;

							// IF ESCROW RELEASE EXIST
							if ($getEscrowRelease) {

								// DETAIL ESCROW
								// $getEscrowAcct = array_shift(array_filter($getSplit, function ($item) {
								// 	return strtolower($item['ACCT_DESC']) == 'escrow';
								// }));
								// $getEscrowAcct = $getEscrowAcct['ACCT'];

								// $escrowService = new Service_Account($getEscrowAcct, '');
								// $getCifEscrow = $escrowService->inquiryAccontInfo()['cif'];

								// $cifEscrowService = new Service_Account('', '', '', '', '', $getCifEscrow);
								// $listAcctCifEscrow = $cifEscrowService->inquiryCIFAccount();
								// $currEscrow = 'IDR'; // DEFAULT IDR JIKA RESPONSE CORE TIDAK BISA

								// if ($listAcctCifEscrow['response_code'] == '0000') {
								// 	$listAcct = $listAcctCifEscrow['accounts'];

								// 	$detailEscrow = array_shift(array_filter($listAcct, function ($item) use ($getEscrowAcct) {
								// 		return strpos($item['account_number'], $getEscrowAcct) !== false;
								// 	}));

								// 	$currEscrow = $detailEscrow['currency'];
								// }

								// $inqBalanceEscrow = $escrowService->inquiryAccountBalance();

								// // INFO ESCROW
								// $acctNameEscrow = $inqBalanceEscrow['account_name'];
								// $accountTypeEscrow = $inqBalanceEscrow['account_type'] ?: $detailEscrow['type'];
								// $productTypeEscrow = $inqBalanceEscrow['product_type'] ?: $detailEscrow['product_type'];

								// $count = 1;
								// while (true) {

								// 	$getIndexAcct = array_filter($getEscrowRelease, function ($item) use ($count) {
								// 		return $item['ACCT_INDEX'] == $count;
								// 	});

								// 	// JIKA REKENING TIDAK ADA
								// 	if (!$getIndexAcct) break;

								// 	$parseData = array_combine(array_map(function ($item) {
								// 		return str_replace(' ', '_', strtolower($item));
								// 	}, array_column($getIndexAcct, 'PS_FIELDNAME')), array_column($getIndexAcct, 'PS_FIELDVALUE'));

								// 	$benefService = new Service_Account($parseData['beneficiary_account_number'], '');
								// 	$benefInfo = $benefService->inquiryAccontInfo();

								// 	$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
								// 		'CLOSE_REF_NUMBER' => $bgdata['CLOSE_REF_NUMBER'],
								// 		'ACCT_INDEX' => $count,
								// 		'PS_FIELDNAME' => 'Remark 1',
								// 		'PS_FIELDVALUE' => ($bgdata['CHANGE_TYPE_CLOSE'] == '1') ? ('Pelepasan MD BG No. ' . $bgdata['BG_NUMBER']) : (($bgdata['CHANGE_TYPE_CLOSE'] == '3') ? ('Klaim BG No. ' . $bgdata['BG_NUMBER']) : '')
								// 	]);

								// 	$this->_db->insert('T_BG_TRANSACTION', [
								// 		'TRANSACTION_ID' => $bgdata['CLOSE_REF_NUMBER'] . str_pad($serviceCount++, 2, '0', STR_PAD_LEFT),
								// 		'CLOSE_REF_NUMBER' => $bgdata['CLOSE_REF_NUMBER'],
								// 		'BG_NUMBER' => $bgdata['BG_NUMBER'],
								// 		'SERVICE' => $serviceCount,
								// 		'SOURCE_ACCT' => $getEscrowAcct,
								// 		'SOURCE_ACCT_NAME' => $acctNameEscrow,
								// 		'SOURCE_ACCT_TYPE' => $accountTypeEscrow,
								// 		'SOURCE_PRODUCT_TYPE' => $productTypeEscrow,
								// 		'SOURCE_ACCT_CCY' => $currEscrow,
								// 		'BENEF_ACCT' => $parseData['beneficiary_account_number'],
								// 		'TRA_CCY' => $parseData['transaction_ccy'],
								// 		'TRA_AMOUNT' => $parseData['transaction_amount'],
								// 		'UPDATE_SUGGESTED' => new Zend_Db_Expr('now()'),
								// 		'UPDATE_SUGGESTEDBY' => $this->_userIdLogin,
								// 		'BENEF_ACCT_NAME' => $parseData['beneficiary_account_name'],
								// 		'BENEF_PHONE' => $benefInfo['phone_number']
								// 	]);

								// 	$count++;
								// }

								// // INSERT KE T_BG_TRANSACTION UNTUK DEPOSITO DAN SAVING TANPA DI UNLOCK
								// foreach ($getSplit as $key => $value) {

								// 	$callService = new Service_Account($value['ACCT'], '');

								// 	$infoAcct = $callService->inquiryAccontInfo();
								// 	$saveCIF = $infoAcct['cif'];
								// 	$serviceCIF = new Service_Account('', '', '', '', '', $saveCIF);
								// 	$getAllAcc = $serviceCIF->inquiryCIFAccount();

								// 	if ($getAllAcc['response_code'] == '0000') {
								// 		$getDetailAcc = array_shift(array_filter($getAllAcc['accounts'], function ($item) use ($value) {
								// 			return ltrim($item['account_number'], '0') == ltrim($value['ACCT'], '0');
								// 		}));
								// 	} else {
								// 		$getDetailAcc = [];
								// 	}

								// 	if (strtolower($getDetailAcc['type_desc']) == 'giro') continue;

								// 	// JIKA DEPOSITO ATAU TABUNGAN
								// 	// SIMPAN KE T_BG_TRANSACTION
								// 	$this->_db->insert('T_BG_TRANSACTION', [
								// 		'TRANSACTION_ID' => $bgdata['CLOSE_REF_NUMBER'] . str_pad($serviceCount++, 2, '0', STR_PAD_LEFT),
								// 		'CLOSE_REF_NUMBER' => $bgdata['CLOSE_REF_NUMBER'],
								// 		'BG_NUMBER' => $bgdata['BG_NUMBER'],
								// 		'SERVICE' => $serviceCount,
								// 		'SOURCE_ACCT' => $value['ACCT'],
								// 		'SOURCE_ACCT_NAME' => $infoAcct['account_name'],
								// 		'SOURCE_ACCT_TYPE' => $getDetailAcc['type'],
								// 		'SOURCE_PRODUCT_TYPE' => $getDetailAcc['product_type'],
								// 		'SOURCE_ACCT_CCY' => $getDetailAcc['currency'],
								// 		'TRA_AMOUNT' => $value['AMOUNT'],
								// 		'UPDATE_SUGGESTED' => new Zend_Db_Expr('now()'),
								// 		'UPDATE_SUGGESTEDBY' => $this->_userIdLogin,
								// 	]);
								// }
							}

							// JIKA TIDAK ADA ESCROW UNLOCK DEPOSITO DAN SAVING JIKA ADA
							else {
								// foreach ($getSplit as $key => $value) {

								// 	$callService = new Service_Account($value['ACCT'], '');

								// 	$infoAcct = $callService->inquiryAccontInfo();
								// 	$saveCIF = $infoAcct['cif'];
								// 	$serviceCIF = new Service_Account('', '', '', '', '', $saveCIF);
								// 	$getAllAcc = $serviceCIF->inquiryCIFAccount();

								// 	if ($getAllAcc['response_code'] == '0000') {
								// 		$getDetailAcc = array_shift(array_filter($getAllAcc['accounts'], function ($item) use ($value) {
								// 			return ltrim($item['account_number'], '0') == ltrim($value['ACCT'], '0');
								// 		}));
								// 	} else {
								// 		$getDetailAcc = [];
								// 	}

								// 	if (strtolower($getDetailAcc['type_desc']) == 'giro') continue;

								// 	// jika deposito
								// 	if (strtolower($getDetailAcc['type_desc']) == 'deposito') {
								// 		$inqbalance = $callService->inquiryDeposito();

								// 		// JIKA REKENING TERHOLD
								// 		if (strtolower($inqbalance['hold_description']) == 'y') {
								// 			$callServiceUnlockDepo = new Service_Account($value['ACCT'], 'IDR');
								// 			$callServiceUnlockDepo->unlockDeposito('T', $value['HOLD_BY_BRANCH'], $value['HOLD_SEQUENCE']);
								// 		}
								// 	}
								// 	// end jika deposito

								// 	// jika tabungan
								// 	else {
								// 		$inqbalance = $callService->inqLockSaving();

								// 		$cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
								// 			return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
								// 		}));

								// 		// JIKA DITEMUKAN LAKUKAN ACTION
								// 		if ($cekSaving) {
								// 			$callServiceUnlockSaving = new Service_Account($value['ACCT'], 'IDR');
								// 			$callServiceUnlockSaving->unlockSaving([
								// 				'branch_code' => $value['HOLD_BY_BRANCH'],
								// 				'sequence_number' => $value['HOLD_SEQUENCE']
								// 			]);
								// 		}
								// 	}
								// 	// end jika tabungan

								// 	// JIKA DEPOSITO ATAU TABUNGAN
								// 	// SIMPAN KE T_BG_TRANSACTION
								// 	$this->_db->insert('T_BG_TRANSACTION', [
								// 		'TRANSACTION_ID' => $bgdata['CLOSE_REF_NUMBER'] . str_pad($serviceCount++, 2, '0', STR_PAD_LEFT),
								// 		'CLOSE_REF_NUMBER' => $bgdata['CLOSE_REF_NUMBER'],
								// 		'BG_NUMBER' => $bgdata['BG_NUMBER'],
								// 		'SERVICE' => $serviceCount,
								// 		'SOURCE_ACCT' => $value['ACCT'],
								// 		'SOURCE_ACCT_NAME' => $infoAcct['account_name'],
								// 		'SOURCE_ACCT_TYPE' => $getDetailAcc['type'],
								// 		'SOURCE_PRODUCT_TYPE' => $getDetailAcc['product_type'],
								// 		'SOURCE_ACCT_CCY' => $getDetailAcc['currency'],
								// 		'TRA_AMOUNT' => $value['AMOUNT'],
								// 		'UPDATE_SUGGESTED' => new Zend_Db_Expr('now()'),
								// 		'UPDATE_SUGGESTEDBY' => $this->_userIdLogin,
								// 	]);
								// }
							}

							// INSERT PSLIP
							$this->_db->insert('T_BG_PSLIP', [
								'CLOSE_REF_NUMBER' => $bgdata['CLOSE_REF_NUMBER'],
								'BG_NUMBER' => $bgdata['BG_NUMBER'],
								'PS_STATUS' => '1',
								'TYPE' => $getCloseData['CHANGE_TYPE']
							]);

							// SEND EMAIL
							$bankName = $model->getBankName($bgdata['CLOSE_REF_NUMBER']);
							$benefAcctNumber = array_shift(array_filter($getEscrowRelease, function ($item) {
								return strtolower($item['PS_FIELDNAME']) == 'beneficiary account number';
							}))['PS_FIELDVALUE'];

							$benefAcctName = array_shift(array_filter($getEscrowRelease, function ($item) {
								return strtolower($item['PS_FIELDNAME']) == 'beneficiary account name';
							}))['PS_FIELDVALUE'];

							$transAmount = array_shift(array_filter($getEscrowRelease, function ($item) {
								return strtolower($item['PS_FIELDNAME']) == 'transaction amount';
							}))['PS_FIELDVALUE'];

							$contractName = $this->handleDokumenUnderlying($bgdata['BG_REG_NUMBER']);
							$bgStatus = $updateData['BG_STATUS'] == '16' ? 'BERAKHIR' : 'AKTIF';

							$data = [
								'[[close_ref_number]]' => $updateData['CLOSE_REF_NUMBER'],
								'[[bg_number]]' => $bgdata['BG_NUMBER'],
								'[[cust_cp]]' => $bgdata["CUST_CP"],
								'[[cust_name]]' => $bgdata["PRINCIPLE_NAME"],
								'[[recipient_cp]]' => $bgdata['RECIPIENT_CP'],
								'[[recipient_name]]' => $bgdata['RECIPIENT_NAME'],
								'[[counter_warranty_type]]' => $arrbgcg[$bgdata['COUNTER_WARRANTY_TYPE']],
								'[[change_type]]' => $arrbgclosingType[$bgdata['CHANGE_TYPE_CLOSE']],
								'[[suggestion_status]]' => $arrbgclosingStatus[3],
								'[[repair_notes]]' => $reason ?: '-',
								'[[usage_purpose]]' => ucwords(strtolower($bgdata['USAGE_PURPOSE_DESC'])),
								'[[time_period_start]]' => date('d M Y', strtotime($bgdata['TIME_PERIOD_START'])),
								'[[time_period_end]]' => date('d M Y', strtotime($bgdata['TIME_PERIOD_END'])),
								'[[settlement_ref_number]]' => $bgdata['CLOSE_REF_NUMBER'],
								'[[bank_name]]' => $bankName,
								'[[beneficiary_account_number]]' => $benefAcctNumber,
								'[[beneficiary_account_name]]' => $benefAcctName,
								'[[transaction_amount]]' => $this->view->bgCurrency . ' ' . number_format($transAmount, 2),
								'[[contract_name]]' => $contractName,
								'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($bgdata['BG_AMOUNT'], 2),
								'[[bg_status]]' => $bgStatus,
								'[[bg_offrisk_date]]' => date('d-m-Y'),
								'[[closing_type]]' => $arrbgclosingType[$bgdata['CHANGE_TYPE_CLOSE']],
								'[[master_app_name]]' => $allSetting["master_bank_app_name"],
							];

							if ($bgdata['CHANGE_TYPE_CLOSE'] == '1') {
								$data['[[title_counter_type]]'] = 'Jaminan';

								$getEmailTemplatePrincipal = $allSetting['bemailtemplate_0A'];
								$getEmailTemplatePrincipal = strtr($getEmailTemplatePrincipal, $data);
	
								$getEmailTemplateObligee = $allSetting['bemailtemplate_0B'];
								$getEmailTemplateObligee = strtr($getEmailTemplateObligee, $data);
	
								$getEmailTemplateBank = $allSetting['bemailtemplate_0C'];
								$getEmailTemplateBank = strtr($getEmailTemplateBank, $data);
	
								$emailSubject = 'PEMBERITAHUAN BERAKHIR BG NO. ' . $bgdata['BG_NUMBER'];

							} else if ($bgdata['CHANGE_TYPE_CLOSE'] == '3') {
								$getEmailTemplatePrincipal = $allSetting['bemailtemplate_5A'];
								$getEmailTemplatePrincipal = strtr($getEmailTemplatePrincipal, $data);
	
								$getEmailTemplateObligee = $allSetting['bemailtemplate_5B'];
								$getEmailTemplateObligee = strtr($getEmailTemplateObligee, $data);
	
								$getEmailTemplateBank = $allSetting['bemailtemplate_5C'];
								$getEmailTemplateBank = strtr($getEmailTemplateBank, $data);
	
								$emailSubject = 'PERSETUJUAN KLAIM BG NO. ' . $bgdata['BG_NUMBER'];
							}

							// SEND EMAIL TO PRINCIPAL
							if ($bgdata['CUST_EMAIL']) $this->sendEmail($bgdata['CUST_EMAIL'], $emailSubject, $getEmailTemplatePrincipal);

							// SEND EMAIL TO OBLIGEE
							if ($bgdata['RECIPIENT_EMAIL']) $this->sendEmail($bgdata['RECIPIENT_EMAIL'], $emailSubject, $getEmailTemplateObligee);

							// SEND EMAIL TO KANTOR CABANG PENERBIT
							if ($bgdata['BRANCH_EMAIL']) $this->sendEmail($bgdata['BRANCH_EMAIL'], $emailSubject, $getEmailTemplateBank);

							switch (true) {
								case $bgdata['COUNTER_WARRANTY_TYPE'] == '1':
									// SEND EMAIL TO GROUP CASH COL
									if ($allSetting['email_group_cashcoll']) $this->sendEmail($allSetting['email_group_cashcoll'], $emailSubject, $getEmailTemplateBank);
									break;

								case $bgdata['COUNTER_WARRANTY_TYPE'] == '2':
									// SEND EMAIL TO GROUP LINE FACILITY
									if ($allSetting['email_group_linefacility']) $this->sendEmail($allSetting['email_group_linefacility'], $emailSubject, $getEmailTemplateBank);
									break;

								case $bgdata['COUNTER_WARRANTY_TYPE'] == '3':
									// SEND EMAIL TO GROUP ASURANSI
									if ($allSetting['email_group_insurance']) $this->sendEmail($allSetting['email_group_insurance'], $emailSubject, $getEmailTemplateBank);
									break;

								default:
									# code...
									break;
							}

							// SEND EMAIL TO ASURANSI
							if ($bgdata['COUNTER_WARRANTY_TYPE'] == '3') {
								$branchFullName = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $insuranceBranch[0]["CUST_ID"])
									->query()->fetchAll();
								
								$dataInsurance = array_merge($data, [
									'[[insurance_name]]' => $branchFullName[0]['CUST_NAME'],
									'[[insurance_branch_name]]' => $insuranceBranch[0]["INS_BRANCH_NAME"],
									'[[title_counter_type]]' => 'MD',
								]);

								$getEmailTemplateInsurance = $bgdata['CHANGE_TYPE_CLOSE'] == '1' ? $allSetting['bemailtemplate_0D'] : $allSetting['bemailtemplate_5D'];
								$getEmailTemplateInsurance = strtr($getEmailTemplateInsurance, $dataInsurance);
								
								if ($insuranceBranch[0]['INS_BRANCH_EMAIL']) $this->sendEmail($insuranceBranch[0]['INS_BRANCH_EMAIL'], $emailSubject, $getEmailTemplateInsurance);
							}

							// LOG
							$text = ($bgdata['CHANGE_TYPE_CLOSE'] == '1') ? 'Persetujuan Pengajuan Penutupan' : ($bgdata['CHANGE_TYPE_CLOSE'] == '3' ? 'Persetujuan Pengajuan Klaim' : '');
							$logText = $this->language->_($text . ' untuk BG No : ' . $bgdata['BG_NUMBER'] . ', Principal : ' . $bgdata['CUST_ID'] . ', NoRef Pengajuan : ' . $bgdata['CLOSE_REF_NUMBER']);
							$privLog = $bgdata['COUNTER_WARRANTY_TYPE'] == '1' ? 'ACCS' : 'ANCS';
							Application_Helper_General::writeLog($privLog, $logText);
						}

						$this->setbackURL('/' . $this->_request->getModuleName() . '/approve/');
						return $this->_redirect('/notification/success');
					}
				}
			} else {
				return $this->_redirect('/bgclosingworkflow/approve');
			}

			if ($bgdata['CHANGE_TYPE_CLOSE'] == '3') {
				$this->render('claimobligee');
			}
		}
	}

	function handleDokumenUnderlying($bgRegNumber) {
		$dokumenUnderlying = $this->_db->select()
			->from('T_BANK_GUARANTEE_UNDERLYING')
			->where('BG_REG_NUMBER = ?', $bgRegNumber)
			->query()->fetchAll();
		
		if (empty($dokumenUnderlying)) {
			return '-';
		}
		
		$docName = array_column($dokumenUnderlying, 'DOC_NAME');
		$combinedDocNames = implode(', ', $docName);

		if (strlen($combinedDocNames) > 90) {
			$combinedDocNames = substr($combinedDocNames, 0, 90) . '...';
		}

		$combinedDocNames .= ' (total ' . count($dokumenUnderlying) . ' dokumen)';
		return $combinedDocNames;
	}

	function buserEmail($bprivi, $bgBranch) {
		$bpriviGroup = $this->_db->select()
			->from('M_BPRIVI_GROUP')
			->where('BPRIVI_ID IN (?)', $bprivi)
			->query()->fetchAll();

		$bpriviGroups = array_column($bpriviGroup, 'BGROUP_ID');

		$busers = $this->_db->select()
			->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
			->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
			->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
			->where('MB.BGROUP_ID IN (?)', $bpriviGroups)
			->where('MBR.BRANCH_CODE = ?', $bgBranch)
			->query()->fetchAll();
		
		return $busers;
	}

	private function combineCodeAndDesc($arrayCode, $arrayDesc)
	{
		return array_combine($arrayCode, array_map(function ($arrayMap) use ($arrayDesc, $arrayCode) {
			return $arrayDesc[$arrayMap];
		}, array_keys($arrayCode)));
	}

	private function sendEmail($emailAddress, $subjectEmail, $emailTemplate)
	{
		$setting = new Settings();
		$allSetting = $setting->getAllSetting();

		$data = [
			'[[master_bank_name]]' => $allSetting["master_bank_name"],
			'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_bank_email]]' => $allSetting["master_bank_email"],
			'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
		];

		$getEmailTemplate = strtr($emailTemplate, $data);
		Application_Helper_Email::sendEmail($emailAddress, $subjectEmail, $getEmailTemplate);
	}

	public function showcertificateAction()
	{
		$this->_helper->layout()->disableLayout();

		$AESMYSQL = new Crypt_AESMYSQL();
		$bgnumbDecode = urldecode($this->_getParam('bgnumb'));
		$bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

		$this->view->bgnumbDecode = $bgnumbDecode;

		$getDocId = $this->_db->select()
			->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
			->where('BG_NUMBER = ?', $bgnumbDecrypt)
			->query()->fetch();

		$this->view->bgNumb = $getDocId['BG_NUMBER'];

		$this->render('showcertificate');
	}

	public function showcertAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();
		$bgnumbDecode = urldecode($this->_getParam('bgnumb'));
		$bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

		if (!$bgnumbDecrypt) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		$getDocId = $this->_db->select()
			->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
			->where('BG_NUMBER = ?', $bgnumbDecrypt)
			->query()->fetch();

		$pathFile = UPLOAD_PATH . '/document/submit/';
		$getFile = $pathFile . '/' . $getDocId['DOCUMENT_ID'] . '.pdf';

		if (!file_exists($getFile)) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=BG_" . $getDocId['BG_NUMBER'] . '.pdf' . "");

		readfile($getFile);
		return true;
	}

	public function downloadcertificateAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();
		$bgnumbDecode = urldecode($this->_getParam('bgnumb'));
		$bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

		if (!$bgnumbDecrypt) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		$getDocId = $this->_db->select()
			->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
			->where('BG_NUMBER = ?', $bgnumbDecrypt)
			->query()->fetch();

		$pathFile = UPLOAD_PATH . '/document/submit/';
		$getFile = $pathFile . '/' . $getDocId['DOCUMENT_ID'] . '.pdf';

		if (!file_exists($getFile)) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		header("Content-type: application/pdf");
		header("Content-Disposition: attachtment; filename=BG_" . $getDocId['BG_NUMBER'] . '.pdf' . "");

		readfile($getFile);
		return true;
	}

	public function downloadlampiranAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();
		$bgnumbDecode = urldecode($this->_getParam('bgnumb'));
		$bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

		$jenis = $this->_request->getParam('jenis');
		$jenis = str_replace(['_'], ' ', $jenis);

		if (!$bgnumbDecrypt) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		$closeFile = $this->_db->select()
			->from(['TBG' => 'T_BANK_GUARANTEE'], ['BG_NUMBER'])
			->joinLeft(['TBGC' => 'TEMP_BANK_GUARANTEE_CLOSE'], 'TBG.BG_NUMBER = TBGC.BG_NUMBER OR TBG.BG_REG_NUMBER = TBGC.BG_REG_NUMBER', [''])
			->joinLeft(['TBGFC' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'], 'TBGC.CLOSE_REF_NUMBER = TBGFC.CLOSE_REF_NUMBER', ['*'])
			->where('TBG.BG_NUMBER = ?', $bgnumbDecrypt)
			->where('LOWER(FILE_NOTE) = ?', $jenis)
			->query()->fetch();

		$pathFile = UPLOAD_PATH . '/document/closing/';
		$getFile = $pathFile . '/' . $closeFile['BG_FILE'];

		if (!file_exists($getFile)) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		$filename = trim(str_replace([explode('_', $closeFile['BG_FILE'])[0], explode('_', $closeFile['BG_FILE'])[1], explode('_', $closeFile['BG_FILE'])[2], explode('_', $closeFile['BG_FILE'])[3]], '', $closeFile['BG_FILE']), '_');

		header("Content-type: application/pdf");
		header("Content-Disposition: attachtment; filename=BG_" . $closeFile['BG_NUMBER'] . '_' . $filename . "");

		readfile($getFile);
		return true;
	}

	public function mdfcAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decodeBgRegNumber = urldecode($bgRegNumber);
		$bgRegNumber = $AESMYSQL->decrypt($decodeBgRegNumber, uniqid());

		if ($bgRegNumber == null) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'nothing']);
			return true;
		}

		$model = new bgclosingworkflow_Model_Approvedetailclosing();

		$bgdatasplit = $model->getBgSplit($bgRegNumber);

		$mdAmount = 0;

		$haveEscrow = false;

		$htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

		$noMd =
			"
			<tr>
				<td colspan='6' class='text-center'>" . $this->language->_('Tidak ada marginal deposit') . "</td>
			</tr>
			";

		if ($bgdatasplit) {
			$countShow = 0;
			foreach ($bgdatasplit as $key => $value) {

				$callService = new Service_Account($value['ACCT'], '');

				$saveCIF = $callService->inquiryAccontInfo()['cif'];
				$serviceCIF = new Service_Account('', '', '', '', '', $saveCIF);
				$getAllAcc = $serviceCIF->inquiryCIFAccount();

				if ($getAllAcc['response_code'] == '0000') {
					$getDetailAcc = array_shift(array_filter($getAllAcc['accounts'], function ($item) use ($value) {
						return ltrim($item['account_number'], '0') == ltrim($value['ACCT'], '0');
					}));
				} else {
					$getDetailAcc = [];
				}

				if (strtolower($getDetailAcc['type_desc']) == 'giro' && strtolower($getDetailAcc['product_type']) != 'k2') continue;

				if (strtolower($value['ACCT_DESC']) == 'escrow') {
					$haveEscrow = true;
					$inqbalance = $callService->inquiryAccountBalance();

					// if (intval($inqbalance['available_balance']) == 0) {
					// 	// hapus data atau ??
					// 	continue;
					// } else
					$mdAmount += $value['AMOUNT'];
				} else {
					// jika deposito
					if (strtolower($getDetailAcc['type_desc']) == 'deposito') {
						$inqbalance = $callService->inquiryDeposito();

						// if (strtolower($inqbalance['hold_description']) == 'n') {
						// 	// hapus data atau ??
						// 	continue;
						// }
						// else
						$mdAmount += $value['AMOUNT'];
					}
					// end jika deposito

					// jika tabungan
					else {
						$inqbalance = $callService->inqLockSaving();

						$cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
							return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
						}));

						// if (!$cekSaving) {
						// 	// hapus data atau
						// 	continue;
						// } else
						$mdAmount += $value['AMOUNT'];
					}
					// end jika tabungan
				}

				$countShow += 1;
				$htmlResult .= "
				<tr>
					<td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
					<td>" . $value['ACCT'] . "</td>
					<td>" . $value['NAME'] . "</td>
					<td>" . ($getDetailAcc['type_desc'] . ' ' . $getDetailAcc['product_type']) . "</td>
					<td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($getDetailAcc['currency'])) . "</td>
					<td>" . number_format($value['AMOUNT'], 2) . "</td>
				</tr>";
			}

			if ($countShow === 0) {
				$htmlResult .= $noMd;
			}
		} else {
			$htmlResult .= $noMd;
		}

		$htmlResult .= "</tbody></table>";
		$htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksis'></input>";
		$htmlResult .= "<input type='hidden' value=" . $haveEscrow . " id='haveescrow'></input>";

		echo $htmlResult;
	}

	public function escrowreleaseAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decodeBgRegNumber = urldecode($bgRegNumber);
		$bgRegNumber = $AESMYSQL->decrypt($decodeBgRegNumber, uniqid());

		if ($bgRegNumber == null) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'nothing']);
			return true;
		}

		$model = new bgclosingworkflow_Model_Approvedetailclosing();

		$detailBg = $model->getDetailBg('', $bgRegNumber);

		$getEscrowRelease = $model->getEscrowRelease($detailBg['CLOSE_REF_NUMBER']);

		if (!$getEscrowRelease) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'nothing']);
			return true;
		}

		$saveAcct = [];
		$count = 1;
		while (true) {

			$getIndexAcct = array_filter($getEscrowRelease, function ($item) use ($count) {
				return $item['ACCT_INDEX'] == $count;
			});

			// JIKA REKENING TIDAK ADA
			if (!$getIndexAcct) break;

			$parseData = array_combine(array_map(function ($item) {
				return str_replace(' ', '_', strtolower($item));
			}, array_column($getIndexAcct, 'PS_FIELDNAME')), array_column($getIndexAcct, 'PS_FIELDVALUE'));

			$saveAcct[] = $parseData;

			$count++;
		}

		$totalEscrowAmount = array_sum(array_column($saveAcct, 'transaction_amount'));

		$htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Rek Sendiri') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

		foreach ($saveAcct as $key => $value) {

			$htmlResult .= "
            <tr>
                <td>" . $value['beneficiary_is_own_account'] . "</td>
                <td>" . $value['beneficiary_account_number'] . "</td>
                <td>" . $value['beneficiary_account_name'] . "</td>
                <td>" . $value['beneficiary_account_ccy'] . "</td>
                <td>" . number_format($value['transaction_amount'], 2) . "</td>
            </tr>";
		}

		$htmlResult .= "</tbody></table>";
		$htmlResult .= "<input type='hidden' value='" . $totalEscrowAmount . "' id='totalescrowamount'></input>";

		echo $htmlResult;
	}

	public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BGBOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))

			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount)
			->limit(1);

		$datauser = $this->_db->fetchAll($selectuser);

		// JIKA TIDAK ADA MATRIKS RETURN TRUE
		if (empty($datauser)) return true;

		// AMBIL GROUP BUSER
		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_BUSER'), array(
				'*'
			))
			->where("C.BUSER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);

		$this->view->boundarydata = $datauser;

		// JIKA GROUP BUSER EXIST
		if (!empty($usergroup)) {

			$cek = false;

			foreach ($usergroup as $key => $value) {

				// PARSE GROUP BUSER ID
				$group = explode('_', $value['GROUP_BUSER_ID']);

				// ALPHABET GROUP BUSER ID
				$alphabet = array_combine(range(1, count(range('A', 'Z'))), array_values(range('A', 'Z')));

				// AMBIL ALPHABET SESUAI KODE GROUP
				$groupalfa = $alphabet[(int) $group[2]];

				// INSERT GROUP ALPHABET KE GROUP BUSER
				$usergroup[$key]['GROUP'] = $groupalfa;

				foreach ($datauser as $nub => $val) {
					// CLEANSING STRING
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);

					$list = explode(' ', $command);

					// CEK APAKAH USER TERMASUK KEDALAM MATRIKS
					foreach ($list as $row => $data) {
						if ($data == $groupalfa) {
							$cek = true;
							break;
						}
					}
				}
			}

			// SPECIAL GROUP
			if ($group[0] == 'S') {
				return true;
			}

			// APABILA USER TIDAK TERMASUK KEDALAM MATRIKS MAKA RETURN FALSE
			if (!$cek) {
				return false;
			}
		}

		// JIKA GROUP BUSER TIDAK ADA RETURN FALSE
		else {
			return false;
		}

		$tempusergroup = $usergroup;

		// JIKA USER TERMASUK KE DALAM MATRIKS
		if ($cek) {

			// PARSING POLICY MATRIKS
			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);

			// CLEANING WHITESPACE RIGHT AND LEFT
			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S', // SPECIAL GROUP
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			// CHANGE POLICY TO PHP COMMAND
			$phpCommand =  strtr($cleanCommand, $translate);

			// ------ ????
			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S', // SPECIAL GROUP
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);

			function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);

			$list = explode(' ', $command);

			// APABILA POLICY MENGGUNAKAN THEN
			$thendata = explode('THEN', $command);

			// HITUNG BERAPA THEN USER
			$cthen = count($thendata);

			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);

			// JIKA MENGGUNAKAN THEN
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {

					foreach ($secondlist as $row => $thenval) {

						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
						}
					}
				}
			}

			// JIKA USER THEN LEBIH ATAU SAMA DENGAN 2
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
					foreach ($thendata as $row => $thenval) {
						$newsecondcommand = str_replace('(', '', trim($thenval));
						$newsecondcommand = str_replace(')', '', $newsecondcommand);
						$newsecondcommand = str_replace('AND', '', $newsecondcommand);
						$newsecondcommand = str_replace('OR', '', $newsecondcommand);
						$newsecondlist = explode(' ', $newsecondcommand);

						// JIKA GROUP USER MASUK KE DALAM POLICY MATRIKS --> THEN 
						if (in_array(trim($value['GROUP']), $newsecondlist)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
						}
					}
				}
			}

			// JIKA USER MASUK KE DALAM THEN GROUP
			if ($thengroup == true) {

				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;
					$indno = $i;

					for ($a = $cthen - $indno; $a >= 1; --$a) {

						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}

					$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);

					if ($result) {

						$replace = 'THEN$ $' . trim($thendata[$i + 1]);

						if (!empty($thendata[$i + 1])) {
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);

							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											//echo 'sini';

											return false;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);

						if (!$result) {

							if ($groupalfa == trim($thendata[$i])) {
								return true;
							} else {
								//return true;
							}
						} else {

							//return false;
						}
					} else {
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);

						$approver = array();
						$countlist = array_count_values($list);

						foreach ($list as $key => $value) {

							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								$selectapprover	= $this->_db->select()
									->from(array('C' => 'T_BGAPPROVAL'), array(
										'USER_ID'
									))
									->where("C.REG_NUMBER = ?", (string) $psnumb)
									->where("C.GROUP = ?", (string) $value);

								$usergroup = $this->_db->fetchAll($selectapprover);


								$approver[$value] = $usergroup;

								if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
									return false;
								}
							}
						}


						if (!empty($thendata[$i])) {

							if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
								return false;
							}
						}

						foreach ($grouplist as $key => $vg) {
							$newgroupalpa = str_replace('AND', '', $vg);
							$newgroupalpa = str_replace('OR', '', $newgroupalpa);
							$groupsecondlist = explode(' ', $newgroupalpa);

							if (in_array($groupalfa, $groupsecondlist)) {
								return true;
							}

							if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
								return true;
							}
						}

						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {
										if (empty($thendata[1])) {
											return true;
										}
									}
								}
							}
						}

						$secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);

						foreach ($grouplist as $key => $valgroup) {
							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								if ($secondresult) {
									return false;
								} else {
									return true;
								}
							}
						}
					}
				}
			}

			// JIKA USER TIDAK MASUK KE DALAM THEN GROUP
			else if (!empty($thendata) && $thengroup == false) {

				foreach ($thendata as $ky => $vlue) {
					$newsecondcommand = str_replace('(', '', trim($vlue));
					$newsecondcommand = str_replace(')', '', $newsecondcommand);
					$newsecondcommand = str_replace('AND', '', $newsecondcommand);
					$newsecondcommand = str_replace('OR', '', $newsecondcommand);
					$newsecondlist = explode(' ', $newsecondcommand);
					if ($newsecondlist['0'] == $groupalfa) {
						return true;
					}
				}
				return false;
			}

			$approver = array();
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_BGAPPROVAL'), array(
							'USER_ID'
						))

						->where("C.REG_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
					$usergroup = $this->_db->fetchAll($selectapprover);
					$approver[$value] = $usergroup;
				}
			}

			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);

					if (!empty($approver)) {
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
									}
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
							}
						}
						$BG_NUMBER = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $BG_NUMBER . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;

						if (${$values}[$i] == $replace) {
							$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
						} else {
							$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
						}
					}
				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			$thendata = explode('THEN$', $phpCommand);

			if (!empty($thendata['1'])) {
				$phpCommand = '';
				foreach ($thendata as $tkey => $tval) {
					$phpCommand .= '(';
					$phpCommand .= $tval . ')';
					if (!empty($thendata[$tkey + 1])) {
						$phpCommand .= ' && ';
					}
				}
			} else {
				$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			}
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			if (!$result) {
				return true;
			}
		} else {
			return true;
		}
	}

	public function generate($command, $list, $param, $psnumb, $group, $thengroup)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();

		$count_list = array_count_values($list);
		//print_r($count_list);
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_BGAPPROVAL'), array(
						'USER_ID'
					))

					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.REG_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		//var_dump($param);die;
		//var_dump($group);
		foreach ($approver as $appval) {
			$totaldata = count($approver[$group]);
			$totalgroup = $count_list[$group];
			//var_dump($totaldata);
			//var_dump($totalgroup);
			if ($totalgroup == $totaldata && $totalgroup != 0) {

				return false;
			}
		} //die;
		//die;





		foreach ($param as $url) {

			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$BG_NUMBER = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $BG_NUMBER . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}

		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		//print_r($phpCommand);echo '<br/>';

		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			//var_dump($thengroup);
			// var_dump($result);

			if ($result) {
				//var_dump($thengroup);die;
				if ($thengroup) {
					return true;
				} else {
					return false;
				}
			} else {

				if ($thengroup) {
					return false;
				} else {

					return true;
				}
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;
	}

	public function findPolicyBoundary($transfertype, $amount)
	{

		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BGBOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))

			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{
		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BGBOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))

			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		// $command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
					->where("C.GROUP_BUSER_ID LIKE ?", '%S_%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
					->where("C.GROUP_BUSER_ID LIKE ?", '%_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_BUSER'), array(
					'BUSER_ID'
				))

				->where("C.GROUP_BUSER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
		//	echo '<pre>';
		//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['BUSER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_BUSER'), array(
							'*'
						))

						->where("BUSER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['BUSER_NAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	public function validateapproval($policy, $list, $bgnumb)
	{
		//die('a');
		$command = ' ' . $policy . ' ';
		$command = strtoupper($command);

		$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
		//var_dump($cleanCommand);
		$commandspli = explode('THEN', $cleanCommand);
		$commandnew = '';
		foreach ($commandspli as $ky => $valky) {
			if ($commandspli[$ky + 1] != '') {
				$commandnew .= '(' . $valky . ') THEN ';
			} else {
				$commandnew .= '(' . $valky . ')';
			}
		}

		//transform to php logical operator syntak
		$translate = array(
			'AND' => '&&',
			'OR' => '||',
			'THEN' => '&&',
			'A' => '$A',
			'B' => '$B',
			'C' => '$C',
			'D' => '$D',
			'E' => '$E',
			'F' => '$F',
			'G' => '$G',
			'H' => '$H',
			'I' => '$I',
			'J' => '$J',
			'K' => '$K',
			'L' => '$L',
			'M' => '$M',
			'N' => '$N',
			'O' => '$O',
			'P' => '$P',
			'Q' => '$Q',
			'R' => '$R',
			// 'S' => '$S',
			'T' => '$T',
			'U' => '$U',
			'V' => '$V',
			'W' => '$W',
			'X' => '$X',
			'Y' => '$Y',
			'Z' => '$Z',
			'SG' => '$SG',
		);

		$phpCommand =  strtr($commandnew, $translate);

		$param = array(
			'0' => '$A',
			'1' => '$B',
			'2' => '$C',
			'3' => '$D',
			'4' => '$E',
			'5' => '$F',
			'6' => '$G',
			'7' => '$H',
			'8' => '$I',
			'9' => '$J',
			'10' => '$K',
			'11' => '$L',
			'12' => '$M',
			'13' => '$N',
			'14' => '$O',
			'15' => '$P',
			'16' => '$Q',
			'17' => '$R',
			// '18' => '$S',
			'19' => '$T',
			'20' => '$U',
			'21' => '$V',
			'22' => '$W',
			'23' => '$X',
			'24' => '$Y',
			'25' => '$Z',
			'26' => '$SG',
		);
		// print_r($phpCommand);die;
		// function str_replace_first($from, $to, $content,$row)
		// {
		//     $from = '/'.preg_quote($from, '/').'/';
		//     return preg_replace($from, $to, $content, $row);
		// }

		$command = str_replace('(', '', $policy);
		$command = str_replace(')', '', $command);
		$list = explode(' ', $command);
		// print_r($list);die;
		$approver = array();
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_BGAPPROVAL'), array(
						'USER_ID'
					))

					->where("C.REG_NUMBER = ?", (string) $bgnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;die;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		// print_r($approver);die;
		foreach ($param as $url) {
			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
							foreach ($approver[$value] as $row => $val) {
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
								}
							}
						}
					}
				}
				for ($i = 1; $i <= $ta; $i++) {
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
							$values = 'G' . $value;
							if (empty(${$values}[$i])) {
								${$values}[$i] = false;
							}
						}
					}

					$BG_NUMBER = $i;
					$label = str_replace('$', '$G', $url);

					$replace = $label . '[' . $BG_NUMBER . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
						//	 print_r($phpCommand);
					} else {
						$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
						//	 print_r($phpCommand);
					}
				}
			}
		}
		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		if (str_replace([')', '('], '', $phpCommand) != '') eval('$result = ' . "$phpCommand;");
		return $result;
	}

	public function str_replace_first($from, $to, $content, $row)
	{
		$from = '/' . preg_quote($from, '/') . '/';
		return preg_replace($from, $to, $content, $row);
	}

	public function inquiryrekeningklaimAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		$swiftcode = $this->_request->getParam('swiftcode');
		$acct_name = $this->_request->getParam('acct_name');

		if (strtolower($swiftcode) == 'btanidja') {
			if ($result['response_desc'] == 'Success') //00 = success
			{
				$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
				$result2 = $svcAccountCIF->inquiryCIFAccount();

				$filterBy = $result['account_number']; // or Finance etc.
				$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
					return ($var['account_number'] == $filterBy);
				});

				$singleArr = array();
				foreach ($new as $key => $val) {
					$singleArr = $val;
				}
			} else {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($result);
				return 0;
			}

			if ($singleArr['type_desc'] == 'Deposito') {
				$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountDeposito->inquiryDeposito();

				if ($result3["response_code"] != "0000") {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				$sqlRekeningJaminanExist = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
					->where('A.ACCT = ?', $result['account_number'])
					->query()->fetchAll();

				$result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;

				$get_product_type = current($new)["type"];

				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			} else {
				$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountBalance->inquiryAccountBalance();

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode($result3);
					return 0;
				}

				$get_product_type = current($new)["type"];
				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			}

			$return = array_merge($result, $singleArr, $result3);

			$data['data'] = false;
			is_array($return) ? $return :  $return = $data;

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($return);
		} else {
			$clientUser  =  new SGO_Soap_ClientUser();

			$getInfoBank = $this->_db->select()
				->from('M_DOMESTIC_BANK_TABLE')
				->where('SWIFT_CODE = ?', $swiftcode)
				->query()->fetch();

			$request = [
				'account_number' => $acct_no,
				'bank_code' => $getInfoBank['BANK_CODE']
			];

			$clientUser->callapi('otherbank', NULL, $request);
			$responseService = $clientUser->getResult();

			$result = [
				'valid' => true,
				'account_name' => $responseService['account_name']
			];

			if ($responseService['response_code'] != '0000') {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Rekening tidak ditemukan')
				];
			}

			if (strtolower($responseService['account_name']) !== strtolower($acct_name)) {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Nama dan Nomor Rekening tidak sesuai')
				];
			}

			$result = array_merge($result, array_intersect_key($responseService, ['response_code' => '']));
			echo json_encode($result);
			return;
		}
	}

	function calculateTimeSpan($date)
	{
		$now = new DateTime();
		$dateObj = new DateTime($date);
		$interval = $now->diff($dateObj);

		$days = $interval->days;
		$hours = $interval->h;
		$minutes = $interval->i;

		if ($days > 0) {
			$time = $days . " hari " . $hours . " jam " . $minutes . " menit";
		} else if ($hours > 0) {
			$time = $hours . " jam " . $minutes . " menit";
		} else {
			$time = $minutes . " menit";
		}

		return $time;
	}
}
