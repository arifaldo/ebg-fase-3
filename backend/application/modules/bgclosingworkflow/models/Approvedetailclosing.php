<?php

class bgclosingworkflow_Model_Approvedetailclosing
{
    protected $_db;
    private $userid;

    public function __construct($userid = '')
    {
        $this->userid = $userid;
        $this->_db = Zend_Db_Table::getDefaultAdapter();
    }

    /**
     * @param string $bgnumber
     * @param string $bgregnumber
     * 
     * @return array
     */
    public function getDetailBg(string $bgnumber = 'none', string $bgregnumber = 'none'): array
    {
        $bgdata = $this->_db->select()
            ->from(array('A' => 'T_BANK_GUARANTEE'), [
                '*',
                'PRINCIPLE_NAME' => new Zend_Db_Expr('(SELECT MC.CUST_NAME FROM M_CUSTOMER MC WHERE CUST_ID = A.CUST_ID LIMIT 1)'),
                'PRINCIPLE_EMAIL' => new Zend_Db_Expr('(SELECT CUST_EMAIL FROM M_CUSTOMER mc WHERE mc.CUST_ID = A.CUST_ID LIMIT 1)'),
            ])
            ->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.CUST_NAME'))
            ->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME', 'C.BRANCH_EMAIL'))
            ->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('CHANGE_TYPE_CLOSE' => 'D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLOSE_REF_NUMBER'))
            ->where('A.BG_NUMBER = ?', $bgnumber)
            ->orWhere('A.BG_REG_NUMBER = ?', $bgregnumber)
            ->query()->fetch();

        return $bgdata;
    }

    /**
     * @param string $bgnumber
     * 
     * @return array
     */
    public function getPaperUsed(string $bgnumber): array
    {
        $getPaperUsed = $this->_db->select()
            ->from('M_PAPER')
            ->where('NOTES = ?', $bgnumber)
            ->query()->fetchAll();

        return $getPaperUsed;
    }

    /**
     * @param string $closerefnumber
     * @param string $custid
     * 
     * @return array
     */
    public function getHistoryClose(string $closerefnumber): array
    {

        $getHistoryClose = $this->_db->select()
            ->from(['TBGHC' => 'T_BANK_GUARANTEE_HISTORY_CLOSE'])
            ->joinLeft(['MB' => 'M_BUSER'], 'TBGHC.USER_LOGIN = MB.BUSER_ID', ['MB.BUSER_NAME'])
            ->joinLeft(['MU' => 'M_USER'], 'TBGHC.USER_LOGIN = MU.USER_ID', ['MU.USER_FULLNAME', 'CUST_ID_FO' => 'MU.CUST_ID'])
            ->where('TBGHC.CLOSE_REF_NUMBER = ?', $closerefnumber)
            ->query()->fetchAll();

        return $getHistoryClose;
    }

    /**
     * @param string $closerefnumber
     * 
     * @return array
     */
    public function getLastHistoryClose(string $closerefnumber): array
    {
        $getLastHistoryClose = $this->_db->select()
            ->from('T_BANK_GUARANTEE_HISTORY_CLOSE')
            ->where('CLOSE_REF_NUMBER = ?', $closerefnumber)
            ->order('DATE_TIME DESC')
            ->query()->fetch();

        return $getLastHistoryClose;
    }

    /**
     * @param string $custid
     * 
     * @return array
     */
    public function principalDetail(string $custid): array
    {

        $principalDetail = $this->_db->select()
            ->from(['MCS' => 'M_CUSTOMER'], [
                '*',
                'CITY_NAME' => new Zend_Db_Expr('(SELECT CITY_NAME FROM M_CITYLIST MC WHERE MC.CITY_CODE = MCS.CUST_CITY LIMIT 1)')
            ])
            ->where('CUST_ID = ?', $custid)
            ->query()->fetch();

        return $principalDetail;
    }

    /**
     * @param string $bgregnumber
     * 
     * @return array
     */
    public function getTbgDetail(string $bgregnumber): array
    {
        $getTbgDetail = $this->_db->select()
            ->from('T_BANK_GUARANTEE_DETAIL')
            ->where('BG_REG_NUMBER = ?', $bgregnumber)
            ->query()->fetchAll();

        return $getTbgDetail;
    }

    /**
     * @param string $closerefnumber
     * 
     * @return array
     */
    public function getBgCloseFile(string $closerefnumber): array
    {
        $tempBgCloseFile = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE_FILE_CLOSE')
            ->where('CLOSE_REF_NUMBER = ?', $closerefnumber)
            ->query()->fetchAll();

        return $tempBgCloseFile;
    }

    /**
     * @param string $bgregnumber
     * 
     * @return array
     */
    public function getBgSplit(string $bgregnumber): array
    {
        $bgdatasplit = $this->_db->select()
            ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
            ->where('A.BG_REG_NUMBER = ?', $bgregnumber)
            ->query()->fetchAll();

        return $bgdatasplit;
    }

    /**
     * @param string $closerefnumber
     * 
     * @return array
     */
    public function getEscrowRelease(string $closerefnumber = 'none'): array
    {
        $getEscrowRelease = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
            ->where('CLOSE_REF_NUMBER = ?', $closerefnumber)
            ->query()->fetchAll();

        return $getEscrowRelease;
    }

    /**
     * @param string $closerefnumber
     * @param string $suggeststatus
     * 
     * @return void
     */
    public function updateStatusClose(string $closerefnumber, string $suggeststatus): void
    {
        $this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', [
            'SUGGESTION_STATUS' => $suggeststatus,
            'LASTUPDATED' => new Zend_Db_Expr('now()'),
            'LASTUPDATEDFROM' => 'BANK',
            'LASTUPDATEDBY' => $this->userid
        ], [
            'CLOSE_REF_NUMBER = ?' => $closerefnumber
        ]);
    }

    /**
     * @param array $data
     * 
     * @return void
     */
    public function insertTbgHistoryClose(array $data): void
    {
        $this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', [
            'CLOSE_REF_NUMBER' => $data['CLOSE_REF_NUMBER'],
            'BG_NUMBER' => $data['BG_NUMBER'],
            'CUST_ID' => $data['CUST_ID'],
            'DATE_TIME' => new Zend_Db_Expr('now()'),
            'USER_FROM' => 'BANK',
            'USER_LOGIN' => $this->userid,
            'BG_REASON' => $data['REASON_POST'],
            'HISTORY_STATUS' => $data['HISTORY_STATUS']
        ]);
    }

    public function getHistoryApproval(string $bgregnumber): array
    {
        $getHistoryApproval = $this->_db->select()
            ->from(array('C' => 'T_BGAPPROVAL'), array(
                '*'
            ))
            ->joinLeft(['MU' => 'M_BUSER'], 'MU.BUSER_ID = C.USER_ID', ['MU.BUSER_NAME'])
            ->where("C.REG_NUMBER = ?", $bgregnumber)
            ->query()->fetchAll();

        return $getHistoryApproval;
    }

    /**
     * @param array $privi
     * @param string $bgBranch
     * 
     * @return array
     */
    public function getBuserEmailWithPrivi(array $privi, string $bgBranch): array
    {
        $priviJoin = array_map(function ($item) {
            return "'$item'";
        }, $privi);
        $priviJoin = join(',', $priviJoin);

        $result = $this->_db->select()
            ->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL'])
            ->where('MB.BGROUP_ID IN (?)', new Zend_Db_Expr('(SELECT BGROUP_ID FROM M_BPRIVI_GROUP mbg WHERE mbg.BPRIVI_ID IN(' . $priviJoin . ') GROUP BY BGROUP_ID)'))
            ->where('MB.BUSER_EMAIL != ?', '');

        $result->where('BUSER_BRANCH IN (?)', new Zend_Db_Expr('(SELECT ID FROM M_BRANCH WHERE BRANCH_CODE = ' . $this->_db->quote($bgBranch) . ')'));

        $result = $result->group('MB.BUSER_EMAIL')
            ->query()->fetchAll();

        return $result;
    }

    public function getBankName(string $closeRefNumber): string
    {
        $getBankName = $this->_db->select()
            ->from(['mdbt' => 'M_DOMESTIC_BANK_TABLE'], ['mdbt.BANK_NAME'])
            ->where('mdbt.SWIFT_CODE = ?', new Zend_Db_Expr('(SELECT PS_FIELDVALUE FROM TEMP_BANK_GUARANTEE_DETAIL_CLOSE tbgdc WHERE tbgdc.CLOSE_REF_NUMBER = ' . $this->_db->quote($closeRefNumber) . ' AND LOWER(tbgdc.PS_FIELDNAME) = \'swift code\')'))
            ->query()->fetch();

        return $getBankName['BANK_NAME'] ?: '';
    }
}
