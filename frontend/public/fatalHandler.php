<?php

set_error_handler(function ($errno, $errstr, $errfile, $errline ,array $errcontex) {
	
	if (!(error_reporting() & $errno)) {
        // This error code is not included in error_reporting, so let it fall
        // through to the standard PHP error handler
        return false;
    }
    switch ($errno) {
		case E_USER_ERROR:
			throw new ErrorException($errstr, 0, $errno, $errfile, $errline);exit();
			break;

		case E_USER_WARNING:
			$errorMsg = "<b>Warning: </b> <br/>".PHP_EOL;
			$errorMsg .= "Error No 		: ".$errno."<br/>".PHP_EOL;
			$errorMsg .= "Error Str 	: ".$errstr."<br/>".PHP_EOL;
			$errorMsg .= "Error File 	: ".$errfile."<br/>".PHP_EOL;
			$errorMsg .= "Error Line	: ".$errline."<br/>".PHP_EOL;
			saveLog($errorMsg);
			break;

		case E_USER_NOTICE:
			$errorMsg = "<b>Notice: </b> <br/>".PHP_EOL;
			$errorMsg .= "Error No 		: ".$errno."<br/>".PHP_EOL;
			$errorMsg .= "Error Str 	: ".$errstr."<br/>".PHP_EOL;
			$errorMsg .= "Error File 	: ".$errfile."<br/>".PHP_EOL;
			$errorMsg .= "Error Line	: ".$errline."<br/>".PHP_EOL;
			saveLog($errorMsg);
			break;

		default:
			// throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
			$errorMsg = "<b>ERROR: </b> <br/>".PHP_EOL;
			$errorMsg .= "Error No 		: ".$errno."<br/>".PHP_EOL;
			$errorMsg .= "Error Str 	: ".$errstr."<br/>".PHP_EOL;
			$errorMsg .= "Error File 	: ".$errfile."<br/>".PHP_EOL;
			$errorMsg .= "Error Line	: ".$errline."<br/>".PHP_EOL;
			saveLog($errorMsg);
			break;
    }
    return true;
});

register_shutdown_function( "fatal_handler" );


function fatal_handler() {
	$errfile = "unknown file";
	$errstr  = "shutdown";
	$errno   = E_CORE_ERROR;
	$errline = 0;

	$error = error_get_last();

	if( $error !== NULL) {
		// $errno   = $error["type"];
		// $errfile = $error["file"];
		// $errline = $error["line"];
		// $errstr  = $error["message"];
	
		$errorMsg = var_export($error,true);

		$backtrace = debug_backtrace();
		// Zend_Debug::dump($errorMsg);
		// Zend_Debug::dump($backtrace);
		/* foreach($backtrace AS $file_path) {
			foreach($file_path AS $key => $var) {
				if($key == 'args') {
					foreach($var AS $key_arg => $var_arg) {
						$errorMsg =  $key_arg . ': ' . $var_arg . PHP_EOL;
					}
				} else {
					$errorMsg = $key . ': ' . $var . PHP_EOL;
				}
			}
		} */	
	}
}

function saveLog($errorMsg)
{
	$logPath = APPLICATION_PATH.'/logs/application.log';
	$message = date('d-M-Y  H:i:s -> ').$errorMsg.PHP_EOL;
	if($logPath)
	{
		// $fp = fopen($logPath, "a+");
		// fwrite($fp, $message);
		// fclose($fp);
		// echo $message;
		
	}
}
?>