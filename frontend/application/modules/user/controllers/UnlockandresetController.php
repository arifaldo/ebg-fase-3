<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';

class user_UnlockandresetController extends Application_Main
{


	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER      = urldecode($this->_getParam('user_id'));
		$user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

		$user_id = strtoupper($user_id);

		$select =	$this->_db->select()
			->from('M_USER', array('USER_FULLNAME', 'USER_EMAIL', 'USER_ISEMAIL'))
			->where('USER_ID = ? ', $user_id)
			->where('CUST_ID = ? ', $this->_custIdLogin);

		$arr = $this->_db->fetchRow($select);
		$this->view->email = (!empty($arr['USER_EMAIL'])) ? $arr['USER_EMAIL'] : 'N/A';
		$this->view->user_isemail = $arr['USER_ISEMAIL'];
		$this->view->userid = $user_id;

		// if($this->_request->isPost())
		//  	{
		$CustomerUser = new CustomerUser($this->_custIdLogin, $user_id);
		// $key = $this->_getParam('isEmail');
		/**
		 * requestUnlockResetPassword
		 * @key method 1,email 2 print
		 * @resetByBO reset by backend 1, reset by frontend 0 print
		 * @suggester
		 */
		$key 			 	= 1;
		$resetByBO 	= 0;
		$suggester 		= $this->_userIdLogin;
		// Application_Helper_General::writeLog('UVEP', 'Requesting Unlock and Reset Password : Cust Id ( ' . $this->_custIdLogin . ' ) ,User Id ( ' . $user_id . ' )');
		Application_Helper_General::writeLog('UVEP', 'Usulan Permintaan Buka Kunci / Atur Ulang Kata Sandi : ' . $arr['USER_FULLNAME'] . ' (' . $user_id . ')');
		$CustomerUser->requestUnlockResetPassword($key, $resetByBO, $suggester);
		$this->setbackURL('/' . $this->_request->getModuleName() . '/userlist/index/');
		$this->_redirect('/notification/success/index');
		// }
	}
}
