<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));

$db = Zend_Db_Table::getDefaultAdapter();
$insertT_PSLIP = 0;
$minDiff = 5; // x minute from updated -> now before sending Email
	
//T_PSLIP
$data = array();
$data = $db->SELECT()
     ->FROM('T_PSLIP',array('PS_NUMBER','CUST_ID'))
     ->WHERE('PS_STATUS = 13') 
     ->WHERE(' (TIMESTAMPDIFF(Minute, PS_UPDATED , now())) > ? ',$minDiff)
     ->QUERY()->FETCHALL();

	if(count($data) > 0)
	{
		foreach($data as $content) 
		{
			$updateT_PSLIP = array(
														'PS_STATUS' => 9,
														'PS_UPDATED' => new Zend_Db_Expr("now()"),
													);
			$whereT_PSLIP  = array(
														'PS_NUMBER = ?' => $content['PS_NUMBER']
													);
			$db->update('T_PSLIP', $updateT_PSLIP, $whereT_PSLIP);

			$dataHistory = array();
			$dataHistory = array(
												'DATE_TIME'				=> new Zend_Db_Expr("now()"),
												'PS_NUMBER'				=> $content['PS_NUMBER'],
												'USER_LOGIN'			=> 'System',
												'CUST_ID'					=> $content['CUST_ID'],
												'PS_REASON'				=> 'Exception',
												'HISTORY_STATUS'	=> 9,
											);
			$insertT_PSLIP += $db->insert('T_PSLIP_HISTORY', $dataHistory);
		}
	}

	$cronResult = "T_PSLIP : ".$insertT_PSLIP;
	echo "\nT_PSLIP : ".$insertT_PSLIP;

	$filename = basename(__FILE__);
	Application_Helper_General::cronLog($filename, $cronResult);
?>