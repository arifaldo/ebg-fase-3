<?php
require_once 'Zend/Controller/Action.php';
class customer_CheckaccController extends customer_Model_Customer 
{
  public function indexAction() 
  {
	$ccy = $this->_getParam('ccy'); // sementara
    $prk = $this->_getParam('prk');
    $prk = (Zend_Validate::is($prk,'Digits') && Zend_Validate::is($prk,'StringLength',array('min'=>1,'max'=>19)))? $prk : 0;
    $repay = $this->_getParam('repay');
    $repay = (Zend_Validate::is($repay,'Digits') && Zend_Validate::is($repay,'StringLength',array('min'=>1,'max'=>19)))? $repay : 0;
    $charges = $this->_getParam('charges');
    $charges = (Zend_Validate::is($charges,'Digits') && Zend_Validate::is($charges,'StringLength',array('min'=>1,'max'=>19)))? $charges : 0;
    $operational = $this->_getParam('operational');
    $operational = (Zend_Validate::is($operational,'Digits') && Zend_Validate::is($operational,'StringLength',array('min'=>1,'max'=>19)))? $operational : 0;
    $disburse = $this->_getParam('disburse');
    $disburse = (Zend_Validate::is($disburse,'Digits') && Zend_Validate::is($disburse,'StringLength',array('min'=>1,'max'=>19)))? $disburse : 0;
    
    $this->_moduleDB = 'POP';
    $checkacc = array(); $fullarray = array();
    $error_remark = null; $fulldesc = null;
    $customer_id = $this->_getParam('customer_id');
    $customer_id = (Zend_Validate::is($customer_id,'Digits'))? $customer_id : null;
    
    if($prk)
    {
      $this->view->Loan_No = $prk;
	  $this->view->Loan_CCY = $ccy; //sementara
      $fullarray[] = 'LOAN_ACCT_NO:'.$prk;
    }  
	  	
    if($repay)
    {
      $this->view->Repay_No = $repay;
	  $this->view->Repay_CCY = $ccy; //sementara
      $fullarray[] = 'REPAY_ACCT_NO:'.$repay;
	}
	  	
    if($charges)
    {
      $this->view->Charges_No = $charges;
	  $this->view->Charges_CCY = $ccy; //sementara
      $fullarray[] = 'CHARGES_ACCT_NO:'.$charges;
	}

    if($operational)
    {
      $this->view->Operational_No = $operational;
	  $this->view->Operational_CCY = $ccy; //sementara
      $fullarray[] = 'OPERATIONAL_ACCT_NO:'.$operational;
	}

    if($disburse)
    {
      $this->view->Disburse_No = $disburse;
	  $this->view->Disburse_CCY = $ccy; //sementara
      $fullarray[] = 'DISBURSE_ACCT_NO:'.$disburse;
	}
	
	if(count($checkacc))
	{
	  $error_remark = implode(chr(10),$checkacc);
	  $this->view->error = 1;
	  $this->view->member_msg = implode('<br />',$checkacc);	
	}
	if(count($fullarray))$fulldesc = implode(chr(10),$fullarray);
	$this->_helper->layout()->setLayout('popup');
	
    //insert log
	try 
	{
	  $this->_db->beginTransaction();
	  $this->backendLog(strtoupper($this->_actionID['cekacc']),$this->_moduleDB,$customer_id,$fulldesc,$error_remark);
	  $this->_db->commit();
	}
    catch(Exception $e)
    {
 	  $this->_db->rollBack();
	  SGO_Helper_GeneralLog::technicalLog($e);
	}
  }
}