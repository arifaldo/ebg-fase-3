<?php
require_once 'Zend/Controller/Action.php';
class newsmaker_IndexController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$arr = null;
		$viewFilter = null;
		$errors = null;
		
		$fields = array	(
							'HelpTopic'  			=> array	(
																	'field' => 'TITLE',
																	'label' => $this->language->_('Subject'),
																	'sortable' => true
																),
							'TargetTopic'  			=> array	(
																	'field' => 'TARGET',
																	'label' => $this->language->_('Target'),
																	'sortable' => true
																),
							
							'LayoutTopic'  			=> array	(
																	'field' => 'TYPE',
																	'label' => $this->language->_('Layout Position'),
																	'sortable' => true
																),
							// 'FileDescription'  					=> array	(
							// 											'field' => 'IMAGE',
							// 											'label' => $this->language->_('Image'),
							// 											'sortable' => true
							// 										),
							// 'FileName'  			=> array	(
							// 										'field' => 'HELP_FILENAME',
							// 										'label' => $this->language->_('File Name'),
							// 										'sortable' => true
							// 									),
							'Uploaded By'  					=> array	(
																		'field' => 'CREATEDBY',
																		'label' => $this->language->_('Last Updated'),
																		'sortable' => true
																	),	
							/*'Uploaded Date'  					=> array	(
																		'field' => 'CREATED',
																		'label' => $this->language->_('Created Date'),
																		'sortable' => true
																	),	*/
							'efdate'  					=> array	(
																		'field' => 'EFDATE',
																		'label' => $this->language->_('Effective Date'),
																		'sortable' => true
																	),	
							'expDate'  					=> array	(
																		'field' => 'EXPDATE',
																		'label' => $this->language->_('Expired Date'),
																		'sortable' => true
																	),	
							'status'  					=> array	(
																		'field' => 'STATUS',
																		'label' => $this->language->_('Status'),
																		'sortable' => true
																	),
								
						);
		$this->view->fields = $fields;


		$filterlist = array('TITLE','CREATED','CREATEDBY');
		
		$this->view->filterlist = $filterlist;
	  
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'TITLE'   => array('StringTrim','StripTags','StringToUpper'),
							'CREATED'   	=> array('StringTrim','StripTags','StringToUpper'),
							'CREATEDBY' 	=> array('StringTrim','StripTags')
							
		);

		 $dataParam = array('TITLE','CREATED','CREATEDBY');
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}

		if(!empty($this->_request->getParam('uploaddate'))){
				$createarr = $this->_request->getParam('uploaddate');
					$dataParamValue['CREATED'] = $createarr[0];
					$dataParamValue['CREATED_END'] = $createarr[1];
			}
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		// $zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		// $filter 	= $zf_filter->getEscaped('filter');
		$delete 	= $this->_getParam('delete');
		
		if($delete)
		{
			//echo("<br>ok<br>");
			$postreq_id	= $this->_request->getParam('req_id');
			if($postreq_id)
			{
				foreach ($postreq_id as $key => $value) 
				{
					if($postreq_id[$key]==0)
					{
						unset($postreq_id[$key]);
					}
					
				}
			}
			
			if($postreq_id == null)
			{
				$params['req_id'] = null;
			}
			else
			{
				$params['req_id'] = 1;
			}
			
			$validators = array	(
										'req_id' => 	array	(	
																	'NotEmpty',
																	'messages' => array	(
																							'Error File ID Submitted',
																						)
																),
									);
			$filtersVal = array	( 	
									'req_id' => array('StringTrim','StripTags')
								);
			$zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
			
			if($zf_filter_input->isValid())
			{			
				try
				{
					foreach ($postreq_id as  $key =>$value) 
					{		
						$HELP_ID_DELETE =  $postreq_id[$key];
				
						$this->_db->beginTransaction();
						$param = array('STATUS'=> '4',);	
						
						$where = array('ID = ?' => $HELP_ID_DELETE);
						$query = $this->_db->update ( "M_CONTENT", $param, $where );
						$this->_db->commit();
					}
					$id = implode(",", $postreq_id);
					Application_Helper_General::writeLog('HLUD','Delete News. News ID: ['.$id.']');
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
			else
			{
				$error 			= true;
				$errors 		= $zf_filter_input->getMessages();
				$req_idErr 		= (isset($errors['req_id']))? $errors['req_id'] : null;
				
				$this->_redirect("/newsmaker/index?error=true&req_idErr=$req_idErr&filter=Filter");
			}
			
			//$filter = 'Filter';
			
		}
		$csv = $this->_getParam('csv');
		if($this->_getParam('error'))
		{
			$this->view->error 			= $this->_getParam('error');
			
			$this->view->req_idErr 		= $this->_getParam('req_idErr');
			
		}
		
		//if($filter)
		//{
			$select = $this->_db->select()
								->from(	'M_CONTENT',
										array('ID',
												'IMAGE', 
												'TITLE',
												'NEWS', 
												'CREATED', 
												'CREATEDBY',
												'EFDATE',
												'EXPDATE',
												'STATUS', 
												'TARGET',
												'TYPE'));
								// ->where("HELP_ISDELETED != 1");
								
		//}
		
		
		$HELP_ID   	= $this->_getParam('ID');
		
		if($HELP_ID)
		{
			$select->where('ID =?',$HELP_ID);
			$data = $this->_db->fetchRow($select);
			$attahmentDestination = UPLOAD_PATH . '/document/help/';
			$this->_helper->download->file($data['HELP_FILENAME'],$attahmentDestination.$data['HELP_SYS_FILENAME']);
			Application_Helper_General::writeLog('HLDL','Download Help.  Help ID: ['.$HELP_ID.'] , Help Topic: ['.$data['HELP_TOPIC'].']');
		}
		else
		{
			Application_Helper_General::writeLog('HLLS','View help list');
		}
		
		if($filter == TRUE)
		{
			$SEARCH_TEXT   	= $this->_getParam('SEARCH_TEXT');
			$HELP_TOPIC   	= $this->_getParam('HELP_TOPIC');
			$UPLOADED_BY    = $this->_getParam('UPLOADED_BY');
			$DATE_START    	= $zf_filter->getEscaped('UPLOAD_DATE');					
			$DATE_END		= $zf_filter->getEscaped('UPLOAD_DATE_END');					
			

			// print_r($DATE_START);die;
			$DATESTART   			= (Zend_Date::isDate($DATE_START,$this->_dateDisplayFormat))?
									   new Zend_Date($DATE_START,$this->_dateDisplayFormat):
									   false;
			
			$DATEEND    			= (Zend_Date::isDate($DATE_END,$this->_dateDisplayFormat))?
									   new Zend_Date($DATE_END,$this->_dateDisplayFormat):
									   false;
									   
			$description	= $this->_getParam('description');
			
			if(!empty($DATE_START))
			{
				$select->where("DATE (CREATED) >= DATE(".$this->_db->quote($DATESTART->toString($this->_dateDBFormat)).")");
			}
			
			if(!empty($DATE_END))
			{
				$select->where("DATE (CREATED) <= DATE(".$this->_db->quote($DATEEND->toString($this->_dateDBFormat)).")");
			}
			
			if($description)
			{
				$select->where("UPPER(HELP_DESCRIPTION) LIKE ".$this->_db->quote('%'.$description.'%'));
			}
			
			if($SEARCH_TEXT)
			{
				$select->where("UPPER(HELP_FILENAME) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%'));
			}
			
			if($HELP_TOPIC)
			{
				$select->where("UPPER(HELP_TOPIC) LIKE ".$this->_db->quote('%'.$HELP_TOPIC.'%'));
			}
			
			if($UPLOADED_BY)
			{
				$select->where("UPPER(UPLOADED_BY) LIKE ".$this->_db->quote('%'.$UPLOADED_BY.'%'));
			}
			// echo $select;die;
			$this->view->DATE_END 		= $DATE_END;
			$this->view->DATE_START 	= $DATE_START;
			$this->view->description 	= $description;
			$this->view->SEARCH_TEXT 	= $SEARCH_TEXT;
			$this->view->HELP_TOPIC 	= $HELP_TOPIC;
			$this->view->UPLOADED_BY 	= $UPLOADED_BY;
		}

		$select->order($sortBy.' '.$sortDir);
		$arr = $this->_db->fetchAll($select);

		if($this->_request->getParam('print') == 1 || $csv){

			foreach($arr as $key=>$row)
			{
				$arr[$key]['CREATEDBY'] = $row['CREATEDBY'].'<br>'.Application_Helper_General::convertDate($row['CREATED'],"dd MMM yyyy HH:mm:ss ");
				//$arr[$key]['BENEFICIARY_ACCOUNT'] = $row['BENEFICIARY_ACCOUNT'].' - '.$row['BENEFICIARY_NAME'];

				if($row['STATUS'] == '1'){
                    $arr[$key]['STATUS'] = $this->language->_('Started');
                }else if($row['STATUS'] == '2'){
                    $arr[$key]['STATUS'] = $this->language->_('Ended');
                }else if($row['STATUS'] == '3'){
                    $arr[$key]['STATUS'] = $this->language->_('Pending Future date');
                }else if($row['STATUS'] == '4'){
                    $arr[$key]['STATUS'] = $this->language->_('Deleted');
                }

                if($row['TARGET'] == 1){
                    $arr[$key]['TARGET'] = "Front End";
                }
                else if($row['TARGET'] == 2){
                    $arr[$key]['TARGET'] = "Back End";
                }else{
                    $arr[$key]['TARGET'] = "FO and BO";
                }

                if($row['TYPE'] == 1){
                    $arr[$key]['TYPE'] = "Promo Top";
                }
                else if($row['TYPE'] == 2){
                    $arr[$key]['TYPE'] = "Promo Bottom";
                }else{
                    $arr[$key]['TYPE'] = "News & Feeds";
                }
				
			}

			// echo "<pre>";
			// var_dump($arr);
			// die();

			
			
			if($csv)
			{
				$newarr = array();
				//echo '<pre>';
				//var_dump($arr);die;
				foreach($arr as $keys => $val){
					$newarr[$keys]['TITLE'] = $val['TITLE'];
					$newarr[$keys]['TARGET'] = $val['TARGET'];
					$newarr[$keys]['TYPE'] = $val['TYPE'];
					$newarr[$keys]['CREATEDBY'] = $val['CREATEDBY'];
					$newarr[$keys]['EFDATE'] = $val['EFDATE'];
					$newarr[$keys]['EXPDATE'] = $val['EXPDATE'];
					$newarr[$keys]['STATUS'] = $val['STATUS'];
				}
				Application_Helper_General::writeLog('SYLG','Download CSV News');				
				$this->_helper->download->csv(array('Subject','Target','Layout Position','Last Update','Effective Date','Expired Date','Status'),$newarr,null,'News Maker');
			}else{
				
					$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'News Maker', 'data_header' => $fields));
			}
		}

		$this->view->arr = $arr;
		$this->paging($arr);
		unset($dataParamValue['CREATED_END']);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}