<?php

Class userlimit_Model_Userlimit
{

  public function __construct()
  {
    $this->_db = Zend_Db_Table::getDefaultAdapter();
  }

  public function getUser($fParam,$sorting)
  {
    $select = $this->_db->select()
                           ->from(array('M'=>'M_USER'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$fParam['cust_id']));
                           // ->where('USER_STATUS = 1');

    if(isSet($fParam['userid']))
    { 
      $select->where("UPPER(USER_ID) LIKE ".$this->_db->quote('%'.$fParam['userid'].'%')); 
    }

    if(isSet($fParam['uname']))
    { 
      $select->where("UPPER(USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['uname'].'%')); 
    }
    
    if(isSet($fParam['suggeststart']))
    {
      $select->where('date(USER_SUGGESTED) >= ?', $fParam['suggeststart']);
    }
    
    if(isSet($fParam['suggestend']))
    {
      $select->where('date(USER_SUGGESTED) <= ?', $fParam['suggestend']);
    }
    
    if(isSet($fParam['approvestart']))
    {
      $select->where('date(USER_UPDATED) >= ?', $fParam['approvestart']);
    }
    
    if(isSet($fParam['approveend']))
    {
      $select->where('date(USER_UPDATED) <= ?', $fParam['approveend']);
    }

    if($sorting)
      $select->order($sorting);

    return $this->_db->fetchAll($select);
  }
  
}




