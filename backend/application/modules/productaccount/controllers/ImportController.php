<?php

require_once 'Zend/Controller/Action.php';

class productaccount_ImportController extends Application_Main
{
	public function indexAction()
	{
		$model = new productaccount_Model_Productaccount();
		
		$attahmentDestination 	= UPLOAD_PATH.'/document/temp';		
		$adapter 				= new Zend_File_Transfer_Adapter_Http();

		if($this->_request->isPost())
		{
			$params = $this->_request->getParams();
			$adapter->setDestination ($attahmentDestination);
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
			$extensionValidator->setMessage(
												$this->language->_('Error: Extension file must be *.txt')
											);
									
			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
										$this->language->_('Error: File siz must not more than ').$this->getSetting('Fe_attachment_maxbyte')
										);

			$adapter->setValidators(array($extensionValidator,$sizeValidator));	
			
			if($adapter->isValid())
			{
				
				$success = 0;
				$sourceFileName = $adapter->getFileName();
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				$adapter->addFilter ( 'Rename',$newFileName);
				$adapter->receive();
				$myFile = file_get_contents($newFileName);
				$arry_myFile = explode("\n", $myFile);
				@unlink($newFileName);
				if($adapter->receive())
				{
					
					$this->_db->beginTransaction();
					$this->_db->query('TRUNCATE TABLE M_PRODUCT_TYPE');
					foreach ($arry_myFile as $row) 
					{
						$product_code = trim(str_replace("'","\'",substr($row,0,2)));
						$plan_code = trim(str_replace("'","\'",substr($row,2,6)));
						$product_name = str_replace("'","\'",substr($row,6,37));
						$account_type = str_replace("'","\'",substr($row,37,40));
						
						try
						{
							if($product_code)
							{
								$paramPayment = array(
														"PRODUCT_CODE"		=> substr($product_code, 0,2),
														"PRODUCT_PLAN"		=> trim($plan_code),
//														"PRODUCT_NAME"		=> trim(str_replace(" ","",$product_name)),
													 	"PRODUCT_NAME"		=> $product_name,
													 	"ACCT_TYPE"			=> trim($account_type),
													 );
								
								$model->insertData($paramPayment);
								$success = 1;
							}
						}
						catch(Exception $e)
						{	
							$success = 0;
							$this->_db->rollBack();		
						}
					}
				}
				
				if($success == 1)
				{
					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
					Application_Helper_General::writeLog('AALS','Import Product Account Type');
					$this->_redirect('/notification/success/index');
				}		
			}
			else
			{
				$this->view->errorMsg = $adapter->getMessages();
			}
		}
		Application_Helper_General::writeLog('AALS','Import Product Account Type');
	}
}