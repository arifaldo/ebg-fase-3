<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
class preliminary_IndexController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new preliminary_Model_Preliminary();

		Zend_Session::namespaceUnset('tempPreliminary');

		$config = Zend_Registry::get('config');

		$entityCode = $config["bg"]["entity"]["code"];
		$entityDesc = $config["bg"]["entity"]["desc"];
		$entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));
		$this->view->entityArr = $entityArr;

		$citizenshipCode = $config["bg"]["citizen"]["code"];
		$citizenshipDesc = $config["bg"]["citizen"]["desc"];
		$citizenshipArr  = array_combine(array_values($citizenshipCode), array_values($citizenshipDesc));
		$this->view->citizenshipArr = $citizenshipArr;

		$statusCode = $config["bg"]["statusowner"]["code"];
		$statusDesc = $config["bg"]["statusowner"]["desc"];
		$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));
		$this->view->statusArr = $statusArr;

		$cekView = $this->_getParam("view");
		$this->view->cekView = $cekView;

		$getCustId = $this->_getParam("custid");

		$getPosition = $model->getPosition();
		$positionArr = Application_Helper_Array::listArray($getPosition, 'CODE', 'POSITION');
		$this->view->positionArr = $positionArr;

		$getRelationship = $model->getRelationship();
		$relationshipArr = Application_Helper_Array::listArray($getRelationship, 'NO', 'RELATION_WITH_BANK');
		$this->view->relationshipArr = $relationshipArr;

		$cust_id = $this->_custIdLogin;
		if ($getCustId != "") {
			$cust_id = $getCustId;
		}

		$data = $model->getPreliminary($cust_id);

		if (empty($data)) {
			return $this->_redirect("/preliminary/index/update");
		}

		$lastsuggest = $this->_db->select()
			->from(array('A' => 'M_PRELIMINARY'))
			->joinLeft(
				array('B' => 'M_USER'),
				'A.LAST_SUGGESTEDBY = B.USER_ID',
				array('*')
			)
			->where('A.CUST_ID = ?', $cust_id);
		$datalastsuggest =  $this->_db->fetchRow($lastsuggest);

		$this->view->datalastsuggest = $datalastsuggest;

		$lastapprove = $this->_db->select()
			->from(array('A' => 'M_PRELIMINARY'))
			->joinLeft(
				array('B' => 'M_USER'),
				'A.LAST_APPROVEDBY = B.USER_ID',
				array('*')
			)
			->where('A.CUST_ID = ?', $cust_id);
		$datalastapprove =  $this->_db->fetchRow($lastapprove);
		$this->view->datalastapprove = $datalastapprove;


		$data['CUST_NAME'] = $this->_custNameLogin;
		if ($getCustId != "") {
			$getCustName = $this->_db->select()
				->from("M_CUSTOMER", ["CUST_NAME"])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();
			$data["CUST_NAME"] = $getCustName[0]["CUST_NAME"];
		}
		$this->view->data = $data;

		$dataMember = $model->getPreliminaryMember($cust_id);
		$this->view->dataMember = $dataMember;

		$this->view->is_update = false;
		$dataTempPreliminary = $model->getTempPreliminary($cust_id);
		if ($dataTempPreliminary) {
			$this->view->is_update = true;
		}

		if (!$this->view->is_update) {
			if (empty($datalastsuggest)) {
				return $this->_redirect("/preliminary/index/update");
			}
		}
		// echo '<pre>';
		// print_r($data);
		// echo '</pre><br>';

		$id 		= $this->_getParam('id');
		$mid 		= $this->_getParam('mid');
		$download 	= $this->_getParam('download');

		if ($download == 'file') {
			$path = UPLOAD_PATH . '/document/submit/';

			if ($id) {
				if ($id == 1) {
					$file = $data['NPWP_FILE'];
				} elseif ($id == 2) {
					$file = $data['PENDIRIAN_NUMBER_FILE'];
				} elseif ($id == 3) {
					$file = $data['PERUBAHAN_FILE'];
				} elseif ($id == 4) {
					$file = $data['SK_PERUBAHAN_FILE'];
				}

				// var_dump($path . $file);
				// die();

				$this->_helper->download->file($file, $path . $file);
			}

			if ($mid) {
				$member = $model->getPreliminaryMemberByIdNumber($cust_id, $mid);
				$this->_helper->download->file($member['ID_FILE'], $path . $member['ID_FILE']);
			}
		} else {
			Application_Helper_General::writeLog('VSPD', 'View Preliminary Document');
		}
	}

	public function updateAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new preliminary_Model_Preliminary();

		if (!$this->view->hasPrivilege('USPD')) {
			$this->_redirect('/preliminary');
		}

		$dataTempPreliminary = $model->getTempPreliminary($this->_custIdLogin);
		if ($dataTempPreliminary) {
			return $this->_redirect('/preliminary');
		}

		$config = Zend_Registry::get('config');

		$entityCode = $config["bg"]["entity"]["code"];
		$entityDesc = $config["bg"]["entity"]["desc"];
		$entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));
		$this->view->entityArr = $entityArr;

		$citizenshipCode = $config["bg"]["citizen"]["code"];
		$citizenshipDesc = $config["bg"]["citizen"]["desc"];
		$citizenshipArr  = array_combine(array_values($citizenshipCode), array_values($citizenshipDesc));
		$this->view->citizenshipArr = $citizenshipArr;

		$statusCode = $config["bg"]["statusowner"]["code"];
		$statusDesc = $config["bg"]["statusowner"]["desc"];
		$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));
		$this->view->statusArr = $statusArr;

		$getPosition = $model->getPosition();
		$positionArr = Application_Helper_Array::listArray($getPosition, 'CODE', 'POSITION');
		$this->view->positionArr = $positionArr;

		$getRelationship = $model->getRelationship();
		$relationshipArr = Application_Helper_Array::listArray($getRelationship, 'NO', 'RELATION_WITH_BANK');
		$this->view->relationshipArr = $relationshipArr;

		$getCity = $model->getCity();
		$cityArr = Application_Helper_Array::listArray($getCity, 'CITY_CODE', 'CITY_NAME');
		$this->view->cityArr = $cityArr;

		$this->view->cust_name = $this->_custNameLogin;

		$cust_id = $this->_custIdLogin;

		$data = $model->getPreliminary($cust_id);
		$this->view->data = $data;

		$dataMember = $model->getPreliminaryMember($cust_id);
		$this->view->dataMember = $dataMember;

		if ($this->_request->isPost()) {
			$params = $this->_request->getParams();

			$adapter                = new Zend_File_Transfer_Adapter_Http();
			$npwpFileName = basename($adapter->getFileName(['npwp_file']));
			$sknumberFileName = basename($adapter->getFileName(['sk_number_file']));
			$skchangeFileName = basename($adapter->getFileName(['sk_change_file']));
			$skpengesahanFileName = basename($adapter->getFileName(['sk_pengesahan_file']));
			$npwpfile = $adapter->getFileInfo(['npwp_file']);
			$sknumberfile = $adapter->getFileInfo(['sk_number_file']);
			$skchangefile = $adapter->getFileInfo(['sk_change_file']);
			$ktp_file = $adapter->getFileInfo(['ktp_file']);
			// $ktpFileName = basename($adapter->getFileName(['ktp_file']));
			$ktpFileName = $adapter->getFileName(['ktp_file']);

			// var_dump($npwpFileName);die;
			// 	$npwp_file 			= $_FILES['npwp_file'];
			// // var_dump($adapter->getFileName(['npwp_file']));die;


			// 	$sk_number_file 	= $_FILES['sk_number_file'];
			// 	$sk_change_file 	= $_FILES['sk_change_file'];
			// 	$sk_pengesahan_file = $_FILES['sk_pengesahan_file'];
			// $ktp_file 			= $_FILES['ktp_file'];

			$npwpsubtr_dot = str_replace(".", "", $params['npwp']);
			$npwpsubtr_strip = str_replace("-", "", $npwpsubtr_dot);

			$path = UPLOAD_PATH . '/document/submit/';

			if (!empty($npwpFileName)) {
				$filename_npwp = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $npwpFileName));
				// $path_old_npwp = $npwpfile['tmp_name'];
				// $path_new_npwp = $path . $filename_npwp;


				$adapter->setDestination($path);
				$adapter->addFilter('Rename', $filename_npwp, 'npwp_file');
				$adapter->receive('npwp_file');


				// move_uploaded_file($path_old_npwp, $path_new_npwp);
			} else {
				$filename_npwp = $data["NPWP_FILE"];
			}

			if (!empty($sknumberFileName)) {
				$filename_sk_number = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $sknumberFileName));
				// $path_old_sk_number = $sknumberfile['tmp_name'];
				// $path_new_sk_number = $path . $filename_sk_number;
				// move_uploaded_file($path_old_sk_number, $path_new_sk_number);
				$adapter->setDestination($path);
				$adapter->addFilter('Rename', $filename_sk_number, 'sk_number_file');
				$adapter->receive('sk_number_file');
			} else {
				$filename_sk_number = $data["PENDIRIAN_NUMBER_FILE"];
			}

			if (!empty($skchangeFileName)) {
				$filename_sk_change = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $skchangeFileName));
				// $path_old_sk_change = $skchangefile['tmp_name'];
				// $path_new_sk_change = $path . $filename_sk_change;
				// move_uploaded_file($path_old_sk_change, $path_new_sk_change);
				$adapter->setDestination($path);
				$adapter->addFilter('Rename', $filename_sk_change, 'sk_change_file');
				$adapter->receive('sk_change_file');
			} else {
				$filename_sk_change = $data["PERUBAHAN_FILE"];
			}

			if (!empty($skpengesahanFileName)) {
				$filename_sk_pengesahan = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $skpengesahanFileName));
				// $path_old_sk_pengesahan = $skpengesahanfile['tmp_name'];
				// $path_new_sk_pengesahan = $path . $filename_sk_pengesahan;
				// move_uploaded_file($path_old_sk_pengesahan, $path_new_sk_pengesahan);
				$adapter->setDestination($path);
				$adapter->addFilter('Rename', $filename_sk_pengesahan, 'sk_pengesahan_file');
				$adapter->receive('sk_pengesahan_file');
			} else {
				$filename_sk_pengesahan = $data["SK_PERUBAHAN_FILE"];
			}

			$lastsuggest = $this->_db->select()
				->from(array('A' => 'M_PRELIMINARY'))
				->joinLeft(
					array('B' => 'M_USER'),
					'A.LAST_SUGGESTEDBY = B.USER_ID',
					array('*')
				)
				->where('A.CUST_ID = ?', $cust_id);
			$datalastsuggest =  $this->_db->fetchRow($lastsuggest);

			$changes_type = $this->_changeType['code']['edit'];

			if (empty($datalastsuggest)) {
				$changes_type = $this->_changeType['code']['new'];
			}

			$getCustomer = $model->getCustomerById($cust_id);
			$cust_name   = $getCustomer['CUST_NAME'];
			$info        = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
			$change_id   = $this->suggestionWaitingApproval('Preliminary Document', $info, $changes_type, null, 'M_PRELIMINARY', 'TEMP_PRELIMINARY', $cust_id, $cust_name, $cust_id, $cust_name);

			try {
				$this->_db->beginTransaction();

				$dataInsert = [
					'CHANGES_ID'			=> $change_id,
					'CUST_ID'				=> $cust_id,
					'RELATIONSHIP'			=> $params['relationship'],
					// 'NPWP'					=> $params['npwp'],
					'NPWP'					=> $npwpsubtr_strip,
					'NPWP_FILE'				=> $filename_npwp,
					'PENDIRIAN_NUMBER'		=> $params['sk_number'],
					'PENDIRIAN_NUMBER_FILE'	=> $filename_sk_number,
					'PENDIRIAN_DATE'		=> $params['sk_date'] ?: $data['PENDIRIAN_DATE'],
					'PENDIRIAN_PLACE'		=> $params['sk_place'] ?: $data['PENDIRIAN_PLACE'],
					// 'PENDIRIAN_PLACE'		=> $params['sk_place'] ?: $data['PENDIRIAN_PLACE'],
					'PERUBAHAN_NUMBER'		=> $params['sk_change'],
					// 'PERUBAHAN_NUMBER'		=> $params['sk_change'] ?: $data['PERUBAHAN_NUMBER'],
					'PERUBAHAN_FILE'		=> $filename_sk_change,
					'PERUBAHAN_DATE'		=> $params['sk_change_date'],
					// 'PERUBAHAN_DATE'		=> $params['sk_change_date'] ?: $data['PERUBAHAN_FILE'],
					'SK_PERUBAHAN_NUMBER'	=> $params['sk_pengesahan'],
					// 'SK_PERUBAHAN_NUMBER'	=> $params['sk_pengesahan'] ?: $data['SK_PERUBAHAN_NUMBER'],
					// 'SK_PERUBAHAN_FILE'		=> ($sk_pengesahan_file['name'] != '') ? $filename_sk_pengesahan : $data['SK_PERUBAHAN_FILE'],
					'SK_PERUBAHAN_FILE'		=> $filename_sk_pengesahan,
					'SK_PERUBAHAN_DATE'		=> $params['sk_pengesahan_date'],
					// 'SK_PERUBAHAN_DATE'		=> $params['sk_pengesahan_date'] ?: $data['SK_PERUBAHAN_DATE'],
					'LAST_SUGGESTED'		=> new Zend_Db_Expr('now()'),
					'LAST_SUGGESTEDBY'		=> $this->_userIdLogin
				];
				$this->_db->insert('TEMP_PRELIMINARY', $dataInsert);

				for ($i = 0; $i < count($params['name']); $i++) {
					if ($ktpFileName['ktp_file_' . $i . '_'] != '') {
						$path = UPLOAD_PATH . '/document/submit/';

						$filename_ktp = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', basename($ktpFileName['ktp_file_' . $i . '_'])));
						// $path_old_ktp = $ktp_file['tmp_name'][$i];
						// $path_new_ktp = $path . $filename_ktp;
						// move_uploaded_file($path_old_ktp, $path_new_ktp);
						$adapter->setDestination($path);
						$adapter->addFilter('Rename', $filename_ktp, 'ktp_file_' . $i . '_');
						$adapter->receive('ktp_file_' . $i . '_');
					} else {
						$filename_ktp = $dataMember[array_search(strval($params['ktp'][$i]), array_column($dataMember, "ID_NUMBER"))]["ID_FILE"];
					}

					if ($params['entity_id'][$i] == 'B') {
						$idType = 'NPWP';
					} elseif ($params['entity_id'][$i] == 'M') {
						$idType = '-';
					} elseif ($params['citizenship'][$i] == 1) {
						$idType = 'KTP';
					} elseif ($params['citizenship'][$i] == 2) {
						$idType = 'Paspor';
					}

					$dataInsertMember = [
						'CHANGES_ID'	=> $change_id,
						'CUST_ID'		=> $cust_id,
						'NAME'			=> $params['name'][$i],
						'ENTITY_ID'		=> $params['entity_id'][$i],
						'CITIZENSHIP'	=> $params['citizenship'][$i],
						'POSITION'		=> $params['position'][$i],
						'SHARE'			=> $params['share'][$i],
						'STATUS'		=> $params['status'][$i],
						'ID_TYPE'		=> $idType,
						'ID_NUMBER'		=> $params['ktp'][$i],
						'ID_FILE'		=> $filename_ktp,
						'ADDRESS'		=> $params['address'][$i],
						'KELURAHAN'		=> $params['subdistrict'][$i],
						'KECAMATAN'		=> $params['district'][$i],
						'KAB_CITY'		=> $params['city'][$i]
					];
					$this->_db->insert('TEMP_PRELIMINARY_MEMBER', $dataInsertMember);
				}

				$this->_db->commit();

				Application_Helper_General::writeLog('USPD', 'Update Preliminary Document CUST_ID: ' . $cust_id);
				$this->setbackURL('/preliminary');
				$this->_redirect('/notification/submited/index');
			} catch (Exception $error) {
				$this->_db->rollBack();
				echo '<pre>';
				print_r($error->getMessage());
				echo '</pre><br>';
				die;
			}

			// if (empty($preliminary)) {
			// 	$insertArr = [
			// 		'CUST_ID'				=> $cust_id,
			// 		'RELATIONSHIP'			=> $params['relationship'] ?: null,
			// 		'NPWP'					=> $params['npwp'],
			// 		'NPWP_FILE'				=> $filename_npwp,
			// 		'PENDIRIAN_NUMBER'		=> $params['sk_number'],
			// 		'PENDIRIAN_NUMBER_FILE'	=> $filename_sk_number,
			// 		'PENDIRIAN_DATE'		=> $params['sk_date'] ?: null,
			// 		'PENDIRIAN_PLACE'		=> $params['sk_place'] ?: null,
			// 		'PERUBAHAN_NUMBER'		=> $params['sk_change'] ?: null,
			// 		'PERUBAHAN_FILE'		=> ($sk_change_file['name'] != '') ? $filename_sk_change : null,
			// 		'PERUBAHAN_DATE'		=> $params['sk_change_date'] ?: null,
			// 		'SK_PERUBAHAN_NUMBER'	=> $params['sk_pengesahan'] ?: null,
			// 		'SK_PERUBAHAN_FILE'		=> ($sk_pengesahan_file['name'] != '') ? $filename_sk_pengesahan : null,
			// 		'SK_PERUBAHAN_DATE'		=> $params['sk_pengesahan_date'] ?: null
			// 	];
			// 	$this->_db->insert('TEMP_PRELIMINARY', $insertArr);

			// 	for ($i = 0; $i < count($params['name']); $i++) {
			// 		$filename_ktp = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $ktp_file['name'][$i]));
			// 		$path_old_ktp = $ktp_file['tmp_name'][$i];
			// 		$path_new_ktp = $path . $filename_ktp;
			// 		move_uploaded_file($path_old_ktp, $path_new_ktp);

			// 		if ($params['entity_id'][$i] == 'B') {
			// 			$idType = 'NPWP';
			// 		} elseif ($params['entity_id'][$i] == 'M') {
			// 			$idType = '-';
			// 		} elseif ($params['citizenship'][$i] == 1) {
			// 			$idType = 'KTP';
			// 		} elseif ($params['citizenship'][$i] == 2) {
			// 			$idType = 'Paspor';
			// 		}

			// 		$insertMemberArr = [
			// 			'CUST_ID'		=> $cust_id,
			// 			'NAME'			=> $params['name'][$i],
			// 			'ENTITY_ID'		=> $params['entity_id'][$i],
			// 			'CITIZENSHIP'	=> $params['citizenship'][$i],
			// 			'POSITION'		=> $params['position'][$i],
			// 			'SHARE'			=> $params['share'][$i],
			// 			'STATUS'		=> $params['status'][$i],
			// 			'ID_TYPE'		=> $idType,
			// 			'ID_NUMBER'		=> $params['ktp'][$i],
			// 			'ID_FILE'		=> $filename_ktp,
			// 			'ADDRESS'		=> $params['address'][$i],
			// 			'KELURAHAN'		=> $params['subdistrict'][$i],
			// 			'KECAMATAN'		=> $params['district'][$i],
			// 			'KAB_CITY'		=> $params['city'][$i]
			// 		];
			// 		$this->_db->insert('TEMP_PRELIMINARY_MEMBER', $insertMemberArr);
			// 	}
			// 	Application_Helper_General::writeLog('USPD', 'Update Preliminary Document');
			// 	$this->setbackURL('/preliminary');
			// 	$this->_redirect('/notification/success');
			// } else {
			// 	$tempPreliminary = $this->_db->select()
			// 		->from(array('A' => 'TEMP_PRELIMINARY'), array('*'))
			// 		->where('A.CUST_ID = ?', $cust_id)
			// 		->query()->fetchAll();
			// 	$dir1 = $path . $tempPreliminary[0]['NPWP_FILE'];
			// 	if (is_file($dir1)) {
			// 		unlink($dir1);
			// 	}
			// 	$dir2 = $path . $tempPreliminary[0]['PENDIRIAN_NUMBER_FILE'];
			// 	if (is_file($dir2)) {
			// 		unlink($dir2);
			// 	}
			// 	if ($sk_change_file['name'] != '') {
			// 		$dir3 = $path . $tempPreliminary[0]['PERUBAHAN_FILE'];
			// 		if (is_file($dir3)) {
			// 			unlink($dir3);
			// 		}
			// 	}
			// 	if ($sk_pengesahan_file['name'] != '') {
			// 		$dir4 = $path . $tempPreliminary[0]['SK_PERUBAHAN_FILE'];
			// 		if (is_file($dir4)) {
			// 			unlink($dir4);
			// 		}
			// 	}
			// 	$updateArr = [
			// 		'RELATIONSHIP'			=> $params['relationship'] ?: null,
			// 		'NPWP'					=> $params['npwp'],
			// 		'NPWP_FILE'				=> $filename_npwp,
			// 		'PENDIRIAN_NUMBER'		=> $params['sk_number'],
			// 		'PENDIRIAN_NUMBER_FILE'	=> $filename_sk_number,
			// 		'PENDIRIAN_DATE'		=> $params['sk_date'] ?: null,
			// 		'PENDIRIAN_PLACE'		=> $params['sk_place'] ?: null,
			// 		'PERUBAHAN_NUMBER'		=> $params['sk_change'] ?: null,
			// 		'PERUBAHAN_FILE'		=> ($sk_change_file['name'] != '') ? $filename_sk_change : null,
			// 		'PERUBAHAN_DATE'		=> $params['sk_change_date'] ?: null,
			// 		'SK_PERUBAHAN_NUMBER'	=> $params['sk_pengesahan'] ?: null,
			// 		'SK_PERUBAHAN_FILE'		=> ($sk_pengesahan_file['name'] != '') ? $filename_sk_pengesahan : null,
			// 		'SK_PERUBAHAN_DATE'		=> $params['sk_pengesahan_date'] ?: null
			// 	];
			// 	$whereArr = ['CUST_ID = ?' => $cust_id];
			// 	$this->_db->update('TEMP_PRELIMINARY', $updateArr, $whereArr);

			// 	$preliminaryMember = $this->_db->select()
			// 		->from(array('A' => 'TEMP_PRELIMINARY_MEMBER'), array('*'))
			// 		->where('A.CUST_ID = ?', $cust_id)
			// 		->query()->fetchAll();
			// 	foreach ($preliminaryMember as $row) {
			// 		$dir = $path . $row['ID_FILE'];
			// 		if (is_file($dir)) {
			// 			unlink($dir);
			// 		}
			// 	}
			// 	$this->_db->delete('TEMP_PRELIMINARY_MEMBER', $whereArr);

			// 	for ($i = 0; $i < count($params['name']); $i++) {
			// 		$filename_ktp = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $ktp_file['name'][$i]));
			// 		$path_old_ktp = $ktp_file['tmp_name'][$i];
			// 		$path_new_ktp = $path . $filename_ktp;
			// 		move_uploaded_file($path_old_ktp, $path_new_ktp);

			// 		if ($params['entity_id'][$i] == 'B') {
			// 			$idType = 'NPWP';
			// 		} elseif ($params['entity_id'][$i] == 'M') {
			// 			$idType = '-';
			// 		} elseif ($params['citizenship'][$i] == 1) {
			// 			$idType = 'KTP';
			// 		} elseif ($params['citizenship'][$i] == 2) {
			// 			$idType = 'Paspor';
			// 		}

			// 		$insertMemberArr = [
			// 			'CUST_ID'		=> $cust_id,
			// 			'NAME'			=> $params['name'][$i],
			// 			'ENTITY_ID'		=> $params['entity_id'][$i],
			// 			'CITIZENSHIP'	=> $params['citizenship'][$i],
			// 			'POSITION'		=> $params['position'][$i],
			// 			'SHARE'			=> $params['share'][$i],
			// 			'STATUS'		=> $params['status'][$i],
			// 			'ID_TYPE'		=> $idType,
			// 			'ID_NUMBER'		=> $params['ktp'][$i],
			// 			'ID_FILE'		=> $ktp_file['name'][$i],
			// 			'ADDRESS'		=> $params['address'][$i],
			// 			'KELURAHAN'		=> $params['subdistrict'][$i],
			// 			'KECAMATAN'		=> $params['district'][$i],
			// 			'KAB_CITY'		=> $params['city'][$i]
			// 		];
			// 		$this->_db->insert('TEMP_PRELIMINARY_MEMBER', $insertMemberArr);
			// 	}
			// 	Application_Helper_General::writeLog('USPD', 'Update Preliminary Document');
			// 	$this->setbackURL('/preliminary');
			// 	$this->_redirect('/notification/success');
			// }
		}
	}

	public function detailmemberAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new preliminary_Model_Preliminary();

		$id = $this->_getParam('id');

		$getCity = $model->getCity();
		$cityArr = Application_Helper_Array::listArray($getCity, 'CITY_CODE', 'CITY_NAME');

		$data = $model->getPreliminaryMemberById($id);

		$data['KAB_CITY'] = $cityArr[$data['KAB_CITY']];

		echo json_encode($data);
	}

	public function premdocAction()
	{
		$cust_id = $this->_custIdLogin;
		$temp = $this->_request->getParam("temp");
		$master = $this->_request->getParam("master");
		$file = $this->_request->getParam("file");
		$path = UPLOAD_PATH . '/document/submit/';
		$fileName = '';

		if ($temp) {
			$getPremDoc = $this->_db->select()
				->from("TEMP_PRELIMINARY")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetch();
		}

		if ($master) {
			$getPremDoc = $this->_db->select()
				->from("M_PRELIMINARY")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetch();
		}

		switch ($file) {
			case 'NPWP':
				$fileName = $getPremDoc["NPWP_FILE"];
				break;
			case 'AKTA_PENDIRIAN':
				$fileName = $getPremDoc["PENDIRIAN_NUMBER_FILE"];
				break;
			case 'AKTA_PERUBAHAN':
				$fileName = $getPremDoc["PERUBAHAN_FILE"];
				break;
			case 'SK_PENGESAHAN':
				$fileName = $getPremDoc["SK_PERUBAHAN_FILE"];
				break;
			default:
				# code...
				break;
		}

		$this->_helper->download->file($file . "_" . $cust_id . "." . pathinfo($fileName)['extension'], $path . $fileName);
	}

	public function premdocmemberAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$cust_id = $this->_custIdLogin;
		$temp = $this->_request->getParam("temp");
		$master = $this->_request->getParam("master");
		$path = UPLOAD_PATH . '/document/submit/';

		if ($temp != null) {
			$getPremDocMember = $this->_db->select()
				->from("TEMP_PRELIMINARY_MEMBER")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();

			$fileName = $getPremDocMember[$temp]["ID_FILE"];
			$this->_helper->download->file($getPremDocMember[$temp]["NAME"] . "_" . $getPremDocMember[$temp]["ID_TYPE"] . "_" . $cust_id . "." . pathinfo($fileName)['extension'], $path . $fileName);
		}

		if ($master != null) {
			$getPremDocMember = $this->_db->select()
				->from("M_PRELIMINARY_MEMBER")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();

			$fileName = $getPremDocMember[$master]["ID_FILE"];
			$this->_helper->download->file($getPremDocMember[$master]["NAME"] . "_" . $getPremDocMember[$master]["ID_TYPE"] . "_" . $cust_id . "." . pathinfo($fileName)['extension'], $path . $fileName);
		}
	}

	public function viewAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$bgRegNumber = $this->_getParam('bgnumb');

		$table = "M_PRELIMINARY";
		$tableMember = 'M_PRELIMINARY_MEMBER';

		$cekTable = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE')
			->where('BG_REG_NUMBER = ?', $bgRegNumber)
			->query()->fetch();

		$temp = true;
		if ($cekTable) {
			if (!in_array($cekTable['BG_STATUS'], ['1', '2', '3'])) {
				$table = 'TEMP_BANK_GUARANTEE_PRELIMINARY';
				$tableMember = 'TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER';
			}
		} else {

			$temp = false;
			$cekTable = $this->_db->select()
				->from('T_BANK_GUARANTEE')
				->where('BG_REG_NUMBER = ?', $bgRegNumber)
				->query()->fetch();

			$table = 'T_BANK_GUARANTEE_PRELIMINARY';
			$tableMember = 'T_BANK_GUARANTEE_PRELIMINARY_MEMBER';
		}

		$model = new preliminary_Model_Preliminary();

		Zend_Session::namespaceUnset('tempPreliminary');

		$config = Zend_Registry::get('config');

		$entityCode = $config["bg"]["entity"]["code"];
		$entityDesc = $config["bg"]["entity"]["desc"];
		$entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));
		$this->view->entityArr = $entityArr;

		$citizenshipCode = $config["bg"]["citizen"]["code"];
		$citizenshipDesc = $config["bg"]["citizen"]["desc"];
		$citizenshipArr  = array_combine(array_values($citizenshipCode), array_values($citizenshipDesc));
		$this->view->citizenshipArr = $citizenshipArr;

		$statusCode = $config["bg"]["statusowner"]["code"];
		$statusDesc = $config["bg"]["statusowner"]["desc"];
		$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));
		$this->view->statusArr = $statusArr;

		$cekView = $this->_getParam("view");
		$this->view->cekView = $cekView;

		$getCustId = $this->_getParam("custid");

		$getPosition = $model->getPosition();
		$positionArr = Application_Helper_Array::listArray($getPosition, 'CODE', 'POSITION');
		$this->view->positionArr = $positionArr;

		$getRelationship = $model->getRelationship();
		$relationshipArr = Application_Helper_Array::listArray($getRelationship, 'NO', 'RELATION_WITH_BANK');
		$this->view->relationshipArr = $relationshipArr;

		$cust_id = $this->_custIdLogin;
		if ($getCustId != "") {
			$cust_id = $getCustId;
		}

		$data = $this->_db->select()
			->from(array('MP' => $table))
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = MP.CUST_ID',
				array('CUST_NAME')
			);
		if ($temp) {
			if (!in_array($cekTable['BG_STATUS'], ['1', '2', '3'])) {
				$data->where('BG_REG_NUMBER = ?', $cekTable['BG_REG_NUMBER']);
			} else {
				$data->where('MP.CUST_ID = ?', $cekTable['CUST_ID']);
			}
		} else {
			$data->where('BG_REG_NUMBER = ?', $cekTable['BG_REG_NUMBER']);
		}
		
		$data = $data->query()->fetch();

		$lastsuggest = $this->_db->select()
			->from(array('A' => 'M_PRELIMINARY'))
			->joinLeft(
				array('B' => 'M_USER'),
				'A.LAST_SUGGESTEDBY = B.USER_ID',
				array('*')
			)
			->where('A.CUST_ID = ?', $cust_id);
		$datalastsuggest =  $this->_db->fetchRow($lastsuggest);

		$this->view->datalastsuggest = $datalastsuggest;

		$lastapprove = $this->_db->select()
			->from(array('A' => 'M_PRELIMINARY'))
			->joinLeft(
				array('B' => 'M_USER'),
				'A.LAST_APPROVEDBY = B.USER_ID',
				array('*')
			)
			->where('A.CUST_ID = ?', $cust_id);
		$datalastapprove =  $this->_db->fetchRow($lastapprove);
		$this->view->datalastapprove = $datalastapprove;


		$data['CUST_NAME'] = $this->_custNameLogin;
		if ($getCustId != "") {
			$getCustName = $this->_db->select()
				->from("M_CUSTOMER", ["CUST_NAME"])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();
			$data["CUST_NAME"] = $getCustName[0]["CUST_NAME"];
		}
		$this->view->data = $data;

		// $dataMember = $model->getPreliminaryMember($cust_id);

		$dataMember = $this->_db->select()
			->from($tableMember)
			->joinRight(["C" => "M_CITYLIST"], "C.CITY_CODE = KAB_CITY", ["C.CITY_NAME"]);
		if ($temp) {
			if (!in_array($cekTable['BG_STATUS'], ['1', '2', '3'])) {
				$dataMember->where('BG_REG_NUMBER = ?', $cekTable['BG_REG_NUMBER']);
			} else {
				$dataMember->where('CUST_ID = ?', $cekTable['CUST_ID']);
			}
		} else {
			$dataMember->where('BG_REG_NUMBER = ?', $cekTable['BG_REG_NUMBER']);
		}
		// ->where('CUST_ID = ?', $cust_id)
		$dataMember = $dataMember->query()->fetchAll();
		$this->view->dataMember = $dataMember;

		$this->view->is_update = false;
		$dataTempPreliminary = $model->getTempPreliminary($cust_id);
		if ($dataTempPreliminary) {
			$this->view->is_update = true;
		}

		// echo '<pre>';
		// print_r($data);
		// echo '</pre><br>';

		$id 		= $this->_getParam('id');
		$mid 		= $this->_getParam('mid');
		$download 	= $this->_getParam('download');

		if ($download == 'file') {
			$path = UPLOAD_PATH . '/document/submit/';

			if ($id) {
				if ($id == 1) {
					$file = $data['NPWP_FILE'];
				} elseif ($id == 2) {
					$file = $data['PENDIRIAN_NUMBER_FILE'];
				} elseif ($id == 3) {
					$file = $data['PERUBAHAN_FILE'];
				} elseif ($id == 4) {
					$file = $data['SK_PERUBAHAN_FILE'];
				}

				$this->_helper->download->file($file, $path . $file);
			}

			if ($mid) {
				// $member = $model->getPreliminaryMemberByIdNumber($cust_id, $mid);

				$searchId = array_search($mid, array_column($dataMember, 'ID_NUMBER'));

				if ($searchId !== false) {
					$member = $dataMember[$searchId];
					$this->_helper->download->file($member['ID_FILE'], $path . $member['ID_FILE']);
				}
			}
		} else {
			Application_Helper_General::writeLog('VSPD', 'View Preliminary Document');
		}
	}
}
