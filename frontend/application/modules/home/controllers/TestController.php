<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'General/SystemBalance.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Extendedmodule/FPDF/FPDI_Protection.php';


class home_TestController extends Application_Main {


    
    public function indexAction()
    {   


        $this->_helper->_layout->setLayout('newlayout');
        

        $authkey = "12345";
    //Na/me of the original file (unprotected).
        $origFile = "sample.pdf"; 
        $rand = rand(10,100);
        //Name of the destination file (password protected and printing rights removed).
        $destFile ="sample_protected".$rand.".pdf";
        //Encrypt the book and create the protected file.
        // var_dump($destFile);die;
        try {
            // die;
            $this->pdfEncrypt($origFile, $authkey, $destFile );    

            $filepath =  $destFile;
    
    // Process download
            if(file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                exit;
            }
        } catch (Exception $e) {
            var_dump($e);die;
        }

    }


    function pdfEncrypt ($origFile, $authkey, $destFile){
        
        $pdf = new FPDI_Protection();
        $pdf->FPDF('P', 'in');
        //Calculate the number of pages from the original document.
        $pagecount = $pdf->setSourceFile($origFile);
        //Copy all pages from the old unprotected pdf in the new one.
        for ($loop = 1; $loop <= $pagecount; $loop++) {
            $tplidx = $pdf->importPage($loop);
            $pdf->addPage();
            $pdf->useTemplate($tplidx);
        }

        //Protect the new pdf file, and allow no printing, copy, etc. and
        //leave only reading allowed.
        $pdf->SetProtection(array(), $authkey);
        // var_dump($pdf);die;
        $pdf->Output($destFile, 'F');
        return $destFile;
    }

}
