<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';

class rdntrans_BulkdebitController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	protected $_destinationUploadDir = '';
	protected $_listAccValidate = '';
	protected $_maxRow = '';

	public function initController(){
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');

		$Customer = new Customer($this->_custIdLogin,$this->_userIdLogin);
		$AccArr = $Customer->getAccounts();
		$this->view->AccArr =  $AccArr;
		$this->_listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');
	}

	public function indexAction()
	{
		$this->setbackURL();

		$this->view->ccyArr = $this->getCcy();
		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustomerUser->getBeneficiaries();		
		$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		$select->where("B.BENEFICIARY_BANKSTATUS = 1");
		$select->where("B.BENEFICIARY_ISAPPROVE = 1");
		$select->where("B.CURR_CODE = 'IDR'");
		$select->joinleft(array('MCA' => 'M_CUSTOMER_ACCT'), 'B.BENEFICIARY_ACCOUNT = MCA.ACCT_NO ', array());
		$select->where("MCA.CUST_ID = ?", (string)$this->_custIdLogin);
		$select->where("MCA.ISRDN_TRX = ?", '1');
		//echo $select;die;		
		$resultBeneficiary = $this->_db->fetchAll($select);				
		$this->view->AccBenefArr =  $resultBeneficiary;

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

		if($this->_request->isPost() )
		{
			$filter = new Application_Filtering();
			$confirm = false;

			$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 			= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACBENEF 			= $filter->filter($this->_request->getParam('ACBENEF'), "ACCOUNT_NO");
//			$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
//			$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
//			$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");

			$minLen = 10;
			$maxLen = 20;
			$error_msg[0] = "";
			
				

			if (Zend_Validate::is($ACBENEF, 'NotEmpty') == false)
				$error_msg[0] = $this->language->_('Beneficiary Account cannot be left blank').".";
			elseif (Zend_Validate::is($ACBENEF, 'Digits') == false)
				$error_msg[0] = $this->language->_('Beneficiary Account must be numeric').".";
			elseif (strlen($ACBENEF) < $minLen || strlen($ACBENEF) > $maxLen)
				//$error_msg[0] = "Beneficiary Account length should be between $minLen and $maxLen.";
				$error_msg[0] = $this->language->_('Beneficiary Account length should be between 10 and 20.')."";

			/*elseif ($ACBENEF_ALIAS == "")
				$error_msg[0] = $this->language->_('Beneficiary Alias Name cannot be left blank.')."";*/
//			elseif (strlen($ACBENEF_ALIAS) > 35)
//				$error_msg[0] = $this->language->_('Maximum lengths of Alias Name is 35 characters. Please correct it').".";
//			else if ($ACBENEF_CCY == "")
//				$error_msg[0] = $this->language->_('Currency cannot be left blank').".";
			else if(!$PS_EFDATE)
				$error_msg[0] = $this->language->_('Payment Date can not be left blank').".";
			else
			{
				
				$acct = $this->_db->fetchRow(
					$this->_db->select()
					->from(array('C' => 'M_CUSTOMER_ACCT'))
					->where("ACCT_NO = ".$this->_db->quote($ACBENEF))
					->limit(1)
				);

				$validateacct   = new ValidateAccountSource($ACBENEF,$this->_custIdLogin,$this->_userIdLogin,$acct['CCY_ID']);				
				$PECode = $this->_db->fetchRow(
					$this->_db->select()
					->from(array('C' => 'M_USER'))
					->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CODE'))
					->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
					->limit(1)
				);
				//print_r($PECode);die;
				if ($validateacct->checkValidateRdnAcct($this->_custIdLogin,$PECode['CUST_CODE']) == FALSE){
					$error_msg[0] = $ACBENEF.' '.$this->language->_('accounts are not allowed to do this transaction').".";
				}
				
				
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );

				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'txt'));
				$extensionValidator->setMessage(
					$this->language->_('Extension file must be').' *.csv or *.txt'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );
					
					$getTypeArr = explode(".", $sourceFileName);
					$getType = $getTypeArr[1];

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}

						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{
								$rowNum = 0;

								$paramPayment = array( "CATEGORY"      		=> "BULK DEBET",
													   "FROM"       		=> "I",
													   "PS_NUMBER"     		=> "",
													   "PS_SUBJECT"    		=> $PS_SUBJECT,
													   "PS_EFDATE"     		=> $PS_EFDATE,
													   "_dateFormat"    	=> $this->_dateDisplayFormat,
													   "_dateDBFormat"    	=> $this->_dateDBFormat,
													   "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
													   "_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
													   "_createPB"     		=> $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
													   "_createDOM"    		=> false,        // cannot create DOM trx
													   "_createREM"    		=> false,        // cannot create REM trx
													   "ISRDN"      		=> "1",	
													  );

								$paramTrxArr = array();
								$totalamt = 0;
								foreach ( $csvData as $row )
								{									
									if(count($row)==11)
									{										
										$rowNum++;
										$sourceAcct = trim($row[0]);
										$sourceAcctName = trim($row[1]);
										$ccy = strtoupper(trim($row[2]));
										$stockCode = trim($row[3]);
										$stockName = trim($row[4]);
										$qty = strtoupper(trim($row[5]));
										$price = trim($row[6]);
										$amount = trim($row[7]);
										$message = trim($row[8]);
										$addMessage = trim($row[9]);
										$email = strtoupper(trim($row[10]));
				
										$totalamt = $totalamt+$amount;
				
										$filter = new Application_Filtering();

										$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
										$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
										$EMAIL 				= $filter->filter($email, "EMAIL");
										// $ACCTSRC 			= $filter->filter($sourceAcct, "ACCOUNT_NO");
										$ACCTSRC 			= $sourceAcct;
										$TRANSFER_TYPE 		= 'PB';

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										$filter->__destruct();
										unset($filter);
										$srcData = $this->_db->select()
										->from(array('M_BENEFICIARY'),array('BENEFICIARY_ALIAS'))
										->where('BENEFICIARY_ACCOUNT = ?', $ACBENEF)
										->limit(1)
										;
										//echo $srcData;die;
										$acsrcData = $this->_db->fetchRow($srcData);
										//print_r($acsrcData);die;
										$paramTrx = array(	"TRANSFER_TYPE" 	=> $TRANSFER_TYPE,
															"TRA_AMOUNT" 		=> $TRA_AMOUNT_num,
															"TRA_MESSAGE" 		=> $TRA_MESSAGE,
															"TRA_REFNO" 		=> $TRA_REFNO,
															"ACCTSRC" 			=> $ACCTSRC,
															"ACBENEF" 			=> $ACBENEF,
															"ACBENEF_NAME"		=> $sourceAcctName,
															"ACBENEF_CCY" 		=> $ACBENEF_CCY,
															"ACBENEF_EMAIL" 	=> $EMAIL,

														// for Beneficiary data, except (bene CCY and email), must be passed by reference
															"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
															"ACBENEF_ALIAS" 			=> $acsrcData['BENEFICIARY_ALIAS'],	
															"STOCK_CODE" 			=> $stockCode ,
															"STOCK_NAME" 			=> $stockName,
															"QTY" 				=> $qty,
															"PRICE" 			=> $price,
															"TOTAL" 			=> $amount																			
														 );

										array_push($paramTrxArr,$paramTrx);
									}
									else
									{										
										$error_msg[0] = $this->language->_('Wrong File Format').'';
										break;
									}
								}
							}
							// kalo jumlah trx lebih dari setting
							else
							{
								$error_msg[0] = 'The number of rows to be imported should not more than '.$this->_maxRow.'.';
							}

							// kalo gak ada error
							if(!$error_msg[0])
							{
								$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								//print_r($paramPayment);die;
								$paramPayment['TOTAL_AMOUNT_RDN'] = $totalamt;
								//print_r($paramTrxArr);die;
								$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr);
								
								$payment 		= $validate->getPaymentInfo();								
								
								$i = 0;
								foreach($payment['acctsrcArr'] as $key=>$dataAcctType){
									//Zend_Debug::dump($dataAcctType);
									$paramTrxArr[$i++]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
								}

								if($validate->isError() === false)	// payment data is valid
								{
									$confirm = true;

									$validate->__destruct();
									unset($validate);
								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
									//var_dump($errorTrxMsg);die;
									$validate->__destruct();
									unset($validate);

									if($errorMsg)
									{
										$error_msg[0] = $errorMsg;
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else //kalo total record = 0
						{
							//$error_msg[0] = 'Wrong File Format. There is no data on csv File.';
							$error_msg[0] = $this->language->_('Wrong File Format').'.';
						}
					}
				}
				else
				{
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
				}

			}

			if($confirm)
			{
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				//echo '<pre>';
				//print_r($content);die;
				$sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
				$sessionNamespace->content = $content;
				$this->_redirect('/rdntrans/bulkdebit/confirm');
			}

			$this->view->error 		= true;
			//$error_msg[0] = $this->language->_('Error').': '.$error_msg[0];
			$error_msg[0] = $error_msg[0];
			$this->view->report_msg	= $this->displayError($error_msg);

			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACBENEF = $ACBENEF;
			$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
			$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
			$this->view->CURR_CODE = $ACBENEF_CCY;
			$this->view->PSEFDATE = $PS_EFDATE;
		}
		Application_Helper_General::writeLog('IPMO','Viewing Create Bulk Debit Payment by Import File (CSV)');
	}

	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
		$data = $sessionNamespace->content;
		//Zend_Debug::dump($data);die;
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only
		foreach($AccArr as $val){
			if($val['ACCT_NO']==$data['paramTrxArr'][0]['ACBENEF']){
				$data['paramTrxArr'][0]['ACBENEF_ALIAS'] = $val['ACCT_ALIAS_NAME'];
			}
		}
		
		$select = $this->_db->select()
							->from(	array('MBU'=>'M_BENEFICIARY_USER'),
									array(
											'BENEFICIARY_ID' =>'MBU.BENEFICIARY_ID',
											'BENEFICIARY_NAME' =>'MB.BENEFICIARY_NAME',
											'BENEFICIARY_TYPE' =>'MB.BENEFICIARY_TYPE',
											'CURR_CODE' =>'MB.CURR_CODE'
										))
							->where('MB.BENEFICIARY_ACCOUNT = ? ',$data['paramTrxArr']['0']['ACBENEF'])
							->where('MBU.USER_ID   = ?' , $this->_userIdLogin)
							->where('MBU.CUST_ID   = ?' , $this->_custIdLogin)
							->joinLeft	(
											array('MB' => 'M_BENEFICIARY'),
											'MB.BENEFICIARY_ID = MBU.BENEFICIARY_ID',
											array('BENEFICIARY_ACCOUNT' =>'MB.BENEFICIARY_ACCOUNT')
										);
		//echo $select;
		$arrBenef = $this->_db->fetchAll($select);
		//print_r($arrBenef);die;
		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->ACBENEF = $data['paramTrxArr'][0]['ACBENEF'];
		$this->view->ACBENEF_CUR = $arrBenef[0]['CURR_CODE'];
		$this->view->ACBENEF_CCY = $data['paramTrxArr'][0]['ACBENEF_CCY'];
		$this->view->ACBENEF_BANKNAME = $data['paramTrxArr'][0]['ACBENEF_BANKNAME'];
		$this->view->ACBENEF_ALIAS = $data['paramTrxArr'][0]['ACBENEF_ALIAS'];

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;
		$this->view->totalRecord = $data["payment"]["countTrxTOTAL"];

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkDebet']);
				$this->_redirect('/rdntrans/bulkdebit');
			}

			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= $this->_paymenttype['code']['bulkdebet'];
			$param['PS_CCY']  = $data['paramTrxArr'][0]['ACBENEF_CCY'];

			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				
				$param['TRANSACTION_DATA'][] = array(
						'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
						'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
						'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
						'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_NAME'],
						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'],
						// 'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
						// 'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
						// 'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
						// 'CLR_CODE' 					=> $row['BANK_CODE'],
						'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'],
						'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
						'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
						'TRA_REFNO' 				=> $row['TRA_REFNO'],

						'STOCK_CODE' 				=> $row['STOCK_CODE'],
						'STOCK_NAME' 				=> $row['STOCK_NAME'],
						'QTY' 					=> $row['QTY'],
						'PRICE' 				=> $row['PRICE'],
						'sourceAccountType' 		=> $row['ACCOUNT_TYPE'],
				);
			}

			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = 'IPMO';
			// Zend_Debug::dump($param);die;

			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin, TRUE);

			$result = $BulkPayment->createPayment($param);
			if($result)
			{
				unset($_SESSION['confirmBulkDebet']);
				$this->_redirect('/notification/success');
			}
			else
			{
				$this->view->error 		= true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/multidebet/bulk');
			}
		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
				if(!empty($csvData['0']['0'])){
					$searchString = ';';

					 if( strpos($csvData['0']['0'], $searchString) !== false ) {
						     $Csv = new Application_Csv (  $fileName, $separator = ";" );
				    		     $csvData = $Csv->readAll ();
					 }

				}
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}
	
	public function downloadtxtAction(){	

		$ns = new Zend_Session_Namespace('language');
		$sessionLanguage = $ns->langCode;
				
		$handle = fopen("file.txt", "w");
		if($sessionLanguage == 'id'){
			fwrite($handle, "NO REKENING RDN,NAMA REKENING RDN,MATA UANG,KODE SAHAM,NAMA SAHAM,JML(LEMBAR),HARGA,TOTAL,PESAN,PESAN TAMBAHAN,EMAIL");
			$filename = "RDN_Multi_Debit_Template_id.txt";
		}else{
			fwrite($handle, "RDN ACCT NO,RDN ACCT NAME,CCY,STOCK CODE,STOCK NAME,QTY(SHARES),PRICE,TOTAL AMOUNT,MESSAGE,ADDITIONAL MESSAGE,EMAIL");
			$filename = "RDN_Multi_Debit_Template_en.txt";
		}
	    
	    fclose($handle);
	
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename='.basename($filename));
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize('file.txt'));
	    readfile('file.txt');
	    exit;
	}
}
