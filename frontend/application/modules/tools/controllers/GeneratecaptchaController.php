<?php
class tools_GeneratecaptchaController extends Application_Main {
	
	public function indexAction() 
	{
		$dirs = glob('captcha_image/*');
		
		foreach ($dirs as $value) {
			unlink($value);
		}
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		//---------------------- GENERATE CAPTCHA IMAGE  --------------------------------------
		$captcha = Application_Captcha::generateCaptcha ();
		// $this->view->captchaId = $captcha ['id']; //returns the ID given to session &amp; image
		// $this->view->captImgDir = $captcha ['imgDir'];
		echo '<img src="'.$captcha ['imgDir'] . $captcha ['id'].'.png" alt=""/><br>
		<input type="hidden" value="'.$captcha ['id'].'" name="captcha[id]"/>';
		//---------------------	END GENERATE CAPTCHA IMAGE -------------------------------------
	}	
}
