<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';

class services_TransferinhouseController extends services_Model_Services
{

	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$this->view->compName = $this->_custNameLogin;

		$this->view->acct_num = '7001486335';
		$this->view->acct_dest = '7008631435';
		$this->view->amount = '10000';


		if ($this->_getParam('submit')) {
					
			$filters = array(
		        'acct_source' => array('StringTrim','StripTags'),
		        'acct_dest'   => array('StringTrim','StripTags'),
		        'amount' 	  => array('StringTrim','StripTags'),
		        'notes' 	  => array('StringTrim','StripTags')
			);

			$validators = array(
	            
				'acct_source' => array(
	            				'NotEmpty',
								'messages' => array(
											    $this->language->_('Source Account Number Can not be empty'),
	                                          )
							),
				'acct_dest'  => array(
								'NotEmpty',
								'messages' => array(
												$this->language->_('Destination Account Number')
											  )
							),
				'amount'  	=> array(
								'NotEmpty',
								'messages' => array(
												$this->language->_('Amount Can not be empty')
											  )
							)
			);

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			$params = $this->_request->getParams();

			if($zf_filter_input->isValid())
			{					

				$clientUser  =  new SGO_Soap_ClientUser();

				$paramObj = array(
					'source_account_number' 		=> $zf_filter_input->acct_source,
					'beneficiary_account_number'	=> $zf_filter_input->acct_dest,
					'amount'		=> $zf_filter_input->amount,
					'currency'		=> 'IDR',
					'description' 	=> $this->_getParam('notes')
				);
				Zend_Debug::dump($paramObj); die;
				$success = $clientUser->callapi('pocTransferInhouse',$paramObj,'poc/transfer/inhouse');
			
				if($clientUser->isTimedOut()){
					$returnStruct = array(
							'ResponseCode'	=>'XT',
							'ResponseDesc'	=>'Service Timeout',
					);
				}else{
					$result  = $clientUser->getResult();

					$this->view->result = json_encode($result);

					if($result->error_code == '0000'){

						// $balancearr[$value['AIRLINE_CODE']] = $result->account_info->available_credit;
					}
				}

			}
			else{

				$this->view->error = true;

				$error = $zf_filter_input->getMessages();

				$errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

                $this->view->error_msg = $errorArray;
			}

			foreach(array_keys($filters) as $field)
				$this->view->$field = $this->_getParam($field);


		}
	}

	
}
