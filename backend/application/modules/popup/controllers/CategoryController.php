<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
class popup_CategoryController extends Application_Main
{
	 protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
	
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newpopup');
	    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
	
	
	    $fields = array(
						'category'      => array('field' => 'CATEGORY',
											      'label' => 'Category',
											      'sortable' => true),
					
						/*'swift_code'     => array('field' => 'SWIFT_CODE',
											      'label' => 'Swift Code',
											      'sortable' => true)*/
	                  );
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							//'clearing_code'  => array('StringTrim','StripTags'),
// 							'city'           => array('StringTrim','StripTags','StringToUpper'),
							'category'      => array('StringTrim','StripTags'),
							//'swift_code'     => array('StringTrim','StripTags')
		);
		
		$validators =  array(
				'category' => array(
						'NotEmpty',
						array('Db_NoRecordExists', array('table' => 'M_CATEGORY', 'field' => 'CATEGORY')),
						'messages' => array(
								'Cannot be empty',
								'Category already exist',
						)
				)
		);
		
		
		$zf_filter = new Zend_Filter_Input($filterArr,$validators,$this->_request->getParams(),$this->_optionsValidator);
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CATEGORY')); 
// 		$select->where("A.BANK_ISDISPLAYED = 1");	
		
		$errors = $zf_filter->getMessages();
		$rest = empty($errors);

	        
		if($this->_request->isPost() && $rest)
		{
			
// 			print_r($filter);
			
			//$fclearing_code = $zf_filter->getEscaped('clearing_code');
			$fcategory     = $zf_filter->getEscaped('category');
			//$fswift_code    = $zf_filter->getEscaped('swift_code');
// 			$fcity          = $zf_filter->getEscaped('city');
			$content = array(
					'CATEGORY' 	 => $fcategory,
			);
// 			print_r($fcategory);die;
			try {
				$this->_db->beginTransaction();
				$this->_db->insert('M_CATEGORY',$content);
			} catch(Exception $e) {
					//rollback changes
					$this->_db->rollBack();
				
			}
			
			
		}else if($this->_request->isPost()){
			$this->view->error = '1';
			if(!empty($errors['category']['recordFound'])){
				$error = $errors['category']['recordFound'];
			}else{
				$error = $errors['category']['isEmpty'];
			}
			
			$this->view->report_msg = $error;
			
		}
		
	
		//$this->view->success = true;
		
		
	    $select->order($sortBy.' '.$sortDir);   
		$this->paging($select);
// 		$this->frontendLog('V',$this->_moduleDB,null);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}
	
	public function deleteAction()
	{
		$category = $this->_getParam('category');
		$this->_db->delete('M_CATEGORY',array('CATEGORY = ?'=>$category));
		$this->_redirect('/popup/category');
	}

}
