<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'General/Charges.php';
require_once 'General/Settings.php';
require_once ('../../frontend/application/modules/purchasing/models/Purchasing.php');
require_once 'Crypt/AESMYSQL.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class suspectreportnew_DetailController extends Application_Main
{

	// public function initController(){
	// 		  $this->_helper->layout()->setLayout('popup');
	// }

	protected $_controllerList 	= "index";
	protected $_destinationUploadDir = '';
	protected $_maxRow = '';

	public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

	public function indexAction()
	{
		//ini_set('display_errors', 1);
		//ini_set('display_startup_errors', 1);
		//error_reporting(E_ALL);

		$this->_helper->layout()->setLayout('newlayout');
		$filter 			= new Application_Filtering();
		// $PS_NUMBER 			= $filter->filter($this->_getParam('payRef')				, "PS_NUMBER");
		 $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	     $password = $sessionNamespace->token; 
	     $this->view->token = $sessionNamespace->token;  

	    $submit = $this->_getParam('submit');
    	$reset  = $this->_getParam('reset');

		// if($submit){
        if($this->_request->isPost() )
        {

        	$paymentnumber  = $this->_request->getParam('payReff');
        	$failed   		= $this->_request->getParam('failed');
			$upload   		= $this->_request->getParam('upload');
			$update1   		= $this->_request->getParam('update1');

			$sqlTransaction = $this->_db->select()
								->from(array('A' => 'T_PSLIP'),array())
								->join(array('B' => 'T_TRANSACTION'),'A.PS_NUMBER = B.PS_NUMBER',array())
								->join(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('A.*','B.*','C.CUST_NAME'))
								->where("A.PS_NUMBER = ".$this->_db->quote($paymentnumber))
								->where("A.PS_STATUS = '9'");
			$result = $this->_db->fetchAll($sqlTransaction);

			$insertArr = array();
			$insertArr['PS_NUMBER'] = $paymentnumber;
			$insertArr['CUST_ID'] = $result[0]['CUST_ID'];
			$insertArr['CUST_NAME'] = $result[0]['CUST_NAME'];
			$insertArr['SUGGEST_USER'] = $this->_userIdLogin;
			$insertArr['SUGGEST_DATE'] = new Zend_Db_Expr('now()');
			$insertArr['PS_STATUS'] = 23;
			$insertArr['TYPE'] = $result[0]['PS_TYPE'];
			$insertArr['SUGGEST_STATUS'] = 0;
			$insertArr['EXCEPTION_TYPE'] = 1;

			$this->_db->insert('T_PSLIP_EXCEPTION_REPAIR', $insertArr);
			$lastIdTemp = $this->_db->fetchOne('select @@identity');

			$sqlTransaction1 = $this->_db->select()
								->from(array('A' => 'T_TRANSACTION'),array('TRANSACTION_ID' => 'A.TRANSACTION_ID'))
								->where('A.PS_NUMBER =? ',$paymentnumber);
			$result1 = $this->_db->fetchAll($sqlTransaction1);

			foreach ($result1 as $row) {
				
				$updatestatus   = $this->_request->getParam('updatestatus'.$row['TRANSACTION_ID']);
				$trx_id   		= $this->_request->getParam('trx_id'.$row['TRANSACTION_ID']);

	        	if ($upload != NULL && $update1 == 1) {

	        		unset($failed);
	        		unset($updatestatus);

					$adapter = new Zend_File_Transfer_Adapter_Http();

		            $adapter->setDestination ( $this->_destinationUploadDir );
		            $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
		            $extensionValidator->setMessage(
		                $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
		            );

		            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
		            $sizeValidator->setMessage(
		                'Error: File exceeds maximum size'
		            );

		            $adapter->setValidators ( array (
		                $extensionValidator,
		                $sizeValidator,
		            ));

		            $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
		            $extension = explode('.', $sourceFileName);
		            $extensionName = $extension[1];
		            $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
		            $adapter->addFilter ( 'Rename',$newFileName  );

		            if ($adapter->isValid ())
		                {

		                if ($adapter->receive()) {

		                    $data = $this->_helper->parser->parseCSV($newFileName);
		                    unset($data[0]);

		                    foreach ( $data as $row ){
								
								$no 			= trim($row[0]);
								$trxid 			= trim($row[1]);
								$sourcebank 	= trim($row[2]);
								$source 		= trim($row[3]);
								$benefbank 		= trim($row[4]);
								$benef 			= trim($row[5]);
								$ccy 			= trim($row[6]);
								$amount 		= trim($row[7]);
								$rc 			= trim($row[8]);
								$status 		= trim($row[9]);
								$updateto 		= trim($row[10]);	

								if ($updateto == 1) {
									$psstatus = 3;
								}elseif ($updateto == 0) {
									$psstatus = 4;
								}

								$insertArrDetail 					= array();
								$insertArrDetail['SUGGEST_ID'] 		= $lastIdTemp;
								$insertArrDetail['TRANSACTION_ID'] 	= $row[1];
								$insertArrDetail['TRA_STATUS'] 		= $psstatus;
								$insertArrDetail['MUST_REVERSE'] 	= 0;

								$this->_db->insert('T_PSLIP_EXCEPTION_REPAIR_DETAIL', $insertArrDetail);
							}

		                }
		            }

				}

				if ($updatestatus != NULL || $failed != NULL) {
					
					if (!empty($updatestatus)) {
						unset($failed);

						if($updatestatus == 6)
						{
							$update = 4;
						}
						else
						{	

							$update = 3;
							
						}

						// foreach($result as $row)
						// {

						// 	$insertArrDetail 					= array();
						// 	$insertArrDetail['SUGGEST_ID'] 		= $lastIdTemp;
						// 	$insertArrDetail['TRANSACTION_ID'] 	= $row['TRANSACTION_ID'];
						// 	$insertArrDetail['TRA_STATUS'] 		= $update;
						// 	$insertArrDetail['MUST_REVERSE'] 	= 0;
						// 	Zend_Debug::dump($insertArrDetail);

						// 	//$this->_db->insert('T_PSLIP_EXCEPTION_REPAIR_DETAIL', $insertArrDetail);
						// }

						$insertArrDetail = array(
							'SUGGEST_ID' 		=> $lastIdTemp,
							'TRANSACTION_ID' 	=> $trx_id,
							'TRA_STATUS' 		=> $update,
							'MUST_REVERSE'		=> 0
						);
						
						try{
							$this->_db->insert('T_PSLIP_EXCEPTION_REPAIR_DETAIL', $insertArrDetail);
						}catch(Exception $e){
							echo $e;die();
						}

					}elseif (!empty($failed)) {

						foreach($result as $row)
						{
							if($failed == 6)
							{
								$update = 4;
							}
							else
							{	

								$update = 3;
							
							}
							
							$insertArrDetail 					= array();
							$insertArrDetail['SUGGEST_ID'] 		= $lastIdTemp;
							$insertArrDetail['TRANSACTION_ID'] 	= $row['TRANSACTION_ID'];
							$insertArrDetail['TRA_STATUS'] 		= $update;
							$insertArrDetail['MUST_REVERSE'] 	= 0;
							
							$this->_db->insert('T_PSLIP_EXCEPTION_REPAIR_DETAIL', $insertArrDetail);
						}
						
					}
				}

			}

			$insertHistory['DATE_TIME'] = new Zend_Db_Expr('now()');
			$insertHistory['PS_NUMBER'] = $paymentnumber;
			$insertHistory['USER_LOGIN'] = $this->_userIdLogin;
			$insertHistory['CUST_ID'] = $result[0]['CUST_ID'];
			$insertHistory['HISTORY_STATUS'] = '26';
			$insertHistory['PS_REASON'] = 'Exception Repair';
			$this->_db->insert('T_PSLIP_HISTORY', $insertHistory);

			$ns = new Zend_Session_Namespace('FVC');
    		$ns->backURL = $this->view->backURL;
			$this->_redirect('/notification/success/request');
        }

		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($filter->filter($this->_getParam('payRef'), "PS_NUMBER"));
		$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

		$pdf 				= $filter->filter($this->_getParam('pdf')					, "BUTTON");
		$this->_paymentRef 	= $PS_NUMBER;

		$conf = Zend_Registry::get('config');
		$paymentStatus = $conf['payment']['status'];
		$paymentStatusFlip = array_flip($paymentStatus['code']);

		$this->view->paymentStatus = $paymentStatus;
		$this->view->paymentStatusFlip = $paymentStatusFlip;

		$setting = new Settings();
		$master_bank_telp = $setting->getSetting('master_bank_telp');

		$this->view->master_bank_telp = $master_bank_telp;

		$sessionNamespace = new Zend_Session_Namespace('URL_CP_PR');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ?
									   $sessionNamespace->URL : '/'.$this->view->modulename.'/'.$this->_controllerList.'/index';

		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);

		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";

		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($arrPayType as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";

		$select	= $this->_db->select()
							->from(		array(	'P' 		=> 'T_PSLIP'),
										array(	'payStatus'	=>$casePayStatus,
												'PS_STATUS'	=>'P.PS_STATUS',
												'ps_statusrepair'	=> 'R.PS_STATUS',
												'compCode'	=>'C.CUST_ID',
												'compName'	=>'C.CUST_NAME',
												'paySubject'=>'P.PS_SUBJECT',
												'pscount'	=>'P.PS_TXCOUNT',
												'created'	=>'P.PS_CREATED',
												'createdby'	=>'P.PS_CREATEDBY',
												'updated'	=>'P.PS_UPDATED',
												'efdate'	=>'P.PS_EFDATE',
												'sumamount'	=>'P.PS_TOTAL_AMOUNT',
												'psperiodic'=>'P.PS_PERIODIC',
												'periodicuom'	=>'TP.PS_EVERY_PERIODIC_UOM',
												'periodicnumber'=>'TP.PS_PERIODIC_NUMBER',
												'PS_CATEGORY'	=>'P.PS_CATEGORY',
												'ccy'			=>'P.PS_CCY',
												'ps_file'		=>'P.PS_FILE',
												'transmission'	=>'',
												'reference'		=>'',
												'traceno'		=>'P.UUID',
												'releaser'		=>'P.PS_RELEASER_USER_LOGIN',
												'challengecode'	=>'P.PS_RELEASER_CHALLENGE',
												'PS_TYPE'		=>'P.PS_TYPE',
												'bookrate'		=>'T.BOOK_RATE',
												'bookbuy'		=>'T.BOOK_RATE_BUY',
												'amount'		=>'T.TRA_AMOUNT',
												'transferfee'	=>'T.TRANSFER_FEE',
												'provfee'		=>'T.PROVISION_FEE',
												'fafee'			=>'T.FULL_AMOUNT_FEE',
												'sourceccy'		=>'T.SOURCE_ACCOUNT_CCY',
												'beneccy'		=>'T.BENEFICIARY_ACCOUNT_CCY',
												'payType'		=>$casePayType,
												'BALANCE_TYPE'=>new Zend_Db_Expr("(SELECT BALANCE_TYPE FROM T_PERIODIC_DETAIL WHERE PS_PERIODIC = P.PS_PERIODIC limit 1)"),
												'sourceresident'=>'CA.ACCT_RESIDENT',
												'sourcecitizen'=>'CA.ACCT_CITIZENSHIP',
												'sourcecategory'=>'CA.ACCT_CATEGORY',
												// 'sourceidtype'=>'CA.ACCT_ID_TYPE',
												// 'sourceidnum'=>'CA.ACCT_ID_NUM',
												'benefresident'=>'T.BENEFICIARY_RESIDENT',
												'benefcitizen'=>'T.BENEFICIARY_CITIZENSHIP',
												'benefcategory'=>'T.BENEFICIARY_CATEGORY',
												// 'benefidtype'=>'T.BENEFICIARY_ID_TYPE',
												// 'benefidnum'=>'T.BENEFICIARY_ID_NUMBER',
												'message'=>'T.TRA_MESSAGE',
												'lldidentity'=>'T.LLD_IDENTITY',
												'lldpurpose'=>'T.LLD_TRANSACTION_PURPOSE',
												'lldrel'=>'T.LLD_TRANSACTOR_RELATIONSHIP',
												'T.EQUIVALENT_AMOUNT_IDR',
												'T.EQUIVALENT_AMOUNT_USD',
												'T.TRACE_NO',
												'PS_REMAIN'		=>'P.PS_REMAIN'

											))
							->joinLeft(	array(	'C' => 'M_CUSTOMER' ),'P.CUST_ID = C.CUST_ID',array())
							->joinLeft(	array(	'T' => 'T_TRANSACTION' ),'T.PS_NUMBER = P.PS_NUMBER',array('T.TRA_AMOUNT'))
							->joinLeft(	array(	'TP' => 'T_PERIODIC' ),'TP.PS_PERIODIC = P.PS_PERIODIC',array())
							->joinLeft( array( 'CA' => 'M_CUSTOMER_ACCT'), 'T.SOURCE_ACCOUNT = CA.ACCT_NO', array())
							->joinLeft( array( 'R' => 'T_PSLIP_EXCEPTION_REPAIR'), 'R.PS_NUMBER = P.PS_NUMBER', array())
							// ->where('C.CUST_ID = ? ', $this->_custIdLogin)
							->where('P.PS_NUMBER =? ',$PS_NUMBER);
		$pslip = $this->_db->fetchRow($select);

		$PSSTATUS  = $pslip["PS_STATUS"];
		$payStatus = $pslip["payStatus"];
// die;
		if($PSSTATUS == 5){

			// COMPLETED WITH (_) TRANSACTION (S) FAILED
			// TRA_STATUS FAILED (4)

			$select = $this->_db->select()
								->from('T_TRANSACTION',array('countfailed'=>'count(TRANSACTION_ID)'))
								->where("TRA_STATUS = '4' AND PS_NUMBER = ?",$PS_NUMBER);
			$countFailed = $this->_db->fetchOne($select);

			if($countFailed == 0) 	$value = $payStatus;
			else 					$value = 'Completed with '.$countFailed.' Failed Transaction(s)';

		}
		else $value = $payStatus;

		if($pslip['paySubject']== NULL){
			$pssubject = "no subject"; 
		}else{
			$pssubject = $pslip['paySubject'];
		}

		$persenLabel = $pslip["BALANCE_TYPE"] == '2' ? ' %' : '';
		// View Data
		$this->_tableMst[0]["label"] = "Payment Ref#";
		$this->_tableMst[1]["label"] = "Payment Status";
		$this->_tableMst[2]["label"] = "Company Code";
		$this->_tableMst[3]["label"] = "Company Name";
		$this->_tableMst[4]["label"] = "Payment Subject";
		$this->_tableMst[5]["label"] = "Master Account";
		$this->_tableMst[6]["label"] = "Created Date";
		$this->_tableMst[7]["label"] = "Updated Date";
		$this->_tableMst[8]["label"] = "Payment Date";
		// $this->_tableMst[9]["label"] = "Transmission";
		// $this->_tableMst[10]["label"] = "User Reference";
		//$this->_tableMst[11]["label"] = "Bank Response";
		//$this->_tableMst[12]["label"] = "Bank Resp. Code";
		$this->_tableMst[11]["label"] = "Trace No.";
		$this->_tableMst[12]["label"] = "Releaser";
		$this->_tableMst[13]["label"] = "Challenge Code";
		$this->_tableMst[14]["label"] = "Total Payment";
		$this->_tableMst[15]["label"] = "Payment Type";

		$this->view->ps_file 		 = $pslip['ps_file'];
		$this->view->pscount 		 = $pslip['pscount'];
		$this->view->psnumber 		 = $PS_NUMBER;
		$this->view->paymentstatus 	 = $value;
		$this->view->ps_status 		 = $pslip['PS_STATUS'];
		$this->view->ps_statusrepair = $pslip['ps_statusrepair'];
		$this->view->compCode		 = $pslip['compCode'];
		$this->view->compname 		 = $pslip['compName'];
		$this->view->pssubject 		 = $pssubject;
		$this->view->message 		 = $pslip['message'];
		$this->view->addmessage 	 = $pslip['addmessage'];

		$suggested = '(Last Suggested by '.$pslip['createdby'].' - '.$pslip['updated'].') - Unread Suggestion';
		$this->view->suggested = $suggested;

		// $this->_tableMst[0]["value"] = $PS_NUMBER;
		// $this->_tableMst[1]["value"] = $value;
		//$this->_tableMst[2]["value"] = $pslip['compCode'];
		// $this->_tableMst[3]["value"] = $pslip['compName'];
		// $this->_tableMst[4]["value"] = $pslip['paySubject'];
		$this->_tableMst[5]["value"] = "";
		$this->_tableMst[6]["value"] = Application_Helper_General::convertDate($pslip['created'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		$this->_tableMst[7]["value"] = Application_Helper_General::convertDate($pslip['updated'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		// $this->_tableMst[8]["value"] = Application_Helper_General::convertDate($pslip['efdate'],$this->_dateViewFormat);
		$this->view->paymentdate = Application_Helper_General::convertDate($pslip['efdate'],$this->_dateViewFormat);
		// $this->_tableMst[9]["value"] = ($PSSTATUS==5) ? Application_Helper_General::convertDate($pslip['updated'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat) : "";
		// $this->_tableMst[10]["value"] = (isset($pslip['reference'])) ? $pslip['reference'] : '';
		//$this->_tableMst[11]["value"] = $pslip['bankresponse'];
		//$this->_tableMst[12]["value"] = $pslip['bankrespcode'];
		$this->_tableMst[11]["value"] = $pslip['TRACE_NO'];
		$this->_tableMst[12]["value"] = $pslip['releaser'];
		$this->_tableMst[13]["value"] = $pslip['challengecode'];

		$this->view->pslip = $pslip['psperiodic'];
		if ($pslip['psperiodic'] != NULL || !empty($pslip['psperiodic'])) {

			if ($pslip['psperiodic'] == 0) {
				$frequently = '1x';
				$this->view->frequently = $frequently;
			}else{

				if ($pslip['periodicuom'] == 1) {
				$frequently = 'Daily'.' (FreqRef# ';
				$this->view->frequentlylink = $pslip['periodicnumber'];
				$this->view->frequently = $frequently; 
				}
				else if($pslip['periodicuom'] == 2){
					$frequently = 'Weekly'.' (FreqRef# ';
					$this->view->frequentlylink = $pslip['periodicnumber'];
					$this->view->frequently = $frequently;
				}
				else if($pslip['periodicuom'] == 3){
					$frequently = 'Monthly'.' (FreqRef# ';
					$this->view->frequentlylink = $pslip['periodicnumber'];
					$this->view->frequently = $frequently;
				}

			}

		}else{
			$frequently = '1x';
			$this->view->frequently = $frequently;
		}

		//print_r($pslip);die;
		if(!empty($persenLabel)){
			if($pslip['PS_TYPE']=='14' || $pslip['PS_TYPE']=='15'){
			// $this->_tableMst[14]["value"] = $pslip['sumamount'].'% ('.$pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['PS_REMAIN']).')';
			$this->view->totalamount = $pslip['sumamount'].'% ('.$pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['PS_REMAIN']).')';
			}else{
			// $this->_tableMst[14]["value"] = $pslip['sumamount'].'% ('.$pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']).')';
			$this->view->totalamount = $pslip['sumamount'].'% ('.$pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']).')';
			}

		}else{
			if($pslip['PS_TYPE']=='14' || $pslip['PS_TYPE']=='15'){
				// $this->_tableMst[14]["value"] = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['PS_REMAIN']);
				$this->view->totalamount = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['PS_REMAIN']);
			}
			elseif($pslip['PS_TYPE'] != '3'){
				if($pslip['ccy']=='USD'){

					if($pslip['beneccy'] == 'USD' && $pslip['sourceccy']=='USD'){
						$tra_amout = $pslip['sumamount'];
						// $this->_tableMst[14]["value"] = 'USD '.Application_Helper_General::displayMoney($tra_amout);
						$this->view->totalamount = 'USD '.Application_Helper_General::displayMoney($tra_amout);
					}
					else if($pslip['EQUIVALENT_AMOUNT_IDR']!='0.00'){
						$tra_amout = $pslip['EQUIVALENT_AMOUNT_IDR'];
						// $this->_tableMst[14]["value"] = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']).' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
						$this->view->totalamount = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']).' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
					}else{
						$tra_amout = $pslip['sumamount'];
						// $this->_tableMst[14]["value"] = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']).' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
						$this->view->totalamount = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']).' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
					}

				}else{
					// $this->_tableMst[14]["value"] = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']);
					$this->view->totalamount = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']);
				}

			}else{

				if($pslip['beneccy'] == 'USD' && $pslip['sourceccy']=='USD'){

					// $this->_tableMst[14]["value"] = 'USD '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']);
					$this->view->totalamount = 'USD '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']);
				}else if($pslip['sourceccy'] == 'USD' && $pslip['ccy'] == 'USD'){
					$totalinvalas = $pslip['EQUIVALENT_AMOUNT_USD'];
					$totalinidr = $pslip['EQUIVALENT_AMOUNT_IDR'];
					//$totalinvalas = $pslip['amount'] + $pslip['transferfee'] + $pslip['provfee'] + $pslip['fafee'];
					//$totalinidr = $totalinvalas * $pslip['bookrate'];
						$this->_tableMst[14]["value"] = 'IDR '.Application_Helper_General::displayMoney($totalinidr);
						$this->view->totalamount = 'IDR '.Application_Helper_General::displayMoney($totalinidr);
				}
				else{
					//$totalinidr = (($pslip['amount'] + $pslip['fafee'])*$pslip['bookrate']) + $pslip['transferfee'];
					//$totalinvalas = $totalinidr / $pslip['bookbuy'];
					$totalinidr = $pslip['EQUIVALENT_AMOUNT_IDR'];
					$totalinvalas = $pslip['amount'];
				//$this->_tableMst[14]["value"] = 'IDR '.Application_Helper_General::displayMoney($totalinidr);
				$this->view->totalamount = 'IDR '.Application_Helper_General::displayMoney($totalinidr);
				}


			}
		}

		//die;
		//$this->_tableMst[15]["value"] = $pslip["payType"];
		$this->view->payType = $pslip["payType"];
		$this->view->pstype = $pslip["PS_TYPE"];
		// print_r($pslip);die;
		// if ($pslip["PS_CATEGORY"] == "BULK PAYMENT")
		// {
			// download trx bulk file
			$downloadURL = $this->view->url(array('module'=>'suspectreportnew','controller'=>'detail','action'=>'downloadtrx','csv'=>'1','payReff'=>$PS_NUMBER),null,true);
			//$this->_tableMst[18]["label"] = "Download File";
			$this->view->download = $this->view->formButton('download','download',array('class'=>'inputbtn', 'onclick'=>"window.location = ".$this->_db->quote($downloadURL).";"));

			$downloadURL2 = $this->view->url(array('module'=>'suspectreportnew','controller'=>'detail','action'=>'downloadtrx2','csv'=>'1','payReff'=>$PS_NUMBER),null,true);
			//$this->_tableMst[18]["label"] = "Download File";
			$this->view->download2 = $this->view->formButton('download','download',array('class'=>'inputbtn', 'onclick'=>"window.location = ".$this->_db->quote($downloadURL2).";"));

		// }

	//var_dump($pslip);die;
		// separate credit and debet view
		// if pstype = multidebet, bulkdebet
		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
			$this->debet($pslip);
		else if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["within"])
			$this->within($pslip);
		else if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["domestic"]){
			$this->domestic($pslip);
			// $this->LLDContent($pslip);
		}
		elseif($pslip["PS_TYPE"] == $this->_paymenttype["code"]["remittance"]){
			$this->credit($pslip);
			// $this->LLDContent($pslip);
		}
		else
			$this->credit($pslip);

		$this->approvalMatrix($pslip);
		$this->userInvolved($pslip);

		$this->view->PS_NUMBER 			= $PS_NUMBER;
		$this->view->tableMst 			= $this->_tableMst;
		$this->view->totalTrx 			= (isset($pslip["numtrx"])) ? $pslip["numtrx"] : '';
		$this->view->totalAmt 			= (isset($pslip["amount"])) ? $pslip["amount"] : '';
		$this->view->pdf 				= ($pdf)? true: false;

		if($this->view->tableDtl)
		{
			$tableDtl = $this->view->tableDtl;

//			echo($PSSTATUS);die;
			if($PSSTATUS == '1' || $PSSTATUS == '2' || $PSSTATUS == '3' || $PSSTATUS == '7')
			{

				$no=0;
				foreach($tableDtl as $row)
				{
					if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["domestic"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
					{
						$transferType = $row['TRANS_TYPE_FULL'];
					}
					else
					{
						$transferType = $row['TRANSFER_TYPE'];
					}

					//echo($row['SOURCE_ACCOUNT']." -- ".$pslip["compCode"]." == ".$pslip["PS_TYPE"]." - - ".$pslip["compCode"]." - - ".$transferType."<br>");

					if($pslip["PS_TYPE"] !== $this->_paymenttype["code"]["remittance"]){
						$chargesObj = Charges::factory($pslip["compCode"],$transferType);
						$paramCharges = array('accsrc'=>$row['SOURCE_ACCOUNT'],'transferType'=>$transferType);
						$chargesAmt = $chargesObj->getCharges($paramCharges);
					}

					//Zend_Debug::dump($chargesAmt);
					//$this->view->tableDtl[$no]['TRANSFER_FEE']=$chargesAmt;
					//$this->view->tableDtl[$no]['TRANSFER_FEE']= Application_Helper_General::displayMoney($chargesAmt);

					$no++;
				}
			}
			/*else if($PSSTATUS == '5' || $PSSTATUS == '6' || $PSSTATUS == '9')
			{

			}*/
			else if($PSSTATUS == '4' || $PSSTATUS == '14')
			{
				$no=0;
				foreach($tableDtl as $row)
				{
					// $this->view->tableDtl[$no]['TRANSFER_FEE']= ' - ';
					$no++;
				}
			}

			$no=0;
			foreach($tableDtl as $row)
			{
									//
				if($tableDt1[$no]['TRANSFER_TYPE']=='PB'){
					$tableDt1[$no]['TRANSFER_TYPE'] = 'Mayapada';
				}
				unset($this->view->tableDtl[$no]['SOURCE_ACCOUNT']);
					$no++;
			}
			//print_r($tableDtl);die;
			unset($this->view->fields['SOURCE_ACCOUNT']);
			//Zend_Debug::dump($this->view->tableDtl);
		}

		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render('/detail/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Payment Detail',$outputHTML);
			Application_Helper_General::writeLog('RPPY','Download PDF Payment Detail Report');
		}
		else
		{
			Application_Helper_General::writeLog('RPPY','View Payment Detail Report');
		}


	}

	public function downloadtrxAction()
	{
		$PS_NUMBER 			= trim(strip_tags($this->_getParam('payReff')));
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);

		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";


		$select	= $this->_db->select()
							->from(		array(	'P' 		=> 'T_PSLIP'),
										array(	'payStatus'	=>$casePayStatus,
												'PS_STATUS'	=>'P.PS_STATUS',
												'compCode'	=>'C.CUST_ID',
												'compName'	=>'C.CUST_NAME',
												'paySubject'=>'P.PS_SUBJECT',
												'created'	=>'P.PS_CREATED',
												'updated'	=>'P.PS_UPDATED',
												'efdate'	=>'P.PS_EFDATE',
												'sumamount'	=>'P.PS_TOTAL_AMOUNT',
												'PS_CATEGORY'	=>'P.PS_CATEGORY',
												'ccy'			=>'P.PS_CCY',
												'transmission'	=>'',
												'reference'		=>'',
												'bankresponse'	=>'',
												'bankrespcode'	=>'',
												'traceno'		=>'P.UUID',
												'releaser'		=>'P.PS_RELEASER_USER_LOGIN',
												'challengecode'	=>'P.PS_RELEASER_CHALLENGE',
												'PS_TYPE'		=>'P.PS_TYPE'

											))
							->joinLeft(	array(	'C' => 'M_CUSTOMER' ),'P.CUST_ID = C.CUST_ID',array())
							->where('P.PS_NUMBER =? ',$PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);

		if (!empty($pslip))
		{
			// separate credit and debet view
			if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
				$this->downloadTrxDebet($PS_NUMBER);
			else
				$this->downloadTrxCredit($PS_NUMBER);
		}
		else
		{
			$data = array();
			$data[0][0] = "Invalid Payment Number";
			$this->_helper->download->csv(array(),$data,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
		}

		// payment.type.code.multicredit  	= 6
		// payment.type.code.multidebet  	= 7
		// payment.type.code.bulkcredit  	= 4
		// payment.type.code.bulkdebet  	= 5
	}

	protected function downloadTrxCredit($PS_NUMBER)
	{
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$headerData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");
		
		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											'BANK_CODE'				=> 'D.BANK_NAME',
											'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
											'BENEF_BANK_CODE'		=> 'E.BANK_NAME',
											'BENEFICIARY_ACCOUNT'	=> 'TT.BENEFICIARY_ACCOUNT',
											'CCY'					=> 'C.PS_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'TRA_STATUS'			=> $caseTraStatus,
										  )
									)
							->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		$no = 1;
		
		foreach ($data as $p => $pTrx)
		{

			if ($pTrx['BANK_RESPONSE'] != NULL) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$status = $bankresponse[1];
				$RC 	= $bankresponse[0];
			}

			
			$paramTrx = array(	"NO"					=> $no++,
								"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
								"BANK_CODE"  			=> $pTrx['BANK_CODE'],
								"ACCTSRC"  				=> $pTrx['ACCTSRC'],
								"BENEF_BANK_CODE"  		=> $pTrx['BENEF_BANK_CODE'],
								"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
								"CCY"  					=> $pTrx['CCY'],
								"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
								"RC"					=> $RC,
								"STATUS"				=> $pTrx['TRA_STATUS'],
							);

			$newData[] = $paramTrx;
			
		}

		$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
		Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
		
	}

	protected function downloadTrxDebet($PS_NUMBER)
	{
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$headerData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");
		
		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											'BANK_CODE'				=> 'D.BANK_NAME',
											'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
											'BENEF_BANK_CODE'		=> 'E.BANK_NAME',
											'BENEFICIARY_ACCOUNT'	=> 'TT.BENEFICIARY_ACCOUNT',
											'CCY'					=> 'C.PS_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'TRA_STATUS'			=> $caseTraStatus,
										  )
									)
							->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		$no = 1;
		
		foreach ($data as $p => $pTrx)
		{

			if ($pTrx['BANK_RESPONSE'] != NULL) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$status = $bankresponse[1];
				$RC 	= $bankresponse[0];
			}

			
			$paramTrx = array(	"NO"					=> $no++,
								"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
								"BANK_CODE"  			=> $pTrx['BANK_CODE'],
								"ACCTSRC"  				=> $pTrx['ACCTSRC'],
								"BENEF_BANK_CODE"  		=> $pTrx['BENEF_BANK_CODE'],
								"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
								"CCY"  					=> $pTrx['CCY'],
								"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
								"RC"					=> $RC,
								"STATUS"				=> $pTrx['TRA_STATUS'],
							);

			$newData[] = $paramTrx;
			
		}
		
		$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
		Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
	}

	public function downloadtrx2Action()
	{
		$PS_NUMBER 			= trim(strip_tags($this->_getParam('payReff')));
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);

		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";


		$select	= $this->_db->select()
							->from(		array(	'P' 		=> 'T_PSLIP'),
										array(	'payStatus'	=>$casePayStatus,
												'PS_STATUS'	=>'P.PS_STATUS',
												'compCode'	=>'C.CUST_ID',
												'compName'	=>'C.CUST_NAME',
												'paySubject'=>'P.PS_SUBJECT',
												'created'	=>'P.PS_CREATED',
												'updated'	=>'P.PS_UPDATED',
												'efdate'	=>'P.PS_EFDATE',
												'sumamount'	=>'P.PS_TOTAL_AMOUNT',
												'PS_CATEGORY'	=>'P.PS_CATEGORY',
												'ccy'			=>'P.PS_CCY',
												'transmission'	=>'',
												'reference'		=>'',
												'bankresponse'	=>'',
												'bankrespcode'	=>'',
												'traceno'		=>'P.UUID',
												'releaser'		=>'P.PS_RELEASER_USER_LOGIN',
												'challengecode'	=>'P.PS_RELEASER_CHALLENGE',
												'PS_TYPE'		=>'P.PS_TYPE'

											))
							->joinLeft(	array(	'C' => 'M_CUSTOMER' ),'P.CUST_ID = C.CUST_ID',array())
							->where('P.PS_NUMBER =? ',$PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);

		if (!empty($pslip))
		{
			// separate credit and debet view
			if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
				$this->downloadTrxDebet2($PS_NUMBER);
			else
				$this->downloadTrxCredit2($PS_NUMBER);
		}
		else
		{
			$data = array();
			$data[0][0] = "Invalid Payment Number";
			$this->_helper->download->csv(array(),$data,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
		}

		// payment.type.code.multicredit  	= 6
		// payment.type.code.multidebet  	= 7
		// payment.type.code.bulkcredit  	= 4
		// payment.type.code.bulkdebet  	= 5
	}

	protected function downloadTrxCredit2($PS_NUMBER)
	{
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

  		$caseTraStatus1 = "(CASE G.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus1 .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus1 .= " END)";

		$headerData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");
		
		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											'BANK_CODE'				=> 'D.BANK_NAME',
											'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
											'BENEF_BANK_CODE'		=> 'E.BANK_NAME',
											'BENEFICIARY_ACCOUNT'	=> 'TT.BENEFICIARY_ACCOUNT',
											'CCY'					=> 'C.PS_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'TRA_STATUS'			=> $caseTraStatus,
											'TRASTATUS'				=> $caseTraStatus1,
										  )
									)
							->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->joinLeft ( array('G' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'), 'TT.TRANSACTION_ID = G.TRANSACTION_ID', array())
							->where('TT.TRA_STATUS = ?', '9')
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		if (empty($data)) {
			
			$paramTrx = array(	"NO"					=> ' ',
								"TRANSACTION_ID"  		=> ' ',
								"BANK_CODE"  			=> ' ',
								"ACCTSRC"  				=> ' ',
								"BENEF_BANK_CODE"  		=> ' ',
								"BENEFICIARY_ACCOUNT" 	=> ' ',
								"CCY"  					=> ' ',
								"TRA_AMOUNT"  			=> ' ',
								"RC"					=> ' ',
								"STATUS"				=> ' ',
								);

			$newData[] = $paramTrx;

			$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
	
		}else{
			
			$no = 1;

			foreach ($data as $p => $pTrx)
			{

				if ($pTrx['BANK_RESPONSE'] != NULL) {
					$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
					//$status = $bankresponse[1];
					$RC 	= $bankresponse[0];
				}

				if ($pTrx['BANK_CODE'] == NULL) {
					$setting = new Settings();
					$master_bank_name = $setting->getSetting('master_bank_name');
					$bank_name = $master_bank_name;
				}else{
					$bank_name = $pTrx['BANK_CODE'];
				}

				if ($pTrx['BENEF_BANK_CODE'] == NULL) {
					$setting = new Settings();
					$master_bank_name = $setting->getSetting('master_bank_name');
					$bank_name_benef = $master_bank_name;
				}else{
					$bank_name_benef = $pTrx['BENEF_BANK_CODE'];
				}

				if ($pTrx['TRASTATUS'] != NULL) {
					
					$paramTrx = array(	"NO"					=> $no++,
										"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
										"BANK_CODE"  			=> $bank_name,
										"ACCTSRC"  				=> $pTrx['ACCTSRC'],
										"BENEF_BANK_CODE"  		=> $bank_name_benef,
										"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
										"CCY"  					=> $pTrx['CCY'],
										"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
										"RC"					=> $RC,
										"STATUS"				=> $pTrx['TRA_STATUS'],
										"UPDATE_TO"				=> $pTrx['TRASTATUS'],
									);

				}else{
					
					$paramTrx = array(	"NO"					=> $no++,
										"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
										"BANK_CODE"  			=> $bank_name,
										"ACCTSRC"  				=> $pTrx['ACCTSRC'],
										"BENEF_BANK_CODE"  		=> $bank_name_benef,
										"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
										"CCY"  					=> $pTrx['CCY'],
										"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
										"RC"					=> $RC,
										"STATUS"				=> $pTrx['TRA_STATUS'],
									);
				}

				$newData[] = $paramTrx;
				
			}

			$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');	
		}
	}

	protected function downloadTrxDebet2($PS_NUMBER)
	{
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

  		$caseTraStatus1 = "(CASE G.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus1 .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus1 .= " END)";

		$headerData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");
		
		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											'BANK_CODE'				=> 'D.BANK_NAME',
											'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
											'BENEF_BANK_CODE'		=> 'E.BANK_NAME',
											'BENEFICIARY_ACCOUNT'	=> 'TT.BENEFICIARY_ACCOUNT',
											'CCY'					=> 'C.PS_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'TRA_STATUS'			=> $caseTraStatus,
											'TRASTATUS'				=> $caseTraStatus1,
										  )
									)
							->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->joinLeft ( array('G' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'), 'TT.TRANSACTION_ID = G.TRANSACTION_ID', array())
							->where('TT.TRA_STATUS = ?', '9')
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		if (empty($data)) {
			
			$paramTrx = array(	"NO"					=> ' ',
								"TRANSACTION_ID"  		=> ' ',
								"BANK_CODE"  			=> ' ',
								"ACCTSRC"  				=> ' ',
								"BENEF_BANK_CODE"  		=> ' ',
								"BENEFICIARY_ACCOUNT" 	=> ' ',
								"CCY"  					=> ' ',
								"TRA_AMOUNT"  			=> ' ',
								"RC"					=> ' ',
								"STATUS"				=> ' ',
								);

			$newData[] = $paramTrx;

			$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
	
		}else{

			$no = 1;
			
			foreach ($data as $p => $pTrx)
			{

				if ($pTrx['BANK_RESPONSE'] != NULL) {
					$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
					//$status = $bankresponse[1];
					$RC 	= $bankresponse[0];
				}

				if ($pTrx['BANK_CODE'] == NULL) {
					$setting = new Settings();
					$master_bank_name = $setting->getSetting('master_bank_name');
					$bank_name = $master_bank_name;
				}else{
					$bank_name = $pTrx['BANK_CODE'];
				}

				if ($pTrx['BENEF_BANK_CODE'] == NULL) {
					$setting = new Settings();
					$master_bank_name = $setting->getSetting('master_bank_name');
					$bank_name_benef = $master_bank_name;
				}else{
					$bank_name_benef = $pTrx['BENEF_BANK_CODE'];
				}

				if ($pTrx['TRASTATUS'] != NULL) {
					
					$paramTrx = array(	"NO"					=> $no++,
										"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
										"BANK_CODE"  			=> $bank_name,
										"ACCTSRC"  				=> $pTrx['ACCTSRC'],
										"BENEF_BANK_CODE"  		=> $bank_name_benef,
										"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
										"CCY"  					=> $pTrx['CCY'],
										"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
										"RC"					=> $RC,
										"STATUS"				=> $pTrx['TRA_STATUS'],
										"UPDATE_TO"				=> $pTrx['TRASTATUS'],
									);

				}else{
					
					$paramTrx = array(	"NO"					=> $no++,
										"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
										"BANK_CODE"  			=> $bank_name,
										"ACCTSRC"  				=> $pTrx['ACCTSRC'],
										"BENEF_BANK_CODE"  		=> $bank_name_benef,
										"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
										"CCY"  					=> $pTrx['CCY'],
										"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
										"RC"					=> $RC,
										"STATUS"				=> $pTrx['TRA_STATUS'],
									);
				}

				$newData[] = $paramTrx;
				
			}

			$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
		}

	}

	private function LLDContent($pslip)
	{
		$PS_NUMBER 		= $this->_paymentRef;
		$arrEFTStatus 	= array_combine($this->_eftstatus["code"],$this->_eftstatus["desc"]);

		$caseEFTStatus = "(CASE TT.EFT_STATUS ";
  		foreach($arrEFTStatus as $key => $val)	{ $caseEFTStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseEFTStatus .= " END)";

		// Table Detail Header
		$fields = array("TRANSANCTION_ID"	=> 'Transaction Ref#',
						// "EFT_STATUS"		=> 'EFT Status',
						"LLD_DESC" 			=> 'LLD Content',
            			);

		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSANCTION_ID'		=> 'TT.TRANSACTION_ID',
											// 'EFT_STATUS'			=> $caseEFTStatus,
											'LLD_DESC'				=> 'TT.LLD_DESC',
											'*'

										  )
									)
							->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);

		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value = $pTrx[$key];

				if($key == 'LLD_DESC' && $pslip['PS_TYPE'] == '3'){
					if($pslip['benefresident'] == 'R')
				   	{
				   		$resident = 'Resident';
				   	}
					elseif($pslip['benefresident'] == 'NR')
				   	{
				   		$resident = 'Non Resident';
				   	}
				   	else
				   	{
				   		$resident = '';
				   	}

				   	if($pslip['sourceresident'] == 'R')
				   	{
				   		$sourceresident = 'Resident';
				   	}
					elseif($pslip['sourceresident'] == 'NR')
				   	{
				   		$sourceresident = 'Non Resident';
				   	}
				   	else
				   	{
				   		$sourceresident = '';
				   	}

				   	if($pslip['benefcitizen'] == 'W')
				   	{
				   		$benenationality = 'WNI';
				   	}
					elseif($pslip['benefcitizen'] == 'N')
				   	{
				   		$benenationality = 'WNA';
				   	}
				   	else
				   	{
				   		$benenationality = '';
				   	}

				   	if($pslip['sourcecitizen'] == 'W')
				   	{
				   		$sourcenationality = 'WNI';
				   	}
					elseif($pslip['sourcecitizen'] == 'N')
				   	{
				   		$sourcenationality = 'WNA';
				   	}
				   	else
				   	{
				   		$sourcenationality = '';
				   	}

				   	// Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		 	  		$lldRelationshipArr = $settings->getLLDDOMRelationship();

		 	  		$model = new purchasing_Model_Purchasing();
					$purposeArr = $model->getTranspurpose();
					$purposeList = array();
					foreach ($purposeArr as $key => $value ){
						$purposeList[$value['CODE']] = $value['DESCRIPTION'];
					}

					if (!empty($pslip['benefcategory']))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_CATEGORY_POST = $lldCategoryArr[$pslip['benefcategory']];
						$LLD_CATEGORY_SOURCE = $lldCategoryArr[$pslip['sourcecategory']];
					}
					//print_r($pslipTrx);die;
					$SourceAcount = $this->_db->select()
			->from(
				array('M_CUSTOMER_ACCT'),
				array('*')
			)
			->where('ACCT_NO = ?',$pslipTrx['0']['SOURCE_ACCOUNT'])

		;

				$SourceAcountData = $this->_db->fetchRow($SourceAcount );
//				print_r($SourceAcountData);die;
// 			   	print_r(ucfirst($country));
// 			   	die;
				$citizenship = ($SourceAcountData['ACCT_RESIDENT'] == "R")? 'Resident' : 'Non Resident';
				$benecitizenship = ($pslipTrx['0']['BENEFICIARY_RESIDENT'] == "R")? 'Resident' : 'Non Resident';

				$country     = ($SourceAcountData['ACCT_CITIZENSHIP'] == "W")? 'WNI' : 'WNA';
				$benecountry = ($pslipTrx['0']['BENEFICIARY_CITIZENSHIP'] == "W")? 'WNI' : 'WNA';

				$settings 			= new Application_Settings();

				$lldCategoryArr  	= $settings->getLLDDOMCategory();
				$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
				$lldRelationshipArr = $settings->getLLDDOMRelationship();
				$purpose = $this->_db->select()
			->from(
				array('M_TRANSACTION_PURPOSE'),
				array('*')
			)
			->where('CODE = ?',$pslipTrx['0']['LLD_TRANSACTION_PURPOSE'])

		;

				$purposeData = $this->_db->fetchRow($purpose);
				$category = $lldCategoryArr[$SourceAcountData['ACCT_CATEGORY']];

					$value = "Sender Citizenship : ".$citizenship."; Sender Nationality : ".$country."; Sender Category : ".$category."; Destination Citizenship : ".$benecitizenship."; Destination Nationality : ".$benecountry."; Destination Category : ".$lldCategoryArr[$pslipTrx['0']['BENEFICIARY_CATEGORY']]."; Identity : ".$lldIdenticalArr[$pslipTrx['0']['LLD_IDENTITY']]."; Transactor Relationship : ".$lldRelationshipArr[$pslipTrx['0']['LLD_TRANSACTOR_RELATIONSHIP']]."; Transaction Purpose : ".$purposeData['DESCRIPTION'];
				}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}
		}

		$this->view->fieldsLLDContent 	= $fields;
		$this->view->tableDtlLLDContent = $tableDtl;

	}

	private function within($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		$arrCitizenship 	= array_combine($this->_citizenship["code"],$this->_citizenship["desc"]);

		$caseCityzenship = "(CASE TT.BENEFICIARY_CITIZENSHIP";
  		foreach($arrCitizenship as $key => $val)	{ $caseCityzenship .= " WHEN '".$key."' THEN '".$val."'"; }
  		$caseCityzenship .= " END)";


		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		//Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_name"], $pslip["accsrc_alias"])

		$select = $this->_db->select()
								->from(	array(	'T'				=>'T_TRANSACTION'),
										array(	'accsrc'		=>'T.SOURCE_ACCOUNT',
												'accsrc_ccy'	=>'T.SOURCE_ACCOUNT_CCY',
												'accsrc_name'	=>'T.SOURCE_ACCOUNT_NAME',
												'accsrc_alias'	=>'T.SOURCE_ACCOUNT_ALIAS_NAME',
											))
								->where('T.PS_NUMBER =? ',$PS_NUMBER);
		$pslipaccount = $this->_db->fetchRow($select);
		$account = $pslipaccount['accsrc'].' ['.$pslipaccount['accsrc_ccy'].'] - '.$pslipaccount['accsrc_name'];

		$this->_tableMst[5]["label"] = "Source Account";
		$this->_tableMst[5]["value"] = $account;

		//$bankresponse = explode(':', "BANK_RESPONSE");

		/*

		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"ACCTSRC"			=> 'Source Account',
						"ACCTSRC_NAME"		=> 'Source Account Name',
						"BANK_NAME"			=> 'Bank',
						"CITY"				=> 'City',
						"BENE_CITYZENSHIP"	=> 'Citizenship',
						"ACCTSRC_CCY"		=> 'Ccy',
						"TRA_AMOUNT"		=> 'Amount',
						"TRANSFER_FEE"		=> 'Charge',
						"TRA_MESSAGE" 		=> 'Message',
						"TRA_ADDMESSAGE"  	   	=> 'Additional Message',
						"TRANS_TYPE_FULL"	=> 'Transfer Type',
						"TRA_STATUS" 		=> 'Status',
            			);

		*/
		// Table Detail Header
		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"SOURCE_NAME" 		=> 'Source Account',
						"BENEF_NAME"		=> 'Beneficiary Account',
						// "ACBENEF_NAME_ALIAS"=> 'Beneficiary Name ',
						// "BANK_NAME"			=> 'Bank',
						// "CITY"				=> 'City',
						// "BENE_CITYZENSHIP"	=> 'Citizenship',
						"PS_CCY"		=> 'CCY',
						"TRA_AMOUNT"  	   	=> 'Amount',
						// "TRANSFER_FEE"		=> 'Charge',
						// "TRA_MESSAGE"  	   	=> 'Message',
						// "TRA_ADDMESSAGE"			=> 'Additional Message',
						// "TRANSFER_TYPE"		=> 'Transfer Type',
						"RC"					=> 'RC',
						"TRA_STATUS" 		=> 'Status',
// 						"BANK_RESPONSE" 		=> 'Response',
						//"SOURCE_ACCOUNT" 	=> 'SOURCE_ACCOUNT',
            			);

		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											//'numbertrx'				=> 'COUNT(TT.TRANSACTION_ID)',
											'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
											'ACBENEF_NAME_ALIAS'	=> 'TT.BENEFICIARY_ACCOUNT_NAME',
											'ACBENEF_NAME'			=> new Zend_Db_Expr("
																							CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') / ' , TT.BENEFICIARY_ACCOUNT_NAME ) "),
											'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
											'SOURCE_ACCOUNT_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.SOURCE_ACCOUNT , ' (' , TT.SOURCE_ACCOUNT_CCY , ') - ' , D.BANK_NAME ) "),
											'BENEFICIARY_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') - ' , E.BANK_NAME ) "),
											'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
											'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
											'TRA_REFNO'				=> 'TT.TRA_REFNO',

											'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'Mayapada (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'Mayapada (Buy)'
																				 ELSE 'N/A'
																			END"),
											'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
											'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
											'TRA_STATUS' 			=> $caseTraStatus,
											'CITY'					=> 'TT.BENEFICIARY_BANK_CITY',
											'BENE_CITYZENSHIP'		=> 'TT.BENEFICIARY_CITIZENSHIP',
											'C.PS_CCY',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'RC' 					=> 'TT.BANK_RESPONSE',
											/* 'BANK_RESPONSE'		=> new Zend_Db_Expr("
																							CASE WHEN TT.BANK_RESPONSE = '' THEN 'N/A'
																								 WHEN TT.BANK_RESPONSE = null THEN 'N/A'
																								 ELSE TT.BANK_RESPONSE
																							END"), */
											'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
											'SOURCE_ACCOUNT_CCY'	=> 'TT.SOURCE_ACCOUNT_CCY',
											'SOURCE_ACCOUNT_BANK'	=> 'D.BANK_NAME',
											'BENEFICIARY_ACCOUNT'		=> 'TT.BENEFICIARY_ACCOUNT',
											'BENEFICIARY_ACCOUNT_CCY'	=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'BENEFICIARY_ACCOUNT_BANK'	=> 'E.BANK_NAME',
											'PS_CCY'			=> 'C.PS_CCY',

										  )
									)
							->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
							->joinLeft	(   array('C' => 'T_PSLIP'), 'C.PS_NUMBER = TT.PS_NUMBER', array() )
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->where('TT.TRA_STATUS = ?', '9')
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo "<pre>";
		// echo $select->__toString();
		// echo "<br />";
		//die;
		$pslipTrx = $this->_db->fetchAll($select);

		$numbertrx 				= count($pslipTrx);
		$this->view->numbertrx 	= $numbertrx;

		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value = $pTrx[$key];
				$ccy = $pTrx["ACBENEF_CCY"];

				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}
				elseif($key == "TRANSFER_TYPE")
				{
					if($value=='PB'){
						$value = 'Mayapada';
					}
				}

				if($key == 'BENE_CITYZENSHIP'){
					if($value=='W'){
						$value='Resident';
					}else{
						$value='Non Resident';

					}
				}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if ($pTrx['BANK_RESPONSE'] != NULL || !empty($pTrx['BANK_RESPONSE'])) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$tableDtl[$p]['BANK_RESPONSE'] = $bankresponse[1];

				$tableDtl[$p]['RC'] = $bankresponse[0];
			}

			if ($pTrx['SOURCE_ACCOUNT_BANK'] != NULL || !empty($pTrx['SOURCE_ACCOUNT_BANK'])) {
				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$pTrx['SOURCE_ACCOUNT_BANK'];
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}

			if ($pTrx['BENEFICIARY_ACCOUNT_BANK'] != NULL || !empty($pTrx['BENEFICIARY_ACCOUNT_BANK'])) {
				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$pTrx['BENEFICIARY_ACCOUNT_BANK'];
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}
			//$this->view->numbertrx 			= $pTrx['numbertrx'];
			
		}

		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
	}

	private function domestic($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		$arrCitizenship 	= array_combine($this->_citizenship["code"],$this->_citizenship["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$caseCityzenship = "(CASE TT.BENEFICIARY_CITIZENSHIP";
  		foreach($arrCitizenship as $key => $val)	{ $caseCityzenship .= " WHEN '".$key."' THEN '".$val."'"; }
  		$caseCityzenship .= " END)";

		//Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_name"], $pslip["accsrc_alias"])

		$select = $this->_db->select()
								->from(	array(	'T'				=>'T_TRANSACTION'),
										array(	'accsrc'		=>'T.SOURCE_ACCOUNT',
												'accsrc_ccy'	=>'T.SOURCE_ACCOUNT_CCY',
												'accsrc_name'	=>'T.SOURCE_ACCOUNT_NAME',
												'accsrc_alias'	=>'T.SOURCE_ACCOUNT_ALIAS_NAME',
											))
								->where('T.PS_NUMBER =? ',$PS_NUMBER);
		$pslipaccount = $this->_db->fetchRow($select);
		$account = $pslipaccount['accsrc'].' ['.$pslipaccount['accsrc_ccy'].'] - '.$pslipaccount['accsrc_name'].' / '.$pslipaccount['accsrc_alias'];

		$this->_tableMst[5]["label"] = "Source Account";
		$this->_tableMst[5]["value"] = $account;

		/*

		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"ACCTSRC"			=> 'Source Account',
						"ACCTSRC_NAME"		=> 'Source Account Name',
						"BANK_NAME"			=> 'Bank',
						"CITY"				=> 'City',
						"BENE_CITYZENSHIP"	=> 'Citizenship',
						"ACCTSRC_CCY"		=> 'Ccy',
						"TRA_AMOUNT"		=> 'Amount',
						"TRANSFER_FEE"		=> 'Charge',
						"TRA_MESSAGE" 		=> 'Message',
						"TRA_REFNO"  	   	=> 'Additional Message',
						"TRANS_TYPE_FULL"	=> 'Transfer Type',
						"TRA_STATUS" 		=> 'Status',
            			);

		*/

		// Table Detail Header
		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"SOURCE_NAME" => 'Source Account',
						"BENEF_NAME"	=> 'Beneficiary Account',
// 						"ACBENEF_NAME_ALIAS"=> 'Beneficiary Name',

						// "BENEFICIARY_CATEGORY"			=> 'Beneficiary Category',
						// "BENEFICIARY_ID_TYPE"			=> 'Beneficiary Identification Type',
						// "BENEFICIARY_CITY_CODE"			=> 'Beneficiary Identification Number',
						// "BANK_NAME"			=> 'Beneficiary City',

						// "BANK_NAME"			=> 'Bank',
						// "CITY"				=> 'City',
						// "BENE_CITYZENSHIP"	=> 'Citizenship',
						"PS_CCY"		=> 'CCY',
						"TRA_AMOUNT"  	   	=> 'Amount',
						// "TRANSFER_FEE"		=> 'Charge',
						// "TRA_MESSAGE"		=> 'Message',
						// "TRA_REFNO"			=> 'Additional Message',
						// "TRANS_TYPE_FULL"	=> 'Transfer Type',
						"RC"					=> 'RC',
						"TRA_STATUS" 		=> 'Status',
// 						"BANK_RESPONSE" 		=> 'Response',
						// "SOURCE_ACCOUNT" 	=> 'SOURCE_ACCOUNT',
            			);

		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											//'numbertrx'				=> 'COUNT(TT.TRANSACTION_ID)',
											'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
											'ACBENEF_NAME_ALIAS'	=> new Zend_Db_Expr("
																			CONCAT ( TT.BENEFICIARY_ACCOUNT_NAME , ' ' ) "),
											'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT ( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ')  ' , TT.BENEFICIARY_ACCOUNT_NAME , ' / ' , TT.BENEFICIARY_BANK_NAME ) "),
											'SOURCE_ACCOUNT_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.SOURCE_ACCOUNT , ' (' , TT.SOURCE_ACCOUNT_CCY , ') - ' , D.BANK_NAME ) "),
											'BENEFICIARY_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') - ' , E.BANK_NAME ) "),
											'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
											'CITY'					=> 'TT.BENEFICIARY_BANK_CITY',
											'BENEFICIARY_CATEGORY'				=> 'TT.BENEFICIARY_CATEGORY',
											'BENEFICIARY_ID_TYPE'				=> 'TT.BENEFICIARY_ID_TYPE',
											'BENEFICIARY_ID_NUMBER'				=> 'TT.BENEFICIARY_ID_NUMBER',
											'BENEFICIARY_CITY_CODE'				=> 'TT.BENEFICIARY_CITY_CODE',
											'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
											'TRA_REFNO'				=> 'TT.TRA_REFNO',
											'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
											'TRANS_TYPE_FULL'		=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 ELSE 'N/A'
																			END"),
											'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
											'BENE_CITYZENSHIP'		=> $caseCityzenship,
											'TRA_STATUS' 			=> $caseTraStatus,
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'RC'					=> 'TT.BANK_RESPONSE',
											/* 'BANK_RESPONSE'		=> new Zend_Db_Expr("
																							CASE WHEN TT.BANK_RESPONSE = '' THEN 'N/A'
																								 WHEN TT.BANK_RESPONSE = null THEN 'N/A'
																								 ELSE TT.BANK_RESPONSE
																							END"), */
											'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
											'SOURCE_ACCOUNT_CCY'	=> 'TT.SOURCE_ACCOUNT_CCY',
											'SOURCE_ACCOUNT_BANK'	=> 'D.BANK_NAME',
											'BENEFICIARY_ACCOUNT'		=> 'TT.BENEFICIARY_ACCOUNT',
											'BENEFICIARY_ACCOUNT_CCY'	=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'BENEFICIARY_ACCOUNT_BANK'	=> 'E.BANK_NAME',
											'PS_CCY'				=> 'F.PS_CCY,'
										  )
									)
							->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->joinLeft ( array('F' => 'T_PSLIP'), 'TT.PS_NUMBER = F.PS_NUMBER', array() )
							->where('TT.TRA_STATUS = ?', '9')
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		 //echo "<pre>";
		 //echo $select;die;
		// echo $select->__toString();
		// die;

		$pslipTrx = $this->_db->fetchAll($select);

		$numbertrx 				= count($pslipTrx);
		$this->view->numbertrx 	= $numbertrx;

		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value 		= $pTrx[$key];
				$CITY 		= $pTrx["CITY"];
				$BANKNAME 	= $pTrx["BANK_NAME"];
				$ccy 		= $pTrx["ACBENEF_CCY"];

				if($key == "BENEFICIARY_CATEGORY"){
					$LLD_CATEGORY = $value;
					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}
					$value = $LLD_CATEGORY_POST;
				}
				if($key == "BENEFICIARY_CITY_CODE"){
					$CITY_CODE = $value;
					$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
					$select = $this->_db->select()
					->from(array('A' => 'M_CITY'),array('*'));
					$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
					$arr = $this->_db->fetchall($select);
					$value = $arr[0]['CITY_NAME'];
				}

				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if ($pTrx['BANK_RESPONSE'] != NULL || !empty($pTrx['BANK_RESPONSE'])) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$tableDtl[$p]['BANK_RESPONSE'] = $bankresponse[1];

				$tableDtl[$p]['RC'] = $bankresponse[0];
			}

			if ($pTrx['SOURCE_ACCOUNT_BANK'] != NULL || !empty($pTrx['SOURCE_ACCOUNT_BANK'])) {
				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$pTrx['SOURCE_ACCOUNT_BANK'];
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}

			if ($pTrx['BENEFICIARY_ACCOUNT_BANK'] != NULL || !empty($pTrx['BENEFICIARY_ACCOUNT_BANK'])) {
				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$pTrx['BENEFICIARY_ACCOUNT_BANK'];
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}

			//$this->view->numbertrx 			= $pTrx['numbertrx'];
		}

		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
		$this->view->TITLE_MST		 	= "Transfer From";
		$this->view->TITLE_DTL		 	= "Transfer To";
	}

	private function userInvolved($pslip)
	{
		$PS_NUMBER 				= $this->_paymentRef;
		$tableDtlUserInvolved 	= array();

		$arrHistoryStatus 		= array_combine($this->_historystatus["code"],$this->_historystatus["desc"]);

		$caseHistoryStatus = "(CASE TP.HISTORY_STATUS ";
  		foreach($arrHistoryStatus as $key => $val)	{ 
		if($key == 9){
			$caseHistoryStatus .= " WHEN ".$key." THEN 'Execute'"; 
		}else{
			$caseHistoryStatus .= " WHEN ".$key." THEN '".$val."'"; 
		}
		}
  		$caseHistoryStatus .= " END)";

		// Table Detail Header User Involved
		$fieldsinvolved = array("USER"		=> 'User',
								"ACTION"	=> 'Action',
								"DATETIME"	=> 'Date Time',
								"REASON"	=> 'Notes',
								);

		$select = $this->_db->select()
								->from(	array(	'TP'		=>'T_PSLIP_HISTORY'),
										array(	'DATETIME'	=>'DATE_TIME',
												'USER'		=>'USER_LOGIN',
												'ACTION'	=>$caseHistoryStatus,
												'REASON'	=>'PS_REASON',
											))
								->where('TP.PS_NUMBER =? ',$PS_NUMBER)
								->order('DATE_TIME ASC');

		$pslipinvolved = $this->_db->fetchAll($select);

		foreach ($pslipinvolved as $p => $pInv)
		{
			// create table detail data
			foreach ($fieldsinvolved as $key => $field)
			{
				$value = $pInv[$key];

				if ($key == "DATETIME")
				{	$value = Application_Helper_General::convertDate($value, $this->view->displayDateTimeFormat,$this->view->defaultDateFormat);	}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtlUserInvolved[$p][$key] = $value;
			}
		}

		$this->view->fieldsinvolved	 		= $fieldsinvolved;
		$this->view->tableDtlUserInvolved 	= $tableDtlUserInvolved;
	}


	private function approvalMatrix($pslip)
	{
		$PS_NUMBER 		= $this->_paymentRef;
		$tableDtlApp 	= array();
		$payAmount 		= $pslip["sumamount"];
		$payCcy 		= $pslip["ccy"];
		$payCustId 		= $pslip["compCode"];
		$psremain 		= $pslip['PS_REMAIN'];
//		print_r($pslip['PS_REMAIN']);die;
		if($payAmount != ""){
			if($pslip['PS_TYPE'] != 14){
			$select = $this->_db->select()
								->from(		array(	'B'				=>'M_APP_BOUNDARY'),
											array(	'BOUNDARY_ID'	=>'BOUNDARY_ID',
													'BOUNDARY_MAX'	=>'BOUNDARY_MAX',
													'BOUNDARY_MIN'	=>'BOUNDARY_MIN'
										))
								->joinLeft(	array('C'		=>'M_CUSTOMER'),'B.CUST_ID = C.CUST_ID',array())
								->where("	BOUNDARY_MIN <= '$payAmount'
											AND BOUNDARY_MAX >= '$payAmount'
											AND C.CUST_ID = '$payCustId'
											AND CCY_BOUNDARY = '$payCcy'");
			}else{
						$select = $this->_db->select()
								->from(		array(	'B'				=>'M_APP_BOUNDARY'),
											array(	'BOUNDARY_ID'	=>'BOUNDARY_ID',
													'BOUNDARY_MAX'	=>'BOUNDARY_MAX',
													'BOUNDARY_MIN'	=>'BOUNDARY_MIN'
										))
								->joinLeft(	array('C'		=>'M_CUSTOMER'),'B.CUST_ID = C.CUST_ID',array())
								->where("	BOUNDARY_MIN <= '$psremain'
											AND BOUNDARY_MAX >= '$psremain'
											AND C.CUST_ID = '$payCustId'
											AND CCY_BOUNDARY = '$payCcy'");
			}
			// echo "<pre>";
			// echo $select->__toString();
			// die;

			$sel = $this->_db->fetchRow($select);
			$BOUNDARYID = $sel["BOUNDARY_ID"];

			if($BOUNDARYID){

				$BOUNDARY_MIN = Application_Helper_General::displayMoney($sel["BOUNDARY_MIN"]);
				$BOUNDARY_MAX = Application_Helper_General::displayMoney($sel["BOUNDARY_MAX"]);

				$BOUNDARYRANGE = $BOUNDARY_MIN.' - '.$BOUNDARY_MAX;

				$selectBoundGroup = $this->_db->select()
												->from(	array(	'BG'			=>'M_APP_BOUNDARY_GROUP'),
														array(	'BGID'			=>'BOUNDARY_GROUP_ID',
																'GROUPUSERID'	=>'GROUP_USER_ID',
																	)
														)
												->where("BG.BOUNDARY_ID = '$BOUNDARYID'");
				// echo "<pre>";
				// echo $selectBoundGroup->__toString();
				// die;

				$BoundGroup = $this->_db->fetchAll($selectBoundGroup);
				// echo "boundaryid: $BOUNDARYID<br />";
				// echo "<pre>";
				// print_r($BoundGroup);
				//die;

				if(!empty($BoundGroup)){
					foreach($BoundGroup as $key => $value){
						$originalGROUPUSERID = $value["GROUPUSERID"];
						$GROUPUSERID = $value["GROUPUSERID"];
						//$arrBoundGroup[$key]['GROUPUSERID'] = $GROUPUSERID;
						//echo "groupuserid: $GROUPUSERID<BR />";

						if(substr($GROUPUSERID,0,1) == 'N')
						{
							$GROUPUSERID = 'Group '.intval(substr($GROUPUSERID,-2));
						}
						if(substr($GROUPUSERID,0,1) == 'S')
						{
							$GROUPUSERID = 'Special Group';
						}

						$selGroupUser = $this->_db->select()
												->from('M_APP_GROUP_USER', array('USER_ID'=>'USER_ID'))
												->where("GROUP_USER_ID = '$originalGROUPUSERID'");
						// echo "<pre>";
						// echo $selGroupUser->__toString();
						// echo "<br />";
						// die;

						$GroupUser = $this->_db->fetchAll($selGroupUser);

						//echo "<pre>";
						//print_r($GroupUser);
						if(!empty($GroupUser)){
							foreach($GroupUser as $d => $dt){
								foreach($dt as $dd => $data){

									$value = $data;
									$arrBoundGroup[$GROUPUSERID][$d] = $value;
									//echo "d, dt,dd,data: $d, $dt, $dd, $data<br />";
								}

							}
						}
						else{ $arrBoundGroup[$GROUPUSERID] = ""; }

					}
					//echo "arrboundgroup: <br />";
					//print_r($arrBoundGroup);

				}
				else { $BOUNDARYRANGE = "No Boundary Group"; }

			}
			else $BOUNDARYRANGE = "No Record Found";
		}
		else $BOUNDARYRANGE = "No Record Found";


		// Select Detail Header Approval Matrix


		$this->view->fieldsapproval	 = (isset($fieldsapproval)) ? $fieldsapproval : '';
		$this->view->tableDtlApp 	 = $tableDtlApp;
		$this->view->BOUNDARYRANGE 	 = $BOUNDARYRANGE;
		$this->view->arrBoundGroup 	 = (isset($arrBoundGroup)) ? $arrBoundGroup : '';
	}

	private function debet($pslip)
	{

		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		$arrCitizenship 	= array_combine($this->_citizenship["code"],$this->_citizenship["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$caseCityzenship = "(CASE TT.BENEFICIARY_CITIZENSHIP";
  		foreach($arrCitizenship as $key => $val)	{ $caseCityzenship .= " WHEN '".$key."' THEN '".$val."'"; }
  		$caseCityzenship .= " END)";

		// Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_name"], $pslip["acbenef_alias"])

		$select = $this->_db->select()
								->from(	array(	'T'				=>'T_TRANSACTION'),
										array(	'acbenef'		=>'T.BENEFICIARY_ACCOUNT',
												'acbenef_ccy'	=>'T.BENEFICIARY_ACCOUNT_CCY',
												'acbenef_name'	=>'T.BENEFICIARY_ACCOUNT_NAME',
												'acbenef_alias'	=>'T.BENEFICIARY_ALIAS_NAME',
											))
								->where('T.PS_NUMBER =? ',$PS_NUMBER);
		$pslipaccount = $this->_db->fetchRow($select);
		$account = $pslipaccount['acbenef'].' ['.$pslipaccount['acbenef_ccy'].'] - '.$pslipaccount['acbenef_name'].' / '.$pslipaccount['acbenef_alias'];

		$this->_tableMst[5]["label"] = "Beneficiary Account";
		$this->_tableMst[5]["value"] = $account;



		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"SOURCE_NAME" => 'Source Account',
						"BENEF_NAME"	=> 'Beneficiary Account',
						// "ACCTSRC_NAME"		=> 'Source Name',
						// "BANK_NAME"			=> 'Bank',
						// "CITY"				=> 'City',
						// "BENE_CITYZENSHIP"	=> 'Citizenship',
						"PS_CCY"		=> 'CCY',
						"TRA_AMOUNT"		=> 'Amount',
						// "TRANSFER_FEE"		=> 'Charge',
						// "TRA_MESSAGE" 		=> 'Message',
						// "TRA_REFNO"  	   	=> 'Additional Message',
						// "TRANS_TYPE_FULL"	=> 'Transfer Type',
						"RC"					=> 'RC',
						"TRA_STATUS" 		=> 'Status',
// 						"BANK_RESPONSE" 		=> 'Response',
						// "SOURCE_ACCOUNT" 	=> 'SOURCE_ACCOUNT',
            			);

        $select	= $this->_db->select()
							->from	(
										array(	'TT' => 'T_TRANSACTION'),
										array(
												'TRANSACTION_ID'			=> 'TT.TRANSACTION_ID',
												//'numbertrx'					=> 'COUNT(TT.TRANSACTION_ID)',
												'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
												'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
												'ACCTSRC_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																				 ELSE CONCAT ( TT.SOURCE_ACCOUNT_NAME , ' (' , TT.SOURCE_ACCOUNT_ALIAS_NAME , ') ')
																			END"),
												'SOURCE_ACCOUNT_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.SOURCE_ACCOUNT , ' (' , TT.SOURCE_ACCOUNT_CCY , ') - ' , D.BANK_NAME ) "),
												'BENEFICIARY_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') - ' , E.BANK_NAME ) "),
												'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
												'TRA_AMOUNT'				=> 'TT.TRA_AMOUNT',
												'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
												'TRA_REFNO'					=> 'TT.TRA_REFNO',
												'TRA_STATUS'				=> $caseTraStatus,
												'CITY'					=> 'TT.BENEFICIARY_BANK_CITY',
												'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
												'TRANS_TYPE_FULL'		=> new Zend_Db_Expr("
																				CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																					 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																					 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																					 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																					 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																					 ELSE 'N/A'
																				END"),

												'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
												'BANK_NAME'				=> new Zend_Db_Expr("
																				CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																					 ELSE TT.BENEFICIARY_BANK_NAME
																				END"),
												'BENE_CITYZENSHIP'		=> $caseCityzenship,
												'RC'					=> 'TT.BANK_RESPONSE',
												'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',	
												/* 'BANK_RESPONSE'		=> new Zend_Db_Expr("
																							CASE WHEN TT.BANK_RESPONSE = '' THEN 'N/A'
																								 WHEN TT.BANK_RESPONSE = null THEN 'N/A'
																								 ELSE TT.BANK_RESPONSE
																							END"), */
												'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
												'SOURCE_ACCOUNT_CCY'	=> 'TT.SOURCE_ACCOUNT_CCY',
												'SOURCE_ACCOUNT_BANK'	=> 'D.BANK_NAME',
												'BENEFICIARY_ACCOUNT'		=> 'TT.BENEFICIARY_ACCOUNT',
												'BENEFICIARY_ACCOUNT_CCY'	=> 'TT.BENEFICIARY_ACCOUNT_CCY',
												'BENEFICIARY_ACCOUNT_BANK'	=> 'E.BANK_NAME',
												'PS_CCY'				=> 'F.PS_CCY',
											  )
									)
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->joinLeft ( array('F' => 'T_PSLIP'), 'TT.PS_NUMBER = F.PS_NUMBER', array() )
							->where('TT.TRA_STATUS = ?', '9')
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);

		$numbertrx 				= count($pslipTrx);
		$this->view->numbertrx 	= $numbertrx;

		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value 	= $pTrx[$key];
				$ccy 	= $pTrx["ACCTSRC_CCY"];

				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}


				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;
			}

			if ($pTrx['BANK_RESPONSE'] != NULL || !empty($pTrx['BANK_RESPONSE'])) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$tableDtl[$p]['BANK_RESPONSE'] = $bankresponse[1];

				$tableDtl[$p]['RC'] = $bankresponse[0];
			}
			if ($pTrx['SOURCE_ACCOUNT_BANK'] != NULL || !empty($pTrx['SOURCE_ACCOUNT_BANK'])) {
				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$pTrx['SOURCE_ACCOUNT_BANK'];
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}

			if ($pTrx['BENEFICIARY_ACCOUNT_BANK'] != NULL || !empty($pTrx['BENEFICIARY_ACCOUNT_BANK'])) {
				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$pTrx['BENEFICIARY_ACCOUNT_BANK'];
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}
			//$this->view->numbertrx 			= $pTrx['numbertrx'];
		}

		$this->view->fields			 = $fields;
		$this->view->tableDtl 		 = $tableDtl;
		$this->view->TITLE_MST		 = "Transfer To";
		$this->view->TITLE_DTL		 = "Transfer From";
	}

	private function credit($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		$arrCitizenship 	= array_combine($this->_citizenship["code"],$this->_citizenship["desc"]);
		$arrEFTStatus 		= array_combine($this->_eftstatus["code"],$this->_eftstatus["desc"]);

		$caseEFTStatus = "(CASE TT.EFT_STATUS ";
  		foreach($arrEFTStatus as $key => $val)	{ $caseEFTStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseEFTStatus .= " END)";

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

  		$caseTraStatus1 = "(CASE G.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus1 .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus1 .= " END)";

		$caseCityzenship = "(CASE TT.BENEFICIARY_CITIZENSHIP";
  		foreach($arrCitizenship as $key => $val)	{ $caseCityzenship .= " WHEN '".$key."' THEN '".$val."'"; }
  		$caseCityzenship .= " END)";


		//Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_name"], $pslip["accsrc_alias"])

		$select = $this->_db->select()
								->from(	array(	'T'				=>'T_TRANSACTION'),
										array(	'accsrc'		=>'T.SOURCE_ACCOUNT',
												'accsrc_ccy'	=>'T.SOURCE_ACCOUNT_CCY',
												'accsrc_name'	=>'T.SOURCE_ACCOUNT_NAME',
												'accsrc_alias'	=>'T.SOURCE_ACCOUNT_ALIAS_NAME',
											))
								->where('T.PS_NUMBER =? ',$PS_NUMBER);
		$pslipaccount = $this->_db->fetchRow($select);
		$account = $pslipaccount['accsrc'].' ['.$pslipaccount['accsrc_ccy'].'] - '.$pslipaccount['accsrc_name'].' / '.$pslipaccount['accsrc_alias'];

		$this->_tableMst[5]["label"] = "Source Account";
		$this->_tableMst[5]["value"] = $account;

		/*

		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"ACCTSRC"			=> 'Source Account',
						"ACCTSRC_NAME"		=> 'Source Account Name',
						"BANK_NAME"			=> 'Bank',
						"CITY"				=> 'City',
						"BENE_CITYZENSHIP"	=> 'Citizenship',
						"ACCTSRC_CCY"		=> 'Ccy',
						"TRA_AMOUNT"		=> 'Amount',
						"TRANSFER_FEE"		=> 'Charge',
						"TRA_MESSAGE" 		=> 'Message',
						"TRA_ADDMESSAGE"  	   	=> 'Additional Message',
						"TRANS_TYPE_FULL"	=> 'Transfer Type',
						"TRA_STATUS" 		=> 'Status',
            			);

		*/

		if($pslip['PS_TYPE'] == '3'){
			$fields = array("TRANSACTION_ID"	  =>'Transaction ID',
							"SOURCE_NAME" => 'Source Account',
							"BENEF_NAME"	=> 'Beneficiary Account',
							// "ACBENEF_NAME"		=> 'Beneficiary Name',
							// "BANK_NAME"			=> 'Beneficiary Bank',
							// "NOSTRO_NAME"		=> 'Nostro Bank',
							// "COUNTRY_NAME"		=> 'Country',
							"PS_CCY"  	   			=> 'CCY',
							// "RATE"			=> 'Rate',
							"TRA_AMOUNT"  	   	=> 'Amount',
							// "TRANSFER_FEE"		=> 'Transfer Fee',
							// "FULL_AMOUNT_FEE"	=> 'Full Amount Fee',
							// "PROVISION_FEE"		=> 'Provision Fee',
							// "TOTAL_AMOUNT"		=> 'Total Amount',
							// "BOOK_RATE"		=> 'Book Rate',
							// "TRA_MESSAGE" 		=> 'Message',
							// "TRA_ADDMESSAGE"  	   	=> 'Additional Message',
							// "TRANSFER_TYPE"	   	=> 'Transfer Type',
							"RC"					=> 'RC',
							"TRA_STATUS" 			=> 'Status',
	            			);
		}else if($pslip['PS_TYPE'] == '12'){
			// Table Detail Header
			$fields = array("TRANSACTION_ID"		=>'Transaction ID',
							"SOURCE_NAME" 	=> 'Source Account',
							"BENEF_NAME"		=> 'Beneficiary Account',
							// "ACBENEF_NAME"		=> 'Beneficiary Name',
							// "BENEFICIARY_ID_NUMBER"		=> 'Beneficiary NRC',
							// "BENEFICIARY_MOBILE_PHONE_NUMBER"		=> 'Beneficiary Phone',

							// "BANK_NAME"			=> 'Beneficiary Bank',
							// "BENEFICIARY_BANK_ADDRESS1"				=> 'Beneficiary Bank Address',
							// "CITY"				=> 'Beneficiary Bank City',
							// "NOSTRO_NAME"	=> 'Nostro Bank',
							// "COUNTRY_NAME"	=> 'Country',

							"PS_CCY"  	   			=> 'CCY',
							// "RATE"  	   	=> 'Rate',
							"TRA_AMOUNT"  	   	=> 'Amount',
							// "BOOK_RATE"		=> 'Book Rate',
							// "TRANSFER_FEE"		=> 'Transfer Fee',
							// "FULL_AMOUNT_FEE"		=> 'Full Amount Fee',
							// "PROVISION_FEE"		=> 'Provision Fee',
							// "TOTAL"		=> 'Total',
							// "TRA_MESSAGE" 		=> 'Message',
							// "TRA_ADDMESSAGE"  	   	=> 'Additional Message',
							// "TRANSFER_TYPE"	   	=> 'Transfer Type',
							"RC"					=> 'RC',
							"TRA_STATUS" 			=> 'Status',
							// "EFT_STATUS" 		=> 'EFT Status',
	// 						"BANK_RESPONSE" 		=> 'Response',
							// "SOURCE_ACCOUNT" 	=> 'SOURCE_ACCOUNT',
	            			);
		}
		else{
			// Table Detail Header
			$fields = array("TRANSACTION_ID"		=>'Transaction ID',
							"SOURCE_NAME" 	=> 'Source Account',
							"BENEF_NAME"		=> 'Beneficiary Account',
							// "ACBENEF_NAME"		=> 'Beneficiary Name',
							// "BANK_NAME"			=> 'Bank',
							// "CITY"				=> 'City',
							// "BENE_CITYZENSHIP"	=> 'Citizenship',
							"PS_CCY"  	   			=> 'CCY',
							"TRA_AMOUNT"  	   		=> 'Amount',
							// "TRANSFER_FEE"		=> 'Charge',

							// "TRA_MESSAGE" 		=> 'Message',
							// "TRA_ADDMESSAGE"  	   	=> 'Additional Message',
							// "TRANSFER_TYPE"	   	=> 'Transfer Type',
							"RC"					=> 'RC',
							"TRA_STATUS" 			=> 'Status',
							// "EFT_STATUS" 		=> 'EFT Status',
	// 						"BANK_RESPONSE" 		=> 'Response',
							// "SOURCE_ACCOUNT" 	=> 'SOURCE_ACCOUNT',
	            			);
		}

		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											//'numbertrx'				=> 'COUNT(TT.TRANSACTION_ID)',
											'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
											'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT ( TT.BENEFICIARY_ACCOUNT_NAME , ' ' ) "),
											'SOURCE_ACCOUNT_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.SOURCE_ACCOUNT , ' (' , TT.SOURCE_ACCOUNT_CCY , ') - ' , F.BANK_NAME ) "),
											'BENEFICIARY_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') - ' , E.BANK_NAME ) "),
											'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
											'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
											'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
											'TT.BENEFICIARY_MOBILE_PHONE_NUMBER',
											'TT.BENEFICIARY_ID_NUMBER',
											'TT.BENEFICIARY_BANK_ADDRESS1',
											'TT.RATE',
											'TRA_REFNO'				=> 'TT.TRA_REFNO',
											'NOSTRO_NAME'			=> 'TT.NOSTRO_NAME',
											'BOOK_RATE'				=> 'TT.BOOK_RATE',
											'TT.RATE',
											'BOOK_RATE_BUY'			=> 'TT.BOOK_RATE_BUY',
											'FULL_AMOUNT_FEE'		=> 'TT.FULL_AMOUNT_FEE',
											'PROVISION_FEE'			=> 'TT.PROVISION_FEE',
											'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE 'N/A'
																			END"),
											'SOURCE_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
											'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
											'COUNTRY_NAME'			=> 'C.COUNTRY_NAME',
											'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
											'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
											'TRA_STATUS' 			=> $caseTraStatus,
											'TRASTATUS' 			=> $caseTraStatus1,
											'CITY'					=> 'TT.BENEFICIARY_BANK_CITY',
											'BENE_CITYZENSHIP'		=> $caseCityzenship,
											// 'EFT_STATUS'			=> new Zend_Db_Expr("
																			// CASE WHEN TT.TRANSFER_TYPE = '0' THEN '-'
																				 // ELSE ".$caseEFTStatus."
																			// END"),
											/* 'BANK_RESPONSE'		=> new Zend_Db_Expr("
																							CASE WHEN TT.BANK_RESPONSE = '' THEN 'N/A'
																								 WHEN TT.BANK_RESPONSE = null THEN 'N/A'
																								 ELSE TT.BANK_RESPONSE
																							END"), */
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'RC'					=> 'TT.BANK_RESPONSE',
											'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
											'SOURCE_ACCOUNT_CCY'	=> 'TT.SOURCE_ACCOUNT_CCY',
											'SOURCE_ACCOUNT_BANK'	=> 'F.BANK_NAME',
											'BENEFICIARY_ACCOUNT'		=> 'TT.BENEFICIARY_ACCOUNT',
											'BENEFICIARY_ACCOUNT_CCY'	=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'BENEFICIARY_ACCOUNT_BANK'	=> 'E.BANK_NAME',
											'D.PS_CCY','D.CUST_ID',
											'TT.EQUIVALENT_AMOUNT_IDR',
											'TT.EQUIVALENT_AMOUNT_USD',
											'PS_TYPE'				=> 'D.PS_TYPE',
											'PS_STATUS'				=> 'D.PS_STATUS',
											'PS_CCY'				=> 'D.PS_CCY',
										  )
									)
							->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
							->joinLeft ( array('C' => 'M_COUNTRY'), 'TT.BENEFICIARY_BANK_COUNTRY = C.COUNTRY_CODE', array() )
							->joinLeft ( array('D' => 'T_PSLIP'), 'TT.PS_NUMBER = D.PS_NUMBER', array() )
							->joinLeft(	array('F' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = F.BANK_CODE',array())
							->joinLeft(	array('E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->joinLeft ( array('G' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'), 'TT.TRANSACTION_ID = G.TRANSACTION_ID', array())
							->where('D.PS_STATUS = ?', '9')
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		//echo $select;die;
		$pslipTrx = $this->_db->fetchAll($select);

		$numbertrx 				= count($pslipTrx);
		$this->view->numbertrx 	= $numbertrx;

		// echo "<pre>";
		// print_r($pslipTrx);die;
		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				if($key == "TOTAL_AMOUNT"){
					if($pTrx['PS_CCY'] == 'USD'){
						$totalinvalas = $pTrx['EQUIVALENT_AMOUNT_USD'];
						$totalinidr = $pTrx['EQUIVALENT_AMOUNT_IDR'];
						//print_r($pTrx);die;
						if($pTrx['SOURCE_CCY']=='USD' && $pTrx['ACBENEF_CCY']=='USD' && $pslip['PS_TYPE']=='1'){
								$value = 'USD '.Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);

						}else if($pTrx['SOURCE_CCY']=='USD' && $pTrx['ACBENEF_CCY']=='USD'){
								$value = 'USD '.Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']);

							}else if($pTrx['PS_TYPE']!='3'){
						$value = $pTrx['ACBENEF_CCY'].' '.Application_Helper_General::displayMoney($totalinvalas).' (IDR '.Application_Helper_General::displayMoney($totalinidr).')';
						}else{
						$value = 'IDR '.Application_Helper_General::displayMoney($totalinidr);

						}

					}
					else{
						if($pTrx['EQUIVALENT_AMOUNT_IDR'] != '0.00' || $pTrx['EQUIVALENT_AMOUNT_IDR'] != ''){
							$pTrx['EQUIVALENT_AMOUNT_IDR'] = $pTrx['EQUIVALENT_AMOUNT_IDR'];
						}else{
							$pTrx['EQUIVALENT_AMOUNT_IDR'] = $pTrx['TRA_AMOUNT'];
						}

						$totalinidr = $pTrx['EQUIVALENT_AMOUNT_IDR'];
						$value = 'IDR '.Application_Helper_General::displayMoney($totalinidr);

					}


				}
				else{
					$value = $pTrx[$key];
				}
				if($key == 'ACBENEF' && ($pTrx['TRANSFER_TYPE']=='FA' || $pTrx['TRANSFER_TYPE']=='No FA')){
					// $app = Zend_Registry::get('config');
					// $appBankname = $app['app']['bankname'];
					$value = '';
				}
				if($key == 'FULL_AMOUNT_FEE' && ($pTrx['TRANSFER_TYPE']=='No FA')){
					// $app = Zend_Registry::get('config');
					// $appBankname = $app['app']['bankname'];
					$value = '0';
				}


				if($key == 'BANK_NAME' && ($pTrx['TRANSFER_TYPE']=='FA' || $pTrx['TRANSFER_TYPE']=='No FA')){
					$app = Zend_Registry::get('config');
					$appBankname = $app['app']['bankname'];
					$value = $appBankname.' - '.$pTrx['BANK_NAME'];
				}

				if($key == 'TOTAL'){
					if($pTrx['TRANSFER_TYPE']=='No FA'){
						$total = $pTrx['TRA_AMOUNT'];
					}else{
						$total = $pTrx['TRA_AMOUNT'] + $pTrx['FULL_AMOUNT_FEE'];
					}

					$value = $pTrx['SOURCE_CCY'].' '.Application_Helper_General::displayMoney($total);
				}
				if ($key == "TRA_AMOUNT")
				{
					if($pTrx['PS_TYPE']=='15' || $pTrx['PS_TYPE']=='14'){
						if($pTrx['PS_STATUS'] == '5' || $pTrx['PS_STATUS'] == '1' || $pTrx['PS_STATUS'] == '2'){
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
						}else{
						$value = '-';
						}

					}else{
					$value = Application_Helper_General::displayMoney($value);
					}
				}

				if($key == 'BOOK_RATE'){

					$value = Application_Helper_General::displayMoney($value);
					// print_r($value);die;
				}

				if($key == "TRANSFER_FEE"){

					$selecttrffee = $this->_db->select()->from(array('T'=>'M_CHARGES_REMITTANCE'),array('*'))
					->where("T.CHARGE_TYPE =?",'3')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_CCY'])
			;
			//echo $selecttrffee;
			$trffee = $this->_db->fetchRow($selecttrffee);
					$value = $trffee['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($value);
				}elseif($key == "FULL_AMOUNT_FEE"){
								$selecttrfFA = $this->_db->select()
			->from(		array(	'T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'4')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
			;
			$trfFA = $this->_db->fetchRow($selecttrfFA);

					$value = $trfFA['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($value);
				}elseif($key == "TRANSFER_TYPE")
				{
					if($value=='PB'){
						$value = 'Mayapada';
					}
				}
				if($key == 'RATE'){
				if($pTrx['SOURCE_CCY']=='USD' && $pTrx['ACBENEF_CCY']=='USD'){
					$value = '0.00';
				}
					$value = 'IDR '.Application_Helper_General::displayMoney($value);
				}
				if($key == 'PROVISION_FEE'){




			$selecttrfpro = $this->_db->select()
			->from(		array(	'T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'5')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =?",$pTrx['SOURCE_CCY'])
			;
			$trfpro = $this->_db->fetchRow($selecttrfpro);
				$value = $trfpro['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($value);
				//print_r($value);die;
				}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if ($pTrx['BANK_RESPONSE'] != NULL || !empty($pTrx['BANK_RESPONSE'])) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$tableDtl[$p]['BANK_RESPONSE'] = $bankresponse[1];

				$tableDtl[$p]['RC'] = $bankresponse[0];
			}

			if ($pTrx['SOURCE_ACCOUNT_BANK'] != NULL || !empty($pTrx['SOURCE_ACCOUNT_BANK'])) {
				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$pTrx['SOURCE_ACCOUNT_BANK'];
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}

			if ($pTrx['BENEFICIARY_ACCOUNT_BANK'] != NULL || !empty($pTrx['BENEFICIARY_ACCOUNT_BANK'])) {
				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$pTrx['BENEFICIARY_ACCOUNT_BANK'];
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}

			$tableDtl[$p]['TRASTATUS'] = $pTrx['TRASTATUS'];
			
			//$this->view->numbertrx 			= $pTrx['numbertrx'];
		}

		// echo "<pre>";
		// print_r($tableDtl);
		// die();

		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
	}
}