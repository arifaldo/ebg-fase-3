<?php
class pass_Model_Pass
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getCountry()
	{
		$select = $this->_db->select()
							->from('M_COUNTRY');

		return $this->_db->fetchAll($select);
	}

	public function getCCY()
	{
		$select = $this->_db->select()
							->from('M_MINAMT_CCY');

		return $this->_db->fetchAll($select);
	}

	public function getCodeLength()
	{
		$select = $this->_db->select()
							->from('M_SETTING',array('SETTING_VALUE'))
							->where('SETTING_ID = ?', 'company_code_len');

		return $this->_db->fetchOne($select);
	}

	public function insertMyOnlineReg($type,$data){
		if($type == 'ind')
			$tablename = 'TEMP_MYONLINE_IND';
		else
			$tablename = 'TEMP_MYONLINE_BIS';

		$this->_db->insert($tablename,$data);

		return $this->_db->lastInsertId();
	}

	public function insertMyOnlineIndAcc($data){
		$this->_db->insert('T_MYONLINE_IND_ACC',$data);
	}

	public function insertMyOnlineBisAcc($data){
		$this->_db->insert('T_MYONLINE_BIS_ACC',$data);
	}

	public function insertMyOnlineBisUser($data){
		$this->_db->insert('T_MYONLINE_BIS_USER',$data);
	}
}