<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';
require_once 'Crypt/AESMYSQL.php';

class backenduser_UnlockpassController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('userid'), $password));

		$submit = $this->_getParam('submit');
		$action = $this->_getParam('isEmail');
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('ADRP', 'View Unlock and Reset password Backend user (' . $user_id . ') page');
		}
		$select =	$this->_db->SELECT()
			->FROM(array('A' => 'M_BUSER'), array('BUSER_ID', 'BUSER_EMAIL', "BUSER_NAME"))
			->WHERE('BUSER_ID = ? ', $user_id);
		//echo $select;die;
		$result = $this->_db->FETCHROW($select);
		//Zend_Debug::dump($result);die;
		$this->view->email = $result['BUSER_EMAIL'];
		$this->view->userid = $result['BUSER_ID'];
		$this->view->username = $result['BUSER_NAME'];

		if ($submit && $this->view->hasPrivilege('ADRP')) {
			$actor = $this->_userIdLogin;
			$unlockfunction = new BankUser($user_id);
			$exeunlock = $unlockfunction->requestUnlockResetPassword($action, $actor);
			//die;
			Application_Helper_General::writeLog('ADRP', 'Unlock and Reset password Backend user (' . $user_id . ')');
			$this->setbackURL('/' . $this->_request->getModuleName() . '/index/index/');
			$this->_redirect('/notification/submited/index');
		}
	}
}
