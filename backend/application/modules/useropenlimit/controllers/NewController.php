<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class useropenlimit_NewController extends Application_Main 
{

  public function indexAction() 
  {
    $this->_helper->layout()->setLayout('newlayout');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $this->view->cust_idenc = $this->_getParam('cust_id');
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $result = $this->_db->fetchRow($select);
      if($result['CUST_ID'])
      {
        $this->view->cust_name = $result['CUST_NAME'];
      }
      else{ $cust_id = null; }
    }
    
    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/cust_id/'.$this->_getParam('cust_id'));
    
    if(empty($cust_id))
    {
      $error_remark = $this->language->_("Customer ID does not exist");
      //insert log
      try 
      {
      $this->_db->beginTransaction();
      $this->_db->commit();
    }
    catch(Exception $e) 
    {
      $this->_db->rollBack();
        // SGO_Helper_GeneralLog::technicalLog($e);
    }
      
    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_helper->url->url(array('module'=>'customer','controller'=>'index','action'=>'index')));
    }
               
    $fields = array('userid'       => array('field'    => 'USER_ID',
                                           'label'    => $this->language->_('User ID'),
                                           'sortable' => true),
    
                    'username'    => array('field'   => 'USER_FULLNAME',
                                          'label'    => $this->language->_('User Name'),
                                          'sortable' => true),
                    );

    $page = $this->_getParam('page');
    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    $sortBy = $this->_getParam('sortby');
    $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
  $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

    $filterArr = array('filter' => array('StripTags','StringTrim'),
                       'uid'    => array('StripTags','StringTrim','StringToUpper'),
                       'uname'  => array('StripTags','StringTrim','StringToUpper')
                      );
    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
    $filter = $zf_filter->getEscaped('filter');
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;
      
    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
  if(count($temp)>1){
        if($temp[0]=='F' || $temp[0]=='S'){
          if($temp[0]=='F')
            $this->view->error = 1;
          else
            $this->view->success = 1;
          $msg = ''; unset($temp[0]);
          foreach($temp as $value)
          {
            if(!is_array($value))
              $value = array($value);
            $msg .= $this->view->formErrors($value);
          }
          $this->view->user_msg = $msg;
      } 
    }

    if($filter==$this->language->_('Set Filter'))
    {
      $uid = html_entity_decode($zf_filter->getEscaped('uid'));
      $uname = html_entity_decode($zf_filter->getEscaped('uname'));
      $this->view->uid = $uid;
      $this->view->uname = $uname;
    }

    $select = $this->_db->select()
               ->from(array('A' => 'M_USER'),array('USER_ID','USER_FULLNAME'))
               ->joinLeft(array('B' => 'TEMP_MAKERLIMIT'),'A.USER_ID = B.USER_LOGIN AND A.CUST_ID = B.CUST_ID',array('B.USER_LOGIN'));

    $select->where('UPPER(A.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('A.USER_STATUS<>3');
                             
    
    if($filter==$this->language->_('Set Filter'))
    {
      if($uid)$select->where('UPPER(A.USER_ID) LIKE '.$this->_db->quote('%'.$uid.'%'));
      if($uname)$select->where('UPPER(A.USER_FULLNAME) LIKE '.$this->_db->quote('%'.$uname.'%'));
    }
    

    // TEMP_MAKERLIMIT
    $select->order($sortBy.' '.$sortDir);                                   
    $select->group('A.USER_ID');
  //echo $select;
    $this->paging($select);
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    
    $this->view->status_type = $this->_masterglobalstatus;
    $this->view->cust_id = $cust_id;
    $this->view->modulename = $this->_request->getModuleName();
  Application_Helper_General::writeLog('MLLS','View User Limit, Customer ID : '.$cust_id);
  }
}