<?php

require_once 'Zend/Controller/Action.php';

class backendgroup_SuggestiondetailController extends Application_Main
{
	public function indexAction()
	{
		$changeid = $this->_getParam('changes_id');
		$this->view->changes_id = $changeid;

		$select = $this->_db->select()
			->from(array('A' => 'T_GLOBAL_CHANGES'), array('*'))
			->joinleft(array('B' => 'TEMP_BGROUP'), 'A.CHANGES_ID = B.CHANGES_ID', array('*'));
		$select->where("B.CHANGES_ID = ?", $changeid);
		$select->where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$result = $select->query()->FetchAll();
		$this->view->result = $result;
		//Zend_Debug::dump($result);die;

		if (!$result) {
			$this->_redirect('/notification/invalid/index');
		}

		$select2 = $this->_db->select()
			->from(array('C' => 'TEMP_BPRIVI_GROUP'), array('*'))
			->joinleft(array('D' => 'M_BPRIVILEGE'), 'C.BPRIVI_ID = D.BPRIVI_ID', array('*'));
		$select2->where("C.CHANGES_ID = ?", $changeid);
		$result2 = $select2->query()->FetchAll();
		$this->view->result2 = $result2;

		$groupid = $result[0]['KEY_FIELD'];
		if (!$this->_request->isPost()) {

			Application_Helper_General::writeLog('VBGC', 'Backend group (' . $groupid . ') changes list');
		}
		$select3 = $this->_db->select()
			->from(array('M_BGROUP'), array('*'));
		$select3->where("BGROUP_ID LIKE " . $this->_db->quote($groupid));
		$result3 = $select3->query()->FetchAll();
		$this->view->result3 = $result3;

		$select4 = $this->_db->select()
			->from(array('C' => 'M_BPRIVI_GROUP'), array('*'))
			->joinleft(array('D' => 'M_BPRIVILEGE'), 'C.BPRIVI_ID = D.BPRIVI_ID', array('*'));
		$select4->where("C.BGROUP_ID LIKE " . $this->_db->quote($groupid));
		$result4 = $select4->query()->FetchAll();
		$this->view->result4 = $result4;

		$this->view->typeCode = array_flip($this->_changeType['code']);
		$this->view->typeDesc = $this->_changeType['desc'];
		$this->view->modulename = $this->_request->getModuleName();

		$changestype = $result[0]["CHANGES_TYPE"];
		//Zend_Debug::dump($groupid2); die;
		$this->view->changes_type = $changestype;

		if ($changestype == "N") {
			$this->_helper->viewRenderer('suggestiondetail/new', null, true);
		}
		if ($changestype == "E") {

			$this->_helper->viewRenderer('suggestiondetail/edit', null, true);
		}
		if ($changestype == "S") {
			$this->_helper->viewRenderer('suggestiondetail/suspend', null, true);
		}
		if ($changestype == "U") {
			$this->_helper->viewRenderer('suggestiondetail/unsuspend', null, true);
		}
		if ($changestype == "L") {
			$this->_helper->viewRenderer('suggestiondetail/delete', null, true);
		}
		//echo $select2; die;

		if ($this->_request->isPost()) {
			if (strtolower($this->_request->getPost("submit")) == "approve") {
				if ($changestype == "E") {
					$bgroup = $result2[0]["BGROUP_ID"];

					$this->_db->update("M_BUSER", [
						"BUSER_ISLOGIN" => 0
					], [
						"BGROUP_ID = ?" => $bgroup
					]);
				}
			}
		}
	}
}
