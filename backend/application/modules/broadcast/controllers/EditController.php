<?php

require_once 'Zend/Controller/Action.php';


class broadcast_EditController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$HELP_ID   	= $this->_getParam('ID');
		$this->view->HELP_ID = $HELP_ID;
		
		$filterArr = array(	'filter'=> array('StringTrim','StripTags'),
							'submit'=> array('StringTrim','StripTags'));
		
		$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$filter 	= $zf_filter->getEscaped('filter');
		
		$submit 	= $zf_filter->getEscaped('submit');
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}	
		}

		$select = $this->_db->select()
										->from(	'M_BROADCAST',
												array(
														'*'))
										->where('ID =?',$HELP_ID);

		$data = $this->_db->fetchRow($select);
		// print_r($data);die;
		if($submit)
		{
			if($this->_request->isPost())
			{
				$errorRemark = null;
				
				$filters = array(
									'helpTopic'	=> array('StripTags', 'StringTrim'),
									'text_content'	=> array('StripTags', 'StringTrim'),
								);
				$exclude = "HELP_ID != $HELP_ID and HELP_ISDELETED != 1";
				$validators = array(
										'helpTopic' => array(),
										'text_content' => array(),
									);
									
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
				$description 	= ($zf_filter_input->isValid('description'))? $zf_filter_input->description : $this->_request->getParam('description'); 
				$helpTopic 		= ($zf_filter_input->isValid('helpTopic'))? $zf_filter_input->helpTopic : $this->_request->getParam('helpTopic'); 
			
				// $helpTopic 		= $this->getRequest()->getParam('helpTopic');
				// $description 	= $this->getRequest()->getParam('description');

				if($zf_filter_input->isValid())
				{

					try
						{
							$this->_db->beginTransaction();
							// print_r($params);die;
							$insertArr = array(
								'B_TYPE'			=> $params['typesendid'], 
								'B_SUBJECT' 		=> $params['helpTopic'], 
								// 'B_EMAIL' 			=> $params['helpTopic'],
								// 'B_EFDATE'			=> $params['news'],
								'B_SENDTYPE' 		=> $params['sendtoid'],
								'B_DATETYPE'		=> $params['postdatetype'],
								'B_CONTENT'			=> $params['text_content'],
								'B_CREATEDBY'		=> $this->_userIdLogin, 
								'B_CREATED'			=> new Zend_Db_Expr('now()'), 
							);			

							if($params['sendtoid'] == '3'){
								$insertArr['B_EMAIL'] = $params['receiver_name'];
							}

							if($params['postdatetype'] == '1'){
								$insertArr['B_EFDATE'] = new Zend_Db_Expr('now()');

							}else if($params['postdatetype'] == '2'){
								$insertArr['B_EFDATE'] = $params['from_date'];
								$insertArr['B_EFTIME'] = $params['from_time'];
							}

							if($params['postdatetype'] == '1'){
								$insertArr['B_STATUS'] = '1';
							}else{
								$insertArr['B_STATUS'] = '2';
							}

							$where = array('ID = ?' => $HELP_ID);
							$query = $this->_db->update ( "M_BROADCAST", $insertArr, $where );

							// $select = $this->_db->insert('M_BROADCAST', $insertArr);
							//die($select);
							$this->_db->commit();
							$id = $this->_db->lastInsertId();
							Application_Helper_General::writeLog('HLAD','Add Broadcast. ID: ['.$id.'], Subject : ['.$params['helpTopic'].']');
							$this->setbackURL('/broadcast');
							$this->_redirect('/notification/success');

						}
						catch(Exception $e)
						{
							// print_r($e);die;
							$this->_db->rollBack();
							$this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
						}


				}
				else
				{
					$this->view->error 				= true;
					$errors 						= $zf_filter_input->getMessages();
					
					$this->view->helpTopicErr			= (isset($errors['helpTopic']))? $errors['helpTopic'] : null;
					$this->view->descriptionErr 		= (isset($errors['description']))? $errors['description'] : null;
				}	
			}
		}
		// $this->view->file = $data['HELP_FILENAME'];
		if($this->_request->isPost())
		{
			$this->view->helpTopic = (isset($zf_filter_input->helpTopic)) ? $zf_filter_input->helpTopic : $this->getRequest()->getParam('helpTopic');
			$this->view->description = (isset($zf_filter_input->description)) ? $zf_filter_input->description : $this->getRequest()->getParam('description');
		}
		else
		{
			$this->view->helpTopic = $data['B_SUBJECT'];
			$this->view->email  = $data['B_EMAIL'];
			$this->view->content = $data['B_CONTENT'];
			
			// $description = $data['NEWS'];
			// $this->view->news = $description;
		}
		$disable_note_len 	 = (isset($description))  ? strlen($description)  : 0;	
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
		
		$disable_note_len 	 = 100 - $disable_note_len;		
		$note_len 	 = 100 - $note_len;	

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('HLUD','Viewing Edit Help');
		}
						
	}
}
