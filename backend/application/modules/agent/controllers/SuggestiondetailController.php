<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class agent_SuggestiondetailController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('popup');

		$model = new agent_Model_Agent();
		$change_id = $this->_getParam('changes_id');

		// decrypt  ----------------------------------------------------

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER 	= urldecode($change_id);

		$change_id = $AESMYSQL->decrypt($BG_NUMBER, $password);

		// -------------------------------------------------------------

		$change_id = (Zend_Validate::is($change_id, 'Digits')) ? $change_id : 0;
		//$this->_moduleDB = strtoupper($this->_moduleID['user']);

		// $modelCust = new customer_Model_Customer();
		// $this->view->countryArr = Application_Helper_Array::listArray($modelCust->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');

		$this->view->suggestionType = $this->_suggestType;
		$this->view->status_type = $this->_masterglobalstatus;

		$this->_userAuth = Zend_Auth::getInstance()->getIdentity();
		$branchCode = $this->_userAuth->branchCode;
		$branchName = $this->_userAuth->branchName;

		$this->view->branchCode = $branchCode;
		$this->view->branchName = $branchName;
		$select = $this->_db->select()
			->from(array('L' => 'M_BUSER'), array('L.BUSER_BRANCH'))
			->joinLeft(array('B' => 'M_BRANCH'), 'B.ID = L.BUSER_BRANCH', array('B.*'))
			->where('BUSER_ID = ?', $this->_userIdLogin);
		// ->where('ACCT_STATUS = ?', '1');
		// echo $select;die;
		$branchdata = $this->_db->fetchRow($select);
		// print_r($branchdata);die;
		$this->view->branchCode = $branchdata['BRANCH_CODE'];
		$this->view->branchName = $branchdata['BRANCH_NAME'];

		if ($change_id) {
			$result = $this->getGlobalChanges($change_id);
			if ($result) {
				if (!in_array($result['CHANGES_STATUS'], array('WA', 'RR'))) {
					$this->_redirect('/notification/invalid/index');
				} else {
					//content send to view
					$resultdata = $model->getTempAgent($change_id);
					// print_r($resultdata);die;
					if ($resultdata['TEMP_ID']) {
						$user_id = $resultdata['USER_ID'];
						//----------------------------------------------TEMP DATA--------------------------------------------------------------
						foreach ($resultdata as $key => $value) {
							$id = strtolower($key);
							$this->view->$id = $resultdata[$key];
						}

						$detail = $model->getMasterDetail($user_id);

						$this->view->user_fullname = $resultdata['USER_FULLNAME'];
						$this->view->user_address = $resultdata['USER_SERVICE_ADDRESS'];
						$this->view->user_city = $resultdata['USER_CITY'];
						$this->view->user_zip = $detail['USER_ZIP'];
						$this->view->user_province = $resultdata['USER_PROVINCE'];
						$this->view->user_country = $detail['USER_COUNTRY'];

						// $modelAcc = new useraccount_Model_Useraccount();
						//       $accData = $modelAcc->getUserAcct(array('user_id'=>$user_id,'acct_no'=>$resultdata['USER_ACCT_NO']));
						//       $this->view->user_acct_name = $this->view->m_user_acct_name = $accData[0]['ACCT_NAME'];

						//----------------------------------------------MASTER DATA--------------------------------------------------------------
						if ($this->view->changes_type == 'E') {
							$m_resultdata = $model->getUserData(array('user_id' => $user_id));
							foreach ($m_resultdata as $key => $value) {
								$id = 'm_' . strtolower($key);
								$this->view->$id = $m_resultdata[$key];

								if ($id == "m_user_status")
									$this->view->$id = $m_resultdata['AGENT_STATUS'];
							}
						} else if ($this->view->changes_type == 'S' || $this->view->changes_type == 'U') {
							$m_resultdata = $model->getUserData(array('user_id' => $user_id));
							foreach ($m_resultdata as $key => $value) {
								$id = strtolower($key);
								$this->view->$id = $m_resultdata[$key];

								if ($id == "m_user_status")
									$this->view->$id = $m_resultdata['AGENT_STATUS'];
							}
							//$this->view->user_status = $resultdata['USER_STATUS'];
						}
					} else {
						$change_id = 0;
					}
				}
			}
		}





		if (!$change_id) {
			$error_remark = $this->language->_('Changes ID is not found');
			//insert log
			try {
				$this->_db->beginTransaction();
				$this->backendLog(strtoupper($this->_actionID['view']), strtoupper($this->_moduleID['user']), null, null, $error_remark);
				$this->_db->commit();
			} catch (Exception $e) {
				$this->_db->rollBack();
				SGO_Helper_GeneralLog::technicalLog($e);
			}

			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			//$this->_redirect($this->_backURL);
			$this->_redirect('/popuperror/index/index');
		}

		//insert log
		try {
			$this->_db->beginTransaction();
			$fulldesc = 'CHANGES_ID:' . $change_id;

			// Application_Helper_General::writeLog('CSCL','');
			//Application_Helper_General::writeLog('CSCA','Approve Frontend User : '. $this->_request->getParam('changes_id'));
			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollBack();
			SGO_Helper_GeneralLog::technicalLog($e);
		}

		$this->view->changes_id = $change_id;
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('CCCL', 'View Customer Change List : ' . $user_id);
		}
	}
}
