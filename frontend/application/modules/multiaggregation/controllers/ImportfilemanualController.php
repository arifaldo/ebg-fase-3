<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
//ini_set("display_errors",'1');

class multiaggregation_ImportfilemanualController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	protected $_destinationUploadDir = '';
	protected $_maxRow = '';

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
	}

	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL();

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $databank = $cache->load($cacheID);
		//echo '<pre>';
		//var_dump($newbank);die;
        if(empty($databank)){
			$selectbank = $this->_db->select()
						 ->from(array('C' => 'M_BANK_TABLE'),array('*'));
			$databank 					= $this->_db->fetchAll($selectbank);
			$cache->save($databank,$cacheID);
		}
		//$total = count($databank);
		$databanknew = array();
		//$newbank = array_push($databanknew,$databank);
		//echo '<pre>';
		//var_dump($newbank);die;
		$i = 0;
		foreach ($databank as $key => $val)
		{
			//var_dump($ket);
			///var_dump($row_product);
			$databanknew [$i]["BANK_CODE"]= $key;
			$databanknew [$i]["BANK_NAME"]= $val;
			$i++;
		}
		//echo '<pre>';
		//var_dump($databanknew);die;
		$this->view->databank = $databanknew;

		$settingObj = new Settings();
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();
		$this->view->AccArr =  $AccArr;
		$listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');

		if($this->_request->isPost() )
		{
		    $this->_request->getParams();

			$filter = new Application_Filtering();
			$confirm = false;
			$error_msg[0] = "";

			$bankcode = $this->_getParam('bank_code');

			if(!$bankcode)
			{
				$error_msg[0] = $this->language->_('Bank cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			
			else
			{
				// $paramSettingID = array('range_futuredate', 'auto_release_payment');

				// $settings = new Application_Settings();
				// $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				// $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				// $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				// $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					$this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}

						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{
								$rowNum = 0;

								

								$paramTrxArr = array();

								$success = 0;
								$failed = 0;
								//echo '<pre>';
								//var_dump($csvData);die;
								
								$new_csv = array_unique($csvData, SORT_REGULAR);
								if(count($new_csv) == count($csvData)){
								
								foreach ( $csvData as $row )
								{
									if(count($row)==3)
									{
										$rowNum++;
										$benefAcct = trim($row[0]);
//										$benefName = trim($row[1]);
										$name = strtoupper(trim($row[1]));
										$amount = trim($row[2]);
										$ccy = 'IDR';

										$selectdata = $this->_db->select()
										->from(array('C' => 'T_BALANCE'),array('*'))
													->where('C.ACCT_NO = ?',$benefAcct)
													->where('C.CUST_ID = ?', $this->_custIdLogin);
													// echo $selectbank;die;
										$datacheck 					= $this->_db->fetchAll($selectdata);

										$valid = 1;
										$success++;

										if(empty($datacheck)){
											$registered = 0;
										}else{
											$registered = 1;
										}

										$paramTrx = array( 
											'BANK_CODE'		=> $bankcode,
											'BENEFICIARY_ACCOUNT' => $benefAcct,
											'BENEFICIARY_NAME' => $name,
											'BENEFICIARY_ACCOUNT_CCY' => $ccy,
											'AMOUNT' => $amount,
											'VALID'	=> $valid,
											'REGISTERED' => $registered
										);


										array_push($paramTrxArr,$paramTrx);
									}
									else
									{
										$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
										break;
									}
								}
								}else{
										$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Duplicate data').'.';
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
										//break;
								}
							}
							// kalo jumlah trx lebih dari setting
							else
							{
								$error_msg[0] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}

							// kalo gak ada error
							if(!$error_msg[0])
							{
								// $validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								// $resWs = array();
								// $resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);
								// $payment 		= $validate->getPaymentInfo();
								// $sourceAccountType = $resWs['accountType'];

								// if($validate->isError() === false)	// payment data is valid
								// {
								// 	$confirm = true;

								// 	$validate->__destruct();
								// 	unset($validate);
								// }
								// else
								// {
									// $errorMsg 		= $validate->getErrorMsg();
									// $errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

									// $validate->__destruct();
									// unset($validate);

									// if($errorMsg)
									// {
									// 	$error_msg[0] = $errorMsg;
									// 	$this->view->error 		= true;
									// 	$this->view->report_msg	= $this->displayError($error_msg);
									// }
									// else
									// {
										$confirm = true;
									// }
								// }
							}
						}
						else //kalo total record = 0
						{
							//$error_msg[0] = 'Error: Wrong File Format. There is no data on csv File.';
							//$error_msg[0] = 'Error: Wrong File Format.';
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}
					}
				}
				else
				{
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
			}

			if($confirm)
			{
				$result['success'] = $success;
				$result['failed'] = $failed;
				// $result['success'] = $total;
				$content['paramcount'] = $result;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorMsg'] = $errorMsg;
				// $content['errorTrxMsg'] = $errorTrxMsg;
				// $content['payment'] = $payment;
				// $content['sourceAccountType'] = $sourceAccountType;


				$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
				$sessionNamespace->content = $content;
				$this->_redirect('/multiaggregation/importfilemanual/confirm');
			}

			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACCTSRC = $ACCTSRC;
			$this->view->PSEFDATE = $PS_EFDATE;
		}
		Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
	}

	private function resData($benefAccount){
			$select	= $this->_db->select()
								->from(array('B'	 			=> 'M_BENEFICIARY'), array('BANK_NAME','BENEFICIARY_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_CITIZENSHIP','BENEFICIARY_RESIDENT','BENEFICIARY_ID_NUMBER','BENEFICIARY_ID_TYPE','BENEFICIARY_CITY_CODE','BENEFICIARY_CATEGORY')
									   );
			$select->where("B.BENEFICIARY_ACCOUNT = ?", $benefAccount);

			$bene = $this->_db->fetchAll($select);
			return $bene;
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$dataimport = $sessionNamespace->content;
		
		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANK_TABLE'),array('*'))
					->where('C.BANK_CODE = ?',$dataimport['paramTrxArr'][0]['BANK_CODE']);
					// echo $selectbank;die;
		$databank 					= $this->_db->fetchAll($selectbank);
		$this->view->BANK_NAME = $databank['0']['BANK_NAME'];

		$this->view->countdata = $dataimport['paramcount'];

		if ($dataimport['paramcount']['failed'] > 0) {
			$this->view->failed = true;
		}

		// echo "<pre>";
		// print_r($data);die();


		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/multiaggregation/importfilemanual');
			}
			
			$frontendOptions2 = array ('lifetime' => 86400,
                                  'automatic_serialization' => true );
	        $backendOptions2 = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
	        $cache2 = Zend_Cache::factory ( 'Core', 'File', $frontendOptions2, $backendOptions2 );
	        $cacheID2 = 'MAGR'.$this->_custIdLogin;
			
			$data = $cache2->load($cacheID2);
	        $cache2->remove($cacheID2);
			$totaldata = count($data['datapers']);
			$totalmanual = 0;
			
			$selectbank = $this->_db->select()
							->from(array('C' => 'M_BANK_TABLE'), array('*'));

						$databank = $this->_db->fetchAll($selectbank);
						
			foreach($dataimport['paramTrxArr'] as $row)
			{
				
				if($row['VALID']){
				 // $this->_db->beginTransaction();

					if ($row['REGISTERED']) {
						// die('gere');

						$updateArr = array('PLAFOND' => $row['AMOUNT']);				
						$where['CUST_ID = ?'] = $this->_custIdLogin;
						$where['ACCT_NO = ?'] = $row['BENEFICIARY_ACCOUNT'];
						$this->_db->update('T_BALANCE',$updateArr,$where);

					}
					else{
						
						$inserthistory = array(
		                      'CUST_ID'    => $this->_custIdLogin,
		                      'ACCT_NO'    => $row['BENEFICIARY_ACCOUNT'],
		                      'CCY_ID'    => 'IDR',
		                      'ACCT_NAME'    => $row['BENEFICIARY_NAME'],
		                      'ACCT_ALIAS' 	=> $row['BENEFICIARY_NAME'],
		                      'PLAFOND'    => $row['AMOUNT'],
		                      'ACCT_STATUS'    => '5',
		                      'PLAFOND_END'    => date('Y-m-d'),
		                      'BANK_CODE'    => $row['BANK_CODE'],
		                      'PLAFOND_DATE' => new Zend_Db_Expr("now()")
		                   		 ); 
					
						

						foreach ($databank as $key => $value) 
							if($value['BANK_CODE'] == $row['BANK_CODE']){
								$bankname = $value['BANK_NAME'];
							}
							//$newDataBank[$row['BANK_CODE']] = $value['BANK_NAME'];
						
						// $this->_db->delete('T_HISTORY_STATEMENT',$where);
						$this->_db->insert('T_BALANCE',$inserthistory);
						$totalmanual  = $totalmanual + $row['AMOUNT'];
							
							$data['datapers'][$totaldata+1]['bank_name']  = $bankname;
							$data['datapers'][$totaldata+1]['bank_code']  = $row['BANK_CODE'];
							$data['datapers'][$totaldata+1]['account_number']  = $row['BENEFICIARY_ACCOUNT'];
							$data['datapers'][$totaldata+1]['account_name']  = $row['BENEFICIARY_NAME'];

							if (isset($row['BENEFICIARY_NAME'])) {
								$data['datapers'][$totaldata+1]['account_alias']  = $row['BENEFICIARY_NAME'];
							}

							$data['datapers'][$totaldata+1]['account_currency']  = 'IDR';
							$data['datapers'][$totaldata+1]['account_balance']  = $row['AMOUNT'];
							$data['datapers'][$totaldata+1]['rs_datetime']	=  Application_Helper_General::convertDate(date('Y-m-d h:i:s'),$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
							$data['datapers'][$totaldata+1]['account_status'] = '5';
							//$data['datapers'][$totaldata+1]['user'] = $this->_userIdLogin;
							//$data['datapers'][$totaldata+1]['cust'] = $this->_custIdLogin;
							//echo '<pre>';
							//var_dump($data);die;
						
					}
				
				// $this->_db->commit();
				}

			}

			
			
				

		        $save['lastupdate']	= date('Y-m-d H:i:s');
				
				$save['totalmanual'] = $data['totalmanual']+$totalmanual;
				$save['totalonline'] = $data['totalonline'];
				
				$save['datapers'] = $data['datapers'];
				//echo "<pre>";
				//var_dump($save);die;
				//$totalonline = $data['totalonline'] + $balance['0']['BALANCE'];
				//$save['totalonline'] = $totalonline;
				$cache2->save($save,$cacheID2);
			
				
			unset($_SESSION['confirmBulkCredit']);
			$this->_redirect('/notification/success');
			
		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}
}
