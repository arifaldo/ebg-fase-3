<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';


class bgclosingworkflow_ReviewController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /* 'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg Number / Subject'),
      ), */
      'bgno'     => array(
        'field'    => 'BG_NUMBER',
        'label'    => $this->language->_('Nomor BG / Subjek'),
      ),
      'principal'     => array(
        'field'    => 'CUST_ID',
        'label'    => $this->language->_('Principal'),
      ),
      'obligee_name'     => array(
        'field'    => 'RECIPIENT_NAME',
        'label'    => $this->language->_('Obligee'),
      ),
      /* 'startdate' => array(
        'field' => 'TIME_PERIOD_START',
        'label' => $this->language->_('Start Date'),
      ),
      'enddate'   => array(
        'field'    => 'TIME_PERIOD_END',
        'label'    => $this->language->_('End Date'),
      ), */
      'bank_branch'   => array(
        'field'    => 'BRANCH_NAME',
        'label'    => $this->language->_('Bank Cabang'),
      ),
      /*'counter_type'   => array(
        'field'    => 'BG_FORMAT',
        'label'    => $this->language->_('Tipe Kontra'),
      ),*/
      'bgamount'  => array(
        'field'    => 'BG_AMOUNT',
        'label'    => $this->language->_('Nominal BG')
      ),
      'type' => array(
        'field' => 'CHANGE_TYPE',
        'label' => $this->language->_('Tipe Usulan'),
      ),
      /* 'type' => array(
        'field' => 'IS_AMENDMENT',
        'label' => $this->language->_('Type'),
      ), */
    );

    if ($this->view->hasPrivilege("RPCC")) $warantyIn = ['1']; // Cash Collateral 
		if ($this->view->hasPrivilege("RPNC")) $warantyIn = ['2', '3']; // Non-Cash Collateral
		if ($this->view->hasPrivilege("RPCC") && $this->view->hasPrivilege("RPNC")) $warantyIn = ['1','2', '3']; 

    $selectbgArr = $this->_db->select()
      ->from(
        array('A' => 'T_BANK_GUARANTEE'),
        array(
          'REG_NUMBER'                    => 'BG_REG_NUMBER',
          'BG_NUMBER'                    =>  'BG_NUMBER',
          'SUBJECT'                       => 'BG_SUBJECT',
          'APLICANT'                      => 'B.CUST_NAME',
          'RECIPIENT_NAME'                => 'RECIPIENT_NAME',
          'TIME_PERIOD_START'             => 'TIME_PERIOD_START',
          'TIME_PERIOD_END'               => 'TIME_PERIOD_END',
          'COUNTER_WARRANTY_TYPE'         => 'COUNTER_WARRANTY_TYPE',
          'COUNTER_WARRANTY_ACCT_NO'      => 'COUNTER_WARRANTY_ACCT_NO',
          'COUNTER_WARRANTY_ACCT_NAME'    => 'COUNTER_WARRANTY_ACCT_NAME',
          'IS_AMENDMENT'                  => 'CHANGE_TYPE',
          'AMOUNT'                        => 'BG_AMOUNT',
          'BRANCH_NAME'                   => 'C.BRANCH_NAME',

        )
      )
      ->join(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID')
      ->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE')
      ->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
      //->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn)
      ->where('D.SUGGESTION_STATUS IN (5)')
      //->where('A.BG_STATUS IN (2, 22)')
      ->order('A.BG_CREATED DESC');

    $selectbg = $selectbgArr;

    // select branch ------------------------------------------------------------
    $select_branch = $this->_db->select()
      ->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
      ->query()->fetchAll();

    $save_branch = [];

    foreach ($select_branch as $key => $value) {
      $save_branch[$value["BRANCH_CODE"]] = $value["BRANCH_NAME"];
    }

    $this->view->sel_branch = $save_branch;
    //  --------------------------------------------------------------------------

    // select APPICANT ------------------------------------------------------------
    $select_applicant = $this->_db->select()
      ->from(array('A' => 'M_CUSTOMER'), array("CUST_ID", "CUST_NAME"))
      ->query()->fetchAll();

    $save_applicant = [];

    foreach ($select_applicant as $key => $value) {
      $save_applicant[$value["CUST_ID"]] = $value["CUST_ID"];
    }

    $this->view->sel_applicant = $save_applicant;
    //  --------------------------------------------------------------------------

    // select counter type ------------------------------------------------------------
    $save_counter_type = [
      1 => 'FC',
      2 => 'LF',
      3 => 'Insurance'
    ];

    $this->view->counter_type = $save_counter_type;
    //  -------------------------------------------------------------------------- 

    // select tipe usulan ------------------------------------------------------------
    $tipe_usulan = [
      1 => 'Penutupan Permintaan Principal',
      2 => 'Pelepasan MD',
      3 => 'Klaim Obligee'
    ];

    $this->view->tipe_usulan = $tipe_usulan;
    //  -------------------------------------------------------------------------- 


    // select type ------------------------------------------------------------
    $getct        = Zend_Registry::get('config');
    $ctdesc     = $getct["bg"]["changetype"]["desc"];
    $ctcode     = $getct["bg"]["changetype"]["code"];
    $arr_type = array_combine(array_values($ctcode), array_values($ctdesc));

    $this->view->arr_type = $arr_type;
    //  -------------------------------------------------------------------------- 

    // filter ----------------------------------------------------------

    $filterlist = array("BG_NUMBER", "BG_SUBJECT", "APPLICANT","OBLIGEE", "BRANCH", "CHANGE_TYPE");

    $this->view->filterlist = $filterlist;

    $filterArr = array(
      'filter'  =>  array('StripTags'),
      'BG_NUMBER'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'BG_SUBJECT'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'APPLICANT'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'OBLIGEE'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'BRANCH'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      //'COUNTER_TYPE'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'CHANGE_TYPE' => array('StripTags', 'StringTrim', 'StringToUpper'),
    );


    $dataParam = array("BG_NUMBER", "BG_SUBJECT", "APPLICANT","OBLIGEE", "BRANCH", "COUNTER_TYPE", "CHANGE_TYPE");

    $dataParamValue = array();

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }
    }

    // var_dump($this->_request->getParams());
    // die();

    if (!empty($this->_request->getParam('efdate'))) {
      $efdatearr = $this->_request->getParam('efdate');
      $dataParamValue['BG_PERIOD'] = $efdatearr[0];
      $dataParamValue['BG_PERIOD_END'] = $efdatearr[1];
    }

    $validator = array(
      'BG_NUMBER'     =>  array(),
      'BG_SUBJECT'     =>  array(),
      'APPLICANT'     =>  array(),
      'OBLIGEE'     =>  array(),
      'BRANCH'     =>  array(),
      //'COUNTER_TYPE'     =>  array(),
      'CHANGE_TYPE'     =>  array(),
    );

    // echo "<pre>";
    // print_r($dataParamValue);die;


    $zf_filter   = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

    //echo $zf_filter->getEscaped('BRANCH_NAME');

    $filter     = $this->_getParam('filter');
    $BG_NUMBER    = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));
    $BG_SUBJECT    = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
    $APPLICANT    = html_entity_decode($zf_filter->getEscaped('APPLICANT'));
    $OBLIGEE    = html_entity_decode($zf_filter->getEscaped('OBLIGEE'));
    $BRANCH    = html_entity_decode($zf_filter->getEscaped('BRANCH'));
    //$COUNTER_TYPE    = html_entity_decode($zf_filter->getEscaped('COUNTER_TYPE'));
    $LAST_SAVED_BY    = html_entity_decode($zf_filter->getEscaped('LAST_SAVED_BY'));
    $CHANGE_TYPE    = html_entity_decode($zf_filter->getEscaped('CHANGE_TYPE'));


    if ($filter == TRUE) {

      if ($BG_NUMBER != null) {
        $selectbg->where("A.BG_NUMBER LIKE " . $this->_db->quote('%' . $BG_NUMBER . '%'));
      }

      if ($BG_SUBJECT != null) {
        $selectbg->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $BG_SUBJECT . '%'));
      }

      if ($APPLICANT   != null) {
        $selectbg->where("C.CUST_NAME LIKE " . $this->_db->quote('%' . $APPLICANT . '%'));
      }

      if ($OBLIGEE   != null) {
        $selectbg->where("A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $OBLIGEE . '%'));
      }

      if ($BRANCH     != null) {
        $selectbg->where("C.BRANCH_CODE LIKE " . $this->_db->quote('%' . $BRANCH . '%'));
      }

      /*if ($COUNTER_TYPE     != null) {
        $selectbg->where("A.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $COUNTER_TYPE . '%'));
      }*/

      if ($CHANGE_TYPE     != null) {
        $selectbg->where("D.CHANGE_TYPE LIKE " . $this->_db->quote('%' . $CHANGE_TYPE . '%'));
      }
    }

    $this->view->fields = $fields;
    $this->view->filter = $filter;



    //echo $selectbg;die;
    // -----------------------------------------------------------------

    $auth = Zend_Auth::getInstance()->getIdentity();
    if ($auth->userHeadQuarter == "NO") {
      $selectbg->where('C.ID = ?', $auth->userBranchId); // Add Bahri
    }

    $selectbg = $this->_db->fetchAll($selectbg);

    $result = $selectbg;

    $setting = new Settings();
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $pw_hash = md5($enc_salt . $enc_pass);
    $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
    $sessionNamespace->token   = $rand;
    $this->view->token = $sessionNamespace->token;

    foreach ($result as $key => $value) {
      $get_bg_number = $value["BG_NUMBER"];

      $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_bgnumber = $AESMYSQL->encrypt($get_bg_number, $rand);
      $encpayreff_bgnumber = urlencode($encrypted_bgnumber);

      $result[$key]["BG_NUMBER_ENCRYPTED"] = $encpayreff_bgnumber;

      $get_reg_number = $value["BG_REG_NUMBER"];

      $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_regnumber = $AESMYSQL->encrypt($get_reg_number, $rand);
      $encpayreff_regnumber = urlencode($encrypted_regnumber);

      $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff_regnumber;
    }

    $this->paging($result);

    $conf = Zend_Registry::get('config');

    $this->view->bankname = $conf['app']['bankname'];

    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;

    if (!empty($dataParamValue)) {

      $this->view->efdateStart = $dataParamValue['BG_PERIOD'];
      $this->view->efdateEnd = $dataParamValue['BG_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[]  = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[]  = $key;
          $whereval[] = $value;
        }
      }
      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }


    Application_Helper_General::writeLog('RCCS', 'Lihat Daftar Pengajuan Penutupan dan Klaim Menunggu Review Bank');
  }

  public function getcountertypeAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');


    $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

    $arrWarrantyType = array(
      '1' => 'FC',
      '2' => 'LF',
      '3' => 'INS'
    );

    foreach ($arrWarrantyType as $key => $row) {
      if ($tblName == $key) {
        $select = 'selected';
      } else {
        $select = '';
      }
      $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
    }

    echo $optHtml;
  }
}
