<?php

use function PHPSTORM_META\type;

require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'CMD/Validate/Validate.php';

class backenduser_AddController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$selectGroup =	$this->_db->select()
			->from('M_BGROUP', array('BGROUP_ID', 'BGROUP_DESC'))
			->where("BGROUP_STATUS = '1'");
		//->where('PASSWORD = \''.$v_password_old.'\'');

		// echo $v_password;
		$group = $this->_db->fetchAll($selectGroup);
		$arrgroup = Application_Helper_Array::listArray($group, 'BGROUP_ID', 'BGROUP_DESC');

		$this->view->arrGroup = $arrgroup;


		$selectBranch =	$this->_db->select()
			->from('M_BRANCH', array('ID', 'BRANCH_NAME'));
		//->where('PASSWORD = \''.$v_password_old.'\'');

		// echo $v_password;
		$branch = $this->_db->fetchAll($selectBranch);
		$arrbranch = Application_Helper_Array::listArray($branch, 'ID', 'BRANCH_NAME');

		$this->view->arrBranch = $arrbranch;

		$flbranch = Application_Helper_Array::listArray($branch, 'BRANCH_NAME', 'ID');
		$flip = Application_Helper_Array::listArray($group, 'BGROUP_DESC', 'BGROUP_ID');

		$setting = new Settings();
		$minBuserID = $setting->getSetting('min_length_buserid');
		$maxBuserID = $setting->getSetting('max_length_buserid');
		$this->view->minBuserID = $minBuserID;
		$this->view->maxBuserID = $maxBuserID;

		if ($this->_request->isPost()) {
			$filters = array(
				'userId'	=> array('StringToUpper', 'StripTags', 'StringTrim'),

				'nik' 	=> array('StripTags', 'StringTrim'),
				'groupId'  	=> array('StripTags', 'StringTrim'),
				'branch'  	=> array('StripTags', 'StringTrim'),
				'email'  	=> array('StripTags', 'StringTrim'),
				'password_to_ldap'  	=> array('StripTags', 'StringTrim'),
			);

			$validators = array(
				'userId' => array(
					'NotEmpty',
					// 'Alnum',
					array('StringLength', array('min' => $minBuserID, 'max' => $maxBuserID)),
					'messages' => array(
						$this->language->_('Can not be empty, Invalid user Id format'),
						// $this->language->_('Invalid user Id format'),
						$this->language->_('Minimal ' . $minBuserID . ' karakter dan maksimal ' . $maxBuserID . ' karakter'),
					)
				),

				'nik' => array(
					'NotEmpty',
					//new SGO_Validate_EmailAddress(),
					array('StringLength', array('min' => 16, 'max' => 16)),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Minimal 16 digit'),
						//$this->language->_('Invalid email format'),
					)
				),
				'groupId' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Please Select Group'),
					)
				),
				'branch' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Please Select Branch'),
					)
				),
				'password_to_ldap' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty'),
					)
				),
				'email' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Email can not be empty'),
					)
				),
			);
			$params = $this->_request->getParams();
			// $params["email"] = explode("@", $params["email"])[0] . "@btn.co.id";
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

			/*
			//validasi multiple email
			if ($this->_getParam('email')) {
				$validate = new validate;
				$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('email'));
			} else {
				$cek_multiple_email = true;
			}
			*/

			//$groupid = $this->_request->getParam('groupId');
			//$valid = new Zend_Validate_Identical(array($groupid => '0'));


			//			Zend_Debug::dump($arrgroup);die;
			//			CEK HURUF DAN ANGKA DI USERID			
			$strtemp = 0;
			$digittemp = 0;
			$user_id = $zf_filter_input->userId;
			$str = str_split($user_id);
			// foreach ($str as $string) {
			// 	if ((Zend_Validate::is($string, 'Digits')) ? true : false) {
			// 		$digittemp = 1;
			// 	}
			// 	if ((Zend_Validate::is($string, 'Alpha')) ? true : false) {
			// 		$strtemp = 1;
			// 	}
			// }
			$cekkombinasi = true;
			// if ($digittemp == 1 && $strtemp == 1) {
			// 	$cekkombinasi = true;
			// }


			$authAdapter = new SGO_Auth_Adapter_Ldap($this->_userIdLogin, $this->_request->getParam("password_to_ldap"));

			$checkLDAP = $authAdapter->verifyldap();

			$checkLDAP = json_decode($checkLDAP, true);

			if ($checkLDAP['status'] === '0') {
				$error_LDAP = $this->language->_("Password Salah");
			}

			$notFound = false;
			if ($checkLDAP['status'] === '1') {
				$checkUserInput = $authAdapter->checkUserLDAP(strtolower($user_id));

				//var_dump($checkUserInput);die;

				if ($checkUserInput['status'] === '0') {
					$notFound = true;
					$error_LDAP = $this->language->_("User id tidak ditemukan");
				}

				if ($checkUserInput['status'] === '1') {
					$name = $checkUserInput["fullName"];
				}
			}

			$validLDAP = false;
			if ($checkLDAP['status'] === '1') {
				/*
				if ($this->_getParam('email') != $checkLDAP['userprincipalname']) {
					$errLDAP = false;
				} else {
					$errLDAP = true;;
				}*/
				if (!$notFound) {
					$validLDAP = true;
				}
				//echo '<pre>';
				//var_dump($checkLDAP['userprincipalname']);die;
				//if($checkLDAP['userprincipalname'])
			}
			//var_dump($errLDAP);die;
			//			echo $strtemp.' '.$digittemp;die;
			//			CEK GROUPID
			//			$group = (($this->_getParam('groupId')==0) ? false : true);

			//if ($zf_filter_input->isValid() && $cekkombinasi && $cek_multiple_email == true && $errLDAP) {

			// echo '<pre>';


			if ($zf_filter_input->isValid() && $cekkombinasi && $validLDAP) {

				//$email = $zf_filter_input->email;

				$selectmaster =	$this->_db->select()
					->from('M_BUSER', array('BUSER_ID'))
					->where('BUSER_ID = ?', $user_id)
					->query()->fetchAll();

				$selecttemp = $this->_db->select()
					->from('TEMP_BUSER')
					->where('BUSER_ID = ?', $user_id)
					->query()->fetchAll();

				if (count($selectmaster) == 0 && count($selecttemp) == 0 && $this->view->hasPrivilege('ADAD')) {
					Application_Helper_General::writeLog('ADAD', 'Add Backend User');
					$info = 'User ID = ' . $params['userId'] . ', User Name = ' . $params['name'];
					$this->_db->beginTransaction();
					$change_id = $this->suggestionWaitingApproval('Backend User', $info, 'N', null, 'M_BUSER', 'TEMP_BUSER', $params['userId'], $params['name']);
					try {

						$insertArr = array(
							'CHANGES_ID' 		=> $change_id,
							'BUSER_ID'			=> $zf_filter_input->userId,
							// 'BUSER_NAME' 		=> $checkLDAP['givenname'],
							'BUSER_NAME' 		=> (trim($name)) ? $name : "-",
							'BGROUP_ID' 		=> $zf_filter_input->groupId,
							'BUSER_BRANCH' 		=> $zf_filter_input->branch,
							'BUSER_DESC' 		=> $params['desc'],
							'BUSER_EMAIL'		=> $zf_filter_input->email . "@btn.co.id", //$checkLDAP['userprincipalname'],
							'BUSER_NIK'			=> $zf_filter_input->nik
						);

						//						Zend_Debug::dump($insertArr);die;
						$this->_db->insert('TEMP_BUSER', $insertArr);
						$this->_db->commit();
						$this->setbackURL('/' . $this->_request->getModuleName() . '/index/index/');
						$this->_redirect('/notification/submited/index');
						$this->view->success 	= true;
						$this->view->successMsg = $this->language->_('Record Saved');
					} catch (Exception $e) {
						$this->_db->rollBack();
					}
				} else if (count($selectmaster) > 0) {
					$this->view->error 			= true;
					$errors 							= "$user_id " . $this->language->_('telah didaftarkan.') . " ";
					$this->view->errorMsg 		= $errors;
				} else if (count($selecttemp) > 0) {
					$this->view->error 			= true;
					$errors 							= "$user_id " . $this->language->_('telah diajukan dan menunggu persetujuan.') . "";
					$this->view->errorMsg 		= $errors;
				}
			} else {

				$this->view->error = true;
				if (!$validLDAP) {
					$docErr = $error_LDAP;
				}

				$this->view->errorMsg = $docErr;

				if ($cekkombinasi == false) {
					$err[] = array($this->language->_('Invalid user id format'));
					$this->view->xuserId = $this->displayError($err);
				}

				/*
				if (isset($cek_multiple_email) && $cek_multiple_email == false) {
					$errr[] = array($this->language->_('Invalid email format'));
					$this->view->xemail = $this->displayError($errr);
				}
				*/
				//var_dump($zf_filter_input->getMessages());
				foreach ($zf_filter_input->getMessages() as $key => $err) {
					$xxx = 'x' . $key;
					$this->view->$xxx = $this->displayError($err);
				}

				$this->view->userId 	= ($zf_filter_input->isValid('userId')) ? $zf_filter_input->userId : $params['userId'];
				$this->view->name	 	= ($zf_filter_input->isValid('name')) ? $zf_filter_input->name : $params['name'];
				$this->view->desc 		= $params['desc'];
				$this->view->email	 	= ($zf_filter_input->isValid('email')) ? $zf_filter_input->email . "@btn.co.id" : $params['email'];
				$this->view->nik	 	= ($zf_filter_input->isValid('nik')) ? $zf_filter_input->nik : $params['nik'];
				$this->view->groupId	= ($zf_filter_input->isValid('groupId')) ? $zf_filter_input->groupId : $params['groupId'];
				$this->view->branch	= ($zf_filter_input->isValid('branch')) ? $zf_filter_input->branch : $params['branch'];
				//				echo $docErr,die;
			}





			Application_Helper_General::writeLog('ADAD', 'Add Backend User (' . $zf_filter_input->userId . ')');
		} else {
			Application_Helper_General::writeLog('ADAD', 'Viewing Add Backend User page');
		}

		$desc 		 = $this->_request->getParam('desc');
		$desc_len 	 = (isset($desc))  ? strlen($desc)  : 0;
		$desc_len 	 = 200 - $desc_len;

		$this->view->desc_len		= $desc_len;
	}
}
