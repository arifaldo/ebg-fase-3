<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';
require_once 'General/CustomerUser.php';

class singlepayment_ImportController extends Application_Main
{
	protected 	$_moduleDB 	= 'RTF';
	protected	$_rowNum 	= 0;
	protected	$_errmsg 	= null;
	protected	$params		= null;

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		if($this->_request->isPost() )
		{
			$adapter1 					= new Zend_File_Transfer_Adapter_Http();
			$params 					= $this->_request->getParams();
			$sourceFileName 			= $adapter1->getFileName();

			if($sourceFileName == null)
			{
				$sourceFileName = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter1->getFileName()), 0);
			}

			$paramsName['sourceFileName'] 	= $sourceFileName;

			$filtersName = array(
									'sourceFileName'		=> array('StringTrim'),
								);

			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File size too big or left blank"
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);

			if($zf_filter_input_name->isValid())
			{
				$filter 	= new Application_Filtering();
				$adapter 	= new Zend_File_Transfer_Adapter_Http ();
// 				print_r($this->_custIdLogin);
// 				print_r($this->_userIdLogin);die;
				$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
				$max		= $this->getSetting('max_import_single_payment');

				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
													'Error: Extension file must be *.csv'
												);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
											'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
										);

				$adapter->setValidators(array($extensionValidator,$sizeValidator));

				if ($adapter->isValid ())
				{
					$sourceFileName = $adapter->getFileName();
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						$Csv = new Application_Csv ($newFileName,",");
						$csvData = $Csv->readAll ();

						@unlink($newFileName);

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}

						if ($totalRecords){
							if($totalRecords <= $max)
							{
								$no =0;
								// var_dump($csvData); die;
								foreach ( $csvData as $columns )
								{
									if(count($columns)==10)
									{
										$params['PAYMENT SUBJECT'] 			= trim($columns[0]);
										$params['SOURCE ACCT NO'] 			= trim($columns[1]);
										$params['BENEFICIARY ACCT NO'] 		= trim($columns[2]);
//										$params['BENEFICIARY NAME'] 		= trim($columns[3]);
										$params['CCY'] 						= trim($columns[3]);
										$params['AMOUNT'] 					= trim($columns[4]);
										$params['MESSAGE'] 					= trim($columns[5]);
										$params['ADDITIONAL MESSAGE'] 		= trim($columns[6]);
//										$params['EMAIL ADDRESS'] 			= trim($columns[8]);
										$params['PAYMENT DATE'] 			= trim($columns[7]);
										$params['TRANSFER TYPE'] 			= trim($columns[8]);
										$params['BANK CODE'] 				= trim($columns[9]);
//										$params['BANK NAME'] 				= trim($columns[12]);
										// 									$params['BANK CITY'] 				= trim($columns[13]);
//										$params['CITIZENSHIP'] 				= trim($columns[13]);

										$PS_SUBJECT 		= $filter->filter($params['PAYMENT SUBJECT'],"PS_SUBJECT");
										$PS_EFDATE 			= $filter->filter($params['PAYMENT DATE'],"PS_DATE");
										$TRA_AMOUNT 		= $filter->filter($params['AMOUNT'],"AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($params['ADDITIONAL MESSAGE'],"TRA_REFNO");
										$ACCTSRC 			= $filter->filter($params['SOURCE ACCT NO'],"ACCOUNT_NO");
										$ACBENEF 			= $filter->filter($params['BENEFICIARY ACCT NO'],"ACCOUNT_NO");
										$ACBENEF_BANKNAME 	= $filter->filter($params['BENEFICIARY NAME'],"ACCOUNT_NAME");
//										$ACBENEF_ALIAS 		= $filter->filter($params['BENEFICIARY NAME'],"ACCOUNT_ALIAS");
//										$ACBENEF_EMAIL 		= $filter->filter($params['EMAIL ADDRESS'],"EMAIL");
										$ACBENEF_CCY 		= $filter->filter($params['CCY'],"SELECTION");
//										$ACBENEF_CITIZENSHIP= $filter->filter($params['CITIZENSHIP'], "SELECTION");
										// 									$BANK_CITY			= $filter->filter($params['BANK CITY'], "ADDRESS");
										$CLR_CODE			= $filter->filter($params['BANK CODE'], "BANK_CODE");
										$TRANSFER_TYPE 		= $filter->filter($params['TRANSFER TYPE'], "SELECTION");
//										$BANK_NAME 			= $filter->filter($params['BANK NAME'],"BANK_NAME");

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										if($TRANSFER_TYPE == 'RTGS'){
											$chargeType = '1';
											$select = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType);
											$resultSelecet = $this->_db->FetchAll($select);
											$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

											//$param['TRANSFER_FEE'] = $chargeAmt;
										}
										else if($TRANSFER_TYPE == 'SKN'){
											$chargeType1 = '2';
											$select1 = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType1);
											$resultSelecet1 = $this->_db->FetchAll($select1);
											$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

											//$param['TRANSFER_FEE'] = $chargeAmt1;
										}
										else{
											$chargeAmt = '0';
											//$param['TRANSFER_FEE'] = $chargeAmt2;
										}


										$paramPayment = array(
												"CATEGORY" 					=> "SINGLE PAYMENT",
												"FROM" 						=> "I",				// F: Form, I: Import
												"PS_NUMBER"					=> "",
												"PS_SUBJECT"				=> $PS_SUBJECT,
												"PS_EFDATE"					=> $PS_EFDATE,
												"_dateFormat"				=> $this->_dateUploadFormat,
												"_dateDBFormat"				=> $this->_dateDBFormat,
												"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
												"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
												"_createPB"					=> $this->view->hasPrivilege('CRIP'),								// cannot create PB trx
												"_createDOM"				=> $this->view->hasPrivilege('CRDI'),	// privi CDFT (Create Domestic Fund Transfer)
												"_createREM"				=> $this->view->hasPrivilege('CRIR'),								// cannot create REM trx
										);

										$paramTrxArr[0] = array(
												"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
												"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
												"TRANSFER_FEE" 				=> $chargeAmt,
												"TRA_MESSAGE" 				=> $TRA_MESSAGE,
												"TRA_REFNO" 				=> $TRA_REFNO,
												"ACCTSRC" 					=> $ACCTSRC,
												"ACBENEF" 					=> $ACBENEF,
												"ACBENEF_CCY" 				=> $ACBENEF_CCY,
												"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
												"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
//												"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
												"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
												// 																"ACBENEF_ADDRESS1" 			=> $BANK_CITY,
//												"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,
												"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
												"BANK_CODE" 				=> $CLR_CODE,
//												"BENEFICIARY_BANK_NAME"		=> $BANK_NAME,
//												"LLD_IDENTICAL" 			=> "",
//												"LLD_CATEGORY" 				=> "",
//												"LLD_RELATIONSHIP" 			=> "",
//												"LLD_PURPOSE" 				=> "",
//												"LLD_DESCRIPTION" 			=> "",
												"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
												"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
												"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
												"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
												"BANK_NAME" 	=> $BANK_NAME,

										);

										$arr[$no]['paramPayment'] = $paramPayment;
										$arr[$no]['paramTrxArr'] = $paramTrxArr;
									}
									else
									{
										$this->view->error 		= true;
										break;
									}
									$no++;
								}

								if(!$this->view->error)
								{
									$resWs = array();
									$err 	= array();

									$resultVal	= $validate->checkCreate($arr, $resWs);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();

									//Zend_Debug::dump($resWs);die;
									$i = 0;
									foreach($resWs as $key=>$dataAcctType){
										//Zend_Debug::dump($dataAcctType);
										$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
									}


									//die;

									$sourceAccountType 	= $resWs['accountType'];

									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
									$content['sourceAccountType'] 	= $sourceAccountType;

									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;

									$this->_redirect('/singlepayment/import/confirm');
								}

							}
							else
							{
								$this->view->error2 = true;
								$this->view->max 	= $max;
							}
						}else{
							$this->view->error = true;
						}
					}
				}
				else
				{
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}
			}
			else
			{
				$this->view->error3		= true;
			}
		}
		Application_Helper_General::writeLog('CRDI','Viewing Create Single Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CRIP','Viewing Create Single Payment In House by Import File (CSV)');
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
		$data = $sessionNamespace->content;

		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}

		// var_dump($data); die;
		if(!$data["payment"]["countTrxCCY"])
		{
			$this->_redirect("/authorizationacl/index/nodata");
		}

		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];

		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			// var_dump($data["payment"]["countTrxCCY"]); die;
			foreach($row as $ccy)
			{
				// $totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;

		$fields = array(
						'PaymentType'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Type'),
														'sortable' => false
													),
						'CCY'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('CCY'),
														'sortable' => false
													),
						'Payment Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Success'),
														'sortable' => false
													),
						'Total Amount Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Success'),
														'sortable' => false
													),
						'Payment Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Failed'),
														'sortable' => false
													),
						'Total Amount Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Failed'),
														'sortable' => false
													),
                        );
		$this->view->fields = $fields;

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmImportCredit']);
				$this->_redirect('/newmultibulk/');
			}


			if($this->_custSameUser){
				
				if(!$this->view->hasPrivilege('PRLP')){
					// die('here');
					$error_msg[] = $this->language->_("Error: You don't have privilege to release payment");
					$this->view->error = true;
					$this->view->report_msg = $this->displayError($error_msg);

					$checktoken = false;

				}else{
					// die('sini');

					$challengeCode		= $this->_getParam('challengeCode');

					$inputtoken1 		= $this->_getParam('inputtoken1');
					$inputtoken2 		= $this->_getParam('inputtoken2');
					$inputtoken3 		= $this->_getParam('inputtoken3');
					$inputtoken4 		= $this->_getParam('inputtoken4');
					$inputtoken5 		= $this->_getParam('inputtoken5');
					$inputtoken6 		= $this->_getParam('inputtoken6');

					$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;

					$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$verToken 	= $Token->verify($challengeCode, $responseCode);
					// print_r($verToken);
					// die('here');
					if ($verToken['ResponseCode'] != '00'){
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

						if ($tokenFailed === true)
						{
							$this->_redirect('/default/index/logout');
						}

						$checktoken = false;

					}else{

						$checktoken = true;
					}
				}
			} else{

				$checktoken = true;
			}

			if($checktoken){

			$err = 0;
			foreach($data["arr"] as $row)
			{
				$param['PS_SUBJECT'] 				= $row['paramPayment']['PS_SUBJECT'];
				$param['PS_EFDATE']  				= Application_Helper_General::convertDate($row['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateUploadFormat);
				$param['PS_TYPE'] 					= $this->_paymenttype['code']['bulkcredit'];
				$param['PS_CCY']  					= $row['paramTrxArr'][0]['ACBENEF_CCY'];
				$param['SOURCE_ACCOUNT']			= $row['paramTrxArr'][0]['ACCTSRC'];
				$param['BENEFICIARY_ACCOUNT'] 		= $row['paramTrxArr'][0]['ACBENEF'];
				$param['BENEFICIARY_ACCOUNT_CCY'] 	= $row['paramTrxArr'][0]['ACBENEF_CCY'];
				$param['BENEFICIARY_ACCOUNT_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_BANKNAME'];
//				$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_ALIAS'];
				$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_BANKNAME'];
				$param['BENEFICIARY_EMAIL'] 		= $row['paramTrxArr'][0]['ACBENEF_EMAIL'];
				$param['BENEFICIARY_ADDRESS'] 		= $row['paramTrxArr'][0]['ACBENEF_ADDRESS1'];
				$param['BENEFICIARY_CITIZENSHIP'] 	= $row['paramTrxArr'][0]['ACBENEF_CITIZENSHIP'];
				$param['CLR_CODE'] 					= $row['paramTrxArr'][0]['BANK_CODE'];
//				$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BENEFICIARY_BANK_NAME'];
				$param['TRANSFER_TYPE'] 			= $row['paramTrxArr'][0]['TRANSFER_TYPE'];
				$param['TRA_AMOUNT'] 				= $row['paramTrxArr'][0]['TRA_AMOUNT'];
				$param['TRANSFER_FEE'] 				= $row['paramTrxArr'][0]['TRANSFER_FEE'];
				$param['TRA_MESSAGE'] 				= $row['paramTrxArr'][0]['TRA_MESSAGE'];
				$param['TRA_REFNO'] 				= $row['paramTrxArr'][0]['TRA_REFNO'];
				$param['_addBeneficiary'] 			= $row['paramPayment']['_addBeneficiary'];
				$param['_beneLinkage'] 				= $row['paramPayment']['_beneLinkage'];
				$param["_dateFormat"]				= $row['paramPayment']['_dateFormat'];
				$param["_dateDBFormat"]				= $row['paramPayment']['_dateDBFormat'];
				$param["_createPB"]					= $row['paramPayment']['_createPB'];
				$param["_createDOM"]				= $row['paramPayment']['_createDOM'];
				$param["_createREM"]				= $row['paramPayment']['_createREM'];
				$param["sourceAccountType"]			= $row['paramTrxArr'][0]['ACCOUNT_TYPE'];

				$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BANK_NAME'];
				$param['LLD_CATEGORY'] 				= $row['paramTrxArr'][0]['BENEFICIARY_CATEGORY'];
				$param['CITY_CODE'] 				= $row['paramTrxArr'][0]['BENEFICIARY_CITY_CODE'];
				$param['LLD_BENEIDENTIF'] 			= $row['paramTrxArr'][0]['BENEFICIARY_ID_TYPE'];
				$param['LLD_BENENUMBER'] 			= $row['paramTrxArr'][0]['BENEFICIARY_ID_NUMBER'];

				try
				{
					$SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);
					$result = $SinglePayment->createPayment($param);

					// var_dump($result);die;


					if($this->_custSameUser){

											$paramSQL = array("WA" 				=> false,
															  "ACCOUNT_LIST" 	=> $this->_accountList,
															  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
															 );

											// get payment query
											$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
											$select   = $CustUser->getPayment($paramSQL);
											$select->where('P.PS_NUMBER = ?' , (string) $result);
											// echo $select;
											$pslip = $this->_db->fetchRow($select);
											$settingObj = new Settings();
											$setting = array("COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
															 "COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
															 "COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
															 "COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
															 "COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
															 'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
															 "range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
															 "auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
															 "_dateFormat" 			=> $this->_dateDisplayFormat,
															 "_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
															 "_transfertype" 		=> array_flip($this->_transfertype["code"]),
															);

											$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
											$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

											$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
											$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

											$app = Zend_Registry::get('config');
											$appBankname = $app['app']['bankname'];

											$selectTrx = $this->_db->select()
											  ->from(	array(	'TT' => 'T_TRANSACTION'),
														array(
																'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
																'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
																'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
																//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
																'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
																'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
																'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
																'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
																'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
																'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
																'TRA_REFNO'				=> 'TT.TRA_REFNO',
																'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
																'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
																'TRA_STATUS'			=> 'TT.TRA_STATUS',
																'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
																'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
																'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
																'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
																'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'CLR_CODE'				=> 'TT.CLR_CODE',
																'TT.RATE',
																'TT.PROVISION_FEE',
																'TT.NOSTRO_NAME',
																'TT.FULL_AMOUNT_FEE',
																'C.PS_CCY','C.CUST_ID',
																'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
																'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
																'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$appBankname."'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('".$appBankname."',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('".$appBankname."',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
																'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
					        																FROM T_PERIODIC_DETAIL Y
					        																inner join T_PSLIP Z
					        																on Y.PS_PERIODIC = Z.PS_PERIODIC
					        																where
					        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
															  )
														)
												->joinLeft(	array(	'C' => 'T_PSLIP' ),'C.PS_NUMBER = TT.PS_NUMBER',array())
												->where('TT.PS_NUMBER = ?', $result);
							// echo $selectTrx;
												$paramTrxArr = $this->_db->fetchAll($selectTrx);

												$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $result);
												$paramPayment = array_merge($pslip, $setting);
												// echo '<pre>';
												// print_r($paramPayment);
												// print_r($paramTrxArr);
												// die;
												$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
												$infoWarnOri = (!empty($check['infoWarning'])?'*) '.$check['infoWarning']:'');
												$sessionNameConfrim->infoWarnOri = $infoWarnOri;

												if($validate->isError() === true)
												{
													$error = true;
													$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
													$err++;
												}

												$Payment = new Payment($result, $this->_custIdLogin, $this->_userIdLogin);
												// if ($this->_hasPriviReleasePayment){
													$resultRelease = $Payment->releasePayment();
													// print_r($resultRelease);
													$this->view->ps_numb = $result;
													$this->view->hidetoken = true;
													if ($resultRelease['status'] == '00'){
														
														// $this->_redirect('/notification/success/index');
													}
													else
													{
														$err++;
													}
												// }
										}



				}
				catch(Exception $e)
				{
					Application_Helper_General::exceptionLog($e);
					$errr = 1;
				}
			}
			unset($sessionNamespace->content);

			$this->_helper->getHelper('FlashMessenger')->addMessage('/'.$this->view->modulename.'/'.$this->view->controllername.'/index');

			if ($errr != 1)
			{	$this->_redirect('/notification/success/index');	}
			else
			{	$this->_redirect('/notification/success/index');	}	// TODO: what to do, if failed create payment


			}else{
				$this->view->error = true;
				$error_msg[0] = 'Error: Invalid Token';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/singlepayment/import/confirm');
			}

		}
	}

}
