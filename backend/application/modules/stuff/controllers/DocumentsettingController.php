<?php

class stuff_DocumentsettingController extends Application_Main
{
    public function indexAction() 
    { 
			
		$listActor = array(  'S' => 'Supplier',
										  'B' => 'Buyer',
										  'O' => 'Supplier Or Buyer'
										 );
	
		$listOnOff = array('0' => 'OFF',												
									    '1' => 'ON'
									  );	
									  
		$listYesNo = array('N' => 'No',
										'Y' => 'Yes',
										);
										
									
		$this->view->listActor = $listActor;
		$this->view->listOnOff = $listOnOff;	
		$this->view->listYesNo = $listYesNo;			
    }
}