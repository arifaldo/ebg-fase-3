<?php

require_once 'Zend/Controller/Action.php';

class group_ActivateController extends group_Model_Group
{
    public function indexAction() 
    {
		//build validator chains
		$filters = array('group_code' => array('StringTrim'));
							 
		$validators =  array(
						    'group_code' => array('NotEmpty', //tdk berfungsi
											      array('Db_RecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_ID')),
											      array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_ID')), 
											      'messages' => array(
													$this->getErrorRemark('01','GROUP ID'), //set error message for group_code
													$this->getErrorRemark('22','GROUP ID'),
													$this->getErrorRemark('03','GROUP ID'),
												                     ) //set error message for 2nd validator (Db_NoRecordExists)													   					   )
											     ),
						   );

		// utk mengecek apakah parameter yg dikirim adalah group_code, jika tdk maka proses tdk dilakukan
	    // pengganti validator NotEmpty, krn tdk berfungsi					   
		if(array_key_exists('group_code',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());
			
			if($zf_filter_input->isValid()) 
			{
				try
				{
					$this->_db->beginTransaction();
					$groupCode = ($zf_filter_input->group_code)? $zf_filter_input->group_code : $this->_request->getParam('group_code'); 
						
					$group = $this->getGroup($groupCode);
                    			
					if(is_array($group)) $groupName = $group['BGROUP_DESC'];
						        	
				    $changeType = $this->_changeType['code']['activate'];
					$status     = $this->_masterStatus['code']['active'];
					
					// insert ke T_GLOBAL_CHANGES
					$changesId  = $this->suggestionWaitingApproval('Backend Group',"Group ID = $groupCode, Group Name = $groupName",$changeType,'','M_BGROUP','TEMP_BGROUP','BGROUP_ID',$groupCode);
                    $this->insertTempGroup($groupCode,$groupName,$changesId,$status);	
						
					$msg_success = "Group ID : $groupCode Request to deactivate";
                    $this->view->msg_success = ($msg_success !="")? $msg_success : null;
                    
				    $this->_db->commit();
				    
					$this->_helper->getHelper('FlashMessenger')->addMessage('S');
					$this->_helper->getHelper('FlashMessenger')
								  ->addMessage($this->getErrorRemark('00','Group ID',$groupCode));

								
					$this->_redirect($this->_backURL);
			    }
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					//rethrow exception
					$this->backendLog('A',$this->_moduleDB,$groupCode,null,null);
					Application_Log_GeneralLog::technicalLog($e);
				}
				
			}// END IF IS VALID
			else
			{
				$errors = $zf_filter_input->getMessages();
				$this->backendLog('A',$this->_moduleDB,$groupCode,null,$this->displayErrorRemark($errors));
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					 
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					}					
				}	
				
				$this->_redirect($this->_backURL);
			} 
			
	  }// END IF KEY ARRAY EXISTS 
  }
	

}

