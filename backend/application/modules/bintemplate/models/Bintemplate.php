<?php

class bintemplate_Model_Bintemplate extends Application_Main
{
    protected $_tempData;

    // constructor
    public function initModel()
    {
        $this->_tempData = array(
            'TEMP_NAME'             => null,
            'TEMP_DESC'            => null,
            'BIN_TYPE'          => null,
            'VA_TYPE'            => null,
            'AMOUNT_TYPE'         => null,
            'SKIP_ERROR'             => null,
            'PARTIAL_TYPE'          => null,
            'BILLING_TYPE'            => null,
            'UNIQUE'        => null,
            'CAL_INVOICE'         => null,
            'CAL_DEPO'           => null,
            'BUYER_FEE'         => null,
            'SELLER_FEE'             => null,
            'VP_MIN'             => null,
            'VP_MAX'           => null,
            'VA_CREDIT_MAX'           => null,
            'VA_DAILY_LIMIT'           => null,
            'REMAINING_AVAILABLE_CREDIT'  => null,
            'EXP_TIME'  => null,
            'EX_TIME'  => null,
            'EX_TIME_TYPE'  => null,
            'DUDATE_STATUS'         => null,
            'TEMP_CREATEDBY' => null,
            'TEMP_CREATED'       => null,
            'TEMP_UPDATEDBY'       => null,
            'TEMP_UPDATED'       => null,
            'TEMP_SUGGESTEDBY'       => null,
            'TEMP_SUGGESTED'       => null,
        );
    }
    public function getBinTemplate($temp_code)
    {
        $select = $this->_db->select()
            ->from(array('A' => 'BIN_TEMPLATE'), array('*'))
            ->where('UPPER(A.TEMP_CODE)=' . $this->_db->quote((string) $temp_code))
            ->query()->fetch();
        return $select;
    }
    public function insertTempBinTemplate($change_id, $temp_data)
    {
        $content = array(
            'CHANGES_ID'              => $change_id,
            'TEMP_CODE'                   => $temp_data['TEMP_CODE'],
            'TEMP_NAME'                   => $temp_data['TEMP_NAME'],
            'TEMP_DESC'             => $temp_data['TEMP_DESC'],
            'BIN_TYPE'          => $temp_data['BIN_TYPE'],
            'VA_TYPE'          => $temp_data['VA_TYPE'],

            'EXP_TIME'             => $temp_data['EXP_TIME'],
            'EX_TIME'             => $temp_data['EX_TIME'],
            'EX_TIME_TYPE'             => $temp_data['EX_TIME_TYPE'], 
            'SKIP_ERROR'           => $temp_data['SKIP_ERROR'],
            'PARTIAL_TYPE'             => $temp_data['PARTIAL_TYPE'],
            'BILLING_TYPE'           => $temp_data['BILLING_TYPE'],

            'BUYER_FEE'        => $temp_data['BUYER_FEE'],
            'SELLER_FEE'        => $temp_data['SELLER_FEE'],
            'VP_MIN'        => $temp_data['VP_MIN'],
            'VP_MAX'        => $temp_data['VP_MAX'],

            //va debit
            'VA_CREDIT_MAX'   => $temp_data['VA_CREDIT_MAX'],
            'VA_DAILY_LIMIT'   => $temp_data['VA_DAILY_LIMIT'],
            'REMAINING_AVAILABLE_CREDIT'   => $temp_data['REMAINING_AVAILABLE_CREDIT'],

            'TEMP_SUGGESTED'            => $temp_data['TEMP_SUGGESTED'],
            'TEMP_SUGGESTEDBY'          => $temp_data['TEMP_SUGGESTEDBY'],


            'TEMP_CREATED'   => $temp_data['TEMP_CREATED'],


            'TEMP_CREATEDBY'   => $temp_data['TEMP_CREATEDBY'],

        );



        $this->_db->insert('TEMP_BIN_TEMPLATE', $content);
    }
    //insert for delete
    public function insertTempBinTemplateUpdate($change_id, $temp_data, $temp_code)
    {
        $content = array(
            'CHANGES_ID'              => $change_id,
            'TEMP_CODE'                   => $temp_code, 
            'TEMP_NAME'                   => $temp_data['TEMP_NAME'],
            'TEMP_DESC'             => $temp_data['TEMP_DESC'],
            'BIN_TYPE'          => $temp_data['BIN_TYPE'],
            'VA_TYPE'          => $temp_data['VA_TYPE'],

            'EXP_TIME'             => $temp_data['EXP_TIME'],
            'EX_TIME'             => $temp_data['EX_TIME'],
            'EX_TIME_TYPE'             => $temp_data['EX_TIME_TYPE'], 
            'SKIP_ERROR'           => $temp_data['SKIP_ERROR'],
            'PARTIAL_TYPE'             => $temp_data['PARTIAL_TYPE'],
            'BILLING_TYPE'           => $temp_data['BILLING_TYPE'],

            'BUYER_FEE'        => $temp_data['BUYER_FEE'],
            'SELLER_FEE'        => $temp_data['SELLER_FEE'],
            'VP_MIN'        => $temp_data['VP_MIN'],
            'VP_MAX'        => $temp_data['VP_MAX'],

            //va debit
            'VA_CREDIT_MAX'   => $temp_data['VA_CREDIT_MAX'],
            'VA_DAILY_LIMIT'   => $temp_data['VA_DAILY_LIMIT'],
            'REMAINING_AVAILABLE_CREDIT'   => $temp_data['REMAINING_AVAILABLE_CREDIT'],

            'TEMP_UPDATED'            => $temp_data['TEMP_UPDATED'],
            'TEMP_UPDATEDBY'          => $temp_data['TEMP_UPDATEDBY'],

            'TEMP_SUGGESTED'            => $temp_data['TEMP_SUGGESTED'],
            'TEMP_SUGGESTEDBY'          => $temp_data['TEMP_SUGGESTEDBY'],
            'TEMP_CREATED'   => $temp_data['TEMP_CREATED'],
            'TEMP_CREATEDBY'   => $temp_data['TEMP_CREATEDBY'], 
        );



        $this->_db->insert('TEMP_BIN_TEMPLATE', $content);
    }
    protected function getTempBinTemplate($changeId)
    {
        $select  = $this->_db->fetchRow(
            $this->_db->select()
                ->from(array('T' => 'TEMP_BIN_TEMPLATE'))
                ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS'))
                ->where('T.CHANGES_ID = ?', $changeId)
        );

        return $select;
    }
}
