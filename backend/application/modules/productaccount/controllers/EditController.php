<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class productaccount_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model = new productaccount_Model_Productaccount();

	    //pengaturan url untuk button back
		$this->setbackURL('/'.$this->_request->getModuleName().'/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}


		$this->view->report_msg = array();

		$account_typeArr 	= array_combine($this->_productaccount["code"],$this->_productaccount["desc"]);
		
		$this->view->account_typeArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+$account_typeArr);
		
		$product_id = $this->_getParam('product_id');
		$this->view->product_id        = $product_id;
		$product_id = (Zend_Validate::is($product_id,'Digits'))? $product_id : null;

		if($product_id)
		{
			$temp = $model->getData(array('fproduct_id' =>$product_id));
			if(empty($temp))
			{
				$this->_redirect('/productaccount');
			}
			$resultdata = $temp[0];
			if($resultdata)
			{
				$this->view->product_code        = $resultdata['PRODUCT_CODE'];
				// $this->view->plan_code        = $resultdata['PRODUCT_PLAN'];
				$this->view->product_name      = $resultdata['PRODUCT_NAME'];
				$this->view->account_type      = $resultdata['ACCT_TYPE'];
				/*echo $resultdata['ACCT_TYPE'];*/


				if($this->_request->isPost())
				{
					$filters = array('product_code'       => array('StringTrim','StripTags'),
						'plan_code'       => array('StringTrim','StripTags'),
						'product_name'     => array('StringTrim','StripTags'),
					);
					
					$PRODUCT_CODE = "PRODUCT_CODE <> '".$resultdata['PRODUCT_CODE']."'";
					// $PRODUCT_PLAN = "PRODUCT_PLAN <> '".$resultdata['PRODUCT_PLAN']."'";
					$PRODUCT_NAME = "PRODUCT_NAME <> '".$resultdata['PRODUCT_NAME']."'";
					
					$validators = array('product_code'  => array('NotEmpty',
						//'Digits',
						//new Zend_Validate_StringLength(array('min'=>1,'max'=>2)),
						array	(
							'Db_NoRecordExists',
							array	(
								'table'=>'M_PRODUCT_TYPE',
								'field'=>'PRODUCT_CODE',
								'exclude'=>$PRODUCT_CODE
							)
						),
						'messages' => array(
							$this->language->_('Can not be empty'),
							//$this->language->_('Invalid Format'),
							//$this->language->_('Data too long (min. 1 chars and max. 2 chars)'),
							$this->language->_('Product Code already exists'),
						)
					),
					
					// 'plan_code'  => array('NotEmpty',
					// 	'Digits',
					// 	new Zend_Validate_StringLength(array('min'=>1,'max'=>4)),
					// 	array	(
					// 		'Db_NoRecordExists',
					// 		array	(
					// 			'table'=>'M_PRODUCT_TYPE',
					// 			'field'=>'PRODUCT_PLAN',
					// 			'exclude'=>$PRODUCT_PLAN
					// 		)
					// 	),
					// 	'messages' => array(
					// 		$this->language->_('Can not be empty'),
					// 		$this->language->_('Invalid Format'),
					// 		$this->language->_('Data too long (min. 1 chars and max. 4 chars)'),
					// 		$this->language->_('Plan Code already exists'),
					// 	)
					// ),

					'product_name' => array('NotEmpty',
						new Zend_Validate_Alnum(true),
						new Zend_Validate_StringLength(array('max'=>35)),
						array	(
							'Db_NoRecordExists',
							array	(
								'table'=>'M_PRODUCT_TYPE',
								'field'=>'PRODUCT_NAME',
								'exclude'=>$PRODUCT_NAME
							)
						),
						'messages' => array(
							$this->language->_('Can not be empty'),
							$this->language->_('Invalid Bank Name Format ( no special characters )'),
							$this->language->_('Data too long (max 35 chars)'),
							$this->language->_('Product Name already exists'),
						)
					),
					
				);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{
						$content = array(
							'PRODUCT_NAME'  => $zf_filter_input->product_name,
							// 'PRODUCT_PLAN' 	=> $zf_filter_input->plan_code,
							'PRODUCT_CODE' 	=> $zf_filter_input->product_code,
							'ACCT_TYPE'  	=> '1'
						);

						try
						{

							//-----insert--------------
							$this->_db->beginTransaction();

							$model->updateData($product_id,$content);

							$this->_db->commit();

							$this->fillParam($zf_filter_input);
							$this->_redirect('/notification/success/index');
						}
						catch(Exception $e)
						{
							//rollback changes
							$this->_db->rollBack();

							$this->fillParam($zf_filter_input);

							$errorMsg = 'exception';
							$this->_helper->getHelper('FlashMessenger')->addMessage('F');
							$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						}
						Application_Helper_General::writeLog('PAIM','Edit Product Account Type,Product ID : '.$product_id);
						
					}
					else
					{
						$this->view->error = true;
						$this->fillParam($zf_filter_input);
						$error = $zf_filter_input->getMessages();

						//format error utk ditampilkan di view html
						foreach($error as $keyRoot => $rowError)
						{
							foreach($rowError as $errorString)
							{
								$errID = 'error_'.$keyRoot;
								$this->view->$errID = $errorString;
							}
						}
						$this->view->error = true;
					}
				}
			}
		}
		else
		{
			$err = $this->language->_('No Data');
		}
		Application_Helper_General::writeLog('PAIM','Edit Product Account Type,Product ID : '.$product_id);
	}


	private function fillParam($zf_filter_input)
	{
		$this->view->product_code = ($zf_filter_input->isValid('product_code')) ? $zf_filter_input->product_code : $this->_getParam('product_code');
		$this->view->plan_code = ($zf_filter_input->isValid('plan_code')) ? $zf_filter_input->plan_code : $this->_getParam('plan_code');
		$this->view->product_name     = ($zf_filter_input->isValid('product_name')) ? $zf_filter_input->product_name : $this->_getParam('product_name');
		$this->view->account_type     = ($zf_filter_input->isValid('account_type')) ? $zf_filter_input->account_type : $this->_getParam('account_type');
	}



}