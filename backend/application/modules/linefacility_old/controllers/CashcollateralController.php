<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class linefacility_CashCollateralController extends Application_Main
{
    public function indexAction()
    {
        if ($this->_request->isPost()) {

            $getData = $this->_request->getPost();
            // biaya umum ----------------------------
            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "MINPROV01",
                "CHARGES_NAME" => "GLOBAL MINIMUM PROVISI",
                "CHARGES_CCY" => "IDR",
                "CHARGES_AMT" => str_replace(",", "", $getData["MINPROV01"]),
                "ACTIVE" => "Y"
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "METERAI01",
                "CHARGES_NAME" => "GLOBAL METERAI",
                "CHARGES_CCY" => "IDR",
                "CHARGES_AMT" => str_replace(",", "", $getData["METERAI01"]),
                "ACTIVE" => "Y"
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "ADM01",
                "CHARGES_NAME" => "GLOBAL ADMINISTRASI",
                "CHARGES_CCY" => "IDR",
                "CHARGES_AMT" => str_replace(",", "", $getData["ADM01"]),
                "ACTIVE" => "Y"
            ]);
            // --------------------------------------

            // biaya kontra garansi fullcover ------------------------

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "CASHCOL001",
                "CHARGES_NAME" => "GLOBAL CASH COLLATERAL",
                "TENOR_FROM" => str_replace(",", "", $getData["CASHCOL001"])[0],
                "TENOR_TO" => str_replace(",", "", $getData["CASHCOL001"])[1],
                "CHARGES_PCT" => str_replace(",", "", $getData["CASHCOL001"])[2],
                "ACTIVE" => "Y"
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "CASHCOL002",
                "CHARGES_NAME" => "GLOBAL CASH COLLATERAL",
                "TENOR_FROM" => str_replace(",", "", $getData["CASHCOL002"])[0],
                "TENOR_TO" => str_replace(",", "", $getData["CASHCOL002"])[1],
                "CHARGES_PCT" => str_replace(",", "", $getData["CASHCOL002"])[2],
                "ACTIVE" => "Y"
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "CASHCOL003",
                "CHARGES_NAME" => "GLOBAL CASH COLLATERAL",
                "TENOR_FROM" => str_replace(",", "", $getData["CASHCOL003"])[0],
                "TENOR_TO" => str_replace(",", "", $getData["CASHCOL003"])[1],
                "CHARGES_PCT" => str_replace(",", "", $getData["CASHCOL003"])[2],
                "ACTIVE" => "Y"
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "CASHCOL004",
                "CHARGES_NAME" => "GLOBAL CASH COLLATERAL",
                "TENOR_FROM" => str_replace(",", "", $getData["CASHCOL004"])[0],
                "TENOR_TO" => str_replace(",", "", $getData["CASHCOL004"])[1],
                "CHARGES_PCT" => str_replace(",", "", $getData["CASHCOL004"])[2],
                "ACTIVE" => "Y"
            ]);

            // -------------------------------------------------------

            // biaya kontra garansi lf debitur ---------------------------------------

            // bumn ----------------------------------
            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFBUMN01",
                "CHARGES_NAME" => "LF JAMINAN PEMBAYARAN GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFBUMN01"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF01"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFBUMN02",
                "CHARGES_NAME" => "LF JAMINAN PENAWARAN GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFBUMN02"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF02"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFBUMN03",
                "CHARGES_NAME" => "LF JAMINAN PELAKSANAAN GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFBUMN03"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF03"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFBUMN04",
                "CHARGES_NAME" => "LF JAMINAN UANG MUKA GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFBUMN04"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF04"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFBUMN05",
                "CHARGES_NAME" => "LF JAMINAN PEMELIHARAAN GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFBUMN05"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF05"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFBUMN06",
                "CHARGES_NAME" => "LF JAMINAN SP2D GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFBUMN06"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF06"]) ? "Y" : "N")
            ]);
            // ---------------------------------------------------------

            // non-bumn ----------------------------------
            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFNONBUMN01",
                "CHARGES_NAME" => "LF JAMINAN PEMBAYARAN GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFNONBUMN01"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF01"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFNONBUMN02",
                "CHARGES_NAME" => "LF JAMINAN PENAWARAN GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFNONBUMN02"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF02"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFNONBUMN03",
                "CHARGES_NAME" => "LF JAMINAN PELAKSANAAN GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFNONBUMN03"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF03"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFNONBUMN04",
                "CHARGES_NAME" => "LF JAMINAN UANG MUKA GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFNONBUMN04"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF04"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFNONBUMN05",
                "CHARGES_NAME" => "LF JAMINAN PEMELIHARAAN GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFNONBUMN05"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF05"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "LFNONBUMN06",
                "CHARGES_NAME" => "LF JAMINAN SP2D GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["LFNONBUMN06"]),
                "ACTIVE" => (str_replace(",", "", $getData["LF06"]) ? "Y" : "N")
            ]);
            // ---------------------------------------------------------

            // -----------------------------------------------------------------------

            // biaya kontra garansi asuransi ---------------------------------------

            // bumn ----------------------------------
            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSBUMN01",
                "CHARGES_NAME" => "ASURANSI JAMINAN PEMBAYARAN GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSBUMN01"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS01"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSBUMN02",
                "CHARGES_NAME" => "ASURANSI JAMINAN PENAWARAN GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSBUMN02"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS02"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSBUMN03",
                "CHARGES_NAME" => "ASURANSI JAMINAN PELAKSANAAN GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSBUMN03"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS03"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSBUMN04",
                "CHARGES_NAME" => "ASURANSI JAMINAN UANG MUKA GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSBUMN04"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS04"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSBUMN05",
                "CHARGES_NAME" => "ASURANSI JAMINAN PEMELIHARAAN GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSBUMN05"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS05"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSBUMN06",
                "CHARGES_NAME" => "ASURANSI JAMINAN SP2D GROUP BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSBUMN06"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS06"]) ? "Y" : "N")
            ]);
            // ---------------------------------------------------------

            // non-bumn ----------------------------------
            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSNONBUMN01",
                "CHARGES_NAME" => "ASURANSI JAMINAN PEMBAYARAN GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSNONBUMN01"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS01"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSNONBUMN02",
                "CHARGES_NAME" => "ASURANSI JAMINAN PENAWARAN GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSNONBUMN02"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS02"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSNONBUMN03",
                "CHARGES_NAME" => "ASURANSI JAMINAN PELAKSANAAN GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSNONBUMN03"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS03"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSNONBUMN04",
                "CHARGES_NAME" => "ASURANSI JAMINAN UANG MUKA GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSNONBUMN04"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS04"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSNONBUMN05",
                "CHARGES_NAME" => "ASURANSI JAMINAN PEMELIHARAAN GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSNONBUMN05"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS05"]) ? "Y" : "N")
            ]);

            $this->_db->insert("TEMP_CHARGES_BG", [
                "CHARGES_ID" => "INSNONBUMN06",
                "CHARGES_NAME" => "ASURANSI JAMINAN SP2D GROUP NON BUMN",
                "CHARGES_PCT" => str_replace(",", "", $getData["INSNONBUMN06"]),
                "ACTIVE" => (str_replace(",", "", $getData["INS06"]) ? "Y" : "N")
            ]);
            // ---------------------------------------------------------

            // -----------------------------------------------------------------------

            $change_id = $this->suggestionWaitingApproval('BG Charges', $info, $this->_changeType['code']['new'], null, 'T_CHARGES_OTHER', 'TEMP_CHARGES_OTHER', 'BG Charges', 'BG Charges', '-', '-');

            Application_Helper_General::writeLog('UCSC', 'Update BG Charges');

            // $checker = $this->_db->select()
            //     ->from(["A" => "TEMP_CHARGES_OTHER"], ["*"])
            //     ->where("CUST_ID = 'GLOBAL' AND CHARGES_TYPE = 10")
            //     ->query()->fetchAll();

            // if (count($checker) != 0) {
            //     return $this->_redirect('/companycharges/cashcollateral?alert=1');;
            // }
            // $getAllValue = $this->_request->getParams();

            // $info = 'Cash Collateral';

            // $insertData = [
            //     "CHANGES_ID" => $change_id,
            //     "CHARGES_SUGGESTED" => new Zend_Db_Expr('now()'),
            //     "CHARGES_SUGGESTEDBY" => $this->_userIdLogin,
            //     "CHARGES_TYPE" => 10,
            //     "CHARGES_CCY" => "IDR",
            //     "CHARGES_AMT" => null,
            //     "CUST_ID" => "GLOBAL",
            //     "CHARGES_ADM" => Application_Helper_General::convertDisplayMoney($getAllValue["bank_administration_fee"]),
            //     "CHARGES_STAMP" => Application_Helper_General::convertDisplayMoney($getAllValue["bank_stamp_fee"]),
            //     "CHARGES_PCT" => Application_Helper_General::convertDisplayMoney($getAllValue["bank_provision_fee"])
            // ];

            // $this->_db->beginTransaction();
            // $this->_db->insert("TEMP_CHARGES_OTHER", $insertData);
            // $this->_db->commit();

            // $this->_db->update("T_GLOBAL_CHANGES", ["MODULE = ? " => "linefacility"], ["CHANGES_ID = ?" => $insertData["CHANGES_ID"]]);

            $this->setbackURL('/companycharges/cashcollateral/');
            $this->_redirect('/notification/submited/index');
        }
    }

    public function updateAction()
    {
        if ($this->_request->isPost()) {
            $getAllParam = $this->_request->getParams();

            // $this->_db->delete("M_CHARGES_OTHER", [
            //     "CUST_ID = ?" => "GLOBAL",
            //     "CHARGES_TYPE" => "10"
            // ]);

            $change_id = $this->suggestionWaitingApproval('Cash Collateral', "Cash Collateral", $this->_changeType['code']['new'], null, 'T_CHARGES_OTHER', 'TEMP_CHARGES_OTHER', '-', '-', '-', '-');

            $insertData = [
                "CHANGES_ID" => $change_id,
                "CHARGES_SUGGESTED" => new Zend_Db_Expr('now()'),
                "CHARGES_SUGGESTEDBY" => $this->_userIdLogin,
                "CHARGES_TYPE" => 10,
                "CHARGES_CCY" => "IDR",
                "CHARGES_AMT" => null,
                "CUST_ID" => "GLOBAL",
                "CHARGES_ADM" => Application_Helper_General::convertDisplayMoney($getAllParam["bank_administration_fee"]),
                "CHARGES_STAMP" => Application_Helper_General::convertDisplayMoney($getAllParam["bank_stamp_fee"]),
                "CHARGES_PCT" => Application_Helper_General::convertDisplayMoney($getAllParam["bank_provision_fee"])
            ];

            $this->_db->beginTransaction();
            $this->_db->insert("TEMP_CHARGES_OTHER", $insertData);
            $this->_db->commit();

            $this->setbackURL('/' . $this->_request->getModuleName() . '/cashcollateral/update');
            $this->_redirect('/notification/submited/index');
        }
    }
}
