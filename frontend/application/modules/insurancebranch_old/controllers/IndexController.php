<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Insurancebranch_IndexController extends Application_Main
{
	
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}	
		}

		$model = new insurancebranch_Model_Insurancebranch();
// 		$conf 		= Zend_Registry::get('config');
		
		$fields = array(
			'insurance_branch_name'      => array('field' => 'INS_BRANCH_NAME',
				'label' => $this->language->_('Branch'),
				'sortable' => true),
			'insurance_branch_email'      => array('field' => 'INS_BRANCH_EMAIL',
				'label' => $this->language->_('Branch Email'),
				'sortable' => true),
			'insurance_branch_acct'   => array('field' => 'INS_BRANCH_ACCT',
				'label' => $this->language->_('Branch Rekening'),
				'sortable' => true),
			'insurance_branch_ccy'   => array('field' => 'INS_BRANCH_CCY',
				'label' => $this->language->_('Currency'),
				'sortable' => true),						  
			'insurance_branch_type'   => array('field' => 'ACCT_TYPE',
				'label' => $this->language->_('Type'),
				'sortable' => true),
			'last_approve'           => array('field' => 'LAST_APPROVED',
				'label' => $this->language->_('Last Approved'),
				'sortable' => false),
		);
		
		$filterlist = array("INS_BRANCH_NAME","INS_BRANCH_EMAIL","INS_BRANCH_ACCT","ACCT_TYPE");
		
		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','product_code');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
			array(array_keys($fields))
		))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
			array('haystack'=>array('asc','desc'))
		))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
			'INS_BRANCH_NAME'  => array('StringTrim','StripTags'),
			'INS_BRANCH_EMAIL'  => array('StringTrim','StripTags'),
			'INS_BRANCH_ACCT'      => array('StringTrim','StripTags','StringToUpper'),
			'ACCT_TYPE'      => array('StringTrim','StripTags','StringToUpper'),
		);
		

		$dataParam = array("INS_BRANCH_NAME","INS_BRANCH_EMAIL","INS_BRANCH_ACCT","ACCT_TYPE");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if($dtParam==$value){
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
				
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		// print_r($dataParamValue);die;
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		// $csv = $this->_getParam('csv');
		// $pdf = $this->_getParam('pdf');	
		$custId = $this->_custIdLogin;
		$fParam = array();
		if($filter == $this->language->_('Set Filter'))
		{
			// die;
			$fParam['fbranch_name'] 		= $zf_filter->getEscaped('INS_BRANCH_NAME');
			$fParam['fbranch_email'] 		= $zf_filter->getEscaped('INS_BRANCH_EMAIL');
			$fParam['fbranch_acct']     	= $zf_filter->getEscaped('INS_BRANCH_ACCT');
			$fParam['fbranch_acct_type']     	= $zf_filter->getEscaped('ACCT_TYPE');
		}
		else if($filter == $this->language->_('Clear Filter') || $filter == '')
		{
			$fParam['fbranch_name'] = '';
			$fParam['fbranch_email'] = '';
			$fParam['fbranch_acct'] = '';
			$fParam['fbranch_acct_type'] = '';
		}
		
		$this->view->branch_name = $fParam['fbranch_name'];
		$this->view->branch_email = $fParam['fbranch_email'];
		$this->view->branch_acct     = $fParam['fbranch_acct'];
		$this->view->branch_acct_type     = $fParam['fbranch_acct_type'];
// 		die('here');
		$select = $model->getData($fParam,$sortBy,$sortDir,$filter,$custId);		
		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		// $selectPdfCsv = $select;
		// foreach($selectPdfCsv as $key => $row)
		// {
		//     //unset($selectPdfCsv[$key]['PRODUCT_ID']);
		// }
		// //--------konfigurasicsv dan pdf---------
		// if($csv)
		// {		
		// 	$this->_helper->download->csv(array($this->language->_('Branch'),$this->language->_('Branch Email'),$this->language->_('Branch Rekening'),$this->language->_('Currency'),$this->language->_('Type'),$this->language->_('Last Approved')),$selectPdfCsv,null,'insurance_branch');  
		// 	Application_Helper_General::writeLog('VIBL','Export to CSV');
		// }
		// else if($pdf)
		// {
		// 	$this->_helper->download->pdf(array($this->language->_('Branch'),$this->language->_('Branch Email'),$this->language->_('Branch Rekening'),$this->language->_('Currency'),$this->language->_('Type'),$this->language->_('Last Approved')),$selectPdfCsv,null,'insurance_branch');   
		// 	Application_Helper_General::writeLog('VIBL','Export to PDF');
		// }
		// elseif($this->_request->getParam('print') == 1){
		// 		//unset($fields['action']);
		// 	$this->_forward('print', 'index', 'widget', array('data_content' => $selectPdfCsv, 'data_caption' => 'insurance_branch', 'data_header' => $fields));
		// 	Application_Helper_General::writeLog('VIBL','Export to PDF');
		// }
		// else
		// {		
		// 	Application_Helper_General::writeLog('VIBL','View Insurance Branch List');
		// }	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		unset($fields['insurance_branch_ccy']);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;

		}

		$allData = [];

		if(!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1){
			foreach ($select as $key => $row) {
				$subData = [];
				$subData['INS_BRANCH_NAME'] = $row['INS_BRANCH_NAME'];
				$subData['INS_BRANCH_EMAIL'] = $row['INS_BRANCH_EMAIL'];
				$subData['INS_BRANCH_ACCT'] = $row['INS_BRANCH_ACCT']." (".$row['INS_BRANCH_CCY'].")";
				$subData['ACCT_TYPE'] = $row['ACCT_TYPE'];
				$subData['LAST_APPROVED'] = Application_Helper_General::convertDate($row['LAST_APPROVED'], $this->viewDateFormat, $this->defaultDateFormat);
				$allData[] = $subData;
			}
		}

		if ($this->_getParam('csv')) {

			$this->_helper->download->csv(array($this->language->_('Branch'), $this->language->_('Branch Email'), $this->language->_('Branch Rekening'), $this->language->_('Jenis'), $this->language->_('Last Approved')), $allData, null, 'Insurance Branch');
		} else if ($this->_request->getParam('print') == 1) {

			$fields = array(
				'insbranchname'     => array(
					'field'    => 'INS_BRANCH_NAME',
					'label'    => $this->language->_('Branch'),
				),
				'branchemail'     => array(
					'field'    => 'INS_BRANCH_EMAIL',
					'label'    => $this->language->_('Branch Email'),
				),
				'isnsbranchacct'  => array(
					'field'    => 'INS_BRANCH_ACCT',
					'label'    => $this->language->_('Branch Rekening'),
				),
				'jenis'  => array(
					'field'    => 'ACCT_TYPE',
					'label'    => $this->language->_('Jenis'),
				),
				'lastapprove'  => array(
					'field'    => 'LAST_APPROVED',
					'label'    => $this->language->_('Last Approved'),
				),
			);

			$this->_forward('print', 'index', 'widget', array('data_content' => $allData, 'data_caption' => 'Insurance Branch', 'data_header' => $fields));
		}

	}
}