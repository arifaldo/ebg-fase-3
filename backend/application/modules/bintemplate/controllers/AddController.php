<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';


class bintemplate_AddController extends bintemplate_Model_Bintemplate
{

    private $_binType;

    public function initController()
    {       
        $this->_binType = $this->_bin['type']['code'];
    }

    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newlayout');
        $select = $this->_db->select()
            ->from(array('A' => 'BIN_TEMPLATE'), array('max(TEMP_CODE)'));
        $autogen_template_code = $this->_db->fetchRow($select);
        // var_dump($autogen_template_code['max(TEMP_CODE)']);die;
        $this->view->auto_template_code = $autogen_template_code['max(TEMP_CODE)'] + 1;

        $this->view->bin_type = $this->_binType['ecollection']; //default ecollection
        $this->view->allowskip = '0';

        if ($this->_request->isPost() && $this->_getParam('submit') == $this->language->_('Submit')) {
            $this->view->inputParam = $this->getRequest()->getParams();
            $template_code = strtoupper($this->_getParam('template_code'));
            $template_code = (Zend_Validate::is($template_code, 'Alnum') && Zend_Validate::is($template_code, 'StringLength', array('min' => 1, 'max' => 12))) ? $template_code : null;

            /*echo strtoupper($this->_getParam('cust_id'));
			die;*/

            $bin_type = $this->_request->getParams()['bin_type'];

            $filters = array(
                'template_code'           => array('StripTags', 'StringTrim', 'StringToUpper'),
                'template_name'         => array('StripTags', 'StringTrim'),
                'description'        => array('StripTags', 'StringTrim'),
                'bin_type'        => array('StripTags', 'StringTrim'),
                'ex_time'        => array('StripTags', 'StringTrim'),
            );

            if ($bin_type == $this->_binType['ecollection']) {
                $filters['expiredtime'] = array('StripTags', 'StringTrim');
                $filters['ex_time_type'] = array('StripTags', 'StringTrim');
                $filters['vptype'] = array('StripTags', 'StringTrim');
                $filters['partial_payment'] = array('StripTags', 'StringTrim');
                $filters['billing_method'] = array('StripTags', 'StringTrim');
                $filters['buyer_fee'] = array('StripTags', 'StringTrim');
                $filters['seller_fee'] = array('StripTags', 'StringTrim');
                $filters['vp_min'] = array('StripTags', 'StringTrim');
                $filters['vp_max'] = array('StripTags', 'StringTrim');
                $filters['allowskip'] = array('StripTags', 'StringTrim');
            }
            else if($bin_type == $this->_binType['vadebit']){
                $filters['allowskip_debit'] = array('StripTags', 'StringTrim');
                $filters['va_credit_max'] = array('StripTags', 'StringTrim');
                $filters['va_daily_limit'] = array('StripTags', 'StringTrim');
                $filters['remaining_available_credit'] = array('StripTags', 'StringTrim');
                $filters['ex_time_type_debit'] = array('StripTags', 'StringTrim');
            }

            if ($bin_type == $this->_binType['vadebit']) {
                $validators =  array(
                    'template_code'        => array(
                        'NotEmpty',
                        'messages' => array(
                            $this->language->_('Can not be empty'),
                            //$this->getErrorRemark('04','Company Name')
                        )
                    ),
                    'template_name'        => array(
                        'NotEmpty', 'messages' => array($this->language->_('Can not be empty'))
                    ),
                    'description'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'va_credit_max'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'va_daily_limit'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'remaining_available_credit'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'bin_type'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'allowskip_debit' => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'ex_time'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'ex_time_type_debit'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                );

            } else {
                $validators =  array(    
                    'template_code'        => array(
                        'NotEmpty',
                        'messages' => array(
                            $this->language->_('Can not be empty'),
                            //$this->getErrorRemark('04','Company Name')
                        )
                    ),
                    'template_name'        => array(
                        'NotEmpty', 'messages' => array($this->language->_('Can not be empty'))
                    ),
                    'description'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'buyer_fee'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'seller_fee'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),

                    'ex_time'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'ex_time_type'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),

                    'vp_max'        => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'vp_min'        => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'bin_type'        => array(),
                    'expiredtime'        => array(),
                    'vptype'             => array(),
                    'partial_payment'    => array(),

                    'billing_method'     => array(),
                    'allowskip'          => array(),
                );
            }

            $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

            //validasi emobile, cust phone dan cust fax = harus dimulai dengan '62'

            if ($zf_filter_input->isValid()) {
                // $Bintemplate = new bintemplate_Model_Bintemplate();
                $info = "New BIN Template";
                $temp_data = $this->_tempData;

                foreach ($validators as $key => $value) {
                    if ($zf_filter_input->$key) $temp_data[strtoupper($key)] = $zf_filter_input->$key;
                }

                try {

                    $this->_db->beginTransaction();

                    $temp_data['TEMP_NAME']   = $zf_filter_input->template_name;
                    $temp_data['TEMP_DESC']   = $zf_filter_input->description;

                    //eCollection
                    if ($bin_type == $this->_binType['ecollection']) {
                        $temp_data['VA_TYPE']   = $zf_filter_input->vptype;

                        $temp_data['BIN_TYPE']   = $zf_filter_input->bin_type;

                        $temp_data['EXP_TIME']   = $zf_filter_input->expiredtime;
                        $temp_data['EX_TIME']   = $zf_filter_input->ex_time;
                        $temp_data['EX_TIME_TYPE']   = $zf_filter_input->ex_time_type;

                        $temp_data['PARTIAL_TYPE']   = $zf_filter_input->partial_payment;
                        $temp_data['BILLING_TYPE']   = $zf_filter_input->billing_method;

                        $buyer_fee =    Application_Helper_General::convertDisplayMoney($zf_filter_input->buyer_fee);
                        $seller_fee =   Application_Helper_General::convertDisplayMoney($zf_filter_input->seller_fee);
                        $vp_min =   Application_Helper_General::convertDisplayMoney($zf_filter_input->vp_min);
                        $vp_max =   Application_Helper_General::convertDisplayMoney($zf_filter_input->vp_max);
                        $temp_data['BUYER_FEE']   = $buyer_fee;
                        $temp_data['SELLER_FEE']   = $seller_fee;
                        if ($bin_type == $this->_binType['vadebit']) {
                            $temp_data['VP_MIN']   = 0;
                            $temp_data['VP_MAX']   = 0;
                        } else {
                            $temp_data['VP_MIN']   = $vp_min;
                            $temp_data['VP_MAX']   = $vp_max;
                        }

                        $temp_data['SKIP_ERROR']   = $zf_filter_input->allowskip;
                    }
                    // VA Debit
                    else {
                        $temp_data['SKIP_ERROR']   = $zf_filter_input->allowskip_debit;
                        $vaCreditMax =  Application_Helper_General::convertDisplayMoney($zf_filter_input->va_credit_max);
                        $vaDailyLimit = Application_Helper_General::convertDisplayMoney($zf_filter_input->va_daily_limit);
                        $temp_data['VA_CREDIT_MAX'] = $vaCreditMax;
                        $temp_data['VA_DAILY_LIMIT'] = $vaDailyLimit;
                        $temp_data['REMAINING_AVAILABLE_CREDIT'] = $zf_filter_input->remaining_available_credit;
                        $temp_data['EX_TIME_TYPE']   = $zf_filter_input->ex_time_type_debit;

                        // weekly
                        if ($zf_filter_input->ex_time_type_debit == '2') {
                            $ex_time = $this->_getParam('ex_time_weekly');
                        }
                        //monthly
                        else if($zf_filter_input->ex_time_type_debit == '3') {
                            $ex_time = $this->_getParam('ex_time_monthly');

                        }
                        //yearly
                        else if($zf_filter_input->ex_time_type_debit == '4'){
                            $ex_time = $this->_getParam('ex_time_yearly_date').'_'.$this->_getParam('ex_time_yearly_month');
                        }
                        //daily
                        else{
                            $ex_time = '';
                        }

                        $temp_data['EX_TIME']   = $ex_time;
                    }

                    $temp_data['TEMP_CREATED']    = new Zend_Db_Expr('now()');
                    $temp_data['TEMP_CREATEDBY']  = $this->_userIdLogin;
                    $temp_data['TEMP_SUGGESTED']    = new Zend_Db_Expr('now()');
                    $temp_data['TEMP_SUGGESTEDBY']  = $this->_userIdLogin;

                    $temp_data = $this->resetData($temp_data);

                    // echo "<pre>";
                    // print_r($temp_data);die();

                    // insert ke T_GLOBAL_CHANGES
                    $changeid = $this->suggestionWaitingApproval('Bin Template', $info, $this->_changeType['code']['new'], null, 'M_CUSTOMER_BIN', 'TEMP_CUSTOMER_BIN', 'BIN TEMPLATE', $zf_filter_input->template_name);
                    $this->insertTempBinTemplate($changeid, $temp_data);

                    //log CRUD
                    Application_Helper_General::writeLog('BNAD', 'BIN Template has been added, Change id : ' . $changeid);
                    $this->_db->commit();

                    $this->_redirect('/notification/submited/index');
                } catch (Exception $e) {
                    print_r($e);
                    die;
                    $this->_db->rollBack();
                    $error_remark = 'exception';
                }

                if (isset($error_remark)) {
                } else {
                    $this->view->success = 1;
                    $msg   = 'Record added sucessfully';
                    $this->view->customer_msg = $msg;
                    //insert log
                    try {
                        $this->_db->beginTransaction();
                        Application_Helper_General::writeLog('BNAD', ' Create BIN Template [' . $this->view->template_code . ']');
                        $this->_db->commit();
                    } catch (Exception $e) {
                        $this->_db->rollBack();
                        Application_Log_GeneralLog::technicalLog($e);
                    }
                }
            } // END IF IS VALID
            else {
                $this->view->error = 1;
                $this->view->template_code   = ($zf_filter_input->isValid('template_code')) ? $zf_filter_input->template_code : $this->_getParam('template_code');
                $this->view->template_name = ($zf_filter_input->isValid('template_name')) ? $zf_filter_input->template_name : $this->_getParam('template_name');
                $this->view->description = ($zf_filter_input->isValid('description')) ? $zf_filter_input->description : $this->_getParam('description');
                
                $this->view->vp_max   = ($zf_filter_input->isValid('vp_max')) ? $zf_filter_input->vp_max : $this->_getParam('vp_max');
                $this->view->vp_min   = ($zf_filter_input->isValid('vp_min')) ? $zf_filter_input->vp_min : $this->_getParam('vp_min');
                $this->view->seller_fee   = ($zf_filter_input->isValid('seller_fee')) ? $zf_filter_input->seller_fee : $this->_getParam('seller_fee');
                $this->view->buyer_fee   = ($zf_filter_input->isValid('buyer_fee')) ? $zf_filter_input->buyer_fee : $this->_getParam('buyer_fee');
                $this->view->partial_payment   = ($zf_filter_input->isValid('partial_payment')) ? $zf_filter_input->partial_payment : $this->_getParam('partial_payment');
                $this->view->billing_method   = ($zf_filter_input->isValid('billing_method')) ? $zf_filter_input->billing_method : $this->_getParam('billing_method');
                $this->view->vptype   = ($zf_filter_input->isValid('vptype')) ? $zf_filter_input->vptype : $this->_getParam('vptype');
                $this->view->expiredtime   = ($zf_filter_input->isValid('expiredtime')) ? $zf_filter_input->expiredtime : $this->_getParam('expiredtime');
                $this->view->ex_time   = ($zf_filter_input->isValid('ex_time')) ? $zf_filter_input->ex_time : $this->_getParam('ex_time');
                $this->view->ex_time_type   = ($zf_filter_input->isValid('ex_time_type')) ? $zf_filter_input->ex_time_type : $this->_getParam('ex_time_type');
                $this->view->allowskip   = ($zf_filter_input->isValid('allowskip')) ? $zf_filter_input->allowskip : $this->_getParam('allowskip');

                $this->view->bin_type = $bin_type;

                //va debit
                $this->view->allowskip_debit   = ($zf_filter_input->isValid('allowskip_debit')) ? $zf_filter_input->allowskip_debit : $this->_getParam('allowskip_debit');
                $this->view->va_credit_max   = ($zf_filter_input->isValid('va_credit_max')) ? $zf_filter_input->vacreditmax : $this->_getParam('va_credit_max');
                $this->view->va_daily_limit   = ($zf_filter_input->isValid('va_daily_limit')) ? $zf_filter_input->va_daily_limit : $this->_getParam('va_daily_limit');
                $this->view->remaining_available_credit   = ($zf_filter_input->isValid('remaining_available_credit')) ? $zf_filter_input->remaining_available_credit : $this->_getParam('remaining_available_credit');
                $this->view->ex_time_type_debit   = ($zf_filter_input->isValid('ex_time_type_debit')) ? $zf_filter_input->ex_time_type_debit : $this->_getParam('ex_time_type_debit');
                $this->view->ex_time_weekly   = ($zf_filter_input->isValid('ex_time_weekly')) ? $zf_filter_input->ex_time_weekly : $this->_getParam('ex_time_weekly');
                $this->view->ex_time_monthly   = ($zf_filter_input->isValid('ex_time_monthly')) ? $zf_filter_input->ex_time_monthly : $this->_getParam('ex_time_monthly');
                $this->view->ex_time_yearly_date  = ($zf_filter_input->isValid('ex_time_yearly_date')) ? $zf_filter_input->ex_time_yearly_date : $this->_getParam('ex_time_yearly_date');
                $this->view->ex_time_yearly_month  = ($zf_filter_input->isValid('ex_time_yearly_month')) ? $zf_filter_input->ex_time_yearly_month : $this->_getParam('ex_time_yearly_month');
                $error = $zf_filter_input->getMessages();
                // print_r($error);die;
                //format error utk ditampilkan di view html 
                $errorArray = null;
                foreach ($error as $keyRoot => $rowError) {
                    foreach ($rowError as $errorString) {
                        $errorArray[$keyRoot] = $errorString;
                    }
                }
                foreach ($error as $keyRoot => $rowError) {
                    foreach ($rowError as $errorString) {
                        $keyname = "error_" . $keyRoot;
                        // print_r($keyname);die;
                        $this->view->$keyname = $this->language->_($errorString);
                    }
                }

                $this->view->customer_msg  = $errorArray;
            }
        }
        //END if($this->_request->isPost())
    }

    public function resetData($cust_data){
        //ecollection
        if($cust_data['BIN_TYPE'] == $this->_binType['ecollection']){
            $cust_data['VA_CREDIT_MAX'] = '0';
            $cust_data['VA_DAILY_LIMIT'] = '0';
            $cust_data['REMAINING_AVAILABLE_CREDIT'] = '';
        }
        //vadebit
        else if($cust_data['BIN_TYPE'] == $this->_binType['vadebit']){
            $cust_data['VA_TYPE'] = '';
            $cust_data['EXP_TIME'] = '';
            $cust_data['PARTIAL_TYPE'] = '';
            $cust_data['BILLING_TYPE'] = '';
            $cust_data['BUYER_FEE'] = '0';
            $cust_data['SELLER_FEE'] = '0';
            $cust_data['VP_MIN'] = '0';
            $cust_data['VP_MAX'] = '0';
        }

        return $cust_data;
    }
}
