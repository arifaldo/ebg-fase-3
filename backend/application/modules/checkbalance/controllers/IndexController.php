<?php

/**
 * IndexController
 * 
 * @author Yonathan Lesmana
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class Checkbalance_IndexController extends Application_Main{
	

	//protected $_moduleDB = null;
	protected $_moduleDB = 'POP';
	
	
	public function indexAction() 
	{
	    $this->view->masterhasStatusDesc = $this->_masterhasStatus['desc'];
		$this->view->masterhasStatusCode = array_flip($this->_masterhasStatus['code']);
		  
		$this->view->masterStatus = $this->_masterStatus;
		  
		$this->_txStatusCode = $this->_txStatus['code'];
        $this->_txStatusDesc = $this->_txStatus['desc'];
          
		$this->view->dateDisplayFormat     = $this->_dateDisplayFormat;
		$this->view->dateTimeDisplayFormat = $this->_dateTimeDisplayFormat;
		$this->view->dateDBFormat          = $this->_dateDBFormat;
		$this->view->dateDBDefaultFormat   = $this->_dateDBDefaultFormat;
	
		$this->_helper->layout()->setLayout('popup');
		
//		if(!array_key_exists('module_id',$this->_request->getParams())){
//			$errorMsg = $this->getErrorRemark('01','Module ID');
//			
//			$this->_helper->getHelper('FlashMessenger')
//								->addMessage('F');
//			$this->_helper->getHelper('FlashMessenger')
//					->addMessage($errorMsg);
//					
//			$this->frontendLog('CB', $this->_moduleDB, null, null, $errorMsg);
//			
//			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
//		}
		
		if (array_key_exists('account_no',$this->_request->getParams())){
			$filters = array(
							'account_no' => array('StringTrim', 'StripTags'), 	
//							'module_id' => array('StringTrim', 'StripTags'), 	
							'mode' => array('StringTrim', 'StripTags'),			
				 		);
								 
			$validators =  array(
							'account_no' => array(
									'NotEmpty',
									'Digits', 
								   	'messages' => array(
								   						$this->getErrorRemark('22','Account No'), 
								   						$this->getErrorRemark('04','Account No'), 
								   					   ) 
								   ),
//							'module_id' => array(
//									'NotEmpty',
//									array('Db_RecordExists', array('table' => 'M_MODULE', 'field' => 'MODULE_ID')),
//								   	'messages' => array(
//								   						$this->getErrorRemark('22','Module ID'),
//								   						$this->getErrorRemark('22','Module ID'),
//								   					   ) 
//								   ),
							'mode' => array(),
							);
			
			
							
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
			if($zf_filter_input->isValid()) {
				
				if (!$this->authSessionAccount($zf_filter_input->getEscaped('account_no')))
				{
					$errorMsg = $this->getErrorRemark('22','Account No');
					
					$this->_helper->getHelper('FlashMessenger')
								->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')
							->addMessage($errorMsg);
							
					$this->backendLog('CB', $this->_moduleDB, $zf_filter_input->getEscaped('account_no'), null, $errorMsg);
					
					$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
				}
				
				if (array_key_exists('mode',$this->_request->getParams()))
				{
					$this->view->mode = $zf_filter_input->getEscaped('mode');
				}
				else
				{
					$this->view->mode = 'V';
				}
				
				//$this->_moduleDB = $zf_filter_input->getEscaped('module_id');
				
				$errorNote = '';
				
				
				//------by pass------
				//$accountInfo = Application_Model_AnchorMemberFunction::inquiryMainAccount($zf_filter_input->getEscaped('account_no'), $errorNote);
				  $accountInfo = true;
				//-----END by pass------
				
				  
				if($accountInfo==true)
				{
					$undisbursedTxAmount = 
							$this->_db->fetchOne
							(
								$this->_db->select()
								->from(array('T' => 'T_TX'), array('UNDISBURSED' => new Zend_Db_Expr('ISNULL(SUM(AMOUNT),0)')))
								->where('T.TX_ISHOLD='.$this->_db->quote($this->_masterhasStatus['code']['yes']).' OR T.TX_STATUS='.$this->_db->quote($this->_txStatusCode['waitingForExecution']) )
								->where("T.LOAN_ACCT_NO=?", $zf_filter_input->getEscaped('account_no')));
					
					//------by pass------			
					//$accountInfo['HOLD_AMOUNT'] = $undisbursedTxAmount;
					//$this->view->params =  $accountInfo;
					//-----END by pass------
					$this->view->params   =  '';
					
					$this->backendLog('CB', $this->_moduleDB, $zf_filter_input->getEscaped('account_no'), null, null);
				}
				else
				{
					$this->_helper->getHelper('FlashMessenger')
							->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')
							->addMessage($errorNote);
							
					$this->backendLog('CB', $this->_moduleDB, $zf_filter_input->getEscaped('account_no'), null, $errorNote);
					
					$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
				}
			}
			else
			{
				$this->view->error = true;
				 
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')
					->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')
								->addMessage($errorMsg);
						
						$this->backendLog('CB', $this->_moduleDB, null, null, $errorMsg);
						
						$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');						
					}					
				}	
			}
		}
		else
		{
			$errorMsg = $this->getErrorRemark('01','Account No');
			
			$this->_helper->getHelper('FlashMessenger')
								->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')
					->addMessage($errorMsg);
					
			$this->backendLog('CB', $this->_moduleDB, null, null, $errorMsg);
			
			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
		}
		
	}
	
	
	function authSessionAccount($accountNo)
	{
		/*
		$concatMemberId = '';
		
		$anchorCust = $this->_dbObj->fetchOne(
							$this->_dbObj->select()
							->from(array('A' => 'M_ANCHOR'), array('ANCHOR_ID'))
							->where("A.ANCHOR_CUST=?", $this->_custIdLogin));
		
		$memberId = $this->_dbObj->select()
						->from(array('S' => 'M_SCHEME'), array())
						->join(array('C' => 'M_COMMUNITY'), 'C.SCHEME_ID=S.SCHEME_ID', array())
						->join(array('M' => 'M_MEMBER'), 'M.COMM_ID=C.COMM_ID', array('MEMBER_ID'))
						->where("S.ANCHOR_ID = ?", $anchorCust)
						->query()
						->fetchAll();

		$memberId = SGO_Helper_GeneralFunction::inArrayValidate($memberId, 'MEMBER_ID');
		
		$concatMemberId = implode(',', $memberId);	

		$memberId = 
					$this->_dbObj->select()
					->from(array('M' => 'M_MEMBER'), array('MEMBER_ID'))
					->where("M.MEMBER_CUST=?", $this->_custIdLogin)
					->query()
					->fetchAll();
		
		$memberId = SGO_Helper_GeneralFunction::inArrayValidate($memberId, 'MEMBER_ID');
		
		$memberId = implode(',', $memberId);
		
		if($memberId!=""){
			if($concatMemberId!=""){
				$concatMemberId = $concatMemberId.','.$memberId;
			}else{
				$concatMemberId = $memberId;
			}
		}

		if($concatMemberId=='') $concatMemberId = 'NULL';
								
		$memberAcct = 
					$this->_dbObj->select()
					->from(array('M' => 'M_MEMBER_ACCT'), array('LOAN_ACCT_NO'))
					->where("M.MEMBER_ID IN (" . $concatMemberId . ")")
					->where("M.LOAN_ACCT_NO=?", $accountNo)
					->query()
					->fetchAll();
					
		if(count($memberAcct)==0) return false;
		return true;
		*/
		$select = $this->_db->select()
						    ->from('M_MEMBER_ACCT',array('LOAN_ACCT_NO'))
							->where('LOAN_ACCT_NO = ?',$accountNo);
        $result = $this->_db->fetchOne($select);
        
        if($result){ return true; }else{ return false; }
	}
	
	public function failedAction()
	{
		$this->_redirect('/popuperror/index/index');
	}
 
	
}


	

