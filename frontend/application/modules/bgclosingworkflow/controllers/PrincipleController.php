<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_PrincipleController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg Number / Subject'),
      ),
      'recipient' => array(
        'field' => 'RECIPIENT_NAME',
        'label' => $this->language->_('Aplicant'),
      ),

      'cust'     => array(
        'field'    => 'CUST_NAME',
        'label'    => $this->language->_('Obligee'),
      ),
      'bg_amount' => array(
        'field' => 'TIME_PERIOD_START',
        'label' => $this->language->_('BG Amount'),
      ),
      'branch'   => array(
        'field'    => 'BG_BRANCH',
        'label'    => $this->language->_('Bank Branch'),
      ),

      'insurance_branch'   => array(
        'field'    => 'TIME_PERIOD_END',
        'label'    => $this->language->_('Insurance Branch'),
      ),
      'type'              => array(
        'field'    => 'IS_AMENDMENT',
        'label'    => $this->language->_('Type') . " - " . $this->language->_('Status'),
      )
    );

    Application_Helper_General::writeLog('VPAL', $this->language->_('Lihat Daftar Permintaan Ijin Prinsip'));

    $selectbg = $this->_db->select()
      ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array(
        'REG_NUMBER' => 'BG_REG_NUMBER',
        'SUBJECT' => 'BG_SUBJECT',
        'CREATED' => 'BG_CREATED',
        'B.BRANCH_NAME',
        'C.CUST_NAME',
        'A.RECIPIENT_NAME',

        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_START',
        'TIME_PERIOD_END',
        'CREATEDBY' => 'BG_CREATEDBY',
        'AMOUNT' => 'BG_AMOUNT',
        'IS_AMENDMENT' => "A.CHANGE_TYPE",
        'FULLNAME' => 'T.USER_FULLNAME',
        "BG_STATUS"

      ))
      ->join(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID AND T.CUST_ID = A.CUST_ID')
      ->join(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID')
      ->join(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE')
      //->where('A.BG_INSURANCE_CODE ='.$this->_db->quote((string)$this->_custIdLogin))
      // ->where('A.BG_STATUS = ?', '11')
      ->where('A.BG_STATUS IN (8, 10)')
      ->where('A.PRINCIPLE_APPROVE = 0 AND BG_INSURANCE_CODE = ' . $this->_db->quote($this->_custIdLogin) . "AND COUNTER_WARRANTY_TYPE = '3'")
      ->order('A.BG_CREATED DESC');

    // filter ----------------------------------------------------------

    $filterlist = array('BG_REG_NUMBER', 'APPLICANT', 'OBLIGEE_NAME', 'AMOUNT', 'BG_BRANCH');

    // $filterlist = array(
    //   "BG_REG_NUMBER" => 'BG REG NUMBER',
    //   'APPLICANT' => "APPLICANT",
    //   "OBLIGEE_NAME" => "OBLIGEE NAME",
    //   "AMOUNT" => "AMOUNT",
    //   "BG_BRANCH" => "BANK BRANCH",
    // );

    $this->view->filterlist = $filterlist;

    $dataParam = array("BG_REG_NUMBER", "APPLICANT", "OBLIGEE_NAME", "AMOUNT", "BG_BRANCH");
    $dataParamValue = [];

    $index = 0;

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            if (!empty($dataParamValue[$dtParam])) {
              $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
            }
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }
    }


    $filter = TRUE;

    $filterlist = array(
      "BG_REG_NUMBER" => 'BG REG NUMBER',
      'APPLICANT' => "APPLICANT",
      "OBLIGEE_NAME" => "OBLIGEE NAME",
      "AMOUNT" => "AMOUNT",
      "BG_BRANCH" => "BANK BRANCH",
    );


    $filterArr = array(
      'BG_REG_NUMBER'  => array('StringTrim', 'StripTags'),
      'APPLICANT'  => array('StringTrim', 'StripTags'),
      'OBLIGEE_NAME'  => array('StringTrim', 'StripTags'),
      'AMOUNT'   => array('StringTrim', 'StripTags'),
      'BG_BRANCH'  => array('StringTrim', 'StripTags'),
    );

    $options = array('allowEmpty' => true);
    $validators = array(
      'BG_REG_NUMBER' => array(),
      'APPLICANT' => array(),
      'OBLIGEE_NAME' => array(),
      'AMOUNT' => array(),
      'BG_BRANCH' => array(),
    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

    $bg_number = $zf_filter->getEscaped('BG_REG_NUMBER');
    $applicant = html_entity_decode($zf_filter->getEscaped('APPLICANT'));
    $obligee_nanme = $zf_filter->getEscaped('OBLIGEE_NAME');
    $amount = $zf_filter->getEscaped('AMOUNT');
    $branch_name = $zf_filter->getEscaped('BG_BRANCH');

    if ($filter == null || $filter == TRUE) {
      $append_query = "";

      if (!empty($bg_number) > 0) {
        $selectbg->where("A.BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $bg_number . '%'));
      }

      if (!empty($applicant) > 0) {
        $selectbg->where("C.CUST_NAME LIKE " . $this->_db->quote('%' . $applicant . '%'));
      }

      if (!empty($obligee_nanme) > 0) {
        $selectbg->where("A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $obligee_nanme . '%'));
      }

      if (!empty($amount) > 0) {
        $selectbg->where("A.BG_AMOUNT LIKE " . $this->_db->quote('%' . $amount . '%'));
      }

      if (count($branch_name) > 0) {
        $selectbg->where("B.BRANCH_NAME LIKE " . $this->_db->quote('%' . $branch_name . '%'));
      }

      // if (count($obligee_name) > 0) {
      //   foreach ($obligee_name as $key => $value) {
      //     if ($append_query != "") {
      //       $append_query .= " OR ";
      //     }
      //     $append_query .= "A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $value . '%');
      //   }
      // }

      // if (count($amount) > 0) {
      //   foreach ($amount as $key => $value) {
      //     if ($append_query != "") {
      //       $append_query .= " OR ";
      //     }
      //     $append_query .= "A.BG_AMOUNT = " . $this->_db->quote($value);
      //   }
      // }

      if ($append_query != "") {
        $selectbg->where($append_query);
      }
    }

    // -----------------------------------------------------------------

    $selectbg = $selectbg->query()->fetchAll();

    $selectlc = $this->_db->select()
      ->from(array('A' => 'T_LC'), array(
        'REG_NUMBER' => 'LC_REG_NUMBER',
        'SUBJECT' => 'LC_CREDIT_TYPE',
        'CREATED' => 'LC_CREATED',
        'CCYID' => 'LC_CCY',
        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_END' => 'LC_EXPDATE',
        'CREATEDBY' => 'LC_CREATEDBY',
        'AMOUNT' => 'LC_AMOUNT',
        'FULLNAME' => 'T.USER_FULLNAME'
      ))
      ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
      ->where('A.LC_CUST =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.LC_STATUS = 1')
      ->order('A.LC_CREATED DESC')
      ->query()->fetchAll();


    $result = array_merge($selectbg, $selectlc);

    $setting = new Settings();
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $pw_hash = md5($enc_salt . $enc_pass);
    $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
    $sessionNamespace->token   = $rand;
    $this->view->token = $sessionNamespace->token;

    foreach ($result as $key => $value) {
      $get_reg_number = $value["REG_NUMBER"];

      $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
      $encpayreff = urlencode($encrypted_payreff);

      $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
    }

    foreach ($result as $key => $value) {
      // $getInsuranceBranch = $this->_db->select()
      //   ->from(["TBD" => "TEMP_BANK_GUARANTEE_DETAIL"])
      //   ->joinLeft(["MIB" => "M_INS_BRANCH"], "MIB.INS_BRANCH_CODE = TBD.PS_FIELDVALUE")
      //   ->where("TBD.BG_REG_NUMBER = ?", $value["REG_NUMBER"])
      //   ->query()->fetchAll();
      // $result[$key]["INSURANCE_BRANCH"] = $getInsuranceBranch[0]["INS_BRANCH_NAME"];

      $temp_save = $this->_db->select()
        ->from(["TBGD" => "TEMP_BANK_GUARANTEE_DETAIL"])
        ->where("TBGD.BG_REG_NUMBER = ?", $value["REG_NUMBER"])
        ->where("TBGD.PS_FIELDNAME = ?", "Insurance Branch");

      $temp_save = $this->_db->fetchRow($temp_save);

      $temp_ins_branch = $this->_db->select()
        ->from("M_INS_BRANCH")
        ->where("INS_BRANCH_CODE = ?", $temp_save["PS_FIELDVALUE"]);

      $temp_ins_branch = $this->_db->fetchRow($temp_ins_branch);

      $result[$key]["INSURANCE_BRANCH"] = $temp_ins_branch["INS_BRANCH_NAME"];
    }

    $this->paging($result);

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    // select type ------------------------------------------------------------
    $getct        = Zend_Registry::get('config');
    $ctdesc     = $getct["bg"]["changetype"]["desc"];
    $ctcode     = $getct["bg"]["changetype"]["code"];
    $arr_type = array_combine(array_values($ctcode), array_values($ctdesc));

    $this->view->arr_type = $arr_type;
    //  -------------------------------------------------------------------------- 


    $this->view->arrStatus = $arrStatus;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;

    if (!empty($dataParamValue)) {

      foreach ($dataParamValue as $key => $value) {

        $value = str_replace(",", "", $value);

        $wherecol[] = $key;
        $whereval[] = $value;
      }

      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }
  }
}
