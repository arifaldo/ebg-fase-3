<?php
ob_start();
require_once (realpath(dirname(__FILE__) . '/../scheduler/zf-cli.php'));
$output = ob_get_contents();
ob_end_clean();


class SupervisorAlertListener {

	private $listenerService;

	public function __construct(){
		$this->listenerService = SGO_Extendedmodule_Supervisorservice_Service::getInstance();
	}

	public function sendEmail($fromName, $recipients, $subject, $message){

		$config = Zend_Registry::get('config');
		$smtpServer = $config['email']['smtpserver'];
		$smtpPort	= $config['email']['smtpport'];
		$username = $config['email']['username'];
		$password = $config['email']['password'];

		$configEmailAuth = (isset($config['email']['auth']) ? $config['email']['auth'] : NULL);

		$config = array(
			'port' 	  	=> $smtpPort,
			'username'  => $username,
			'password'  => $password,
		);

		if (isset($configEmailAuth)){
			$config['auth'] = $configEmailAuth; //test
		} else {
			$config['auth'] = '';
		}				

		if ($smtpPort !== '25'){
			$config['ssl'] = 'tls';
		}

		$transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);

		$mail = new Zend_Mail();
		$mail->setFrom($username, $fromName);
		$mail->setSubject($subject);
		$mail->setBodyHtml($message);
		$mail->addTo($recipients,'');
		$mail->send($transport);
	}


	public function execute(){
		$listener = $this->listenerService->getListener();
		$callback = function($listenerData, $eventData) use ($listener){
			$eventDetail = $eventData->getData();
			if(!empty($eventDetail["processname"]) && !$listener->isWorkerListener()){
				$recipients = $listener->getEmailRecipientsWhoManagingSupervisor();
				$processName = $eventDetail["processname"];
				$subject = "Process Information - {$processName}";
				$fromName = "SUPERVISOR DAEMON";
				$message = $this->composeMessage($listener, $eventDetail);

				$this->sendEmail($fromName, $recipients, $subject, $message);
			}

			return true;
		};

		$listener->listen($callback);
	}

	public function composeMessage($listener, $eventDetail){

		$message = "<table style='table-collapse: collapse; border: 1px solid grey;'>";
		$message .= "<tr>";
		$message .= "<td colspan='2'style='background: #cfcfcf;'>";
		$message .= "<h3>Worker Detail</h3>";
		$message .= "</td>";
		$message .= "</tr>";
		$message .= "<td style='background: #cfcfcf;'>";
		$message .= "<strong>Process Name</strong>";
		$message .= "</td>";
		$message .= "<td>";
		$message .= $eventDetail ["processname"];
		$message .= "</td>";
		$message .= "</tr>";

		$message .= "<tr>";
		$message .= "<td style='background: #cfcfcf;'>";
		$message .= "<strong>Event Name</strong>";
		$message .= "</td>";
		$message .= "<td>";
		$message .= $eventDetail ["eventname"];
		$message .= "</td>";
		$message .= "</tr>";

		$message .= "</table>";

		$message .= "<table style='table-collapse: collapse; border: 1px solid grey;'>";
		$message .= "<tr>";
		$message .= "<td colspan='2'style='background: #cfcfcf;'>";
		$message .= "<h3 style='background: #cfcfcf;'>Log Info</h3>";
		$message .= "</td>";
		$message .= "</tr>";
		$message .= "<td>";
		$message .= "<code>";
		foreach ($listener->readLog(null, true) as $line) {
			$message .= $line . "<br />";
		}
		$message .= "</code>";
		$message .= "</td>";
		$message .= "</table>";
		return $message;
	}
}

$workerClass = new SupervisorAlertListener();
$workerClass->execute();


?>