<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Onlinebank_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');    
	    $this->setbackURL('/onlinebank/index');    

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$this->view->report_msg = array();
		if($this->_request->isPost() )
		{
			$filters = array(
			                 'bank_name'     => array('StringTrim','StripTags'),			                
							 'bank_code'	 => array('StringTrim','StripTags'),							
							);

			$validators = array('bank_code'  => array('NotEmpty',
																'Digits',
																 new Zend_Validate_StringLength(array('min'=>3,'max'=>3)),
																 array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'M_DOMESTIC_ONLINE_BANK_TABLE',
																							'field'=>'BANK_OL_CODE'
																						)
																			),
																'messages' => array(
																				   $this->language->_('Can not be empty'),
																				   $this->language->_('Invalid Format'),
																					$this->language->_('Data too long (min. 3 chars and max. 3 chars)'),
																					$this->language->_('Bank Code already exists'),)
																		),

											'bank_name' => array('NotEmpty',
																 new Zend_Validate_Alnum(array('allowWhiteSpace' => true)),
																 new Zend_Validate_StringLength(array('max'=>35)),
																 array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'M_DOMESTIC_ONLINE_BANK_TABLE',
																							'field'=>'BANK_OL_NAME'
																						)
																			),
																 'messages' => array(
																					 $this->language->_('Can not be empty'),
																					 $this->language->_('Invalid Bank Name Format (no special characters)'),
																					 $this->language->_('Data too long (max 35 chars)'),
																					 $this->language->_('Bank Name already exists'),
																					 )
																),
										   );							
		
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				$content = array(
								'BANK_OL_CODE' 	=> $zf_filter_input->bank_code,
								'BANK_OL_NAME'  => $zf_filter_input->bank_name,
						       );
				//$content['BANK_ISDISPLAYED'] = (!empty($zf_filter_input->swift_code)) ? 1 : 0;
						       
				try 
				{
					
					//-------- insert --------------
					$this->_db->beginTransaction();
					
					$this->_db->insert('M_DOMESTIC_ONLINE_BANK_TABLE',$content);
					
					// $this->_db->commit();
					$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('DBAD','Add Online Bank. Bank ID : ['.$id.'], Bank Name : ['.$zf_filter_input->bank_name.']');
					$this->view->success = true;
					$this->view->report_msg = array();
					
					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

				$error = $zf_filter_input->getMessages();
				
				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
		
		Application_Helper_General::writeLog('DBAD','Viewing Add Online Bank');
	}

}