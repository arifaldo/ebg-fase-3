<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class filesharingreport_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$setting 	= new Settings();
		$enc_pass 	= $setting->getSetting('enc_pass');
		$enc_salt 	= $setting->getSetting('enc_salt');

		$sessToken 			= new Zend_Session_Namespace('Tokenenc');
		$password_hash 		= md5($enc_salt . $enc_pass);
		$rand 				= $this->_userIdLogin . date('YmdHis') . $password_hash;
		$sessToken->token 	= $rand;
		$this->view->token 	= $sessToken->token;

		$filter 		= $this->_getParam('filter');
		$filter_clear 	= $this->_getParam('filter_clear');

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		//get filtering param
		$filters = array('*' => array('StringTrim', 'StripTags'));

		$validators = array('*' => array('allowEmpty' => true));

		$optionValidators = array('breakChainOnFailure' => false);

		$filterlist = array("fCompanyCode" => "Company Code", 'fFileName' => "File Name", 'fDeleted' => "Status");
		$this->view->filterlist = $filterlist;

		$filterArr = array(
			'fCompanyCode'	=>  array('StringTrim', 'StripTags'),
			'fFileName'	=>  array('StringTrim', 'StripTags'),
			'fDeleted'	=>  array('StringTrim', 'StripTags'),
		);

		$dataParam = array("fCompanyCode", "fFileName", "fDeleted");
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "Last Suggestion" || $value == "APPROVE_DATE" || $value == "Last Login") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}

			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}

		$zf_filter = new Zend_Filter_Input($filterArr, [], $dataParamValue);

		$filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');
		$headers = array(
			'file_id'  => array(
				'field' => 'FILE_ID',
				'label' => 'No'
			),
			'customer_code'  => array(
				'field' => 'CUSTOMER_CODE',
				'label' => 'Customer Code'
			),
			'file_name'  => array(
				'field' => 'FILE_NAME',
				'label' => 'File Name'
			),
			'upload_by'  => array(
				'field' => 'UPLOAD_BY',
				'label' => 'Upload By'
			),
			'uploaded_date'     => array(
				'field' => 'UPLOADED_DATE',
				'label' => 'Uploaded Date'
			),
			'DELETED'     => array(
				'field' => 'DELETED',
				'label' => 'File is Deleted'
			)
		);

		if ($filter == TRUE && $filter_clear == NULL) {


			$fCompanyCode 		= $zf_filter->getEscaped("fCompanyCode");
			// $fCompanyCode 		= $this->_request->getParam('fCompanyCode');
			$fFileName 			= $zf_filter->getEscaped('fFileName');
			// $fFileName 			= $this->_request->getParam('fFileName');
			$fUploadDateFrom 	= html_entity_decode($zf_filter->getEscaped('fUploadDateFrom'));
			$fUploadDateTo 		= html_entity_decode($zf_filter->getEscaped('fUploadDateTo'));
			$fDeleted 			= $zf_filter->getEscaped('fDeleted');
			// $fDeleted 			= $this->_request->getParam('fDeleted');
		}

		if ($fUploadDateFrom) {
			$FormatDate = new Zend_Date($fUploadDateFrom, $this->_dateDisplayFormat);
			$fUploadDateFrom = $FormatDate->toString($this->_dateDBFormat);
		}

		if ($fUploadDateTo) {
			$FormatDate = new Zend_Date($fUploadDateTo, $this->_dateDisplayFormat);
			$fUploadDateTo = $FormatDate->toString($this->_dateDBFormat);
		}

		$select = $this->_db->select()
			->from(
				array('TFS' => 'T_FILE_SUBMIT'),
				array(
					'FILE_ID'				=> 'TFS.FILE_ID',
					'CUST_ID'				=> 'TFS.CUST_ID',
					'FILE_NAME'				=> 'TFS.FILE_NAME',
					'FILE_UPLOADEDBY'		=> 'TFS.FILE_UPLOADEDBY',
					'FILE_UPLOADED_TIME'	=> 'TFS.FILE_UPLOADED_TIME',
					'FILE_DELETED'			=> new Zend_Db_Expr("(CASE TFS.FILE_DELETED
					WHEN '0' THEN 'No'
					WHEN '1' THEN 'Yes'
					END)")
				)
			)
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = TFS.CUST_ID',
				array()
			)
			->joinLeft(['MB' => 'M_BUSER'], 'MC.CUST_CREATEDBY = MB.BUSER_ID', [''])
			->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', ['MBR.BRANCH_CODE']);

		if ($filter == TRUE || $csv || $pdf || $this->_request->getParam('print')) {
			$header = Application_Helper_Array::simpleArray($headers, 'label');

			if ($fCompanyCode) {
				$select->where('TFS.CUST_ID LIKE ' . $this->_db->quote('%' . $fCompanyCode . '%'));
			}
			if ($fFileName) {
				$select->where('TFS.FILE_NAME LIKE ' . $this->_db->quote('%' . $fFileName . '%'));
			}
			if ($fDeleted) {
				$fDeleted = ($fDeleted == 2) ? 0 : 1;
				$select->where('TFS.FILE_DELETED = ?', $fDeleted);
				$fDeleted = ($fDeleted == 0) ? 2 : 1;
			}
		}
		$select->order('TFS.FILE_UPLOADED_TIME DESC');

		$auth = Zend_Auth::getInstance()->getIdentity();

		if ($auth->userHeadQuarter == "NO") {
			$select->where('MBR.BRANCH_CODE = ?', $auth->userBranchCode); // Add Bahri
		}

		$data = $this->_db->fetchAll($select);

		$this->paging($data);

		$deletedArr = [
			'1' => 'Yes',
			'2' => 'No'
		];

		$this->view->filter = $filter;

		if (!empty($dataParamValue)) {
			$filterlistdata = array("Filter");
			foreach ($dataParamValue as $fil => $val) {
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
			}
		} else {
			$filterlistdata  = array("Filter - None");
		}

		$this->view->data_filter = $filterlistdata;

		if ($csv || $pdf || $this->_request->getParam('print')) {
			$arr = $this->_db->fetchAll($select);

			for ($i = 0; $i < count($arr); $i++) {

				$result[$i][] = $i + 1;
				$result[$i][] = $arr[$i]['CUST_ID'];
				$result[$i][] = $arr[$i]['FILE_NAME'];
				$result[$i][] = $arr[$i]['FILE_UPLOADEDBY'];
				$result[$i][] = $arr[$i]['FILE_UPLOADED_TIME'];
				$result[$i][] = $arr[$i]['FILE_DELETED'];

				//$result[$i] = $select[$i]['PAPER_FROM'];

			}

			foreach ($select as $key => $value) {
				unset($select[$key]['ID']);
			}

			if ($csv) {
				$this->_db->insert('T_BACTIVITY', array(
					'LOG_DATE'         => new Zend_Db_Expr('now()'),
					'USER_ID'          => $this->_userIdLogin,
					'USER_NAME'        => $this->_userNameLogin,
					'ACTION_DESC'      => 'DSLS',
					'ACTION_FULLDESC'  => 'View File Sharing List',
				));
				$this->_helper->download->csv($header, $result, null, $this->language->_('File Sharing List Report'));
			} else if ($pdf) {
				$this->_helper->download->pdf($header, $result, null, $this->language->_('File Sharing List Report'));
				Application_Helper_General::writeLog('DSLS', 'Download File Sharing List Report');
			} else if ($this->_request->getParam('print') == 1) {
				$zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optionValidators);
				$filter 		= $this->_getParam('filter');
				$filterlistdatax = $this->_request->getParam('data_filter');
				if ($filterlistdatax != "Filter - None") {
					foreach ($filterlistdatax as $key => $val) {
						$arr = explode("-", $val);
						$colmn = ltrim(rtrim($arr[0]));
						$value = ltrim(rtrim($arr[1]));
						$arrFilter[$colmn] = $value;
					}
					$cCompanyCode 		= $this->_request->getParam('fCompanyCode');
					$cFileName 			= $this->_request->getParam('fFileName');
					$cUploadDateFrom 	= html_entity_decode($arrFilter['fUploadDateFrom']);
					$cUploadDateTo 		= html_entity_decode($arrFilter['fUploadDateTo']);
					$cDeleted 			= $this->_request->getParam('fDeleted');

					if ($cCompanyCode) {
						$select->where('TFS.CUST_ID LIKE ' . $this->_db->quote('%' . $cCompanyCode . '%'));
					}
					if ($cFileName) {
						$select->where('TFS.FILE_NAME LIKE ' . $this->_db->quote('%' . $cFileName . '%'));
					}
					if ($cDeleted) {
						$cDeleted = ($cDeleted == 2) ? 0 : 1;
						$select->where('TFS.FILE_DELETED = ?', $cDeleted);
						$cDeleted = ($cDeleted == 0) ? 2 : 1;
					}
				}
				$select->order('TFS.FILE_UPLOADED_TIME DESC');
				$data = $this->_db->fetchall($select);
				$field_export = array(
					'file_id'  => array(
						'field' => 'FILE_ID',
						'label' => 'File ID'
					),
					'customer_code'  => array(
						'field' => 'CUSTOMER_CODE',
						'label' => 'Customer Code'
					),
					'file_name'  => array(
						'field' => 'FILE_NAME',
						'label' => 'File Name'
					),
					'upload_by'  => array(
						'field' => 'UPLOAD_BY',
						'label' => 'Upload By'
					),
					'uploaded_date'     => array(
						'field' => 'UPLOADED_DATE',
						'label' => 'Uploaded Date'
					),
					'DELETED'     => array(
						'field' => 'DELETED',
						'label' => 'File is Deleted'
					)
				);
				foreach ($data as $pTrx) {
					$paramTrx = array(
						"FILE_ID"  				=> $pTrx['FILE_ID'],
						"CUSTOMER_CODE" 			=> $pTrx['CUST_ID'],
						"FILE_NAME"  			=> $pTrx['FILE_NAME'],
						"UPLOAD_BY"  			=> $pTrx['FILE_UPLOADEDBY'],
						"UPLOADED_DATE"  		=> $pTrx['FILE_UPLOADED_TIME'],
						"DELETED"  		=> $pTrx['FILE_DELETED'],
					);
					$newData[] = $paramTrx;
				}

				$this->_forward('print', 'index', 'widget', array('data_content' => $newData, 'data_caption' => 'Customer List Report', 'data_header' => $field_export, 'data_filter' => $filterlistdatax));
			}
		}

		$this->view->filter 			= $filter;
		$this->view->deletedArr 		= $deletedArr;
		$this->view->fCompanyCode 		= $fCompanyCode;
		$this->view->fFileName 			= $fFileName;
		$this->view->fUploadDateFrom 	= $fUploadDateFrom;
		$this->view->fUploadDateTo 		= $fUploadDateTo;
		$this->view->fDeleted 			= $fDeleted;

		Application_Helper_General::writeLog('DSLS', 'View File Sharing List');
	}

	public function detailAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$id 	= $this->_getParam('id');
		$pdf 	= $this->_getParam('pdf');
		$print 	= $this->_getParam('print');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$decryption_id = $AESMYSQL->decrypt($decryption, $password);

		$select = $this->_db->select()
			->from(
				array('TFS' => 'T_FILE_SUBMIT'),
				array(
					'FILE_ID'			=> 'TFS.FILE_ID',
					'CUST_ID'			=> 'TFS.CUST_ID',
					'CUST_NAME'			=> 'MC.CUST_NAME',
					'FILE_DESCRIPTION'	=> 'TFS.FILE_DESCRIPTION',
					'FILE_UPLOADEDBY'	=> 'TFS.FILE_UPLOADEDBY',
					'FILE_UPLOADED_TIME'	=> 'TFS.FILE_UPLOADED_TIME',
					'FILE_DOWNLOADED'	=> 'TFS.FILE_DOWNLOADED',
					'FILE_DOWNLOADEDBY'	=> 'TFS.FILE_DOWNLOADEDBY',
					'FILE_DELETED'		=> new Zend_Db_Expr("(CASE TFS.FILE_DELETED
					WHEN '0' THEN 'No'
					WHEN '1' THEN 'Yes'
					END)"),
					'FILE_DELETEDBY'	=> 'TFS.FILE_DELETEDBY'
				)
			)
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = TFS.CUST_ID',
				array()
			);

		if ($pdf) {
			$select->where('TFS.FILE_ID = ?', $pdf);
			$data = $this->_db->fetchRow($select);

			$outputData = array(
				'FILE_ID' => $data['FILE_ID'],
				'CUST_ID' => $data['CUST_ID'],
				'CUST_NAME' => $data['CUST_NAME'],
				'FILE_DESCRIPTION' => $data['FILE_DESCRIPTION'],
				'FILE_UPLOADEDBY' => $data['FILE_UPLOADEDBY'],
				'FILE_UPLOADED_TIME'	=> Application_Helper_General::convertDate($data['FILE_UPLOADED_TIME'], $this->displayDateFormat, $this->defaultDateFormat),
				'FILE_DOWNLOADED' => $data['FILE_DOWNLOADED'],
				'FILE_DOWNLOADEDBY' => $data['FILE_DOWNLOADEDBY'],
				'FILE_DELETED' => $data['FILE_DELETED'],
				'FILE_DELETEDBY' => $data['FILE_DELETEDBY'],
			);

			$outputHTML = $this->view->render('/index/indexpdf.phtml');
			$outputHTML = strtr($outputHTML, $outputData);

			$this->_helper->download->pdfModif(null, null, null, 'File Sharing List Detail', $outputHTML);
			Application_Helper_General::writeLog('COLS', 'Download PDF File Sharing List Detail');
		} else if ($print) {
			$select->where('TFS.FILE_ID = ?', $decryption_id);
			$data = $this->_db->fetchRow($select);

			$fields = array(
				array(
					'field' => 'datakey',
					'label' => 'Data Key',
					'sortable' => false
				),
				array(
					'field' => 'dataseparator',
					'label' => ':',
					'sortable' => false,
				),
				array(
					'field' => 'datavalue',
					'label' => 'Data Value',
					'sortable' => false
				),
			);

			$dataContent = [
				[
					'datakey' =>  $this->language->_('Customer Code'),
					'dataseparator' => ':',
					'datavalue' => $data['CUST_ID'],
				],
				[
					'datakey' =>  $this->language->_('Company Name'),
					'dataseparator' => ':',
					'datavalue' => $data['CUST_NAME'],
				],
				[
					'datakey' =>  $this->language->_('File Description'),
					'dataseparator' => ':',
					'datavalue' => $data['FILE_DESCRIPTION'],
				],
				[
					'datakey' =>  $this->language->_('Uploaded By'),
					'dataseparator' => ':',
					'datavalue' => $data['FILE_UPLOADEDBY'],
				],
				[
					'datakey' =>  $this->language->_('Uploaded Date'),
					'dataseparator' => ':',
					'datavalue' => Application_Helper_General::convertDate($data['FILE_UPLOADED_TIME'], $this->displayDateFormat, $this->defaultDateFormat),
				],
				[
					'datakey' =>  $this->language->_('Last Downloaded By'),
					'dataseparator' => ':',
					'datavalue' => $data['FILE_DOWNLOADEDBY'],
				],
				[
					'datakey' =>  $this->language->_('File Is Deleted'),
					'dataseparator' => ':',
					'datavalue' => $data['FILE_DELETED'],
				],
				[
					'datakey' =>  $this->language->_('Deleted By'),
					'dataseparator' => ':',
					'datavalue' => $data['FILE_DELETEDBY'],
				],
			];

			$this->_forward('print', 'index', 'widget', array('data_content' => $dataContent, 'data_caption' => 'Bank Account List Report', 'data_header' => $fields));
		} else {
			$select->where('TFS.FILE_ID = ?', $decryption_id);
			$data = $this->_db->fetchRow($select);
			$this->view->data = $data;
		}
	}
}
