<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once("Service/Account.php");

class eform_AmdetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        // echo "<code>masuk = $data</code>"; die;	
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->maxClaimPeriod = $settings->getSetting('max_claim_period');
        $this->view->systemType = $system_type;
        // echo "<code>systemType = $system_type</code><br>";	
        // echo '<pre>';
        // print_r($system_type); 
        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $toc_ind = $settings->getSetting('ftemplate_bg_ind');
        $this->view->toc_ind = $toc_ind;
        $toc_eng = $settings->getSetting('ftemplate_bg_eng');
        $this->view->toc_eng = $toc_eng;

        list($getCurrency, $minNominal) = $this->getCurrency();

        $this->view->currencyArr = $getCurrency;
        $this->view->minNominal = $minNominal;

        // get cash collateral

        $get_cash_collateral = $this->_db->select()
            ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
            ->where("CUST_ID = ?", "GLOBAL")
            ->where("CHARGES_TYPE = ?", "10")
            ->query()->fetchAll();

        $this->view->cash_collateral = $get_cash_collateral[0];

        // end get cash collateral

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];
        $bgparam = $this->_getParam('bgnumb');
        // $bg = $this->_getParam('bg');


        $sessToken  = new Zend_Session_Namespace('Tokenenc');
        $password   = $sessToken->token;

        $AESMYSQL = new Crypt_AESMYSQL();
        $decryption = urldecode($bgparam);
        $numb = $AESMYSQL->decrypt($decryption, $password);
        // $this->view->bgnumb = $numb;
        // $this->view->bg = $numb;
        // echo "<code>numb = $numb</code><BR>";

        if (!empty($numb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $checkProgressAmd = $this->_db->select()
                ->from('TEMP_BANK_GUARANTEE')
                ->where('BG_OLD = ?', $bgdata[0]['BG_NUMBER'])
                ->query()->fetch();

            $checkProgressClose = $this->_db->select()
                ->from('TEMP_BANK_GUARANTEE_CLOSE')
                ->where('BG_NUMBER = ?', $bgdata[0]['BG_NUMBER'])
                ->orWhere('BG_REG_NUMBER = ?', $bgdata[0]['BG_REG_NUMBER'])
                ->query()->fetch();

            if ($bgdata[0]['BG_STATUS'] != '15' || $checkProgressAmd || $checkProgressClose) {
                $this->_redirect('/eform');
            }

            // get guarantee transactions
            $guaranteeTrasactions = $this->getGuarantedTransactions($bgdata[0]['BG_REG_NUMBER']);
            $this->view->guaranteeTrasactions = $guaranteeTrasactions;

            $gtStatus       = $conf["bgdoc"]["type"]["desc"];
            $gtCode         = $conf["bgdoc"]["type"]["code"];

            $gtStatus = array_combine(array_values($gtCode), array_values($gtStatus));
            $this->view->gtStatus = $gtStatus;
            // end get guarantee transactions

            // lampiran dokumen pendukung

            $getLampiranPendukung = $this->getLampiranPendukung($numb);
            $this->view->getLampiranPendukung = $getLampiranPendukung;

            // end lampiran dokumen pendukung

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            // penkondisian kontra
            if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {
                $getInsuranceBranch = array_filter($bgdatadetail, function ($bgdatadetail) {
                    return strtolower($bgdatadetail['PS_FIELDNAME']) == 'insurance branch';
                });

                $getInsuranceBranch = array_shift($getInsuranceBranch)['PS_FIELDVALUE'];
                list($getInsurance, $getInsuranceBranch) = $this->getInsuranceInfo($bgdata[0]['BG_INSURANCE_CODE'], $getInsuranceBranch);

                $this->view->getInsurance = $getInsurance;
                $this->view->getInsuranceBranch = $getInsuranceBranch;

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['CUST_ID'] = $bgdata[0]["BG_INSURANCE_CODE"];
                $paramProvisionFee['GRUP'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
                $paramProvisionFee['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

                $prorata = $tenor / 360;

                $percentage = $getProvisionFee['provisionFee'];

                $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2);

                $getMinCharge = $this->_db->select()
                    ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                    ->where("CHARGES_ID = ?", "MINPROV01")
                    ->query()->fetch();

                if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                    $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
                }

                $this->view->biayaProvisi = $biaya_provisi;

                $this->view->getFee = $getProvisionFee;
            }

            if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '1') {

                $this->view->getInsurance = $getInsurance;
                $this->view->getInsuranceBranch = $getInsuranceBranch;

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['CUST_ID'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['GRUP'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
                $paramProvisionFee['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

                $prorata = $tenor / 360;

                $percentage = $getProvisionFee['provisionFee'];

                $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2);

                $getMinCharge = $this->_db->select()
                    ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                    ->where("CHARGES_ID = ?", "MINPROV01")
                    ->query()->fetch();

                if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                    $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
                }

                $this->view->biayaProvisi = $biaya_provisi;

                $this->view->getFee = $getProvisionFee;
            }

            if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '2') {

                $this->view->getInsurance = $getInsurance;
                $this->view->getInsuranceBranch = $getInsuranceBranch;

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['CUST_ID'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['GRUP'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
                $paramProvisionFee['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

                $prorata = $tenor / 360;

                $percentage = $getProvisionFee['provisionFee'];

                $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2);

                $getMinCharge = $this->_db->select()
                    ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                    ->where("CHARGES_ID = ?", "MINPROV01")
                    ->query()->fetch();

                if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                    $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
                }

                $this->view->biayaProvisi = $biaya_provisi;

                $this->view->getFee = $getProvisionFee;

                $paramLimit['CUST_ID'] =  $bgdata[0]["CUST_ID"];
                $paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
                $getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

                $this->view->getLineFacility = $getLineFacility;
            }
            // end pengkondisian kontra

            // rekening pembebanan biaya
            $callService = new Service_Account($bgdata[0]['FEE_CHARGE_TO'], '');
            $getAcctInfo = $callService->inquiryAccontInfo();
            $getAcctBalance = $callService->inquiryAccountBalance();

            $cif = $getAcctInfo['cif'];
            $serviceCif = new Service_Account('', '', '', '', '', $cif);
            $getAllAccount = $serviceCif->inquiryCIFAccount();

            if ($getAllAccount['response_code'] != '0000') {
                header('Content-Type: application/json');
                echo json_encode(['result' => 'fail inquiry']);
                return true;
            }

            $acctTrim = ltrim($bgdata[0]['FEE_CHARGE_TO'], '0');

            $selectAccount = array_shift(array_filter($getAllAccount['accounts'], function ($account) use ($acctTrim) {
                if (strpos($account['account_number'], $acctTrim) !== false) return true;
            }));

            $this->view->rekeningPembebananBiaya = array_merge($selectAccount, $getAcctBalance, $getAcctInfo);

            $bgdatasplit = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $bgdatasplit = array_filter($bgdatasplit, function ($acct) {
                if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro')
                    return true;
            });

            $this->view->bgdatasplit = $bgdatasplit;

            $mdAmount = array_sum(array_column($bgdatasplit, 'AMOUNT'));
            $this->view->totalMd = $mdAmount;

            if (!empty($bgdata)) {

                $data = $bgdata['0'];
                $this->view->bgdata = $data;

                //$data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart2 = $data['TIME_PERIOD_START'];
                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd2 = $data['TIME_PERIOD_END'];
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                // $updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'],$this->view->viewDateFormat,$this->view->defaultDateFormat);


                // echo "<code>updateEnd = $updateEnd</code><BR>"; die;

                // BG status
                /*$bgStatus       = $conf["bg"]["status"]["desc"];
                $bgCode         = $conf["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($bgCode), array_values($bgStatus));
                */

                // $arrStatus = array(
                //     '0' => 'New',
                //     '1' => 'Amendment Changes',
                //     '2' => 'Amendment Draft'
                // );

                // $arrStatus = array(
                //     '7'  => 'Canceled',
                //     '20' => 'On Risk',
                //     '21' => 'Off Risk',
                //     '22' => 'Claimed On Process',
                //     '23' => 'Claimed'
                //   );

                $bgStatus       = $conf["bg"]["status"]["desc"];
                $bgCode         = $conf["bg"]["status"]["code"];
                $arrStatus = array_combine(array_values($bgCode), array_values($bgStatus));

                $this->view->arrStatus = $arrStatus;

                $comp = "'" . $this->_custIdLogin . "'";
                $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                        ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                        // ->where('A.ACCT_STATUS = ?','5')
                        ->where("A.CUST_ID IN (" . $comp . ")")
                        ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
                );

                // echo "<pre>";
                // var_dump($acctlist);

                $account = array();
                foreach ($acctlist as $key => $value) {
                    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                    $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                    $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                    $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                    $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                    $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
                }
                // echo "<pre>";
                // var_dump($account);die;
                // $acct = array();
                $i = 0;
                foreach ($account as $key => $value) {

                    $acct[$i]['ACCT_NO'] = $value['account_number'];
                    $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                    $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                    $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                    $acct[$i]['ACCT_NAME'] = $value['account_name'];
                    $i++;
                }

                $this->view->sourceAcct = $acct;

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


                $param = array('CCY_IN' => "");
                // $param = array('ACCT_TYPE' => "My Giro");
                $AccArr = $CustomerUser->getAccountsBG($param);
                //}
                if (!empty($AccArr)) {
                    foreach ($AccArr as $i => $value) {

                        $AccArr[$i]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArr = $AccArr;

                //usd
                $paramUSD = array('CCY_IN' => 'USD');
                $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
                if (!empty($AccArrUSD)) {
                    foreach ($AccArrUSD as $iUSD => $valueUSD) {
                        $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArrUSD = $AccArrUSD;
                // echo '<pre>';
                // echo "<code>AccArrUSD = $AccArrUSD</code>";
                // print_r($AccArrUSD);

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->BG_FORMAT = $data['BG_FORMAT'];
                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                if ($data['BG_STATUS'] == '7' || $data['BG_STATUS'] == '11' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                        a.USER_LOGIN,
                        b.`USER_FULLNAME` AS u_name,
                        c.`BUSER_NAME` AS b_name,
                        a.DATE_TIME,
                        a.BG_REASON,
                        a.HISTORY_STATUS,
                        a.BG_REG_NUMBER
                        
                        
                    FROM
                        T_BANK_GUARANTEE_HISTORY AS a
                        LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                        LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                    WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS IN (10) GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    if ($data['BG_STATUS'] == '10') {
                        $result =  $this->_db->fetchAll($selectQuery);
                    } else {
                        $result = array();
                    }
                    if (!empty($result)) {
                        $this->view->reqrepair = true;
                        $data['REASON'] = $result['0']['BG_REASON'];
                        $this->view->username = ' (By ' . $result['0']['u_name'] . '' . $result['0']['b_name'] . ')';
                    }
                    //var_dump($result);die;


                    $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];



                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->BG_LANGUAGE = $data['BG_LANGUAGE'];
                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                foreach ($arrbgType as $key => $val) {
                    $arrbgType_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_purpose                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_bg_purpose                += Application_Helper_Array::listArray($arrbgType_new, 'ID', 'VALUE');
                $this->view->bgpurposeArr       = $list_bg_purpose;

                // $this->view->arrbgType = $arrbgType;
                $this->view->BG_PURPOSE = $data['USAGE_PURPOSE'];
                $this->view->BG_PURPOSE_LBL = $arrbgType[$data['USAGE_PURPOSE']];
                $this->view->BG_PURPOSE_DESC = $arrbgType[$data['USAGE_PURPOSE']];

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                $this->view->arrBgDoc = $arrbgdoc;

                foreach ($arrbgdoc as $key => $val) {
                    $arrbgdoc_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_document_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_document_type                += Application_Helper_Array::listArray($arrbgdoc_new, 'ID', 'VALUE');
                $this->view->documenttypeArr     = $list_document_type;


                $this->view->GT_DOC_TYPE =  $data['GT_DOC_TYPE'];
                $this->view->GT_DOC_DESC = $arrbgdoc[$data['GT_DOC_TYPE']];
                // echo '<pre>';
                // print_r($bgdocCode); 
                // print_r($bgdocType); 
                // print_r($arrbgdoc); 
                // print_r($data['GT_DOC_TYPE']); 

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
                // echo '<pre>';
                // var_dump($arrbgcg[$data['COUNTER_WARRANTY_TYPE']]);
                // die;

                foreach ($arrbgcg as $key => $val) {
                    $arrbgcg_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_warranty_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_warranty_type                += Application_Helper_Array::listArray($arrbgcg_new, 'ID', 'VALUE');
                $this->view->warrantytypeArr     = $list_warranty_type;


                // echo '<pre>';
                // print_r($bgcgCode); 
                // print_r($bgcgType); 
                // print_r($arrbgcg); 
                // print_r($arrbgcg_new); 
                // print_r($list_warranty_type); 
                $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
                foreach ($arrbgpublish as $key => $val) {
                    $arrbgpublish_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_publist                = array('' => ' --- ' . $this->language->_('Any Value') . ' ---');
                $list_bg_publist                += Application_Helper_Array::listArray($arrbgpublish_new, 'ID', 'VALUE');

                // hilangkan swift
                $list_bg_publist = array_diff_key($list_bg_publist, ['3' => '']);

                $this->view->BG_PUBLISH_ARR     = $list_bg_publist;
                // echo '<pre>';
                // print_r($list_bg_publist); 
                $this->view->BG_PUBLISH = $data['BG_PUBLISH'];
                $this->view->BG_PUBLISH_DESC = $arrbgpublish[$data['BG_PUBLISH']];

                //$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];


                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];

                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];

                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];

                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];

                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->SERVICE = $data['SERVICE'];

                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                $this->view->CHANGE_TYPE = $data['CHANGE_TYPE'];


                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDataEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }

                        if (strtolower($value['PS_FIELDNAME']) == 'currency') {
                            $this->view->currency = $value['PS_FIELDVALUE'];
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];
                // preliminary Member
                $selectMP = $this->_db->select()
                    ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
                $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
                // echo "<code>selectMP = $selectMP</code><BR>";  
                $preliminaryMemberArr = $this->_db->fetchAll($selectMP);
                // echo '<pre>';
                // print_r($preliminaryMemberArr);
                $this->view->preliminaryMemberArr = $preliminaryMemberArr;

                $params = $this->_request->getParams();
                $toa = $params['toa'];

                $BG_UNDERLYING_DOC = $this->_getParam('BG_UNDERLYING_DOC');
                $GT_DOC_NUMBER = $this->_getParam('GT_DOC_NUMBER');
                //print_r($edit);die;

                $selectCity = $this->_db->select()
                    ->from(array('MP' => 'M_CITYLIST'), array('*'));
                $selectCityArr = $this->_db->fetchAll($selectCity);
                $searchCity = array_search($selectcomp[0]['CUST_CITY'], array_column($selectCityArr, 'CITY_CODE'));

                $this->view->arrCity = $selectCityArr[$searchCity];


                $download = $this->_getParam('download');
                if ($download) {

                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    if ($GT_DOC_NUMBER) {
                        $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                    }

                    if ($BG_UNDERLYING_DOC) {
                        // echo "<code>masuk = $GT_DOC_NUMBER</code><BR>"; die;
                        // $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                        $this->_helper->download->file($data['BG_UNDERLYING_DOC'], $attahmentDestination . $data['BG_UNDERLYING_DOC']);
                    }
                }

                // Marginal Deposit View ------------------------
                // Marginal Deposit Principal ---------------
                $bgdatadetailmd = $this->_db->select()
                    ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                if (!empty($bgdatadetailmd)) {
                    foreach ($bgdatadetailmd as $key => $value) {
                        if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
                            $this->view->marginalDepositPercentage =   $value['PS_FIELDVALUE'];
                        }
                    }
                }
                // Marginal Deposit Principal ---------------

                // Marginal Deposit Eksisting ---------------
                $bgdatamdeks = $this->_db->select()
                    ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                    ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('B.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamdeks = $bgdatamdeks;
                // Marginal Deposit Eksisting ---------------

                // Top Up Marginal Deposit ---------------
                $bgdatamd = $this->_db->select()
                    ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                    // ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    // ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamd = $bgdatamd;
                // Top Up Marginal Deposit ---------------
                // Marginal Deposit View ------------------------

                if ($this->_request->isPost()) {
                    $this->validateAll($bgdata[0], $this->_request->getParams(), $_FILES, $getInsurance);

                    if (false) {
                        if (!empty($bgdatasplit)) {
                            foreach ($bgdatasplit as $vl) {
                                //var_dump($vl);
                                $svcAccount = new Service_Account($vl['ACCT'], 'IDR');
                                $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
                                if ($result['balanceactive'] <= $vl['AMOUNT']) {
                                    $err = true;
                                    $err_msg = 'not enought balance ' . $vl['ACCT'];
                                }
                            }
                        }

                        if (!empty($data['FEE_CHARGE_TO'])) {
                            $svcAccount = new Service_Account($vl['FEE_CHARGE_TO'], 'IDR');
                            $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
                            if ($result['response_code'] != '0000' && $result['balance_active'] >= 0) {
                                $err = true;
                                $err_msg = 'not enought balance ' . $vl['ACCT'];
                            }
                        }
                        //die('here');

                        $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
                        if (count($temp) > 1) {
                            if ($temp[0] == 'F' || $temp[0] == 'S') {
                                if ($temp[0] == 'F')
                                    $this->view->error = 1;
                                else
                                    $this->view->success = 1;
                                $msg = '';
                                unset($temp[0]);
                                foreach ($temp as $value) {
                                    if (!is_array($value))
                                        $value = array($value);
                                    $msg .= $this->view->formErrors($value);
                                }
                                $this->view->report_msg = $msg;
                            }
                        }
                        $attahmentDestination     = UPLOAD_PATH . '/document/submit/';
                        $errorRemark             = null;
                        $adapter                 = new Zend_File_Transfer_Adapter_Http();
                        $fileExt                 = "jpeg,jpg,pdf";

                        $sourceFileName = $adapter->getFileName();

                        if ($sourceFileName == null) {
                            $sourceFileName = null;
                            $fileType = null;
                        } else {
                            $sourceFileName = substr(basename($adapter->getFileName()), 0);
                            // echo "<code>sourceFileName = $sourceFileName</code>"; die;	
                            if ($_FILES["uploadSource"]["type"]) {
                                $adapter->setDestination($attahmentDestination);
                                $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                                $fileType = $adapter->getMimeType();
                                $fileInfo = $adapter->getFileInfo();
                                $size = $_FILES["uploadSource"]["size"];
                            } else {
                                $fileType = null;
                                $size = null;
                            }
                        }

                        // $filters = array(
                        //     'updatedate'                        => array('StripTags','StringTrim'),   
                        // );

                        // if ($toa=='1')
                        // {
                        $filters = array(
                            'updatedate'                    => array('StripTags', 'StringTrim'),
                            'CUST_CP_TXT'                   => array('StripTags', 'StringTrim'),
                            'CUST_CONTACT_NUMBER_TXT'       => array('StripTags', 'StringTrim'),
                            'CUST_EMAIL_TXT'                => array('StripTags', 'StringTrim'),
                            'BG_PURPOSE_TXT'                => array('StripTags', 'StringTrim'),
                            'bank_amount_txt'               => array('StripTags', 'StringTrim'),
                            'currency'                      => array('StripTags', 'StringTrim'),
                            'RECIPIENT_CP_TXT'              => array('StripTags', 'StringTrim'),
                            'RECIPIENT_CONTACT_TXT'         => array('StripTags', 'StringTrim'),
                            'RECIPIENT_EMAIL_TXT'           => array('StripTags', 'StringTrim'),
                            'SERVICE_TXT'                   => array('StripTags', 'StringTrim'),
                            'GT_DOC_TYPE_TXT'               => array('StripTags', 'StringTrim'),
                            'GT_DOC_NUMBER_TXT'             => array('StripTags', 'StringTrim'),
                            'underlyingDocument'            => array('StripTags', 'StringTrim'),
                            'GT_DOC_DATE_TXT'               => array('StripTags', 'StringTrim'),
                            'acct_txt'                      => array('StripTags', 'StringTrim'),
                            'COUNTER_WARRANTY_TYPE_TXT'     => array('StripTags', 'StringTrim'),
                            'BG_PUBLISH_TXT'                => array('StripTags', 'StringTrim'),
                            'BG_FORMAT'                     => array('StripTags', 'StringTrim'),
                            'BG_LANGUAGE'                   => array('StripTags', 'StringTrim'),

                        );
                        // }

                        $validators =  array(
                            'acct_txt'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PUBLISH_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_FORMAT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_LANGUAGE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),

                        );

                        // echo "<code>toa = $toa</code><BR>"; die;	
                        if ($toa == '1') {

                            $validators +=  array(
                                'CUST_CP_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'CUST_CONTACT_NUMBER_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'CUST_EMAIL_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'BG_PURPOSE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'bank_amount_txt'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'updatedate'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'currency'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'RECIPIENT_CP_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'RECIPIENT_CONTACT_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'RECIPIENT_EMAIL_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'SERVICE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'GT_DOC_TYPE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'GT_DOC_NUMBER_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'underlyingDocument'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'GT_DOC_DATE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'COUNTER_WARRANTY_TYPE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),

                            );
                        }

                        // echo "<code>masuk1 = $data</code>"; die;	
                        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

                        if ($zf_filter_input->isValid()) {

                            // echo "<code>masuk1 = $data</code><BR>"; die;	

                            if ($zf_filter_input->COUNTER_WARRANTY_TYPE_TXT == '1') {

                                if (!empty($params['flselect_account_manual'])) {
                                    $splitArr = array();
                                    foreach ($params['flselect_account_manual'] as $ky => $vl) {
                                        if ($vl == '') {
                                            $nameAcct = explode('|', $params['flselect_account_number'][$ky]);
                                            $splitArr[$ky]['acct'] = $nameAcct[0];
                                        } else {
                                            $splitArr[$ky]['acct'] = $vl;
                                        }

                                        if ($params['flname_manual'][$ky] == '') {
                                            $splitArr[$ky]['acct_name'] = $params['flname'][$ky];
                                        } else {
                                            $splitArr[$ky]['acct_name'] = $params['flname_manual'][$ky];
                                        }

                                        $splitArr[$ky]['amount'] = $params['flamount'][$ky];
                                        $splitArr[$ky]['flag'] = $params['flselect_own'][$ky];
                                        $splitArr[$ky]['bank_code'] = '999';


                                        // echo "<code>masuk = $params</code>"; 
                                    }
                                    $this->view->acctSplit = $splitArr;
                                }
                            }
                            // echo '<pre>';
                            // print_r($splitArr);die;

                            if ($params['currency_type'] == "1") {

                                // echo "<code>masuk1 = $data</code><BR>"; die;
                                // echo '<pre>';
                                // print_r($params['flselect_own']);die;
                                // foreach($params['flselect_own'] as $i => $val)
                                // {
                                //     if($val == "1")
                                //     {
                                //         $arr_select_account_number = explode("|",$select_account_number[$i]);
                                //         $account_number .= $arr_select_account_number[0].',';
                                //         $account_name .= $arr_select_account_number[1].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                //     else if($val == "2")
                                //     {
                                //         $account_number .= $select_account_number[$i].',';
                                //         $account_name .= $flname_manual[$i].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                // }

                                // echo "<code>account_number = $account_number</code><BR>"; die;
                                // if($params['select_own'] == "1")
                                // {
                                // echo "<code>masuk1 = $data</code><BR>"; die;
                                $select_account_number = $params['flselect_account_number'];
                                $account_number = '';
                                $account_name = '';
                                $amount = '';
                                for ($i = 0; $i <= count($params['flselect_account_number']); $i++) {
                                    $arr_select_account_number = explode("|", $select_account_number[$i]);
                                    $account_number .= $arr_select_account_number[0] . ',';
                                    $account_name .= $arr_select_account_number[1] . ',';
                                    $amount .= $params['flamount'][$i] . ',';
                                }
                                $account_number = substr($account_number, 0, -1);
                                $account_name = substr($account_name, 0, -1);
                                $amount = substr($amount, 0, -1);

                                // }
                                // else
                                // {
                                //     // echo "<code>masuk2 = $data</code><BR>"; die;
                                //     $select_account_number = $params['flselect_account_manual'];
                                //     $flname_manual = $params['flname_manual'];

                                //     $account_number = '';
                                //     $account_name = '';
                                //     $amount = '';
                                //     for($i=0;$i<=count($params['flselect_account_manual']);$i++){
                                //         $account_number .= $select_account_number[$i].',';
                                //         $account_name .= $flname_manual[$i].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                //     $account_number = substr($account_number, 0, -1);
                                //     $account_name = substr($account_name, 0, -1);
                                //     $amount = substr($amount, 0, -1);
                                // }


                                $acct = $params['acct'];
                            } elseif ($params['currency_type'] == "2") {
                                $select_account_numberUSD = $params['select_account_numberUSD'];
                                for ($i = 0; $i <= count($params['select_account_numberUSD']); $i++) {
                                    $arr_account_numberUSD = explode("|", $select_account_numberUSD[$i]);
                                    $account_number .= $arr_account_numberUSD[0] . ',';;
                                    $account_name .= $arr_account_numberUSD[1] . ',';;
                                }

                                $acct = $params['acctUSD'];
                            }

                            $fileTypeMessage = explode('/', $fileType);
                            $fileType =  $fileTypeMessage[1];
                            $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                            $extensionValidator->setMessage("Extension file must be *.pdf");

                            $maxFileSize = "1024000";
                            $size = number_format($size);

                            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                            $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                            $adapter->setValidators(array($extensionValidator, $sizeValidator));

                            if (!empty($fileInfo['uploadSource']["name"])) {

                                if ($adapter->isValid()) {

                                    if ($adapter->receive()) {
                                        // echo "<code>masuk = $data</code>"; die;	
                                        $file_name = $fileInfo['uploadSource']["name"];
                                        // unlink($attahmentDestination.$data['BG_UNDERLYING_DOC']); 

                                    } else {
                                        $errmsgs = $adapter->getMessages();
                                        foreach ($errmsgs as $key => $val) {
                                            $errfield = "error_" . $fieldname;
                                            $errorArray[$errfield] = $val;
                                        }
                                        $this->view->error = true;
                                        $this->view->err_msg = $errmsgs;
                                        // echo '<pre>';
                                        // echo "<code>errors1 = $errors</code>"; die;	
                                        // print_r($errmsgs);die;
                                    }
                                } else {
                                    $this->view->error = true;
                                    $errors = $this->language->_('Extension file must be *.jpg /.jpeg / .pdf');
                                    $this->view->err_msg = $errors;
                                    // echo '<pre>';
                                    // echo "<code>errors2 new= $errors</code>"; die;	
                                    // print_r($errors);die;
                                }
                            } else {
                                $file_name = $data['BG_UNDERLYING_DOC'];
                            }

                            // echo '<pre>';
                            // print_r($fileInfo);die;

                            // echo "<code>file_name = $file_name</code>"; die;
                            try {

                                $this->_db->beginTransaction();

                                if ($zf_filter_input->BG_FORMAT == 1) {
                                    $BG_LANGUAGE = $zf_filter_input->BG_LANGUAGE;
                                } else {
                                    $BG_LANGUAGE = 0;
                                }

                                if ($toa == '1') {
                                    // $insertArr = array(   
                                    //     'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'],$zf_filter_input->BG_PURPOSE_TXT), 
                                    //     'CUST_CP'                           => $zf_filter_input->CUST_CP_TXT,
                                    //     'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER_TXT,
                                    //     'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL_TXT,
                                    //     'BG_AMOUNT'                         => $zf_filter_input->bank_amount_txt, 
                                    //     'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE_TXT,
                                    //     'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP_TXT,
                                    //     'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT_TXT,
                                    //     'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL_TXT,
                                    //     'SERVICE'                           => $zf_filter_input->SERVICE_TXT,
                                    //     'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE_TXT,
                                    //     'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER_TXT,
                                    //     'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE_TXT,
                                    //     'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                    //     'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE_TXT,
                                    //     'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT, 
                                    //     'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                    //     'BG_LANGUAGE'                       => $zf_filter_input->BG_LANGUAGE,
                                    //     'BG_UNDERLYING_DOC'                 => $file_name,   
                                    //     'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                    //     'BG_UPDATEDBY'                      => $this->_userIdLogin, 
                                    // );


                                    $insertArr = array(
                                        'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'], $zf_filter_input->BG_PURPOSE_TXT),
                                        // 'BG_REG_NUMBER'                     => $data['BG_REG_NUMBER'], 
                                        'ACCT_NO'                           => $data['ACCT_NO'],
                                        'BG_NUMBER'                         => $data['BG_NUMBER'],
                                        'BG_SUBJECT'                        => $data['BG_SUBJECT'],
                                        'CUST_ID'                           => $this->_custIdLogin,
                                        'CUST_CP'                           => $zf_filter_input->CUST_CP_TXT,
                                        'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER_TXT,
                                        'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL_TXT,
                                        'BG_STATUS'                         => 2,
                                        'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE_TXT,
                                        'USAGE_PURPOSE_DESC'                => $data['USAGE_PURPOSE_DESC'],
                                        'RECIPIENT_NAME'                    => $data['RECIPIENT_NAME'],
                                        'RECIPIENT_ADDRES'                  => $data['RECIPIENT_ADDRES'],
                                        'RECIPIENT_CITY'                    => $data['RECIPIENT_CITY'],
                                        'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP_TXT,
                                        'RECIPIENT_OFFICE_NUMBER'           => $data['RECIPIENT_OFFICE_NUMBER'],
                                        'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT_TXT,
                                        'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL_TXT,

                                        "PROVISION_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("provision_fee_input")),
                                        "ADM_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("administration_fee_input")),
                                        "STAMP_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("stamp_fee_input")),

                                        'SERVICE'                           => $zf_filter_input->SERVICE_TXT,
                                        'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE_TXT,
                                        'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER_TXT,
                                        'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE_TXT,
                                        'GT_DOC_OTHER'                      => $data['GT_DOC_OTHER'],
                                        'GUARANTEE_TRANSACTION'             => $data['GUARANTEE_TRANSACTION'],
                                        'SP_OBLIGEE_CODE'                   => $data['SP_OBLIGEE_CODE'],
                                        'FILE'                              => $data['FILE'],
                                        'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($zf_filter_input->bank_amount_txt),
                                        'TIME_PERIOD_START'                 => $data['TIME_PERIOD_START'],
                                        'TIME_PERIOD_END'                   => $zf_filter_input->updatedate,

                                        'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE_TXT,
                                        // 'COUNTER_WARRANTY_TYPE'             => $data['COUNTER_WARRANTY_TYPE'], 
                                        'COUNTER_WARRANTY_ACCT_NO'          => rtrim($account_number, ","),
                                        'COUNTER_WARRANTY_ACCT_NAME'        => rtrim($account_name, ","),
                                        'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney(rtrim($amount, ",")),

                                        'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                        'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                        'BG_LANGUAGE'                       => $BG_LANGUAGE,

                                        'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_CREATEDBY'                      => $this->_userIdLogin,
                                        'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                        'BG_REJECT_NOTES'                   => $data['BG_REJECT_NOTES'],
                                        'BG_APPROVE_DOC'                    => $data['BG_APPROVE_DOC'],
                                        'REASON'                            => $data['REASON'],
                                        'BG_CLAIM_PERIOD'                   => $data['BG_CLAIM_PERIOD'],
                                        'BG_CLAIMEDBY'                      => $data['BG_CLAIMEDBY'],
                                        'BG_CLAIM_NOTES'                    => $data['BG_CLAIM_NOTES'],
                                        'BG_CANCELEDBY'                     => $data['BG_CANCELEDBY'],
                                        'BG_CANCEL_NOTES'                   => $data['BG_CANCEL_NOTES'],
                                        'BG_UNDERLYING_DOC'                 => $data['BG_UNDERLYING_DOC'],
                                        'BG_INSURANCE_CODE'                 => $data['BG_INSURANCE_CODE'],
                                        'BG_BRANCH'                         => $data['BG_BRANCH'],
                                        'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT,
                                        'IS_AMENDMENT'                      => 1,
                                        'CHANGE_TYPE'                       => $toa

                                    );
                                    // echo '<pre>';
                                    // print_r($insertArr);die;
                                    // echo "<code>masuk2 = $data</code><BR>"; die;	
                                    $this->_db->insert('TEMP_BANK_GUARANTEE', $insertArr);

                                    // echo "<code>masuk2 = $data</code><BR>"; die;	
                                    if ($zf_filter_input->COUNTER_WARRANTY_TYPE_TXT == '1') {
                                        // echo "<code>COUNTER_WARRANTY_TYPE_TXT = 1</code>"; die;	   
                                        // $this->_db->delete('T_BANK_GUARANTEE_SPLIT','BG_NUMBER = '.$this->_db->quote($numb));

                                        foreach ($splitArr as $ky => $vl) {
                                            $tmparrDetail = array(
                                                'BG_REG_NUMBER'         => $insertArr['BG_REG_NUMBER'], //update reza
                                                'ACCT'              => $vl['acct'],
                                                'BANK_CODE'         => $vl['bank_code'],
                                                'NAME'              => $vl['acct_name'],
                                                'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['amount'], ",")),
                                                'FLAG'              => $vl['flag']
                                            );

                                            // echo '<pre>';
                                            // print_r($tmparrDetail);die;
                                            $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                        }
                                    }

                                    // else
                                    // {
                                    //     echo "<code>COUNTER_WARRANTY_TYPE_TXT = $COUNTER_WARRANTY_TYPE_TXT</code>"; die;
                                    // }

                                } else if ($toa == '2') {

                                    // echo '<pre>';
                                    // print_r($zf_filter_input->updatedate);die;
                                    $insertArr = array(
                                        'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'], $data['USAGE_PURPOSE'][0]),
                                        'ACCT_NO'                           => $data['ACCT_NO'],
                                        'BG_NUMBER'                         => $data['BG_NUMBER'],
                                        'BG_SUBJECT'                        => $data['BG_SUBJECT'],
                                        'CUST_ID'                           => $data['CUST_ID'],
                                        'CUST_CP'                           => $data['CUST_CP'],
                                        'CUST_EMAIL'                        => $data['CUST_EMAIL'],
                                        'CUST_CONTACT_NUMBER'               => $data['CUST_CONTACT_NUMBER'],
                                        'BG_STATUS'                         => 2,
                                        'USAGE_PURPOSE'                     => $data['USAGE_PURPOSE'][0],
                                        'USAGE_PURPOSE_DESC'                => $data['USAGE_PURPOSE_DESC'],
                                        'RECIPIENT_NAME'                    => $data['RECIPIENT_NAME'],
                                        'RECIPIENT_ADDRES'                  => $data['RECIPIENT_ADDRES'],
                                        'RECIPIENT_CITY'                    => $data['RECIPIENT_CITY'],
                                        'RECIPIENT_CP'                      => $data['RECIPIENT_CP'],
                                        'RECIPIENT_OFFICE_NUMBER'           => $data['RECIPIENT_OFFICE_NUMBER'],
                                        'RECIPIENT_EMAIL'                   => $data['RECIPIENT_EMAIL'],
                                        'RECIPIENT_CONTACT'                 => $data['RECIPIENT_CONTACT'],
                                        'SERVICE'                           => $data['SERVICE'],
                                        'GT_DOC_TYPE'                       => $data['GT_DOC_TYPE'],
                                        'GT_DOC_OTHER'                      => $data['GT_DOC_OTHER'],
                                        'GT_DOC_NUMBER'                     => $data['GT_DOC_NUMBER'],
                                        'GT_DOC_DATE'                       => $data['GT_DOC_DATE'],
                                        'GUARANTEE_TRANSACTION'             => $data['GUARANTEE_TRANSACTION'],
                                        'SP_OBLIGEE_CODE'                   => $data['SP_OBLIGEE_CODE'],
                                        'FILE'                              => $data['FILE'],
                                        'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($data['BG_AMOUNT']),
                                        'TIME_PERIOD_START'                 => $data['TIME_PERIOD_START'],
                                        'TIME_PERIOD_END'                    => $data['TIME_PERIOD_END'],
                                        // 'TIME_PERIOD_END'                   => $zf_filter_input->updatedate,

                                        'COUNTER_WARRANTY_TYPE'             => $data['COUNTER_WARRANTY_TYPE'],
                                        'COUNTER_WARRANTY_ACCT_NO'          => $data['COUNTER_WARRANTY_ACCT_NO'],
                                        'COUNTER_WARRANTY_ACCT_NAME'        => $data['COUNTER_WARRANTY_ACCT_NAME'],
                                        'COUNTER_WARRANTY_AMOUNT'           => $data['COUNTER_WARRANTY_AMOUNT'],

                                        // 'FEE_CHARGE_TO'                     => $data['FEE_CHARGE_TO'],
                                        // 'BG_FORMAT'                         => $data['BG_FORMAT'],
                                        // 'BG_LANGUAGE'                       => $data['BG_LANGUAGE'],

                                        'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                        'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                        'BG_LANGUAGE'                       => $BG_LANGUAGE,

                                        "PROVISION_FEE" => 0,
                                        "ADM_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("administration_fee_input")),
                                        "STAMP_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("stamp_fee_input")),

                                        'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_CREATEDBY'                      => $this->_userIdLogin,
                                        'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                        'BG_REJECT_NOTES'                   => $data['BG_REJECT_NOTES'],
                                        'BG_APPROVE_DOC'                    => $data['BG_APPROVE_DOC'],
                                        'REASON'                            => $data['REASON'],
                                        'BG_CLAIM_PERIOD'                   => $data['BG_CLAIM_PERIOD'],
                                        'BG_CLAIMEDBY'                      => $data['BG_CLAIMEDBY'],
                                        'BG_CLAIM_NOTES'                    => $data['BG_CLAIM_NOTES'],
                                        'BG_CANCELEDBY'                     => $data['BG_CANCELEDBY'],
                                        'BG_CANCEL_NOTES'                   => $data['BG_CANCEL_NOTES'],
                                        'BG_UNDERLYING_DOC'                 => $data['BG_UNDERLYING_DOC'],
                                        'BG_INSURANCE_CODE'                 => $data['BG_INSURANCE_CODE'],
                                        'BG_BRANCH'                         => $data['BG_BRANCH'],
                                        // 'BG_PUBLISH'                        => $data['BG_PUBLISH'],
                                        'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT,
                                        'IS_AMENDMENT'                      => 1,
                                        'CHANGE_TYPE'                       => $toa,

                                    );
                                    // echo '<pre>';

                                    // print_r($insertArr);die;
                                    // $this->_db->insert('T_BANK_GUARANTEE', $insertArr);
                                    $this->_db->insert('TEMP_BANK_GUARANTEE', $insertArr);

                                    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

                                        foreach ($bgdatasplit as $ky => $vl) {
                                            $tmparrDetail = array(
                                                'BG_REG_NUMBER'         => $insertArr['BG_REG_NUMBER'], //update reza
                                                'ACCT'              => $vl['ACCT'],
                                                'BANK_CODE'         => $vl['BANK_CODE'],
                                                'NAME'              => $vl['NAME'],
                                                'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['AMOUNT'], ",")),
                                                'FLAG'              => $vl['FLAG']
                                            );

                                            // echo '<pre>';
                                            // print_r($tmparrDetail);die;
                                            // $this->_db->insert('T_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                            $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                        }
                                    }
                                }

                                $historyInsert = array(
                                    'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                    'BG_REG_NUMBER'     => $insertArr['BG_REG_NUMBER'],
                                    'CUST_ID'           => $this->_custIdLogin,
                                    'USER_LOGIN'        => $this->_userIdLogin,
                                    'HISTORY_STATUS'    => 2
                                );

                                $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                                // echo '<pre>';
                                // print_r($insertArr);die;
                                // $where = array('BG_REG_NUMBER = ?' => $numb);
                                // $query = $this->_db->update ( "T_BANK_GUARANTEE", $updArr, $where ); 

                                $bg_number = $data['BG_NUMBER'];
                                $bg_reg_number = $insertArr['BG_REG_NUMBER'];
                                if ($toa == '1') {
                                    Application_Helper_General::writeLog('CCBG', "Amendment Submission Change. BG Number: $bg_number. Register Number: [BG_REG_NUMBER - $bg_reg_number] ");
                                } else if ($toa == '2') {
                                    Application_Helper_General::writeLog('CCBG', "Amendment Submission Reprint Draft. BG Number: $bg_number. Register Number: [BG_REG_NUMBER - $bg_reg_number] ");
                                }

                                $this->_db->commit();
                                $this->setbackURL('/' . $this->_request->getModuleName() . '/');
                                $this->_redirect('/notification/success');
                            } catch (Exception $e) {

                                echo '<pre>';
                                print_r('test');
                                print_r($e);
                                die;
                                echo "<code>masuk3 = $data</code><BR>";
                                die;
                                $this->_db->rollBack();
                                $this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
                            }
                        } else {
                            // echo "<code>masuk4 = $data</code><BR>"; die;	
                            $error = $zf_filter_input->getMessages();
                            // $error = true;



                            //format error utk ditampilkan di view html 
                            $errorArray = null;
                            foreach ($error as $keyRoot => $rowError) {
                                foreach ($rowError as $errorString) {
                                    $errorArray[$keyRoot] = $errorString;
                                }
                            }
                            $this->view->report_msg = $errorArray;
                            // echo '<pre>';
                            // print_r($errorArray);die;
                        }
                    }
                }


                $download = $this->_getParam('download');
                $BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
                //print_r($edit);die;
                if ($BG_APPROVE_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/bg/';
                    //var_dump($attahmentDestination.$data['BG_APPROVE_DOC']);die;
                    $this->_helper->download->file($data['BG_APPROVE_DOC'], $attahmentDestination . $data['BG_APPROVE_DOC']);
                }
            }
        }
    }

    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function Terbilangen($nilai)
    {
        $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 20 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 100) {
            return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilangen($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilangen($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function generateTransactionID($branch, $bgtype)
    {

        // echo "<code>branch = $branch | bgtype = $bgtype  </code><BR>"; die;	
        $currentDate = date("Ymd");
        //$seqNumber   = Application_Helper_General::get_rand_id(4);
        $seqNumber   = mt_rand(1111, 9999);
        $trxId = $seqNumber . $branch . '01' . $bgtype;

        return $trxId;
    }

    public function inquirydepositoAction()
    {

        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        //echo $acct_no;

        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryDeposito('AB', TRUE);
        //var_dump($result);

        $data['data'] = false;
        is_array($result) ? $result :  $result = $data;
        echo json_encode($result);
    }

    public function inquiryaccountbalanceAction()
    {

        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        //echo $acct_no;

        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
        //var_dump($result);
        //die();
        $data['data'] = false;
        is_array($result) ? $result :  $result = $data;
        echo json_encode($result);
    }

    public function inquiryaccountinfoAction()
    {

        //integrate ke core
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $acct_no = $this->_getParam('acct_no');
        $svcAccount = new Service_Account($acct_no, null);
        $result = $svcAccount->inquiryAccontInfo('AI', TRUE);

        if ($result['response_desc'] == 'Success') //00 = success
        {
            $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
            $result2 = $svcAccountCIF->inquiryCIFAccount();

            $filterBy = $result['account_number']; // or Finance etc.
            $new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
                return ($var['account_number'] == $filterBy);
            });

            $singleArr = array();
            foreach ($new as $key => $val) {
                $singleArr = $val;
            }
        }

        if ($result['type_desc'] == 'Deposito') {
            $svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountDeposito->inquiryDeposito();
        } else {
            $svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountBalance->inquiryAccountBalance();
        }
        $return = array_merge($result, $singleArr, $result3);
        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;
        echo json_encode($return);
    }


    public function inquirydepositomdAction()
    {
        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        $bg_reg_number = $this->_getParam('bg_reg_number');
        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryDeposito('AB', TRUE);

        $info = $svcAccount->inquiryAccontInfo("AB");

        $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
        $result2 = $svcAccountCIF->inquiryCIFAccount();

        if ($result["response_code"] != "0000") {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            return 0;
        }

        if ($result["status"] != 1 && $result["status"] != 4) {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            return 0;
        }

        $sqlRekeningJaminanExist = $this->_db->select()
            ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
            ->where('A.ACCT = ?', $result['account_number'])
            ->query()->fetchAll();

        if ($bg_reg_number && $sqlRekeningJaminanExist) {
            foreach ($sqlRekeningJaminanExist as $key => $value) {
                if ($value["BG_REG_NUMBER"] == $bg_reg_number) {
                    $result["check_exist_split"] = 0;
                } else {
                    $result["check_exist_split"] = 1;
                }
            }
        } else {
            $result["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;
        }

        $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
        $result2 = $svcAccountCIF->inquiryCIFAccount();
        $filterBy = $result['account_number']; // or Finance etc.
        $check_prod_type = $this->_db->select()
            ->from("M_PRODUCT_TYPE")
            ->where("PRODUCT_CODE = ?", $result["account_type"])
            ->query()->fetch();

        $result["check_product_type"] = 0;
        if (!empty($check_prod_type)) {
            $result["check_product_type"] = 1;
        }

        $data['data'] = false;
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($result);
    }

    public function inquiryaccountinfomdAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $acct_no = $this->_getParam('acct_no');
        $svcAccount = new Service_Account($acct_no, null);
        $result = $svcAccount->inquiryAccontInfo('AI', TRUE);

        if ($result['response_desc'] == 'Success') //00 = success
        {
            $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
            $result2 = $svcAccountCIF->inquiryCIFAccount();

            $filterBy = $result['account_number']; // or Finance etc.
            $new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
                return ($var['account_number'] == $filterBy);
            });

            $singleArr = array();
            foreach ($new as $key => $val) {
                $singleArr = $val;
            }
        } else {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            return 0;
        }

        if ($singleArr['type_desc'] == 'Deposito') {
            $svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountDeposito->inquiryDeposito();

            if ($result3["response_code"] != "0000") {
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode(array_merge($result3, $singleArr, $result));
                return 0;
            }

            if ($result3["status"] != 1 && $result3["status"] != 4) {
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode(array_merge($result3, $singleArr, $result));
                return 0;
            }

            $sqlRekeningJaminanExist = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                ->where('A.ACCT = ?', $result['account_number'])
                ->query()->fetchAll();

            $result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;

            $get_product_type = current($new)["type"];

            $check_prod_type = $this->_db->select()
                ->from("M_PRODUCT_TYPE")
                ->where("PRODUCT_CODE = ?", $result3["account_type"])
                ->query()->fetch();

            $result3["check_product_type"] = 0;
            if (!empty($check_prod_type)) {
                $result3["check_product_type"] = 1;
            }
        } else {
            $svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountBalance->inquiryAccountBalance();

            if ($result3["status"] != 1 && $result3["status"] != 4) {
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($result3);
                return 0;
            }

            $get_product_type = current($new)["type"];
            $check_prod_type = $this->_db->select()
                ->from("M_PRODUCT_TYPE")
                ->where("PRODUCT_CODE = ?", $result3["account_type"])
                ->query()->fetch();

            $result3["check_product_type"] = 0;
            if (!empty($check_prod_type)) {
                $result3["check_product_type"] = 1;
            }
        }

        $return = array_merge($result, $singleArr, $result3);

        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($return);
    }

    public function inquiryaccountbalancemdAction()
    {
        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
        $info = $svcAccount->inquiryAccontInfo("AB");

        if ($result["status"] != 1 && $result["status"] != 4) {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            return 0;
        }

        $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
        $filterBy = $result['account_number']; // or Finance etc.
        $check_prod_type = $this->_db->select()
            ->from("M_PRODUCT_TYPE")
            ->where("PRODUCT_CODE = ?", $result["account_type"])
            ->query()->fetch();

        $result["check_product_type"] = 0;
        if (!empty($check_prod_type)) {
            $result["check_product_type"] = 1;
        }
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($result);
        die();

        $data['data'] = false;
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($result);
    }

    private function getCurrency()
    {
        $getCurrency = $this->_db->select()
            ->from('M_MINAMT_CCY')
            ->query()->fetchAll();

        $result = array_map(function ($item) {
            return $item['CCY_ID'];
        }, $getCurrency);

        $resultMin = array_map(function ($item) {
            return $item['MIN_TX_AMT'];
        }, $getCurrency);

        return [array_combine($result, $result), array_combine($result, $resultMin)];
    }

    private function getGuarantedTransactions($bgregnumber)
    {
        $getGuarantedTrasactions = $this->_db->select()
            ->from('T_BANK_GUARANTEE_UNDERLYING')
            ->where('BG_REG_NUMBER = ?', $bgregnumber)
            ->query()->fetchAll();

        $result = $getGuarantedTrasactions;

        return $result;
    }

    private function getInsuranceInfo($insuranceCode, $insuranceBranch)
    {
        $getCustomer = $this->_db->select()
            ->from(['MC' => 'M_CUSTOMER'], [
                'CUST_NAME',
                'CUST_ID',
                'FEE_DEBITED' => new Zend_Db_Expr("(SELECT FEE_DEBITED FROM M_CUST_LINEFACILITY MCL WHERE MCL.CUST_ID = MC.CUST_ID LIMIT 1)")
            ])
            ->where('CUST_ID = ?', $insuranceCode)
            ->query()->fetch();

        $getInsuranceBranch = $this->_db->select()
            ->from('M_INS_BRANCH')
            ->where('INS_BRANCH_CODE = ?', $insuranceBranch)
            ->query()->fetch();

        return [$getCustomer, $getInsuranceBranch];
    }

    public function filteracctAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $acct = $this->_request->getParam('acct');

        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


        $type = array('D', 'S', '20', '10');
        $param = array('ACCT_TYPE' => $type);
        $AccArr = $CustomerUser->getAccountsBG($param);
        $newArr2 = [];

        $getAllProduct = $this->_db->select()
            ->from("M_PRODUCT")
            ->query()->fetchAll();

        foreach ($AccArr as $keyArr => $arrAcct) {

            if ($arrAcct['ACCT_TYPE'] == '10' || strtolower($arrAcct['ACCT_TYPE']) == 's') continue;

            $accountTypeCheck = false;
            $productTypeCheck = false;

            $svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
            $result = $svcAccount->inquiryAccountBalance();
            if ($result['response_code'] == '0000') {
                $accountType = $result['account_type'];
                $productType = $result['product_type'];
            } else {
                $result = $svcAccount->inquiryDeposito();

                $accountType = $result['account_type'];
                $productType = $result['product_type'];
            }

            foreach ($getAllProduct as $key => $value) {
                # code...
                if ($value['PRODUCT_CODE'] == $accountType) {
                    $accountTypeCheck = true;
                };

                if ($value['PRODUCT_PLAN'] == $productType) {
                    $productTypeCheck = true;
                };
            }
            if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
        }

        foreach ($newArr2 as $key => $value) {
            echo "<option value='" . $value['ACCT_NO'] . "' " . (trim($acct) == trim($value['ACCT_NO']) ? 'selected' : '') . ">" . $value['ACCT_NO'] . " (" . $value['CCY_ID'] . ") / " . $value['ACCT_NAME'] . "</option>";
        }
    }

    public function mdacctAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $acct = $this->_request->getParam('acct');

        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $AccArr = $CustomerUser->getAccountsBG();
        $newArr2 = [];

        $getAllProduct = $this->_db->select()
            ->from("M_PRODUCT")
            ->query()->fetchAll();

        foreach ($AccArr as $keyArr => $arrAcct) {

            $accountTypeCheck = false;
            $productTypeCheck = false;

            $svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
            $result = $svcAccount->inquiryAccountBalance();
            if ($result['response_code'] == '0000') {
                $accountType = $result['account_type'];
                $productType = $result['product_type'];
            } else {
                $result = $svcAccount->inquiryDeposito();

                $accountType = $result['account_type'];
                $productType = $result['product_type'];
            }

            foreach ($getAllProduct as $key => $value) {
                # code...
                if ($value['PRODUCT_CODE'] == $accountType) {
                    $accountTypeCheck = true;
                };

                if ($value['PRODUCT_PLAN'] == $productType) {
                    $productTypeCheck = true;
                };
            }
            if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
        }

        echo "<option value=''></option>";

        foreach ($newArr2 as $key => $value) {
            echo "<option value='" . $value['ACCT_NO'] . "'>" . $value['ACCT_NO'] . "</option>";
        }
    }

    public function mdfcAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

        if ($bgRegNumber == null) {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'nothing']);
            return true;
        }

        $bgdatasplit = $this->_db->select()
            ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
            ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
            ->where('A.BG_REG_NUMBER = ?', $bgRegNumber)
            ->query()->fetchAll();

        $bgdatasplit = array_filter($bgdatasplit, function ($acct) {
            if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro' && (strtolower($acct['ACCT_TYPE']) != 'giro'))
                return true;
        });

        $mdAmount = 0;

        $htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

        foreach ($bgdatasplit as $key => $value) {

            $callService = new Service_Account($value['ACCT'], '');
            $cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

            if (strtolower($value['ACCT_DESC']) == 'escrow') {
                $inqbalance = $callService->inquiryAccountBalance();
                // if (intval($inqbalance['available_balance']) == 0) {
                //     // hapus data atau ??
                //     continue;
                // } else $mdAmount += $value['AMOUNT'];
                $mdAmount += $value['AMOUNT'];
            } else {
                // jika deposito
                if (strtolower($cekAcctType) == 'deposito') {
                    $inqbalance = $callService->inquiryDeposito();

                    // if (strtolower($inqbalance['hold_description']) == 'n') {
                    //     // hapus data atau ??
                    //     continue;
                    // } else $mdAmount += $value['AMOUNT'];
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika deposito

                // jika tabungan
                else {
                    $inqbalance = $callService->inqLockSaving();

                    $cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
                        return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
                    }));

                    // if (!$cekSaving) {
                    //     // hapus data atau
                    //     continue;
                    // } else $mdAmount += $value['AMOUNT'];
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika tabungan
            }

            if ($inqbalance['varian_rate']) {
                if (floatval($inqbalance['varian_rate']) !== floatval('0')) {
                    $iconSpecialRate = '<i id="icwarning'.str_pad($value['ACCT'], 16, '0', STR_PAD_LEFT).'" class="fas fa-info pt-1 mr-2" title="Special Rate" style="background-color: white;color: red;width: 23px;height: 23px;text-align: center;border: 1px solid red;border-radius: 50px;font-size: 12px;float: right;"></i>';
                }
            }

            $htmlResult .= "
            <tr>
                <td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
                <td>" . str_pad($value['ACCT'], 16, '0', STR_PAD_LEFT) . "</td>
                <td>" . $value['NAME'] . "</td>
                <td>" . ($value['ACCT_DESC'] ?: $value['ACCT_DESC_MCA']) . "</td>
                <td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($value['CCY_ID'] ?: $value['CCY_ID_MCA'])) . "</td>
                <td>" . number_format($value['AMOUNT'], 2) . ($iconSpecialRate ?: '') . "</td>
            </tr>";
        }

        $htmlResult .= "</tbody></table>";
        $htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksis'></input>";

        echo $htmlResult;
    }

    public function mdfcoldAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

        if ($bgRegNumber == null) {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'nothing']);
            return true;
        }

        $getBgData = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE', ['CUST_ID'])
            ->where('BG_REG_NUMBER = ?', $bgRegNumber)
            ->query()->fetch();

        if (!$getBgData) {
            $getBgData = $this->_db->select()
                ->from('T_BANK_GUARANTEE', ['CUST_ID'])
                ->where('BG_REG_NUMBER = ?', $bgRegNumber)
                ->query()->fetch();
        }

        $bgdatasplit = $this->_db->select()
            ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
            ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = "' . $getBgData['CUST_ID'] . '"', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
            ->where('A.BG_REG_NUMBER = ?', $bgRegNumber)
            ->where('A.IS_TRANSFER = ?', '1')
            ->query()->fetchAll();

        $bgdatasplit = array_filter($bgdatasplit, function ($acct) {
            if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro' && (strtolower($acct['ACCT_TYPE']) != 'giro'))
                return true;
        });

        $mdAmount = 0;

        $htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

        foreach ($bgdatasplit as $key => $value) {

            $callService = new Service_Account($value['ACCT'], '');
            $cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

            if (strtolower($value['ACCT_DESC']) == 'escrow') {
                $inqbalance = $callService->inquiryAccountBalance();
                // if (intval($inqbalance['available_balance']) == 0) {
                //     // hapus data atau ??
                //     continue;
                // } else $mdAmount += $value['AMOUNT'];
                $mdAmount += $value['AMOUNT'];
            } else {
                // jika deposito
                if (strtolower($cekAcctType) == 'deposito') {
                    $inqbalance = $callService->inquiryDeposito();

                    // if (strtolower($inqbalance['hold_description']) == 'n') {
                    //     // hapus data atau ??
                    //     continue;
                    // } else $mdAmount += $value['AMOUNT'];
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika deposito

                // jika tabungan
                else {
                    $inqbalance = $callService->inqLockSaving();

                    $cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
                        return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
                    }));

                    // if (!$cekSaving) {
                    //     // hapus data atau
                    //     continue;
                    // } else $mdAmount += $value['AMOUNT'];
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika tabungan
            }

            if (strtolower($cekAcctType) == 'deposito') {
                if (floatval($inqbalance['varian_rate']) !== floatval('0')) {
                    $iconSpecialRate = '<i id="icwarning'.str_pad($value['ACCT'], 16, '0', STR_PAD_LEFT).'" class="fas fa-info pt-1 mr-2" title="Special Rate" style="background-color: white;color: red;width: 23px;height: 23px;text-align: center;border: 1px solid red;border-radius: 50px;font-size: 12px;float: right;"></i>';
                }
            } else {
                $iconSpecialRate = false;
            }

            $htmlResult .= "
            <tr>
                <td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
                <td>" . $value['ACCT'] . "</td>
                <td>" . $value['NAME'] . "</td>
                <td>" . ($value['ACCT_DESC'] ?: $value['ACCT_TYPE'] ?: $value['ACCT_DESC_MCA'])  . "</td>
                <td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($value['CCY_ID'] ?: $value['CCY_ID_MCA'])) . "</td>
                <td>" . number_format($value['AMOUNT'], 2) . ($iconSpecialRate ?: '') . "</td>
            </tr>";
        }

        if (!$bgdatasplit) {
            $htmlResult .= "
            <tr>
                <td colspan='6' class='text-center'>" . $this->language->_('Tidak ada marginal deposit') . "</td>
            </tr>";
        }

        $htmlResult .= "</tbody></table>";
        $htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksis'></input>";

        echo $htmlResult;
    }

    public function mdfcoldtAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;
        $buttonescrow = $this->_request->getParam('buttonescrow') ?: null;

        if ($bgRegNumber == null) {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'nothing']);
            return true;
        }

        $getBgData = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE', ['CUST_ID'])
            ->where('BG_REG_NUMBER = ?', $bgRegNumber)
            ->query()->fetch();

        if (!$getBgData) {
            $getBgData = $this->_db->select()
                ->from('T_BANK_GUARANTEE', ['CUST_ID'])
                ->where('BG_REG_NUMBER = ?', $bgRegNumber)
                ->query()->fetch();
        }

        $bgdatasplit = $this->_db->select()
            ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
            ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = "' . $getBgData['CUST_ID'] . '"', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
            ->where('A.BG_REG_NUMBER = ?', $bgRegNumber)
            ->query()->fetchAll();

        $bgdatasplit = array_filter($bgdatasplit, function ($acct) {
            if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro' && (strtolower($acct['ACCT_TYPE']) != 'giro'))
                return true;
        });

        $mdAmount = 0;

        $htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

        foreach ($bgdatasplit as $key => $value) {

            $callService = new Service_Account($value['ACCT'], '');
            $cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

            if (strtolower($value['ACCT_DESC']) == 'escrow') {
                $inqbalance = $callService->inquiryAccountBalance();
                // if (intval($inqbalance['available_balance']) == 0) {
                //     // hapus data atau ??
                //     continue;
                // } else $mdAmount += $value['AMOUNT'];
                $mdAmount += $value['AMOUNT'];
            } else {
                // jika deposito
                if (strtolower($cekAcctType) == 'deposito') {
                    $inqbalance = $callService->inquiryDeposito();

                    // if (strtolower($inqbalance['hold_description']) == 'n') {
                    //     // hapus data atau ??
                    //     continue;
                    // } else $mdAmount += $value['AMOUNT'];
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika deposito

                // jika tabungan
                else {
                    $inqbalance = $callService->inqLockSaving();

                    $cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
                        return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
                    }));

                    // if (!$cekSaving) {
                    //     // hapus data atau
                    //     continue;
                    // } else $mdAmount += $value['AMOUNT'];
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika tabungan
            }

            $htmlResult .= "
            <tr>
                <td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
                <td>" . $value['ACCT'] . "</td>
                <td>" . $value['NAME'] . "</td>
                <td>" . ($value['ACCT_DESC'] ?: $value['ACCT_TYPE'] ?: $value['ACCT_DESC_MCA'])  . "</td>
                <td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($value['CCY_ID'] ?: $value['CCY_ID_MCA'])) . "</td>
                <td align=\"center\">" . number_format($value['AMOUNT'], 2) . (strtolower($value['ACCT_DESC']) == 'escrow' && $buttonescrow ? '<button type="button" class="btnwhite ml-1" style="min-width: 20px !important;height: 20px !important;text-transform: inherit;color: #8282E6 !important" onclick="openwindow(`/bgreport/detail/escrowdetail/bgnumb/${window.location.href.split(\'/\')[7]}`, 600, 400)">i</button>' : "") . "</td>
            </tr>";
        }

        if (!$bgdatasplit) {
            $htmlResult .= "
            <tr>
                <td colspan='6' class='text-center'>" . $this->language->_('Tidak ada marginal deposit') . "</td>
            </tr>";
        }

        $htmlResult .= "</tbody></table>";
        $htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksis'></input>";

        echo $htmlResult;
    }

    public function mdfcnewAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;
        $validate = $this->_request->getParam('validate') === 'true' ? true : false;
        $saldo = $this->_request->getParam('saldo') === 'true' ? true : false;

        if ($bgRegNumber == null) {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'nothing']);
            return true;
        }

        $getBgData = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE', ['CUST_ID'])
            ->where('BG_REG_NUMBER = ?', $bgRegNumber)
            ->query()->fetch();

        if (!$getBgData) {
            $getBgData = $this->_db->select()
                ->from('T_BANK_GUARANTEE', ['CUST_ID'])
                ->where('BG_REG_NUMBER = ?', $bgRegNumber)
                ->query()->fetch();
        }

        $bgdatasplit = $this->_db->select()
            ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
            ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($getBgData['CUST_ID']) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
            ->where('A.BG_REG_NUMBER = ?', $bgRegNumber)
            ->where('A.IS_TRANSFER != ?', '1')
            ->query()->fetchAll();

        $bgdatasplit = array_filter($bgdatasplit, function ($acct) {
            if (strtolower($acct['ACCT_DESC']) != 'escrow')
                return true;
        });

        $mdAmount = 0;

        $htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Rek Sendiri') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

        if (!$bgdatasplit) {
            $htmlResult .= "
            <tr>
                <td colspan='6' class='text-center'>" . $this->language->_('Tidak ada penambahan marginal deposit') . "</td>
            </tr>";
        }
        foreach ($bgdatasplit as $key => $value) {

            if ($validate) {
                try {
                    //code...
                    $validateAcct = $this->validateAcct($value, $saldo);
                } catch (\Throwable $th) {
                    //throw $th;
                    Zend_Debug::dump($th);
                    die();
                }
            }

            // $callService = new Service_Account($value['ACCT'], '');
            // $cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

            // if (strtolower($value['ACCT_DESC']) == 'escrow') {
            //     $inqbalance = $callService->inquiryAccountBalance();
            // } else {
            //     if (strtolower($cekAcctType) == 'deposito') {
            //         $inqbalance = $callService->inquiryDeposito();
            //     } else {
            //         $inqbalance = $callService->inqLockSaving();
            //     }
            // }
            
            // if ($inqbalance['varian_rate'] && strtolower($cekAcctType) == 'deposito') {
            //     if (floatval($inqbalance['varian_rate']) !== floatval('0')) {
            //         $iconSpecialRate = '<i id="icwarning'.str_pad($value['ACCT'], 16, '0', STR_PAD_LEFT).'" class="fas fa-info pt-1 mr-2" title="Special Rate" style="background-color: white;color: red;width: 23px;height: 23px;text-align: center;border: 1px solid red;border-radius: 50px;font-size: 12px;float: right;"></i>';
            //     }
            // }
            
            $htmlResult .= "
            <tr>
                <td>" . ($value['FLAG'] ? 'Y' : 'T') . "</td>
                <td>" . $value['ACCT'] . "</td>
                <td>" . $value['NAME'] . "</td>
                <td>" . ($value['ACCT_DESC'] ?: $value['ACCT_TYPE'] ?: $value['ACCT_DESC_MCA']) . "</td>
                <td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($value['CCY_ID'] ?: $value['CCY_ID_MCA'])) . "</td>
                <td>" . number_format($value['AMOUNT'], 2) . ($validate ? ($validateAcct['error'] ? '&nbsp <i class="fas fa-info pt-1 mr-2" data-toggle="tooltip" data-placement="top" title="' . $validateAcct['message'] . '" style="background-color: white;color: red;width: 23px;height: 23px;text-align: center;border: 1px solid red;border-radius: 50px;font-size: 12px;float: right;"></i>' : '') : '') ."</td>
            </tr>";

            $mdAmount += $value['AMOUNT'];
        }

        $htmlResult .= "</tbody></table>";
        $htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksisnew'></input>";

        echo $htmlResult;
    }

    /**
     * @param array $acctDetail
     * 
     * @return [type]
     */
    public function validateAcct(array $acctDetail, $saldo = false): array
    {
        $tipeRekening = strtolower($acctDetail['ACCT_DESC']) ?: strtolower($acctDetail['ACCT_TYPE']);
        $amount = ($saldo) ? floatval($acctDetail['AMOUNT']) : 0;

        if ($tipeRekening === 'giro' || $tipeRekening === 'tabungan') {

            $callService = new Service_Account($acctDetail['ACCT'], '');

            $result = $callService->inquiryAccountBalance();

            if (!$result) {
                return [
                    'error' => true,
                    'message' => 'server didn\'t response'
                ];
            }

            if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
                if ($result['status'] != '1' && $result['status'] != '4') {
                    return [
                        'error' => true,
                        'message' => $this->language->_('Status rekening tidak aktif')
                    ];
                }

                $getProductType = $this->_db->select()
                    ->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
                    ->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
                    ->query()->fetchAll();

                if (empty($getProductType)) {
                    return [
                        'error' => true,
                        'message' => $this->language->_('Product type tidak terdaftar di product management')
                    ];
                }

                if ($saldo && $result['available_balance'] < $amount) {
                    return [
                        'error' => true,
                        'message' => $this->language->_('Saldo tidak mencukupi'),
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => $result['response_desc']
                ];
            }

            return [
                'error' => false,
                'message' => ''
            ];
        }

        if ($tipeRekening === 'deposito') {

            $callService = new Service_Account($acctDetail['ACCT'], '');
            $result = $callService->inquiryDeposito();

            if (!$result) {
                return [
                    'error' => true,
                    'message' => 'server didn\'t response'
                ];
            }

            if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
                if ($result['status'] != '1' && $result['status'] != '4') {
                    return [
                        'error' => true,
                        'message' => $this->language->_('Status rekening tidak aktif')
                    ];
                }

                $getProductType = $this->_db->select()
                    ->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
                    ->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
                    ->query()->fetchAll();

                if (empty($getProductType)) {
                    return [
                        'error' => true,
                        'message' => $this->language->_('Product type tidak terdaftar di product management')
                    ];
                }

                if ($result['aro_status'] != 'Y') {
                    return [
                        'error' => true,
                        'message' => $this->language->_('Type bukan ARO')
                    ];
                }

                if ($result['varian_rate'] != '0') {
                    return [
                        'error' => true,
                        'message' => $this->language->_('Special Rate')
                    ];
                }

                if ($result['hold_description'] == 'Y') {
                    return [
                        'error' => true,
                        'message' => $this->language->_('Account Number sudah digunakan sebagai jaminan')
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => $result['response_desc']
                ];
            }

            return [
                'error' => false,
                'message' => ''
            ];
        }
    }

    public function checkcompleteAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $acct = $this->_request->getParam('acct') ?: null;
        $name = $this->_request->getParam('acctName') ?: null;

        if ($acct == null) {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'nothing']);
            return true;
        }

        // trim 0 di kiri
        $acctTrim = ltrim($acct, '0');

        $callService = new Service_Account($acct, '');
        $getAcctInfo = $callService->inquiryAccontInfo();

        if ($getAcctInfo['response_code'] != '0000') {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'fail inquiry']);
            return true;
        }

        // inquiry CIF
        $cif = $getAcctInfo['cif'];
        $serviceCif = new Service_Account('', '', '', '', '', $cif);
        $getAllAccount = $serviceCif->inquiryCIFAccount();

        if ($getAllAccount['response_code'] != '0000') {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'fail inquiry']);
            return true;
        }

        $selectAccount = array_shift(array_filter($getAllAccount['accounts'], function ($account) use ($acctTrim) {
            if (strpos($account['account_number'], $acctTrim) !== false) return true;
        }));

        $result = array_merge($getAcctInfo, $selectAccount);

        // ambil data yang diperlukan
        $intersect = [
            'type' => '',
            "type_desc" => '',
            "currency" => '',
            "product_type" => '',
            "account_name" => '',
            "account_number" => '',
        ];

        if ($name && strtolower($getAcctInfo['account_name']) !== strtolower($name)) {
            $intersect = array_diff_key($intersect, ['account_name' => '']);
        }

        list($error, $errorMessage, $amount) = $this->checkAccount($result, $name);

        $result = array_intersect_key($result, $intersect);
        $result = array_merge($result, ['error' => $error, 'error_message' => $errorMessage, 'amount' => $amount ?: 0]);

        header('Content-Type: application/json');
        echo json_encode($result);
        return true;
    }

    public function checkcomplete2Action()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $acct = $this->_request->getParam('acct') ?: null;
        $name = $this->_request->getParam('acctName') ?: null;

        if ($acct == null) {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'nothing']);
            return true;
        }

        // trim 0 di kiri
        $acctTrim = ltrim($acct, '0');

        $callService = new Service_Account($acct, '');
        $getAcctInfo = $callService->inquiryAccontInfo();

        if ($getAcctInfo['response_code'] != '0000') {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'fail inquiry']);
            return true;
        }

        // inquiry CIF
        $cif = $getAcctInfo['cif'];
        $serviceCif = new Service_Account('', '', '', '', '', $cif);
        $getAllAccount = $serviceCif->inquiryCIFAccount();

        if ($getAllAccount['response_code'] != '0000') {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'fail inquiry']);
            return true;
        }

        $selectAccount = array_shift(array_filter($getAllAccount['accounts'], function ($account) use ($acctTrim) {
            if (strpos($account['account_number'], $acctTrim) !== false) return true;
        }));

        $result = array_merge($getAcctInfo, $selectAccount);

        // ambil data yang diperlukan
        $intersect = [
            'type' => '',
            "type_desc" => '',
            "currency" => '',
            "product_type" => '',
            "account_name" => '',
            "account_number" => '',
        ];

        if ($name && strtolower($getAcctInfo['account_name']) !== strtolower($name)) {
            $intersect = array_diff_key($intersect, ['account_name' => '']);
        }

        list($error, $errorMessage, $amount) = $this->checkAccount2($result, $name);

        $result = array_intersect_key($result, $intersect);
        $result = array_merge($result, ['error' => $error, 'error_message' => $errorMessage, 'amount' => $amount ?: 0]);

        header('Content-Type: application/json');
        echo json_encode($result);
        return true;
    }

    public function getLampiranPendukung($bgregnumber)
    {
        $lampiranPendukungs = $this->_db->select()
            ->from('T_BANK_GUARANTEE_FILE')
            ->where('BG_REG_NUMBER = ?', $bgregnumber)
            ->query()->fetchAll();

        foreach ($lampiranPendukungs as $key => $value) {
            $explodeTemp = explode('_', $value['BG_FILE']);
            $cleanText = str_replace([$explodeTemp[0], $explodeTemp[1], $explodeTemp[2], $explodeTemp[3]], '', $value['BG_FILE']);
            $trimText = trim($cleanText, '_');
            $lampiranPendukungs[$key]['NAMA_FILE'] = $trimText;
        }

        return $lampiranPendukungs;
    }

    public function checkAccount($infoAcct, $name = null)
    {
        $errorMessage = '';
        $error = false;

        $account_name = $infoAcct['account_name'];

        if (strtolower($name) != strtolower($account_name) && $name) {
            $error = true;
            $errorMessage = $this->language->_('Nama rekening tidak sama');

            return [$error, $errorMessage];
        }

        // cek status
        if ($infoAcct['status'] != 1 && $infoAcct['status'] != 4) {
            $error = true;
            $errorMessage = $this->language->_('Status rekening tidak aktif');

            return [$error, $errorMessage];
        }

        // jika rekening giro dan tabunga return value
        if (strtolower($infoAcct['type']) == 'd' || strtolower($infoAcct['type']) == 's') {
            return [$error, $errorMessage];
        }

        // deposito cek
        $callService = new Service_Account($infoAcct['account_number'], '');
        $getAcctInfoDep = $callService->inquiryDeposito();

        if ($getAcctInfoDep['response_code'] !== '0000') {
            $error = true;
            $errorMessage = $this->language->_('Gagal inquiry');

            return [$error, $errorMessage, $getAcctInfoDep['balance']];
        }

        // cek deposito aro atau bukan
        if (strtolower($getAcctInfoDep['aro_status']) != 'y') {
            $error = true;
            $errorMessage = $this->language->_('Bukan Aro');

            return [$error, $errorMessage, $getAcctInfoDep['balance']];
        }

        // varian rate bukan 0
        if (floatval($getAcctInfoDep['varian_rate']) !== floatval('0')) {
            $error = true;
            $errorMessage = $this->language->_('Varian rate bukan 0');

            return [$error, $errorMessage, $getAcctInfoDep['balance']];
        }

        // cek apakah hold atau tidak
        if (strtolower($getAcctInfoDep['hold_description']) !== 'n') {
            $error = true;
            $errorMessage = $this->language->_('Rekening sudah dijaminkan');

            return [$error, $errorMessage, $getAcctInfoDep['balance']];
        }

        return [$error, $errorMessage, $getAcctInfoDep['balance']];
    }

    public function checkAccount2($infoAcct, $name = null)
    {
        $errorMessage = '';
        $error = false;

        $account_name = $infoAcct['account_name'];

        if (strtolower($name) != strtolower($account_name) && $name) {
            $error = true;
            $errorMessage = $this->language->_('Nama rekening tidak sama');

            return [$error, $errorMessage];
        }

        // cek status
        if ($infoAcct['status'] != 1 && $infoAcct['status'] != 4) {
            $error = true;
            $errorMessage = $this->language->_('Status rekening tidak aktif');

            return [$error, $errorMessage];
        }

        // jika rekening giro dan tabungan return value
        if (strtolower($infoAcct['type']) == 'd') {
            return [$error, $errorMessage];
        }

        // jika rekening bukan giro
        if (strtolower($infoAcct['type']) != 'd') {
            $error = true;
            $errorMessage = $this->language->_('Transfer Dana hanya dapat ditujukan ke Rekening SACA');
            return [$error, $errorMessage];
        }
    }

    private function validateAll($bgdata, $request, $files, $insurace = null)
    {
        // Zend_Debug::dump([$bgdata, $request, $files, $insurace]);
        // die();

        $saveResult = [];

        $attachmentDestination = UPLOAD_PATH . '/document/submit/'; // save file path

        $tipeUsulan = $request['toa']; // 1 : amandemen isi , 2 : amandemen format
        $saveResult['tipeUsulan'] = $tipeUsulan;

        $rekeningPembebananBiaya = $request['acct_txt'] ?: '-'; // rekening pembebanan biaya
        $saveResult['rekeningPembebananBiaya'] = $rekeningPembebananBiaya;

        $bentukPenerbitan = $request['BG_PUBLISH_TXT']; // 1 : sertifikat, 2 : elektronik
        $formatBankGaransi = $request['BG_FORMAT']; // 1 : bank standard, 2 : spesial format
        if ($formatBankGaransi == '2') {
            $getSpecialFormatFile = $files['sepcialFormatFile'];
            $newName = date('dmy') . '_' . date('his') . '_' . uniqid('amd') . '_' . $getSpecialFormatFile['name'];
            $getTmpName = $getSpecialFormatFile['tmp_name'];

            $saveResult['specialFormatFile'] = $newName;

            move_uploaded_file($getTmpName, $attachmentDestination . $newName);
        }

        if ($tipeUsulan == '1') {
            switch ($bgdata['COUNTER_WARRANTY_TYPE']) {
                case '1':
                    $resultUsulan = $this->validateFc($bgdata, $request, $files);
                    break;

                case '2':
                    $resultUsulan = $this->validateLf($bgdata, $request, $files);
                    break;

                case '3':
                    $resultUsulan = $this->validateIns($bgdata, $request, $files, $insurace);
                    break;
            }

            // cek apakah ada dokumen underlying yang dihapus
            $deleteUnderlying = [];
            foreach ($request as $key => $value) {
                if (strpos($key, 'gtx_') !== false) {
                    $deleteUnderlying[] = str_replace('~X~', '.', str_replace('~|~', ' ', str_replace(['gtx_'], '', $key)));
                }
            }

            if (!$deleteUnderlying) $deleteUnderlying = [''];
            $getNotDeleteUnderlying = $this->_db->select()
                ->from('T_BANK_GUARANTEE_UNDERLYING')
                ->where('DOC_FILE NOT IN (?)', $deleteUnderlying)
                ->where('BG_REG_NUMBER = ?', $bgdata['BG_REG_NUMBER'])
                ->query()->fetchAll();
            // end apakah ada dokumen underlying yang dihapus

            // cek ada penambahan dokumen underlying
            if ($files['uploadSource']) {
                foreach ($files['uploadSource']['name'] as $key => $value) {
                    $saveName = $value;
                    $newName = date('dmy') . '_' . date('his') . '_' . $bgdata['CUST_ID'] . '_' . uniqid('gt') . '_' . $saveName;
                    move_uploaded_file($files['uploadSource']['tmp_name'][$key], $attachmentDestination . $newName);

                    $getNotDeleteUnderlying[] = [
                        'DOC_FILE' => $newName,
                        'DOC_NAME' => $request['SERVICE'][$key],
                        'DOC_TYPE' => $request['GT_DOC_TYPE'][$key],
                        'DOC_NUMBER' => $request['GT_DOC_NUMBER'][$key],
                        'DOC_DATE' => $request['GT_DOC_DATE'][$key],
                    ];
                }
            }
            // end cek ada penambahan dokumen underlying

            // cek apakah ada lampiran dokumen yang dihapus
            $deleteOthersDocument = [];
            foreach ($request as $key => $value) {
                if (strpos($key, 'odx_') !== false) {
                    $deleteOthersDocument[] = str_replace('~X~', '.', str_replace('~|~', ' ', str_replace(['odx_'], '', $key)));
                }
            }

            if (!$deleteOthersDocument) $deleteOthersDocument = [''];
            $getNotDeleteOthersDocument = $this->_db->select()
                ->from('T_BANK_GUARANTEE_FILE')
                ->where('BG_FILE NOT IN (?)', $deleteOthersDocument)
                ->where('BG_REG_NUMBER = ?', $bgdata['BG_REG_NUMBER'])
                ->query()->fetchAll();
            // end cek apakah ada lampiran dokumen yang dihapus

            // cek ada penambahan lampiran dokumen
            if ($files['othersDocument']) {
                foreach ($files['othersDocument']['name'] as $key => $value) {
                    $saveName = $value;
                    $newName = date('dmy') . '_' . date('his') . '_' . $bgdata['CUST_ID'] . '_' . uniqid('od') . '_' . $saveName;
                    move_uploaded_file($files['othersDocument']['tmp_name'][$key], $attachmentDestination . $newName);

                    $getNotDeleteOthersDocument[] = [
                        'BG_FILE' => $newName,
                        'FILE_NOTE' => $request['od'][$key],
                    ];
                }
            }
            // end cek ada penambahan dokumen underlying
        }

        if ($tipeUsulan == '1') {
            $saveResult = array_merge($saveResult, $resultUsulan, ['underlying' => $getNotDeleteUnderlying, 'othersDocument' => $getNotDeleteOthersDocument], $request, ['bgOld' => $bgdata['BG_NUMBER']]);
        } else {
            $saveResult = array_merge($request, $bgdata, ['bgOld' => $bgdata['BG_NUMBER']]);
        }

        $sessionNamespace = new Zend_Session_Namespace('amdetail');
        $sessionNamespace->content = $saveResult;

        // Zend_Session::namespaceUnset('eform');

        $this->setbackURL($_SERVER['REQUEST_URI']);
        $this->_redirect('/eform/amdetail/confirm');
    }

    private function validateFc($bgdata, $request, $files)
    {
        // check apakah ada penambahan MD FC
        $checkAddMd = $request['own_account'];
        $saveAddAcct = [];
        if (is_array($checkAddMd) && count($checkAddMd) > 0) {
            // simpan ke session
            foreach ($checkAddMd as $key => $value) {
                // jika rekening milik sendiri
                if ($value == '1') {
                    $acctNumber = $request['own_account_number'][$key];
                    $getName = $this->_db->select()
                        ->from('M_CUSTOMER_ACCT')
                        ->where('ACCT_NO = ?', $acctNumber)
                        ->where('CUST_ID = ?', $bgdata['CUST_ID'])
                        ->query()->fetch();
                    $acctName = $getName['ACCT_NAME'];
                }

                // jika rekening pihak ketiga
                else {
                    $acctNumber = $request['manual_account_number'][$key];
                    $acctName = $request['manual_account_name'][$key];
                }

                $acctType = $request['acct_type'][$key];
                $currency = $request['currency_acct'][$key];
                $amount = str_replace([','], '', $request['account_amount'][$key]);

                $saveAddAcct[] = [
                    'acctNumber' => $acctNumber,
                    'acctName' => $acctName,
                    'acctType' => $acctType,
                    'currency' => $currency,
                    'amount' => $amount,
                ];
            }
        }
        $result = ['topUpMd' => $saveAddAcct];

        return $result;
    }

    private function validateLf($bgdata, $request, $files)
    {
        // validate Line Facility
        return [];
    }

    private function validateIns($bgdata, $request, $files, $insurace)
    {
        // validate Line Facility
        return $insurace;
    }

    public function confirmAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $sessionNamespace = new Zend_Session_Namespace('amdetail');

        if ($this->_request->isGet()) {
            if (!$sessionNamespace->content) {
                $this->_redirect('/eform');
            }
        }

        $requestAmd = $sessionNamespace->content;

        $ns = new Zend_Session_Namespace('FVC');

        $this->view->backURL = '/eform';
        if (isset($ns->backURL) && !(empty($ns->backURL))) {
            $this->view->backURL = $ns->backURL;
        }

        // Zend_Debug::dump($requestAmd);
        // die();

        // move session
        // $sessionNamespaceConfirm = new Zend_Session_Namespace('confirmamdetail');
        // $sessionNamespaceConfirm->content = $requestAmd;

        $this->view->requestAmd = $requestAmd;

        // load config
        $conf = Zend_Registry::get('config');

        //BG Counter Guarantee Type
        $bgcgType         = $conf["bgcg"]["type"]["desc"];
        $bgcgCode         = $conf["bgcg"]["type"]["code"];
        $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
        $this->view->arrbgcg = $arrbgcg;

        // bg doc type
        $gtStatus       = $conf["bgdoc"]["type"]["desc"];
        $gtCode         = $conf["bgdoc"]["type"]["code"];

        $gtStatus = array_combine(array_values($gtCode), array_values($gtStatus));
        $this->view->gtStatus = $gtStatus;


        // load settings
        $settings = new Settings();

        // max claim period
        $this->view->maxClaimPeriod = $settings->getSetting('max_claim_period');

        // detail data bg
        $detailBg = $this->_db->select()
            ->from(['TB' => 'T_BANK_GUARANTEE'], [
                '*',
                'PRINCIPLE_NAME' => new Zend_Db_Expr('(SELECT MC.CUST_NAME FROM M_CUSTOMER MC WHERE CUST_ID = TB.CUST_ID LIMIT 1)'), // nama principle
                'BANK_CABANG_PENERBIT' => new Zend_Db_Expr('(SELECT MB.BRANCH_NAME FROM M_BRANCH MB WHERE BRANCH_CODE = TB.BG_BRANCH LIMIT 1)'), // nama principle
            ])
            ->where('BG_NUMBER = ?', $requestAmd['bgOld'])
            ->query()->fetch();

        // T_BANK_GUARANTEE_DETAIL
        $currency = $this->_db->select()
            ->from('T_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE'])
            ->where('LOWER(PS_FIELDNAME) = ?', 'currency')
            ->where('BG_REG_NUMBER = ?', $detailBg['BG_REG_NUMBER'])
            ->query()->fetch();

        $this->view->bgCurrency = $currency['PS_FIELDVALUE'];

        $this->view->detailBg = $detailBg;

        // detail principle
        $detailPrinciple = $this->_db->select()
            ->from(['MC' => 'M_CUSTOMER'], [
                '*',
                'CITY_NAME' => new Zend_Db_Expr("(SELECT MCI.CITY_NAME FROM M_CITYLIST MCI WHERE MCI.CITY_CODE = MC.CUST_CITY)")
            ])
            ->where('CUST_ID = ?', $detailBg['CUST_ID'])
            ->query()->fetch();
        $this->view->compinfo = $detailPrinciple;

        // bg split
        if ($detailBg['COUNTER_WARRANTY_TYPE'] == '1' || $detailBg['COUNTER_WARRANTY_TYPE'] == '3') {

            $getSplit = $this->_db->select()
                ->from(['TBS' => 'T_BANK_GUARANTEE_SPLIT'])
                ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = TBS.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($detailBg['CUST_ID']) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
                ->where('BG_REG_NUMBER = ?', $detailBg['BG_REG_NUMBER'])
                ->query()->fetchAll();

            $this->view->getSplit = $getSplit;

            // $getSplit = array_filter($getSplit, function ($acct) {
            //     return strtolower($acct['ACCT_DESC']) != 'escrow';
            // });

            $saveSplit = [];
            if ($requestAmd['own_account']) {
                foreach ($requestAmd['own_account'] as $key => $value) {
                    $saveSplit[] = [
                        "BG_REG_NUMBER" => "-",
                        "BG_NUMBER" => "-",
                        "ACCT" => ($value == '1') ? $requestAmd['own_account_number'][$key] : $requestAmd['manual_account_number'][$key],
                        "BANK_CODE" => "-",
                        "NAME" => ($value == '1') ? '-' : $requestAmd['manual_account_name'][$key],
                        "AMOUNT" => str_replace([','], '', $requestAmd['account_amount'][$key]),
                        "FLAG" => $value,
                        "CCY_ID" => $requestAmd['currency_acct'][$key],
                        "ACCT_TYPE" => '',
                        "ACCT_DESC" => $requestAmd['acct_type'][$key],
                        "HOLD_BY_BRANCH" => '',
                        "HOLD_SEQUENCE" => '',
                        "HOLD_REMARK" => '',
                    ];
                }

                $totalAmountNew = 0;
                foreach ($saveSplit as $key => $value) {
                    if (strtolower($value['ACCT_DESC']) == 'escrow') continue;

                    if (strtolower($value['NAME']) == '-' && $value['FLAG'] == '1') {
                        $getTempName = $this->_db->select()
                            ->from('M_CUSTOMER_ACCT')
                            ->where('ACCT_NO = ?', $value['ACCT'])
                            ->query()->fetch();

                        $saveSplit[$key]['NAME'] = $getTempName['ACCT_NAME'];
                    }
                    $totalAmountNew += floatval($value['AMOUNT']);
                }

                $totalAmountOld = 0;
                foreach ($saveSplit as $key => $value) {
                    if (strtolower($value['ACCT_DESC']) == 'escrow') continue;

                    $totalAmountOld += floatval($value['AMOUNT']);
                }

                $this->view->getSplit = $getSplit;
                $this->view->totalAmountNew = $totalAmountNew;

                $this->view->saveSplit = $saveSplit;
                $this->view->totalAmountOld = $totalAmountOld;
            }
        }

        // jika amandemen format
        if ($requestAmd['tipeUsulan'] == '2' || $requestAmd['toa'] == '2') {
            // rekening pembebanan biaya
            $getInfo = $this->_db->select()
                ->from('M_CUSTOMER_ACCT')
                ->where('ACCT_NO = ?', $detailBg['FEE_CHARGE_TO'])
                ->where('CUST_ID = ?', $detailBg['CUST_ID'])
                ->query()->fetch();

            $this->view->rekPembebananBiaya = $getInfo;
        } else {
            $getInfo = $this->_db->select()
                ->from('M_CUSTOMER_ACCT')
                ->where('ACCT_NO = ?', $requestAmd['rekeningPembebananBiaya'])
                ->where('CUST_ID = ?', $detailBg['CUST_ID'])
                ->query()->fetch();

            $this->view->rekPembebananBiaya = $getInfo;
        }

        // penkondisian kontra biaya provisi
        if ($detailBg['COUNTER_WARRANTY_TYPE'] == '3') {
            $bgdatadetail = $this->_db->select()
                ->from('T_BANK_GUARANTEE_DETAIL')
                ->where('BG_REG_NUMBER = ?', $detailBg['BG_REG_NUMBER'])
                ->query()->fetchAll();

            $getInsuranceBranch = array_filter($bgdatadetail, function ($bgdatadetail) {
                return strtolower($bgdatadetail['PS_FIELDNAME']) == 'insurance branch';
            });

            $getInsuranceBranch = array_shift($getInsuranceBranch)['PS_FIELDVALUE'];
            list($getInsurance, $getInsuranceBranch) = $this->getInsuranceInfo($detailBg['BG_INSURANCE_CODE'], $getInsuranceBranch);

            $this->view->getInsurance = $getInsurance;
            $this->view->getInsuranceBranch = $getInsuranceBranch;

            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $detailBg['COUNTER_WARRANTY_TYPE'];
            $paramProvisionFee['CUST_ID'] = $detailBg["BG_INSURANCE_CODE"];
            $paramProvisionFee['GRUP'] = $detailBg["CUST_ID"];
            $paramProvisionFee['USAGE_PURPOSE'] = $detailBg['USAGE_PURPOSE'];
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $detailBg['USAGE_PURPOSE_DESC'];
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $date_start = date_create($detailBg["TIME_PERIOD_START"]);
            $date_end = date_create($detailBg["TIME_PERIOD_END"]);
            $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

            $prorata = $tenor / 360;

            $percentage = $getProvisionFee['provisionFee'];

            $biaya_provisi = round($prorata * ($percentage / 100) * $detailBg["BG_AMOUNT"], 2);

            $getMinCharge = $this->_db->select()
                ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                ->where("CHARGES_ID = ?", "MINPROV01")
                ->query()->fetch();

            if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
            }

            $this->view->biayaProvisi = $biaya_provisi;

            $this->view->getFee = $getProvisionFee;
        }

        if ($detailBg['COUNTER_WARRANTY_TYPE'] == '1') {

            $this->view->getInsurance = $getInsurance;
            $this->view->getInsuranceBranch = $getInsuranceBranch;

            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $detailBg['COUNTER_WARRANTY_TYPE'];
            $paramProvisionFee['CUST_ID'] = $detailBg["CUST_ID"];
            $paramProvisionFee['GRUP'] = $detailBg["CUST_ID"];
            $paramProvisionFee['USAGE_PURPOSE'] = $detailBg['USAGE_PURPOSE'];
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $detailBg['USAGE_PURPOSE_DESC'];
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $date_start = date_create($detailBg["TIME_PERIOD_START"]);
            $date_end = date_create($detailBg["TIME_PERIOD_END"]);
            $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

            $prorata = $tenor / 360;

            $percentage = $getProvisionFee['provisionFee'];

            $biaya_provisi = round($prorata * ($percentage / 100) * $detailBg["BG_AMOUNT"], 2);

            $getMinCharge = $this->_db->select()
                ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                ->where("CHARGES_ID = ?", "MINPROV01")
                ->query()->fetch();

            if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
            }

            $this->view->biayaProvisi = $biaya_provisi;

            $this->view->getFee = $getProvisionFee;
        }

        if ($detailBg['COUNTER_WARRANTY_TYPE'] == '2') {

            $this->view->getInsurance = $getInsurance;
            $this->view->getInsuranceBranch = $getInsuranceBranch;

            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $detailBg['COUNTER_WARRANTY_TYPE'];
            $paramProvisionFee['CUST_ID'] = $detailBg["CUST_ID"];
            $paramProvisionFee['GRUP'] = $detailBg["CUST_ID"];
            $paramProvisionFee['USAGE_PURPOSE'] = $detailBg['USAGE_PURPOSE'];
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $detailBg['USAGE_PURPOSE_DESC'];
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $date_start = date_create($detailBg["TIME_PERIOD_START"]);
            $date_end = date_create($detailBg["TIME_PERIOD_END"]);
            $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

            $prorata = $tenor / 360;

            $percentage = $getProvisionFee['provisionFee'];

            $biaya_provisi = round($prorata * ($percentage / 100) * $detailBg["BG_AMOUNT"], 2);

            $getMinCharge = $this->_db->select()
                ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                ->where("CHARGES_ID = ?", "MINPROV01")
                ->query()->fetch();

            if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
            }

            $this->view->biayaProvisi = $biaya_provisi;

            $this->view->getFee = $getProvisionFee;

            $paramLimit['CUST_ID'] =  $detailBg["CUST_ID"];
            $paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
            $getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

            $this->view->getLineFacility = $getLineFacility;
        }
        // end pengkondisian kontra

        // BG Publishing Form
        $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
        $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

        $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

        $this->view->BG_PUBLISH_ARR     = $arrbgpublish;

        // jika amandemen format
        if ($requestAmd['tipeUsulan'] == '2' || $requestAmd['toa'] == '2') {
            $this->view->requestAmd['underlying'] = $this->_db->select()
                ->from('T_BANK_GUARANTEE_UNDERLYING')
                ->where('BG_REG_NUMBER = ?', $detailBg['BG_REG_NUMBER'])
                ->query()->fetchAll();

            $this->view->requestAmd['othersDocument'] = $this->_db->select()
                ->from('T_BANK_GUARANTEE_FILE')
                ->where('BG_REG_NUMBER = ?', $detailBg['BG_REG_NUMBER'])
                ->query()->fetchAll();
        }


        // send request amandemen
        if ($this->_request->isPost()) {

            // check apakah principal menggunakan approver
            $cekPrincipal = $this->_db->select()
                ->from('M_CUSTOMER')
                ->where('CUST_ID = ?', $detailBg['CUST_ID'])
                ->query()->fetch();

            $custApprover = $cekPrincipal['CUST_APPROVER'];

            // ntu
            $max_ntu = $settings->getSetting('max_ntu_date');

            // amandement
            $tipeUsulan = $requestAmd['tipeUsulan'] ?: $requestAmd['toa'];

            // status
            $status = ($custApprover == '1') ? '2' : '3';

            // insert data to TEMP_BANK_GUARANTEE
            $BG_CLAIM_DATE = date('Y-m-d', strtotime('+' . $settings->getSetting('max_claim_period') . ' days', strtotime($requestAmd['updatedate'][1])));
            $genBgRegNumber = $this->genBgRegNumber($detailBg['BG_BRANCH'], strtoupper($detailBg['USAGE_PURPOSE']));

            $insertData = [
                'BG_REG_NUMBER'             => $genBgRegNumber,
                'CUST_ID'                   => $this->_custIdLogin,
                'CUST_CP'                   => ($tipeUsulan == '2') ? $detailBg['CUST_CP'] : $requestAmd['CUST_CP_TXT'],
                'CUST_EMAIL'                => ($tipeUsulan == '2') ? $detailBg['CUST_EMAIL'] : $requestAmd['CUST_EMAIL_TXT'],
                'CUST_CONTACT_NUMBER'       => ($tipeUsulan == '2') ? $detailBg['CUST_CONTACT_NUMBER'] : $requestAmd['CUST_CONTACT_NUMBER_TXT'],
                'BG_STATUS'                 => $status,
                'BG_SUBJECT'                => $detailBg['BG_SUBJECT'],
                'RECIPIENT_NAME'            => ($tipeUsulan == '2') ? $detailBg['RECIPIENT_NAME'] : $requestAmd['RECIPIENT_NAME'],
                'RECIPIENT_ADDRES'          => ($tipeUsulan == '2') ? $detailBg['RECIPIENT_ADDRES'] : $requestAmd['RECIPIENT_ADDRESS'],
                'RECIPIENT_CITY'            => ($tipeUsulan == '2') ? $detailBg['RECIPIENT_CITY'] : $requestAmd['RECIPIENT_CITY'],
                'RECIPIENT_CP'              => ($tipeUsulan == '2') ? $detailBg['RECIPIENT_CP_TXT'] : $requestAmd['RECIPIENT_CP_TXT'],
                'RECIPIENT_OFFICE_NUMBER'   => ($tipeUsulan == '2') ? $detailBg['RECIPIENT_OFFICE_NUMBER'] : $requestAmd['RECIPIENT_OFFICE_NUMBER'],
                'RECIPIENT_EMAIL'           => ($tipeUsulan == '2') ? $detailBg['RECIPIENT_EMAIL'] : $requestAmd['RECIPIENT_EMAIL_TXT'],
                'RECIPIENT_CONTACT'         => ($tipeUsulan == '2') ? $detailBg['RECIPIENT_CONTACT'] : $requestAmd['RECIPIENT_CONTACT_TXT'],
                'SERVICE'                   => '-',
                'GT_DOC_TYPE'               => '-',
                'GT_DOC_NUMBER'             => '-',
                'GT_DOC_DATE'               => '-',
                'FILE'                      => '-',
                'BG_AMOUNT'                 => ($tipeUsulan == '2') ? str_replace([','], '', $detailBg['BG_AMOUNT']) : str_replace([','], '', $requestAmd['bank_amount_txt']),
                'PROVISION_FEE'             => $getProvisionFee['provisionFee'],
                'ADM_FEE'                   => $getProvisionFee['adminFee'],
                'STAMP_FEE'                 => $getProvisionFee['stampFee'],
                'TIME_PERIOD_START'         => ($tipeUsulan == '2') ? $detailBg['TIME_PERIOD_START'] : $requestAmd['updatedate'][0],
                'TIME_PERIOD_END'           => ($tipeUsulan == '2') ? $detailBg['TIME_PERIOD_END'] : $requestAmd['updatedate'][1],
                'COUNTER_WARRANTY_TYPE'     => $detailBg['COUNTER_WARRANTY_TYPE'],
                'COUNTER_WARRANTY_ACCT_NO'  => '-',
                'COUNTER_WARRANTY_ACCT_NAME' => '-',
                'COUNTER_WARRANTY_AMOUNT'   => '-',
                'FEE_CHARGE_TO'             => ($tipeUsulan == '2') ? $detailBg['FEE_CHARGE_TO'] : $requestAmd['rekeningPembebananBiaya'],
                'BG_FORMAT'                 => $requestAmd['BG_FORMAT'],
                'BG_LANGUAGE'               => $requestAmd['BG_LANGUAGE'] ?: '-',
                'USAGE_PURPOSE'             => $detailBg['USAGE_PURPOSE'],
                'USAGE_PURPOSE_DESC'        => $detailBg['USAGE_PURPOSE_DESC'],
                'BG_CREATED'                => new Zend_Db_Expr('now()'),
                'BG_CREATEDBY'              => $this->_userIdLogin,
                'BG_INSURANCE_CODE'         => $detailBg['BG_INSURANCE_CODE'],
                'BG_UNDERLYING_DOC'         => '-',
                'BG_BRANCH'                 => $detailBg['BG_BRANCH'],
                'BG_PUBLISH'                => $detailBg['BG_PUBLISH'],
                'SPECIAL_FORMAT_DOC'        => $detailBg['sepcialFormatFile'] ?: '-',
                'NTU_COUNTING'              => $max_ntu,
                'BG_CLAIM_DATE'             => ($tipeUsulan == '2') ? $detailBg['BG_CLAIM_PERIOD'] : $BG_CLAIM_DATE,
                'BG_OLD'                    => $detailBg['BG_NUMBER'],
                'IS_AMENDMENT'              => 1,
                'CHANGE_TYPE'               => $tipeUsulan,
            ];

            $this->_db->insert('TEMP_BANK_GUARANTEE', $insertData);

            // get t_bank_guarantee_detail move to temp_bank_guarantee_detail
            $getTBDetail = $this->_db->select()
                ->from('T_BANK_GUARANTEE_DETAIL')
                ->where('BG_REG_NUMBER = ?', $detailBg['BG_REG_NUMBER'])
                ->query()->fetchAll();

            $onlyInsert = [
                'plafond owner',
                'amount owner',
                'currency',
                'insurance name',
                'insurance branch',
            ];

            foreach ($getTBDetail as $key => $value) {
                if (in_array(strtolower($value['PS_FIELDNAME']), $onlyInsert)) {
                    $value['BG_REG_NUMBER'] = $genBgRegNumber;
                    $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $value);
                }
            }

            if ($detailBg['COUNTER_WARRANTY_TYPE'] == '2') {
                $this->_db->update('TEMP_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE' => $insertData['BG_AMOUNT']], ['LOWER(PS_FIELDNAME) = ?' => 'amount owner', 'BG_REG_NUMBER = ?' => $genBgRegNumber]);
            }

            // update bg menjadi off risk
            // $this->_db->update('T_BANK_GUARANTEE', ['BG_STATUS' => '16'], ['BG_NUMBER = ?' => $detailBg['BG_NUMBER']]);

            // update history
            $historyInsert = array(
                'DATE_TIME'         => new Zend_Db_Expr("now()"),
                'BG_REG_NUMBER'     => $insertData['BG_REG_NUMBER'],
                'CUST_ID'           => $this->_custIdLogin,
                'USER_LOGIN'        => $this->_userIdLogin,
                'HISTORY_STATUS'    => 33
            );
            $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

            // insert log
            if ($tipeUsulan == '1') {
                $log = 'Pengajuan Amendemen Isi atas Nomor BG : ' . $detailBg['BG_NUMBER'] . ', Subjek : ' . $detailBg['BG_SUBJECT'] . ', dengan Nomor Registrasi Pengajuan : ' . $insertData['BG_REG_NUMBER'] . '';
            } else {
                $log = 'Pengajuan Amendemen Format atas Nomor BG : ' . $detailBg['BG_NUMBER'] . ', Subjek : ' . $detailBg['BG_SUBJECT'] . ', dengan Nomor Registrasi Pengajuan : ' . $insertData['BG_REG_NUMBER'] . '';
            }
            Application_Helper_General::writeLog('CCBG', $log);

            // end insert data to TEMP_BANK_GUARANTEE

            // insert temp_bank_guarantee_underlying
            if ($this->view->requestAmd['underlying']) {
                foreach ($this->view->requestAmd['underlying'] as $key => $value) {
                    $insertTBGU = [
                        'BG_REG_NUMBER' => $genBgRegNumber,
                        'DOC_NAME' => $value['DOC_NAME'],
                        'DOC_TYPE' => $value['DOC_TYPE'],
                        'DOC_NUMBER' => $value['DOC_NUMBER'],
                        'DOC_DATE' => $value['DOC_DATE'],
                        'DOC_FILE' => $value['DOC_FILE'],
                    ];
                    $this->_db->insert('TEMP_BANK_GUARANTEE_UNDERLYING', $insertTBGU);
                }
            }

            // insert temp_bank_guarantee_file
            if ($this->view->requestAmd['othersDocument']) {
                foreach ($this->view->requestAmd['othersDocument'] as $key => $value) {
                    $insertTBGU = [
                        'BG_REG_NUMBER' => $genBgRegNumber,
                        'BG_FILE' => $value['BG_FILE'],
                        'FILE_NOTE' => $value['FILE_NOTE'],
                        'INDEX' => $key,
                    ];
                    $this->_db->insert('TEMP_BANK_GUARANTEE_FILE', $insertTBGU);
                }
            }

            // jika kontra fullcover
            if ($detailBg['COUNTER_WARRANTY_TYPE'] == '1' || $detailBg['COUNTER_WARRANTY_TYPE'] == '3') {
                // jika tipe usulan format

                if ($getSplit) {
                    foreach ($getSplit as $key => $value) {
                        $insertSplit = [
                            'BG_REG_NUMBER' => $genBgRegNumber,
                            'ACCT' => $value['ACCT'],
                            'BANK_CODE' => '999',
                            'NAME' => $value['NAME'],
                            'AMOUNT' => $value['AMOUNT'],
                            'FLAG' => $value['FLAG'],
                            'ACCT_TYPE' => $value['ACCT_TYPE'],
                            'ACCT_DESC' => $value['ACCT_DESC'],
                            'HOLD_SEQUENCE' => $value['HOLD_SEQUENCE'],
                            'HOLD_REMARK' => $value['HOLD_REMARK'],
                            'HOLD_BY_BRANCH' => $value['HOLD_BY_BRANCH'],
                            'IS_TRANSFER' => 1
                        ];
                        $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $insertSplit);
                    }
                }
                if ($tipeUsulan == '1' || $detailBg['COUNTER_WARRANTY_TYPE'] == '1') {
                    if ($saveSplit) {
                        foreach ($saveSplit as $key => $value) {
                            $insertSplit = [
                                'BG_REG_NUMBER' => $genBgRegNumber,
                                'ACCT' => $value['ACCT'],
                                'BANK_CODE' => '999',
                                'NAME' => $value['NAME'],
                                'AMOUNT' => $value['AMOUNT'],
                                'FLAG' => $value['FLAG'],
                                'ACCT_TYPE' => $value['ACCT_TYPE'],
                                'ACCT_DESC' => $value['ACCT_DESC'],
                                'HOLD_SEQUENCE' => '',
                                'HOLD_REMARK' => '',
                                'HOLD_BY_BRANCH' => ''
                            ];
                            $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $insertSplit);
                        }
                    }
                }
            }

            // delete session
            unset($sessionNamespace->content);

            // redirect 
            $this->setbackURL('/eform');
            $this->_redirect('/notification/success');
        }
    }

    private function genBgRegNumber($branch, $bgtype)
    {
        $currentDate = date("Ymd");
        $seqNumber   = mt_rand(1, 9999);
        $seqNumber = str_pad($seqNumber, 4, '0', STR_PAD_LEFT);
        $trxId = $seqNumber . $branch . '02' . $bgtype;

        return $trxId;
    }

    public function countprovisionAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $kontra = $this->_request->getParam('kontra');
        $timePeriodStart = $this->_request->getParam('time_period_start');
        $timePeriodEnd = $this->_request->getParam('time_period_end');
        $bgAmount = floatval($this->_request->getParam('bg_amount'));
        $custId = $this->_request->getParam('cust_id');
        $usagePurpose = $this->_request->getParam('usage_purpose');
        $usagePurposeDesc = $this->_request->getParam('usage_purpose_desc');
        $insuranceCode = $this->_request->getParam('insurance_code');
        $previousProvision = $this->_request->getParam('previous_provision');

        // get min global provisi
        $getMinGlobalProvisi = $this->_db->select()
            ->from('M_CHARGES_BG')
            ->where('CHARGES_ID = ?', 'MINPROV01')
            ->query()->fetch();

        $minGlobalProvisi = $getMinGlobalProvisi['CHARGES_AMT'];

        // penkondisian kontra biaya provisi
        if ($kontra == '3') {

            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
            $paramProvisionFee['CUST_ID'] = $insuranceCode;
            $paramProvisionFee['GRUP'] = $custId;
            $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $provisionPercentage = $getProvisionFee['provisionFee'];
        }

        if ($kontra == '1') {

            $paramProvisionFee['TIME_PERIOD_START'] = $timePeriodStart;
            $paramProvisionFee['TIME_PERIOD_END'] = $timePeriodEnd;
            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
            $paramProvisionFee['CUST_ID'] = $custId;
            $paramProvisionFee['GRUP'] = $custId;
            $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $provisionPercentage = $getProvisionFee['provisionFee'];
        }

        if ($kontra == '2') {

            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
            $paramProvisionFee['CUST_ID'] = $custId;
            $paramProvisionFee['GRUP'] = $custId;
            $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $provisionPercentage = $getProvisionFee['provisionFee'];
        }
        // end pengkondisian kontra

        header('Content-Type: application/json');

        $provisi = Application_Helper_General::countProvision($kontra, ['TIME_PERIOD_START' => $timePeriodStart, 'TIME_PERIOD_END' => $timePeriodEnd, 'BG_AMOUNT' => $bgAmount], $provisionPercentage);

        $realProv = $provisi;
        $selisihProvisi = $provisi - $previousProvision;

        if ($provisi <= $previousProvision) {
            $provisi = 0;
        } elseif ($selisihProvisi < $minGlobalProvisi) {
            $provisi = $minGlobalProvisi;
        } else {
            $provisi = $selisihProvisi;
        }

        $changeProvision = ($realProv !== $previousProvision) ? true : false;

        echo json_encode(['percentage_provisi' => $provisionPercentage, 'provisi' => $provisi, 'administrasi' => floatval($getProvisionFee['adminFee']), 'meterai' => floatval($getProvisionFee['stampFee']), 'change_provision' => $changeProvision]);
        return true;
    }

    public function countprovisionrebateAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $kontra = $this->_request->getParam('kontra');
        $timePeriodStart = $this->_request->getParam('time_period_start');
        $timePeriodEnd = $this->_request->getParam('time_period_end');
        $bgAmount = floatval($this->_request->getParam('bg_amount'));
        $custId = $this->_request->getParam('cust_id');
        $usagePurpose = $this->_request->getParam('usage_purpose');
        $usagePurposeDesc = $this->_request->getParam('usage_purpose_desc');
        $insuranceCode = $this->_request->getParam('insurance_code');
        $previousProvision = $this->_request->getParam('previous_provision');
        $rebate = $this->_request->getParam('rebate');

        // get min global provisi
        $getMinGlobalProvisi = $this->_db->select()
            ->from('M_CHARGES_BG')
            ->where('CHARGES_ID = ?', 'MINPROV01')
            ->query()->fetch();

        $minGlobalProvisi = $getMinGlobalProvisi['CHARGES_AMT'];

        // penkondisian kontra biaya provisi
        if ($kontra == '3') {

            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
            $paramProvisionFee['CUST_ID'] = $insuranceCode;
            $paramProvisionFee['GRUP'] = $custId;
            $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $provisionPercentage = $getProvisionFee['provisionFee'];
        }

        if ($kontra == '1') {

            $paramProvisionFee['TIME_PERIOD_START'] = $timePeriodStart;
            $paramProvisionFee['TIME_PERIOD_END'] = $timePeriodEnd;
            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
            $paramProvisionFee['CUST_ID'] = $custId;
            $paramProvisionFee['GRUP'] = $custId;
            $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $provisionPercentage = $getProvisionFee['provisionFee'];
        }

        if ($kontra == '2') {

            $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
            $paramProvisionFee['CUST_ID'] = $custId;
            $paramProvisionFee['GRUP'] = $custId;
            $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
            $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
            $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

            $provisionPercentage = $getProvisionFee['provisionFee'];
        }
        // end pengkondisian kontra

        $provisionPercentage = floatval(round($rebate, 2));

        header('Content-Type: application/json');

        $provisi = Application_Helper_General::countProvision($kontra, ['TIME_PERIOD_START' => $timePeriodStart, 'TIME_PERIOD_END' => $timePeriodEnd, 'BG_AMOUNT' => $bgAmount], $provisionPercentage);

        $realProv = $provisi;
        $selisihProvisi = $provisi - $previousProvision;


        if($previousProvision){
            if ($provisi <= $previousProvision) {
                $provisi = 0;
            } elseif ($selisihProvisi < $minGlobalProvisi) {
                $provisi = $minGlobalProvisi;
            } else {
                $provisi = $selisihProvisi;
            }
        }

        $changeProvision = (floatval($realProv) !== floatval($previousProvision)) ? true : false;

        echo json_encode(['percentage_provisi' => $provisionPercentage, 'provisi' => $provisi, 'administrasi' => floatval($getProvisionFee['adminFee']), 'meterai' => floatval($getProvisionFee['stampFee']), 'change_provision' => $changeProvision]);
        return true;
    }

    public function mdacctsacaAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $acct = $this->_request->getParam('acct');

        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $AccArr = $CustomerUser->getAccountsBG();
        $newArr2 = [];

        $getAllProduct = $this->_db->select()
            ->from("M_PRODUCT")
            ->query()->fetchAll();

        foreach ($AccArr as $keyArr => $arrAcct) {

            $accountTypeCheck = false;
            $productTypeCheck = false;

            $svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
            $result = $svcAccount->inquiryAccountBalance();

            if ($result['response_code'] == '0000' && strtolower($result['account_type']) == 'd') {
                $accountType = $result['account_type'];
                $productType = $result['product_type'];
            } else {
                $accountType = '';
                $productType = '';
            }

            foreach ($getAllProduct as $key => $value) {
                # code...
                if ($value['PRODUCT_CODE'] == $accountType) {
                    $accountTypeCheck = true;
                };

                if ($value['PRODUCT_PLAN'] == $productType) {
                    $productTypeCheck = true;
                };
            }
            if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
        }

        echo "<option value=''></option>";

        foreach ($newArr2 as $key => $value) {
            echo "<option value='" . $value['ACCT_NO'] . "'>" . $value['ACCT_NO'] . "</option>";
        }
    }
}
