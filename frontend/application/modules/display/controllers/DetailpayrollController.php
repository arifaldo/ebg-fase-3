<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'Service/Token.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'Crypt/AESMYSQL.php';
class display_DetailpayrollController extends Application_Main
{

	protected $_bankName;

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
	}

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$cfd = $this->_getParam('cfd');
		$pdf = $this->_getParam('pdf');
		$reExecute = $this->_getParam('reExecute');

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;

		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		//$psnumber = ;
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$this->view->masterBankName = $this->_bankName;


		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($this->_getParam('psnumber'));
		$psnumber = $AESMYSQL->decrypt($PS_NUMBER, $password);



		$getPaymentDetail 	= new display_Model_Paymentreport();
		$detail = $getPaymentDetail->getPaymentDetail($psnumber);
		//echo '<pre>';
		//var_dump($detail);
		$psnumber = substr($psnumber, 0, -2);
		$history = $getPaymentDetail->getHistory($psnumber);
		$historyaprove = $getPaymentDetail->getHistoryAprove($psnumber);
//var_dump($history);die;
		$this->view->detail = $detail[0];

		$paramPayment = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);

		$PSSTATUS  = $pslip["PS_STATUS"];
		$PSNUMBER  = $pslip["payReff"];
		$payStatus = $pslip["payStatus"];
		$this->view->AccArr = $CustUser->getAccounts();
		$this->view->AccArrDomestic = $CustUser->getAccounts(array("CCY_IN" => array("IDR")));

		// Set variables needed in view
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();

		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldCategoryArr = $settings->getLLDDOMCategory();

		$model = new purchasing_Model_Purchasing();

		$purposeArr = $model->getTranspurpose();

		$purposeList = array('' => '-- Select Transaction Purpose --');
		foreach ($purposeArr as $key => $value) {
			$purposeList[$value['CODE']] = $value['DESCRIPTION'];
		}

		$this->view->TransPurposeArr = $purposeList;

		$selectuser = $this->_db->select()
			->from(array('A' => 'M_USER'));
		$selectuser->where("A.CUST_ID = " . $this->_db->quote($this->_custIdLogin));
		$this->view->dataact = $selectuser->query()->fetchAll();

		$selectccy = $this->_db->select()
			->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
		$this->view->dataccy = $selectccy->query()->fetchAll();

		// print_r($this->view->datatrx);die();


		$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
		$ccyList  			= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'

		//for skn rtgs
		$lldTypeArr  		= $settings->getLLDDOMType();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
		$residentArr 		= array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
		$citizenshipArr		= array("W" => "WNI", "N" => "WNA");

		$this->view->residentArr = $residentArr;
		$this->view->citizenshipArr = $citizenshipArr;
		$this->view->lldCategoryArr = $lldCategoryArr;
		$this->view->lldBeneIdentifArr = $lldBeneIdentifArr;
		$this->view->TransactorArr = $lldRelationshipArr;
		$this->view->IdentyArr = $lldIdenticalArr;


		$anyValue = '-- ' . $this->language->_('Select City') . ' --';
		$select = $this->_db->select()
			->from(array('A' => 'M_CITY'), array('*'));
		$select->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);
		$cityCodeArr 			= array('' => $anyValue);
		$cityCodeArr 			+= Application_Helper_Array::listArray($arr, 'CITY_CODE', 'CITY_NAME');
		$this->view->cityCodeArr 	= $cityCodeArr;

		if ($this->view->hasPrivilege('MTSP')) {
			$usetemp = 1;
		} else {
			$usetemp = 0;
		}

		$this->view->usetemplate = $usetemp;

		$paymentref = $detail[0]['PS_NUMBER'];
		$paystatus = $detail[0]['PS_STATUS'];
		$source = $detail[0]['ACCT_SOURCE'];
		$transmission = '';
		$traceno = '';
		$challenge = $detail[0]['PS_RELEASER_CHALLENGE'];
		$created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
		$updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
		$efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
		if($detail[0]['CEK_PS_TYPE'] == '23' || $detail[0]['CEK_PS_TYPE'] == '20' || $detail[0]['CEK_PS_TYPE'] == '19'){
			
				$amount = $detail[0]['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']);
		}else{
				$amount = $detail[0]['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']);
		}
		

		$type = $detail[0]['PS_TYPE'];
		$cektype = $detail[0]['CEK_PS_TYPE'];
		$paysubject = $detail[0]['PS_SUBJECT'];
		if (($cektype == 16 || $cektype == 17) && $detail[0]['PS_BILLER_ID'] == '1158') {
			$paysubject = json_decode($detail[0]['LOG']);
			$paysubject = $paysubject->paymentSubject;
		}

		//transaction summary
		$this->_tableMstleft[0]["label"] = $this->language->_('Payment Ref') . "#";
		$this->_tableMstleft[1]["label"] = $this->language->_('Transaction ID') . "#";
		$this->_tableMstleft[2]["label"] = $this->language->_('Frequently');
		$this->_tableMstleft[3]["label"] = $this->language->_('Payment Subject');

		if ($detail[0]["CEK_PS_TYPE"] == "19"  && $detail[0]['PS_TXCOUNT'] > 1) {
			
		}else{
			$this->_tableMstleft[4]["label"] = $this->language->_('Message');
		}

		$this->_tableMstleft[5]["label"] = $this->language->_('Additional Message');


		$this->_tableMstleft[0]["value"] = $detail[0]['PS_NUMBER'];
		$this->_tableMstleft[1]["value"] = $detail[0]['TRANSACTION_ID'];

		if (($detail[0]["PS_CATEGORY"] == "BULK PAYMENT" || ($detail[0]["PS_CATEGORY"] == "SWEEP TRANSFER" ) && $detail[0]['numtrx'] > 1) && !$pdf && $detail[0]["CEK_PS_TYPE"] != "11") {
			// download trx bulk file
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $detail[0]['PS_NUMBER']), null, true);
			$this->_tableMstleft[1]["value"] = $this->view->formButton('download', $this->language->_('download'), array('class' => 'btngreen hov', 'onclick' => "window.location = '" . $downloadURL . "';"));
		}

		if (empty($detail[0]['PS_PERIODIC'])) {
			$repeat = '1x';
		}
		else{

			$select	= $this->_db->select()
					->from(array('S' => 'T_PERIODIC'))
					->where("S.PS_PERIODIC = ?", (string) $detail[0]['PS_PERIODIC']);

			$periodicData = $this->_db->fetchAll($select);

			if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] === null) {
				$repeat = '1x';
			} elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '1') {
				$repeat = 'Daily';
			} elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '2') {
				$repeat = 'Weekly';
			} elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '3') {
				$repeat = 'Monthly';
			} elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '4') {
				$repeat = 'Yearly';
			} elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '5') {
				$periodicEveryArr = array(
					'1' => $this->language->_('Monday'),
					'2' => $this->language->_('Tuesday'),
					'3' => $this->language->_('Wednesday'),
					'4' => $this->language->_('Thursday'),
					'5' => $this->language->_('Friday'),
					'6' => $this->language->_('Saturday'),
					'7' => $this->language->_('Sunday'),
				);
				$repeat = $this->language->_('Every Day of ') . $periodicEveryArr[$periodicData[0]['PS_EVERY_PERIODIC']];
			} elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '6') {
				$repeat = $this->language->_('Every Date of ') . $periodicData[0]['PS_EVERY_PERIODIC'];
			} else {
				$repeat = '-';
			}
		}

		$AESMYSQL = new Crypt_AESMYSQL();
		$encrypted_pwd = $AESMYSQL->encrypt($detail[0]['PS_NUMBER'], $rand);
		$encreff = urlencode($encrypted_pwd);

		$this->_tableMstleft[2]["value"] = ($repeat == '1x') ? $repeat : $repeat .' (FreqRef# <a href="/paymentworkflow/ongoing/viewdetail/payReff/'.$encreff.'">'.$detail[0]['PS_PERIODIC_NUMBER'].'</a>)';

		//if sweep 
		if ($detail[0]['PS_CATEGORY'] == 'SWEEP PAYMENT') {

			$arrday = array(
					'0' => 'Sunday',
					'1' => 'Monday',
					'2' => 'Tuesday',
					'3' => 'Wednesday',
					'4' => 'Thursday',
					'5' => 'Friday',
					'6' => 'Saturday'
				);

			if ($detail[0]['TRA_STATUS'] == '4' && $detail[0]['PREV_BALANCE'] == NULL) {

				if ($detail[0]['CEK_PS_TYPE'] == '15' || $detail[0]['CEK_PS_TYPE'] == '20'){
					$tableMstSweepLeft[0]['label'] = $this->language->_('Beneficiary Balance');
				}else{
					$tableMstSweepLeft[0]['label'] = $this->language->_('Source Balance');
				}
				
				$tableMstSweepLeft[0]['value'] = '<span class="homeCardText">IDR n/a - Failed Inquiry</span>';

			}else{

				//sweep condition section
				if ($detail[0]['CEK_PS_TYPE'] == '14') {
				
					$tableMstSweepLeft[0]['label'] = $this->language->_('Source Balance');


					$tableMstSweepRight[0]['label'] = $this->language->_('Remains on source');
					$tableMstSweepRight[0]['value'] = 'IDR '.Application_Helper_General::displayMoney($detail[0]['TRA_REMAIN']);

				}else if($detail[0]['CEK_PS_TYPE'] == '19'){
					$tableMstSweepLeft[0]['label'] = $this->language->_('Source Balance');
					
					$tableMstSweepRight[0]['label'] = $this->language->_('Remains on source');
					$tableMstSweepRight[0]['value'] = 'IDR '.Application_Helper_General::displayMoney($detail[0]['TRA_REMAIN']);
					$tableMstSweepRight[1]['label'] = $this->language->_('Minimum Amount');
					$tableMstSweepRight[1]['value'] = 'IDR '.Application_Helper_General::displayMoney($detail[0]['PS_MIN_AMOUNT']);
				
				}
				else if ($detail[0]['CEK_PS_TYPE'] == '15' || $detail[0]['CEK_PS_TYPE'] == '20'){

					$tableMstSweepLeft[0]['label'] = $this->language->_('Beneficiary Balance');

					$maintainsData = '';
					$select1 = $this->_db->select()
						->from(array('C' => 'T_PERIODIC_DAY', array('C.DAY_ID')))
						->where("C.PERIODIC_ID  = ?", $detail[0]['PS_PERIODIC']);

					$PERIODIC_DAY = $this->_db->fetchAll($select1);
					foreach ($PERIODIC_DAY as $key => $value) {
						$maintainsData .= substr($arrday[$value['DAY_ID']], 0, 3) . ' IDR ' . Application_Helper_General::displayMoney($value['LIMIT_AMOUNT']) . '<br>';
					}

					$tableMstSweepRight[0]['label'] = $this->language->_('Maintains Beneficiary');
					$tableMstSweepRight[0]['value'] = $maintainsData;
				}else if($detail[0]['CEK_PS_TYPE'] == '23'){
					$tableMstSweepLeft[0]['label'] = $this->language->_('Source Balance');

					//echo '<pre>';
					//var_dump($detail[0]);die;
					$tableMstSweepRight[0]['label'] = $this->language->_('Remains on source');
					$tableMstSweepRight[0]['value'] = 'IDR '.Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']);
					$tableMstSweepRight[1]['label'] = $this->language->_('Minimum Amount');
					$tableMstSweepRight[1]['value'] = 'IDR '.Application_Helper_General::displayMoney($detail[0]['PS_MIN_AMOUNT']);
					
				}

				$tableMstSweepLeft[0]['value'] = 'IDR '.Application_Helper_General::displayMoney($detail[0]['PREV_BALANCE']);

			}

			$tableMstSweepLeft[1]['label'] = $this->language->_('Capture Time');
			$tableMstSweepLeft[1]['value'] = Application_Helper_General::convertDate($detail[0]['PREV_BALANCE_UPDATE_TIME'], $this->view->displayDateTimeFormat, 'D-M-Y H:i:s');
			//end sweep condition section

			$this->view->tableMstSweepLeft = $tableMstSweepLeft;
			$this->view->tableMstSweepRight = $tableMstSweepRight; 
		}
		if($detail[0]['PS_SUBJECT'] == ''){
			$detail[0]['PS_SUBJECT'] = 'no subject';
		}
		$this->_tableMstleft[3]["value"] = $detail[0]['PS_SUBJECT'];

		if ($detail[0]["CEK_PS_TYPE"] == "19" && $detail[0]['PS_TXCOUNT'] > 1) {
			
		}else{
			$this->_tableMstleft[4]["value"] = $detail[0]['TRA_MESSAGE'];
		}

		$this->_tableMstleft[5]["value"] = $detail[0]['TRA_ADDITIONAL_MESSAGE'];

		$this->_tableMstright[0]["label"] = $this->language->_('Payment Date');
		$this->_tableMstright[0]["value"] = substr(Application_Helper_General::convertDate($detail[0]['PS_EFDATE'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);

		$this->_tableMstright[1]["label"] = $this->language->_('Last Update');
		$this->_tableMstright[1]["value"] = substr(Application_Helper_General::convertDate($detail[0]['PS_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);

		$this->_tableMstright[2]["label"] = $this->language->_('Payment Type');

		// if (($detail[0]['PS_CATEGORY'] == 'SINGLE PAYMENT' || $detail[0]['PS_CATEGORY'] == 'OPEN TRANSFER')) {
		// 	if ($detail[0]['TRANSFER_TYPE'] == 'PB') {
		// 		if ($detail[0]['SOURCE_ACCT_BANK_CODE'] == $detail[0]['BENEF_ACCT_BANK_CODE']) {
		// 			$payType = $detail[0]['PS_TYPE'] . ' - ' . 'PB';
		// 		} else {
		// 			$payType = $detail[0]['PS_TYPE'] . ' - ' . 'ONLINE';
		// 		}
		// 	} else if ($detail[0]['TRANSFER_TYPE'] == 'SKN') {
		// 		$payType = $detail[0]['PS_TYPE'] . ' - ' . 'SKN';
		// 	} else if ($pslip['TRANSFER_TYPE'] == 'RTGS') {
		// 		$payType = $detail[0]['PS_TYPE'] . ' - ' . 'RTGS';
		// 	} else {
		// 		$payType = $detail[0]['PS_TYPE'];
		// 	}
		// } else if ($detail[0]['PS_CATEGORY'] == 'SWEEP PAYMENT') {

		// 	if ($detail[0]['CEK_PS_TYPE'] == '19') {
		// 		$poolingType = 'Remains';
		// 	} else {
		// 		$poolingType = 'Maintains';
		// 	}

		// 	$payType = $detail[0]['PS_TYPE'] . ' - ' . $poolingType;
		// } else {
		// 	$payType = $detail[0]['PS_TYPE'];
		// }

		$tra_type2	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");
		$tra_type3 = $tra_type2[$detail[0]['TRA_TYPE']];
                
        if ($detail[0]['CEK_PS_TYPE'] == '19') {
             $payType = 'CP Same Bank Remains';
        }else if ($detail[0]['CEK_PS_TYPE'] == '20') {
             $payType = 'CP Same Bank Maintains';
        }else if ($detail[0]['CEK_PS_TYPE'] == '23') {
             $payType = 'CP Others Remains - '.$tra_type3;
        }else if ($detail[0]['CEK_PS_TYPE'] == '21') {
             $payType = 'MM - '.$tra_type3;
        }else {
            $payType = $detail[0]['PS_TYPE'] . ' - ' . $detail[0]['TRANSFER_TYPE'];
        }

		$this->_tableMstright[2]["value"] = $payType;

		$this->_tableMstright[3]["label"] = $this->language->_('Total Payment');
		$this->_tableMstright[4]["label"] = $this->language->_('Transaction Status');

		if ($detail[0]['TRA_STATUS'] == '4' && $detail[0]['PREV_BALANCE'] == NULL) {
			$this->_tableMstright[3]["value"] = '<span class="homeCardText">IDR</span>';
		}else{
			if($detail[0]['CEK_PS_TYPE'] == '23' || $detail[0]['CEK_PS_TYPE'] == '20' || $detail[0]['CEK_PS_TYPE'] == '19'){
				$this->_tableMstright[3]["value"] = '<span class="text-danger"><b>' .$detail[0]['PS_CCY'] . " " . Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']).'</b></span>';
			}else{
				$this->_tableMstright[3]["value"] = '<span class="text-danger"><b>' .$detail[0]['PS_CCY'] . " " . Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']).'</b></span>';
			}
		}

		$this->_tableMstright[4]["value"] = $detail[0]['TRANSFER_STATUS'];


		$this->view->tableMstleft = $this->_tableMstleft;
		$this->view->tableMstright = $this->_tableMstright; 

		//data bene modal
		if ($detail[0]['PS_CATEGORY'] == 'SINGLE PAYMENT' || $detail[0]['PS_CATEGORY'] == 'OPEN TRANSFER') {
			if(!empty($detail[0]['BENEFICIARY_ID'])){
			 $this->getBenefData($detail[0]['BENEFICIARY_ID']);

			}else{
				$benefData = $detail[0];
				//echo '<pre>';
				//var_dump($detail[0]);die;
				$tableMst[0]["label"] = $this->language->_('Beneficiary Bank');
				$tableMst[1]["label"] = $this->language->_('City');
				$tableMst[2]["label"] = $this->language->_('Beneficiary Account');
				$tableMst[3]["label"] = $this->language->_('Beneficiary Name');
				$tableMst[4]["label"] = $this->language->_('Beneficiary Alias Name');
				$tableMst[5]["label"] = $this->language->_('Beneficiary Address 1');
				$tableMst[6]["label"] = $this->language->_('Beneficiary Address 2');
				$tableMst[7]["label"] = $this->language->_('Beneficiary Email');
				$tableMst[8]["label"] = $this->language->_('Citizenship');
				$tableMst[9]["label"] = $this->language->_('Nationality');
				$tableMst[10]["label"] = $this->language->_('Beneficiary Category');
				$tableMst[11]["label"] = $this->language->_('Beneficiary ID Type');
				$tableMst[12]["label"] = $this->language->_('Beneficiary ID Number');

				if (!empty($benefData['BENEF_ACCT_BANK_CODE'])) {
                      $benefBankName = Application_Helper_General::getBankTableName($benefData['BENEF_ACCT_BANK_CODE']);
                 }
                else{
                       $benefBankName = $this->masterBankName;
                }
				$tableMst[0]["value"] = $benefBankName;
				$tableMst[1]["value"] = empty($benefData['BANK_CITY']) ? '-' : $benefData['BANK_CITY'];
				$tableMst[2]["value"] = $benefData['BENEFICIARY_ACCOUNT'];
				$tableMst[3]["value"] = $benefData['BENEFICIARY_ACCOUNT_NAME'];
				$tableMst[4]["value"] = empty($benefData['BENEFICIARY_ALIAS_NAME']) ? '-' : $benefData['BENEFICIARY_ALIAS_NAME'];
				$tableMst[5]["value"] = empty($benefData['BENEFICIARY_ADDRESS']) ? '-' : $benefData['BENEFICIARY_ADDRESS'];
				$tableMst[6]["value"] = empty($benefData['BENEFICIARY_ADDRESS2']) ? '-' : $benefData['BENEFICIARY_ADDRESS2'];
				$tableMst[7]["value"] = empty($benefData['BENEFICIARY_EMAIL']) ? '-' : $benefData['BENEFICIARY_EMAIL'];
				$tableMst[8]["value"] = empty($benefData['BENEFICIARY_RESIDENT']) ? '-' : $residentArr[$benefData['BENEFICIARY_RESIDENT']];
				$tableMst[9]["value"] = empty($benefData['BENEFICIARY_CITIZENSHIP']) ? '-' : $citizenshipArr[$benefData['BENEFICIARY_CITIZENSHIP']];
				$tableMst[10]["value"] = empty($LLD_CATEGORY_POST) ? '-' : $LLD_CATEGORY_POST;
				$tableMst[11]["value"] = empty($benefData['BENEFICIARY_ID_TYPE']) ? '-' : $benefData['BENEFICIARY_ID_TYPE'];
				$tableMst[12]["value"] = empty($benefData['BENEFICIARY_ID_NUMBER']) ? '-' : $benefData['BENEFICIARY_ID_NUMBER'];

				$this->view->tableBene 		= $tableMst;
				
			}
		}

		//trans summary data
		$tableTransSum[0]["label"] = $this->language->_('Total Amount');
		$tableTransSum[1]["label"] = $this->language->_('Total Transfer Fee');
		$tableTransSum[2]["label"] = $this->language->_('Total Payment');

//var_dump($detail);
		$tableTransSum[0]["value"] = $detail[0]['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']);
		$tableTransSum[1]["value"] = $detail[0]['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($detail[0]['TRANSFER_FEE']);
		$tableTransSum[2]["value"] = $detail[0]['PS_CCY'] . ' ' . Application_Helper_General::displayMoney(($detail[0]['TRA_AMOUNT'] + $detail[0]['TRANSFER_FEE']));
		$this->view->tableTransSum 		= $tableTransSum;


		//if single payment then enable re execute 
		$allowReExecute = false;
		if (($detail[0]['PS_CATEGORY'] == 'SINGLE PAYMENT' || $detail[0]['PS_CATEGORY'] == 'OPEN TRANSFER') && $this->view->hasPrivilege('RXCT')) {
			$curDate = date('Y-m-d');
			if ($detail[0]['PS_EFDATE'] == $curDate) {

				$allowReExecute = true;

				$curTime = date('H:i:s');
				$settingObj = new Settings();
				$setting = array(
					"COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
					"COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00")
				);
				if ($detail[0]['TRANSFER_TYPE'] == 'SKN') {	
					if ($curTime > $setting['COT_SKN']) {
						$allowReExecute = false;
					}
				}
				else if($detail[0]['TRANSFER_TYPE'] == 'RTGS'){
					if ($curTime > $setting['COT_RTGS']) {
						$allowReExecute = false;
					}
				}

				$tokenType = $settingObj->getSetting('cut_off_time_skn', '1');

				$conf = Zend_Registry::get('config');

				$tokenTypeGlobalVar = $conf['token']['type']['code'];

				//if google auth
				if ($tokenType == $tokenTypeGlobalVar['googleauth']) {

					$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
	                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
					$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

					if (!empty($usergoogleAuth)) {
						if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
							$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
							$maxtoken = $settingObj->getSetting("max_failed_token");
							$tokenfail = (int)$maxtoken - (int)$usergoogleAuth['0']['USER_FAILEDTOKEN'];
							$this->view->tokenfail = $tokenfail;
						} 
					}
					else{
						$this->view->nogoauth = true;
					}

					$googleAuth = true;
					$this->view->googleauth = true;
				}
				else if($tokenType == $tokenTypeGlobalVar['hardtoken']){
					$hardToken = true;
					$this->view->hardToken = true;
					$Token 			= new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
					$challengeCode 	= $Token->generateChallengeCode();
					$this->view->challengeCode = $challengeCode;
				}
			}
		}

		$this->view->allowReExecute = $allowReExecute;

		$inputtoken1 		= $this->_getParam('inputtoken1');
		$inputtoken2 		= $this->_getParam('inputtoken2');
		$inputtoken3 		= $this->_getParam('inputtoken3');
		$inputtoken4 		= $this->_getParam('inputtoken4');
		$inputtoken5 		= $this->_getParam('inputtoken5');
		$inputtoken6 		= $this->_getParam('inputtoken6');
		$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

		//if google auth
		if ($reExecute && $googleAuth) {

			$pga = new PHPGangsta_GoogleAuthenticator();
			$setting 		= new Settings();
			$google_duration 	= $setting->getSetting('google_duration');
			if ($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, $google_duration)) {
				$datatoken = array(
															'USER_FAILEDTOKEN' => 0
														);

														$wheretoken =  array();
														$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
														$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
														$data = $this->_db->update('M_USER',$datatoken,$wheretoken);
			 	$resultToken = true;
			} else {
			 	$resultToken = false;
			 	$tokenFailed = $CustUser->setLogToken(); //log token activity
			 	if ($tokenFailed === true) {
			 		$this->_redirect('/default/index/logout');
			 	}
			}
		}
		//if hardtoken
		else if($reExecute && $hardToken){

			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?', $this->_userIdLogin)
			->where('CUST_ID = ?', $this->_custIdLogin)
			->limit(1);

			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 		= new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
			$verToken 	= $Token->verify($challengeCode, $responseCode);

			if ($verToken['ResponseCode'] != '00') {
				$resultToken = false;
				$tokenFailed = $CustUser->setLogToken(); //log token activity
				if ($tokenFailed === true) {
					$this->_redirect('/default/index/logout');
				}
			} else {
				$resultToken = true;
			}
		}
		
		//re Execute
		if ($reExecute && $resultToken) {
			$Payment = new Payment($detail[0]['PS_NUMBER'], $this->_custIdLogin, $this->_userIdLogin);
			$resultRelease = $Payment->reReleasePayment();

			if ($resultRelease['status'] == '00') {
				$this->setbackURL('/display/executedtransaction');
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage($detail[0]['PS_NUMBER']);
				$this->setbackURL('/display/executedtransaction');
				$this->_redirect('/notification/index/release');
			}
		}
		else if($reExecute && !$resultToken){
			//show token error
			$errorMsg = $this->language->_('Invalid Response Code');
			$this->view->popauth = true;
			$this->view->error_msg = $errorMsg;
		}


		//------------------------------------------------------------dibawah ini adalah code lama--------------------------------------------------------------



		//TEMPLATE DETAIL
		$htmldataDetailDetail = '<style>
input.currency{ width: 200px; font-size: 10px; }
ul { padding-left:10px; }
h2 { color: #5B0000; font-size: 16px; font-weight: bold } body .ui-helper-hidden-accessible { position: fixed }
body { font-family : Arial, Helvetica, sans-serif; font-size: 11px; background-color: white; }
b { font-weight:bold; font-size:11px }
td { font-family: Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-decoration:none; }
a { font-family: Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-decoration:underline; }
.tbl-evencontent { background-color: none; padding: 4px 4px 4px 4px; border-bottom: 1px solid #A9A9A9;border-right: 1px solid #A9A9A9;border-left: 1px solid #A9A9A9;border-top: 1px solid #A9A9A9;vertical-align: top; font-size: 10px; }
.tbl-oddcontent { background-color: none; padding: 4px 4px 4px 4px; border-bottom: 1px solid #A9A9A9;border-right: 1px solid #A9A9A9;border-left: 1px solid #A9A9A9;border-top: 1px solid #A9A9A9;vertical-align: top; font-size: 10px; }
.tdform-sub {background-color: #0c67a0;padding: 4px 4px 4px 4px; border-bottom: 0px solid #A9A9A9;border-top: 1px solid #A9A9A9;;border-right: 1px solid #A9A9A9;;border-left: 1px solid #A9A9A9; vertical-align: top; font-weight: bold; color: white; }
.tdform-odd { background-color: #FFFFFF; padding: 4px 4px 4px 4px; border: 1px solid; vertical-align: top; }
.errmsg { font-weight:bold; font-size:11px; color:#FF0000 }
body { font-family: "Trebuchet MS", "Helvetica", "Arial", "Verdana", "sans-serif";
-webkit-print-color-adjust:exact; /*chrome*/
-moz-print-color-adjust:exact; /*mozila*/
-o-print-color-adjust:exact; /*opera*/
-ms-print-color-adjust:exact; /*ie*/
}
	</style>';

		if ($cektype == 16 || $cektype == 17 || $cektype == 18) {
			$pslipdetail = $getPaymentDetail->getPslipDetail($psnumber);
			$htmldataDetailDetail .= '';
			foreach ($pslipdetail as $pslipdetaillist) {
				if ($pslipdetaillist['PS_FIELDTYPE'] == 1) {
					$value = Application_Helper_General::displayMoney($pslipdetaillist['PS_FIELDVALUE']);
				} elseif ($pslipdetaillist['PS_FIELDTYPE'] == 2) {
					$value = $pslipdetaillist['PS_FIELDVALUE'];
				} elseif ($pslipdetaillist['PS_FIELDTYPE'] == 3) {
					$value = $pslipdetaillist['PS_FIELDVALUE'];
				} else {
					$value = '';
				}

				$htmldataDetailDetail .= '
			<tr>
				<td class="tbl-evencontent">&nbsp; ' . $this->language->_($pslipdetaillist['PS_FIELDNAME']) . '</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">' . $this->language->_($value) . '</td>
			</tr>
			';
			}
			// 		print_r($pslipdetail);die;
			// 		$created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			// 		$updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			// 		$efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
			$htmldataDetail .=
				'<table cellspacing="0" cellpadding="5" class="table table-sm table-striped" width="100%">
			<tr>
				<td class="tbl-evencontent">' . $this->language->_('Payment Ref#') . '</td>
				
				<td class="tbl-evencontent">' . $paymentref . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">' . $this->language->_('Created Date') . '</td>
				
				<td class="tbl-evencontent">' . $created . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">' . $this->language->_('Updated Date') . '</td>
				
				<td class="tbl-evencontent">' . $updated . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">' . $this->language->_('Payment Date') . '</td>
				
				<td class="tbl-evencontent">' . $efdate . '</td>
			</tr>';
			if ($detail[0]['PS_BILLER_ID'] != '1156' || $cektype == 18) {
				$htmldataDetail .=
					'<tr>
				<td class="tbl-evencontent">' . $this->language->_('Payment Subject') . '</td>
				
				<td class="tbl-evencontent">' . $paysubject . '</td>
			</tr>';
			}
			$htmldataDetail .=
				'<tr>
				<td class="tbl-evencontent">' . $this->language->_('Source Account') . '</td>
				
				<td class="tbl-evencontent">' . $source . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">' . $this->language->_('Payment Type') . '</td>
				
				<td class="tbl-evencontent">' . $this->language->_($type) . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">' . $this->language->_('Payment Status') . '</td>
				
				<td class="tbl-evencontent">' . $this->language->_($paystatus) . '</td>
			</tr>
		</table>';
		} else {
			/*<tr>
				<td class="tbl-evencontent">&nbsp; Transmission</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$transmission.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; Trace No.</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$traceno.'</td>
		</tr>
		<tr>
				<td class="tbl-evencontent">&nbsp; Challenge Code</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$challenge.'</td>
		</tr>

		<tr>
				<td class="tbl-evencontent">&nbsp; Total Payment</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$amount.'</td>
		</tr>
		*/

			$htmldataDetail =
				'<table cellspacing="0" cellpadding="5" class="table table-sm table-striped" width="100%">
      <tr>
        <td class="tbl-evencontent">&nbsp; ' . $this->language->_('Payment Type') . '</td>

        <td class="tbl-evencontent">' . $this->language->_($type) . '</td>
      </tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; ' . $this->language->_('Released Completed') . '</td>

				<td class="tbl-evencontent">' . $efdate . '</td>
			</tr>
      <tr>
        <td class="tbl-evencontent">&nbsp; ' . $this->language->_('Total Payment') . '</td>

        <td class="tbl-evencontent">' . $amount . '</td>
      </tr>

		</table>';
		}

		// TEMPLATE TRANSACTION
		//$cektype  11 utnutk purchase
		//$cektype  1 utnutk transfer
		//$cektype  12 utnutk payment

		//if($cektype != 11 && $cektype != 12)
		if ($cektype != 110 && $cektype != 120) {
			// 		print_r($detail);die;
			$i = 1;
			$htmlTransactionDetail = '';
			$htmldataTransactionDetail = '';
			foreach ($detail as $transactionlist) {
				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';

				if ($transactionlist['BENEFICIARY_CITIZENSHIP'] == 'R') {
					$citizenship = 'Resident';
				} elseif ($transactionlist['BENEFICIARY_CITIZENSHIP'] == 'NR') {
					$citizenship = 'Non Resident';
				} else {
					$citizenship = '';
				}

				if ($transactionlist['BENEFICIARY_CITIZENSHIP'] == 'R') {
					$citizenship = 'Resident';
				} elseif ($transactionlist['BENEFICIARY_CITIZENSHIP'] == 'NR') {
					$citizenship = 'Non Resident';
				} else {
					$citizenship = '';
				}

				if ($cektype == '16' || $cektype == '17') {
					$detailLog = json_decode($transactionlist['LOG']);
				}

				if ($cektype == '16') { //type purchase
					$BENEFICIARY_ACCOUNT = '-';
					$BENEFICIARY_ACCOUNT_NAME = '-';
				} elseif ($cektype == '17') { //payment
					$BENEFICIARY_ACCOUNT = '-';
					$BENEFICIARY_ACCOUNT_NAME = '-';
				} else {
					$BENEFICIARY_ACCOUNT = $transactionlist['BENEFICIARY_ACCOUNT'];
					$BENEFICIARY_ACCOUNT_NAME = $transactionlist['BENEFICIARY_ACCOUNT_NAME'];
				}
				//<td class="'.$td_css.'">'.$transactionlist['TRANSACTION_ID'].'</td>
				//<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($transactionlist['TRANSFER_FEE']).'</td>
				//<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_BANK_CITY'].'</td>
				//<td class="'.$td_css.'">'.$citizenship.'</td>

				// 9. Create LLD string
				$settings 			= new Application_Settings();
				$LLD_array 			= array();
				$LLD_DESC_arrayCat 	= array();
				$lldTypeArr  		= $settings->getLLDDOMType();

				if (!empty($transactionlist['BENEFICIARY_CATEGORY'])) {
					$lldCategoryArr  	= $settings->getLLDDOMCategory();
					$LLD_array["CT"] 	= $transactionlist['BENEFICIARY_CATEGORY'];
					$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$transactionlist['BENEFICIARY_CATEGORY']];
				} else {
					$LLD_CATEGORY_POST = '-';
				}

				if (!empty($transactionlist['BENEFICIARY_ID_TYPE'])) {
					$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
					$LLD_array["CT"] 	= $transactionlist['BENEFICIARY_ID_TYPE'];
					$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$transactionlist['BENEFICIARY_ID_TYPE']];
				} else {
					$LLD_BENEIDENTIF_POST = '-';
				}

				//add hamdan
				if (!empty($transactionlist['BENEFICIARY_ID_NUMBER'])) {
					$BENEFICIARY_ID_NUMBER = $transactionlist['BENEFICIARY_ID_NUMBER'];
				} else {
					$BENEFICIARY_ID_NUMBER = '-';
				}
				if (!empty($arr[0]['CITY_NAME'])) {
					$CITY_NAME = $arr[0]['CITY_NAME'];
				} else {
					$CITY_NAME = '-';
				}
				//end

				$modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();
				$CITY_CODEGet = (!empty($transactionlist['BENEFICIARY_CITY_CODE']) ? $transactionlist['BENEFICIARY_CITY_CODE'] : '');
				$arr 					= $modelCity->getCityCode($CITY_CODEGet);
				// 			echo "<pre>";
				// 			print_r($transactionlist);die;
				if (empty($transactionlist['COUNTRY_CODE'])) {
					$transactionlist['COUNTRY_CODE'] = 'ID';
				}

				$country = $getPaymentDetail->getCountry($transactionlist['COUNTRY_CODE']);
				$rate = '0.00';
				//			print_r($transactionlist);die;
				if ($transactionlist['SOURCE_ACCOUNT_CCY'] == 'USD' && $transactionlist['BENEFICIARY_ACCOUNT_CCY'] == 'USD') {
					$rate = '0.00';
				} else {
					if ($transactionlist['EQUIVALENT_AMOUNT_IDR'] != '0.00') {
						$rate = $transactionlist['RATE'];
					}
				}

				if ($transactionlist['TRANSFER_TYPE'] == "OUR" || $transactionlist['TRANSFER_TYPE'] == "SHA") {

					/*	if($transactionlist['PS_CCY'] != "IDR")
					$totalinidr = (($transactionlist['TRA_AMOUNT'] + $transactionlist['FULL_AMOUNT_FEE'] + $transactionlist['PROVISION_FEE']) * Application_Helper_General::convertDisplayMoney($transactionlist['RATE'])) + $transactionlist['TRANSFER_FEE'];
			//print_r($totalinidr);die;
					if($transactionlist['RATE_BUY'] == NULL)
						$totalinvalas = $transactionlist['TRA_AMOUNT'];
					else
						$totalinvalas = $totalinidr / Application_Helper_General::convertDisplayMoney($transactionlist['RATE_BUY']);

				$rate = $transactionlist['RATE'];
		*/
					//if($transactionlist['SOURCE_ACCOUNT_CCY']=='USD' && $transactionlist['BENEFICIARY_ACCOUNT_CCY']=='USD'){
					$totalinvalas = $transactionlist['EQUIVALENT_AMOUNT_IDR'];
					//}

				} else {
					$totalinvalas = $transactionlist['TRA_AMOUNT'];
					$totalinidr = $transactionlist['EQUIVALENT_AMOUNT_IDR'];
				}



				if ($transactionlist['TRANSFER_TYPE'] == 'No FA' || $transactionlist['TRANSFER_TYPE'] == 'FA') {
					$BENEFICIARY_ACCOUNT = '';
				}

				//print_r($transactionlist);die;
				if ($transactionlist['TRANSFER_TYPE'] == "OUR" || $transactionlist['TRANSFER_TYPE'] == "SHA") {
					if ($transactionlist['SOURCE_ACCOUNT_CCY'] == 'USD' && $transactionlist['BENEFICIARY_ACCOUNT_CCY'] == 'USD') {
						$amount_text = '<td class="' . $td_css . '">' . 'USD ' . Application_Helper_General::displayMoney($totalinvalas) . '</td>';
					} else {
						$amount_text = '<td class="' . $td_css . '">' . 'IDR ' . Application_Helper_General::displayMoney($totalinvalas) . '</td>';
					}
				} elseif (!empty($transactionlist['BALANCE_TYPE']) && $transactionlist['BALANCE_TYPE'] == '2') {
					$amount_text = '<td class="' . $td_css . '">' . $transactionlist['PS_TOTAL_AMOUNT'] . '% (' . $transactionlist['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($totalinvalas) . ')</td>';
				} else {
					$amount_text = '<td class="' . $td_css . '">' . $transactionlist['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($totalinvalas) . '</td>';
				}

				if ($transactionlist['TRANSFER_TYPE'] == 'PB') {
					$transactionlist['TRANSFER_TYPE'] = 'In House';
				}
				//print_r($transactionlist);die;
				if ($transactionlist['TRANSFER_TYPE'] == 'No FA') {
					$transactionlist['FULL_AMOUNT_FEE'] = 0;
				} else {
					$totalinvalas = $transactionlist['TRA_AMOUNT'] + $transactionlist['FULL_AMOUNT_FEE'];
					$amount_text = '<td class="' . $td_css . '">' . $transactionlist['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($totalinvalas) . '</td>';
				}

				if ($transactionlist['SOURCE_ACCOUNT_CCY'] == 'USD' && $transactionlist['BENEFICIARY_ACCOUNT_CCY'] == 'USD') {
					$rate = '0.00';
				}
				$htmldataTransactionDetail .= '
				<tr>
					<td class="' . $td_css . '">' . $BENEFICIARY_ACCOUNT_NAME . '</td>
					<td class="' . $td_css . '">' . $BENEFICIARY_ACCOUNT . '</td>
					<td class="' . $td_css . '">' . $transactionlist['BENEFICIARY_ID_NUMBER'] . '</td>
					<td class="' . $td_css . '">' . $transactionlist['BENEFICIARY_MOBILE_PHONE_NUMBER'] . '</td>';
				if ($transactionlist['TRANSFER_TYPE'] == 'No FA' || $transactionlist['TRANSFER_TYPE'] == 'FA') {
					$code = substr($transactionlist['PS_NUMBER'], -6);
					$tracode = chunk_split($code, 3, ' ');
					$htmldataTransactionDetail .= '<td class="' . $td_css . '">' . $tracode . '</td>';
				}

				$selectBank	= $this->_db->select()
					->from(
						array('B' => 'M_BANK_TABLE'),
						array(
							'BANK_CODE'			=> 'B.BANK_CODE',
							'BANK_NAME'			=> 'B.BANK_NAME'
						)
					);

				$bankList = $this->_db->fetchAll($selectBank);

				foreach ($bankList as $key => $value) {
					$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
				}

				if (!empty($transactionlist['BANK_CODE'])) {
					$bankcode = $transactionlist['BANK_CODE'];
				} else {
					$bankcode = $transactionlist['BENEF_ACCT_BANK_CODE'];
				}

				if ($transactionlist['PS_CATEGORY'] == 'Payment' || $transactionlist['PS_CATEGORY'] == 'Purchase') {
					$bankname = '-';
				} else if (empty($bankcode)) {
					$bankname = $this->_bankName;
				} else {
					$bankname = $bankNameArr[$bankcode];
				}


				$htmldataTransactionDetail .= '
					<td class="' . $td_css . '">' . $transactionlist['TRA_MESSAGE'] . '</td>
					<td class="' . $td_css . '">' . $transactionlist['TRA_ADDITIONAL_MESSAGE'] . '</td>
					<td class="' . $td_css . '">' . $this->language->_($transactionlist['TRANSFER_TYPE']) . '</td>
					<td class="' . $td_css . '">' . $transactionlist['PS_CCY'] . '</td>

					<td class="' . $td_css . '">' . Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']) . '</td>
					<td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($rate) . '</td>
					<td class="' . $td_css . '">' . $transactionlist['SOURCE_ACCOUNT_CCY'] . " " . Application_Helper_General::displayMoney($transactionlist['TRANSFER_FEE']) . '</td>
					<td class="' . $td_css . '">' . $transactionlist['PS_CCY'] . " " . Application_Helper_General::displayMoney($transactionlist['FULL_AMOUNT_FEE']) . '</td>
					<td class="' . $td_css . '">USD ' . Application_Helper_General::displayMoney($transactionlist['PROVISION_FEE']) . '</td>
					' . $amount_text . '


					<td class="' . $td_css . '">' . $bankname . '</td>
					<td class="' . $td_css . '">' . $transactionlist['NOSTRO_NAME'] . '</td>

					<td class="' . $td_css . '">' . $this->language->_($transactionlist['TRANSFER_STATUS']) . '</td>
				</tr>
				';


				if (!empty($transactionlist['BENEFICIARY_CITIZENSHIP_COUNTRY'])) {
					$benecountry = $getPaymentDetail->getCountry($transactionlist['BENEFICIARY_CITIZENSHIP_COUNTRY']);
				} else {
					$benecountry = '';
				}

				$SourceAcount = $this->_db->select()
					->from(
						array('M_CUSTOMER_ACCT'),
						array('*')
					)
					->where('ACCT_NO = ?', $transactionlist['SOURCE_ACCOUNT']);

				$SourceAcountData = $this->_db->fetchRow($SourceAcount);
				//				print_r($SourceAcountData);die;
				// 			   	print_r(ucfirst($country));
				// 			   	die;
				$citizenship = ($SourceAcountData['ACCT_RESIDENT'] == "R") ? 'Resident' : 'Non Resident';
				$benecitizenship = ($transactionlist['BENEFICIARY_RESIDENT'] == "R") ? 'Resident' : 'Non Resident';

				$country     = ($SourceAcountData['ACCT_CITIZENSHIP'] == "W") ? 'WNI' : 'WNA';
				$benecountry = ($transactionlist['BENEFICIARY_CITIZENSHIP'] == "W") ? 'WNI' : 'WNA';

				$settings 			= new Application_Settings();

				$lldCategoryArr  	= $settings->getLLDDOMCategory();
				$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
				$lldRelationshipArr = $settings->getLLDDOMRelationship();

				if (!empty($transactionlist['LLD_TRANSACTION_PURPOSE'])) {
					$purpose = $this->_db->select()
						->from(
							array('M_TRANSACTION_PURPOSE'),
							array('*')
						)
						->where('CODE = ?', $transactionlist['LLD_TRANSACTION_PURPOSE']);

					$purposeData = $this->_db->fetchRow($purpose);
				}

				$category = $lldCategoryArr[$SourceAcountData['ACCT_CATEGORY']];
				//print_r($transactionlist);die;
				$htmlTransactionDetail .= '
				<tr>
					<td class="' . $td_css . '">' . $transactionlist['TRANSACTION_ID'] . '</td>
					<td class="' . $td_css . '">Sender Citizenship : ' . $citizenship . '; Sender Nationality : ' . ucfirst($country) . ' ; Sender Category : ' . $category . '; Destination Citizenship : ' . $benecitizenship . '; Destination Nationality : ' . $benecountry . '; Destination Category : ' . $lldCategoryArr[$transactionlist['BENEFICIARY_CATEGORY']] . '; Identity : ' . $lldIdenticalArr[$transactionlist['LLD_IDENTITY']] . '; Transactor Relationship : ' . $lldRelationshipArr[$transactionlist['LLD_TRANSACTOR_RELATIONSHIP']] . '; Transaction Purpose : ' . $purposeData['DESCRIPTION'] . '</td>
				</tr>';

				$i++;
				//<td class="'.$td_css.'">'.$transactionlist['TRA_REFNO'].'</td> //additional message
			}
			if (empty($transactionlist)) {
				$htmldataTransactionDetail = '
			<tr>
				<td class="tbl-evencontent" colspan="15" align="center">' . $this->language->_('No Data') . '</td>
			</tr>';

				$htmlTransactionDetail = '
			<tr>
				<td class="tbl-evencontent" colspan="2" align="center">' . $this->language->_('No Data') . '</td>
			</tr>';
			}


			//<th valign="top">Transaction ID</th>
			//<th valign="top">Charge</th>
			//<th valign="top">City</th>
			//<th valign="top">Citizenship</th>

			if (!empty($transactionlist['EQUIVALENT_AMOUNT_IDR'])) {
				if ($transactionlist['TRANSFER_TYPE'] == "OUR" || $transactionlist['TRANSFER_TYPE'] == "SHA") {
					if ($transactionlist['SOURCE_ACCOUNT_CCY'] == 'USD' && $transactionlist['BENEFICIARY_ACCOUNT_CCY'] == 'USD') {
						$detailpayment = $this->language->_('Total 1 payment(s), Total') . ' USD ' . Application_Helper_General::displayMoney($totalinvalas);
					} else {
						$detailpayment = $this->language->_('Total 1 payment(s), Total') . ' IDR ' . Application_Helper_General::displayMoney($totalinvalas);
					}
				}
$td_css = '';
				$htmldataTransaction =
					'<div class=""><table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-sm table-striped" style="width:100%">
      <tr class="headercolor">
        <td width="350px" valign="top">' . $this->language->_('Beneficiary Account Name') . ' </td>
        <td class="' . $td_css . '">' . $BENEFICIARY_ACCOUNT_NAME . '</td>
      </tr>
      <tr>
        <td valign="top">' . $this->language->_('Beneficiary Account') . '</td>
        <td class="' . $td_css . '">' . $BENEFICIARY_ACCOUNT . '</td>
      </tr>
      <tr>
        <td valign="top">' . $this->language->_('Beneficiary NRC') . ' </td>
        <td class="' . $td_css . '">' . $transactionlist['BENEFICIARY_ID_NUMBER'] . '</td>
      </tr>
      <tr>
        <td valign="top">' . $this->language->_('Beneficiary Phone') . '</td>
        <td class="' . $td_css . '">' . $transactionlist['BENEFICIARY_MOBILE_PHONE_NUMBER'] . '</td>
      </tr>';
				if ($transactionlist['TRANSFER_TYPE'] == 'No FA' || $transactionlist['TRANSFER_TYPE'] == 'FA') {
					$htmldataTransaction .= '<th valign="top">' . $this->language->_('Verification Code') . ' </th>';
				}
				$htmldataTransaction .= '
        <tr>
          <td valign="top">' . $this->language->_('Message') . '</td>
          <td class="' . $td_css . '">' . $transactionlist['TRA_MESSAGE'] . '</td>
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Additional Message') . '</td>
          <td class="' . $td_css . '">' . $transactionlist['TRA_ADDITIONAL_MESSAGE'] . '</td>
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Transfer Type') . '</td>
          <td class="' . $td_css . '">' . $this->language->_($transactionlist['TRANSFER_TYPE']) . '</td>
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('CCY') . '</td>
          <td class="' . $td_css . '">' . $transactionlist['PS_CCY'] . '</td>
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Amount') . '</td>
          <td class="' . $td_css . '">' . Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']) . '</td>
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Rate') . '</td>
          <td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($rate) . '</td>
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Transfer Fee') . '</td>
          <td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($rate) . '</td>
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Full Amount Fee') . '</td>
          <td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($rate) . '</td>
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Provision Fee') . '</td>
          <td class="' . $td_css . '">USD ' . Application_Helper_General::displayMoney($transactionlist['PROVISION_FEE']) . '</td>

        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Total') . '</td>
          ' . $amount_text . '
        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Beneficiary Bank') . '</td>
          <td class="' . $td_css . '">' . $bankname . '</td>

        </tr>
        <tr>
        	<td valign="top">' . $this->language->_('Nostro Bank') . '</td>
          <td class="' . $td_css . '">' . $transactionlist['NOSTRO_NAME'] . '</td>

        </tr>
        <tr>
          <td valign="top">' . $this->language->_('Transaction Status') . '</td>
          <td class="' . $td_css . '">' . $this->language->_($transactionlist['TRANSFER_STATUS']) . '</td>
        </tr>
		</table></div>
		<br>
		<b>
					' . $detailpayment . '</b>';
				// $htmldataTransaction .= '<br/><br/><div class="portlet-body flip-scroll"><table border="1" cellspacing="0" cellpadding="0" class="table table-bordered table-condensed flip-content">
				// 	<tr class="headercolor">
				//
				// 		<th valign="top">'.$this->language->_('Transaction Ref#').' </th>
				// 		<th valign="top">'.$this->language->_('LLD Content').'</th>
				// 	</tr>'
				// 	.$htmlTransactionDetail.'
				// </table>';

			} else {
				$htmldataTransaction =
					'<div class="portlet-body flip-scroll"><table border="1" cellspacing="0" cellpadding="0" class="table table-bordered table-condensed flip-content">
			<tr class="headercolor">

				<th valign="top">' . $this->language->_('Beneficiary Account Name') . ' </th>
				<th valign="top">' . $this->language->_('Beneficiary Account') . '</th>

				<th valign="top">' . $this->language->_('Message') . '</th>
				<th valign="top">' . $this->language->_('Additional Message') . '</th>
				<th valign="top">' . $this->language->_('Transfer Type') . '</th>
				<th valign="top">' . $this->language->_('CCY') . '</th>


				<th valign="top">' . $this->language->_('Amount') . '</th>
				<th valign="top">' . $this->language->_('Rate') . '</th>
				<th valign="top">' . $this->language->_('Transfer Fee') . '</th>
				<th valign="top">' . $this->language->_('Full Amount Fee') . '</th>


				<th valign="top">' . $this->language->_('Provision Fee') . '</th>
				<th valign="top">' . $this->language->_('Total') . '</th>
				<th valign="top">' . $this->language->_('Beneficiary Bank') . '</th>
				<th valign="top">' . $this->language->_('Nostro Bank') . '</th>
				<th valign="top">' . $this->language->_('Transaction Status') . '</th>
			</tr>'
					. $htmldataTransactionDetail . '
		</table></div>
		<br>
		<b>
					' . $this->language->_('Total 1 payment(s), Total') . ' ' . $transactionlist['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']) . ')</b>';

				$htmldataTransaction .= '<table border="1" cellspacing="0" cellpadding="0" class="table table-bordered table-condensed flip-content">
			<tr class="headercolor">

				<th valign="top">' . $this->language->_('Transaction Ref#') . ' </th>
				<th valign="top">' . $this->language->_('LLD Content') . '</th>
			</tr>'
					. $htmlTransactionDetail . '
		</table>';
			}
			//Etax
			if (($cektype == 16 || $cektype == 17) && $detail[0]['PS_BILLER_ID'] == '1158') {
				$compulsory = array('0' => 'No', '1' => 'Yes');
				$identity = array(
					'1' => 'KTP',
					'2' => 'NPWP',
					'3' => 'SIM',
					'4' => 'PASPOR',
					'5' => 'KITAS'
				);
				// roki
				$month = array(
					$this->language->_('January'),
					$this->language->_('February'),
					$this->language->_('March'),
					$this->language->_('April'),
					$this->language->_('May'),
					$this->language->_('June'),
					$this->language->_('July'),
					$this->language->_('August'),
					$this->language->_('September'),
					$this->language->_('October'),
					$this->language->_('November'),
					$this->language->_('December')
				);
				$map_code = array();
				$depositType = array();
				$select = $this->_db->select()
					->from(array('M_MAP_CODE'), array('MAP_CODE', 'MAP_NAME'))
					->query()->fetchAll();

				foreach ($select as $key) {
					$map_code[$key['MAP_CODE']] = $key['MAP_NAME'] . " (" . $key['MAP_CODE'] . ")";
				}
				if (isset($detailLog->akuncode)) {
					$select = $this->_db->fetchAll(
						$this->_db->select()
							->from(array('M_MAP_CODE'), array('DEPOSIT_CODE', 'DEPOSIT_NAME'))
							->where('MAP_CODE = ?', $detailLog->akuncode)
					);

					foreach ($select as $key) {
						$depositType[$key['DEPOSIT_CODE']] = $key['DEPOSIT_NAME'] . " (" . $key['DEPOSIT_CODE'] . ")";
					}
				}
				//billing code masih hardcode Roki 01/04/2020
				$htmldataTransaction =
					'<div class=""><table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-sm table-striped">
						<tr>
							<td valign="top">' . $this->language->_('Billing Code') . '</td>
							<td class="' . $td_css . '">2020032300014203</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Amount') . '</td>
							<td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']) . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Payer NPWP') . '</td>
							<td class="' . $td_css . '">' . $detailLog->payerNpwp . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Payer Name') . '</td>
							<td class="' . $td_css . '">' . $detailLog->payername . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Compulsory') . '</td>
							<td class="' . $td_css . '">' . $compulsory[$detailLog->chargetype] . ' ' . ($detailLog->chargetype == '1' ? ' - (' . $detailLog->chargeid . ')' : '') . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Asessable NPWP') . '</td>
							<td class="' . $td_css . '">' . $detailLog->asessableNpwp . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Asessable Name') . '</td>
							<td class="' . $td_css . '">' . $detailLog->asessablename . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Asessable Address ') . '</td>
							<td class="' . $td_css . '">' . $detailLog->asessableaddress . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Asessable City') . '</td>
							<td class="' . $td_css . '">' . $detailLog->asessablecity . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Asessable Identity') . '</td>
							<td class="' . $td_css . '">' . $identity[$detailLog->asessableidentity1] . ' - ' .  $detailLog->asessableidentity2 . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Map / Akun Code') . '</td>
							<td class="' . $td_css . '">' . $map_code[$detailLog->akuncode] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Deposit Type') . '</td>
							<td class="' . $td_css . '">' . $depositType[$detailLog->deposittype] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Tax Object Number (NOP)') . '</td>
							<td class="' . $td_css . '">' . $detailLog->taxobjectnumber . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('SK Number') . '</td>
							<td class="' . $td_css . '">' . $detailLog->skNumber . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Remark') . '</td>
							<td class="' . $td_css . '">' . $detailLog->remark . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Tax Period  Payment') . '</td>
							<td class="' . $td_css . '">' . $month[$detailLog->month1] . ' ' . $this->language->_('to') . ' ' . $month[$detailLog->month2] . ' ' . $detailLog->periodic . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('NTB / NTP') . '</td>
							<td class="' . $td_css . '"></td>
						<tr>
							<td valign="top">' . $this->language->_('NTPN') . '</td>
							<td class="' . $td_css . '"></td>
						<tr>
							<td valign="top">' . $this->language->_('STAN ') . '</td>
							<td class="' . $td_css . '"></td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Transaction Status') . '</td>
							<td class="' . $td_css . '">' . $this->language->_($transactionlist['TRANSFER_STATUS']) . '</td>
						</tr>
				</table></div>';
			} elseif (($cektype == 16 || $cektype == 17) && $detail[0]['PS_BILLER_ID'] == '1156') {
				$typeOfTax = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('M_SERVICE_PROVIDER'), array('PROVIDER_NAME'))
						->where('PROVIDER_ID = ?', $detail[0]['PS_BILLER_ID'])
				);
				$htmldataTransaction =
					'<div class=""><table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-sm table-striped">
						<tr>
							<td valign="top">' . $this->language->_('Type of Tax') . '</td>
							<td class="' . $td_css . '">' . $typeOfTax['PROVIDER_NAME'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Billing Code') . '</td>
							<td class="' . $td_css . '">' . $detailLog->orderId . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Amount') . '</td>
							<td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']) . '</td>
						</tr>';

				$billingPrefix = substr($detailLog->orderId, 0, 1);
				if ($billingPrefix == '0' || $billingPrefix == '1' || $billingPrefix == '2' || $billingPrefix == '3') {
					$htmldataTransaction .=
						'<tr>
									<td valign="top">' . $this->language->_('NPWP') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->npwp . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Customer Name') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->customer_name . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Customer Address') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->customer_address . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Map / Akun Code') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->account_map . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Deposit Type') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->type . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Tax Object Number (NOP)') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->NOP . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('SK Number') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->sk_number . '</td>
								</tr>';
				} elseif ($billingPrefix == '4' || $billingPrefix == '5' || $billingPrefix == '6') {
					$htmldataTransaction .=
						'<tr>
									<td valign="top">' . $this->language->_('Customer Name') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->customer_name . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('ID Type Customer') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->customer_id . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Document Type') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->document_type . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Document Number') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->document_number . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Document Date') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->document_date . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('KPBC Code') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->kppbc_code . '</td>
								</tr>';
				} elseif ($billingPrefix == '7' || $billingPrefix == '8' || $billingPrefix == '9') {
					$htmldataTransaction .=
						'<tr>
									<td valign="top">' . $this->language->_('Customer Name') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->customer_name . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('K/L') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->k_l . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Echelon Unit 1') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->eselon_unit . '</td>
								</tr>
								<tr>
									<td valign="top">' . $this->language->_('Code Unit') . '</td>
									<td class="' . $td_css . '">' . $detailLog->dataUi->work_unit . '</td>
								</tr>';
				}

				$htmldataTransaction .=
					'<tr>
							<td valign="top">' . $this->language->_('NTB / NTP') . '</td>
							<td class="' . $td_css . '">' . $detailLog->dataUi->ntb . '</td>
						<tr>
							<td valign="top">' . $this->language->_('NTPN') . '</td>
							<td class="' . $td_css . '">' . $detailLog->dataUi->ntpn . '</td>
						<tr>
							<td valign="top">' . $this->language->_('STAN ') . '</td>
							<td class="' . $td_css . '">' . $detailLog->dataUi->stan . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Transaction Status') . '</td>
							<td class="' . $td_css . '">' . $this->language->_($transactionlist['TRANSFER_STATUS']) . '</td>
						</tr>
				</table></div>';
			}

			//sp2d
			if ($cektype == 18) {
				$htmldataTransaction =
					'<div class=""><table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-sm table-striped">
						<tr>
							<td valign="top">' . $this->language->_('No SP2D') . '</td>
							<td class="' . $td_css . '">' . $detail[0]['SP2D_NO'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('SP2D Date') . '</td>
							<td class="' . $td_css . '">' . Application_Helper_General::convertDate($detail[0]['SP2D_DATE'], $this->_dateViewFormat) . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('No SPM') . '</td>
							<td class="' . $td_css . '">' . $detail[0]['SPM_NO'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('SPM Date') . '</td>
							<td class="' . $td_css . '">' . Application_Helper_General::convertDate($detail[0]['SPM_DATE'], $this->_dateViewFormat) . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('SKPD Name') . '</td>
							<td class="' . $td_css . '">' . $detail[0]['SKPD_NAME'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('SPP Type') . '</td>
							<td class="' . $td_css . '">' . $detail[0]['SPP_TYPE'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Beneficiary Account Name') . '</td>
							<td class="' . $td_css . '">' . $detail[0]['BENEFICIARY_ACCOUNT_NAME'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Beneficiary Account Number') . '</td>
							<td class="' . $td_css . '">' . $detail[0]['BENEFICIARY_ACCOUNT'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Beneficiary Identity') . '</td>
							<td class="' . $td_css . '">' . $detail[0]['BENEFICIARY_ID_TYPE'] . ' ' . $detail[0]['BENEFICIARY_ID_NUMBER'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Transaction Purpose ') . '</td>
							<td class="' . $td_css . '">' . $detail[0]['LLD_TRANSACTION_PURPOSE'] . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Description') . '</td>
							<td class="' . $td_css . '">' . nl2br($detail[0]['LLD_DESC']) . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Actual Amount') . '</td>
							<td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']) . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Deduction Amount') . '</td>
							<td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($detail[0]['TOTAL_CHARGES']) . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Total Amount') . '</td>
							<td class="' . $td_css . '">IDR ' . Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']) . '</td>
						</tr>
						<tr>
							<td valign="top">' . $this->language->_('Transaction Status') . '</td>
							<td class="' . $td_css . '">' . $this->language->_($transactionlist['TRANSFER_STATUS']) . '</td>
						</tr>
						</table></div>';
			}
		}

		// TEMPLATE HISTORY
		{
			$i = 1;
			$htmldataHistoryDetail = '';

			if (is_array($history)) {
				
				
				foreach ($history as $historylist) {
					if ($historylist['HISTORY_STATUS'] == "Approve") {
						if($i==1){
							$lastApprove_user = $historylist['USER_LOGIN'];
							$lastApprove_time = $historylist['DATE_TIME'];
						}else{
							if ($lastApprove_time < $historylist['DATE_TIME']) {
								$lastApprove_time = $historylist['DATE_TIME'];
								$lastApprove_user = $historylist['USER_LOGIN'];
							}
						}
					}
					$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';

					$htmldataHistoryDetail .= '
				<tr>
					<td class="' . $td_css . '">' . $historylist['USER_LOGIN'] . '</td>
					<td class="' . $td_css . '">' . $this->language->_($historylist['HISTORY_STATUS']) . '</td>
					<td class="' . $td_css . '">' . Application_Helper_General::convertDate($historylist['DATE_TIME'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . '</td>
					<td class="' . $td_css . '">' . $historylist['PS_REASON'] . '</td>
				</tr>
				';
					$i++;
				}
			}

			if (empty($history)) {
				$htmldataHistoryDetail = '
				<tr>
					<td class="tbl-evencontent" colspan="5" align="center">' . $this->language->_('No Data') . '</td>
				</tr>';
			}

			$htmldataHistory =
				'<div class="container">
        <div class="row">
            <div align="left" class="col-md-6">
                <p> ' . $this->language->_('Approved By') . '</p>
            </div>
            <div align="right" class="col-md-6">
                <p> ' . $lastApprove_user. '   </p>
            </div>

        </div>
    </div>

    <table  cellspacing="0" cellpadding="0" class="table table-sm table-striped">
			<tr class="headercolor">
				<th valign="top">' . $this->language->_('User') . '</th>
				<th valign="top">' . $this->language->_('Action') . '</th>
				<th valign="top">' . $this->language->_('Date/Time') . '</th>
				<th valign="top">' . $this->language->_('Reason') . '</th>
			</tr>'
				. $htmldataHistoryDetail . '
		</table>';

			$i = 1;
			$htmldataHistoryAproveDetail = '';
			foreach ($historyaprove as $historylist) {
				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';

				$htmldataHistoryDetailAprove .= '
				<tr>
					<td class="' . $td_css . '">' . $historylist['USER_LOGIN'] . '</td>
					<td class="' . $td_css . '">' . Application_Helper_General::convertDate($historylist['DATE_TIME'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . '</td>

				</tr>
				';
				$i++;
			}
			if (empty($history)) {
				$htmldataHistoryDetailAprove = '
				<tr>
					<td class="tbl-evencontent" colspan="5" align="center">' . $this->language->_('No Data') . '</td>
				</tr>';
			}


			$htmldataHistoryAproved =
				'<table border="1" cellspacing="0" cellpadding="0" class="table table-bordered table-condensed flip-content">
			<tr class="headercolor">
				<th valign="top">' . $this->language->_('User') . '</th>
				<th valign="top">' . $this->language->_('Approve Date') . '</th>

			</tr>'
				. $htmldataHistoryDetailAprove . '
		</table>';

			if ($paystatus == 'Pending Future Date') {

				$htmldataForm =
					'
		<form method="post">
			<input class="inputbtn" value="' . $this->language->_('Cancel Future Date') . '" name="cfd"  type="submit">
		</form>
		';
				$formButton = 'Pending Future Date';
			} else {
			}
		}

		if (isset($htmldataDetail)) $this->view->templateDetail = $htmldataDetail;
		if (isset($htmldataTransaction)) $this->view->templateTransaction = $htmldataTransaction;
		if (isset($htmldataHistory)) $this->view->templateHistory = $htmldataHistory;
		if (isset($htmldataHistoryAproved)) $this->view->templateHistoryAproved = $htmldataHistoryAproved;

		if (isset($htmldataForm)) $this->view->templateForm = $htmldataForm;


		if ($cfd) {
			require_once 'CMD/SinglePayment.php';
			require_once 'CMD/Validate/ValidatePaymentSingle.php';
			require_once 'General/CustomerUser.php';

			$singlePayment  = new SinglePayment($paymentref, $this->_userIdLogin);
			$singlePayment->cancelFutureDate("");
			//echo'Cancel Future Date';
			$this->_redirect('/notification/success/index');
		}


		if ($pdf) {
			Application_Helper_General::writeLog('RPPY', 'Download PDF Payment Report Detail (' . $psnumber . ')');

			if ($cektype != 16 && $cektype != 17) {
				$datapdf 	=
					"<h2>" . $this->language->_('View Payment') . "</h2><br/><br/>
						<h2>" . $this->language->_('Transfer From') . "</h2><hr></hr>
						" . $htmldataDetail . "
						<br />
						<br />
						<h2>" . $this->language->_('Transaction') . "</h2>
						" . $htmldataTransaction . "<br />
						<br />
						<h2>" . $this->language->_('Approved By') . "</h2>
						" . $htmldataHistoryAproved . "
						<br />
						<br />
						<h2>" . $this->language->_('History') . "</h2>
						" . $htmldataHistory;
			} else {
				$datapdf 	=
					"<h2>" . $this->language->_('View Payment') . "</h2><br/><br/>
						<h2>" . $this->language->_('Transfer From') . "</h2><hr></hr>
						" . $htmldataDetail . "<br />
						<br />
						<h2>" . $this->language->_('Approved By') . "</h2>
						" . $htmldataHistoryAproved . "
						<br />
						<br />
						<h2>" . $this->language->_('History') . "</h2>
						" . $htmldataHistory;
			}
			$datapdf = "<tr><td>" . $datapdf . "</td></tr>";
			$this->_helper->download->pdf(null, null, null, $this->language->_('Payment Report Detail'), $datapdf);
		} else {
			Application_Helper_General::writeLog('RPPY', 'View Payment Report Detail (' . $psnumber . ')');
		}
	}

	private function getBenefData($benefId){

		$select = $this->_db->select()
			->from('M_BENEFICIARY', array('*'))
			->where("BENEFICIARY_ID = ?", $benefId); 

		$benefData = $this->_db->fetchRow($select);
		//echo '<pre>';
		//var_dump($benefData);die;
		$citizenshipArr	= array("W" => "WNI", "N" => "WNA");
		$residentArr = array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);

		$settings 			= new Application_Settings();
		$LLD_array 			= array();
		$LLD_DESC_arrayCat 	= array();
		$lldTypeArr  		= $settings->getLLDDOMType();

		$LLD_CATEGORY_POST = '';
		if (!empty($benefData['BENEFICIARY_CATEGORY'])) {
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$LLD_array["CT"] 	= $benefData['BENEFICIARY_CATEGORY'];
			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$benefData['BENEFICIARY_CATEGORY']];
		}

		$tableMst[0]["label"] = $this->language->_('Beneficiary Bank');
		$tableMst[1]["label"] = $this->language->_('City');
		$tableMst[2]["label"] = $this->language->_('Beneficiary Account');
		$tableMst[3]["label"] = $this->language->_('Beneficiary Name');
		$tableMst[4]["label"] = $this->language->_('Beneficiary Alias Name');
		$tableMst[5]["label"] = $this->language->_('Beneficiary Address 1');
		$tableMst[6]["label"] = $this->language->_('Beneficiary Address 2');
		$tableMst[7]["label"] = $this->language->_('Beneficiary Email');
		$tableMst[8]["label"] = $this->language->_('Citizenship');
		$tableMst[9]["label"] = $this->language->_('Nationality');
		$tableMst[10]["label"] = $this->language->_('Beneficiary Category');
		$tableMst[11]["label"] = $this->language->_('Beneficiary ID Type');
		$tableMst[12]["label"] = $this->language->_('Beneficiary ID Number');

		if (!empty($benefData['BENEF_ACCT_BANK_CODE'])) {
			$benefBankName = Application_Helper_General::getBankTableName($benefData['BENEF_ACCT_BANK_CODE']);
	   	}else if(!empty($benefData['BANK_NAME'])){
			$benefBankName = $benefData['BANK_NAME'];
		}
		else{
				$benefBankName = $this->_bankName;
		}

		$tableMst[0]["value"] = $benefBankName;
		$tableMst[1]["value"] = empty($benefData['BANK_CITY']) ? '-' : $benefData['BANK_CITY'];
		$tableMst[2]["value"] = $benefData['BENEFICIARY_ACCOUNT'];
		$tableMst[3]["value"] = $benefData['BENEFICIARY_NAME'];
		$tableMst[4]["value"] = empty($benefData['BENEFICIARY_ALIAS']) ? '-' : $benefData['BENEFICIARY_ALIAS'];
		$tableMst[5]["value"] = empty($benefData['BENEFICIARY_ADDRESS']) ? '-' : $benefData['BENEFICIARY_ADDRESS'];
		$tableMst[6]["value"] = empty($benefData['BENEFICIARY_ADDRESS2']) ? '-' : $benefData['BENEFICIARY_ADDRESS2'];
		$tableMst[7]["value"] = empty($benefData['BENEFICIARY_EMAIL']) ? '-' : $benefData['BENEFICIARY_EMAIL'];
		$tableMst[8]["value"] = empty($benefData['BENEFICIARY_RESIDENT']) ? '-' : $residentArr[$benefData['BENEFICIARY_RESIDENT']];
		$tableMst[9]["value"] = empty($benefData['BENEFICIARY_CITIZENSHIP']) ? '-' : $citizenshipArr[$benefData['BENEFICIARY_CITIZENSHIP']];
		$tableMst[10]["value"] = empty($LLD_CATEGORY_POST) ? '-' : $LLD_CATEGORY_POST;
		$tableMst[11]["value"] = empty($benefData['BENEFICIARY_ID_TYPE']) ? '-' : $benefData['BENEFICIARY_ID_TYPE'];
		$tableMst[12]["value"] = empty($benefData['BENEFICIARY_ID_NUMBER']) ? '-' : $benefData['BENEFICIARY_ID_NUMBER'];

		$this->view->tableBene 		= $tableMst;
	}

	public function templateaddAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$cross = $this->_getParam('cross');
		$transpose = $this->_getParam('transpose');
		$exrate = $this->_getParam('exrate');
		$refnum = $this->_getParam('refnum');
		$custnum = $this->_getParam('custnum');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "1",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_CROSS' 			=> $cross,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_EXCHANGE' 		=> $exrate,
			'TEMP_REFNUM' 			=> $refnum,
			'TEMP_CUSTREF' 			=> $custnum,
			'TEMP_LOCKED'			=> $locked,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function templateadddomesticAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$tratype = $this->_getParam('tratype');
		$bene = $this->_getParam('bene');
		$beneaddress = $this->_getParam('beneaddress');
		$citizenship = $this->_getParam('citizenship');
		$nationality = $this->_getParam('nationality');
		$benecategory = $this->_getParam('benecategory');
		$beneidtype = $this->_getParam('beneidtype');
		$beneidnum = $this->_getParam('beneidnum');
		$bank = $this->_getParam('bank');
		$city = $this->_getParam('city');
		$email_domestic = $this->_getParam('email_domestic');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');


		try {
			$this->_db->beginTransaction();
			$insertArr = array(
				'T_NAME' 			=> $template,
				'T_CREATEDBY' 		=> $this->_userIdLogin,
				'T_CREATED' 		=> new Zend_Db_Expr("now()"),
				'T_GROUP'			=> $this->_custIdLogin,
				'T_TARGET'			=> $target,
				'T_TYPE'			=> '2',
			);

			$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);



			$lastId = $this->_db->lastInsertId();

			if (!empty($bene)) {
				$select = $this->_db->select()
					->from(array('A' => 'M_BENEFICIARY'), array('*'));
				$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
				$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
				$benedata = $this->_db->fetchall($select);
				$benename = $benedata['0']['BENEFICIARY_NAME'];
			} else {
				$benedata = array();
				$benename = '';
			}

			// print_r($insertArr);die;

			$insertData = array(
				'TEMP_ID' 				=> $lastId,
				'TEMP_SUBJECT' 			=> $subject,
				'TEMP_SOURCE' 			=> $source,
				'TEMP_TRATYPE' 			=> $tratype,
				'TEMP_BENE' 			=> $bene,
				'TEMP_BENE_NAME' 		=> $benename,
				'TEMP_BENEADDRESS' 		=> $beneaddress,
				'TEMP_CITIZENSHIP' 		=> $citizenship,
				'TEMP_NATIONALITY' 		=> $nationality,
				'TEMP_BENCATEGORY' 		=> $benecategory,
				'TEMP_BENEIDTYPE' 		=> $beneidtype,
				'TEMP_BENEIDNUM' 		=> $beneidnum,
				'TEMP_BANK' 			=> $bank,
				'TEMP_CITY' 			=> $city,
				'TEMP_EMAIL_DOMESTIC' 	=> $email_domestic,

				'TEMP_AMOUNT' 			=> $amount,
				'TEMP_NOTIF' 			=> $notif,
				'TEMP_EMAIL' 			=> $email,
				'TEMP_SMS' 				=> $sms,
				'TEMP_LOCKED'			=> $locked,
			);

			$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollback();
			Zend_Debug::Dump($e->getMessages());
		}

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function templateaddremittanceAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();


		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$benename2 = $this->_getParam('benename');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$chargetype = $this->_getParam('chargetype');
		$transpose = $this->_getParam('transpose');
		$emaildomestic = $this->_getParam('emaildomestic');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');
		$messagetext = $this->_getParam('messagetext');
		$identity = $this->_getParam('identity');
		$transactor = $this->_getParam('transactor');
		$ccy = $this->_getParam('ccy');
		$bankname = $this->_getParam('bankname');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "3",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_CHARGE_TYPE' 		=> $chargetype,
			'TEMP_MESSAGE' 		=> $messagetext,
			'TEMP_EMAIL_DOMESTIC' 	=> $emaildomestic,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_IDENTY' 			=> $identity,
			'TEMP_RELATIONSHIP'		=> $transactor,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_LOCKED'			=> $locked,
			'TEMP_BANK'				=> $bankname,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}
}
