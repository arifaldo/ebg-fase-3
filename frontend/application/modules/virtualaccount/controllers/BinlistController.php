<?php

require_once 'Zend/Controller/Action.php';

class virtualaccount_BinlistController extends Application_Main
{


  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');
    $fields = array(
      'bin' => array(
        'field'    => 'CUST_NAME',
        'label'    => $this->language->_('BIN'),
        'sortable' => true
      ),

      'bin_type'     => array(
        'field'    => 'BIN_TYPE',
        'label'    => $this->language->_('Type'),
        'sortable' => true
      ),

      'bin_name'   => array(
        'field'    => 'BIN_NAME',
        'label'    => $this->language->_('BIN Name'),
        'sortable' => true
      ),

      'status'     => array(
        'field'    => 'STATUS',
        'label'    => $this->language->_('Status'),
        'sortable' => true
      ),
      'master_account'     => array(
        'field'    => 'MASTER_ACCOUNT',
        'label'    => $this->language->_('Master Account'),
        'sortable' => true
      ),
      'ccy'     => array(
        'field'    => 'CCY',
        'label'    => $this->language->_('CCY'),
        'sortable' => true
      ),
      'bin_updated'     => array(
        'field'    => 'BIN_UPDATED',
        'label'    => $this->language->_('Last Updated'),
        'sortable' => true
      )
    );

    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('transaction_time' => array('asc', 'desc')))) ? $sortDir : 'asc';

    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;
    $this->view->fields = $fields;
  }
}
