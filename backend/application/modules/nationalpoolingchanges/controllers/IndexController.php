<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class nationalpoolingchanges_IndexController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $select = $this->_db->select()
					        ->from(array('A' => 'TEMP_NOTIONAL_POOLING'),array('N_NUMBER','N_CUST_ID','N_STATUS','N_CREATED','N_CREATEDBY'))
							->join(array('B' => 'M_CUSTOMER'), 'B.CUST_ID = A.N_CUST_ID',array('B.CUST_NAME'))
							->join(array('C' => 'T_GLOBAL_CHANGES'), 'C.CHANGES_ID = A.CHANGES_ID',array('C.CHANGES_INFORMATION'))
                            ->order('N_CREATED DESC');
							
							
		$this->paging($select);

    }

}