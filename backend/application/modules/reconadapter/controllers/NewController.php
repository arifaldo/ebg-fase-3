<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';


class reconadapter_NewController extends Application_Main {

    protected $_destinationUploadDir = '';
    protected $_maxRow = '';

    public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/reconProfile/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

	public function indexAction()
    { 
    	$this->_helper->_layout->setLayout('newlayout');

        if($this->_request->isPost())
        {
            $param = $this->_request->getParams();

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $adapter->setDestination ( $this->_destinationUploadDir );
            $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
            $extensionValidator->setMessage(
                $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
            );

            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
            $sizeValidator->setMessage(
                'Error: File exceeds maximum size'
            );

            $adapter->setValidators ( array (
                $extensionValidator,
                $sizeValidator,
            ));

            $files = $adapter->getFileName();

            foreach ($files as $key => $value) {

                $sourceFileName = substr_replace(basename($value),'',100);

                $extension = explode('.', $sourceFileName);

                $extensionName = $extension[1];

                $newFileName = $value . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                $adapter->addFilter ( 'Rename',$newFileName  );

                if ($adapter->isValid ($key)){

                    if ($adapter->receive($key)) {

                        //PARSING CSV HERE
                        $data[] = $this->_helper->parser->parseCSV($newFileName);  

                        //to save fileInfo (filepath, format)
                        $fileInfo[] = array(
                            'filePath'  => $newFileName,
                            'fileName'  => $sourceFileName,
                            'format'    => $extensionName
                        );    
                    }

                }
                else
                {   
                    $this->view->error = true;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                        if($key=='fileUploadErrorNoFile')
                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                        else
                            $error_msg[0] = $val;
                        break;
                    }
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;
                }   
            }

            if (count($files) < 2) {
                $this->view->error = true;
                $error_msg[] = $this->language->_('Error').': '.$this->language->_('Please upload both file').'.';
                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;
            }

            //if no error occured
            if (empty($errors)) {

                $statementStartRow = !empty($param['statementStartRow']) ? $param['statementStartRow'] : 0;
                $reportStartRow = !empty($param['reportStartRow']) ? $param['reportStartRow'] : 0;

                $statementHeaderData = $data[0][$statementStartRow-1];
                $reportHeaderData = $data[1][$reportStartRow-1];

                $statementData = $data[0];
                $reportData = $data[1];

                //--------------unset data from above start row---------------------
                if (!empty($statementStartRow)) {
                    for($i=0; $i<$statementStartRow;$i++){
                        unset($statementData[$i]);
                    }
                }

                if (!empty($reportStartRow)) {
                    for($i=0; $i<$reportStartRow;$i++){
                        unset($reportData[$i]);
                    }
                }

                //---------get rid of empty spaces between column on statement data---------
                $statementEmptySpaces = array();
                foreach ($statementHeaderData as $key => $value) {
                    if (empty($value)) {
                        array_push($statementEmptySpaces, $key);
                        unset($statementHeaderData[$key]);
                    }
                }
                foreach ($statementData as $key => $value) {
                    foreach ($statementEmptySpaces as $spaces) {
                        unset($statementData[$key][$spaces]);
                    }
                }

                //---------get rid of empty spaces between column on report data---------
                $reportEmptySpaces = array();
                foreach ($reportHeaderData as $key => $value) {
                    if (empty($value)) {
                        array_push($reportEmptySpaces, $key);
                        unset($reportHeaderData[$key]);
                    }
                }
                foreach ($reportData as $key => $value) {
                    foreach ($reportEmptySpaces as $spaces) {
                        unset($reportData[$key][$spaces]);
                    }
                }
            

                $sessionNamespace = new Zend_Session_Namespace('reconAdapter');
                $sessionNamespace->statementHeaderData = $statementHeaderData;
                $sessionNamespace->reportHeaderData = $reportHeaderData;

                $sessionNamespace->statementData = $statementData;
                $sessionNamespace->reportData = $reportData;

                $sessionNamespace->statementFilePath = $fileInfo[0]['filePath'];
                $sessionNamespace->reportFilePath = $fileInfo[1]['filePath'];
                $sessionNamespace->statementFileName = $fileInfo[0]['fileName'];
                $sessionNamespace->reportFileName = $fileInfo[1]['fileName'];
                $sessionNamespace->statementFileFormat = $fileInfo[0]['format'];
                $sessionNamespace->reportFileFormat = $fileInfo[1]['format'];

                $sessionNamespace->statementStartRow = $statementStartRow;
                $sessionNamespace->reportStartRow = $reportStartRow;

                $this->_redirect('/reconadapter/new/next');
            }
        }
    }  

    public function nextAction(){

        $this->_helper->_layout->setLayout('newlayout');

        $sessionNamespace = new Zend_Session_Namespace('reconAdapter');
        $this->view->statementHeaderData = $sessionNamespace->statementHeaderData;
        $this->view->reportHeaderData = $sessionNamespace->reportHeaderData;

        $this->view->statementData = $sessionNamespace->statementData;
        $this->view->reportData = $sessionNamespace->reportData;

        $this->view->statementFileName = $sessionNamespace->statementFileName;
        $this->view->reportFileName = $sessionNamespace->reportFileName;

        $this->view->statementHeaderCount = count($statementHeaderData);
        $statementHalfHeaderCount = (int) (count($statementHeaderData) / 2);
        $this->view->statementHalfHeaderCount = $statementHalfHeaderCount;

        $this->view->reportHeaderCount = count($reportHeaderData);
        $reportHalfHeaderCount = (int) (count($reportHeaderData) / 2);
        $this->view->reportHalfHeaderCount = $reportHalfHeaderCount;

        // $select = $this->_db->select()
        //         ->from(array('B' => 'M_BANK_TABLE'),array('*'));

        // $bankData = $this->_db->fetchAll($select);

        // $this->view->bankData = $bankData;

        if($this->_request->isPost())
        {
            $params = $this->_request->getParams();

            $filter = array(
               'profileName' => array('StripTags','StringTrim'),
               // 'bank' => array('StripTags','StringTrim')
            );
            
            $options = array('allowEmpty' => FALSE);
            
            $validators = array(
                'profileName'   => array(
                                    array('Db_NoRecordExists', array('table' => 'M_RECON_ADAPTER', 'field' => 'ADAPTER_NAME')),
                                    'messages' => array(
                                                    $this->language->_('Recon adapter profile with this name is Already Exist')
                                                )
                                ),
                // 'bank'          => array(
                //                     array('Db_NoRecordExists', array('table' => 'M_RECON_ADAPTER', 'field' => 'BANK_CODE')),
                //                     'messages' => array(
                //                                     $this->language->_('Recon adapter profile with this bank is Already Exist')
                //                                 )
                //                 ),
                );

            $filter  = new Zend_Filter_Input($filter, $validators, $params, $options);

            $label = $params['label'];
            $statementContent = $params['statementContent'];
            $reportContent = $params['reportContent'];
            $operator = $params['operator'];

            $checkEmpty = false;
            if (!$this->isArrayEmpty($label) || !$this->isArrayEmpty($statementContent) || !$this->isArrayEmpty($reportContent) || !$this->isArrayEmpty($operator)) {
                $checkEmpty = true;
            }

            if ($filter->isValid() && !$checkEmpty){

                $this->_db->beginTransaction();
                try{
                    $tableName = 'M_RECON_ADAPTER';
                    $arrData = array(
                        'ADAPTER_NAME' => $filter->getEscaped('profileName'),
                        // 'BANK_CODE' => $filter->getEscaped('bank'),
                        'STATEMENT_FILE_FORMAT' => $sessionNamespace->statementFileFormat,
                        'REPORT_FILE_FORMAT' => $sessionNamespace->reportFileFormat,
                        'STATEMENT_START_FROM_ROW' => $sessionNamespace->statementStartRow,
                        'REPORT_START_FROM_ROW' => $sessionNamespace->reportStartRow,
                        'STATEMENT_FILE_PATH' => $sessionNamespace->statementFilePath,
                        'REPORT_FILE_PATH' => $sessionNamespace->reportFilePath,
                        'SUGGESTED_DATE' => date('Y-m-d H:i:s'),
                        'SUGGESTED_BY' => $this->_userIdLogin
                    );

                    $this->_db->insert($tableName, $arrData);

                    $lastId = $this->_db->fetchOne('select @@identity');
                    
                    $this->_db->commit();

                }catch(Exception $e){
                    $success = false;
                    $this->_db->rollBack();
                    print_r($e);die();
                    $error[] = $e->getMessage();
                }

                $label = $params['label'];
                $statementContent = $params['statementContent'];
                $reportContent = $params['reportContent'];
                $operator = $params['operator'];

                $i = 0;
                foreach ($label as $key => $value) {

                    $data2 = array(
                        'ADAPTER_ID' => $lastId,
                        'HEADER_NAME' => $label[$key],
                        'STATEMENT_COLUMN' => $statementContent[$key],
                        'REPORT_COLUMN' => $reportContent[$key],
                        'OPERATOR' => $operator[$key]
                    );

                    $this->_db->beginTransaction();

                    try{
                        $tableName = 'M_RECON_ADAPTER_DETAIL';
                        $this->_db->insert($tableName, $data2);
                        
                        $this->_db->commit();

                        $success = true;

                    }catch(Exception $e){
                        $success = false;
                        $this->_db->rollBack();
                        print_r($e);die();
                        $error[] = $e->getMessage();
                    }
                    $i++;
                }

                if ($success) {
                    Application_Helper_General::writeLog('RPAD','Add Reconciliation Adapter List. Profile Name: '.$arrData['ADAPTER_NAME']);
                    Zend_Session::namespaceUnset('reconAdapter');
                    $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                    $this->_redirect('/notification/success/index');
                }
                else{
                    $this->_db->rollBack();
                    $this->view->error = true;
                    $error_msg = $error;
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;
                }   

            }else{
                $error = $filter->getMessages();
                $this->view->error = true;
                $error_msg = $error;

                if($checkEmpty){
                    $error_msg[] = 'Please fill the empty field';
                }

                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;

                $this->view->profileName = $params['profileName'];
                // $this->view->bank = $params['bank'];
                $this->view->label = $params['label'];
                $this->view->statementContent = $params['statementContent'];
                $this->view->reportContent = $params['reportContent'];
                $this->view->operator = $params['operator'];
            }
        }
    }

    public function isArrayEmpty($array) {

        foreach($array as $key => $val) {
            if ($val == '')
                return false;
        }
        return true;
    }
}
