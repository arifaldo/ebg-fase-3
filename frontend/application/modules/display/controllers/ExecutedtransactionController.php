<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
class display_ExecutedtransactionController extends Application_Main
{
	
	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];


		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
        	$selectBank	= $this->_db->select()
			->from(
				array('B' => 'M_BANK_TABLE'),
				array(
					'BANK_CODE'			=> 'B.BANK_CODE',
					'BANK_NAME'			=> 'B.BANK_NAME'
				)
			);

			$bankList = $this->_db->fetchAll($selectBank);

			foreach ($bankList as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}

			$cache->save($bankNameArr,$cacheID);
        }


		

		$this->bankNameList = $bankNameArr;
	}

	public function indexAction()
	{
		$filter_clear 		= $this->_getParam('clearfilter');
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->bankNameList = $this->bankNameList;
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$range_reporthistory = $setting->getSettingVal('range_reporthistory');
		$this->view->range_reporthistory = $range_reporthistory;
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;

		$transstatus = $this->_transferstatus;
		$transstatuscode = array_flip($transstatus['code']);
		$statusarr = array_combine(array_values($this->_transferstatus['code']), array_values($this->_transferstatus['desc']));

		$paymenttype = $this->_paymenttype;

		$arrPayType  = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		//echo '<pre>';
		//var_dump($arrPayType);die;
		$this->view->arrPayType = $arrPayType;

		$typearr = array_combine(array_values($this->_paymenttype['code']), array_values($this->_paymenttype['desc']));
		// print_r($typearr);die;
		$tratypearr = array_combine(array_values($this->_transfertype['code']), array_values($this->_transfertype['desc']));

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
		$cacheID = 'CUSTOMER';
		
		$conf = Zend_Registry::get('config');
		$this->view->bankName = $conf['app']['bankname'];
        
        $select = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($select)){
        	$select = $this->_db->select()->distinct()
			->from(
				array('A' => 'M_CUSTOMER'),
				array('CUST_ID')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();

			$cache->save($select,$cacheID);
        }

		
		//Zend_Debug::dump($arrPayType); die;

		$this->view->var = $select;

		$fields = array(
			'P.PS_NUMBER'      	=> array(
				'field' => 'B.PS_NUMBER',
				'label' => $this->language->_('Payment Ref').' - '.$this->language->_('Transaction ID').' - '.$this->language->_('Subject'),
				'sortable' => true
			),
			'PS_SUBJECT'  	=> array(
				'field' => 'PS_SUBJECT',
				'label' => $this->language->_('Subject'),
				'sortable' => true
			),
			'SOURCE_ACCOUNT'  	=> array(
				'field' => 'SOURCE_ACCOUNT',
				'label' => $this->language->_('Source Account'),
				'sortable' => true
			),
			/*'SOURCE_ACCOUNT_NAME'  	=> array('field' => 'SOURCE_ACCOUNT_NAME',
													'label' => $this->language->_('Source Account Name'),
													'sortable' => true),*/
			'BENEFICIARY_ACCOUNT'  	=> array(
				'field' => 'BENEFICIARY_ACCOUNT',
				'label' => $this->language->_('Beneficiary Account'),
				'sortable' => true
			),
			/*'BENEFICIARY_ACCOUNT_NAME'  	=> array('field' => 'BENEFICIARY_ACCOUNT_NAME',
													'label' => $this->language->_('Beneficiary Name'),
													'sortable' => true),*/
			// 'PS_TXCOUNT'      	=> array(
			// 	'field' => 'PS_TXCOUNT',
			// 	'label' => $this->language->_('#Trans <br>'),
			// 	'sortable' => true
			// ),
			'PS_CCY'           => array(
				'field' => 'PS_CCY',
				'label' => $this->language->_('Amount'),
				'sortable' => true
			),




			//						'BENEFICIARY_ACCOUNT_CCY'  	=> array('field' => 'BENEFICIARY_ACCOUNT_CCY',
			//											      'label' => 'CCY / Amount',
			//											      'sortable' => true),
			/*'AMOUNT'  	=> array('field' => 'AMOUNT_EQ',
											      'label' => $this->language->_('Amount'),
											      'sortable' => true),*/

  			'PS_CREATED'      	=> array(
				'field' => 'PS_CREATED',
				'label' => $this->language->_('Created Date'),
				'sortable' => true
			),
			'PS_EFDATERPT'  	=> array(
				'field' => 'PS_EFDATERPT',
				'label' => $this->language->_('Payment Date'),
				'sortable' => true
			),

			'PS_UPDATED'  	=> array(
				'field' => 'PS_UPDATED',
				'label' => $this->language->_('Updated Date'),
				'sortable' => true
			),
			'PS_TYPE'     => array(
				'field' => 'PS_TYPE',
				'label' => $this->language->_('Payment - Trf Type'),
				'sortable' => true
			),
			'TRA_STATUS'  	=> array(
				'field' => 'TRA_STATUS',
				'label' => $this->language->_('Transaction Status'),
				'sortable' => true
			)
		);

		$page    = $this->_getParam('page');
		$filterlist = array('PS_CREATED', 'PS_NUMBER', 'PS_SUBJECT', 'SOURCE_ACCOUNT', 'BENEFICIARY_ACCOUNT', 'PS_EFDATERPT', 'PS_UPDATED', 'PS_TYPE', 'TRA_STATUS', 'TRANSFER_TYPE');

		$this->view->filterlist = $filterlist;
		// print_r($this->view->filterlist);die;

		// $sortBy  = $this->_getParam('sortby','B.PS_UPDATED');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('PS_UPDATED');
		$sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		// $sortDir = $this->_getParam('sortdir','desc');
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		// echo '<pre>';
		// var_dump($typearr);die;
		$caseType = "(CASE P.PS_TYPE ";
		foreach ($typearr as $key => $val) {
			if ($key == 18) {
				$val = 'Disbursement';
			}
			$caseType .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseType .= " END)";

		$caseStatus = "(CASE T.TRA_STATUS ";
		foreach ($statusarr as $key => $val) {
			$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseStatus .= " END)";

		$caseTraType = "(CASE T.TRANSFER_TYPE ";
		foreach ($tratypearr as $key => $val) {
			$caseTraType .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseTraType .= " END)";
		
		$config    		= Zend_Registry::get('config');
		$paymentStatus 	= $config["payment"]["status"];
		$paymentType 	= $config["payment"]["type"];
		$transferType 	= $config["transfer"]["type"];
		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		$this->view->trfType = $trfType;
		// case when string
		$caseArr 		 = Application_Helper_General::casePaymentType($paymentType, $transferType);
		$casePaymentType = $caseArr["PS_TYPE"];
		//Zend_Debug::dump($caseStatus); die;
		$excPstype = array(27,28);
		$select2 = $this->_db->select()
			->from(array('P' => 'T_PSLIP'), array())
			#->joinleft(array('C' => new Zend_Db_Expr("(SELECT * FROM T_TRANSACTION WHERE REEXECUTE_INDEX = (SELECT MAX(REEXECUTE_INDEX) FROM T_TRANSACTION AS TRX WHERE TRX.PS_NUMBER = T_TRANSACTION.PS_NUMBER))")), 'B.PS_NUMBER = T.PS_NUMBER', array(
			#->joinleft(array('C' => new Zend_Db_Expr("(SELECT * FROM T_TRANSACTION WHERE REEXECUTE_INDEX = (SELECT MAX(REEXECUTE_INDEX) FROM T_TRANSACTION AS TRX WHERE TRX.PS_NUMBER = T_TRANSACTION.PS_NUMBER))")), 'B.PS_NUMBER = T.PS_NUMBER', array(
			->joinleft(array('T' => 'T_TRANSACTION'), 'T.PS_NUMBER = P.PS_NUMBER',array(
				'P.PS_NUMBER',
				'P.PS_CREATED',
				'P.PS_SUBJECT',
				'T.SOURCE_ACCOUNT',
				'T.SOURCE_ACCOUNT_CCY',
				'T.SOURCE_ACCOUNT_NAME',
				'T.SOURCE_ACCOUNT_ALIAS_NAME',
				'T.BENEFICIARY_ACCOUNT',
				'T.BENEFICIARY_ACCOUNT_NAME',
				'T.BENEFICIARY_ALIAS_NAME',
				'P.PS_TXCOUNT',
				'P.PS_CCY',
				'P.PS_STATUS',
				'T.TRA_AMOUNT',
				'P.PS_REMAIN',
				'T.EQUIVALENT_AMOUNT_IDR',
				'T.BENEFICIARY_ACCOUNT_CCY',
				'T.RATE_BUY', 'T.FULL_AMOUNT_FEE', 'T.PROVISION_FEE', 'T.RATE', 'T.TRANSFER_FEE',
				'P.PS_UPDATED',
				'P.PS_EFDATE',
				'T.TRANSACTION_ID',
				'T.TRANSFER_TYPE',
				'TRA_TYPE' => 'T.TRANSFER_TYPE',
				'T.TRA_MESSAGE',
				'payType'		=> new Zend_Db_Expr("CASE $casePaymentType ELSE 'N/A' END"),

				'TRA_STATUS' => new Zend_Db_Expr("(CASE T.TRA_STATUS 
		    	  								   	WHEN '3' THEN 'Success' 
		    	  									WHEN '4' THEN 'Failed' 
		    	  									END)"),
				'PSTYPE' => 'P.PS_TYPE',
				'SOURCE_ACCOUNT_BANK'	=> 'F.BANK_NAME',
				'BENEFICIARY_ACCOUNT_BANK'	=> 'E.BANK_NAME',
				//'BENEFICIARY_ACCOUNT_BANK_SWIFT'	=> 'G.BANK_NAME',
				'BENEFICIARY_BANK_NAME',
				'PS_TYPE' => $caseType,
				//'TRANSFER_TYPE' => $caseTraType,
				'P.PS_TOTAL_AMOUNT',
				'BALANCE_TYPE' => new Zend_Db_Expr("(SELECT BALANCE_TYPE FROM T_PERIODIC_DETAIL WHERE PS_PERIODIC = P.PS_PERIODIC limit 1)"),
				'T.REEXECUTE_INDEX',
				'T.SWIFT_CODE',
				'T.CLR_CODE'
			))
			->joinLeft(array('F' => 'M_BANK_TABLE' ), 'T.SOURCE_ACCT_BANK_CODE = F.BANK_CODE',array())
			->joinLeft(array('E' => 'M_BANK_TABLE' ), 'T.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
			//->joinLeft(array('G' => 'M_BANK_TABLE' ), 'T.SWIFT_CODE = G.SWIFT_CODE AND T.CLR_CODE = G.CLR_CODE',array())
			->order('P.PS_UPDATED DESC');
		$select2->where("P.PS_TYPE NOT IN (?)",$excPstype); 
		$select2->where("P.PS_STATUS in ('5','6')");
		$select2->where("P.CUST_ID LIKE " . $this->_db->quote($this->_custIdLogin));
		$select2->group("T.TRANSACTION_ID");
		$select2->order("T.REEXECUTE_INDEX DESC");

		$filterArr = array(
			'PS_UPDATED' 	=> array('StringTrim', 'StripTags'),
			'PS_EFDATERPT' 	=> array('StringTrim', 'StripTags'),
			'PS_CREATED' 	=> array('StringTrim', 'StripTags'),
			'PS_SUBJECT' 	=> array('StringTrim', 'StripTags'),
			'SOURCE_ACCOUNT' 	=> array('StringTrim', 'StripTags'),
			'BENEFICIARY_ACCOUNT' 	=> array('StringTrim', 'StripTags'),

			'PS_UPDATED_END' 	=> array('StringTrim', 'StripTags'),
			'PS_EFDATE_END' 	=> array('StringTrim', 'StripTags'),
			'PS_CREATED_END' 		=> array('StringTrim', 'StripTags'),

			'PS_NUMBER' 		=> array('StringTrim', 'StripTags', 'StringToUpper'),
			// 'TRANSACTION_ID'	=> array('StringTrim','StripTags'),
			// 'PS_CCY'	=> array('StringTrim','StripTags'),
			'PS_STATUS'	=> array('StringTrim', 'StripTags'),
			'PS_TYPE' 	=> array('StringTrim', 'StripTags'),
			'TRANSFER_TYPE' 	=> array('StringTrim', 'StripTags'),
		);

		$options = array('allowEmpty' => true);
		$validators = array(
			'PS_UPDATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_EFDATERPT' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_CREATED'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),

			'PS_UPDATED_END' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_EFDATE_END' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_CREATED_END'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),



			'PS_SUBJECT' 			=> array(),
			'SOURCE_ACCOUNT' 		=> array(),
			'BENEFICIARY_ACCOUNT' 		=> array(),
			// 'SOURCE_ACCOUNT_NAME'		=> array(),		// $filter!
			// 'BENEFICIARY_ACCOUNT'		=> array(),
			// 'BENEFICIARY_ACCOUNT_NAME'		=> array(),
			'PS_NUMBER'		=> array(),
			// 'TRANSACTION_ID'		=> array(),
			// 'PS_CCY'		=> array(),
			// 'PS_STATUS'		=> array(),
			'PS_STATUS' => array(array('InArray', array('haystack' => array_keys($optPayStatus)))),	// $filter!
			'PS_TYPE' 	=> array(array('InArray', array('haystack' => array_keys($optPayType)))),	// $filter!
			'TRANSFER_TYPE' => array(array('InArray', array('haystack' => array_keys($filterTrfType)))),	// $filter!

		);

		// $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

		$dataParam = array("PS_SUBJECT","SOURCE_ACCOUNT", 'BENEFICIARY_ACCOUNT', "PS_NUMBER", "PS_STATUS", "PS_TYPE", "TRANSFER_TYPE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		if (!empty($this->_request->getParam('createdate'))) {
			$createarr = $this->_request->getParam('createdate');
			$dataParamValue['PS_CREATED'] = $createarr[0];
			$dataParamValue['PS_CREATED_END'] = $createarr[1];
		}
		if (!empty($this->_request->getParam('updatedate'))) {
			$updatearr = $this->_request->getParam('updatedate');
			$dataParamValue['PS_UPDATED'] = $updatearr[0];
			$dataParamValue['PS_UPDATED_END'] = $updatearr[1];
		}

		if (!empty($this->_request->getParam('efdaterpt'))) {
			$efdatearr = $this->_request->getParam('efdaterpt');
			$dataParamValue['PS_EFDATERPT'] = $efdatearr[0];
			$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
		}


		$zf_filter 	= new Zend_Filter_Input($filterArr, $validator, $dataParamValue, $options);
		$filter 		= $this->_getParam('filter');


		//     $fUpdatedStart 	= $zf_filter->getEscaped('PS_UPDATED');
		// $fUpdatedEnd 	= $zf_filter->getEscaped('PS_UPDATED_END');
		$fPaymentStart 	= $zf_filter->getEscaped('PS_EFDATERPT');
		$fPaymentEnd 	= $zf_filter->getEscaped('PS_EFDATE_END');

		$datefrom 	= $zf_filter->getEscaped('PS_CREATED');
		$dateto 	= $zf_filter->getEscaped('PS_CREATED_END');
		// print_r($fCreatedEnd);die;


		$subject 		= $zf_filter->getEscaped('PS_SUBJECT');
		$AccArr 		= $zf_filter->getEscaped('SOURCE_ACCOUNT');
		$benefArr 		= $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');

		$payment 	= $zf_filter->getEscaped('PS_NUMBER');
		$status = $zf_filter->getEscaped('PS_STATUS');
		$paytype 	= $zf_filter->getEscaped('PS_TYPE');
		$transtype 	= $zf_filter->getEscaped('TRANSFER_TYPE');

		//    $AccArr 		= html_entity_decode($zf_filter->getEscaped('ACCTSRC'));
		// $payment 		= html_entity_decode($zf_filter->getEscaped('payment'));
		// $paytype 		= html_entity_decode($zf_filter->getEscaped('paytype'));
		// $beneficiary 	= html_entity_decode($zf_filter->getEscaped('beneficiary'));
		// $status 		= html_entity_decode($zf_filter->getEscaped('status'));
		// $transtype 	= html_entity_decode($zf_filter->getEscaped('transtype'));
		// $datefrom 		= html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		// $dateto 		= html_entity_decode($zf_filter->getEscaped('fDateTo'));

		// print_r($payment);die;
		if ($filter == null) {
			//$datefrom = (date("d/m/Y"));
			//$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}

		if ($filter_clear == '1') {
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';
			$datefrom = '';
			$dateto = '';
		}

		if ($filter == null || $filter == TRUE) {
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;
			if (!empty($datefrom)) {
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($dateto)) {
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto    = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($datefrom) && empty($dateto))
				$select2->where("DATE(P.PS_UPDATED) >= " . $this->_db->quote($datefrom));

			if (empty($datefrom) && !empty($dateto))
				$select2->where("DATE(P.PS_UPDATED) <= " . $this->_db->quote($dateto));

			if (!empty($datefrom) && !empty($dateto))
				$select2->where("DATE(P.PS_UPDATED) between " . $this->_db->quote($datefrom) . " and " . $this->_db->quote($dateto));




			if ($filter == TRUE) {
				// die;

				if (!empty($fPaymentStart)) {
					$FormatDate = new Zend_Date($fPaymentStart, $this->_dateDisplayFormat);
					$datepayment  = $FormatDate->toString($this->_dateDBFormat);
				}

				if (!empty($fPaymentEnd)) {
					$FormatDate = new Zend_Date($fPaymentEnd, $this->_dateDisplayFormat);
					$datepaymentend    = $FormatDate->toString($this->_dateDBFormat);
				}
				
				if (!empty($fPaymentStart) && empty($fPaymentEnd))
					$select2->where("DATE(P.PS_EFDATE) >= " . $this->_db->quote($datepayment));

				if (empty($fPaymentStart) && !empty($fPaymentEnd))
					$select2->where("DATE(P.PS_EFDATE) <= " . $this->_db->quote($datepaymentend));

				if (!empty($fPaymentStart) && !empty($fPaymentEnd))
					$select2->where("DATE(P.PS_EFDATE) between " . $this->_db->quote($datepayment) . " and " . $this->_db->quote($datepaymentend));

				if ($subject != null) {
					$subjectArr = explode(',', $subject);
					$select2->where("UPPER(P.PS_SUBJECT)  in (?)",$subjectArr );
					foreach($subjectArr as $v){
						$select2->where("P.PS_SUBJECT LIKE " . $this->_db->quote('%'.$v.'%'));
					}
				}

				if ($AccArr != null) {
					$this->view->ACCTSRC = $AccArr;
					$AccArrArr = explode(',', $AccArr);
					$select2->where("T.SOURCE_ACCOUNT  in (?)",$AccArrArr );	
					//$select2->where("T.SOURCE_ACCOUNT LIKE " . $this->_db->quote('%'.$AccArr.'%'));
				}

				if ($benefArr != null) {
					$benefArrArr = explode(',', $benefArr);
					$select2->where("T.BENEFICIARY_ACCOUNT  in (?)",$benefArrArr );	
					//$select2->where("T.BENEFICIARY_ACCOUNT LIKE " . $this->_db->quote('%'.$benefArr.'%'));
				}

				if ($payment != null) {
					$this->view->payment = $payment;
					$paymentArr = explode(',', $payment);
					$select2->where("P.PS_NUMBER  in (?)",$paymentArr );	
					//$select2->where("B.PS_NUMBER LIKE " . $this->_db->quote('%' . $payment . '%'));
				}

				if ($paytype != null) {
					$this->view->paytype = $paytype;
					$fpaymentarr = explode(',', $paytype);
					if($fpaymentarr == '33' || $fpaymentarr == '35'){
						$select2->where("P.PS_TYPE IN (?) AND DISPLAY_FLAG = '1'", $fpaymentarr);
					}else{
						$select2->where("P.PS_TYPE IN (?)", $fpaymentarr);	
					}
					
				}

				if ($transtype != null) {
					$this->view->transtype = $transtype;
					$transtypeArr = explode(',', $transtype);
					$select2->where("T.TRANSFER_TYPE  in (?)",$transtypeArr );	
					//$select2->where("T.TRANSFER_TYPE LIKE " . $this->_db->quote($transtype));
				}

				if ($beneficiary != null) {
					$this->view->beneficiary = $beneficiary;
					$beneficiaryArr = explode(',', $beneficiary);
					$select2->where("T.BENEFICIARY_ACCOUNT  in (?)",$beneficiaryArr );	
					//$select2->where("T.BENEFICIARY_ACCOUNT LIKE " . $this->_db->quote('%' . $beneficiary . '%'));
				}


				if ($status != null) {
					$this->view->status = $status;
					//$status = 
					$statusArr = explode(',', $status);
					$select2->where("TRA_STATUS  in (?)",$statusArr );	
					//$select2->where("TRA_STATUS LIKE " . $this->_db->quote($status));
				}
			}else{
				$select2->where("MONTH(P.PS_UPDATED) = MONTH(NOW())");
			}
		}

		//$select2->where('T.DISPLAY_FLAG = ? ','1');
		// $select2->order($sortBy . ' ' . $sortDir);
		//$select2->orWhere("P.PS_STATUS IN ('5','6') AND P.PS_TYPE IN (38) AND P.CUST_ID = ? ",$this->_custIdLogin);
		//echo '<pre>';
		 //echo $select2;
		 //die();
	 	$filterlistdata = array("Filter");
		foreach($dataParamValue as $fil => $val){
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
		}
		
		$data = $this->_db->fetchAll($select2);
		if(!empty($data)){
			foreach($data as $key => $val){
				if(!empty($val['SWIFT_CODE']) && !empty($val['CLR_CODE'])){
					$banknamedata = $this->_db->select()->distinct()
					->from(
						array('A' => 'M_BANK_TABLE'),
						array('BANK_NAME')
					)
					->where('CLR_CODE = ?',$val['CLR_CODE'])
					->where('SWIFT_CODE = ?',$val['SWIFT_CODE']);
					//->order('CUST_ID ASC')
					//->query()->fetchRow();
					$banknamedata = $this->_db->fetchRow($banknamedata);
					//echo $banknamedata;
					//var_dump($banknamedata);die;
					$data[$key]['BENEFICIARY_BANK_NAME'] = $banknamedata['BANK_NAME'];
				}
			}
		}

		if ($csv || $pdf || $this->_request->getParam('print')) {
			$arr = $this->_db->fetchAll($select2);
			 //echo '<pre>';
			 //var_dump($arr);
			 //die;
			foreach ($arr as $key => $value) {
				//echo $key;
				$arr[$key]["PS_CREATED"] = Application_Helper_General::convertDate($value["PS_CREATED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

				$arr[$key]["SOURCE_ACCOUNT_NAME"] = $value["SOURCE_ACCOUNT_NAME"];

				unset($arr[$key]["SOURCE_ACCOUNT_CCY"]);
				unset($arr[$key]["PS_REMAIN"]);
				unset($arr[$key]["RATE_BUY"]);
				unset($arr[$key]["FULL_AMOUNT_FEE"]);
				unset($arr[$key]["PROVISION_FEE"]);
				unset($arr[$key]["RATE"]);
				unset($arr[$key]["TRANSFER_FEE"]);
				$arr[$key]["BENEFICIARY_ACCOUNT_NAME"] = $value["BENEFICIARY_ACCOUNT_NAME"];
				$arr[$key]["BENEFICIARY_ACCOUNT_CCY"] = $value["BENEFICIARY_ACCOUNT_CCY"] . ' / ' . $value["TRA_AMOUNT"];
				$arr[$key]["PS_TYPE"] = $this->language->_($value["PS_TYPE"]) . ' (' . $value["TRANSFER_TYPE"] . ')';
				unset($arr[$key]["SOURCE_ACCOUNT_ALIAS_NAME"]);
				unset($arr[$key]["BENEFICIARY_ALIAS_NAME"]);
				$amount = Application_Helper_General::displayMoney($value['TRA_AMOUNT']);
				unset($arr[$key]["TRA_AMOUNT"]);
				unset($arr[$key]["TRA_MESSAGE"]);
				unset($arr[$key]["TRANSFER_TYPE"]);
				if(!empty($value['BENEFICIARY_BANK_NAME'])){
					$arr[$key]["BENEFICIARY_ACCOUNT_BANK"] = $value["BENEFICIARY_BANK_NAME"];
				}
				
				if(!empty($value['SWIFT_CODE']) && !empty($value['CLR_CODE'])){
					$banknamedata = $this->_db->select()->distinct()
					->from(
						array('A' => 'M_BANK_TABLE'),
						array('BANK_NAME')
					)
					->where('CLR_CODE = ?',$value['CLR_CODE'])
					->where('SWIFT_CODE = ?',$value['SWIFT_CODE']);
					//->order('CUST_ID ASC')
					//->query()->fetchRow();
					$banknamedata = $this->_db->fetchRow($banknamedata);
					//echo $banknamedata;
					//var_dump($banknamedata);die;
					$arr[$key]['BENEFICIARY_BANK_NAME'] = $banknamedata['BANK_NAME'];
				}
				unset($arr[$key]["SWIFT_CODE"]);
				unset($arr[$key]["CLR_CODE"]);
				unset($arr[$key]["BENEFICIARY_ACCOUNT_BANK"]);
				$ps_amount =  $value['PS_TOTAL_AMOUNT'];
				$balance =  $value['BALANCE_TYPE'];
				if ($balance == '2') {
					$amounteq = $ps_amount . '% (' . $value['PS_CCY'] . ' ' . $amount . ')';
				} else if ($value['EQUIVALENT_AMOUNT_IDR'] > 0) {
					//$amounteq = $value['PS_CCY'].' '.$amount.'( IDR :'. Application_Helper_General::displayMoney($value['EQUIVALENT_AMOUNT_IDR']).' )';
					if ($value["BENEFICIARY_ACCOUNT_CCY"] == 'USD' && $value['SOURCE_ACCOUNT_CCY'] == 'USD') {
						$amounteq = 'USD ' . Application_Helper_General::displayMoney($value['EQUIVALENT_AMOUNT_IDR']);
					} else {

						$amounteq = 'IDR ' . Application_Helper_General::displayMoney($value['EQUIVALENT_AMOUNT_IDR']);
					}
				} else {
					$amounteq = $value['PS_CCY'] . ' ' . $amount;
				}
				// 				$amounteq = $value['PS_CCY'].' '.$amount.'( IDR :'. Application_Helper_General::displayMoney($value['EQUIVALENT_AMOUNT_IDR']).' )';
				unset($arr[$key]["PS_TOTAL_AMOUNT"]);
				unset($arr[$key]["BALANCE_TYPE"]);
				unset($arr[$key]["EQUIVALENT_AMOUNT_IDR"]);
				unset($arr[$key]["TRANSACTION_ID"]);
				unset($arr[$key]["BENEFICIARY_ACCOUNT_CCY"]);
				$updated = Application_Helper_General::convertDate($value["PS_UPDATED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$efdate = Application_Helper_General::convertDate($value["PS_EFDATE"], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$arr[$key]["AMOUNT_EQ"] = $amounteq;
				unset($arr[$key]["PS_UPDATED"]);
				unset($arr[$key]["PS_EFDATE"]);
				$status = $this->language->_($value["TRA_STATUS"]);
				unset($arr[$key]["TRA_STATUS"]);
				$pstype = $this->language->_($value["PS_TYPE"]);
				unset($arr[$key]["PS_TYPE"]);
				$arr[$key]["PS_EFDATE"] = $efdate;
				$arr[$key]["PS_UPDATED"] = $updated;
				$arr[$key]["TRA_STATUS"] = $status;
				$arr[$key]["PS_TYPE"] = $pstype;
			}
			// 			echo "<pre>";
			// 			print_r($arr);die;
			$header = Application_Helper_Array::simpleArray($fields, 'label');
			//Zend_Debug::dump($arr);die;

			if ($csv) {
				$listable = array_merge_recursive(array($header), $arr);
				Application_Helper_General::writeLog('DTRX', 'Download CSV Transaction Report');
				//Zend_Debug::dump($arr);die;
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->csv($filterlistdata, $listable, null, 'Executed Transaction');
			}

			if ($pdf) {
				Application_Helper_General::writeLog('DTRX', 'Download PDF Transaction Report');
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->pdf($header, $arr, null, 'Executed Transaction');
			}
			if ($this->_request->getParam('print')) {
				$filterlistdatax = $this->_request->getParam('data_filter');
				$this->_forward('printtable', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
			}
		} else {
			Application_Helper_General::writeLog('DTRX', 'View Transaction Report');
			$this->view->filterlistdata = $filterlistdata;
		}

		if (!empty($dataParamValue)) {

			$this->view->createdStart = $dataParamValue['PS_CREATED'];
			$this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
			$this->view->updatedStart = $dataParamValue['PS_UPDATED'];
			$this->view->updatedEnd = $dataParamValue['PS_UPDATED_END'];
			$this->view->paymentStart = $dataParamValue['PS_EFDATERPT'];
			$this->view->paymentEnd = $dataParamValue['PS_EFDATE_END'];


			unset($dataParamValue['PS_CREATED_END']);
			unset($dataParamValue['PS_EFDATE_END']);
			unset($dataParamValue['PS_UPDATED_END']);

			foreach ($dataParamValue as $key => $value) {
						$duparr = explode(',',$value);
								if(!empty($duparr)){
									
									foreach($duparr as $ss => $vs){
										$wherecol[]	= $key;
										$whereval[] = $vs;
									}
								}else{
										$wherecol[]	= $key;
										$whereval[] = $value;
								}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
		// 		echo '<pre>';
		// 		print_r($select2->query());die;

		unset($fields['PS_SUBJECT']);

		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($data);
		
	}
}
