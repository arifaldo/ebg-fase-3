<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';

class predefinedbeneficiary_ImportbeneficiaryController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController(){
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		if($this->_request->isPost() )
		{
			// Set variables needed in view
			$settings = new Application_Settings();
			$ccyList  = $settings->setCurrencyRegistered();
			$privibenelinkage = $this->view->hasPrivilege('BLBU');


			$adapter = new Zend_File_Transfer_Adapter_Http ();
			$adapter->setDestination ( $this->_destinationUploadDir );
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
			$extensionValidator->setMessage(
				//$language->_('Error: Extension file must be').' *.csv'
				$this->language->_('Error: Extension file must be').'*.csv'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				$this->language->_('Error: File size must not more than').' '.$this->getSetting('Fe_attachment_maxbyte')
			);

			$adapter->setValidators ( array (
				$extensionValidator,
				$sizeValidator,
			));


			if ($adapter->isValid ())
			{

				$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

				$adapter->addFilter ( 'Rename',$newFileName  );

				if ($adapter->receive ())
				{
					//PARSING CSV HERE
					$csvData = $this->parseCSV($newFileName);
					//after parse delete document temporary
					@unlink($newFileName);
					//end

					$totalRecords = count($csvData);
					if($totalRecords)
					{
						unset($csvData[0]);
						$totalRecords = count($csvData);
					}

					if($totalRecords)
					{
						$maxTotalRows 	= $this->getSetting('max_import_bene');

						if($totalRecords <= $maxTotalRows)
						{
							$rowNum = 0;
							$totalSucces = 0;
							$totalFailed = 0;
							$errmsg = array();

							foreach ( $csvData as $columns )
							{
								if(count($columns)==4)
								{
									$BENEFICIARY_ACCOUNT = trim($columns[0]);
									$BENEFICIARY_NAME = trim($columns[1]);
									$CURR_CODE = strtoupper(trim($columns[2]));
									$BENEFICIARY_EMAIL = trim($columns[3]);

									$filter = new Application_Filtering();
						//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
									$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT  , "ACCOUNT_NO");
									$ACBENEF_NAME   = $filter->filter($BENEFICIARY_NAME , "BENEFICIARY_NAME");
									$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL , "EMAIL");
									$ACBENEF_CCY   = $filter->filter($CURR_CODE  , "SELECTION");
//	   								$filters = array(
//	   													'BENEFICIARY_ACCOUNT' 		=> array('StringTrim','StripTags','StringToUpper'),
//														'CURR_CODE' 	=> array('StringTrim','StripTags','StringToUpper'),
//													);

									$params['BENEFICIARY_ACCOUNT'] = $BENEFICIARY_ACCOUNT;
									$params['CURR_CODE'] = $CURR_CODE;

									$paramBene = array("TRANSFER_TYPE"      => 'PB',
																			"FROM"      => 'B',
																			 "ACBENEF"     => $ACBENEF,
																			 "ACBENEF_CCY"    => $ACBENEF_CCY,
																			 //"ACBENEF_ALIAS"    => $ACBENEF_ALIAS,
																			 "ACBENEF_BANKNAME"    => $ACBENEF_BANKNAME,
																			 "ACBENEF_EMAIL"    => $ACBENEF_EMAIL,
																			 "_addBeneficiary"   => $this->view->hasPrivilege('BADA'),
																			 "_beneLinkage"    => $privibenelinkage,
																		);

//									$validators = array('BENEFICIARY_ACCOUNT' 		=> array(	'NotEmpty',
//																					'Digits',
//																					new Zend_Validate_StringLength(array('max'=>15,'min'=>8)),
//																					'messages' => array(
//																						'Beneficiary Account cannot be left blank. Please correct it.',
//																						'Beneficiary Account must be numbers. Please correct it.',
//																						'Beneficiary Account length must be 10 digits. Please correct it.',
//																						)
//																					),
//														'CURR_CODE' 	=> array(	'NotEmpty',
//																						array('InArray', array('haystack' => $this->_listCCYValidate)),
//																						'messages' => array(
//																						'Currency cannot be left blank. Please correct it.',
//																						'Currency is not on the database.')
//																				),
//																);
//									 $zf_filter_input = new Zend_Filter_Input($filters,$validators,$params,$this->_optionsValidator);
									$zf_filter_input = array('*' => array('HtmlEntities', 'StripTags', 'StringTrim'));
									$validators = array('BENEFICIARY_ACCOUNT' 		=> array(	'NotEmpty',
																					'Digits',
																					new Zend_Validate_StringLength(array('max'=>15,'min'=>8)),
																					'messages' => array(
																						'Beneficiary Account cannot be left blank. Please correct it.',
																						'Beneficiary Account must be numbers. Please correct it.',
																						'Beneficiary Account length must be 10 digits. Please correct it.',
																						)
																					),
														'CURR_CODE' 	=> array(	'NotEmpty',
																						array('InArray', array('haystack' => $this->_listCCYValidate)),
																						'messages' => array(
																						'Currency cannot be left blank. Please correct it.',
																						'Currency is not on the database.')
																				),
																);

									$zf_filter_input = new Zend_Filter_Input($zf_filter_input, $validators, $this->_getAllParams(), array('breakChainOnFailure' => true));
//									 	$content[$rowNum] = array(
//													'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
//													'USER_ID_LOGIN' 			=> $this->_userIdLogin,
//													//'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
//													'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
//													'BENEFICIARY_NAME' 			=> $paramBene['ACBENEF_BANKNAME'],
//													'CURR_CODE' 				=> $CURR_CODE,
//													'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
//													'BENEFICIARY_CITIZENSHIP' 	=> null,
//													'BENEFICIARY_ADDRESS'		=> null,
//													'BENEFICIARY_TYPE' 			=> 1,
//													'SWIFT_CODE' 				=> null,
//													'CLR_CODE' 					=> null,
//													'_beneLinkage'     => $privibenelinkage,
//										);

									if($zf_filter_input->isValid()){
									 	$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
									 	$validateACBENEF->check($paramBene);

									 	$content[$rowNum] = array(
													'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
													'USER_ID_LOGIN' 			=> $this->_userIdLogin,
													//'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
													'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
													'BENEFICIARY_NAME' 			=> $paramBene['ACBENEF_BANKNAME'],
													'CURR_CODE' 				=> $CURR_CODE,
													'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
													'BENEFICIARY_CITIZENSHIP' 	=> null,
													'BENEFICIARY_ADDRESS'		=> null,
													'BENEFICIARY_TYPE' 			=> 1,
													'SWIFT_CODE' 				=> null,
													'CLR_CODE' 					=> null,
													'_beneLinkage'     => $privibenelinkage,
													'BENEFICIARY_NEWNAME'     => $ACBENEF_NAME,
										);

										 if ($validateACBENEF->isError())
										 {
											$content[$rowNum] += array( 'ERROR_MESSAGE' => $validateACBENEF->getErrorMsg() );
										 }

										 $validateACBENEF->__destruct();
										 unset($validateACBENEF);
									 }
									 else{
									 	//Zend_Debug::dump($zf_filter_input->getMessages());die;
//									 	die;
									 	$content[$rowNum] += array( 'ERROR_MESSAGE' => 	$this->language->_('Benficiary Account data is invalid'));
//									 	$content[$rowNum] += array( 'ERROR_MESSAGE' => 	$zf_filter_input->getMessages());
									 }


								 $rowNum++;



								}else{
									$this->view->error =1;
									//$errmsg[] = 'Number of column for all rows to be imported should be 4';
									$errmsg[] = 'Wrong File Format';
									$this->view->errmsg = $this->DisplayError($errmsg);
									 $sessionNamespace = new Zend_Session_Namespace('errimport');
									 $sessionNamespace->error = $this->DisplayError($errmsg);
									 $this->_redirect('/predefinedbeneficiary/inhouseacct'); 
									break;
								}
							}

							if(!$errmsg)
							{
								$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
								$sessionNamespace->content = $content;
								//Zend_Debug::dump($sessionNamespace->content);die;
								//$_SESSION['import_predefbenef'] = $content;
								$this->_redirect("predefinedbeneficiary/importbeneficiaryapprove");
								//Zend_Debug::dump($_SESSION['import_predefbenef']);die;
								Application_Helper_General::writeLog('BIMA','Import Beneficiary');
							}
						}
						else{
							$this->view->error =1;
							$errmsg[] = 'Total row imported cannot be more than'.$maxTotalRows;
							$this->view->errmsg = $this->DisplayError($errmsg);
							$sessionNamespace = new Zend_Session_Namespace('errimport');
									 $sessionNamespace->error = $this->DisplayError($errmsg);
									 $this->_redirect('/predefinedbeneficiary/inhouseacct'); 
						}
					}
					else{
						$this->view->error =1;
// 						$errmsg[] = 'Number of column for all rows to be imported should be 4';
						$errmsg[] = 'Wrong File Format. There is no data on csv File.';
						$this->view->errmsg = $this->DisplayError($errmsg);
									 $sessionNamespace = new Zend_Session_Namespace('errimport');
									 $sessionNamespace->error = $this->DisplayError($errmsg);
									 $this->_redirect('/predefinedbeneficiary/inhouseacct'); 
					}
				}
			}
			else
			{
				$this->view->error =1;
				$this->view->errmsg = $this->DisplayError($adapter->getMessages());
				$sessionNamespace = new Zend_Session_Namespace('errimport');
									 $sessionNamespace->error = $this->DisplayError($adapter->getMessages());
									 $this->_redirect('/predefinedbeneficiary/inhouseacct'); 
				//Zend_Debug::dump($adapter->getMessages());die;
			}
		}
		Application_Helper_General::writeLog('BIMA','Viewing Import Beneficiary');
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}

}
