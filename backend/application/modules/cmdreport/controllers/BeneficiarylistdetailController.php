<?php

require_once 'Zend/Controller/Action.php';

class Cmdreport_BeneficiarylistdetailController extends Application_Main
{
  	public function indexAction() 
  	{ 
		$pdf = $this->_getParam('pdf');
	  	$userid = $this->_getParam('userid');
	  	$dataid = $this->_getParam('dataid');
	  	$beneftype = $this->_getParam('type');
	  	
	  	$sendparam = array();
	  	$sendparam['userid'] = $userid;
	  	$sendparam['dataid'] = $dataid;
	  	$sendparam['type'] = $beneftype;
	  	
	  	$getBeneficiary = new cmdreport_Model_Cmdreport();
		$detail = $getBeneficiary->getBeneficiary('all',$sendparam,null);
		//$detail = $getBeneficiary -> getBeneficiary('list',$sendparam,null);
	  	//Zend_Debug::dump($detail);
		// BENEFICIARY DETAIL
		Application_Helper_General::writeLog('RPBU','View Destination Detail . Beneficiary Account :'.($detail['BENEFICIARY_ACCOUNT']).'. Beneficiary Name :'.($detail['USER_FULLNAME']).'');
		$userid_lg = $this->language->_('User ID');
	    $username = $this->language->_('User Name');
	    $beneficiaryaccount = $this->language->_('Beneficiary Accounnt');
	    $beneficiaryname = $this->language->_('Beneficiary Name');
	    $beneficiaryalias = $this->language->_('Beneficiary Alias');
	    $type = $this->language->_('Type');
	    $currency = $this->language->_('Currency');
	    $beneficiaryaddress = $this->language->_('Beneficiary Address');
	    $citizenship = $this->language->_('Citizenship');
	    $bank = $this->language->_('Bank Name');
	    $city = $this->language->_('City');
	    $email = $this->language->_('Email');
	    $createddate = $this->language->_('Created Date');
	    $createdby = $this->language->_('Created By');
	    $approvedstatus = $this->language->_('Approved Status');
	    $requesttodelete = $this->language->_('Request to Delete');
	    $beneficiarydet = $this->language->_('Beneficiary Detail');
	    $customerdetail = $this->language->_('Customer Detail');
	    
	    
	    $htmldataDetail .= 
			'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="400px">
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$userid_lg.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['USER_ID'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$username.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['USER_FULLNAME'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp;  '.$beneficiaryaccount.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_ACCOUNT'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$beneficiaryname.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_NAME'].'</td>
				</tr>';
				/*<tr>
					<td class="tbl-evencontent">&nbsp; '.$beneficiaryalias.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_ALIAS'].'</td>
				</tr>*/
	    
	    	// 9. Create LLD string
			$settings 			= new Application_Settings();
			$LLD_array 			= array();
			$LLD_DESC_arrayCat 	= array();
			$lldTypeArr  		= $settings->getLLDDOMType();
			
			if (!empty($detail['BENEFICIARY_CATEGORY']))
			{
				$lldCategoryArr  	= $settings->getLLDDOMCategory();
				$LLD_array["CT"] 	= $detail['BENEFICIARY_CATEGORY'];
				$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$detail['BENEFICIARY_CATEGORY']];
			}
			
  			if (!empty($detail['BENEFICIARY_ID_TYPE']))
			{
				$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
				$LLD_array["CT"] 	= $detail['BENEFICIARY_ID_TYPE'];
				$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$detail['BENEFICIARY_ID_TYPE']];
			}
			
			$CITY_CODEGet = (!empty($detail['BENEFICIARY_CITY_CODE'])?$detail['BENEFICIARY_CITY_CODE']:'');
			$arr 					= $getBeneficiary->getCityCode($CITY_CODEGet);
			
			if($detail['BENEFICIARY_RESIDENT'] == 'R'){
				$resident = $this->language->_('Resident');
			}
  			elseif($detail['BENEFICIARY_RESIDENT'] == 'NR'){
				$resident = $this->language->_('Non Resident');
			}
			
			if($detail['BENEFICIARY_ISAPPROVE']=='1'){
				$detail['BENEFICIARY_ISAPPROVE'] = 'Yes';
			}else{
				$detail['BENEFICIARY_ISAPPROVE'] = 'No';
			}
			
	    $htmldataDetail .=
				'<tr>
					<td class="tbl-evencontent">&nbsp; '.$type.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_TYPE'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$currency.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['CURR_CODE'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$beneficiaryaddress.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_ADDRESS'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$citizenship.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$resident.'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$email.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_EMAIL'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Bank Code</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BANK_CODE'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$bank.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BANK_NAME'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Bank Address 1</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BANK_ADDRESS1'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Bank Address 2</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BANK_ADDRESS2'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Identical Status</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent"></td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Citizenship').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent"></td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Category').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$LLD_CATEGORY_POST.'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Identification Type').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_ID_TYPE'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Identification Number').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_ID_NUMBER'].'</td>
				</tr>			
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Transactor Relationship').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent"></td>
				</tr>
				
				
				
				
				
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$createddate.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.Application_Helper_General::convertDate($detail['BENEFICIARY_CREATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat).'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$createdby.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_CREATEDBY'].'</td>
				</tr>
				
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$approvedstatus.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_ISAPPROVE'].'</td>
				</tr>';
				
				/*<tr>
					<td class="tbl-evencontent">&nbsp; '.$requesttodelete.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$detail['BENEFICIARY_ISREQUEST_DELETE'].'</td>
				</tr>*/
	   	$htmldataDetail .='
			</table>';
	    
		$this->view->templateDetail = $htmldataDetail;	
				
	$dump = '<tr>
		<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Id Type').'</td>
		<td class="tbl-evencontent">:</td>
		<td class="tbl-evencontent">'.$LLD_BENEIDENTIF_POST.'</td>
		</tr>
		
		<tr>
		<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Id Number').'</td>
		<td class="tbl-evencontent">:</td>
		<td class="tbl-evencontent">'.$detail['BENEFICIARY_ID_NUMBER'].'</td>
		</tr>
		
		<tr>
		<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary City').'</td>
		<td class="tbl-evencontent">:</td>
		<td class="tbl-evencontent">'.$arr[0]['CITY_NAME'].'</td>
		</tr>
		';
	
		if($pdf)
		{
			Application_Helper_General::writeLog('RPCU','Download PDF Beneficiary Detail ('.$userid.')');
			$datapdf 	= 
						"<div class='tabheader' id='tabheader'>$beneficiarydet</div>
						<h2>$customerdetail</h2>
						".$htmldataDetail;

			$datapdf = "<tr><td>".$datapdf."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Beneficiary Detail',$datapdf);		
		}
		else
		{
			Application_Helper_General::writeLog('RPBU','View Destination Detail ('.$userid.')');
		}
	}
}