<?php

class Logout_IndexController extends Application_Main {
	
	
	public function indexAction(){
			if(Zend_Auth::getInstance()->hasIdentity()){
			Application_Helper_General::writeLog('BLGT','Logout system from '.$_SERVER['REMOTE_ADDR']);
			Zend_Auth::getInstance()->clearIdentity();
		}
		$this->_redirect('/');
	}
}
?>
