<?php
require_once(realpath(dirname(__FILE__) . '/zf-cli.php'));

class autoDownloadCertificate
{
    protected $db;
    protected $path;

    public function __construct()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->path = UPLOAD_PATH . '/document/submit/';
    }

    /**
     * @return void
     */
    public function download(): void
    {
        $data = $this->getData();

        if ($data) {
            foreach ($data as $key => $value) {

                $file = $this->path . $value['DOCUMENT_ID'] . '.pdf';

                file_put_contents($file, fopen($value['URL'], 'r'));
                sleep(2);

                $getMimeType = mime_content_type($file);
                $getFileSize = filesize($file);
                $fileExist = file_exists($file);

                if ($getMimeType != 'application/pdf' || $getFileSize == 0 || !$fileExist) {
                    $this->updateData($value['DOCUMENT_ID']);

                    $getUpdateData = $this->getSelectedData($value['DOCUMENT_ID']);
                    
                    $create = new DateTime($getUpdateData['CREATED_AT']);
                    $diffWithLast = (array) $create->diff(new DateTime($getUpdateData['LAST_TRY_DOWNLOAD']));

                    if ($diffWithLast['d'] >= 3) $this->deleteData($value['DOCUMENT_ID']);
                } else {
                    $this->deleteData($value['DOCUMENT_ID']);
                }
            }
        }
    }

    /**
     * @return array
     */
    protected function getData(): array
    {

        $dataToDownloads = $this->db->select()
            ->from('SERTIFIKAT_TEKENAJA')
            ->query()->fetchAll();

        return $result = $dataToDownloads;
    }

    /**
     * @param string $documentId
     * 
     * @return void
     */
    protected function deleteData(string $documentId): void
    {
        $this->db->delete('SERTIFIKAT_TEKENAJA', [
            'DOCUMENT_ID = ?' => $documentId
        ]);
    }

    protected function getSelectedData($documentId): array
    {

        $getData = $this->db->select()
            ->from('SERTIFIKAT_TEKENAJA')
            ->where('DOCUMENT_ID = ?', $documentId)
            ->query()->fetch();

        return $result = $getData;
    }

    /**
     * @param string $documentId
     * 
     * @return void
     */
    protected function updateData(string $documentId): void
    {
        $this->db->update('SERTIFIKAT_TEKENAJA', [
            'LAST_TRY_DOWNLOAD' => new Zend_Db_Expr('now()'),
        ], [
            'DOCUMENT_ID = ?' => $documentId
        ]);
    }

    public function insertData(): void
    {
        $this->db->insert('SERTIFIKAT_TEKENAJA', [
            'DOCUMENT_ID' => 'test123',
            'URL' => 'https://mag.wcoomd.org/uploads/2018/05/blank.pdf',
            'LAST_TRY_DOWNLOAD' => new Zend_Db_Expr('now()'),
            'CREATED_AT' => new Zend_Db_Expr('now()')
        ]);
    }
}

$call = new autoDownloadCertificate;

// $call->insertData();
$call->download();
