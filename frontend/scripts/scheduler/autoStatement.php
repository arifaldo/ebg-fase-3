<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/Account.php';
	require_once 'General/Settings.php';
	
	$setting = new Settings();
	$starttime = $setting->getSetting('start_running_balance');
	$endtime = $setting->getSetting('cut_off_running_balance');	
	$begin = new DateTime($starttime);
	$end = new DateTime($endtime);
	$now = new DateTime();
		
			  	


	// if ($now >= $begin && $now <= $end){
		// die;
	
	// die('here');
	$db = Zend_Db_Table::getDefaultAdapter();
	$actionid = 'scheduler';
	$moduleid = 'reject payment';
	$interval = 2;
	$truecounter = 0;
	$falsecounter = 0;
	
	$dataCM = $db->select()
						->FROM('M_CUSTOMER_ACCT',array('ACCT_NO','CUST_ID','CCY_ID'))
						->WHERE('ACCT_STATUS = 1 OR ACCT_STATUS = 3 ')
						// ->WHERE('CUST_ID = ?','FIRSTMED1A')
						->QUERY()->FETCHALL();
	
	$dateFromDB = date('Y-m-01');
	$dateToDB   = date('Y-m-d');
	if($dataCM)
	{
		foreach($dataCM as $row)
		{

					$data =  new Service_Account($row['ACCT_NO'],$row['CCY_ID']);
					$dataInquiry = $data->accountInquiry('AB',FALSE); 
					$dataAccountType = $dataInquiry['AccountType'];
					
					$dataStatement = $data->accountStatementHistory($dateFromDB, $dateToDB, $isMobile = FALSE, $dataAccountType);
					$sessionData = new Zend_Session_Namespace('TRX_INQ'); 
					$sessionData->Detail 			= $dataStatement['Detail'];
					// print_r($dataInquiry);
					print_r($dataStatement);
			if($dataInquiry['ResponseCode'] == '00' && $dataStatement['ResponseCode'] == '00'){

					if(!empty($dataStatement['Detail'])){
						$totalCredit = 0;
						$totalDebet = 0;
						// print_r($datadtl->DBC);
						// 	print_r($datadtl->Amount);die('here');
						foreach($dataStatement['Detail'] as $dt => $datadtl)
						{

							if ($datadtl->DBCR == 'D')
							{
							$totalDebet = $totalDebet + $datadtl->Amount;
							}else{
							$totalCredit = $totalCredit + $datadtl->Amount;
							}
						}


						$cekdatac = $db->select()
						->FROM('T_STATEMENT',array('ID'))
						->WHERE('MONTH(NOW()) = MONTH(STAT_DATE)')
						->WHERE('YEAR(NOW()) = YEAR(STAT_DATE)')
						->WHERE('STAT_TYPE = ?','C')
						->WHERE('ACCT_NO = ?',$row['ACCT_NO'])
						// echo $cekdatac;
						->QUERY()->FETCHALL();

						$cekdatad = $db->select()
						->FROM('T_STATEMENT',array('ID'))
						->WHERE('MONTH(NOW()) = MONTH(STAT_DATE)')
						->WHERE('YEAR(NOW()) = YEAR(STAT_DATE)')
						->WHERE('STAT_TYPE = ?','D')
						->WHERE('ACCT_NO = ?',$row['ACCT_NO'])
						->QUERY()->FETCHALL();
						// echo $cekdatad;

						if(empty($cekdatac)){
							$logDataC = array(	
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'STAT_DATE'			=> $dateToDB,
										'STAT_TYPE' 		=> 'C',
										'STAT_AMOUNT'		=> $totalCredit
									);
							// print_r($logDataC);
							$db->insert('T_STATEMENT', $logDataC);

						}else{
							$paymentData = array(
								 'ACCT_NO' 			=> $row['ACCT_NO'],
								 'STAT_DATE' 		=> $dateToDB,
								 'STAT_TYPE' 		=> 'C',
								 'STAT_AMOUNT'		=> $totalCredit
								);
							$whereData   = array('ID = ?'	=> (string) $cekdatac['ID']);
							$db->update('T_STATEMENT', $paymentData, $whereData);


						}


						if(empty($cekdatad)){

							$logDataD = array(	
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'STAT_DATE'			=> $dateToDB,
										'STAT_TYPE' 		=> 'D',
										'STAT_AMOUNT'		=> '-'.$totalDebet
									);
							// print_r($logDataD);
							$db->insert('T_STATEMENT', $logDataD);
						}else{
							$paymentData = array(
								 'ACCT_NO' 			=> $row['ACCT_NO'],
								 'STAT_DATE' 		=> $dateToDB,
								 'STAT_TYPE' 		=> 'D',
								 'STAT_AMOUNT'		=> $totalCredit
								);
							$whereData   = array('ID = ?'	=> (string) $cekdatad['ID']);
							$db->update('T_STATEMENT', $paymentData, $whereData);

						}

						// die;

					}else{
						$cekdatac = $db->select()
						->FROM('T_STATEMENT',array('ID'))
						->WHERE('MONTH(NOW()) = MONTH(STAT_DATE)')
						->WHERE('YEAR(NOW()) = YEAR(STAT_DATE)')
						->WHERE('STAT_TYPE = ?','C')
						->WHERE('ACCT_NO = ?',$row['ACCT_NO'])
						// echo $cekdatac;
						->QUERY()->FETCHALL();

						$cekdatad = $db->select()
						->FROM('T_STATEMENT',array('ID'))
						->WHERE('MONTH(NOW()) = MONTH(STAT_DATE)')
						->WHERE('YEAR(NOW()) = YEAR(STAT_DATE)')
						->WHERE('STAT_TYPE = ?','D')
						->WHERE('ACCT_NO = ?',$row['ACCT_NO'])
						->QUERY()->FETCHALL();
						// echo $cekdatad;

						if(empty($cekdatac)){
							$logDataC = array(	
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'STAT_DATE'			=> $dateToDB,
										'STAT_TYPE' 		=> 'C',
										'STAT_AMOUNT'		=> 0
									);
							// print_r($logDataC);
							$db->insert('T_STATEMENT', $logDataC);

						}


						if(empty($cekdatad)){

							$logDataD = array(	
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'STAT_DATE'			=> $dateToDB,
										'STAT_TYPE' 		=> 'D',
										'STAT_AMOUNT'		=> 0
									);
							// print_r($logDataD);
							$db->insert('T_STATEMENT', $logDataD);
						}


					}

				}else{

					$cekdatac = $db->select()
						->FROM('T_STATEMENT',array('ID'))
						->WHERE('MONTH(NOW()) = MONTH(STAT_DATE)')
						->WHERE('YEAR(NOW()) = YEAR(STAT_DATE)')
						->WHERE('STAT_TYPE = ?','C')
						->WHERE('ACCT_NO = ?',$row['ACCT_NO'])
						// echo $cekdatac;
						->QUERY()->FETCHALL();

						$cekdatad = $db->select()
						->FROM('T_STATEMENT',array('ID'))
						->WHERE('MONTH(NOW()) = MONTH(STAT_DATE)')
						->WHERE('YEAR(NOW()) = YEAR(STAT_DATE)')
						->WHERE('STAT_TYPE = ?','D')
						->WHERE('ACCT_NO = ?',$row['ACCT_NO'])
						->QUERY()->FETCHALL();
						// echo $cekdatad;

						if(empty($cekdatac)){
							$logDataC = array(	
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'STAT_DATE'			=> $dateToDB,
										'STAT_TYPE' 		=> 'C',
										'STAT_AMOUNT'		=> 0
									);
							// print_r($logDataC);
							$db->insert('T_STATEMENT', $logDataC);

						}


						if(empty($cekdatad)){

							$logDataD = array(	
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'STAT_DATE'			=> $dateToDB,
										'STAT_TYPE' 		=> 'D',
										'STAT_AMOUNT'		=> 0
									);
							// print_r($logDataD);
							$db->insert('T_STATEMENT', $logDataD);
						}

				}

		
			
		}
		
				
	}

// }

	
?>