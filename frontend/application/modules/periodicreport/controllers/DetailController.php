<?php

require_once 'Zend/Controller/Action.php';

class periodicreport_DetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$cfd = $this->_getParam('cfd');
  	$pdf = $this->_getParam('pdf');
  	$psnumber = $this->_getParam('psnumber');

  	$getPaymentDetail 	= new paymentreport_Model_Paymentreport();
  	$detail = $getPaymentDetail->getPaymentDetail($psnumber);
  	$history = $getPaymentDetail->getHistory($psnumber);
  	
  	$paymentref = $detail[0]['PS_NUMBER'];
  	$paystatus = $detail[0]['PS_STATUS'];
  	$source = $detail[0]['ACCT_SOURCE'];
  	$transmission = '';
  	$traceno = '';
  	$challenge = $detail[0]['PS_RELEASER_CHALLENGE'];
  	$created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
  	$updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
  	$efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
  	$amount = $detail[0]['PS_CCY'].' '.Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']);
  	$type = $detail[0]['PS_TYPE'];
  	$cektype = $detail[0]['CEK_PS_TYPE'];
  	$paysubject = $detail[0]['PS_SUBJECT'];
  	//TEMPLATE DETAIL
	if($cektype == 16 || $cektype == 17)
	{
		$pslipdetail = $getPaymentDetail->getPslipDetail($psnumber);
		$htmldataDetailDetail = '';
		foreach($pslipdetail as $pslipdetaillist)
		{
		   	if($pslipdetaillist['PS_FIELDTYPE'] == 1)
		   	{
		   		$value = Application_Helper_General::displayMoney($pslipdetaillist['PS_FIELDVALUE']);
		   	}
			elseif($pslipdetaillist['PS_FIELDTYPE'] == 2)
		   	{
		   		$value = Application_Helper_General::convertDate($pslipdetaillist['PS_FIELDVALUE'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		   	}
		   	elseif($pslipdetaillist['PS_FIELDTYPE'] == 3)
		   	{
		   		$value = $pslipdetaillist['PS_FIELDVALUE'];
		   	}
		   	else
		   	{
		   		$value = '';
		   	}
		   	
		   	$htmldataDetailDetail .='
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_($pslipdetaillist['PS_FIELDNAME']).'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$this->language->_($value).'</td>
			</tr>
			';
		}
		
		$htmldataDetail = 
		'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="500px">
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Payment Ref').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$paymentref.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Date Time').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$updated.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Source Account').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$source.'</td>
			</tr>
			'.$htmldataDetailDetail.'
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Status').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$this->language->_($paystatus).'</td>
			</tr>
		</table>';
	}
	else
	{
		/*<tr>
				<td class="tbl-evencontent">&nbsp; Transmission</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$transmission.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; Trace No.</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$traceno.'</td>
		</tr>
		<tr>
				<td class="tbl-evencontent">&nbsp; Challenge Code</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$challenge.'</td>
		</tr>
		
		<tr>
				<td class="tbl-evencontent">&nbsp; Total Payment</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$amount.'</td>
		</tr>
		*/
			
    	$htmldataDetail = 
		'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="500px">
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Payment Ref').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$paymentref.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Created Date').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$created.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Updated Date').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$updated.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Payment Date').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$efdate.'</td>
			</tr>
			
	
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Source Account').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$source.'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Payment Type').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$this->language->_($type).'</td>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp; '.$this->language->_('Status').'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$this->language->_($paystatus).'</td>
			</tr>
		
		</table>';
	}
	
	// TEMPLATE TRANSACTION
	//$cektype  11 utnutk purchase
	//$cektype  1 utnutk transfer
	//$cektype  12 utnutk payment
	
	//if($cektype != 11 && $cektype != 12)
	if($cektype != 110 && $cektype != 120)
	{
		$i = 1;
		$htmldataTransactionDetail = '';
		foreach($detail as $transactionlist)
		{
		   	$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
		   	
		   	if($transactionlist['BENEFICIARY_CITIZENSHIP'] == 'R')
		   	{
		   		$citizenship = 'Resident';
		   	}
			elseif($transactionlist['BENEFICIARY_CITIZENSHIP'] == 'NR')
		   	{
		   		$citizenship = 'Non Resident';
		   	}
		   	else
		   	{
		   		$citizenship = '';
		   	}
		   	
		   	if($cektype == '16'){ //type purchase
		   		$BENEFICIARY_ACCOUNT = '-';
		   		$BENEFICIARY_ACCOUNT_NAME = '-';
		   	}
		   	elseif($cektype == '17'){ //payment
		   		$BENEFICIARY_ACCOUNT = '-';
		   		$BENEFICIARY_ACCOUNT_NAME = '-';
		   	}
		   	else{
		   		$BENEFICIARY_ACCOUNT = $transactionlist['BENEFICIARY_ACCOUNT'];
		   		$BENEFICIARY_ACCOUNT_NAME = $transactionlist['BENEFICIARY_ACCOUNT_NAME'].' ('.$transactionlist['BENEFICIARY_ALIAS_NAME'].')';
		   	}
		   	//<td class="'.$td_css.'">'.$transactionlist['TRANSACTION_ID'].'</td>
		   	//<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($transactionlist['TRANSFER_FEE']).'</td>
		   	//<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_BANK_CITY'].'</td>
			//<td class="'.$td_css.'">'.$citizenship.'</td>
		
			   	$htmldataTransactionDetail .='
				<tr>
					<td class="'.$td_css.'">'.$BENEFICIARY_ACCOUNT_NAME.'</td>
					<td class="'.$td_css.'">'.$BENEFICIARY_ACCOUNT.'</td>				
					<td class="'.$td_css.'">'.$transactionlist['TRA_MESSAGE'].'</td>
					<td class="'.$td_css.'">'.$this->language->_($transactionlist['TRANSFER_TYPE']).'</td>
					<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_ACCOUNT_CCY'].'</td>
					<td class="'.$td_css.'">'.Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']).'</td>
					<td class="'.$td_css.'">'.$transactionlist['BENEFICIARY_BANK_NAME'].'</td>
					<td class="'.$td_css.'">'.$this->language->_($transactionlist['TRANSFER_STATUS']).'</td>
				</tr>
				';
			
			$i++;
			//<td class="'.$td_css.'">'.$transactionlist['TRA_REFNO'].'</td> //additional message
		}
		if(empty($transactionlist))
		{
			$htmldataTransactionDetail ='
			<tr>
				<td class="tbl-evencontent" colspan="5" align="center">'.$this->language->_('No Data').'</td>
			</tr>';
		}
		//<th valign="top">Transaction ID</th>	
		//<th valign="top">Charge</th>
		//<th valign="top">City</th>
		//<th valign="top">Citizenship</th>
		$htmldataTransaction = 
		'<table border="1" cellspacing="0" cellpadding="0" class="tableform">
			<tr>
				
				<th valign="top">'.$this->language->_('Beneficiary Account Name (Alias Name)').' </th>
				<th valign="top">'.$this->language->_('Beneficiary Account').'</th>
				<th valign="top">'.$this->language->_('Message').'</th>
				<th valign="top">'.$this->language->_('Transfer Type').'</th>
				<th valign="top">'.$this->language->_('CCY').'</th>
				<th valign="top">'.$this->language->_('Amount').'</th>
				<th valign="top">'.$this->language->_('Bank').'</th>
				<th valign="top">'.$this->language->_('Transaction Status').'</th>
			</tr>'
			.$htmldataTransactionDetail.'
		</table>
		<br>
		<b>'.$this->language->_('Total 1 payment(s), Amount').' '.Application_Helper_General::displayMoney($transactionlist['TRA_AMOUNT']).'
		</b>
		';
	}
	
  	// TEMPLATE HISTORY
	{
			$i = 1;
			$htmldataHistoryDetail = '';
			foreach($history as $historylist)
			{
			   	$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';

				$htmldataHistoryDetail .='
				<tr>
					<td class="'.$td_css.'">'.$historylist['USER_LOGIN'].'</td>
					<td class="'.$td_css.'">'.$this->language->_($historylist['HISTORY_STATUS']).'</td>
					<td class="'.$td_css.'">'.Application_Helper_General::convertDate($historylist['DATE_TIME'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat).'</td>
					<td class="'.$td_css.'">'.$historylist['PS_REASON'].'</td>
				</tr>
				';
				$i++;
			}
			if(empty($history))
			{
				$htmldataHistoryDetail ='
				<tr>
					<td class="tbl-evencontent" colspan="5" align="center">'.$this->language->_('No Data').'</td>
				</tr>';
			}
			
		$htmldataHistory = 
		'<table border="1" cellspacing="0" cellpadding="0" class="tableform">
			<tr>
				<th valign="top">'.$this->language->_('User').'</th>
				<th valign="top">'.$this->language->_('Action').'</th>
				<th valign="top">'.$this->language->_('Date/Time').'</th>
				<th valign="top">'.$this->language->_('Remarks').'</th>
			</tr>'
			.$htmldataHistoryDetail.'
		</table>';
			
		if($paystatus == 'Pending Future Date')
		{
		
		$htmldataForm = 
		'
		<form method="post">
			<input class="inputbtn" value="'.$this->language->_('Cancel Future Date').'" name="cfd"  type="submit">
		</form>
		';
			$formButton = 'Pending Future Date';
		}
	
		else{}
	}

	if(isSet($htmldataDetail))$this->view->templateDetail = $htmldataDetail;
	if(isSet($htmldataTransaction))$this->view->templateTransaction = $htmldataTransaction;
	if(isSet($htmldataHistory))$this->view->templateHistory = $htmldataHistory;	
	
	if(isSet($htmldataForm))$this->view->templateForm = $htmldataForm;	
			
		
		if($cfd){
			require_once 'CMD/SinglePayment.php';
			require_once 'CMD/Validate/ValidatePaymentSingle.php';
			require_once 'General/CustomerUser.php';

		    $singlePayment  = new SinglePayment($paymentref,$this->_userIdLogin);
		    $singlePayment->cancelFutureDate("");
			//echo'Cancel Future Date';
			$this->_redirect('/notification/success/index'); 
		}
			
		
		if($pdf)
		{
			Application_Helper_General::writeLog('RPPY','Download PDF Payment Report Detail ('.$psnumber.')');
			
			if($cektype != 16 && $cektype != 17)
			{
				$datapdf 	= 
						"<h2>".$this->language->_('View Payment')."</h2><br/><br/>
						<h2>".$this->language->_('Transfer From')."</h2><hr></hr>
						".$htmldataDetail."
						<br />
						<br />
						<h2>".$this->language->_('Transaction')."</h2>
						".$htmldataTransaction."
						<br />
						<br />
						<h2>".$this->language->_('History')."</h2>
						".$htmldataHistory;
			}
			else
			{
				$datapdf 	= 
						"<h2>".$this->language->_('View Payment')."</h2><br/><br/>
						<h2>".$this->language->_('Transfer From')."</h2><hr></hr>
						".$htmldataDetail."
						<br />
						<br />
						<h2>".$this->language->_('History')."</h2>
						".$htmldataHistory;
			}
			$datapdf = "<tr><td>".$datapdf."</td></tr>";
			$this->_helper->download->pdf(null,null,null,$this->language->_('Payment Report Detail'),$datapdf);		
		}
		else
		{
			Application_Helper_General::writeLog('RPPY','View Payment Report Detail ('.$psnumber.')');
		}
	}
}