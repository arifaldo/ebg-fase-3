<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
class accountstatement_DepositdetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$fields = array(
						'depositno'  => array('field' => 'DEPOSIT_NO',
											   'label' => $this->language->_('Deposit No'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
												'label'    => $this->language->_('CCY'),
												'sortable' => true),
						'interest'  => array('field' => 'INTEREST_RATE',
											   'label' => $this->language->_('Interest Rate'),
											   'sortable' => true),
						'availablebal'   => array('field'    => 'AVAIL_BAL',
											  'label'    => $this->language->_('Available Balance'),
											  'sortable' => true),
						'createddate'   => array('field'    => 'CREATED_DATE',
											  'label'    => $this->language->_('Created Date'),
											  'sortable' => true),
						'duedate'   => array('field'    => 'DUE_DATE',
											  'label'    => $this->language->_('Due Date'),
											  'sortable' => true),
				);
				
		$sortBy  = $this->_getParam('sortby','acct');
		$sortDir = $this->_getParam('sortdir','asc');
		
		// $sortBy = (Zend_Validate::is($sortBy,'InArray',
		// 							 array(array_keys($fields))
		// 							 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		// $sortDir = (Zend_Validate::is($sortDir,'InArray',
		// 								  array('haystack'=>array('asc','desc'))
		// 								  ))? $sortDir : 'asc';
			
										  
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
	/*	$data = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->group('A.ACCT_NO')						 
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
		*/
		$userId = $this->_userIdLogin;
		$model = new accountstatement_Model_Accountstatement();
		$userCIF = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_USER'))
				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
				->limit(1)
		);
// 		$userCIF = $model->getUserCIF($this->_userIdLogin);
		//print_r($this->_userIdLogin);die;
		$param['userId'] = $userId;
		$userAcctInfo = $model->getCustAccount($param);
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];
		
		//print_r($userAcctInfo);die;
		// print_r($accountNo);
		// print_r($accountCcy);
		// print_r($accountType);
		// print_r($userCIF);
		// print_r($userId);die;
		// pei
		if(!empty($userAcctInfo)){
		$Account = new Account($accountNo, $accountCcy, $accountType, $userCIF['CUST_CIF'], $userId);
		$Account->setFlag(false);

		$res = $Account->checkBalanceDeposit();
		$accountListData = $res['AccountList'];
		//print_r($accountListData);die;
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		$totalAvailableBalance = 0;
		
		if (count ( $accountListData ) > 0)
		{
			$data = array();
			
			foreach ( $accountListData as $key=>$row ) {
				/*$Account = new Account($row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
				 $Account->setFlag(false);
				$Account->checkBalanceDeposit();*/
				if (isset($row ['accountNo'])){
					//				if ($row ['status'] == "A"){
					$data[$key]['accountNo'] 	= $row ['accountNo'];
					$data[$key]['currency'] 	= $row ['currency'];
					$data[$key]['interestRate'] = $row ['interestRate'];
					$data[$key]['amount'] 		= $row ['amount'];
					$data[$key]['createDate'] 	= $row ['createDate'];
					$data[$key]['dueDate'] 		= $row ['dueDate'];
					$data[$key]['status'] 		= $row ['status'];
					//				}
				}
			}
		}
		}
		// print_r($data);die;
		$data = array();
		$data[0]['accountNo'] = "20520054011";
		$data[0]['currency'] = "IDR";
		$data[0]['interestRate'] = "0.0925";
		$data[0]['amount'] = "900,000.00";
		$data[0]['createDate'] = "2018-07-15";
		$data[0]['dueDate'] = "2019-07-15";
		$data[0]['status'] = "Open";

		$data[1]['accountNo'] = "20520065209";
		$data[1]['currency'] = "IDR";
		$data[1]['interestRate'] = "0.09";
		$data[1]['amount'] = "650,000.00";
		$data[1]['createDate'] = "2017-06-16";
		$data[1]['dueDate'] 		= "2019-06-16";
		$data[1]['status'] = "Open";
		
		$this->view->custId = $this->_custIdLogin;
// 		$this->view->resultdata = $data;
// 		$this->view->fields = $fields;
		
		$logDesc = 'Viewing';
		
		if($csv || $pdf){
			$arr = $data;
			$header = array(
				$this->language->_('Deposit No'),
				$this->language->_('CCY'),
				$this->language->_('Interest Rate'),
				$this->language->_('Available Balance'),
				$this->language->_('Create Date'),
				$this->language->_('Due Date'),
				$this->language->_('Status'),
			);
		}
		if($csv)
		{
			$logDesc = 'Export to CSV';
			$this->_helper->download->csv($header,$arr,null,$this->language->_('Deposit Account Inquiry'));  
		}
		
		if($pdf){
			$logDesc = 'Export to PDF';
			$this->_helper->download->pdf($header,$arr,null,$this->language->_('Deposit Account Inquiry'));  
		}
		
		if($this->_request->getParam('print') == 1){
			$arr = $data;
			$fields = array(
						'ACCT_NO'  => array('field' => 'accountNo',
											   'label' => $this->language->_('Deposit No'),
											   'sortable' => true),
						'CCY_ID'   => array('field'    => 'currency',
												'label'    => $this->language->_('CCY'),
												'sortable' => true),
						'INRATE'  => array('field' => 'interestRate',
											   'label' => $this->language->_('Interest Rate'),
											   'sortable' => true),
						'AVAILABLE_AMOUNT'   => array('field'    => 'amount',
												'label'    => $this->language->_('Available Balance'),
												'sortable' => true),
			
						'CREATEDATE'   => array('field'    => 'createDate',
											  'label'    => $this->language->_('Create Date'),
											  'sortable' => true),
			
						'DUEDATE'   => array('field'    => 'dueDate',
											  'label'    => $this->language->_('Due Date'),
											  'sortable' => true),
						
						'STATUS'   => array('field'    => 'status',
											  'label'    => $this->language->_('Status'),
											  'sortable' => true)
						
			);
			
//            $data = $arr;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Deposit Inquiry', 'data_header' => $fields));
        }
		
		
		Application_Helper_General::writeLog('DEIQ',$logDesc);
// 		$this->view->userId = $this->_userIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
		$this->view->totalAvailableBalance = $totalAvailableBalance;
	}
	
	
}
