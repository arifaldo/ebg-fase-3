<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class customer_AddinsurancebranchController extends customer_Model_Customer 
{
  public function indexAction() 
  {
    $this->_helper->layout()->setLayout('newlayout');

    $model = new insurancebranch_Model_Insurancebranch();

    if (!$this->view->hasPrivilege("MIBO")) {
      return $this->_redirect("/home/dashboard");
    }

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $process   = $this->_getParam('process');
    $isConfirm = (empty($this->_request->getParam('isConfirm'))) ? false : true;
    $submitBtn = ($this->_request->isPost() && $process == "submit") ? true : false;

    if ($this->_request->isPost()) {
      $params     = $this->_request->getParams();

      $filters    = array('*' => array('StringTrim', 'StripTags'));
      $validators = array(

        'cust_name'   => array('allowEmpty' => true),
        'branch_ccy'  => array('allowEmpty' => true),
        'acct_type'   => array('allowEmpty' => true),
        'branch_name' => array(
          'NotEmpty',
          'messages' => array($this->language->_('Branch Name cannot be empty')),
        ),
        'branch_email'  => array(
          'NotEmpty',
          'messages' => array($this->language->_('Branch Email cannot be empty')),
        ),
        'branch_acct' => array(
          'NotEmpty',
          'messages' => array($this->language->_('Branch Account cannot be empty')),
        ),
      );

      $zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

      $cust_name    = $zf_filter_input->cust_name;
      $branch_ccy   = $zf_filter_input->branch_ccy;
      $acct_type    = $zf_filter_input->acct_type;
      $branch_name  = $zf_filter_input->branch_name;
      $branch_email   = $zf_filter_input->branch_email;
      $branch_acct  = $zf_filter_input->branch_acct;

      if ($submitBtn) {
        if ($zf_filter_input->isValid()) {
          if($isConfirm == false){
            $isConfirm = true;
          }else{
            $generateCode = $model->generateCode();
            $info         = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
            $change_id    = $this->suggestionWaitingApproval('Insurance Branch', $info, $this->_changeType['code']['new'], null, 'M_INS_BRANCH', 'TEMP_INS_BRANCH', $generateCode, $branch_name, $cust_id, $cust_name);

            try {
              $this->_db->beginTransaction();

              $dataInsert = [
                'CHANGES_ID'    => $change_id,
                'INS_BRANCH_CODE' => $generateCode,
                'CUST_ID'     => $cust_id,
                'INS_BRANCH_NAME' => $branch_name,
                'INS_BRANCH_EMAIL'  => $branch_email,
                'INS_BRANCH_ACCT' => $branch_acct,
                'INS_BRANCH_CCY'  => $branch_ccy,
                'ACCT_TYPE'     => $acct_type,
                'FLAG'        => 2, // Waiting Approve
                'LAST_SUGGESTED'  => new Zend_Db_Expr('now()'),
                'LAST_SUGGESTEDBY'  => $this->_userIdLogin
              ];
              $this->_db->insert('TEMP_INS_BRANCH', $dataInsert);

              $this->_db->commit();

              Application_Helper_General::writeLog('MIBO', 'Register New Insurance Branch for ' . $cust_name . ' (' . $cust_id . ')');
              $this->setbackURL('/insurancebranch');
              $this->_redirect('/notification/submited/index');
            } catch (Exception $error) {
              $this->_db->rollBack();
              echo '<pre>';
              print_r($error->getMessage());
              echo '</pre><br>';
              die;
            }
          }
        }else{
          $errors = $zf_filter_input->getMessages();
          $errDesc['cust_id']     = isset($errors['cust_id']) ? $errors['cust_id'] : null;
          $errDesc['branch_name']   = isset($errors['branch_name']) ? $errors['branch_name'] : null;
          $errDesc['branch_email']  = isset($errors['branch_email']) ? $errors['branch_email'] : null;
          $errDesc['branch_acct']   = isset($errors['branch_acct']) ? $errors['branch_acct'] : null;
        }
      } else {
        $isConfirm = false;
      }
    } else {
      Application_Helper_General::writeLog('MIBO', 'View Register New Insurance Branch');
    }

    $this->view->isConfirm    = $isConfirm;
    $this->view->errDesc    = $errDesc;
    $this->view->cust_id    = $cust_id;
    $this->view->cust_name    = $cust_name;
    $this->view->branch_ccy   = $branch_ccy;
    $this->view->acct_type    = $acct_type;
    $this->view->branch_name  = $branch_name;
    $this->view->branch_email   = $branch_email;
    $this->view->branch_acct  = $branch_acct;
  }

  public function checkcustomerAction()
  {
    $this->_helper->viewRenderer->setNoRender(true);
    $this->_helper->layout->disableLayout();

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $model = new insurancebranch_Model_Insurancebranch();

    $getCustAcctById = $model->getCustAcctById($cust_id);

    $custAcctArr = [];
    if($getCustAcctById){
      foreach($getCustAcctById as $row){
        $custAcctArr[$row['ACCT_NO']] = $row['ACCT_NO'].' ['.$row['CCY_ID'].'] / '.$row['ACCT_NAME'].' / '.$row['ACCT_DESC'];
      }
    }

    echo json_encode(array('custAcctArr' => $custAcctArr));
  }

  public function checkaccountAction()
  {
    $this->_helper->viewRenderer->setNoRender(true);
    $this->_helper->layout->disableLayout();

    $model = new insurancebranch_Model_Insurancebranch();

    $request = $this->getRequest();

    $acct_no = $request->acct_no;

    $getCustAcctByAcctNo = $model->getCustAcctByAcctNo($acct_no);

    echo json_encode($getCustAcctByAcctNo);
  }
}