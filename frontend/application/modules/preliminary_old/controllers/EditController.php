<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';
class preliminary_EditController extends user_Model_User
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$cust_id = strtoupper($this->_getParam('cust_id'));
		$cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$conf = Zend_Registry::get('config');
		//BG Document Type
		$bgentType 		= $conf["bg"]["entity"]["desc"];
		$bgentCode 		= $conf["bg"]["entity"]["code"];

		$arrbgEntity = array_combine(array_values($bgentCode), array_values($bgentType));

		$this->view->entityArr  = $arrbgEntity;

		$bgcitizenType 		= $conf["bg"]["citizen"]["desc"];
		$bgcitizenCode 		= $conf["bg"]["citizen"]["code"];

		$arrbgCitizen = array_combine(array_values($bgcitizenCode), array_values($bgcitizenType));
		$this->view->citizenArr  = $arrbgCitizen;

		$bgStatusownerType 		= $conf["bg"]["statusowner"]["desc"];
		$bgStatusownerCode 		= $conf["bg"]["statusowner"]["code"];

		$arrbgStatusowner = array_combine(array_values($bgStatusownerCode), array_values($bgStatusownerType));
		$this->view->statusArr  = $arrbgStatusowner;




		$selectposition = $this->_db->select()
			->from(array('A' => 'M_POSITION'), array('*'))
			->query()->fetchAll();
		$arrposition = array('' => '--Select Position--');
		foreach ($selectposition as $vl) {
			$arrposition[$vl['CODE']] = $vl['POSITION'];
		}
		$this->view->positionArr = $arrposition;

		//var_dump($this->_custNameLogin);
		$this->view->cust_name = $this->_custNameLogin;



		//die('here'); 


		if ($this->_request->isPost()) {


			$attahmentDestination 	= UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			// print_r($attahmentDestination);die;
			$params = $this->_request->getParams();
			$files = $_FILES;
			$jumlahFile = count($files['DOC_FILE']['name']);
			$this->view->error = false;
			if (empty($params['NPWP'])) {
				$this->view->error = true;
				$errors = $this->language->_('NPWP cannot be blank');
				$this->view->errorMsg = $errors;
			} elseif (empty($params['AKTASK'])) {
				$this->view->error = true;
				$errors = $this->language->_('Akta SK cannot be blank');
				$this->view->errorMsg = $errors;
			} elseif ($jumlahFile != 3) {
				$this->view->error = true;
				$errors = $this->language->_('Need upload file');
				$this->view->errorMsg = $errors;
			}


			if (!$this->view->error) {
				for ($i = 0; $i < $jumlahFile; $i++) {
					$namaFile = $files['DOC_FILE']['name'][$i];
					$lokasiTmp = $files['DOC_FILE']['tmp_name'][$i];

					# kita tambahkan uniqid() agar nama gambar bersifat unik
					$namaBaru = uniqid() . '-' . $namaFile;
					$params['listfile'][] = $namaBaru;

					$lokasiBaru = $attahmentDestination . $namaBaru;
					if (file_exists($lokasiTmp)) {
						exec("clamdscan --fdpass " . $lokasiTmp, $out, $int);

						// if()
						//var_dump($int);
						//var_dump($out);
						//die;
						if ($int == 1) {

							unlink($lokasiTmp);
							//die('here');
							$this->view->error = true;
							$errors = array(array('FileType' => 'Error: An infected file. Please upload other file'));
							$this->view->errorMsg = $errors;
						}
						$prosesUpload = move_uploaded_file($lokasiTmp, $lokasiBaru);
						//var_dump($prosesUpload);

					}
				}
				//die('here');




				$filtersName = array(
					'NPWP'	=> array('StringTrim'),
					'AKTASK'		=> array('StringTrim'),
				);

				$validatorsName = array(
					'NPWP' => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Error : File cannot be left blank')
						)
					),
					'AKTASK' => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Error: Description cannot be left blank')
						)
					)
				);

				$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $this->_request->getParams(), $this->_optionsValidator);

				if ($zf_filter_input_name->isValid()) {
					$sessionNamespace = new Zend_Session_Namespace('Preliminary');
					$sessionNamespace->data 	= $params;
					if (!empty($params['SHARE'])) {
						$total = 0;
						foreach ($params['SHARE'] as $val) {
							$total = $total + $val;
						}
						if ($total != 100) {
							$this->view->error = true;
							$errors = $this->language->_('Total Share must be 100');
							$this->view->errorMsg = $errors;
						}
					} else {

						$this->_redirect('/preliminary/edit/next');
					}
				} else {
					$this->view->error = true;
					$errors 						= $zf_filter_input_name->getMessages();
					$this->view->errorMsg = $errors;
					//var_dump($errors);die('here');
				}
			} else {
				$this->view->errorMsg = $errors;
			}



			$this->view->user_id = $user_id;

			Application_Helper_General::writeLog('ADML', 'Add Maker Limit');
		}
	}

	public function nextAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('Preliminary');
		echo '<pre>';
		var_dump($sessionNamespace->data);
		$this->view->data = $sessionNamespace->data;
		$data = $sessionNamespace->data;
		$selectposition = $this->_db->select()
			->from(array('A' => 'M_POSITION'), array('*'))
			->query()->fetchAll();
		$arrposition = array('' => '--Select Position--');
		foreach ($selectposition as $vl) {
			$arrposition[$vl['CODE']] = $vl['POSITION'];
		}
		$this->view->positionArr = $arrposition;


		$selectcity = $this->_db->select()
			->from(array('A' => 'M_CITYLIST'), array('*'))
			->query()->fetchAll();
		$arrcity = array('' => '--Select City--');
		foreach ($selectcity as $vl) {
			$arrcity[$vl['CITY_CODE']] = $vl['CITY_NAME'];
		}
		//var_dump($arrcity);//die;
		$this->view->cityArr = $arrcity;

		if ($this->_request->isPost()) {

			$attahmentDestination 	= UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			// print_r($attahmentDestination);die;
			$params = $this->_request->getParams();
			$files = $_FILES;
			$jumlahFile = count($files['KTP_FILE']['name']);
			for ($i = 0; $i < $jumlahFile; $i++) {
				$namaFile = $files['KTP_FILE']['name'][$i];
				$lokasiTmp = $files['KTP_FILE']['tmp_name'][$i];

				# kita tambahkan uniqid() agar nama gambar bersifat unik
				$namaBaru = uniqid() . '-' . $namaFile;
				$params['listfilektp'][] = $namaBaru;

				$lokasiBaru = $attahmentDestination . $namaBaru;
				if (file_exists($lokasiTmp)) {
					exec("clamdscan --fdpass " . $lokasiTmp, $out, $int);

					// if()
					//var_dump($int);
					//var_dump($out);
					//die;
					if ($int == 1) {

						unlink($lokasiTmp);
						//die('here');
						$this->view->error = true;
						$errors = array(array('FileType' => 'Error: An infected file. Please upload other file'));
						$this->view->errorMsg = $errors;
					}
					$prosesUpload = move_uploaded_file($lokasiTmp, $lokasiBaru);
					//var_dump($prosesUpload);

				}
			}
			$checkdata = $this->_db->select()
				->from(array('A' => 'M_PRELIMINARY'), array('*'))
				->where('A.CUST_ID = ?', $this->_custIdLogin)
				->query()->fetchAll();
			if (empty($checkdata)) {

				$insertArr = array(
					'CUST_ID'               => $this->_custIdLogin,
					'NPWP'               => $data['NPWP'],
					'NPWP_FILE'               => $data['listfile'][0],
					'SK_NUMBER'            => $data['AKTASK'],
					'SK_NUMBER_FILE'            => $data['listfile'][1],
					'SK_DATE'   => $data['ESDATE'],
					'SK_PLACE'   => $data['ESPLACE'],
					'SK_CHANGE'   => $data['LASTAKTA'],
					'SK_CHANGE_FILE'   => $data['listfile'][2]
				);

				$this->_db->insert('M_PRELIMINARY', $insertArr);

				foreach ($data['NAME'] as $key => $val) {
					$insertArr = array(
						'CUST_ID'               => $this->_custIdLogin,
						'NAME'               => $val,
						'ENITTY_ID'               => $data['entity'][$key],
						'CITIZENSHIP'            => $data['citizen'][$key],
						'POSITION'            => $data['position'][$key],
						'SHARE'   => $data['SHARE'][$key],
						'STATUS'   => $data['status'][$key],
						'KTP'   => $params['KTP'][$key],
						'KTP_FILE'   => $params['listfilektp'][$key],
						'ADDRESS'   => $params['ADDRESS'][$key],
						'SUBDISTRICT'   => $params['SUBDISTRICT'][$key],
						'DISTRICT'   => $params['DISTRICT'][$key],
						'CITY'   => $params['CITY'][$key]
					);

					$this->_db->insert('M_PRELIMINARY_MEMBER', $insertArr);
				}
			} else {
				$updateArr = array(
					'NPWP'               => $data['NPWP'],
					'NPWP_FILE'               => $data['listfile'][0],
					'SK_NUMBER'            => $data['AKTASK'],
					'SK_NUMBER_FILE'            => $data['listfile'][1],
					'SK_DATE'   => $data['ESDATE'],
					'SK_PLACE'   => $data['ESPLACE'],
					'SK_CHANGE'   => $data['LASTAKTA'],
					'SK_CHANGE_FILE'   => $data['listfile'][2]
				);
				$whereUpdate = array(
					'CUST_ID = ?' => (string)$this->_custIdLogin
				);
				$this->_db->update('M_PRELIMINARY', $updateArr, $whereUpdate);

				$whereArr = array('CUST_ID = ?' => $this->_custIdLogin);

				$this->_db->delete('M_PRELIMINARY_MEMBER', $whereArr);

				foreach ($data['NAME'] as $key => $val) {
					$insertData = array(
						'CUST_ID'               => $this->_custIdLogin,
						'NAME'               => $val,
						'ENITTY_ID'               => $data['entity'][$key],
						'CITIZENSHIP'            => $data['citizen'][$key],
						'POSITION'            => $data['position'][$key],
						'SHARE'   => $data['SHARE'][$key],
						'STATUS'   => $data['status'][$key],
						'KTP'   => $params['KTP'][$key],
						'KTP_FILE'   => $params['listfilektp'][$key],
						'ADDRESS'   => $params['ADDRESS'][$key],
						'SUBDISTRICT'   => $params['SUBDISTRICT'][$key],
						'DISTRICT'   => $params['DISTRICT'][$key],
						'CITY'   => $params['CITY'][$key]
					);



					$this->_db->insert('M_PRELIMINARY_MEMBER', $insertData);
				}
			}


			$this->setbackURL('/' . $this->_request->getModuleName() . '/');
			$this->_redirect('/notification/success');
		}
	}
}
