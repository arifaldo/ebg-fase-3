<?php
/**
 * Tempcustomer model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempuseropenlimit extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'CST';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) 
	{
	
		//query from TEMP_MAKERLIMIT
		$userLimit = $this->dbObj->select()
						  			->from('TEMP_MAKERLIMIT')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($userLimit))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User Limit)';
			return false;
        }
	
	    //==========================================================================================================
		//insert to master table M_MAKERLIMIT
		$insertArr = array_diff_key($userLimit,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		$insertArr['UPDATED'] = new Zend_Db_Expr('now()');
		$insertArr['UPDATEDBY']   = $actor;
		
		
		$userLimitInsert = $this->dbObj->insert('M_MAKERLIMIT',$insertArr);
		if(!(boolean)$userLimitInsert) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User Limit)';
			return false;
		}
				
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges){
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(User Limit)';
			return false;
		}
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		
		//query from TEMP_MAKERLIMIT
		$userLimit = $this->dbObj->select()
						   ->from('TEMP_MAKERLIMIT')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!count($userLimit))
		{
			
			$global = $this->dbObj->select()
						   ->from('T_GLOBAL_CHANGES')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetchAll(Zend_Db::FETCH_ASSOC);
			
			
								
//			$where['CUST_ID = ?'] = $global['0']['CUST_ID'];
//			$where['USER_LOGIN = ?'] = $global['0']['KEY_FIELD'];
//			$updateArr = array('MAXLIMIT' => 0);
//			$this->dbObj->update('M_MAKERLIMIT',$updateArr,$where);

			$where = array(
			'USER_LOGIN = ?' =>(string)$global[0]['KEY_FIELD'],
			'CUST_ID = ?' =>(string)$global[0]['CUST_ID']
			);
		// print_r($where);
			try {
				$del = $this->dbObj->delete('M_MAKERLIMIT',$where);
			} catch (Exception $e) {
				// print_r($e);
			}
			
			//$selectQuery	= "UPDATE SET MAXLIMIT = 0 WHERE CUST_ID = " . $this->dbObj->quote($global['0']['CUST_ID']) . " AND USER_LOGIN = " . $this->dbObj->quote($global['0']['KEY_FIELD']);
			// echo $selectQuery;
			//$usergoogleAuth =  $this->dbObj->update($selectQuery);
			//die;
			//var_dump($whereArr);
			//var_dump($updateArr);die;
				//$customerupdate = $this->dbObj->update('M_MAKERLIMIT',$updateArr,$whereArr);
			//die; 
			$deleteChanges  = $this->deleteEdit();
			if(!$deleteChanges){
				$this->_errorCode = '82';
				$this->_errorMsg = 'Query failed(User Limit)';
				return false;
			}
        	//$this->_errorCode = '22';
			//$this->_errorMsg = 'Query failed(User Limit)';
			return true;
        }
        
        //delete dari master table M_MAKERLIMIT yg USER_LOGIN nya sama
		$where = array(
			'USER_LOGIN = ?' =>(string)$userLimit[0]['USER_LOGIN'],
			'CUST_ID = ?' =>(string)$userLimit[0]['CUST_ID']
			);
		// print_r($where);
		try {
			$del = $this->dbObj->delete('M_MAKERLIMIT',$where);
		} catch (Exception $e) {
			// print_r($e);
		}
		// die;
		// echo '<pre>';
		foreach($userLimit as $key=>$row)
		{
			//insert to master table M_MAKERLIMIT
			$insertArr[$key] = array_diff_key($userLimit[$key],array('TEMP_ID'=>'','CHANGES_ID'=>''));
			if(!$row['MAXLIMIT']) $insertArr[$key]['MAXLIMIT'] = 0;
			
			$insertArr[$key]['CREATED'] = new Zend_Db_Expr('now()');
			$insertArr[$key]['CREATEDY']   = $actor;
			$insertArr[$key]['UPDATED'] = new Zend_Db_Expr('now()');
			$insertArr[$key]['UPDATEDBY']   = $actor;
			$insertArr[$key]['MAKERLIMIT_STATUS']   = 1;
			// print_r($insertArr[$key]);
			try {
			// $del = $this->dbObj->delete('M_MAKERLIMIT',$where);
				$userLimitInsert[] = $this->dbObj->insert('M_MAKERLIMIT',$insertArr[$key]);
			} catch (Exception $e) {
				// print_r($e);
			}
			
		}
		// die;
		if(!(boolean)$userLimitInsert) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User Limit)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges){
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(User Limit)';
			return false;
		}
		
		return true;
	}
	
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	
		//query from TEMP_CUSTOMER
		$customerAcct = $this->dbObj->select()
						   		 ->from('TEMP_MAKERLIMIT')
						  		 ->where('CHANGES_ID = ?',$this->_changeId)
						  	     ->query()
						  	     ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customerAcct))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
        }
						  
		$updateArr = array('ACCT_STATUS' => 1);
		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('CUST_ID = ?'=>(string)$customerAcct['CUST_ID'],
		                  'ACCT_NO = ?'=>(string)$customerAcct['ACCT_NO']);
		
		$customerupdate = $this->dbObj->update('M_MAKERLIMIT_ACCT',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}

		/*
		//update all users status
		$updateArr = array('USER_STATUS' => 'A');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		*/
		
		$deleteChanges  = $this->deleteActivate();
		if(!$deleteChanges){
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(User Limit)';
			return false;
		}
		
		return true;
	}
	
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	
		//query from TEMP_CUSTOMER
		$customerAcct = $this->dbObj->select()
						  ->from('TEMP_MAKERLIMIT')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customerAcct ))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User Limit)';
			return false;
        }
		
        //update customer account status		  
		$updateArr = array('ACCT_STATUS' => 2);
		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('CUST_ID = ?'=>(string)$customerAcct['CUST_ID'],
		                  'ACCT_NO = ?'=>(string)$customerAcct['ACCT_NO']);
		
		$customerupdate = $this->dbObj->update('M_MAKERLIMIT_ACCT',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(User Limit)';
			return false;
		}
		
		
		
		$deleteChanges  = $this->deleteDeactivate();
		if(!$deleteChanges){
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(User Limit)';
			return false;
		}
		
		return true;
	}
	
	public function approveDelete() 
	{
		//query from TEMP_CUSTOMER
		$dailylimit = $this->dbObj->select()
						   		  ->from('TEMP_MAKERLIMIT')
						  		  ->where('CHANGES_ID = ?',$this->_changeId)
						  	      ->query()
						  	      ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($dailylimit))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User Limit)';
			return false;
        }
						  
		$updateArr = array('DAILYLIMIT_STATUS' => 3);
		
		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('CUST_ID = ?'     => (string)$dailylimit['CUST_ID'],
		                  'USER_LOGIN = ?'  => (string)$dailylimit['USER_LOGIN'],
		                  'CCY_ID  = ?'     => (string)$dailylimit['CCY_ID']);
		
		$customerupdate = $this->dbObj->update('M_MAKERLIMIT',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User Limit)';
			return false;
		}

		
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges){
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(User Limit)';
		    return false;
		}
			    
		
		return true;
	}
	
	
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() 
	{

		//delete from TEMP_CUSTOMER
		//$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		//delete from TEMP_USER
		//$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		//delete from TEMP_MAKERLIMIT
		$acctdelete = $this->dbObj->delete('TEMP_MAKERLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$acctdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Account)';
//			return false;
//		}

		//delete from TEMP_CUSTOMER_LIMIT
		//$limitdelete = $this->dbObj->delete('TEMP_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$limitdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Limit)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_CUSTOMER
		//$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		//delete from TEMP_MAKERLIMIT
		$acctdelete = $this->dbObj->delete('TEMP_MAKERLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$acctdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Account)';
//			return false;
//		}

		//delete from TEMP_CUSTOMER_LIMIT
		//$limitdelete = $this->dbObj->delete('TEMP_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$limitdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Limit)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_MAKERLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_MAKERLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteDelete() 
	{
		$customerdelete = $this->dbObj->delete('TEMP_MAKERLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
		
		
	}
}