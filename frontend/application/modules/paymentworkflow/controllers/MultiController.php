<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'CMD/Validate/ValidatePaymentOpen.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'Crypt/AESMYSQL.php';
class paymentworkflow_MultiController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function rejectAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$hasPriviRejectPayment = $this->view->hasPrivilege('PRJT');
		// echo "<pre>";
		// print_r($this->_request->getParams());
		// Filter Param
    	$filterArr  = array( 'mPayReff'  		=> new Application_Filtering("PS_NUMBER"));

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	     $password = $sessionNamespace->token; 
	     $this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$mpay 			= $this->_getParam('mPayReff');
		$npayreff = array();
		foreach ($mpay as $key => $value) {
			$PS_NUMBER 			= urldecode($value);
			$payreff = $AESMYSQL->decrypt($PS_NUMBER, $password);
			$npayreff['mPayReff'][$key] = $payreff;
		}
		


		$zf_filter  = new Zend_Filter_Input($filterArr, array(), $npayreff);
		$payReffArr = $npayreff['mPayReff'];

		$process 	= $this->_getParam('process');
		$reject 	= $this->_getParam('reject');
		$reason 	= $this->_getParam('PS_REASON');
		$status 	= $this->_getParam('status');

		$fromPage 	= $this->_getParam('from');
		
		$isWaitingApproval = ($fromPage == "wa") ? true: false;
		$paymentStatus 	   = ($isWaitingApproval)? $this->_paymentstatus["code"]["waitingforapproval"]:
												   $this->_paymentstatus["code"]["waitingtorelease"];

		if ($isWaitingApproval)
		{
			$sessionURL = new Zend_Session_Namespace('URL_CP_WA');
			$this->view->backURL = (!empty($sessionURL->URL)) ?
										   $sessionURL->URL : '/'.$this->view->modulename.'/waitingforapproval/index';
		}
		else
		{
			$sessionURL = new Zend_Session_Namespace('URL_CP_WR');
			$this->view->backURL = (!empty($sessionURL->URL)) ?
										   $sessionURL->URL : '/'.$this->view->modulename.'/waitingrelease/index';
		}

		if (empty($payReffArr))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error: Please select payment ref#').".");
			$this->_redirect($this->view->backURL);
		}

		////////////////////////////////////////

		$paramPayment = array("WA" 				=> $isWaitingApproval,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		// print_r($payReffArr);die();

		$pstatus = array('17','1','2');
		$select->where('P.PS_STATUS in (?)', $pstatus);
		$select->where("P.PS_NUMBER IN (?)", $payReffArr);
		// echo $select;
		$dataSQL = $this->_db->fetchAll($select);

		// Reject Payment
		if ($reject)
		{
			foreach($dataSQL as $data)
			{
				$PS_NUMBER = $data["payReff"];
				$Payment   = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
				$Payment->rejectPayment($reason);
				$Payment->__destruct();
				unset($Payment);
			}

			$ns = new Zend_Session_Namespace('FVC');
	    	$ns->backURL = $this->view->backURL;
			$this->_redirect('/notification/success/index');
		}

		$headers = array("payReff" 		=> $this->language->_('Payment Ref# - Subject'),
						 // "paySubj" 		=> $this->language->_('Subject'),
						 "created" 		=> $this->language->_('Created Date'),
						 "efdate" 		=> $this->language->_('Payment Date'),
						 "accsrc" 		=> $this->language->_('Source Account'),
						 // "accsrc_name" 	=> $this->language->_('Source Account Name'),
						 "acbenef" 		=> $this->language->_('Beneficiary Account'),
						 // "acbenef_name" => $this->language->_('Beneficiary Name'),
						 "numtrx" 		=> $this->language->_('#Trans.'),
						 // "ccy" 			=> $this->language->_('CCY'),
						 "amount" 		=> $this->language->_('Amount'),
						 "payType" 		=> $this->language->_('Payment Type'),
						);

		// arrange data for viewing in html, csv, pdf
		$data = array();
		foreach ($dataSQL as $d => $dt)
		{
			foreach ($headers as $key => $field)
			{
				$value = $dt[$key];

				if ($key == "amount")
				{	$value = $dt['ccy'].' '.Application_Helper_General::displayMoney($value);	}
				elseif ($key == "created")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);			}
				elseif ($key == "efdate")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);					}
				elseif ($key == "accsrc" && $value != "-")
				{	
					
					if (empty($dt["accsrc_alias"]) || $dt["accsrc_alias"] == '-') {
						$value = $dt['accsrc'] . ' (' .$dt['accsrc_ccy']  . ')' . " / ".$dt["SOURCE_ACCOUNT_BANK"]." - " . $dt['accsrc_name'] . " (S)";
					} else {

						$value = $dt['accsrc'] . ' (' .$dt['accsrc_ccy']  . ')' . " - ".$dt["SOURCE_ACCOUNT_BANK"]." - " . $dt['accsrc_alias'] . " (A)";
					}

				}elseif ($key == "acbenef" && $value != "-")
				{	
						
					if (empty($dt["acbenef_alias"]) || $dt["acbenef_alias"] == '-') {
						$value = $dt['acbenef'] . ' (' . $dt['acbenef_ccy'] . ')' .  " / ".$dt["BENEFICIARY_ACCOUNT_BANK"] ." - " . $dt['acbenef_name'] . " (S)";
					} else {
						$value = $dt['acbenef'] . ' (' . $dt['acbenef_ccy'] . ')' .  " / ".$dt["BENEFICIARY_ACCOUNT_BANK"] ." - " . $dt['acbenef_alias'] . " (A)";
					}

				}
				elseif ($key == "payReff") {
					if(empty($dt["paySubj"])){
						$value = $dt['payReff']." - no subject";
					}
					else{
						$value = $dt['payReff']." - ".$dt['paySubj'];
					}
					// $value = $dt['payReff']."<br> - ".$dt['paySubj']." (A)";
				}else if ($key == 'payType' && $value != '-') {

					$dt['TRANSFER_TYPE1'] = $trfType[$dt['TRANSFER_TYPE']];

                    $tra_type	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");

                    $tra_type1 = $tra_type[$dt['TRANSFER_TYPE']];
                    
                    if ($dt['PS_TYPE'] == '19') {
                         $payType = 'CP Same Bank Remains';
                    }else if ($dt['PS_TYPE'] == '20') {
                         $payType = 'CP Same Bank Maintains';
                    }else if ($dt['PS_TYPE'] == '23') {
                         $payType = 'CP Others Remains - '.$tra_type1;
                    }else if ($dt['PS_TYPE'] == '21') {
                         $payType = 'MM - '.$tra_type1;
                    }else {
                        $payType = $dt['payType'] . ' - ' . $dt['TRANSFER_TYPE1'];
                    }

                    $value = $payType;
				}

				$value = ($value == "")? "&nbsp;": $value;

				$data[$d][$key] = $value;
			}
		}
		
		$this->view->data 			= $data;
		$this->view->headers 		= $headers;
		$this->view->from			= $fromPage;
		$this->view->allowReject 	= $hasPriviRejectPayment;
		$this->view->status 		= $status;
	}

	public function approveAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$hasPriviApprovePayment = $this->view->hasPrivilege('PAPV');
		$hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');

		if ($hasPriviApprovePayment == false)
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to approve payment.");
			$this->_redirect('/paymentworkflow/waitingforapproval/index');
		}

		// Filter Param
    	$filterArr  = array( 'mPayReff'  		=> new Application_Filtering("PS_NUMBER"));
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	     $password = $sessionNamespace->token; 
	     $this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$mpay 			= $this->_getParam('mPayReff');
		$npayreff = array();
		foreach ($mpay as $key => $value) {
			$PS_NUMBER 			= urldecode($value);
			$payreff = $AESMYSQL->decrypt($PS_NUMBER, $password);
			$npayreff['mPayReff'][$key] = $payreff;
		}
		


		$zf_filter  = new Zend_Filter_Input($filterArr, array(), $npayreff);
		$payReffArr = $npayreff['mPayReff'];
		$process 	= $this->_getParam('process');
		$approve 	= $this->_getParam('approve');

		if (empty($payReffArr))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			//$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Please select payment ref# .");
			$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error: Please select payment ref#').".");
			$this->_redirect('/paymentworkflow/waitingforapproval/index');
		}

		////////////////////////////////////////

		$paramPayment = array("WA" 				=> true,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);

		$select->where('P.PS_STATUS = ?', (string)$this->_paymentstatus["code"]["waitingforapproval"]);
		$select->where("P.PS_NUMBER IN (?)", $payReffArr);

		$dataSQL = $this->_db->fetchAll($select);


		// Start Validasi multiple

		Zend_Registry::set('MAKER_LIMIT' 	 , array());

		$valErrorMsg 	= array();
		$boundary	 	= array();
		$ACCTSRC_arr 	= array();	// array for accumulative accsrc amount (multiple payment)

		foreach($dataSQL as $ps => $pslip)
		{
			$PS_NUMBER  = $pslip["payReff"];

			$selectTrx	= $this->_db->select()
									->from(	array(	'TT' => 'T_TRANSACTION'),
											array(	'ACCTSRC'			=> 'TT.SOURCE_ACCOUNT',
													'ACCTSRC_CCY'		=> 'TT.SOURCE_ACCOUNT_CCY',
													'ACBENEF_ID'		=> 'TT.BENEFICIARY_ID',
													'ACBENEF_CCY'		=> 'TT.BENEFICIARY_ACCOUNT_CCY',
													'TRA_AMOUNT'		=> 'TT.TRA_AMOUNT',
												 )
											)
									->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$pslipTrx = $this->_db->fetchAll($selectTrx);

			$ACCTSRCthis_arr = array();		// array for accumulative accsrc amount (1 payment only)
			$ACBENEF_IDarr 	 = array();
			foreach ($pslipTrx as $p => $pTrx)
			{
				// Create array acctsrc & acbenef for validation
				if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]]))
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
				}
				else
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
				}

				$ACCTSRCthis_arr[$pTrx["ACCTSRC"]] = $ACCTSRC_arr[$pTrx["ACCTSRC"]];

				if (!empty($pTrx["ACBENEF_ID"]))
				{
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}
			}

			$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
			$paramApprove = array("FROM" 				=> "D",						// D: by Detail, M: by Multiple
								  "PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
								  "PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
								  "ACCTSRCarr" 			=> $ACCTSRCthis_arr,
								  "ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
								  "_approveBeneficiary" => $hasPriviApproveBene,
								  "_approvePayment" 	=> $hasPriviApprovePayment,
								  );
			$validate->checkApprove($paramApprove);

			if ($validate->isError())
			{
				$valErrorMsg[$ps] = $validate->getErrorMsg();
			}

			$boundary[$ps] = $validate->getValidateInfo();

			$validate->__destruct();
			unset($validate);
		}
		// End Validasi multiple

		$sessionURL = new Zend_Session_Namespace('URL_CP_WA');
		$this->view->backURL = (!empty($sessionURL->URL)) ?
									   $sessionURL->URL : '/'.$this->view->modulename.'/waitingforapproval/index';

		// Approve Payment after validation
		if ($approve && empty($valErrorMsg))
		{
			foreach($dataSQL as $data)
			{
				$PS_NUMBER = $data["payReff"];
				$Payment   = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
				$Payment->approvePayment();
				$Payment->__destruct();
				unset($Payment);
			}

			$ns = new Zend_Session_Namespace('FVC');
	    	$ns->backURL = $this->view->backURL;
			$this->_redirect('/notification/success/index');
		}

//		$headers = array("payReff" 		=> "Payment Ref#",
//						 "paySubj" 		=> "Subject",
//						 "created" 		=> "Created Date",
//						 "efdate" 		=> "Payment Date",
//						 "accsrc" 		=> "Source Account",
//						 "accsrc_name" 	=> "Source Account Name / Alias",
//						 "acbenef" 		=> "Beneficiary Account",
//						 "acbenef_name" => "Beneficiary Name / Alias",
//						 "numtrx" 		=> "#Trans.",
//						 "ccy" 			=> "CCY",
//						 "amount" 		=> "Amount",
//						 "payType" 		=> "Payment Type",
//						);

		$headers = array("payReff" 		=> $this->language->_('Payment Ref#'),
						 "paySubj" 		=> $this->language->_('Subject'),
						 "created" 		=> $this->language->_('Created Date'),
						 "efdate" 		=> $this->language->_('Payment Date'),
						 "accsrc" 		=> $this->language->_('Source Account'),
						 // "accsrc_name" 	=> $this->language->_('Source Account Name'),
						 "acbenef" 		=> $this->language->_('Beneficiary Account'),
						 // "acbenef_name" => $this->language->_('Beneficiary Name'),
						 "numtrx" 		=> $this->language->_('#Trans.'),
						 // "ccy" 			=> $this->language->_('CCY'),
						 "amount" 		=> $this->language->_('Amount'),
						 "payType" 		=> $this->language->_('Payment Type'),
						);

		// arrange data for viewing in html, csv, pdf
		$data = array();
		foreach ($dataSQL as $d => $dt)
		{
			foreach ($headers as $key => $field)
			{
				$value = $dt[$key];

				if ($key == "amount")
				{	$value = $dt['ccy'].' '.Application_Helper_General::displayMoney($value);		}
				elseif ($key == "created")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);			}
				elseif ($key == "efdate")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);					}
				elseif ($key == "accsrc" && $value != "-")
				{
					if(empty($dt["acct_alias"])){
						$value = $dt['accsrc']."<br> - ".$dt['accsrc_name']." (S)";
					}
					else{
						$value = $dt['accsrc']."<br> - ".$dt['accsrc_alias']." (A)";
					}
				}elseif ($key == "acbenef" && $value != "-"){
					if(empty($dt["acbenef_alias"])){
						$value = $dt['acbenef']."<br> - ".$dt['acbenef_name']." (S)";
					}
					else{
						$value = $dt['acbenef']."<br> - ".$dt['acbenef_alias']." (A)";
					}
				}
				$value = ($value == "")? "&nbsp;": $value;

				$data[$d][$key] = $value;
			}
		}

		$this->view->data 			= $data;
		$this->view->headers 		= $headers;
		$this->view->valErrorMsg 	= $valErrorMsg;
		$this->view->boundary 		= $boundary;
		$this->view->allowApprove 	= (empty($valErrorMsg))? true: false;

		$sessionNamespace = new Zend_Session_Namespace('confirmMultiApprove');
		$sessionNamespace->boundary = $boundary;
		$sessionNamespace->errorMsg = $valErrorMsg;

	}

	public function releaseAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		require_once 'Service/Token.php';
		$hasPriviReleasePayment = $this->view->hasPrivilege('PRLP');

		if ($hasPriviReleasePayment == false)
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to release payment.");
			$this->_redirect('/'.$this->view->modulename.'/waitingrelease/index');
		}

		// Filter Param
    	$filterArr  = array(
    		'mPayReff'  		=> new Application_Filtering("PS_NUMBER"),
    		'userOnBehalf'  	=> new Application_Filtering("SELECTION"),
    		'challengeCode'  	=> new Application_Filtering("SELECTION"),
    		'responseCode'  	=> new Application_Filtering("SELECTION"),
		);



		$zf_filter  	= new Zend_Filter_Input($filterArr, array(), $this->getRequest()->getParams());
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	     $password = $sessionNamespace->token; 
	     $this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$mpay 			= $this->_getParam('mPayReff');
		$npayreff = array();
		foreach ($mpay as $key => $value) {
			$PS_NUMBER 			= urldecode($value);
			$payreff = $AESMYSQL->decrypt($PS_NUMBER, $password);
			$npayreff['mPayReff'][$key] = $payreff;
		}
		
		
		
		


		// $zf_filter  = new Zend_Filter_Input($filterArr, array(), $npayreff);
		$payReffArr = $npayreff['mPayReff'];
		// $payReffArr 	= $zf_filter->getEscaped('mPayReff');
//		$userOnBehalf 	= $zf_filter->getEscaped('userOnBehalf');
		$userOnBehalf 	= $this->_getParam('userOnBehalf');
		$challengeCode 	= $zf_filter->getEscaped('challengeCode');
		$responseCode 	= $zf_filter->getEscaped('responseCode');
		$step 			= $this->_getParam('step', 1);

		$selectCheckToken = $this->_db->select()
								->from(array('M_CUSTOMER'), array(
								   		 'CUST_RLS_TOKEN'
								   		))
								->where("CUST_ID = ?", (string)$this->_custIdLogin);

		$checkToken = $this->_db->fetchAll($selectCheckToken);

		$this->view->checkToken = $checkToken[0]["CUST_RLS_TOKEN"];
		
		
		$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
	//	 echo $selectQuery;
		$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

		 //var_dump($usergoogleAuth);die;
		if (!empty($usergoogleAuth)) {
			$this->view->googleauth = true;
			//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
			$settingObj = new Settings();
			$maxtoken = $settingObj->getSetting("max_failed_token");
			$this->view->tokenfail = (int)$maxtoken-1;
			if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
				//die;
				$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
				
				
				
				$tokenfail = (int)$maxtoken - (int)$usergoogleAuth['0']['USER_FAILEDTOKEN'];
				$this->view->tokenfail = $tokenfail;
			}
			$step 			= $this->_getParam('step', 3);
		}
		else{
			$this->view->nogoauth = '1';
			$step 			= $this->_getParam('step', 3);
		}

		

		$this->view->googleauth = true;

		$process 	= $this->_getParam('process');
		$release 	= ($this->_request->isPost() && $process == "release")? true: false;
	//var_dump($step);die;
		  //var_dump($release);die; 
		  //var_dump($payReffArr);die;
		if (empty($payReffArr))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			//$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Please select payment ref# .");
			$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error: Please select payment ref#').".");
			$this->_redirect('/'.$this->view->modulename.'/waitingrelease/index');
		}

		////////////////////////////////////////

		$paramPayment = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);

		//$select->where('P.PS_STATUS = ?', (string)$this->_paymentstatus["code"]["waitingtorelease"]);
		$select->where("P.PS_NUMBER IN (?)", $payReffArr);

		$dataSQL = $this->_db->fetchAll($select);

		// Start Validasi multiple
//		echo "<pre>";
		$userOnBehalfList =  $CustUser->getUserOnBehalf();
		$error 			= false;		// var error all payment
		$errorMsg 		= array();		// var error message all payment
		$valErrorMsg 	= array();		// var error each payment
		$valErrorSuspect 	= array();		// var error each payment
		$sumAmtACCTSRC	= array();		// array for accumulative accsrc amount (multiple payment)
		$sumChgACCTSRC	= array();		// array for accumulative charges amount (multiple payment)
		$warningMsg		= array();
		$adjustDate		= array();

		$settingObj = new Settings();
		$setting = array(
			"COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
			"COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
			"COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
			"COT_REMIT" 			=> $settingObj->getSetting("cut_off_time_remittance", 	"00:00:00"),
			"range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
			"auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
			"_dateFormat" 			=> $this->_dateDisplayFormat,
			"_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
			"_transfertype" 		=> array_flip($this->_transfertype["code"]),
		);

		if ($hasPriviReleasePayment == false)
		{	$error = true;	$errorMsg[] = "sorry, you don't have privilege to release payment";		}

		// if no releaser on behalf available, can't release
		if (count($userOnBehalfList) < 1)
		{	$error = true;	$errorMsg[] = "no releaser available";		}
		
		foreach($dataSQL as $ps => $pslip)
		{
			$PS_NUMBER  = $pslip["payReff"];

			if ($pslip["PS_STATUS"] != $this->_paymentstatus["code"]["waitingtorelease"])
			{
				$error = true;
				$valErrorMsg[$ps] = $this->language->_('Payment Status has changed')."";
				continue;
			}


			$selectTrx	= $this->_db->select()
									->from(	array(	'TT' => 'T_TRANSACTION'),
											array(	'ACCTSRC'			=> 'TT.SOURCE_ACCOUNT',
													'ACCTSRC_CCY'		=> 'TT.SOURCE_ACCOUNT_CCY',
													'ACBENEF_ID'		=> 'TT.BENEFICIARY_ID',
													'ACBENEF_CCY'		=> 'TT.BENEFICIARY_ACCOUNT_CCY',
													'TRA_AMOUNT'		=> 'TT.TRA_AMOUNT',
												 )
											)
									->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$pslipTrx = $this->_db->fetchAll($selectTrx);

			

			$ACCTSRCthis_arr = array();		// array for accumulative accsrc amount (1 payment only)
			$ACBENEF_IDarr 	 = array();
			foreach ($pslipTrx as $p => $pTrx)
			{
				// Create array acctsrc & acbenef for validation
				if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]]))
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
				}
				else
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
				}

				$ACCTSRCthis_arr[$pTrx["ACCTSRC"]] = $ACCTSRC_arr[$pTrx["ACCTSRC"]];

				if (!empty($pTrx["ACBENEF_ID"]))
				{
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}
			}

			$hasPriviApprovePayment = $this->view->hasPrivilege('PAPV');
			$hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');

			$validateapp  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
			$paramApprove = array("FROM" 				=> "D",						// D: by Detail, M: by Multiple
								  "PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
								  "PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
								  "ACCTSRCarr" 			=> $ACCTSRCthis_arr,
								  "ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
								  "_approveBeneficiary" => $hasPriviApproveBene,
								  "_approvePayment" 	=> $hasPriviApprovePayment,
								  );
			$validateapp->checkApprove($paramApprove);

			
			//echo '<pre>';
			//var_dump($pslip);die;
			if($pslip['PS_TYPE'] == 21 || $pslip['PS_TYPE'] == 19 || $pslip['PS_TYPE'] == 20){
				
				// Get Transaction
				$selectTrx = $this->_db->select()
									   ->from(	array(	'TT' => 'T_TRANSACTION'),
												array(	'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
														'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
														'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
														'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
														'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
														'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
														'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
														'CLR_CODE'				=> 'TT.CLR_CODE',
													  )
												)
										->where('TT.PS_NUMBER = ?', $PS_NUMBER);

				$paramTrxArr = $this->_db->fetchAll($selectTrx);
 
				// Start check payment for release
				$paramPayment = array_merge($pslip, $setting);
				$validate  	  = new ValidatePaymentOpen($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
				$validate->setSumAmtACCTSRC($sumAmtACCTSRC);		// utk akumulasi total amount accsrc (check balance)
				$validate->setSumChgACCTSRC($sumChgACCTSRC);		// utk akumulasi total charges accsrc (check balance)
				$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr, $step);

				
			}else{
			// Get Transaction
			$selectTrx = $this->_db->select()
								   ->from(	array(	'TT' => 'T_TRANSACTION'),
											array(	'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
													'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
													'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
													'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
													'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
													'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
													'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
													'CLR_CODE'				=> 'TT.CLR_CODE',
												  )
											)
									->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$paramTrxArr = $this->_db->fetchAll($selectTrx);

			// Start check payment for release
			$paramPayment = array_merge($pslip, $setting);
			$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
			$validate->setSumAmtACCTSRC($sumAmtACCTSRC);		// utk akumulasi total amount accsrc (check balance)
			$validate->setSumChgACCTSRC($sumChgACCTSRC);		// utk akumulasi total charges accsrc (check balance)
			$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr, $step);
			}

			
			if($validate->isError() === true || $validateapp->isError() === true)
			{
				
				//die;
				$error = true;
				$valErrorMsg[$ps] = $check["errorMsg"];
				$valErrorMsg[$ps] = $validateapp->getErrorMsg();
				if(empty($validateapp->getErrorMsg())){
				$valErrorMsg[$ps] = $validate->getErrorMsg();
				}
				//var_dump($valErrorMsg);
			}
		
			$paymentInfo 	= $validate->getPaymentInfo();
			$sumAmtACCTSRC  = $paymentInfo["sumAmtACCTSRC"];
			$sumChgACCTSRC  = $paymentInfo["sumChgACCTSRC"];

			// view adjusted payment date
			if (!empty($check["adjustDateMsg"]) && $step == 1)
			{	$warningMsg[] = "Payment Ref# ($PS_NUMBER) have backdated transaction. (Payment date will be adjusted)";		}
			elseif (!empty($check["adjustDateMsg"]) && $step == 2)
			{	$warningMsg[] = "Payment Ref# ($PS_NUMBER) Date already adjusted";		}

			if (!empty($check["adjustDateMsg"]))
			{	$adjustDate[$ps] = $check["adjustDate"];	}

			//print_r($paymentInfo);
//			print_r($check);
//			echo "<br><br>";

			$validate->__destruct();
			unset($validate);
		}
		// End Validasi multiple

		if($checkToken[0]["CUST_RLS_TOKEN"] == '1'){
		//get Token ID User
		$tokenIdUser = NULL;
		if (isset($userOnBehalf) && !(empty($userOnBehalf))){
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID')
				)
				->where('USER_ID = ?',$userOnBehalf)
				->where('CUST_ID = ?',$this->_custIdLogin)
				->limit(1)
			;

			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];
		}
		
		//var_dump($release);
		//var_dump($step);
		//var_dump($error);//die;
		// generate challenge code
		if($release && $step == 2 && $error === false)
		{
// 			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $this->_tokenIdLogin);
			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
		}

		// verify token
		if($release && $step == 3 && $error === false)
		{
// 			$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $this->_tokenIdLogin);
			$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$verToken 	= $Token->verify($challengeCode, $responseCode);

//			echo "<pre>";print_r($verToken);
			if ($verToken['ResponseCode'] != '00')
			{
				$tokenFailed = $CustUser->setLogToken(); //log token activity

				$error = true;
				$errorMsg[] = "Invalid Response Code";	//$verToken['ResponseDesc'];

				if ($tokenFailed === true)
				{
					$this->_redirect('/default/index/logout');
				}
			}
			else
			{
				/*$paymentData = array('PS_RELEASER_CHALLENGE' => $challengeCode);

				$whereData 	 = array('PS_NUMBER in (?)' 	 => $payReffArr,
									 'CUST_ID = ?'	 		 => (string) $this->_custIdLogin);

				$this->_db->update('T_PSLIP', $paymentData, $whereData);*/
			}
//			print_r($errorMsg);
//			die();
		}
		  
	// verify token
		if($release && $step == 4 && $error === false)
		{
			
			//die('hge'); 
			
			if (!empty($usergoogleAuth)) {

					$pga = new PHPGangsta_GoogleAuthenticator();
	//				 var_dump($usergoogleAuth['0']['GOOGLE_CODE']);
//					 var_dump($responseCode);die;
					 		$inputtoken1 		= $this->_getParam('inputtoken1');
							$inputtoken2 		= $this->_getParam('inputtoken2');
							$inputtoken3 		= $this->_getParam('inputtoken3');
							$inputtoken4 		= $this->_getParam('inputtoken4');
							$inputtoken5 		= $this->_getParam('inputtoken5');
							$inputtoken6 		= $this->_getParam('inputtoken6');

							$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

		//			var_dump($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, 2));die;
							$setting 		= new Settings();
							$google_duration 	= $setting->getSetting('google_duration');
					 if ($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, $google_duration)) {
					 	$resultToken = $resHard['ResponseCode'] == '0000';
					 } else {
					 	$tokenFailed = $CustUser->setLogToken(); //log token activity
						$step = 3; 
					 	$error = true;
					 	$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
					 	$this->view->popauth = true;
					 	if ($tokenFailed === true) {
					 		$this->_redirect('/default/index/logout');
					 	}
					 }
					
					//$resultToken = $resHard['ResponseCode'] == '0000';
				} else {
			
					$HardToken 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$resHard = $HardToken->validateOtp($responseCode);
					$resultToken = $resHard['ResponseCode'] = 'XT';

					if ($resHard['ResponseCode'] != '00') {
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$error = true;
						$errorMsg[] = "Google Auth hasn't been registered'";	//$verToken['ResponseDesc'];

						if ($tokenFailed === true) {
							$this->_redirect('/default/index/logout');
						}
					} else {
						$paymentData = array('PS_RELEASER_CHALLENGE' => $challengeCode);

						$whereData 	 = array(
							'PS_NUMBER = ?' => (string) $PS_NUMBER,
							'CUST_ID = ?'	 => (string) $this->_custIdLogin
						);
						
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$error = true;
						$errorMsg[] = "Google Auth hasn't been registered'";

						//$this->_db->update('T_PSLIP', $paymentData, $whereData);
					}
				}
			
			
			
			
//			print_r($errorMsg);
//			die();
		}

		// Release Payment
		if ($release && $step == 4 && $error === false)
		{
			//die('here');
			foreach($dataSQL as $ps => $data)
			{
				$PS_NUMBER = $data["payReff"];
				$Payment   = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
				$resultRelease = $Payment->releasePayment();
				//echo '<pre>';
				//var_dump($resultRelease);
				$Payment->__destruct();
				unset($Payment); 
				if($resultRelease['status'] == 'XT' ){
					//$this->view->errorsuspect = true;
					$valErrorSuspect[$ps] = "Error: ".$resultRelease['errorMessage']; //$resultRelease['status']." ".$resultRelease['errorMessage'];
					$this->view->error = true;
					$error = true;
					//$valErrorMsg[$ps] = "Error: ".$resultRelease['errorMessage']; //$resultRelease['status']." ".$resultRelease['errorMessage'];
				}else if ($resultRelease['status'] != '00' && $resultRelease['status'] != 'XT')
				{
					$this->view->error = true;
					
					$valErrorMsg[$ps] = "Error: ".$resultRelease['errorMessage']; //$resultRelease['status']." ".$resultRelease['errorMessage'];
					
					
				}
			}
			//echo '<pre>';
			//var_dump($valErrorMsg);
			//var_dump($valErrorSuspect);
			//die;
			 //die;
			if(empty($valErrorMsg) && empty($valErrorSuspect)){
					$ns = new Zend_Session_Namespace('FVC');
				  $ns->backURL = '/paymentworkflow/waitingrelease/index';
					$this->_redirect('/notification/success/index');
			}

			//$this->_redirect('/notification/success/index');
		}

		}else{
			$step = 4;
		}


		

		// View Data
//		$headers = array("payReff" 		=> "Payment Ref#",
//						 "paySubj" 		=> "Subject",
//						 "created" 		=> "Created Date",
//						 "efdate" 		=> "Payment Date",
//						 "accsrc" 		=> "Source Account",
//						 "accsrc_name" 	=> "Source Account Name / Alias",
//						 "acbenef" 		=> "Beneficiary Account",
//						 "acbenef_name" => "Beneficiary Name / Alias",
//						 "numtrx" 		=> "#Trans.",
//						 "ccy" 			=> "CCY",
//						 "amount" 		=> "Amount",
//						 "payType" 		=> "Payment Type",
//						);

		$headers = array("payReff" 		=> $this->language->_('Payment Ref# - Subject'),
						// "paySubj" 		=> $this->language->_('Subject'),
						 "created" 		=> $this->language->_('Created Date'),
						 "efdate" 		=> $this->language->_('Payment Date'),
						 "accsrc" 		=> $this->language->_('Source Account'),
					//	 "accsrc_name" 	=> $this->language->_('Source Account Name'),
						 "acbenef" 		=> $this->language->_('Beneficiary Account'),
					//	 "acbenef_name" => $this->language->_('Beneficiary Name'),
						 "numtrx" 		=> $this->language->_('#Trans.'),
						// "ccy" 			=> $this->language->_('CCY'),
						 "amount" 		=> $this->language->_('Amount'),
						 "payType" 		=> $this->language->_('Payment Type'),
						);

		// arrange data for viewing in html, csv, pdf
		$data = array();
		
		foreach ($dataSQL as $d => $dt)
		{
			foreach ($headers as $key => $field)
			{
				$value = $dt[$key];

				if ($key == "amount")
				{	$value = $dt['ccy'].' '.Application_Helper_General::displayMoney($value);		}
				elseif ($key == "created")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);			}
				elseif ($key == "efdate")
				{
					$value = (!empty($adjustDate[$d]) && $step == 2)? $adjustDate[$d]: $value;
					$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);
				}
				elseif (($key == "accsrc") && $value != "-")
				{	$value = $value." [".$dt[$key.'_ccy']."] - ".$dt['accsrc_name']." / ".$dt['accsrc_alias'] ;		}
				elseif (($key == "acbenef") && $value != "-")
				{	$value = $value." [".$dt[$key.'_ccy']."] - ".$dt['acbenef_name']." / ".$dt['acbenef_alias'];		}
				elseif($key == 'payReff'){
					$value = $value.' - '.$dt['paySubj'];
				}

				$value = ($value == "")? "&nbsp;": $value;

				$data[$d][$key] = $value;
			}
		}
		
	//	echo '<pre>';
	//		var_dump($step);
	//		var_dump($valErrorSuspect);
	//		var_dump($valErrorMsg);
	//		var_dump($error); 
	//		die;   
		$this->view->data 			= $data;
		$this->view->headers 		= $headers;
		$this->view->valErrorMsg 	= $valErrorMsg;
		$this->view->valErrorSuspect 	= $valErrorSuspect;
		
		$this->view->warningMsg 	= $warningMsg;
		//$this->view->allowRelease 	= !$error;

		$this->view->allowCheckBalance 	= $this->view->hasPrivilege('BAIQ');
		$this->view->userId 			= strtoupper($this->_userIdLogin);
		$this->view->userOnBehalfList	= $userOnBehalfList;
		$this->view->userOnBehalf		= $userOnBehalf;
		$this->view->challengeCode		= $challengeCode;
		$this->view->step 				= $step;
		$this->view->error 				= $error;
		$this->view->errorMsg 			= $errorMsg;
		//var_dump($this->view->error);
		$sessionNamespace = new Zend_Session_Namespace('confirmMultiRelease');
		$sessionNamespace->errorMsg = $valErrorMsg;
		$sessionNamespace->errorSuspect = $valErrorSuspect;

		$sessionURL = new Zend_Session_Namespace('URL_CP_WR');
		$this->view->backURL = (!empty($sessionURL->URL)) ?
									   $sessionURL->URL : '/'.$this->view->modulename.'/waitingrelease/index';

	}


	public function reviewAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$hasPriviApprovePayment = $this->view->hasPrivilege('PAPV');
		$hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');

		if ($hasPriviApprovePayment == false)
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to review payment.");
			$this->_redirect('/paymentworkflow/waitingforreview/index');
		}

		// Filter Param
    	$filterArr  = array( 'mPayReff'  		=> new Application_Filtering("PS_NUMBER"));
    	
    	 $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	     $password = $sessionNamespace->token; 
	     $this->view->token = $sessionNamespace->token;

	    $trfType = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		$AESMYSQL = new Crypt_AESMYSQL();
		$mpay 			= $this->_getParam('mPayReff');
		$npayreff = array();
		foreach ($mpay as $key => $value) {
			$PS_NUMBER 			= urldecode($value);
			$payreff = $AESMYSQL->decrypt($PS_NUMBER, $password);
			$npayreff['mPayReff'][$key] = $payreff;
		}
		


		$zf_filter  = new Zend_Filter_Input($filterArr, array(), $npayreff);
		$payReffArr = $npayreff['mPayReff'];
		$process 	= $this->_getParam('process');
		$approve 	= $this->_getParam('approve');
		//var_dump($payReffArr);
		if (empty($payReffArr))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			//$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Please select payment ref# .");
			$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error: Please select payment ref#').".");
			$this->_redirect('/paymentworkflow/waitingforreview/index');
		}

		////////////////////////////////////////

		$paramPayment = array("WA" 				=> true,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);

		$select->where('P.PS_STATUS = ?', (string)$this->_paymentstatus["code"]["waitingreviewer"]);
		$select->where("P.PS_NUMBER IN (?)", $payReffArr);

		$dataSQL = $this->_db->fetchAll($select);


		// Start Validasi multiple

		Zend_Registry::set('MAKER_LIMIT' 	 , array());

		$valErrorMsg 	= array();
		$boundary	 	= array();
		$ACCTSRC_arr 	= array();	// array for accumulative accsrc amount (multiple payment)

		// print_r($dataSQL);die;
		foreach($dataSQL as $ps => $pslip)
		{
			$PS_NUMBER  = $pslip["payReff"];

			$selectTrx	= $this->_db->select()
									->from(	array(	'TT' => 'T_TRANSACTION'),
											array(	'ACCTSRC'			=> 'TT.SOURCE_ACCOUNT',
													'ACCTSRC_CCY'		=> 'TT.SOURCE_ACCOUNT_CCY',
													'ACBENEF_ID'		=> 'TT.BENEFICIARY_ID',
													'ACBENEF_CCY'		=> 'TT.BENEFICIARY_ACCOUNT_CCY',
													'TRA_AMOUNT'		=> 'TT.TRA_AMOUNT',
												 )
											)
									->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$pslipTrx = $this->_db->fetchAll($selectTrx);

			$ACCTSRCthis_arr = array();		// array for accumulative accsrc amount (1 payment only)
			$ACBENEF_IDarr 	 = array();
			foreach ($pslipTrx as $p => $pTrx)
			{
				// Create array acctsrc & acbenef for validation
				if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]]))
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
				}
				else
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
				}

				$ACCTSRCthis_arr[$pTrx["ACCTSRC"]] = $ACCTSRC_arr[$pTrx["ACCTSRC"]];

				if (!empty($pTrx["ACBENEF_ID"]))
				{
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}
			}

			$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
			$paramApprove = array("FROM" 				=> "D",						// D: by Detail, M: by Multiple
								  "PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
								  "PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
								  "ACCTSRCarr" 			=> $ACCTSRCthis_arr,
								  "ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
								  "_approveBeneficiary" => $hasPriviApproveBene,
								  "_approvePayment" 	=> $hasPriviApprovePayment,
								  );
			$validate->checkApprove($paramApprove);

			if ($validate->isError())
			{
				$valErrorMsg[$ps] = $validate->getErrorMsg();
			}
			// print_r($valErrorMsg);die;
			$boundary[$ps] = $validate->getValidateInfo();

			$validate->__destruct();
			unset($validate);
		}
		// End Validasi multiple



		$sessionURL = new Zend_Session_Namespace('URL_CP_WA');
		$this->view->backURL = (!empty($sessionURL->URL)) ?
									   $sessionURL->URL : '/'.$this->view->modulename.'/waitingforreview/index';
		// print_r($this->getRequest()->getParams());
		// Approve Payment after validation
		if ($approve && empty($valErrorMsg))
		{
			foreach($dataSQL as $data)
			{
				$PS_NUMBER = $data["payReff"];
				$Payment   = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
				$Payment->reviewPayment();
				$Payment->__destruct();
				unset($Payment);
			}

			$ns = new Zend_Session_Namespace('FVC');
	    	$ns->backURL = $this->view->backURL;
			$this->_redirect('/notification/success/index');
		}

//		$headers = array("payReff" 		=> "Payment Ref#",
//						 "paySubj" 		=> "Subject",
//						 "created" 		=> "Created Date",
//						 "efdate" 		=> "Payment Date",
//						 "accsrc" 		=> "Source Account",
//						 "accsrc_name" 	=> "Source Account Name / Alias",
//						 "acbenef" 		=> "Beneficiary Account",
//						 "acbenef_name" => "Beneficiary Name / Alias",
//						 "numtrx" 		=> "#Trans.",
//						 "ccy" 			=> "CCY",
//						 "amount" 		=> "Amount",
//						 "payType" 		=> "Payment Type",
//						);

		$headers = array("payReffLabel" 		=> $this->language->_('Payment Ref# - Subject'),
						 // "paySubj" 		=> $this->language->_('Subject'),
						 "created" 		=> $this->language->_('Created Date'),
						 "efdate" 		=> $this->language->_('Payment Date'),
						 "accsrc" 		=> $this->language->_('Source Account'),
						 // "accsrc_name" 	=> $this->language->_('Source Account Name'),
						 "acbenef" 		=> $this->language->_('Beneficiary Account'),
						 // "acbenef_name" => $this->language->_('Beneficiary Name'),
						 "numtrx" 		=> $this->language->_('#Trans.'),
						 // "ccy" 			=> $this->language->_('CCY'),
						 "amount" 		=> $this->language->_('Amount'),
						 "payType" 		=> $this->language->_('Payment Type'),
						);

		// arrange data for viewing in html, csv, pdf
		$data = array();
		// print_r($dataSQL);die;
		foreach ($dataSQL as $d => $dt)
		{
			foreach ($headers as $key => $field)
			{
				$value = $dt[$key];

				if ($key == "amount")
				{	$value = $dt['ccy'].' '.Application_Helper_General::displayMoney($value);		}
				elseif ($key == "created")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);			}
				elseif ($key == "efdate")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);					}
				elseif ($key == "accsrc" && $value != "-")
				{	
					
					if (empty($dt["accsrc_alias"]) || $dt["accsrc_alias"] == '-') {
						$value = $dt['accsrc'] . ' (' .$dt['accsrc_ccy']  . ')' . " / ".$dt["SOURCE_ACCOUNT_BANK"]." - " . $dt['accsrc_name'] . " (S)";
					} else {

						$value = $dt['accsrc'] . ' (' .$dt['accsrc_ccy']  . ')' . " - ".$dt["SOURCE_ACCOUNT_BANK"]." - " . $dt['accsrc_alias'] . " (A)";
					}

				}elseif ($key == "acbenef" && $value != "-")
				{	
						
					if (empty($dt["acbenef_alias"]) || $dt["acbenef_alias"] == '-') {
						$value = $dt['acbenef'] . ' (' . $dt['acbenef_ccy'] . ')' .  " / ".$dt["BENEFICIARY_ACCOUNT_BANK"] ." - " . $dt['acbenef_name'] . " (S)";
					} else {
						$value = $dt['acbenef'] . ' (' . $dt['acbenef_ccy'] . ')' .  " / ".$dt["BENEFICIARY_ACCOUNT_BANK"] ." - " . $dt['acbenef_alias'] . " (A)";
					}

				}
				elseif ($key == "payReffLabel") {
					if(empty($dt["paySubj"])){
						$value = $dt['payReff']."<br> - no subject";
					}
					else{
						$value = $dt['payReff']."<br> - ".$dt['paySubj'];
					}
					$data[$d]['payReff'] = $dt['payReff'];
					// $value = $dt['payReff']."<br> - ".$dt['paySubj']." (A)";
				}else if ($key == 'payType' && $value != '-') {

					$dt['TRANSFER_TYPE1'] = $trfType[$dt['TRANSFER_TYPE']];

                    $tra_type	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");

                    $tra_type1 = $tra_type[$dt['TRANSFER_TYPE']];
                    
                    if ($dt['PS_TYPE'] == '19') {
                         $payType = 'CP Same Bank Remains';
                    }else if ($dt['PS_TYPE'] == '20') {
                         $payType = 'CP Same Bank Maintains';
                    }else if ($dt['PS_TYPE'] == '23') {
                         $payType = 'CP Others Remains - '.$tra_type1;
                    }else if ($dt['PS_TYPE'] == '21') {
                         $payType = 'MM - '.$tra_type1;
                    }else {
                        $payType = $dt['payType'] . ' - ' . $dt['TRANSFER_TYPE1'];
                    }

                    $value = $payType;
				}

				$value = ($value == "")? "&nbsp;": $value;
		
				$data[$d][$key] = $value;
			}
		}

		$this->view->data 			= $data;
		$this->view->headers 		= $headers;
		$this->view->valErrorMsg 	= $valErrorMsg;
		$this->view->boundary 		= $boundary;
		$this->view->allowApprove 	= (empty($valErrorMsg))? true: false;

		$sessionNamespace = new Zend_Session_Namespace('confirmMultiApprove');
		$sessionNamespace->boundary = $boundary;
		$sessionNamespace->errorMsg = $valErrorMsg;

	}



	public function approvefutureAction()
	{
		$hasPriviApprovePayment = $this->view->hasPrivilege('PAPV');
		$hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');

		if ($hasPriviApprovePayment == false)
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to approve payment.");
			$this->_redirect('/paymentworkflow/waitingforapproval/index');
		}

		// Filter Param
    	$filterArr  = array( 'mPayReff'  		=> new Application_Filtering("PS_NUMBER"));
		$zf_filter  = new Zend_Filter_Input($filterArr, array(), $this->getRequest()->getParams());
		$payReffArr = $zf_filter->getEscaped('mPayReff');
		$process 	= $this->_getParam('process');
		$approve 	= ($this->_request->isPost() && $process == "approve")? true: false;

		if (empty($payReffArr))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			//$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Please select payment ref# .");
			$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error: Please select payment ref#').".");
			$this->_redirect('/paymentworkflow/waitingforapprovalfuture/index');
		}

		////////////////////////////////////////

		$paramPayment = array("WA" 				=> true,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);

		$select->where('P.PS_STATUS = ?', (string)$this->_paymentstatus["code"]["requestcancelfuturedate"]);
		$select->where("P.PS_NUMBER IN (?)", $payReffArr);

		$dataSQL = $this->_db->fetchAll($select);


		// Start Validasi multiple

		Zend_Registry::set('MAKER_LIMIT' 	 , array());

		$valErrorMsg 	= array();
		$boundary	 	= array();
		$ACCTSRC_arr 	= array();	// array for accumulative accsrc amount (multiple payment)

		foreach($dataSQL as $ps => $pslip)
		{
			$PS_NUMBER  = $pslip["payReff"];

			$selectTrx	= $this->_db->select()
									->from(	array(	'TT' => 'T_TRANSACTION'),
											array(	'ACCTSRC'			=> 'TT.SOURCE_ACCOUNT',
													'ACCTSRC_CCY'		=> 'TT.SOURCE_ACCOUNT_CCY',
													'ACBENEF_ID'		=> 'TT.BENEFICIARY_ID',
													'ACBENEF_CCY'		=> 'TT.BENEFICIARY_ACCOUNT_CCY',
													'TRA_AMOUNT'		=> 'TT.TRA_AMOUNT',
												 )
											)
									->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$pslipTrx = $this->_db->fetchAll($selectTrx);

			$ACCTSRCthis_arr = array();		// array for accumulative accsrc amount (1 payment only)
			$ACBENEF_IDarr 	 = array();
			foreach ($pslipTrx as $p => $pTrx)
			{
				// Create array acctsrc & acbenef for validation
				if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]]))
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
				}
				else
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
				}

				$ACCTSRCthis_arr[$pTrx["ACCTSRC"]] = $ACCTSRC_arr[$pTrx["ACCTSRC"]];

				if (!empty($pTrx["ACBENEF_ID"]))
				{
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}
			}

			$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
			$paramApprove = array("FROM" 				=> "D",						// D: by Detail, M: by Multiple
								  "PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
								  "PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
								  "ACCTSRCarr" 			=> $ACCTSRCthis_arr,
								  "ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
								  "_approveBeneficiary" => $hasPriviApproveBene,
								  "_approvePayment" 	=> $hasPriviApprovePayment,
								  );
			$validate->checkApprove($paramApprove);

			if ($validate->isError())
			{
				$valErrorMsg[$ps] = $validate->getErrorMsg();
			}

			$boundary[$ps] = $validate->getValidateInfo();

			$validate->__destruct();
			unset($validate);
		}
		// End Validasi multiple

		$sessionURL = new Zend_Session_Namespace('URL_CP_WA');
		$this->view->backURL = (!empty($sessionURL->URL)) ?
									   $sessionURL->URL : '/'.$this->view->modulename.'/waitingforapprovalfuture/index';

		// Approve Payment after validation
		if ($approve && empty($valErrorMsg))
		{
			foreach($dataSQL as $data)
			{
				$PS_NUMBER = $data["payReff"];
				$data = array ('PS_STATUS' => '14');
				$where['PS_NUMBER = ?'] = $PS_NUMBER;
				$this->_db->update('T_PSLIP',$data,$where);

				$historyInsert = array(	'DATE_TIME' 		=> new Zend_Db_Expr("now()"),
											'PS_NUMBER' 		=> $PS_NUMBER,
											'CUST_ID' 			=> $this->_custIdLogin,
											'USER_LOGIN' 		=> $this->_userIdLogin,
											'HISTORY_STATUS' 	=> 18);

				$this->_db->insert('T_PSLIP_HISTORY',$historyInsert);

			}

			$ns = new Zend_Session_Namespace('FVC');
	    	$ns->backURL = $this->view->backURL;
			$this->_redirect('/notification/success/index');
		}

//		$headers = array("payReff" 		=> "Payment Ref#",
//						 "paySubj" 		=> "Subject",
//						 "created" 		=> "Created Date",
//						 "efdate" 		=> "Payment Date",
//						 "accsrc" 		=> "Source Account",
//						 "accsrc_name" 	=> "Source Account Name / Alias",
//						 "acbenef" 		=> "Beneficiary Account",
//						 "acbenef_name" => "Beneficiary Name / Alias",
//						 "numtrx" 		=> "#Trans.",
//						 "ccy" 			=> "CCY",
//						 "amount" 		=> "Amount",
//						 "payType" 		=> "Payment Type",
//						);

		$headers = array("payReff" 		=> $this->language->_('Payment Ref#'),
						 "paySubj" 		=> $this->language->_('Subject'),
						 "created" 		=> $this->language->_('Created Date'),
						 "efdate" 		=> $this->language->_('Payment Date'),
						 "accsrc" 		=> $this->language->_('Source Account'),
						 "accsrc_name" 	=> $this->language->_('Source Account Name'),
						 "acbenef" 		=> $this->language->_('Beneficiary Account'),
						 "acbenef_name" => $this->language->_('Beneficiary Name'),
						 "numtrx" 		=> $this->language->_('#Trans.'),
						 "ccy" 			=> $this->language->_('CCY'),
						 "amount" 		=> $this->language->_('Amount'),
						 "payType" 		=> $this->language->_('Payment Type'),
						);

		// arrange data for viewing in html, csv, pdf
		$data = array();
		foreach ($dataSQL as $d => $dt)
		{
			foreach ($headers as $key => $field)
			{
				$value = $dt[$key];

				if ($key == "amount")
				{	$value = Application_Helper_General::displayMoney($value);		}
				elseif ($key == "created")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);			}
				elseif ($key == "efdate")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);					}
				elseif (($key == "accsrc" || $key == "acbenef") && $value != "-")
				{	$value = $value." [".$dt[$key.'_ccy']."]";		}

				$value = ($value == "")? "&nbsp;": $value;

				$data[$d][$key] = $value;
			}
		}

		$this->view->data 			= $data;
		$this->view->headers 		= $headers;
		$this->view->valErrorMsg 	= $valErrorMsg;
		$this->view->boundary 		= $boundary;
		$this->view->allowApprove 	= (empty($valErrorMsg))? true: false;

		$sessionNamespace = new Zend_Session_Namespace('confirmMultiApprove');
		$sessionNamespace->boundary = $boundary;
		$sessionNamespace->errorMsg = $valErrorMsg;

	}

public function rejectfutureAction()
	{
		$hasPriviRejectPayment = $this->view->hasPrivilege('PRJT');

		// Filter Param
    	$filterArr  = array( 'mPayReff'  		=> new Application_Filtering("PS_NUMBER"));
		$zf_filter  = new Zend_Filter_Input($filterArr, array(), $this->getRequest()->getParams());
		$payReffArr = $zf_filter->getEscaped('mPayReff');

		$process 	= $this->_getParam('process');
		$reject 	= ($this->_request->isPost() && $process == "reject")? true: false;

		$fromPage 	= $this->_getParam('from');

		$isWaitingApproval = ($fromPage == "wa") ? true: false;
		$paymentStatus 	   = ($isWaitingApproval)? $this->_paymentstatus["code"]["requestcancelfuturedate"]:
												   $this->_paymentstatus["code"]["requestcancelfuturedate"];

		if ($isWaitingApproval)
		{
			$sessionURL = new Zend_Session_Namespace('URL_CP_WA');
			$this->view->backURL = (!empty($sessionURL->URL)) ?
										   $sessionURL->URL : '/'.$this->view->modulename.'/waitingforapprovalfuture/index';
		}
		

		if (empty($payReffArr))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error: Please select payment ref#').".");
			$this->_redirect($this->view->backURL);
		}

		////////////////////////////////////////

		$paramPayment = array("WA" 				=> $isWaitingApproval,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);

		$select->where('P.PS_STATUS = ?', (string) $paymentStatus);
		$select->where("P.PS_NUMBER IN (?)", $payReffArr);

		$dataSQL = $this->_db->fetchAll($select);

		// Reject Payment
		if ($reject)
		{
			foreach($dataSQL as $data)
			{
				$PS_NUMBER = $data["payReff"];
				$data = array ('PS_STATUS' => '7');
				$where['PS_NUMBER = ?'] = $PS_NUMBER;
				$this->_db->update('T_PSLIP',$data,$where);
		
				$historyInsert = array(	'DATE_TIME' 		=> new Zend_Db_Expr("now()"),
											'PS_NUMBER' 		=> $PS_NUMBER,
											'CUST_ID' 			=> $this->_custIdLogin,
											'USER_LOGIN' 		=> $this->_userIdLogin,
											'HISTORY_STATUS' 	=> 19);

				$this->_db->insert('T_PSLIP_HISTORY',$historyInsert);

				
			}

			$ns = new Zend_Session_Namespace('FVC');
	    	$ns->backURL = $this->view->backURL;
			$this->_redirect('/notification/success/index');
		}

		$headers = array("payReff" 		=> $this->language->_('Payment Ref#'),
						 "paySubj" 		=> $this->language->_('Subject'),
						 "created" 		=> $this->language->_('Created Date'),
						 "efdate" 		=> $this->language->_('Payment Date'),
						 "accsrc" 		=> $this->language->_('Source Account'),
						 "accsrc_name" 	=> $this->language->_('Source Account Name'),
						 "acbenef" 		=> $this->language->_('Beneficiary Account'),
						 "acbenef_name" => $this->language->_('Beneficiary Name'),
						 "numtrx" 		=> $this->language->_('#Trans.'),
						 "ccy" 			=> $this->language->_('CCY'),
						 "amount" 		=> $this->language->_('Amount'),
						 "payType" 		=> $this->language->_('Payment Type'),
						);

		// arrange data for viewing in html, csv, pdf
		$data = array();
		foreach ($dataSQL as $d => $dt)
		{
			foreach ($headers as $key => $field)
			{
				$value = $dt[$key];

				if ($key == "amount")
				{	$value = Application_Helper_General::displayMoney($value);		}
				elseif ($key == "created")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);			}
				elseif ($key == "efdate")
				{	$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);					}
				elseif (($key == "accsrc" || $key == "acbenef") && $value != "-")
				{	$value = $value." [".$dt[$key.'_ccy']."]";		}

				$value = ($value == "")? "&nbsp;": $value;

				$data[$d][$key] = $value;
			}
		}

		$this->view->data 			= $data;
		$this->view->headers 		= $headers;
		$this->view->from			= $fromPage;
		$this->view->allowReject 	= $hasPriviRejectPayment;
	}




}
