<?php
class printemailuser_Model_Printemailuser
{
	protected $_db;

	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getData($filterParam,$sortBy,$sortDir,$filter,$filter_clear)
	{
		$data = $this->_db->select()
			->from(
				array('A' => 'M_USER'),
				array(
					'USER_ID'			=> 'A.USER_ID',
					'USER_FULLNAME'		=> 'A.USER_FULLNAME',
					'USER_ISNEW'		=> new Zend_Db_Expr("(CASE A.USER_ISNEW
																WHEN '1' THEN 'Yes'
																WHEN '0' THEN 'No'
									  		 				END)"),
					'USER_ISEMAILPWD'	=> new Zend_Db_Expr("(CASE A.USER_ISEMAILPWD
																WHEN '1' THEN 'Email'
																ELSE 'Posted Mail'
									  		 				END)"),
					'USER_EMAIL'		=> 'A.USER_EMAIL'
				)
			)
			->where("USER_RCHANGE = 1")
			->where("USER_RRESET = 0")
			->where("USER_STATUS NOT LIKE '3'");

		if($filter == TRUE){
			if($filterParam['fUserId']){
				$data->where("A.USER_ID LIKE ".$this->_db->quote('%'.$filterParam['fUserId'].'%'));
			}

			if(!empty($filterParam['fUserNew'])){
				if($filterParam['fUserNew'] == 2){
					$data->where('A.USER_ISNEW = ?', 0);
				}else{
					$data->where('A.USER_ISNEW = ?', 1);
				}
			}

			if(!empty($filterParam['fUserMethod'])){
				if($filterParam['fUserMethod'] == 2){
					$data->where('A.USER_ISEMAILPWD = ?', 0);
				}else{
					$data->where('A.USER_ISEMAILPWD = ?', 1);
				}
			}
		}
		$data->order($sortBy.' '.$sortDir);
		return $this->_db->fetchAll($data);
	}

	public function getUser($userId)
	{
		$data = $this->_db->select()
			->from(array('M_USER'),array('*'))
			->where('USER_ID = ?', $userId);
		return $this->_db->fetchRow($data);
	}
}