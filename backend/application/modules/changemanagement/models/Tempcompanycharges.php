<?php

/**
 * Tempwelcome model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempcompanycharges extends Changemanagement_Model_Tempchanges
{

	protected $_moduleId = 'MAC';
	/**
	 * Approve Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */

	/**
	 * Approve Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveNew($actor = null)
	{

		//query from TEMP_SETTING
		$minamtData = $this->dbObj->fetchAll(
			$this->dbObj->select()
				->from('TEMP_CHARGES_REMITTANCE')
				->where('CHANGES_ID = ?', $this->_changeId)
		);


		$listSetting = $this->dbObj->select()
			->from('TEMP_SETTING')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetchAll(Zend_Db::FETCH_ASSOC);

		$cekChangesInformation = $this->dbObj->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $this->_changeId)
			->where('CHANGES_INFORMATION = ?', 'Cash Collateral')
			->query()
			->fetchAll();

		if (count($cekChangesInformation) > 0) {
			$getRecord = $this->dbObj->select()
				->from("TEMP_CHARGES_OTHER")
				->where("CHANGES_ID = ?", $this->_changeId)
				->query()
				->fetchAll();

			$getRecord = $getRecord[0];

			$dataArr = [
				"CUST_ID" => $getRecord["CUST_ID"],
				"CHARGES_TYPE" => $getRecord["CHARGES_TYPE"],
				"CHARGES_CCY" => $getRecord["CHARGES_CCY"],
				"CHARGES_PCT" => $getRecord["CHARGES_PCT"],
				"CHARGES_ADM" => $getRecord["CHARGES_ADM"],
				"CHARGES_STAMP" => $getRecord["CHARGES_STAMP"],
				"CHARGES_SUGGESTED" => $getRecord["CHARGES_SUGGESTED"],
				"CHARGES_SUGGESTEDBY" => $getRecord["CHARGES_SUGGESTEDBY"],
				"CHARGES_APPROVED" => new Zend_Db_Expr('now()'),
				"CHARGES_APPROVEDBY" => $actor
			];
			$this->dbObj->insert("M_CHARGES_OTHER", $dataArr);
		}
		/*if(!count($listSetting)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Setting)';
			return false;
        }*/
		//var_dump($listSetting);die;

		if (!empty($listSetting)) {
			foreach ($listSetting as $row) {
				try {
					$updateArr = array_diff_key($row, array('TEMP_ID' => '', 'CHANGES_ID' => '', 'SETTING_ID' => ''));
					$whereArr = array('SETTING_ID = ?' => (string) $row['SETTING_ID']);
					$userupdate = $this->dbObj->update('M_SETTING', $updateArr, $whereArr);
				} catch (Exception $err) {
					print_r($err);
					die;
				}
				echo '<pre>';
				print_r($userupdate);
			}
		}
		// die;
		//
		$userdelete = $this->dbObj->delete('TEMP_SETTING', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		if (!empty($minamtData)) {


			//         	$this->deleteMasterDaily();
			//         	print_r($minamtData);die;
			foreach ($minamtData as $minamt) {

				// $cekdata = $this->dbObj->fetchAll($this->dbObj->select()
				// 			->from('M_CHARGES_REMITTANCE')
				// 			->where('CHARGE_CCY = ?',$minamt['CHARGE_CCY'])
				// 			->where('CHARGE_AMOUNT_CCY = ?',$minamt['CHARGE_AMOUNT_CCY'])
				// 			->where('CHARGE_TYPE = ?',$minamt['CHARGE_TYPE'])
				// 	);
				$cekdata = $this->dbObj->fetchAll(
					$this->dbObj->select()
						->from('M_CHARGES_REMITTANCE')
						->where('CHARGE_CCY = ?', $minamt['CHARGE_CCY'])
						//->where('CHARGE_AMOUNT_CCY = ?',$minamt['CHARGE_AMOUNT_CCY'])
						->where('CHARGE_TYPE = ?', $minamt['CHARGE_TYPE'])
						->where('CUST_ID = ?', $minamt['CUST_ID'])
				);
				// 				print_r($minamt);die;
				if (!empty($cekdata)) {
					//$whereArr = $this->dbObj->quoteInto('CHARGE_CCY = ? ',$minamt['CHARGE_CCY']);
					//         		print_r($whereArr);die;
					//         		('CHARGE_CCY = ?',$minamt['CHARGE_CCY'],
					//         				'CHARGE_AMOUNT_CCY'=>$minamt['CHARGE_AMOUNT_CCY'],
					//         				'CHARGE_TYPE'=>$minamt['CHARGE_TYPE']
					//         		);

					//         		$this->_db->delete('M_CHARGES_REMITTANCE',$whereArr);
					//         		array('CHARGE_CCY = ?',$minamt['CHARGE_CCY'],
					//         				'CHARGE_AMOUNT_CCY'=>$minamt['CHARGE_AMOUNT_CCY'],
					//         				'CHARGE_TYPE'=>$minamt['CHARGE_TYPE']
					//         		);

					$where = array(
						'CHARGE_CCY = ?' => $minamt['CHARGE_CCY'],
						//'CHARGE_AMOUNT_CCY = ?' => $minamt['CHARGE_AMOUNT_CCY'],
						'CHARGE_TYPE = ?' => $minamt['CHARGE_TYPE'],
						'CUST_ID = ?' => $minamt['CUST_ID']
					);
					//     		print_r($where);die;

					$this->dbObj->delete('M_CHARGES_REMITTANCE', $where);
					//         		print_r($where);die;
					$insertArr = array_diff_key($minamt, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
					//         		print_r($minamt);die;
					$minamtinsert = $this->dbObj->insert('M_CHARGES_REMITTANCE', $insertArr);
				} else {
					$insertArr = array_diff_key($minamt, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
					//         		print_r($insertArr);die;
					$minamtinsert = $this->dbObj->insert('M_CHARGES_REMITTANCE', $insertArr);
				}


				if (!(bool)$minamtinsert) {
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Min Amt)';
					//echo 'here';die;
					return false;
				}
			}
		}
		//         print_r($update);die;
		Application_Helper_General::writeLog('CRCA', 'Approve Minimum Amount Currency');

		$deleteChanges  = $this->deleteNew();

		if (!$deleteChanges)
			return false;

		// 		$this->_redirect('/notification/success/index');
		//echo 'here1';die;
		return true;
	}

	public function approveDelete()
	{
		//will never be called
	}

	/**
	 * Delete Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteNew()
	{
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_CHARGES_REMITTANCE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		//		if(!(boolean)$settingdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Min Amt)';
		//			return false;
		//		}

		$cekChangesInformation = $this->dbObj->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $this->_changeId)
			->where('CHANGES_INFORMATION = ?', 'Cash Collateral')
			->query()
			->fetchAll();

		if (count($cekChangesInformation) > 0) {

			$this->dbObj->delete("TEMP_CHARGES_OTHER", [
				"CHANGES_ID = ? " => $this->_changeId
			]);
		}

		return true;
	}

	// 	protected function deleteMaster($data) {
	// 		//delete from TEMP_MINAMT_CCY
	// // 		$settingdelete = $this->dbObj->delete('M_CHARGES_REMITTANCE');
	// 		$settingdelete = $this->dbObj->delete('M_CHARGES_REMITTANCE',
	// 							$this->dbObj->quoteInto('CHARGE_CCY = ?',$data['CHARGE_CCY'])

	// 				);
	// 		return true;
	// 	}

	// 	protected function deleteMasterDaily() {
	// 		//delete from TEMP_MINAMT_CCY
	// 		$settingdelete = $this->dbObj->delete('M_DAILYLIMIT');

	// 		return true;
	// 	}

	/**
	 * Delete Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteEdit()
	{

		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		//		if(!(boolean)$settingdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Min Amt)';
		//			return false;
		//		}

		return true;
	}

	/**
	 * Delete Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteActivate()
	{
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		//		if(!(boolean)$settingdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Min Amt)';
		//			return false;
		//		}

		return true;
	}

	/**
	 * Delete Changes for changes type: Deactivate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteDeactivate()
	{
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		//		if(!(boolean)$settingdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Min Amt)';
		//			return false;
		//		}

		return true;
	}

	public function deleteDelete()
	{
		//will never be called
	}
	/* (non-PHPdoc)
	 * @see Changemanagement_Model_Tempchanges::approveActivate()
	 */
	public function approveActivate()
	{
		// TODO Auto-generated method stub

	}

	/* (non-PHPdoc)
	 * @see Changemanagement_Model_Tempchanges::approveDeactivate()
	 */
	public function approveDeactivate()
	{
		// TODO Auto-generated method stub

	}
	/* (non-PHPdoc)
	 * @see Changemanagement_Model_Tempchanges::approveEdit()
	 */
	public function approveEdit()
	{
		$minamtData = $this->dbObj->fetchAll(
			$this->dbObj->select()
				->from('TEMP_CHARGES_REMITTANCE')
				->where('CHANGES_ID = ?', $this->_changeId)
		);


		$listSetting = $this->dbObj->select()
			->from('TEMP_SETTING')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetchAll(Zend_Db::FETCH_ASSOC);
		/*	if(!count($listSetting)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Setting)';
			return false;
        } */
		// echo "<pre>";
		// print_r($listSetting);die;

		$cekChangesInformation = $this->dbObj->select()
			->from('T_GLOBAL_CHANGES')
			->where('CHANGES_ID = ?', $this->_changeId)
			->where('CHANGES_INFORMATION = ?', 'Cash Collateral')
			->query()
			->fetchAll();

		if (count($cekChangesInformation) > 0) {
			$getRecord = $this->dbObj->select()
				->from("TEMP_CHARGES_OTHER")
				->where("CHANGES_ID = ?", $this->_changeId)
				->query()
				->fetchAll();

			$getRecord = $getRecord[0];

			$dataArr = [
				"CUST_ID" => $getRecord["CUST_ID"],
				"CHARGES_TYPE" => $getRecord["CHARGES_TYPE"],
				"CHARGES_CCY" => $getRecord["CHARGES_CCY"],
				"CHARGES_PCT" => $getRecord["CHARGES_PCT"],
				"CHARGES_ADM" => $getRecord["CHARGES_ADM"],
				"CHARGES_STAMP" => $getRecord["CHARGES_STAMP"],
				"CHARGES_SUGGESTED" => $getRecord["CHARGES_SUGGESTED"],
				"CHARGES_SUGGESTEDBY" => $getRecord["CHARGES_SUGGESTEDBY"],
				"CHARGES_APPROVED" => new Zend_Db_Expr('now()'),
				"CHARGES_APPROVEDBY" => $actor
			];
			$this->dbObj->insert("M_CHARGES_OTHER", $dataArr);
		}

		try {
			if (!empty($listSetting)) {
				foreach ($listSetting as $row) {
					$updateArr = array_diff_key($row, array('TEMP_ID' => '', 'CHANGES_ID' => '', 'SETTING_ID' => ''));
					$whereArr = array('SETTING_ID = ?' => (string) $row['SETTING_ID']);
					$userupdate = $this->dbObj->update('M_SETTING', $updateArr, $whereArr);
					// echo '<pre>';
					// print_r($updateArr);
					// print_r($whereArr);
				}
			}
		} catch (Exception $err) {
			print_r($err);
			die;
		}
		// die;
		//
		try {
			$userdelete = $this->dbObj->delete('TEMP_SETTING', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		} catch (Exception $err) {
			print_r($err);
			die;
		}

		if (!empty($minamtData)) {


			//         	$this->deleteMasterDaily();
			// echo "<pre>";
			// print_r($minamtData);die;
			foreach ($minamtData as $minamt) {

				// $cekdata = $this->dbObj->fetchAll($this->dbObj->select()
				// 			->from('M_CHARGES_REMITTANCE')
				// 			->where('CHARGE_CCY = ?',$minamt['CHARGE_CCY'])
				// 			->where('CHARGE_AMOUNT_CCY = ?',$minamt['CHARGE_AMOUNT_CCY'])
				// 			->where('CHARGE_TYPE = ?',$minamt['CHARGE_TYPE'])
				// 	);
				$cekdata = $this->dbObj->fetchAll(
					$this->dbObj->select()
						->from('M_CHARGES_REMITTANCE')
						->where('CHARGE_CCY = ?', $minamt['CHARGE_CCY'])
						//->where('CHARGE_AMOUNT_CCY = ?',$minamt['CHARGE_AMOUNT_CCY'])
						->where('CHARGE_TYPE = ?', $minamt['CHARGE_TYPE'])
						->where('CUST_ID = ?', $minamt['CUST_ID'])
				);

				if (!empty($cekdata)) {

					$where = array(
						'CHARGE_CCY = ?' => $minamt['CHARGE_CCY'],
						//'CHARGE_AMOUNT_CCY = ?' => $minamt['CHARGE_AMOUNT_CCY'],
						'CHARGE_TYPE = ?' => $minamt['CHARGE_TYPE'],
						'CUST_ID = ?' => $minamt['CUST_ID']
					);
					// print_r($where);die; 
					try {
						$this->dbObj->delete('M_CHARGES_REMITTANCE', $where);
						$insertArr = array_diff_key($minamt, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
						//         		print_r($minamt);die;
						$minamtinsert = $this->dbObj->insert('M_CHARGES_REMITTANCE', $insertArr);
					} catch (Exception $err) {
						print_r($err);
						die;
					}
				} else {
					try {
						$insertArr = array_diff_key($minamt, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
						//         		print_r($insertArr);die;
						$minamtinsert = $this->dbObj->insert('M_CHARGES_REMITTANCE', $insertArr);
					} catch (Exception $err) {
						print_r($err);
						die;
					}
				}

				if (!(bool)$minamtinsert) {
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Min Amt)';
					return false;
				}
			}
		}
		//         print_r($update);die;
		Application_Helper_General::writeLog('CRCA', 'Approve Minimum Amount Currency');

		$deleteChanges  = $this->deleteNew();
		if (!$deleteChanges)
			return false;

		// 		$this->_redirect('/notification/success/index');
		return true;
	}
}
