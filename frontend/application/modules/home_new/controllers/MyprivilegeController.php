<?php

require_once 'Zend/Controller/Action.php';


class home_MyprivilegeController extends Application_Main {
	
	 public function indexAction()
    {
    	$userLogin 		= $this->_custIdLogin;
    	$userid = $this->_userIdLogin;
    	$select2 = $this->_db->select()
					        ->from(array('B' => 'M_USER'));
		$select2->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$select2->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));	
		$data = $this->_db->fetchRow($select2);
		$this->view->fullname=$data['USER_FULLNAME'];
		
		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_CUSTOMER'));
		$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));	
		$data2 = $this->_db->fetchRow($select3);
		$this->view->custname=$data2['CUST_NAME'];
		$this->view->userphone=$data['USER_PHONE'];
		$this->view->useremail=$data['USER_EMAIL'];
		$this->view->userid=$userid;
		//Zend_debug::dump()
		$mix = $this->_custIdLogin.$this->_userIdLogin;
		
		$select4 = $this->_db->select()
					        ->from(array('D' => 'M_FPRIVILEGE'), array())
					        ->join(array('E' => 'M_FPRIVI_USER'), 'D.FPRIVI_ID = E.FPRIVI_ID',
					        array('D.FPRIVI_ID','D.FPRIVI_DESC'));
		$select4 -> where("E.FUSER_ID = ".$this->_db->quote($mix));
		$result = $select4->query()->FetchAll();
		$this->view->privilist=$result;
		//Zend_Debug::dump($result); die;
    }
}
