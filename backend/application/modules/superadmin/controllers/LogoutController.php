<?php

class superadmin_LogoutController extends Application_Main {
	
	
	public function indexAction(){
		Zend_Session::namespaceUnset('SYSADMIN');
		$this->_redirect('/superadmin');
	}
}
?>