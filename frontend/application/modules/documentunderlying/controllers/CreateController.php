<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';

class documentunderlying_CreateController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	protected $_destinationUploadDir = '';
	protected $_listCCYValidate = '';

	public function initController()
	{       
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 	
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
        		
	}
	
	public function indexAction()
	{		

		$this->_helper->layout()->setLayout('newlayout');
		
		$Settings = new Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();		
		//Zend_Debug::dump($ccyList);die;
		
		$this->view->userIdLogin  = $this->_userIdLogin;
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
        
        $getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;

        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

        $maxFileSize = "1024"; //"1024000"; //1024
        //~ $fileExt = "csv,dbf";
        $fileExt = "jpeg,jpg,pdf";
        if($this->_request->isPost() )
        {
            $attahmentDestination   = UPLOAD_PATH . '/document/submit/';
            $errorRemark            = null;
            $adapter                = new Zend_File_Transfer_Adapter_Http();
            $datas = $this->_request->getParams();
            
            $sourceFileName = trim($adapter->getFileName());
             
            if($sourceFileName == null){
                //die('here');
                $sourceFileName = null;
                $fileType = null;
            }
            else
            {

                $sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
                if($_FILES["uploadSource"]["type"])
                {
                    $adapter->setDestination($attahmentDestination);
                    $fileType               = $adapter->getMimeType();
                    $size                   = $_FILES["document"]["size"];
                }
                else{
                    $fileType = null;
                    $size = null;
                }
            }
            $paramsName['sourceFileName']   = $sourceFileName;
            //print_r($sourceFileName);die;
            $fileTypeMessage = explode('/',$fileType);
            if (isset($fileTypeMessage[1])){
                $fileType =  $fileTypeMessage[1];
                $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                $extensionValidator->setMessage("Extension file must be *.jpg /.jpeg / .pdf");

                $size           = number_format($size);
                $sizeTampil     = $size/1000;
                //$sizeValidator    = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                // $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");
                
                $adapter->setValidators(array($extensionValidator));

                if($adapter->isValid())
                {
                    $date = date("dmy");
                    $time = date("his");

                    $newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
                    $adapter->addFilter('Rename', $newFileName);

                    if($adapter->receive())
                    {
                        

                        $filedata = $attahmentDestination.$newFileName;
                        // var_dump($filedata);
                        if (file_exists($filedata)) {
                        exec("clamdscan --fdpass ".$filedata, $out ,$int);
                            
                        // if()
                            // var_dump($int);
                            // var_dump($out);
                            // die;
                        if($int == 1){
                            // echo "aa";die;
                            // unlink($filedata);
                            //die('here');
                            $this->view->error = true;
                            $errors = array(array('FileType'=>'Error: An infected file. Please upload other file'));
                            $this->view->errorMsg = $errors;


                        }else{
                            $datas['fileName'] = $newFileName;
                            $datas['sourcefileName'] = $sourceFileName;
                            $sessionNamespace = new Zend_Session_Namespace('documentunderlying');
                            $sessionNamespace->content = $datas;
                            $this->_redirect('/documentunderlying/create/confirm');
                            
                            
                        }
                        
                        
                        }
                    }
                    
                }else{
                    $this->view->error = true;
                    $errors = array($adapter->getMessages());
                    $this->view->errorMsg = $errors;
                }
                
            }else{
                            $sessionNamespace = new Zend_Session_Namespace('documentunderlying');
                            $sessionNamespace->content = $datas;
                            $this->_redirect('/documentunderlying/create/confirm');
            }

        }
    }

    public function confirmAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

         $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;
        
        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

        $sessionNamespace = new Zend_Session_Namespace('documentunderlying');
        $data = $sessionNamespace->content;
        
        
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        
            $param = array('CCY_IN' => 'IDR','ACCT_NO'=>$data['acct']);
            $AccArr = $CustomerUser->getAccounts($param);
        
         
        if(!empty($AccArr)){
            $this->view->src_name = $AccArr['0']['ACCT_NAME'];
        }
        
        foreach($data as $key => $val){
            
            if($key != 'language'){
        $this->view->$key = $val;
            }
        }

        if($this->_request->isPost() )
		{	
            try 
            {
                
                $this->_db->beginTransaction();					
                $dataArr = array(	
                                'DOC_ID' => $this->generateTransactionID(),
                                'CUST_ID' => $this->_custIdLogin,
                                'CCY' => $data['ccyid'],
                                'AMOUNT' => Application_Helper_General::convertDisplayMoney($data['amount']),
                                'PAYMENT_TYPE' => $data['paytype'],
                                'PAYMENT_DATE' => Application_Helper_General::convertDate($data['paydate'],$this->_dateDBFormat, $this->_dateDisplayFormat),
                                'DOC_NAME' => $data['sourcefileName'],
                                'DOC_UPLOADEDBY' => $this->_userIdLogin,
                                'DOC_UPLOADED' =>new Zend_Db_Expr("now()"),
                                'DOC_SYSNAME' => $data['fileName'],
                                'DOC_STATUS' => '1'
                            );
                            // var_dump($data);die;
                $this->_db->insert('T_UNDERLYING',$dataArr);
                $this->_db->commit();
                
                $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                $this->_redirect('/notification/success');
            }
            catch(Exception $e) 
            {   
                var_dump($e);die;
                $this->_db->rollBack();
                
            }
			
		}


    }

    public function generateTransactionID(){

        $currentDate = date("Ymd");
        $seqNumber   = strtoupper(Application_Helper_General::str_rand(6));
        $trxId = 'UD'.$currentDate.$seqNumber;

        return $trxId;
    }
        
}