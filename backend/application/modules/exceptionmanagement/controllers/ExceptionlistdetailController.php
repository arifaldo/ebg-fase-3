<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';
require_once 'Crypt/AESMYSQL.php';

class Exceptionmanagement_ExceptionlistdetailController extends Application_Main
{
	public function indexAction() 
	{ 
		$transferstatus = $this->_transferstatus;
		$transferstatuscode = $transferstatus['code'];
		$transferstatusdesc = $transferstatus['desc'];
		$this->view->transferstatuscode = array_flip($transferstatuscode);
		$this->view->transferstatusdesc = $transferstatusdesc;
		$this->view->transferstatus = $transferstatus;
		
		$paycashmstatusarr = array_combine(array_values($this->_paymentstatus['code']),array_values($this->_paymentstatus['desc']));
		$trastatusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
		$acctarr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
		// $feearr = array_combine(array_values($this->_feetype['code']),array_values($this->_feetype['desc']));
		//Zend_Debug::dump($feearr);die;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();
        $slipnumb = $AESMYSQL->decrypt($this->_getParam('slipnumb'), $password);

		$action = $this->_getParam('radiobut');
		$submit = $this->_getParam('submit');
		$custid = $this->_getParam('custid');
		$type = $this->_getParam('type');
		
		if($submit)
		{
			Application_Helper_General::writeLog('EXUD','Update Payment Exception ('.$slipnumb.')');
		}
		else
		{
			Application_Helper_General::writeLog('EXLS','Viewing Payment Exception List Detail ('.$slipnumb.')');
		}

		$cek = $this->_db->select()
								->from(array('A' => 'T_PSLIP_EXCEPTION_REPAIR'),array('*'))
								->where("A.PS_NUMBER LIKE ".$this->_db->quote($slipnumb))
								->where("A.SUGGEST_STATUS LIKE '0' OR A.SUGGEST_STATUS LIKE '1'")
								-> query()->fetchAll();
		
		if($cek)
		{
			$this->view->disabled = 'disabled';
		}

  		if($submit && !$cek && $this->view->hasPrivilege('EFUD'))
  		{
  			$sqlTransaction = $this->_db->select()
								->from(array('A' => 'T_PSLIP'),array())
								->join(array('B' => 'T_TRANSACTION'),'A.PS_NUMBER = B.PS_NUMBER',array())
								->join(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('A.*','B.*','C.CUST_NAME'))
								->where("A.PS_NUMBER = ".$this->_db->quote($slipnumb))
								->where("A.PS_STATUS = '9'");
			$result = $this->_db->fetchAll($sqlTransaction);
			$psstatus = $this->_getParam('radiobut');

			if($result)
			{
				$this->_db->beginTransaction();
				
				try
				{
					$insertArr = array();
					$insertArr['PS_NUMBER'] = $slipnumb;
					$insertArr['CUST_ID'] = $result[0]['CUST_ID'];
					$insertArr['CUST_NAME'] = $result[0]['CUST_NAME'];
					$insertArr['SUGGEST_USER'] = $this->_userIdLogin;
					$insertArr['SUGGEST_DATE'] = new Zend_Db_Expr('now()');
					$insertArr['PS_STATUS'] = $psstatus;
					$insertArr['TYPE'] = $result[0]['PS_TYPE'];
					$insertArr['SUGGEST_STATUS'] = 0;
					$insertArr['EXCEPTION_TYPE'] = 1;
					
					$this->_db->insert('T_PSLIP_EXCEPTION_REPAIR', $insertArr);
					
					$lastIdTemp = $this->_db->fetchOne('select @@identity');
					//Zend_Debug::dump($result);die;
					if(!($result[0]['PS_TYPE']==4 && ($result[0]['SENDFILE_STATUS']==0 /*|| $result[0]['SENDFILE_STATUS']==3*/))) // jika bukan bulk credit atau escrow to bene
					{
						foreach($result as $row)
						{
							if($psstatus == '6')
							{
								$update = 4;
							}
							else
							{
								$update = $this->_getParam($row['TRANSACTION_ID']);
								if($update == '5')
								{
									$update = 3;
								}
								
								if($update == '6')
								{
									$update = 4;
								}
							}
							
							$insertArrDetail = array();
							$insertArrDetail['SUGGEST_ID'] = $lastIdTemp;
							$insertArrDetail['TRANSACTION_ID'] = $row['TRANSACTION_ID'];
							$insertArrDetail['TRA_STATUS'] = $update;
							$insertArrDetail['MUST_REVERSE'] = 0;
							//Zend_Debug::dump($insertArrDetail);
							$this->_db->insert('T_PSLIP_EXCEPTION_REPAIR_DETAIL', $insertArrDetail);
						}
					}
					
					$sqlISO = $this->_db->select()
										->from(array('A' => 'T_ISO_QUEUE'),array('*'))
										->where("A.PS_NUMBER = ".$this->_db->quote($slipnumb))
										->where("A.SEND_STATUS = '0'");		// 0 : not sent yet, 1 : already sent, 2 : got response
					$rsISOQ = $this->_db->fetchAll($sqlISO);
					
					if (!empty($rsISOQ))
					{
						$translate = array( '[|]' => ' ');
						
						foreach($rsISOQ as $iq => $isoq)
						{
							$isoID = $isoq['ISO_ID'];
							// insert into t_iso_message
							unset($isoq['ISO_ID']);
							$isoq['IS_READ'] = 0;		// 0: not yet read, 1: is read, 2: timeout
							$isoq['MSG_RECEIVE'] = strtr($isoq['MSG_RECEIVE'],$translate);
							$this->_db->insert('T_ISO_MESSAGE', $isoq);
							
							// delete dari t_iso_queue
							$whereUpd  = array('ISO_ID = ?' => (string) $isoID);
							$this->_db->delete('T_ISO_QUEUE', $whereUpd);
						}
					}
					
					$this->_db->commit();
					$this->_redirect('/popup/submited/index');
					
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();			
				}
			}
  		}
  		
  		$caseStatus = "(CASE B.PS_STATUS ";
  		foreach($paycashmstatusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";
  			
  		$caseStatus2 = "(CASE C.TRA_STATUS";
  		foreach($trastatusarr as $key=>$val)
  		{
   			$caseStatus2 .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus2 .= " END)";
  		
  		$select = $this->_db->select()->distinct()
					        ->from(array('B' => 'T_PSLIP'),array())
					        ->join(array('C' => 'T_TRANSACTION'), 'B.PS_NUMBER = C.PS_NUMBER',array())
					        ->join(array('D' => 'M_CUSTOMER'), 'B.CUST_ID = D.CUST_ID',array(	'payref'		=>	'B.PS_NUMBER',
					        																	'paystatus'		=>	$caseStatus,
					        																	'custid'		=>	'D.CUST_ID',
				        																		'custname'		=>	'D.CUST_NAME',
				        																		'paysubject'	=>	'B.PS_SUBJECT',
					        																	'benefacct'		=>	'C.BENEFICIARY_ACCOUNT',
					        																	'benefccy'		=>	'C.BENEFICIARY_ACCOUNT_CCY',
					        																	'benefname'		=>	'C.BENEFICIARY_ACCOUNT_NAME',
					        																	'benefalname'	=>	'C.BENEFICIARY_ALIAS_NAME',
					        																	'created'		=>	'B.PS_CREATED',
					        																	'updated'		=>	'B.PS_UPDATED',
					        																	'paydate'		=>	'B.PS_EFDATE',
					        																	'releaser'		=>	'B.PS_RELEASER_USER_LOGIN',
					        																	'ccode'			=>	'B.PS_RELEASER_CHALLENGE',
	        																					'payamt'		=>	'B.PS_TOTAL_AMOUNT',
	        																					'transid'		=>	'C.TRANSACTION_ID',
	        																					'sourceacct'	=>	'C.SOURCE_ACCOUNT',
	        																					'sourceccy'		=>	'C.SOURCE_ACCOUNT_CCY',
					        																	'sourcename'	=>	'C.SOURCE_ACCOUNT_NAME',
					        																	'sourcealname'	=>	'C.SOURCE_ACCOUNT_ALIAS_NAME',
	        																					'transamt'		=>	'C.TRA_AMOUNT',
	        																					'message'		=>	'C.BANK_RESPONSE',
	        																					'status'		=>	$caseStatus2,
					        																	'type'			=>	'B.PS_TYPE',
					        																	'reversal'			=>	'B.REVERSAL_STATUS',
					        																	'sendfile'			=>	'B.SENDFILE_STATUS'));
		$select -> where("B.PS_NUMBER LIKE ".$this->_db->quote($slipnumb));
		//echo $select; die;
		$result = $select->query()->FetchAll();
		$result2 = $this->_db->fetchRow($select);
		$cektotal = 0;
		foreach($result as $total)
		{
			$cektotal += $total['transamt'];
		}
		
		/* $this->view->escrowAcct = '';
		if($result[0]['type'] == 4 && $result[0]['sendfile'] == 0)
		{
			$escrow = $this->_db->fetchRow(
								$this->_db->select()
													->from('M_MASTER',array('M_KEY1','M_KEY2','M_KEY3'))
													->where("M_KEY = 'ESCROW'")
			);
			$this->view->escrowAcct = $escrow['M_KEY2']." [".$escrow['M_KEY1']."] - ".$escrow['M_KEY3'];
		}
		 */
		$this->view->result=$result;
		$this->view->result2=$result2;
		$this->view->total = $cektotal;
		$this->_helper->viewRenderer('exceptionlistdetail/cashm', null, true);
	}
}