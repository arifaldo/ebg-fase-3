<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class linkagebeneficiary_RepairlinkageController extends Application_Main
{
	// public function initController()
	// {       
	
	// 	$this->_helper->layout()->setLayout('popup');
	// }
	
	public function indexAction()
	{
		$suggestid = $this->_getParam('suggestid');
		$this->view->suggestid = $suggestid;
		//Zend_Debug::dump($suggestid);die;
		$in = null;
		$fields = array(
						'bank_name'  => array('field' => 'BANK_NAME',
											   'label' => $this->language->_('Bank Name'),
											   'sortable' => true),
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CURR_CODE',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => $this->language->_('Beneficiary Account Name'),
											   'sortable' => true),
						'benef_type'  => array('field' => 'BENEFICIARY_TYPE',
											   'label' => $this->language->_('Beneficiary Type'),
											   'sortable' => true)
				);
				
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  		
		//$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$this->benefcode = array_flip($this->_beneftype['code']);
		$this->benefdesc = $this->_beneftype['desc'];
		$this->view->benefcode = $this->benefcode;
		$this->view->benefdesc = $this->benefdesc;
		
		$select	= $this->_db->select()
								->from(array('A' => 'T_BENEFICIARY_USER'),array())
								->join(array('B' => 'TEMP_BENEFICIARY_USER'), 'A.SUGGEST_ID = B.SUGGEST_ID',array(	'BENEFICIARY_ID'	=> 'B.BENEFICIARY_ID',
																													'USER_ID'			=> 'A.USER_ID'))
								->where('A.SUGGEST_ID =?',$suggestid);
			//die($select);
		$arr = $this->_db->fetchAll($select);
		$userid = $arr[0]['USER_ID'];
		$this->view->userid = $userid;
		//Zend_Debug::dump($arr);die;
		$in = "(";
		$x = 0;
		foreach($arr as $row) 
		{	
			if($x==1){$in .= ",";}
			$in .= "'".$row['BENEFICIARY_ID']."'"; 
			$x=1;
		}
		$in .= ")";
		if($x == 0)
		{
			$in = null;
		}
		
		
		if($this->_getParam('submit'))
		{
			$filters = array( 	
								'user_id'		=> 	array('StringTrim','StripTags'),
								'benef'		=> 	array('StringTrim','StripTags'),
							);

			$validators = array	(
									'benef' 			=> 	array	(	
																		new Zend_Validate_GreaterThan(0),
																		'messages' => array	(
																								$this->language->_('Error: Beneficiary Account Has not been Choosen yet.'),
																							)
																	),
							
								);
			$postBenef_id	= $this->getRequest()->getParam('benef_id');
			$postBenef_acc 	= $this->getRequest()->getParam('benef_acc');
			//Zend_Debug::dump($validators);die;
			
			foreach ($postBenef_id as $key => $value) 
			{
				if($postBenef_id[$key]==0)
				{
					unset($postBenef_id[$key]);
					unset($postBenef_acc[$key]);
				}
			}
			
			if($postBenef_id == null)
			{
				$params['benef'] = 0;
			}
			else
			{
				$params['benef'] = 1;
			}
			//Zend_Debug::dump($params);die;
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$params,$this->_optionsValidator);
			
			if($zf_filter_input->isValid())
			{
				try 
				{
					$now = Zend_Date::now()->toString("d-MMM-yyyy");
					$whereUpdate['SUGGEST_ID = ?'] = $suggestid;
					$param = array 	(
								"SUGGEST_USER" => $this->_userIdLogin,
								"SUGGEST_DATE" => new Zend_Db_Expr('now()'),
								"SUGGEST_STATUS" => 5,
							); 
					//Zend_Debug::dump($param);die;
					$select = $this->_db->update('T_BENEFICIARY_USER',$param,$whereUpdate);
					//die("id = ".$select);
					
					$deleteWhere['SUGGEST_ID = ?'] = $suggestid;
					$this->_db->delete('TEMP_BENEFICIARY_USER',$deleteWhere);
					
					foreach ($postBenef_id as $key => $value) 
					{
						$param = array 	( 
									"USER_ID" => $userid,  
									"BENEFICIARY_ID" => $postBenef_acc[$key],
									"SUGGEST_ID" => $suggestid,
								);

						$this->_db->insert('TEMP_BENEFICIARY_USER',$param);
					}
					
					Application_Helper_General::writeLog('BMBU',"Repair Pairing Beneficiary Sugggestion for User=".$this->_userIdLogin."; for Company= ".$userid);
				
					$this->setbackURL('/'.$this->_request->getModuleName().'/approve/index/');
					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
					
			}
			else
			{
				$this->view->error 			= true;
				$errors 					= $zf_filter_input->getMessages();
				
				$this->view->benefErr 		= (isset($errors['benef']))? $errors['benef'] : null;
			}
		}
		
		
		$sqlA = "SELECT M_BENEFICIARY.BENEFICIARY_ACCOUNT,
						M_BENEFICIARY.BENEFICIARY_ID,
						M_BENEFICIARY.BENEFICIARY_NAME,
						M_BENEFICIARY.CURR_CODE,
						M_BENEFICIARY.BENEFICIARY_ALIAS,
						M_BENEFICIARY.BENEFICIARY_TYPE,
						1 AS 'CHECK',
						M_BANK_TABLE.BANK_NAME
				FROM 	M_BENEFICIARY
				LEFT JOIN M_BANK_TABLE
				ON M_BENEFICIARY.BANK_CODE=M_BANK_TABLE.BANK_CODE
				WHERE	CUST_ID = ".$this->_db->quote($this->_custIdLogin).
				"AND		BENEFICIARY_ID IN".$in;
				
		$sqlB = "SELECT M_BENEFICIARY.BENEFICIARY_ACCOUNT,
						M_BENEFICIARY.BENEFICIARY_ID,
						M_BENEFICIARY.BENEFICIARY_NAME,
						M_BENEFICIARY.CURR_CODE,
						M_BENEFICIARY.BENEFICIARY_ALIAS,
						M_BENEFICIARY.BENEFICIARY_TYPE,
						0 AS 'CHECK',
						M_BANK_TABLE.BANK_NAME
				FROM 	M_BENEFICIARY
				LEFT JOIN M_BANK_TABLE
				ON M_BENEFICIARY.BANK_CODE=M_BANK_TABLE.BANK_CODE
				WHERE	CUST_ID = ".$this->_db->quote($this->_custIdLogin).
				"AND	BENEFICIARY_ID NOT IN".$in;
		
		$select = $this->_db->select()->distinct()->union(array($sqlA, $sqlB));
		//Zend_Debug::dump($select->query()->fetchAll());die;
		//die($select);
		$select ->order('BANK_NAME ASC');
		$select->order($sortBy.' '.$sortDir);  
		$arr = $select->query()->fetchAll();
		// echo "<pre>";
		// var_dump($arr);
		// die();
		$this->view->data = $arr;
		//Zend_Debug::dump($arr);die;
		$this->paging($arr);
		$this->view->fields = $fields;
	}
}
