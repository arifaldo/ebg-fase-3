<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
class userdailylimit_ViewController extends userdailylimit_Model_Userdailylimit{ 

   public function initController()
  {       
  
    $this->_helper->layout()->setLayout('popup');
  } 

  public function indexAction() 
  { 
    //$this->_helper->layout()->setLayout('newlayout');
    $cust_id = strtoupper($this->_getParam('cust_id'));
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;  


    $AESMYSQL = new Crypt_AESMYSQL();
    $PS_NUMBER      = urldecode($this->_getParam('user_id'));
    $user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    $user_id = strtoupper($user_id);
//    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $user_id : null;
    
    $ccy_id = strtoupper($this->_getParam('ccy_id'));
    $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
    
    $error_remark = null;    

    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $result = $this->_db->fetchRow($select);
      if(!$result['CUST_ID']){ $cust_id = null; }
      else{ $this->view->cust_name = $result['CUST_NAME']; }
    }


    if(!$cust_id)
    {
      $error_remark = 'Customer ID is not found.';
      Application_Helper_General::writeLog('VDLU','View User Daily Limit');
        
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }

     
    if($user_id == true && $ccy_id == true)
    {
       $select = $this->_db->select()
                             ->from(array('d'=>'M_DAILYLIMIT'))
                             ->join(array('u'=>'M_USER'),'d.CUST_ID = u.CUST_ID AND d.USER_LOGIN=u.USER_ID',array('USER_FULLNAME'))
                             //->where('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id));
                             //->where('d.DAILYLIMIT_STATUS!=3');
                             
      $resultdata = $this->_db->fetchRow($select);
      
      if($resultdata['DAILYLIMIT'])
      {
        $this->view->user_id     = $user_id;
        $this->view->cust_id     = $cust_id;
        $this->view->ccy_id      = $ccy_id;
        $this->view->user_name   = $resultdata['USER_FULLNAME'];
        $this->view->daily_limit =  Application_Helper_General::displayMoney($resultdata['DAILYLIMIT']);
        $this->view->status      =  $resultdata['DAILYLIMIT_STATUS'];
      
        
        $this->view->daily_created     = $resultdata['CREATED'];
        $this->view->daily_createdby   = $resultdata['CREATEDBY'];
        $this->view->daily_suggested   = $resultdata['SUGGESTED'];
        $this->view->daily_suggestedby = $resultdata['SUGGESTEDBY'];
        $this->view->daily_updated     = $resultdata['UPDATED'];
        $this->view->daily_updatedby   = $resultdata['UPDATEDBY'];
        
        
        //cek apakah ada di temp atau tidak
        $select = $this->_db->select()
                               ->from('TEMP_DAILYLIMIT',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                               ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id));
                               //->where('d.DAILYLIMIT_STATUS!=3');
                               
        $result = $this->_db->fetchOne($select);    
        if($result){ $temp = 0; }else{ $temp = 1; }
        $this->view->daily_temp = $temp;

       /* echo $this->view->acct_temp;
        die;*/
        Application_Helper_General::writeLog('VDLU','View User Daily Limit Detail [ Cust ID : '.$cust_id.', User ID : ' .$user_id.' ]');
             
      }
      else{ $acct_no = null; }
    }
    else{
      $error_remark = 'User ID / CCY is not found.';
      Application_Helper_General::writeLog('VDLU','View User Daily Limit');
        
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }

     $setting = new Settings();          
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
    $pw_hash = md5($enc_salt.$enc_pass);
    $rand = $this->_userIdLogin.date('dHis').$pw_hash; 
    $sessionNamespace->token  = $rand;
    $this->view->token = $sessionNamespace->token; 
    
    $this->view->cust_id     = $cust_id;
    $this->view->change_type = $this->_changeType;
    $this->view->status_type = $this->_masterglobalstatus;
    $this->view->login_type  = $this->_masterhasStatus;
    $this->view->modulename  = $this->_request->getModuleName();
    
  }
}
