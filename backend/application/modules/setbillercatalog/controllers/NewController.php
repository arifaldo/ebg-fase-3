<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';

class billeraccount_NewController extends Application_Main
{
	public function indexAction() 
	{
		$this->view->aaa = $a;
		// min max untuk provider Code and Name Length
  		$min = 10;
		$max = 10;
		$failed = '';
		$confirmPage = false;

		$providerid 	= $this->_getParam('providerid');
		if(empty($providerid))
		{
			header('location: /'.$this->view->modulename);exit();
		}

		$biller 			 = new biller_Model_Biller();
		$providerData 	 = $biller->getDetailProvider($providerid);
		$this->view->providerid = $providerid;
		if($providerData)
		{
			$this->view->providerCode = $providerData['PROVIDER_CODE'];
			$this->view->providerName = $providerData['PROVIDER_NAME'];
			
			$arrCCY = array('IDR'=>'IDR');
			$this->view->arrCcy = array('' => '--- Please Select ---')+$arrCCY;

			if($this->_request->isPost())
			{
				$ACCT_NO = $this->_getParam('ACCT_NO');
				$ACCT_CIF = $this->_getParam('ACCT_CIF');

				$filters = array(
								   'ACCT_NO'    	=> array('StripTags','StringTrim','StringToUpper'),
								   'CCY'  	=> array('StripTags','StringTrim'),
								   'ACCT_CIF'  	=> array('StripTags','StringTrim'),
								  );
				$validators = array(
											'ACCT_NO' 	=> array(	
																	'NotEmpty',
																	'Alnum',
																	array	(
																				'StringLength', 
																				array	(
																							'min' => $min,
																							'max' => $max
																						)
																			),
																	array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'M_PROVIDER_ACCT',
																							'field'=>'ACCT_NO'
																						)
																			),
																	array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'TEMP_PROVIDER_ACCT',
																							'field'=>'ACCT_NO'
																						)
																			),
																	'messages' => array(
																							"Error: Account Number Can not be empty.",
																							"Error: Invalid Account Number.",
																							"Error: Invalid Account Number length( $min - $max character allowed).",
																							"Error: $ACCT_NO is already registered.",
																							"Error: $ACCT_NO is already taken."
																						)
																),
											'ACCT_CIF' 	=> array(	
																	'NotEmpty',
																	'Alnum',
																	array	(
																				'StringLength', 
																				array	(
																							'min' => $min,
																							'max' => $max
																						)
																			),
																	array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'TEMP_PROVIDER_ACCT',
																							'field'=>'ACCT_CIF'
																						)
																			),
																	array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'TEMP_PROVIDER_ACCT',
																							'field'=>'ACCT_CIF'
																						)
																			),
																	'messages' => array(
																							"Error: Account CIF Can not be empty.",
																							"Error: Invalid Account CIF.",
																							"Error: Invalid Account CIF length( $min - $max character allowed).",
																							"Error: $ACCT_CIF is already registered.",
																							"Error: $ACCT_CIF is already taken."
																						)
																),
											'CCY' 	=> array(
																	'NotEmpty',
																	array('InArray',array_keys($arrCCY)),
																	'messages' => array(																																								
																							'Error: Please Select from the List.',
																						)
																),																																
										);

				$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());
				if( $zf_filter->isValid() && !empty($zf_filter->ACCT_NO) )
				{
					$param['PROVIDER_ID'] 	= $providerid;
					$param['ACCT_NO'] 	= html_entity_decode($zf_filter->getEscaped('ACCT_NO'));
					$param['ACCT_CIF'] 	= html_entity_decode($zf_filter->getEscaped('ACCT_CIF'));
					$param['CCY_ID'] 	= html_entity_decode($zf_filter->getEscaped('CCY'));

					if($providerData['PROVIDER_STATUS'] == 'Approved') $biller_flag = true;
					else $biller_flag = false;
					//integrate ke core
					//get number from ccy_id
					// $ccy_id_num = Application_Helper_General::getCurrNum($param['CCY_ID']);
					$result = $this->getAccountInfo($param['ACCT_NO'],$param['CCY_ID']);
					$acct_flag = true;
					
					if($result['checkBalanceStatus'] != '00') //00 = success
					{
						$acct_flag = false;
						$acct_type = 1;
					}
					if($biller_flag == true && $acct_flag == true)
					{
						$confirmPage = true;
						
						if($this->_getParam('submit') == 'Confirm')
						{
							$this->_db->beginTransaction();
							try
							{
								$info = 'Biller Account';
								$info2 = 'Add New Biller Account';
								
								$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_PROVIDER_ACCT','TEMP_PROVIDER_ACCT',$param['ACCT_NO'],$param['ACCT_CIF'],$param['ACCT_NO']);

								$data= $param+array(
																					'CHANGES_ID' 			=> $change_id,
																					'ACCT_STATUS' 			=> 1,
																					'ACCT_NAME' 			=> $result['ACCT_NAME'],
																					'ACCT_ALIAS_NAME' 			=> $result['ACCT_NAME'],
																					'ACCT_SOURCE' 			=> $result['ACCT_NAME'],
																					'ACCT_DESC' 			=> $result['ACCT_DESC'],
																					'ACCT_SOURCE' 			=> $result['ACCT_SOURCE'],
																					'ACCT_TYPE' 			=> $result['ACCT_TYPE'],
																					'ACCT_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
																					'ACCT_SUGGESTEDBY' 	=> $this->_userIdLogin
																				);
								$biller->insertTempacct($data);
								Application_Helper_General::writeLog('CBLA','Add New Biller Account. Provider ID : '.$providerID.'.,Account Number : '.$param['ACCT_NO'].'.,Account CIF : '.$param['ACCT_CIF']);
								
								if($error == 0)
								{
									$this->_db->commit();
									$this->setbackURL('/biller');
									$this->_redirect('/notification/submited/index');
								}
							}
							catch(Exception $e)
							{
								$this->_db->rollBack();
							}
						}
					}
					else
					{
						//konfigurasi error cust id jika didelete
						if($acct_flag === false)
						{
							 if($acct_type === 1)      
							 {
							   if($result['errorMessage']) $this->view->ACCT_NO_errMsg = $result['errorMessage'];
							   else $this->view->ACCT_NO_errMsg = 'Error connection with Host';
							 }
						}
						if($biller_flag === false) 
						{
						   if($providerData['PROVIDER_STATUS'] == 'Deleted')       $this->view->ACCT_NO_errMsg = 'Cannot add record, user already deleted';
						   else if($providerData['PROVIDER_STATUS'] == 'Suspended')  $this->view->ACCT_NO_errMsg = 'Cannot add record, user already Suspended';
						}
					}
				}
				else
				{
					$listMsg = $zf_filter->getMessages();
					foreach($listMsg as $key => $arrMsg)
					{
						foreach($arrMsg as $msg)
						{
							$keyID = $key.'_errMsg';
							$this->view->$keyID = $msg;
						}
					}
					$failed = 'failed';
				}
				$this->view->confirmPage = $confirmPage;
				$this->view->ACCT_NO = ($zf_filter->isValid() && $acct_flag === true && $biller_flag === true) ? $param['ACCT_NO'] : $this->_getParam('ACCT_NO');
				$this->view->ACCT_CIF = ($zf_filter->isValid() && $acct_flag === true && $biller_flag === true) ? $param['ACCT_CIF'] : $this->_getParam('ACCT_CIF');
				$this->view->CCY = ($zf_filter->isValid() && $acct_flag === true && $biller_flag === true) ? $param['CCY_ID'] : $this->_getParam('CCY');
				if($confirmPage)	$this->view->result = $result;
			}

			Application_Helper_General::writeLog('CBLA',$failed.'Add Biller,Provider ID : '.$providerid);
		}
		else
		{
			if($providerid)
				Application_Helper_General::writeLog('CBLA','View Add Biller,Provider ID '.$providerid.' not found');
			else
				Application_Helper_General::writeLog('CBLA','View Add Biller .No Provider ID');
		}
	}
	
	//information from core
	private function getAccountInfo($acct_no,$ccy_id) 
	{
		//integrate ke core
		//get number from ccy_id
		$ccy_id_num = Application_Helper_General::getCurrNum($ccy_id);
		$account    = new Account($acct_no,$ccy_id_num);
		$result     = $account->checkBalance();
		//$result['checkBalanceStatus'] = '00';
		  
		if($result['checkBalanceStatus'] == '00') //00 = success
		{
			$acct_balance = $account->getAvailableBalance();
			$acct_owner   = $account->getCoreAccountName();
			$acct_plafond = $account->getCorePlafond();
			$product_type = $account->getProductType();
			$acct_status  = $account->getCoreAccountStatusDesc();
		}
		$acctDesc	=  $this->getAcctDesc($acct_no,$product_type,$acct_plafond);
		
		$result['ACCT_BALANCE'] = $acct_balance;
		$result['ACCT_NAME']   = $acct_owner;
		$result['ACCT_PLAFOND'] = $acct_plafond;
		$result['ACCT_TYPE'] = $product_type;
		$result['ACCT_SOURCE'] = $acctDesc[0];
		$result['ACCT_DESC'] = $acctDesc[1];

		return $result;
	}
	
	private function getAcctDesc($acct_no,$product_type,$acct_plafond)
	{  
          $first_digit = substr($acct_no,0,1);
          
		  if($first_digit == '1')  
		  {
		      $acct_source = 3;    //3 = saving  
		      $acct_desc   = 'SAVING';
		  }
		  else
		  {
		       if($product_type == '070')      
		       {
		           $acct_source = 1;     //1 = prk scm 
		           $acct_desc   = 'PRK FSCM';
		       }
		       else if($product_type == '020') 
		       {
		           if($acct_plafond == 0)       
		           {
		              $acct_source = 2;    //2 = prk non scm atau giro
		              $acct_desc   = 'GIRO';
		           }
		           else if($acct_plafond > 0)   
		           {
		              $acct_source = 2;    //2 = prk non scm atau giro
		              $acct_desc   = 'PRK';
		           }
		       }
		       else
		       {
		           if($acct_plafond == 0)       
		           {
		              $acct_source = 2;    //2 = prk non scm atau giro
		              $acct_desc   = 'GIRO';
		           }
		           else if($acct_plafond > 0)   
		           {
		              $acct_source = 2;    //2 = prk non scm atau giro
		              $acct_desc   = 'PRK';
		           }
		       }
		 }
		 
		 return array($acct_source,$acct_desc);
	}
	
}