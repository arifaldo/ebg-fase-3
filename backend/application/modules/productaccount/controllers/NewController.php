<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class productaccount_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		//pengaturan url untuk button back
		$this->setbackURL('/' . $this->view->modulename);

		$model = new productaccount_Model_Productaccount();

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$this->view->report_msg = array();


		$account_typeArr 	= array_combine($this->_productaccount["code"], $this->_productaccount["desc"]);

		$this->view->account_typeArr = (array('' => '-- ' . $this->language->_('Please Select') . ' --') + $account_typeArr);


		if ($this->_request->isPost()) {
			$filters = array(
				'product_code'	=> array('StringTrim', 'StripTags'),
				// 'plan_code'	=> array('StringTrim','StripTags'),
				'product_name'    => array('StringTrim', 'StripTags'),
			);

			$inputs = new Zend_Filter_Input($filters, null, $this->_request->getParams());

			$expProdCodeAndName = "PRODUCT_CODE = " . $this->_db->quote($inputs->product_code) . " AND PRODUCT_NAME = " . $this->_db->quote($inputs->product_name);
			// $expPlanProdCode = "PRODUCT_PLAN = ".$this->_db->quote($inputs->plan_code)." AND PRODUCT_CODE = ".$this->_db->quote($inputs->product_code);

			$validators = array(
				'product_code'  => array(
					'NotEmpty',
					//'Digits',
					new Zend_Validate_StringLength(array('min' => 1, 'max' => 3)),
					array(
						'Db_NoRecordExists',
						array(
							'table' => 'M_PRODUCT_TYPE',
							'field' => 'PRODUCT_CODE'
						)
					),
					'messages' => array(
						$this->language->_('Can not be empty'),
						//$this->language->_('Invalid Format'),
						$this->language->_('Data too long (min. 1 chars and max. 3 chars)'),
						$this->language->_('Product Code already exists'),
					)
				),

				// 'plan_code'  => array('NotEmpty',
				// 	'Digits',
				// 	new Zend_Validate_StringLength(array('min'=>1,'max'=>4)),
				// 	array	(
				// 		'Db_NoRecordExists',
				// 		array	(
				// 			'table'=>'M_PRODUCT_TYPE',
				// 			'field'=>'PRODUCT_PLAN',
				// 			'exclude'=>$expProdCodeAndName
				// 		)
				// 	),
				// 	'messages' => array(
				// 		$this->language->_('Can not be empty'),
				// 		$this->language->_('Invalid Format'),
				// 		$this->language->_('Data too long (min. 1 chars and max. 4 chars)'),
				// 		$this->language->_('Plan Code already exists'),)
				// ),

				'product_name' => array(
					'NotEmpty',
					new Zend_Validate_Alnum(array('allowWhiteSpace' => true)),
					new Zend_Validate_StringLength(array('max' => 35)),
					array(
						'Db_NoRecordExists',
						array(
							'table' => 'M_PRODUCT_TYPE',
							'field' => 'PRODUCT_NAME',
							// 'exclude'=>$expPlanProdCode
						)
					),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Invalid Bank Name Format (no special characters)'),
						$this->language->_('Data too long (max 35 chars)'),
						$this->language->_('Product Name already exists'),
					)
				)

			);


			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
			if ($zf_filter_input->isValid()) {
				$content = array(
					'PRODUCT_CODE' 	=> $zf_filter_input->product_code,
					// 'PRODUCT_PLAN' 	=> $zf_filter_input->plan_code,
					'PRODUCT_NAME'  => $zf_filter_input->product_name,
					'ACCT_TYPE'  	=> '1',
				);

				try {

					//-------- insert --------------
					$this->_db->beginTransaction();

					$model->insertData($content);
					Application_Helper_General::writeLog('PAIM', 'Add Product Account Type. Product ID : [' . $zf_filter_input->product_code . '], Product Name : [' . $zf_filter_input->product_name . ']');
					$this->_db->commit();

					$this->view->success = true;
					$this->view->error = false;

					$this->_redirect('/notification/success/index');
				} catch (Exception $e) {
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			} else {
				$this->fillParam($zf_filter_input);
				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$errID = 'error_' . $keyRoot;
						$this->view->$errID = $errorString;
					}
				}
				$this->view->error = true;
			}
		}

		Application_Helper_General::writeLog('PAIM', 'Add Product Account Type');
	}

	private function fillParam($zf_filter_input)
	{
		$this->view->product_code = ($zf_filter_input->isValid('product_code')) ? $zf_filter_input->product_code : $this->_getParam('product_code');
		$this->view->plan_code = ($zf_filter_input->isValid('plan_code')) ? $zf_filter_input->plan_code : $this->_getParam('plan_code');
		$this->view->product_name     = ($zf_filter_input->isValid('product_name')) ? $zf_filter_input->product_name : $this->_getParam('product_name');
		$this->view->account_type     = ($zf_filter_input->isValid('account_type')) ? $zf_filter_input->account_type : $this->_getParam('account_type');
	}
}
