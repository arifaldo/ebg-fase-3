<?php

require_once 'Zend/Controller/Action.php';

class Corporateddebitcard_DebitaddController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$attahmentDestination 	= UPLOAD_PATH.'/document/temp';
		$adapter 				= new Zend_File_Transfer_Adapter_Http();

		$field = array(
			'BRANCH_CODE' 		=> 6,
			'BRANCH_NAME' 		=> 50,
			'BANK_ADDRESS' 		=> 140,
		    'CITY_NAME' 		=> 50,
		    'REGION_NAME' 		=> 50,
		    'CONTACT' 		=> 50,
		);
		
		$cust_id = $this->_getParam('cust');
		$REG_NUMBER = $this->_getParam('regid');
		$this->view->regid = $REG_NUMBER;
		$this->view->cust = $cust_id;
		$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							->where('A.CUST_ID = ?',$cust_id)
							->where('A.REG_NUMBER = ?',$REG_NUMBER);
							
		$data = $this->_db->fetchRow($select);			
		if($data['DEBIT_TYPE'] == '1'){
			$data['DEBIT_TYPE'] = 'Corporate';
		}else{
			$data['DEBIT_TYPE'] = 'Card Holder';
		}
		
		if($data['DEBIT_ATM'] == '1'){
			$data['DEBIT_ATM'] = 'On';
		}else{
			$data['DEBIT_ATM'] = 'Off';
		}
		
		if($data['DEBIT_EDC'] == '1'){
			$data['DEBIT_EDC'] = 'On';
		}else{
			$data['DEBIT_EDC'] = 'Off';
		}
		
		
		$this->view->data = $data;

		$this->view->field = $field;
		if($this->_request->isPost())
		{
			$params = $this->_request->getParams();
			
			$adapter->setDestination ($attahmentDestination);

			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
			$extensionValidator->setMessage(
				'Error: Extension file must be *.txt'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
			);
			
			
			
			//var_dump($params);
			$adapter->setValidators(array($extensionValidator,$sizeValidator));

			if($adapter->isValid())
			{

				$success = 1;
				$sourceFileName = $adapter->getFileName();
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				$adapter->addFilter ( 'Rename',$newFileName);
				$adapter->receive();
				//Zend_Debug::dump($newFileName);die;
				$myFile = file_get_contents($newFileName);
				$arry_myFile = explode("\n", $myFile);
				@unlink($newFileName);
				//Zend_Debug::dump($arry_myFile);die;
				if($adapter->receive())
				{
					$this->_db->beginTransaction();
					/*$master = $this->_db->FetchRow(
						$this->_db->select()
							->from('M_DOMESTIC_BANK_TABLE')
							->where('BANK_ISMASTER = 1')
					);*/

					//$this->_db->query('TRUNCATE TABLE M_BRANCH');

					/*if(!empty($master))
					{
						unset($master['BANK_ID']);
						$this->_db->insert("M_DOMESTIC_BANK_TABLE",$master);
					}*/
					//var_dump($arry_myFile);die;
					unset($arry_myFile[0]);
					$change_id = $this->suggestionWaitingApproval('Corporate Debit Card',$info,$this->_changeType['code']['new'],null,'T_DEBITCARD','TEMP_DEBITCARD',$cust_id,$data['CUST_NAME'],$cust_id,$data['CUST_NAME']);
					foreach ($arry_myFile as $row)
					{

						

						//$var['created'] = date('Y-m-d H:i:s');
						//$var['createdby'] = $this->_userIdLogin;

						try
						{
							$var = array();
							if( !empty($row) )
							{
								$var['CHANGES_ID'] = $change_id;
								$var['CUST_ID'] = $cust_id;
								$var['REG_NUMBER'] = $regid;
								$var['DEBIT_NUMBER'] = $row;
								$var['DEBIT_STATUS'] = '3';
								
								$var['DEBIT_SUGGESTEDBY'] = $this->_userIdLogin;
								$var['DEBIT_SUGGESTED'] = new Zend_Db_Expr('now()');
								
 								// echo "<pre>";
								// print_r($var);die;
								$this->_db->insert("TEMP_DEBITCARD",$var);
							}
						}
						catch(Exception $e)
						{
							$success = 0;
						}
					}
				}
				if($success == 1)
				{
					$this->_db->commit();
					Application_Helper_General::writeLog('COIM','Import Debit Card List');
					$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
					$this->_redirect('/notification/submited/index');
				}else{
					$this->_db->rollBack();
				}
			}
			else
			{
				$this->view->errorMsg = $adapter->getMessages();
				Zend_Debug::dump($adapter->getMessages());die;
			}
		}
		Application_Helper_General::writeLog('COIM','Import Debit Card List');
	}
}