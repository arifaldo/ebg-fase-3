<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class charges_SuggestionDetailController extends Application_Main
{
	public function indexAction() 
	{

		$set = new Settings();
		$coa = $set->getSetting('charges_acct_IDR');
		$filters = array( 'COAAccount' => array('StripTags','StringTrim')
							);
		$validators = array(
							'COAAccount' => array('Digits',array('StringLength',array('min'=>10,'max'=>10)),
												'messages' => array($this->getErrorRemark('04','COA Account'),
																	$this->getErrorRemark('04','COA Account Number length must be 10 digits')
																	)
												)	
						);    								  										  							  
		$zf_filters = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		$submit = $this->_getParam('submit');
		$cek = $this->_db->select()
							->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID'))
							->WHERE ('MODULE_ID = ?','COA')
							->query()->FetchAll();												
		if($cek == null){
			if($submit == 'Submit'){			
				$this->_db->beginTransaction();
					if($zf_filters->isValid())
						try{
							$info = 'OLD COA ACCOUNT : '.$coa.'<br />NEW COA ACCOUNT : '.$zf_filters->COAAccount;
							$change_id = $this->suggestionWaitingApproval('Change COA',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','MODULE_ID','COA');
		//					$change_id = 1;
							$data = array(	'CHANGES_ID' => $change_id,
					     					'SETTING_ID' => 'charges_acct_IDR',
											'SETTING_VALUE' => $zf_filters->COAAccount,
					     					'MODULE_ID' => 'COA',
											);
							$this->_db->insert('TEMP_SETTING',$data);
							$this->_db->commit();
					    	$this->view->success = true;						    
							$msg = $this->getErrorRemark('00','Settings Change Request Saved');
							$this->view->report_msg = $msg;
						}
						catch(Exception $e)
						{
								$this->_db->rollBack();					
						}
					else{
						$this->view->error = true;
						$docErr = $this->displayError($zf_filters->getMessages());
						$this->view->report_msg = $docErr;
					}
					
				}			
			}
		else
			{
				$this->view->error = true;
				$this->view->report_msg = ('No changes allowed for this record while awaiting approval for previous change.');
			}
		
//		Zend_Debug::dump($coa);die;                 	
		$this->view->COAAccount = $coa;
	}
}

?>