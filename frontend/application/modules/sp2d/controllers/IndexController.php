<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'Crypt/AESMYSQL.php';






class sp2d_IndexController extends Application_Main
{
	public function initModel()
	{
		$this->model 					= new payment_Model_Payment();
		$this->param['SERVICE_TYPE'] 	= 6;
		$this->param['PROVIDER_TYPE'] 	= 1;
		$this->param['fetch'] 			= 'fetchAll';
		$this->param['USER_ID'] 		= $this->_userIdLogin;
		// 		$this->CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$this->CustomerUser 			= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->userId	= $this->_userIdLogin;

		$SinglePayment 		= new SinglePayment(null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('sp2d');
		$paramSession 		= $sessionNamespace->paramSession;


		
		
		/*
		$complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                       
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );	
        // echo $complist;;die;
         // var_dump($complist);die;
    	$comp = "'";
    	// print_r($complist);die;
    	foreach ($complist as $key => $value) {
    		$comp .= "','".$value['CUST_ID']."','";
    	}
    	$comp .= "'";

		$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 // ->where('A.ACCT_STATUS = ?','5')
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->order('A.APIKEY_ID ASC')
						 // echo $acctlist;
				);
						 // echo $acctlist;die;
		// echo '<pre>';
		// echo $acctlist;
		// print_r($acctlist);die;
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}
		// echo "<pre>";
		// var_dump($account);die;
		// $acct = array();
			$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];

			$acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
			$acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
			$acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
			$acct[$i]['CCY_ID']	=	$value['account_currency'];
			$acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			$i++;
		}
		
		// var_dump($acct);
		$AccArr = $acct;
		*/
		//if(empty($AccArr)){
		
		$paramac = array(
			'CCY_IN' => 'IDR'
		);
		$AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
		//}
		$this->view->AccArr 		= $AccArr;
		
		//roki
		//if back form confirm page 
		if ($paramSession['CONFIRM_PAGE']) {
			// var_dump($paramSession);die;
			$this->view->ACCTSRC = $paramSession['ACCTSRC'];
			$this->view->no_sp2d = $paramSession['no_sp2d'];

			$this->view->sp2dDate = $paramSession['sp2dDate'];
			$this->view->noSpm = $paramSession['noSpm'];
			$this->view->spmDate = $paramSession['spmDate'];
			$this->view->skpdName = $paramSession['skpdName'];
			$this->view->sppType = $paramSession['sppType'];
			$this->view->PS_SUBJECT = $paramSession['PS_SUBJECT'];
			$this->view->ACBENEF = $paramSession['ACBENEF'];
			$this->view->ACBENEF_NAME = $paramSession['ACBENEF_NAME'];
			$this->view->ACBENEF_IDENTITY = $paramSession['ACBENEF_IDENTITY'];
			$this->view->transactionPurpose = $paramSession['transactionPurpose'];
			$this->view->description = $paramSession['description'];
			$this->view->actualAmount = $paramSession['actualAmount'];
			$this->view->deductionAmount = $paramSession['deductionAmount'];
			$this->view->totalAmount = $paramSession['totalAmount'];
			$this->view->clickedsearch = $paramSession['clickedsearch'];

			$this->view->TrfDateType = $paramSession['tranferdatetype'];
			$this->view->notif = $paramSession['notif'];
			$this->view->tempfile = $paramSession['tempfile'];
			if ($paramSession['notif'] == "2") {
				$this->view->email = $paramSession['email'];
				$this->view->sms = $paramSession['sms'];
			}

			$param = $paramSession;
			$param['CONFIRM_PAGE'] = false;
			$sessionNamespace->paramSession	= $param;
		}
		// $sessionNamespace 	= new Zend_Session_Namespace('newtax');
		// $billingIdType 		= $sessionNamespace->billingIdType;

		// $this->view->billingIdType = $billingIdType;

		//added repair purchase payment - begin

		$periodicEveryArr = array(
			'1' => $this->language->_('Monday'),
			'2' => $this->language->_('Tuesday'),
			'3' => $this->language->_('Wednesday'),
			'4' => $this->language->_('Thursday'),
			'5' => $this->language->_('Friday'),
			'6' => $this->language->_('Saturday'),
			'7' => $this->language->_('Sunday'),
		);
		$periodicEveryDateArr = range(1, 28);
		$periodicEveryDateArr['31'] = $this->language->_('End of month');
		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;

		// $data = $this->model->getProviderName($paramSession['operator'],1);
		// 		print_r($data);die;
		// $paramSession['operatorNama'] = $data['PROVIDER_NAME'];
		// die('here');
		$this->view->token = false;
		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];
			// var_dump($tokenIdUser);
			// var_dump($tokendata);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			// var_dump($tokendata);
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		//$this->view->googleauth = true;

		//templatedetail - begin
		$htmldataDetailDetail = '';
		foreach ($paramSession['dataUi'] as $datadetail => $key) {
			//Zend_Debug::dump($key);

			if ($datadetail == 'total_amount') {
			} else {

				$htmldataDetailDetail .= '
			<tr>
				<td class="tbl-evencontent">' . $this->language->_($datadetail) . '</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">' . $key . '</td>
			</tr>
			';
			}
		}

		if (isset($htmldataDetailDetail)) $this->view->templateDetail = $htmldataDetailDetail;
		//templatedetail - end

		//select m_user - begin
		$userData = $this->CustomerUser->getUser($this->_userIdLogin);

		//added token type
		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];
		$tokenIdUser 				= $userData['TOKEN_ID'];
		$tokenType 					= $userData['TOKEN_TYPE'];
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);

		$paramSession['TOKEN_ID'] 	= $tokenIdUser;
		$paramSession['PS_EFDATE'] 	= $PS_EFDATE;
		$paramSession['date'] 		= $date;

		$paramSession['dateDisplayFormat'] 	= $this->_dateDisplayFormat;
		$paramSession['dateDBFormat'] 		= $this->_dateDBFormat;

		//ADDED PRIV
		$paramSession['priv1'] 	= $this->view->hasPrivilege('CTSP');
		$paramSession['priv2'] 	= NULL;
		$paramSession['priv3']	= NULL;
		$paramSession['confirmPage']	= 1; //harus ada kalau tidak $this->_isConfirm false & cek benef

		$filter 		= new Application_Filtering();
		
		$sessionNameEnc = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNameEnc->token;

		// get repair - begin
		$AESMYSQL 	= new Crypt_AESMYSQL();
		$PS_NUMBER 	= urldecode($filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER"));
		$PS_NUMBER 	= $AESMYSQL->decrypt($PS_NUMBER, $password);
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER)) ? $PS_NUMBER : '';

		if (!empty($PS_NUMBER)) {
			$paramSession['isRepair'] = true;
		} else {
			$paramSession['isRepair'] = false;
		}

		// get ps_number data for repair payment
		if (!empty($PS_NUMBER) && !$this->_request->isPost()) {
			$paramList = array(
				"WA" 			=> false,
				"ACCOUNT_LIST" 	=> $this->_accountList,
				"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
			);
			$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$select   = $CustomerUser->getPayment($paramList, false);
			$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

			$select->columns(
				array(
					"tra_message"		=> "T.TRA_MESSAGE",
					"tra_refno"			=> "T.TRA_REFNO",
					"acbenef_email"		=> "T.BENEFICIARY_EMAIL",
					"acbenef_bankname"	=> "T.BENEFICIARY_ACCOUNT_NAME",
					"acbenef_alias"		=> "T.BENEFICIARY_ALIAS_NAME",
				)
			);

			$pslipData 	= $this->_db->fetchRow($select);
			
			if (!empty($pslipData)) {
				$PS_EFDATE_ORI  	= date("d/m/Y", strtotime($pslipData['efdate']));
				$PS_SUBJECT 		= $pslipData['paySubj'];

				if ($PS_EFDATE_ORI > date('d/m/Y')) {
					$TransferDate 	=  "2"; //future
					$PS_EFDATE = $PS_EFDATE_ORI;
				} else {
					$TransferDate	=  "1"; //imediate
					$PS_EFDATE = date('d/m/Y');
				}

				$TRA_AMOUNT  		= Application_Helper_General::displayMoney($pslipData['amount']);
				$TOTAL_AMOUNT		= $pslipData['TRA_AMOUNT'] - $pslipData['TOTAL_CHARGES'];
				$TRA_MESSAGE 		= $pslipData['tra_message'];			
				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE'];

				$this->view->ACCTSRC = $pslipData['SOURCE_ACCOUNT'];
				$this->view->no_sp2d = $pslipData['SP2D_NO'];

				$this->view->sp2dDate = Application_Helper_General::convertDate($pslipData['SP2D_DATE'], $this->_dateViewFormat);
				$this->view->noSpm = $pslipData['SPM_NO'];
				$this->view->spmDate = Application_Helper_General::convertDate($pslipData['SPM_DATE'], $this->_dateViewFormat);
				$this->view->skpdName = $pslipData['SKPD_NAME'];
				$this->view->sppType = $pslipData['SPP_TYPE'];
				$this->view->PS_SUBJECT = $pslipData['PS_SUBJECT'];
				$this->view->ACBENEF = $pslipData['BENEFICIARY_ACCOUNT'];
				$this->view->ACBENEF_NAME = $pslipData['BENEFICIARY_ACCOUNT_NAME'];
				$this->view->ACBENEF_IDENTITY = $pslipData['BENEFICIARY_ID_NUMBER'];
				$this->view->transactionPurpose = $pslipData['LLD_TRANSACTION_PURPOSE'];
				$this->view->description = $pslipData['LLD_DESC'];
				$this->view->tempfile = $pslipData['PS_FILE'];
				
				$this->view->actualAmount = $TRA_AMOUNT;
				$this->view->deductionAmount = Application_Helper_General::displayMoney($pslipData['TOTAL_CHARGES']);
				$this->view->totalAmount = Application_Helper_General::displayMoney($TOTAL_AMOUNT);
				$this->view->clickedsearch = 1;

				// $this->view->TrfDateType = $pslipData['tranferdatetype'];
				// $this->view->notif = $pslipData['notif'];

				// if ($pslipData['notif'] == "2") {
				// 	$this->view->email = $pslipData['email'];
				// 	$this->view->sms = $pslipData['sms'];
				// }				

				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"]) {
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				} elseif ($PS_TYPE != $this->_paymenttype["code"]["disbursement"])	// 0: PB
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
			} else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
		}
		//added cms purchase payment get repair - end
		

		if ($this->_getParam('submit1') == TRUE) {

			$inputtoken1 		= $this->_getParam('inputtoken1');
			$inputtoken2 		= $this->_getParam('inputtoken2');
			$inputtoken3 		= $this->_getParam('inputtoken3');
			$inputtoken4 		= $this->_getParam('inputtoken4');
			$inputtoken5 		= $this->_getParam('inputtoken5');
			$inputtoken6 		= $this->_getParam('inputtoken6');

			$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;
			$filters = array(
				'ACCTSRC'         => array('StripTags', 'StringTrim'),
				'paymentSubject'        => array('StripTags', 'StringTrim'),
			);

			$validators =  array(
				'ACCTSRC'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
				'no_sp2d'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

			//clickd search button validation
			$clickedSearch = true;
			if ($this->_getParam('clickedsearch') != '1') {
				$clickedSearch = false;
			}

			//check makerlimit, dailylimit
			if ($this->_getParam('totalAmount') !== '' && $this->_getParam('ACCTSRC') !== '') {
				$amount = preg_replace("/([^0-9\\.])/i", "", $this->_getParam('totalAmount'));
				$createLimit 	= $this->validateLimit($amount, 'IDR', $this->_getParam('ACCTSRC'), $msgg);
			}

			//check privilege relase
			$this->view->error = false;
			if ($this->_custSameUser) {
				if (!$this->view->hasPrivilege('PRLP')) {
					// die('here');
					$errMessage = $this->language->_("Error: You don't have privilege to release payment");
					$this->view->error = true;
					$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
				}
			}

			if ($zf_filter_input->isValid()  && $createLimit === true && $this->view->error==false) {

				
				$param['ACCTLIST']		= $AccArr;
				$param['PS_NUMBER']		= $this->_getParam('PS_NUMBER');
				$param['ACCTSRC']		= $this->_getParam('ACCTSRC');
				$param['no_sp2d'] 		= $this->_getParam('no_sp2d');

				$param['sp2dDate'] 		= $this->_getParam('sp2dDate');
				$param['noSpm']			= $this->_getParam('noSpm');
				$param['spmDate'] 		= $this->_getParam('spmDate');

				//date for confirmation

				//
				$param['skpdName']				= $this->_getParam('skpdName');
				$param['sppType'] 				= $this->_getParam('sppType');
				$param['PS_SUBJECT']			= $this->_getParam('PS_SUBJECT');
				$param['ACBENEF'] 				= $this->_getParam('ACBENEF');
				$param['ACBENEF_NAME']			= $this->_getParam('ACBENEF_NAME');
				$param['ACBENEF_IDENTITY']		= $this->_getParam('ACBENEF_IDENTITY');
				$param['transactionPurpose']	= $this->_getParam('transactionPurpose');
				$param['description']			= $this->_getParam('description');
				$param['actualAmount']			= $this->_getParam('actualAmount');
				$param['deductionAmount']		= $this->_getParam('deductionAmount');
				$param['totalAmount']			= $this->_getParam('totalAmount');
				$param['PS_FILE']				= $this->_getParam('tempfile');
 
				$param['clickedsearch']			= $this->_getParam('clickedsearch');

				$param['tranferdatetype'] = $this->_getParam('tranferdatetype');
				if ($this->_getParam('tranferdatetype') == "1") {
					$param['PS_EFDATE'] = $this->_getParam('immediateTransfer');
					$param['date']		= $this->_getParam('immediateTransfer');
				} elseif ($this->_getParam('tranferdatetype') == "2") {
					$param['PS_EFDATE'] = $this->_getParam('PS_FUTUREDATE');
					$param['date'] 		= $this->_getParam('PS_FUTUREDATE');
				} else {
					if ($this->_getParam('tranferdateperiodictype') == '5') {
						$param['periodicValue'] = $this->_getParam('PERIODIC_EVERY');
						$dateNow = mktime(0, 0, 0, date("n"), date("j"), date("Y"));
						$every = (int) $this->_getParam('PERIODIC_EVERY');
						$d = date("w", $dateNow);

						$addDay = $every;
						if ($every > $d) {
							$addDay = $addDay;
						} else {
							$addDay = $addDay + 7;
						}
						$nextDate  =  (int) $addDay - (int) $d;
						$NEXT_DATE = date("Y-m-d", strtotime("+$nextDate day"));
						$PERIODIC_EVERY_VAL = $this->_getParam('PERIODIC_EVERY');
					} else {
						$param['periodicValue'] = $this->_getParam('PERIODIC_EVERYDATE');
						$dateNextMonth = mktime(0, 0, 0, date("n"), date("j") + 1, date("Y"));

						$dateNow = date("j");
						$maxDays = date('t', $dateNextMonth);
						//echo $maxDays; die;
						$every = (int) $this->_getParam('PERIODIC_EVERYDATE');

						if ($every > $dateNow) {
							$addMonth = 0;
						} else {
							$addMonth = 1;
						}

						if ($maxDays >=  $every) {
							$every = $every;
						} else {
							$every = $maxDays;
						}

						$nextDate = mktime(0, 0, 0, date("n") + $addMonth, $every, date("Y"));
						$NEXT_DATE = date("Y-m-d", $nextDate);
						$PERIODIC_EVERY_VAL = $this->_getParam('PERIODIC_EVERYDATE');
					}
					$param['END_DATE'] = join('-', array_reverse(explode('/', $this->_getParam('PS_ENDDATEPERIODIC'))));
					$param['PS_ENDDATEPERIODIC'] = $this->_getParam('PS_ENDDATEPERIODIC');
					$param['PS_EFDATE'] = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
					$param['tranferdateperiodictype']			= $this->_getParam('tranferdateperiodictype');
				}

				$param['notif'] = $this->_getParam('notif');
				if ($this->_getParam('notif') == "1") {
					$param['email'] = $this->_getParam('email');
					$param['sms'] = $this->_getParam('sms');
				}
				 
				$param['PS_EFDATE'] = Application_Helper_General::convertDate($param['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
				$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO' => $param['ACCTSRC']));
				$param['ACCT_CCY'] = $detailCustomer['CCY_ID'];
				$param['ACCT_NAME'] = $detailCustomer['ACCT_NAME'];
				$param['ACCT_ALIAS_NAME'] = $detailCustomer['ACCT_ALIAS_NAME'];
				$param['ACCT_TYPE'] = $detailCustomer['ACCT_TYPE'];

				$param['SERVICE_TYPE']	= '6';
				$param['operator']	= '1158';
				$param['provInquiry']	= 'N';
				$param['PS_CCY']	= 'IDR';
				$param['ACBENEF_CCY'] = 'IDR';
				$param['CUST_ID']	= $this->_custIdLogin;
				$param['USER_ID']	= $this->_userIdLogin;
				$param['PS_CATEGORY'] = 'SINGLE PAYMENT';
				$param['operatorNama'] = 'MPN';
				$param['dateDisplayFormat'] 	= $this->_dateDisplayFormat;
				$param['dateDBFormat'] 		= $this->_dateDBFormat;

				$currentDate = date("Ymd");
				$randomdigit = str_pad(mt_rand(0, 99999), 8, '0', STR_PAD_LEFT);
				// $rand = rand(10,100);
				$param['orderId'] = $currentDate . $randomdigit;
				
				// Zend_Debug::dump($param, "param"); die;

				$sessionNamespace->paramSession 	= $param;
				$this->_redirect('/sp2d/index/confirm');
				////
				// if ($this->_custSameUser) {
				// 	$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
				// 	$verToken 	= $Token->verify($challengeCode, $responseCode);

				// 	if ($verToken['ResponseCode'] != '00') {
				// 		$tokenFailed = $CustUser->setLogToken(); //log token activity

				// 		$error = true;
				// 		$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

				// 		if ($tokenFailed === true) {
				// 			$this->_redirect('/default/index/logout');
				// 		}
				// 	}
				// 	if (!$error) {
				// 		$sendProvider 		= new SinglePayment(null, $this->_userIdLogin);
				// 		$resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg, $resWS);
				// 	} else {
				// 		$resProvider = false;
				// 	}
				// } else {
				// 	$sendProvider 		= new SinglePayment(null, $this->_userIdLogin);
				// 	$resProvider 		= true;
				// }
				// if($msg == 'force_logout'){
				// 	$this->_forward('home'); //harus ketendan kareng salah token sebanyak
				// 	//$this->_redirect('/home');
				// }
			} else {
				$this->view->ACCTSRC   = ($zf_filter_input->isValid('ACCTSRC')) ? $zf_filter_input->ACCTSRC : $this->_getParam('ACCTSRC');
				$this->view->no_sp2d   = ($zf_filter_input->isValid('no_sp2d')) ? $zf_filter_input->no_sp2d : $this->_getParam('no_sp2d');

				$this->view->sp2dDate   = ($zf_filter_input->isValid('sp2dDate')) ? $zf_filter_input->sp2dDate : $this->_getParam('sp2dDate');
				$this->view->noSpm   = ($zf_filter_input->isValid('noSpm')) ? $zf_filter_input->noSpm : $this->_getParam('noSpm');
				$this->view->spmDate   = ($zf_filter_input->isValid('spmDate')) ? $zf_filter_input->spmDate : $this->_getParam('spmDate');
				$this->view->skpdName   = ($zf_filter_input->isValid('skpdName')) ? $zf_filter_input->skpdName : $this->_getParam('skpdName');
				$this->view->sppType   = ($zf_filter_input->isValid('sppType')) ? $zf_filter_input->sppType : $this->_getParam('sppType');
				$this->view->PS_SUBJECT   = ($zf_filter_input->isValid('PS_SUBJECT')) ? $zf_filter_input->PS_SUBJECT : $this->_getParam('PS_SUBJECT');
				$this->view->ACBENEF   = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
				$this->view->ACBENEF_NAME   = ($zf_filter_input->isValid('ACBENEF_NAME')) ? $zf_filter_input->ACBENEF_NAME : $this->_getParam('ACBENEF_NAME');
				$this->view->ACBENEF_IDENTITY   = ($zf_filter_input->isValid('ACBENEF_IDENTITY')) ? $zf_filter_input->ACBENEF_IDENTITY : $this->_getParam('ACBENEF_IDENTITY');
				$this->view->transactionPurpose   = ($zf_filter_input->isValid('transactionPurpose')) ? $zf_filter_input->transactionPurpose : $this->_getParam('transactionPurpose');
				$this->view->description   = ($zf_filter_input->isValid('description')) ? $zf_filter_input->description : $this->_getParam('description');
				$this->view->actualAmount   = ($zf_filter_input->isValid('actualAmount')) ? $zf_filter_input->actualAmount : $this->_getParam('actualAmount');
				$this->view->deductionAmount   = ($zf_filter_input->isValid('deductionAmount')) ? $zf_filter_input->deductionAmount : $this->_getParam('deductionAmount');
				$this->view->totalAmount   = ($zf_filter_input->isValid('totalAmount')) ? $zf_filter_input->totalAmount : $this->_getParam('totalAmount');

				$error = $zf_filter_input->getMessages();

				//if (!$clickedSearch) {
				//	$this->view->error_clickedsearch = 'Please click the search button';
				//}
				//$this->view->clickedsearch   = ($zf_filter_input->isValid('clickedsearch')) ? $zf_filter_input->clickedsearch : $this->_getParam('clickedsearch');
				//format error utk ditampilkan di view html 
				$errorArray = null;
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$errorArray[$keyRoot] = $errorString;
					}
				}
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$keyname = "error_" . $keyRoot;
						// print_r($keyname);die;
						$this->view->$keyname = $this->language->_($errorString);
					}
				}

				if ($createLimit === true) {
				} else {
					if ($createLimit !== null) {
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $createLimit;
					}
				}
			}
		} else if ($this->_getParam('submit') == true) {
			$this->_redirect('/home/dashboard');
		}
		// die('here');
		Application_Helper_General::writeLog('MTAX', 'Create SP2D');
	}

	public function confirmAction()
	{
		$sessionNamespace 					= new Zend_Session_Namespace('sp2d');
		$paramSession 						= $sessionNamespace->paramSession;

		$PS_NUMBER							= $paramSession['PS_NUMBER'];
		// echo "<pre>";
		// 	var_dump($paramSession);die;
		// $this->view->ERROR_MSG_N			= $paramSession['paymentMessage'];

		$this->view->token = false;
		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];
			// var_dump($tokenIdUser);
			// var_dump($tokendata);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			// var_dump($tokendata);
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		if ($this->_request->isPost()) {
			$paramSession['sp2dDate'] 			= date('Y-m-d', strtotime($paramSession['sp2dDate']));
			$paramSession['spmDate'] 			= date('Y-m-d', strtotime($paramSession['spmDate']));
			if($paramSession['deductionAmount'] == ''){
				$paramSession['deductionAmount'] = 0;
			}
			$paramSession['actualAmount']		= Application_Helper_General::convertDisplayMoney($paramSession['actualAmount']);
			$paramSession['deductionAmount']	= Application_Helper_General::convertDisplayMoney($paramSession['deductionAmount']);
			$paramSession['totalAmount']		= Application_Helper_General::convertDisplayMoney($paramSession['totalAmount']);

			if ($this->_getParam('submit1') == TRUE) {
				//back button
				$param = $paramSession;
				$param['CONFIRM_PAGE'] = true;
				$sessionNamespace->paramSession 	= $param;
				$this->_redirect('/sp2d/index');
			}

			if ($this->_custSameUser) {
				// var_dump($tokenGoogle);
				// die('here');

				$inputtoken1 		= $this->_getParam('inputtoken1');
				$inputtoken2 		= $this->_getParam('inputtoken2');
				$inputtoken3 		= $this->_getParam('inputtoken3');
				$inputtoken4 		= $this->_getParam('inputtoken4');
				$inputtoken5 		= $this->_getParam('inputtoken5');
				$inputtoken6 		= $this->_getParam('inputtoken6');

				$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;
				
				if (!empty($tokenGoogle)) {

					$pga = new PHPGangsta_GoogleAuthenticator();
					// var_dump($data2['GOOGLE_CODE']);
					$setting 		= new Settings();
					$google_duration 	= $setting->getSetting('google_duration');
					$resultcapca = $pga->verifyCode($tokenGoogle, $responseCode, $google_duration);
					// var_dump($resultcapca);
					//  	var_dump($responseCode);
					//  	var_dump($tokenGoogle);die;
					if ($resultcapca) {
						$resultToken = $resHard['ResponseCode'] == '0000';
					} else {
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$error = true;
						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Google Auth Code');	//$verToken['ResponseDesc'];
						$this->view->ERROR_MSG =  $this->language->_('Invalid Google Auth Code');

						// if ($tokenFailed === true)
						// {
						// 	$this->_redirect('/default/index/logout');
						// }
					}
				} else {

					$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$verToken 	= $Token->verify($challengeCode, $responseCode);

					if ($verToken['ResponseCode'] != '00') {
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$error = true;
						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
						$this->view->ERROR_MSG =  $this->language->_('Invalid Response Code');

						if ($tokenFailed === true) {
							$this->_redirect('/default/index/logout');
						}
					}
				}
			}

			if ($error==false) {
				// $sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
				// $resProvider	 = $sendProvider->createPaymentPurchase($paramSession, $msg);
				$SinglePayment = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
				
				if(!empty($paramSession['ACCTLIST'])){
					foreach($paramSession['ACCTLIST'] as $valacct){
							if($valacct['ACCT_NO'] == $paramSession['ACCTSRC']){
								$paramSession['ACBENEF_BANKCODE'] = $valacct['BANK_CODE'];
								$paramSession['BENEFICIARY_BANKCODE'] = $valacct['BANK_CODE'];
								$paramSession['ACCT_CCY'] = $valacct['CCY_ID'];
								
							}
					}
				}
				  
				if (!empty($PS_NUMBER)) {
					$SinglePayment->isRepair = true;
				}
				//echo '<pre>';
				//var_dump($paramSession);die;
				$result = $SinglePayment->createPaymentSp2d($paramSession);
				
				if ($this->_custSameUser) {

					$paramSQL = array(
						"WA" 				=> false,
						"ACCOUNT_LIST" 	=> $this->_accountList,
						"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
					);

					// get payment query
					$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
					$select   = $CustUser->getPayment($paramSQL);
					$select->where('P.PS_NUMBER = ?', (string) $result);
					// echo $select;
					$pslip = $this->_db->fetchRow($select);
					$settingObj = new Settings();
					$setting = array(
						"COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
						"COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
						"COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
						"COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
						"COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
						'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
						"range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
						"auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
						"_dateFormat" 			=> $this->_dateDisplayFormat,
						"_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
						"_transfertype" 		=> array_flip($this->_transfertype["code"]),
					);

					$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
					$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

					$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
					$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

					$app = Zend_Registry::get('config');
					$appBankname = $app['app']['bankname'];

					$selectTrx = $this->_db->select()
						->from(
							array('TT' => 'T_TRANSACTION'),
							array(
								'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
								'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
								'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																	CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																		 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																	END"),
								//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
								'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
								'ACBENEF_NAME'			=> new Zend_Db_Expr("
																		CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
								'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
								'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
								'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
								'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
								'TRA_REFNO'				=> 'TT.TRA_REFNO',
								'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
								'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
								'TRA_STATUS'			=> 'TT.TRA_STATUS',
								'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
								'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
								'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
								'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
								'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
								'CLR_CODE'				=> 'TT.CLR_CODE',
								'TT.RATE',
								'TT.PROVISION_FEE',
								'TT.NOSTRO_NAME',
								'TT.FULL_AMOUNT_FEE',
								'C.PS_CCY', 'C.CUST_ID',
								'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
								'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
								'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
								'BANK_NAME'				=> new Zend_Db_Expr("
																		CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																		WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																		 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																			 ELSE TT.BENEFICIARY_BANK_NAME
																		END"),
								'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
																	FROM T_PERIODIC_DETAIL Y
																	inner join T_PSLIP Z
																	on Y.PS_PERIODIC = Z.PS_PERIODIC
																	where
																	Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
							)
						)
						->joinLeft(array('C' => 'T_PSLIP'), 'C.PS_NUMBER = TT.PS_NUMBER', array())
						->where('TT.PS_NUMBER = ?', $result);
					// echo $selectTrx;
					$paramTrxArr = $this->_db->fetchAll($selectTrx);

					$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $result);
					$paramPayment = array_merge($pslip, $setting);
					// echo '<pre>';
					// print_r($paramPayment);
					// print_r($paramTrxArr);
					// die;
					$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
					$infoWarnOri = (!empty($check['infoWarning']) ? '*) ' . $check['infoWarning'] : '');
					$sessionNameConfrim->infoWarnOri = $infoWarnOri;

					if ($validate->isError() === true) {
						$error = true;
						$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
					}

					$Payment = new Payment($result, $this->_custIdLogin, $this->_userIdLogin);
					// if ($this->_hasPriviReleasePayment){
					$resultRelease = $Payment->releasePayment();
					// print_r($resultRelease);
					$this->view->ps_numb = $result;
					$this->view->hidetoken = true;
					if ($resultRelease['status'] == '00') {
						$ns = new Zend_Session_Namespace('FVC');
						$ns->backURL = $this->view->backURL;
						$this->view->releaseresult = true;
						// $this->_redirect('/notification/success/index');
					} else {
						$this->view->releaseresult = false;
						$this->_helper->getHelper('FlashMessenger')->addMessage($result);
						//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
						//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
						$this->_redirect('/notification/index/release');
					}
					// }
				}

				// var_dump($resProvider);die;
				if ($result) {
					$ns = new Zend_Session_Namespace('FVC');
					$ns->backURL = '/sp2d';
					$this->_redirect('/notification/success/index');
				}
			}else{

			}
			
		}
		

		$periodicEveryArr = array(
			'1' => $this->language->_('Monday'),
			'2' => $this->language->_('Tuesday'),
			'3' => $this->language->_('Wednesday'),
			'4' => $this->language->_('Thursday'),
			'5' => $this->language->_('Friday'),
			'6' => $this->language->_('Saturday'),
			'7' => $this->language->_('Sunday'),
		);
		$periodicEveryDateArr = range(1, 28);
		$periodicEveryDateArr['31'] = $this->language->_('End of month');
		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;
		
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO' => $paramSession['ACCTSRC']));
		if(empty($detailCustomer)){
		$sourceList = $this->_db->fetchAll(
											$this->_db->select()
												->from(array('A' => 'M_APIKEY'), array('*'))
												->where("A.VALUE = ? ", $paramSession['ACCTSRC'])
												->where("A.CUST_ID = ? ", $this->_custIdLogin)
												->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
												->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
										);

		foreach ($sourceList as $key => $value) {
			$detailCustomer[$value['FIELD']] = $value['VALUE'];
			$detailCustomer['bank_code'] = $value['BANK_CODE'];
			$detailCustomer['bank_name'] = $value['BANK_NAME'];
			$detailCustomer['CCY_ID'] = $value['account_currency'];
			$detailCustomer['ACCT_NAME'] = $value['account_name'];
			$detailCustomer['ACCT_ALIAS_NAME'] = $value['account_alias'];
			$detailCustomer['ACCT_TYPE'] = '-';
		}
			
		}
										
		
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];
		// if (!$paramSession) {
		// 	$this->cancel();
		// }
		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		$this->view->paramSession			= $paramSession;

		Application_Helper_General::writeLog('MTAX', 'SP2D Confirmation');
	}

	public function bulkAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
		$this->view->AccArr 		= $AccArr;
	}

	public function cancel()
	{
		unset($_SESSION['sp2d']);
		$this->_redirect("/home/index");
	}
	public function validateLimit($amount, $ccy, $account, &$msg1)
	{
		$validate   = new ValidatePayment($this->_custIdLogin, $this->_userIdLogin);
		$resultValMakerlimit	= $validate->checkAmountACCSRC($amount, $ccy, $account);
		$resultValDailylimit	= $validate->checkAmountCCY($amount, $ccy);
		$minAmt 				= $this->validateMinAmt($ccy, $amount);

		if ($resultValDailylimit === TRUE) {
			if ($resultValMakerlimit === TRUE) {
				if ($minAmt === TRUE) {
					return TRUE;
				} else {
					$msg1 = $minAmt;
					return $msg1;
				}
			} else {
				$msg1 = $resultValMakerlimit;
				return $msg1;
			}
		} else {
			$msg1 = $resultValDailylimit;
			return $msg1;
		}
	}
	
	
	public function getstrAction()
    {
	 	 
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
         
		$valid_extensions = array('jpeg', 'jpg', 'png' , 'pdf' ); // valid extensions
		$path = UPLOAD_PATH . '/document/temp/';
		//echo json_encode($_FILES['file']);die; 
		$img = $_FILES['file']['name'];
		$tmp = $_FILES['file']['tmp_name'];
		//echo json_encode($_FILES);die;
		$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
		$final_image = rand(1000,1000000).$img;
		
		if(in_array($ext, $valid_extensions)) 
		{ 
	
		$filePath = $path.strtolower($final_image); 
		//echo json_encode($_FILES['file']);
		$result = move_uploaded_file($tmp,$filePath); 
		//echo $result;
		}
		//echo $filePath;
		//echo $ext;die;
		/*
		if($ext == 'pdf'){
			 
			//$target = "converted.jpg";
			//shell_exec('/usr/local/bin/convert "'.$_FILES .'" -colorspace RGB -resize 800 "'.$target.'"', $output, $response);
			 //$pdf = 'testfile.pdf';
			//$save = 'output.png';
			//shell_exec('/usr/local/bin/convert "'.$filePath.'" -colorspace RGB -resize 800 "'.$save.'"', $output, $return_var);
			//echo $output;die;
			
			try{
			//$imagick = new Imagick();
			$imagick = new Imagick();
			$imagick->setResolution(288,288);
			$imagick->readImage($filePath);
			//$imagick->setResolution( 300, 300 );
			//$imagick->setResolution( 595, 842 );
			$imagick->setImageFormat('png');
			$imagick->setSize(1700,2000);
			$imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
			//$imagick->setImageFormat( "png" );
			//$result = $imagick->writeImage('newfilename.png');
			//$imagick->setImageCompression(imagick::COMPRESSION_PNG); 
			$imagick->setImageCompressionQuality(100);
			// -flatten option, this is necessary for images with transparency, it will produce white background for transparent regions
			//$imagick = $imagick->flattenImages();
			$imagick->setImageFilename('writeImage.png');
  
// Write the image
			$imagick->writeImage($path.$final_image.".png"); 
			$filePath = $path.$final_image.".png";
			//$imagick->writeImage();
			header('Content-Type: image/'.$imagick->getImageFormat());
			//echo $imagick;
			//var_dump($result);
			}catch (Exception $e) {
				echo $e->getMessage();
				die();
			}
			
			
		}
		*/
		//echo 'here';die;
		//var_dump($response);
		//var_dump($output);die;
		
		
		$uploadFieldName = 'data';
		 
		$postField = array();
        $tmpfile = $_FILES['file']['tmp_name'];
        $filename = basename($_FILES['file']['name']);
        $postField['data'] =  curl_file_create($tmpfile, $_FILES['file']['type'], $filename);
        $headers = array("Content-Type" => "multipart/form-data");
		//echo json_encode($_FILES); 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL, '192.168.86.150:1234/read/file');
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//echo $filePath;
		//echo $final_image;die;
		if(function_exists('curl_file_create')){
			
			$filePath = curl_file_create($filePath,'application/pdf',$final_image);
			
		} else{ 
		
			$filePath = '@' . realpath($filePath);
			curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
		}
		
		$postFields = array(
			$uploadFieldName => $filePath
	//		$uploadFieldName => realpath($tmp) 
		); 
		//echo json_encode($postFields);die; 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

		$response = curl_exec($ch);
		
		$result = json_decode($response);
		$result->filename = $final_image;
		//$response->filename = $final_image;
		//$data = array('filename' => $final_image);
		//array_push($result, $data);
		
		
		

		if(curl_errno($ch)){
			
			echo curl_error($ch);die;
		} 
	//die('here');
		echo json_encode($result);die;
		
		
	/*
		// Connecting to website.
		$ch = curl_init();
 
		$data = array('data' => $fp); 
		
		foreach($data as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');
		
		curl_setopt($ch, CURLOPT_URL, '192.168.86.150:1234/read/image' . $strFileName);
		curl_setopt($ch,CURLOPT_POST, count($data));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_UPLOAD, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 300); // 1 Day Timeout
		//curl_setopt($ch, CURLOPT_INFILE, $fp);
		//curl_setopt($ch, CURLOPT_POST,1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_NOPROGRESS, false);
		curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, 'CURL_callback');
		curl_setopt($ch, CURLOPT_BUFFERSIZE, 128);
		curl_setopt($ch, CURLOPT_INFILESIZE, filesize($localFile));
		//curl_exec ($ch);
		$response = curl_exec($ch);
		$result = json_decode($response);
		if (curl_errno($ch)) {

			$msg = curl_error($ch);
		}
		else {

			$msg = $result;
		}

		//curl_close ($ch);

		$return = array('msg' => $msg);
		*/

		echo json_encode($return);
    }
	
	
	public function validateMinAmt($ccy, $amount = null)
	{
		if (Zend_Registry::isRegistered('language')) {
			$language = Zend_Registry::get('language');
		}
		$select = $this->_db->select()
			->from(array('M_MINAMT_CCY'), array('CCY_ID', 'MIN_TX_AMT'))
			->where('CCY_ID = ?', $ccy);

		$select = $this->_db->fetchRow($select);

		$minAmt = $select['MIN_TX_AMT'];
		$checked = true;

		if ($amount < $minAmt) {
			$checked = 	$language->_('Amount cannot be lower than') . " " . $select['CCY_ID'] . " " . Application_Helper_General::displayMoney($minAmt) . ".";
		}

		return $checked;
	}
}
