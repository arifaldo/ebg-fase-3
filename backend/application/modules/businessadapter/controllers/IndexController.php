<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';
class businessadapter_IndexController extends Application_Main {


    
    public function indexAction()
    { 
        $this->_helper->_layout->setLayout('newlayout');

        //h = header || d = detail
        $select = $this->_db->select()
                            ->from(array('H' => 'M_ADAPTER_PROFILE'),array('*'));

        $profileData = $this->_db->fetchAll($select);

        $this->view->profileData = $profileData;

        Application_Helper_General::writeLog('PFAD','View Business Profile Adapter List');

        $delete     = $this->_getParam('delete');
        $arrStatus = array(
                    '1' => 'Active', 
                    '2' => 'Suspended',
                    '3' => 'Deleted'
        );
        $this->view->statusArr = $arrStatus;

        if($delete)
        { 
            $profileid = $this->_request->getParam('profileid');

            $validators = array (
                                    'profileid' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'profileid' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            foreach ($profileid as $key => $value) {
                if(!empty($value)){
                    $success = true;
                }
            }
			//var_dump($profileid);die;
            $select = $this->_db->select()
                                    ->from(array('H' => 'M_CUST_ADAPTER_PROFILE'),array('*'));

             $select->where('H.ADAPTER_PROFILE_ID IN (?) ', $profileid);
    
             $profileCust = $this->_db->fetchAll($select);
             if(!empty($profileCust)){
                 $success = false;
                 $msg = 'Adapter Profile is being used by customers';
             }

            if($zf_filter_input->isValid() && $success)
            {

                try
                {
                    foreach ($profileid as $key => $value)
                    {

                        $id = $value;

                        //unlink files
                        $select = $this->_db->select()
                                    ->from(array('H' => 'M_ADAPTER_PROFILE'),array('*'));

                        $select->where('H.PROFILE_ID = '. $this->_db->quote($id));

                        $profileData = $this->_db->fetchAll($select);

                        $file_path = $profileData[0]['FILE_PATH'];
                        @unlink($file_path);

                        $this->_db->beginTransaction();

                        //$where['PROFILE_ID = ?'] = $id;
						
						$tableName = 'TEMP_ADAPTER_PROFILE';
                        $arrData = array(
                            'STATUS' => 3,
                            // 'HEADER_ORDER' => $filter->getEscaped('headerOrder'),
                            'SUGGESTED_BY' => $this->_userIdLogin,
                            'SUGGESTED_DATE' => date('Y-m-d H:i:s')
                        );

                        $whereArr  = array('PROFILE_ID = ?'=> $id);
                        $this->_db->update($tableName,$arrData,$whereArr);
						
						
                       // $result =  $this->_db->delete('M_ADAPTER_PROFILE',$where);
                        //$wheredelete['PROFILE_ID = ?'] = $id;
                        //$result = $this->_db->delete('M_ADAPTER_PROFILE_DETAIL',$wheredelete);

                        $profilename = $profileData[0]['PROFILE_NAME'];
                        $profilid    = $profileData[0]['PROFILE_ID'];

                        Application_Helper_General::writeLog('PFAD','Delete Business Profile Adapter. Profile Name : '.$profilename.' (ID: '.$profilid.')');

                        $this->_db->commit();
                    }

                }
                catch(Exception $e)
                {
                    $this->_db->rollBack();
                }
                $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                $error          = true;
                $errors         = $zf_filter_input->getMessages();
                if(!empty($msg)){
                    $errors = $msg;
                }
                $profileiderr   = (isset($errors['profileiderr']))? $errors['profileiderr'] : null;
				$this->view->error = $error; 
            }

        }
		
		
		if($suspend)
        { 
            $profileid = $this->_request->getParam('profileid');

            $validators = array (
                                    'profileid' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'profileid' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            foreach ($profileid as $key => $value) {
                if(!empty($value)){
                    $success = true;
                }
            }
			//var_dump($profileid);die;
            $select = $this->_db->select()
                                    ->from(array('H' => 'M_CUST_ADAPTER_PROFILE'),array('*'));

             $select->where('H.ADAPTER_PROFILE_ID IN (?) ', $profileid);
    
             $profileCust = $this->_db->fetchAll($select);
             if(!empty($profileCust)){
                 $success = false;
                 $msg = 'Adapter Profile is being used by customers';
             }

            if($zf_filter_input->isValid() && $success)
            {

                try
                {
                    foreach ($profileid as $key => $value)
                    {

                        $id = $value;

                        //unlink files
                        $select = $this->_db->select()
                                    ->from(array('H' => 'M_ADAPTER_PROFILE'),array('*'));

                        $select->where('H.PROFILE_ID = '. $this->_db->quote($id));

                        $profileData = $this->_db->fetchAll($select);

                        $file_path = $profileData[0]['FILE_PATH'];
                        @unlink($file_path);

                        $this->_db->beginTransaction();

                        //$where['PROFILE_ID = ?'] = $id;
						
						$tableName = 'TEMP_ADAPTER_PROFILE';
                        $arrData = array(
                            'STATUS' => 3,
                            // 'HEADER_ORDER' => $filter->getEscaped('headerOrder'),
                            'SUGGESTED_BY' => $this->_userIdLogin,
                            'SUGGESTED_DATE' => date('Y-m-d H:i:s')
                        );

                        $whereArr  = array('PROFILE_ID = ?'=> $id);
                        $this->_db->update($tableName,$arrData,$whereArr);
						
						
                       // $result =  $this->_db->delete('M_ADAPTER_PROFILE',$where);
                        //$wheredelete['PROFILE_ID = ?'] = $id;
                        //$result = $this->_db->delete('M_ADAPTER_PROFILE_DETAIL',$wheredelete);

                        $profilename = $profileData[0]['PROFILE_NAME'];
                        $profilid    = $profileData[0]['PROFILE_ID'];

                        Application_Helper_General::writeLog('PFAD','Delete Business Profile Adapter. Profile Name : '.$profilename.' (ID: '.$profilid.')');

                        $this->_db->commit();
                    }

                }
                catch(Exception $e)
                {
                    $this->_db->rollBack();
                }
                $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                $error          = true;
                $errors         = $zf_filter_input->getMessages();
                if(!empty($msg)){
                    $errors = $msg;
                }
                $profileiderr   = (isset($errors['profileiderr']))? $errors['profileiderr'] : null;
				$this->view->error = $error; 
            }

        }

       
    }
    
}
