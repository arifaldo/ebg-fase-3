<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class Cmdreport_LldreportController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	
	public function indexAction()
	{
		
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
				
		// 6,4 THEN 'Multi Credit'
		// 7,5 THEN 'Multi Debet'
		$paytypeall		= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);

		foreach($paytypeall as $key => $value){
			
			if($key != 3 && $key != 8 && $key != 9 && $key != 10){
			
				if($key == 4 OR $key == 6) {
					$value = $paytypeall[4];
					$optpaytypeAll[$key] = $value;
				}
				else if($key == 5 OR $key == 7) {
					$value = $paytypeall[5];
					$optpaytypeAll[$key] = $value;
				}
				else $optpaytypeAll[$key] = $value;
			}
			
		}

		$optpaytypeRaw = array(
														1=>'Domestic (RTGS)',
														2=>'Domestic (SKN)',
														3=>'Remittance',
													);
		
		$listId = $this->_db->select()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID','CUST_NAME'))
						->order('CUST_ID ASC')
						->query()->fetchAll();
		
       	$list = array(""=>"--- ".$this->language->_('Any Value')." ---");
		$list += Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');
		
		$this->view->arrPayType 		= $optpaytypeRaw;
		$this->view->listCustId 		= $list;
		
		$fields = array	(
							'companycode'  		=> array(
															'field' => 'companycode',
															'label' => $this->language->_('Company Code'),
															'sortable' => true
														),
							'companyname'  		=> array(
															'field' => 'companyname',
															'label' => $this->language->_('Company Name'),
															'sortable' => true
														),
							'payref'  			=> array(
															'field' => 'payref',
															'label' => $this->language->_('Payment Ref#'),
															'sortable' => true
														),
							'benebankname'  	=> array(
															'field' => 'benebankname',
															'label' => $this->language->_('Bank Name'),
															'sortable' => true
													),
// 							'transactionid' 	=> array(
// 															'field' => 'transactionid',
// 															'label' => $this->language->_('Transaction Ref').'#',
// 															'sortable' => true
// 														),
							'nostrobank' 	=> array(
															'field' => 'NOSTRO_NAME',
															'label' => $this->language->_('Nostro Bank'),
															'sortable' => true
													),
							
							
							'transferdate'  	=> array(
															'field' => 'transferdate',
															'label' => $this->language->_('Payment Date'),
															'sortable' => true
														),
							'acctsrc'  			=> array(
															'field' => 'acctsrc',
															'label' => $this->language->_('Source Account'),
															'sortable' => true
														),
							'beneacct'  		=> array(
															'field' => 'beneacct',
															'label' => $this->language->_('Beneficiary Account'),
															'sortable' => true
														),
							'traamount'  		=> array(
															'field' => 'traamount',
															'label' => $this->language->_('Currency / Amount'),
															'sortable' => true
														),
							
							'tramessage'  		=> array(
															'field' => 'tramessage',
															'label' => $this->language->_('Message'),
															'sortable' => true
														),
// 							'lldcontent'  		=> array(
// 															'field' => 'lldcontent',
// 															'label' => $this->language->_('LLD Content'),
// 															'sortable' => false
// 														),
							
						);
		
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');
		
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;
			
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array('PAYMENTREF' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'COMPANYCODE'    	=> array('StripTags'),
	                       					   
						   'TRANSACTIONID' 		=> array('StripTags','StringTrim','StringToUpper'),
						   						   
						   'TRANSFERFROM' 		=> array('StripTags','StringTrim'),
						   'TRANSFERTO' 		=> array('StripTags','StringTrim'),
						   						   
						   'ACCTSRC' 			=> array('StripTags','StringTrim'),
						   'BENEACCT' 			=> array('StripTags','StringTrim'),
						  						   
						   'PAYTYPE' 			=> array('StripTags'),
						   
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array(	"PAYMENTREF","COMPANYCODE","TRANSACTIONID","TRANSFERFROM","TRANSFERTO",
							"ACCTSRC","BENEACCT","PAYTYPE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
		
						'PAYMENTREF' 	=> array(),	
						'COMPANYCODE' 	=> array(array('InArray', array('haystack' => array_keys($list)))),
											
						'TRANSACTIONID' => array(),	
												
						'TRANSFERFROM' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'TRANSFERTO'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
												
						'ACCTSRC' 		=> array(),	
						'BENEACCT' 		=> array(),	
						
						'PAYTYPE' 		=> array(array('InArray', array('haystack' => array_keys($optpaytypeRaw)))),							
						
						
						);
		
		$zf_filter  		= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$fPAYMENTREF 		= html_entity_decode($zf_filter->getEscaped('PAYMENTREF'));
		$fCOMPANYCODE 		= html_entity_decode($zf_filter->getEscaped('COMPANYCODE'));
				
		$fTRANSACTIONID		= html_entity_decode($zf_filter->getEscaped('TRANSACTIONID'));
		
		$fACCTSRC 			= html_entity_decode($zf_filter->getEscaped('ACCTSRC'));
		$fBENEACCT 			= html_entity_decode($zf_filter->getEscaped('BENEACCT'));
		
		$fPAYTYPE 			= html_entity_decode($zf_filter->getEscaped('PAYTYPE'));
				
		
		if($filter == NULL && $clearfilter != 1){
			$fTRANSFERFROM 	= date("d/m/Y");
			$fTRANSFERTO 	= date("d/m/Y");
		}
		else{
		
			if($filter != NULL){
				$fTRANSFERFROM 		= html_entity_decode($zf_filter->getEscaped('TRANSFERFROM'));
				$fTRANSFERTO 		= html_entity_decode($zf_filter->getEscaped('TRANSFERTO'));
			}
			if($clearfilter == 1){
				$fTRANSFERFROM 	= "";
				$fTRANSFERTO 	= "";
			}
		}
		
		/*
			CONCAT(sb_pslip.acctsrc,' (',sb_pslip.accsrc_ccy,')',' / ', sb_pslip.accsrc_bankname,' / ',sb_pslip.accsrc_alias) AS fullaccfrom,
			CONCAT(sb_transaction.tra_accttgt,' (', sb_transaction.tra_accttgtcurr,')',' / ', sb_transaction.tra_accttgtbankname) AS fullaccto,
				
			IF (tra_alreadycharged=1,'Y','N') AS tra_statuscharged,
		
		*/
		
// CASE UNTUK TRANSFER TYPE		
		$transType = array(
												1 => 'RTGS',
												2 => 'SKN',
											);
		$caseTransType = "(CASE T.TRANSFER_TYPE ";
  		foreach($transType as $key => $val)	{ $caseTransType .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTransType .= " END)";
		

		
		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($optpaytypeAll as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";
		
		$selectCM = $this->_db->select()
										->FROM(		array(	'T'=>'T_TRANSACTION'),array())
										->JOINLEFT(	array(	'P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',
													array(	'companycode'	=>'C.CUST_ID',
															'companyname'	=>'C.CUST_NAME',
															'payref'		=>'P.PS_NUMBER',	
															'transactionid'	=>'T.TRANSACTION_ID',	
															'benebankname'	=>'T.BENEFICIARY_BANK_NAME',
															'transferdate'	=>'P.PS_EFDATE',
																											
															'acctsrc'		=> new Zend_Db_Expr("
																				T.SOURCE_ACCOUNT + ' (' + T.SOURCE_ACCOUNT_CCY + ')  ' + T.SOURCE_ACCOUNT_NAME + ' / ' + T.SOURCE_ACCOUNT_ALIAS_NAME"),
															'beneacct'		=> new Zend_Db_Expr("
																				T.BENEFICIARY_ACCOUNT + ' (' + T.BENEFICIARY_ACCOUNT_CCY + ')  ' + T.BENEFICIARY_ACCOUNT_NAME "),
															
															'ccy'			=>'P.PS_CCY',	
															'traamount'		=>'T.TRA_AMOUNT',	
															
															'tramessage'	=>'T.TRA_MESSAGE',	
															'lldid'			=>'T.LLD_CODE',
															'lldcontent'	=>'T.LLD_DESC',
															'TRA_STATUS'	=>'T.TRA_STATUS',	
															'paymenttype'	=>$casePayType,	
															'TRANSFER_TYPE'	=>$caseTransType,	
															'T.NOSTRO_NAME',
															))
										->JOINLEFT(array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())
										->WHERE(" P.PS_TYPE = 2 OR P.PS_TYPE = 3")
										->WHERE(" T.TRANSFER_TYPE = 1 OR T.TRANSFER_TYPE = 2")
										->WHERE(" T.BENEFICIARY_RESIDENT = 'NR' ")
										->WHERE(" T.LLD_CODE is not null")
										->WHERE(" T.LLD_CODE not like ''");
										

		if($fPAYMENTREF){
			$selectCM->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPAYMENTREF.'%'));
		}
		if($fCOMPANYCODE){
			$selectCM->where("UPPER(C.CUST_ID) = ".$this->_db->quote($fCOMPANYCODE));
		}
			
		if($fTRANSACTIONID) {
			$selectCM->where("UPPER(T.TRANSACTION_ID) LIKE ".$this->_db->quote('%'.$fTRANSACTIONID.'%'));
		}
		
		if($fTRANSFERFROM){
			$FormatDate 	= new Zend_Date($fTRANSFERFROM, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$selectCM->where('DATE( P.PS_EFDATE) >= ?', $transferfrom);
		}
		if($fTRANSFERTO){
			$FormatDate 	= new Zend_Date($fTRANSFERTO, $this->_dateDisplayFormat);
			$transferto  	= $FormatDate->toString($this->_dateDBFormat);
			$selectCM->where('DATE( P.PS_EFDATE) <= ?', $transferto);
		}
		
		if($fACCTSRC)	{
			$selectCM->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fACCTSRC.'%'));
		}
		if($fBENEACCT)	{
			$selectCM->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBENEACCT.'%'));
		}
		
		if($fPAYTYPE){ 
			$fPayType 	 	= explode(",", $fPAYTYPE);
			$selectCM->where("T.TRANSFER_TYPE in (?) ", $fPayType);		
		}
		
		$selectCM->order($sortBy.' '.$sortDir);
		// echo "<pre>";
		// echo $select->__toString();
		// die;
		
		
		$dataSQL = $this->_db->fetchAll($selectCM);
		
		
		if(!empty($dataSQL)){
			foreach($dataSQL as $d => $val){
				foreach($fields as $key => $row){
				
					$value 	= $val[$key];
							
					if($key == "transferdate")		{ $value = Application_Helper_General::convertDate($value, $this->_dateViewFormat); }
					else if($key == "beneacct")		{ $value = $val["beneacct"].' / '.$val["benebankname"]; }
					else if($key == "traamount")	{ $value = $val["ccy"].' '.Application_Helper_General::displayMoney($value); }
					else if($key == "payref")		{ $value = $val["payref"].' / '.$val["paymenttype"] .' (' .$val["TRANSFER_TYPE"]. ')'; }
										
					$data[$d][$key] = $value;
					
				}
			}
			
		}
		else{
			$data = array();
		}
		
		// echo "<pre>";
		// print_r($data);
		// die;
		
		$this->paging($data);
		
		if ($csv || $pdf || $this->_request->getParam('print')) {	$header  = Application_Helper_Array::simpleArray($fields, "label"); }		
		
		if($csv)
		{
			$this->_helper->download->csv($header,$data,null,'LLD Report');  
			Application_Helper_General::writeLog('SBLD','Download CSV LLD Report');
			//$this->frontendLog('CSV', $this->_moduleDB, null,null,null);  
		}
		elseif($pdf)
		{
			$this->_helper->download->pdf($header,$data,null,'LLD Report');  
			Application_Helper_General::writeLog('SBLD','Download PDF LLD Report');
			//$this->frontendLog('PDF', $this->_moduleDB, null,null,null);  
		}
		elseif($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'LLD Report', 'data_header' => $fields));
		}
		else
		{			
			$stringParam = array(
									'PAYMENTREF'	=>$fPAYMENTREF,
									'COMPANYCODE'	=>$fCOMPANYCODE,
												
									'TRANSACTIONID'	=>$fTRANSACTIONID,
									
									'TRANSFERFROM'	=>$fTRANSFERFROM,
									'TRANSFERTO'	=>$fTRANSFERTO,
									
									'ACCTSRC'		=>$fACCTSRC,
									'BENEACCT'		=>$fBENEACCT,
									
									'PAYTYPE'		=>$fPAYTYPE,
									'clearfilter'	=> $clearfilter,
									'filter'		=> $filter,
								
								);
			
			$this->view->PAYMENTREF 	= $fPAYMENTREF;
			$this->view->COMPANYCODE 	= $fCOMPANYCODE;
						
			$this->view->TRANSACTIONID 	= $fTRANSACTIONID;
			
			$this->view->TRANSFERFROM 	= $fTRANSFERFROM;
			$this->view->TRANSFERTO 	= $fTRANSFERTO;
			
			$this->view->ACCTSRC 		= $fACCTSRC;
			$this->view->BENEACCT 		= $fBENEACCT;
			
			$this->view->PAYTYPE 		= $fPAYTYPE;
						
			$this->view->fields 		= $fields;
			$this->view->filter 		= $filter;
			
			// $this->view->arrPayType 		= $optpaytypeRaw;
			// $this->view->listCustId 		= $list;
			
			Application_Helper_General::writeLog('SBLD','View LLD Report');
			
		}		
		
	}
}
