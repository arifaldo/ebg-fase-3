<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';

require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';
require_once("Service/Account.php");

class eformworkflow_VerificationdetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');
		$this->view->systemType = $system_type;

		$stamp_fee = $settings->getSetting('stamp_fee');
		$this->view->stamp_fee = $stamp_fee;
		$adm_fee = $settings->getSetting('adm_fee');
		$this->view->adm_fee = $adm_fee;

		$this->view->systemType = $system_type;
		$this->view->ProvFee = 2000000;

		$conf = Zend_Registry::get('config');
		$this->_bankName = $conf['app']['bankname'];
		$this->view->masterbankname = $this->_bankName;

		$selectQuery    = "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
		// echo $selectQuery;
		$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

		$selectcomp = $this->_db->select()
			->from('M_CUSTOMER', array(
				'value' => new Zend_Db_Expr("CONCAT(CUST_NAME , ' (' , CUST_ID , ')  ' )"),
				'CUST_NAME', 'CUST_ID', 'CUST_TYPE'
			))
			->where('CUST_MODEL = ?', "3")
			->where('CUST_STATUS = 1 ');

		$tempColumn = $this->_db->fetchAll($selectcomp);

		$this->view->custarr = json_encode($tempColumn);

		// var_dump($usergoogleAuth);die;
		if (!empty($usergoogleAuth)) {
			$this->view->googleauth = true;
			//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
			$settingObj = new Settings();
			$maxtoken = $settingObj->getSetting("max_failed_token");
			$this->view->tokenfail = (int)$maxtoken - 1;
			if ($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0') {
				//die;
				$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];



				$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN'] + 1);
				$this->view->tokenfail = $tokenfail;
			}
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
		} else {
			$this->view->nogoauth = '1';
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
		}



		$this->view->googleauth = true;


		// $numb = $this->_getParam('bgnumb');

		// decrypt numb
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER 	= urldecode($this->_getParam('bgnumb'));

		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

		// others attachment

		$checkOthersAttachment = $this->_db->select()
			->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
			->where("BG_REG_NUMBER = '$BG_NUMBER'")
			->order('A.INDEX ASC')
			->query()->fetchAll();

		if (count($checkOthersAttachment) > 0) {
			$this->view->othersAttachment = $checkOthersAttachment;
		}

		// end others attachment

		if (!empty($BG_NUMBER)) {

			$verifyData = $this->_db->select()
				->from(array('A' => 'TEMP_BGVERIFY_DETAIL'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
				->query()->fetchAll();
			//var_dump($verifyData);die;
			if (!empty($verifyData)) {
				foreach ($verifyData as $vl) {
					//if()
					$label = 'check' . $vl['INDEX'];
					//var_dump($label);
					$this->view->$label = true;
					$labeldate = 'verify' . $vl['INDEX'];
					$verifydate = Application_Helper_General::convertDate($vl['VERIFIED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					$this->view->$labeldate = $verifydate . ' by ' . $vl['VERIFIEDBY'];
				}
			}

			$bgdata = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))

				->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
				->where('A.BG_STATUS in (5,20)')
				->query()->fetchAll();

			$refdata = $this->_db->select()
				->from(array('A' => 'M_REFERALCODE'), array('*'))
				->query()->fetchAll();

			$saveRefData = [];

			foreach ($refdata as $value) {
				$saveRefData[$value['CODE']] = $value["NAME"];
			}

			$this->view->refdataArr = $saveRefData;

			$selectcomp = $this->_db->select()
				->from(array('A' => 'M_CUSTOMER'), array('*'))
				//  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
				->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$bgdata['0']['CUST_ID']))
				->query()->fetchAll();

			$this->view->compinfo = $selectcomp[0];

			$bgdatadetail = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))

				->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
				->query()->fetchAll();
			//$datas = $this->_request->getParams();
			//var_dump($bgdatadetail);


			if (!empty($bgdata)) {

				$get_cash_collateral = $this->_db->select()
					->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
					->where("CUST_ID = ?", "GLOBAL")
					->where("CHARGES_TYPE = ?", "10")
					->query()->fetchAll();

				$this->view->cash_collateral = $get_cash_collateral[0];

				$data = $bgdata['0'];

				$principleData = [];
				// if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
				foreach ($bgdatadetail as $key => $value) {
					$principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
				}

				$this->view->principleData = $principleData;
				// }

				if ($data["COUNTER_WARRANTY_TYPE"] == "1") {

					$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$paramProvisionFee['TIME_PERIOD_START'] = $data['TIME_PERIOD_START'];
					$paramProvisionFee['TIME_PERIOD_END'] = $data['TIME_PERIOD_END'];
					$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
					$this->view->provisionFee = $getProvisionFee['provisionFee'];
					$this->view->adminFee = $getProvisionFee['adminFee'];
					$this->view->stampFee = $getProvisionFee['stampFee'];

					if ($data["REDEBATE_STATUS"] == "0") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();

						if (count($cekrebate) > 0) {



							if ($cekrebate[0]["FLAG"] == "1") {
								$this->view->statusrebate = 1;
								$this->view->REBATE_REVIEWEDBY = $cekrebate[0]["REBATE_REVIEWEDBY"];

								$this->view->REBATE_REVIEWED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REVIEWED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
							} else {
								$this->view->statusrebate = 4;
								$this->view->REBATE_SUGGESTEDBY = $cekrebate[0]["REBATE_SUGGESTEDBY"];
								$this->view->REBATE_SUGGESTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_SUGGESTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
							}
						} else {

							$this->view->statusrebate = 0;
						}
					} elseif ($data["REDEBATE_STATUS"] == "2") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();
						$this->view->statusrebate = 2;

						$this->view->REBATE_REJECTEDBY = $cekrebate[0]["REBATE_REJECTEDBY"];

						$this->view->REBATE_REJECTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REJECTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					} else {
						$this->view->statusrebate = 3;
					}

					if ($data["REDEBATE_STATUS"] == "1") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();
						$this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

						$this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					}
				}


				if ($data["COUNTER_WARRANTY_TYPE"] == "2") {
					$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$paramProvisionFee['CUST_ID'] = $data['CUST_ID'];
					$paramProvisionFee['USAGE_PURPOSE'] = $data['USAGE_PURPOSE'];
					$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
					$this->view->provisionFee = $getProvisionFee['provisionFee'];
					$this->view->adminFee = $getProvisionFee['adminFee'];
					$this->view->stampFee = $getProvisionFee['stampFee'];


					// get linefacillity
					$paramLimit = array();

					$paramLimit['CUST_ID'] =  $bgdata[0]["CUST_ID"];
					$paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];

					//$this->view->linefacility = $get_linefacility[0];

					// end get linefacility

					// cek status rebate
					if ($data["REDEBATE_STATUS"] == "0") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();

						if (count($cekrebate) > 0) {



							if ($cekrebate[0]["FLAG"] == "1") {
								$this->view->statusrebate = 1;
								$this->view->REBATE_REVIEWEDBY = $cekrebate[0]["REBATE_REVIEWEDBY"];

								$this->view->REBATE_REVIEWED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REVIEWED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
							} else {
								$this->view->statusrebate = 4;
								$this->view->REBATE_SUGGESTEDBY = $cekrebate[0]["REBATE_SUGGESTEDBY"];
								$this->view->REBATE_SUGGESTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_SUGGESTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
							}
						} else {

							$this->view->statusrebate = 0;
						}
					} elseif ($data["REDEBATE_STATUS"] == "2") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();
						$this->view->statusrebate = 2;

						$this->view->REBATE_REJECTEDBY = $cekrebate[0]["REBATE_REJECTEDBY"];

						$this->view->REBATE_REJECTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REJECTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					} else {
						$this->view->statusrebate = 3;
					}

					if ($data["REDEBATE_STATUS"] == "1") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();
						$this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

						$this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					}

					//

				}

				$this->view->repairto = ["1" => "Prinsipal"];

				if ($data["COUNTER_WARRANTY_TYPE"] == "3") {

					$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$paramProvisionFee['CUST_ID'] = $data["BG_INSURANCE_CODE"];
					$paramProvisionFee['USAGE_PURPOSE'] = $data['USAGE_PURPOSE'];
					$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
					$this->view->provisionFee = $getProvisionFee['provisionFee'];
					$this->view->adminFee = $getProvisionFee['adminFee'];
					$this->view->stampFee = $getProvisionFee['stampFee'];


					$this->view->repairto = ["1" => "Prinsipal", "2" => "Asuransi"];

					$this->view->isinsurance = true;

					// kontra garansi dan provision fee jika kontra garansi insurance -----------

					$provision_insurance = $this->_db->select()
						->from(["A" => "M_CUST_LINEFACILITY"], ["FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
						->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
						->query()->fetchAll();


					$this->view->provision_insurance = $provision_insurance[0];


					// --------------------------------------------------------------------------

					$get_linefacilityINS = $this->_db->select()
						->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT", "STATUS"])
						->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
						->query()->fetchAll();


					$check_all_detail = $this->_db->select()
						->from("T_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $data["BG_INSURANCE_CODE"])
						->query()->fetchAll();

					$total_bgamount_on_risk = 0;

					if (count($check_all_detail) > 0) {
						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_risks = $this->_db->select()
							->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
							->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->query()->fetchAll();

						foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
							$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
						}
					}

					$check_all_detail = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $data["BG_INSURANCE_CODE"])
						->query()->fetchAll();

					$total_bgamount_on_temp = 0;

					if (count($check_all_detail) > 0) {

						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_temps = $this->_db->select()
							->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
							->where("COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->where("BG_STATUS IN (?)", ["5", "6", "7", "14", "17", "20"])
							->query()->fetchAll();

						foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
							$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
						}
					}

					$current_limitINS = $get_linefacilityINS[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;

					$this->view->current_limit_ins = $current_limitINS;

					// cek status rebate 
					if ($data["REDEBATE_STATUS"] == "0") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();

						if (count($cekrebate) > 0) {



							if ($cekrebate[0]["FLAG"] == "1") {
								$this->view->statusrebate = 1;
								$this->view->REBATE_REVIEWEDBY = $cekrebate[0]["REBATE_REVIEWEDBY"];

								$this->view->REBATE_REVIEWED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REVIEWED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
							} else {
								$this->view->statusrebate = 4;
								$this->view->REBATE_SUGGESTEDBY = $cekrebate[0]["REBATE_SUGGESTEDBY"];
								$this->view->REBATE_SUGGESTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_SUGGESTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
							}
						} else {

							$this->view->statusrebate = 0;
						}
					} elseif ($data["REDEBATE_STATUS"] == "2") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();
						$this->view->statusrebate = 2;

						$this->view->REBATE_REJECTEDBY = $cekrebate[0]["REBATE_REJECTEDBY"];

						$this->view->REBATE_REJECTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REJECTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					} else {
						$this->view->statusrebate = 3;
					}

					if ($data["REDEBATE_STATUS"] == "1") {
						$cekrebate = $this->_db->select()
							->from("TEMP_REQUEST_REBATE")
							->where("BG_REG_NUMBER = ?", $BG_NUMBER)
							->query()->fetchAll();
						$this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

						$this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					}

					//
				}





				switch ($data["CHANGE_TYPE"]) {
					case '0':
						$this->view->suggestionType = "New";
						break;
					case '1':
						$this->view->suggestionType = "Amendment Changes";
						break;
					case '2':
						$this->view->suggestionType = "Amendment Draft";
						break;
				}
				$this->view->data = $data;

				if ($data["SP_OBLIGEE_CODE"] != "") {
					foreach ($tempColumn as $value) {
						if ($value["CUST_ID"] == $data["SP_OBLIGEE_CODE"]) {
							$this->view->spobcode = $value["CUST_NAME"];
						}
					}
				}

				$policyBoundary = $this->findPolicyBoundary(38, $bgdata['0']['BG_AMOUNT']);
				//echo '<pre>';
				//var_dump($policyBoundary);die;
				$this->view->policyBoundary = $policyBoundary;

				$approverUserList = $this->findUserBoundary(38, $bgdata['0']['BG_AMOUNT']);

				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->BG_PUBLISH = $arrbgpublish[$data['BG_PUBLISH']];

				if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();

					$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
					$this->view->insuranceAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];
				}

				$getCustomerAccountCharge = $this->_db->select()
					->from(array('A' => 'M_CUSTOMER_ACCT'), array('A.CCY_ID'))
					->where('A.ACCT_NO = ?', $data['FEE_CHARGE_TO'])
					->where('A.CUST_ID = ?', $data['CUST_ID'])
					->query()->fetchAll();

				$this->view->chargeto_ccy = $getCustomerAccountCharge[0]['CCY_ID'];

				// start validasi 




				$err = false;
				$errmsg = array();

				$gerRequestRebate = $this->_db->select()
					->from(array('A' => 'TEMP_REQUEST_REBATE'))
					->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
					->query()->fetchAll();
				if (count($gerRequestRebate) > 0) {
					//var_dump($gerRequestRebate);
					if ($gerRequestRebate[0]['REBATE_APPROVEDBY'] == null && $gerRequestRebate[0]['REBATE_REJECTEDBY'] == null) {

						$err = true;
						$errmsg = 'proses tidak bisa berlanjut bila sedang ada pengajuan rebate yg sdg menunggu review/persetujuan';
						$this->view->errorRequestRebate = $errmsg;
					} else {
						$err = false;
					}
				}

				if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {

					$bgdatasplit = $this->_db->select()
						->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
						->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
						->query()->fetchAll();





					$jmlbgdatasplit = count($bgdatasplit);
					//echo $jmlbgdatasplit;
					for ($i = 0; $i < $jmlbgdatasplit; $i++) {

						//echo $bgdatasplit[$i]["AMOUNT"];
						$totalPenjaminan += $bgdatasplit[$i]["AMOUNT"];
						$temp_save[$bgdatasplit[$i]["ACCT"]] = $this->_db->select()
							->from("M_CUSTOMER_ACCT")
							->where("ACCT_NO = ?", $bgdatasplit[$i]["ACCT"])
							->where("CUST_ID = ?", $data['CUST_ID'])
							->query()->fetchAll();

						$bgdatasplit[$i]["CURRENCY"] = $temp_save[$bgdatasplit[$i]["ACCT"]][0]["CCY_ID"];
						$bgdatasplit[$i]["TYPE"] = $temp_save[$bgdatasplit[$i]["ACCT"]][0]["ACCT_DESC"];


						//echo $bgdatasplit[$i]["ACCT"];
						//echo $temp_save[$i]["ACCT_DESC"];
						$svcAccount = new Service_Account($bgdatasplit[$i]["ACCT"], 'IDR');
						if ($temp_save[$bgdatasplit[$i]["ACCT"]][0]["ACCT_DESC"] == 'Giro') {
							$result = $svcAccount->inquiryAccountBalance('AB', FALSE);

							if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
								if ($result['status'] != '1' && $result['status'] != '4') {
									//if ($result['status'] == '1') {
									$err = true;
									$errmsg[$result['account_number']] = 'Status rekening tidak aktif';
									$this->view->errorStatusAccount = $errmsg;
									//die('here');
									//$valid = false;
								}

								$getProductType = $this->_db->select()
									->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
									->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
									->query()->fetchAll();
								//var_dump($getProductType);

								if (empty($getProductType)) {
									$err = true;
									$errmsg[$result['account_number']] = 'Product type tidak terdaftar di product management';
									$this->view->errorProductTypeGiro = $errmsg;
									//$valid = false;
								}
							} else {
								$err = true;
								$errmsg = $result['response_desc'];
								$this->view->errorwaranty = $errmsg;
								$valid = false;
							}
						} else if ($temp_save[$bgdatasplit[$i]["ACCT"]][0]["ACCT_DESC"] == 'Deposito') {

							$result = $svcAccount->inquiryDeposito('AB', FALSE);

							if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
								if ($result['status'] != '1' && $result['status'] != '4') {
									//if ($result['status'] == '1') {
									$err = true;
									$errmsg[$result['account_number']] = 'Status rekening tidak aktif';
									$this->view->errorStatusAccount = $errmsg;
									//$valid = false;
								}

								$getProductType = $this->_db->select()
									->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
									->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
									->query()->fetchAll();

								if (empty($getProductType)) {
									$err = true;
									$errmsg[$result['account_number']] = 'Product type tidak terdaftar di product management';
									$this->view->errorProductTypeDeposito = $errmsg;
									//$valid = false;
								}

								if ($result['aro_status'] != 'Y') {
									$err = true;
									$errmsg[$result['account_number']] = 'Type bukan ARO';
									$this->view->errorAccountType = $errmsg;
									//$valid = false;
								}

								if ($result['varian_rate'] != '0') {
									$err = true;
									$errmsg[$result['account_number']] = 'Variant Rate bukan 0';
									$this->view->errorAccountRate = $errmsg;
									//$valid = false;
								}

								if ($result['hold_description'] == 'Y') {
									$err = true;
									$errmsg[$result['account_number']] = 'Account Number sudah digunakan sebagai jaminan';
									$this->view->errorRekeningJaminanDeposito = $errmsg;
									//$valid = false;
								}


								//var_dump($result['account_number']);
								/*$sqlRekeningJaminanExist = $this->_db->select()
										->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
										->where('A.BG_REG_NUMBER != ?', $data['BG_REG_NUMBER'])
										->where('A.ACCT = ?', $result['account_number']);
									//die;
									$getRekeningJaminanExist = $sqlRekeningJaminanExist
										->query()->fetchAll();
									//die;
									if (!empty($getRekeningJaminanExist)) {
										$err = true;
										$errmsg[$result['account_number']] = 'Account Number sudah digunakan sebagai jaminan';
										$this->view->errorRekeningJaminanDeposito = $errmsg;
										//$valid = false;
									}*/
							} else {
								$err = true;
								$errmsg = $result['response_desc'];
								$this->view->errorwaranty = $errmsg;
								$valid = false;
							}
						}

						//echo '2';
						//var_dump($result);//die;
						//var_dump($value["ACCT"]);


					}


					$this->view->fullmember = $bgdatasplit;

					if ($totalPenjaminan < $data['BG_AMOUNT']) {

						$errmsg = 'Total nilai penjaminan harus sama dengan / lebih besar dari nilai BG';
						$this->view->minpenjaminan_err = $errmsg;
						$valid = false;
					}

					$svcAccount = new Service_Account($data["FEE_CHARGE_TO"], 'IDR');
					$result = $svcAccount->inquiryAccountBalance('AB', FALSE);

					if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
						if ($result['status'] != '1' && $result['status'] != '4') {
							$err = true;
							$errmsg = 'Status rekening tidak aktif';
							$this->view->errorChargeStatusAccount = $errmsg;
							//$valid = false;
						}

						$getProductType = $this->_db->select()
							->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
							->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
							->query()->fetchAll();
						if (empty($getProductType)) {
							$err = true;
							$errmsg = 'Product type tidak terdaftar di product management';
							$this->view->errorChargeProductType = $errmsg;
							//$valid = false;
						}


						if ($getCustomerAccountCharge[0]['CCY_ID'] != "IDR") {
							$err = true;
							$errmsg = 'mata uang bukan IDR';
							$this->view->errorChargeCurrency = $errmsg;
							//$valid = false;
						}
					} else {
						$err = true;
						$errmsg = $result['response_desc'];
						$this->view->errorwaranty = $errmsg;
						$valid = false;
					}
				}

				if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '2' || $bgdata['0']['COUNTER_WARRANTY_TYPE'] == '3') {
					$cust_id = ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == 2) ? $bgdata[0]["CUST_ID"] : $principleData["Insurance Name"];
					$getCustomerStatus = $this->_db->select()
						->from(array('A' => 'M_CUST_LINEFACILITY'), array('A.STATUS'))
						->where('A.CUST_ID = ?', $cust_id)
						->query()->fetchAll();

					$getCustomer = $this->_db->select()
						->from(array('A' => 'M_CUSTOMER'), array('A.CUST_STATUS'))
						->where('A.CUST_ID = ?', $cust_id)
						->query()->fetchAll();


					if ($getCustomerStatus[0]['STATUS'] != "1") { //APPROVED
						$err = true;
						if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '3') {
							$errmsg = 'Pengajuan belum dapat diproses. Cek status perjanjian Mitra Asuransi';
						}

						if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '2') {
							$errmsg = 'Pengajuan belum dapat diproses. Cek status perjanjian Debitur';
						}



						$this->view->errorStatusAsuransi = $errmsg;
						//$valid = false;
					}

					if ($getCustomer[0]['CUST_STATUS'] != "1") { //APPROVED
						$err = true;
						if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '3') {
							$errmsg = 'Pengajuan belum dapat diproses. Cek status nasabah Mitra Asuransi';
						}

						if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '2') {
							$errmsg = 'Pengajuan belum dapat diproses. Cek status Debitur';
						}


						$this->view->errorStatusCustomer = $errmsg;
						//$valid = false;
					}

					$get_linefacility_insurace = $this->_db->select()
						->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_ID", "STATUS", "TICKET_SIZE"])
						->where("CUST_ID = ?", $cust_id)
						->query()->fetchAll();


					$percent10 = (10 / 100) * (float)$get_linefacility_insurace[0]["PLAFOND_LIMIT"];

					if ($get_linefacility_insurace[0]["TICKET_SIZE"] == 1) {

						$ticketSizeAmount = $percent10;
					} elseif ($get_linefacility_insurace[0]["TICKET_SIZE"] == 2) {

						$ticketSizeAmount = $get_linefacility_insurace[0]["PLAFOND_LIMIT"];
					} else {

						$ticketSizeAmount = $get_linefacility_insurace[0]["TICKET_SIZE"];
					}

					if (floatval($data['BG_AMOUNT']) > floatval($ticketSizeAmount)) {
						$err = true;
						$errmsg = 'BG Nominal yang diajukan tidak boleh lebih besar dari ticket size asuransi';
						$this->view->errorTickerSize = $errmsg;
						//$valid = false;
					}
				}

				if ($data["COUNTER_WARRANTY_TYPE"] == "2") {
					// get linefacillity
					$paramLimit = array();

					$paramLimit['CUST_ID'] =  $bgdata[0]["CUST_ID"];
					$paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);


					$current_limit = $getLineFacility['currentLimit'];

					//$this->view->linefacility = $get_linefacility[0];

					// end get linefacility
					if ($data['BG_AMOUNT'] >= $current_limit) {
						$err = true;
					}

					// end get linefacility
				} else if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					$get_linefacilityINS = $this->_db->select()
						->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT", "STATUS"])
						->where("CUST_ID = ?", $cust_id)
						->query()->fetchAll();


					$check_all_detail = $this->_db->select()
						->from("T_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $cust_id)
						->query()->fetchAll();

					$total_bgamount_on_risk = 0;

					if (count($check_all_detail) > 0) {
						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_risks = $this->_db->select()
							->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
							->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->query()->fetchAll();

						foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
							$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
						}
					}

					$check_all_detail = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $cust_id)
						->query()->fetchAll();

					$total_bgamount_on_temp = 0;

					if (count($check_all_detail) > 0) {

						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_temps = $this->_db->select()
							->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
							->where("COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->where("BG_STATUS IN (?)", ["5", "6", "7", "14", "17", "20"])
							->query()->fetchAll();

						foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
							$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
						}
					}

					$current_limitINS = $get_linefacilityINS[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;


					$this->view->current_limit_ins = $current_limitINS;
					// if ($data['BG_AMOUNT'] >= $current_limitINS) {
					if ($current_limitINS < 0) {
						$err = true;

						$errmsg = 'Limit asuransi tidak tersedia';
						$this->view->errorTickerSize = $errmsg;
					}

					//die;

				}


				if ($this->view->current_limit < 0) {
					$err = true;
					// $errmsg = "Limit tidak mencukupi";
					// $this->view->errorStatusAccount = $errmsg;
				}

				if ($this->view->warranty_type == "2") {
					if ($this->view->status_linefacility == "2" or $this->view->status_linefacility == "3" or $this->view->status_linefacility == "4") {
						$err = true;
						// $errmsg = "Limit tidak mencukupi";
						// $this->view->errorStatusAccount = $errmsg;
					}
				}



				// end validasi



				$selectHistory	= $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY')
					->where("BG_REG_NUMBER = ?", $BG_NUMBER);

				$history = $this->_db->fetchAll($selectHistory);

				$cust_approver = 1;

				$bg_submission_hisotrys = $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY')
					->where("BG_REG_NUMBER = ?", $BG_NUMBER)
					->where("CUST_ID IN (?) ", [$selectcomp[0]["CUST_ID"], $bgdata[0]["BG_INSURANCE_CODE"]]);
				$bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

				foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {

					// maker
					if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
						$makerStatus = 'active';
						$makerIcon = '<i class="fas fa-check"></i>';

						$custlogin = $bg_submission_hisotry['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						// $custFullname = $customer[0]['USER_FULLNAME'];
						$custFullname = $customer[0]['USER_ID'];

						$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

						$align = 'align="center"';

						$this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
					}

					// approver
					if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
						$approverStatus = 'active';
						$approverIcon = '<i class="fas fa-check"></i>';

						$custlogin = $bg_submission_hisotry['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						// $custFullname = $customer[0]['USER_FULLNAME'];
						$custFullname = $customer[0]['USER_ID'];

						$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

						$align = 'align="center"';

						$this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

						$this->view->approverStatus = $approverStatus;
					}

					if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
						//if releaser
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 5 || $bg_submission_hisotry['HISTORY_STATUS'] == 10) {
							$releaserStatus = 'active';
							$releaserIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							// $custFullname = $customer[0]['USER_FULLNAME'];
							$custFullname = $customer[0]['USER_ID'];

							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

							$align = 'align="center"';

							$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
							$this->view->releaserStatus = $releaserStatus;
						}
					} else {
						//if releaser
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 5) {
							$releaserStatus = 'active';
							$releaserIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							// $custFullname = $customer[0]['USER_FULLNAME'];
							$custFullname = $customer[0]['USER_ID'];

							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

							$align = 'align="center"';

							$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
							$this->view->releaserStatus = $releaserStatus;
						}
					}
				}

				foreach ($history as $row) {

					if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
						if ($row['HISTORY_STATUS'] == 8) {
							$makerStatus = 'active';
							$insuranceIcon = '<i class="fas fa-check"></i>';
							/*$approveStatus = 'active';
							$reviewStatus = 'active';
							$releaseStatus = 'active';
							$releaseIcon = '<i class="fas fa-check"></i>';*/
							$insuranceStatus = 'active';
							$reviewStatus = '';
							//$releaseStatus = '';
							//$releaseIcon = '';

							$makerOngoing = '';
							$reviewerOngoing = '';
							$approverOngoing = '';
							//$releaserOngoing = '';

							$custlogin = $row['USER_LOGIN'];

							$selectCust    = $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin);
							//->where("CUST_ID = ?", $row['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							// $custFullname = $customer[0]['USER_FULLNAME'];
							$custFullname = $customer[0]['USER_ID'];
							// $custEmail 	  = $customer[0]['USER_EMAIL'];
							// $custPhone	  = $customer[0]['USER_PHONE'];

							$insuranceApprovedBy = $custFullname;

							$align = 'align="center"';
							$marginLeft = '';
							if ($cust_reviewer == 0 && $cust_approver == 0) {
								$align = '';
								$marginLeft = 'style="margin-left: 15px;"';
							}

							$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
							$this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
						}
					}

					if ($row['HISTORY_STATUS'] == 15) {
						$verifyStatus = 'active';
						$verifyIcon = '<i class="fas fa-check"></i>';

						$verifyOngoing = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
							$reviewerOngoing = '';
							$verifyOngoing = '';
							$approverOngoing = '';
							//$releaserOngoing = 'ongoing';
							$releaserOngoing = '';
						} else {
							//$reviewerOngoing = 'ongoing';
							$reviewerOngoing = '';
							$verifyOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						}

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						$custEmail 	  = $customer[0]['USER_EMAIL'];
						$custPhone	  = $customer[0]['USER_PHONE'];

						$verifyApprovedBy = $custFullname;

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

						$align = 'align="center"';
						$marginRight = '';

						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginRight = 'style="margin-right: 15px;"';
						}

						$this->view->verifyApprovedBy = '';
						// $this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
					}
					//if reviewer done

					//if approver done
					if ($row['HISTORY_STATUS'] == 16) {
						$makerStatus = 'active';
						$approveStatus = '';
						$reviewStatus = '';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = 'ongoing';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectuserapp	= $this->_db->select()
							->from(array('C' => 'T_APPROVAL'), array(
								'*'
							))
							->where("C.PS_NUMBER = ?", $BG_NUMBER)
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							//->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
						;

						$userapprove = $this->_db->fetchAll($selectuserapp);
						//tampung data user yang sudah approve
						if (!empty($userapprove) && empty($userid)) {
							foreach ($userapprove as $vl) {
								$userid[] = $vl['USER_ID'];
							}
						}
						//$userid[] = $custlogin;

						$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
					}
					//if releaser done
					if ($row['HISTORY_STATUS'] == 5) {
						$makerStatus = 'active';
						/*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
						$approveStatus = '';
						$reviewStatus = '';
						$releaseStatus = '';
						$releaseIcon = '';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						// $custEmail 	  = $customer[0]['USER_EMAIL'];
						// $custPhone	  = $customer[0]['USER_PHONE'];

						$releaserApprovedBy = $custFullname;

						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						//$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
					}
				}

				//approvernamecircle jika sudah ada yang approve
				if (!empty($userid)) {

					$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

					$flipAlphabet = array_flip($alphabet);

					$approvedNameList = array();
					$i = 0;
					//var_dump($userid);die;
					foreach ($userid as $key => $value) {

						//select utk nama dan email
						$selectusername = $this->_db->select()
							->from(array('M_USER'), array(
								'*'
							))
							->where("USER_ID = ?", (string) $value)
							->where("CUST_ID = ?", (string) $this->_custIdLogin);

						$username = $this->_db->fetchAll($selectusername);

						//select utk cek user berada di grup apa
						$selectusergroup	= $this->_db->select()
							->from(array('C' => 'M_APP_GROUP_USER'), array(
								'*'
							))
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							->where("C.USER_ID 	= ?", (string) $value);

						$usergroup = $this->_db->fetchAll($selectusergroup);

						$groupuserid = $usergroup[0]['GROUP_USER_ID'];
						$groupusername = $usergroup[0]['USER_ID'];
						$groupuseridexplode = explode("_", $groupuserid);

						if ($groupuseridexplode[0] == "S") {
							$usergroupid = "SG";
						} else {
							$usergroupid = $alphabet[$groupuseridexplode[2]];
						}

						// $tempuserid = "";
						// foreach ($approverNameCircle as $row => $data) {
						// 	foreach ($data as $keys => $val) {
						// 		if ($keys == $usergroupid) {
						// 			if (preg_match("/active/", $val)) {
						// 				continue;
						// 			}else{
						// 				if ($groupuserid == $tempuserid) {
						// 					continue;
						// 				}else{
						// 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
						// 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
						// 				}
						// 				$tempuserid = $groupuserid;
						// 			}
						// 		}
						// 	}
						// }

						array_push($approvedNameList, $username[0]['USER_FULLNAME']);

						$efdate = $approveEfDate[$i];

						$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
						$i++;
					}
					//var_dump($approverApprovedBy);die;
					$this->view->approverApprovedBy = $approverApprovedBy;


					//kalau sudah approve semua
					if (!$checkBoundary) {
						$approveStatus = '';
						$approverOngoing = '';
						$approveIcon = '';
						$releaserOngoing = 'ongoing';
					}
				}



				$selectsuperuser = $this->_db->select()
					->from(array('C' => 'T_APPROVAL'))
					->where("C.PS_NUMBER 	= ?", $BG_NUMBER)
					->where("C.GROUP 	= 'SG'");

				$superuser = $this->_db->fetchAll($selectsuperuser);

				if (!empty($superuser)) {
					$userid = $superuser[0]['USER_ID'];

					//select utk nama dan email
					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("USER_ID = ?", (string) $userid)
						->where("CUST_ID = ?", (string) $this->_custIdLogin);

					$username = $this->_db->fetchAll($selectusername);

					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

					$approveStatus = 'active';
					$approverOngoing = '';
					$approveIcon = '<i class="fas fa-check"></i>';
					$releaserOngoing = 'ongoing';
				}
				// <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
				//define circle
				$makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . ' </button>';

				$insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . ' </button>';

				foreach ($reviewerList as $key => $value) {

					$textColor = '';
					if ($value == $reviewerApprovedBy) {
						$textColor = 'text-white-50';
					}

					$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				// 
				$reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '
		
					</button>';

				$verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup  hovertext" disabled>' . $reviewIcon . '
		
					</button>';



				$groupNameList = $approverUserList['GROUP_NAME'];
				unset($approverUserList['GROUP_NAME']);

				if ($approverUserList != '') {
					//echo '<pre>';
					//var_dump($approverUserList);die;
					foreach ($approverUserList as $key => $value) {
						$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
						$i = 1;
						foreach ($value as $key2 => $value2) {

							$textColor = '';
							if (in_array($value2, $approvedNameList)) {
								$textColor = 'text-white-50';
							}

							if ($i == count($value)) {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
							} else {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
							}
							$i++;
						}
					}
				} else {
					$approverListdata = 'There is no Approver User';
				}
				// 
				$spandata = '';
				if (!empty($approverListdata) && !$error_msg2) {
					$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
				}

				$approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '
						' . $spandata . '
					</button>';

				foreach ($releaserList as $key => $value) {

					$textColor = '';
					if ($value == $releaserApprovedBy) {
						$textColor = 'text-white-50';
					}

					$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				// 
				$releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . ' <span class="hovertextcontent ml-0" style="right: 0px; text-align: center;">' . $releaserListView . '</span> </button>';


				$this->view->policyBoundary = $policyBoundary;
				$this->view->makerNameCircle = $makerNameCircle;
				$this->view->reviewerNameCircle = $reviewerNameCircle;
				$this->view->verifyNameCircle = $verifyNameCircle;
				$this->view->insuranceNameCircle = $insuranceNameCircle;

				$this->view->approverNameCircle = $approverNameCircle;
				$this->view->releaserNameCircle = $releaserNameCircle;

				$this->view->makerStatus = $makerStatus;
				$this->view->verifyStatus = $verifyStatus;
				$this->view->approveStatus = $approveStatus;
				$this->view->reviewStatus = $reviewStatus;
				$this->view->releaseStatus = $releaseStatus;


				$bgType 		= $conf["bg"]["type"]["desc"];
				$bgCode 		= $conf["bg"]["type"]["code"];

				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));
				//var_dump($arrbgType);
				$this->view->arrbgType = $arrbgType;


				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					//var_dump($selectbranch[0]['BRANCH_NAME']);die;
					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

				$this->view->arrStatus = $arrStatus;

				$CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);


				$param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
				$AccArr = $CustomerUser->getAccounts($param);
				//var_dump($AccArr);die;

				if (!empty($AccArr)) {
					$this->view->src_name = $AccArr['0']['ACCT_NAME'];
				}

				$arrBankFormat = array(
					1 => 'Bank Standard',
					2 => 'Special Format (with bank approval)'
				);

				$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
				$this->view->bankFormatNumber = $data['BG_FORMAT'];

				$arrLang = array(
					1 => 'Indonesian',
					2 => 'English',
					3 => 'Billingual',
				);

				$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

				// $arrWaranty = array(
				// 	1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
				// 	2 => 'Bank Guarantee Line Facility',
				// 	3 => 'Insurance'

				// );

				// $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

				//BG Counter Guarantee Type
				$bgcgType         = $conf["bgcg"]["type"]["desc"];
				$bgcgCode         = $conf["bgcg"]["type"]["code"];

				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

				$this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

				if (!empty($data['USAGE_PURPOSE'])) {
					$data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
					foreach ($data['USAGE_PURPOSE'] as $key => $val) {
						$str = 'checkp' . $val;
						//var_dump($str);
						$this->view->$str =  'checked';
					}
				}

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}
				$this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];
				$this->view->data = $data;
				if (empty($data['SP_OBLIGEE_CODE'])) {
					// $this->view->COMPANY_ID = $this->_getParam('COMPANY_ID');
					// $this->view->COMPANY_TEXT = $this->_getParam('COMPANY_TEXT');
					$this->view->COMPANY_ID = "";
					$this->view->COMPANY_TEXT = "";
				} else {
					$this->view->COMPANY_ID = $data['SP_OBLIGEE_CODE'];
				}
				$this->view->bgprovtype = 'Provision IDR';
				$arrProvtype = array(
					'1' => 'Provision %',
					'2' => 'Provision IDR',
				);
				$this->view->provtypeArr = $arrProvtype;
				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->contact_person = $data['RECIPIENT_CP'];
				$this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
				$this->view->contact_email = $data['RECIPIENT_EMAIL'];
				//$this->view->comment = $data['GUARANTEE_TRANSACTION'];

				$this->view->fileName = $data['FILE'];
				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];

				$get_name = new Service_Account($data['FEE_CHARGE_TO'], "IDR");
				$get_name = $get_name->inquiryAccontInfo();
				$this->view->chargeto_name = $get_name["account_name"];

				$this->view->status = $data['BG_STATUS'];

				$Settings = new Settings();
				$claim_period = $Settings->getSetting('claim_period');
				$this->view->claim_period = $claim_period;


				$this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
				$bgdocType 		= $conf["bgdoc"]["type"]["desc"];
				$bgdocCode 		= $conf["bgdoc"]["type"]["code"];

				$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

				$this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];
				//$this->view->GT_TYPE = $data['GT_DOC_TYPE'];
				$this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];
				//var_dump($data['USAGE_PURPOSE']);die;
				$this->view->usage_purpose = $data['USAGE_PURPOSE'];
				$this->view->comment = $data['SERVICE'];


				if ($data['BG_STATUS'] == '9' || $data['BG_STATUS'] == '5' || $data['BG_STATUS'] == '20' || $data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$selectQuery  = "SELECT
                                a.USER_LOGIN,
                                b.`USER_FULLNAME` AS u_name,
                                c.`BUSER_NAME` AS b_name,
                                a.DATE_TIME,
                                a.BG_REASON,
                                a.HISTORY_STATUS,
                                a.BG_REG_NUMBER
                                
                                
                              FROM
                                T_BANK_GUARANTEE_HISTORY AS a
                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME DESC";
					$result =  $this->_db->fetchAll($selectQuery);
					if (!empty($result)) {
						$data['REASON'] = $result['0']['BG_REASON'];
					}
					$this->view->reqrepair = true;
					$this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
				}



				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$getInsuranceName = $this->_db->select()
									->from("M_CUSTOMER")
									->where("CUST_ID = ?", $value['PS_FIELDVALUE'])
									->query()->fetch();

								// $this->view->insuranceName =   $value['PS_FIELDVALUE'];
								$this->view->insuranceName =   $getInsuranceName['CUST_NAME'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}



				$conf = Zend_Registry::get('config');
				$this->view->bankname = $conf['app']['bankname'];



				//echo '<pre>';
				//var_dump($data);

				$download = $this->_getParam('download');
				//print_r($edit);die;
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				// var_dump("test");
				if ($this->_request->isPost()) {

					$datas = $this->_request->getParams();
					// Zend_Debug::dump($datas);die;

					$back = $this->_getParam('back');
					if ($back) {
						$this->_redirect('/eformworkflow/verification');
					}



					// start validasi 


					$err = false;
					$errmsg = array();

					$gerRequestRebate = $this->_db->select()
						->from(array('A' => 'TEMP_REQUEST_REBATE'))
						->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
						->query()->fetchAll();
					if (count($gerRequestRebate) > 0) {
						if ($gerRequestRebate[0]['REBATE_APPROVEDBY'] == null && $gerRequestRebate[0]['REBATE_REJECTEDBY'] == null) {

							$err = true;
							$errmsg = 'proses tidak bisa berlanjut bila sedang ada pengajuan rebate yg sdg menunggu review/persetujuan';
							$this->view->errorRequestRebate = $errmsg;
						} else {
							$err = false;
							$valid = true;
						}
					}


					if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {

						$bgdatasplit = $this->_db->select()
							->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
							->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
							->query()->fetchAll();




						$jmlbgdatasplit = count($bgdatasplit);
						for ($i = 0; $i < $jmlbgdatasplit; $i++) {

							//echo $bgdatasplit[$i]["AMOUNT"];
							$totalPenjaminan += $bgdatasplit[$i]["AMOUNT"];
							$temp_save[$bgdatasplit[$i]["ACCT"]] = $this->_db->select()
								->from("M_CUSTOMER_ACCT")
								->where("ACCT_NO = ?", $bgdatasplit[$i]["ACCT"])
								->where("CUST_ID = ?", $data['CUST_ID'])
								->query()->fetchAll();

							$bgdatasplit[$i]["CURRENCY"] = $temp_save[$bgdatasplit[$i]["ACCT"]][0]["CCY_ID"];
							$bgdatasplit[$i]["TYPE"] = $temp_save[$bgdatasplit[$i]["ACCT"]][0]["ACCT_DESC"];


							//echo $bgdatasplit[$i]["ACCT"];
							//echo $temp_save[$i]["ACCT_DESC"];

							$svcAccount = new Service_Account($bgdatasplit[$i]["ACCT"], 'IDR');
							if ($temp_save[$bgdatasplit[$i]["ACCT"]][0]["ACCT_DESC"] == 'Giro') {

								$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
								//	var_dump($result);
								if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
									if ($result['status'] != "1") {
										//if ($result['status'] == '1') {
										$err = true;

										$errmsg[$result['account_number']] = 'Status rekening tidak aktif';

										$this->view->errorStatusAccount = $errmsg;

										//$valid = false;
									}
									$getProductType = $this->_db->select()
										->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
										->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
										->query()->fetchAll();

									if (empty($getProductType)) {
										$err = true;
										$errmsg[$result['account_number']] = 'Product type tidak terdaftar di product management';
										$this->view->errorProductTypeGiro = $errmsg;
										//$valid = false;
									}
								} else {
									$err = true;
									$errmsg = $result['response_desc'];
									$this->view->errorwaranty = $errmsg;
									$valid = false;
								}
							} else if ($temp_save[$bgdatasplit[$i]["ACCT"]][0]["ACCT_DESC"] == 'Deposito') {

								$result = $svcAccount->inquiryDeposito('AB', FALSE);



								if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
									if ($result['status'] != '1' && $result['status'] != '4') {
										//if ($result['status'] == '1') {
										$err = true;
										$errmsg[$result['account_number']] = 'Status rekening tidak aktif';
										$this->view->errorStatusAccount = $errmsg;
										//$valid = false;
									}

									$getProductType = $this->_db->select()
										->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
										->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
										->query()->fetchAll();

									if (empty($getProductType)) {
										$err = true;
										$errmsg[$result['account_number']] = 'Product type tidak terdaftar di product management';
										$this->view->errorProductTypeDeposito = $errmsg;
										//$valid = false;
									}

									if ($result['aro_status'] != 'Y') {
										$err = true;
										$errmsg[$result['account_number']] = 'Type bukan ARO';
										$this->view->errorAccountType = $errmsg;
										//$valid = false;
									}

									if ($result['varian_rate'] != '0') {
										$err = true;
										$errmsg[$result['account_number']] = 'Variant Rate bukan 0';
										$this->view->errorAccountRate = $errmsg;
										//$valid = false;
									}

									if ($result['hold_description'] == 'Y') {
										$err = true;
										$errmsg[$result['account_number']] = 'Account Number sudah digunakan sebagai jaminan';
										$this->view->errorRekeningJaminanDeposito = $errmsg;
										//$valid = false;
									}

									/*

									//var_dump($result['account_number']);
									$sqlRekeningJaminanExist = $this->_db->select()
										->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
										->where('A.BG_REG_NUMBER != ?', $data['BG_REG_NUMBER'])
										->where('A.ACCT = ?', $result['account_number']);
									//die;
									$getRekeningJaminanExist = $sqlRekeningJaminanExist
										->query()->fetchAll();
									//die;
									if (!empty($getRekeningJaminanExist)) {
										$err = true;
										$errmsg[$result['account_number']] = 'Account Number sudah digunakan sebagai jaminan';
										$this->view->errorRekeningJaminanDeposito = $errmsg;
										//$valid = false;
									}
									*/
								} else {
									$err = true;
									$errmsg = $result['response_desc'];
									$this->view->errorwaranty = $errmsg;
									$valid = false;
								}
							}

							//echo '2';
							//var_dump($result);//die;
							//var_dump($value["ACCT"]);


						}



						$this->view->fullmember = $bgdatasplit;

						if ($totalPenjaminan < $data['BG_AMOUNT']) {

							$errmsg = 'Total nilai penjaminan harus sama dengan / lebih besar dari nilai BG';
							$this->view->minpenjaminan_err = $errmsg;
							$valid = false;
						}

						$svcAccount = new Service_Account($data["FEE_CHARGE_TO"], 'IDR');
						$result = $svcAccount->inquiryAccountBalance('AB', FALSE);

						if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
							if ($result['status'] != '1' && $result['status'] != '4') {
								$err = true;
								$errmsg = 'Status rekening tidak aktif';
								$this->view->errorChargeStatusAccount = $errmsg;
								//$valid = false;
							}

							$getProductType = $this->_db->select()
								->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
								->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
								->query()->fetchAll();
							if (empty($getProductType)) {
								$err = true;
								$errmsg = 'Product type tidak terdaftar di product management';
								$this->view->errorChargeProductType = $errmsg;
								//$valid = false;
							}


							if ($getCustomerAccountCharge[0]['CCY_ID'] != "IDR") {
								$err = true;
								$errmsg = 'mata uang bukan IDR';
								$this->view->errorChargeCurrency = $errmsg;
								//$valid = false;
							}
						} else {
							$err = true;
							$errmsg = $result['response_desc'];
							$this->view->errorwaranty = $errmsg;
							$valid = false;
						}
					}

					if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '2' || $bgdata['0']['COUNTER_WARRANTY_TYPE'] == '3') {
						$cust_id = ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == 2) ? $bgdata[0]["CUST_ID"] : $principleData["Insurance Name"];
						$getCustomerStatus = $this->_db->select()
							->from(array('A' => 'M_CUST_LINEFACILITY'), array('A.STATUS'))
							->where('A.CUST_ID = ?', $cust_id)
							->query()->fetchAll();

						$getCustomer = $this->_db->select()
							->from(array('A' => 'M_CUSTOMER'), array('A.CUST_STATUS'))
							->where('A.CUST_ID = ?', $cust_id)
							->query()->fetchAll();


						if ($getCustomerStatus[0]['STATUS'] != "1") { //APPROVED
							$err = true;
							if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '3') {
								$errmsg = 'Pengajuan belum dapat diproses. Cek status perjanjian Mitra Asuransi';
							}

							if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '2') {
								$errmsg = 'Pengajuan belum dapat diproses. Cek status perjanjian Debitur';
							}



							$this->view->errorStatusAsuransi = $errmsg;
							//$valid = false;
						}

						if ($getCustomer[0]['CUST_STATUS'] != "1") { //APPROVED
							$err = true;
							if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '3') {
								$errmsg = 'Pengajuan belum dapat diproses. Cek status nasabah Mitra Asuransi';
							}

							if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '2') {
								$errmsg = 'Pengajuan belum dapat diproses. Cek status Debitur';
							}


							$this->view->errorStatusCustomer = $errmsg;
							//$valid = false;
						}

						$get_linefacility_insurace = $this->_db->select()
							->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_ID", "STATUS", "TICKET_SIZE"])
							->where("CUST_ID = ?", $cust_id)
							->query()->fetchAll();


						$percent10 = (10 / 100) * (float)$get_linefacility_insurace[0]["PLAFOND_LIMIT"];

						if ($get_linefacility_insurace[0]["TICKET_SIZE"] == 1) {

							$ticketSizeAmount = $percent10;
						} elseif ($get_linefacility_insurace[0]["TICKET_SIZE"] == 2) {

							$ticketSizeAmount = $get_linefacility_insurace[0]["PLAFOND_LIMIT"];
						} else {

							$ticketSizeAmount = $get_linefacility_insurace[0]["TICKET_SIZE"];
						}


						if ($data['BG_AMOUNT'] > $ticketSizeAmount) {
							$err = true;
							$errmsg = 'BG Nominal yang diajukan tidak boleh lebih besar dari ticket size asuransi';
							$this->view->errorTickerSize = $errmsg;
							//$valid = false;
						}
					}

					if ($data["COUNTER_WARRANTY_TYPE"] == "2") {
						// get linefacillity

						$get_linefacility = $this->_db->select()
							->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_ID", "STATUS", "TICKET_SIZE"])
							->where("CUST_ID = ?", $cust_id)
							->query()->fetchAll();

						$get_bgamount_on_risks = $this->_db->select()
							->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
							->where("BG_STATUS IN (?)", ["15"])
							->where("COUNTER_WARRANTY_TYPE = ?", "2")
							->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
							->query()->fetchAll();

						$total_bgamount_on_risk = 0;

						foreach ($get_bgamount_on_risks as $key => $get_bgamount_on_risk) {
							$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
						}

						$total_bgamount_on_temp = 0;

						$get_bgamount_on_temps = $this->_db->select()
							->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
							->where("COUNTER_WARRANTY_TYPE = '2'")
							->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
							->where("BG_STATUS in (1,2,3,5,6,7,14,1,20)")
							->query()->fetchAll();

						foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
							$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
						}

						$current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
						// if ($data['BG_AMOUNT'] >= $current_limit) {
						if ($current_limit < 0) {
							$err = true;
						}

						// end get linefacility
					} else if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
						$get_linefacilityINS = $this->_db->select()
							->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT", "STATUS"])
							->where("CUST_ID = ?", $cust_id)
							->query()->fetchAll();


						$check_all_detail = $this->_db->select()
							->from("T_BANK_GUARANTEE_DETAIL")
							->where("PS_FIELDNAME = ?", "Insurance Name")
							->where("PS_FIELDVALUE = ?", $cust_id)
							->query()->fetchAll();

						$total_bgamount_on_risk = 0;

						if (count($check_all_detail) > 0) {
							$save_bg_reg_number = [];
							foreach ($check_all_detail as $value) {
								array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
							}

							$get_bgamount_on_risks = $this->_db->select()
								->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
								->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
								->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
								->query()->fetchAll();

							foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
								$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
							}
						}

						$check_all_detail = $this->_db->select()
							->from("TEMP_BANK_GUARANTEE_DETAIL")
							->where("PS_FIELDNAME = ?", "Insurance Name")
							->where("PS_FIELDVALUE = ?", $cust_id)
							->query()->fetchAll();

						$total_bgamount_on_temp = 0;

						if (count($check_all_detail) > 0) {

							$save_bg_reg_number = [];
							foreach ($check_all_detail as $value) {
								array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
							}

							$get_bgamount_on_temps = $this->_db->select()
								->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
								->where("COUNTER_WARRANTY_TYPE = '3'")
								->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
								->where("BG_STATUS IN (?)", ["5", "6", "7", "14", "17", "20"])
								->query()->fetchAll();

							foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
								$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
							}
						}

						$current_limitINS = $get_linefacilityINS[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;


						$this->view->current_limit_ins = $current_limitINS;
						// if ($data['BG_AMOUNT'] >= $current_limitINS) {
						if ($current_limitINS < 0) {
							$err = true;

							// $errmsg = 'BG Nominal yang diajukan tidak boleh lebih besar dari limit';
							$errmsg = 'Limit asuransi tidak tersedia';
							$this->view->errorTickerSize = $errmsg;
						}

						//die;

					}



					if ($this->view->warranty_type == "2") {
						if ($this->view->status_linefacility == "2" or $this->view->status_linefacility == "3" or $this->view->status_linefacility == "4") {
							$err = true;
							// $errmsg = "Limit tidak mencukupi";
							// $this->view->errorStatusAccount = $errmsg;
						}
					}

					// end validasi

					$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
					$approve = $this->_getParam('release');
					$savework = $this->_getParam('savework');
					$redebate = $this->_getParam('redebate');
					$resetall = $this->_getParam('reset');
					$refresh = $this->_getParam('refresh');

					//$BG_NUMBER = $this->_getParam('bgnumb');
					$BG_NUMBER 	= urldecode($this->_getParam('bgnumb'));

					$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
					$reject = $this->_getParam('reject');
					$repair = $this->_getParam('repair');
					$filter             = new Application_Filtering();
					$inputtoken1        = $this->_getParam('inputtoken1');
					$inputtoken2        = $this->_getParam('inputtoken2');
					$inputtoken3        = $this->_getParam('inputtoken3');
					$inputtoken4        = $this->_getParam('inputtoken4');
					$inputtoken5        = $this->_getParam('inputtoken5');
					$inputtoken6        = $this->_getParam('inputtoken6');

					$responseCode       = $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

					$responseCode       = $filter->filter($responseCode, "SELECTION");
					//print_r($edit);die;
					//var_dump($redebate);die;
					if ($redebate) {
						//die('a');
						// add atan
						$change_id = $this->suggestionWaitingApproval('Request Rebate', "Request Rebate", $this->_changeType['code']['new'], null, 'TEMP_BANK_GUARANTEE', 'TEMP_REQUEST_REBATE', '-', '-', '-', '-');

						$insertData = [
							"CHANGES_ID" => $change_id,
							"PROVISION_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam('REDEBATE_AMOUNT')),
							"BG_REG_NUMBER" => $BG_NUMBER,
							"REBATE_SUGGESTED" => new Zend_Db_Expr("now()"),
							"REBATE_SUGGESTEDBY" => $this->_userIdLogin
						];

						$this->_db->beginTransaction();
						$this->_db->insert("TEMP_REQUEST_REBATE", $insertData);
						$this->_db->commit();

						$this->_db->update("T_GLOBAL_CHANGES", ["module" => "requestrebate"], ["CHANGES_ID = ?" => $change_id]);

						$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						$this->_redirect('/notification/success/index');

						// $data = array(
						// 	//'REDEBATE_STATUS' => $this->_getParam('verifyindex'),
						// 	'REDEBATE_TYPE' => $this->_getParam('redebate_type'),
						// 	'REDEBATE_AMOUNT' => Application_Helper_General::convertDisplayMoney($this->_getParam('REDEBATE_AMOUNT'))
						// );

						// $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
						// $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);
						// $this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						// $this->_redirect('/notification/success/index');
					}

					if ($refresh) {
						$datas = $this->_request->getParams();
						$index_code = $datas["refresh"];

						$this->_db->delete("TEMP_BGVERIFY_DETAIL", "TEMP_BGVERIFY_DETAIL.BG_REG_NUMBER = '$BG_NUMBER' AND TEMP_BGVERIFY_DETAIL.INDEX = '$index_code'");

						if ($index_code == "3") {
							$this->_db->update('TEMP_BANK_GUARANTEE', [
								"SP_OBLIGEE_CODE" => ""
							], ["BG_REG_NUMBER = ?" => $BG_NUMBER]);
						}

						if ($index_code == "9") {
							$this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL', "TEMP_BANK_GUARANTEE_DETAIL.BG_REG_NUMBER = '$BG_NUMBER' AND TEMP_BANK_GUARANTEE_DETAIL.PS_FIELDNAME = 'SLIK OJK Document'");
						}

						if ($index_code == "10") {
							$this->_db->update('TEMP_BANK_GUARANTEE', [
								"AGREE_FORMAT" => "",
								"MEMO_LEGAL" => ""
							], ["BG_REG_NUMBER = ?" => $BG_NUMBER]);
						}

						$AESMYSQL = new Crypt_AESMYSQL();
						$rand = $this->token;

						$encrypted_payreff = $AESMYSQL->encrypt($BG_NUMBER, $rand);
						$bg_number_encrypt = urlencode($encrypted_payreff);

						$this->_redirect("/eformworkflow/verificationdetail/index/bgnumb/" . urlencode($bg_number_encrypt));
					}

					if ($resetall) {

						$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;

						$this->_db->delete('TEMP_BGVERIFY_DETAIL', $where);

						$this->_db->update('TEMP_BANK_GUARANTEE', [
							"SP_OBLIGEE_CODE" => ""
						], ["BG_REG_NUMBER = ?" => $BG_NUMBER]);

						$this->_db->update('TEMP_BANK_GUARANTEE_DETAIL', [
							"PS_FIELDVALUE" => ""
						], ["BG_REG_NUMBER = ?" => $BG_NUMBER, "PS_FIELDNAME=?" => "SLIK OJK Document"]);


						$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] != "1") {
							Application_Helper_General::writeLog('VVNC', "Reset Save Work BG. Noreg : [" . $BG_NUMBER . "]");
						} else {
							Application_Helper_General::writeLog('VVCC', "Reset Save Work BG. Noreg : [" . $BG_NUMBER . "]");
						}
						$this->_redirect('/notification/success/index');
					}


					if ($approve) {
						$valid = true;
						if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == "3") {
							$svcAccount = new Service_Account($insuranceBranch[0]["INS_BRANCH_ACCT"], 'IDR');
							$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
							//echo '2';
							//var_dump($result);
							//var_dump($result);
							if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
								$total = $principleData["Principle Insurance Premium"] + $principleData["Principle Insurance Administration"] + $principleData["Principle Insurance Stamp"];
								if ($result['balanceactive'] > $total) {
									$err = true;
									$errmsg = 'balance is not enough';
									$this->view->errorwaranty = $errmsg;
									$valid = false;
								}
							} else {
								$err = true;
								$errmsg = 'Service not available';
								$this->view->errorwaranty = $errmsg;
								$valid = false;
							}
						}
						if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
							if (!empty($bgdatasplit)) {

								//var_dump($bgdatasplit);
							}
						}


						if (!empty($data['FEE_CHARGE_TO'])) {
							$svcAccount = new Service_Account($data['FEE_CHARGE_TO'], 'IDR');
							$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
							//echo '1';
							//var_dump($result);

							if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
								$total = $data["PROVISION_FEE"] + $data["ADM_FEE"] + $data["STAMP_FEE"] + $principleData["Principle Insurance Premium"] + $principleData["Principle Insurance Administration"] + $principleData["Principle Insurance Stamp"];
								if ($result['balanceactive'] > $total) {
									$err = true;
									$errmsg = 'balance is not enough';
									$this->view->errorcharge = $errmsg;
									$valid = false;
								}
							} else {
								$err = true;
								$errmsg = 'Service not available';
								$this->view->errorcharge = $errmsg;
								$valid = false;
							}
						}
						//var_dump($valid);die;
						if ($valid) {

							$attachmentDestination   = UPLOAD_PATH . '/document/submit/';
							$adapter = new Zend_File_Transfer_Adapter_Http();
							$files = $adapter->getFileInfo();


							if ($data["COUNTER_WARRANTY_TYPE"] != "1") {
								if (empty($principleData["SLIK OJK Document"])) {
									$ojkFile = $files["slikOjk"];
									if (($ojkFile['size'] + 0) > 0) {
										$ojkFileName = $ojkFile["name"];
										$adapter->setDestination($attachmentDestination);

										$date = date("dmy");
										$time = date("his");

										$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $ojkFileName;

										if (file_exists($attachmentDestination . $newFileName2)) {
											$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $ojkFileName;
										}

										$adapter->addFilter('Rename', $newFileName2, "slikOjk");
										$adapter->receive("slikOjk");

										$this->_db->beginTransaction();
										$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', [
											"BG_REG_NUMBER" => $BG_NUMBER,
											"CUST_ID" => $this->_custIdLogin,
											"USER_ID" => $this->_userIdLogin,
											"PS_FIELDNAME" => "SLIK OJK Document",
											"PS_FIELDTYPE" => 1,
											"PS_FIELDVALUE" => $newFileName2
										]);
										$this->_db->commit();
									}
								}
							}

							if ($data["BG_FORMAT"] == "2") {
								if (empty($data["AGREE_FORMAT"])) {
									$agreeFormatFile = $files["formatdisetujui"];
									if (($agreeFormatFile['size'] + 0) > 0) {
										$agreeFormatFileName = $agreeFormatFile["name"];
										$adapter->setDestination($attachmentDestination);

										$date = date("dmy");
										$time = date("his");

										$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $agreeFormatFileName;

										if (file_exists($attachmentDestination . $newFileName2)) {
											$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $agreeFormatFileName;
										}

										$adapter->addFilter('Rename', $newFileName2, "formatdisetujui");
										$adapter->receive("formatdisetujui");

										$this->_db->update('TEMP_BANK_GUARANTEE', [
											"AGREE_FORMAT" => $newFileName2
										], [
											"BG_REG_NUMBER = ?" => $BG_NUMBER
										]);
									}
								}

								if (empty($data["MEMO_LEGAL"])) {
									$memoLegalFile = $files["memopersetujuan"];
									if (($memoLegalFile['size'] + 0) > 0) {
										$memoLegalFileName = $memoLegalFile["name"];
										$adapter->setDestination($attachmentDestination);

										$date = date("dmy");
										$time = date("his");

										$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $memoLegalFileName;

										if (file_exists($attachmentDestination . $newFileName2)) {
											$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $memoLegalFileName;
										}

										$adapter->addFilter('Rename', $newFileName2, "memopersetujuan");
										$adapter->receive("memopersetujuan");

										$this->_db->update('TEMP_BANK_GUARANTEE', [
											"MEMO_LEGAL" => $newFileName2
										], [
											"BG_REG_NUMBER = ?" => $BG_NUMBER
										]);
									}
								}
							}

							// Surat permohonan kuasa direksi
							if (empty($this->view->check11)) {

								$kuasaDireksiFile = $files["permohonanbgfile"];
								if (($kuasaDireksiFile['size'] + 0) > 0) {
									$kuasaDireksiFileName = $kuasaDireksiFile["name"];
									$adapter->setDestination($attachmentDestination);

									$date = date("dmy");
									$time = date("his");

									$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $kuasaDireksiFileName;

									if (file_exists($attachmentDestination . $newFileName2)) {
										$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $kuasaDireksiFileName;
									}

									$adapter->addFilter('Rename', $newFileName2, "permohonanbgfile");
									$adapter->receive("permohonanbgfile");

									$this->_db->update('TEMP_BANK_GUARANTEE', [
										"KUASA_DIREKSI_FILE" => $newFileName2
									], [
										"BG_REG_NUMBER = ?" => $BG_NUMBER
									]);
								}
							}
							// ------------------------------------

							$verifyData = $this->_db->select()
								->from(array('A' => 'TEMP_BGVERIFY_DETAIL'), array('*'))
								->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
								->query()->fetchAll();
							//var_dump(count($verifyData));die;
							$checkVerify = count($this->_getParam('verifyindex'));
							if ($this->_getParam('verifyindex') == "") {
								$checkVerify = 0;
							}
							//$datas = $this->_request->getParams();
							$totalVerify = count($verifyData) + $checkVerify;

							$total = 7;

							if (count($checkOthersAttachment) > 0) {
								$total += 1;
							}

							if ($data["BG_FORMAT"] == "2") {
								$total += 1;
							}

							if ($data["COUNTER_WARRANTY_TYPE"] != "1") {
								$total += 1;
							}
							//echo $total;
							//var_dump($totalVerify);die;
							if ($totalVerify == $total) {
								if (empty($err)) {
									/*if (!empty($usergoogleAuth)) {
	 
										$pga = new PHPGangsta_GoogleAuthenticator();
										// var_dump($usergoogleAuth['0']['GOOGLE_CODE']);
										 //var_dump($responseCode);die;
											$settingObj = new Settings();
											$gduration = $settingObj->getSetting("google_duration");
										 if ($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, $gduration)) { */
									$this->_db->beginTransaction();
									$resultToken = $resHard['ResponseCode'] == '0000';
									$datatoken = array(
										'USER_FAILEDTOKEN' => 0
									);

									$wheretoken =  array();
									$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
									$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
									$data = $this->_db->update('M_USER', $datatoken, $wheretoken);

									$refCode = $this->_getParam('refCode');
									$obligeecode =  $this->_getParam('COMPANY_ID');

									$data = array(
										'BG_STATUS' => '6',
										'BG_REFCODE' => $refCode,
										'SP_OBLIGEE_CODE' => $obligeecode,
									);

									$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
									$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

									//bgverify detail
									$verifydt = $this->_getParam('verifyindex');
									if (!empty($verifydt)) {
										foreach ($verifydt as $vl) {
											$verifyInsert = array(
												'VERIFIED'          => new Zend_Db_Expr("now()"),
												'BG_REG_NUMBER'     => $BG_NUMBER,
												'CUST_ID'           => $this->_custIdLogin,
												'VERIFIEDBY'        => $this->_userIdLogin,
												'INDEX' 		    => $vl
											);

											$this->_db->insert('TEMP_BGVERIFY_DETAIL', $verifyInsert);
										}
									}

									//Others Attachment Documents
									$footnotes = $this->_getParam('footnotes');
									foreach ($footnotes as $key => $value) {

										$updatenotes = array(
											'FILE_NOTE' => $value,
										);

										$this->_db->update('TEMP_BANK_GUARANTEE_FILE', $updatenotes, "TEMP_BANK_GUARANTEE_FILE.BG_REG_NUMBER = '$BG_NUMBER' AND TEMP_BANK_GUARANTEE_FILE.INDEX = '$key'");
									}


									// $notes = $this->_getParam('PS_REASON_REJECT');

									$historyInsert = array(
										'DATE_TIME'         => new Zend_Db_Expr("now()"),
										'BG_REG_NUMBER'         => $BG_NUMBER,
										'CUST_ID'           => $this->_custIdLogin,
										'USER_LOGIN'        => $this->_userIdLogin,
										'HISTORY_STATUS'    => 6
										//  'BG_REASON'         => $notes,
									);

									$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
									$this->_db->commit();
									$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
									if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] != "1") {
										Application_Helper_General::writeLog('VNCS ', "Verifikasi Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
									} else {
										Application_Helper_General::writeLog('VCCS', "Verifikasi Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
									}
									$this->_redirect('/notification/success/index');
								}
							} else {
								$this->view->error = true;
								$errorMsg[] = $this->language->_('Masih ditemukan kesalahan, silahkan refresh ceklis terlebih dahulu');  //$verToken['ResponseDesc'];
								$this->view->report_msg           = $errorMsg[0];
							}
						}


						/* } else {
                                        $tokenFailed = $CustUser->setLogToken(); //log token activity

                                        $error = true;
                                        $errorMsg[] = $this->language->_('Invalid Response Code');  //$verToken['ResponseDesc'];
                                        $this->view->popauth = true;
                                        if ($tokenFailed === true) {
                                            $this->_redirect('/default/index/logout');
                                        }
                                     } */

						//$resultToken = $resHard['ResponseCode'] == '0000';
						//}

						if ($error) {
							$this->view->popauth                = true;
							//var_dump($errorMsg);
							$this->view->errorMsg           = $errorMsg[0];
						}
					}

					if ($savework) {
						$datas = $this->_request->getParams();

						$attachmentDestination   = UPLOAD_PATH . '/document/submit/';
						$adapter = new Zend_File_Transfer_Adapter_Http();
						$files = $adapter->getFileInfo();

						if (!empty($datas['verifyindex'])) {
							foreach ($datas['verifyindex'] as $vl) {
								$verifyInsert = array(
									'VERIFIED'          => new Zend_Db_Expr("now()"),
									'BG_REG_NUMBER'     => $BG_NUMBER,
									'CUST_ID'           => $this->_custIdLogin,
									'VERIFIEDBY'        => $this->_userIdLogin,
									'INDEX' 		    => $vl
								);

								$this->_db->insert('TEMP_BGVERIFY_DETAIL', $verifyInsert);
							}
						}

						$get_all_verifies = $this->_db->select()
							->from("TEMP_BGVERIFY_DETAIL", ["*"])
							->where("BG_REG_NUMBER = " . $this->_db->quote($BG_NUMBER))
							->query()->fetchAll();

						foreach ($get_all_verifies as $get_all_verify) {
							if ($get_all_verify["INDEX"] == "3") $bankGuaranteeRecipient = 1;
							if ($get_all_verify["INDEX"] == "9") $slikOjk = 1;
							if ($get_all_verify["INDEX"] == "10") $agreeFormat = 1;
							if ($get_all_verify["INDEX"] == "11") $docBg = 1;
						}

						if ($slikOjk) {
							$ojkFile = $files["slikOjk"];
							if (($ojkFile['size'] + 0) > 0) {
								$ojkFileName = $ojkFile["name"];
								$adapter->setDestination($attachmentDestination);

								$date = date("dmy");
								$time = date("his");

								$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $ojkFileName;

								if (file_exists($attachmentDestination . $newFileName2)) {
									$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $ojkFileName;
								}

								$adapter->addFilter('Rename', $newFileName2, "slikOjk");
								$adapter->receive("slikOjk");

								$this->_db->beginTransaction();
								$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', [
									"BG_REG_NUMBER" => $BG_NUMBER,
									"CUST_ID" => $this->_custIdLogin,
									"USER_ID" => $this->_userIdLogin,
									"PS_FIELDNAME" => "SLIK OJK Document",
									"PS_FIELDTYPE" => 1,
									"PS_FIELDVALUE" => $newFileName2
								]);
								$this->_db->commit();
							}
						}

						if ($agreeFormat) {
							$agreeFormatFile = $files["formatdisetujui"];
							if (($agreeFormatFile['size'] + 0) > 0) {
								$agreeFormatFileName = $agreeFormatFile["name"];
								$adapter->setDestination($attachmentDestination);

								$date = date("dmy");
								$time = date("his");

								$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $agreeFormatFileName;

								if (file_exists($attachmentDestination . $newFileName2)) {
									$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $agreeFormatFileName;
								}

								$adapter->addFilter('Rename', $newFileName2, "formatdisetujui");
								$adapter->receive("formatdisetujui");

								$this->_db->update('TEMP_BANK_GUARANTEE', [
									"AGREE_FORMAT" => $newFileName2
								], [
									"BG_REG_NUMBER = ?" => $BG_NUMBER
								]);
							}

							$memoLegalFile = $files["memopersetujuan"];
							if (($memoLegalFile['size'] + 0) > 0) {
								$memoLegalFileName = $memoLegalFile["name"];
								$adapter->setDestination($attachmentDestination);

								$date = date("dmy");
								$time = date("his");

								$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $memoLegalFileName;

								if (file_exists($attachmentDestination . $newFileName2)) {
									$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $memoLegalFileName;
								}

								$adapter->addFilter('Rename', $newFileName2, "memopersetujuan");
								$adapter->receive("memopersetujuan");

								$this->_db->update('TEMP_BANK_GUARANTEE', [
									"MEMO_LEGAL" => $newFileName2
								], [
									"BG_REG_NUMBER = ?" => $BG_NUMBER
								]);
							}
						}

						if ($bankGuaranteeRecipient) {
							$data = array(
								'SP_OBLIGEE_CODE' => $datas['COMPANY_ID']
							);

							$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
							$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);
						}

						if ($docBg) {
							// Surat permohonan kuasa direksi

							$kuasaDireksiFile = $files["permohonanbgfile"];
							if (($kuasaDireksiFile['size'] + 0) > 0) {
								$kuasaDireksiFileName = $kuasaDireksiFile["name"];
								$adapter->setDestination($attachmentDestination);

								$date = date("dmy");
								$time = date("his");

								$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $kuasaDireksiFileName;

								if (file_exists($attachmentDestination . $newFileName2)) {
									$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $kuasaDireksiFileName;
								}

								$adapter->addFilter('Rename', $newFileName2, "permohonanbgfile");
								$adapter->receive("permohonanbgfile");

								$this->_db->update('TEMP_BANK_GUARANTEE', [
									"KUASA_DIREKSI_FILE" => $newFileName2
								], [
									"BG_REG_NUMBER = ?" => $BG_NUMBER
								]);
							}

							// ------------------------------------
						}

						//Others Attachment Documents
						$footnotes = $this->_getParam('footnotes');
						foreach ($footnotes as $key => $value) {

							$updatenotes = array(
								'FILE_NOTE' => $value,
							);

							$this->_db->update('TEMP_BANK_GUARANTEE_FILE', $updatenotes, "TEMP_BANK_GUARANTEE_FILE.BG_REG_NUMBER = '$BG_NUMBER' AND TEMP_BANK_GUARANTEE_FILE.INDEX = '$key'");
						}

						$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] != "1") {
							Application_Helper_General::writeLog('VVNC', "Save Work Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
						} else {
							Application_Helper_General::writeLog('VVCC', "Save Work Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
						}

						$this->_redirect('/notification/success/index');
						//echo '<pre>';
						//var_dump($datas);die;


					}

					if ($reject) {
						$data = array('BG_STATUS' => '12');
						$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
						$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);



						$notes = $this->_getParam('PS_REASON_REJECT');
						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'         => $BG_NUMBER,
							'CUST_ID'           => $this->_custIdLogin,
							'USER_LOGIN'        => $this->_userIdLogin,
							'HISTORY_STATUS'    => 9,
							'BG_REASON'         => $notes,
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

						$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] != "1") {
							Application_Helper_General::writeLog('VVNC', "Menolak Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
						} else {
							Application_Helper_General::writeLog('VVCC', "Menolak Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
						}
						$this->_redirect('/notification/success/index');
					}

					if ($repair) {
						//die('here');
						$repairto = $this->_getParam('repairto');
						$data = array('BG_STATUS' => '10', "PRINCIPLE_APPROVE" => '0');
						if ($repairto == "1") {
							$data = array('BG_STATUS' => '11', "PRINCIPLE_APPROVE" => '0');
						}
						$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
						$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

						$notes = $this->_getParam('PS_REASON_REPAIR');

						$bg_status = $repairto == "1" ? 15 : 10;

						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'         => $BG_NUMBER,
							'CUST_ID'           => $this->_custIdLogin,
							'USER_LOGIN'        => $this->_userIdLogin,
							'HISTORY_STATUS'    => $bg_status,
							'BG_REASON'
							=> $notes,
						);

						$this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL', "TEMP_BANK_GUARANTEE_DETAIL.BG_REG_NUMBER = '$BG_NUMBER' AND TEMP_BANK_GUARANTEE_DETAIL.PS_FIELDNAME = 'Info Slik OJK'");
						//Application_Helper_General::writeLog('DELI', "Request Repair " . $BG_NUMBER);
						$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

						$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] != "1") {
							Application_Helper_General::writeLog('VVNC', "Perbaikan Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
						} else {
							Application_Helper_General::writeLog('VVCC', "Perbaikan Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
						}
						$this->_redirect('/notification/success/index');
					}

					if ($err) {
						//$err = true;
						$errmsg = 'Error Validasi';
						//var_dump($errmsg);
						$this->view->err = true;
						$this->view->errmsg = $errmsg;
					}
				}
			}
		}
		if ($data["COUNTER_WARRANTY_TYPE"] != "1") {
			Application_Helper_General::writeLog('VVNC', "Lihat Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
		} else {
			Application_Helper_General::writeLog('VVCC', "Lihat Pengajuan BG. Noreg : [" . $BG_NUMBER . "]");
		}
	}


	public function findPolicyBoundary($transfertype, $amount)
	{


		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{



		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
		//	echo '<pre>';
		//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	public function removetempAction()
	{
		$this->_db->delete("TEMP_REQUEST_REBATE", "1");
	}

	public function getbusersbgcAction()
	{
		$getprivi = $this->_db->select()
			->from(["MBPG" => "M_BPRIVI_GROUP"], ["*"])
			->where("MBPG.BPRIVI_ID = ?", "SBGC")
			->query()->fetchAll();

		$savegroup = [];
		foreach ($getprivi as $key => $value) {
			array_push($savegroup, $value["BGROUP_ID"]);
		}

		$getbuser = $this->_db->select()
			->from(["MB" => "M_BUSER"], ["*"])
			->joinleft(["BRANCH" => "M_BRANCH"], "BRANCH.ID = MB.BUSER_BRANCH", ["BRANCH.BRANCH_NAME"])
			->where("MB.BGROUP_ID IN (?)", $savegroup);

		$getbuser->where("MB.BUSER_BRANCH = ?", "26");

		$getbuser->where("MB.BUSER_NAME LIKE ?", "%" . "KRI" . "%");

		$getbuser = $getbuser->query()->fetchAll();

		var_dump($getbuser);
		die();
	}
}
