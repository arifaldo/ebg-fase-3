<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once('Crypt/AES.php');
class user_EmailController extends Application_Main
{
	public function initController()
	{

		$this->_helper->layout()->setLayout('popup');
	}
	public function indexAction()
	{
		//die('here');
		$set = new Settings();
		$FEmailResetPass = $set->getSetting('femailtemplate_resetpwd');

		$templateEmailMasterBankAddress = $set->getSetting('master_bank_address');
		$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
		$templateEmailMasterBankAppUrl = $set->getSetting('master_bank_app_url');
		$templateEmailMasterBankCity = $set->getSetting('master_bank_city');
		$templateEmailMasterBankCountry = $set->getSetting('master_bank_country');
		$templateEmailMasterBankEmail = $set->getSetting('master_bank_email');
		$templateEmailMasterBankEmail1 = $set->getSetting('master_bank_email1');
		$templateEmailMasterBankFax = $set->getSetting('master_bank_fax');
		$templateEmailMasterBankName = $set->getSetting('master_bank_name');
		$templateEmailMasterBankProvince = $set->getSetting('master_bank_province');
		$templateEmailMasterBankTelp = $set->getSetting('master_bank_telp');
		$templateEmailMasterBankWapp = $set->getSetting('master_bank_wapp');
		$templateEmailMasterBankWebsite = $set->getSetting('master_bank_website');
		$url_fo = $set->getSetting('url_fo');

		$user_id = $this->_getParam('user_id');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$userenc      = urldecode($this->_getParam('user_id'));
		$user_id = $AESMYSQL->decrypt($userenc, $password);
		$cust_id = $this->_custIdLogin;

		$isi = $this->_db->SELECT()
			->FROM('M_USER', array('USER_ID', 'CUST_ID', 'USER_EMAIL', 'USER_FULLNAME', 'USER_CLEARTEXT_PWD'))
			->WHERE('USER_ID = ? ', $user_id)
			->WHERE('CUST_ID = ?', $cust_id);

		$isi = $this->_db->fetchrow($isi);
		$user = ($this->sslEnc($user_id));
		$cust = ($this->sslEnc($isi['CUST_ID']));


		$str = rand();
		$rand = md5($str);
		// echo $result; die;
		$newpassword = $url_fo . '/pass/index?safetycheck=&code=' . urldecode($rand) . '&cust_id=' . urlencode($cust) . '&user_id=' . urlencode($user);
		//die($newpassword);
		//$newpassword = substr(base64_decode($isi['USER_CLEARTEXT_PWD']),4, -4);
		$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));

		$FEmailResetPass = str_ireplace('[[user_fullname]]', $isi['USER_FULLNAME'], $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[comp_accid]]', $isi['CUST_ID'], $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[user_login]]', $isi['USER_ID'], $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[user_email]]', $isi['USER_EMAIL'], $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[confirm_link]]', $newpassword, $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[exp_date]]', $datenow, $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankEmail, $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[master_bank_telp]]', $templateEmailMasterBankTelp, $FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[master_bank_wapp]]', $templateEmailMasterBankWapp, $FEmailResetPass);


		$this->view->forms = $FEmailResetPass;
		$filename = $user_id . '_PDF';
		$email = $this->_getParam('email');

		// if($email)
		{
			$status = Application_Helper_Email::sendEmail($isi['USER_EMAIL'], 'Reset Password Information', $FEmailResetPass);
			if ($status == 1) {
				$data = array(
					'USER_RPWD_ISEMAILED' => 1,
					'USER_DATEPASS' => new Zend_Db_Expr('now() + INTERVAL 1 DAY')
				);
				$where =  array();
				$where['CUST_ID = ?'] = $cust_id;
				$where['USER_ID = ?'] = $user_id;
				$updated = $this->_db->update('M_USER', $data, $where);

				Application_Helper_General::writeLog('CSRL', 'Menyetujui Kirim Link Email Kata Sandi, User : ' . $isi['USER_FULLNAME'] . ' (' . $user_id . ')');
				$data = array(
					'USER_RPWD_ISEMAILED' 			=> 1,
				);
				$where =  array();
				$where['CUST_ID 		 = ?'] 	= $cust_id;
				$where['USER_ID 		 = ?'] 	= $user_id;
				$this->_db->update('M_USER', $data, $where);
				$this->view->forms = $FEmailResetPass;
			} else
				Application_Helper_General::writeLog('CSRL', 'Gagal Kirim Link Email Kata Sandi, User : ' . $isi['USER_FULLNAME'] . ' (' . $user_id . ')');
			$this->view->notification = $status;
		}
		// else
		// Application_Helper_General::writeLog('CSRL','View User Email Cust Id ( '.$cust_id.' ) ,User Id ( '.$user_id.' )');
	}

	public function sslPrm()
	{
		return array("6a1f325be4c0492063e83a8cb2cb9ae7", "IV (optional)", "aes-128-cbc");
	}

	public function sslEnc($msg)
	{
		list($pass, $iv, $method) = $this->sslPrm();
		return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}
}
