<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
class accountstatement_LoanaccountController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$conf = Zend_Registry::get('config');
		$acctDposito = $conf['productaccount']['type']['desc']['loan'];
		$userId = $this->_userIdLogin;
		$fields = array(
						'acct'  => array('field' => 'ACCT_NO',
											   'label' => $this->language->_('Account Number'),
											   'sortable' => true),
						'acct_name'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Account Name'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						'prod_type'   => array('field'    => 'ACCT_PRODUCT_NAME',
											  'label'    => $this->language->_('Product Type'),
											  'sortable' => true),
				);
				
		$sortBy  = $this->_getParam('sortby','acct');
		$sortDir = $this->_getParam('sortdir','asc');
		

		/*$data = array();

		$data[0]['ACCT_NO'] = "100345721001";
		$data[0]['PROD_TYPE'] = "Pinjaman BJB";
		$data[0]['PLAFOND'] = "100,000,000.00";
		$data[0]['OUTSTANDING'] = "60,000,000.00";

		$data[1]['ACCT_NO'] = "1003457823004";
		$data[1]['PROD_TYPE'] = "Pinjaman BJB";
		$data[1]['PLAFOND'] = "150,000,000.00";
		$data[1]['OUTSTANDING'] = "45,000,000.00";*/			
										  
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
	/*	$data = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->where("D.ACCT_TYPE = ". $this->_db->quote($acctDposito))
						 ->group('A.ACCT_NO')						 
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
		*/
		$model = new accountstatement_Model_Accountstatement();
		$userCIF = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_USER'))
				->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
				->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
				->limit(1)
		);
		
		$param['userId'] = $userId;
		$userAcctInfo = $model->getCustAccount($param);
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];
		//print_r($accountCcy);
		//print_r($accountNo);die;
		if(!empty($userAcctInfo)){
		$Account = new Account($accountNo, $accountCcy, $accountType, $userCIF['CUST_CIF'], $userId);
		$Account->setFlag(false);
		$res = $Account->checkBalanceLoan();
		$accountListData = $res['AccountList'];
		
		
				
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$this->view->custId = $this->_custIdLogin;
// 		$this->view->resultdata = $data;
// 		$this->view->fields = $fields;
		
		$totalAvailableBalance = 0;
		if (count ( $accountListData ) > 0) 
		{
			$data = array();
			$dataprint = array();
			foreach ( $accountListData as $key=>$row ) {
				/*$Account = new Account($row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
				$Account->setFlag(false);
				$Account->checkBalanceLoan();*/
				if (isset($row ['accountNo'])){
					/*$productList = $this->_db->select()->from(
		   		    		array('MPT' => 'M_PRODUCT_TYPE'),
		   		    		array('MPT.PRODUCT_CODE', 'MPT.PRODUCT_NAME')
		   		    	)
	 	   		    	->where('MPT.PRODUCT_CODE = ?',$row ['productPlan'])
	 	   		    	->where('MPT.PRODUCT_PLAN = ?',$row ['planCode'])
	 	   		    	->query()
		    			->fetchAll()
		    		;	    				
		    		$accountName = $productList[0]['PRODUCT_NAME'] == NULL ? "N/A" : $productList[0]['PRODUCT_NAME'];*/
		    		
		    		$productList = $model->getProduct($row ['planCode'],$row ['productPlan']);		    		
					$product_name = $productList[0]['PRODUCT_NAME'] == NULL ? "N/A" : $productList[0]['PRODUCT_NAME'];
					
					//if ($row ['status'] == "A"){								
						$data[$key]['accountNo'] 			= $row ['accountNo'];
						$data[$key]['accountName'] 			= $product_name;
						$data[$key]['status'] 				= $row ['status'];				
						$data[$key]['currency'] 			= $row ['currency'];				
						$data[$key]['plafond'] 				= $row ['plafond'];
						$data[$key]['numberOfInstallment'] 	= $row ['numberOfInstallment'];
						$data[$key]['outstanding'] 			= $row ['outstanding'];
						$data[$key]['interestObligations'] 	= $row ['interestObligations'];
						$data[$key]['installment'] 			= $row ['installment'];
						$data[$key]['totalPayment'] 		= $row ['totalPayment'];
						$data[$key]['interestArrears'] 		= $row ['interestArrears'];
						$data[$key]['arrearsInInstallments']= $row ['arrearsInInstallments'];
						$data[$key]['paymentOfFines'] 		= $row ['paymentOfFines'];
						$data[$key]['termsLoansTimes'] 		= $row ['termsLoansTimes'];
						$data[$key]['accountCreateDate'] 	= $row ['accountCreateDate'];
						$data[$key]['disbursementDate'] 	= $row ['disbursementDate'];
						$data[$key]['nextPaymentDate'] 		= $row ['nextPaymentDate'];	
						$data[$key]['dueDate'] 				= $row ['dueDate'];		
						$dataprint[$key]['accountNo'] 		= $row ['accountNo'];
						$dataprint[$key]['accountName'] 	= $row ['accountName'];
						$dataprint[$key]['plafond'] 		= $row ['plafond'];
						$dataprint[$key]['outstanding'] 	= $row ['outstanding'];						
					//}	
				}
			}
		}
	}
		$logDesc = 'Viewing';
		
		if($csv || $pdf){			
			$arr = $dataprint;
			$header = array(
				$this->language->_('Account No'),
				$this->language->_('Product Type'),
				$this->language->_('Plafond'),
				$this->language->_('Outstanding')
			);
		}
		if($csv)
		{
			$logDesc = 'Export to CSV';
			$this->_helper->download->csv($header,$arr,null,$this->language->_('Loan Account Inquiry'));  
		}
		
		if($pdf){
			$logDesc = 'Export to PDF';
			$this->_helper->download->pdf($header,$arr,null,$this->language->_('Loan Account Inquiry'));  
		}
		
		
		if($this->_request->getParam('print') == 1){
			$arr = $dataprint;
			$fields = array(
						'ACCT_NO'  => array('field' => 'accountNo',
											   'label' => $this->language->_('Account No'),
											   'sortable' => true),
						'ACCT_NAME'  => array('field' => 'accountName',
											   'label' => $this->language->_('Product Type'),
											   'sortable' => true),
						'CCY_ID'   => array('field'    => 'plafond',
											  'label'    => $this->language->_('Plafond'),
											  'sortable' => true),
			
						'PROD_TYPE'   => array('field'    => 'outstanding',
											  'label'    => $this->language->_('Outstanding'),
											  'sortable' => true)
			);
			
//            $data = $arr;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Loan Account Inquiry', 'data_header' => $fields));
        }
		
		
		Application_Helper_General::writeLog('BAIQ',$logDesc);
		$this->view->userId = $this->_userIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
		$this->view->totalAvailableBalance = $totalAvailableBalance;
	}
	
	public function detailAction(){
		//die('here');
		$this->_helper->layout()->setLayout('newlayout');
		$acct_no = $this->_getParam('acct_no');

		$ccy_id = $this->_getParam('ccy_id');
		$prod = $this->_getParam('prod');
		$status = $this->_getParam('status');
		$plafond = $this->_getParam('plafond');
		$numberOfInstallment = $this->_getParam('numberOfInstallment');
		$outstanding = $this->_getParam('outstanding');
		$interestObligations = $this->_getParam('interestObligations');
		$installment = $this->_getParam('installment');
		$totalPayment = $this->_getParam('totalPayment');
		$interestArrears = $this->_getParam('interestArrears');
		$arrearsInInstallments = $this->_getParam('arrearsInInstallments');
		$paymentOfFines = $this->_getParam('paymentOfFines');
		$termsLoansTimes = $this->_getParam('termsLoansTimes');
		$accountCreateDate = $this->_getParam('accountCreateDate');
		$disbursementDate = $this->_getParam('disbursementDate');
		$nextPaymentDate = $this->_getParam('nextPaymentDate');
		$dueDate = $this->_getParam('dueDate');

		//print_r($dueDate );die;

		if(empty($ccy_id)){
			$ccy_id = '';
		}
		
		/*$Account = new Account($acct_no,Application_Helper_General::getCurrNum($ccy_id));
		$Account->setFlag(false);
		$Account->checkBalanceLoan();		
		
		$this->view->detail = $Account->getCoreAccountDetail();*/
		
		$this->view->product = $prod;
		$this->view->acct_no = $acct_no;
		$this->view->ccy_id = $ccy_id;
		$this->view->status = $status;
		$this->view->plafond = $plafond;
		$this->view->numberOfInstallment = $numberOfInstallment;
		$this->view->outstanding = $outstanding;
		$this->view->interestObligations = $interestObligations;
		$this->view->installment = $installment;
		$this->view->totalPayment = $totalPayment;
		$this->view->interestArrears = $interestArrears;
		$this->view->arrearsInInstallments = $arrearsInInstallments;
		$this->view->paymentOfFines = $paymentOfFines;
		$this->view->termsLoansTimes = $termsLoansTimes;
		$this->view->accountCreateDate = $accountCreateDate;
		$this->view->disbursementDate = $disbursementDate;
		$this->view->nextPaymentDate = $nextPaymentDate;
		$this->view->dueDate = $dueDate;
		
		
		$data = array();
		$data['product'] = 'LOAN';
		$data['acct_no'] = $acct_no;
		$data['ccy_id'] = $ccy_id;
		$data['status'] = $status;
		$data['plafond'] = $plafond;
		$data['numberOfInstallment'] = $numberOfInstallment;
		$data['outstanding'] = $outstanding;
		$data['interestObligations'] = $interestObligations;
		$data['installment'] = $installment;
		$data['arrearsInInstallments'] = $arrearsInInstallments;
		$data['totalPayment'] = $totalPayment;
		$data['interestArrears'] = $interestArrears;
		$data['paymentOfFines'] = $paymentOfFines;
		$data['termsLoansTimes'] = $termsLoansTimes;
		$data['accountCreateDate'] = $accountCreateDate;
		$data['disbursementDate'] = $disbursementDate;
		$data['nextPaymentDate'] = $nextPaymentDate;
		$data['dueDate'] = $dueDate;
		$pdf 	= $this->_getParam('pdf');

		if ($pdf == 'PDF')
		{

			$param = array();
			$outputHTML = '
				<tr>
					<td>
						<table cellpadding="3" cellspacing="0" border="0" width="600px">
							<tr><th colspan="3">Loan Account Detail</th></tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Account Number</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$acct_no.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Currency	</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$ccy_id.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Product Type	</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent"> LOAN </td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Status</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$status.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Plafond</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">IDR '.$plafond.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Number of Installments (Months)</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$numberOfInstallment.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Outstanding</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">IDR '.$outstanding.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Interest Obligations	</td>
								<td class="tbl-evencontent" width="10px">&nbsp;</td>
								<td class="tbl-evencontent">IDR '.$interestObligations.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Installment</td>
								<td class="tbl-evencontent" width="10px">&nbsp;</td>
								<td class="tbl-evencontent">IDR '.$installment.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Total Payment</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">IDR '.$totalPayment.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Interest Arrears</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">IDR '.$interestArrears.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Arrears in Installments</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$arrearsInInstallments.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Payment of Fines</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$paymentOfFines.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Term Loans Time (Months)</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$termsLoansTimes.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Created Date</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$accountCreateDate.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Due Date</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$dueDate.'</td>
							</tr>
							<tr>
								<td class="tbl-evencontent" width="200px">Next Payment Date</td>
								<td class="tbl-evencontent" width="10px">:</td>
								<td class="tbl-evencontent">'.$nextPaymentDate.'</td>
							</tr>
							
						</table>
					</td>
				</tr>
			';
			
// 			echo 'here';die;
// 			$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/detail.phtml')."</td></tr>";
// 			print_r($outputHTML);die;
// 			$this->_helper->download->pdf(null,null,null,'Online Form',$outputHTML);
			$this->_helper->download->pdf('Loan Account Detail',null,null,'LoanAccountDetail',$outputHTML);		}
			if($this->_request->getParam('print') == 1){
			$arr = $data;
			
				
			//            $data = $arr;//$this->_dbObj->fetchAll($select);
			$this->_forward('printtrxloan', 'index', 'widget', array('param' => $data, 'data_caption' => 'Loan Inquiry'));
		}
// 		print_r($Account->getCoreAccountDetail());die;
		
		
	}
	
	
}
