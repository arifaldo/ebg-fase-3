<?php

class Changemanagement_Model_Tempuserbackend extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'USB';
	
	public function approveNew($actor = null) 
	{
		//select from master table TEMP_BUSER
		$user = $this->dbObj->select()
						   ->from('TEMP_BUSER')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()->fetch(Zend_Db::FETCH_ASSOC);
						  
		if(!count($user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }
        
	
		//insert to master table M_USER
		$insertArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['CREATED']     = new Zend_Db_Expr('now()');
		$insertArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$insertArr['CREATEDBY']   = $actor;
		$insertArr['BUSER_ISLOGIN']   = 0;
		
		$userins = $this->dbObj->insert('M_BUSER',$insertArr);
		if(!(boolean)$userins) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}	
		
		$deleteChanges = $this->deleteNew();
		if(!$deleteChanges) return false;
			
		return true;
	}
	
	
	
	public function approveDeactivate($actor = null){}
	public function approveDelete($actor = null){}
	public function deleteDeactivate(){}
	public function deleteDelete(){}
	
	
	public function approveEdit($actor = null) 
	{
		//query from TEMP_BUSER
		$user = $this->dbObj->select()
						  	->from('TEMP_BUSER')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }

		
        //query from M_BUSER
		$m_user = $this->dbObj->select()
						  	  ->from('M_BUSER')
						  	  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  	  ->query()->fetch(Zend_Db::FETCH_ASSOC);
						  	  
		if(!count($m_user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }								  	  
		
		
		//update record
		$updateArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>'','USER_ID'=>''));
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('USER_ID = ?'=>$user['USER_ID']);
		
		$userupdate = $this->dbObj->update('M_BUSER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges) return false;
		
		return true;
	}
	
    public function approveActivate($actor = null) 
    {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_BUSER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()->fetch(Zend_Db::FETCH_ASSOC);
						  
		if(!count($user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }

		$updateArr = array('USER_STATUS' => 'A');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		
		$whereArr  = array('USER_ID = ?'=>(string)$user['USER_ID']);
		
		$userupdate = $this->dbObj->update('M_BUSER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
		$deleteChanges  = $this->deleteActivate();
		if(!$deleteChanges) return false;
		
		return true;
	}
	
    public function deleteEdit() 
    {
        //delete from TEMP_BUSER
		$userdelete = $this->dbObj->delete('TEMP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}
	
    public function deleteNew() 
    {
        //delete from TEMP_BUSER
		$userdelete = $this->dbObj->delete('TEMP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}
	
    public function deleteActivate() 
    {
        //delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}
	
	
}


