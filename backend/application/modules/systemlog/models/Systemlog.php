<?php
class systemlog_Model_Systemlog
{
	protected $_db;

	protected $_masterglobalstatus;

	// constructor
	public function __construct()
	{
		$config = Zend_Registry::get('config');
		$this->_masterglobalstatus = $config['master']['globalstatus'];
		$this->_beneftype = $config['account']['beneftype'];
		$this->_chargesto = $config['charges']['to'];
		$this->_providertype = $config['provider']['type'];
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getFPrivilege()
	{
		$data = $this->_db->select()->distinct()
			->from(array('A' => 'M_FPRIVILEGE'), array('FPRIVI_ID', 'FPRIVI_DESC'))
			->order('FPRIVI_DESC ASC')
			->query()->fetchAll();
		return $data;
	}

	public function getTransactionLog($fParam, $sortBy, $sortDir, $filter)
	{

		$today = date("Ymd");
		$nametext = $today . '.txt';
		$path_file = LIBRARY_PATH . '/data/logs/';

		// echo $path_file.$nametext;
		// $myfile =  htmlentities(file_get_contents($path_file . $nametext));
		$myfile =  file($path_file . $nametext);

		$maxLine = 1000;

		$saveLog = '';

		$count = 1;
		foreach (array_reverse($myfile) as $line) {
			$saveLog .= htmlentities($line);

			if ($count == $maxLine) break;
			$count++;
		}

		$myfile = $saveLog;

		// preg_match('/rhs:\s*"([^"]+)"/', $myfile, $m);
		// print_r($myfile);die();

		$noBackSlash = str_replace(array('\r', '\n'), ' ', $myfile);

		$jsonFix = str_replace('"{', '{', $noBackSlash);

		$jsonFix = str_replace('\\/', '/', $jsonFix);
		$jsonFix = str_replace('\\"', '"', $jsonFix);
		$jsonFix = str_replace('"\\', '"', $jsonFix);
		$jsonFix = str_replace('}"', '}', $jsonFix);

		//xml tag fixing
		$jsonFix = str_replace('<?xml version="1.0" encoding="UTF-8"?>', "<?xml version='1.0' encoding='UTF-8'?>", $jsonFix);
		$jsonFix = str_replace('="', "='", $jsonFix);
		$jsonFix = str_replace('" x', "' x", $jsonFix);
		$jsonFix = str_replace('/"', "/'", $jsonFix);

		$jsonFix = str_replace('">', "'>", $jsonFix);
		$jsonFix = str_replace('"/>', "'/>", $jsonFix);

		$jsonFix = trim($jsonFix);
		$jsonFix = trim($jsonFix, '\,');

		$jsonFix = '[' . $jsonFix . ']';
		// header('Content-Type: text/plain');
		// $jsonFix = htmlentities($jsonFix);

		// var_dump($jsonFix);die;
		// $jsonFixnew = trim(preg_replace('/\s\s+/', ' ', $jsonFix));
		// $jsonFix=str_replace("\r\n","",$jsonFix);
		// $jsonFix = trim(preg_replace('/\s\s+/', ' ', $jsonFix));
		// var_dump($jsonFix);
		// die();

		// $jsonFix = $this->remove_utf8_bom($jsonFix);
		// $jsonFix = $this->prepareCharset($jsonFix);
		// $jsonFix = str_replace("\xEF\xBB\xBF",'',$jsonFix); 
		// $jsonFix = preg_replace('/\x{FEFF}/u', '', $jsonFix);

		// for ($i = 0; $i <= 31; ++$i) { 
		//     $jsonFix = str_replace(chr($i), "", $jsonFix); 
		// }
		// $jsonFix = str_replace(chr(127), "", $jsonFix);

		// if (0 === strpos(bin2hex($jsonFix), 'efbbbf')) {
		//    $jsonFix = substr($jsonFix, 3);
		// }

		// echo $jsonFix;die();

		// $jsonString = json_encode($jsonFix);

		// $jsonFix = stripslashes($jsonFix); 

		// $jsondata = json_decode($jsonFix, true);

		// echo "<pre>";
		// var_dump($jsondata);
		// echo json_last_error();

		$jsonFix = str_replace('&quot;', '"', $jsonFix);

		$jsonFix = str_replace(['\\"'], "'", $jsonFix);
		$jsonFix = str_replace(["\\\/"], "/", $jsonFix);

		$jsondata = Json::decode($jsonFix, TRUE);

		// 	$select2 = $this->_db->select()->from(
		// 											array('T_INTERFACE_LOG'),
		// 											array(															
		// 													'FUNCTION_NAME',
		// 													'STRING',
		// 													'UID',
		// 													'LOG_DATE',
		// 													'ACTION',
		// 												)
		// 										);

		// if($filter == null || $filter == true)
		// {
		// 	if(!empty($fParam['datefrom']) && empty($fParam['dateto']))
		// 			$select2->where("DATE(LOG_DATE) >= ".$this->_db->quote($fParam['datefrom']));

		// 	if(empty($fParam['datefrom']) && !empty($fParam['dateto']))
		// 			$select2->where("DATE(LOG_DATE) <= ".$this->_db->quote($fParam['dateto']));

		// #	if(!empty($fParam['datefrom']) && !empty($fParam['dateto']))
		// #			$select2->where("DATE(LOG_DATE) BETWEEN ".$this->_db->quote($fParam['datefrom'])." and ".$this->_db->quote($fParam['dateto']));
		// }

		// if($filter == TRUE)
		// {
		// 	if($fParam['function'])
		// 	{
		// 		$select2->where("FUNCTION_NAME LIKE ".$this->_db->quote($fParam['function']));
		// 	}

		// 	if($fParam['string'])
		// 	{
		// 	   $select2->where("STRING LIKE ".$this->_db->quote('%'.$fParam['string'].'%'));
		// 	}

		// 	if($fParam['uid'])
		// 	{
		// 	   $select2->where("UID LIKE ".$this->_db->quote('%'.$fParam['uid'].'%'));
		// 	}
		// }

		// $select2->order($sortBy.' '.$sortDir);
		// $select2->limit(20);
		// // echo $select2;die;
		// return $this->_db->fetchAll($select2);

		return $jsondata;
	}

	public function getEmailLog($fParam, $sortBy, $sortDir, $filter)
	{

		$select2 = $this->_db->select()->from(
			array('T_EMAIL_LOG'),
			array(
				'EMAIL_RECEIVER',
				'EMAIL_CONTENT',
				'EMAIL_DATE',
				'EMAIL_STATUS',
			)
		);

		if ($filter == null || $filter == true) {
			if (!empty($fParam['datefrom']) && empty($fParam['dateto']))
				$select2->where("DATE(EMAIL_DATE) >= " . $this->_db->quote($fParam['datefrom']));

			if (empty($fParam['datefrom']) && !empty($fParam['dateto']))
				$select2->where("DATE(EMAIL_DATE) <= " . $this->_db->quote($fParam['dateto']));

			if (!empty($fParam['datefrom']) && !empty($fParam['dateto']))
				$select2->where("DATE(EMAIL_DATE) BETWEEN " . $this->_db->quote($fParam['datefrom']) . " and " . $this->_db->quote($fParam['dateto']));
		}

		if ($filter == TRUE) {
			/*if($fParam['function'])
			{
				$select2->where("FUNCTION_NAME LIKE ".$this->_db->quote($fParam['function']));
			}*/

			if ($fParam['emailreceiver']) {
				$select2->where("EMAIL_RECEIVER LIKE " . $this->_db->quote('%' . $fParam['emailreceiver'] . '%'));
			}
		}

		$select2->order($sortBy . ' ' . $sortDir);
		//echo $select2;
		return $this->_db->fetchAll($select2);
	}
}

//for json decode, clean string to be able to decode
abstract class Json
{
	public static function getLastError($asString = FALSE)
	{
		$lastError = \json_last_error();

		if (!$asString) return $lastError;

		// Define the errors.
		$constants = \get_defined_constants(TRUE);
		$errorStrings = array();

		foreach ($constants["json"] as $name => $value)
			if (!strncmp($name, "JSON_ERROR_", 11))
				$errorStrings[$value] = $name;

		return isset($errorStrings[$lastError]) ? $errorStrings[$lastError] : FALSE;
	}

	public static function getLastErrorMessage()
	{
		return \json_last_error_msg();
	}

	public static function clean($jsonString)
	{
		if (!is_string($jsonString) || !$jsonString) return '';

		// Remove unsupported characters
		// Check http://www.php.net/chr for details
		for ($i = 0; $i <= 31; ++$i)
			$jsonString = str_replace(chr($i), "", $jsonString);

		$jsonString = str_replace(chr(127), "", $jsonString);

		// Remove the BOM (Byte Order Mark)
		// It's the most common that some file begins with 'efbbbf' to mark the beginning of the file. (binary level)
		// Here we detect it and we remove it, basically it's the first 3 characters.
		if (0 === strpos(bin2hex($jsonString), 'efbbbf')) $jsonString = substr($jsonString, 3);

		return $jsonString;
	}

	public static function encode($value, $options = 0, $depth = 512)
	{
		return \json_encode($value, $options, $depth);
	}

	public static function decode($jsonString, $asArray = TRUE, $depth = 512, $options = JSON_BIGINT_AS_STRING)
	{
		if (!is_string($jsonString) || !$jsonString) return NULL;

		$result = \json_decode($jsonString, $asArray, $depth, $options);

		if ($result === NULL)
			switch (self::getLastError()) {
				case JSON_ERROR_SYNTAX:
					// Try to clean json string if syntax error occured
					$jsonString = self::clean($jsonString);
					$result = \json_decode($jsonString, $asArray, $depth, $options);
					break;

				default:
					// Unsupported error
			}

		return $result;
	}
}
