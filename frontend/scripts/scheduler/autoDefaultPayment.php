<?
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/Settings.php';
	
	$actionid = 'scheduler';
	$moduleid = 'reset payment';
	
	$set = new Settings();
	$effect = $set->setSetting('payment_counter', '0');
	// $a 		= $set->setSetting('SKN_counter'	, '100');
	// $b 		= $set->setSetting('RTGS1_counter'	, '0');
	// $c 		= $set->setSetting('RTGS2_counter'	, '0');
	// $d 		= $set->setSetting('RTGS2_seqno'	, '0');
	
	if($effect){
		$string  = "Resetting Payment Counter";
		Application_Helper_General::cronLog(basename(__FILE__),$string,1);
	}else{
		$string  = "Failed Resetting Payment Counter";
		Application_Helper_General::cronLog(basename(__FILE__),$string,0);
	}
?>