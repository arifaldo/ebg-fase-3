<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class eform_BgdetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];
        $numb = $this->_getParam('bgnumb');

        if (!empty($numb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();
            //$datas = $this->_request->getParams();
            // var_dump($datas);


            if (!empty($bgdata)) {

                $data = $bgdata['0'];

                //$data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }







                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);


                $arrStatus = array(
                    '1' => 'Waiting for review',
                    '2' => 'Waiting for approve',
                    '3' => 'Waiting to release',
                    '4' => 'Waiting for bank approval',
                    '5' => 'Issued',
                    '6' => 'Expired',
                    '7' => 'Canceled',
                    '8' => 'Claimed by applicant',
                    '9' => 'Claimed by recipient',
                    '10' => 'Request Repair',
                    '11' => 'Reject'
                );

                $this->view->arrStatus = $arrStatus;

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


                $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                $AccArr = $CustomerUser->getAccounts($param);


                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                if ($data['BG_STATUS'] == '7' || $data['BG_STATUS'] == '11' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                a.USER_LOGIN,
                b.`USER_FULLNAME` AS u_name,
                c.`BUSER_NAME` AS b_name,
                a.DATE_TIME,
                a.BG_REASON,
                a.HISTORY_STATUS,
                a.BG_REG_NUMBER
                
                
              FROM
                T_BANK_GUARANTEE_HISTORY AS a
                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS IN (10) GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    if ($data['BG_STATUS'] == '10') {
                        $result =  $this->_db->fetchAll($selectQuery);
                    } else {
                        $result = array();
                    }
                    if (!empty($result)) {
                        $this->view->reqrepair = true;
                        $data['REASON'] = $result['0']['BG_REASON'];
                        $this->view->username = ' (By ' . $result['0']['u_name'] . '' . $result['0']['b_name'] . ')';
                    }
                    //var_dump($result);die;


                    $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                $this->view->GT_DOC_DESC = $arrbgdoc[$data['GT_DOC_TYPE']];

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

                $this->view->BG_PUBLISH_DESC = $arrbgpublish[$data['BG_PUBLISH']];

                //$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];

                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];

                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];

                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];

                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->SERVICE = $data['SERVICE'];

                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];


                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDataEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);

                $back = $this->_getParam('back');
                if ($back) {
                    $this->_redirect('/eform');
                }
                //echo '<pre>';
                //var_dump($back);die;

                $BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
                //print_r($edit);die;
                if ($BG_APPROVE_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/bg/';
                    //var_dump($attahmentDestination.$data['BG_APPROVE_DOC']);die;
                    $this->_helper->download->file($data['BG_APPROVE_DOC'], $attahmentDestination . $data['BG_APPROVE_DOC']);
                }

                $download2 = $this->_getParam('pdf');

                if ($download2) {
                    //echo '<pre>';
                    //var_dump($bgdata);die;
                    $tempdir = APPLICATION_PATH . "/../public/QRImages/";

                    $teks_qrcode    = 'http://' . $_SERVER['HTTP_HOST'] . "/bgchecking/detail/index/bgnumb/" . $bgdata['0']['BG_NUMBER'];
                    $namafile       = $bgdata['0']['BG_NUMBER'] . ".png";
                    $quality        = "H";
                    $ukuran         = 5;
                    $padding        = 1;
                    $qrImage        = $tempdir . $namafile;

                    QRCode::png($teks_qrcode, $tempdir . $namafile, $quality, $ukuran, $padding);
                    //die;
                    if ($bgdata['0']['BG_LANGUAGE'] == '2') {
                        $outputHTML = "<tr><td>" . $this->view->render('/bgdetail/indexpdfen.phtml') . "</td></tr>";
                    } else {
                        $outputHTML = "<tr><td>" . $this->view->render('/bgdetail/indexpdf.phtml') . "</td></tr>";
                    }
                    //var_dump(APPLICATION_PATH);die;

                    $path  = APPLICATION_PATH . '/../public/QRImages/';
                    $files = scandir($path);
                    $files = array_diff(scandir($path), array('.', '..'));
                    $dh = opendir($path);
                    while ($file = readdir($dh)) {
                        if ($file == $namafile) {
                            $imageData = base64_encode(file_get_contents($path . '' . $namafile));
                            $image = $path . '' . $data['IMAGE'];
                            $src = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;
                        }
                    }

                    $path  = APPLICATION_PATH . '/../public/images/image001.jpg';
                    $imageData = base64_encode(file_get_contents($path));
                    $image = $path;
                    $srcbank = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;
                    $terbilangs = $this->Terbilang($data['BG_AMOUNT']);

                    $terbilang_en = $this->Terbilangen($data['BG_AMOUNT']);
                    //echo '<pre>';
                    //var_dump($data);
                    //var_dump($terbilangs);
                    //die;
                    //$path = 'myfolder/myimage.png';
                    //$type = pathinfo($path, PATHINFO_EXTENSION);
                    //$data = file_get_contents($path);
                    //$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $translate = array(
                        '[[qrcode]]'             => $src,
                        '[[bankimage]]'            => $srcbank,
                        '[[bg_numb]]'            => $data['BG_NUMBER'],
                        '[[date_start]]'        => $data['TIME_PERIOD_START'],
                        '[[city]]'        => $data['RECIPIENT_CITY'],
                        '[[benef_name]]'        => $data['RECIPIENT_NAME'],
                        '[[benef_add]]'        => $data['RECIPIENT_ADDRES'],
                        '[[benef_city]]'        => $data['RECIPIENT_CITY'],
                        '[[app_name]]'        => $selectcomp[0]['CUST_NAME'],
                        '[[app_add]]'        => $selectcomp[0]['CUST_ADDRESS'],
                        '[[app_city]]'        => $selectcomp[0]['CUST_CITY'],
                        '[[amount]]'        => number_format($data['BG_AMOUNT'], 2, ',', '.'),
                        '[[terbilang]]'        => $terbilangs,
                        '[[terbilang_en]]'        => $terbilang_en,
                        '[[guarantee_letter]]'        => $data['GUARANTEE_TRANSACTION'],
                        '[[date_start]]'        => $this->indodate($data['TIME_PERIOD_START']),
                        '[[date_end]]'        => $this->indodate($data['TIME_PERIOD_END']),


                    );

                    $outputHTMLnew = strtr($outputHTML, $translate);

                    //echo $outputHTMLnew;die;
                    $setting = new Settings();
                    $master_bank_app_name = $setting->getSetting('master_bank_app_name');
                    $master_bank_name = $setting->getSetting('master_bank_name');
                    try {
                        if ($bgdata['0']['BG_LANGUAGE'] != '3') {
                            $this->_helper->download->newPdfblank(null, null, null, $master_bank_app_name . ' - ' . $master_bank_name, $outputHTMLnew);
                        } else {
                            $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                            //die('here');
                            $this->_helper->download->file($bgdata['0']['BG_NUMBER'] . '.pdf', $attahmentDestination . 'Sample_BG.pdf');
                        }
                        //die;
                    } catch (Exception $e) {
                        var_dump($e);
                        die;
                    }
                    //die;
                }
                //print_r($edit);die;
            }

            $download = $this->_getParam('download');
            if ($download == 1) {

                $getbgdata = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                if (!$getbgdata) {
                    $getbgdata = $this->_db->select()
                        ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $numb)
                        ->query()->fetchAll();
                }

                $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                $this->_helper->download->file($getbgdata[0]['FILE'], $attahmentDestination . $getbgdata[0]['FILE']);
            }
        }
    }
    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function Terbilangen($nilai)
    {
        $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 20 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 100) {
            return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilangen($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilangen($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }
}
