<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';

class user_ForcelogoutController extends Application_Main
{
	

	public function indexAction() 
	{
		//$user_id = $this->_getParam('user_id');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	     $password = $sessionNamespace->token; 
	     $this->view->token = $sessionNamespace->token;  


		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($this->_getParam('user_id'));
		$user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

		$this->_helper->layout()->setLayout('newlayout');
	   	$select =	$this->_db->select()
									->from(	'M_USER',array('USER_FULLNAME'))
									->where('USER_ID = ? ', $user_id )
									->where('CUST_ID = ? ', $this->_custIdLogin );		
									
		$arr = $this->_db->fetchRow($select);
		$this->view->fullname = $arr["USER_FULLNAME"];
		$this->view->user_id = $user_id;
//		Zend_Debug::Dump($arr);	
		
		if($this->view->hasPrivilege('UFLG'))
	  	{
			//requestUnlockResetPassword("1");
			$CustomerUser = new CustomerUser($this->_custIdLogin,$user_id );
			$CustomerUser->forceLogout();
			Application_Helper_General::writeLog('UFLG','Force to Logout Cust Id ( '.$this->_custIdLogin.' ) ,User Id ( '.$user_id.' )');
			$this->_redirect('/notification/success/index');
		}
		
		
		
	}
}
