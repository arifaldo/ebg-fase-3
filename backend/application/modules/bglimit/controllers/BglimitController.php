<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class bglimit_BglimitController extends customer_Model_Customer 
{

    public function indexAction() 
    {
       //pengaturan url untuk button back
       $this->_helper->layout()->setLayout('newlayout');
	   //$this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$this->_getParam('cust_id'));    
	   $this->setbackURL('/customer/view/index/cust_id/'.$this->_getParam('cust_id'));    
	    
       $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token; 


       $AESMYSQL = new Crypt_AESMYSQL();
       $cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password)); 
       $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

       $cust_view = 1; 
       $error_remark = null; 
       $flag = 0;
    
      
       //data customer
       $resultdata = $this->getCustomer($cust_id);  
	   //echo '<pre>';
       // print_r($resultdata);die;
       
       if($cust_id)
       {
			$tempCustomerId = $this->getTempCustomerId($cust_id);
			if($tempCustomerId) $error_remark = 'invalid format'; 
			else $flag = 1; 
       }
       else
       {
          $error_remark = 'invalid format';  
       }


		$custdata = $this->_db->select()->distinct()
					->from(array('M_CUSTOMER'),array('*'))
					->where("CUST_STATUS != '3'")
					->where("CUST_ID = ?",$cust_id)
					->order('CUST_ID ASC')
				 	-> query() ->fetchAll();
					$comp = array();
		
	
		
    //convert limit idr & usd agar bisa masuk ke database
    $limitidr = $this->_getParam('cust_bg_limit');
    $limitidr = Application_Helper_General::convertDisplayMoney($limitidr);
    $this->view->cust_bg_limit = $limitidr;

   

      if($this->_request->isPost())
      {

       	

         
		 $customer_filter = array();

            $filters = array('cust_id'           => array('StripTags','StringTrim','StringToUpper'),			                
			                 'cust_bg_limit'      => array('StripTags','StringTrim')
			                );

	     $validators =  array(
	     					'cust_id'		=> array(),
	     			
                            'cust_bg_limit'           => array('allowEmpty' => true,'messages' => array()),				
                    
                                                                                                   
							);

            $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
            
            if($zf_filter_input->isValid() )
            {
				
				
	           $info = 'Customer ID = '.$cust_id;
               $cust_data = $this->_custData;
			   $cust_data = $custdata[0];
	  	       foreach($validators as $key=>$value)
	  	       {
	  	          if($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;
	  	       }
			//	echo '<pre>';
				//var_dump($cust_data);die;
	  	       
				
              try 
              {
		          $this->_db->beginTransaction();
		          
		         
				     
		  	      $change_id = $this->suggestionWaitingApproval('Customer BG Limit',$info,$this->_changeType['code']['edit'],null,'M_CUSTOMER','TEMP_CUSTOMER',$cust_id,$zf_filter_input->cust_name,$cust_id);
		  	      $cust_data['CUST_ID'] = $cust_id;
				  $cust_data['CUST_BG_LIMIT'] = str_replace(',','',$zf_filter_input->cust_bg_limit);
					//echo '<pre>';
				//var_dump($cust_data);die;

		  	      $this->insertTempCustomer($change_id,$cust_data);


		  	      //log CRUD
			      Application_Helper_General::writeLog('CCUD','Customer has been updated (edit), Cust ID : '.$cust_id. ' Cust Name : '.$custdata[0]['CUST_NAME'].' Change id : '.$change_id);

		          $this->_db->commit();

		          $this->_redirect('/notification/submited/index');
	  	      }
		      catch(Exception $e)
		      {
		      	print_r($e);die;
		         $this->_db->rollBack();
		  	     $error_remark = $this->language->_('Database Error');
	 	      }

		     if(isset($error_remark))
		     { 
		        $msg = $error_remark; 
		        $class = 'F'; 
		     }
             else
             {
		       $msg = 'Success';
		       $class = 'S';
		     }
		
		     $this->_helper->getHelper('FlashMessenger')->addMessage($class);
		     $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
		     // $this->_redirect($this->_backURL);
		     
	  }//END IF is VALID
	  else
	  {
	  	$this->view->cust_id   = $cust_id;
	  	$this->view->cust_name = ($zf_filter_input->isValid('cust_name'))? $zf_filter_input->cust_name : $this->_getParam('cust_name');
	  	
        $this->view->cust_bg_limit  = ($zf_filter_input->isValid('cust_bg_limit'))? $zf_filter_input->cust_bg_limit : $this->_getParam('cust_bg_limit');
        
        
		$error = $zf_filter_input->getMessages();

		//format error utk ditampilkan di view html 
        $errorArray = null;
        foreach($error as $keyRoot => $rowError)
        {
           foreach($rowError as $errorString)
           {
              $errorArray[$keyRoot] = $errorString;
           }
        }
        
       
        $this->view->customer_msg  = $errorArray;

	  }
	
	  
    }// END if($this->_request->isPost())
    else 
    {
       

       $this->view->cust_id        = strtoupper($resultdata['CUST_ID']);
	   $this->view->cust_bg_limit = Application_Helper_General::convertDisplayMoney($resultdata['CUST_BG_LIMIT']);
       $this->view->cust_name      = $resultdata['CUST_NAME'];
      
       
       
       
    }

     $this->view->modulename    = $this->_request->getModuleName();
     $this->view->lengthCustId  = $this->_custIdLength;
     $this->view->approve_type  =  $this->_masterhasStatus;
    
    
    
    //insert log
	try
	{
	   $this->_db->beginTransaction();
	  
	   Application_Helper_General::writeLog('CCUD','Edit Customer ['.$this->view->cust_id.']');
	   
	   $this->_db->commit();
	}
    catch(Exception $e)
    {
 	     $this->_db->rollBack();
	     Application_Log_GeneralLog::technicalLog($e);
	}
	
	
	
  }
  
  
}




