<?php

	$dir = realpath(dirname(__FILE__));

	$global = file_get_contents($dir.'/global.js');
	$jqueryUiCore = file_get_contents($dir.'/jquery/ui/jquery.ui.core.js');
	$jqueryUiWidget = file_get_contents($dir.'/jquery/ui/jquery.ui.widget.js');
	$jqueryUiAutocomplete = file_get_contents($dir.'/jquery/ui/jquery.ui.autocomplete.js');
	$jqueryUiDatepicker = file_get_contents($dir.'/jquery/ui/jquery.ui.datepicker.js');
	$custom = file_get_contents($dir.'/sgo/custom.js');
	$ddsmoothmenu = file_get_contents($dir.'/ddsmoothmenu.js');

	//added by DW (14-10-2019)
	// $jquery3 				= file_get_contents($dir.'/jquery-3.3.1.min.js');
	$jqueryMin 				= file_get_contents($dir.'/jquery.min.js');
	// $kendo 					= file_get_contents($dir.'/kendo.all.min.js');
	$jqueryNiceScroll 		= file_get_contents($dir.'/jquery.nicescroll.min.js');
	$popper 				= file_get_contents($dir.'/popper.min.js');
	$bootstrap 				= file_get_contents($dir.'/bootstrap.min.js');
	$bootstrapSelect 		= file_get_contents($dir.'/bootstrap-select.min.js');
	$bootstrapDatePicker 	= file_get_contents($dir.'/bootstrap-datepicker.js');
	$jqueryForm 			= file_get_contents($dir.'/jquery.form.js');
	$gijgo 					= file_get_contents($dir.'/gijgo.min.js');
	$fixTable 				= file_get_contents($dir.'/jQuery.fixTableHeader.js');
	$muriHammer 			= file_get_contents($dir.'/muri/hammer.min.js');
	$muriWebAnimations 		= file_get_contents($dir.'/muri/web-animations.min.js');
	$muriJs					= file_get_contents($dir.'/muri/muuri.min.js');
	$croppie 				= file_get_contents($dir.'/croppie.js');
	$webcam 				= file_get_contents($dir.'/webcam.min.js');
	//---------------------------------------------------------------------------------

	//added by DW (14-10-2019)
	// echo $jquery3."\n\n\n\n";
	echo $jqueryMin."\n\n\n\n";
	echo $popper."\n\n\n\n";
	echo $bootstrap."\n\n\n\n";
	echo $bootstrapSelect."\n\n\n\n";
	echo $bootstrapDatePicker."\n\n\n\n";
	//-------------------------

	echo $ddsmoothmenu."\n\n\n\n";
	echo $global."\n\n\n\n";
	// echo $jqueryUiCore."\n\n\n\n";
	echo $jqueryUiWidget."\n\n\n\n";
	echo $jqueryUiAutocomplete."\n\n\n\n";
	echo $jqueryUiDatepicker."\n\n\n\n";
	echo $custom."\n\n\n\n";

	//added by DW (14-10-2019)
	// echo $kendo."\n\n\n\n";
	echo $jqueryNiceScroll."\n\n\n\n";
	echo $jqueryForm."\n\n\n\n";
	echo $gijgo."\n\n\n\n";
	echo $fixTable."\n\n\n\n";
	echo $muriHammer."\n\n\n\n";
	echo $muriWebAnimations."\n\n\n\n";
	echo $muriJs."\n\n\n\n";
	echo $croppie."\n\n\n\n";
	echo $webcam."\n\n\n\n";
	//-------------------------
?>

