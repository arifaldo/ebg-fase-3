<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class bgreport_DetailController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $bg = $this->_getParam('bg');

    $sessToken  = new Zend_Session_Namespace('Tokenenc');
    $password   = $sessToken->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $decryption = urldecode($bg);
    $decryption_bg = $AESMYSQL->decrypt($decryption, $password);

    $conf = Zend_Registry::get('config');

    $select = $this->_db->select()
      ->from(
        array('TBG' => 'T_BANK_GUARANTEE'),
        array('*')
      )
      ->joinLeft(
        array('MB' => 'M_BRANCH'),
        'MB.BRANCH_CODE = TBG.BG_BRANCH',
        array('BRANCH_NAME')
      )
      ->joinLeft(
        array('MC' => 'M_CUSTOMER'),
        'MC.CUST_ID = TBG.CUST_ID',
        array(
          'CUST_ID',
          'CUST_NAME',
          'CUST_NPWP',
          'CUST_ADDRESS',
          'CUST_CITY',
          'CUST_FAX',
          'CUST_CONTACT',
          'CUST_PHONE',
        )
      )
      ->joinLeft(
        array('MCST' => 'M_CUSTOMER'),
        'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
        array(
          "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
        )
      )
      ->joinLeft(
        array('INSURANCE' => 'M_CUSTOMER'),
        'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
        array(
          "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
        )
      )
      ->joinLeft(
        array('MCL' => 'M_CITYLIST'),
        'MCL.CITY_CODE = MC.CUST_CITY',
        array('CITY_NAME')
      )
      ->joinLeft(
        array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
        'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
        array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
      )
      ->joinLeft(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        'TBGS.BG_NUMBER = TBG.BG_REG_NUMBER',
        [
          "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
        ]
      )
      ->where('TBG.BG_REG_NUMBER = ?', $decryption_bg);
    //->where('TBG.CUST_ID = ?', $this->_custIdLogin);
    //die();
    $data = $this->_db->fetchRow($select);

    switch ($data["CHANGE_TYPE"]) {
      case '0':
        $this->view->suggestion_type = "New";
        break;
      case '1':
        $this->view->suggestion_type = "Amendment Changes";
        break;
      case '2':
        $this->view->suggestion_type = "Amendment Draft";
        break;
    }

    //echo '<pre>';print_r($data);
    $conf = Zend_Registry::get('config');
    // BG TYPE
    $bgType         = $conf["bg"]["type"]["desc"];
    $bgCode         = $conf["bg"]["type"]["code"];

    $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

    $this->view->arrbgType = $arrbgType;

    //BG Counter Guarantee Type
    $bgcgType         = $conf["bgcg"]["type"]["desc"];
    $bgcgCode         = $conf["bgcg"]["type"]["code"];

    $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

    $this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

      $bgdatasplit = $this->_db->select()
        ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
        ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
        ->query()->fetchAll();

      foreach ($bgdatasplit as $key => $value) {
        $temp_save = $this->_db->select()
          ->from("M_CUSTOMER_ACCT")
          ->where("ACCT_NO = ?", $value["ACCT"])
          ->query()->fetchAll();

        $bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
        $bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
      }

      $this->view->fullmember = $bgdatasplit;
    }


    $sqlbgdetail = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
      //->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"]);
    //  die('a');
    $bgdatadetail = $sqlbgdetail
      ->query()->fetchAll();


    if (!empty($bgdatadetail)) {
      foreach ($bgdatadetail as $key => $value) {
        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
          if ($value['PS_FIELDNAME'] == 'Insurance Name') {
            $this->view->insuranceName =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principle Agreement Number') {
            $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principle Agreement Granted Date') {
            $this->view->PrincipleAgreementGrantedDate =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Counter Guarantee Number') {
            $this->view->CounterGuaranteeNumber =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Counter Guarantee Granted Date') {
            $this->view->CounterGuaranteeGrantedDate =   $value['PS_FIELDVALUE'];
          }


          if ($value['PS_FIELDNAME'] == 'Amount') {
            $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
            $this->view->paDateStart =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
            $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
          }
        } else {

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
            $this->view->owner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
            $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
            $this->view->owner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
            $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
            $this->view->owner3 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
            $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
          }
        }
      }
    }

    $this->view->BG_REG_NUMBER = $data["BG_REG_NUMBER"];

    $checkCounterGuaranteeFile = $this->_db->select()
      ->from(["A" => "T_BANK_GUARANTEE_DETAIL"], ["PS_FIELDVALUE"])
      ->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
      ->where("PS_FIELDNAME = ?", "Counter Guarantee Document")
      ->query()->fetchAll();

    $this->view->checkCounterGuaranteeFile = count($checkCounterGuaranteeFile);

    $principleData = [];
    // if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
    foreach ($bgdatadetail as $key => $value) {
      $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
    }

    $this->view->principleData = $principleData;
    // }

    if (!empty($principleData["SLIK OJK Document"])) {
      $temp_slikojk = explode("_", $principleData["SLIK OJK Document"]);
      $new_temp_name = $temp_slikojk[0] . "_" . $temp_slikojk[1] . "_" . $temp_slikojk[2] . "_" . $temp_slikojk[3] . "_";

      $this->view->slikojk = str_replace($new_temp_name, "", $principleData["SLIK OJK Document"]);
    }

    if (!empty($data["KUASA_DIREKSI_FILE"])) {
      $temp_suratpermohonan = explode("_", $data["KUASA_DIREKSI_FILE"]);
      $new_temp_name = $temp_suratpermohonan[0] . "_" . $temp_suratpermohonan[1] . "_" . $temp_suratpermohonan[2] . "_" . $temp_suratpermohonan[3] . "_";

      $this->view->suratpermohonan = str_replace($new_temp_name, "", $data["KUASA_DIREKSI_FILE"]);
    }

    if (!empty($data["AGREE_FORMAT"])) {
      $temp_agreeformat = explode("_", $data["AGREE_FORMAT"]);
      $new_temp_name = $temp_agreeformat[0] . "_" . $temp_agreeformat[1] . "_" . $temp_agreeformat[2] . "_" . $temp_agreeformat[3] . "_";

      $this->view->agreeformat = str_replace($new_temp_name, "", $data["AGREE_FORMAT"]);
    }

    if (!empty($data["MEMO_LEGAL"])) {
      $temp_memolegal = explode("_", $data["MEMO_LEGAL"]);
      $new_temp_name = $temp_memolegal[0] . "_" . $temp_memolegal[1] . "_" . $temp_memolegal[2] . "_" . $temp_memolegal[3] . "_";

      $this->view->memolegal = str_replace($new_temp_name, "", $data["MEMO_LEGAL"]);
    }

    if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {
      $this->view->isinsurance = true;
    }

    switch ($bgdata[0]["CHANGE_TYPE"]) {
      case '0':
        $this->view->suggestionType = "New";
        break;
      case '1':
        $this->view->suggestionType = "Amendment Changes";
        break;
      case '2':
        $this->view->suggestionType = "Amendment Draft";
        break;
    }

    $config        = Zend_Registry::get('config');
    $BgcomplationType     = $config["bg"]["complation"]["desc"];
    $BgcomplationCode     = $config["bg"]["complation"]["code"];

    $arrStatusSettlement = array_combine(array_values($BgcomplationCode), array_values($BgcomplationType));

    $this->view->arrStatusSettlement = $arrStatusSettlement;

    $provision_insurance = $this->_db->select()
      ->from(["A" => "M_CUST_LINEFACILITY"], ["FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
      ->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
      ->query()->fetchAll();

    if ($data["COUNTER_WARRANTY_TYPE"] == "3") $this->view->provision_insurance = $provision_insurance[0];


    $get_linefacility = $this->_db->select()
      ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
      ->where("CUST_ID = ?", $data["CUST_ID"])
      ->query()->fetchAll();

    $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
    $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

    $this->view->linefacility = $get_linefacility[0];

    $this->view->acct = $data['FEE_CHARGE_TO'];

    $conf = Zend_Registry::get('config');
    $this->view->bankname = $conf['app']['bankname'];

    $CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);
    $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
    $AccArr = $CustomerUser->getAccounts($param);
    //var_dump($AccArr);die;

    if (!empty($AccArr)) {
      $this->view->src_name = $AccArr['0']['ACCT_NAME'];
    }

    $get_cash_collateral = $this->_db->select()
      ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
      ->where("CUST_ID = ?", "GLOBAL")
      ->where("CHARGES_TYPE = ?", "10")
      ->query()->fetchAll();

    $this->view->cash_collateral = $get_cash_collateral[0];

    $bgpublishType     = $conf["bgpublish"]["type"]["desc"];
    $bgpublishCode     = $conf["bgpublish"]["type"]["code"];

    $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

    $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

    if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
      $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
      $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

      $insuranceBranch = $this->_db->select()
        ->from("M_INS_BRANCH")
        ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
        ->query()->fetchAll();

      $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
    }

    $arrBankFormat = array(
      1 => 'Bank Standard',
      2 => 'Special Format (with bank approval)'
    );

    $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
    $this->view->bankFormatNumber = $data['BG_FORMAT'];

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Billingual',
    );

    $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

    $checkOthersAttachment = $this->_db->select()
      ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
      ->where("BG_REG_NUMBER = '" . $data['BG_REG_NUMBER'] . "'")
      ->order('A.INDEX ASC')
      ->query()->fetchAll();

    if (count($checkOthersAttachment) > 0) {
      $this->view->othersAttachment = $checkOthersAttachment;
    }

    // Get data T_BANK_GUARANTEE_HISTORY
    $select = $this->_db->select()
      ->from(
        array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
        array('*')
      )
      ->where('TBGH.BG_REG_NUMBER = ?', $decryption_bg)
      ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
    $dataHistory = $this->_db->fetchAll($select);

    // Get data TEMP_BANK_GUARANTEE_SPLIT
    $select = $this->_db->select()
      ->from(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        array('*')
      )
      ->where('TBGS.BG_REG_NUMBER = ?', $decryption_bg);
    $dataAccSplit = $this->_db->fetchAll($select);

    $config = Zend_Registry::get('config');

    $docTypeCode = $config["bgdoc"]["type"]["code"];
    $docTypeDesc = $config["bgdoc"]["type"]["desc"];
    $docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

    $statusCode = $config["bg"]["status"]["code"];
    $statusDesc = $config["bg"]["status"]["desc"];
    $statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

    $historyStatusCode = $config["history"]["status"]["code"];
    $historyStatusDesc = $config["history"]["status"]["desc"];
    $historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

    $counterTypeCode = $config["bgcg"]["type"]["code"];
    $counterTypeDesc = $config["bgcg"]["type"]["desc"];
    $counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

    //$config    		= Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    // $arrStatus = array(
    //   '7'  => 'Canceled',
    //   '20' => 'On Risk',
    //   '21' => 'Off Risk',
    //   '22' => 'Claimed On Process',
    //   '23' => 'Claimed'
    // );

    $this->view->arrStatus = $arrStatus;



    $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
    $this->view->data               = $data;
    //Zend_Debug::dump($data);
    $this->view->dataHistory        = $dataHistory;
    $this->view->dataAccSplit       = $dataAccSplit;
    $this->view->docTypeArr         = $docTypeArr;
    $this->view->statusArr          = $statusArr;
    $this->view->historyStatusArr   = $historyStatusArr;
    $this->view->counterTypeArr     = $counterTypeArr;

    $download = $this->_getParam('download');
    if ($download == 1) {
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';
      $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
    }
  }

  public function Terbilang($nilai)
  {
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    if ($nilai == 0) {
      return "";
    } elseif ($nilai < 12 & $nilai != 0) {
      return "" . $huruf[$nilai];
    } elseif ($nilai < 20) {
      return $this->Terbilang($nilai - 10) . " Belas ";
    } elseif ($nilai < 100) {
      return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
    } elseif ($nilai < 200) {
      return " Seratus " . $this->Terbilang($nilai - 100);
    } elseif ($nilai < 1000) {
      return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
    } elseif ($nilai < 2000) {
      return " Seribu " . $this->Terbilang($nilai - 1000);
    } elseif ($nilai < 1000000) {
      return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
    } elseif ($nilai < 1000000000) {
      return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
    } elseif ($nilai < 1000000000000) {
      return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
    } elseif ($nilai < 100000000000000) {
      return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
    } elseif ($nilai <= 100000000000000) {
      return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
    }
  }

  public function Terbilangen($nilai)
  {
    $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
    if ($nilai == 0) {
      return "";
    } elseif ($nilai < 20 & $nilai != 0) {
      return "" . $huruf[$nilai];
    } elseif ($nilai < 100) {
      return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
    } elseif ($nilai < 200) {
      return " Seratus " . $this->Terbilangen($nilai - 100);
    } elseif ($nilai < 1000) {
      return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
    } elseif ($nilai < 2000) {
      return " Seribu " . $this->Terbilangen($nilai - 1000);
    } elseif ($nilai < 1000000) {
      return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
    } elseif ($nilai < 1000000000) {
      return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
    } elseif ($nilai < 1000000000000) {
      return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
    } elseif ($nilai < 100000000000000) {
      return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
    } elseif ($nilai <= 100000000000000) {
      return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
    }
  }

  public function indodate($tanggal)
  {
    $bulan = array(
      1 =>   'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
  }
}
