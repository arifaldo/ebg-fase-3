<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/BankUser.php';

class Administrator_BuserpassresetreqlistController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		// $files = array_diff(scandir(__DIR__ . "/../../../../../library/General"), array('..', '.'));
		// readfile(__DIR__ . "/../../../../../library/General/BankUser.php");
		// var_dump($files);

		$fields = array(
			'BUSER_ID'      	=> array(
				'field' => 'A.BUSER_ID',
				'label' => $this->language->_('User ID'),
				'sortable' => true
			),
			'BUSER_NAME'      	=> array(
				'field' => 'BUSER_NAME',
				'label' => $this->language->_('Name'),
				'sortable' => true
			),
			'BUSER_ISLOCKED'           => array(
				'field' => 'BUSER_ISLOCKED',
				'label' => $this->language->_('Lock Status'),
				'sortable' => true
			),
			'BUSER_LOCKREASON'      	=> array(
				'field' => 'BUSER_LOCKREASON',
				'label' => $this->language->_('Lock Reason'),
				'sortable' => true
			),
			'recomendded'     => array(
				'field' => '',
				'label' => $this->language->_('Recommended Method'),
				'sortable' => false
			)
		);

		$filterlist = array("USER_ID", "BUSER_NAME", "BRANCH_BANK", "REQUEST_BY");

		$this->view->filterlist = $filterlist;


		$page    = $this->_getParam('page');

		$page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';


		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$filter = $this->_getParam('filter');
		//$action = $this->_getparam('reqaction');
		$action_approve = $this->_getparam('reqaction_approve');
		$action_reject = $this->_getparam('reqaction_reject');

		$select2 = $this->_db->select()
			->from(array('A' => 'M_BUSER'), array('*'))
			->JOIN(array('C' => 'M_BRANCH'), 'C.ID = A.BUSER_BRANCH', array('C.BRANCH_NAME'))
			->where("BUSER_RCHANGE = '0'")
			->where("BUSER_RRESET = '1'")
			->where("BUSER_ID NOT LIKE " . $this->_db->quote($this->_userIdLogin));
		//echo $select2;die;
		//Zend_Debug::dump($select2);

		$filterArr = array(
			'filter' => array('StripTags', 'StringTrim'),
			'USER_ID'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'LOCK_STATUS'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'BUSER_NAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'BRANCH_BANK'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'REQUEST_BY'  => array('StripTags', 'StringTrim', 'StringToUpper'),
		);

		// $dataParam = array("USER_ID", "LOCK_STATUS", "BUSER_NAME");
		$dataParam = array("USER_ID", "BUSER_NAME", "REQUEST_BY", "BRANCH_BANK");
		$dataParamValue = array();
		if ($this->_request->getParam('wherecol')) {
			$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
			$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
			// print_r($this->_request->getParam('wherecol'));
			foreach ($dataParam as $no => $dtParam) {

				if (!empty($this->_request->getParam('wherecol'))) {
					$dataval = $this->_request->getParam('whereval');
					// print_r($dataval);
					$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if ($value == "UPDATE_DATE") {
							$order--;
						}
						if ($dtParam == $value) {
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				}
			}
		}


		$zf_filter = new Zend_Filter_Input($filterArr, array(), $dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$userid = html_entity_decode($zf_filter->getEscaped('USER_ID'));
		$lock = html_entity_decode($zf_filter->getEscaped('LOCK_STATUS'));
		$username = html_entity_decode($zf_filter->getEscaped('BUSER_NAME'));
		$bankBranch = html_entity_decode($zf_filter->getEscaped('BRANCH_BANK'));
		$requestBy = html_entity_decode($zf_filter->getEscaped('REQUEST_BY'));

		//Zend_Debug::dump($printed); die;
		if ($filter == TRUE) {

			//Zend_Debug::dump($select2); die;

			if ($userid) {
				$this->view->userid = $userid;
				$select2->where("BUSER_ID LIKE " . $this->_db->quote('%' . $userid . '%'));
			}

			if ($username) {
				$this->view->username = $username;
				$select2->where("BUSER_NAME LIKE " . $this->_db->quote('%' . $username . '%'));
			}

			if ($lock) {
				$this->view->lock = $lock;
				if ($lock == "YES") {
					$lockcek = "1";
				} elseif ($lock == "NO") {
					$lockcek = "0";
				}
				$select2->where("BUSER_ISLOCKED LIKE " . $this->_db->quote($lockcek));
			}

			if ($bankBranch) {
				$select2->where("C.ID LIKE " . $this->_db->quote('%' . $bankBranch . '%'));
			}

			if ($requestBy) {
				$select2->where("A.SUGGESTEDBY LIKE " . $this->_db->quote('%' . $requestBy . '%'));
			}
		}
		$select2->where("BUSER_STATUS != 3");

		$select2->order($sortBy . ' ' . $sortDir);
		$result = $select2->query()->fetchAll();

		//Zend_Debug::dump($action);die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($result);

		$temp = 0;

		//if($action == 'Approve')
		if ($action_approve == TRUE) {
			foreach ($result as $row) {
				if ($this->_getParam($row['BUSER_ID']) == "on") {
					$approve = new BankUser($row['BUSER_ID']);
					$exeapprove = $approve->approveRequestResetPassword();
					$temp++;

					Application_Helper_General::writeLog('ADRP', 'Success Approve : User Id ( ' . $row['BUSER_ID'] . ' )');
				}
			}

			if ($temp == 0) {
				//die;
				$this->view->error = 1;
				$this->view->report_msg = $this->language->_("Error: Please checked selection.");
			}

			if ($temp > 0) {
				$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index');
				$this->_redirect('/notification/success');
			}
		}

		//if($action == 'Reject')
		if ($action_reject == TRUE) {
			foreach ($result as $row) {
				if ($this->_getParam($row['BUSER_ID']) == "on") {
					$reject = new BankUser($row['BUSER_ID']);
					$exereject = $reject->rejectRequestResetPassword();
					$temp++;
					Application_Helper_General::writeLog('ADRP', 'Success Reject : User Id ( ' . $row['BUSER_ID'] . ' )');
				}
			}

			if ($temp == 0) {
				$this->view->error = 1;
				$this->view->report_msg = $this->language->_("Error: Please checked selection.");
			}

			if ($temp > 0) {

				//$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/payRef/'.$this->_payRef.'/flag/'.$this->_flag);

				$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index');
				$this->_redirect('/notification/success');

				//$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
				//$this->_redirect('/notification/success/index');
			}
		}
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('ADCP', 'Administrator > Backend User Password Reset Request List');
		}

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
		} else {
			$wherecol = array();
			$whereval = array();
		}
		$this->view->wherecol     = $wherecol;
		$this->view->whereval     = $whereval;
	}
}
