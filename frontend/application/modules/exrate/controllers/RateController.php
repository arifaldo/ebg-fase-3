<?php

require_once 'Zend/Controller/Action.php';

class exrate_RateController extends Application_Main
{
	public function indexAction() 
	{ 
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$svcInquiry = new Service_Inquiry();
		$result		= $svcInquiry->exrateInquiry();

		$fields = array(
						'ccy'  => array('field' => '',
											   'label' => $this->language->_('CCY'),
											   'sortable' => false),
						'sell'  => array('field' => '',
											   'label' => $this->language->_('Sell'),
											   'sortable' => false),
						'buy'   => array('field'    => '',
											  'label'    => $this->language->_('Buy'),
											  'sortable' => false),
				);

		$this->view->resultdataUpdate = Application_Helper_General::convertDate($result["UpdateDate"],$this->displayDateTimeFormat,$this->defaultDateFormat);

		$devUmum = $result["KursDevisaUmum"];
		$bankNotes = $result["KursBankNotes"];

		$this->view->userId = $this->_userIdLogin;
		$this->view->devumum = $devUmum;
		$this->view->banknotes = $bankNotes;
		$this->view->fields = $fields;

	     //insert log
	     Application_Helper_General::writeLog('','Exchange Rate');
	} 
}