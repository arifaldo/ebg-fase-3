<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class Cmdreport_customeruserlistController extends Application_Main
{
	public function indexAction()
	{

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');

		$this->view->statusCode = $this->_masterglobalstatus['code'];
		$this->view->statusDesc = $this->_masterglobalstatus['desc'];
		$this->view->modulename = $this->_request->getModuleName();
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']), array_values($this->_masterglobalstatus['desc']));

		$select = $this->_db->select()->distinct()
			->from(
				array('A' => 'M_CUSTOMER'),
				array('CUST_ID')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();
		$this->view->var = $select;

		$fields = array(
			'C.CUST_ID'   		=> array(
				'field'    => 'C.CUST_ID',
				'label'    => $this->language->_('Company'),
				'sortable' => true
			),

			// 'CUST_NAME' 		=> array('field'    => 'CUST_NAME',
			//                     		 'label'    => $this->language->_('Company Name'),
			//                     		 'sortable' => true),

			'USER_ID'     		=> array(
				'field'    => 'USER_ID',
				'label'    => $this->language->_('User ID'),
				'sortable' => true
			),

			'USER_FULLNAME'     => array(
				'field'    => 'USER_FULLNAME',
				'label'    => $this->language->_('User Name'),
				'sortable' => true
			),



			'USER_EMAIL'    	=> array(
				'field'  	=> 'USER_EMAIL',
				'label'    => $this->language->_('Email'),
				'sortable' => true
			),

			'USER_PHONE'    	=> array(
				'field'  	=> 'USER_PHONE',
				'label'    => $this->language->_('Phone Number'),
				'sortable' => true
			),

			'USER_UPDATED'     	=> array(
				'field'  	=> 'USER_UPDATED',
				'label'    => $this->language->_('Approved Date'),
				'sortable' => true
			),
			'USER_ISLOCKED'     	=> array(
				'field'  	=> 'USER_ISLOCKED',
				'label'    => $this->language->_('User is Locked'),
				'sortable' => true
			),


			'USER_STATUS'   	=> array(
				'field'    => 'USER_STATUS',
				'label'    => $this->language->_('Status'),
				'sortable' => true
			),
		);

		$filterlist = array("COMP_CODE", "COMP_NAME", "USER_ID", "USER_FULLNAME", "STATUS", "USER_EMAIL", "USER_PHONE");

		$this->view->filterlist = $filterlist;


		//validasi page, jika input page bukan angka
		$page = $this->_getParam('page');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');


		$page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;

		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';

		$filterArr = array(
			'filter' => array('StripTags', 'StringTrim'),
			'COMP_CODE'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'COMP_NAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'USER_FULLNAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'USER_ID'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'USER_EMAIL'  => array('StripTags', 'StringTrim'),
			'USER_PHONE'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'STATUS' => array('StripTags', 'StringTrim')
		);


		$dataParam = array("COMP_CODE", "COMP_NAME", "USER_FULLNAME", "USER_ID", "STATUS", "USER_EMAIL", "USER_PHONE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {

						if (empty($dataParamValue[$dtParam])) {
							$dataParamValue[$dtParam] = [];
						}
						array_push($dataParamValue[$dtParam], $dataval[$key]);

						// $dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}
		}

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $dataParamValue);
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;


		// proses pengambilan data filter,display all,sorting

		$caseStatus = "(CASE B.USER_STATUS ";
		foreach ($statusarr as $key => $val) {
			$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseStatus .= " END)";

		$select2 = $this->_db->select()
			->from(array('B' => 'M_USER'), array())
			->join(array('C' => 'M_CUSTOMER'), 'B.CUST_ID = C.CUST_ID', array(
				'C.CUST_ID',
				'C.CUST_NAME',
				'B.USER_ID',
				'B.USER_FULLNAME',
				'B.TOKEN_ID',
				'B.USER_EMAIL',
				'B.USER_PHONE',
				'B.USER_CREATED',
				'B.USER_SUGGESTED',
				'B.USER_UPDATED',
				'B.USER_LOCKREASON',
				'B.USER_ISLOCKED',
				'USER_STATUS' => $caseStatus
			))
			->joinLeft(['MB' => 'M_BUSER'], 'C.CUST_CREATEDBY = MB.BUSER_ID', [''])
			->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', ['MBR.BRANCH_CODE']);

		$compcode  = $dataParamValue["COMP_CODE"];
		$compname = $dataParamValue["COMP_NAME"];
		$userfullname = $dataParamValue["USER_FULLNAME"];
		$userid = $dataParamValue["USER_ID"];
		$status_q = $dataParamValue["STATUS"];
		$useremail = $dataParamValue["USER_EMAIL"];
		$userphone = $dataParamValue["USER_PHONE"];

		if ($this->_request->getParam("filter")) {
			$append_query = "";

			if (count($compcode) > 0) {
				foreach ($compcode as $key => $value) {
					if ($append_query != "") {
						$append_query .= " AND ";
					}
					$append_query .= "UPPER(C.CUST_ID) LIKE " . $this->_db->quote('%' . $value . '%');
					// $this->view->bgNubmer = $bgNubmer;
				}
			}

			if (count($compname) > 0) {
				foreach ($compname as $key => $value) {
					if ($append_query != "") {
						$append_query .= " AND ";
					}
					$append_query .= "UPPER(CUST_NAME) LIKE " . $this->_db->quote('%' . $value . '%');
					// $this->view->bgNubmer = $bgNubmer;
				}
			}

			if (count($userfullname) > 0) {
				foreach ($userfullname as $key => $value) {
					if ($append_query != "") {
						$append_query .= " AND ";
					}
					$append_query .= "B.USER_FULLNAME LIKE " . $this->_db->quote('%' . $value . '%');
					// $this->view->bgNubmer = $bgNubmer;
				}
			}

			if (count($userid) > 0) {
				foreach ($userid as $key => $value) {
					if ($append_query != "") {
						$append_query .= " AND ";
					}
					$append_query .= "B.USER_ID LIKE " . $this->_db->quote('%' . $value . '%');
					// $this->view->bgNubmer = $bgNubmer;
				}
			}

			if (count($status_q) > 0) {
				foreach ($status_q as $key => $value) {
					if ($append_query != "") {
						$append_query .= " AND ";
					}
					$append_query .= "B.USER_STATUS LIKE " . $this->_db->quote('%' . $value . '%');
					// $this->view->bgNubmer = $bgNubmer;
				}
			}

			if (count($useremail) > 0) {
				foreach ($useremail as $key => $value) {
					if ($append_query != "") {
						$append_query .= " AND ";
					}
					$append_query .= "B.USER_EMAIL LIKE " . $this->_db->quote('%' . $value . '%');
					// $this->view->bgNubmer = $bgNubmer;
				}
			}

			if (count($userphone) > 0) {
				foreach ($userphone as $key => $value) {
					if ($append_query != "") {
						$append_query .= " AND ";
					}
					$append_query .= "B.USER_PHONE LIKE " . $this->_db->quote('%' . $value . '%');
					// $this->view->bgNubmer = $bgNubmer;
				}
			}

			if ($append_query != "") {
				$select2->where($append_query);
			}
		}

		// if ($filter) {
		// 	$custid     = html_entity_decode($zf_filter->getEscaped('COMP_CODE'));
		// 	$custname   = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
		// 	$userid     = html_entity_decode($zf_filter->getEscaped('USER_ID'));
		// 	$username   = html_entity_decode($zf_filter->getEscaped('USER_FULLNAME'));
		// 	$useremail   = html_entity_decode($zf_filter->getEscaped('USER_EMAIL'));
		// 	$userphone   = html_entity_decode($zf_filter->getEscaped('USER_PHONE'));
		// 	$status  = $zf_filter->getEscaped('STATUS');
		// 	if ($custid) $select2->where('UPPER(C.CUST_ID) LIKE ' . $this->_db->quote('%' . strtoupper($custid) . '%'));
		// 	if ($custname) $select2->where('UPPER(CUST_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($custname) . '%'));
		// 	if ($userid) $select2->where('UPPER(USER_ID) LIKE ' . $this->_db->quote('%' . strtoupper($userid) . '%'));
		// 	if ($username) $select2->where('UPPER(USER_FULLNAME) LIKE ' . $this->_db->quote('%' . strtoupper($username) . '%'));
		// 	if ($useremail) $select2->where('UPPER(B.USER_EMAIL) LIKE ' . $this->_db->quote('%' . strtoupper($useremail) . '%'));
		// 	if ($userphone) $select2->where('UPPER(B.USER_PHONE) LIKE ' . $this->_db->quote('%' . strtoupper($useremail) . '%'));
		// 	if ($status) $select2->where('USER_STATUS=?', $status);
		// 	//echo $status; die;

		// 	$this->view->custid = $custid;
		// 	$this->view->custname = $custname;
		// 	$this->view->userid = $userid;
		// 	$this->view->username = $username;
		// 	$this->view->useremail = $useremail;
		// 	$this->view->userphone = $userphone;
		// 	$this->view->status = $status;
		// }
		$select2->order($sortBy . ' ' . $sortDir);

		// END proses pengambilan data filter,display all,sorting
		//echo $select2; die;

		if ($csv || $pdf || $this->_request->getParam('print')) {
			$result = $select2->query()->FetchAll();

			foreach ($result as $key => $value) {
				$result[$key]["USER_CREATED"] = Application_Helper_General::convertDate($value["USER_CREATED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$result[$key]["USER_SUGGESTED"] = Application_Helper_General::convertDate($value["USER_SUGGESTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$result[$key]["USER_UPDATED"] = Application_Helper_General::convertDate($value["USER_UPDATED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}

			$header = Application_Helper_Array::simpleArray($fields, 'label');

			if ($csv) {
				Application_Helper_General::writeLog('RPCU', 'Download CSV Customer User List');
				$newResult = [];
				foreach ($result as $value) {
					$save_temp = [
						$value["CUST_ID"] . "(" . $value["CUST_NAME"] . ")",
						$value["USER_ID"],
						$value["USER_FULLNAME"],
						$value["USER_EMAIL"],
						'"' . $value["USER_PHONE"] . '"',
						$value["USER_UPDATED"],
						($value["USER_ISLOCKED"] == 1) ? "Ya" : "Tidak",
						$value["USER_STATUS"]
					];
					array_push($newResult, $save_temp);
				}


				$this->_helper->download->csv($header, $newResult, null, 'Customer User List');
			}

			if ($pdf) {
				Application_Helper_General::writeLog('RPCU', 'Download PDF Customer User List');
				$this->_helper->download->pdf($header, $result, null, 'Customer User List');
			}
			if ($this->_request->getParam('print') == 1) {
				$this->_forward('print', 'index', 'widget', array('data_content' => $result, 'data_caption' => 'Customer User List', 'data_header' => $fields));
			}
		} else {
			Application_Helper_General::writeLog('RPCU', 'View Customer User List');
		}

		$auth = Zend_Auth::getInstance()->getIdentity();

		if ($auth->userHeadQuarter == "NO") {
			$select2->where('MBR.BRANCH_CODE = ?', $auth->userBranchCode); // Add Bahri
		}

		$this->paging($select2);
		//die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if (!empty($dataParamValue)) {

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',', $value);
				// var_dump("test");
				// die();
				if (!empty($duparr)) {

					foreach ($duparr as $ss => $vs) {
						$wherecol[]  = $key;
						$whereval[] = $vs;
					}
				} else {
					if (count($value) > 1) {
						foreach ($value as $k) {
							$wherecol[]  = $key;
							$whereval[] = $k;
						}
					} else {
						$wherecol[]  = $key;
						$whereval[] = $value[0];
					}
				}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
		//insert log
	}
}
