<?php
require_once 'Zend/Controller/Action.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Helper/Encryption.php';

class bgclosingworkflow_claimobligeedetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$this->view->userIdLogin = $this->_userIdLogin;

		// LOAN DISBURSE
		// $callServiceInqLoan = new Service_Account('1601010040271', '');
		// $inqLoan = $callServiceInqLoan->inquiryLoan();
		// $transferSumberDanaService =  new Service_Account('', '');
		// $resultTransferSumberDana = $transferSumberDanaService->loanDisbursement('IDR', '1601010040271', 'NASABAH J080549', 'IDR', '16101320001554', 'ANASABAH AE30481 LAL', 10000, "10000000", "10000000", "10000000", "10000000");

		// OPEN ACCOUNT
		// $clientUser = new SGO_Soap_ClientUser();
		// $requestopen = array();
		// $requestopen['cif'] = 'AE30481';
		// $requestopen['branch_code'] = '161';
		// $requestopen['product_code'] = 'K2';
		// $requestopen['currency'] = 'IDR';

		// $clientUser->callapi('createaccount', null, $requestopen);
		// $reppkResult = $clientUser->getResult();

		// Zend_Debug::dump($resultTransferSumberDana);
		// die();

		$settings = new Settings();
		$global_charges_skn = $settings->getSetting('global_charges_skn');
		$global_charges_rtgs = $settings->getSetting('global_charges_rtgs');

		// MAX MIN RTGS
		$maxrtgs = $settings->getSetting('max_amount_rtgs');
		$this->view->maxrtgs = $maxrtgs;

		$minrtgs = $settings->getSetting('min_amount_rtgs');
		$this->view->minrtgs = $minrtgs;

		// MAX MIN SKN
		$maxskn = $settings->getSetting('max_amount_skn');
		$this->view->maxskn = $maxskn;

		$minskn = $settings->getSetting('min_amount_skn');
		$this->view->minskn = $minskn;

		// BEGIN GLOBAL VARIABEL

		$this->view->global_charges_skn = $global_charges_skn;
		$this->view->global_charges_rtgs = $global_charges_rtgs;


		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		$bgType   = $config["bg"]["type"]["desc"];
		$bgCode   = $config["bg"]["type"]["code"];
		$arrbg 	= array_combine(array_values($bgCode), array_values($bgType));
		$this->view->arrbg = $arrbg;

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= array_combine(array_values($bgclosingCode), array_values($bgclosingType));
		$this->view->arrbgclosingType = $arrbgclosingType;

		$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
		$arrbgclosingStatus 	= array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));
		$this->view->arrbgclosingStatus = $arrbgclosingStatus;

		$historyStatusCode = $config["bgclosinghistory"]["status"]["code"];
		$historyStatusDesc = $config["bgclosinghistory"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));
		$this->view->historyStatusArr   = $historyStatusArr;

		$arrStatusPenutupan = [
			1 => 'Menunggu Konfirmasi Penyelesaian',
			2 => 'Permintaan Perbaikan Konfirmasi Penyelesaian',
			3 => 'Menunggu Persetujuan Penyelesaian',
			4 => 'Service Dalam Proses',
			5 => 'Proses Selesai',
			6 => 'Processing'
		];

		$this->view->arrStatusPenutupan = $arrStatusPenutupan;

		$config = Zend_Registry::get('config');

		$transCode = $config['transaction']['list']['code'];
		$transDesc = $config['transaction']['list']['desc'];

		$arrListService = array_combine($transCode, $transDesc);

		$this->view->arrListService = $arrListService;

		$arrStatusTransaction = [
			1 => 'Sukses',
			2 => 'Sukses dengan Pembaharuan Status',
			3 => 'Time Out',
			4 => 'Gagal',
			5 => 'Menunggu Pembayaran',
			6 => 'Pending Service'
		];

		$this->view->arrStatusTransaction = $arrStatusTransaction;

		$arrType = [
			1 => 'Penutupan Permintaan Principal',
			2 => 'Penutupan Tanpa Klaim',
			3 => 'Klaim Obligee'
		];

		$this->view->arrType = $arrType;

		// END GLOBAL VARIABEL

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI

		$bgparam = $this->_getParam('bgnumb');
		$this->view->bgnumb_enc = $bgparam;
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, $password);

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array(
					'*', 'CLOSE_REF_NUMBER_T' => 'A.CLOSE_REF_NUMBER',
					'INS_NAME' => new Zend_Db_Expr('(SELECT CI.CUST_NAME FROM M_CUSTOMER CI WHERE CI.CUST_ID = A.BG_INSURANCE_CODE LIMIT 1)'),
					'INS_BRANCH_NAME' => new Zend_Db_Expr('(SELECT INS_BRANCH_NAME FROM M_INS_BRANCH WHERE INS_BRANCH_CODE = (SELECT PS_FIELDVALUE FROM T_BANK_GUARANTEE_DETAIL tbgd WHERE tbgd.BG_REG_NUMBER = A.BG_REG_NUMBER AND LOWER(tbgd.PS_FIELDNAME) = \'insurance branch\' LIMIT 1) LIMIT 1)'),
					'CUST_EMAIL_BG' => 'A.CUST_EMAIL',
					'BG_REG_NUMBER_SPESIAL' => 'A.BG_REG_NUMBER'
					// 'INS_BRANCH_EMAIL' => new Zend_Db_Expr('(SELECT INS_BRANCH_EMAIL FROM M_INS_BRANCH WHERE INS_BRANCH_CODE = (SELECT PS_FIELDVALUE FROM T_BANK_GUARANTEE_DETAIL tbgd WHERE tbgd.BG_REG_NUMBER = A.BG_REG_NUMBER AND LOWER(tbgd.PS_FIELDNAME) = \'insurance branch\' LIMIT 1) LIMIT 1)')
				))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME', 'C.BRANCH_EMAIL'))
				->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLOSE_REF_NUMBER', 'A.CLAIM_FIRST_VERIFIED'))
				->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
				->joinleft(array('F' => 'T_BG_PSLIP'), 'F.BG_NUMBER = A.BG_NUMBER', array('F.PS_STATUS', 'F.TYPE', 'PSLIP_DONE' => 'F.PS_DONE'))
				//->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$bgdata[0]['CLOSE_REF_NUMBER'] = $bgdata[0]['CLOSE_REF_NUMBER_T'];
			$bgdata[0]['BG_REG_NUMBER'] = $bgdata[0]['BG_REG_NUMBER_SPESIAL'];

			switch (true) {

				case ($bgdata[0]['PS_STATUS'] == '1'):
					$logAction = 'Lihat Detail Menunggu Konfirmasi Penyelesaian untuk BG No : ' . $bgdata[0]['BG_NUMBER'] . ', Principal : ' . $bgdata[0]['CUST_ID'] . ', NoRef Penyelesaian : ' . $bgdata[0]['CLOSE_REF_NUMBER'];
					break;

				case ($bgdata[0]['PS_STATUS'] == '2'):
					$logAction = 'Lihat Detail Permintaan Perbaikan Penyelesaian untuk BG No : ' . $bgdata[0]['BG_NUMBER'] . ', Principal : ' . $bgdata[0]['CUST_ID'] . ', NoRef Penyelesaian : ' . $bgdata[0]['CLOSE_REF_NUMBER'];
					break;

				case ($bgdata[0]['PS_STATUS'] == '3'):
					$logAction = 'Lihat Detail Menunggu Persetujuan Penyelesaian untuk BG No : ' . $bgdata[0]['BG_NUMBER'] . ', Principal : ' . $bgdata[0]['CUST_ID'] . ', NoRef Penyelesaian : ' . $bgdata[0]['CLOSE_REF_NUMBER'];
					break;

				default:
					$logAction = 'Lihat Detail Penyelesaian untuk BG No : ' . $bgdata[0]['BG_NUMBER'] . ', Principal : ' . $bgdata[0]['CUST_ID'] . ', NoRef Penyelesaian : ' . $bgdata[0]['CLOSE_REF_NUMBER'];
					break;
			}

			// WRITE LOG VIEW DETAIL
			$privilege = 'DPBG';
			Application_Helper_General::writeLog($privilege, $logAction);

			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;
				$this->view->time_calculation = $this->calculate_time_span($getBG['CLAIM_FIRST_VERIFIED']);


				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();

				$this->view->bgdetaildata = array_combine(array_map('strtolower', array_column($bgdetaildata, 'PS_FIELDNAME')), array_column($bgdetaildata, 'PS_FIELDVALUE'));

				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuraceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgIssuedDate = Application_Helper_General::convertDate($getBG['BG_ISSUED'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];
				$this->view->changeType = $getBG['CHANGE_TYPE'];

				// END KONTRA GARANSI

				// BEGIN LAMPIRAN DOKUMEN

				$bgdatafile = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$bgdatafileselesai = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$this->view->lampiranDokumen = $bgdatafile;
				$this->view->lampiranDokumenSelesai = $bgdatafileselesai;

				// END LAMPIRAN DOKUMEN

				// BEGIN MARGINAL DEPOSIT PRINCIPAL
				$saveHoldAmount = null;

				$bgdatasplit = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($getBG['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();

				$totalAmountSplit = array_filter($bgdatasplit, function ($item) {
					return strtolower($item['ACCT_DESC']) != 'escrow';
				});

				$totalAmountSplit =  array_sum(array_column($totalAmountSplit, 'AMOUNT'));

				$this->view->totalAmountSplit = $totalAmountSplit;

				$this->view->bgdatasplit = $bgdatasplit;

				foreach ($bgdatasplit as $key => $value) {
					if ($value['CUST_ID'] != $getBG['CUST_ID']) continue;
					if ($value["ACCT_DESC"]) continue;

					$saveHoldAmount += intval($value["AMOUNT"]);
				}

				$this->view->holdAmount = $saveHoldAmount;
				// END MARGINAL DEPOSIT PRINCIPAL

				// BEGIN PELEPASAN DANA
				// BEGIN LIST ACCOUNT PELEPASAN DANA
				$dataListAccount = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->where('ACCT_INDEX = ?', '1')
					->query()->fetchAll();

				$this->view->dataListAccount = array_combine(array_column($dataListAccount, 'PS_FIELDNAME'), array_column($dataListAccount, 'PS_FIELDVALUE'));

				$this->view->tambahan = 0;

				if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {
					$getMdIns = $this->_db->select()
						->from('M_MARGINALDEPOSIT_DETAIL')
						->where('CUST_ID = ?', $getBG['BG_INSURANCE_CODE'])
						->query()->fetchAll();

					$butuhKMK = array_sum(array_column($getMdIns, 'GUARANTEE_AMOUNT')) + $totalAmountSplit;

					if ($butuhKMK < $this->view->dataListAccount['Transaction Amount']) {
						$this->view->tambahan = floatval(array_sum(array_column($getMdIns, 'GUARANTEE_AMOUNT')));
					}
				}

				$dataListAccount_ins = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->where('ACCT_INDEX != ?', '1')
					->query()->fetchAll();

				$this->view->dataListAccount_ins = array_combine(array_column($dataListAccount_ins, 'PS_FIELDNAME'), array_column($dataListAccount_ins, 'PS_FIELDVALUE'));

				$kmkAcctIndex = array_shift(array_filter($dataListAccount_ins, function ($item) {
					return strtolower($item['PS_FIELDNAME']) == 'source of payment' && $item['PS_FIELDVALUE'] == '5';
				}));

				$kmkAcctNumber = array_shift(array_filter($dataListAccount_ins, function ($item) use ($kmkAcctIndex) {
					return $item['ACCT_INDEX'] == $kmkAcctIndex['ACCT_INDEX'] && strtolower($item['PS_FIELDNAME']) == 'source account number';
				}));

				$this->view->kmkAcctNumber = $kmkAcctNumber;

				foreach ($dataListAccount as $key => $value) {

					if ($value['PS_FIELDNAME'] == 'SWIFT CODE') {
						$domesticBanks = $this->_db->select()
							->from(array('A' => 'M_DOMESTIC_BANK_TABLE'), array('*'))
							->where('A.SWIFT_CODE = ?', $value['PS_FIELDVALUE'])
							->query()->fetchAll();

						$this->view->beneficiaryBankName = $domesticBanks[0]['BANK_NAME'];
						$this->view->bankCode = $domesticBanks[0]['BANK_CODE'];
					}

					if ($value['PS_FIELDNAME'] == 'Beneficiary Account Number') {
						$this->view->beneficiaryAccount =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Beneficiary Account Name') {
						$this->view->beneficiaryName =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Beneficiary Address') {
						$this->view->beneficiaryAddress =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Transaction CCY') {
						$this->view->beneficiaryCurrency =   $value['PS_FIELDVALUE'];
					}
					if ($value['PS_FIELDNAME'] == 'Transaction Amount') {
						$this->view->beneficiaryAmount =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Remark 1') {
						$this->view->beneficiaryRemark =   $value['PS_FIELDVALUE'];
					}
				}


				// END LIST ACCOUNT PELEPASAN DANA
				// END PELEPASAN DANA

			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI

		// Get data T_BANK_GUARANTEE_HISTORY
		$select = $this->_db->select()
			->from(
				array('A' => 'T_BANK_GUARANTEE_HISTORY_CLOSE'),
				array('*')
			)
			->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
			->order('DATE_TIME DESC');
		$dataHistory = $this->_db->fetchAll($select);

		$this->view->dataHistory = $dataHistory;

		// Get data T_BG_PSLIP
		$selectPslip = $this->_db->select()
			->from(
				array('A' => 'T_BG_PSLIP'),
				array('*')
			)
			->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER']);
		$dataHistoryPslip = $this->_db->fetchAll($selectPslip);

		foreach ($dataHistoryPslip as $row) {

			if ($row['DISPLAY_FLAG'] == 1) {
				$flagTransaction = true;
			} else {
				$flagTransaction = false;
			}
		}

		$this->view->flagTransaction = $flagTransaction;

		// Get data T_BG_TRANSACTION
		$selectTrans = $this->_db->select()
			->from(
				array('A' => 'T_BG_TRANSACTION'),
				array('*')
			)
			->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
			->order('A.TRANSACTION_ID ASC');
		$dataHistoryTransaction = $this->_db->fetchAll($selectTrans);

		$hasTimeoutTrans = array_shift(array_filter($dataHistoryTransaction, function ($item) {
			return $item['TRA_STATUS'] == 3;
		}));

		$hasFailedTrans = array_shift(array_filter($dataHistoryTransaction, function ($item) {
			return $item['TRA_STATUS'] == 4;
		}));

		$this->view->hasTimeoutTrans = $hasTimeoutTrans;
		$this->view->hasFailedTrans = $hasFailedTrans;

		if ($this->_request->isPost() && $this->_request->getParam('timeout')) {

			// $this->cekTimeout($getBG['CLOSE_REF_NUMBER']);

			$dataPslip = array(
				'PS_STATUS' => 6,
			);

			$where['CLOSE_REF_NUMBER = ?'] = $getBG['CLOSE_REF_NUMBER'];
			$this->_db->update('T_BG_PSLIP', $dataPslip, $where);

			Application_Helper_Email::runTransaction($getBG['CLOSE_REF_NUMBER'], '3');

			$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
			$this->_redirect('/notification/success/index');
		}

		if ($this->_request->isPost() && $this->_request->getParam('gagal')) {

			// $this->cekTimeout($getBG['CLOSE_REF_NUMBER']);

			$dataPslip = array(
				'PS_STATUS' => 6,
			);

			$where['CLOSE_REF_NUMBER = ?'] = $getBG['CLOSE_REF_NUMBER'];
			$this->_db->update('T_BG_PSLIP', $dataPslip, $where);

			Application_Helper_Email::runTransaction($getBG['CLOSE_REF_NUMBER'], '4');

			$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
			$this->_redirect('/notification/success/index');
		}

		// Zend_Debug::dump($dataHistoryTransaction);
		// die();
		$this->view->dataHistoryTransaction = $dataHistoryTransaction;

		// Get data T_BG_TRANSACTION
		$selectListBank = $this->_db->select()
			->from(
				array('A' => 'M_BANK_TABLE'),
				array('*')
			);
		$dataBank = $this->_db->fetchAll($selectListBank);
		$listBank = array();
		foreach ($dataBank as $row) {
			$listBank[$row['BANK_CODE']] = $row['BANK_NAME'];
		}
		$this->view->listBank = $listBank;


		// FOR DOWNLOAD PDF ---------------- ********
		if ($this->_request->getParam('pdf')) {
			$viewHtml = $this->view->render('claimobligeedetail/generatepdf/index.phtml');

			$layout_path = $this->_helper->layout()->getLayoutPath();

			$layout_print = new Zend_Layout();
			$layout_print->setLayoutPath($layout_path) // assuming your layouts are in the same directory, otherwise change the path
				->setLayout('newpopup');

			$layout_print->content = $viewHtml;

			// echo $viewHtml;
			// echo $layout_print->render();

			$this->_helper->download->pdfModif(null, null, null, $this->language->_('Penutupan Bank Garansi Nomor BG') . ' ( ' . $getBG['BG_NUMBER'] . ' )', $viewHtml);
			die();
		}
		// FOR DOWNLOAD PDF ------------------------------- ****************

		if ($this->_request->isPost()) {

			$getAllRequest = $this->_request->getParams();
			$getAllFiles = $_FILES;

			// MENUNGGU KONFIRMASI PENYELESAIAN
			if ($getBG['PS_STATUS'] == 1) {
				$this->confirmRelease(['databg' => $getBG, 'requests' => $getAllRequest, 'files' => $getAllFiles, 'bankcode' => $this->view->bankCode]);
			}

			// PERBAIKAN KONFIRMASI PENYELESAIAN
			if ($getBG['PS_STATUS'] == 2) {
				$this->repairRelease(['databg' => $getBG, 'requests' => $getAllRequest, 'files' => $getAllFiles, 'bankcode' => $this->view->bankCode]);
			}

			if ($getAllRequest['perbaikan'] == '1') {
				$this->repairKlaim(['databg' => $getBG, 'requests' => $getAllRequest, 'files' => $getAllFiles, 'bankcode' => $this->view->bankCode]);
			}

			// PERSETUJUAN
			if ($getBG['PS_STATUS'] == 3) {
				try {
					//code...
					$this->approveRelease(['databg' => $getBG, 'requests' => $getAllRequest, 'files' => $getAllFiles, 'bankcode' => $this->view->bankCode]);
				} catch (\Throwable $th) {
					Zend_Debug::dump($th);
					die();
					//throw $th;
				}
			}

			//die();

			$datas = $this->_request->getParams();


			$kirim = $this->_getParam('kirim');
			if ($kirim) {

				$attachmentDestination   = UPLOAD_PATH . '/document/closing/';

				$files = $_FILES;
				$namaFile = $files['uploadSource']['name'][0];
				$lokasiTmp = $files['uploadSource']['tmp_name'][0];

				# kita tambahkan uniqid() agar nama gambar bersifat unik
				$date = date("dmy");
				$time = date("his");
				$namaBaru = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . '_' . $namaFile;

				$prosesUpload = move_uploaded_file($lokasiTmp, $attachmentDestination . $namaBaru);

				$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
					"CLOSE_REF_NUMBER" => $getBG['CLOSE_REF_NUMBER'],
					"FILE_NOTE" => 'Memo / Surat Persetujuan Klaim',
					"BG_FILE" => $namaBaru,
				]);


				$data = array(
					'PS_CONFIRMED' => new Zend_Db_Expr("now()"),
					'PS_CONFIRMEDBY' => $this->_userIdLogin,
					'PS_STATUS' => 3,
				);

				$where['CLOSE_REF_NUMBER= ?'] = $getBG['CLOSE_REF_NUMBER'];
				$this->_db->update('T_BG_PSLIP', $data, $where);

				$historyInsert = array(
					'DATE_TIME'         => new Zend_Db_Expr("now()"),
					'CLOSE_REF_NUMBER'         => $getBG['CLOSE_REF_NUMBER'],
					'BG_NUMBER'         => $getBG['BG_NUMBER'],
					'CUST_ID'           => "-",
					'USER_FROM'        => 'BANK',
					'USER_LOGIN'        => $this->_userIdLogin,
					'BG_REASON'        => null,
					'HISTORY_STATUS'        => 15,
				);

				$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);



				if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
					Application_Helper_General::writeLog('KPCC', 'Konfirmasi Penyelesaian Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
				} else {
					Application_Helper_General::writeLog('KPNC', 'Konfirmasi Penyelesaian Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
				}

				$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
				$this->_redirect('/notification/success/index');
			}

			$persetujuan = $this->_getParam('persetujuan');
			if ($persetujuan) {

				$authAdapter = new SGO_Auth_Adapter_Ldap($this->_userIdLogin, $this->_request->getParam("password_to_ldap"));

				$checkLDAP = $authAdapter->verifyldap();

				$checkLDAP = json_decode($checkLDAP, true);

				//Zend_Debug::dump($checkLDAP);die;

				if ($checkLDAP['status'] === '0') {
					$error_LDAP = $this->language->_("Password Salah");
					$this->view->error = true;
					$this->view->errorMsg = $error_LDAP;
				}

				if ($checkLDAP['status'] === '1') {
					$data = array(
						'PS_CONFIRMED' => new Zend_Db_Expr("now()"),
						'PS_CONFIRMEDBY' => $this->_userIdLogin,
						'PS_STATUS' => 4,
						'DISPLAY_FLAG' => 1,
					);

					$where['CLOSE_REF_NUMBER= ?'] = $getBG['CLOSE_REF_NUMBER'];
					$this->_db->update('T_BG_PSLIP', $data, $where);

					$historyInsert = array(
						'DATE_TIME'         => new Zend_Db_Expr("now()"),
						'CLOSE_REF_NUMBER'         => $getBG['CLOSE_REF_NUMBER'],
						'BG_NUMBER'         => $getBG['BG_NUMBER'],
						'CUST_ID'           => "-",
						'USER_FROM'        => 'BANK',
						'USER_LOGIN'        => $this->_userIdLogin,
						'BG_REASON'        => null,
						'HISTORY_STATUS'        => 20,
					);

					foreach ($dataHistoryTransaction as $row) {

						/*$arrListService = [
							1 => 'Transfer Inhouse',
							2 => 'Unlock Deposito',
							3 => 'Unlock Saving',
							4 => 'Open Account',
							5 => 'Deposito Disbursement',
							6 => 'Loan Disbursement',
							7 => 'SKN',
							8 => 'RTGS'
						  ];*/
						$statusService = array();

						if ($row['SERVICE'] == 1) {


							$param = [
								"SOURCE_ACCOUNT_NUMBER" => $row["SOURCE_ACCT"],
								"SOURCE_ACCOUNT_NAME" => $row["SOURCE_ACCT_NAME"],
								"BENEFICIARY_ACCOUNT_NUMBER" => $row['BENEF_ACCT'],
								"BENEFICIARY_ACCOUNT_NAME" => $row["BENEF_ACCT_NAME"],
								"PHONE_NUMBER_FROM" =>  $row["SOURCE_PHONE"],
								"AMOUNT" => $row["TRA_AMOUNT"],
								"PHONE_NUMBER_TO" =>  $row["BENEF_ACCT_PHONE"],
								"RATE1" => 0,
								"RATE2" => 0,
								"RATE3" => 0,
								"RATE4" => 0,
								"DESCRIPTION" => null,
								"DESCRIPTION_DETAIL" => null,
							];


							$service =  new Service_TransferWithin($param);
							$response = $service->sendTransfer();

							if ($response["RawResult"]["response_code"] == "0000") {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $response["RawResult"]["response_code"] . " - " . $response["RawResult"]["response_desc"],
									"UUID" => $response["RawResult"]["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 1
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
								$statusService = 1;
							} else if ($response["RawResult"]["response_code"] == "9999") {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $response["RawResult"]["response_code"] . " - " . $response["RawResult"]["response_desc"],
									"UUID" => $response["RawResult"]["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 3
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
							} else {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $response["RawResult"]["response_code"] . " - " . $response["RawResult"]["response_desc"],
									"UUID" => $response["RawResult"]["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 4
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
							}
						}

						if ($row['SERVICE'] == 2) {
							$svcAccount = new Service_Account($row["SOURCE_ACCT"], null);
							$unlockDeposito = $svcAccount->unlockDeposito('T', $row['HOLD_BY_BRANCH'], $row['HOLD_SEQUENCE']);

							if ($unlockDeposito['response_code'] == "0000") {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $unlockDeposito['response_code'] . " - " . $unlockDeposito['response_desc'],
									"UUID" => $unlockDeposito["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 1
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
								$statusService++;
							} else if ($unlockDeposito['response_code'] == "9999") {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $unlockDeposito['response_code'] . " - " . $unlockDeposito['response_desc'],
									"UUID" => $unlockDeposito["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 3
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
							} else {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $unlockDeposito['response_code'] . " - " . $unlockDeposito['response_desc'],
									"UUID" => $unlockDeposito["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 4
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
							}
						}

						if ($row['SERVICE'] == 3) {
							$svcAccount = new Service_Account($row["SOURCE_ACCT"], null);
							$unlockSaving = $svcAccount->unlockSaving([
								'branch_code' => $row['HOLD_BY_BRANCH'],
								'sequence_number' => $row['HOLD_SEQUENCE']
							]);

							if ($unlockSaving['response_code'] == "0000") {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $unlockSaving['response_code'] . " - " . $unlockSaving['response_desc'],
									"UUID" => $unlockSaving["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 1
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
								$statusService++;
							} else if ($unlockSaving['response_code'] == "9999") {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $unlockSaving['response_code'] . " - " . $unlockSaving['response_desc'],
									"UUID" => $unlockSaving["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 3
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
							} else {
								$this->_db->update("T_BG_TRANSACTION", [
									"BANK_RESPONSE" => $unlockSaving['response_code'] . " - " . $unlockSaving['response_desc'],
									"UUID" => $unlockSaving["uuid"],
									'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
									'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
									"TRA_STATUS" => 4
								], [
									"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
								]);
							}
						}
					}



					if (count($dataHistoryTransaction) == $statusService) {
						$data = array(
							'PS_APPROVED' => new Zend_Db_Expr("now()"),
							'PS_APPROVEDBY' => $this->_userIdLogin,
							'PS_STATUS' => 5,
						);

						$where['CLOSE_REF_NUMBER= ?'] = $getBG['CLOSE_REF_NUMBER'];
						$this->_db->update('T_BG_PSLIP', $data, $where);
					} else {
						$data = array(
							'PS_APPROVED' => new Zend_Db_Expr("now()"),
							'PS_APPROVEDBY' => $this->_userIdLogin,
						);

						$where['CLOSE_REF_NUMBER= ?'] = $getBG['CLOSE_REF_NUMBER'];
						$this->_db->update('T_BG_PSLIP', $data, $where);
					}

					if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
						Application_Helper_General::writeLog('PPCC', 'Persetujuan Penyelesaian Pelepasan MD untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
					} else {
						Application_Helper_General::writeLog('PPNC', 'Persetujuan Penyelesaian Pelepasan MD untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
					}

					$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
					$this->_redirect('/notification/success/index');
				}
			}

			$back = $this->_getParam('back');
			if ($back) {
				$this->_redirect('/bgclosingworkflow/release');
			}
		}
	}

	public function inquiryaccountinfoAction()
	{

		//integrate ke core
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		if ($result['response_desc'] == 'Success') //00 = success
		{
			$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
			$result2 = $svcAccountCIF->inquiryCIFAccount();

			$filterBy = $result['account_number']; // or Finance etc.
			$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
				return ($var['account_number'] == $filterBy);
			});

			$singleArr = array();
			foreach ($new as $key => $val) {
				$singleArr = $val;
			}
		}

		if ($singleArr['type_desc'] == 'Deposito') {
			$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountDeposito->inquiryDeposito();
		} else {
			$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountBalance->inquiryAccountBalance();
		}
		$return = array_merge($result, $singleArr, $result3);

		$data['data'] = false;
		is_array($return) ? $return :  $return = $data;


		echo json_encode($return);
	}

	public function detailAction()
	{

		$this->_helper->layout()->setLayout('popup');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');

		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		$bgType   = $config["bg"]["type"]["desc"];
		$bgCode   = $config["bg"]["type"]["code"];
		$arrbg 	= array_combine(array_values($bgCode), array_values($bgType));
		$this->view->arrbg = $arrbg;

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= array_combine(array_values($bgclosingCode), array_values($bgclosingType));
		$this->view->arrbgclosingType = $arrbgclosingType;

		$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
		$arrbgclosingStatus 	= array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));
		$this->view->arrbgclosingStatus = $arrbgclosingStatus;

		$historyStatusCode = $config["bgclosinghistory"]["status"]["code"];
		$historyStatusDesc = $config["bgclosinghistory"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));
		$this->view->historyStatusArr   = $historyStatusArr;

		$arrStatusPenutupan = [
			1 => 'Menunggu Konfirmasi Penyelesaian',
			2 => 'Permintaan Perbaikan Konfirmasi Penyelesaian',
			3 => 'Menunggu Persetujuan Penyelesaian',
			4 => 'Service Dalam Proses',
			5 => 'Proses Selesai',
			6 => 'Processing'
		];

		$this->view->arrStatusPenutupan = $arrStatusPenutupan;

		$arrListService = [
			1 => 'Transfer Inhouse',
			2 => 'Unlock Deposito',
			3 => 'Unlock Saving',
			4 => 'Open Account',
			5 => 'Deposito Disbursement',
			6 => 'Loan Disbursement',
			7 => 'SKN',
			8 => 'RTGS',
			9 => 'Manual Settlement'
		];

		$this->view->arrListService = $arrListService;

		$arrStatusTransaction = [
			1 => 'Sukses',
			2 => 'Sukses dengan Pembaharuan Status',
			3 => 'Time Out',
			4 => 'Gagal',
			5 => 'Menunggu Pembayaran',
			6 => 'Pending Service'
		];

		$this->view->arrStatusTransaction = $arrStatusTransaction;

		// END GLOBAL VARIABEL

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI

		$bgparam = $this->_getParam('bgnumb');

		$this->view->bgnumb_enc = $bgparam;
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, $password);

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
				->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLOSE_REF_NUMBER', 'D.CLAIM_FIRST_VERIFIED'))
				->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
				->joinleft(array('F' => 'T_BG_PSLIP'), 'F.CLOSE_REF_NUMBER = D.CLOSE_REF_NUMBER', array('F.PS_STATUS'))
				//->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;
				$this->view->time_calculation = $this->calculate_time_span($getBG['CLAIM_FIRST_VERIFIED']);


				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();


				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuraceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgIssuedDate = Application_Helper_General::convertDate($getBG['BG_ISSUED'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];
				$this->view->changeType = $getBG['CHANGE_TYPE'];

				// END KONTRA GARANSI


			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI

		// Get data T_BG_TRANSACTION
		$ptransid = $this->_getParam('transid');

		$decryption2 = urldecode($ptransid);
		$AESMYSQL2 = new Crypt_AESMYSQL();
		$transid = $AESMYSQL2->decrypt($decryption2, $password);

		$select = $this->_db->select()
			->from(
				array('A' => 'T_BG_TRANSACTION'),
				array('*')
			)
			->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
			->where('A.TRANSACTION_ID = ?', $transid)
			->order('A.SERVICE ASC');
		$dataHistoryTransaction = $this->_db->fetchAll($select);
		$this->view->dataHistoryTransaction = $dataHistoryTransaction[0];
	}


	public function calculate_time_span($date)
	{
		$seconds  = strtotime(date('Y-m-d H:i:s')) - strtotime($date);

		$months = floor($seconds / (3600 * 24 * 30));
		$day = floor($seconds / (3600 * 24));
		$hours = floor($seconds / 3600);
		$mins = floor(($seconds - ($hours * 3600)) / 60);
		$secs = floor($seconds % 60);

		if ($seconds < 60)
			$time = $secs . " seconds ago";
		else if ($seconds < 60 * 60)
			$time = $mins . " min ago";
		else if ($seconds < 24 * 60 * 60)
			$time = $hours . " hours ago";
		else if ($seconds < 24 * 60 * 60)
			$time = $day . " day ago";
		else
			$time = $months . " month ago";

		return $day . " hari " . $hours . " jam " . $mins . " menit ";
	}

	public function inquirymarginaldepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$regNumber = $this->_getParam('reqNumber');

		$bgdata = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		$optHtml = '<table id="guarantedTransactions">
		<thead>
                                <th>Hold Seq</th>
                                <th>Nomor Rekening</th>
                                <th>Nama Rekening</th>
                                <th>Tipe</th>
                                <th>Valuta</th>
								<th>Nominal</th>
                            </thead>
                            <tbody>';

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($bgdata[0]['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		if (!empty($bgdatasplit)) {
			foreach ($bgdatasplit as $key => $row) {
				if (strtolower($row["M_ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D") continue;
				// GET SERVICE 
				$serviceAccount = new Service_Account($row['ACCT'], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($row['ACCT'], array_column($saveResult, "account_number"));

				$optHtml .= '<tr>
						<td>' . $row['HOLD_SEQUENCE'] . '</td>
						<td>' . $row['ACCT'] . '</td>
						<td>' . $row['NAME'] . '</td>
						<td>' . $saveResult[$listAccount]["type_desc"] . " " . $saveResult[$listAccount]["product_type"] . '</td>
						<td>' . $saveResult[$listAccount]["currency"] . '</td>
						<td>' . Application_Helper_General::displayMoney($row['AMOUNT']) . '</td>
					</tr>
					';
			}
		} else {
			$optHtml .= '<tr><td colspan="7" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}

		$optHtml .= '</tbody></table>';

		echo $optHtml;
	}

	public function inquirypembebananbiayanewAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();

		$acctNumber = $this->_request->getParam('acctnumber');
		$ownerAcct = $this->_request->getParam('owneracct');

		$rekeningClaim = $this->_request->getParam('claimnumber');
		$rekeningClaim = $AESMYSQL->decrypt(urldecode($rekeningClaim), uniqid());

		$amount = $this->_request->getParam('amount');

		if (!$acctNumber || !$ownerAcct) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'nothing']);
			return true;
		}

		$ownerAcct = urldecode($AESMYSQL->decrypt($ownerAcct, uniqid()));

		$searchAcct = $this->_db->select()
			->from('M_CUSTOMER_ACCT')
			->where('ACCT_NO LIKE ?', '%' . $acctNumber . '%')
			->where('CUST_ID LIKE ?', '%' . $ownerAcct . '%')
			->query()->fetch();

		// if (!$searchAcct) {
		// 	$searchAcct = $this->_db->select()
		// 		->from('M_CUSTOMER_ACCT')
		// 		->where('ACCT_NO LIKE ?', '%' . ltrim($acctNumber, '0') . '%')
		// 		->where('CUST_ID LIKE ?', '%' . $ownerAcct . '%')
		// 		->query()->fetch();

		// 	if (!$searchAcct) {
		// 		header('Content-Type: application/json');
		// 		echo json_encode(['error' => true, 'result' => 'rekening tidak ditemukan']);
		// 		return true;
		// 	}
		// }

		$callService = new Service_Account($acctNumber, '');
		$getAcctInfo = $callService->inquiryAccontInfo();

		if (!$getAcctInfo || $getAcctInfo['response_code'] != '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$callServiceCif = new Service_Account('', '', '', '', '', $getAcctInfo['cif']);
		$getCifList = $callServiceCif->inquiryCIFAccount();

		if (!$getCifList || $getCifList['response_code'] != '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$getCifDetailAcct = array_filter($getCifList['accounts'], function ($item) use ($acctNumber) {
			return ltrim($item['account_number'], '0') == ltrim($acctNumber, '0');
		});

		$getCifDetailAcct = array_shift(array_values($getCifDetailAcct));

		if (!$getCifDetailAcct) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		if (strtolower($getCifDetailAcct['type']) == 't') {
			// DEPOSITO
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tipe Deposito tidak dapat dipilih sebagai Rekening Sumber Dana']);
			return true;
		} else {
			// TABUNGAN DAN GIRO
			$saveBalance = $callService->inquiryAccountBalance();

			if ($saveBalance['response_code'] != '0000') {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
				return true;
			}

			if ($saveBalance['status'] != 1 && $saveBalance['status'] != 4) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak aktif']);
				return true;
			}

			if (round($saveBalance['available_balance'], 2) < round($amount, 2)) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Saldo tidak mencukupi']);
				return true;
			}

			if (ltrim($saveBalance['account_number'], '0') == ltrim($rekeningClaim, '0')) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak boleh sama dengan Rekening Tujuan Pembayaran Klaim']);
				return true;
			}
		}

		$result = array_merge($getAcctInfo, $getCifDetailAcct);
		$result = array_intersect_key($result, ['account_name' => '', 'type_desc' => '', 'currency' => '', 'product_type' => '', 'product_desc' => '', 'produc_desc' => '', 'type' => '']);

		header('Content-Type: application/json');
		echo json_encode(['error' => false, 'result' => $result]);
		return true;
	}

	public function inquirypembebananbiayaAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();

		$acctNumber = $this->_request->getParam('acctnumber');
		$ownerAcct = $this->_request->getParam('owneracct');

		$rekeningClaim = $this->_request->getParam('claimnumber');
		$rekeningClaim = $AESMYSQL->decrypt(urldecode($rekeningClaim), uniqid());

		$amount = $this->_request->getParam('amount');
		$amount = $AESMYSQL->decrypt(urldecode($amount), uniqid());

		if (!$acctNumber || !$ownerAcct) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'nothing']);
			return true;
		}

		$ownerAcct = urldecode($AESMYSQL->decrypt($ownerAcct, uniqid()));

		$searchAcct = $this->_db->select()
			->from('M_CUSTOMER_ACCT')
			->where('ACCT_NO LIKE ?', '%' . $acctNumber . '%')
			->where('CUST_ID LIKE ?', '%' . $ownerAcct . '%')
			->query()->fetch();

		// if (!$searchAcct) {
		// 	$searchAcct = $this->_db->select()
		// 		->from('M_CUSTOMER_ACCT')
		// 		->where('ACCT_NO LIKE ?', '%' . ltrim($acctNumber, '0') . '%')
		// 		->where('CUST_ID LIKE ?', '%' . $ownerAcct . '%')
		// 		->query()->fetch();

		// 	if (!$searchAcct) {
		// 		header('Content-Type: application/json');
		// 		echo json_encode(['error' => true, 'result' => 'rekening tidak ditemukan']);
		// 		return true;
		// 	}
		// }

		$callService = new Service_Account($acctNumber, '');
		$getAcctInfo = $callService->inquiryAccontInfo();

		if (!$getAcctInfo || $getAcctInfo['response_code'] != '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$callServiceCif = new Service_Account('', '', '', '', '', $getAcctInfo['cif']);
		$getCifList = $callServiceCif->inquiryCIFAccount();

		if (!$getCifList || $getCifList['response_code'] != '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$getCifDetailAcct = array_filter($getCifList['accounts'], function ($item) use ($acctNumber) {
			return ltrim($item['account_number'], '0') == ltrim($acctNumber, '0');
		});

		$getCifDetailAcct = array_shift(array_values($getCifDetailAcct));

		if (!$getCifDetailAcct) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		if (strtolower($getCifDetailAcct['type']) == 't') {
			// DEPOSITO
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tipe Deposito tidak dapat dipilih sebagai Rekening Sumber Dana']);
			return true;
		} else {
			// TABUNGAN DAN GIRO
			$saveBalance = $callService->inquiryAccountBalance();

			if ($saveBalance['response_code'] != '0000') {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
				return true;
			}

			if ($saveBalance['status'] != 1 && $saveBalance['status'] != 4) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak aktif']);
				return true;
			}

			if (round($saveBalance['available_balance'], 2) < round($amount, 2)) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Saldo tidak mencukupi']);
				return true;
			}

			if (ltrim($saveBalance['account_number'], '0') == ltrim($rekeningClaim, '0')) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak boleh sama dengan Rekening Tujuan Pembayaran Klaim']);
				return true;
			}
		}

		$result = array_merge($getAcctInfo, $getCifDetailAcct);
		$result = array_intersect_key($result, ['account_name' => '', 'type_desc' => '', 'currency' => '', 'product_type' => '', 'product_desc' => '', 'produc_desc' => '', 'type' => '']);

		header('Content-Type: application/json');
		echo json_encode(['error' => false, 'result' => $result]);
		return true;
	}

	public function inquirypembebananbiayainsAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();

		$acctNumber = $this->_request->getParam('acctnumber');
		$ownerAcct = $this->_request->getParam('owneracct');

		$rekeningClaim = $this->_request->getParam('claimnumber');
		$rekeningClaim = $AESMYSQL->decrypt(urldecode($rekeningClaim), uniqid());

		$amount = $this->_request->getParam('amount');

		if (!$acctNumber || !$ownerAcct) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'nothing']);
			return true;
		}

		$ownerAcct = urldecode($AESMYSQL->decrypt($ownerAcct, uniqid()));

		$searchAcct = $this->_db->select()
			->from('M_CUSTOMER_ACCT')
			->where('ACCT_NO LIKE ?', '%' . $acctNumber . '%')
			->where('CUST_ID LIKE ?', '%' . $ownerAcct . '%')
			->query()->fetch();

		// if (!$searchAcct) {
		// 	$searchAcct = $this->_db->select()
		// 		->from('M_CUSTOMER_ACCT')
		// 		->where('ACCT_NO LIKE ?', '%' . ltrim($acctNumber, '0') . '%')
		// 		->where('CUST_ID LIKE ?', '%' . $ownerAcct . '%')
		// 		->query()->fetch();

		// 	if (!$searchAcct) {
		// 		header('Content-Type: application/json');
		// 		echo json_encode(['error' => true, 'result' => 'rekening tidak ditemukan']);
		// 		return true;
		// 	}
		// }

		$callService = new Service_Account($acctNumber, '');
		$getAcctInfo = $callService->inquiryAccontInfo();

		if (!$getAcctInfo || $getAcctInfo['response_code'] != '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$callServiceCif = new Service_Account('', '', '', '', '', $getAcctInfo['cif']);
		$getCifList = $callServiceCif->inquiryCIFAccount();

		if (!$getCifList || $getCifList['response_code'] != '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$getCifDetailAcct = array_filter($getCifList['accounts'], function ($item) use ($acctNumber) {
			return ltrim($item['account_number'], '0') == ltrim($acctNumber, '0');
		});

		$getCifDetailAcct = array_shift(array_values($getCifDetailAcct));

		if (!$getCifDetailAcct) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		if (strtolower($getCifDetailAcct['type']) == 't') {
			// DEPOSITO
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tipe Deposito tidak dapat dipilih sebagai Rekening Sumber Dana']);
			return true;
		} else {
			// TABUNGAN DAN GIRO
			$saveBalance = $callService->inquiryAccountBalance();

			if ($saveBalance['response_code'] != '0000') {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
				return true;
			}

			if ($saveBalance['status'] != 1 && $saveBalance['status'] != 4) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak aktif']);
				return true;
			}

			if (round($saveBalance['available_balance'], 2) < round($amount, 2)) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Saldo tidak mencukupi']);
				return true;
			}

			if (ltrim($saveBalance['account_number'], '0') == ltrim($rekeningClaim, '0')) {
				header('Content-Type: application/json');
				echo json_encode(['error' => true, 'result' => 'Rekening tidak boleh sama dengan Rekening Tujuan Pembayaran Klaim']);
				return true;
			}
		}

		$result = array_merge($getAcctInfo, $getCifDetailAcct);
		$result = array_intersect_key($result, ['account_name' => '', 'type_desc' => '', 'currency' => '', 'product_type' => '', 'product_desc' => '', 'produc_desc' => '', 'type' => '']);

		header('Content-Type: application/json');
		echo json_encode(['error' => false, 'result' => $result]);
		return true;
	}

	private function confirmRelease($data)
	{

		// SAVE REK PEMBEBANAN BIAYA JIKA ADA
		if ($data['requests']['beban_acct_name']) {
			$getDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['ACCT_INDEX'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->group('ACCT_INDEX')
				->query()->fetchAll();

			$setIndex = count($getDetail) + 1;

			$array_insert = ['Claim Transfer Type', 'Source of Payment', 'Charge Account CCY', 'Charge Account Number', 'Charge Account Name'];
			$array_data_insert = [$data['requests']['transferType'], 6, $data['requests']['beban_acct_currency'], $data['requests']['beban_acct_number'], $data['requests']['beban_acct_name']];

			foreach ($array_insert as $key => $value) {
				if ($data['bankcode'] != 200 && strtolower($value) == 'claim transfer type') {
					$insertData = [
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'ACCT_INDEX' => $setIndex,
						'PS_FIELDNAME' => $value,
						'PS_FIELDVALUE' => $array_data_insert[$key],
					];
				} else {
					if (strtolower($value) == 'claim transfer type') continue;
					$insertData = [
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'ACCT_INDEX' => $setIndex,
						'PS_FIELDNAME' => $value,
						'PS_FIELDVALUE' => $array_data_insert[$key],
					];
				}

				$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
			}
		}

		// LINEFACILITY
		if ($data['databg']['COUNTER_WARRANTY_TYPE'] == '2') {
			$getDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['ACCT_INDEX'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->group('ACCT_INDEX')
				->query()->fetchAll();

			$setIndex = count($getDetail) + 1;

			$sumberDana = $data['requests']['sumber_dana_type'] == '1' ? 1 : 5;

			$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
			$array_data_insert = [
				$data['requests']['lfsumber_acct_currency'],
				$sumberDana,
				$data['requests']['lfsumber_acct_number'],
				$data['requests']['lfsumber_acct_name'],
				'IDR',
				$data['databg']['BG_AMOUNT'],
				'BG No. ' . $data['databg']['BG_NUMBER'],
				'Pooling Dana Klaim'
			];

			foreach ($array_insert as $key => $value) {

				$insertData = [
					'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
					'ACCT_INDEX' => $setIndex,
					'PS_FIELDNAME' => $value,
					'PS_FIELDVALUE' => $array_data_insert[$key],
				];

				$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
			}
		}

		// ASURANSI
		if ($data['databg']['COUNTER_WARRANTY_TYPE'] == '3') {

			$getDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['ACCT_INDEX'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->group('ACCT_INDEX')
				->query()->fetchAll();

			$dataListAccount = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('ACCT_INDEX = ?', '1')
				->query()->fetchAll();
			$dataListAccount = array_combine(array_column($dataListAccount, 'PS_FIELDNAME'), array_column($dataListAccount, 'PS_FIELDVALUE'));

			// CHECK MD PRINCIPAL
			$checkMdPrincipal = $this->_db->select()
				->from('T_BANK_GUARANTEE_SPLIT')
				->where('BG_NUMBER = ?', $data['databg']['BG_NUMBER'])
				->query()->fetchAll();

			$checkMdPrincipal = array_filter($checkMdPrincipal, function ($item) {
				return strtolower($item['ACCT_DESC']) != 'escrow';
			});

			$totalMdPrincipal = ($checkMdPrincipal) ? array_sum(array_column($checkMdPrincipal, 'AMOUNT')) : 0;

			$bgdetaildata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
				->query()->fetchAll();

			$bgdetaildata = array_combine(array_map('strtolower', array_column($bgdetaildata, 'PS_FIELDNAME')), array_column($bgdetaildata, 'PS_FIELDVALUE'));

			$totalMdPrincipal = $totalMdPrincipal > 0 ? ($totalMdPrincipal * floatval($bgdetaildata['marginal deposit pecentage']) / 100) : $totalMdPrincipal;

			$setIndex = count($getDetail) + 1;

			$sumberDana = $data['requests']['sumber_dana_type']; // 1 = rekening asuransi, 2 = md asuransi

			// REKENING ASURANSI
			if ($sumberDana == '1') {
				// $mustTransfer = round(floatval($dataListAccount['Transaction Amount']), 2) - round(floatval($totalMdPrincipal), 2);
				$mustTransfer = round(floatval($dataListAccount['Transaction Amount']), 2);

				$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
				$array_data_insert = [
					$data['requests']['lfsumber_acct_currency'],
					'1',
					$data['requests']['lfsumber_acct_number'],
					$data['requests']['lfsumber_acct_name'],
					'IDR',
					$mustTransfer,
					'BG No. ' . $data['databg']['BG_NUMBER'],
					'Pooling Dana Klaim'
				];

				foreach ($array_insert as $key => $value) {

					$insertData = [
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'ACCT_INDEX' => $setIndex,
						'PS_FIELDNAME' => $value,
						'PS_FIELDVALUE' => $array_data_insert[$key],
					];

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
				}
			}

			// MD ASURANSI
			else {
				$needKMK = $data['requests']['need_kmk'];

				// BUTUH KMK
				if ($needKMK == '1') {
					// CHECK MD PRINCIPAL
					$checkMdIns = $this->_db->select()
						->from('M_MARGINALDEPOSIT_DETAIL')
						->where('CUST_ID = ?', $data['databg']['BG_INSURANCE_CODE'])
						->query()->fetchAll();

					$totalMdIns = ($checkMdIns) ? array_sum(array_column($checkMdIns, 'GUARANTEE_AMOUNT')) : 0;

					// $mustTransfer = round(floatval($dataListAccount['Transaction Amount']), 2) - round(floatval($totalMdPrincipal), 2) - round(floatval($totalMdIns), 2);
					$mustTransfer = round(floatval($dataListAccount['Transaction Amount']), 2) - round(floatval($totalMdIns), 2);

					$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
					$array_data_insert = [
						$data['requests']['kmkasuransi_acct_currency'],
						'5',
						$data['requests']['kmkasuransi_acct_number'],
						$data['requests']['kmkasuransi_acct_name'],
						'IDR',
						$mustTransfer,
						'BG No. ' . $data['databg']['BG_NUMBER'],
						'Pooling Dana Klaim'
					];

					foreach ($array_insert as $key => $value) {

						$insertData = [
							'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
							'ACCT_INDEX' => $setIndex,
							'PS_FIELDNAME' => $value,
							'PS_FIELDVALUE' => $array_data_insert[$key],
						];

						$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
					}

					$setIndex++;

					foreach ($checkMdIns as $key => $acct) {
						$tempDetailInfo = $this->getDetailInfoAcct($acct['MD_ACCT']);

						$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
						if (strtolower($tempDetailInfo['type']) == 't' || strtolower($tempDetailInfo['type']) == 's') {
							$array_insert[] = 'Hold Sequence';
							$array_insert[] = 'Hold By Branch';
						}

						switch (strtolower($tempDetailInfo['type'])) {
							case 'd':
								$sourcePayment = '2';
								break;

							case 't':
								$sourcePayment = '3';
								break;

							case 's':
								$sourcePayment = '4';
								break;

							default:
								$sourcePayment = '0';
								break;
						}

						$array_data_insert = [
							$tempDetailInfo['currency'],
							$sourcePayment,
							$acct['MD_ACCT'],
							$tempDetailInfo['account_name'],
							'IDR',
							// round(floatval($acct['GUARANTEE_AMOUNT']), 2),
							round(floatval(str_replace(',', '', $data['requests']['amountmduse'][$key])), 2),
							'BG No. ' . $data['databg']['BG_NUMBER'],
							'Pooling Dana Klaim'
						];

						if (strtolower($tempDetailInfo['type']) == 't' || strtolower($tempDetailInfo['type']) == 's') {
							$array_data_insert[] = $acct['HOLD_SEQUENCE'];
							$array_data_insert[] = $acct['HOLD_BY_BRANCH'];
						}

						foreach ($array_insert as $key => $value) {

							$insertData = [
								'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
								'ACCT_INDEX' => $setIndex,
								'PS_FIELDNAME' => $value,
								'PS_FIELDVALUE' => $array_data_insert[$key],
							];

							$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
						}

						$setIndex++;
					}
				}

				// TIDAK BUTUH KMK
				else {
					// CHECK MD PRINCIPAL
					$checkMdIns = $this->_db->select()
						->from('M_MARGINALDEPOSIT_DETAIL')
						->where('CUST_ID = ?', $data['databg']['BG_INSURANCE_CODE'])
						->query()->fetchAll();

					foreach ($data['requests']['indexacctmd'] as $key => $index) {
						$tempAcctIndex = $data['requests']['indexacct'][$key];

						$acct = array_shift(array_filter($checkMdIns, function ($item) use ($tempAcctIndex) {
							return ltrim($item['MD_ACCT'], '0') == ltrim($tempAcctIndex, '0');
						}));

						$tempDetailInfo = $this->getDetailInfoAcct($acct['MD_ACCT']);

						$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
						if (strtolower($tempDetailInfo['type']) == 't' || strtolower($tempDetailInfo['type']) == 's') {
							$array_insert[] = 'Hold Sequence';
							$array_insert[] = 'Hold By Branch';
						}

						switch (strtolower($tempDetailInfo['type'])) {
							case 'd':
								$sourcePayment = '2';
								break;

							case 't':
								$sourcePayment = '3';
								break;

							case 's':
								$sourcePayment = '4';
								break;

							default:
								$sourcePayment = '0';
								break;
						}

						$array_data_insert = [
							$tempDetailInfo['currency'],
							$sourcePayment,
							$acct['MD_ACCT'],
							$tempDetailInfo['account_name'],
							'IDR',
							// round(floatval($acct['GUARANTEE_AMOUNT']), 2),
							round(floatval(str_replace(',', '', $data['requests']['amountmduse'][$key])), 2),
							'BG No. ' . $data['databg']['BG_NUMBER'],
							'Pooling Dana Klaim'
						];

						if (strtolower($tempDetailInfo['type']) == 't' || strtolower($tempDetailInfo['type']) == 's') {
							$array_data_insert[] = $acct['HOLD_SEQUENCE'];
							$array_data_insert[] = $acct['HOLD_BY_BRANCH'];
						}

						foreach ($array_insert as $key => $value) {

							$insertData = [
								'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
								'ACCT_INDEX' => $setIndex,
								'PS_FIELDNAME' => $value,
								'PS_FIELDVALUE' => $array_data_insert[$key],
							];

							$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
						}

						$setIndex++;
					}
				}
			}

			$insertData = [
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'ACCT_INDEX' => '9999',
				'PS_FIELDNAME' => 'Sumber Dana Pembayaran',
				'PS_FIELDVALUE' => $sumberDana,
			];

			$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
		}

		// SAVE FILE MEMO
		$attachmentDestination = UPLOAD_PATH . '/document/closing/'; // save file path

		$getSpecialFormatFile = $data['files']['dasar_penentuan'];
		$xplodeFile = explode('.', $getSpecialFormatFile['name']);
		$newName = date('dmy') . '_' . date('his') . '_' . uniqid('dasarpenentuan') . '_' . rtrim(substr($getSpecialFormatFile['name'], 0, 30), '.pdf') . '.' . array_pop($xplodeFile);
		$getTmpName = $getSpecialFormatFile['tmp_name'];

		$saveResult['specialFormatFile'] = $newName;

		move_uploaded_file($getTmpName, $attachmentDestination . $newName);

		// DELETE FILE MEMO
		$this->_db->delete('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
			'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
			'LOWER(FILE_NOTE) = ?' => 'memo / surat persetujuan klaim',
		]);

		// UPDATE FILE MEMO
		$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'FILE_NOTE' => 'Memo / Surat Persetujuan Klaim',
			'BG_FILE' => $newName
		]);

		// UPDATE T BG SLIP
		$this->_db->update('T_BG_PSLIP', [
			'PS_CONFIRMED' => new Zend_Db_Expr('now()'),
			'PS_CONFIRMEDBY' => $this->_userIdLogin,
			'PS_STATUS' => '3'

		], [
			'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
		]);

		// INSERT HISTORY
		$historyData = [
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'CUST_ID' => $data['CUST_ID'],
			'DATE_TIME' => new Zend_Db_Expr('now()'),
			'USER_FROM' => 'BANK',
			'USER_LOGIN' => $this->_userIdLogin,
			'BG_REASON' => 'NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'],
			'HISTORY_STATUS' => 15
		];
		$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyData);

		// SEND EMAIL
		$settings = new Settings();
		$allSetting = $settings->getAllSetting();

		$config = Zend_Registry::get('config');

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);

		$bgclosingStatus   = $config["bgclosinghistory"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosinghistory"]["status"]["code"];
		$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);

		$dataEmail = [
			'[[close_ref_number]]' => $data['databg']['CLOSE_REF_NUMBER'],
			'[[bg_number]]' => $data['databg']['BG_NUMBER'],
			'[[bg_subject]]' => $data['databg']["BG_SUBJECT"],
			'[[cust_name]]' => $data['databg']["CUST_NAME"],
			'[[recipient_name]]' => $data['databg']['RECIPIENT_NAME'],
			'[[counter_warranty_type]]' => $arrbgcg[$data['databg']['COUNTER_WARRANTY_TYPE']],
			'[[change_type]]' => $arrbgclosingType[$data['databg']['CHANGE_TYPE_CLOSE']],
			'[[suggestion_status]]' => $arrbgclosingStatus[15],
			'[[repair_notes]]' => $historyData['BG_REASON'],
			'[[recipient_name]]' => $data['databg']['RECIPIENT_NAME'],
		];

		$getEmailTemplate = $allSetting['bemailtemplate_proses_penutupan_klaim'];
		$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);

		$privi = $data['databg']['COUNTER_WARRANTY_TYPE'] == '1' ? ['PPCC'] : ['PPNC'];
		$emailAddresses = $this->getBuserEmailWithPrivi($privi, $data['databg']['BG_BRANCH']);

		// PERSIAPAN EMAIL
		// BEGIN SEND EMAIL
		$Settings = new Settings();
		$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
		$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
		$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
		$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
		$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
		$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
		$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
		$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
		$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
		$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
		$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
		$templateEmailMasterBankWapp = $Settings->getSetting('master_bank_wapp');
		$url_bo  = $Settings->getSetting('url_bo');
		//$template = $Settings->getSetting('femailtemplate_closebg');
		$email_grup_bts = $Settings->getSetting('email_grup_bts');
		$email_grup_cstd = $Settings->getSetting('email_grup_cstd');
		$email_grup_ficd = $Settings->getSetting('email_grup_ficd');

		// T BANK GUARANTEE UNDERLYING
		$getAllUnderlying = $this->_db->select()
			->from('T_BANK_GUARANTEE_UNDERLYING')
			->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
			->query()->fetchAll();

		$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

		$underlyingForEmail = '
			<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
				<br>
			<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>
			';

		// SEND EMAIL KE SME/BTS
		foreach ($emailAddresses as $email) {

			$template = $settings->getSettingNew('bemailtemplate_6A');

			$translate = array(
				'[[title_change_type]]' => 'Persetujuan Penyelesaian',
				'[[close_ref_number]]' => $data['databg']['CLOSE_REF_NUMBER'],
				'[[bg_number]]' => $data['databg']['BG_NUMBER'],
				'[[contract_name]]' => $underlyingForEmail,
				'[[usage_purpose]]' => $data['databg']['USAGE_PURPOSE_DESC'],
				'[[cust_name]]' => $data['databg']["CUST_NAME"],
				'[[recipient_name]]' => $data['databg']['RECIPIENT_NAME'],
				'[[bg_amount]]' => number_format($data['databg']['BG_AMOUNT,'], 2),
				'[[counter_warranty_type]]' => $arrbgcg[$data['databg']['COUNTER_WARRANTY_TYPE']],
				'[[change_type]]' => $arrbgclosingType[$data['databg']['CHANGE_TYPE_CLOSE']],
				'[[suggestion_status]]' => $arrbgclosingStatus[15],
				'[[repair_notes]]' => $historyData['BG_REASON'],
				'[[master_app_name]]' => $templateEmailMasterBankAppName,
				'[[master_bank_email]]' => $templateEmailMasterBankEmail,
				'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
				'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
			);


			$mailContent = strtr($template, $translate);
			$subject = 'NOTIFIKASI PROSES PERSETUJUAN PENYELESAIAN BANK GARANSI';

			//Application_Helper_Email::sendEmail($email['BUSER_EMAIL'], $subject, $mailContent);

			// $this->sendEmail($email['BUSER_EMAIL'], $this->language->_('Notifikasi Proses Penutupan / Klaim'), $getEmailTemplate);
		}

		// INSERT LOG
		$privLog = $data['databg']['COUNTER_WARRANTY_TYPE'] == '1' ? 'KPCC' : 'KPNC';
		Application_Helper_General::writeLog($privLog, 'Konfirmasi Penyelesaian Klaim untuk BG No : ' . $data['databg']['BG_NUMBER'] . ', Principal : ' . $data['databg']['CUST_ID'] . ', NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'] . '');

		// REDIRECT SUKSES
		$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
		$this->_redirect('/notification/success/index');
	}

	public function downloadmemoAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();

		$closeRefNumber = $this->_request->getParam('closerefnumber');
		$closeRefNumber = $AESMYSQL->decrypt(urldecode($closeRefNumber), uniqid());

		$detailBg = $this->_db->select()
			->from('T_BANK_GUARANTEE')
			->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->query()->fetch();

		$getMemoFile = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_FILE_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->where('LOWER(FILE_NOTE) = ?', 'memo / surat persetujuan klaim')
			->query()->fetch();

		if (!$getMemoFile) {
			$getMemoFile = $this->_db->select()
				->from('T_BANK_GUARANTEE_FILE_CLOSE')
				->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
				->where('LOWER(FILE_NOTE) = ?', 'memo / surat persetujuan klaim')
				->query()->fetch();
		}

		$attahmentDestination = UPLOAD_PATH . '/document/closing/';

		if (!file_exists($attahmentDestination . $getMemoFile['BG_FILE']) || !$getMemoFile) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'file tidak ditemukan']);
			return true;
		}

		$explodeNameFile = explode('_', $getMemoFile['BG_FILE']);
		$nameFile = str_replace([$explodeNameFile[0], $explodeNameFile[1], $explodeNameFile[2]], '', $getMemoFile['BG_FILE']);
		$nameFile = trim($nameFile, '_');

		// WRITE LOG DOWNLOAD
		$privilege = 'DPBG';
		$logAction = 'Unduh Detail Penyelesaian untuk BG No : ' . $detailBg['BG_NUMBER'] . ', Principal : ' . $detailBg['CUST_ID'] . ', NoRef Penyelesaian : ' . $closeRefNumber;
		Application_Helper_General::writeLog($privilege, $logAction);

		return $this->_helper->download->file(implode('_', [$closeRefNumber, 'memo', $nameFile]), $attahmentDestination . $getMemoFile['BG_FILE']);
	}

	private function repairKlaim($data)
	{
		// UPDATE T BG SLIP
		$this->_db->update('T_BG_PSLIP', [
			'PS_CONFIRMED' => new Zend_Db_Expr('now()'),
			'PS_CONFIRMEDBY' => $this->_userIdLogin,
			'PS_STATUS' => '2'

		], [
			'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
		]);

		// INSERT HISTORY
		$historyData = [
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'CUST_ID' => $data['CUST_ID'],
			'DATE_TIME' => new Zend_Db_Expr('now()'),
			'USER_FROM' => 'BANK',
			'USER_LOGIN' => $this->_userIdLogin,
			'BG_REASON' => $data['requests']['reason'],
			'HISTORY_STATUS' => 17
		];
		$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyData);

		// INSERT LOG
		$privLog = $data['databg']['COUNTER_WARRANTY_TYPE'] == '1' ? 'PPCC' : 'PPNC';
		Application_Helper_General::writeLog($privLog, 'Permintaan Perbaikan Konfirmasi Penyelesaian untuk BG No : ' . $data['databg']['BG_NUMBER'] . ', Principal : ' . $data['databg']['CUST_ID'] . ', NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'] . '');

		// REDIRECT SUKSES
		$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
		$this->_redirect('/notification/success/index');
	}

	private function repairRelease($data)
	{

		// LF
		if ($data['databg']['COUNTER_WARRANTY_TYPE'] == '2') {
			$getDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['ACCT_INDEX'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->group('ACCT_INDEX')
				->query()->fetchAll();

			$setIndex = count($getDetail) + 1;

			$sumberDana = $data['requests']['sumber_dana_type'] == '1' ? 1 : 5;

			$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
			$array_data_insert = [
				$data['requests']['lfsumber_acct_currency'],
				$sumberDana,
				$data['requests']['lfsumber_acct_number'],
				$data['requests']['lfsumber_acct_name'],
				'IDR',
				$data['databg']['BG_AMOUNT'],
				'BG No. ' . $data['databg']['BG_NUMBER'],
				'Pooling Dana Klaim'
			];

			foreach ($array_insert as $key => $value) {

				$updateData = [
					'PS_FIELDVALUE' => $array_data_insert[$key],
				];

				$this->_db->update('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $updateData, [
					'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
					'PS_FIELDNAME = ?' => $value
				]);
			}
		}

		if ($data['databg']['COUNTER_WARRANTY_TYPE'] == '3') {

			$this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
				'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
				'ACCT_INDEX != ?' => '1'
			]);

			$getDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['ACCT_INDEX'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->group('ACCT_INDEX')
				->query()->fetchAll();

			$dataListAccount = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->query()->fetchAll();
			$dataListAccount = array_combine(array_column($dataListAccount, 'PS_FIELDNAME'), array_column($dataListAccount, 'PS_FIELDVALUE'));

			// CHECK MD PRINCIPAL
			$checkMdPrincipal = $this->_db->select()
				->from('T_BANK_GUARANTEE_SPLIT')
				->where('BG_NUMBER = ?', $data['databg']['BG_NUMBER'])
				->query()->fetchAll();

			$bgdetaildata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
				->query()->fetchAll();

			$bgdetaildata = array_combine(array_map('strtolower', array_column($bgdetaildata, 'PS_FIELDNAME')), array_column($bgdetaildata, 'PS_FIELDVALUE'));

			$totalMdPrincipal = ($checkMdPrincipal) ? array_sum(array_column($checkMdPrincipal, 'AMOUNT')) : 0;

			$totalMdPrincipal = $totalMdPrincipal > 0 ? ($totalMdPrincipal * floatval($bgdetaildata['marginal deposit pecentage']) / 100) : $totalMdPrincipal;

			$setIndex = count($getDetail) + 1;

			$sumberDana = $data['requests']['sumber_dana_type']; // 1 = rekening asuransi, 2 = md asuransi

			// REKENING ASURANSI
			if ($sumberDana == '1') {
				// $mustTransfer = round(floatval($dataListAccount['Transaction Amount']), 2) - round(floatval($totalMdPrincipal), 2);
				$mustTransfer = round(floatval($dataListAccount['Transaction Amount']), 2);

				$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
				$array_data_insert = [
					$data['requests']['lfsumber_acct_currency'],
					'1',
					$data['requests']['lfsumber_acct_number'],
					$data['requests']['lfsumber_acct_name'],
					'IDR',
					$mustTransfer,
					'BG No. ' . $data['databg']['BG_NUMBER'],
					'Pooling Dana Klaim'
				];

				foreach ($array_insert as $key => $value) {

					$insertData = [
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'ACCT_INDEX' => $setIndex,
						'PS_FIELDNAME' => $value,
						'PS_FIELDVALUE' => $array_data_insert[$key],
					];

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
				}
			}

			// MD ASURANSI
			else {
				$needKMK = $data['requests']['need_kmk'];

				// BUTUH KMK
				if ($needKMK) {
					// CHECK MD PRINCIPAL
					$checkMdIns = $this->_db->select()
						->from('M_MARGINALDEPOSIT_DETAIL')
						->where('CUST_ID = ?', $data['databg']['BG_INSURANCE_CODE'])
						->query()->fetchAll();

					$totalMdIns = ($checkMdIns) ? array_sum(array_column($checkMdIns, 'GUARANTEE_AMOUNT')) : 0;

					// $mustTransfer = round(floatval($dataListAccount['Transaction Amount']), 2) - round(floatval($totalMdPrincipal), 2) - round(floatval($totalMdIns));
					$mustTransfer = round(floatval($dataListAccount['Transaction Amount']), 2) - round(floatval($totalMdIns));

					$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
					$array_data_insert = [
						$data['requests']['kmkasuransi_acct_currency'],
						'5',
						$data['requests']['kmkasuransi_acct_number'],
						$data['requests']['kmkasuransi_acct_name'],
						'IDR',
						$mustTransfer,
						'BG No. ' . $data['databg']['BG_NUMBER'],
						'Pooling Dana Klaim'
					];

					foreach ($array_insert as $key => $value) {

						$insertData = [
							'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
							'ACCT_INDEX' => $setIndex,
							'PS_FIELDNAME' => $value,
							'PS_FIELDVALUE' => $array_data_insert[$key],
						];

						$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
					}

					$setIndex++;

					foreach ($checkMdIns as $key => $acct) {
						$tempDetailInfo = $this->getDetailInfoAcct($acct['MD_ACCT']);

						$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
						if (strtolower($tempDetailInfo['type']) == 't' || strtolower($tempDetailInfo['type']) == 's') {
							$array_insert[] = 'Hold Sequence';
							$array_insert[] = 'Hold By Branch';
						}

						switch (strtolower($tempDetailInfo['type'])) {
							case 'd':
								$sourcePayment = '2';
								break;

							case 't':
								$sourcePayment = '3';
								break;

							case 's':
								$sourcePayment = '4';
								break;

							default:
								$sourcePayment = '0';
								break;
						}

						$array_data_insert = [
							$tempDetailInfo['currency'],
							$sourcePayment,
							$acct['MD_ACCT'],
							$tempDetailInfo['account_name'],
							'IDR',
							// round(floatval($acct['GUARANTEE_AMOUNT']), 2),
							round(floatval(str_replace(',', '', $data['requests']['amountmduse'][$key])), 2),
							'BG No. ' . $data['databg']['BG_NUMBER'],
							'Pooling Dana Klaim'
						];

						if (strtolower($tempDetailInfo['type']) == 't' || strtolower($tempDetailInfo['type']) == 's') {
							$array_data_insert[] = $acct['HOLD_SEQUENCE'];
							$array_data_insert[] = $acct['HOLD_BY_BRANCH'];
						}

						foreach ($array_insert as $key => $value) {

							$insertData = [
								'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
								'ACCT_INDEX' => $setIndex,
								'PS_FIELDNAME' => $value,
								'PS_FIELDVALUE' => $array_data_insert[$key],
							];

							$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
						}

						$setIndex++;
					}
				}

				// TIDAK BUTUH KMK
				else {
					// CHECK MD PRINCIPAL
					$checkMdIns = $this->_db->select()
						->from('M_MARGINALDEPOSIT_DETAIL')
						->where('CUST_ID = ?', $data['databg']['BG_INSURANCE_CODE'])
						->query()->fetchAll();

					foreach ($data['requests']['indexacctmd'] as $key => $index) {
						$tempAcctIndex = $data['requests']['indexacct'][$key];

						$acct = array_shift(array_filter($checkMdIns, function ($item) use ($tempAcctIndex) {
							return ltrim($item['MD_ACCT'], '0') == ltrim($tempAcctIndex, '0');
						}));

						$tempDetailInfo = $this->getDetailInfoAcct($acct['MD_ACCT']);

						$array_insert = ['Source Account CCY', 'Source of Payment', 'Source Account Number', 'Source Account Name', 'Transaction CCY', 'Transaction Amount', 'Remark 1', 'Remark 2'];
						if (strtolower($tempDetailInfo['type']) == 't' || strtolower($tempDetailInfo['type']) == 's') {
							$array_insert[] = 'Hold Sequence';
							$array_insert[] = 'Hold By Branch';
						}

						switch (strtolower($tempDetailInfo['type'])) {
							case 'd':
								$sourcePayment = '2';
								break;

							case 't':
								$sourcePayment = '3';
								break;

							case 's':
								$sourcePayment = '4';
								break;

							default:
								$sourcePayment = '0';
								break;
						}

						$array_data_insert = [
							$tempDetailInfo['currency'],
							$sourcePayment,
							$acct['MD_ACCT'],
							$tempDetailInfo['account_name'],
							'IDR',
							// round(floatval($acct['GUARANTEE_AMOUNT']), 2),
							round(floatval(str_replace(',', '', $data['requests']['amountmduse'][$key])), 2),
							'BG No. ' . $data['databg']['BG_NUMBER'],
							'Pooling Dana Klaim'
						];

						if (strtolower($tempDetailInfo['type']) == 't' || strtolower($tempDetailInfo['type']) == 's') {
							$array_data_insert[] = $acct['HOLD_SEQUENCE'];
							$array_data_insert[] = $acct['HOLD_BY_BRANCH'];
						}

						foreach ($array_insert as $key => $value) {

							$insertData = [
								'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
								'ACCT_INDEX' => $setIndex,
								'PS_FIELDNAME' => $value,
								'PS_FIELDVALUE' => $array_data_insert[$key],
							];

							$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
						}

						$setIndex++;
					}
				}
			}

			$insertData = [
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'ACCT_INDEX' => '9999',
				'PS_FIELDNAME' => 'Sumber Dana Pembayaran',
				'PS_FIELDVALUE' => $sumberDana,
			];

			$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
		}

		// SAVE REK PEMBEBANAN BIAYA JIKA ADA
		if ($data['requests']['beban_acct_name']) {
			$getDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['ACCT_INDEX'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->group('ACCT_INDEX')
				->query()->fetchAll();

			$getDetail_second = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['ACCT_INDEX'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('LOWER(PS_FIELDNAME) LIKE ?', '%charge%')
				->query()->fetch();

			if ($getDetail_second) {
				$this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
					'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
					'ACCT_INDEX = ?' => $getDetail_second['ACCT_INDEX']
				]);
			}

			$setIndex = count($getDetail) + 1;

			$array_insert = ['Claim Transfer Type', 'Source of Payment', 'Charge Account CCY', 'Charge Account Number', 'Charge Account Name'];
			$array_data_insert = [$data['requests']['transferType'], 6, $data['requests']['beban_acct_currency'], $data['requests']['beban_acct_number'], $data['requests']['beban_acct_name']];

			foreach ($array_insert as $key => $value) {
				if ($data['bankcode'] != 200 && strtolower($value) == 'claim transfer type') {
					$insertData = [
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'ACCT_INDEX' => $setIndex,
						'PS_FIELDNAME' => $value,
						'PS_FIELDVALUE' => $array_data_insert[$key],
					];
				} else {
					if (strtolower($value) == 'claim transfer type') continue;
					$insertData = [
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'ACCT_INDEX' => $setIndex,
						'PS_FIELDNAME' => $value,
						'PS_FIELDVALUE' => $array_data_insert[$key],
					];
				}
				$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', $insertData);
			}
		}

		// SAVE FILE MEMO
		// JIKA ADA PERUBAHAN FILE
		if ($data['files']['dasar_penentuan']['size'] > 0) {
			$attachmentDestination = UPLOAD_PATH . '/document/closing/'; // save file path

			$getSpecialFormatFile = $data['files']['dasar_penentuan'];
			$xplodeFile = explode('.', $getSpecialFormatFile['name']);
			$newName = date('dmy') . '_' . date('his') . '_' . uniqid('dasarpenentuan') . '_' . rtrim(substr($getSpecialFormatFile['name'], 0, 30), '.pdf') . '.' . array_pop($xplodeFile);
			$getTmpName = $getSpecialFormatFile['tmp_name'];

			$saveResult['specialFormatFile'] = $newName;

			move_uploaded_file($getTmpName, $attachmentDestination . $newName);

			// UPDATE FILE MEMO
			$this->_db->update('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
				'BG_FILE' => $newName
			], [
				'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
				'LOWER(FILE_NOTE) = ?' => 'memo / surat persetujuan klaim'
			]);
		}

		// UPDATE T BG SLIP
		$this->_db->update('T_BG_PSLIP', [
			'PS_CONFIRMED' => new Zend_Db_Expr('now()'),
			'PS_CONFIRMEDBY' => $this->_userIdLogin,
			'PS_STATUS' => '3'

		], [
			'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
		]);

		// INSERT HISTORY
		$historyData = [
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'CUST_ID' => $data['CUST_ID'],
			'DATE_TIME' => new Zend_Db_Expr('now()'),
			'USER_FROM' => 'BANK',
			'USER_LOGIN' => $this->_userIdLogin,
			'BG_REASON' => 'NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'],
			'HISTORY_STATUS' => 18
		];
		$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyData);

		// INSERT LOG
		$privLog = $data['databg']['COUNTER_WARRANTY_TYPE'] == '1' ? 'KPCC' : 'KPNC';
		Application_Helper_General::writeLog($privLog, 'Perbaikan Konfirmasi Penyelesaian Klaim untuk BG No : ' . $data['databg']['BG_NUMBER'] . ', Principal : ' . $data['databg']['CUST_ID'] . ', NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'] . '');

		// REDIRECT SUKSES
		$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
		$this->_redirect('/notification/success/index');
	}

	private function approveRelease($data)
	{
		// CEK KONTRA
		$counterWarrantyType = $data['databg']['COUNTER_WARRANTY_TYPE'];

		// KONTRA CASH COL
		if ($counterWarrantyType == 1) {
			$this->cashColRelease($data);
		}

		// KONTRA LINE FACILITY
		if ($counterWarrantyType == 2) {
			$this->lineFcRelease($data);
		}

		// KONTRA ASURANSI
		if ($counterWarrantyType == 3) {
			$this->insuranceRelease($data);
		}
	}

	private function cashColRelease($data)
	{
		// // TRANSFER BACK
		// $transferData = [
		// 	"SOURCE_ACCOUNT_NUMBER" => '1601306666109',
		// 	"SOURCE_ACCOUNT_NAME" => 'ANASABAH AE30481 LAL',
		// 	"BENEFICIARY_ACCOUNT_NUMBER" => '16101320000532',
		// 	"BENEFICIARY_ACCOUNT_NAME" => 'ANASABAH AE30481 LAL',
		// 	"PHONE_NUMBER_FROM" => '081299997777',
		// 	"AMOUNT" => 10000000.00,
		// 	"PHONE_NUMBER_TO" => '081299997777',
		// 	"RATE1" => "10000000",
		// 	"RATE2" => "10000000",
		// 	"RATE3" => "10000000",
		// 	"RATE4" => "10000000",
		// 	"DESCRIPTION" => 'tt',
		// 	"DESCRIPTION_DETAIL" => 'tt',
		// ];

		// $transferSavingService =  new Service_TransferWithin($transferData);
		// $result = $transferSavingService->sendTransfer();
		// $resultTransferSaving = $result["RawResult"];

		// Zend_Debug::dump($resultTransferSaving);
		// die();

		// INTITIALIZE SERVICE CHECK
		$serviceCheck = true;

		// CEK CREATE OPEN ACCOUNT ON T BG TRANSACTION
		$cekOA = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('SERVICE = ?', '4')
			->query()->fetch();

		$seqNumber = 1;

		// JIKA REKENING PENAMPUNG BELUM ADA
		if (!$cekOA) {
			// BUAT REPPK / REKENING PENAMPUNG
			$clientUser = new SGO_Soap_ClientUser();
			$requestopen = array();
			$requestopen['cif'] = $data['databg']["CUST_CIF"];
			$requestopen['branch_code'] = $data['databg']['BG_BRANCH'];
			$requestopen['product_code'] = 'K2';
			$requestopen['currency'] = 'IDR';

			$clientUser->callapi('createaccount', null, $requestopen);
			$reppkResult = $clientUser->getResult();

			// PARSE DATE TIME
			$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $reppkResult['datetime']);
			$traTime = $parseDateTime->format('Y-m-d H:i:s');

			$serviceCheck = ($reppkResult['response_code'] == '0000') ? $serviceCheck : false;

			$reppkAcct = ($reppkResult['response_code'] == '0000') ? $reppkResult['account_number'] : false;

			$reppkDetailService = new Service_Account($reppkAcct, '');
			$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

			// INSERT TO T BG TRANSACTION
			$insertData = [
				'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'BG_NUMBER' => $data['databg']['BG_NUMBER'],
				'SERVICE' => 4, // OPEN ACCOUNT
				'SOURCE_ACCT' => ($reppkAcct) ?: 'XXX',
				'SOURCE_ACCT_CCY' => 'IDR',
				'SOURCE_ACCT_NAME' => $reppkDetailAcct['account_name'],
				'CHARGE_ACCT' => '',
				'BENEF_SWIFT_CODE' => '',
				'BENEF_ACCT' => '',
				'TRA_CCY' => 'IDR',
				'TRA_AMOUNT' => '',
				'CHARGE_AMOUNT' => '',
				'REMARKS1' => '',
				'REMARKS2' => '',
				'SOURCE_ADDRESS' => '',
				'SOURCE_PHONE' => '',
				'BENEF_ADDRESS' => '',
				'TRA_TIME' => $traTime,
				'TRA_STATUS' => ($reppkAcct) ? 1 : 4,
				'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				'UPDATE_APPROVEDBY' => $this->_userIdLogin,
				'UUID' => $reppkResult['uuid'],
				'RESPONSE_CODE' => $reppkResult['response_code'],
				'RESPONSE_DESC' => $reppkResult['response_desc'],
				'TRA_REF' => $reppkResult['ref']
			];

			$this->_db->insert('T_BG_TRANSACTION', $insertData);
		}
		// JIKA REKENING PENAMPUNG SUDAH ADA
		else {
			$reppkAcct = $cekOA['SOURCE_ACCT'];
		}

		// GET MD
		$getMd = $this->_db->select()
			->from('T_BANK_GUARANTEE_SPLIT')
			->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
			->query()->fetchAll();

		// CEK APAKAH ADA ESCROW
		$getEscrow = array_filter($getMd, function ($item) {
			return strtolower($item['ACCT_DESC']) == 'escrow';
		});

		if ($getEscrow) {
			$escrowKey = ($getEscrow) ? array_keys($getEscrow)[0] : false;
			$getEscrow = array_shift($getEscrow);
			unset($getMd[$escrowKey]);
		}

		foreach ($getMd as $key => $acct) {
			# code...
			$callService = new Service_Account($acct['ACCT'], '');
			$acctInfo = $callService->inquiryAccountBalance();

			if (strtolower($acctInfo['account_type']) == 'd') {
				unset($getMd[$key]);
				continue;
			}

			// TABUNGAN
			if (strtolower($acctInfo['account_type']) == 's') {

				// UNLOCK TABUNGAN ---------------------------------------------------------
				// $unlockSavingService = new Service_Account($acctInfo['account_number'], '');
				// $unlockTabungan = $unlockSavingService->unlockSaving([
				// 	'branch_code' => $acct['HOLD_BY_BRANCH'],
				// 	'sequence_number' => $acct['HOLD_SEQUENCE']
				// ]);

				// $serviceCheck = ($unlockTabungan['response_code'] == '0000') ? $serviceCheck : false;

				// // PARSE DATE TIME
				// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockTabungan['datetime']);
				// $traTime = $parseDateTime->format('Y-m-d H:i:s');

				// switch (true) {
				// 	case $unlockTabungan['response_code'] == '0000':
				// 		$traStatus = 1;
				// 		break;
				// 	case $unlockTabungan['response_code'] == 'XT' || $unlockTabungan['response_code'] == '0012' || $unlockTabungan['response_code'] == '9999':
				// 		$traStatus = 3;
				// 		break;
				// 	case $unlockTabungan['response_code'] == '1111':
				// 		$traStatus = 5;
				// 		break;
				// 	default:
				// 		$traStatus = 4;
				// 		break;
				// }

				$tempDetailInfo = $this->getDetailInfoAcct($acctInfo['account_number']);

				// INSERT TO T BG TRANSACTION
				$insertData = [
					'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
					'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
					'BG_NUMBER' => $data['databg']['BG_NUMBER'],
					'SERVICE' => 3, // UNLOCK SAVING
					'SOURCE_ACCT' => $acct['ACCT'],
					'SOURCE_ACCT_CCY' => $tempDetailInfo['currency'],
					// 'SOURCE_ACCT_NAME' => $tempDetailInfo['account_name'],
					'SOURCE_ACCT_NAME' => $acct['NAME'],
					'CHARGE_ACCT' => '',
					'BENEF_SWIFT_CODE' => '',
					'BENEF_ACCT' => '',
					'HOLD_BY_BRANCH' => $acct['HOLD_BY_BRANCH'],
					'HOLD_SEQUENCE' => $acct['HOLD_SEQUENCE'],
					'TRA_CCY' => 'IDR',
					'TRA_AMOUNT' => $acct['AMOUNT'],
					'CHARGE_AMOUNT' => '',
					'REMARKS1' => '',
					'REMARKS2' => '',
					'SOURCE_ADDRESS' => '',
					'SOURCE_PHONE' => '',
					'BENEF_ADDRESS' => '',
					// 'TRA_TIME' => $traTime,
					'TRA_TIME' => '',
					// 'TRA_STATUS' => $traStatus,
					'TRA_STATUS' => '6',
					'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
					'UPDATE_APPROVEDBY' => $this->_userIdLogin,
					// 'UUID' => $unlockTabungan['uuid'],
					'UUID' => '',
					// 'RESPONSE_CODE' => $unlockTabungan['response_code'],
					'RESPONSE_CODE' => '',
					// 'RESPONSE_DESC' => $unlockTabungan['response_desc'],
					'RESPONSE_DESC' => '',
					// 'TRA_REF' => $unlockTabungan['ref']
					'TRA_REF' => ''
				];

				$this->_db->insert('T_BG_TRANSACTION', $insertData);

				if ($insertData['TRA_STATUS'] == 3) {
					$maxTry = 3;
					$counterTry = 1;
					while (true) {
						$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

						// JIKA SUDAH DICOBA SEBANYAK 3 KALI
						if ($counterTry == $maxTry || $checkTrxTimeout) break;

						$counterTry++;
						sleep(2);
					}

					if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
						$this->runTransaction($insertData);
					}
				}

				// END UNLOCK TABUNGAN ---------------------------------------------------------

				// TRANSFER TABUNGAN KE REKENING REPPK -------------------------------------------

				$detailAcctSaving = $callService->inquiryAccontInfo();
				$reppkDetailService = new Service_Account($reppkAcct, '');
				$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

				$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

				// $transferData = [
				// 	"SOURCE_ACCOUNT_NUMBER" => $acct['ACCT'],
				// 	"SOURCE_ACCOUNT_NAME" => $detailAcctSaving['account_name'],
				// 	"BENEFICIARY_ACCOUNT_NUMBER" => $reppkAcct ?: 'XXX',
				// 	"BENEFICIARY_ACCOUNT_NAME" => $reppkDetailAcct['account_name'],
				// 	"PHONE_NUMBER_FROM" => $detailAcctSaving['phone_number'] ?: '000',
				// 	"AMOUNT" => round(floatval($acct["AMOUNT"]), 2),
				// 	"PHONE_NUMBER_TO" => $reppkDetailAcct['phone_number'] ?: '000',
				// 	"RATE1" => "10000000",
				// 	"RATE2" => "10000000",
				// 	"RATE3" => "10000000",
				// 	"RATE4" => "10000000",
				// 	// "DESCRIPTION" => substr(strval($traId), 0, 40),
				// 	"DESCRIPTION" => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
				// 	"DESCRIPTION_DETAIL" => substr($data['databg']["BG_NUMBER"], 0, 40),
				// ];

				// if ($traStatus == 1) {
				// 	$transferSavingService =  new Service_TransferWithin($transferData);
				// 	$result = $transferSavingService->sendTransfer();
				// 	$resultTransferSaving = $result["RawResult"];
				// } else {
				// 	$resultTransferSaving['response_code'] = '1111';
				// 	$resultTransferSaving['response_desc'] = 'unhold tabungan gagal';
				// 	$resultTransferSaving['datetime'] = '0000-00-00T00:00:00+00:00';
				// 	$resultTransferSaving['uuid'] = '-';
				// 	$resultTransferSaving['ref'] = '-';
				// }


				// $serviceCheck = ($resultTransferSaving['response_code'] == '0000') ? $serviceCheck : false;

				// // PARSE DATE TIME
				// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSaving['datetime']);
				// $traTime = $parseDateTime->format('Y-m-d H:i:s');

				// switch (true) {
				// 	case $resultTransferSaving['response_code'] == '0000':
				// 		$traStatus = 1;
				// 		break;
				// 	case $resultTransferSaving['response_code'] == 'XT' || $resultTransferSaving['response_code'] == '0012' || $resultTransferSaving['response_code'] == '9999':
				// 		$traStatus = 3;
				// 		break;
				// 	case $resultTransferSaving['response_code'] == '1111':
				// 		$traStatus = 5;
				// 		break;
				// 	default:
				// 		$traStatus = 4;
				// 		break;
				// }

				// INSERT TO T BG TRANSACTION
				$insertData = [
					'TRANSACTION_ID' => $traId,
					'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
					'BG_NUMBER' => $data['databg']['BG_NUMBER'],
					'SERVICE' => 1, // TRANSFER BTN
					'SOURCE_ACCT' => $acct['ACCT'],
					'SOURCE_ACCT_NAME' => $acct['NAME'],
					'SOURCE_ACCT_CCY' => $acct['CCY_ID'],
					'CHARGE_ACCT' => '',
					'BENEF_SWIFT_CODE' => '',
					'BENEF_ACCT' => $reppkAcct ?: 'XXX',
					'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
					'TRA_CCY' => 'IDR',
					'TRA_AMOUNT' => $acct["AMOUNT"],
					'CHARGE_AMOUNT' => '',
					'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
					'REMARKS2' => '',
					'SOURCE_ADDRESS' => '',
					'SOURCE_PHONE' => $detailAcctSaving['phone_number'] ?: '000',
					'BENEF_ADDRESS' => '',
					'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
					// 'TRA_TIME' => $traTime,
					'TRA_TIME' => '',
					// 'TRA_STATUS' => $traStatus,
					'TRA_STATUS' => '6',
					'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
					'UPDATE_APPROVEDBY' => $this->_userIdLogin,
					// 'UUID' => $resultTransferSaving['uuid'],
					'UUID' => '',
					// 'RESPONSE_CODE' => $resultTransferSaving['response_code'],
					'RESPONSE_CODE' => '',
					// 'RESPONSE_DESC' => $resultTransferSaving['response_desc'],
					'RESPONSE_DESC' => '',
					// 'TRA_REF' => $resultTransferSaving['ref']
					'TRA_REF' => ''
				];

				$this->_db->insert('T_BG_TRANSACTION', $insertData);

				if ($insertData['TRA_STATUS'] == 3) {
					$maxTry = 3;
					$counterTry = 1;
					while (true) {
						$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

						// JIKA SUDAH DICOBA SEBANYAK 3 KALI
						if ($counterTry == $maxTry || $checkTrxTimeout) break;

						$counterTry++;
						sleep(2);
					}

					if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
						$this->runTransaction($insertData);
					}
				}

				continue;
			}

			$acctInfo = $callService->inquiryDeposito();

			// DEPOSITO
			if (strtolower($acctInfo['account_type']) == 't') {
				// UNLOCK DEPOSITO ---------------------------------------------------------
				// $unlockDepositoService = new Service_Account($acctInfo['account_number'], '');
				// $unlockDeposito = $unlockDepositoService->unlockDeposito('T', $acct['HOLD_BY_BRANCH'], $acct['HOLD_SEQUENCE']);

				// $serviceCheck = ($unlockDeposito['response_code'] == '0000') ? $serviceCheck : false;

				// // PARSE DATE TIME
				// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockDeposito['datetime']);
				// $traTime = $parseDateTime->format('Y-m-d H:i:s');

				// switch (true) {
				// 	case $unlockDeposito['response_code'] == '0000':
				// 		$traStatus = 1;
				// 		break;
				// 	case $unlockDeposito['response_code'] == 'XT' || $unlockDeposito['response_code'] == '0012' || $unlockDeposito['response_code'] == '9999':
				// 		$traStatus = 3;
				// 		break;
				// 	case $unlockDeposito['response_code'] == '1111':
				// 		$traStatus = 5;
				// 		break;
				// 	default:
				// 		$traStatus = 4;
				// 		break;
				// }

				$tempDetailInfo = $this->getDetailInfoAcct($acctInfo['account_number']);

				// INSERT TO T BG TRANSACTION
				$insertData = [
					'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
					'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
					'BG_NUMBER' => $data['databg']['BG_NUMBER'],
					'SERVICE' => 2, // UNLOCK DEPOSITO
					'SOURCE_ACCT' => $acct['ACCT'],
					'CHARGE_ACCT' => '',
					'SOURCE_ACCT_CCY' => $tempDetailInfo['currency'],
					'SOURCE_ACCT_NAME' => $acct['NAME'],
					'BENEF_SWIFT_CODE' => '',
					'BENEF_ACCT' => '',
					'HOLD_BY_BRANCH' => $acct['HOLD_BY_BRANCH'],
					'HOLD_SEQUENCE' => $acct['HOLD_SEQUENCE'],
					'TRA_CCY' => 'IDR',
					'TRA_AMOUNT' => $acct['AMOUNT'],
					'CHARGE_AMOUNT' => '',
					'REMARKS1' => '',
					'REMARKS2' => '',
					'SOURCE_ADDRESS' => '',
					'SOURCE_PHONE' => '',
					'BENEF_ADDRESS' => '',
					// 'TRA_TIME' => $traTime,
					'TRA_TIME' => '',
					// 'TRA_STATUS' => $traStatus,
					'TRA_STATUS' => '6',
					'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
					'UPDATE_APPROVEDBY' => $this->_userIdLogin,
					// 'UUID' => $unlockDeposito['uuid'],
					'UUID' => '',
					// 'RESPONSE_CODE' => $unlockDeposito['response_code'],
					'RESPONSE_CODE' => '',
					// 'RESPONSE_DESC' => $unlockDeposito['response_desc'],
					'RESPONSE_DESC' => '',
					// 'TRA_REF' => $unlockDeposito['ref']
					'TRA_REF' => ''
				];

				$this->_db->insert('T_BG_TRANSACTION', $insertData);

				if ($insertData['TRA_STATUS'] == 3) {
					$maxTry = 3;
					$counterTry = 1;
					while (true) {
						$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

						// JIKA SUDAH DICOBA SEBANYAK 3 KALI
						if ($counterTry == $maxTry || $checkTrxTimeout) break;

						$counterTry++;
						sleep(2);
					}

					if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
						$this->runTransaction($insertData);
					}
				}

				// END UNLOCK DEPOSITO ---------------------------------------------------------

				// TRANSFER DEPOSITO KE REKENING REPPK -------------------------------------------

				$stateTransfer = true;

				$detailAcctDeposito = $callService->inquiryAccontInfo();
				$reppkDetailService = new Service_Account($reppkAcct, '');
				$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

				// $stateTransfer = $reppkAcct ? true : false;
				// $stateTransfer = ($reppkDetailAcct['response_code'] == '0000') ? true : false;

				$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

				// if ($traStatus == 1) {
				// 	$depositoDisrburseService =  new Service_Account($acct['ACCT'], '');
				// 	$resultDepoDisburse = $depositoDisrburseService->depositoDisbursement($acct['CCY_ID'], $acct['ACCT'], $detailAcctDeposito['account_name'], 'IDR', $reppkAcct, $reppkDetailAcct['account_name'], $acct['AMOUNT'], '10000000', '10000000', '10000000', '10000000', strval('Klaim BG No ' . $data['databg']['BG_NUMBER']), strval($data['databg']['BG_NUMBER']));
				// } else {
				// 	$resultDepoDisburse['response_code'] = '1111';
				// 	$resultDepoDisburse['response_desc'] = 'unhold tabungan gagal';
				// 	$resultDepoDisburse['datetime'] = '0000-00-00T00:00:00+00:00';
				// 	$resultDepoDisburse['uuid'] = '-';
				// 	$resultDepoDisburse['ref'] = '-';
				// }

				// $serviceCheck = ($resultDepoDisburse['response_code'] == '0000') ? $serviceCheck : false;

				// // PARSE DATE TIME
				// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultDepoDisburse['datetime']);
				// $traTime = $parseDateTime->format('Y-m-d H:i:s');

				// switch (true) {
				// 	case $resultDepoDisburse['response_code'] == '0000':
				// 		$traStatus = 1;
				// 		break;
				// 	case $resultDepoDisburse['response_code'] == 'XT' || $resultDepoDisburse['response_code'] == '0012' || $resultDepoDisburse['response_code'] == '9999':
				// 		$traStatus = 3;
				// 		break;
				// 	case $resultDepoDisburse['response_code'] == '1111':
				// 		$traStatus = 5;
				// 		break;
				// 	default:
				// 		$traStatus = 4;
				// 		break;
				// }

				// INSERT TO T BG TRANSACTION
				$insertData = [
					'TRANSACTION_ID' => $traId,
					'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
					'BG_NUMBER' => $data['databg']['BG_NUMBER'],
					'SERVICE' => 5, // TRANSFER BTN
					'SOURCE_ACCT' => $acct['ACCT'],
					'SOURCE_ACCT_CCY' => $acct['CCY_ID'],
					'SOURCE_ACCT_NAME' => $acct['NAME'],
					'CHARGE_ACCT' => '',
					'BENEF_SWIFT_CODE' => '',
					'BENEF_ACCT' => $reppkAcct ?: 'XXX',
					'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
					'TRA_CCY' => 'IDR',
					'TRA_AMOUNT' => $acct["AMOUNT"],
					'CHARGE_AMOUNT' => '',
					'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
					'REMARKS2' => '',
					'SOURCE_ADDRESS' => '',
					'SOURCE_PHONE' => $detailAcctDeposito['phone_number'] ?: '000',
					'BENEF_ADDRESS' => '',
					'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
					// 'TRA_TIME' => $traTime,
					'TRA_TIME' => '',
					// 'TRA_STATUS' => $traStatus,
					'TRA_STATUS' => '6',
					'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
					'UPDATE_APPROVEDBY' => $this->_userIdLogin,
					// 'UUID' => $resultDepoDisburse['uuid'],
					'UUID' => '',
					// 'RESPONSE_CODE' => $resultDepoDisburse['response_code'],
					'RESPONSE_CODE' => '',
					// 'RESPONSE_DESC' => $resultDepoDisburse['response_desc'],
					'RESPONSE_DESC' => '',
					// 'TRA_REF' => $resultDepoDisburse['ref']
					'TRA_REF' => ''
				];

				$this->_db->insert('T_BG_TRANSACTION', $insertData);

				if ($insertData['TRA_STATUS'] == 3) {
					$maxTry = 3;
					$counterTry = 1;
					while (true) {
						$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

						// JIKA SUDAH DICOBA SEBANYAK 3 KALI
						if ($counterTry == $maxTry || $checkTrxTimeout) break;

						$counterTry++;
						sleep(2);
					}

					if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
						$this->runTransaction($insertData);
					}
				}

				continue;
			}
		}

		// TRF ESCROW TO REPPK if EXIST
		if ($getEscrow) {
			// TRANSFER ESCROW KE REKENING REPPK -------------------------------------------

			$detailAcctEscrowService = new Service_Account($getEscrow['ACCT'], '');
			$detailAcctEscrow = $detailAcctEscrowService->inquiryAccontInfo();
			$reppkDetailService = new Service_Account($reppkAcct, '');
			$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

			$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

			// $transferData = [
			// 	"SOURCE_ACCOUNT_NUMBER" => $getEscrow['ACCT'],
			// 	"SOURCE_ACCOUNT_NAME" => $detailAcctEscrow['account_name'],
			// 	"BENEFICIARY_ACCOUNT_NUMBER" => $reppkAcct ?: 'XXX',
			// 	"BENEFICIARY_ACCOUNT_NAME" => $reppkDetailAcct['account_name'],
			// 	"PHONE_NUMBER_FROM" => $detailAcctEscrow['phone_number'] ?: '000',
			// 	"AMOUNT" => round(floatval($getEscrow["AMOUNT"]), 2),
			// 	"PHONE_NUMBER_TO" => $reppkDetailAcct['phone_number'] ?: '000',
			// 	"RATE1" => "10000000",
			// 	"RATE2" => "10000000",
			// 	"RATE3" => "10000000",
			// 	"RATE4" => "10000000",
			// 	// "DESCRIPTION" => strval($traId),
			// 	"DESCRIPTION" => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
			// 	"DESCRIPTION_DETAIL" => substr($data['databg']["BG_NUMBER"], 0, 40),
			// ];

			// $transferSavingService =  new Service_TransferWithin($transferData);
			// $result = $transferSavingService->sendTransfer();
			// $resultTransferSaving = $result["RawResult"];

			// $serviceCheck = ($resultTransferSaving['response_code'] == '0000') ? $serviceCheck : false;

			// PARSE DATE TIME
			// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSaving['datetime']);
			// $traTime = $parseDateTime->format('Y-m-d H:i:s');

			// switch (true) {
			// 	case $resultTransferSaving['response_code'] == '0000':
			// 		$traStatus = 1;
			// 		break;
			// 	case $resultTransferSaving['response_code'] == 'XT' || $resultTransferSaving['response_code'] == '0012' || $resultTransferSaving['response_code'] == '9999':
			// 		$traStatus = 3;
			// 		break;
			// 	case $resultTransferSaving['response_code'] == '1111':
			// 		$traStatus = 5;
			// 		break;
			// 	default:
			// 		$traStatus = 4;
			// 		break;
			// }

			// INSERT TO T BG TRANSACTION
			$insertData = [
				'TRANSACTION_ID' => $traId,
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'BG_NUMBER' => $data['databg']['BG_NUMBER'],
				'SERVICE' => 1, // TRANSFER BTN
				'SOURCE_ACCT' => $getEscrow['ACCT'],
				'SOURCE_ACCT_CCY' => 'IDR',
				'CHARGE_ACCT' => '',
				'BENEF_SWIFT_CODE' => '',
				'BENEF_ACCT' => $reppkAcct ?: 'XXX',
				'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
				'TRA_CCY' => 'IDR',
				'TRA_AMOUNT' => $getEscrow["AMOUNT"],
				'CHARGE_AMOUNT' => '',
				"SOURCE_ACCT_NAME" => $getEscrow['NAME'],
				'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
				'REMARKS2' => '',
				'SOURCE_ADDRESS' => '',
				'SOURCE_PHONE' => $detailAcctEscrow['phone_number'] ?: '000',
				'BENEF_ADDRESS' => '',
				'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
				// 'TRA_TIME' => $traTime,
				'TRA_TIME' => '',
				// 'TRA_STATUS' => $traStatus,
				'TRA_STATUS' => '6',
				'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				'UPDATE_APPROVEDBY' => $this->_userIdLogin,
				// 'UUID' => $resultTransferSaving['uuid'],
				'UUID' => '',
				// 'RESPONSE_CODE' => $resultTransferSaving['response_code'],
				'RESPONSE_CODE' => '',
				// 'RESPONSE_DESC' => $resultTransferSaving['response_desc'],
				'RESPONSE_DESC' => '',
				// 'TRA_REF' => $resultTransferSaving['ref']
				'TRA_REF' => ''
			];

			$this->_db->insert('T_BG_TRANSACTION', $insertData);

			if ($insertData['TRA_STATUS'] == 3) {
				$maxTry = 3;
				$counterTry = 1;
				while (true) {
					$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

					// JIKA SUDAH DICOBA SEBANYAK 3 KALI
					if ($counterTry == $maxTry || $checkTrxTimeout) break;

					$counterTry++;
					sleep(2);
				}

				if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
					$this->runTransaction($insertData);
				}
			}
		}

		// TRANSFER REPPK TO REKENING TUJUAN PEMBAYARAN KLAIM
		if ($data['bankcode'] == 200) {
			$data = array_merge($data, ['reppkacct' => $reppkAcct]);
			$resultDump = $this->transferBTN($data, $seqNumber++);
		} else {
			$data = array_merge($data, ['reppkacct' => $reppkAcct]);
			$resultDump = $this->transferOtherBank($data, $seqNumber++);
		}

		$serviceCheck = ($resultDump['response_code'] == '0000') ? $serviceCheck : false;

		// CEK REKENING REPPK APAKAH MASIH ADA SISA
		$reppkDetailService = new Service_Account($reppkAcct, '');
		$reppkDetailAcct = $reppkDetailService->inquiryAccountBalance();
		$reppkInfoAcct = $reppkDetailService->inquiryAccontInfo();

		$sisaDana = false;
		if ($reppkDetailAcct['available_balance'] > 0 && $resultDump['response_code'] == '0000') {
			$sisaDana = true;
			// INSERT TO T BG TRANSACTION
			$insertData = [
				'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'BG_NUMBER' => $data['databg']['BG_NUMBER'],
				'SERVICE' => 9, // MANUAL SETTLEMENT
				'SOURCE_ACCT' => $reppkAcct,
				'SOURCE_ACCT_CCY' => 'IDR',
				'CHARGE_ACCT' => '',
				'BENEF_SWIFT_CODE' => '',
				'BENEF_ACCT' => '',
				'BENEF_ACCT_NAME' => '',
				'TRA_CCY' => 'IDR',
				'TRA_AMOUNT' => $reppkDetailAcct['available_balance'],
				'CHARGE_AMOUNT' => '',
				"SOURCE_ACCT_NAME" => $reppkDetailAcct['account_name'],
				'REMARKS1' => strval($data['databg']['CLOSE_REF_NUMBER'] . " Manual Settlement"),
				'REMARKS2' => '',
				'SOURCE_ADDRESS' => '',
				'SOURCE_PHONE' => $reppkInfoAcct['phone_number'] ?: '000',
				'BENEF_ADDRESS' => '',
				'BENEF_PHONE' => '',
				'TRA_TIME' => new Zend_Db_Expr('now()'),
				'TRA_STATUS' => '5',
				'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				'UPDATE_APPROVEDBY' => $this->_userIdLogin,
				'UUID' => '',
				'RESPONSE_CODE' => $resultDump['response_code'],
				'RESPONSE_DESC' => $resultDump['response_desc'],
				'TRA_REF' => $resultDump['ref']
			];

			$this->_db->insert('T_BG_TRANSACTION', $insertData);
		}

		$checkAllTransaction = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('TRA_STATUS != ?', '1')
			->query()->fetchAll();

		// UPDATE T BG PSLIP
		$dataPslip = array(
			'PS_APPROVED' => new Zend_Db_Expr("now()"),
			'PS_APPROVEDBY' => $this->_userIdLogin,
			// 'PS_STATUS' => (!$checkAllTransaction && !$sisaDana) ? 5 : 4,
			'PS_STATUS' => 6,
			'PS_DONE' => ($serviceCheck) ? new Zend_Db_Expr('now()') : ''
		);

		$where['CLOSE_REF_NUMBER = ?'] = $data['databg']['CLOSE_REF_NUMBER'];
		$this->_db->update('T_BG_PSLIP', $dataPslip, $where);

		// SEND TRANSACTION TO WORKER
		Application_Helper_Email::runTransaction($data['databg']['CLOSE_REF_NUMBER']);

		// PINDAH MEMO DARI TEMP KE T BANK GUARANTEE FILE CLOSE
		$memoFile = $this->_db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
			->where('A.CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(FILE_NOTE) = ?', 'memo / surat persetujuan klaim')
			->query()->fetch();

		$this->_db->delete('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
			'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
			'LOWER(FILE_NOTE) = ?' => 'memo / surat persetujuan klaim',
		]);

		// INSERT KE T BG FILE CLOSE
		$this->_db->insert('T_BANK_GUARANTEE_FILE_CLOSE', $memoFile);

		// HAPUS MEMO DARI TABEL TEMP
		$this->_db->delete('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
			'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
			'LOWER(FILE_NOTE) = ?' => 'memo / surat persetujuan klaim'
		]);

		// INSERT HISTORY
		$historyData = [
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'CUST_ID' => $data['CUST_ID'],
			'DATE_TIME' => new Zend_Db_Expr('now()'),
			'USER_FROM' => 'BANK',
			'USER_LOGIN' => $this->_userIdLogin,
			'BG_REASON' => 'NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'],
			'HISTORY_STATUS' => 19
		];
		$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyData);

		// JIKA SEMUA TRANSAKSI BERHASIL MAKA KIRIM EMAIL
		if ($serviceCheck && !$sisaDana) {
			// SEND EMAIL

			$getIndexChargeAcctNumber = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
				->query()->fetch();

			$getAcctChargeDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('ACCT_INDEX = ?', $getIndexChargeAcctNumber['ACCT_INDEX'])
				->query()->fetchAll();

			$nominal = number_format($getAcctChargeDetail["Transaction Amount"], 2);

			$data['historydata'] = $historyData;

			// SEND EMAIL TO PRINCIPAL
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3A', $nominal);
			// if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL_BG'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO OBLIGEE
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3B', $nominal);
			// if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO KANTOR CABANG PENERBIT
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', $nominal);
			// if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// FULL COVER
			// $settings = new Settings();
			// $getGroupFC = $settings->getSettingNew('email_group_cashcoll');
			// $getGroupFC = explode(',', $getGroupFC);
			// $getGroupFC = array_map('trim', $getGroupFC);
			// if ($getGroupFC) {
			// 	foreach ($getGroupFC as $emailGroup) {
			// 		$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', $nominal);
			// 		if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
			// 	}
			// }
		}

		// INSERT LOG
		$privLog = $data['databg']['COUNTER_WARRANTY_TYPE'] == '1' ? 'PPCC' : 'PPNC';
		Application_Helper_General::writeLog($privLog, 'Persetujuan Penyelesaian Klaim untuk BG No : ' . $data['databg']['BG_NUMBER'] . ', Principal : ' . $data['databg']['CUST_ID'] . ', NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'] . '');

		$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
		$this->_redirect('/notification/success/index');
	}

	private function lineFcRelease($data)
	{
		// INTITIALIZE SERVICE CHECK
		$serviceCheck = true;

		// CEK CREATE OPEN ACCOUNT ON T BG TRANSACTION
		$cekOA = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('SERVICE = ?', '4')
			->query()->fetch();

		$seqNumber = 1;

		// JIKA REKENING PENAMPUNG BELUM ADA
		if (!$cekOA) {
			// BUAT REPPK / REKENING PENAMPUNG
			$clientUser = new SGO_Soap_ClientUser();
			$requestopen = array();
			$requestopen['cif'] = $data['databg']["CUST_CIF"];
			$requestopen['branch_code'] = $data['databg']['BG_BRANCH'];
			$requestopen['product_code'] = 'K2';
			$requestopen['currency'] = 'IDR';

			$clientUser->callapi('createaccount', null, $requestopen);
			$reppkResult = $clientUser->getResult();

			// PARSE DATE TIME
			$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $reppkResult['datetime']);
			$traTime = $parseDateTime->format('Y-m-d H:i:s');

			$serviceCheck = ($reppkResult['response_code'] == '0000') ? $serviceCheck : false;

			$reppkAcct = ($reppkResult['response_code'] == '0000') ? $reppkResult['account_number'] : false;

			$reppkDetailService = new Service_Account($reppkAcct, '');
			$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

			// INSERT TO T BG TRANSACTION
			$insertData = [
				'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'BG_NUMBER' => $data['databg']['BG_NUMBER'],
				'SERVICE' => 4, // OPEN ACCOUNT
				'SOURCE_ACCT' => ($reppkAcct) ?: 'XXX',
				'SOURCE_ACCT_CCY' => 'IDR',
				'SOURCE_ACCT_NAME' => $reppkDetailAcct['account_name'],
				'CHARGE_ACCT' => '',
				'BENEF_SWIFT_CODE' => '',
				'BENEF_ACCT' => '',
				'TRA_CCY' => 'IDR',
				'TRA_AMOUNT' => '',
				'CHARGE_AMOUNT' => '',
				'REMARKS1' => '',
				'REMARKS2' => '',
				'SOURCE_ADDRESS' => '',
				'SOURCE_PHONE' => '',
				'BENEF_ADDRESS' => '',
				'TRA_TIME' => $traTime,
				'TRA_STATUS' => ($reppkAcct) ? 1 : 4,
				'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				'UPDATE_APPROVEDBY' => $this->_userIdLogin,
				'UUID' => $reppkResult['uuid'],
				'RESPONSE_CODE' => $reppkResult['response_code'],
				'RESPONSE_DESC' => $reppkResult['response_desc'],
				'TRA_REF' => $reppkResult['ref']
			];

			$this->_db->insert('T_BG_TRANSACTION', $insertData);
		}
		// JIKA REKENING PENAMPUNG SUDAH ADA
		else {
			$reppkAcct = $cekOA['SOURCE_ACCT'];
		}

		// TRANSFER REKENING SUMBER DANA KE REKENING REPPK -------------------------------------------

		$detailAcctSumberDanaService = new Service_Account($data['requests']['lfsumber_acct_number'], '');
		$detailAcctSumberDana = $detailAcctSumberDanaService->inquiryAccontInfo();
		$reppkDetailService = new Service_Account($reppkAcct, '');
		$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

		$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

		// ambil amount
		$transactionAmount = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('ACCT_INDEX = ?', 1)
			->where('LOWER(PS_FIELDNAME) = ?', 'transaction amount')
			->query()->fetch();

		if ($data['requests']['sumber_dana_type'] == '1') {
			// $transferData = [
			// 	"SOURCE_ACCOUNT_NUMBER" => $data['requests']['lfsumber_acct_number'],
			// 	"SOURCE_ACCOUNT_NAME" => $detailAcctSumberDana['account_name'],
			// 	"BENEFICIARY_ACCOUNT_NUMBER" => $reppkAcct ?: 'XXX',
			// 	"BENEFICIARY_ACCOUNT_NAME" => $reppkDetailAcct['account_name'],
			// 	"PHONE_NUMBER_FROM" => $detailAcctSumberDana['phone_number'] ?: '000',
			// 	"AMOUNT" => round(floatval($transactionAmount['PS_FIELDVALUE']), 2),
			// 	"PHONE_NUMBER_TO" => $reppkDetailAcct['phone_number'] ?: '000',
			// 	"RATE1" => "10000000",
			// 	"RATE2" => "10000000",
			// 	"RATE3" => "10000000",
			// 	"RATE4" => "10000000",
			// 	// "DESCRIPTION" => strval($traId),
			// 	"DESCRIPTION" => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
			// 	"DESCRIPTION_DETAIL" => substr($data['databg']["BG_NUMBER"], 0, 40),
			// ];

			// $transferSumberDanaService =  new Service_TransferWithin($transferData);
			// $result = $transferSumberDanaService->sendTransfer();
			// $resultTransferSumberDana = $result["RawResult"];

			// $serviceCheck = ($resultTransferSumberDana['response_code'] == '0000') ? $serviceCheck : false;

			$tempService = 1;
		} else {
			// $transferSumberDanaService =  new Service_Account('', '');
			// $resultTransferSumberDana = $transferSumberDanaService->loanDisbursement($data['requests']['lfsumber_acct_currency'], $data['requests']['lfsumber_acct_number'], $detailAcctSumberDana['account_name'], 'IDR', $reppkAcct, $reppkDetailAcct['account_name'], round(floatval($transactionAmount['PS_FIELDVALUE']), 2), "10000000", "10000000", "10000000", "10000000");

			// $serviceCheck = ($resultTransferSumberDana['response_code'] == '0000') ? $serviceCheck : false;

			$tempService = 6;
		}

		// PARSE DATE TIME
		// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSumberDana['datetime']);
		// $traTime = $parseDateTime->format('Y-m-d H:i:s');

		// switch (true) {
		// 	case $resultTransferSumberDana['response_code'] == '0000':
		// 		$traStatus = 1;
		// 		break;
		// 	case $resultTransferSumberDana['response_code'] == 'XT' || $resultTransferSumberDana['response_code'] == '0012' || $resultTransferSumberDana['response_code'] == '9999':
		// 		$traStatus = 3;
		// 		break;
		// 	case $resultTransferSumberDana['response_code'] == '1111':
		// 		$traStatus = 5;
		// 		break;
		// 	default:
		// 		$traStatus = 4;
		// 		break;
		// }

		// INSERT TO T BG TRANSACTION
		$insertData = [
			'TRANSACTION_ID' => $traId,
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'SERVICE' => $tempService, // TRANSFER BTN
			'SOURCE_ACCT' => $data['requests']['lfsumber_acct_number'],
			'SOURCE_ACCT_CCY' => $data['requests']['lfsumber_acct_currency'],
			"SOURCE_ACCT_NAME" => $data['requests']['lfsumber_acct_name'],
			'CHARGE_ACCT' => '',
			'BENEF_SWIFT_CODE' => '',
			'BENEF_ACCT' => $reppkAcct ?: 'XXX',
			'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
			'TRA_CCY' => 'IDR',
			'TRA_AMOUNT' => $transactionAmount['PS_FIELDVALUE'],
			'CHARGE_AMOUNT' => '',
			'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
			'REMARKS2' => '',
			'SOURCE_ADDRESS' => '',
			'SOURCE_PHONE' => $detailAcctSumberDana['phone_number'] ?: '000',
			'BENEF_ADDRESS' => '',
			'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
			// 'TRA_TIME' => $traTime,
			'TRA_TIME' => '',
			// 'TRA_STATUS' => $traStatus,
			'TRA_STATUS' => '6',
			'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
			'UPDATE_APPROVEDBY' => $this->_userIdLogin,
			// 'UUID' => $resultTransferSumberDana['uuid'],
			'UUID' => '',
			// 'RESPONSE_CODE' => $resultTransferSumberDana['response_code'],
			'RESPONSE_CODE' => '',
			// 'RESPONSE_DESC' => $resultTransferSumberDana['response_desc'],
			'RESPONSE_DESC' => '',
			// 'TRA_REF' => $resultTransferSumberDana['ref']
			'TRA_REF' => ''
		];

		$this->_db->insert('T_BG_TRANSACTION', $insertData);

		if ($insertData['TRA_STATUS'] == 3) {
			$maxTry = 3;
			$counterTry = 1;
			while (true) {
				$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

				// JIKA SUDAH DICOBA SEBANYAK 3 KALI
				if ($counterTry == $maxTry || $checkTrxTimeout) break;

				$counterTry++;
				sleep(2);
			}

			if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
				$this->runTransaction($insertData);
			}
		}

		// TRANSFER REPPK TO REKENING TUJUAN PEMBAYARAN KLAIM
		if ($data['bankcode'] == 200) {
			$data = array_merge($data, ['reppkacct' => $reppkAcct]);
			$resultDump = $this->transferBTN($data, $seqNumber++);
		} else {
			$data = array_merge($data, ['reppkacct' => $reppkAcct]);
			$resultDump = $this->transferOtherBank($data, $seqNumber++);
		}

		$serviceCheck = ($resultDump['response_code'] == '0000') ? $serviceCheck : false;

		$checkAllTransaction = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('TRA_STATUS != ?', '1')
			->query()->fetchAll();

		// UPDATE T BG PSLIP
		$dataPslip = array(
			'PS_APPROVED' => new Zend_Db_Expr("now()"),
			'PS_APPROVEDBY' => $this->_userIdLogin,
			// 'PS_STATUS' => !$checkAllTransaction ? 5 : 4,
			'PS_STATUS' => 6,
			'PS_DONE' => ($serviceCheck) ? new Zend_Db_Expr('now()') : ''
		);

		$where['CLOSE_REF_NUMBER = ?'] = $data['databg']['CLOSE_REF_NUMBER'];
		$this->_db->update('T_BG_PSLIP', $dataPslip, $where);

		// SEND TRANSACTION TO WORKER
		Application_Helper_Email::runTransaction($data['databg']['CLOSE_REF_NUMBER']);

		// PINDAH MEMO DARI TEMP KE T BANK GUARANTEE FILE CLOSE
		$memoFile = $this->_db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
			->where('A.CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(FILE_NOTE) = ?', 'memo / surat persetujuan klaim')
			->query()->fetch();

		// INSERT KE T BG FILE CLOSE
		$this->_db->insert('T_BANK_GUARANTEE_FILE_CLOSE', $memoFile);

		// HAPUS MEMO DARI TABEL TEMP
		$this->_db->delete('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
			'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
			'LOWER(FILE_NOTE) = ?' => 'memo / surat persetujuan klaim'
		]);

		// INSERT HISTORY
		$historyData = [
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'CUST_ID' => $data['CUST_ID'],
			'DATE_TIME' => new Zend_Db_Expr('now()'),
			'USER_FROM' => 'BANK',
			'USER_LOGIN' => $this->_userIdLogin,
			'BG_REASON' => 'NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'],
			'HISTORY_STATUS' => 19
		];
		$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyData);

		// JIKA SEMUA TRANSAKSI BERHASIL MAKA KIRIM EMAIL
		if ($serviceCheck) {
			// SEND EMAIL

			$data['historydata'] = $historyData;

			$getIndexChargeAcctNumber = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
				->query()->fetch();

			$getAcctChargeDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('ACCT_INDEX = ?', $getIndexChargeAcctNumber['ACCT_INDEX'])
				->query()->fetchAll();

			$nominal = number_format($getAcctChargeDetail["Transaction Amount"], 2);

			// SEND EMAIL TO PRINCIPAL
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3A', $nominal);
			// if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL_BG'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO OBLIGEE
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3B', $nominal);
			// if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO KANTOR CABANG PENERBIT
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', $nominal);
			// if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// GROUP LINEFACILITY
			// $settings = new Settings();
			// $getGroupFC = $settings->getSettingNew('email_group_linefacility');
			// $getGroupFC = explode(',', $getGroupFC);
			// $getGroupFC = array_map('trim', $getGroupFC);
			// if ($getGroupFC) {
			// 	foreach ($getGroupFC as $emailGroup) {
			// 		$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', $nominal);
			// 		if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
			// 	}
			// }

			// SEND EMAIL TO PRINCIPAL
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_prinsipal', $nominal);
			// if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);

			// // SEND EMAIL TO OBLIGEE
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_obligee', $nominal);
			// if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);

			// // SEND EMAIL TO KANTOR CABANG PENERBIT
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_kcp', $nominal);
			// if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);
		}

		// INSERT LOG
		$privLog = $data['databg']['COUNTER_WARRANTY_TYPE'] == '1' ? 'PPCC' : 'PPNC';
		Application_Helper_General::writeLog($privLog, 'Persetujuan Penyelesaian Klaim untuk BG No : ' . $data['databg']['BG_NUMBER'] . ', Principal : ' . $data['databg']['CUST_ID'] . ', NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'] . '');

		$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
		$this->_redirect('/notification/success/index');
	}

	private function insuranceRelease($data)
	{
		// INTITIALIZE SERVICE CHECK
		$serviceCheck = true;

		// CEK CREATE OPEN ACCOUNT ON T BG TRANSACTION
		$cekOA = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('SERVICE = ?', '4')
			->query()->fetch();

		$seqNumber = 1;

		// JIKA REKENING PENAMPUNG BELUM ADA
		if (!$cekOA) {
			// BUAT REPPK / REKENING PENAMPUNG
			$clientUser = new SGO_Soap_ClientUser();
			$requestopen = array();
			$requestopen['cif'] = $data['databg']["CUST_CIF"];
			$requestopen['branch_code'] = $data['databg']['BG_BRANCH'];
			$requestopen['product_code'] = 'K2';
			$requestopen['currency'] = 'IDR';

			$clientUser->callapi('createaccount', null, $requestopen);
			$reppkResult = $clientUser->getResult();

			// PARSE DATE TIME
			$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $reppkResult['datetime']);
			$traTime = $parseDateTime->format('Y-m-d H:i:s');

			$serviceCheck = ($reppkResult['response_code'] == '0000') ? $serviceCheck : false;

			$reppkAcct = ($reppkResult['response_code'] == '0000') ? $reppkResult['account_number'] : false;

			$reppkDetailService = new Service_Account($reppkAcct, '');
			$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

			// INSERT TO T BG TRANSACTION
			$insertData = [
				'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'BG_NUMBER' => $data['databg']['BG_NUMBER'],
				'SERVICE' => 4, // OPEN ACCOUNT
				'SOURCE_ACCT' => ($reppkAcct) ?: 'XXX',
				'SOURCE_ACCT_CCY' => 'IDR',
				'SOURCE_ACCT_NAME' => $reppkDetailAcct['account_name'],
				'CHARGE_ACCT' => '',
				'BENEF_SWIFT_CODE' => '',
				'BENEF_ACCT' => '',
				'TRA_CCY' => 'IDR',
				'TRA_AMOUNT' => '',
				'CHARGE_AMOUNT' => '',
				'REMARKS1' => '',
				'REMARKS2' => '',
				'SOURCE_ADDRESS' => '',
				'SOURCE_PHONE' => '',
				'BENEF_ADDRESS' => '',
				'TRA_TIME' => $traTime,
				'TRA_STATUS' => ($reppkAcct) ? 1 : 4,
				'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				'UPDATE_APPROVEDBY' => $this->_userIdLogin,
				'UUID' => $reppkResult['uuid'],
				'RESPONSE_CODE' => $reppkResult['response_code'],
				'RESPONSE_DESC' => $reppkResult['response_desc'],
				'TRA_REF' => $reppkResult['ref']
			];

			$this->_db->insert('T_BG_TRANSACTION', $insertData);
		}
		// JIKA REKENING PENAMPUNG SUDAH ADA
		else {
			$reppkAcct = $cekOA['SOURCE_ACCT'];
		}

		// CEK SUMBER DANA
		$sumberDana = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(PS_FIELDNAME) = ?', 'sumber dana pembayaran')
			->query()->fetch();

		$sumberDana = $sumberDana['PS_FIELDVALUE'];

		if ($sumberDana == '1') {

			$getSumberDana = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['ACCT_INDEX'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('LOWER(PS_FIELDNAME) LIKE ?', '%source account number%')
				->query()->fetch();

			$getSumberDana = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['*'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('ACCT_INDEX = ?', $getSumberDana['ACCT_INDEX'])
				->query()->fetchAll();

			$getSumberDana = array_combine(array_column($getSumberDana, 'PS_FIELDNAME'), array_column($getSumberDana, 'PS_FIELDVALUE'));

			$detailAcctEscrowService = new Service_Account($getSumberDana['Source Account Number'], '');
			$detailAcctEscrow = $detailAcctEscrowService->inquiryAccontInfo();
			$reppkDetailService = new Service_Account($reppkAcct, '');
			$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

			$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

			// $transferData = [
			// 	"SOURCE_ACCOUNT_NUMBER" => $getSumberDana['Source Account Number'],
			// 	"SOURCE_ACCOUNT_NAME" => $detailAcctEscrow['account_name'],
			// 	"BENEFICIARY_ACCOUNT_NUMBER" => $reppkAcct ?: 'XXX',
			// 	"BENEFICIARY_ACCOUNT_NAME" => $reppkDetailAcct['account_name'],
			// 	"PHONE_NUMBER_FROM" => $detailAcctEscrow['phone_number'] ?: '000',
			// 	"AMOUNT" => round(floatval($getSumberDana['Transaction Amount']), 2),
			// 	"PHONE_NUMBER_TO" => $reppkDetailAcct['phone_number'] ?: '000',
			// 	"RATE1" => "10000000",
			// 	"RATE2" => "10000000",
			// 	"RATE3" => "10000000",
			// 	"RATE4" => "10000000",
			// 	// "DESCRIPTION" => strval($traId),
			// 	"DESCRIPTION" => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
			// 	"DESCRIPTION_DETAIL" => substr($data['databg']["BG_NUMBER"], 0, 40),
			// ];

			// $transferSavingService =  new Service_TransferWithin($transferData);
			// $result = $transferSavingService->sendTransfer();
			// $resultTransferSaving = $result["RawResult"];

			// $serviceCheck = ($resultTransferSaving['response_code'] == '0000') ? $serviceCheck : false;

			// PARSE DATE TIME
			// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSaving['datetime']);
			// $traTime = $parseDateTime->format('Y-m-d H:i:s');

			// switch (true) {
			// 	case $resultTransferSaving['response_code'] == '0000':
			// 		$traStatus = 1;
			// 		break;
			// 	case $resultTransferSaving['response_code'] == 'XT' || $resultTransferSaving['response_code'] == '0012' || $resultTransferSaving['response_code'] == '9999':
			// 		$traStatus = 3;
			// 		break;
			// 	case $resultTransferSaving['response_code'] == '1111':
			// 		$traStatus = 5;
			// 		break;
			// 	default:
			// 		$traStatus = 4;
			// 		break;
			// }

			// INSERT TO T BG TRANSACTION
			$insertData = [
				'TRANSACTION_ID' => $traId,
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'BG_NUMBER' => $data['databg']['BG_NUMBER'],
				'SERVICE' => 1, // TRANSFER BTN
				'SOURCE_ACCT' => $getSumberDana['Source Account Number'],
				'SOURCE_ACCT_CCY' => 'IDR',
				'CHARGE_ACCT' => '',
				'BENEF_SWIFT_CODE' => '',
				'BENEF_ACCT' => $reppkAcct ?: 'XXX',
				'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
				'TRA_CCY' => 'IDR',
				'TRA_AMOUNT' => $getSumberDana['Transaction Amount'],
				'CHARGE_AMOUNT' => '',
				"SOURCE_ACCT_NAME" => $getSumberDana['Source Account Name'],
				'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
				'REMARKS2' => '',
				'SOURCE_ADDRESS' => '',
				'SOURCE_PHONE' => $detailAcctEscrow['phone_number'] ?: '000',
				'BENEF_ADDRESS' => '',
				'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
				// 'TRA_TIME' => $traTime,
				'TRA_TIME' => '',
				// 'TRA_STATUS' => $traStatus,
				'TRA_STATUS' => '6',
				'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				'UPDATE_APPROVEDBY' => $this->_userIdLogin,
				// 'UUID' => $resultTransferSaving['uuid'],
				'UUID' => '',
				// 'RESPONSE_CODE' => $resultTransferSaving['response_code'],
				'RESPONSE_CODE' => '',
				// 'RESPONSE_DESC' => $resultTransferSaving['response_desc'],
				'RESPONSE_DESC' => '',
				// 'TRA_REF' => $resultTransferSaving['ref']
				'TRA_REF' => ''
			];

			$this->_db->insert('T_BG_TRANSACTION', $insertData);

			if ($insertData['TRA_STATUS'] == 3) {
				$maxTry = 3;
				$counterTry = 1;
				while (true) {
					$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

					// JIKA SUDAH DICOBA SEBANYAK 3 KALI
					if ($counterTry == $maxTry || $checkTrxTimeout) break;

					$counterTry++;
					sleep(2);
				}

				if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
					$this->runTransaction($insertData);
				}
			}
		} else {
			$getGroupIndex = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['*'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('LOWER(PS_FIELDNAME) LIKE ?', '%source%')
				->where('ACCT_INDEX != ?', '1')
				->group('ACCT_INDEX')
				->query()->fetchAll();

			$getGroupIndex = array_column($getGroupIndex, 'ACCT_INDEX');

			$getSumberDana = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['*'])
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('ACCT_INDEX IN (?)', $getGroupIndex)
				->query()->fetchAll();

			$saveAcctSumberDana = array_map(function ($item) use ($getSumberDana) {
				$test = array_filter($getSumberDana, function ($list) use ($item) {
					return $list['ACCT_INDEX'] == $item;
				});

				return array_combine(array_column($test, 'PS_FIELDNAME'), array_column($test, 'PS_FIELDVALUE'));
			}, $getGroupIndex);

			// MD Giro (=2)
			// MD Deposito (=3)
			// MD Saving (=4)

			$getAllMdIns = $this->_db->select()
				->from('M_MARGINALDEPOSIT_DETAIL')
				->where('CUST_ID = ?', $data['databg']['BG_INSURANCE_CODE'])
				->query()->fetchAll();

			foreach ($getAllMdIns as $key => $value) {
				$existSumberDana = array_shift(array_filter($saveAcctSumberDana, function ($item) use ($value) {
					return $item['Source Account Number'] == $value['MD_ACCT'];
				}));

				if (!$existSumberDana) continue;

				if ($existSumberDana['Source of Payment'] == '2' || $existSumberDana['Source of Payment'] == '4') {

					if ($existSumberDana['Source of Payment'] == '4') {
						// UNLOCK TABUNGAN ---------------------------------------------------------
						// $unlockSavingService = new Service_Account($existSumberDana['Source Account Number'], '');
						// $unlockTabungan = $unlockSavingService->unlockSaving([
						// 	'branch_code' => $existSumberDana['Hold By Branch'],
						// 	'sequence_number' => $existSumberDana['Hold Sequence']
						// ]);

						// $serviceCheck = ($unlockTabungan['response_code'] == '0000') ? $serviceCheck : false;

						// // PARSE DATE TIME
						// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockTabungan['datetime']);
						// $traTime = $parseDateTime->format('Y-m-d H:i:s');

						// switch (true) {
						// 	case $unlockTabungan['response_code'] == '0000':
						// 		$traStatus = 1;
						// 		break;
						// 	case $unlockTabungan['response_code'] == 'XT' || $unlockTabungan['response_code'] == '0012' || $unlockTabungan['response_code'] == '9999':
						// 		$traStatus = 3;
						// 		break;
						// 	case $unlockTabungan['response_code'] == '1111':
						// 		$traStatus = 5;
						// 		break;
						// 	default:
						// 		$traStatus = 4;
						// 		break;
						// }

						// $tempDetailInfo = $this->getDetailInfoAcct($existSumberDana['Source Account Number']);

						// INSERT TO T BG TRANSACTION
						$insertData = [
							'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
							'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
							'BG_NUMBER' => $data['databg']['BG_NUMBER'],
							'SERVICE' => 3, // UNLOCK SAVING
							'SOURCE_ACCT' => $existSumberDana['Source Account Number'],
							'SOURCE_ACCT_CCY' => $existSumberDana['Source Account CCY'],
							'SOURCE_ACCT_NAME' => $existSumberDana['Source Account Name'],
							'CHARGE_ACCT' => '',
							'BENEF_SWIFT_CODE' => '',
							'BENEF_ACCT' => '',
							'HOLD_BY_BRANCH' => $existSumberDana['Hold By Branch'],
							'HOLD_SEQUENCE' => $existSumberDana['Hold Sequence'],
							'TRA_CCY' => 'IDR',
							'TRA_AMOUNT' => $value['GUARANTEE_AMOUNT'],
							'CHARGE_AMOUNT' => '',
							'REMARKS1' => '',
							'REMARKS2' => '',
							'SOURCE_ADDRESS' => '',
							'SOURCE_PHONE' => '',
							'BENEF_ADDRESS' => '',
							// 'TRA_TIME' => $traTime,
							'TRA_TIME' => '',
							// 'TRA_STATUS' => $traStatus,
							'TRA_STATUS' => '6',
							'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
							'UPDATE_APPROVEDBY' => $this->_userIdLogin,
							// 'UUID' => $unlockTabungan['uuid'],
							'UUID' => '',
							// 'RESPONSE_CODE' => $unlockTabungan['response_code'],
							'RESPONSE_CODE' => '6',
							// 'RESPONSE_DESC' => $unlockTabungan['response_desc'],
							'RESPONSE_DESC' => '',
							// 'TRA_REF' => $unlockTabungan['ref']
							'TRA_REF' => ''
						];

						$this->_db->insert('T_BG_TRANSACTION', $insertData);

						if ($insertData['TRA_STATUS'] == 3) {
							$maxTry = 3;
							$counterTry = 1;
							while (true) {
								$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

								// JIKA SUDAH DICOBA SEBANYAK 3 KALI
								if ($counterTry == $maxTry || $checkTrxTimeout) break;

								$counterTry++;
								sleep(2);
							}

							if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
								$this->runTransaction($insertData);
							}
						}

						// CHECK APAKAH TRANSFER BERHASIL
						$cekStatusTemp = $this->_db->select()
							->from('T_BG_TRANSACTION')
							->where('TRANSACTION_ID = ?', $insertData['TRANSACTION_ID'])
							->query()->fetch();

						// if ($unlockTabungan['response_code'] == '0000') {
						if ($cekStatusTemp['TRA_STATUS'] == '1') {
							$this->_db->delete('M_MARGINALDEPOSIT_DETAIL', [
								'MD_ACCT = ?' => $existSumberDana['Source Account Number'],
								'CUST_ID = ?' => $data['databg']['BG_INSURANCE_CODE'],
								'HOLD_SEQUENCE = ?' => $value['HOLD_SEQUENCE']
							]);
							// $this->_db->update('M_MARGINALDEPOSIT_DETAIL', [
							// 	'USED' => 1
							// ], [
							// 	'MD_ACCT = ?' => $existSumberDana['Source Account Number'],
							// 	'CUST_ID = ?' => $data['databg']['BG_INSURANCE_CODE'],
							// 	'HOLD_SEQUENCE = ?' => $value['HOLD_SEQUENCE']
							// ]);
						}
					}

					$detailAcctEscrowService = new Service_Account($existSumberDana['Source Account Number'], '');
					$detailAcctEscrow = $detailAcctEscrowService->inquiryAccontInfo();
					$reppkDetailService = new Service_Account($reppkAcct, '');
					$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

					$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

					// $transferData = [
					// 	"SOURCE_ACCOUNT_NUMBER" => $existSumberDana['Source Account Number'],
					// 	"SOURCE_ACCOUNT_NAME" => $detailAcctEscrow['account_name'],
					// 	"BENEFICIARY_ACCOUNT_NUMBER" => $reppkAcct ?: 'XXX',
					// 	"BENEFICIARY_ACCOUNT_NAME" => $reppkDetailAcct['account_name'],
					// 	"PHONE_NUMBER_FROM" => $detailAcctEscrow['phone_number'] ?: '000',
					// 	"AMOUNT" => round(floatval($existSumberDana['Transaction Amount']), 2),
					// 	"PHONE_NUMBER_TO" => $reppkDetailAcct['phone_number'] ?: '000',
					// 	"RATE1" => "10000000",
					// 	"RATE2" => "10000000",
					// 	"RATE3" => "10000000",
					// 	"RATE4" => "10000000",
					// 	// "DESCRIPTION" => strval($traId),
					// 	"DESCRIPTION" => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
					// 	"DESCRIPTION_DETAIL" => substr($data['databg']["BG_NUMBER"], 0, 40),
					// ];

					// if ($traStatus == 1) {
					// 	$transferSavingService =  new Service_TransferWithin($transferData);
					// 	$result = $transferSavingService->sendTransfer();
					// 	$resultTransferSaving = $result["RawResult"];
					// } else {
					// 	$resultTransferSaving['response_code'] = '1111';
					// 	$resultTransferSaving['response_desc'] = 'unhold tabungan gagal';
					// 	$resultTransferSaving['datetime'] = '0000-00-00T00:00:00+00:00';
					// 	$resultTransferSaving['uuid'] = '-';
					// 	$resultTransferSaving['ref'] = '-';
					// }

					// $serviceCheck = ($resultTransferSaving['response_code'] == '0000') ? $serviceCheck : false;

					// // PARSE DATE TIME
					// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSaving['datetime']);
					// $traTime = $parseDateTime->format('Y-m-d H:i:s');

					// switch (true) {
					// 	case $resultTransferSaving['response_code'] == '0000':
					// 		$traStatus = 1;
					// 		break;
					// 	case $resultTransferSaving['response_code'] == 'XT' || $resultTransferSaving['response_code'] == '0012' || $resultTransferSaving['response_code'] == '9999':
					// 		$traStatus = 3;
					// 		break;
					// 	case $resultTransferSaving['response_code'] == '1111':
					// 		$traStatus = 5;
					// 		break;
					// 	default:
					// 		$traStatus = 4;
					// 		break;
					// }

					// INSERT TO T BG TRANSACTION
					$insertData = [
						'TRANSACTION_ID' => $traId,
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'BG_NUMBER' => $data['databg']['BG_NUMBER'],
						'SERVICE' => 1, // TRANSFER BTN
						'SOURCE_ACCT' => $existSumberDana['Source Account Number'],
						'SOURCE_ACCT_CCY' => 'IDR',
						'CHARGE_ACCT' => '',
						'BENEF_SWIFT_CODE' => '',
						'BENEF_ACCT' => $reppkAcct ?: 'XXX',
						'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
						'TRA_CCY' => 'IDR',
						'TRA_AMOUNT' => $existSumberDana['Transaction Amount'],
						'CHARGE_AMOUNT' => '',
						// "SOURCE_ACCT_NAME" => $detailAcctEscrow['account_name'],
						"SOURCE_ACCT_NAME" => $existSumberDana['Source Account Name'],
						'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
						'REMARKS2' => '',
						'SOURCE_ADDRESS' => '',
						'SOURCE_PHONE' => $detailAcctEscrow['phone_number'] ?: '000',
						'BENEF_ADDRESS' => '',
						'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
						// 'TRA_TIME' => $traTime,
						'TRA_TIME' => '',
						// 'TRA_STATUS' => $traStatus,
						'TRA_STATUS' => '6',
						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
						'UPDATE_APPROVEDBY' => $this->_userIdLogin,
						// 'UUID' => $resultTransferSaving['uuid'],
						'UUID' => '',
						// 'RESPONSE_CODE' => $resultTransferSaving['response_code'],
						'RESPONSE_CODE' => '',
						// 'RESPONSE_DESC' => $resultTransferSaving['response_desc'],
						'RESPONSE_DESC' => '',
						// 'TRA_REF' => $resultTransferSaving['ref']
						'TRA_REF' => ''
					];

					$this->_db->insert('T_BG_TRANSACTION', $insertData);

					if ($insertData['TRA_STATUS'] == 3) {
						$maxTry = 3;
						$counterTry = 1;
						while (true) {
							$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

							// JIKA SUDAH DICOBA SEBANYAK 3 KALI
							if ($counterTry == $maxTry || $checkTrxTimeout) break;

							$counterTry++;
							sleep(2);
						}

						if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
							$this->runTransaction($insertData);
						}
					}

					if ($existSumberDana['Source of Payment'] == '2') {

						// CHECK APAKAH TRANSFER BERHASIL
						$cekStatusTemp = $this->_db->select()
							->from('T_BG_TRANSACTION')
							->where('TRANSACTION_ID = ?', $insertData['TRANSACTION_ID'])
							->query()->fetch();

						// if ($resultTransferSaving['response_code'] == '0000') {
						if ($cekStatusTemp['TRA_STATUS'] == '1') {
							$this->_db->delete('M_MARGINALDEPOSIT_DETAIL', [
								'MD_ACCT = ?' => $existSumberDana['Source Account Number'],
								'CUST_ID = ?' => $data['databg']['BG_INSURANCE_CODE'],
								'HOLD_SEQUENCE = ?' => $value['HOLD_SEQUENCE']
							]);
							// $this->_db->update('M_MARGINALDEPOSIT_DETAIL', [
							// 	'GUARANTEE_AMOUNT' => $value['GUARANTEE_AMOUNT'] - $existSumberDana['Transaction Amount']
							// ], [
							// 	'MD_ACCT = ?' => $existSumberDana['Source Account Number'],
							// 	'CUST_ID = ?' => $data['databg']['BG_INSURANCE_CODE']
							// ]);
						}
					} else {
						// CHECK APAKAH TRANSFER BERHASIL
						$cekStatusTemp = $this->_db->select()
							->from('T_BG_TRANSACTION')
							->where('TRANSACTION_ID = ?', $insertData['TRANSACTION_ID'])
							->query()->fetch();

						// if ($resultTransferSaving['response_code'] == '0000') {
						if ($cekStatusTemp['TRA_STATUS'] == '1') {
							$this->_db->delete('M_MARGINALDEPOSIT_DETAIL', [
								'MD_ACCT = ?' => $existSumberDana['Source Account Number'],
								'CUST_ID = ?' => $data['databg']['BG_INSURANCE_CODE'],
								'HOLD_SEQUENCE = ?' => $value['HOLD_SEQUENCE']
							]);
						}
					}
				} else {
					// UNLOCK DEPOSITO ---------------------------------------------------------
					// $unlockDepositoService = new Service_Account($existSumberDana['Source Account Number'], '');
					// $unlockDeposito = $unlockDepositoService->unlockDeposito('T', $existSumberDana['Hold By Branch'], $existSumberDana['Hold Sequence']);

					// $serviceCheck = ($unlockDeposito['response_code'] == '0000') ? $serviceCheck : false;

					// // PARSE DATE TIME
					// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockDeposito['datetime']);
					// $traTime = $parseDateTime->format('Y-m-d H:i:s');

					// switch (true) {
					// 	case $unlockDeposito['response_code'] == '0000':
					// 		$traStatus = 1;
					// 		break;
					// 	case $unlockDeposito['response_code'] == 'XT' || $unlockDeposito['response_code'] == '0012' || $unlockDeposito['response_code'] == '9999':
					// 		$traStatus = 3;
					// 		break;
					// 	case $unlockDeposito['response_code'] == '1111':
					// 		$traStatus = 5;
					// 		break;
					// 	default:
					// 		$traStatus = 4;
					// 		break;
					// }

					// $tempDetailInfo = $this->getDetailInfoAcct($existSumberDana['Source Account Number']);

					// INSERT TO T BG TRANSACTION
					$insertData = [
						'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'BG_NUMBER' => $data['databg']['BG_NUMBER'],
						'SERVICE' => 2, // UNLOCK DEPOSITO
						'SOURCE_ACCT' => $existSumberDana['Source Account Number'],
						'SOURCE_ACCT_CCY' => $existSumberDana['Source Account CCY'],
						'SOURCE_ACCT_NAME' => $existSumberDana['Source Account Name'],
						'CHARGE_ACCT' => '',
						'BENEF_SWIFT_CODE' => '',
						'BENEF_ACCT' => '',
						'HOLD_BY_BRANCH' => $existSumberDana['Hold By Branch'],
						'HOLD_SEQUENCE' => $existSumberDana['Hold Sequence'],
						'TRA_CCY' => 'IDR',
						'TRA_AMOUNT' => $value['GUARANTEE_AMOUNT'],
						'CHARGE_AMOUNT' => '',
						'REMARKS1' => '',
						'REMARKS2' => '',
						'SOURCE_ADDRESS' => '',
						'SOURCE_PHONE' => '',
						'BENEF_ADDRESS' => '',
						// 'TRA_TIME' => $traTime,
						'TRA_TIME' => '',
						// 'TRA_STATUS' => $traStatus,
						'TRA_STATUS' => '6',
						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
						'UPDATE_APPROVEDBY' => $this->_userIdLogin,
						// 'UUID' => $unlockDeposito['uuid'],
						'UUID' => '',
						// 'RESPONSE_CODE' => $unlockDeposito['response_code'],
						'RESPONSE_CODE' => '',
						// 'RESPONSE_DESC' => $unlockDeposito['response_desc'],
						'RESPONSE_DESC' => '',
						// 'TRA_REF' => $unlockDeposito['ref']
						'TRA_REF' => ''
					];

					$this->_db->insert('T_BG_TRANSACTION', $insertData);

					if ($insertData['TRA_STATUS'] == 3) {
						$maxTry = 3;
						$counterTry = 1;
						while (true) {
							$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

							// JIKA SUDAH DICOBA SEBANYAK 3 KALI
							if ($counterTry == $maxTry || $checkTrxTimeout) break;

							$counterTry++;
							sleep(2);
						}

						if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
							$this->runTransaction($insertData);
						}
					}

					// END UNLOCK DEPOSITO ---------------------------------------------------------

					// TRANSFER DEPOSITO KE REKENING REPPK -------------------------------------------

					$stateTransfer = true;

					$detailAcctDepoService = new Service_Account($existSumberDana['Source Account Number'], '');
					$detailAcctDeposito = $detailAcctDepoService->inquiryAccontInfo();
					$reppkDetailService = new Service_Account($reppkAcct, '');
					$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

					$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

					// if ($traStatus == 1) {
					// 	$depositoDisrburseService =  new Service_Account($existSumberDana['Source Account Number'], '');
					// 	$resultDepoDisburse = $depositoDisrburseService->depositoDisbursement($existSumberDana['Source Account CCY'], $existSumberDana['Source Account Number'], $detailAcctDeposito['account_name'], 'IDR', $reppkAcct, $reppkDetailAcct['account_name'], $existSumberDana['Transaction Amount'], '10000000', '10000000', '10000000', '10000000', strval('Klaim BG No ' . $data['databg']['BG_NUMBER']), strval($data['databg']['BG_NUMBER']));
					// } else {
					// 	$resultDepoDisburse['response_code'] = '1111';
					// 	$resultDepoDisburse['response_desc'] = 'unhold tabungan gagal';
					// 	$resultDepoDisburse['datetime'] = '0000-00-00T00:00:00+00:00';
					// 	$resultDepoDisburse['uuid'] = '-';
					// 	$resultDepoDisburse['ref'] = '-';
					// }

					// $serviceCheck = ($resultDepoDisburse['response_code'] == '0000') ? $serviceCheck : false;

					// // PARSE DATE TIME
					// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultDepoDisburse['datetime']);
					// $traTime = $parseDateTime->format('Y-m-d H:i:s');

					// switch (true) {
					// 	case $resultDepoDisburse['response_code'] == '0000':
					// 		$traStatus = 1;
					// 		break;
					// 	case $resultDepoDisburse['response_code'] == 'XT' || $resultDepoDisburse['response_code'] == '0012' || $resultDepoDisburse['response_code'] == '9999':
					// 		$traStatus = 3;
					// 		break;
					// 	case $resultDepoDisburse['response_code'] == '1111':
					// 		$traStatus = 5;
					// 		break;
					// 	default:
					// 		$traStatus = 4;
					// 		break;
					// }

					// INSERT TO T BG TRANSACTION
					$insertData = [
						'TRANSACTION_ID' => $traId,
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'BG_NUMBER' => $data['databg']['BG_NUMBER'],
						'SERVICE' => 5, // TRANSFER BTN
						'SOURCE_ACCT' => $existSumberDana['Source Account Number'],
						'SOURCE_ACCT_CCY' => $existSumberDana['Source Account CCY'],
						'SOURCE_ACCT_NAME' => $existSumberDana['Source Account Name'],
						'CHARGE_ACCT' => '',
						'BENEF_SWIFT_CODE' => '',
						'BENEF_ACCT' => $reppkAcct ?: 'XXX',
						'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
						'TRA_CCY' => 'IDR',
						'TRA_AMOUNT' => $existSumberDana["Transaction Amount"],
						'CHARGE_AMOUNT' => '',
						'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
						'REMARKS2' => '',
						'SOURCE_ADDRESS' => '',
						'SOURCE_PHONE' => $detailAcctDeposito['phone_number'] ?: '000',
						'BENEF_ADDRESS' => '',
						'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
						// 'TRA_TIME' => $traTime,
						'TRA_TIME' => '',
						// 'TRA_STATUS' => $traStatus,
						'TRA_STATUS' => '6',
						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
						'UPDATE_APPROVEDBY' => $this->_userIdLogin,
						// 'UUID' => $resultDepoDisburse['uuid'],
						'UUID' => '',
						// 'RESPONSE_CODE' => $resultDepoDisburse['response_code'],
						'RESPONSE_CODE' => '',
						// 'RESPONSE_DESC' => $resultDepoDisburse['response_desc'],
						'RESPONSE_DESC' => '',
						// 'TRA_REF' => $resultDepoDisburse['ref']
						'TRA_REF' => ''
					];

					$this->_db->insert('T_BG_TRANSACTION', $insertData);

					if ($insertData['TRA_STATUS'] == 3) {
						$maxTry = 3;
						$counterTry = 1;
						while (true) {
							$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

							// JIKA SUDAH DICOBA SEBANYAK 3 KALI
							if ($counterTry == $maxTry || $checkTrxTimeout) break;

							$counterTry++;
							sleep(2);
						}

						if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
							$this->runTransaction($insertData);
						}
					}

					// CHECK APAKAH TRANSFER BERHASIL
					$cekStatusTemp = $this->_db->select()
						->from('T_BG_TRANSACTION')
						->where('TRANSACTION_ID = ?', $insertData['TRANSACTION_ID'])
						->query()->fetch();

					// if ($resultDepoDisburse['response_code'] == '0000') {
					if ($cekStatusTemp['TRA_STATUS'] == '1') {
						$this->_db->delete('M_MARGINALDEPOSIT_DETAIL', [
							'CUST_ID = ?' => $data['databg']['BG_INSURANCE_CODE'],
							'MD_ACCT = ?' => $existSumberDana['Source Account Number'],
							'HOLD_SEQUENCE = ?' => $value['HOLD_SEQUENCE']
						]);
					}
				}
			}

			$checkKmkAcct = array_filter($saveAcctSumberDana, function ($item) {
				return $item['Source of Payment'] == '5';
			});

			if ($checkKmkAcct) {

				$getGroupIndex = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['*'])
					->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
					->where('LOWER(PS_FIELDNAME) LIKE ?', '%source of payment%')
					->where('PS_FIELDVALUE = ?', '5')
					->query()->fetch();

				$getKMKAcct = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['*'])
					->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
					->where('ACCT_INDEX = ?', $getGroupIndex['ACCT_INDEX'])
					->query()->fetchAll();

				$existSumberDana = array_combine(array_column($getKMKAcct, 'PS_FIELDNAME'), array_column($getKMKAcct, 'PS_FIELDVALUE'));

				$reppkDetailService = new Service_Account($reppkAcct, '');
				$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

				$detailAcctSumberDanaService = new Service_Account($existSumberDana['Source Account Number'], '');
				$detailAcctSumberDana = $detailAcctSumberDanaService->inquiryAccontInfo();

				// $transferSumberDanaService =  new Service_Account('', '');
				// $resultTransferSumberDana = $transferSumberDanaService->loanDisbursement($existSumberDana['Source Account CCY'], $existSumberDana['Source Account Number'], $existSumberDana['Source Account Name'], 'IDR', $reppkAcct, $reppkDetailAcct['account_name'], round(floatval($existSumberDana['Transaction Amount']), 2), "10000000", "10000000", "10000000", "10000000");

				// // PARSE DATE TIME
				// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSumberDana['datetime']);
				// // $traTime = $parseDateTime->format('Y-m-d H:i:s');

				// switch (true) {
				// 	case $resultTransferSumberDana['response_code'] == '0000':
				// 		$traStatus = 1;
				// 		break;
				// 	case $resultTransferSumberDana['response_code'] == 'XT' || $resultTransferSumberDana['response_code'] == '0012' || $resultTransferSumberDana['response_code'] == '9999':
				// 		$traStatus = 3;
				// 		break;
				// 	case $resultTransferSumberDana['response_code'] == '1111':
				// 		$traStatus = 5;
				// 		break;
				// 	default:
				// 		$traStatus = 4;
				// 		break;
				// }

				// INSERT TO T BG TRANSACTION
				$insertData = [
					'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
					'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
					'BG_NUMBER' => $data['databg']['BG_NUMBER'],
					'SERVICE' => 6, // TRANSFER BTN
					'SOURCE_ACCT' => $existSumberDana['Source Account Number'],
					'SOURCE_ACCT_CCY' => $existSumberDana['Source Account CCY'],
					"SOURCE_ACCT_NAME" => $existSumberDana['Source Account Name'],
					'CHARGE_ACCT' => '',
					'BENEF_SWIFT_CODE' => '',
					'BENEF_ACCT' => $reppkAcct ?: 'XXX',
					'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
					'TRA_CCY' => 'IDR',
					'TRA_AMOUNT' => round(floatval($existSumberDana['Transaction Amount']), 2),
					'CHARGE_AMOUNT' => '',
					'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
					'REMARKS2' => '',
					'SOURCE_ADDRESS' => '',
					'SOURCE_PHONE' => $detailAcctSumberDana['phone_number'] ?: '000',
					'BENEF_ADDRESS' => '',
					'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
					// 'TRA_TIME' => $traTime,
					// 'TRA_TIME' => new Zend_Db_Expr('now()'),
					'TRA_TIME' => '',
					// 'TRA_STATUS' => $traStatus,
					'TRA_STATUS' => '6',
					'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
					'UPDATE_APPROVEDBY' => $this->_userIdLogin,
					// 'UUID' => $resultTransferSumberDana['uuid'],
					'UUID' => '',
					// 'RESPONSE_CODE' => $resultTransferSumberDana['response_code'],
					'RESPONSE_CODE' => '',
					// 'RESPONSE_DESC' => $resultTransferSumberDana['response_desc'],
					'RESPONSE_DESC' => '',
					// 'TRA_REF' => $resultTransferSumberDana['ref']
					'TRA_REF' => ''
				];

				$this->_db->insert('T_BG_TRANSACTION', $insertData);

				if ($insertData['TRA_STATUS'] == 3) {
					$maxTry = 3;
					$counterTry = 1;
					while (true) {
						$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

						// JIKA SUDAH DICOBA SEBANYAK 3 KALI
						if ($counterTry == $maxTry || $checkTrxTimeout) break;

						$counterTry++;
						sleep(2);
					}

					if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
						$this->runTransaction($insertData);
					}
				}
			}
		}

		// SPLIT -----------------------
		// GET MD
		$getMd = $this->_db->select()
			->from('T_BANK_GUARANTEE_SPLIT')
			->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
			->query()->fetchAll();

		// CEK APAKAH ADA ESCROW
		$getEscrow = array_filter($getMd, function ($item) {
			return strtolower($item['ACCT_DESC']) == 'escrow';
		});

		if ($getEscrow) {
			$escrowKey = ($getEscrow) ? array_keys($getEscrow)[0] : false;
			$getEscrow = array_shift($getEscrow);
			unset($getMd[$escrowKey]);
		}

		if ($getMd && false) {
			foreach ($getMd as $key => $acct) {
				# code...
				$callService = new Service_Account($acct['ACCT'], '');
				$acctInfo = $callService->inquiryAccountBalance();

				if (strtolower($acctInfo['account_type']) == 'd') {
					unset($getMd[$key]);
					continue;
				}

				// TABUNGAN
				if (strtolower($acctInfo['account_type']) == 's') {

					// UNLOCK TABUNGAN ---------------------------------------------------------
					// $unlockSavingService = new Service_Account($acctInfo['account_number'], '');
					// $unlockTabungan = $unlockSavingService->unlockSaving([
					// 	'branch_code' => $acct['HOLD_BY_BRANCH'],
					// 	'sequence_number' => $acct['HOLD_SEQUENCE']
					// ]);

					// $serviceCheck = ($unlockTabungan['response_code'] == '0000') ? $serviceCheck : false;

					// // PARSE DATE TIME
					// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockTabungan['datetime']);
					// $traTime = $parseDateTime->format('Y-m-d H:i:s');

					// switch (true) {
					// 	case $unlockTabungan['response_code'] == '0000':
					// 		$traStatus = 1;
					// 		break;
					// 	case $unlockTabungan['response_code'] == 'XT' || $unlockTabungan['response_code'] == '0012' || $unlockTabungan['response_code'] == '9999':
					// 		$traStatus = 3;
					// 		break;
					// 	case $unlockTabungan['response_code'] == '1111':
					// 		$traStatus = 5;
					// 		break;
					// 	default:
					// 		$traStatus = 4;
					// 		break;
					// }

					$tempDetailInfo = $this->getDetailInfoAcct($acctInfo['account_number']);

					// INSERT TO T BG TRANSACTION
					$insertData = [
						'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'BG_NUMBER' => $data['databg']['BG_NUMBER'],
						'SERVICE' => 3, // UNLOCK SAVING
						'SOURCE_ACCT' => $acct['ACCT'],
						'SOURCE_ACCT_CCY' => $tempDetailInfo['currency'],
						'SOURCE_ACCT_NAME' => $tempDetailInfo['account_name'],
						'CHARGE_ACCT' => '',
						'BENEF_SWIFT_CODE' => '',
						'BENEF_ACCT' => '',
						'HOLD_BY_BRANCH' => $acct['HOLD_BY_BRANCH'],
						'HOLD_SEQUENCE' => $acct['HOLD_SEQUENCE'],
						'TRA_CCY' => 'IDR',
						'TRA_AMOUNT' => $acct['AMOUNT'],
						'CHARGE_AMOUNT' => '',
						'REMARKS1' => '',
						'REMARKS2' => '',
						'SOURCE_ADDRESS' => '',
						'SOURCE_PHONE' => '',
						'BENEF_ADDRESS' => '',
						// 'TRA_TIME' => $traTime,
						'TRA_TIME' => '',
						// 'TRA_STATUS' => $traStatus,
						'TRA_STATUS' => '6',
						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
						'UPDATE_APPROVEDBY' => $this->_userIdLogin,
						// 'UUID' => $unlockTabungan['uuid'],
						'UUID' => '',
						// 'RESPONSE_CODE' => $unlockTabungan['response_code'],
						'RESPONSE_CODE' => '',
						// 'RESPONSE_DESC' => $unlockTabungan['response_desc'],
						'RESPONSE_DESC' => '',
						// 'TRA_REF' => $unlockTabungan['ref']
						'TRA_REF' => ''
					];

					$this->_db->insert('T_BG_TRANSACTION', $insertData);

					if ($insertData['TRA_STATUS'] == 3) {
						$maxTry = 3;
						$counterTry = 1;
						while (true) {
							$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

							// JIKA SUDAH DICOBA SEBANYAK 3 KALI
							if ($counterTry == $maxTry || $checkTrxTimeout) break;

							$counterTry++;
							sleep(2);
						}

						if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
							$this->runTransaction($insertData);
						}
					}

					// END UNLOCK TABUNGAN ---------------------------------------------------------

					// TRANSFER TABUNGAN KE REKENING REPPK -------------------------------------------

					$detailAcctSaving = $callService->inquiryAccontInfo();
					$reppkDetailService = new Service_Account($reppkAcct, '');
					$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

					$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

					// $transferData = [
					// 	"SOURCE_ACCOUNT_NUMBER" => $acct['ACCT'],
					// 	"SOURCE_ACCOUNT_NAME" => $detailAcctSaving['account_name'],
					// 	"BENEFICIARY_ACCOUNT_NUMBER" => $reppkAcct ?: 'XXX',
					// 	"BENEFICIARY_ACCOUNT_NAME" => $reppkDetailAcct['account_name'],
					// 	"PHONE_NUMBER_FROM" => $detailAcctSaving['phone_number'] ?: '000',
					// 	"AMOUNT" => round(floatval($acct["AMOUNT"]), 2),
					// 	"PHONE_NUMBER_TO" => $reppkDetailAcct['phone_number'] ?: '000',
					// 	"RATE1" => "10000000",
					// 	"RATE2" => "10000000",
					// 	"RATE3" => "10000000",
					// 	"RATE4" => "10000000",
					// 	// "DESCRIPTION" => strval($traId),
					// 	"DESCRIPTION" => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
					// 	"DESCRIPTION_DETAIL" => substr($data['databg']["BG_NUMBER"], 0, 40),
					// ];

					// if ($traStatus == 1) {
					// 	$transferSavingService =  new Service_TransferWithin($transferData);
					// 	$result = $transferSavingService->sendTransfer();
					// 	$resultTransferSaving = $result["RawResult"];
					// } else {
					// 	$resultTransferSaving['response_code'] = '1111';
					// 	$resultTransferSaving['response_desc'] = 'unhold tabungan gagal';
					// 	$resultTransferSaving['datetime'] = '0000-00-00T00:00:00+00:00';
					// 	$resultTransferSaving['uuid'] = '-';
					// 	$resultTransferSaving['ref'] = '-';
					// }

					// $serviceCheck = ($resultTransferSaving['response_code'] == '0000') ? $serviceCheck : false;

					// // PARSE DATE TIME
					// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSaving['datetime']);
					// $traTime = $parseDateTime->format('Y-m-d H:i:s');

					// switch (true) {
					// 	case $resultTransferSaving['response_code'] == '0000':
					// 		$traStatus = 1;
					// 		break;
					// 	case $resultTransferSaving['response_code'] == 'XT' || $resultTransferSaving['response_code'] == '0012' || $resultTransferSaving['response_code'] == '9999':
					// 		$traStatus = 3;
					// 		break;
					// 	case $resultTransferSaving['response_code'] == '1111':
					// 		$traStatus = 5;
					// 		break;
					// 	default:
					// 		$traStatus = 4;
					// 		break;
					// }

					// INSERT TO T BG TRANSACTION
					$insertData = [
						'TRANSACTION_ID' => $traId,
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'BG_NUMBER' => $data['databg']['BG_NUMBER'],
						'SERVICE' => 1, // TRANSFER BTN
						'SOURCE_ACCT' => $acct['ACCT'],
						'SOURCE_ACCT_NAME' => $detailAcctSaving['account_name'],
						'SOURCE_ACCT_CCY' => $acct['CCY_ID'],
						'CHARGE_ACCT' => '',
						'BENEF_SWIFT_CODE' => '',
						'BENEF_ACCT' => $reppkAcct ?: 'XXX',
						'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
						'TRA_CCY' => 'IDR',
						'TRA_AMOUNT' => $acct["AMOUNT"],
						'CHARGE_AMOUNT' => '',
						'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
						'REMARKS2' => '',
						'SOURCE_ADDRESS' => '',
						'SOURCE_PHONE' => $detailAcctSaving['phone_number'] ?: '000',
						'BENEF_ADDRESS' => '',
						'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
						// 'TRA_TIME' => $traTime,
						'TRA_TIME' => '',
						// 'TRA_STATUS' => $traStatus,
						'TRA_STATUS' => '6',
						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
						'UPDATE_APPROVEDBY' => $this->_userIdLogin,
						// 'UUID' => $resultTransferSaving['uuid'],
						'UUID' => '',
						// 'RESPONSE_CODE' => $resultTransferSaving['response_code'],
						'RESPONSE_CODE' => '',
						// 'RESPONSE_DESC' => $resultTransferSaving['response_desc'],
						'RESPONSE_DESC' => '',
						// 'TRA_REF' => $resultTransferSaving['ref']
						'TRA_REF' => ''
					];

					$this->_db->insert('T_BG_TRANSACTION', $insertData);

					if ($insertData['TRA_STATUS'] == 3) {
						$maxTry = 3;
						$counterTry = 1;
						while (true) {
							$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

							// JIKA SUDAH DICOBA SEBANYAK 3 KALI
							if ($counterTry == $maxTry || $checkTrxTimeout) break;

							$counterTry++;
							sleep(2);
						}

						if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
							$this->runTransaction($insertData);
						}
					}

					continue;
				}

				$acctInfo = $callService->inquiryDeposito();

				// DEPOSITO
				if (strtolower($acctInfo['account_type']) == 't') {
					// UNLOCK DEPOSITO ---------------------------------------------------------
					// $unlockDepositoService = new Service_Account($acctInfo['account_number'], '');
					// $unlockDeposito = $unlockDepositoService->unlockDeposito('T', $acct['HOLD_BY_BRANCH'], $acct['HOLD_SEQUENCE']);

					// $serviceCheck = ($unlockDeposito['response_code'] == '0000') ? $serviceCheck : false;

					// // PARSE DATE TIME
					// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockDeposito['datetime']);
					// $traTime = $parseDateTime->format('Y-m-d H:i:s');

					// switch (true) {
					// 	case $unlockDeposito['response_code'] == '0000':
					// 		$traStatus = 1;
					// 		break;
					// 	case $unlockDeposito['response_code'] == 'XT' || $unlockDeposito['response_code'] == '0012' || $unlockDeposito['response_code'] == '9999':
					// 		$traStatus = 3;
					// 		break;
					// 	case $unlockDeposito['response_code'] == '1111':
					// 		$traStatus = 5;
					// 		break;
					// 	default:
					// 		$traStatus = 4;
					// 		break;
					// }

					$tempDetailInfo = $this->getDetailInfoAcct($acctInfo['account_number']);

					// INSERT TO T BG TRANSACTION
					$insertData = [
						'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'BG_NUMBER' => $data['databg']['BG_NUMBER'],
						'SERVICE' => 2, // UNLOCK DEPOSITO
						'SOURCE_ACCT' => $acct['ACCT'],
						'SOURCE_ACCT_CCY' => $tempDetailInfo['currency'],
						'SOURCE_ACCT_NAME' => $tempDetailInfo['account_name'],
						'CHARGE_ACCT' => '',
						'BENEF_SWIFT_CODE' => '',
						'BENEF_ACCT' => '',
						'HOLD_BY_BRANCH' => $acct['HOLD_BY_BRANCH'],
						'HOLD_SEQUENCE' => $acct['HOLD_SEQUENCE'],
						'TRA_CCY' => 'IDR',
						'TRA_AMOUNT' => $acct['AMOUNT'],
						'CHARGE_AMOUNT' => '',
						'REMARKS1' => '',
						'REMARKS2' => '',
						'SOURCE_ADDRESS' => '',
						'SOURCE_PHONE' => '',
						'BENEF_ADDRESS' => '',
						// 'TRA_TIME' => $traTime,
						'TRA_TIME' => '',
						// 'TRA_STATUS' => $traStatus,
						'TRA_STATUS' => '6',
						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
						'UPDATE_APPROVEDBY' => $this->_userIdLogin,
						// 'UUID' => $unlockDeposito['uuid'],
						'UUID' => '',
						// 'RESPONSE_CODE' => $unlockDeposito['response_code'],
						'RESPONSE_CODE' => '',
						// 'RESPONSE_DESC' => $unlockDeposito['response_desc'],
						'RESPONSE_DESC' => '',
						// 'TRA_REF' => $unlockDeposito['ref']
						'TRA_REF' => ''
					];

					$this->_db->insert('T_BG_TRANSACTION', $insertData);

					if ($insertData['TRA_STATUS'] == 3) {
						$maxTry = 3;
						$counterTry = 1;
						while (true) {
							$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

							// JIKA SUDAH DICOBA SEBANYAK 3 KALI
							if ($counterTry == $maxTry || $checkTrxTimeout) break;

							$counterTry++;
							sleep(2);
						}

						if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
							$this->runTransaction($insertData);
						}
					}

					// END UNLOCK DEPOSITO ---------------------------------------------------------

					// TRANSFER DEPOSITO KE REKENING REPPK -------------------------------------------

					$stateTransfer = true;

					$detailAcctDepositoService = new Service_Account($acct['ACCT'], '');
					$detailAcctDeposito = $detailAcctDepositoService->inquiryAccontInfo();
					$reppkDetailService = new Service_Account($reppkAcct, '');
					$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

					// $stateTransfer = $reppkAcct ? true : false;
					// $stateTransfer = ($reppkDetailAcct['response_code'] == '0000') ? true : false;

					$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

					// if ($traStatus == 1) {
					// 	$depositoDisrburseService =  new Service_Account($acct['ACCT'], '');
					// 	$resultDepoDisburse = $depositoDisrburseService->depositoDisbursement($acct['CCY_ID'], $acct['ACCT'], $detailAcctDeposito['account_name'], 'IDR', $reppkAcct, $reppkDetailAcct['account_name'], $acct['AMOUNT'], '10000000', '10000000', '10000000', '10000000', strval('Klaim BG No ' . $data['databg']['BG_NUMBER']), strval($data['databg']['BG_NUMBER']));
					// } else {
					// 	$resultDepoDisburse['response_code'] = '1111';
					// 	$resultDepoDisburse['response_desc'] = 'unhold tabungan gagal';
					// 	$resultDepoDisburse['datetime'] = '0000-00-00T00:00:00+00:00';
					// 	$resultDepoDisburse['uuid'] = '-';
					// 	$resultDepoDisburse['ref'] = '-';
					// }

					// $serviceCheck = ($resultDepoDisburse['response_code'] == '0000') ? $serviceCheck : false;

					// // PARSE DATE TIME
					// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultDepoDisburse['datetime']);
					// $traTime = $parseDateTime->format('Y-m-d H:i:s');

					// switch (true) {
					// 	case $resultDepoDisburse['response_code'] == '0000':
					// 		$traStatus = 1;
					// 		break;
					// 	case $resultDepoDisburse['response_code'] == 'XT' || $resultDepoDisburse['response_code'] == '0012' || $resultDepoDisburse['response_code'] == '9999':
					// 		$traStatus = 3;
					// 		break;
					// 	case $resultDepoDisburse['response_code'] == '1111':
					// 		$traStatus = 5;
					// 		break;
					// 	default:
					// 		$traStatus = 4;
					// 		break;
					// }

					// INSERT TO T BG TRANSACTION
					$insertData = [
						'TRANSACTION_ID' => $traId,
						'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
						'BG_NUMBER' => $data['databg']['BG_NUMBER'],
						'SERVICE' => 5, // TRANSFER BTN
						'SOURCE_ACCT' => $acct['ACCT'],
						'SOURCE_ACCT_CCY' => $acct['CCY_ID'],
						'SOURCE_ACCT_NAME' => $detailAcctDeposito['account_name'],
						'CHARGE_ACCT' => '',
						'BENEF_SWIFT_CODE' => '',
						'BENEF_ACCT' => $reppkAcct ?: 'XXX',
						'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
						'TRA_CCY' => 'IDR',
						'TRA_AMOUNT' => $acct["AMOUNT"],
						'CHARGE_AMOUNT' => '',
						'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
						'REMARKS2' => '',
						'SOURCE_ADDRESS' => '',
						'SOURCE_PHONE' => $detailAcctDeposito['phone_number'] ?: '000',
						'BENEF_ADDRESS' => '',
						'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
						// 'TRA_TIME' => $traTime,
						'TRA_TIME' => '',
						// 'TRA_STATUS' => $traStatus,
						'TRA_STATUS' => '6',
						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
						'UPDATE_APPROVEDBY' => $this->_userIdLogin,
						// 'UUID' => $resultDepoDisburse['uuid'],
						'UUID' => '',
						// 'RESPONSE_CODE' => $resultDepoDisburse['response_code'],
						'RESPONSE_CODE' => '',
						// 'RESPONSE_DESC' => $resultDepoDisburse['response_desc'],
						'RESPONSE_DESC' => '',
						// 'TRA_REF' => $resultDepoDisburse['ref']
						'TRA_REF' => ''
					];

					$this->_db->insert('T_BG_TRANSACTION', $insertData);

					if ($insertData['TRA_STATUS'] == 3) {
						$maxTry = 3;
						$counterTry = 1;
						while (true) {
							$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

							// JIKA SUDAH DICOBA SEBANYAK 3 KALI
							if ($counterTry == $maxTry || $checkTrxTimeout) break;

							$counterTry++;
							sleep(2);
						}

						if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
							$this->runTransaction($insertData);
						}
					}

					continue;
				}
			}
		}

		// TRF ESCROW TO REPPK if EXIST
		if ($getEscrow && false) {
			// TRANSFER ESCROW KE REKENING REPPK -------------------------------------------

			$detailAcctEscrowService = new Service_Account($getEscrow['ACCT'], '');
			$detailAcctEscrow = $detailAcctEscrowService->inquiryAccontInfo();
			$reppkDetailService = new Service_Account($reppkAcct, '');
			$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();

			$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

			$transferData = [
				"SOURCE_ACCOUNT_NUMBER" => $getEscrow['ACCT'],
				"SOURCE_ACCOUNT_NAME" => $detailAcctEscrow['account_name'],
				"BENEFICIARY_ACCOUNT_NUMBER" => $reppkAcct ?: 'XXX',
				"BENEFICIARY_ACCOUNT_NAME" => $reppkDetailAcct['account_name'],
				"PHONE_NUMBER_FROM" => $detailAcctEscrow['phone_number'] ?: '000',
				"AMOUNT" => round(floatval($getEscrow["AMOUNT"]), 2),
				"PHONE_NUMBER_TO" => $reppkDetailAcct['phone_number'] ?: '000',
				"RATE1" => "10000000",
				"RATE2" => "10000000",
				"RATE3" => "10000000",
				"RATE4" => "10000000",
				// "DESCRIPTION" => strval($traId),
				"DESCRIPTION" => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
				"DESCRIPTION_DETAIL" => substr($data['databg']["BG_NUMBER"], 0, 40),
			];

			$transferSavingService =  new Service_TransferWithin($transferData);
			$result = $transferSavingService->sendTransfer();
			$resultTransferSaving = $result["RawResult"];

			$serviceCheck = ($resultTransferSaving['response_code'] == '0000') ? $serviceCheck : false;

			// PARSE DATE TIME
			$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSaving['datetime']);
			$traTime = $parseDateTime->format('Y-m-d H:i:s');

			switch (true) {
				case $resultTransferSaving['response_code'] == '0000':
					$traStatus = 1;
					break;
				case $resultTransferSaving['response_code'] == 'XT' || $resultTransferSaving['response_code'] == '0012' || $resultTransferSaving['response_code'] == '9999':
					$traStatus = 3;
					break;
				case $resultTransferSaving['response_code'] == '1111':
					$traStatus = 5;
					break;
				default:
					$traStatus = 4;
					break;
			}

			// INSERT TO T BG TRANSACTION
			$insertData = [
				'TRANSACTION_ID' => $traId,
				'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
				'BG_NUMBER' => $data['databg']['BG_NUMBER'],
				'SERVICE' => 1, // TRANSFER BTN
				'SOURCE_ACCT' => $getEscrow['ACCT'],
				'SOURCE_ACCT_CCY' => 'IDR',
				'CHARGE_ACCT' => '',
				'BENEF_SWIFT_CODE' => '',
				'BENEF_ACCT' => $reppkAcct ?: 'XXX',
				'BENEF_ACCT_NAME' => $reppkDetailAcct['account_name'],
				'TRA_CCY' => 'IDR',
				'TRA_AMOUNT' => $getEscrow["AMOUNT"],
				'CHARGE_AMOUNT' => '',
				"SOURCE_ACCT_NAME" => $detailAcctEscrow['account_name'],
				'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
				'REMARKS2' => '',
				'SOURCE_ADDRESS' => '',
				'SOURCE_PHONE' => $detailAcctEscrow['phone_number'] ?: '000',
				'BENEF_ADDRESS' => '',
				'BENEF_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
				'TRA_TIME' => $traTime,
				'TRA_STATUS' => $traStatus,
				'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				'UPDATE_APPROVEDBY' => $this->_userIdLogin,
				'UUID' => $resultTransferSaving['uuid'],
				'RESPONSE_CODE' => $resultTransferSaving['response_code'],
				'RESPONSE_DESC' => $resultTransferSaving['response_desc'],
				'TRA_REF' => $resultTransferSaving['ref']
			];

			$this->_db->insert('T_BG_TRANSACTION', $insertData);

			if ($insertData['TRA_STATUS'] == 3) {
				$maxTry = 3;
				$counterTry = 1;
				while (true) {
					$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

					// JIKA SUDAH DICOBA SEBANYAK 3 KALI
					if ($counterTry == $maxTry || $checkTrxTimeout) break;

					$counterTry++;
					sleep(2);
				}

				if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
					$this->runTransaction($insertData);
				}
			}
		}

		if ($getEscrow) {
			$escrowKey = ($getEscrow) ? array_keys($getEscrow)[0] : false;
			$getEscrow = array_shift($getEscrow);
			unset($getMd[$escrowKey]);
		}

		// TRANSFER REPPK TO REKENING TUJUAN PEMBAYARAN KLAIM
		if ($data['bankcode'] == 200) {
			$data = array_merge($data, ['reppkacct' => $reppkAcct]);
			$result = $this->transferBTN($data, $seqNumber++);
		} else {
			$data = array_merge($data, ['reppkacct' => $reppkAcct]);
			$result = $this->transferOtherBank($data, $seqNumber++);
		}

		$sisaDana = false;

		if ($result['response_code'] == '0000') {
			$reppkDetailService = new Service_Account($reppkAcct, '');
			$reppkDetailAcct = $reppkDetailService->inquiryAccountBalance();

			// PARSE DATE TIME
			$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $reppkDetailAcct['datetime']);
			$traTime = $parseDateTime->format('Y-m-d H:i:s');

			if ($reppkDetailAcct['available_balance'] > 0) {

				$sisaDana = true;

				// INSERT TO T BG TRANSACTION
				$insertData = [
					'TRANSACTION_ID' => $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT),
					'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
					'BG_NUMBER' => $data['databg']['BG_NUMBER'],
					'SERVICE' => 5, // MANUAL SETTLEMENT
					'SOURCE_ACCT' => $reppkAcct,
					'SOURCE_ACCT_CCY' => 'IDR',
					'CHARGE_ACCT' => '',
					'BENEF_SWIFT_CODE' => '',
					'BENEF_ACCT' => '',
					'BENEF_ACCT_NAME' => '',
					'TRA_CCY' => 'IDR',
					'TRA_AMOUNT' => $reppkDetailAcct['available_balance'],
					'CHARGE_AMOUNT' => '',
					"SOURCE_ACCT_NAME" => $reppkDetailAcct['account_name'],
					'REMARKS1' => strval($data['databg']['CLOSE_REF_NUMBER'] . " Manual Settlement"),
					'REMARKS2' => '',
					'SOURCE_ADDRESS' => '',
					'SOURCE_PHONE' => '',
					'BENEF_ADDRESS' => '',
					'BENEF_PHONE' => '',
					'TRA_TIME' => '',
					'TRA_STATUS' => 5,
					'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
					'UPDATE_APPROVEDBY' => $this->_userIdLogin,
					'UUID' => '',
					'RESPONSE_CODE' => $result['response_code'],
					'RESPONSE_DESC' => $result['response_desc'],
					'TRA_REF' => $result['ref']
				];

				$this->_db->insert('T_BG_TRANSACTION', $insertData);
			}
		}

		// CHECK APAKAH ADA MD PRINCIPAL
		$getMd = $this->_db->select()
			->from('T_BANK_GUARANTEE_SPLIT')
			->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
			->query()->fetchAll();
		if ($getMd) {
			$withoutGiro = array_filter($getMd, function ($item) {
				return strtolower($item['ACCT_TYPE']) != 'giro' && strtolower($item['ACCT_DESC']) != 'giro' && strtolower($item['ACCT_TYPE']) != 'd' && strtolower($item['ACCT_DESC']) != 'd';
			});

			foreach ($withoutGiro as $newMd) {

				$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

				$insertData = [
					'TRANSACTION_ID' => $traId,
					'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
					'BG_NUMBER' => $data['databg']['BG_NUMBER'],
					'SERVICE' => 9, // TRANSFER BTN
					'SOURCE_ACCT' => $newMd['ACCT'],
					'SOURCE_ACCT_CCY' => $newMd['CCY_ID'] ?: 'IDR',
					'CHARGE_ACCT' => '',
					'BENEF_SWIFT_CODE' => '',
					'BENEF_ACCT' => '',
					'BENEF_ACCT_NAME' => '',
					'TRA_CCY' => 'IDR',
					'TRA_AMOUNT' => $newMd['AMOUNT'],
					'HOLD_SEQUENCE' => $newMd['HOLD_SEQUENCE'],
					'HOLD_BY_BRANCH' => $newMd['HOLD_BY_BRANCH'],
					'CHARGE_AMOUNT' => '',
					"SOURCE_ACCT_NAME" => $newMd['NAME'],
					'REMARKS1' => 'MD Principal',
					'REMARKS2' => '',
					'SOURCE_ADDRESS' => '',
					'SOURCE_PHONE' => '000',
					'BENEF_ADDRESS' => '',
					'BENEF_PHONE' => '000',
					'TRA_TIME' => '',
					'TRA_STATUS' => '5',
					'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
					'UPDATE_APPROVEDBY' => $this->_userIdLogin,
					'UUID' => '',
					'RESPONSE_CODE' => '',
					'RESPONSE_DESC' => '',
					'TRA_REF' => ''
				];

				$this->_db->insert('T_BG_TRANSACTION', $insertData);
			}
		}

		// CHECK TRANSACTION
		$checkTrx = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('TRA_STATUS != ?', '1')
			->query()->fetch();

		// UPDATE T BG PSLIP
		$dataPslip = array(
			'PS_APPROVED' => new Zend_Db_Expr("now()"),
			'PS_APPROVEDBY' => $this->_userIdLogin,
			// 'PS_STATUS' => ($checkTrx) ? 4 : 5,
			'PS_STATUS' => 6,
			'PS_DONE' => ($checkTrx) ? new Zend_Db_Expr('now()') : ''
		);

		$where['CLOSE_REF_NUMBER = ?'] = $data['databg']['CLOSE_REF_NUMBER'];
		$this->_db->update('T_BG_PSLIP', $dataPslip, $where);

		// SEND TRANSACTION TO WORKER
		Application_Helper_Email::runTransaction($data['databg']['CLOSE_REF_NUMBER']);

		// PINDAH MEMO DARI TEMP KE T BANK GUARANTEE FILE CLOSE
		$memoFile = $this->_db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
			->where('A.CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(FILE_NOTE) = ?', 'memo / surat persetujuan klaim')
			->query()->fetch();

		// INSERT KE T BG FILE CLOSE
		$this->_db->insert('T_BANK_GUARANTEE_FILE_CLOSE', $memoFile);

		// HAPUS MEMO DARI TABEL TEMP
		$this->_db->delete('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
			'CLOSE_REF_NUMBER = ?' => $data['databg']['CLOSE_REF_NUMBER'],
			'LOWER(FILE_NOTE) = ?' => 'memo / surat persetujuan klaim'
		]);

		// INSERT HISTORY
		$historyData = [
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'CUST_ID' => $data['CUST_ID'],
			'DATE_TIME' => new Zend_Db_Expr('now()'),
			'USER_FROM' => 'BANK',
			'USER_LOGIN' => $this->_userIdLogin,
			'BG_REASON' => 'NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'],
			'HISTORY_STATUS' => 19
		];
		$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyData);

		// JIKA SEMUA TRANSAKSI BERHASIL MAKA KIRIM EMAIL
		if ($checkTrx) {
			// SEND EMAIL

			$data['historydata'] = $historyData;

			$getIndexChargeAcctNumber = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
				->query()->fetch();

			$getAcctChargeDetail = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
				->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
				->where('ACCT_INDEX = ?', $getIndexChargeAcctNumber['ACCT_INDEX'])
				->query()->fetchAll();

			$nominal = number_format($getAcctChargeDetail["Transaction Amount"], 2);

			// SEND EMAIL TO PRINCIPAL
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3A', $nominal);
			// if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL_BG'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO OBLIGEE
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3B', $nominal);
			// if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO KANTOR CABANG PENERBIT
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', $nominal);
			// if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// GROUP LINEFACILITY
			// $settings = new Settings();
			// $getGroupFC = $settings->getSettingNew('email_group_insurance');
			// $getGroupFC = explode(',', $getGroupFC);
			// $getGroupFC = array_map('trim', $getGroupFC);
			// if ($getGroupFC) {
			// 	foreach ($getGroupFC as $emailGroup) {
			// 		$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', $nominal);
			// 		if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
			// 	}
			// }

			// // SEND EMAIL TO PRINCIPAL
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_prinsipal', $nominal);
			// if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);

			// // SEND EMAIL TO OBLIGEE
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_obligee', $nominal);
			// if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);

			// // SEND EMAIL TO KANTOR CABANG PENERBIT
			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_proses_penutupan_klaim_penyelesaian_kcp', $nominal);
			// if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);

			// SEND EMAIL TO KANTOR CABANG PENERBIT
			$getInsBranch = $this->_db->select()
				->from('T_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE'])
				->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
				->where('LOWER(PS_FIELDNAME) = ?', 'insurance branch')
				->query()->fetch();

			// $getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', $nominal);
			// if ($getInsBranch['PS_FIELDVALUE'] && $getMd) $this->sendEmail($getInsBranch['PS_FIELDVALUE'], $this->language->_('Pemberitahuan Pembayaran Klaim'), $getEmailTemplate);
		}

		// INSERT LOG
		$privLog = $data['databg']['COUNTER_WARRANTY_TYPE'] == '1' ? 'PPCC' : 'PPNC';
		Application_Helper_General::writeLog($privLog, 'Persetujuan Penyelesaian Klaim untuk BG No : ' . $data['databg']['BG_NUMBER'] . ', Principal : ' . $data['databg']['CUST_ID'] . ', NoRef Penyelesaian : ' . $data['databg']['CLOSE_REF_NUMBER'] . '');

		$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
		$this->_redirect('/notification/success/index');
	}

	private function transferBTN($data, $seqNumber)
	{
		$getIndexChargeAcctNumber = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
			->query()->fetch();

		$getAcctChargeDetail = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('ACCT_INDEX = ?', $getIndexChargeAcctNumber['ACCT_INDEX'])
			->query()->fetchAll();

		$getAcctChargeDetail = array_combine(array_column($getAcctChargeDetail, 'PS_FIELDNAME'), array_column($getAcctChargeDetail, 'PS_FIELDVALUE'));

		$detailAcctChargeService = new Service_Account($getAcctChargeDetail['Beneficiary Account Number'], '');
		$detailAcctCharge = $detailAcctChargeService->inquiryAccontInfo();
		$reppkDetailService = new Service_Account($data['reppkacct'], '');
		$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();
		$reppkDetailBalance = $reppkDetailService->inquiryAccountBalance();

		$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

		// $transferData = [
		// 	"SOURCE_ACCOUNT_NUMBER" => $data['reppkacct'],
		// 	"SOURCE_ACCOUNT_NAME" => $reppkDetailAcct['account_name'],
		// 	"BENEFICIARY_ACCOUNT_NUMBER" => $getAcctChargeDetail['Beneficiary Account Number'],
		// 	"BENEFICIARY_ACCOUNT_NAME" => $detailAcctCharge['account_name'],
		// 	"PHONE_NUMBER_FROM" => $reppkDetailAcct['phone_number'] ?: '000',
		// 	"AMOUNT" => round(floatval($getAcctChargeDetail["Transaction Amount"]), 2),
		// 	"PHONE_NUMBER_TO" => $detailAcctCharge['phone_number'] ?: '000',
		// 	"RATE1" => "10000000",
		// 	"RATE2" => "10000000",
		// 	"RATE3" => "10000000",
		// 	"RATE4" => "10000000",
		// 	// "DESCRIPTION" => strval($traId),
		// 	"DESCRIPTION" => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
		// 	"DESCRIPTION_DETAIL" => substr($data['databg']["BG_NUMBER"], 0, 40),
		// ];

		// $transferSavingService =  new Service_TransferWithin($transferData);
		// $result = $transferSavingService->sendTransfer();
		// $resultTransferSaving = $result["RawResult"];

		// // PARSE DATE TIME
		// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $resultTransferSaving['datetime']);
		// $traTime = $parseDateTime->format('Y-m-d H:i:s');

		// switch (true) {
		// 	case $resultTransferSaving['response_code'] == '0000':
		// 		$traStatus = 1;
		// 		break;
		// 	case $resultTransferSaving['response_code'] == 'XT' || $resultTransferSaving['response_code'] == '0012' || $resultTransferSaving['response_code'] == '9999':
		// 		$traStatus = 3;
		// 		break;
		// 	case $resultTransferSaving['response_code'] == '1111':
		// 		$traStatus = 5;
		// 		break;
		// 	default:
		// 		$traStatus = 4;
		// 		break;
		// }

		// INSERT TO T BG TRANSACTION
		$insertData = [
			'TRANSACTION_ID' => $traId,
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'SERVICE' => 1, // TRANSFER BTN
			'SOURCE_ACCT' => $data['reppkacct'] ?: 'XXX',
			'SOURCE_ACCT_CCY' => 'IDR',
			'CHARGE_ACCT' => '',
			'BENEF_SWIFT_CODE' => '',
			'BENEF_ACCT' => $getAcctChargeDetail['Beneficiary Account Number'],
			'BENEF_ACCT_NAME' => $detailAcctCharge['account_name'],
			'TRA_CCY' => $getAcctChargeDetail['Beneficiary Account CCY'],
			'TRA_AMOUNT' => $getAcctChargeDetail["Transaction Amount"],
			'CHARGE_AMOUNT' => '',
			"SOURCE_ACCT_NAME" => $reppkDetailAcct['account_name'],
			'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
			'REMARKS2' => '',
			'SOURCE_ADDRESS' => '',
			'SOURCE_PHONE' => $reppkDetailAcct['phone_number'] ?: '000',
			'BENEF_ADDRESS' => '',
			'BENEF_PHONE' => $detailAcctCharge['phone_number'] ?: '000',
			// 'TRA_TIME' => $traTime,
			'TRA_TIME' => '',
			// 'TRA_STATUS' => $traStatus,
			'TRA_STATUS' => '6',
			'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
			'UPDATE_APPROVEDBY' => $this->_userIdLogin,
			// 'UUID' => $resultTransferSaving['uuid'],
			'UUID' => '',
			// 'RESPONSE_CODE' => $resultTransferSaving['response_code'],
			'RESPONSE_CODE' => '',
			// 'RESPONSE_DESC' => $resultTransferSaving['response_desc'],
			'RESPONSE_DESC' => '',
			// 'TRA_REF' => $resultTransferSaving['ref']
			'TRA_REF' => ''
		];

		$this->_db->insert('T_BG_TRANSACTION', $insertData);

		if ($insertData['TRA_STATUS'] == 3) {
			$maxTry = 3;
			$counterTry = 1;
			while (true) {
				$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

				// JIKA SUDAH DICOBA SEBANYAK 3 KALI
				if ($counterTry == $maxTry || $checkTrxTimeout) break;

				$counterTry++;
				sleep(2);
			}

			if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
				$this->runTransaction($insertData);
			}
		}

		if ($insertData['RESPONSE_CODE'] == '0000') {
			$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3B', 0);
			if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO PRINCIPAL
			$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3A', 0);
			if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL_BG'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO KANTOR CABANG PENERBIT
			$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
			if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 1) {
				// FULL COVER
				$settings = new Settings();
				$getGroupFC = $settings->getSettingNew('email_group_cashcoll');
				$getGroupFC = explode(',', $getGroupFC);
				$getGroupFC = array_map('trim', $getGroupFC);
				if ($getGroupFC) {
					foreach ($getGroupFC as $emailGroup) {
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
						if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
					}
				}
			}

			if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 2) {
				// GROUP LINEFACILITY
				$settings = new Settings();
				$getGroupFC = $settings->getSettingNew('email_group_linefacility');
				$getGroupFC = explode(',', $getGroupFC);
				$getGroupFC = array_map('trim', $getGroupFC);
				if ($getGroupFC) {
					foreach ($getGroupFC as $emailGroup) {
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
						if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
					}
				}
			}

			if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 3) {
				// GROUP ASURANSI
				$settings = new Settings();
				$getGroupFC = $settings->getSettingNew('email_group_insurance');
				$getGroupFC = explode(',', $getGroupFC);
				$getGroupFC = array_map('trim', $getGroupFC);
				if ($getGroupFC) {
					foreach ($getGroupFC as $emailGroup) {
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
						if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
					}
				}

				// // GET MD
				// $getMd = $this->_db->select()
				// 	->from('T_BANK_GUARANTEE_SPLIT')
				// 	->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
				// 	->query()->fetchAll();

				$getInsBranch = $this->_db->select()
					->from('T_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE'])
					->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER_SPESIAL'])
					->where('LOWER(PS_FIELDNAME) = ?', 'insurance branch')
					->query()->fetch();

				if ($getInsBranch['PS_FIELDVALUE']) {
					$getInsBranchEmail = $this->_db->select()
						->from('M_INS_BRANCH')
						->where('INS_BRANCH_CODE = ?', $getInsBranch['PS_FIELDVALUE'])
						->query()->fetch();

					$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3D', 0);
					$this->sendEmail($getInsBranchEmail['INS_BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
				}
			}
		}

		return $resultTransferSaving;
	}

	public function validateldapAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();

		$userId = $this->_request->getParam('credential');
		$userId = $AESMYSQL->decrypt(urldecode($userId), uniqid());

		$passLdap = $this->_request->getParam('password');

		$authAdapter = new SGO_Auth_Adapter_Ldap($userId, $passLdap);

		$checkLDAP = $authAdapter->verifyldap();

		$resultLDAP = json_decode($checkLDAP, true);

		if ($resultLDAP['status'] != 1) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'message' => $this->language->_('password salah')]);
			return true;
		}

		header('Content-Type: application/json');
		echo json_encode(['error' => false, 'message' => '']);
		return true;
	}

	public function trxdetailAction()
	{
		$this->_helper->layout()->setLayout('popup');

		$AESMYSQL = new Crypt_AESMYSQL();

		$trxId = $this->_request->getParam('trxid');
		$trxId = $AESMYSQL->decrypt(urldecode($trxId), uniqid());

		$closeRefNumber = substr($trxId, 0, -2);

		$bgdata = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
			->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
			->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLAIM_FIRST_VERIFIED'))
			->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
			->joinleft(array('F' => 'T_BG_PSLIP'), 'F.BG_NUMBER = A.BG_NUMBER', array('F.PS_STATUS', 'F.TYPE', 'F.CLOSE_REF_NUMBER'))
			// ->where('A.CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->where('F.CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->query()->fetch();

		$this->view->getBG = $bgdata;

		$detailTrx = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('TRANSACTION_ID = ?', $trxId)
			->query()->fetch();

		$this->view->detailTrx = $detailTrx;

		// CHECK PROGRESS PEMBAHARUAN
		try {
			//code...
			$progressPembaharuan = $this->_db->select()
				->from('TEMP_TRANSACTION_STATUS_MANUAL_UPDATE')
				->where('TRANSACTION_ID = ?', $trxId)
				->where('SUGGESTION_STATUS = ?', '1')
				->query()->fetch();
		} catch (\Throwable $th) {
			//throw $th;
			Zend_Debug::dump($th);
			die();
		}

		$this->view->progress = ($progressPembaharuan) ? true : false;

		$sourceAcctInfo = $this->getDetailInfoAcct($detailTrx['SOURCE_ACCT']);

		$this->view->sourceAcctInfo = $sourceAcctInfo;

		$bankCode = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->where('LOWER(PS_FIELDNAME) = ?', 'swift code')
			->query()->fetch();

		$bankCode = $bankCode['PS_FIELDVALUE'] ?: 200;

		$getBankName = $this->_db->select()
			->from('M_DOMESTIC_BANK_TABLE')
			->where('SWIFT_CODE = ?', $bankCode)
			->query()->fetch();

		$this->view->getBankName = $getBankName;

		$config = Zend_Registry::get('config');

		$transCode = $config['transaction']['list']['code'];
		$transDesc = $config['transaction']['list']['desc'];

		$arrListService = array_combine($transCode, $transDesc);

		$this->view->arrListService = $arrListService;

		$arrStatusTransaction = [
			1 => 'Sukses',
			2 => 'Sukses dengan Pembaharuan Status',
			3 => 'Time Out',
			4 => 'Gagal',
			5 => 'Menunggu Pembayaran',
			6 => 'Pending Service'
		];

		if ($detailTrx['SERVICE'] == '7' || $detailTrx['SERVICE'] == '8') {
			$feeAcctDetail = $this->getDetailInfoAcct($detailTrx['CHARGE_ACCT']);

			$this->view->feeAcctDetail = $feeAcctDetail;
		}

		$this->view->arrStatusTransaction = $arrStatusTransaction;

		$this->view->getBG = $bgdata;

		// FOR DOWNLOAD PDF ---------------- ********
		if ($this->_request->getParam('pdf')) {
			$viewHtml = $this->view->render('claimobligeedetail/generatepdf/trxdetail/index.phtml');

			$layout_path = $this->_helper->layout()->getLayoutPath();

			$layout_print = new Zend_Layout();
			$layout_print->setLayoutPath($layout_path) // assuming your layouts are in the same directory, otherwise change the path
				->setLayout('newpopup');

			$layout_print->content = $viewHtml;

			// echo $layout_print->render();
			// echo $viewHtml;
			$this->_helper->download->pdfModif(null, null, null, $this->language->_('Penutupan Bank Garansi Transaction ID : ') . ' ( ' . $trxId . ' )', $viewHtml);
			die();
		}
		// FOR DOWNLOAD PDF ------------------------------- ****************
	}

	private function getDetailInfoAcct($acctnumber)
	{
		$callService = new Service_Account($acctnumber, '');
		$resultAcctInfo = $callService->inquiryAccontInfo();

		$getCif = $resultAcctInfo['cif'];

		$callServiceCif = new Service_Account('', '', '', '', '', $getCif);
		$resultCifAccount = $callServiceCif->inquiryCIFAccount();

		$filterAcct = array_shift(array_filter($resultCifAccount['accounts'], function ($item) use ($acctnumber) {
			return ltrim($item['account_number'], '0') == ltrim($acctnumber, '0');
		}));

		return array_merge($resultAcctInfo, $filterAcct);
	}

	private function transferOtherBank($data, $seqNumber)
	{
		$getIndexClaimAcctNumber = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
			->query()->fetch();

		$getAcctClaimDetail = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('ACCT_INDEX = ?', $getIndexClaimAcctNumber['ACCT_INDEX'])
			->query()->fetchAll();

		$getAcctClaimDetail = array_combine(array_map('strtolower', array_column($getAcctClaimDetail, 'PS_FIELDNAME')), array_column($getAcctClaimDetail, 'PS_FIELDVALUE'));

		$claimTransferType = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(PS_FIELDNAME) = ?', 'claim transfer type')
			->query()->fetch();

		$claimTransferType = $claimTransferType['PS_FIELDVALUE'];

		$bankCode = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(PS_FIELDNAME) = ?', 'swift code')
			->query()->fetch();

		$bankCode = $bankCode['PS_FIELDVALUE'];

		// TIPE TRANSFER
		$typeTranfer = ($claimTransferType == '1') ? 'SKNGN' : 'RTGSG';

		$callServiceRunningNumber = new Service_Account('', '');
		$runningNumberResult = $callServiceRunningNumber->inquiryRunningNumber([
			'type' => $typeTranfer
		]);
		$remittenceProduct = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['remittance_product'] : '00';
		$runningNumber = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['running_number'] : '00';

		$remittenceString = implode('', [$data['databg']['BG_BRANCH'], '01', $remittenceProduct, str_pad($runningNumber, 7, '0', STR_PAD_LEFT)]);

		// $detailAcctChargeService = new Service_Account($getAcctClaimDetail['Beneficiary Account Number'], '');
		// $detailAcctCharge = $detailAcctChargeService->inquiryAccontInfo();

		$reppkDetailService = new Service_Account($data['reppkacct'], '');
		$reppkDetailAcct = $reppkDetailService->inquiryAccontInfo();
		$reppkDetailBalance = $reppkDetailService->inquiryAccountBalance();

		$getIndexChargeAcctDetail = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(PS_FIELDNAME) = ?', 'charge account number')
			->query()->fetch();

		$getAcctChargeDetail = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('ACCT_INDEX = ?', $getIndexChargeAcctDetail['ACCT_INDEX'])
			->query()->fetchAll();

		$getAcctChargeDetail = array_combine(array_map('strtolower', array_column($getAcctChargeDetail, 'PS_FIELDNAME')), array_column($getAcctChargeDetail, 'PS_FIELDVALUE'));

		// FEE AMOUNT CHARGE 
		$settings = new Settings();
		$global_charges_skn = $settings->getSetting('global_charges_skn');
		$global_charges_rtgs = $settings->getSetting('global_charges_rtgs');

		$traId = $data['databg']['CLOSE_REF_NUMBER'] . str_pad($seqNumber++, 2, '0', STR_PAD_LEFT);

		// $transferData = [
		// 	'transfer_type' => $claimTransferType,
		// 	'remittance' => $remittenceString,
		// 	'source_account_number' => $data['reppkacct'],
		// 	'source_account_name' => $reppkDetailAcct['account_name'],
		// 	'source_address' => substr($reppkDetailAcct['address'], 0, 40),
		// 	'beneficiary_bank_code' => $bankCode,
		// 	'beneficiary_account_number' => $getAcctClaimDetail['beneficiary account number'],
		// 	'beneficiary_account_name' => $getAcctClaimDetail['beneficiary account name'],
		// 	'beneficiary_address' => $getAcctClaimDetail['beneficiary address'],
		// 	'amount' => $getAcctClaimDetail['transaction amount'],
		// 	// 'remark1' => $getAcctClaimDetail['remark 1'],
		// 	'remark1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
		// 	'remark2' => $traId,
		// 	'remark3' => $data['databg']['BG_NUMBER'],
		// 	'fee' => ($data['requests']['transferType'] == '1') ? $global_charges_skn : $global_charges_rtgs,
		// 	'account_fee' => $getAcctChargeDetail['charge account number'],
		// ];

		// $transferOtherService =  new Service_Account('', '');
		// $result = $transferOtherService->transferDomestic($transferData);

		// // PARSE DATE TIME
		// $parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $result['datetime']);
		// $traTime = $parseDateTime->format('Y-m-d H:i:s');

		// switch (true) {
		// 	case $result['response_code'] == '0000':
		// 		$traStatus = 1;
		// 		break;
		// 	case $result['response_code'] == 'XT' || $result['response_code'] == '0012' || $result['response_code'] == '9999':
		// 		$traStatus = 3;
		// 		break;
		// 	case $result['response_code'] == '1111':
		// 		$traStatus = 5;
		// 		break;
		// 	default:
		// 		$traStatus = 4;
		// 		break;
		// }

		// INSERT TO T BG TRANSACTION
		$insertData = [
			'TRANSACTION_ID' => $traId,
			'CLOSE_REF_NUMBER' => $data['databg']['CLOSE_REF_NUMBER'],
			'BG_NUMBER' => $data['databg']['BG_NUMBER'],
			'SERVICE' => ($data['requests']['transferType'] == 1) ? 7 : 8, // TRANSFER SKN = 7 / RTGS = 8
			'SOURCE_ACCT' => $data['reppkacct'] ?: 'XXX',
			"SOURCE_ACCT_NAME" => $reppkDetailAcct['account_name'],
			'SOURCE_ACCT_CCY' => 'IDR',
			'SOURCE_ADDRESS' => $reppkDetailAcct['address'],
			'SOURCE_PHONE' => '',
			'CHARGE_ACCT' => $getAcctChargeDetail['charge account number'],
			'CHARGE_AMOUNT' => ($data['requests']['transferType'] == '1') ? $global_charges_skn : $global_charges_rtgs,
			'BENEF_SWIFT_CODE' => $getAcctClaimDetail['swift code'],
			'BENEF_ACCT' => $getAcctClaimDetail['beneficiary account number'],
			'BENEF_ACCT_NAME' => $getAcctClaimDetail['beneficiary account name'],
			'BENEF_ADDRESS' => $getAcctClaimDetail['beneficiary address'],
			'TRA_CCY' => 'IDR',
			'TRA_AMOUNT' => $getAcctClaimDetail['transaction amount'],
			'REMARKS1' => strval('Klaim BG No ' . $data['databg']['BG_NUMBER']),
			'REMARKS2' => '',
			'BENEF_PHONE' => '',
			// 'TRA_TIME' => $traTime,
			'TRA_TIME' => '',
			// 'TRA_STATUS' => $traStatus,
			'TRA_STATUS' => '6',
			'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
			'UPDATE_APPROVEDBY' => $this->_userIdLogin,
			// 'UUID' => $result['uuid'],
			'UUID' => '',
			// 'RESPONSE_CODE' => $result['response_code'],
			'RESPONSE_CODE' => '',
			// 'RESPONSE_DESC' => $result['response_desc'],
			'RESPONSE_DESC' => '',
			// 'TRA_REF' => $result['ref']
			'TRA_REF' => ''
		];

		$this->_db->insert('T_BG_TRANSACTION', $insertData);

		if ($insertData['TRA_STATUS'] == 3) {
			$maxTry = 3;
			$counterTry = 1;
			while (true) {
				$checkTrxTimeout = $this->cekTimeoutPerTrx($insertData['TRANSACTION_ID']);

				// JIKA SUDAH DICOBA SEBANYAK 3 KALI
				if ($counterTry == $maxTry || $checkTrxTimeout) break;

				$counterTry++;
				sleep(2);
			}

			if ($counterTry == $maxTry && !$checkTrxTimeout && false) {
				$this->runTransaction($insertData);
			}
		}

		if ($insertData['RESPONSE_CODE'] == '0000') {
			$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3B', 0);
			if ($data['databg']['RECIPIENT_EMAIL']) $this->sendEmail($data['databg']['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO PRINCIPAL
			$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3A', 0);
			if ($data['databg']['CUST_EMAIL']) $this->sendEmail($data['databg']['CUST_EMAIL_BG'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			// SEND EMAIL TO KANTOR CABANG PENERBIT
			$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
			if ($data['databg']['BRANCH_EMAIL']) $this->sendEmail($data['databg']['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);

			if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 1) {
				// FULL COVER
				$settings = new Settings();
				$getGroupFC = $settings->getSettingNew('email_group_cashcoll');
				$getGroupFC = explode(',', $getGroupFC);
				$getGroupFC = array_map('trim', $getGroupFC);
				if ($getGroupFC) {
					foreach ($getGroupFC as $emailGroup) {
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
						if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
					}
				}
			}

			if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 2) {
				// GROUP LINEFACILITY
				$settings = new Settings();
				$getGroupFC = $settings->getSettingNew('email_group_linefacility');
				$getGroupFC = explode(',', $getGroupFC);
				$getGroupFC = array_map('trim', $getGroupFC);
				if ($getGroupFC) {
					foreach ($getGroupFC as $emailGroup) {
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
						if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
					}
				}
			}

			if ($data['databg']['COUNTER_WARRANTY_TYPE'] == 3) {
				// GROUP ASURANSI
				$settings = new Settings();
				$getGroupFC = $settings->getSettingNew('email_group_insurance');
				$getGroupFC = explode(',', $getGroupFC);
				$getGroupFC = array_map('trim', $getGroupFC);
				if ($getGroupFC) {
					foreach ($getGroupFC as $emailGroup) {
						$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3C', 0);
						if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
					}
				}

				// GET MD
				// $getMd = $this->_db->select()
				// 	->from('T_BANK_GUARANTEE_SPLIT')
				// 	->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER'])
				// 	->query()->fetchAll();

				$getInsBranch = $this->_db->select()
					->from('T_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE'])
					->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER_SPESIAL'])
					->where('LOWER(PS_FIELDNAME) = ?', 'insurance branch')
					->query()->fetch();

				if ($getInsBranch['PS_FIELDVALUE']) {
					$getInsBranchEmail = $this->_db->select()
						->from('M_INS_BRANCH')
						->where('INS_BRANCH_CODE = ?', $getInsBranch['PS_FIELDVALUE'])
						->query()->fetch();

					$getEmailTemplate = $this->prepareForSendEmail($data, 'bemailtemplate_3D', 0);
					$this->sendEmail($getInsBranchEmail['INS_BRANCH_EMAIL'], $this->language->_('Pemberitahuan Pembayaran Klaim BG No. ' . $data['databg']['BG_NUMBER']), $getEmailTemplate);
				}
			}
		}

		return $result;
	}

	public function inquiryloanAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();

		$acctNumber = $this->_request->getParam('acctnumber');
		$amount = $this->_request->getParam('amount');
		$kmkAmount = $this->_request->getParam('kmk_amount');

		$amount = $AESMYSQL->decrypt(urldecode($amount), uniqid());

		if (!$acctNumber) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'nothing']);
			return true;
		}

		$detailAcctInfo = $this->getDetailInfoAcct($acctNumber);

		if (strtolower($detailAcctInfo['type']) != 'l' && $detailAcctInfo['response_code'] == '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening yang dimasukkan harus tipe Loan']);
			return true;
		}

		$callService = new Service_Account($acctNumber, '');
		$getAcctInfo = $callService->inquiryAccontInfo();
		$getAcctInqLoan = $callService->inquiryLoan();

		if (!$getAcctInqLoan || $getAcctInqLoan['response_code'] != '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$callServiceCif = new Service_Account('', '', '', '', '', $getAcctInfo['cif']);
		$getCifList = $callServiceCif->inquiryCIFAccount();

		if (!$getCifList || $getCifList['response_code'] != '0000') {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$getCifDetailAcct = array_filter($getCifList['accounts'], function ($item) use ($acctNumber) {
			return ltrim($item['account_number'], '0') == ltrim($acctNumber, '0');
		});

		$getCifDetailAcct = array_shift(array_values($getCifDetailAcct));

		if (!$getCifDetailAcct) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Rekening tidak ditemukan']);
			return true;
		}

		$countLimit = floatval($getAcctInqLoan['original_amount']) - floatval($getAcctInqLoan['release_amount']);

		if ($countLimit < floatval($kmkAmount)) {
			header('Content-Type: application/json');
			echo json_encode(['error' => true, 'result' => 'Limit tidak cukup']);
			return true;
		}

		$result = array_merge($getAcctInfo, $getCifDetailAcct);
		$result = array_intersect_key($result, ['account_name' => '', 'type_desc' => '', 'currency' => '', 'product_type' => '', 'product_desc' => '', 'produc_desc' => '', 'type' => '']);

		header('Content-Type: application/json');
		echo json_encode(['error' => false, 'result' => $result]);
		return true;
	}

	public function mdinsAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decodeBgRegNumber = urldecode($bgRegNumber);
		$bgRegNumber = $AESMYSQL->decrypt($decodeBgRegNumber, uniqid());

		if ($bgRegNumber == null) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'nothing']);
			return true;
		}

		$model = new bgclosingworkflow_Model_Approvedetailclosing();

		$bgdata = $model->getDetailBg('none', $bgRegNumber);

		$getMdIns = $this->_db->select()
			->from('M_MARGINALDEPOSIT_DETAIL')
			->where('CUST_ID = ?', $bgdata['BG_INSURANCE_CODE'])
			->query()->fetchAll();

		foreach ($getMdIns as $key => $acct) {
			$getMdIns[$key]['detail'] = $this->getDetailInfoAcct($acct['MD_ACCT']);
		}

		$mdAmount = 0;

		$haveEscrow = false;

		$htmlResult = "
        <table id=\"marginalDepositInsTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal Eksisting') . "</th>
                <th>" . $this->language->_('Nominal Pembayaran') . "</th>
                <th><div class='w-100 d-flex justify-content-center'><input type='checkbox' class='hidden' id='checkmdinsall'></div></th>
            </thead>
            <tbody>
            ";

		$noMd =
			"
			<tr>
				<td colspan='8' class='text-center'>" . $this->language->_('Tidak ada marginal deposit') . "</td>
			</tr>
			";

		if ($getMdIns) {
			$countShow = 0;
			foreach ($getMdIns as $key => $value) {

				// $getInfoDetail = $this->getDetailInfoAcct($value['MD_ACCT']);

				$callService = new Service_Account($value['MD_ACCT'], '');

				// if (strtolower($getDetailAcc['type_desc']) == 'giro' && strtolower($getDetailAcc['product_type']) != 'k2') continue;

				if (strtolower($value['MD_ACCT_TYPE']) == 'escrow') {
					$haveEscrow = true;
					$inqbalance = $callService->inquiryAccountBalance();

					// if (intval($inqbalance['available_balance']) == 0) {
					// 	// hapus data atau ??
					// 	continue;
					// } else
					$mdAmount += $value['GUARANTEE_AMOUNT'];
				} else {
					// if (strtolower($getInfoDetail['type_desc']) == 'giro') continue;

					// jika deposito
					if (strtolower($value['detail']['type']) == 'd') {
						// $inqbalance = $callService->inquiryDeposito();

						// if (strtolower($inqbalance['hold_description']) == 'n') {
						// 	// hapus data atau ??
						// 	continue;
						// } else
						$mdAmount += $value['GUARANTEE_AMOUNT'];
					}
					// end jika deposito

					// jika tabungan
					else {
						$inqbalance = $callService->inqLockSaving();

						$cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
							return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
						}));

						// if (!$cekSaving) {
						// 	// hapus data atau
						// 	continue;
						// } else
						$mdAmount += $value['GUARANTEE_AMOUNT'];
					}
					// end jika tabungan
				}

				$countShow += 1;
				$htmlResult .= "
				<tr>
					<td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
					<td>" . $value['MD_ACCT'] . "</td>
					<td>" . $value['detail']['account_name'] . "</td>
					<td>" . ($value['detail']['type_desc'] . ' ' . $value['detail']['product_type']) . "</td>
					<td>" . $value['detail']['currency'] . "</td>
					<td>" . number_format($value['GUARANTEE_AMOUNT'], 2) . "</td>
					<td><input type='hidden' name='indexacct[]' value='" . $value['MD_ACCT'] . "'><input type='text' class='form-control input-amount-use' onkeydown='return forceNumberOwn(event)' name='amountmduse[]' data-amount='" . $value['GUARANTEE_AMOUNT'] . "' readonly><div class='for-error'></td>
					<td><div class='w-100 d-flex justify-content-center'><input type='checkbox' name='indexacctmd[]' class='md-ins-check'></div></td>
				</tr>";
			}

			if ($countShow === 0) {
				$htmlResult .= $noMd;
			}
		} else {
			$htmlResult .= $noMd;
		}

		$htmlResult .= "</tbody></table>";
		$htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='amountmdinsurance'></input>";

		echo $htmlResult;
	}

	public function mdinsrepairAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decodeBgRegNumber = urldecode($bgRegNumber);
		$bgRegNumber = $AESMYSQL->decrypt($decodeBgRegNumber, uniqid());

		if ($bgRegNumber == null) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'nothing']);
			return true;
		}

		$model = new bgclosingworkflow_Model_Approvedetailclosing();

		$bgdata = $model->getDetailBg('none', $bgRegNumber);

		$getCloseRef = $this->_db->select()
			->from('T_BG_PSLIP')
			->where('BG_NUMBER = ?', $bgdata['BG_NUMBER'])
			->query()->fetch();

		$getDetailClose = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $getCloseRef['CLOSE_REF_NUMBER'])
			->where('ACCT_INDEX != ?', '1')
			->query()->fetchAll();

		$getDetailCloseAcct = array_filter($getDetailClose, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'source account number';
		});
		$getDetailCloseAcct = array_column($getDetailCloseAcct, 'PS_FIELDVALUE');

		$getDetailCloseAmount = array_filter($getDetailClose, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'transaction amount';
		});
		$getDetailCloseAmount = array_combine($getDetailCloseAcct, array_column($getDetailCloseAmount, 'PS_FIELDVALUE'));

		$getMdIns = $this->_db->select()
			->from('M_MARGINALDEPOSIT_DETAIL')
			->where('CUST_ID = ?', $bgdata['BG_INSURANCE_CODE'])
			->query()->fetchAll();

		foreach ($getMdIns as $key => $acct) {
			$getMdIns[$key]['detail'] = $this->getDetailInfoAcct($acct['MD_ACCT']);
		}

		$mdAmount = 0;
		$amountExist = 0;

		$haveEscrow = false;

		$htmlResult = "
        <table id=\"marginalDepositInsTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal Eksisting') . "</th>
                <th>" . $this->language->_('Nominal Pembayaran') . "</th>
                <th><div class='w-100 d-flex justify-content-center'><input type='checkbox' class='hidden' id='checkmdinsall' disabled></div></th>
            </thead>
            <tbody>
            ";

		$noMd =
			"
			<tr>
				<td colspan='8' class='text-center'>" . $this->language->_('Tidak ada marginal deposit') . "</td>
			</tr>
			";

		if ($getMdIns) {
			$countShow = 0;

			$mdAmount += array_sum(array_column($getMdIns, 'GUARANTEE_AMOUNT'));
			foreach ($getMdIns as $key => $value) {

				// $getInfoDetail = $this->getDetailInfoAcct($value['MD_ACCT']);

				$callService = new Service_Account($value['MD_ACCT'], '');

				// if (strtolower($getDetailAcc['type_desc']) == 'giro' && strtolower($getDetailAcc['product_type']) != 'k2') continue;

				if (strtolower($value['MD_ACCT_TYPE']) == 'escrow') {
					$haveEscrow = true;
					$inqbalance = $callService->inquiryAccountBalance();

					// if (intval($inqbalance['available_balance']) == 0) {
					// 	// hapus data atau ??
					// 	continue;
					// } else
					$amountExist += $getDetailCloseAmount[$value['MD_ACCT']];
				} else {
					// if (strtolower($getInfoDetail['type_desc']) == 'giro') continue;

					// jika deposito
					if (strtolower($value['detail']['type']) == 'd') {
						// $inqbalance = $callService->inquiryDeposito();

						// if (strtolower($inqbalance['hold_description']) == 'n') {
						// 	// hapus data atau ??
						// 	continue;
						// } else
						$amountExist += $getDetailCloseAmount[$value['MD_ACCT']];
					}
					// end jika deposito

					// jika tabungan
					else {
						$inqbalance = $callService->inqLockSaving();

						$cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
							return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
						}));

						// if (!$cekSaving) {
						// 	// hapus data atau
						// 	continue;
						// } else
						$amountExist += $getDetailCloseAmount[$value['MD_ACCT']];
					}
					// end jika tabungan
				}

				$countShow += 1;
				$htmlResult .= "
				<tr>
					<td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
					<td>" . $value['MD_ACCT'] . "</td>
					<td>" . $value['detail']['account_name'] . "</td>
					<td>" . ($value['detail']['type_desc'] . ' ' . $value['detail']['product_type']) . "</td>
					<td>" . $value['detail']['currency'] . "</td>
					<td>" . number_format($value['GUARANTEE_AMOUNT'], 2) . "</td>
					<td><input type='hidden' name='indexacct[]' value='" . $value['MD_ACCT'] . "'><input type='text' class='form-control input-amount-use' onkeydown='return forceNumberOwn(event)' name='amountmduse[]' data-amount='" . $value['GUARANTEE_AMOUNT'] . "' " . (in_array($value['MD_ACCT'], $getDetailCloseAcct) ? "value='" . number_format($getDetailCloseAmount[$value['MD_ACCT']], 2) . "'" : '') . " readonly><div class='for-error'></td>
					<td><div class='w-100 d-flex justify-content-center'><input type='checkbox' name='indexacctmd[]' class='md-ins-check' " . (in_array($value['MD_ACCT'], $getDetailCloseAcct) ? 'checked' : 'disabled') . "></div></td>
				</tr>";
			}

			if ($countShow === 0) {
				$htmlResult .= $noMd;
			}
		} else {
			$htmlResult .= $noMd;
		}

		$htmlResult .= "</tbody></table>";
		$htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='amountmdinsurance'></input>";
		$htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='amountexist'></input>";

		echo $htmlResult;
	}

	public function mdinsapproveAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decodeBgRegNumber = urldecode($bgRegNumber);
		$bgRegNumber = $AESMYSQL->decrypt($decodeBgRegNumber, uniqid());

		if ($bgRegNumber == null) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'nothing']);
			return true;
		}

		$model = new bgclosingworkflow_Model_Approvedetailclosing();

		$bgdata = $model->getDetailBg('none', $bgRegNumber);

		$getCloseRef = $this->_db->select()
			->from('T_BG_PSLIP')
			->where('BG_NUMBER = ?', $bgdata['BG_NUMBER'])
			->query()->fetch();

		$getDetailClose = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $getCloseRef['CLOSE_REF_NUMBER'])
			->where('ACCT_INDEX != ?', '1')
			->query()->fetchAll();

		$getDetailCloseAcct = array_filter($getDetailClose, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'source account number';
		});
		$getDetailCloseAcct = array_column($getDetailCloseAcct, 'PS_FIELDVALUE');

		$getDetailCloseAmount = array_filter($getDetailClose, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'transaction amount';
		});
		$getDetailCloseAmount = array_combine($getDetailCloseAcct, array_column($getDetailCloseAmount, 'PS_FIELDVALUE'));

		$getMdIns = $this->_db->select()
			->from('M_MARGINALDEPOSIT_DETAIL')
			->where('CUST_ID = ?', $bgdata['BG_INSURANCE_CODE'])
			->query()->fetchAll();

		foreach ($getMdIns as $key => $acct) {
			$getMdIns[$key]['detail'] = $this->getDetailInfoAcct($acct['MD_ACCT']);
		}

		$mdAmount = 0;
		$amountExist = 0;

		$haveEscrow = false;

		$htmlResult = "
        <table id=\"marginalDepositInsTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal Eksisting') . "</th>
                <th style='text-align: left'>" . $this->language->_('Nominal Pembayaran') . "</th>
            </thead>
            <tbody>
            ";

		$noMd =
			"
			<tr>
				<td colspan='7' class='text-center'>" . $this->language->_('Tidak ada marginal deposit') . "</td>
			</tr>
			";

		if ($getMdIns) {
			$countShow = 0;

			$mdAmount += array_sum(array_column($getMdIns, 'GUARANTEE_AMOUNT'));

			foreach ($getMdIns as $key => $value) {

				if (!in_array($value['MD_ACCT'], $getDetailCloseAcct)) continue;

				// $getInfoDetail = $this->getDetailInfoAcct($value['MD_ACCT']);

				$callService = new Service_Account($value['MD_ACCT'], '');

				// if (strtolower($getDetailAcc['type_desc']) == 'giro' && strtolower($getDetailAcc['product_type']) != 'k2') continue;

				if (strtolower($value['MD_ACCT_TYPE']) == 'escrow') {
					$haveEscrow = true;
					$inqbalance = $callService->inquiryAccountBalance();

					// if (intval($inqbalance['available_balance']) == 0) {
					// 	// hapus data atau ??
					// 	continue;
					// } else
					$amountExist += $getDetailCloseAmount[$value['MD_ACCT']];
				} else {
					// if (strtolower($getInfoDetail['type_desc']) == 'giro') continue;

					// jika deposito
					if (strtolower($value['detail']['type']) == 'd') {
						// $inqbalance = $callService->inquiryDeposito();

						// if (strtolower($inqbalance['hold_description']) == 'n') {
						// 	// hapus data atau ??
						// 	continue;
						// } else
						$amountExist += $getDetailCloseAmount[$value['MD_ACCT']];
					}
					// end jika deposito

					// jika tabungan
					else {
						$inqbalance = $callService->inqLockSaving();

						$cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
							return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
						}));

						// if (!$cekSaving) {
						// 	// hapus data atau
						// 	continue;
						// } else
						$amountExist += $getDetailCloseAmount[$value['MD_ACCT']];
					}
					// end jika tabungan
				}

				$countShow += 1;
				$htmlResult .= "
				<tr>
					<td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
					<td>" . $value['MD_ACCT'] . "</td>
					<td>" . $value['detail']['account_name'] . "</td>
					<td>" . ($value['detail']['type_desc'] . ' ' . $value['detail']['product_type']) . "</td>
					<td>" . $value['detail']['currency'] . "</td>
					<td>" . number_format($value['GUARANTEE_AMOUNT'], 2) . "</td>
					<td>" . number_format($getDetailCloseAmount[$value['MD_ACCT']], 2) . "</td>
				</tr>";
			}

			if ($countShow === 0) {
				$htmlResult .= $noMd;
			}
		} else {
			$htmlResult .= $noMd;
		}

		$htmlResult .= "</tbody></table>";
		$htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='amountmdinsurance'></input>";
		$htmlResult .= "<input type='hidden' value='" . $amountExist . "' id='amountexist'></input>";

		echo $htmlResult;
	}

	public function trxchangestatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$AESMYSQL = new Crypt_AESMYSQL();

		$params = $this->_request->getParams();
		$memoFile = $_FILES['memo'];

		$trxId = $AESMYSQL->decrypt(urldecode($params['trxid']), uniqid());

		try {
			//code...
			$getDetailTrx = $this->_db->select()
				->from(['TBT' => 'T_BG_TRANSACTION'], [
					'*',
					'CUST_DETAIL' => new Zend_Db_Expr('(SELECT CONCAT(CUST_ID, \'~|~\',CUST_NAME) FROM M_CUSTOMER WHERE CUST_ID = (SELECT CUST_ID FROM T_BANK_GUARANTEE WHERE CLOSE_REF_NUMBER = TBT.CLOSE_REF_NUMBER LIMIT 1) LIMIT 1)')
				])
				->where('TRANSACTION_ID = ?', $trxId)
				->query()->fetch();
		} catch (\Throwable $th) {
			//throw $th;
			Zend_Debug::dump($th);
			die();
		}

		$extension = array_pop(explode('.', $memoFile['name']));

		// UPLOAD FILE
		$attachmentDestination   = UPLOAD_PATH . '/document/memopembaharuan/';
		mkdir($attachmentDestination);

		$date = date("dmy");
		$time = date("his");
		$namaBaru = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . '_' . rtrim(substr($memoFile['name'], 0, 63), '.pdf') . '.' . $extension;

		try {
			//code...
			move_uploaded_file($memoFile['tmp_name'], $attachmentDestination . $namaBaru);
		} catch (\Throwable $th) {
			//throw $th;
			Zend_Debug::dump($th);
			die();
		}

		// INSERT TO GLOBAL CHANGES
		$changesInformation = 'Pembaharuan status manual, transaction id = ' . $getDetailTrx['TRANSACTION_ID'];
		$changesType = 'E';
		$mainTableName = 'T_BG_TRANSACTION';
		$tempTableName = 'TEMP_TRANSACTION_STATUS_MANUAL_UPDATE';

		$custDetail = explode('~|~', $getDetailTrx['CUST_DETAIL']);
		$custId = $custDetail[0];
		$custName = $custDetail[1];

		$change_id    = $this->suggestionWaitingApproval('Penyelesaian Manual', $changesInformation, $changesType, null, $mainTableName, $tempTableName, $custId, $custName, $custId, $custName, 'manualupdate');

		// INSERT TEMP DATA
		$insertData = [
			'CHANGES_ID' => $change_id,
			'CLOSE_REF_NUMBER' => $getDetailTrx['CLOSE_REF_NUMBER'],
			'TRANSACTION_ID' => $getDetailTrx['TRANSACTION_ID'],
			'UPDATE_SUGGESTED' => new Zend_Db_Expr('now()'),
			'UPDATE_SUGGESTEDBY' => $this->_userIdLogin,
			'UPDATE_MEMO_FILE' => $namaBaru,
			'UPDATE_REMARKS' => $params['catatan'],
			'SUGGESTION_STATUS' => 1
		];

		try {
			$this->_db->insert('TEMP_TRANSACTION_STATUS_MANUAL_UPDATE', $insertData);
		} catch (\Throwable $th) {
			//throw $th;
			$this->_db->rollBack();
		}

		// WRITE LOG 
		$privilege = 'PMST';
		$logAction = 'Mengajukan Usulan Pembaharuan Manual Status Transaksi Gagal menjadi Sukses untuk BG No : ' . $getDetailTrx['BG_NUMBER'] . ', NoRef Transaksi : ' . $getDetailTrx['TRANSACTION_ID'];
		Application_Helper_General::writeLog($privilege, $logAction);

		$this->_redirect('/notification/popupsuccess/index');
	}

	public function memofileAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$AESMYSQL = new Crypt_AESMYSQL();
		$trxId = $this->_request->getParam('trxid');

		$trxId = $AESMYSQL->decrypt(urldecode($trxId), uniqid());

		$detailTrx = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('TRANSACTION_ID = ?', $trxId)
			->query()->fetch();

		$explodeNamaFile = explode('_', $detailTrx['UPDATE_MEMO_FILE']);
		$namaFile = str_replace([$explodeNamaFile[0], $explodeNamaFile[1], $explodeNamaFile[2], $explodeNamaFile[3]], '', $detailTrx['UPDATE_MEMO_FILE']);
		$namaFile = trim($namaFile, '_');

		$attachmentDestination   = UPLOAD_PATH . '/document/memopembaharuan/';
		if (file_exists($attachmentDestination . $detailTrx['UPDATE_MEMO_FILE']) && $detailTrx['UPDATE_MEMO_FILE']) {
			return $this->_helper->download->file(implode('_', [$detailTrx['TRANSACTION_ID'], 'MEMO', $namaFile]), $attachmentDestination . $detailTrx['UPDATE_MEMO_FILE']);
		} else {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode([
				'result' => 'file tidak ditemukan'
			]);
		}
	}

	private function prepareForSendEmail($data, $emailTemplate, $nominal)
	{
		$settings = new Settings();
		$allSetting = $settings->getAllSetting();

		$config = Zend_Registry::get('config');

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);

		$bgclosingStatus   = $config["bgclosinghistory"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosinghistory"]["status"]["code"];
		$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);

		$getEscrowRelease = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->query()->fetchAll();

		$benefAcctNumber = array_shift(array_filter($getEscrowRelease, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'beneficiary account number';
		}))['PS_FIELDVALUE'];

		$benefAcctName = array_shift(array_filter($getEscrowRelease, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'beneficiary account name';
		}))['PS_FIELDVALUE'];

		$getBankName = $this->_db->select()
			->from(['mdbt' => 'M_DOMESTIC_BANK_TABLE'], ['mdbt.BANK_NAME'])
			->where('mdbt.SWIFT_CODE = ?', new Zend_Db_Expr('(SELECT PS_FIELDVALUE FROM TEMP_BANK_GUARANTEE_DETAIL_CLOSE tbgdc WHERE tbgdc.CLOSE_REF_NUMBER = ' . $this->_db->quote($data['databg']['CLOSE_REF_NUMBER']) . ' AND LOWER(tbgdc.PS_FIELDNAME) = \'swift code\')'))
			->query()->fetch();

		// T BANK GUARANTEE UNDERLYING
		$getAllUnderlying = $this->_db->select()
			->from('T_BANK_GUARANTEE_UNDERLYING')
			->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER_SPESIAL'])
			->query()->fetchAll();

		$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

		$underlyingForEmail = '
		<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
			<br>
		<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>
		';

		$getBgCurrency = $this->_db->select()
			->from('T_BANK_GUARANTEE_DETAIL')
			->where('BG_REG_NUMBER = ?', $data['databg']['BG_REG_NUMBER_SPESIAL'])
			->where('LOWER(PS_FIELDNAME) = ?', 'currency')
			->query()->fetch();

		$getBgCurrency = $getBgCurrency ? $getBgCurrency['PS_FIELDVALUE'] : '-';

		// GET BENEF ACCOUNT
		$getBenefAccount = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
			->where('ACCT_INDEX = ?', 1)
			->query()->fetch();

		$getBenefAccount = $getBenefAccount['PS_FIELDVALUE'];

		$checkTransaction = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $data['databg']['CLOSE_REF_NUMBER'])
			->where('BENEF_ACCT = ?', $getBenefAccount)
			->query()->fetch();

		$dataEmail = [
			'[[close_ref_number]]' => $data['databg']['CLOSE_REF_NUMBER'],
			'[[bg_number]]' => $data['databg']['BG_NUMBER'],
			'[[bg_subject]]' => $data['databg']["BG_SUBJECT"],
			'[[usage_purpose]]' => ucwords(strtolower($data['databg']['USAGE_PURPOSE_DESC'])),
			'[[cust_name]]' => $data['databg']["CUST_NAME"],
			'[[cust_cp]]' => $data['databg']["CUST_CP"],
			'[[contract_name]]' => $underlyingForEmail,
			'[[bg_amount]]' => implode(' ', [$getBgCurrency, number_format($data['databg']['BG_AMOUNT'], 2)]),
			'[[transaction_id]]' => $checkTransaction['TRANSACTION_ID'],
			'[[transaction_amount]]' => implode(' ', [$checkTransaction['TRA_CCY'], number_format($checkTransaction['TRA_AMOUNT'], 2)]),
			'[[transaction_date]]' => date('d-m-Y', strtotime($checkTransaction['TRA_TIME'])),
			'[[tra_status]]' => 'Sukses',
			'[[recipient_name]]' => $data['databg']['RECIPIENT_NAME'],
			'[[recipient_cp]]' => $data['databg']['RECIPIENT_CP'],
			'[[counter_warranty_type]]' => $arrbgcg[$data['databg']['COUNTER_WARRANTY_TYPE']],
			'[[change_type]]' => $arrbgclosingType[$data['databg']['CHANGE_TYPE_CLOSE']],
			'[[suggestion_status]]' => $arrbgclosingStatus[15],
			'[[repair_notes]]' => $data['historydata']['BG_REASON'],
			'[[recipient_name]]' => $data['databg']['RECIPIENT_NAME'],
			'[[beneficiary_account_number]]' => $benefAcctNumber,
			'[[time_period_start]]' => date('d M Y', strtotime($data['databg']['TIME_PERIOD_START'])),
			'[[time_period_end]]' => date('d M Y', strtotime($data['databg']['TIME_PERIOD_END'])),
			'[[bank_name]]' => $getBankName['BANK_NAME'],
			'[[beneficiary_account_name]]' => $benefAcctName,
			'[[insurance_name]]' => $data['databg']['INS_NAME'] ?: '-',
			'[[insurance_branch_name]]' => $data['databg']['INS_BRANCH_NAME']
		];

		$getEmailTemplate = $allSetting[$emailTemplate];
		$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);

		return $getEmailTemplate;
	}

	private function sendEmail($emailAddress, $subjectEmail, $emailTemplate)
	{
		$setting = new Settings();
		$allSetting = $setting->getAllSetting();

		$data = [
			'[[master_bank_name]]' => $allSetting["master_bank_name"],
			'[[master_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_bank_email]]' => $allSetting["master_bank_email"],
			'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
		];

		$getEmailTemplate = strtr($emailTemplate, $data);
		Application_Helper_Email::sendEmail($emailAddress, $subjectEmail, $getEmailTemplate);
	}

	private function combineCodeAndDesc($arrayCode, $arrayDesc)
	{
		return array_combine($arrayCode, array_map(function ($arrayMap) use ($arrayDesc, $arrayCode) {
			return $arrayDesc[$arrayMap];
		}, array_keys($arrayCode)));
	}

	public function getBuserEmailWithPrivi(array $privi, string $bgBranch): array
	{
		$priviJoin = array_map(function ($item) {
			return "'$item'";
		}, $privi);
		$priviJoin = join(',', $priviJoin);

		$result = $this->_db->select()
			->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL'])
			->where('MB.BGROUP_ID IN (?)', new Zend_Db_Expr('(SELECT BGROUP_ID FROM M_BPRIVI_GROUP mbg WHERE mbg.BPRIVI_ID IN(' . $priviJoin . ') GROUP BY BGROUP_ID)'))
			->where('MB.BUSER_EMAIL != ?', '');

		if ($privi != 'PPNC') $result->where('BUSER_BRANCH IN (?)', new Zend_Db_Expr('(SELECT ID FROM M_BRANCH WHERE BRANCH_CODE = ' . $this->_db->quote($bgBranch) . ')'));

		$result = $result->group('MB.BUSER_EMAIL')
			->query()->fetchAll();

		return $result;
	}

	public function cekTimeout($closeRefNumber)
	{
		$getAllTimeoutTrans = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->where('TRA_STATUS = ?', 3)
			->query()->fetchAll();

		$transCodeMapping = [
			1 => '8766',
			5 => '8771',
			6 => '8772',
			7 => '8768',
			8 => '8769',
		];

		$svcCekStatus = new SGO_Soap_ClientUser();

		foreach ($getAllTimeoutTrans as $trans) {

			if (in_array($trans['SERVICE'], [2, 3])) {
				if ($trans['SERVICE'] == 2) {
					$inqLockSaving = new Service_Account($trans['SOURCE_ACCT'], '');
					$resultInqLockSaving = $inqLockSaving->inquiryLockDeposito();

					if ($resultInqLockSaving['response_code'] == '0404') {
						$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
							'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
						]);
					}

					if ($resultInqLockSaving['response_code'] == '0000') {
						$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 4], [
							'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
						]);
					}
				} else {
					$inqLockSaving = new Service_Account($trans['SOURCE_ACCT'], '');
					$resultInqLockSaving = $inqLockSaving->inqLockSaving();

					if ($resultInqLockSaving['response_code'] == '0000') {
						$cekSeqNumber = array_filter($resultInqLockSaving['lock'], function ($item) use ($trans) {
							return $item['sequence_number'] == $trans['HOLD_SEQUENCE'];
						});

						if (empty($cekSeqNumber)) {
							$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
								'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
							]);
						} else {
							$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 4], [
								'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
							]);
						}
					}
				}
			} else {
				$svcCekStatus->callapi("inqtransfer", null, [
					"ref_trace" => $trans['TRA_REF'],
					"trans_code" => $transCodeMapping[$trans['SERVICE']]
				]);

				$result = $svcCekStatus->getResult();

				if ($result['response_code'] == '0000') {

					$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
						'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
					]);
				} else {
					$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 4], [
						'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
					]);
				}
			}
		}

		$getAllTrans = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->where('TRA_STATUS != ?', 1)
			->query()->fetchAll();

		if (!$getAllTrans) {
			$this->_db->update('T_BG_PSLIP', ['PS_STATUS' => 5], [
				'CLOSE_REF_NUMBER = ?' => $closeRefNumber
			]);
		}

		$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
		$this->_redirect('/notification/success/index');
	}

	public function cekTimeoutPerTrx($transactionId)
	{
		$transaction = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('TRANSACTION_ID = ?', $transactionId)
			->query()->fetch();

		$transCodeMapping = [
			1 => '8766',
			5 => '8771',
			6 => '8772',
			7 => '8768',
			8 => '8769',
		];

		$success = false;

		$svcCekStatus = new SGO_Soap_ClientUser();

		if ($transaction) {
			$trans = $transaction;

			if (in_array($trans['SERVICE'], [2, 3])) {
				if ($trans['SERVICE'] == 2) {
					$inqLockSaving = new Service_Account($trans['SOURCE_ACCT'], '');
					$resultInqLockSaving = $inqLockSaving->inquiryLockDeposito();

					if ($resultInqLockSaving['response_code'] == '0404') {
						$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
							'TRANSACTION_ID = ?' => $transactionId
						]);

						$success = true;
					}
				} else {
					$inqLockSaving = new Service_Account($trans['SOURCE_ACCT'], '');
					$resultInqLockSaving = $inqLockSaving->inqLockSaving();

					if ($resultInqLockSaving['response_code'] == '0000') {
						$cekSeqNumber = array_filter($resultInqLockSaving['lock'], function ($item) use ($trans) {
							return $item['sequence_number'] == $trans['HOLD_SEQUENCE'];
						});

						if (empty($cekSeqNumber)) {
							$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
								'TRANSACTION_ID = ?' => $transactionId
							]);

							$success = true;
						}
					}
				}
			} else {
				$svcCekStatus->callapi("inqtransfer", null, [
					"ref_trace" => $trans['TRA_REF'],
					"trans_code" => $transCodeMapping[$trans['SERVICE']]
				]);

				$result = $svcCekStatus->getResult();

				if ($result['response_code'] == '0000') {

					$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
						'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
					]);

					$success = true;
				} else {
					// $this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 4], [
					// 	'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
					// ]);

					$success = false;
				}
			}
		}

		return $success;
	}

	public function runTransaction($transaction)
	{
		// MAPPING SERVICE
		switch ($transaction['SERVICE']) {
			case 1: // inhouse
				$transferData = [
					"SOURCE_ACCOUNT_NUMBER" => $transaction['SOURCE_ACCT'],
					"SOURCE_ACCOUNT_NAME" => $transaction['SOURCE_ACCT_NAME'],
					"BENEFICIARY_ACCOUNT_NUMBER" => $transaction['BENEF_ACCT'],
					"BENEFICIARY_ACCOUNT_NAME" => $transaction['BENEF_ACCT_NAME'],
					"PHONE_NUMBER_FROM" => $transaction['SOURCE_PHONE'],
					"AMOUNT" => floatval($transaction["TRA_AMOUNT"]),
					"PHONE_NUMBER_TO" => $transaction['BENEF_PHONE'],
					"RATE1" => "10000000",
					"RATE2" => "10000000",
					"RATE3" => "10000000",
					"RATE4" => "10000000",
					"DESCRIPTION" => $transaction['REMARKS1'],
					"DESCRIPTION_DETAIL" => 'EBG',
				];

				$transferInhouse =  new Service_TransferWithin($transferData);
				$result = $transferInhouse->sendTransfer();
				$resultTransferInhouse = $result["RawResult"];

				$this->updateTransaction($transaction['TRANSACTION_ID'], $resultTransferInhouse);
				break;
			case 2: // unlock deposito
				$unlockDeposito = new Service_Account($transaction["SOURCE_ACCT"], null);
				$unlockDeposito = $unlockDeposito->unlockDeposito('T', $transaction['HOLD_BY_BRANCH'], $transaction['HOLD_SEQUENCE']);

				$this->updateTransaction($transaction['TRANSACTION_ID'], $unlockDeposito);
				break;
			case 3: // unhold saving
				$unholdSavingService = new Service_Account($transaction['SOURCE_ACCT'], '');
				$unholdSavingService = $unholdSavingService->unlockSaving([
					'branch_code' => $transaction['HOLD_BY_BRANCH'],
					'sequence_number' => $transaction['HOLD_SEQUENCE']
				]);

				$this->updateTransaction($transaction['TRANSACTION_ID'], $unholdSavingService);
				break;
			case 5: // deposito disbursement
				$depositoDisrburseService =  new Service_Account($transaction['SOURCE_ACCT'], '');
				$resultDepoDisburse = $depositoDisrburseService->depositoDisbursement(
					$transaction['SOURCE_ACCT_CCY'],
					$transaction['SOURCE_ACCT'],
					$transaction['SOURCE_ACCT_NAME'],
					$transaction['SOURCE_ACCT_CCY'],
					$transaction['BENEF_ACCT'],
					$transaction['BENEF_ACCT_NAME'],
					floatval($transaction['TRA_AMOUNT']),
					'10000000',
					'10000000',
					'10000000',
					'10000000',
					$transaction['REMARKS1'],
					'EBG'
				);

				$this->updateTransaction($transaction['TRANSACTION_ID'], $resultDepoDisburse);
				break;
			case 6: // loan disbursement
				$loanDisburse =  new Service_Account('', '');
				$resultLoanDisburse = $loanDisburse->loanDisbursement(
					$transaction['SOURCE_ACCT_CCY'],
					$transaction['SOURCE_ACCT'],
					$transaction['SOURCE_ACCT_NAME'],
					$transaction['TRA_CCY'],
					$transaction['BENEF_ACCT'],
					$transaction['BENEF_ACCT_NAME'],
					floatval($transaction['TRA_AMOUNT']),
					"10000000",
					"10000000",
					"10000000",
					"10000000"
				);

				$this->updateTransaction($transaction['TRANSACTION_ID'], $resultLoanDisburse);
				break;
			case 7: // skn
				$getBgBranch = $this->_db->select()
					->from(['A' => 'T_BANK_GUARANTEE'], ['A.BG_BRANCH'])
					->where('A.BG_NUMBER = ?', $transaction['BG_NUMBER'])
					->query()->fetch();

				$callServiceRunningNumber = new Service_Account('', '');
				$runningNumberResult = $callServiceRunningNumber->inquiryRunningNumber([
					'type' => 'SKNGN'
				]);
				$remittenceProduct = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['remittance_product'] : '00';
				$runningNumber = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['running_number'] : '00';

				$remittenceString = implode('', [$getBgBranch['BG_BRANCH'], '01', $remittenceProduct, str_pad($runningNumber, 7, '0', STR_PAD_LEFT)]);

				$bankCode = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
					->where('CLOSE_REF_NUMBER = ?', $transaction['CLOSE_REF_NUMBER'])
					->where('LOWER(PS_FIELDNAME) = ?', 'swift code')
					->query()->fetch();

				$bankCode = $bankCode['PS_FIELDVALUE'];

				$transferData = [
					'transfer_type' => '1',
					'remittance' => $remittenceString,
					'source_account_number' => $transaction['SOURCE_ACCT'],
					'source_account_name' => $transaction['SOURCE_ACCT_NAME'],
					'source_address' => substr($transaction['SOURCE_ADDRESS'], 0, 40),
					'beneficiary_bank_code' => $bankCode,
					'beneficiary_account_number' => $transaction['BENEF_ACCT'],
					'beneficiary_account_name' => $transaction['BENEF_ACCT_NAME'],
					'beneficiary_address' => substr($transaction['BENEF_ADDRESS'], 0, 40),
					'amount' => floatval($transaction['TRA_AMOUNT']),
					'remark1' => $transaction['REMARKS1'],
					'remark2' => 'EBG',
					'remark3' => 'EBG',
					'fee' => $transaction['CHARGE_AMOUNT'],
					'account_fee' => $transaction['CHARGE_ACCT'],
				];

				$transferOtherService =  new Service_Account('', '');
				$resultSkn = $transferOtherService->transferDomestic($transferData);

				$this->updateTransaction($transaction['TRANSACTION_ID'], $resultSkn);
				break;
			case 8: // rtgs
				$getBgBranch = $this->_db->select()
					->from(['A' => 'T_BANK_GUARANTEE'], ['A.BG_BRANCH'])
					->where('A.BG_NUMBER = ?', $transaction['BG_NUMBER'])
					->query()->fetch();

				$callServiceRunningNumber = new Service_Account('', '');
				$runningNumberResult = $callServiceRunningNumber->inquiryRunningNumber([
					'type' => 'SKNGN'
				]);
				$remittenceProduct = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['remittance_product'] : '00';
				$runningNumber = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['running_number'] : '00';

				$remittenceString = implode('', [$getBgBranch['BG_BRANCH'], '01', $remittenceProduct, str_pad($runningNumber, 7, '0', STR_PAD_LEFT)]);

				$bankCode = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
					->where('CLOSE_REF_NUMBER = ?', $transaction['CLOSE_REF_NUMBER'])
					->where('LOWER(PS_FIELDNAME) = ?', 'swift code')
					->query()->fetch();

				$bankCode = $bankCode['PS_FIELDVALUE'];

				$transferData = [
					'transfer_type' => '2',
					'remittance' => $remittenceString,
					'source_account_number' => $transaction['SOURCE_ACCT'],
					'source_account_name' => $transaction['SOURCE_ACCT_NAME'],
					'source_address' => substr($transaction['SOURCE_ADDRESS'], 0, 40),
					'beneficiary_bank_code' => $bankCode,
					'beneficiary_account_number' => $transaction['BENEF_ACCT'],
					'beneficiary_account_name' => $transaction['BENEF_ACCT_NAME'],
					'beneficiary_address' => substr($transaction['BENEF_ADDRESS'], 0, 40),
					'amount' => floatval($transaction['TRA_AMOUNT']),
					'remark1' => $transaction['REMARKS1'],
					'remark2' => 'EBG',
					'remark3' => 'EBG',
					'fee' => $transaction['CHARGE_AMOUNT'],
					'account_fee' => $transaction['CHARGE_ACCT'],
				];

				$transferOtherService =  new Service_Account('', '');
				$resultRtgs = $transferOtherService->transferDomestic($transferData);

				$this->updateTransaction($transaction['TRANSACTION_ID'], $resultRtgs);
				break;

			default:
				# code...
				break;
		}
	}

	private function updateTransaction($transactionId, $resultService)
	{
		switch ($resultService['response_code']) {
			case '0000':
				$traStatus = 1;
				break;
			case '9999':
				$traStatus = 3;
				break;
			default:
				$traStatus = 4;
				break;
		}

		$this->_db->update('T_BG_TRANSACTION', [
			'TRA_STATUS' => $traStatus,
			'RESPONSE_CODE' => $resultService['response_code'],
			'RESPONSE_DESC' => $resultService['response_desc'],
			'UUID' => $resultService['uuid'],
			'TRA_TIME' => new Zend_Db_Expr('now()')
		], [
			'TRANSACTION_ID = ?' => $transactionId
		]);
	}
}
