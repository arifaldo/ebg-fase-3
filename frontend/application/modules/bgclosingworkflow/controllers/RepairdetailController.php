<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once("Service/Account.php");

class bgclosingworkflow_repairdetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        // get bg number
        $numb = $this->_getParam('bgnumb');

        // decrypt bg number req
        $AESMYSQL = new Crypt_AESMYSQL();
        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
        $numb = $BG_NUMBER;

        $get_ccy_exist = $this->_db->select()
            ->from("M_MINAMT_CCY", ["CCY_ID"])
            ->query()->fetchAll();

        $this->view->ccy_exist = $get_ccy_exist;

        $Settings = new Settings();

        $toc_ind = $Settings->getSetting('ftemplate_bgclosing_ind');
        $this->view->toc_ind = $toc_ind;
        $toc_eng = $Settings->getSetting('ftemplate_bgclosing_eng');
        $this->view->toc_eng = $toc_eng;

        $allSetting = $settings->getAllSetting();
		$this->view->snkPenutupan = $allSetting['ftemplate_bgclosing_ind'];
		$this->view->snkPelepasanMd = $allSetting['ftemplate_bgclosingmd_ind'];
		$this->view->snkPenerbitanInd = $settings->getSetting('ftemplate_bg_ind');
		$this->view->snkPenerbitanIng = $settings->getSetting('ftemplate_bg_eng');

        $params = $this->_request->getParams();
        if (!empty($numb)) {

            // Guaranted Transaction -----------------------
            $getGuarantedTransanctions = $this->_db->select()
                ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
                ->where('BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $this->view->guarantedTransanctions = $getGuarantedTransanctions;

            $conf = Zend_Registry::get('config');

            $bgdocType         = $conf["bgdoc"]["type"]["desc"];
            $bgdocCode         = $conf["bgdoc"]["type"]["code"];

            $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
            $this->view->arrbgdoc = $arrbgdoc;

            // end Guaranted Transaction -------------------

            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), [
                    '*',
                    'BRANCH_NAME' => new Zend_Db_Expr('(SELECT BRANCH_NAME FROM M_BRANCH MB WHERE MB.BRANCH_CODE = A.BG_BRANCH LIMIT 1)'),
                    'PRINCIPLE_NAME' => new Zend_Db_Expr('(SELECT CUST_NAME FROM M_CUSTOMER MC WHERE MC.CUST_ID = A.CUST_ID LIMIT 1)')
                ])
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_NUMBER = ?', $numb)
                ->query()->fetch();

            $getSplit = $this->_db->select()
                ->from('T_BANK_GUARANTEE_SPLIT')
                ->where('BG_REG_NUMBER = ?', $bgdata['BG_REG_NUMBER'])
                ->query()->fetchAll();

            $this->view->getSplit = $getSplit;

            $principalDetail = $this->_db->select()
                ->from(['MCS' => 'M_CUSTOMER'], [
                    '*',
                    'CITY_NAME' => new Zend_Db_Expr('(SELECT CITY_NAME FROM M_CITYLIST MC WHERE MC.CITY_CODE = MCS.CUST_CITY LIMIT 1)')
                ])
                ->where('CUST_ID = ?', $bgdata['CUST_ID'])
                ->query()->fetch();

            $this->view->principalDetail = $principalDetail;

            $this->view->bgdata = $bgdata;

            // tipe usulan
            $bgChangeTypeCode = $conf['bg']['changetype']['code'];
            $bgChangeTypeDesc = $conf['bg']['changetype']['desc'];
            $bgChangeType = $this->combineCodeAndDesc($bgChangeTypeCode, $bgChangeTypeDesc);

            // tipe usulan
            $bgClosingChangeTypeCode = $conf['bgclosing']['changetype']['code'];
            $bgClosingChangeTypeDesc = $conf['bgclosing']['changetype']['desc'];
            $bgClosingChangeType = $this->combineCodeAndDesc($bgClosingChangeTypeCode, $bgClosingChangeTypeDesc);

            $this->view->bgClosingChangeType = $bgClosingChangeType;

            // get M_PAPER if exist
            $getPaperUsed = $this->_db->select()
                ->from('M_PAPER')
                ->where('NOTES = ?', $bgdata['BG_NUMBER'])
                ->query()->fetchAll();

            $paperUsed = array_column($getPaperUsed, 'PAPER_ID');

            $this->view->paperUsed = $paperUsed;

            $numb = $bgdata['BG_REG_NUMBER'];

            $tempBgClose = $this->_db->select()
                ->from(['TBGC' => 'TEMP_BANK_GUARANTEE_CLOSE'])
                ->where('TBGC.BG_REG_NUMBER = ?', $numb)
                ->orWhere('TBGC.BG_NUMBER = ?', $bgdata['BG_NUMBER'])
                ->where('TBGC.SUGGESTION_STATUS = 8')
                ->query()->fetch();
            $this->view->tempBgClose = $tempBgClose;

            if (!$tempBgClose) {
                $this->_redirect('/bgclosingworkflow/repair');
            }

            $tempBgCloseFile = $this->_db->select()
                ->from('TEMP_BANK_GUARANTEE_FILE_CLOSE')
                ->where('CLOSE_REF_NUMBER = ?', $tempBgClose['CLOSE_REF_NUMBER'])
                ->query()->fetchAll();

            $newTempBgCloseFile = array_combine(array_map(function ($item) {
                return implode('_', explode(' ', strtolower($item)));
            }, array_column($tempBgCloseFile, 'FILE_NOTE')), array_map(function ($item) {
                return trim(str_replace([explode('_', $item)[0], explode('_', $item)[1], explode('_', $item)[2], explode('_', $item)[3]], '', $item), '_');
            }, array_column($tempBgCloseFile, 'BG_FILE')));

            $this->view->tempBgCloseFile = $newTempBgCloseFile;

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $bgdatadetailArray = array_combine(array_map('strtolower', array_column($bgdatadetail, 'PS_FIELDNAME')), array_column($bgdatadetail, 'PS_FIELDVALUE'));

            $this->view->bgdatadetail = $bgdatadetailArray;

            $asuransi = $this->_db->select()
                ->from('M_CUSTOMER', [
                    '*',
                    'INS_BRANCH_NAME' => new Zend_Db_Expr('(SELECT MIB.INS_BRANCH_NAME FROM M_INS_BRANCH MIB WHERE MIB.CUST_ID = "' . $bgdata['BG_INSURANCE_CODE'] . '" LIMIT 1)')
                ])
                ->where('CUST_ID = ?', $bgdata['BG_INSURANCE_CODE'])
                ->query()->fetch();

            $this->view->asuransi = $asuransi;

            $get_others_attachment = $this->_db->select()
                ->from("T_BANK_GUARANTEE_FILE")
                ->where("BG_REG_NUMBER = ?", $numb)
                ->query()->fetchAll();

            $this->view->get_others_attachment = $get_others_attachment;

            if (!empty($bgdata)) {

                $REPAIR_NOTE = $this->_db->select()
                    ->from(
                        array('D' => 'T_BANK_GUARANTEE_HISTORY_CLOSE'),
                        array(
                            'BG_REASON'      => 'D.BG_REASON',
                            '*'
                        )
                    )
                    ->where("D.CLOSE_REF_NUMBER = ?", $tempBgClose['CLOSE_REF_NUMBER'])
                    ->order('D.DATE_TIME DESC')
                    ->query()->fetch();

                $REPAIR_NOTE['CUST_ID'] = $this->_custIdLogin;

                $this->view->reason = $REPAIR_NOTE;

                // get linefacillity
                $paramLimit = array();

                $paramLimit['CUST_ID'] =  $this->_custIdLogin;
                $paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
                $getLineFacility = Application_Helper_General::getLineFacility($paramLimit);


                $this->view->current_limit = $getLineFacility['currentLimit'];
                $this->view->max_limit =  $getLineFacility['plafondLimit'];

                //$this->view->linefacility = $get_linefacility[0];

                // end get linefacility

                $this->view->bankFormatNumber = $data["BG_FORMAT"];

                // if ($data['IS_AMENDMENT'] == 1) {
                //     $this->view->suggestion_type = 'Change';
                // } else if ($data['IS_AMENDMENT'] == 0) {
                //     $this->view->suggestion_type = 'New';
                // }

                // new code untuk amendemen
                $getct        = Zend_Registry::get('config');
                $ctdesc     = $getct["bg"]["changetype"]["desc"];
                $ctcode     = $getct["bg"]["changetype"]["code"];
                $suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

                $this->view->suggestion_type = $suggestion_type;

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                //$config    		= Zend_Registry::get('config');
                $config = Zend_Registry::get('config');
                $BgType         = $config["bg"]["status"]["desc"];
                $BgCode         = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

                $this->view->arrStatus = $arrStatus;


                $comp = "'" . $this->_custIdLogin . "'";
                $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                        ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                        // ->where('A.ACCT_STATUS = ?','5')
                        ->where("A.CUST_ID IN (" . $comp . ")")
                        ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
                );

                $account = array();
                foreach ($acctlist as $key => $value) {
                    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                    $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                    $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                    $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                    $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                    $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
                }
                // echo "<pre>";
                // var_dump($account);die;
                // $acct = array();
                $i = 0;
                foreach ($account as $key => $value) {

                    $acct[$i]['ACCT_NO'] = $value['account_number'];
                    $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                    $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                    $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                    $acct[$i]['ACCT_NAME'] = $value['account_name'];
                    $i++;
                }

                // $this->view->sourceAcct = $acct;

                // echo "<code>acct start =========</code>";
                // echo '<pre>'; 
                // print_r($acct);
                // echo "<code>acct end=========</code>";

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->BG_LANGUAGE = $data['BG_LANGUAGE'];
                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

                // $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                // $param = array('CCY_IN' => 'IDR');
                $param = array('CCY_IN in (?)' => ['IDR', 'USD']);
                $AccArr = $CustomerUser->getAccountsBG($param);
                //}
                if (!empty($AccArr)) {
                    foreach ($AccArr as $i => $value) {

                        $AccArr[$i]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $getAllProduct = $this->_db->select()
                    ->from("M_PRODUCT")
                    ->query()->fetchAll();

                $this->view->AccArr = $AccArr;
                // $this->view->AccArr = $AccArr;

                $type = array('D', 'S', '20', '10');
                $param = array('ACCT_TYPE' => $type);
                //var_dump($param);die;
                //$param = array('ACCT_TYPE' => "20");
                $AccArr2 = $CustomerUser->getAccountsBG($param);

                $newArr2 = [];
                foreach ($AccArr2 as $keyArr => $arrAcct) {

                    if ($arrAcct['ACCT_TYPE'] == '10' || strtolower($arrAcct['ACCT_TYPE']) == 's') continue;

                    $accountTypeCheck = false;
                    $productTypeCheck = false;

                    $svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
                    $result = $svcAccount->inquiryAccountBalance();
                    if ($result['response_code'] == '0000') {
                        $accountType = $result['account_type'];
                        $productType = $result['product_type'];
                    } else {
                        $result = $svcAccount->inquiryDeposito();

                        $accountType = $result['account_type'];
                        $productType = $result['product_type'];
                    }

                    foreach ($getAllProduct as $key => $value) {
                        # code...
                        if ($value['PRODUCT_CODE'] == $accountType) {
                            $accountTypeCheck = true;
                        };

                        if ($value['PRODUCT_PLAN'] == $productType) {
                            $productTypeCheck = true;
                        };
                    }

                    if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
                }

                $this->view->AccArr2 = $newArr2;
                // $this->view->AccArr2 = $AccArr2;

                // preliminary Member
                $selectMP = $this->_db->select()
                    ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
                $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));

                $preliminaryMemberArr = $this->_db->fetchAll($selectMP);
                $this->view->preliminaryMemberArr = $preliminaryMemberArr;

                $select_cust_linefacility = $this->_db->select()
                    ->from(
                        array('A' => 'M_CUST_LINEFACILITY'),
                        array('*')
                    )
                    ->joinLeft(
                        array('B' => 'M_CUSTOMER'),
                        'B.CUST_ID = A.CUST_ID',
                        array('CUST_NAME', 'COLLECTIBILITY_CODE')
                    )
                    ->where("A.CUST_ID = ?", $this->_custIdLogin);
                //->where();
                // $select_cust_linefacility->where("DATE(NOW()) between DATE(A.PKS_START_DATE) AND DATE(A.PKS_EXP_DATE)");
                // ->where("A.STATUS in (2,3,4)");
                //echo $select_cust_linefacility;
                $select_cust_linefacility = $this->_db->fetchRow($select_cust_linefacility);

                if (!empty($select_cust_linefacility)) {
                    $this->view->statusLF = $select_cust_linefacility["STATUS"];
                }

                //usd
                $paramUSD = array('CCY_IN' => 'USD');
                $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
                if (!empty($AccArrUSD)) {
                    foreach ($AccArrUSD as $iUSD => $valueUSD) {
                        $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArrUSD = $AccArrUSD;


                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }


                // echo '<pre>';
                // print_r($conf);

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                foreach ($arrbgType as $key => $val) {
                    $arrbgType_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_purpose                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_bg_purpose                += Application_Helper_Array::listArray($arrbgType_new, 'ID', 'VALUE');
                $this->view->bgpurposeArr       = $list_bg_purpose;
                $this->view->BG_PURPOSE = $data['USAGE_PURPOSE'];

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                $arrbgType["F4299"] = ["Jaminan Pembayaran", "Jaminan Pemeliharaan", "Lainnya (SP2D)"];

                $this->view->arrbgType = $arrbgType;

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                foreach ($arrbgdoc as $key => $val) {
                    $arrbgdoc_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_document_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_document_type                += Application_Helper_Array::listArray($arrbgdoc_new, 'ID', 'VALUE');
                $this->view->documenttypeArr     = $list_document_type;
                $this->view->GT_DOC_TYPE =  $data['GT_DOC_TYPE'];


                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = $this->combineCodeAndDesc($bgcgCode, $bgcgType);
                $this->view->arrbgcg = $arrbgcg;

                foreach ($arrbgcg as $key => $val) {
                    $arrbgcg_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }

                $list_warranty_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_warranty_type                += Application_Helper_Array::listArray($arrbgcg_new, 'ID', 'VALUE');
                $this->view->warrantytypeArr     = $list_warranty_type;
                // $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];


                $this->view->currencyArr = array(
                    '1' => $this->language->_('IDR'),
                    '2' => $this->language->_('USD'),
                );

                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
                foreach ($arrbgpublish as $key => $val) {
                    $arrbgpublish_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_publist                = array('' => ' --- ' . $this->language->_('Any Value') . ' ---');
                $list_bg_publist                += Application_Helper_Array::listArray($arrbgpublish_new, 'ID', 'VALUE');
                $this->view->BG_PUBLISH_ARR     = $list_bg_publist;



                $Settings = new Settings();
                $claim_period = $Settings->getSetting('max_claim_period');

                $this->view->maxCalendarLimit = $claim_period;

                // echo '<pre>';
                // print_r($list_bg_publist); 
                $this->view->BG_PUBLISH = $data['BG_PUBLISH'];

                $this->view->BG_FORMAT = $data['BG_FORMAT'];

                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_AMOUNT = $data['BG_AMOUNT'];

                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];


                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];

                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];

                $this->view->SERVICE = $data['SERVICE'];
                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                // $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];
                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


                if ($data['COUNTER_WARRANTY_TYPE'] == 3) {

                    $getDataInsurance = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE", ["BG_INSURANCE_CODE", "BG_BRANCH"])
                        ->where("COUNTER_WARRANTY_TYPE = '3'")
                        ->where("BG_REG_NUMBER = ? ", $BG_NUMBER)
                        ->query()
                        ->fetchAll();

                    $this->view->resInsuranceCode = $getDataInsurance[0]['BG_INSURANCE_CODE'];
                    $this->view->resInsuranceBranch = $getDataInsurance[0]['BG_BRANCH'];
                } elseif ($data['COUNTER_WARRANTY_TYPE'] == 1) {
                    $getFullConver = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE_SPLIT", ["*"])
                        ->where("BG_REG_NUMBER = ? ", $BG_NUMBER)
                        ->query()
                        ->fetchAll();

                    $this->view->getFullConver = $getFullConver;
                }

                if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                        a.USER_LOGIN,
                        b.`USER_FULLNAME` AS u_name,
                        c.`BUSER_NAME` AS b_name,
                        a.DATE_TIME,
                        a.BG_REASON,
                        a.HISTORY_STATUS,
                        a.BG_REG_NUMBER
                        
                        
                        FROM
                        T_BANK_GUARANTEE_HISTORY AS a
                        LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                        LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                        WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    $result =  $this->_db->fetchAll($selectQuery);
                    if (!empty($result)) {
                        $data['REASON'] = $result['0']['BG_REASON'];
                    }
                    $this->view->reqrepair = true;

                    // $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {

                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];

                                $getInsuranceCustomer = $this->_db->select()
                                    ->from(["A" => "M_CUSTOMER"], ["CUST_ID", "CUST_NAME"])
                                    ->joinLeft(["MCLF" => "M_CUST_LINEFACILITY"], "MCLF.CUST_ID = A.CUST_ID")
                                    ->where("A.CUST_ID = ?", $value["PS_FIELDVALUE"])
                                    // ->where("A.CUST_MODEL = '2'")
                                    // ->where("A.CUST_STATUS = '1'")
                                    // ->where("MCLF.CUST_SEGMENT = ?", "4")
                                    // ->where("MCLF.STATUS = ?", "1")
                                    ->query()->fetchAll();

                                $simpanInsurance = [];

                                $simpanInsurance[0] = ["key" => "-", "value" => "-- Choose one --"];

                                foreach ($getInsuranceCustomer as $key => $customer) {
                                    $getId = $customer["CUST_ID"];
                                    $getName = $customer["CUST_NAME"];
                                    if ($customer["TICKET_SIZE"] == "1") {
                                        $ticket_size = floatval((10 / 100) * $customer["PLAFOND_LIMIT"]);
                                    } else {
                                        $ticket_size = floatval($customer["PLAFOND_LIMIT"]);
                                    }
                                    array_push($simpanInsurance, ["key" => $getId, "value" => $getName, "ticket_size" => $ticket_size]);
                                }

                                $this->view->simpanInsurance = $simpanInsurance;
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Date') {
                                $this->view->paDate =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);

                $download = $this->_getParam('download');
                //print_r($edit);die;
                if ($download) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                }

                $getTransfer = $this->_db->select()
                    ->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
                    ->where('CLOSE_REF_NUMBER = ?', $tempBgClose['CLOSE_REF_NUMBER'])
                    ->query()->fetchAll();

                $getAcctTransfer = array_values(array_map(function ($item) {
                    return $item['PS_FIELDVALUE'];
                }, array_filter($getTransfer, function ($item) {
                    return strtolower($item['PS_FIELDNAME']) == 'beneficiary account number';
                })));

                $getAcctOwn = array_values(array_map(function ($item) {
                    return (strtolower($item['PS_FIELDVALUE']) == 'y') ? '1' : '0';
                }, array_filter($getTransfer, function ($item) {
                    return strtolower($item['PS_FIELDNAME']) == 'beneficiary is own account';
                })));

                $getAcctAmount = array_values(array_map(function ($item) {
                    return $item['PS_FIELDVALUE'];
                }, array_filter($getTransfer, function ($item) {
                    return strtolower($item['PS_FIELDNAME']) == 'transaction amount';
                })));

                $getAcctName = array_values(array_map(function ($item) {
                    return $item['PS_FIELDVALUE'];
                }, array_filter($getTransfer, function ($item) {
                    return strtolower($item['PS_FIELDNAME']) == 'beneficiary account name';
                })));

                $saveTrfAcct = [];

                foreach ($getAcctTransfer as $key => $value) {
                    $saveTrfAcct[] = [
                        'ACCT' => $getAcctTransfer[$key],
                        'FLAG' => $getAcctOwn[$key],
                        'AMOUNT' => $getAcctAmount[$key],
                        'NAME' => $getAcctName[$key]
                    ];
                }

                $this->view->saveTrfAcct = $saveTrfAcct;

                // Zend_Debug::dump($getAcctTransfer);
                // Zend_Debug::dump($getAcctOwn);
                // Zend_Debug::dump($getAcctAmount);
                // die();

                if ($this->_request->isGet()) Application_Helper_General::writeLog('RPBG', 'Lihat Detail Data Perbaikan Pengajuan ' . $bgChangeType[$bgdata['CHANGE_TYPE']] . ' untuk BG No : ' . $bgdata['BG_NUMBER'] . ', Subjek : ' . $bgdata['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $tempBgClose['CLOSE_REF_NUMBER']);

                if ($this->_request->isPost()) {


                    $getAllRequests = $this->_request->getParams();
                    $getAllFiles = $_FILES;

                    $closeRefNumber = $tempBgClose['CLOSE_REF_NUMBER'];

                    $onlyFile = ['surat_permohonan_penutupan' => '', 'surat_pernyataan_pekerjaan_selesai' => '', 'bast_pekerjaan' => ''];
                    $files = array_intersect_key($getAllFiles, $onlyFile);

                    $callService = new Service_Account($saveAccount[$key]['Beneficiary Account Number'], '');

                    if ($getAllRequests['own_account']) {

                        $this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', ['CLOSE_REF_NUMBER = ?' => $closeRefNumber]);
                        $saveAccount = [];

                        $getEscrow = $this->_db->select()
                            ->from('T_BANK_GUARANTEE_SPLIT')
                            ->where('BG_REG_NUMBER = ?', $bgdata['BG_REG_NUMBER'])
                            ->where('LOWER(ACCT_DESC) = ?', 'escrow')
                            ->query()->fetch();

                        $callService = new Service_Account($getEscrow['ACCT'], '');
                        $saveCIF = $callService->inquiryAccontInfo()['cif'];

                        $callServiceCif = new Service_Account('', '', '', '', '', $saveCIF);

                        $saveInfoEscrow = array_shift(array_filter($callServiceCif->inquiryCIFAccount()['accounts'], function ($item) use ($getEscrow) {
                            return $item['account_number'] == $getEscrow['ACCT'];
                        }));

                        foreach ($getAllRequests['own_account'] as $key => $value) {

                            $saveAccount[$key]['Beneficiary Account Number'] = ($value == 1) ? $getAllRequests['own_account_number'][$key] : $getAllRequests['manual_account_number'][$key];
                            $saveAccount[$key]['Beneficiary is own account'] = ($value == '1' ? 'Y' : 'T');

                            $callService = new Service_Account($saveAccount[$key]['Beneficiary Account Number'], '');
                            $accName = $callService->inquiryAccontInfo()['account_name'];

                            $saveAccount[$key]['Beneficiary Account Name'] = $accName;
                            $saveAccount[$key]['ACCT_DESC'] = $getAllRequests['acct_type'][$key];
                            $saveAccount[$key]['Beneficiary Account CCY'] = $saveInfoEscrow['currency'];
                            $saveAccount[$key]['Transaction CCY'] = $getAllRequests['currency_acct'][$key];
                            $saveAccount[$key]['Transaction Amount'] = str_replace([','], '', $getAllRequests['account_amount'][$key]);

                            $insertKey = ['Beneficiary is own account', 'Beneficiary Account CCY', 'Beneficiary Account Number', 'Beneficiary Account Name', 'Transaction CCY', 'Transaction Amount'];

                            foreach ($insertKey as $key2 => $value) {
                                # code...
                                $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
                                    'CLOSE_REF_NUMBER' => $closeRefNumber,
                                    'ACCT_INDEX' => $key + 1,
                                    'PS_FIELDNAME' => $value,
                                    'PS_FIELDVALUE' => $saveAccount[$key][$value]
                                ]);
                            }
                        }
                    }

                    foreach ($files as $key => $value) {
                        if ($value['size'] == 0) continue;

                        $path = UPLOAD_PATH . '/document/closing/';
                        $newName = implode('_', [date('dmy'), date('his'), $bgdata['CUST_ID'], uniqid(), $value['name']]);

                        move_uploaded_file($value["tmp_name"], $path . $newName);

                        if ($key == 'bast_pekerjaan') $fileNote = 'BAST Pekerjaan';
                        else $fileNote = ucwords(str_replace(['_'], ' ', $key));

                        $this->_db->update('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
                            'BG_FILE' => $newName,
                        ], [
                            'CLOSE_REF_NUMBER = ?' => $closeRefNumber,
                            'FILE_NOTE = ?' => $fileNote
                        ]);
                    }

                    $this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', [
                        'CLOSE_REF_NUMBER' => $closeRefNumber,
                        'BG_NUMBER' => $bgdata['BG_NUMBER'],
                        'CUST_ID' => $bgdata['CUST_ID'],
                        'DATE_TIME' => new Zend_Db_Expr('NOW()'),
                        'USER_FROM' => 'PRINCIPAL',
                        'USER_LOGIN' => $this->_userIdLogin,
                        'BG_REASON' => 'NoRef Pengajuan : ' . $closeRefNumber,
                        'HISTORY_STATUS' => 24
                    ]);

                    $status = 3;
                    if ($principalDetail) $status = 2;

                    $this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', [
                        'SUGGESTION_STATUS' => $status,
                        'LASTUPDATED' => new Zend_Db_Expr('NOW()'),
                        'LASTUPDATEDFROM' => 'PRINCIPAL',
                        'LASTUPDATEDBY' => $this->_userIdLogin
                    ], [
                        'CLOSE_REF_NUMBER = ?' => $closeRefNumber
                    ]);

                    // HAPUS T APPROVAL
                    $this->_db->delete('T_APPROVAL', [
                        'PS_NUMBER LIKE ?' => '%' . $tempBgClose['CLOSE_REF_NUMBER'] . '%'
                    ]);

                    Application_Helper_General::writeLog('RPBG', 'Perbaikan Pengajuan ' . $bgChangeType[$bgdata['CHANGE_TYPE']] . ' BG No. ' . $bgdata['BG_NUMBER'] . ', Subjek : ' . $bgdata['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $tempBgClose['CLOSE_REF_NUMBER']);

                    // redirect 
                    $this->setbackURL('/bgclosingworkflow/repair');
                    $this->_redirect('/notification/success');
                }
            }
        }
    }

    public function cobaAction()
    {
        $total = 100;

        $Date1 = date("Y-m-d H:i:s");
        $date = new DateTime($Date1);

        while ($total > 0) {
            $date->modify('+1 day');
            $Date2 = $date->format('Y-m-d');
            $check_weekend = date("N", strtotime($Date2));

            if ($check_weekend < 6) {
                $total--;
            }
        }

        $this->_db->update("T_BANK_GUARANTEE", [
            "BG_CG_DEADLINE" => $Date2
        ], [
            "BG_REG_NUMBER = ?" => "992616101F4201"
        ]);

        var_dump($Date2);
        die();

        $test = new Service_Account('1601300020088', '');
        $info = $test->inquiryAccontInfo();
        $girosaving = $test->inquiryAccountBalance();
        $deposito = $test->inquiryDeposito();

        echo "<pre>";
        var_dump($info, $girosaving, $deposito);
        echo "</pre>";
        die();


        // $path_file = "/app/bg/library/data/temp/2022-07-28-85b4ce22-13ac-41e3-bf72-1731abe2f95f.pdf";
        $path_file = "/app/bg/library/data/temp/baru123.pdf";
        // $path_file = "/app/bg/library/data/temp/testing123.pdf";
        $filecontent = file_get_contents($path_file);

        $command = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.6  -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=/app/bg/library/data/temp/baru123.pdf " . $path_file . "";

        // gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=new-pdf1.5.pdf original.pdf
        // $p_result =  exec($command);

        var_dump($filecontent);
        die();

        if (preg_match("/^%PDF-1.5/", $filecontent)) {
            echo "Valid pdf";
        } else {
            echo "In Valid pdf";
        }
        // var_dump(file_exists($path_file));
        die();

        die();
        $svcCekStatus = new SGO_Soap_ClientUser();
        $svcCekStatus->callapi("inqaccount", null, [
            "account_number" => "101400135552"
            // "ref_trace" => "220728026908",
            // "trans_code" => "8774"
        ]);

        var_dump($svcCekStatus->getResult());
        die();

        // die("Hello");

        // $this->_db->update("T_BANK_GUARANTEE_SPLIT", [
        //     "AMOUNT" => round(12000000, 2)
        // ], [
        //     "BG_REG_NUMBER = '149616101F4201' AND ACCT_DESC = 'Escrow'"
        // ]);

        // die();
        // $get_temp_bank_guarantee = $this->_db->select()
        //     ->from("TEMP_BANK_GUARANTEE")
        //     ->where('DOCUMENT_ID = ? ', "96d69090-efe5-4e3b-b58f-65614bae9714")
        //     ->query()->fetch();
        // // ->where('DOCUMENT_ID = ? ', "96a9d028-47a6-4e29-b0f6-b8e2af140967");

        // unset($get_temp_bank_guarantee["NTU_COUNTING"]);
        // if (array_key_exists("SAVE_LINK", $get_temp_bank_guarantee)) {
        //     unset($get_temp_bank_guarantee["SAVE_LINK"]);
        // }

        // // $currentDate = date("ymd");
        // // $seqNumber   = mt_rand(1111, 9999);
        // // $bg_number_gen = strval($get_temp_bank_guarantee["BG_BRANCH"]) . strval($currentDate) . strval($get_temp_bank_guarantee["USAGE_PURPOSE"]) . strval($seqNumber);

        // $get_temp_bank_guarantee["BG_ISSUED"] = new Zend_Db_Expr("now()");

        // if ($get_temp_bank_guarantee["COUNTER_WARRANTY_TYPE"] == 3) {
        //     $get_detail_insurance = $this->_db->select()
        //         ->from("TEMP_BANK_GUARANTEE_DETAIL")
        //         ->where("BG_REG_NUMBER = ?", $get_temp_bank_guarantee["BG_REG_NUMBER"])
        //         ->where("PS_FIELDNAME = ?", "Insurance Name")
        //         ->query()->fetch();

        //     $get_detail_lf = $this->_db->select()
        //         ->from("M_CUST_LINEFACILITY")
        //         ->where("CUST_ID = ?", $get_detail_insurance["PS_FIELDVALUE"])
        //         ->query()->fetch();


        //     $get_temp_bank_guarantee["BG_CG_DEADLINE"] = date("Y-m-d", strtotime(date("Y-m-d") . " + " . ($get_detail_lf["DOC_DEADLINE"] - 1) . " days"));
        // }

        // $get_temp_bank_guarantee["BG_STATUS"] = "15";
        // $get_temp_bank_guarantee["COMPLETION_STATUS"] = "2";

        // $get_temp_bank_guarantee["REBATE_STATUS"] = $get_temp_bank_guarantee["REDEBATE_STATUS"];
        // $get_temp_bank_guarantee["REBATE_TYPE"] = $get_temp_bank_guarantee["REDEBATE_TYPE"];
        // $get_temp_bank_guarantee["REBATE_AMOUNT"] = $get_temp_bank_guarantee["REDEBATE_AMOUNT"];
        // unset($get_temp_bank_guarantee["REDEBATE_STATUS"]);
        // unset($get_temp_bank_guarantee["REDEBATE_TYPE"]);
        // unset($get_temp_bank_guarantee["REDEBATE_AMOUNT"]);
        // unset($get_temp_bank_guarantee["IS_TRANSFER_COA"]);
        // unset($get_temp_bank_guarantee["TRANSFER_COA_TIMEOUT"]);
        // unset($get_temp_bank_guarantee["IS_VALID"]);

        // $this->_db->insert("T_BANK_GUARANTEE", $get_temp_bank_guarantee);

        // die();
        // $svctemp = new Service_Account("1601306666109", "IDR");
        // $save_name = $svctemp->inquiryAccontInfo();
        // $balance = $svctemp->inquiryAccountBalance();

        // $svctemp1 = new Service_Account("1601306666109", "IDR");
        // $save_name1 = $svctemp1->inquiryAccontInfo()["account_name"];
        // $balance1 = $svctemp1->inquiryAccountBalance();

        // // var_dump($balance);
        // // die();

        // $temp_param = [
        //     "SOURCE_ACCOUNT_NUMBER" => "1601306666109",
        //     "SOURCE_ACCOUNT_NAME" => $save_name1,
        //     "BENEFICIARY_ACCOUNT_NUMBER" => "4801300004657",
        //     "BENEFICIARY_ACCOUNT_NAME" => $save_name,
        //     "PHONE_NUMBER_FROM" => "08123123",
        //     "AMOUNT" => "10000",
        //     "PHONE_NUMBER_TO" => "0825312312",
        //     "RATE1" => "10000000",
        //     "RATE2" => "10000000",
        //     "RATE3" => "10000000",
        //     "RATE4" => "10000000",
        //     "DESCRIPTION" => "Pemindahan Dana",
        //     "DESCRIPTION_DETAIL" => "Biaya Asuransi",

        // ];

        // $temp_service =  new Service_TransferWithin($temp_param);
        // $temp_result = $temp_service->sendTransfer();

        // var_dump($temp_result, $balance, $balance1);
        // die();

        // $checkQuota = new SGO_Soap_ClientUser();
        // $checkQuota->callapi("checkquota", null);
        // var_dump($checkQuota->getResult());
        // die();

        // $app      = Zend_Registry::get('config');
        // $bankCode = $app['app']['bankcode'];


        // $svcAccount_temp = new Service_Account("101400135170", "IDR", $bankCode, null, null, null, "EBG19");
        // // $temp_lock_deposito = $svcAccount_temp->lockDeposito(["balance" => "100000"]);
        // $temp_lock_deposito = $svcAccount_temp->inquiryDeposito();

        // var_dump($temp_lock_deposito);
        // die();


        $svcTransferCoa = new SGO_Soap_ClientUser();
        // $svcTransferCoa->callapi("inquiryrate", null, [
        //     "currency" => "IDR"
        // ]);


        $svcTransferCoa->callapi("transfercoa", null, [
            "source_account_currency" => "IDR",
            "source_account_number" => "1601306666109",
            "source_amount" => "600000", //debit amount
            "beneficiary_account_currency" => "IDR",
            "beneficiary_account_number" => "0",
            "branch_code" => "161",
            "beneficiary_amount" => "3000000", //credit amount
            "fee1" => "2000000",
            "fee2" => "1000000",
            "rate1" => "10000000",
            "rate2" => "10000000",
            "rate3" => "10000000",
            "rate4" => "10000000",
            "rate5" => "10000000",
            "remark1" => "Provision Fee",
            "remark2" => "Administration Fee",
            "remark3" => "Stamp Fee",
            "gl_account1" => "467115496250", //467115496250
            "gl_account2" => "46794840", //46794840
            "gl_account3" => "189991081011", //189991081011
            "message" => "Transfer COA"
        ]);


        header("Content-type: application/json");
        echo $svcTransferCoa->getResultPure();
        die();
    }


    public function getstatuslfAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $custid = $this->_getParam('custid');

        // get linefacillity

        $get_linefacility = $this->_db->select()
            ->from("M_CUST_LINEFACILITY")
            ->where("CUST_ID = ?", $custid)
            ->query()->fetchAll();

        //$result[];//json_encode(["linefacility" => $get_linefacility]);
        //$result = $get_linefacility;

        echo json_encode($get_linefacility[0]); // json_encode($get_linefacility,JSON_UNESCAPED_SLASHES);
    }


    public function inquirydepositoAction()
    {

        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        //echo $acct_no;

        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryDeposito('AB', TRUE);
        //var_dump($result);

        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;
        echo json_encode($result);
    }

    public function inquiryaccountbalanceAction()
    {

        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        //echo $acct_no;

        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
        //var_dump($result);
        //die();
        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;
        echo json_encode($result);
    }

    public function inquiryaccountinfoAction()
    {

        //integrate ke core
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $acct_no = $this->_getParam('acct_no');
        $svcAccount = new Service_Account($acct_no, null);
        $result = $svcAccount->inquiryAccontInfo('AI', TRUE);

        if ($result['response_desc'] == 'Success') //00 = success
        {
            $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
            $result2 = $svcAccountCIF->inquiryCIFAccount();

            $filterBy = $result['account_number']; // or Finance etc.
            $new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
                return ($var['account_number'] == $filterBy);
            });

            $singleArr = array();
            foreach ($new as $key => $val) {
                $singleArr = $val;
            }
        }

        if ($singleArr['type_desc'] == 'Deposito') {
            $svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountDeposito->inquiryDeposito();
        } else {
            $svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountBalance->inquiryAccountBalance();
        }
        $return = array_merge($result, $singleArr, $result3);

        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;


        echo json_encode($return);
    }

    public function insertToTempUnderlying($data, $bgregnumber)
    {
        foreach ($data['service'] as $key => $service) {
            try {
                $this->_db->insert('TEMP_BANK_GUARANTEE_UNDERLYING', [
                    'BG_REG_NUMBER' => $bgregnumber,
                    'DOC_NAME' => $service,
                    'DOC_TYPE' => $data['gt_doc_type'][$key],
                    'DOC_NUMBER' => $data['gt_doc_number'][$key],
                    'DOC_DATE' => $data['gt_doc_date'][$key],
                    'DOC_FILE' => $data['filename'][$key],
                ]);
            } catch (Exception $e) {
                $this->_db->rollBack();
            }
        }
    }

    public function checkGuarantedTransaction($files, $post, $bgregnumber)
    {
        $saveAll = [];
        // check file and input

        foreach ($post['SERVICE'] as $key => $file) {
            # code...
            $date = date("dmy");
            $time = date("his");
            $newFileName = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . '_' . $files['name'][$key];

            // destination
            $attachmentDestination   = UPLOAD_PATH . '/document/submit/';
            $extension = strtolower(pathinfo(basename($files['name'][$key]), PATHINFO_EXTENSION));

            // if file not null
            if ($files['size'][$key] > 0 && $extension == 'pdf') {
                move_uploaded_file($files['tmp_name'][$key], $attachmentDestination . $newFileName);
                $saveAll['filename'][$key] = $newFileName;
            } else {
                // $saveAll['filename'][$key] = $guarantedTransactions[$key]['DOC_FILE'];
                $saveAll['filename'][$key] = $post['check_exist_gt'][$key];
            }


            // check input
            // service
            $saveAll['service'][$key] = $post['SERVICE'][$key];

            // gt doc type
            $saveAll['gt_doc_type'][$key] = $post['GT_DOC_TYPE'][$key];

            // gt doc number
            $saveAll['gt_doc_number'][$key] = $post['GT_DOC_NUMBER'][$key];

            // gt doc date
            $saveAll['gt_doc_date'][$key] = $post['GT_DOC_DATE'][$key];
        }

        // delete data lama
        $this->_db->delete('TEMP_BANK_GUARANTEE_UNDERLYING', [
            'BG_REG_NUMBER = ?' => $bgregnumber
        ]);

        return $saveAll;
    }

    private function combineCodeAndDesc($arrayCode, $arrayDesc)
    {
        return array_combine($arrayCode, array_map(function ($arrayMap) use ($arrayDesc, $arrayCode) {
            return $arrayDesc[$arrayMap];
        }, array_keys($arrayCode)));
    }

    public function showcertificateAction()
    {
        $this->_helper->layout()->disableLayout();

        $AESMYSQL = new Crypt_AESMYSQL();
        $bgnumbDecode = urldecode($this->_getParam('bgnumb'));
        $bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

        $this->view->bgnumbDecode = $bgnumbDecode;

        $getDocId = $this->_db->select()
            ->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
            ->where('BG_NUMBER = ?', $bgnumbDecrypt)
            ->query()->fetch();

        $this->view->bgNumb = $getDocId['BG_NUMBER'];

        $this->render('showcertificate');
    }

    public function showcertAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $AESMYSQL = new Crypt_AESMYSQL();
        $bgnumbDecode = urldecode($this->_getParam('bgnumb'));
        $bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

        if (!$bgnumbDecrypt) {
            header('Content-type: application/json');
            echo json_encode(['message' => 'file tidak ditemukan']);
            return false;
        }

        $getDocId = $this->_db->select()
            ->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
            ->where('BG_NUMBER = ?', $bgnumbDecrypt)
            ->query()->fetch();

        $pathFile = UPLOAD_PATH . '/document/submit/';
        $getFile = $pathFile . '/' . $getDocId['DOCUMENT_ID'] . '.pdf';

        if (!file_exists($getFile)) {
            header('Content-type: application/json');
            echo json_encode(['message' => 'file tidak ditemukan']);
            return false;
        }

        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=BG_" . $getDocId['BG_NUMBER'] . '.pdf' . "");

        readfile($getFile);
        return true;
    }

    public function downloadcertificateAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $AESMYSQL = new Crypt_AESMYSQL();
        $bgnumbDecode = urldecode($this->_getParam('bgnumb'));
        $bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

        if (!$bgnumbDecrypt) {
            header('Content-type: application/json');
            echo json_encode(['message' => 'file tidak ditemukan']);
            return false;
        }

        $getDocId = $this->_db->select()
            ->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
            ->where('BG_NUMBER = ?', $bgnumbDecrypt)
            ->query()->fetch();

        $pathFile = UPLOAD_PATH . '/document/submit/';
        $getFile = $pathFile . '/' . $getDocId['DOCUMENT_ID'] . '.pdf';

        if (!file_exists($getFile)) {
            header('Content-type: application/json');
            echo json_encode(['message' => 'file tidak ditemukan']);
            return false;
        }

        header("Content-type: application/pdf");
        header("Content-Disposition: attachtment; filename=BG_" . $getDocId['BG_NUMBER'] . '.pdf' . "");

        readfile($getFile);
        return true;
    }

    public function downloadlampiranAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $AESMYSQL = new Crypt_AESMYSQL();
        $bgnumbDecode = urldecode($this->_getParam('bgnumb'));
        $bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

        $jenis = $this->_request->getParam('jenis');
        $jenis = str_replace(['_'], ' ', $jenis);

        if (!$bgnumbDecrypt) {
            header('Content-type: application/json');
            echo json_encode(['message' => 'file tidak ditemukan']);
            return false;
        }

        $closeFile = $this->_db->select()
            ->from(['TBG' => 'T_BANK_GUARANTEE'], ['BG_NUMBER'])
            ->joinLeft(['TBGC' => 'TEMP_BANK_GUARANTEE_CLOSE'], 'TBG.BG_NUMBER = TBGC.BG_NUMBER OR TBG.BG_REG_NUMBER = TBGC.BG_REG_NUMBER', [''])
            ->joinLeft(['TBGFC' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'], 'TBGC.CLOSE_REF_NUMBER = TBGFC.CLOSE_REF_NUMBER', ['*'])
            ->where('TBG.BG_NUMBER = ?', $bgnumbDecrypt)
            ->where('LOWER(FILE_NOTE) = ?', $jenis)
            ->query()->fetch();

        $pathFile = UPLOAD_PATH . '/document/closing/';
        $getFile = $pathFile . '/' . $closeFile['BG_FILE'];

        if (!file_exists($getFile)) {
            header('Content-type: application/json');
            echo json_encode(['message' => 'file tidak ditemukan']);
            return false;
        }

        $filename = trim(str_replace([explode('_', $closeFile['BG_FILE'])[0], explode('_', $closeFile['BG_FILE'])[1], explode('_', $closeFile['BG_FILE'])[2], explode('_', $closeFile['BG_FILE'])[3]], '', $closeFile['BG_FILE']), '_');

        header("Content-type: application/pdf");
        header("Content-Disposition: attachtment; filename=BG_" . $closeFile['BG_NUMBER'] . '_' . $filename . "");

        readfile($getFile);
        return true;
    }

    public function mdfcAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

        if ($bgRegNumber == null) {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'nothing']);
            return true;
        }

        $bgdatasplit = $this->_db->select()
            ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
            ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
            ->where('A.BG_REG_NUMBER = ?', $bgRegNumber)
            ->query()->fetchAll();

        $bgdatasplit = array_filter($bgdatasplit, function ($acct) {
            if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro')
                return true;
        });

        $mdAmount = 0;
        $escrowAmount = 0;

        $htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

        foreach ($bgdatasplit as $key => $value) {

            $callService = new Service_Account($value['ACCT'], '');

            $saveCIF = $callService->inquiryAccontInfo()['cif'];
            $serviceCIF = new Service_Account('', '', '', '', '', $saveCIF);
            $getAllAcc = $serviceCIF->inquiryCIFAccount();

            if ($getAllAcc['response_code'] == '0000') {
                $getDetailAcc = array_shift(array_filter($getAllAcc['accounts'], function ($item) use ($value) {
                    return ltrim($item['account_number'], '0') == ltrim($value['ACCT'], '0');
                }));
            } else {
                $getDetailAcc = [];
            }

            if (strtolower($value['ACCT_DESC']) == 'escrow') {
                $inqbalance = $callService->inquiryAccountBalance();
                // if (intval($inqbalance['available_balance']) == 0) {
                //     // hapus data atau ??
                //     continue;
                // } else {
                $mdAmount += $value['AMOUNT'];
                $escrowAmount = $value['AMOUNT'];
                // }
            } else {
                // jika deposito
                if (strtolower($getDetailAcc['type_desc']) == 'deposito') {
                    $inqbalance = $callService->inquiryDeposito();

                    // if (strtolower($inqbalance['hold_description']) == 'n') {
                    //     // hapus data atau ??
                    //     continue;
                    // } else {}
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika deposito

                // jika tabungan
                else {
                    $inqbalance = $callService->inqLockSaving();

                    $cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
                        return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
                    }));

                    // if (!$cekSaving) {
                    //     // hapus data atau
                    //     continue;
                    // } else {}
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika tabungan
            }

            $htmlResult .= "
            <tr>
                <td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
                <td>" . str_pad($value['ACCT'], 16, '0', STR_PAD_LEFT) . "</td>
                <td>" . $value['NAME'] . "</td>
                <td>" . ($getDetailAcc['type_desc'] . ' ' . $getDetailAcc['product_type']) . "</td>
                <td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($getDetailAcc['currency'])) . "</td>
                <td>" . number_format($value['AMOUNT'], 2) . "</td>
            </tr>";
        }

        $htmlResult .= "</tbody></table>";
        $htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksis'></input>";
        $htmlResult .= "<input type='hidden' value='" . $escrowAmount . "' id='escrowAmount'></input>";

        echo $htmlResult;
    }
}
