<?php
require_once 'Zend/Controller/Action.php';

class chargessetting_IndexController extends Application_Main
{
	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
	
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()->distinct()
					->from(array('M_CUSTOMER'),array('CUST_ID'))
					->where("CUST_STATUS != '3'")
					->order('CUST_ID ASC')
				 	-> query() ->fetchAll();
					$comp = array();
		foreach($select as $vl){
			$comp[] = $vl['CUST_ID'];
		}
		//Zend_Debug::dump($select); die;
		$this->view->var=$select;

		$sqlCheckTempChange= $this->_db->select()
                            ->from(array('B' => 'TEMP_CHARGES_WITHIN'),array('CUST_ID'));
		$listTempChange = $this->_db->fetchAll($sqlCheckTempChange);
		
		
		if(!empty($listTempChange)){
		    $tempcheck = $listTempChange;
		}else{
		    $tempcheck = '0';
		}

		$this->view->tempcheck = $tempcheck;

		/*$select3 = $this->_db->select()
				->from(array('B' => 'M_CHARGES_WITHIN'),array('*'));
		$coba = $this->_db->fetchAll($select3);
		Zend_Debug::dump($select3); die;*/
		$chargestype = array_combine(array_values($this->_monthlytype['code']),array_values($this->_monthlytype['desc']));
		// print_r($chargestype);die();
		$list = array(	''				=>	'--- '.$this->language->_('Any Value').' ---',
						'Enabled'		=>	$this->language->_('Enabled'),
						'Disabled' 		=> 	$this->language->_('Disabled'));
		$this->view->optionlist = $list;

		$ceklist = array(	'0'		=>	$this->language->_('Disabled'),
							'1' 	=> 	$this->language->_('Enabled'));

		$companyCode = $this->language->_('Company Code');
		$companyName = $this->language->_('Company');
		$monthlyfee = $this->language->_('Monthy Fee Type');
		$companyacct = $this->language->_('Company Account');
		$company_name = $this->language->_('Company Name');
		$monthlyFee = $this->language->_('Monthly Fee');
		$suggestedDate = $this->language->_('Lastest Suggested');
		$suggester = $this->language->_('Suggester');
		$tra_from = $this->language->_('Number trx From');
		$transto = $this->language->_('Number trx To');
		$chargesTo = $this->language->_('Charges Account');
		$approvedDate = $this->language->_('Lastest Approved');
		$fields = array	(
							// 'Company Code'  			=> array	(
							// 										'field' => 'B.CUST_ID',
							// 										'label' => $companyCode,
							// 										'sortable' => true
							// 	 								),
							'Company'  			=> array	(
																	'field' => 'COMPANY',
																	'label' => $this->language->_('Charges ID'),
																	'sortable' => true
																),
							'MonthlyFee'  			=> array	(
																	'field' => 'CUST_MONTHLYFEE_TYPE',
																	'label' => $this->language->_('Package Name'),
																	'sortable' => true
																),
							'CompanyAct'  			=> array	(
																	'field' => 'ACCT_NO',
																	'label' => $this->language->_('Inhouse'),
																	'sortable' => true
																),
							'NumberTrans'  			=> array	(
																	'field' => 'TRA_FROM',
																	'label' => $this->language->_('RTGS'),
																	'sortable' => true
																),
							'TransTo'  			=> array	(
																	'field' => 'TRA_TO',
																	'label' => $this->language->_('SKN'),
																	'sortable' => true
																),
							'Charges'  					=> array	(
																	'field' => 'AMOUNT',
																	'label' => $this->language->_('Online'),
																	'sortable' => true
																),
							
							'Suggest Date'  		=> array	(
																	'field' => 'SUGGEST_DATE',
																	'label' => $suggestedDate,
																	'sortable' => true
																),
							'Approve Date'  		=> array	(
																	'field' => 'APPROVE_DATE',
																	'label' => $approvedDate,
																	'sortable' => true
																),
						);

		$filterlist = array("CUST_ID","MONTHLY_FEE_STATUS","CUST_NAME","SUGGESTOR");
		$this->view->filterlist = $filterlist;

		

		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');

		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$filterArr = array('filter' 			=> array('StripTags','StringTrim'),
	                       'CUST_ID'    			=> array('StripTags','StringTrim'),
	                       'CUST_NAME'  			=> array('StripTags','StringTrim','StringToUpper'),
						   'SUGGESTOR'  		=> array('StripTags','StringTrim','StringToUpper'),
						   // 'chargeStatus'  		=> array('StripTags','StringTrim'),
						   'MONTHLY_FEE_STATUS'  	=> array('StripTags','StringTrim'),
						   // 'fDateFrom'  		=> array('StripTags','StringTrim'),
						   // 'fDateTo'  			=> array('StripTags','StringTrim'),
	                      );

	    $validator = array('filter' 			=> array(),
	                       'CUST_ID'    			=> array(),
	                       'CUST_NAME'  			=> array(),
						   'SUGGESTOR'  		=> array(),
						   // 'chargeStatus'  		=> array(),
						   'MONTHLY_FEE_STATUS'  	=> array(),
						   // 'fDateFrom'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   // 'fDateTo'  			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );



	    // $dataParam = array("CLR_CODE","SWIFT_CODE","BANK_NAME");
	    $dataParam = array("CUST_ID","MONTHLY_FEE_STATUS","CUST_NAME","SUGGESTOR");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}


	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    $filter = $this->_getParam('filter');

	    
		//Zend_Debug::dump($this->_masterglobalstatus); die;

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		/*if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}*/

		


		$selectglobal = $this->_db->select()
						->from(array('B' => 'M_CHARGES_WITHIN'),array('*'))
						->joinleft(array('D' => 'M_CHARGES_OTHER'), 'B.CUST_ID = D.CUST_ID',array('*'))
						->where("B.CUST_ID NOT IN (?) ",$comp)
						->order("B.PACKAGE_NAME ASC");
		//echo $selectglobal;die;
		
		$arrglobal = $this->_db->fetchAll($selectglobal);		
		$chargeArr = array();
		foreach($arrglobal as $ky => $vl){
				if($vl['CHARGES_TYPE'] == '1'){
					//die;
					$chargeArr[$vl['CUST_ID']]['RTGS'] = $vl['CHARGES_AMT'];	
				}
				if($vl['CHARGES_TYPE'] == '2'){
					$chargeArr[$vl['CUST_ID']]['SKN'] = $vl['CHARGES_AMT'];	
				}
				if($vl['CHARGES_TYPE'] == '8'){
					$chargeArr[$vl['CUST_ID']]['ONLINE'] = $vl['CHARGES_AMT'];	
				}
					$chargeArr[$vl['CUST_ID']]['INHOUSE'] = $vl['AMOUNT']; 
					   
				$chargeArr[$vl['CUST_ID']]['INHOUSE'] = $vl['AMOUNT'];	
				$chargeArr[$vl['CUST_ID']]['APPROVED'] =  $vl['CHARGES_APPROVEDBY'].' ('.$vl['CHARGES_APPROVED'].')';	
				$chargeArr[$vl['CUST_ID']]['SUGGESTED'] =  $vl['CHARGES_SUGGESTEDBY'].' ('.$vl['CHARGES_SUGGESTED'].')';	
				$chargeArr[$vl['CUST_ID']]['PACKAGE_NAME'] =  $vl['PACKAGE_NAME'];	
				
				
				$selecttemp = $this->_db->select()
						->from(array('B' => 'TEMP_CHARGES_WITHIN'),array('*'))
						->where("B.CUST_ID = ? ",$vl['CUST_ID']);
				$temp = $this->_db->fetchAll($selecttemp);		
				if(!empty($temp)){
				$chargeArr[$vl['CUST_ID']]['EDIT'] =  false;		
				}else{
				$chargeArr[$vl['CUST_ID']]['EDIT'] =  true;			
				}		
				
		}
		 
		//echo '<pre>';
		//var_dump($chargeArr);die;
		$this->view->arrglobal = $chargeArr;		
		// echo "<pre>";
		// print_r($arr);die;
		//if($filter == null || $filter == 'Set Filter')
		if($filter == true)
		{
			
			$custid = html_entity_decode($zf_filter->getEscaped('CUST_ID'));
			$custname = html_entity_decode($zf_filter->getEscaped('CUST_NAME'));
			$suggestor = html_entity_decode($zf_filter->getEscaped('SUGGESTOR'));
			$chargeStatus = html_entity_decode($zf_filter->getEscaped('chargeStatus'));
			$monthlyfeeStatus = html_entity_decode($zf_filter->getEscaped('MONTHLY_FEE_STATUS'));
			$datefrom = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
			$dateto = html_entity_decode($zf_filter->getEscaped('fDateTo'));
			
			$this->view->fDateTo    = $dateto;
			$this->view->fDateFrom  = $datefrom;
			//Zend_Debug::dump($custid); die;

			if(!empty($datefrom))
		            {
		            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
						$datefrom  = $FormatDate->toString($this->_dateDBFormat);
		            }

		    if(!empty($dateto))
		            {
		            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
						$dateto    = $FormatDate->toString($this->_dateDBFormat);
						//Zend_Debug::dump($dateto); die;
		            }

			if(!empty($datefrom) && empty($dateto))
		            $select2->where("DATE(B.CUST_SUGGESTED) >= ".$this->_db->quote($datefrom));

		   	if(empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(B.CUST_SUGGESTED) <= ".$this->_db->quote($dateto));

		    if(!empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(B.CUST_SUGGESTED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

		   	if($chargeStatus)
			{
				$this->view->chargeStatus = $chargeStatus;
				if($chargeStatus == "Enabled")
				{
					$cekchargeStatus = "1";
				}
				if($chargeStatus == "Disabled")
				{
					$cekchargeStatus = "0";
				}
				$select2->where("B.CUST_CHARGES_STATUS LIKE ".$this->_db->quote($cekchargeStatus));
			}

			if($monthlyfeeStatus)
			{
				$this->view->adminfeeStatus = $monthlyfeeStatus;
				if($monthlyfeeStatus == "Enabled")
				{
					$cekadminfeeStatus = "1";
				}
				if($monthlyfeeStatus == "Disabled")
				{
					$cekadminfeeStatus = "0";
				}
				$select2->where("CUST_MONTHLYFEE_STATUS LIKE ".$this->_db->quote($cekadminfeeStatus));
			}

		    if($custid)
		    {
	       		$this->view->cekcustid = $custid;
	       		$select2->where("B.CUST_ID LIKE ".$this->_db->quote($custid));
		    }

			if($custname)
			{
	       		$this->view->custname = $custname;
	       		$select2->where("B.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));
			}

			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
	       		$select2->where("B.CUST_SUGGESTEDBY LIKE ".$this->_db->quote('%'.$suggestor.'%'));
		    }
		}

		
		// echo '<pre>';
		// echo $select2;

		//$select2->order($sortBy.' '.$sortDir);
		//$select2->order($sortBy.' '.$sortDir);
    	//$this->paging($select2,30);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	

    	if($csv || $pdf)
    	{
    		$header = Application_Helper_Array::simpleArray($fields, "label");
    		$arr = $this->_db->fetchAll($select2);

    		foreach($arr as $key=>$value)
			{
				$arr[$key]["CUST_SUGGESTED"] = Application_Helper_General::convertDate($value["CUST_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}

	    	if($csv)
			{
				Application_Helper_General::writeLog('CHLS','Download CSV Company Charges List');
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}

			if($pdf)
			{
				Application_Helper_General::writeLog('CHLS','Download PDF Company Charges List');
				$this->_helper->download->pdf($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('CHLS','View Company Charges List');
    	}

    	if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Monthly Charges', 'data_header' => $fields));
		}


  //   	$this->view->fields = $fields;
		// $this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = array_unique($wherecol);
        $this->view->whereval     = array_unique($whereval);
     
      }


	}
	
	
	
	public function addchargesAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $packagename = $this->_getParam('packagename');
        $inhouseadd = Application_Helper_General::convertDisplayMoney($this->_getParam('inhouseadd'));
		$rtgsadd = Application_Helper_General::convertDisplayMoney($this->_getParam('rtgsadd'));
		$sknadd = Application_Helper_General::convertDisplayMoney($this->_getParam('sknadd'));
		$onlineadd = Application_Helper_General::convertDisplayMoney($this->_getParam('onlineadd'));
		//var_dump($inhouseadd);
		//var_dump($rtgsadd);
		//var_dump($sknadd);
		//var_dump($onlineadd);die;
		if($packagename != '' && $inhouseadd != '' && $rtgsadd != '' && $sknadd != '' && $onlineadd != '' ){ 
		$selectdata = $this->_db->select()->distinct()
					->from(array('M_CHARGES_WITHIN'),array('CUST_ID'))
					->where("CUST_ID LIKE ?" ,'%SPECIAL%')
				 	-> query() ->fetchAll();
		$totaldata = count($selectdata);
		try{
					$totaldata = $totaldata+1;
					$this->_db->beginTransaction();
					$cust_id = 'SPECIAL'.sprintf("%02d", $totaldata);
					$info = 'Charges Setting';
					$info2 = 'Charges Setting';
						
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_WITHIN','TEMP_CHARGES_WITHIN','','Charges Setting','',null,'specialcharges');
					// print_r($resultunion);die;
					$resultunion= array('1','2','8');

					$idamt = 'pbspecial';
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);

							$data= array(
												'CHANGES_ID' 		=> $change_id,
												'PACKAGE_NAME'		=> $packagename,
												'CUST_ID' 			=> $cust_id,
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $inhouseadd,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> NULL,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);


							
							
								$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '1',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $rtgsadd,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

								$this->_db->insert('TEMP_CHARGES_OTHER',$data1);
								
								$data2 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '2',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $sknadd,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

								$this->_db->insert('TEMP_CHARGES_OTHER',$data2);
								
								$data8 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '8',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $onlineadd,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
 
								$this->_db->insert('TEMP_CHARGES_OTHER',$data8);
			
						$this->_db->commit();
						$this->setbackURL('/chargessetting');
						echo 'success';
						//$this->_redirect('/notification/submited/index');
		}catch(Exception $e)
		{
					echo 'failed';
					//die;
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
		}
		
		}else{
			echo $inhouseadd.'-'.$rtgsadd.'-'.$sknadd.'-'.$onlineadd;;
		}
		
		
       
    }
	
	
	
	public function editchargesAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $packagename = $this->_getParam('packagenameedit');
        $inhouseadd = Application_Helper_General::convertDisplayMoney($this->_getParam('inhouseedit'));
		$rtgsadd = Application_Helper_General::convertDisplayMoney($this->_getParam('rtgsedit'));
		$sknadd = Application_Helper_General::convertDisplayMoney($this->_getParam('sknedit'));
		$onlineadd = Application_Helper_General::convertDisplayMoney($this->_getParam('onlineedit'));
		
		$selectdata = $this->_db->select()->distinct()
					->from(array('M_CHARGES_WITHIN'),array('CUST_ID'))
					->where("CUST_ID = ?" ,$this->_getParam('package'))
				 	-> query() ->fetchAll();
		$totaldata = count($selectdata);
		if($packagename != '' && $inhouseadd != '' && $rtgsadd != '' && $sknadd != '' && $onlineadd != '' ){ 
		try{
					$totaldata = $totaldata+1;
					$this->_db->beginTransaction();
					$cust_id = $selectdata['0']['CUST_ID'];
					$info = 'Charges Setting';
					$info2 = 'Charges Setting';
						
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_WITHIN','TEMP_CHARGES_WITHIN','',$packagename,'',null,'specialcharges');
					// print_r($resultunion);die;
					$resultunion= array('1','2','8');

					$idamt = 'pbspecial';
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);

							$data= array(
												'CHANGES_ID' 		=> $change_id,
												'PACKAGE_NAME'		=> $packagename,
												'CUST_ID' 			=> $cust_id,
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $inhouseadd,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> NULL,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);


							
							
								$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '1',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $rtgsadd,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

								$this->_db->insert('TEMP_CHARGES_OTHER',$data1);
								
								$data2 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '2',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $sknadd,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

								$this->_db->insert('TEMP_CHARGES_OTHER',$data2);
								
								$data8 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> '8',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $onlineadd,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
 
								$this->_db->insert('TEMP_CHARGES_OTHER',$data8);
			
						$this->_db->commit();
						$this->setbackURL('/chargessetting');
						echo 'success';
						//$this->_redirect('/notification/submited/index');
		}catch(Exception $e)
		{
					//var_dump($e);die;
					return false;
					//die;
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
		}
		}else{
			return false;
		}
		
       
    }
	
	
	
	
	public function deletechargesAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        //$custlist = $this->_getParam('custlist');
        $custlist = $this->_getParam('package');
		//echo '<pre>';
		// print_r($this->_request->getParams());
		// die;
		
		$selectdata = $this->_db->select()->distinct()
					->from('M_CHARGES_WITHIN',array('*'))
					->where("CUST_ID = ?" ,$custlist)
				 	-> query() ->fetchAll();
					
		$selectdataother = $this->_db->select()->distinct()
					->from('M_CHARGES_OTHER',array('*'))
					->where("CUST_ID = ? " ,$custlist)
				 	-> query() ->fetchAll();			
		
		try{
					$totaldata = $totaldata+1;
					$this->_db->beginTransaction();
					
					$info = 'Charges Setting';
					$info2 = 'Charges Setting';
						
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['delete'],null,'M_CHARGES_WITHIN','TEMP_CHARGES_WITHIN','','Charges Setting','',null,'specialcharges');
					// print_r($resultunion);die;
					
							
						//var_dump($selectdata);die;
						foreach($selectdata as $val){
							$data= array(
												'CHANGES_ID' 		=> $change_id,
												'PACKAGE_NAME'		=> $val['PACKAGE_NAME'],
												'CUST_ID' 			=> $val['CUST_ID'],
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $val['AMOUNT'],
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> NULL,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);
						}
						
						
						foreach($selectdataother as $valother){
							$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $valother['CUST_ID'],
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> $valother['CHARGES_TYPE'],
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $valother['CHARGES_AMT'],
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

								$this->_db->insert('TEMP_CHARGES_OTHER',$data1);
						}

						$this->_db->commit();
						echo 'success';
						//$this->setbackURL('/chargessetting');
						//$this->_redirect('/notification/submited/index');
		}catch(Exception $e)
		{
					echo 'failed';
					//die;
					Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
		}
		
		
       $this->setbackURL('/chargessetting');
    }
	
	
	
}
