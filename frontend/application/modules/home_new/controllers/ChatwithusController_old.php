<?php
require_once 'Zend/Controller/Action.php';

class home_ChatwithusController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$category 	= $this->language->_('Category');
		$question 	= $this->language->_('Question');
		$answer 	= $this->language->_('Answer');
		$lastAnswer = $this->language->_('Last Answered');

		$fields = [
			'category' => [
				'field' 	=> 'QUESTION_CATEGORY',
				'label'		=> $category,
				'sortable'	=> true
			],
			'question' => [
				'field' 	=> 'QUESTION_Q',
				'label'		=> $question,
				'sortable'	=> true
			],
			'answer' => [
				'field' 	=> 'QUESTION_A',
				'label'		=> $answer,
				'sortable'	=> true
			],
			'lastAnswer' => [
				'field' 	=> 'BUSER_ID',
				'label'		=> $lastAnswer,
				'sortable'	=> true
			]
		];

		//get page, sortby, sortdir
		$page 			= $this->_getParam('page');
		$sortBy 		= $this->_getParam('sortby');
		$sortDir 		= $this->_getParam('sortdir');
		$filter_clear 	= $this->_getParam('filter_clear');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0)) ? $page : 1;
		            
		$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc')))) ? $sortDir : 'desc';

		//get filtering param
		$filters = array(
			'filter'		=>  array('StringTrim', 'StripTags'),
			'fCategory'		=>  array('StringTrim', 'StripTags')
		);

		$validators = array(
			'filter'		=>  array('allowEmpty' => true),
			'fCategory'		=>  array('allowEmpty' => true)
		);

		$optionValidators = array('breakChainOnFailure' => false);

		$zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optionValidators);

		$filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');

		if($filter == TRUE && $filter_clear == NULL){
			$fCategory 		= $filterParam['fCategory'] = $this->_request->getParam('fCategory');
		}

		$data = $this->_db->select()
			->from(
				array('CS' => 'CUSTOMER_SERVICES'),
				array(
					'USER_ID'			=> 'CS.USER_ID',
					'QUESTION_DATE'		=> 'CS.QUESTION_DATE',
					'QUESTION_CATEGORY'	=> new Zend_Db_Expr("(CASE CS.QUESTION_CATEGORY
																WHEN '1' THEN 'Transaction'
																WHEN '2' THEN 'User Administration'
																WHEN '3' THEN 'Report Problem'
									  		 				END)"),
					'QUESTION_Q'		=> 'CS.QUESTION_Q',
					'QUESTION_A'		=> 'CS.QUESTION_A',
					'BUSER_ID'			=> 'CS.BUSER_ID',
					'QUESTION_A_DATE'	=> 'CS.QUESTION_A_DATE'
				)
			)
			->where('CS.USER_ID = '.$this->_db->quote($this->_userIdLogin));

		if($filter == TRUE){
			if($fCategory){
				$data->where('CS.QUESTION_CATEGORY = ?', $fCategory);
			}
		}
		$data->order($sortBy.' '.$sortDir);
		$data = $this->_db->fetchAll($data);
		// echo '<pre>';print_r($data);
		$this->paging($data);

		$categoryArr = [
			'1'	=> 'Transaction',
			'2'	=> 'User Administration',
			'3'	=> 'Report Problem'
		];

		$this->view->currentPage	= $page;
		$this->view->sortby 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->fields 		= $fields;
		$this->view->filter 		= $filter;
		$this->view->fCategory 		= $fCategory;
		$this->view->categoryArr 	= $categoryArr;

		Application_Helper_General::writeLog('CSVQ','Customer Service View Question');
	}

	public function createAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$pleaseSelect = '-- '.$this->language->_('Please Select'). ' --';
		$questioncategory = $this->_questioncategory;

		$questioncategoryArray 	= array( '' => $pleaseSelect);
		$questioncategoryArray += array_combine(array_values($questioncategory['code']),array_values($questioncategory['desc']));

		foreach($questioncategoryArray as $key => $value){
			if($key != 5){
				$optpayStatusRaw[$key] = $this->language->_($value);
			} 
		}

		$optPayType = $optpayStatusRaw;
		$this->view->questioncategoryArray 	= $optPayType;

		if($this->_getParam('submit') == true)
		{
			$filters = 	array(
									'category'	=> 	array('StringTrim','StripTags'),
									'question'	=> 	array('StringTrim','StripTags'),
								);
			$validator = array	(
									'category' 	=> 	array	(
																array	(
																			'NotEmpty',
																			array('inArray',$questioncategory['code']),
																		),
																'messages' => array	(
																						$this->language->_('Error').": ".$this->language->_('Category has not fill yet').".",
																						$this->language->_('Error').": ".$this->language->_('Category has not been exist').".",
																					)
															),
									'question'	=> 	array	(
																'NotEmpty',
																'messages' => array	(
																						$this->language->_('Error').": ".$this->language->_('Question has not fill yet').".",
																					)
															),
								);

			$param['category']	= $this->_getParam('category');
			$param['question']	= $this->_getParam('question');

			$zf_filter_input = new Zend_Filter_Input($filters, $validator, $param, $this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				try
				{
					$arrayInsert = array(
											'USER_ID' 			=> $this->_userIdLogin,
											'QUESTION_DATE' 	=> new Zend_Db_Expr('GETDATE()'),
											'QUESTION_CATEGORY' => $param['category'],
											'QUESTION_Q' 		=> $param['question'],
										);
					$insert = $this->_db->insert('CUSTOMER_SERVICES',$arrayInsert);

					Application_Helper_General::writeLog('CSCQ','Customer Service Create Question');

		 			$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
					$this->_redirect('/notification/success');


				}
				catch(Exception $e)
				{
					$this->view->failErr = "Data Fail To Insert";
					$this->view->category = $param['category'];
					$this->view->question = $param['question'];
				}
			}
			else
			{
				$errors = $zf_filter_input->getMessages();

				$this->view->categoryErr = (isset($errors['category']))? $errors['category'] : null;
				$this->view->questionErr = (isset($errors['question']))? $errors['question'] : null;

				$this->view->category = $param['category'];
				$this->view->question = $param['question'];
			}
		}
		Application_Helper_General::writeLog('CSCQ','Customer Service Create Question');
	}
}