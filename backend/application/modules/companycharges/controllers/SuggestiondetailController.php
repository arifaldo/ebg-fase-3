<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';

class companycharges_SuggestiondetailController extends Application_Main
{
	/**
	 * The default action - show the home page
	 */

	public function indexAction()
	{
		// 		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();

		// 		if(count($temp)>1){
		//       		if($temp[0]=='F' || $temp[0]=='S'){
		//       			if($temp[0]=='F')
		//         			$this->view->error = 1;
		//         		else
		//         			$this->view->success = 1;
		//       			$msg = ''; unset($temp[0]);
		//       			foreach($temp as $value)
		//       			{
		//       				if(!is_array($value))
		//       					$value = array($value);
		//       				$msg .= $this->view->formErrors($value);
		//       			}
		//         		$this->view->report_msg = $msg;
		//      		}	
		//     	}
		// $this->_helper->layout()->setLayout('newlayout');
		$this->_helper->layout()->setLayout('newpopup');

		$params = $this->_request->getParams();
		$this->view->changes_id = $params['changes_id'];

		$checkChangesId = $this->_db->select()
			->from(["A" => "TEMP_CHARGES_OTHER"], ["*"])
			->where("CHARGES_TYPE = 10")
			->query()->fetchAll();

		if (count($checkChangesId) > 0) {

			$getData = $checkChangesId[0];

			$this->view->cashCollateral = 1;
			$this->view->data = $getData;
			return 0;
		}

		$buser_id = $this->_userIdLogin;
		$this->view->buser_id 	= $buser_id;

		$settings =  new Settings();

		//current value Company Monthly Fee and Realtime Local Transfer Charges
		$select = $this->_db->select()
			->from(array('A' => 'M_SETTING'), array('SETTING_ID', 'SETTING_VALUE'))
			->where('MODULE_ID = ?', 'CHR')
			->query()->FetchAll();
		$setting = Application_Helper_Array::listArray($select, 'SETTING_ID', 'SETTING_VALUE');

		foreach ($setting as $key => $value) {
			if (!empty($key))
				$this->view->$key = $value;
		}

		//suggested value Company Monthly Fee and Realtime Local Transfer Charges
		$selectTemp = $this->_db->select()
			->from(array('A' => 'TEMP_SETTING'), array('SETTING_ID', 'SETTING_VALUE'))
			->where('MODULE_ID = ?', 'CHR')
			->where('CHANGES_ID = ?', $params['changes_id'])
			->query()->FetchAll();
		$settingTemp = Application_Helper_Array::listArray($selectTemp, 'SETTING_ID', 'SETTING_VALUE');

		foreach ($settingTemp as $key => $value) {
			if (!empty($key))
				$new = "new" . $key;
			$this->view->$new = $value;
		}
		//

		$modelremitance = new remittancechar_Model_Remittancechar();

		// 		$this->view->tempcek = $model->cekTemp();
		// 		$this->view->paginator = $model->getRemittanceType($charge_type);
		// 		$this->view->transferfee = $model->getRemittanceTypeIndexTemp('3');

		//$this->view->transferfee = $modelremitance->getRemittanceTypeIndexTemp('3',$params['changes_id']);
		//$this->view->transferfeeold = $modelremitance->getRemittanceTypeIndex('3','GLOBAL');
		// Zend_Debug::dump($this->view->transferfee,"transferfee");
		// Zend_Debug::dump($this->view->transferfeeold,"transferfeeold");
		// 		$transferfee = $model->getRemittanceTypeIndexTemp('3',$params['changes_id']);
		// 		print_r($transferfee);die;
		// 		print_r($params);die;
		//$this->view->fullamount = $modelremitance->getRemittanceTypeIndexTemp('4',$params['changes_id']);
		//$this->view->fullamountold = $modelremitance->getRemittanceTypeIndex('4','GLOBAL');

		//$this->view->provfee = $modelremitance->getRemittanceTypeIndexTemp('5',$params['changes_id']);
		//$this->view->provfeeold = $modelremitance->getRemittanceTypeIndex('5','GLOBAL');


		$cust_id = 'GLOBAL';
		$this->view->custid = $cust_id;

		$selectccy = $this->_db->select()
			->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));

		$listccy = $selectccy->query()->fetchAll();
		$paramccy = array();
		foreach ($listccy as $key => $value) {
			$paramccy[$value['CCY_ID']] = $value['CCY_ID'];
		}
		// echo '<pre>';
		// print_r($paramccy);
		$this->view->dataccy = $paramccy;
		$this->view->listccy = $listccy;

		$sqlQueryHoliday = "
			SELECT 
				CHARGE_ID,
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT 
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM M_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?) B
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
		//ORDER BY CHARGE_ID DESC";

		$transfee = $this->_db->fetchAll($sqlQueryHoliday, array('3', $cust_id));

		$fullfee = $this->_db->fetchAll($sqlQueryHoliday, array('4', $cust_id));

		$prfee = $this->_db->fetchAll($sqlQueryHoliday, array('5', $cust_id));

		foreach ($prfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'prccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keymin = 'prmin' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keymin = $value['CHARGE_PROV_MIN_AMT'];

			$keymax = 'prmax' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keymax = $value['CHARGE_PROV_MAX_AMT'];

			$keypct = 'prpct' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keypct = $value['CHARGE_PCT'];
			// }
		}

		foreach ($fullfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'faccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keyamt = 'faamt' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}
		// echo "<pre>";
		// print_r($transfee);
		foreach ($transfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'tfccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keyamt = 'tfamt' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}

		$this->view->transfee = $transfee;


		//temp
		$sqlQueryHoliday = "
			SELECT 
				
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT 
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM TEMP_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?
				AND CHANGES_ID = ?
				) B
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
		//ORDER BY CHARGE_ID DESC";

		$transfee = $this->_db->fetchAll($sqlQueryHoliday, array('3', $cust_id, $params['changes_id']));

		$fullfee = $this->_db->fetchAll($sqlQueryHoliday, array('4', $cust_id, $params['changes_id']));

		$prfee = $this->_db->fetchAll($sqlQueryHoliday, array('5', $cust_id, $params['changes_id']));

		if (empty($transfee) && empty($fullfee) && empty($prfee)) {
			$this->view->remithide = false;
		} else {
			$this->view->remithide = true;
		}
		//var_dump($prfee);die;
		foreach ($prfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'xprccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keymin = 'xprmin' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keymin = $value['CHARGE_PROV_MIN_AMT'];

			$keymax = 'xprmax' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keymax = $value['CHARGE_PROV_MAX_AMT'];

			$keypct = 'xprpct' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keypct = $value['CHARGE_PCT'];
			// }
		}

		foreach ($fullfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'xfaccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keyamt = 'xfaamt' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}
		// echo "<pre>";
		// print_r($transfee);
		foreach ($transfee as $key => $value) {
			// if(!empty($key)){
			$keyw = 'xtfccy' . $value['CCY_ID'];
			$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

			$keyamt = 'xtfamt' . $value['CCY_ID'];
			// print_r($keyamt);
			// echo "<br>";
			$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}

		$this->view->transfee = $transfee;

		// 		

		if (array_key_exists('changes_id', $params)) {
			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
			$validators = array(
				'changes_id' => array(
					'NotEmpty',
					'Digits',
					'messages' => array(
						'No Suggestion ID',
						'Wrong ID Format',
					),
				),
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
			if ($zf_filter_input->isValid()) {
				$tempData = $masterData = null;
				$changeId = $zf_filter_input->changes_id;
				$changeInfo = array();
				$changeInfo = $this->getGlobalChanges($changeId);


				if (empty($changeInfo)) {
					$this->_redirect('/notification/invalid/index');
				} else {

					if (!in_array($changeInfo['CHANGES_STATUS'], array('RR', 'WA'))) {
						$this->_redirect('/notification/success/index');
					}
					// 					$providerType = array ( 1 => 'Payment', 2 => 'Purchase');


					$this->view->suggested_by = $changeInfo['CREATED_BY'];
					$this->view->suggestion_date = $changeInfo['CREATED'];
					$this->view->temp_data = $tempData;
					$this->view->changes_id = $changeId;
					// 					$this->_redirect('/notification/success/index');
				}
			} else {
				$errors = $zf_filter_input->getMessages();
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
			}
		} else {
			$errorRemark = 'No Suggestion ID';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
		}

		Application_Helper_General::writeLog('CHCL', 'View Remittance Charges Changes List');
	}
}
