<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';
require_once 'SGO/Cron.php';
require_once 'CMD/Payment.php';
require_once 'Service/BulkPayment.php';
date_default_timezone_set('Asia/Jakarta');
$cron = new SGO_Cron();
$cron->buildCron('inprogressBulkMultiCreditStatus',realpath(dirname(__FILE__)),1);
$flag = $cron->getCronDirFlag();
$time = $cron->getCronTime();
$i = 0;
echo $cron->start();
// if (file_exists($flag)){
// 	echo $cron->inprogress(date('Y-m-d H:i:s'));
// }else{
	echo $cron->prediction();
	$cron->touchCronDirFlag(basename(__FILE__));
	$ii = 0;
	while(strtotime(date('Y-m-d H:i:s')) <= strtotime($time['end'])){
		$ii ++;
		echo $ii.'.';
		businessLogic($i);
		sleep(65);
	}
	$cron->rmCronDirFlag(basename(__FILE__));
// }
echo $cron->finish(basename(__FILE__),$i);

/****************/
/*** BL Start ***/
/****************/
function businessLogic(&$i = 0){
	$db 		= Zend_Db_Table::getDefaultAdapter();
	$config 	= Zend_Registry::get('config');
	$setting 	= new Settings();

	// INNER JOIN KE T_TRANSACTION
	// PS STATUS  7
	// EFTDATE (today)
	// PS CATEGORY (BULK PAYMENT)
	$listpayroll = array('11','25','31','32');
	$select = $db->select()
		->from(array('P' => 'T_PSLIP'),array('PS_NUMBER','CUST_ID'))
		->where('PS_STATUS = 8')
		->where('SENDFILE_STATUS = 1')
		->where('DATE(P.PS_EFDATE) = DATE(now())')
		//->where('P.PS_TYPE != ?', '11') //do not execute payroll
		->where('P.PS_TYPE NOT IN (?)', $listpayroll) 
		->order('PS_NUMBER ASC')
		->limit(5)
	;
	$data =  $db->fetchAll($select);

	if($data){
		foreach($data as $row){
			$i ++;
			$serviceBulkPayment =  new Service_BulkPayment();
			$result = $serviceBulkPayment->bulkpaymentstatus('CMS',$row['PS_NUMBER']);

			if ($result['ResponseCode'] == '00' || $result['ResponseCode'] == '0000'){
				$sqlTransaction  	= "SELECT * FROM T_TRANSACTION WHERE PS_NUMBER = ? ";
				$alltrx 			= $db->fetchAll($sqlTransaction, (string)$row['PS_NUMBER']);

				$resultTrx = array();

				if (is_array($result['transactionList'])){ //result tx list is array
					foreach ($result['transactionList'] as $key => $val) {
						$responseCode  = $val->status;
						$responseDesc  = $val->description;
						$transactionID = $val->transactionId;
						$responseCode  = ($responseCode == '00' || $responseCode == '0')? '00': $responseCode;

						$resultTrx['TRX'][$transactionID]['ResponseCode'] 		= $responseCode;
						$resultTrx['TRX'][$transactionID]['ResponseDesc'] 		= $responseDesc;	//$responseArray['TBRResult']->ResponseData;
						$resultTrx['TRX'][$transactionID]['ResponseData'] 		= $transactionID;

						$chargesStatus  = ($responseCode == '00')? 1: 0;	// 1: success, 0: failed
						// update
						updateChargesLog(array('chargesStatus'=>$chargesStatus),array('transactionID'=>$transactionID));
					}
				}else{ //result tx list is not array
					$txList = $result['transactionList'];
					$responseCode  = $txList->status;
					$responseDesc  = $txList->description;
					$transactionID = $txList->transactionId;
					$responseCode  = ($responseCode == '00' || $responseCode == '0')? '00': $responseCode;

					$resultTrx['TRX'][$transactionID]['ResponseCode'] 		= $responseCode;
					$resultTrx['TRX'][$transactionID]['ResponseDesc'] 		= $responseDesc;	//$responseArray['TBRResult']->ResponseData;
					$resultTrx['TRX'][$transactionID]['ResponseData'] 		= $transactionID;

					$chargesStatus  = ($responseCode == '00')? 1: 0;	// 1: success, 0: failed
					updateChargesLog(array('chargesStatus'=>$chargesStatus),array('transactionID'=>$transactionID));
				}

				if ($resultTrx && is_array($resultTrx)){
					$paymentObj =  new Payment($row['PS_NUMBER'], $row['CUST_ID']);
					$paymentObj->setPaymentCompleted($alltrx, $resultTrx);
				}
			}
		}
	}
}

function updateChargesLog($update = array(),$where = array()){
	$db 		= Zend_Db_Table::getDefaultAdapter();

	$updData 	= array('STATUS' => $update['chargesStatus']);
	$whereData 	= array('TRANSACTION_ID = ?' => $where['transactionID']);
	$db->update('T_CHARGES_LOG', $updData, $whereData);
}
/****************/
/**** BL End ****/
/****************/
?>