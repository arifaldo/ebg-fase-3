<?php


require_once 'Zend/Controller/Action.php';


class setbilleronboardcharges_IndexController extends Application_Main
{
	public function indexAction() 
	{
		$model = new setbilleronboardcharges_Model_Setbilleronboardcharges();
		
		$providerArr = $model->getServiceProvider();
		
		$this->view->var = $providerArr;	
		
		$fields = array	(
							'biller_code'  			=> array	(
																	'field' => 'PROVIDER_CODE',
																	'label' => $this->language->_('E-Commerce Code'),
																	'sortable' => true
																),
							'biller_name'  			=> array	(
																	'field' => 'PROVIDER_NAME',
																	'label' => $this->language->_('E-Commerce Name'),
																	'sortable' => true
																),
							/*'biller_type'  			=> array	(
																	'field' => 'PROVIDER_TYPE',
																	'label' => $this->language->_('E-Commerce Type'),
																	'sortable' => true
																),*/
							'charges_amt'  			=> array	(
																	'field' => 'CHARGES_AMT',
																	'label' => $this->language->_('Charges Amount'),
																	'sortable' => true
																),
							'suggest_date'  		=> array	(
																	'field' => 'CHARGES_SUGGESTED',
																	'label' => $this->language->_('Suggested Date'),
																	'sortable' => true
																),
							'suggestor'  		=> array	(
																	'field' => 'CHARGES_SUGGESTEDBY',
																	'label' => $this->language->_('Suggester'),
																	'sortable' => true
																),
						);
						
		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'billerCode'    	=> array('StripTags','StringTrim'),
	                       'billerName'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'suggestor'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'fDateFrom'  	=> array('StripTags','StringTrim'),
						   'fDateTo'  		=> array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array('filter' 		=> array(),
	                       'billerCode'    	=> array(),
	                       'billerName'  	=> array(),
						   'suggestor'  	=> array(),
						   'fDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );
	                      
		$fParam = array();	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $billerCode = $fParam['billerCode'] = html_entity_decode($zf_filter->getEscaped('billerCode'));
		$billerName = $fParam['billerName'] = html_entity_decode($zf_filter->getEscaped('billerName'));
		$suggestor = $fParam['suggestor'] = html_entity_decode($zf_filter->getEscaped('suggestor'));
		$datefrom = $fParam['datefrom'] = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$dateto	= $fParam['dateto'] = html_entity_decode($zf_filter->getEscaped('fDateTo'));
		
		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');
		
		$page = (Zend_Validate::is($page,'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		if($filter = TRUE)
		{
			if(!empty($datefrom))
			{
				$this->view->fDateTo    = $dateto;
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$fParam['datefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($dateto))
			{
				$this->view->fDateFrom  = $datefrom;
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
			}
			
		 	if($billerCode)
		    {
	       		$this->view->billerCode = $billerCode;
		    }
		    
			if($billerName)
			{
	       		$this->view->billerName = $billerName;
			}
			
			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
		    }
		}
		
		$data = $model->getData($fParam,$sortBy,$sortDir,$filter);
		
    	$this->paging($data);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;
		$this->setbackURL('/'.$this->_request->getModuleName());
    	if($csv || $pdf || $this->_request->getParam('print'))
    	{
    		$arr = $data;
    		
    		foreach($arr as $key=>$value)
			{
				unset($arr[$key]["PROVIDER_ID"]);
				$arr[$key]["CHARGES_SUGGESTED"] = Application_Helper_General::convertDate($value["CHARGES_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
	    	if($csv)
			{
				Application_Helper_General::writeLog('VWBH','Download CSV E-Commerce Charges');
				$this->_helper->download->csv(array('E-Commerce Code','E-Commerce Name','E-Commerce Type','Suggested Date', 'Suggester'),$arr,null,'E-Commerce Charges');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('VWBH','Download PDF E-Commerce Charges');
				$this->_helper->download->pdf(array('E-Commerce Code','E-Commerce Name','E-Commerce Type' , 'Suggested Date', 'Suggester'),$arr,null,'E-Commerce Charges');
			}
    		if($this->_request->getParam('print') == 1){
				$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'E-Commerce Charges', 'data_header' => $fields));
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('VWBH','View E-Commerce Charges');
    	}
	}
}