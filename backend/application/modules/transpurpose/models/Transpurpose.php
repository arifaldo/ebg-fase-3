<?php
Class transpurpose_Model_Transpurpose {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getCategory()
    {
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CATEGORY'),array('CATEGORY'));
		
       	return $this->_db->fetchall($select);
    }
	
    public function getTranspurpose($code)
    {
    	$select = $this->_db->select()
    	->from(array('A' => 'M_TRANSACTION_PURPOSE'),array('*'))
    	->where('CODE = ?', $code);
//     	->where('BANK_CODE = ?', $bankcode);
    
    	$res = $this->_db->fetchRow($select);
//     	$row = $res['totalrow'];
    	return $res;
    
    }
    
  
   
}