<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/MultiPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';

class payroll_FormController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		
		
		$isConfirmPage 	= $this->_getParam('confirmPage'); 
		$submitBtn 		= $this->_getParam('btnsubmit');
		$verify 		= $this->_getParam('verify');
		$process 		= $this->_getParam('process');
		$TRA_AMOUNT_tot = $this->_getParam('TRA_AMOUNT_tot');
		$disabled 		= 'disabled = "disabled"';
				
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$settings = new Application_Settings();
		$newsetting = new Settings();
		$filter = new Application_Filtering();
		
		$cust = $this->_custIdLogin;
		$maxsetting = 'max_row_multi_transaction';
		
		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  	= $CustomerUser->getAccounts();
		$ccyList  		= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
		$rowmax			= $newsetting->getSetting($maxsetting);
		$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
		$PS_EFDATE 		= Application_Helper_General::convertDate($this->getCurrentDate());
		
		
		$err = array();
		$arr = array();
				
		$this->view->ERROR_MSGTRX 		= $err;
		$this->view->arr 				= $arr;
		$this->view->AccArr 			= $AccArr;
		$this->view->ccyArr 			= $ccyList;
		$this->view->rowmax 			= $rowmax;
		$this->view->disabled 			= $disabled;
		$this->view->TRA_AMOUNT_tot 	= $TRA_AMOUNT_tot;
	
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_EFDATE 			= $PS_EFDATE;
		
		if (!empty($PS_NUMBER) && !$this->_request->isPost())
    	{	
			
    		$paramList = array("WA" 			=> false,
							   "ACCOUNT_LIST" 	=> $this->_accountList,
							   "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							   "paymentType" 	=> $this->_paymenttype,
							   "transferType" 	=> $this->_transfertype,
							 );
    		$select   = $CustomerUser->getPayment($paramList, false);
			$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);
			 
			// $select->columns(array(	"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
									// "acbenef_acct_name"		=> "T.BENEFICIARY_ACCOUNT_NAME"));
									
			$pslipData 	= $this->_db->fetchRow($select);
							 
			if (!empty($pslipData))	
			{	
				
				$PS_SUBJECT 		= $pslipData['paySubj'];
				$ACCTSRC 			= $pslipData['accsrc'];
				$CURR_CODE  		= $pslipData['ccy'];
				
				$PS_EFDATE  		= date("d/m/Y",strtotime($pslipData['efdate']));
				
				// $PS_EFDATE  		= Application_Helper_General::convertDate($pslipData['efdate'],$this->_dateViewFormat);  
				// $PS_EFDATE  		= $pslipData['efdate'];
								
				$TRA_AMOUNT_tot		= Application_Helper_General::displayMoney($pslipData['amount']); 
				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE']; 
								
				$db 	= Zend_Db_Table::getDefaultAdapter();
				$result = $db->fetchAll(
								$db	->select()
									->from(	array(	'a'=>'T_TRANSACTION' ),
											array(	'acbenef'			=>'BENEFICIARY_ACCOUNT',
													'ACBENEF_BANKNAME'	=>'BENEFICIARY_ACCOUNT_NAME',
													'ACBENEF_ALIAS'		=>'BENEFICIARY_ALIAS_NAME',
													'acbenefccy'		=>'BENEFICIARY_ACCOUNT_CCY',
													'traamount'			=>'TRA_AMOUNT',
													'tramessage'		=>'TRA_MESSAGE',
													'trarefno'			=>'TRA_REFNO',
												)
										)
									->where('a.PS_NUMBER = ?',(string) $PS_NUMBER)
				);
				
				foreach($result as $key => $val){
					$x = $key + 1;
										
					$acbenef 			= "ACBENEF".$x;
					$acbenefbankname 	= "ACBENEF_BANKNAME".$x;
					$acbenefalias 		= "ACBENEF_ALIAS".$x;
					$acbenefccy 		= "CURR_CODE".$x;
					
					$traamount			= "TRA_AMOUNT".$x;
					$tramessage 		= "TRA_MESSAGE".$x;
					$trarefno 			= "TRA_REFNO".$x;
					
					/*$accsrc 	= $$vara;
					$traamount 	= $$varb;
					$tramessage = $$varc;
					$trarefno 	= $$vard;*/
					
					$acbenefval 		= $result[$key]['acbenef'];
					$acbenefbanknameval = $result[$key]['acbenefbankname'];
					$acbenefaliasval 	= $result[$key]['acbenefalias'];
					$acbenefccyval 		= $result[$key]['acbenefccy'];
					
					$traamountval 	= Application_Helper_General::displayMoney($result[$key]['traamount']);
					$tramessageval 	= $result[$key]['tramessage'];
					$trarefnoval 	= $result[$key]['trarefno'];
					
					$this->view->$acbenef 			= $acbenefval;
					$this->view->$acbenefbankname 	= $acbenefbanknameval;
					$this->view->$acbenefalias 		= $acbenefaliasval;
					$this->view->$acbenefccy 		= $acbenefccyval;
					
					$this->view->$traamount 	= $traamountval;
					$this->view->$tramessage 	= $tramessageval;
					$this->view->$trarefno 		= $trarefnoval;
					
				}
				unset($acbenef);
				unset($acbenefbankname);
				unset($acbenefalias);
				unset($acbenefccy);
								
				unset($traamount);
				unset($tramessage);
				unset($trarefno);
				
				$this->view->arr = $result;
				
				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
				elseif ($PS_TYPE != $this->_paymenttype["code"]["payroll"])	// 0: PB multidebit & multicredit
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
			}
			else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
								
		
		}
		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP'))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}
		
		if($this->_request->isPost())
		{	
			
			$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), 			"PS_NUMBER");
			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PS_SUBJECT'), 	"PS_SUBJECT");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), 	"ACCOUNT_NO");
			$PS_EFDATE		= $filter->filter($this->_request->getParam('PS_EFDATE'), 	"PS_DATE");
			
			$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
			
			if(!$ACCTSRC)
			{
				//echo "ga ada!$ACCTSRC"; //die();
				$error_msg[0] = 'Error: Source Account can not be left blank.';
				$this->view->error 		= true;
				$this->view->ERROR_MSG	= $this->displayError($error_msg);
			}
			// else if(!$PS_EFDATE)
			else if(!$PS_EFDATE){
			
				$error_msg[0] = 'Error: Payment Date can not be left blank.';
				$this->view->error 		= true;
				$this->view->ERROR_MSG	= $this->displayError($error_msg);
				
			}
			else
			{
				
				
				if(!$validateDateFormat->isValid($PS_EFDATE))
				{					
					// $error_msg[0] = 'Error: Payment Date can not be left blank.';
					// $this->view->PS_EFDATE = $this->_getParam('PS_EFDATE');
					
					$error_msg[0] = 'Error: Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
					$this->view->error 		= true;
					$this->view->ERROR_MSG	= $this->displayError($error_msg);
					
					// print_r($error_msg[0]);
					// echo "yeee";
					// die;
				}
				else{
					
					$PS_EFDATE 		= $filter->filter($this->_request->getParam('PS_EFDATE'), "PS_DATE");
					
					//Validate Data Payment and Transactions
					$paramPayment = array( 	"CATEGORY"      	=> "PAYROLL",
											"FROM"       		=> "F", 								// F: Form, I: Import
											"PS_NUMBER"     	=> $PS_NUMBER,
											"PS_SUBJECT"   	 	=> $PS_SUBJECT,
											"PS_EFDATE"     	=> $PS_EFDATE,
											"_dateFormat"    	=> $this->_dateDisplayFormat,
											"_dateDBFormat"    	=> $this->_dateDBFormat,
											"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), 	// privi BADA (Add Beneficiary)
											"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), 	// privi BLBU (Linkage Beneficiary User)
											"_createPB"     	=> $this->view->hasPrivilege('CRMP'), 	// privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
											"_createDOM"    	=> false, 								// privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
											"_createREM"    	=> false,	       						// cannot create REM trx 
										 );
					
					$arr =  array();
					$bb = 0;
					
					$tot_amount = 0;
					$r = 0;
					
					foreach($_POST as $key => $value){
						
						$bb++;
						if($bb <= $rowmax){ 
								
							$acbenef = 'ACBENEF'.$bb;
							$acbenefbankname = 'ACBENEF_BANKNAME'.$bb;
							$acbenefalias = 'ACBENEF_ALIAS'.$bb;
							$currcode= 'CURR_CODE'.$bb;
							$traamount = 'TRA_AMOUNT'.$bb;
							$tramessage= 'TRA_MESSAGE'.$bb;
							$trarefno= 'TRA_REFNO'.$bb;
								
							$acbenefv = $filter->filter($this->_request->getParam($acbenef), "ACCOUNT_NO");
							$tra_amount = $filter->filter($this->_request->getParam($traamount), "AMOUNT");
								
							$tra_message = $filter->filter($this->_request->getParam($tramessage), "TRA_MESSAGE");
							$tra_refno = $filter->filter($this->_request->getParam($trarefno), "TRA_REFNO");
							$curr_code = $filter->filter($this->_request->getParam($currcode), "SELECTION");
							$acbenef_bankname = $filter->filter($this->_request->getParam($acbenefbankname), "ACCOUNT_NAME");
							$acbenef_alias = $filter->filter($this->_request->getParam($acbenefalias), "ACCOUNT_ALIAS");
																
								
							$TRA_AMOUNT_num = Application_Helper_General::convertDisplayMoney($tra_amount);
														
							if( ($acbenefv != "" && $tra_amount !="") || ($acbenefv != "" && $tra_amount =="") || ($acbenefv == "" && $tra_amount !="")){
								$arr[$r] = array(		"TRANSFER_TYPE" 			=> "PB",
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"TRA_MESSAGE" 				=> $tra_message,
														"TRA_REFNO" 				=> $tra_refno,
														"ACBENEF" 					=> $acbenefv,
														"ACBENEF_CCY" 				=> $curr_code,	
														"ACCTSRC" 					=> $ACCTSRC,	
															
														// for Beneficiary data, except (bene CCY and email), must be passed by reference
														"ACBENEF_BANKNAME" 			=> $acbenef_bankname,	
														"ACBENEF_ALIAS" 			=> $acbenef_alias,
														"ACBENEF_EMAIL" 			=> ""
																
												);
								$tot_amount = $tot_amount + $TRA_AMOUNT_num;
									
							}
								
							unset($acbenef);
							unset($acbenefbankname);
							unset($acbenefalias);
							unset($currcode);
							unset($traamount);
							unset($tramessage);
							unset($trarefno);
								
						}
							
						$r++;
					}
					unset ($bb);
					
					$TRA_AMOUNT_tot = Application_Helper_General::displayMoney($tot_amount);
					$this->view->TRA_AMOUNT_tot 	= $TRA_AMOUNT_tot;
					
									
					//$ACBENEFMINIMAL	= $filter->filter($this->_request->getParam('ACBENEF1'), "ACCOUNT_NO");
					if(empty($arr)){

						$error_msg[0] = 'Error: Please insert at least 1 (One) Account Beneficiary.';
						//$this->view->error		= true;
						//$this->view->ERROR_MSG	= $this->displayError($error_msg);
						
						$this->view->errorbene		= true;
						$this->view->ERROR_MSGbene	= $this->displayError($error_msg);
						
						
					}
					else{
											
						//Zend_Debug::Dump($paramPayment);die;
											
						$paramSettingID = array('range_futuredate', 'auto_release_payment');								 
					
						$settings->setSettings(null, $paramSettingID);	// Zend_Registry => 'APPSETTINGS'
						
	//					echo "<pre>";print_r($arr);
						$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
						$resultVal	= $validate->checkCreate($paramPayment, $arr);
						//Zend_Debug::dump($paramPayment);//die;
						//Zend_Debug::dump($arr);//die;
						
						$payment 		= $validate->getPaymentInfo();
						$ACCTSRC_CCY   	= $payment["acctsrcArr"][$ACCTSRC]["CCY_ID"];
						
						//var_dump($payment);
						
						//$this->view->TRA_AMOUNT_tot = $tot_amount;
						//echo "TRA_AMOUNT_tot: $TRA_AMOUNT_tot";
						
						if($validate->isError() === false)	// payment data is valid
						{
							
							if($isConfirmPage == false && $process == 'VERIFY'){
							
								$isConfirmPage 	= true;
								$validate->__destruct();
								unset($validate);
								
								// if (!empty($PS_NUMBER))
									// Application_Helper_General::writeLog('CRMP',"Confirmation Repair Payment Multi Credit Form. Payment Ref# = $PS_NUMBER");
								// else
									// Application_Helper_General::writeLog('CRMP','Confirmation Create Payment Multi Credit Form');
							
							}
							else{
								
								$validate->__destruct();
								unset($validate);
									
								$param['PS_EFDATE'] 		= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
								$param['PS_SUBJECT'] 		= $PS_SUBJECT;
								$param['PS_CCY'] 			= $ACCTSRC_CCY;
								$param['PS_TYPE'] 			= $this->_paymenttype['code']['payroll'];
								$param['_addBeneficiary'] 	= $this->view->hasPrivilege('BADA');
								$param['_beneLinkage']  	= $this->view->hasPrivilege('BLBU');
								$param['_priviCreate'] 		= 'CRMP';
									
								// Create Payment and Transactions after check
								for($x=0;$x<=$rowmax;$x++)
								{
										
									$y = $x+1;
									$data_temp = $this->_request->getParam('ACBENEF'.$y);
									if($data_temp)
									{
										$param['TRANSACTION_DATA'][$x]['TRANSFER_TYPE'] = "PB";
										$param['TRANSACTION_DATA'][$x]['SOURCE_ACCOUNT'] = $ACCTSRC;
										$param['TRANSACTION_DATA'][$x]['BENEFICIARY_ACCOUNT'] = $arr[$x]['ACBENEF']; //$this->_request->getParam('ACBENEF'.$y);
										$param['TRANSACTION_DATA'][$x]['BENEFICIARY_ACCOUNT_CCY'] = $arr[$x]['ACBENEF_CCY']; //$this->_request->getParam('CURR_CODE'.$y);
										$param['TRANSACTION_DATA'][$x]['BENEFICIARY_ACCOUNT_NAME'] = $arr[$x]['ACBENEF_BANKNAME']; //$this->_request->getParam('ACBENEF_BANKNAME'.$y);
										$param['TRANSACTION_DATA'][$x]['BENEFICIARY_ALIAS_NAME'] = $arr[$x]['ACBENEF_ALIAS']; //$this->_request->getParam('ACBENEF_ALIAS'.$y);
										$param['TRANSACTION_DATA'][$x]['TRA_AMOUNT'] = Application_Helper_General::convertDisplayMoney($this->_request->getParam('TRA_AMOUNT'.$y));
										$param['TRANSACTION_DATA'][$x]['TRA_MESSAGE'] = $this->_request->getParam('TRA_MESSAGE'.$y);
										$param['TRANSACTION_DATA'][$x]['TRA_REFNO'] = $this->_request->getParam('TRA_REFNO'.$y);
									}	
								}
								
								
									
								$MultiPayment = new MultiPayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
								if (!empty($PS_NUMBER))
								{	
									$MultiPayment->isRepair = true;	
								}
									
								$result = $MultiPayment->createPayment($param);
									
								if ($result === true)
								{	
																											
									$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
									$this->_redirect('/notification/success');	
										
								}
								else
								{	
									$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
									$this->_redirect('/notification/success');
								}
								
								
							}
						}
						else
						{
							$errorMsg 		= $validate->getErrorMsg();
							$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
							
							//var_dump($errorMsg);
							//var_dump($errorTrxMsg);
							
							$validate->__destruct();
							unset($validate);
									
							//$errMessage 	= (!empty($errorMsg))? $errorMsg: $errorTrxMsg[0];
							//Zend_Debug::Dump($errorMsg);die;
							
							if($errorMsg)
							{
								$isConfirmPage = false;
								$error_msg[0] = 'Error: '.$errorMsg;
								$this->view->error 		= true;
								$this->view->ERROR_MSG	= $this->displayError($error_msg);
								
							}
							if($errorTrxMsg){
							
								//Zend_Debug::dump($errorTrxMsg);die;
								$isConfirmPage = false;
								//$ccystring = $paramTrxArr['ACBENEF_CCY'];
								
								if(!empty($errorTrxMsg["PB"])){
									
									$err = array();
									foreach($errorTrxMsg["PB"] as $ccy =>$row){
										
										// Zend_Debug::dump($val);die;
										foreach ($row as $key => $val){
										
											$rr[0] = $val;
											$err[$key] = $this->displayError($rr);
											
										}
										
									}
									
									//Zend_Debug::dump($errorTrxMsg);die;
									//$error_msgtrx[$t] = 'Error: '.$errorTrxMsg["PB"][$ccystring][$t];
									//$err_display[$t] 	= $this->displayError($err);
													
								}							
								
							}
							
						}
						
						//Zend_Debug::dump($acbenefval);die;
						
					}
				}
			
			}
			
			$key = 0;
			$allparam = $this->_request->getParams();
			foreach($allparam as $value){
				$key++;
				if($key <= $rowmax){ 
						
					$acbenef = 'ACBENEF'.$key;
					$acbenefbankname = 'ACBENEF_BANKNAME'.$key;
					$acbenefalias = 'ACBENEF_ALIAS'.$key;
					$traamount = 'TRA_AMOUNT'.$key;
					$tramessage = 'TRA_MESSAGE'.$key;
					$trarefno = 'TRA_REFNO'.$key;
					$currcode = 'CURR_CODE'.$key;
							
					$acbenefval = $filter->filter($this->_request->getParam($acbenef), "ACCOUNT_NO");
					$tra_amountval = $filter->filter($this->_request->getParam($traamount), "AMOUNT");
					$tra_messageval = $filter->filter($this->_request->getParam($tramessage), "TRA_MESSAGE");
					$tra_refnoval = $filter->filter($this->_request->getParam($trarefno), "TRA_REFNO");
					$curr_codeval = $filter->filter($this->_request->getParam($currcode), "SELECTION");
					$acbenef_banknameval = $filter->filter($this->_request->getParam($acbenefbankname), "ACCOUNT_NAME");
					$acbenef_aliasval = $filter->filter($this->_request->getParam($acbenefalias), "ACCOUNT_ALIAS");
							
					//echo "$acbenef, $acbenefbankname, $acbenefalias, $traamount, $tramessage, $trarefno,$curr_code<br />";
					//echo "$acbenefval, $acbenef_banknameval, $acbenef_aliasval, $tra_amountval, $tra_messageval, $tra_refnoval,$curr_codeval<br />";
					//echo "this->view->$acbenef = $acbenefval";
							
					if($acbenef !=""){
						$this->view->$acbenef = $acbenefval;
						$this->view->$traamount = $tra_amountval;
						$this->view->$tramessage = $tra_messageval;
						$this->view->$trarefno = $tra_refnoval;
						$this->view->$currcode = $curr_codeval;
						$this->view->$acbenefbankname = $acbenef_banknameval;
						$this->view->$acbenefalias = $acbenef_aliasval;
					}
							
					unset($acbenef);
					unset($acbenefbankname);
					unset($acbenefalias);
					unset($currcode);
					unset($traamount);
					unset($tramessage);
					unset($trarefno);
							
						
				}
						
					
			}
			$filter->__destruct();
			unset($filter);
			unset($key);
			
			$this->view->ERROR_MSGTRX 		= $err;
			$this->view->arr 				= $arr;
			
								
			
	
		}
		
		$this->view->AccArr 			= $AccArr;
		$this->view->ccyArr 			= $ccyList;
		$this->view->rowmax 			= $rowmax;
		$this->view->disabled 			= $disabled;
		$this->view->confirmPage		= $isConfirmPage;
		$this->view->process			= $process;
		//$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;
		$this->view->submit				= $submitBtn;
		
		$this->view->TRA_AMOUNT_tot 	= $TRA_AMOUNT_tot;
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		$this->view->ACCTSRC 			= (isset($ACCTSRC))				? $ACCTSRC				: '';
		$this->view->PS_EFDATE 			= $PS_EFDATE;
		
		// die("$PS_EFDATE -> not valid dd");
		//(isset($PS_EFDATE))			? $PS_EFDATE			: $this->getCurrentDate();
		
		
	}
}
