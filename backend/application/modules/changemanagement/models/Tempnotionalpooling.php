<?php
/**
 * Tempgroup model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempnotionalpooling extends Changemanagement_Model_Tempchanges {	
	protected $_moduleId = 'BGR';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		
		//query from TEMP_BGROUP
		$bgroup = $this->dbObj->select()
						  	  ->from('TEMP_NOTIONAL_POOLING')
						  	  ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($bgroup)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(notional)';
			return false;
			return false;
		}
		//query from TEMP_BPRIVI_GROUP
		$bprivigroup = $this->dbObj->select()
						  		   ->from('TEMP_NOTIONAL_DETAIL')
						  		   ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  		   ->query()
						  		   ->fetchAll(Zend_Db::FETCH_ASSOC);
		
		if(!count($bprivigroup)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(detail)';
			return false;
		}
		//insert to master table M_BGROUP
		$insertArr = array_diff_key($bgroup,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['N_UPDATED'] = new Zend_Db_Expr('now()');
		
		$insertArr['N_UPDATEDBY'] = $actor;
		$bgroupins = $this->dbObj->insert('T_NOTIONAL_POOLING',$insertArr);
		if(!(boolean)$bgroupins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(notional)';
			return false;
		}
		
		//insert to master table M_BPRIVI_GROUP
		foreach($bprivigroup as $key=>$val) {
			$insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>'', 'BG_ID' => ''));
			$bprivigroupins = $this->dbObj->insert('T_NOTIONAL_DETAIL',$insertArr);
			if(!(boolean)$bprivigroupins) {
				$this->_errorCode = '82';
				$this->_errorMsg = 'Query failed(detail)';
				return false;
			}
		}
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		//query from TEMP_BGROUP
		$bgroup = $this->dbObj->select()
						  	  ->from('TEMP_BGROUP')
						  	  ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
						  	  
		if(!count($bgroup))
		{
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(BGroup)';
			return false;
		}
		//query from TEMP_BPRIVI_GROUP
		$bprivigroup = $this->dbObj->select()
						  		   ->from('TEMP_BPRIVI_GROUP')
						  		   ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  		   ->query()
						  		   ->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!count($bprivigroup)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(BPrivi)';
			return false;
		}
		
		//update record
		$updateArr = array_diff_key($bgroup,array('TEMP_ID'=>'','CHANGES_ID'=>'','BGROUP_ID'=>''));
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array($this->dbObj->quoteInto('BGROUP_ID = ?',$bgroup['BGROUP_ID']));
		$bgroupupdate = $this->dbObj->update('M_BGROUP',$updateArr,$whereArr);
		
		if(!(boolean)$bgroupupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(BGroup)';
			return false;
		}
		
		
		if(is_array($bprivigroup) && count($bprivigroup)) 
		{
			$bprivigroupdelete = $this->dbObj->delete('M_BPRIVI_GROUP',$whereArr);	
//			if(!(boolean)$bprivigroupdelete) {
//				$this->_errorCode = '82';
//				$this->_errorMsg = 'Query failed(BPrivi group)';
//				return false;
//			}
			
			
			foreach($bprivigroup as $val) 
			{
				$insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>'', 'BG_ID' => ''));
				
			    $bprivigroupinsert = $this->dbObj->insert('M_BPRIVI_GROUP',$insertArr);
				if(!(boolean)$bprivigroupinsert) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg  = 'Query failed(BPrivi group)';
					return false;
				}
			}
			
		}
		
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
		
		//query from M_BGROUP
		$bgroup = $this->dbObj->select()
						  ->from('TEMP_BGROUP')
						  ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($bgroup)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(BGroup)';
			return false;
		}				  
		$updateArr = array('BGROUP_STATUS' => 'A');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array($this->dbObj->quoteInto('BGROUP_ID = ?',$bgroup['BGROUP_ID']));
		$bgroupupdate = $this->dbObj->update('M_BGROUP',$updateArr,$whereArr);
		
		if(!(boolean)$bgroupupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(BGroup)';
			return false;
		}
		
		$deleteChanges  = $this->deleteActivate();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
		//query from TEMP_BGROUP
		$bgroup = $this->dbObj->select()
						  ->from('TEMP_BGROUP')
						  ->where($this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId))
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($bgroup)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(BGroup)';
			return false;
		}
		$updateArr = array('BGROUP_STATUS' => 'I');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array($this->dbObj->quoteInto('BGROUP_ID = ?',$bgroup['BGROUP_ID']));
		$bgroupupdate = $this->dbObj->update('M_BGROUP',$updateArr,$whereArr);
		
		if(!(boolean)$bgroupupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(BGroup)';
			return false;
		}
		
		$deleteChanges  = $this->deleteDeactivate();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

		//delete from TEMP_BGROUP
		$bgroupdelete = $this->dbObj->delete('TEMP_NOTIONAL_POOLING',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$bgroupdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(BGroup)';
//			return false;
//		}
		
		//delete from TEMP_BPRIVI_GROUP
		$bprivigroupdelete = $this->dbObj->delete('TEMP_NOTIONAL_DETAIL',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$bprivigroupdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(BPrivi group)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_BGROUP
		$bgroupdelete = $this->dbObj->delete('TEMP_BGROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$bgroupdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(BGroup)';
//			return false;
//		}
		
		//delete from TEMP_BPRIVI_GROUP
		$bprivigroupdelete = $this->dbObj->delete('TEMP_BPRIVI_GROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$bprivigroupdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(BPrivi group)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     * 
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	//delete from TEMP_BGROUP
		$bgroupdelete = $this->dbObj->delete('TEMP_BGROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$bgroupdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(BGroup)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

		//delete from TEMP_BGROUP
		$bgroupdelete = $this->dbObj->delete('TEMP_BGROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$bgroupdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(BGroup)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteDelete() {
		//will never be called
	}
}