<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class Changemanagement_IndexController extends Application_Main
{

	//	private $_suggestionStatusCP = array(	"" => "--Any Value--",
	//											"UR"=> "Unread Suggestion" ,
	//											"RS"=> "Read Suggestion",
	//											"RR"=> "Request Repaired",
	//											"RP"=> "Repaired Suggestion",
	//	);

	public function initController()
	{
		$this->_suggestionStatusCP = array(
			"" => '--' . $this->language->_('Any Value') . "--",
			"UR" => $this->language->_('Unread Suggestion'),
			"RS" => $this->language->_('Read Suggestion'),
			"RR" => $this->language->_('Request Repaired'),
			"RP" => $this->language->_('Repaired Suggestion'),
		);
	}

	/**
	 * The default action - show the home page
	 */
	//===============================================================================================================
	protected $_moduleDB  = 'WFA';

	public function indexAction()
	{


		if (
			$this->view->hasPrivilege('ABGC') || $this->view->hasPrivilege('ADLC') || $this->view->hasPrivilege('ASLC') || $this->view->hasPrivilege('AULC') || $this->view->hasPrivilege('RBGC')
			|| $this->view->hasPrivilege('RDLC') || $this->view->hasPrivilege('RSLC') || $this->view->hasPrivilege('RULC') || $this->view->hasPrivilege('APDC') || $this->view->hasPrivilege('VPDC')
		) {
		} else {
			$this->_redirect('/authorizationacl/index/index');
		}

		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$password = $sessionNamespace->token;

		$select = array();
		$displayDateFormat  			= $this->_dateDisplayFormat;
		$databaseSaveFormat 			= $this->_dateDBFormat;
		$this->view->dateJqueryFormat  	= $this->_dateJqueryFormat;

		$changesTypeCodeArr   = $this->_suggestType['code'];
		$changesStatusCodeArr = array_flip($this->_suggestionStatusCP);
		$changesStatusDescArr = $this->_suggestStatus['desc'];

		unset($this->_suggestType['code']['activate']);
		unset($this->_suggestType['desc']['activate']);
		unset($this->_suggestType['code']['deactivate']);
		unset($this->_suggestType['desc']['deactivate']);
		$this->view->suggestionType   =  $this->_suggestType;
		$this->view->suggestionStatus =  $this->_suggestionStatusCP;
		//companyCode
		//$listId = $this->_db->select()
		// ->from(array('M_CUSTOMER'),
		// 	   array('CUST_ID','CUST_NAME'))
		// ->order('CUST_ID ASC')
		// ->query()->fetchAll();

		//$this->view->listCustId 	= array_merge(array(''=>'-- ' .$this->language->_('Any Value').' --'),Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID'));
		//$this->view->listCustName 	= Application_Helper_Array::listArray($listId,'CUST_NAME','CUST_NAME');

		//$privi = Zend_Registry::get('privilege');	// get privi ID
		$privi = $this->_priviId;

		//get filtering param
		$filterArr = array(
			'Sarankan Data' 	  	=> array('StripTags'),
			//'cust_id' 	  		=> array('StringTrim','StripTags'),
			//'cust_name' 	  	=> array('StripTags'),
			'ID Data' 			=> array('StringTrim', 'StripTags'),
			'Nama Data' 		=> array('StripTags'),
			'Jenis Saran' 	=> array('StringTrim', 'StripTags'),
			'Status' 	=> array('StringTrim', 'StripTags'),
			'Disarankan Oleh' 		=> array('StringTrim', 'StripTags'),
			// 'Saran Tanggal' => array('StringTrim', 'StripTags'),
			// 'Saran Tanggal_END'	=> array('StringTrim', 'StripTags'),
		);


		$dataParam = array('Sarankan Data', 'ID Data', 'Jenis Saran', 'Status', 'Disarankan Oleh');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {



			if (!empty($this->_request->getParam('whereco'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('whereco') as $key => $value) {
					if ($dtParam == $value) {
						if (!empty($dataParamValue[$dtParam])) {
							$dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
						}

						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}



			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		if (!empty($this->_request->getParam('createdate'))) {
			$createarr = $this->_request->getParam('createdate');
			$dataParamValue['CREATED'] = $createarr[0];
			$dataParamValue['CREATED_END'] = $createarr[1];
		}

		$options = array('allowEmpty' => true);
		$validators = array(
			'Disarankan Oleh'	=> array(),
			'ID Data' 		=> array(),
			'Nama Data' 		=> array(),
			'Jenis Saran' 		=> array(),
			'Status' 	=> array(),
			'Disarankan Oleh' 		=> array(),
			// 'Saran Tanggal'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			// 'Saran Tanggal_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),

		);
		$zf_filter = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->setbackURL();

		$fields = array(
			'tableName'    => array(
				'field' => 'DISPLAY_TABLENAME',
				'label' => $this->language->_('Suggest Data'),
				'sortable' => true
			),
			// 'companyCode'  => array('field' => 'COMPANY_CODE',
			// 						'label' => $this->language->_('Company Code'),
			// 						'sortable' => true),
			// 'companyName'  => array('field' => 'COMPANY_NAME',
			// 						'label' => $this->language->_('Company Name'),
			// 						'sortable' => true),
			'dataId'  => array(
				'field' => 'KEY_FIELD',
				'label' => $this->language->_('Data ID'),
				'sortable' => true
			),
			'dataName'  => array(
				'field' => 'KEY_VALUE',
				'label' => $this->language->_('Data Name'),
				'sortable' => true
			),
			'suggestionDate' => array(
				'field' => 'G.CREATED',
				'label' => $this->language->_('Suggestion Date'),
				'sortable' => true
			),
			'suggestionBy' => array(
				'field' => 'CREATED_BY',
				'label' => $this->language->_('Suggested By'),
				'sortable' => true
			),
			'suggestionType'  => array(
				'field' => 'CHANGES_TYPE',
				'label' => $this->language->_('Suggestion Type'),
				'sortable' => true
			),
			'suggestionInfo' => array(
				'field' => 'suggestion_status',
				'label' => $this->language->_('Suggestion Status'),
				'sortable' => true
			),
		);

		// $filterlist = array('Sarankan Data', 'ID Data', 'Jenis Saran', 'Status', 'Disarankan Oleh', 'Saran Tanggal');
		$filterlist = array('Sarankan Data', 'ID Data', 'Jenis Saran', 'Status', 'Disarankan Oleh');

		$this->view->filterlist = $filterlist;


		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'suggestionDate');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		//Zend_Debug::dump($sortBy);die;
		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;





		if ($filter == TRUE) {
			$suggestdata 	= html_entity_decode($zf_filter->getEscaped('Sarankan Data'));
			//$cust_id 		= html_entity_decode($zf_filter->getEscaped('cust_id'));
			//$cust_name 		= html_entity_decode($zf_filter->getEscaped('cust_name'));
			$data_id 		= html_entity_decode($zf_filter->getEscaped('ID Data'));
			$data_name 		= html_entity_decode($zf_filter->getEscaped('Nama Data'));
			$suggestor		= html_entity_decode($zf_filter->getEscaped('Disarankan Oleh'));

			$changesCreatedFrom = (Zend_Date::isDate($zf_filter->getEscaped('Saran Tanggal'), $displayDateFormat)) ?
				new Zend_Date($zf_filter->getEscaped('Saran Tanggal'), $displayDateFormat) :
				false;

			$changesCreatedTo 	= (Zend_Date::isDate($zf_filter->getEscaped('Saran Tanggal_END'), $displayDateFormat)) ?
				new Zend_Date($zf_filter->getEscaped('Saran Tanggal_END'), $displayDateFormat) :
				false;

			$suggestionType 	= $zf_filter->getEscaped('Jenis Saran'); //(Zend_Validate::is($zf_filter->getEscaped('CHANGES_TYPE'),'InArray',array('haystack'=>$changesTypeCodeArr)))?
			//$zf_filter->getEscaped('CHANGES_TYPE') :
			//false;
			$suggestionStatus 	= (Zend_Validate::is($zf_filter->getEscaped('Status'), 'InArray', array('haystack' => $changesStatusCodeArr))) ?
				$zf_filter->getEscaped('Status') :
				false;

			$this->view->suggest_data 	= $suggestdata;
			//$this->view->cust_id 		= $cust_id;
			//$this->view->cust_name 		= $cust_name;
			$this->view->data_id 		= $data_id;
			$this->view->data_name 		= $data_name;
			$this->view->suggestor 		= $suggestor;

			if ($changesCreatedFrom)
				$this->view->changesCreatedFrom = $changesCreatedFrom->toString($displayDateFormat);
			if ($changesCreatedTo)
				$this->view->changesCreatedTo   = $changesCreatedTo->toString($displayDateFormat);
			if ($suggestionType)
				$this->view->suggestionTypeValue = $suggestionType;
			if ($suggestionStatus)
				$this->view->suggestionStatusValue = $suggestionStatus;
		}
		//var_dump($zf_filter->getEscaped('CHANGES_TYPE'));

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  	= new Changemanagement_Model_Privilege();
			$listAutModuleArr 			= $changeModulePrivilegeObj->getAuthorizeModule();
			$whPriviID 					= "'" . implode("','", $listAutModuleArr) . "'";

			//echo '<pre>';print_r($whPriviID);die;
			$caseWhenRequestChangeList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN 'Request Repaired'
				 WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN 'Unread Suggestion'
				 WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN 'Read Suggestion'
				 WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN 'Repaired Suggestion'
				 END
				";

			$cust_id = $this->_custIdLogin;

			//compose query (use filtering and sorting)
			$select = $this->_db->select()
				->from(
					array('G' => 'T_GLOBAL_CHANGES'),
					array(
						'*',
						'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)
					)
				)
				// ->where('CHANGES_FLAG = '.$this->_db->quote('B')) // B : Backend / F : Frontend. ############# REMOVE KARENA ADA CREATE USER DARI FO !??!###############
				//->where("CHANGES_FLAG = ?", "F")
				->where("CUST_ID = " . $this->_db->quote($cust_id) . " OR CUST_ID ='BANK'")
				->where("G.MODULE IN (" . $whPriviID . ")")
				->where("COMPANY_CODE = ?", $cust_id);
			//	        				->where('UPPER(CREATED_BY) <> '.$this->_db->quote(strtoupper($this->_userIdLogin)))
			//->order('CREATED DESC');
			//					}	// if (!empty($priviMod))


		}


		if ($filter == TRUE) {

			//where clauses
			// if ($suggestdata && $suggestdata != 'all') {
			if ($dataParamValue["Sarankan Data"] && $dataParamValue["Sarankan Data"] != 'all') {
				// $select->where("UPPER(DISPLAY_TABLENAME) LIKE ".$this->_db->quote($suggestdata));
				$suggestdataarr = explode(',', $dataParamValue["Sarankan Data"]);
				// $select->where("UPPER(DISPLAY_TABLENAME) in (?)", $suggestdataarr);
				$select->where("MODULE in (?)", $suggestdataarr);
			}
			//       if($cust_id)
			// 	$select->where("UPPER(COMPANY_CODE) LIKE ".$this->_db->quote('%'.$cust_id.'%'));
			// if($cust_name)
			// 	$select->where("UPPER(COMPANY_NAME) LIKE ".$this->_db->quote('%'.$cust_name.'%'));
			if ($dataParamValue["ID Data"]) {
				// $select->where("UPPER(KEY_FIELD) LIKE ".$this->_db->quote('%'.$data_id.'%'));
				$data_idarr = explode(',', $dataParamValue["ID Data"]);
				$select->where("UPPER(KEY_FIELD) in (?)", $data_idarr);
			}

			if ($dataParamValue["Jenis Saran"]) {
				$jenisSaran = explode(',', $dataParamValue["Jenis Saran"]);
				$select->where("UPPER(CHANGES_TYPE) in (?)", $jenisSaran);
			}

			if ($dataParamValue["Status"]) {
				$saranBaca = explode(',', $dataParamValue["Status"]);
				$select->where("READ_STATUS in (?)", $saranBaca);
			}

			if ($dataParamValue["Disarankan Oleh"]) {
				$suggester = explode(',', $dataParamValue["Disarankan Oleh"]);
				$select->where("CREATED_BY IN (?)", $suggester);
			}

			if ($dataParamValue["CREATED"])
				$select->where("DATE(CREATED) >= DATE(" . $this->_db->quote($dataParamValue["CREATED"]) . ")");
			if ($dataParamValue["CREATED_END"])
				$select->where("DATE(CREATED) <= DATE(" . $this->_db->quote($dataParamValue["CREATED_END"]) . ")");

			if ($data_name) {
				// $select->where("UPPER(KEY_VALUE) LIKE ".$this->_db->quote('%'.$data_name.'%'));
				$data_namearr = explode(',', $data_name);
				$select->where("UPPER(KEY_VALUE)  in (?)", $data_namearr);
			}
			if ($suggestor) {
				// $select->where("UPPER(CREATED_BY) LIKE ".$this->_db->quote('%'.$suggestor.'%'));
				$suggestorarr = explode(',', $suggestor);
				$select->where("UPPER(CREATED_BY)  in (?)", $suggestorarr);
			}
			if ($changesCreatedFrom)
				$select->where("DATE(CREATED) >= DATE(" . $this->_db->quote($changesCreatedFrom->toString($databaseSaveFormat)) . ")");
			if ($changesCreatedTo)
				$select->where("DATE(CREATED) <= DATE(" . $this->_db->quote($changesCreatedTo->toString($databaseSaveFormat)) . ")");
			if ($suggestionType) {
				$suggestionType = explode(',', $suggestionType);
				$select->where("CHANGES_TYPE IN (?)", $suggestionType);
			}
			if ($suggestionStatus) {
				if ($suggestionStatus == "RR") {
					$suggestionStatus = 'Request Repaired';
					$select->where("CHANGES_STATUS = " . $this->_db->quote('RR'));
				} else {
					if ($suggestionStatus == "UR") {
						$select->where('READ_STATUS = 0 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
					} else if ($suggestionStatus == "RS") {
						$select->where('READ_STATUS = 1 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
					} else if ($suggestionStatus == "RP") {
						$select->where('READ_STATUS = 2 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
					}
				}
			} else {
				$select->where('(CHANGES_STATUS = ' . $this->_db->quote('WA') . ' OR CHANGES_STATUS = ' . $this->_db->quote('RR') . ')');
			}
		} else {
			$select->where('(CHANGES_STATUS = ' . $this->_db->quote('WA') . ' OR CHANGES_STATUS = ' . $this->_db->quote('RR') . ')');
		}

		$select->order(array($sortBy . ' ' . $sortDir));
		// 	        print_r($select->query());die;
		//echo $select;die;
		$result = $select->query()->fetchall();
		// 			Zend_Debug::dump($select->__toString());die;
		$this->view->fields 		= $fields;
		$this->view->paginatorParam = $result;

		//process posted data
		if ($this->_request->isPost()) {
			$filterArr = array(
				'change_id' => 'StringTrim',
				'change_act' => 'Alpha',
				'change_note' => 'StringTrim',
				'action' => 'StringTrim'
			);

			$zf_filter = new Zend_Filter_Input($filterArr, array(), $this->_request->getPost());

			$updated_id 	= array();
			$failed_id 		= array();
			$updated_status = array();
			$failed_status 	= array();
			$failed_message = array();
			$success_remark = array();
			$clauseChanges 	= "";

			if (is_array($zf_filter->change_id)) {
				$content['changesId'] = $payment;

				$rand = null;
				for ($i = 0; $i < 4; $i++) {
					$rand =  $rand . rand(0, 9);
				}

				$AESMYSQL = new Crypt_AESMYSQL();
				foreach ($zf_filter->change_id as $value) {

					$newDataChangesId[] = $AESMYSQL->decrypt($value, $password);
				}

				$sessionNamespace = new Zend_Session_Namespace('confirmRequestChangeList');
				$sessionNamespace->content 	= $newDataChangesId;
				$sessionNamespace->action 	= $zf_filter->action;
				$sessionNamespace->note 	= $zf_filter->change_note;
				$sessionNamespace->token 	= $rand;
				// 					print_r($zf_filter);die;
				$this->_redirect('/changemanagement/index/confirm');
				/*
						$note = array();
						for($i=0;$i<count($zf_filter->change_id);$i++)
						{
								if(Zend_Validate::is($zf_filter->change_id[$i],'Db_RecordExists',(array('table'=>'T_GLOBAL_CHANGES','field'=>'CHANGES_ID'))))
								{
									$gcmodel = new Changemanagement_Model_Globalchanges($zf_filter->change_id[$i]);
									$changesInfo  = $gcmodel->getChangesInfo();
									if($changesInfo['CHANGES_STATUS'] =='WA' ){
										if($zf_filter->action =='Approve'){
											$updated = $gcmodel->approve($this->_userIdLogin,$zf_filter->change_note);
											$message = 'Suggesth on Granted';
										}elseif($zf_filter->action =='Reject'){
											$updated = $gcmodel->reject($this->_userIdLogin,$zf_filter->change_note);
											$message = 'Suggestion Rejected';
										}elseif($zf_filter->action =='Request Repair'){
											$updated = $gcmodel->requestRepair($this->_userIdLogin,$zf_filter->change_note);
											$message = 'Suggestion Repair Requested';
										//else update status not recognized/no change,
										}else{
											continue;
										}

										if($updated) {
											$updated_id[$i] = $gcmodel->getSuggestData();
											$pos = array_search($zf_filter->change_act[$i],$changesStatusCodeArr);
											$updated_status[$i] = $message;
											$this->view->msgSuccess = $message;
											if($gcmodel->getErrorCode())
												$success_remark[$i] = $message;
												Application_Helper_General::writeLog('','Updating suggestion id ' .$zf_filter->change_id[$i].$gcmodel->getErrorMessage());

										}
										else
										{
											//utk keperluan error server token jika terjadi error / server mati

												$error = null;
												$note[$zf_filter->change_id[$i]] = $zf_filter->change_note;
												$failed_id[$i] = $gcmodel->getSuggestData();
												$pos = array_search($zf_filter->change_act[$i],$changesStatusCodeArr);
												$error .= ' : '.$gcmodel->getErrorMessage();
												$failed_message[$i] = $gcmodel->getErrorMessage();;

												Application_Helper_General::writeLog('','Failed Updating suggestion id ' .$zf_filter->change_id[$i].$gcmodel->getErrorMessage());
											//}

										}
									}else{
										$failed_id[$i] = $gcmodel->getSuggestData();
										$pos = array_search($zf_filter->change_act[$i],$changesStatusCodeArr);
										$error .= ' : '.$gcmodel->getErrorMessage();
										$failed_message[$i] = 'Changes could not be processed due to current status are prohibited';

										Application_Helper_General::writeLog('','Failed Updating suggestion id ' .$zf_filter->change_id[$i].$gcmodel->getErrorMessage());
										$this->view->error = true;
									}
							}
						}
						$this->view->note = $note;
					*/
			} else {
				$this->view->error = true;
				$this->view->report_msg = 'Please Checked Selection';
			}
			$this->view->updatedId = $updated_id;
			$this->view->updatedStatus = $updated_status;
			$this->view->failedId = $failed_id;
			$this->view->failedStatus = $failed_status;
			$this->view->failedMessage = $failed_message;
			$this->view->successRemark = $success_remark;

			$this->view->updated = true;
		} {
		}

		$this->paging($select);

		if (!empty($dataParamValue)) {
			// $this->view->createdStart = $dataParamValue['PS_CREATED'];
			// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
			//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
			//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

			// unset($dataParamValue['PS_CREATED_END']);
			// unset($dataParamValue['PS_EFDATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',', $value);
				if (!empty($duparr)) {

					foreach ($duparr as $ss => $vs) {
						$wherecol[]	= $key;
						$whereval[] = $vs;
					}
				} else {
					$wherecol[]	= $key;
					$whereval[] = $value;
				}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}

		// if(!empty($dataParamValue)){
		// 	foreach ($dataParamValue as $key => $value) {
		// 		$wherecol[]	= $key;
		// 		$whereval[] = $value;
		// 	}
		// $this->view->wherecol     = $wherecol;
		// $this->view->whereval     = $whereval;

		//   }

		if (!empty($zf_filter->getEscaped('CREATED'))) {
			$this->view->createdStart 	= $zf_filter->getEscaped('CREATED');
		}

		if (!empty($zf_filter->getEscaped('CREATED_END'))) {
			$this->view->createdEnd 	= $zf_filter->getEscaped('CREATED_END');
		}



		$URL = $this->view->url(array(
			'module'	 => $this->view->modulename,
			'controller' => $this->view->controllername,
			'action'	 => 'index',
			'page'		 => $page,
			'sortBy' 	 => $this->view->sortBy,
			'sortDir' 	 => $this->view->sortDir
		), null, true) . $this->view->qstring;

		$sessionNamespace = new Zend_Session_Namespace('URL_CHG_CONFIRM');
		$sessionNamespace->URL = $URL;
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace 	= new Zend_Session_Namespace('confirmRequestChangeList');
		$dataChangesId 		= $sessionNamespace->content;
		$action 			= $sessionNamespace->action;
		$note 				= $sessionNamespace->note;
		$token 				= $sessionNamespace->token;

		$this->view->action = $action;
		$this->view->note 	= $action;
		$this->view->token 	= $token;

		$sessionNamespace = new Zend_Session_Namespace('URL_CHG_CONFIRM');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ?
			$sessionNamespace->URL : '/' . $this->view->modulename . '/' . $this->_controllerList . '/index';

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$select = array();
		$displayDateFormat  = $this->_dateDisplayFormat;
		$databaseSaveFormat = $this->_dateDBFormat;
		$this->view->dateJqueryFormat  = $this->_dateJqueryFormat;

		$changesTypeCodeArr   = $this->_suggestType['code'];
		$changesStatusCodeArr = array_flip($this->_suggestionStatusCP);
		$changesStatusDescArr = $this->_suggestStatus['desc'];

		unset($this->_suggestType['code']['activate']);
		unset($this->_suggestType['desc']['activate']);
		unset($this->_suggestType['code']['deactivate']);
		unset($this->_suggestType['desc']['deactivate']);
		$this->view->suggestionType   =  $this->_suggestType;

		$this->view->suggestionStatus =  $this->_suggestionStatusCP;
		//companyCode
		// $listId = $this->_db->select()
		// 					->from(array('M_CUSTOMER'),
		// 						   array('CUST_ID','CUST_NAME'))
		// 					->order('CUST_ID ASC')
		// 					->query()->fetchAll();

		// $this->view->listCustId 	= array_merge(array(''=>'-- '.$this->language->_('Any Value').' --'),Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID'));
		// $this->view->listCustName 	= Application_Helper_Array::listArray($listId,'CUST_NAME','CUST_NAME');

		//$privi = Zend_Registry::get('privilege');	// get privi ID
		$privi 						= $this->_priviId;

		//get filtering param
		$filterArr = array(
			'filter'			=> array('StringTrim', 'StripTags'),
			'suggest_data' 	  	=> array('StripTags'),
			//                'cust_id' 	  		=> array('StringTrim','StripTags'),
			// 'cust_name' 	  	=> array('StripTags'),
			'data_id' 			=> array('StringTrim', 'StripTags'),
			'data_name' 		=> array('StripTags'),
			'suggestionType' 	=> array('StringTrim', 'StripTags'),
			'suggestionStatus' 	=> array('StringTrim', 'StripTags'),
			'suggestor' 		=> array('StringTrim', 'StripTags'),
			'changesCreatedFrom' => array('StringTrim', 'StripTags'),
			'changesCreatedTo'	=> array('StringTrim', 'StripTags'),
		);

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$fields = array(
			'tableName'    => array(
				'field' => 'DISPLAY_TABLENAME',
				'label' => 'Suggest Data',
				'sortable' => false
			),
			// 'companyCode'  => array('field' => 'COMPANY_CODE',
			// 				    'label' => 'Company Code',
			// 				    'sortable' => false),
			// 'companyName'  => array('field' => 'COMPANY_NAME',
			// 				    'label' => 'Company Name',
			// 				    'sortable' => false),
			'dataId'  => array(
				'field' => 'KEY_FIELD',
				'label' => 'Data ID',
				'sortable' => false
			),
			'dataName'  => array(
				'field' => 'KEY_VALUE',
				'label' => 'Data Name',
				'sortable' => false
			),
			'suggestionDate' => array(
				'field' => 'G.CREATED',
				'label' => 'Suggested Date',
				'sortable' => false
			),
			'suggestionBy' => array(
				'field' => 'CREATED_BY',
				'label' => 'Suggested By',
				'sortable' => false
			),
			'suggestionType'  => array(
				'field' => 'CHANGES_TYPE',
				'label' => 'Suggestion Type',
				'sortable' => false
			),
			'suggestionInfo' => array(
				'field' => 'suggestion_status',
				'label' => 'Suggestion Status',
				'sortable' => false
			),
		);

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'suggestionDate');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		//Zend_Debug::dump($sortBy);die;
		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
			$listAutModuleArr = $changeModulePrivilegeObj->getAuthorizeModule();

			$whPriviID = "'" . implode("','", $listAutModuleArr) . "'";

			$caseWhenRequestChangeList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN 'Request Repaired'
											 WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN 'Unread Suggestion'
											 WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN 'Read Suggestion'
											 WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN 'Repaired Suggestion'
											 END
											";

			//$cust_id = $this->_custIdLogin;

			//compose query (use filtering and sorting)
			$select = $this->_db->select()
				->from(
					array('G' => 'T_GLOBAL_CHANGES'),
					array(
						'*',
						'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)
					)
				)
				// ->where('CHANGES_FLAG = '.$this->_db->quote('B'))
				->where("CHANGES_FLAG = ?", "F")
				//->where("CUST_ID = ?", $cust_id)
				->where("G.MODULE IN (" . $whPriviID . ")");
		}

		foreach ($dataChangesId as $value) {
			$AESMYSQL = new Crypt_AESMYSQL();
			$acct = $AESMYSQL->decrypt($this->_getParam('acct'), $password);
		}

		//where clauses
		$changesId = "'" . implode("','", $dataChangesId) . "'";
		$select->where("G.CHANGES_ID IN (" . $changesId . ")");
		//order clause

		$select->order(array($sortBy . ' ' . $sortDir));
		$dataSelectedChanges 		= $this->_db->fetchAll($select);

		$this->view->paginator 		= $dataSelectedChanges;
		$arrayChangesError 			= array();
		$this->view->fields 		= $fields;
		$this->view->paginatorParam = $select;



		//process posted data
		if ($this->_request->isPost()) {
			$filterArr = array(
				'change_id' => 'Digits',
				'change_act' => 'Alpha',
				'change_note' => 'StringTrim',
				'action' => 'StringTrim',
				'token' => 'StringTrim'
			);

			$zf_filter 		= new Zend_Filter_Input($filterArr, array(), $this->_request->getPost());
			$updated_id 	= array();
			$failed_id 		= array();
			$updated_status = array();
			$failed_status 	= array();
			$failed_message = array();
			$success_remark = array();

			$clauseChanges = "";


			if (is_array($dataChangesId)) {
				for ($i = 0; $i < count($dataChangesId); $i++) {
					if (Zend_Validate::is($dataChangesId[$i], 'Db_RecordExists', (array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID')))) {
						$gcmodel = new Changemanagement_Model_Globalchanges($dataChangesId[$i]);
						$changesInfo  = $gcmodel->getChangesInfo();
						// 							print_r('here');
						// 							print_r($note);die;

						if ($changesInfo['CHANGES_STATUS'] == 'WA') {
							if ($zf_filter->action == $this->language->_('Approve')) {

								//print_r($zf_filter->action);die;
								$updated = $gcmodel->approve($this->_userIdLogin, $note);
								$message = $this->language->_('Suggestion Granted');
							} elseif ($zf_filter->action == $this->language->_('Reject')) {
								$updated = $gcmodel->reject($this->_userIdLogin, $note);
								$message = $this->language->_('Suggestion Rejected');
							} elseif ($zf_filter->action == $this->language->_('Request Repair')) {
								$updated = $gcmodel->requestRepair($this->_userIdLogin, $note);
								$message = $this->language->_('Suggestion Repair Requested');
								//else update status not recognized/no change,
							} else {
								continue;
							}


							if ($updated) {
								$updated_id[$i] = $gcmodel->getSuggestData();
								$pos = array_search($zf_filter->change_act[$i], $changesStatusCodeArr);
								$updated_status[$i] = $message;
								$this->view->msgSuccess = $message;
								$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($changesInfo['MODULE'], $zf_filter->action);
								Application_Helper_General::writeLog($privilgeDualControl, 'Updating suggestion id ' . $dataChangesId[$i] . $gcmodel->getErrorMessage());
								$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Succes';
							} else {
								$error = null;
								$failed_id[$i] = $gcmodel->getSuggestData();
								$error .= ' : ' . $gcmodel->getErrorMessage();
								$failed_message[$i] = $gcmodel->getErrorMessage();
								$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($changesInfo['MODULE'], $zf_filter->action);
								Application_Helper_General::writeLog($privilgeDualControl, 'Failed Updating suggestion id ' . $dataChangesId[$i] . $gcmodel->getErrorMessage());
								$arrayChangesError[$changesInfo['CHANGES_ID']] = $gcmodel->getErrorMessage();
							}
						} else {
							$failed_id[$i] = $gcmodel->getSuggestData();
							$error .= ' : ' . $gcmodel->getErrorMessage();
							$failed_message[$i] = 'Changes could not be processed due to current status are prohibited';
							$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($changesInfo['MODULE'], $zf_filter->action);
							Application_Helper_General::writeLog($privilgeDualControl, 'Failed Updating suggestion id ' . $dataChangesId[$i] . $gcmodel->getErrorMessage());
							$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Changes could not be processed due to current status are prohibited';
							$this->view->error = true;
						}
					}
				}
				$this->view->note = $note;

				$conten 	= $zf_filter->change_id;
				$error 		= $zf_filter->change_id;
				$action 	= $zf_filter->action;
				$note 		= $zf_filter->change_note;
				if ($zf_filter->token == $token) {
					$tokenerr 	= null;
				} else {
					$tokenerr = 1;
				}
			}

			$sessionNamespace 				= new Zend_Session_Namespace('notificationRequestChangeList');
			$sessionNamespace->content 		= $conten;
			$sessionNamespace->changeserror = $error;
			$sessionNamespace->action 		= $action;
			$sessionNamespace->note 		= $note;
			$sessionNamespace->description 	= $arrayChangesError;
			$sessionNamespace->tokenerr 	= $tokenerr;
			$this->_redirect('/changemanagement/index/notification');
		} else {
			if (is_array($dataSelectedChanges)) {
				foreach ($dataSelectedChanges as  $changesInfo) {
					// if($changesInfo['CHANGES_FLAG'] == 'F' && $changesInfo['CHANGES_TYPE'] == 'N' && $changesInfo['MAIN_TABLENAME'] == 'M_USER' && $action == $this->language->_('Request Repair')){
					// 	$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Changes could not be processed due to current status are prohibited';
					// }


					if ($changesInfo['CHANGES_STATUS'] == 'WA') {
						if ($action == $this->language->_('Approve')) {
							$changeModulePrivilegeObj  	= new Changemanagement_Model_Privilege();
							$isAuthorizeAppove 			= $changeModulePrivilegeObj->isAuthorizeApprove($changesInfo['MODULE']);
							if (!$isAuthorizeAppove) {
								$arrayChangesError[$changesInfo['CHANGES_ID']] = 'You have no privilege for approve specified changes';
							}
						} else if ($action == $this->language->_('Reject')) {
							$changeModulePrivilegeObj  	= new Changemanagement_Model_Privilege();
							$isAuthorizeReject 			= $changeModulePrivilegeObj->isAuthorizeReject($changesInfo['MODULE']);
							if (!$isAuthorizeReject) {
								$arrayChangesError[$changesInfo['CHANGES_ID']] = 'You have no privilege for reject specified changes';
							}
						} else if ($action == $this->language->_('Request Repair')) {
							if ($changesInfo['CHANGES_TYPE'] != 'N' && $changesInfo['CHANGES_TYPE'] != 'E') {
								$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Changes could not be processed to request repair due to suggest type status are prohibited';
							} else {
								$changeModulePrivilegeObj  	= new Changemanagement_Model_Privilege();
								$isAuthorizeRequestRepair 	= $changeModulePrivilegeObj->isAuthorizeRequestRepair($changesInfo['MODULE']);
								if (!$isAuthorizeRequestRepair) {
									$arrayChangesError[$changesInfo['CHANGES_ID']] = 'You have no privilege for request repair specified changes';
								}
							}
						}
					} else {
						$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Changes could not be processed due to current status are prohibited';
					};
				}
			}
			$this->view->arrayChangesError  = $arrayChangesError;
		}
	}


	public function notificationAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace 	= new Zend_Session_Namespace('notificationRequestChangeList');
		$dataChangesId 		= $sessionNamespace->content;
		$action 			= $sessionNamespace->action;
		$note 				= $sessionNamespace->note;
		$tokenerr 			= $sessionNamespace->tokenerr;
		$description 		= $sessionNamespace->description;
		$this->view->action				= $action;
		$this->view->note 				= $action;
		$this->view->tokenerr 			= $tokenerr;

		$select 						= array();
		$displayDateFormat  			= $this->_dateDisplayFormat;
		$databaseSaveFormat 			= $this->_dateDBFormat;
		$this->view->dateJqueryFormat  	= $this->_dateJqueryFormat;

		$changesTypeCodeArr   = $this->_suggestType['code'];
		$changesStatusCodeArr = array_flip($this->_suggestionStatusCP);
		$changesStatusDescArr = $this->_suggestStatus['desc'];

		unset($this->_suggestType['code']['activate']);
		unset($this->_suggestType['desc']['activate']);
		unset($this->_suggestType['code']['deactivate']);
		unset($this->_suggestType['desc']['deactivate']);
		$this->view->suggestionType   =  $this->_suggestType;
		$this->view->suggestionStatus =  $this->_suggestionStatusCP;
		//companyCode
		// $listId = $this->_db->select()
		// 					->from(array('M_CUSTOMER'),
		// 						   array('CUST_ID','CUST_NAME'))
		// 					->order('CUST_ID ASC')
		// 					->query()->fetchAll();

		// $this->view->listCustId 	= array_merge(array(''=>'-- '.$this->language->_('Any Value'). ' --'),Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID'));
		// $this->view->listCustName 	= Application_Helper_Array::listArray($listId,'CUST_NAME','CUST_NAME');

		//$privi = Zend_Registry::get('privilege');	// get privi ID
		$privi 						= $this->_priviId;

		//get filtering param
		$filterArr = array(
			'filter'			=> array('StringTrim', 'StripTags'),
			'suggest_data' 	  	=> array('StripTags'),
			//                'cust_id' 	  		=> array('StringTrim','StripTags'),
			// 'cust_name' 	  	=> array('StripTags'),
			'data_id' 			=> array('StringTrim', 'StripTags'),
			'data_name' 		=> array('StripTags'),
			'suggestionType' 	=> array('StringTrim', 'StripTags'),
			'suggestionStatus' 	=> array('StringTrim', 'StripTags'),
			'suggestor' 		=> array('StringTrim', 'StripTags'),
			'changesCreatedFrom' => array('StringTrim', 'StripTags'),
			'changesCreatedTo'	=> array('StringTrim', 'StripTags'),
		);

		$zf_filter 	= new Zend_Filter_Input($filterArr, array(), $this->_request->getParams());
		$filter 	= $zf_filter->getEscaped('filter');

		$fields = array(
			'tableName'    => array(
				'field' => 'DISPLAY_TABLENAME',
				'label' => 'Suggest Data',
				'sortable' => false
			),
			// 'companyCode'  => array('field' => 'COMPANY_CODE',
			// 				    'label' => 'Company Code',
			// 				    'sortable' => false),
			// 'companyName'  => array('field' => 'COMPANY_NAME',
			// 				    'label' => 'Company Name',
			// 				    'sortable' => false),
			'dataId'  => array(
				'field' => 'KEY_FIELD',
				'label' => 'Data ID',
				'sortable' => false
			),
			'dataName'  => array(
				'field' => 'KEY_VALUE',
				'label' => 'Data Name',
				'sortable' => false
			),
			'suggestionDate' => array(
				'field' => 'G.CREATED',
				'label' => 'Suggested Date',
				'sortable' => false
			),
			'suggestionBy' => array(
				'field' => 'CREATED_BY',
				'label' => 'Suggested By',
				'sortable' => false
			),
			'suggestionType'  => array(
				'field' => 'CHANGES_TYPE',
				'label' => 'Suggestion Type',
				'sortable' => false
			),
			'suggestionInfo' => array(
				'field' => 'suggestion_status',
				'label' => 'Suggestion Status',
				'sortable' => false
			),
		);

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'suggestionDate');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		//Zend_Debug::dump($sortBy);die;
		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage 	= $page;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  	= new Changemanagement_Model_Privilege();
			$listAutModuleArr 			= $changeModulePrivilegeObj->getAuthorizeModule();
			$whPriviID 					= "'" . implode("','", $listAutModuleArr) . "'";

			$caseWhenRequestChangeList = " 	CASE WHEN (G.CHANGES_STATUS = 'RR') THEN 'Request Repaired'
											WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN 'Unread Suggestion'
											WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN 'Read Suggestion'
											WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN 'Repaired Suggestion'
											END
										";


			//$cust_id = $this->_custIdLogin;

			//compose query (use filtering and sorting)
			$select = $this->_db->select()
				->from(
					array('G' => 'T_GLOBAL_CHANGES'),
					array(
						'*',
						'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)
					)
				)
				// ->where('CHANGES_FLAG = '.$this->_db->quote('B'))
				->where("CHANGES_FLAG = ?", "F")
				//->where("CUST_ID = ?", $cust_id)
				->where("G.MODULE IN (" . $whPriviID . ")");
		}

		//where clauses
		$changesId = "'" . implode("','", $dataChangesId) . "'";
		$select->where("G.CHANGES_ID IN (" . $changesId . ")");

		//order clause

		$select->order(array($sortBy . ' ' . $sortDir));

		$dataSelectedChanges 	= $this->_db->fetchAll($select);
		$this->view->paginator 	= $dataSelectedChanges;
		$arrayChangesError 		= array();
		if (is_array($dataSelectedChanges)) {
			foreach ($dataSelectedChanges as  $changesInfo) {
			}
		}

		$this->view->arrayChangesError  = $description;
		$this->view->fields 			= $fields;
		$this->view->paginatorParam 	= $select;
	}
}
