<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class exchangerate_RateController extends Application_Main
{
	public function indexAction() 
	{ 
		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

		$dateNow = date("Y-m-d H:i:s");
		
		$this->view->dateNow = $dateNow;
				
		$param = array();
		$param['USER_ID'] = $userId;
		$param['CUST_ID'] = $custId;
		
		$model = new exchangerate_Model_Exchangerate();
		$userAcctInfo = $model->getCustomerAccount($param);
		
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];
				
		$svcInquiry = new Service_Inquiry($userId, $accountNo, $accountType, $accountCcy);
		$result		= $svcInquiry->ExchangeRateInquiry();
		
		if($result['ResponseCode']=='00' || $result['ResponseCode']=='0000'){
			$data = array();
//			Zend_Debug::dump($result['DataList']);
			foreach ($result['DataList'] as $key => $val){
				$data[$key]['currency'] 			= $val ['currency'];
				$data[$key]['buyForeignExchange'] 	= $val ['buyForeignExchange'];
				$data[$key]['sellForeignExchange'] 	= $val ['sellForeignExchange'];
				$data[$key]['buyNotes'] 			= $val ['buyNotes'];
				$data[$key]['sellNotes'] 			= $val ['sellNotes'];	
			}
		}
	    
	    $this->view->resultdata = $data;
	    $this->view->resultdataUpdate = $dateNow;
	    
	   
     //insert log
     Application_Helper_General::writeLog('','Exchange Rate');
	} 
}