<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/Payment.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';

class topuppayment_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	protected $_destinationUploadDir = '';
	protected $_maxRow = '';

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');

		$conf = Zend_Registry::get('config');
		$this->_bankName = $conf['app']['bankname'];
	}

	

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$data = $sessionNamespace->content;
		//Zend_Debug::dump($data);die;
			
		if($this->_custSameUser){
			$this->view->token = true;

			$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
			// echo $selectQuery;
			$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

			// var_dump($usergoogleAuth);die; 
			if (!empty($usergoogleAuth)) {
				$this->view->googleauth = true;
				//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
				$settingObj = new Settings();
				$maxtoken = $settingObj->getSetting("max_failed_token");
				$this->view->tokenfail = (int)$maxtoken-1;
				if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
					//die;  
					$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
					
					
					
					$tokenfail = (int)$maxtoken - ((int)$usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
					$this->view->tokenfail = $tokenfail;
				}
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}
			else{
				$this->view->nogoauth = '1';
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}

			

			$this->view->googleauth = true;
		}
		
		
		$sourceAcct = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']];

		$this->view->CCY = $sourceAcct['CCY_ID'];
		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->PS_FILEID = $data['paramPayment']['PSFILEID'];
		$this->view->PS_CCY = $data['paramTrxArr'][0]['ACBENEF_CCY'];
		//$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'].' / '.$sourceAcct['ACCT_NAME'].' / '.$sourceAcct['CCY_ID'];
		//$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'];
		// if($sourceAcct['ACCT_ALIAS']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS'];

		$acct_no = $data['paramTrxArr'][0]['ACCTSRC'];

		$selectaccount = $this->_db->select()
								->from(	array(	'T'				=>'T_CUST_DEBIT'),array('*'))
								->where('T.VA_NUMBER =? ',$acct_no);
								//echo $selectaccount;die;
		$pslipaccount = $this->_db->fetchRow($selectaccount);
		
		if ($pslipaccount['VA_NAME'] != NULL || !empty($pslipaccount['VA_NAME'])) {
			$this->view->ACCTSRC = $pslipaccount['VA_NUMBER'].' [IDR] / '.$this->_bankName.' / '.$pslipaccount['VA_NAME'];
		}else{
			$this->view->ACCTSRC = $pslipaccount['VA_NUMBER'].' [IDR] / '.$this->_bankName.' / '.$this->_custNameLogin;
		}

		
		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];

		$countData['TRANSACTION'] = array();
		foreach($data['paramTrxArr'] as $row)
		{
			$countData['TRANSACTION'][] = array(
				'SOURCE_ACCOUNT' 					=> $row['ACCTSRC'],
				'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
				'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
				'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
				'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
				'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
				'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
				'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
				'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
				'CLR_CODE' 							=> $row['BANK_CODE'],
				'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
				'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
				'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
				'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
				'TRA_REFNO' 						=> $row['TRA_REFNO'],
				'SMS_NOTIF' 						=> $row['SMS_NOTIF'],
				'EMAIL_NOTIF' 						=> $row['EMAIL_NOTIF'],
			);
		}

		$transactionCount 	= count($countData['TRANSACTION']);

		$this->view->countData = $transactionCount;

		$totalAmountData = 0;

		//Zend_Debug::dump($countData['TRANSACTION//']);die;
		if(is_array($countData['TRANSACTION']))
	 	{
	     	foreach ($countData['TRANSACTION'] as $key => $paramTransaction)
	     	{
	     		$totalAmountData = $totalAmountData + $paramTransaction['TRA_AMOUNT'];
	     		
	     	}
	    }

	    $this->view->totalAmountData = $totalAmountData;

	    $uploadDate = date("Y-m-d H:i:s");
	    $createDate = date("Y-m-d H:i:s");
	    $this->view->uploadDate = $uploadDate;

		$chargesAmt = array();
		$totalChargesAmt = 0;

		foreach($data['paramTrxArr'] as $row)
		{
			if(!isset($chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']])) $chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] = 0;
			//$chargesObj = Charges::factory($this->_custIdLogin, $row['TRANSFER_TYPE']);

//			if(empty($row['TRANSFER_TYPE'])){
//				$transferType = '0';
//			}
//			else{
//				$transferType = $row['TRANSFER_TYPE'];
//			}
			$chargesObj = Charges::factory($this->_custIdLogin, isset($row['TRANSFER_TYPE']));

			$paramCharges = array("accsrc" => $row['ACCTSRC'], "transferType" => $row['TRANSFER_TYPE']);
			if(!isset($data['errorTrxMsg'][$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']]))
			{
// 				$chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] += $chargesObj->getCharges($paramCharges);
// 				$totalChargesAmt += $chargesObj->getCharges($paramCharges);
			}
		}
		$this->view->chargesAmt = $chargesAmt;
		$this->view->totalChargesAmt = $totalChargesAmt;

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		//Zend_Debug::dump($data["payment"]["countTrxCCY"]);die;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$settings = new Settings();
		$this->view->cutOffSKN = $settings->getSetting('cut_off_time_skn');
		$this->view->cutOffRTGS = $settings->getSetting('cut_off_time_rtgs');

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/newbatchpayment');
			}

			if ($data["payment"]["countTrxPB"] == 0)
				$priviCreate = 'CBPI';
			else
				$priviCreate = 'CBPW';
			$param['BS_ID'] = $data['paramPayment']['BS_ID'];
			$param['PS_FILEID'] = $data['paramPayment']['PSFILEID'];
			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= 25;
			//$param['PS_CCY']  = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];
			$param['PS_FILE'] = $data['paramPayment']['PS_FILE'];
			$param['PS_CCY']  = $data['paramTrxArr'][0]['ACBENEF_CCY'];
			$param['UPLOAD_DATE'] = $uploadDate;
			$param['CREATE_DATE'] = $createDate;

			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
					'SOURCE_ACCOUNT' 					=> $row['ACCTSRC'],
					'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
					'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
					'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
					'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
					'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
					'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
 					'BENEFICIARY_ADDRESS2' 				=> $row['ACBENEF_ADDRESS2'],
					'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
					'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],

					"BENEFICIARY_CATEGORY" 				=> $row['BENEFICIARY_CATEGORY'],
					"BENEFICIARY_ID_TYPE" 				=> $row['BENEFICIARY_ID_TYPE'],
					"BENEFICIARY_ID_NUMBER" 			=> $row['BENEFICIARY_ID_NUMBER'],
					//"BENEFICIARY_CATEGORY" 				=> $row['BENEFICIARY_CITY'],
					//"BENEFICIARY_ID_TYPE" 				=> $row['BENEFICIARY_ID_TYPE'],
					//"BENEFICIARY_ID_NUMBER" 			=> $row['BENEFICIARY_ID_NUMBER'],
														
					'CLR_CODE' 							=> $row['BANK_CODE'],
					'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
					'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
					'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
					'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
					'TRA_REFNO' 						=> $row['TRA_REFNO'],
					'SMS_NOTIF' 						=> $row['SMS_NOTIF'],
					'EMAIL_NOTIF' 						=> $row['EMAIL_NOTIF'],
				);
			}

			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = $priviCreate;

			$sourceAcct = $data['paramTrxArr'][0]['ACCTSRC'];

			$select1	= $this->_db->select()
				->from(array('A' => 'M_CUSTOMER_ACCT'),
					array(
				   		'*'
					)
				)
				->where("A.ACCT_NO	= ?", $sourceAcct);
				

			$datacust1 		= $this->_db->fetchAll($select1);

			$ACCTNO = $datacust1['0']['ACCT_NO'];
			$CCYID	= $datacust1['0']['CCY_ID'];
			$ACCTNAME = $datacust1['0']['ACCT_NAME'];
			$ACCtTYPE = $datacust1['0']['ACCT_TYPE'];
			$ACCTALIAS = $datacust1['0']['ACCT_ALIAS_NAME'];

			$param['ACCTNO'] = $ACCTNO;
			$param['CCYID'] = $CCYID;
			$param['ACCTNAME'] = $ACCTNAME;
			$param['ACCtTYPE'] = $ACCtTYPE;
			$param['ACCTALIAS'] = $ACCTALIAS;

			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
			$paymentRef = NULL;
			
			
			
			if($this->_custSameUser){
										if(!$this->view->hasPrivilege('PRLP')){
											// die('here');
											
											$errMessage = $this->language->_("Error: You don't have privilege to release payment");
											$this->view->error = true;
											$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
										}else{
											
											///google auth
											$challengeCode		= $this->_getParam('challengeCode');

											$inputtoken1 		= $this->_getParam('inputtoken1');
											$inputtoken2 		= $this->_getParam('inputtoken2');
											$inputtoken3 		= $this->_getParam('inputtoken3');
											$inputtoken4 		= $this->_getParam('inputtoken4');
											$inputtoken5 		= $this->_getParam('inputtoken5');
											$inputtoken6 		= $this->_getParam('inputtoken6');

											$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;


											$select3 = $this->_db->select()
												 ->from(array('C' => 'M_USER'));
											$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
											// $select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
											$data2 = $this->_db->fetchRow($select3);

											// $code = $param['googleauth'];
		

											$pga = new PHPGangsta_GoogleAuthenticator();
									    	 //var_dump($data2['GOOGLE_CODE']);
									    	 //var_dump($code);
									    	 //print_r($responseCode);die();
											$setting 		= new Settings();
											$google_duration 	= $setting->getSetting('google_duration');
									        if($pga->verifyCode($data2['GOOGLE_CODE'], $responseCode, $google_duration))
									        {
									        	$datatoken = array(
															'USER_FAILEDTOKEN' => 0
														);

														$wheretoken =  array();
														$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
														$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
														$data = $this->_db->update('M_USER',$datatoken,$wheretoken);
									        	$resultToken = $resHard['ResponseCode'] == '0000';
									        	$tokenAuth = true;
									        }else{
									        	$tokenFailed = $CustomerUser->setLogToken();
									        	$tokenAuth = false;	
									        	$this->view->popauth = true;
									        	if ($tokenFailed === true) {
										 		$this->_redirect('/default/index/logout');
										 	}
									        }
										}
										//var_dump($tokenAuth);
					if($tokenAuth){
						$param['sameuser'] = 1;
						$result = $BulkPayment->createPaymentBatchTopup($param,$paymentRef);
					}else{
						$this->view->error = true;
						// $docErr = $this->displayError($zf_filter->getMessages());
						// print_r($docErr);die;
						$this->view->tokenError = true;
						$docErr = 'Invalid Token';
						$this->view->report_msg = $docErr;
					}					
										
				}else{
					//echo '<pre>';
					//var_dump($param);die; 
					$result = $BulkPayment->createPaymentBatchTopup($param,$paymentRef);
				}
			// echo "<pre>";
			// var_dump($param);
			// var_dump($result);
			// die();
		
			//var_dump($result);die();	

			if($result)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/notification/success');
			}
			else
			{
				$this->view->error = true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/payrollpayment/bulk');
			}
		}

	}

	

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}
}
