<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class debitbalance_DeactivateController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
   {   
       $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;
        $AESMYSQL = new Crypt_AESMYSQL();
        //$cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password));
        //$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
        
        $error_remark = null;
	
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 

		$regid = $this->_getParam('regid');
		//die($regid);
		$debitno = $this->_getParam('debitno');

		$regid = $AESMYSQL->decrypt($regid, $password);
		$debitno = $AESMYSQL->decrypt($debitno, $password);
		
       // jika customer id valid
       if($regid)
       {   
           //$id  = $zf_filter_input->getEscaped('regid'); 
					$this->view->regid = $regid;
					$select = $this->_db->select()
					        ->from(array('A' => 'T_DEBITCARD'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							->where('A.DEBIT_NUMBER = ?',$debitno)
							->where('A.REG_NUMBER = ?',$regid);
							
					$data = $this->_db->fetchRow($select);	
          
           if(!empty($data))
           {   
      	      //  $tempCustomerId = $this->getTempCustomerId($cust_id);
		      //  if(!$tempCustomerId)
		     //   {
                   $info = 'Customer ID = '.$data['CUST_ID'].', Customer Name = '.$data['CUST_NAME'].', Register Number = '.$data['REG_NUMBER'].', Debit Number = '.$data['DEBIT_NUMBER'];
                   //$cust_data['CUST_STATUS'] = $this->_masteruserStatus['code']['inactive'];
                   $key_value = $data['CUST_NAME'];
                   try 
                   { 
			          $this->_db->beginTransaction();
			          
			          $data['DEBIT_STATUS'] = 4;
			          $data['DEBIT_SUGGESTED']    = new Zend_Db_Expr('now()');
		              $data['DEBIT_SUGGESTEDBY']  = $this->_userIdLogin;
			          unset($data['CUST_NAME']);			 
			          // insert ke T_GLOBAL_CHANGES
					  $change_id = $this->suggestionWaitingApproval('Debit Card',$info,$this->_changeType['code']['suspend'],null,'T_DEBITCARD','TEMP_DEBITCARD',$data['CUST_ID'],$key_value,$data['CUST_ID']);
					  //zend_debug::dump($data); die;
			          $data['CHANGES_ID'] = $change_id;
					  $this->_db->insert('TEMP_DEBITCARD', $data);
			          
			          //log CRUD
					  Application_Helper_General::writeLog('CCSP','Corporate Debit Card has been suspended, Cust ID : '.$data['CUST_ID']. ' Cust Name : '.$data['CUST_NAME'].' Change id : '.$change_id);

			          $this->_db->commit();
					  
			          $rand = $this->token;
					  $encrypted_trxid = $AESMYSQL->encrypt($debitno, $rand);
         			  $enctrxid = urlencode($encrypted_trxid); 
			          $urlBack = '/debitbalance/detail/index/debitno/'.$enctrxid;
               		  $this->setbackURL($urlBack);

			          $this->_redirect('/notification/submited/index');
			          //$this->_redirect('/notification/success/index');
		           }
		           catch(Exception $e) 
		           { 
			         $this->_db->rollBack();
			         $error_remark = $this->getErrorRemark('82');
			         Application_Log_GeneralLog::technicalLog($e);
			       }
               // }
              //  else $error_remark = 'Corporate Card is already suggested'; 
            
          }
          else  $regid = null; 
       }// END IF CUST_ID
    
       
  }

}
