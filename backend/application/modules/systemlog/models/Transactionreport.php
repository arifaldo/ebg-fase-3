<?php
class systemlog_Model_Transactionreport
{
	protected $_db;
	
    // constructor
	public function __construct()
	{	
		$config = Zend_Registry::get('config');
		$this->_masterglobalstatus 	= $config['master']['globalstatus'];
		$this->_beneftype 			= $config['account']['beneftype'];
		$this->_paymentStatus 		= $config["payment"]["status"]; 
		$this->_paymentType 		= $config["payment"]["type"]; 
		$this->_transferType 		= $config["transfer"]["type"]; 
		$this->_transferStatus 		= $config["transfer"]["status"];
		$this->_historystatus		= $config["history"]["status"];  
		$this->_db 					= Zend_Db_Table::getDefaultAdapter();
	}
	
	public function getProvider($provId)
    {
		$select2 = $this->_db->select()
						->FROM(array('A' => 'M_SERVICE_PROVIDER'),
						array('A.PROVIDER_ID','A.PROVIDER_NAME','A.SERVICE_TYPE'))
						->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array('B.SERVICE_NAME'));
//						->WHERE('A.PROVIDER_ID = ? ', $provId);
		$select2->where('A.PROVIDER_ID = ? ', $provId);			
		
//		$select2->order($sortBy.' '.$sortDir);       
		
       return $this->_db->fetchAll($select2);
    }
    
	public function getName($provId,$field)
    {
		$select2 = $this->_db->select()
						->FROM(array('A' => 'M_SERVICE_PROVIDER'),
						array('A.PROVIDER_ID','A.PROVIDER_NAME','A.SERVICE_TYPE'))
						->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array('B.SERVICE_NAME'));
		if($field == 'PROVIDER_NAME'){
			if(isSet($provId))
			{ 
				$select2->where("UPPER(A.PROVIDER_NAME) LIKE ".$this->_db->quote('%'.$provId.'%')); 
			}
    	}
    	elseif($field == 'SERVICE_ID'){
			if(isSet($provId))
			{ 
				$select2->where('B.SERVICE_ID = ? ', $provId);
			}
    	}
    	
		
       return $data = $this->_db->fetchAll($select2);
    }
    
    function getCCYId()
    {
    	$select = $this->_db->select()
    	->from(	array(	'M'			=>'M_MINAMT_CCY'),
    			array(	'ccyid'		=>'CCY_ID') )
    			->query()->fetchAll();
    
    	foreach($select as $key => $val)
    	{
    		foreach($val as $key1 => $val1)
    		{
    			$result[$val1] = $val1;
    		}
    	}
    	return $result;
    }
    
    public function getServiceType()
    {
    	$data = $this->_db->select()->distinct()
    	->from(array('M_SERVICES_TYPE'),array('SERVICE_ID','SERVICE_NAME'))
    	->order('SERVICE_NAME ASC')
    	-> query() ->fetchAll();
    
    	return $data;
    }
	
 	public function getPaymentRecon($fParam,$sorting)
    {
		$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
// 		var_dump($this->_paymentType);
		
// 		var_dump($paytypesarr);
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
		
		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($paytypesarr as $key=>$val)
  		{
//    			$casePayType .= " WHEN ".$key." THEN '".$val."'";
  		}
//   			$casePayType .= " WHEN '3'  THEN P.PS_CATEGORY";
  			$casePayType .= "  WHEN 16 THEN 'Purchase'";
  			$casePayType .= " WHEN 17 THEN 'Payment'";
  			$casePayType .= "  END)";
  			
  		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE 'N/A' END)";
		
		$select	= $this->_db->select()
						->distinct()
						->from(	array('P' => 'T_PSLIP'),array())
						->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array())
//						->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array('*'))
						->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'U.USER_ID',
																									'U.USER_FULLNAME',
																									'P.PS_NUMBER',	
																									'T.TRX_ID',
																									'P.PS_CREATED',
																									'P.PS_UPDATED',
																									'P.PS_EFDATE',
																									'PS_TYPE'=> $casePayType,
																									'P.UUID',
																									'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," (",T.SOURCE_ACCOUNT_CCY,") ",T.SOURCE_ACCOUNT_NAME)'),
																									'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," (",T.BENEFICIARY_ACCOUNT_CCY,") ",T.BENEFICIARY_ACCOUNT_NAME)'),									
																									'PS_STATUS'	=> $casePayStatus,
																									
																									'T.TRA_STATUS','T.TRACE_NO','T.BANK_RESPONSE','T.TRANSFER_FEE','T.TRA_AMOUNT'))
						
						//->where("T.TRANSFER_TYPE <= 2");
						->where("T.TRANSFER_TYPE <= 8");
						
    	if(isSet($fParam['userid']))
    	{ 
    		$select->where("UPPER(U.USER_ID) = ".$this->_db->quote($fParam['userid'])); 
    	}
    	if(isSet($fParam['providerName']))
		{ 
			$PS_BILLER_ID_GET = $this->getName($fParam['providerName'],'PROVIDER_NAME');
			$datArr = array();
			foreach ($PS_BILLER_ID_GET as $value) {
				array_push($datArr, $value['PROVIDER_ID']);
			}
			if(count($PS_BILLER_ID_GET) < 1){
				$select->where("P.PS_BILLER_ID in (?) ", $fParam['providerName']);	
			}
			else{
				$select->where("P.PS_BILLER_ID in (?) ", $datArr);	
			}
		}
    	
    	if(isSet($fParam['serviceCategory']))
		{ 
			$PS_BILLER_ID_GET = $this->getName($fParam['serviceCategory'],'SERVICE_ID');
			$datArr = array();
			foreach ($PS_BILLER_ID_GET as $value) {
				array_push($datArr, $value['PROVIDER_ID']);
			}
			if(count($PS_BILLER_ID_GET) < 1){
				$select->where("P.PS_BILLER_ID in (?) ", $fParam['serviceCategory']);	
			}
			else{
				$select->where("P.PS_BILLER_ID in (?) ", $datArr);
			}
		}
// 		var_dump($fParam);die;
		if(isSet($fParam['username']))
		{ 
			$select->where("UPPER(U.USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['username'].'%')); 
		}
		
		if(isSet($fParam['paymentreff']))
		{ 
			$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%')); 
		}
		
    	if(isSet($fParam['referenceno']))
		{ 
			$select->where("UPPER(T.TRX_ID) LIKE ".$this->_db->quote('%'.$fParam['referenceno'].'%')); 
		}
		
		if(isSet($fParam['transferfrom']))
		{
			$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
		}
		
		if(isSet($fParam['transferto']))
		{
			$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
		}
		
		/*if(isSet($fParam['createdfrom']))
		{
			$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
		}
		
		if(isSet($fParam['createdto']))
		{
			$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
		}
		
		if(isSet($fParam['updatedfrom']))
		{
			$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
		}
		
		if(isSet($fParam['updatedto']))
		{
			$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
		}
		
		if(isSet($fParam['acctsource']))
		{ 
			$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
		
		if(isSet($fParam['beneaccount']))	
		{ 
			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }*/
		
		if(isSet($fParam['paymentstatus']))	
		{ 
			$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']); 
		}
		$select->where("P.PS_STATUS = 5 ");
		
		if(isSet($fParam['paymenttype']))
		{ 
			$fPayType 	 	= explode(",", $fParam['paymenttype']);
			$select->where("P.PS_TYPE in (?) ", $fPayType);		
		}
		
		$select->where("P.PS_TYPE in (16,17)");
		$select->where("T.TRA_STATUS = 3 ");
		
		if(isSet($fParam['transfertype']))
		{
			$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
		}
		
		$select->order($sorting);
// 		echo "<pre>";
// 		var_dump($select);die;
		return $this->_db->fetchAll($select);
    }
    
    
    
    public function getPaymentSummary($fParam,$sorting)
    {
    	$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
    	// 		var_dump($this->_paymentType);
    
    	// 		var_dump($paytypesarr);
    	$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
    
    	$casePayType = "(CASE P.PS_TYPE ";
    	foreach($paytypesarr as $key=>$val)
    	{
    		//    			$casePayType .= " WHEN ".$key." THEN '".$val."'";
    	}
    	//   			$casePayType .= " WHEN '3'  THEN P.PS_CATEGORY";
    	$casePayType .= "  WHEN 16 THEN 'Purchase'";
    	$casePayType .= " WHEN 17 THEN 'Payment'";
    	$casePayType .= "  END)";
    		
    	$casePayStatus = "(CASE P.PS_STATUS ";
    	foreach($paystatusarr as $key=>$val)
    	{
    		$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
    	}
    	$casePayStatus .= " ELSE 'N/A' END)";
    
    	$select	= $this->_db->select()
    	->distinct()
    	->from(	array('P' => 'T_PSLIP'),array())
    	->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array())
    							->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array())
    	->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(
    			'TOTAL' =>'COUNT(P.PS_NUMBER)',
    			'P.PS_EFDATE',
    			'P.PS_BILLER_ID',
    			'B.PROVIDER_NAME',
    			'SUM_AMOUNT' =>'SUM(T.TRA_AMOUNT)','T.TRA_AMOUNT','T.TRANSFER_TYPE'))
    
    			//->where("T.TRANSFER_TYPE <= 2");
    	->where("T.TRANSFER_TYPE <= 8")
    	->group('P.PS_BILLER_ID');
    
    	if(isSet($fParam['userid']))
    	{
    		$select->where("UPPER(U.USER_ID) = ".$this->_db->quote($fParam['userid']));
    	}
    	if(isSet($fParam['providerName']))
    	{
    		$PS_BILLER_ID_GET = $this->getName($fParam['providerName'],'PROVIDER_NAME');
    		$datArr = array();
    		foreach ($PS_BILLER_ID_GET as $value) {
    			array_push($datArr, $value['PROVIDER_ID']);
    		}
    		if(count($PS_BILLER_ID_GET) < 1){
    			$select->where("P.PS_BILLER_ID in (?) ", $fParam['providerName']);
    		}
    		else{
    			$select->where("P.PS_BILLER_ID in (?) ", $datArr);
    		}
    	}
    	 
    	if(isSet($fParam['serviceCategory']))
    	{
    		$PS_BILLER_ID_GET = $this->getName($fParam['serviceCategory'],'SERVICE_ID');
    		$datArr = array();
    		foreach ($PS_BILLER_ID_GET as $value) {
    			array_push($datArr, $value['PROVIDER_ID']);
    		}
    		if(count($PS_BILLER_ID_GET) < 1){
    			$select->where("P.PS_BILLER_ID in (?) ", $fParam['serviceCategory']);
    		}
    		else{
    			$select->where("P.PS_BILLER_ID in (?) ", $datArr);
    		}
    	}
    	// 		var_dump($fParam);die;
    	if(isSet($fParam['username']))
    	{
    		$select->where("UPPER(U.USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['username'].'%'));
    	}
    
    	if(isSet($fParam['paymentreff']))
    	{
    		$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%'));
    	}
    
    	if(isSet($fParam['referenceno']))
    	{
    		$select->where("UPPER(T.TRX_ID) LIKE ".$this->_db->quote('%'.$fParam['referenceno'].'%'));
    	}
    
    	if(isSet($fParam['transferfrom']))
    	{
    		$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
    	}
    
    	if(isSet($fParam['transferto']))
    	{
    		$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
    	}
    
    	/*if(isSet($fParam['createdfrom']))
    		{
    	$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
    	}
    
    	if(isSet($fParam['createdto']))
    	{
    	$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
    	}
    
    	if(isSet($fParam['updatedfrom']))
    	{
    	$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
    	}
    
    	if(isSet($fParam['updatedto']))
    	{
    	$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
    	}
    
    	if(isSet($fParam['acctsource']))
    	{
    	$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
    
    	if(isSet($fParam['beneaccount']))
    	{
    	$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }*/
    
    	if(isSet($fParam['paymentstatus']))
    	{
    		$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']);
    	}
    	$select->where("P.PS_STATUS = 5 ");
    
    	if(isSet($fParam['paymenttype']))
    	{
    		$fPayType 	 	= explode(",", $fParam['paymenttype']);
    		$select->where("P.PS_TYPE in (?) ", $fPayType);
    	}
    
    	$select->where("P.PS_TYPE in (16,17)");
    	$select->where("T.TRA_STATUS = 3 ");
    
    	if(isSet($fParam['transfertype']))
    	{
    		$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
    	}
    
    	$select->order($sorting);
    	
    	// 		echo "<pre>";
    	// 		var_dump($select);die;
    	return $this->_db->fetchAll($select);
    }
    
    
    
public function getPayment($fParam,$sorting)
    {
		$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
		
		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($paytypesarr as $key=>$val)
  		{
   			$casePayType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayType .= " WHEN '11' THEN P.PS_CATEGORY";
  			$casePayType .= " WHEN '12' THEN P.PS_CATEGORY";
  			$casePayType .= " ELSE 'N/A' END)";
  			
  		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE 'N/A' END)";
		
		$select	= $this->_db->select()
						->distinct()
						->from(	array('P' => 'T_PSLIP'),array())
						->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array())
//						->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array('*'))
						->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'U.USER_ID',
																									'U.USER_FULLNAME',
																									'P.PS_NUMBER',																							  	
																									'P.PS_CREATED',
																									'P.PS_UPDATED',
																									'P.PS_EFDATE',
																									'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," (",T.SOURCE_ACCOUNT_CCY,") ",T.SOURCE_ACCOUNT_NAME)'),
																									'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," (",T.BENEFICIARY_ACCOUNT_CCY,") ",T.BENEFICIARY_ACCOUNT_NAME)'),									
																									'PS_STATUS'	=> $casePayStatus,
																									'PS_TYPE'=> $casePayType,
																									'T.TRA_STATUS','T.TRACE_NO','T.BANK_RESPONSE','T.TRA_AMOUNT','T.TRANSFER_FEE','T.BILLER_ORDER_ID','P.PS_BILLER_ID','T.TRX_ID'))
						
						//->where("T.TRANSFER_TYPE <= 2");
						->where("T.TRANSFER_TYPE <= 8");
						
    	if(isSet($fParam['userid']))
    	{ 
    		$select->where("UPPER(U.USER_ID) = ".$this->_db->quote($fParam['userid'])); 
    	}
    	if(isSet($fParam['providerName']))
		{ 
			$PS_BILLER_ID_GET = $this->getName($fParam['providerName'],'PROVIDER_NAME');
			$datArr = array();
			foreach ($PS_BILLER_ID_GET as $value) {
				array_push($datArr, $value['PROVIDER_ID']);
			}
			if(count($PS_BILLER_ID_GET) < 1){
				$select->where("P.PS_BILLER_ID in (?) ", $fParam['providerName']);	
			}
			else{
				$select->where("P.PS_BILLER_ID in (?) ", $datArr);	
			}
		}
    	if(isSet($fParam['traceNo']))
		{ 
			$select->where("UPPER(T.TRACE_NO) LIKE ".$this->_db->quote('%'.$fParam['traceNo'].'%')); 
		}
    	if(isSet($fParam['CCYID']))
		{ 
			$select->where("UPPER(P.PS_CCY) LIKE ".$this->_db->quote('%'.$fParam['CCYID'].'%')); 
		}
    	if(isSet($fParam['serviceCategory']))
		{ 
			$PS_BILLER_ID_GET = $this->getName($fParam['serviceCategory'],'SERVICE_ID');
			$datArr = array();
			foreach ($PS_BILLER_ID_GET as $value) {
				array_push($datArr, $value['PROVIDER_ID']);
			}
			if(count($PS_BILLER_ID_GET) < 1){
				$select->where("P.PS_BILLER_ID in (?) ", $fParam['serviceCategory']);	
			}
			else{
				$select->where("P.PS_BILLER_ID in (?) ", $datArr);
			}
		}
		
		if(isSet($fParam['username']))
		{ 
			$select->where("UPPER(U.USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['username'].'%')); 
		}
		
		if(isSet($fParam['paymentreff']))
		{ 
			$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%')); 
		}
		
		if(isSet($fParam['transferfrom']))
		{
			$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
		}
		
		if(isSet($fParam['transferto']))
		{
			$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
		}
		
		if(isSet($fParam['createdfrom']))
		{
			$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
		}
		
		if(isSet($fParam['createdto']))
		{
			$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
		}
		
		if(isSet($fParam['updatedfrom']))
		{
			$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
		}
		
		if(isSet($fParam['updatedto']))
		{
			$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
		}
		
		if(isSet($fParam['acctsource']))
		{ 
			$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
		
		if(isSet($fParam['beneaccount']))	
		{ 
			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }
		
		if(isSet($fParam['paymentstatus']))	
		{ 
			$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']); 
		}
		
		if(isSet($fParam['paymenttype']))
		{ 
			$fPayType 	 	= explode(",", $fParam['paymenttype']);
			$select->where("P.PS_TYPE in (?) ", $fPayType);		
		}
		
		if(isSet($fParam['transfertype']))
		{
			$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
		}
		
		$select->order($sorting);
		return $this->_db->fetchAll($select);
    }
    
    public function getPaymentDetail($psNumber)
    {
    	$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
		$transtypesarr = array_combine(array_values($this->_transferType['code']),array_values($this->_transferType['desc']));
		$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
		
    	$casePayType = "(CASE P.PS_TYPE ";
  		foreach($paytypesarr as $key=>$val)
  		{
   			$casePayType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayType .= " WHEN '11' OR '12' THEN PS_CATEGORY";
  			$casePayType .= " ELSE 'N/A' END)";
  			
  		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE 'N/A' END)";
  			
  		$caseTransType = "(CASE T.TRANSFER_TYPE ";
  		foreach($transtypesarr as $key=>$val)
  		{
   			$caseTransType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransType .= " ELSE 'N/A' END)";
  			
  		$caseTransType = "(CASE T.TRANSFER_TYPE ";
  		foreach($transtypesarr as $key=>$val)
  		{
   			$caseTransType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransType .= " ELSE 'N/A' END)";
  			
  		$caseTransStatus = "(CASE T.TRA_STATUS ";
  		foreach($transstatusarr as $key=>$val)
  		{
   			$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransStatus .= " ELSE 'N/A' END)";
  			
		$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP'),array())
						->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array())
						->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'P.*','U.*','T.*',
																									'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," (",T.SOURCE_ACCOUNT_CCY,") ",T.SOURCE_ACCOUNT_NAME)'),									
																									'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," (",T.BENEFICIARY_ACCOUNT_CCY,") ",T.BENEFICIARY_ACCOUNT_NAME)'),
																									'PS_STATUS'	=> $casePayStatus,
																									'CEK_PS_TYPE' => 'P.PS_TYPE',
																									'PS_TYPE' => $casePayType,
																									'TRANSFER_TYPE' => $caseTransType,
																									'TRANSFER_STATUS' => $caseTransStatus,
																									'T.TRA_STATUS'))
						->where("P.PS_NUMBER = ? ", $psNumber);
		return $this->_db->fetchAll($select);
    }
    
	public function getPslipDetail($psNumber)
    {
    	$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP_DETAIL'),array('*'))
						->where("P.PS_NUMBER = ? ", $psNumber);
		return $this->_db->fetchAll($select);
    }
    
	public function getHistory($psNumber)
    {
    	$historyarr = array_combine(array_values($this->_historystatus['code']),array_values($this->_historystatus['desc']));
    	
    	$caseHistory = "(CASE P.HISTORY_STATUS ";
  		foreach($historyarr as $key=>$val)
  		{
   			$caseHistory .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseHistory .= " ELSE 'N/A' END)";
    	
    	$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP_HISTORY'),array('*','HISTORY_STATUS' => $caseHistory))
						->where("P.PS_NUMBER = ? ", $psNumber);
    	$select -> order('DATE_TIME DESC');
		return $this->_db->fetchAll($select);
    }
}