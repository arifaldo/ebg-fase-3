<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';
class tools_PaymentapprovalhistoryController extends Application_Main {
	
	public function initController(){
	}
	
	public function indexAction() 
	{
		$paymentRef = $this->_getParam('paymentref');
		$Payment = new 	Payment($paymentRef);
		$listHistory =  $Payment->getApprovalHistory();
		$this->view->paymentApprovalHistory =  $listHistory;
		$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;
	}
}

