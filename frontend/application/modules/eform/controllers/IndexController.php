<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eform_IndexController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function detailAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $bg = $this->_getParam('bg');
    $bgNumb = $this->_getParam('bgnumb');

    $sessToken  = new Zend_Session_Namespace('Tokenenc');
    $password   = $sessToken->token;
    $this->view->token = $password;

    $AESMYSQL = new Crypt_AESMYSQL();
    $decryption = urldecode($bg);
    $decryption_bgNumb = urldecode($bgNumb);

    $decryption_bg = $AESMYSQL->decrypt($decryption, $password);
    $decryption_bgnumb = $AESMYSQL->decrypt($decryption_bgNumb, $password);

    $decryption_bg = $decryption_bgnumb ?: $decryption_bg;

    $conf = Zend_Registry::get('config');
    $Settings = new Settings();
    $claim_period = $Settings->getSetting('max_claim_period');
    $this->view->BG_CLAIM_PERIOD = $claim_period;

    $select = $this->_db->select()
      ->from(
        array('TBG' => 'T_BANK_GUARANTEE'),
        array('*')
      )
      ->joinLeft(
        array('MB' => 'M_BRANCH'),
        'MB.BRANCH_CODE = TBG.BG_BRANCH',
        array('BRANCH_NAME')
      )
      ->joinLeft(
        array('MC' => 'M_CUSTOMER'),
        'MC.CUST_ID = TBG.CUST_ID',
        array(
          'CUST_ID',
          'CUST_NAME',
          'CUST_NPWP',
          'CUST_ADDRESS',
          'CUST_CITY',
          'CUST_FAX',
          'CUST_CONTACT',
          'CUST_PHONE',
        )
      )
      ->joinLeft(
        array('MCST' => 'M_CUSTOMER'),
        'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
        array(
          "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
        )
      )
      ->joinLeft(
        array('INSURANCE' => 'M_CUSTOMER'),
        'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
        array(
          "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
        )
      )
      ->joinLeft(
        array('MCL' => 'M_CITYLIST'),
        'MCL.CITY_CODE = MC.CUST_CITY',
        array('CITY_NAME')
      )
      ->joinLeft(
        array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
        'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
        array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
      )
      ->joinLeft(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        'TBGS.BG_NUMBER = TBG.BG_REG_NUMBER',
        [
          "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
        ]
      );
    // ->where('TBG.BG_REG_NUMBER = ?', $decryption_bg);

    if (!$bgNumb) {
      $select->where('TBG.BG_REG_NUMBER = ?', $decryption_bg);
      $select->where('TBG.CUST_ID = ?', $this->_custIdLogin);
    } else {
      $select->where('TBG.BG_REG_NUMBER = ?', $decryption_bgnumb);
    }

    $data = $this->_db->fetchRow($select);

    if ($data['BG_OLD']) {

      $bgOld = $this->_db->select()
        ->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
        ->where('BG_NUMBER = ?', $data['BG_OLD'])
        ->query()->fetch();

      $this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
      $this->view->prevProv = $bgOld['PROVISION_FEE'];
    }

    $config        = Zend_Registry::get('config');
    $BgcomplationType     = $config["bgclosing"]["changetype"]["desc"];
    $BgcomplationCode     = $config["bgclosing"]["changetype"]["code"];

    $arrStatusSettlement = array_combine(array_values($BgcomplationCode), array_values($BgcomplationType));

    $changeTypeCode = $config["bg"]["changetype"]["code"];
    $changeTypeDesc = $config["bg"]["changetype"]["desc"];
    $arrChageType = array_combine($changeTypeCode, $changeTypeDesc);

    $this->view->arrStatusSettlement = $arrStatusSettlement;

    // GET PAPER USED
    $getPaperUsed = $this->_db->select()
      ->from('M_PAPER')
      ->where('NOTES = ?', $data['BG_NUMBER'])
      ->query()->fetchAll();

    $paperUsed = array_column($getPaperUsed, 'PAPER_ID');
    $this->view->paperUsed = $paperUsed;

    if ($data['BG_NUMBER_NEW']) {
      $bgNew = $this->_db->select()
        ->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
        ->where('BG_NUMBER = ?', $data['BG_NUMBER_NEW'])
        ->query()->fetch();

      $this->view->bgRegNumberNew = $bgNew['BG_REG_NUMBER'];
    }

    $checkProgressAmd = $this->_db->select()
      ->from('TEMP_BANK_GUARANTEE')
      ->where('BG_OLD = ?', $data['BG_NUMBER'])
      ->where('BG_STATUS NOT IN (?)', ['9', '12', '13', '25', '26'])
      ->query()->fetch();

    $this->view->onProgressAmendment = ($checkProgressAmd) ? true : false;

    $checkProgressClose = $this->_db->select()
      ->from('TEMP_BANK_GUARANTEE_CLOSE')
      ->where('BG_NUMBER = ?', $data['BG_NUMBER'])
      ->where('SUGGESTION_STATUS NOT IN (7,11)')
      ->orWhere('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
      ->query()->fetch();

    $this->view->onProgressClose = ($checkProgressClose) ? true : false;
    // fixing 28 april 2023
    $this->view->dataClose = $checkProgressClose;
    // fixing 28 april 2023

    // switch ($data["CHANGE_TYPE"]) {
    //   case '0':
    //     $this->view->suggestion_type = "New";
    //     break;
    //   case '1':
    //     $this->view->suggestion_type = "Amendment Changes";
    //     break;
    //   case '2':
    //     $this->view->suggestion_type = "Amendment Draft";
    //     break;
    // }

    $this->view->suggestion_type = $arrChageType[$data['CHANGE_TYPE']];

    // cek data bg close --------------------------
    $bgclose = $this->_db->select()->from(array('TBGC' => 'TEMP_BANK_GUARANTEE_CLOSE'), array('*'));
    (!$bgNumb) ? $bgclose->where('TBGC.BG_REG_NUMBER = ?', $decryption_bg) : $bgclose->where('TBGC.BG_REG_NUMBER = ?', $decryption_bgnumb);
    $dataBgClose = $this->_db->fetchRow($bgclose);
    $dataBgClose != false || !empty($dataBgClose) ? $this->view->check_bg_close = false : $this->view->check_bg_close = true;
    // cek data bg close --------------------------


    // cek data claim date --------------------------
    $bgclaimdate = $this->_db->select()->from(array('TBG' => 'T_BANK_GUARANTEE'), array('*'));
    (!$bgNumb) ? $bgclaimdate->where('TBG.BG_REG_NUMBER = ?', $decryption_bg) : $bgclaimdate->where('TBG.BG_REG_NUMBER = ?', $decryption_bgnumb);
    $bgclaimdate->where("DATE(NOW()) <= DATE(TBG.BG_CLAIM_DATE)");
    $dataBgClaimDate = $this->_db->fetchRow($bgclaimdate);
    if (!empty($dataBgClaimDate)) {
      $this->view->check_bg_claim_date = true;
    } else {
      $this->view->check_bg_claim_date = false;
    }

    // cek data claim date --------------------------

    // cek btn pelepasan md --------------------------
    $bgpelepasanmd = $this->_db->select()->from(array('TBG' => 'T_BANK_GUARANTEE'), array('*'));
    (!$bgNumb) ? $bgpelepasanmd->where('TBG.BG_REG_NUMBER = ?', $decryption_bg) : $bgpelepasanmd->where('TBG.BG_REG_NUMBER = ?', $decryption_bgnumb);
    $bgpelepasanmd->where("TBG.BG_STATUS = 16");
    $bgpelepasanmd->where("TBG.CLOSING_TYPE = 2");
    $bgpelepasanmd->where("TBG.BG_NUMBER NOT IN (SELECT BG_NUMBER FROM T_BG_PSLIP WHERE BG_NUMBER IS NOT NULL)");
    $databgpelepasanmd = $this->_db->fetchRow($bgpelepasanmd);

    if (!empty($databgpelepasanmd)) {
      $this->view->check_btn_md = true;
    } else {

      $this->view->check_btn_md = false;
    }

    // cek btn pelepasan md --------------------------


    //echo '<pre>';print_r($data);
    $conf = Zend_Registry::get('config');
    // BG TYPE
    $bgType         = $conf["bg"]["type"]["desc"];
    $bgCode         = $conf["bg"]["type"]["code"];

    $this->view->BG_REG_NUMBER = $data["BG_REG_NUMBER"];



    $checkCounterGuaranteeFile = $this->_db->select()
      ->from(
        array('A' => 'T_BANK_GUARANTEE_DETAIL'),
        array('*')
      )
      ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
      ->where('A.PS_FIELDNAME = ?', 'Counter Guarantee Document')
      ->query()->fetchAll();


    // var_dump($checkCounterGuaranteeFile);die;
    // $checkCounterGuaranteeFile = $this->_db->select()
    // ->from(["A" => "T_BANK_GUARANTEE_DETAIL"], ["PS_FIELDVALUE"])
    // ->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
    // ->where("PS_FIELDNAME = ?", "Counter Guarantee Document")
    // ->query()->fetchAll();

    $this->view->checkCounterGuaranteeFile = count($checkCounterGuaranteeFile);

    $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

    $this->view->arrbgType = $arrbgType;

    //BG Counter Guarantee Type
    $bgcgType         = $conf["bgcg"]["type"]["desc"];
    $bgcgCode         = $conf["bgcg"]["type"]["code"];

    $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

    $this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

    $download = $this->_getParam('certfinal');
    if ($download == 1) {
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';
      // $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
      return $this->_helper->download->file("BG_NO_" . $data["BG_NUMBER"] . ".pdf", $attahmentDestination . $data['DOCUMENT_ID'] . ".pdf");
    }


    if ($data['COUNTER_WARRANTY_TYPE'] == '1' || $data['COUNTER_WARRANTY_TYPE'] == '3') {
      $saveHoldAmount = null;

      $bgdatasplit = $this->_db->select()
        ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
        // ->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO", ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID'])
        ->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO AND B.CUST_ID = '" . $data['CUST_ID'] . "'", ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
        ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
        ->query()->fetchAll();

      $getEscrow = array_search("Escrow", array_column($bgdatasplit, "ACCT_DESC"));

      $this->view->cekEscrow = $getEscrow;

      if ($getEscrow !== false) {
        $serviceAccount = new Service_Account($bgdatasplit[$getEscrow]["ACCT"], null);

        $getResultService = $serviceAccount->inquiryAccontInfo();
        $cif = $getResultService["cif"];

        $serviceCif = new Service_Account(null, null, null, null, null, $cif);
        $saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
        $getCurrencyEscrow = array_search($bgdatasplit[$getEscrow]["ACCT"], array_column($saveResult, "account_number"));

        $escrowCurrency = 'IDR';
        if ($getCurrencyEscrow !== false) {
          $escrowCurrency = $saveResult[$getCurrencyEscrow]["currency"];
          $escrowType = $saveResult[$getCurrencyEscrow]["type_desc"];
        }

        $this->view->escrowCurrency = $escrowCurrency;
        $this->view->escrowType = $escrowType;
      }

      foreach ($bgdatasplit as $key => $value) {
        if ($value['CUST_ID'] != $data['CUST_ID'] && $value['FLAG'] == 1) continue;
        if (strtolower($value["ACCT_DESC"]) == 'escrow') continue;

        // if (strtolower($value["M_ACCT_DESC"]) != "giro" && $value["M_ACCT_TYPE"] != "D" && $value["M_ACCT_TYPE"] != "20" && $value["M_ACCT_TYPE"] != 20) continue;

        $saveHoldAmount += intval($value["AMOUNT"]);
      }

      $this->view->fullmember = $bgdatasplit;

      $this->view->holdAmount = $saveHoldAmount;
    }

    // Marginal Deposit ---------------
    $bgdatamdsplit = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
      ->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO AND B.CUST_ID = '" . $data['CUST_ID'] . "'", ["TYPE" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", "CURRENCY" => "B.CCY_ID", 'CUST_ID'])
      ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
      ->query()->fetchAll();

    if ($data['COUNTER_WARRANTY_TYPE'] == '3' && !empty($bgdatamdsplit)) {
      $bgdatadetailmd = $this->_db->select()
        ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
        ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
        ->query()->fetchAll();

      if (!empty($bgdatadetailmd)) {
        foreach ($bgdatadetailmd as $key => $value) {
          if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
            $this->view->marginalDepositPercentage = $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Currency') {
            $this->view->currency = $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Insurance Special Format Approval Document') {
            $this->view->specialFormatDocument = $value['PS_FIELDVALUE'];
          }
        }
        $mdpercent = intval($this->view->marginalDepositPercentage);
        $percent = $mdpercent / 100;
        $mdamount = intval($data['BG_AMOUNT']);
        $mdtotal = $mdamount * $percent;

        $this->view->mdtotal = $mdtotal;
      }

      $mdAmount = null;
      $getEscrow = array_search("Escrow", array_column($bgdatamdsplit, "ACCT_DESC"));

      if ($getEscrow !== false) {
        $serviceAccount = new Service_Account($bgdatamdsplit[$getEscrow]["ACCT"], null);
        $getResultService = $serviceAccount->inquiryAccontInfo();
        $cif = $getResultService["cif"];
        $serviceCif = new Service_Account(null, null, null, null, null, $cif);
        $saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
        $getCurrencyEscrow = array_search($bgdatamdsplit[$getEscrow]["ACCT"], array_column($saveResult, "account_number"));
        $escrowCurrency = 'IDR';

        if ($getCurrencyEscrow !== false) {
          $escrowCurrency = $saveResult[$getCurrencyEscrow]["currency"];
          $escrowType = $saveResult[$getCurrencyEscrow]["type_desc"];
        }

        $this->view->escrowCurrency = $escrowCurrency;
        $this->view->escrowType = $escrowType;
      }

      foreach ($bgdatamdsplit as $key => $valmd) {
        if ($valmd['CUST_ID'] != $data['CUST_ID'] && $valmd['FLAG'] == 1) continue;
        if (strtolower($valmd["ACCT_DESC"]) == 'escrow') continue;
        $mdAmount += intval($valmd['AMOUNT']);
      }

      $this->view->bgdatamdsplit = $bgdatamdsplit;
      $this->view->mdAmount = $mdAmount;
    }
    // Marginal Deposit ---------------


    $bgdatadetail = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
      // ->where('A.CUST_ID = ' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"])
      ->query()->fetchAll();

    if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
      $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
      $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

      $insuranceBranch = $this->_db->select()
        ->from("M_INS_BRANCH")
        ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
        ->query()->fetchAll();

      $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
    }

    if (!empty($bgdatadetail)) {
      foreach ($bgdatadetail as $key => $value) {

        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
          if ($value['PS_FIELDNAME'] == 'Insurance Name') {
            $this->view->insuranceName =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
            $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount') {
            $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
            $this->view->paDateStart =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
            $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
          }
        } else {

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
            $this->view->owner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
            $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
            $this->view->owner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
            $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
            $this->view->owner3 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
            $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
          }
        }
      }
    }

    $principleData = [];
    if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
      foreach ($bgdatadetail as $key => $value) {
        $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
      }

      $this->view->principleData = $principleData;
    }

    $get_linefacility = $this->_db->select()
      ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
      ->where("CUST_ID = ?", $data["CUST_ID"])
      ->query()->fetchAll();

    $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
    $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

    $this->view->linefacility = $get_linefacility[0];

    $this->view->acct = $data['FEE_CHARGE_TO'];

    $conf = Zend_Registry::get('config');
    $this->view->bankname = $conf['app']['bankname'];

    $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
    $AccArr = $CustomerUser->getAccountsBG($param);
    //var_dump($AccArr);die;

    if (!empty($AccArr)) {
      $this->view->src_name = $AccArr['0']['ACCT_NAME'];
    }

    $get_cash_collateral = $this->_db->select()
      ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
      ->where("CUST_ID = ?", "GLOBAL")
      ->where("CHARGES_TYPE = ?", "10")
      ->query()->fetchAll();

    $this->view->cash_collateral = $get_cash_collateral[0];

    $bgpublishType     = $conf["bgpublish"]["type"]["desc"];
    $bgpublishCode     = $conf["bgpublish"]["type"]["code"];

    $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

    $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

    $arrBankFormat = array(
      1 => 'Bank Standard',
      2 => 'Special Format (with bank approval)'
    );

    $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
    $this->view->bankFormatNumber = $data['BG_FORMAT'];

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Billingual',
    );

    $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

    $checkOthersAttachment = $this->_db->select()
      ->from(['A' => 'T_BANK_GUARANTEE_FILE'], ["*"])
      // ->where("BG_REG_NUMBER = '268312345601F4201'")
      ->where('BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"])
      ->order('A.INDEX ASC')
      ->query()->fetchAll();

    if (count($checkOthersAttachment) > 0) {
      $this->view->othersAttachment = $checkOthersAttachment;
    }

    // Get data T_BANK_GUARANTEE_HISTORY
    $select = $this->_db->select()
      ->from(
        array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
        array('*')
      )
      ->where('TBGH.BG_REG_NUMBER = ?', $decryption_bg);
    // ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
    $dataHistory = $this->_db->fetchAll($select);

    // Get data T_BANK_GUARANTEE_HISTORY_CLOSE
    $selectClose = $this->_db->select()
      ->from(
        array('TBGH' => 'T_BANK_GUARANTEE_HISTORY_CLOSE'),
        array('*')
      )
      ->where('TBGH.BG_NUMBER = ?', $data['BG_NUMBER']);
    // ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
    $dataHistoryClose = $this->_db->fetchAll($selectClose);

    $historyStatusCode = $config["bgclosinghistory"]["status"]["code"];
    $historyStatusDesc = $config["bgclosinghistory"]["status"]["desc"];
    $historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));
    $this->view->historyStatusArrClose   = $historyStatusArr;

    // Get data TEMP_BANK_GUARANTEE_SPLIT
    $select = $this->_db->select()
      ->from(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        array('*')
      )
      ->where('TBGS.BG_REG_NUMBER = ?', $decryption_bg);
    $dataAccSplit = $this->_db->fetchAll($select);

    $config = Zend_Registry::get('config');

    $docTypeCode = $config["bgdoc"]["type"]["code"];
    $docTypeDesc = $config["bgdoc"]["type"]["desc"];
    $docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

    $statusCode = $config["bg"]["status"]["code"];
    $statusDesc = $config["bg"]["status"]["desc"];
    $statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

    $historyStatusCode = $config["history"]["status"]["code"];
    $historyStatusDesc = $config["history"]["status"]["desc"];
    $historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

    $counterTypeCode = $config["bgcg"]["type"]["code"];
    $counterTypeDesc = $config["bgcg"]["type"]["desc"];
    $counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

    //$config       = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    // $arrStatus = array(
    //   '7'  => 'Canceled',
    //   '20' => 'On Risk',
    //   '21' => 'Off Risk',
    //   '22' => 'Claimed On Process',
    //   '23' => 'Claimed'
    // );
    // var_dump($data['BG_STATUS']);


    $this->view->arrStatus               = $arrStatus;
    $this->view->data               = $data;
    //Zend_Debug::dump($data);
    $this->view->dataHistory        = $dataHistory;
    $this->view->dataHistoryClose        = $dataHistoryClose;
    $this->view->dataAccSplit       = $dataAccSplit;
    $this->view->docTypeArr         = $docTypeArr;
    $this->view->statusArr          = $statusArr;
    $this->view->historyStatusArr   = $historyStatusArr;
    $this->view->counterTypeArr     = $counterTypeArr;

    // fixing 27 april 2023
    // data modal Persetujuan Asuransi -----------------------
    $getBgDataDetail = $this->_db->select()
      ->from("T_BANK_GUARANTEE_DETAIL")
      ->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
      ->query()->fetchAll();

    if (!empty($getBgDataDetail)) {
      foreach ($getBgDataDetail as $key => $value) {
        if ($value['PS_FIELDNAME'] == 'Principle Agreement Number') {
          $this->view->principleAgreementNumber =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Agreement Granted Date') {
          $this->view->principleAgreementDate =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Premium') {
          $this->view->principleInsurancePremium =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Administration') {
          $this->view->principleInsuranceAdm =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Stamp') {
          $this->view->principleInsuranceStamp =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Counter Guarantee Number') {
          $this->view->counterGuaranteeNumber =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Counter Guarantee Document') {
          $this->view->counterGuaranteeDocument =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Counter Guarantee Granted Date') {
          $this->view->counterGuaranteeGrantedDate =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
          $ins_branch_code =   $value['PS_FIELDVALUE'];
          $sqlinsurancebranch = $this->_db->select()
            ->from("M_INS_BRANCH", ["INS_BRANCH_ACCT"])
            ->where("INS_BRANCH_CODE = ?", $ins_branch_code);
          $get_norekinsurance = ($sqlinsurancebranch)
            ->query()->fetchAll();
          $this->view->norekCabang =   $get_norekinsurance[0]['INS_BRANCH_ACCT'];
        }
      }
    }

    $get_cg_doc_number = $this->_db->select()
      ->from("T_BANK_GUARANTEE_DETAIL")
      ->where("BG_REG_NUMBER = ?", $data['BG_REG_NUMBER'])
      ->where("PS_FIELDNAME = ?", "Counter Guarantee Number")
      ->query()->fetchAll();

    $this->view->get_cg_doc_number = $get_cg_doc_number[0]['PS_FIELDVALUE'];

    if (!$get_cg_doc_number[0]['PS_FIELDVALUE']) {
      $get_cg_doc_number = $this->_db->select()
        ->from("TEMP_BANK_GUARANTEE_DETAIL")
        ->where("BG_REG_NUMBER = ?", $data['BG_REG_NUMBER'])
        ->where("PS_FIELDNAME = ?", "Counter Guarantee Number")
        ->query()->fetchAll();

      $this->view->get_cg_doc_number = $get_cg_doc_number[0]['PS_FIELDVALUE'];
    }
    // data modal Persetujuan Asuransi -----------------------

    $download = $this->_getParam('download');
    if ($download == 1) {

      // var_dump($data['FILE']);die;
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';
      $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
    }

    Application_Helper_General::writeLog('VLBG', 'Lihat Detail BG No : ' . $data['BG_NUMBER']);
  }

  public function downloaddocAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $bg = $this->_getParam('bg');


    // var_dump($this->_custIdLogin);die;

    $select = $this->_db->select()
      ->from(
        array('TBG' => 'T_BANK_GUARANTEE'),
        array('*')
      )
      ->where('TBG.BG_REG_NUMBER = ?', $bg);
    // ->where('TBG.CUST_ID = ?', $this->_custIdLogin);
    $data = $this->_db->fetchRow($select);

    // var_dump($data['FILE']);die;
    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
    $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
  }

  public function downloadAction()
  {
    $this->_helper->_layout->setLayout('newlayout');
    // $this->_helper->layout()->disableLayout();
    // $this->_helper->viewRenderer->setNoRender(true);


    $bg = $this->_getParam('bg');

    $sessToken  = new Zend_Session_Namespace('Tokenenc');
    $password   = $sessToken->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $decryption = urldecode($bg);
    $decryption_bg = $AESMYSQL->decrypt($decryption, $password);

    $conf = Zend_Registry::get('config');
    $Settings = new Settings();
    $claim_period = $Settings->getSetting('max_claim_period');
    $this->view->BG_CLAIM_PERIOD = $claim_period;


    $select = $this->_db->select()
      ->from(
        array('TBG' => 'T_BANK_GUARANTEE'),
        array('*')
      )
      ->joinLeft(
        array('MB' => 'M_BRANCH'),
        'MB.BRANCH_CODE = TBG.BG_BRANCH',
        array('BRANCH_NAME')
      )
      ->joinLeft(
        array('MC' => 'M_CUSTOMER'),
        'MC.CUST_ID = TBG.CUST_ID',
        array(
          'CUST_ID',
          'CUST_NAME',
          'CUST_NPWP',
          'CUST_ADDRESS',
          'CUST_CITY',
          'CUST_FAX',
          'CUST_CONTACT',
          'CUST_PHONE',
        )
      )
      ->joinLeft(
        array('MCST' => 'M_CUSTOMER'),
        'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
        array(
          "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
        )
      )
      ->joinLeft(
        array('INSURANCE' => 'M_CUSTOMER'),
        'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
        array(
          "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
        )
      )
      ->joinLeft(
        array('MCL' => 'M_CITYLIST'),
        'MCL.CITY_CODE = MC.CUST_CITY',
        array('CITY_NAME')
      )
      ->joinLeft(
        array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
        'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
        array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
      )
      ->joinLeft(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        'TBGS.BG_NUMBER = TBG.BG_REG_NUMBER',
        [
          "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
        ]
      )
      ->where('TBG.BG_REG_NUMBER = ?', $decryption_bg)
      ->where('TBG.CUST_ID = ?', $this->_custIdLogin);
    $data = $this->_db->fetchRow($select);

    $data['pdf'] = true;

    $test = $this->_forward('printeformdetailbg', 'index', 'widget', $data);

    switch ($data["CHANGE_TYPE"]) {
      case '0':
        $this->view->suggestion_type = "New";
        break;
      case '1':
        $this->view->suggestion_type = "Amendment Changes";
        break;
      case '2':
        $this->view->suggestion_type = "Amendment Draft";
        break;
    }

    //echo '<pre>';print_r($data);
    $conf = Zend_Registry::get('config');
    // BG TYPE
    $bgType         = $conf["bg"]["type"]["desc"];
    $bgCode         = $conf["bg"]["type"]["code"];

    $this->view->BG_REG_NUMBER = $data["BG_REG_NUMBER"];

    // $checkCounterGuaranteeFile = $this->_db->select()
    // ->from(["A" => "T_BANK_GUARANTEE_DETAIL"], [('*')])
    // ->where("A.BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
    // ->where("A.PS_FIELDNAME = ?", "Counter Guarantee Document")
    // ->query()->fetchAll();

    $checkCounterGuaranteeFile = $this->_db->select()
      ->from(
        array('A' => 'T_BANK_GUARANTEE_DETAIL'),
        array('*')
      )
      ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
      ->where('A.PS_FIELDNAME = ?', "Counter Guarantee Document")
      ->query()->fetchAll();


    $this->view->checkCounterGuaranteeFile = count($checkCounterGuaranteeFile);

    $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

    $this->view->arrbgType = $arrbgType;

    //BG Counter Guarantee Type
    $bgcgType         = $conf["bgcg"]["type"]["desc"];
    $bgcgCode         = $conf["bgcg"]["type"]["code"];

    $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

    $this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];


    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
      $saveHoldAmount = null;

      $bgdatasplit = $this->_db->select()
        ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
        // ->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO", ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID'])
        ->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO AND B.CUST_ID = '" . $data['CUST_ID'] . "'", ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
        ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
        ->query()->fetchAll();

      $getEscrow = array_search("Escrow", array_column($bgdatasplit, "ACCT_DESC"));

      if ($getEscrow !== false) {
        $serviceAccount = new Service_Account($bgdatasplit[$getEscrow]["ACCT"], null);

        $getResultService = $serviceAccount->inquiryAccontInfo();
        $cif = $getResultService["cif"];

        $serviceCif = new Service_Account(null, null, null, null, null, $cif);
        $saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
        $getCurrencyEscrow = array_search($bgdatasplit[$getEscrow]["ACCT"], array_column($saveResult, "account_number"));

        $escrowCurrency = 'IDR';
        if ($getCurrencyEscrow !== false) {
          $escrowCurrency = $saveResult[$getCurrencyEscrow]["currency"];
          $escrowType = $saveResult[$getCurrencyEscrow]["type_desc"];
        }

        $this->view->escrowCurrency = $escrowCurrency;
        $this->view->escrowType = $escrowType;
      }

      foreach ($bgdatasplit as $key => $value) {
        if ($value['CUST_ID'] != $data['CUST_ID']) continue;
        if ($value["ACCT_DESC"]) continue;

        // if (strtolower($value["M_ACCT_DESC"]) != "giro" && $value["M_ACCT_TYPE"] != "D" && $value["M_ACCT_TYPE"] != "20" && $value["M_ACCT_TYPE"] != 20) continue;

        $saveHoldAmount += intval($value["AMOUNT"]);
      }

      $this->view->fullmember = $bgdatasplit;

      $this->view->holdAmount = $saveHoldAmount;
    }

    // Marginal Deposit ---------------
    $bgdatamdsplit = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
      ->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO AND B.CUST_ID = '" . $data['CUST_ID'] . "'", ["TYPE" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", "CURRENCY" => "B.CCY_ID", 'CUST_ID'])
      ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
      ->query()->fetchAll();

    if ($data['COUNTER_WARRANTY_TYPE'] == '3' && !empty($bgdatamdsplit)) {
      $mdAmount = null;
      $getEscrow = array_search("Escrow", array_column($bgdatamdsplit, "ACCT_DESC"));

      if ($getEscrow !== false) {
        $serviceAccount = new Service_Account($bgdatamdsplit[$getEscrow]["ACCT"], null);
        $getResultService = $serviceAccount->inquiryAccontInfo();
        $cif = $getResultService["cif"];
        $serviceCif = new Service_Account(null, null, null, null, null, $cif);
        $saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
        $getCurrencyEscrow = array_search($bgdatamdsplit[$getEscrow]["ACCT"], array_column($saveResult, "account_number"));
        $escrowCurrency = 'IDR';

        if ($getCurrencyEscrow !== false) {
          $escrowCurrency = $saveResult[$getCurrencyEscrow]["currency"];
          $escrowType = $saveResult[$getCurrencyEscrow]["type_desc"];
        }

        $this->view->escrowCurrency = $escrowCurrency;
        $this->view->escrowType = $escrowType;
      }

      foreach ($bgdatamdsplit as $key => $valmd) {
        if ($valmd['CUST_ID'] != $data['CUST_ID']) continue;
        if ($valmd["ACCT_DESC"]) continue;
        $mdAmount += intval($valmd['AMOUNT']);
      }

      $this->view->bgdatamdsplit = $bgdatamdsplit;
    }
    // Marginal Deposit ---------------

    $bgdatadetail = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
      // ->where('A.CUST_ID = ' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"])
      ->query()->fetchAll();

    if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
      $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
      $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

      $insuranceBranch = $this->_db->select()
        ->from("M_INS_BRANCH")
        ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
        ->query()->fetchAll();

      $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
    }

    if (!empty($bgdatadetail)) {
      foreach ($bgdatadetail as $key => $value) {

        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
          if ($value['PS_FIELDNAME'] == 'Insurance Name') {
            $this->view->insuranceName =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
            $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount') {
            $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
            $this->view->paDateStart =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
            $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
          }
        } else {

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
            $this->view->owner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
            $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
            $this->view->owner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
            $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
            $this->view->owner3 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
            $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
          }
        }
      }
    }

    $principleData = [];
    if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
      foreach ($bgdatadetail as $key => $value) {
        $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
      }

      $this->view->principleData = $principleData;
    }

    $get_linefacility = $this->_db->select()
      ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
      ->where("CUST_ID = ?", $data["CUST_ID"])
      ->query()->fetchAll();

    $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
    $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

    $this->view->linefacility = $get_linefacility[0];

    $this->view->acct = $data['FEE_CHARGE_TO'];

    $conf = Zend_Registry::get('config');
    $this->view->bankname = $conf['app']['bankname'];

    $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
    $AccArr = $CustomerUser->getAccountsBG($param);
    //var_dump($AccArr);die;

    if (!empty($AccArr)) {
      $this->view->src_name = $AccArr['0']['ACCT_NAME'];
    }

    $get_cash_collateral = $this->_db->select()
      ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
      ->where("CUST_ID = ?", "GLOBAL")
      ->where("CHARGES_TYPE = ?", "10")
      ->query()->fetchAll();

    $this->view->cash_collateral = $get_cash_collateral[0];

    $bgpublishType     = $conf["bgpublish"]["type"]["desc"];
    $bgpublishCode     = $conf["bgpublish"]["type"]["code"];

    $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

    $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

    $arrBankFormat = array(
      1 => 'Bank Standard',
      2 => 'Special Format (with bank approval)'
    );

    $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
    $this->view->bankFormatNumber = $data['BG_FORMAT'];

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Billingual',
    );

    $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

    $checkOthersAttachment = $this->_db->select()
      ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
      ->where("BG_REG_NUMBER = '268312345601F4201'")
      ->order('A.INDEX ASC')
      ->query()->fetchAll();

    if (count($checkOthersAttachment) > 0) {
      $this->view->othersAttachment = $checkOthersAttachment;
    }

    // Get data T_BANK_GUARANTEE_HISTORY
    $select = $this->_db->select()
      ->from(
        array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
        array('*')
      )
      ->where('TBGH.BG_REG_NUMBER = ?', $decryption_bg)
      ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
    $dataHistory = $this->_db->fetchAll($select);

    // Get data TEMP_BANK_GUARANTEE_SPLIT
    $select = $this->_db->select()
      ->from(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        array('*')
      )
      ->where('TBGS.BG_REG_NUMBER = ?', $decryption_bg);
    $dataAccSplit = $this->_db->fetchAll($select);

    $config = Zend_Registry::get('config');

    $docTypeCode = $config["bgdoc"]["type"]["code"];
    $docTypeDesc = $config["bgdoc"]["type"]["desc"];
    $docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

    $statusCode = $config["bg"]["status"]["code"];
    $statusDesc = $config["bg"]["status"]["desc"];
    $statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

    $historyStatusCode = $config["history"]["status"]["code"];
    $historyStatusDesc = $config["history"]["status"]["desc"];
    $historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

    $counterTypeCode = $config["bgcg"]["type"]["code"];
    $counterTypeDesc = $config["bgcg"]["type"]["desc"];
    $counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

    //$config       = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    // $arrStatus = array(
    //   '7'  => 'Canceled',
    //   '20' => 'On Risk',
    //   '21' => 'Off Risk',
    //   '22' => 'Claimed On Process',
    //   '23' => 'Claimed'
    // );

    $printOption = $this->_request->getParam('print');

    $this->view->arrStatus = $arrStatus;

    $getGuarantedTransanctions = $this->_db->select()
      ->from('T_BANK_GUARANTEE_UNDERLYING')
      ->where('BG_REG_NUMBER = ?', $decryption_bg)
      ->query()->fetchAll();

    if ($printOption == 1) {
      // if($compInfo['CUST_MODEL'] == 3){
      $params = [
        'data'        => $data,
        'arrStatus'        => $arrStatus,
      ];
      // }else{
      //   $params = [
      //     'compInfo'        => $compInfo, 
      //     'accountInfo'     => $accountInfo, 
      //     'groupUser'       => $groupUser,
      //     'dataBoundary'    => $dataBoundary,
      //     'transferTypeArr' => $transferTypeArr,
      //     'lineFacility' => $lineFacility,
      //   ];
      // }
      $this->_forward('printeformdetailbg', 'index', 'widget', $params);
    }

    $this->view->arrStatus = $arrStatus;

    $underlyingDoc = $this->_db->select()
      ->from('T_BANK_GUARANTEE_UNDERLYING')
      ->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
      ->query()->fetchAll();

    $this->view->underlyingDoc = $underlyingDoc;

    $this->view->data               = $data;
    //Zend_Debug::dump($data);
    $this->view->dataHistory        = $dataHistory;
    $this->view->dataAccSplit       = $dataAccSplit;
    $this->view->docTypeArr         = $docTypeArr;
    $this->view->getGuarantedTransanctions         = $getGuarantedTransanctions;
    $this->view->statusArr          = $statusArr;
    $this->view->historyStatusArr   = $historyStatusArr;
    $this->view->counterTypeArr     = $counterTypeArr;

    Application_Helper_General::writeLog('VLBG', 'Unduh Detail BG No : ' . $data['BG_NUMBER']);

    $download = $this->_getParam('download');
    if ($download == 1) {
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';

      $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
    }
  }

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg No# /  BG No#'),
      ),
      'subject'     => array(
        'field'    => 'BG_SUBJECT',
        'label'    => $this->language->_('Subject'),
      ),
      'ccy'  => array(
        'field'    => 'BG_AMOUNT',
        'label'    => 'CCY',
      ),
      'bgamount'  => array(
        'field'    => 'BG_AMOUNT',
        'label'    => $this->language->_('BG Amount'),
      ),
      'startdate' => array(
        'field' => 'TIME_PERIOD_START',
        'label' => $this->language->_('Date From'),
      ),
      'enddate'   => array(
        'field'    => 'TIME_PERIOD_END',
        'label'    => $this->language->_('Date To'),
      ),
      'format'   => array(
        'field'    => 'BG_STATUS',
        'label'    => $this->language->_('Status'),
      )
    );

    $filterlist = array('BG Number' => 'BG_NUMBER', 'Subject' => 'BG_SUBJECT', 'Obligee Name' => 'RECIPIENT_NAME', 'Bank Branch' => 'BRANCH_NAME', 'Counter Type' => 'COUNTER_WARRANTY_TYPE', 'Tanggal Terbit' =>  'TANGGAL', 'Status' => 'BG_STATUS');

    $this->view->filterlist = $filterlist;

    $filterArr = array(
      'BG_NUMBER'  => array('StringTrim', 'StripTags'),
      'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
      'RECIPIENT_NAME'  => array('StringTrim', 'StripTags'),
      //'BG_AMOUNT'   => array('StringTrim', 'StripTags'),
      'BRANCH_NAME'   => array('StringTrim', 'StripTags'),
      'COUNTER_WARRANTY_TYPE'    => array('StringTrim', 'StripTags'),
      'TIME_PERIOD_START'  => array('StringTrim', 'StripTags'),
      'TIME_PERIOD_END'   => array('StringTrim', 'StripTags'),
      'BG_STATUS'   => array('StringTrim', 'StripTags')
    );

    $dataParam = array('BG_NUMBER', 'BG_SUBJECT', 'RECIPIENT_NAME', 'BG_STATUS', 'BRANCH_NAME', 'COUNTER_WARRANTY_TYPE', 'TIME_PERIOD_START', 'TIME_PERIOD_END');
    $dataParamValue = array();


    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');

        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            if (!empty($dataParamValue[$dtParam])) {
              $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
            }
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }
    }

    if ($this->_request->getParam('createdate')) {
      $dataParamValue['TIME_PERIOD_START'] = $this->_request->getParam('createdate')[0];
      $dataParamValue['TIME_PERIOD_END'] = $this->_request->getParam('createdate')[1];
    }

    $options = array('allowEmpty' => true);
    $validators = array(
      'TIME_PERIOD_START'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'TIME_PERIOD_END'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BG_NUMBER' => array(),
      'BG_SUBJECT' => array(),
      'RECIPIENT_NAME' => array(),
      //'BG_AMOUNT' => array(),
      'BRANCH_NAME' => array(),
      'COUNTER_WARRANTY_TYPE' => array(),
      'BG_STATUS' => array()
    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

    $fbgnumb    = $zf_filter->getEscaped('BG_NUMBER');
    $subject   = $zf_filter->getEscaped('BG_SUBJECT');
    $recipient   = $zf_filter->getEscaped('RECIPIENT_NAME');
    //$fbgmaount   = $zf_filter->getEscaped('BG_AMOUNT');
    $fbranchname   = $zf_filter->getEscaped('BRANCH_NAME');
    $fcounterType   = $zf_filter->getEscaped('COUNTER_WARRANTY_TYPE');
    $fstatus   = $zf_filter->getEscaped('BG_STATUS');

    $whereIn = [15, 16];

    $select = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
      ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
      ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_STATUS IN (?)', $whereIn)
      ->order('BG_CREATED DESC');

    if ($fbgnumb) {
      $select->where("BG_NUMBER LIKE " . $this->_db->quote('%' . $fbgnumb . '%'));
    }
    if ($subject) {
      $select->where("BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
    }
    if ($recipient) {
      $select->where("RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $recipient . '%'));
    }
    /*if ($fbgmaount) {
      $select->where("BG_AMOUNT LIKE " . $this->_db->quote('%' . $fbgmaount . '%'));
    }*/
    if ($fbranchname) {
      $select->where("BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fbranchname) . '%'));
    }
    if ($fcounterType) {
      $select->where("COUNTER_WARRANTY_TYPE = ?", $fcounterType);
    }
    if ($fstatus) {
      $select->where("BG_STATUS = ?", $fstatus);
    }

    if (!empty($dataParamValue['TIME_PERIOD_START']) && !empty($dataParamValue['TIME_PERIOD_END'])) {

      $select->where("TIME_PERIOD_START >= ?", date('Y-m-d', strtotime($dataParamValue['TIME_PERIOD_START'])));
      $select->where("TIME_PERIOD_END >= ?", date('Y-m-d', strtotime($dataParamValue['TIME_PERIOD_END'])));
    }

    $select = $select->query()->fetchAll();

    if ($this->_request->getParam('downloadcsv')) {

      $this->_helper->viewRenderer->setNoRender();
      $this->_helper->layout()->disableLayout();

      $response = Zend_Controller_Front::getInstance()->getResponse();
      $response->setHeader('Content-Disposition', 'attachment; filename="BankGuaranteeList.csv"');
      $response->setHeader('Content-Type', 'text/csv');

      $arrWarrantyType = array(
        '1' => 'Full Cover',
        '2' => 'Line Facility',
        '3' => 'Insurance'
      );

      $config        = Zend_Registry::get('config');
      $BgType     = $config["bg"]["status"]["desc"];
      $BgCode     = $config["bg"]["status"]["code"];
      $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

      $output = fopen('php://output', 'w');

      $heading = array(
        'BG Number / Subject',
        'Obligee Name',
        'BG Amount',
        'Bank Branch',
        'Counter Type',
        'Start Date',
        'End Date',
        'Status'
      );

      fputcsv($output, $heading);

      foreach ($select as $row) {
        $subData = [];
        $subData['BG_REG_NUMBER'] = $row['BG_REG_NUMBER'] . ' / ' . $row['BG_SUBJECT'];
        $subData['RECIPIENT_NAME'] = $row['RECIPIENT_NAME'];
        $subData['BG_AMOUNT'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['BG_AMOUNT']);
        $subData['BRANCH_NAME'] = $row['BRANCH_NAME'];

        if (empty($row['BG_INSURANCE_CODE'])) {
          $subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']];
        } else {
          $subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']] . ' (' . $row['BG_INSURANCE_CODE'] . ')';
        }

        $subData['TIME_PERIOD_START'] = Application_Helper_General::convertDate($row['TIME_PERIOD_START'], $this->viewDateFormat, $this->defaultDateFormat);
        $subData['TIME_PERIOD_END'] = Application_Helper_General::convertDate($row['TIME_PERIOD_END'], $this->viewDateFormat, $this->defaultDateFormat);
        $subData['BG_STATUS'] = $arrStatus[$row['BG_STATUS']];

        fputcsv($output, $subData);
      }

      fclose($output);

      return true;
    }

    $this->paging($select);

    $conf = Zend_Registry::get('config');

    $this->view->bankname = $conf['app']['bankname'];

    // $arrStatus = array('1' => 'Waiting for review',
    //'2' => 'Waiting for approve',
    //'3' => 'Waiting to release',
    //'4' => 'Waiting for bank approval',
    //'5' => 'Issued',
    //'6' => 'Expired',
    //'7' => 'Canceled',
    //'8' => 'Claimed by applicant',
    //'9' => 'Claimed by recipient',
    //'10' => 'Request Repair',
    //'11' => 'Reject',
    //);

    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;
    // BG limit
    // $cust_id = $this->_db->quote((string)$this->_custIdLogin);
    // $select2 = $this->_db->select()
    // ->from(array('BG' => 'T_BANK_GUARANTEE'), array('*'))
    // ->join(array('C' => 'M_CUSTOMER'), 'BG.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'))
    // ->order('BG_UPDATED DESC');
    // $select2->where('BG.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));

    // $bglist = $this->_db->fetchAll($select2);

    // $usedbg = 0;
    // foreach ($bglist as $vl) {
    //   if ($vl['BG_STATUS'] == '5' && $vl['COUNTER_WARRANTY_TYPE'] == '2') {
    //     $usedbg = $usedbg + $vl['BG_AMOUNT'];
    //   }
    // }
    // $this->view->bgused = Application_Helper_General::displayMoney($usedbg);
    // $this->view->bglist = $bglist;

    // $select3 = $this->_db->select()
    // ->from(array('C' => 'M_CUSTOMER'), array('*'));
    // $select3->where('C.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
    // $bglimit = $this->_db->fetchAll($select3);

    // foreach ($bglimit as $val) {
    //   $this->view->bglimit   = Application_Helper_General::displayMoney($val['CUST_BG_LIMIT']);
    // }

    // END BG limit

    $arrWarrantyType = array(
      '1' => 'Full Cover',
      '2' => 'Line Facility',
      '3' => 'Insurance'
    );

    $this->view->arrWarrantyType = $arrWarrantyType;

    $arrType = array(
      1 => 'Standard',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;

    if (!empty($dataParamValue)) {

      $this->view->createdStart = $dataParamValue['TIME_PERIOD_START'];
      $this->view->createdEnd = $dataParamValue['TIME_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[] = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[] = $key;
          $whereval[] = $value;
        }
      }

      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }


    if ($select) {

      $data = [];

      if (!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1) {

        foreach ($select as $key => $row) {
          $subData = [];
          $subData['BG_REG_NUMBER'] = $row['BG_REG_NUMBER'] . ' / ' . $row['BG_SUBJECT'];
          $subData['RECIPIENT_NAME'] = $row['RECIPIENT_NAME'];
          $subData['BG_AMOUNT'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['BG_AMOUNT']);
          $subData['BRANCH_NAME'] = $row['BRANCH_NAME'];

          if (empty($row['BG_INSURANCE_CODE'])) {
            $subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']];
          } else {
            $subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']] . ' (' . $row['BG_INSURANCE_CODE'] . ')';
          }

          $subData['TIME_PERIOD_START'] = Application_Helper_General::convertDate($row['TIME_PERIOD_START'], $this->viewDateFormat, $this->defaultDateFormat);
          $subData['TIME_PERIOD_END'] = Application_Helper_General::convertDate($row['TIME_PERIOD_END'], $this->viewDateFormat, $this->defaultDateFormat);
          $subData['BG_STATUS'] = $arrStatus[$row['BG_STATUS']];

          $data[] = $subData;
        }
      }

      if ($this->_getParam('csv')) {
        $this->_helper->download->csv(array($this->language->_('BG Number / Subject'), $this->language->_('Obligee Name'), $this->language->_('BG Amount'), $this->language->_('Bank Branch'), $this->language->_('Counter Type'), $this->language->_('Start Date'), $this->language->_('End Date'), $this->language->_('Status')), $data, null, 'Bank Guarantee List');
      } else if ($this->_request->getParam('print') == 1) {

        $fields = array(
          'regno'     => array(
            'field'    => 'BG_REG_NUMBER',
            'label'    => $this->language->_('Reg No# /  BG No#'),
          ),
          'subject'     => array(
            'field'    => 'RECIPIENT_NAME',
            'label'    => $this->language->_('Subject'),
          ),
          'bgamount'  => array(
            'field'    => 'BG_AMOUNT',
            'label'    => $this->language->_('BG Amount'),
          ),
          'branch'  => array(
            'field'    => 'BRANCH_NAME',
            'label'    => $this->language->_('Branch'),
          ),
          'countertype'  => array(
            'field'    => 'COUNTER_WARRANTY_TYPE',
            'label'    => $this->language->_('Counter Type'),
          ),
          'startdate' => array(
            'field' => 'TIME_PERIOD_START',
            'label' => $this->language->_('Date From'),
          ),
          'enddate'   => array(
            'field'    => 'TIME_PERIOD_END',
            'label'    => $this->language->_('Date To'),
          ),
          'format'   => array(
            'field'    => 'BG_STATUS',
            'label'    => $this->language->_('Status'),
          )
        );

        $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Bank Guarantee List', 'data_header' => $fields));
      }
    }

    Application_Helper_General::writeLog('VLBG', 'Lihat Daftar Bank Garansi');
  }

  public function getstatusAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');

    $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    foreach ($arrStatus as $key => $row) {
      if ($key == 16 || $key == 15) {
        if ($tblName == $key) {
          $select = 'selected';
        } else {
          $select = '';
        }
        $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
      }
    }

    echo $optHtml;
  }

  public function getwarantyAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');

    $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

    $arrWarrantyType = array(
      '1' => 'Full Cover',
      '2' => 'Line Facility',
      '3' => 'Insurance'
    );

    foreach ($arrWarrantyType as $key => $row) {
      if ($tblName == $key) {
        $select = 'selected';
      } else {
        $select = '';
      }
      $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
    }

    echo $optHtml;
  }

  public function escrowdetailAction()
  {
    $this->_helper->_layout->setLayout('popup');

    $sessToken  = new Zend_Session_Namespace('Tokenenc');
    $password   = $sessToken->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $decryption = urldecode($this->_request->getParam("bgnumb"));
    $decryption_bg = $AESMYSQL->decrypt($decryption, $password);

    $getCustName = $this->_db->select()
      ->from(["A" => "T_BANK_GUARANTEE"], [])
      ->joinLeft(["B" => "M_CUSTOMER"], "A.CUST_ID = B.CUST_ID", ["CUST_NAME", 'CUST_ID'])
      ->where("BG_REG_NUMBER = ?", $decryption_bg)
      ->query()->fetch();

    $this->view->custName = $getCustName["CUST_NAME"];

    $bgdatasplit = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
      ->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO AND B.CUST_ID = '" . $getCustName['CUST_ID'] . "'", ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", "M_CCY_ID" => "B.CCY_ID"])
      ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
      ->where("LOWER(B.ACCT_TYPE) = 'd' OR B.ACCT_TYPE = '20' OR LOWER(A.ACCT_DESC) = 'giro' OR LOWER(A.ACCT_TYPE) = 'giro'")
      ->query()->fetchAll();

    // $escrow_acct = null;
    // if (array_search('Escrow', array_column($bgdatasplit, "ACCT_DESC")) !== false) {
    //   $escrow_acct = $bgdatasplit[array_search('Escrow', array_column($bgdatasplit, "ACCT_DESC"))]["ACCT"];
    //   unset($bgdatasplit[array_search('Escrow', array_column($bgdatasplit, "ACCT_DESC"))]);
    // }

    $escrow_acct = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
      ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
      ->where('ACCT_DESC = ?', 'Escrow')
      ->query()->fetch();

    $bgdatasplit = array_values($bgdatasplit);

    $this->view->escrow_acct = $escrow_acct['ACCT'];
    $this->view->fullmember = $bgdatasplit;
  }

  public function exportcsvAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $bgNumber = $this->_getParam('bgNumber');

    $response = Zend_Controller_Front::getInstance()->getResponse();
    $response->setHeader('Content-Disposition', 'attachment; filename="BankGuaranteeList.csv"');
    $response->setHeader('Content-Type', 'text/csv');

    $select = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
      ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
      ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_NUMBER IN (?)', $bgNumber)
      ->order('BG_CREATED DESC');
    $select = $select->query()->fetchAll();

    $arrWarrantyType = array(
      '1' => 'Full Cover',
      '2' => 'Line Facility',
      '3' => 'Insurance'
    );

    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];
    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $output = fopen('php://output', 'w');

    $heading = array(
      'BG Number / Subject',
      'Obligee Name',
      'BG Amount',
      'Bank Branch',
      'Counter Type',
      'Start Date',
      'End Date',
      'Status'
    );

    fputcsv($output, $heading);

    foreach ($select as $row) {
      $subData = [];
      $subData['BG_REG_NUMBER'] = $row['BG_REG_NUMBER'] . ' / ' . $row['BG_SUBJECT'];
      $subData['RECIPIENT_NAME'] = $row['RECIPIENT_NAME'];
      $subData['BG_AMOUNT'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['BG_AMOUNT']);
      $subData['BRANCH_NAME'] = $row['BRANCH_NAME'];

      if (empty($row['BG_INSURANCE_CODE'])) {
        $subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']];
      } else {
        $subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']] . ' (' . $row['BG_INSURANCE_CODE'] . ')';
      }

      $subData['TIME_PERIOD_START'] = Application_Helper_General::convertDate($row['TIME_PERIOD_START'], $this->viewDateFormat, $this->defaultDateFormat);
      $subData['TIME_PERIOD_END'] = Application_Helper_General::convertDate($row['TIME_PERIOD_END'], $this->viewDateFormat, $this->defaultDateFormat);
      $subData['BG_STATUS'] = $arrStatus[$row['BG_STATUS']];

      fputcsv($output, $subData);
    }

    fclose($output);
  }
}
