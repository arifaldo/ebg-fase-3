<?php

Class setbillercharges_Model_Setbillercharges
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getServiceProvider()
    {
		$data = $this->_db->select()->distinct()
					->from(array('M_SERVICE_PROVIDER'),array('PROVIDER_CODE'))
					->order('PROVIDER_CODE ASC')
				 	-> query() ->fetchAll();
	
       return $data;
    }
  	public function getServiceProviderAcc()
    {  
       	$data = $this->_db->select()
					->from(array('B' => 'M_PROVIDER_ACCT'),array('*'))
					->joinleft (array('A' => 'M_SERVICE_PROVIDER'),'B.PROVIDER_ID = A.PROVIDER_ID',array('*'))
					//->where("B.PROVIDER_ID = ".$providerid);M_SERVICE_PROVIDER
					->where("A.PROVIDER_CODE != ''")
					->order('A.PROVIDER_CODE ASC')
					-> query() ->fetchAll();
		return $data;		

		Zend_Debug::dump($data);
    }
  	
    public function getServiceType()
    {
		$data = $this->_db->select()->distinct()
					->from(array('M_SERVICES_TYPE'),array('SERVICE_ID','SERVICE_NAME'))
					->order('SERVICE_NAME ASC')
				 	-> query() ->fetchAll();
	
       return $data;
    }
  
    public function getData($fParam,$sortBy,$sortDir,$filter)
    {
		$config = Zend_Registry::get('config');
		$providerType = $config['provider']['type'];
		$providerType = array_combine(array_values($providerType['code']),array_values($providerType['desc']));
		$caseProvider = "(CASE B.PROVIDER_TYPE ";
// 		print_r($providerType);die;
		foreach($providerType as $key=>$val)
		{
		  $caseProvider .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseProvider .= " END )";

		$select2 = $this->_db->select()->distinct()
						->from(array('B' => 'M_SERVICE_PROVIDER'),
						array('B.PROVIDER_ID','B.PROVIDER_CODE','B.PROVIDER_NAME','PROVIDER_TYPE' => $caseProvider,'A.CHARGES_SUGGESTED','A.CHARGES_SUGGESTEDBY'))
						->joinleft (array('A' => 'M_CHARGES_PROVIDER'),'A.PROVIDER_ID = B.PROVIDER_ID',array());
						
		if($filter == 'Set Filter')
		{			
			if(!empty($fParam['datefrom']) && empty($fParam['dateto']))
					$select2->where("DATE(A.CHARGES_SUGGESTED) >= ".$this->_db->quote($fParam['datefrom']));
					
			if(empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(A.CHARGES_SUGGESTED) <= ".$this->_db->quote($fParam['dateto']));
					
			if(!empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(A.CHARGES_SUGGESTED) BETWEEN ".$this->_db->quote($fParam['datefrom'])." AND ".$this->_db->quote($fParam['dateto']));
		}
		
		if($filter == 'Set Filter')
		{
		    if($fParam['billerCode'])
		    {
	       		$select2->where("B.PROVIDER_CODE = ".$this->_db->quote($fParam['billerCode']));
		    }
		    
			if($fParam['billerName'])
			{
	       		$select2->where("B.PROVIDER_NAME LIKE ".$this->_db->quote('%'.$fParam['billerName'].'%'));
			}
			
			if($fParam['suggestor'])
		    {
	       		$select2->where("A.CHARGES_SUGGESTEDBY LIKE ".$this->_db->quote($fParam['suggestor']));
		    }
		}
		$select2->order($sortBy.' '.$sortDir);       
		
       return $this->_db->fetchAll($select2);
    }
	
	public function getOneProvider($providerid,$ccy)
	{
		$sqlProvider = $this->_db->select()
						->from(array('B' => 'M_SERVICE_PROVIDER'),array('B.PROVIDER_ID','B.PROVIDER_CODE','B.PROVIDER_NAME','A.CHARGES_CCY','A.CHARGES_AMT'))
						->joinleft (array('A' => 'M_CHARGES_PROVIDER'),'A.PROVIDER_ID = B.PROVIDER_ID',array())
						->where("B.PROVIDER_ID = ?", $providerid);
		
		if($ccy)
			$sqlProvider->where("A.CHARGES_CCY = ".$this->_db->quote($ccy));
					
		return 	$this->_db->fetchRow($sqlProvider);
	}
	
	public function getBillerAcctList($providerid)
	{
		$billerAcct = $this->_db->fetchAll(
						$this->_db->select()
							->from(array('B' => 'M_PROVIDER_ACCT'),array('B.ACCT_NO'))
							->where("B.PROVIDER_ID = ?", $providerid)
						);
		
		return Application_Helper_Array::simpleArray($billerAcct,'ACCT_NO');
	}
	
	public function isRequestChange($provider_code)
	{
		$cek = $this->_db->fetchRow(
				$this->_db->select()
						->from('T_GLOBAL_CHANGES')
						->where("KEY_FIELD LIKE ".$this->_db->quote($provider_code))
						->where("DISPLAY_TABLENAME = 'Biller Charges'")
						->where("CHANGES_STATUS = 'WA'")
					);
		return $cek;
	}
	
	public function getGlobalChanges($change_id)
	{
		$cek = $this->_db->fetchRow(
				$this->_db->select()
						->from('T_GLOBAL_CHANGES')
						->where("CHANGES_ID = ?", $change_id)
					);
		return $cek;
	}
	
	public function getTemp($changes_id)
	{
		return $this->_db->fetchAll(
									$this->_db->select()
									->from(array('A' => 'TEMP_CHARGES_PROVIDER'),array('B.PROVIDER_ID','B.PROVIDER_CODE','B.PROVIDER_NAME','A.CHARGES_CCY','A.CHARGES_AMT',
																							'CHARGES_TO' => $casechargesTo,'A.CHARGES_ACCT'))
									->joinleft (array('B' => 'M_SERVICE_PROVIDER'),'A.PROVIDER_ID = B.PROVIDER_ID',array())
									->where('A.CHANGES_ID = ?', $changes_id)
								);
	}
	
	public function insertTemp($param)
	{
		$this->_db->insert('TEMP_CHARGES_PROVIDER',$param);
	}
	
	public function updateTemp($change_id,$amt)
	{
		$updateArr = array(
						'CHARGES_AMT' => $amt 				
					);
		
		$whereArr = array(
						'CHANGES_ID = ?'     => $change_id
					);
					
		$this->_db->update('TEMP_CHARGES_PROVIDER',$updateArr,$whereArr);
	}

	public function deleteTemp($change_id)
	{
		$where = array('CHANGES_ID = ?' => $change_id);
		$this->_db->delete('TEMP_CHARGES_PROVIDER', $where);
	}

}