<?php

require_once 'Zend/Controller/Action.php';

class createnewhelp_IndexController extends Application_Main 
{
	public function indexAction() 
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		$attahmentDestination 	= UPLOAD_PATH . '/document/help/';		
		$errorRemark 			= null;
		$adapter 				= new Zend_File_Transfer_Adapter_Http();
		
		if($this->_request->isPost())
		{
			$params = $this->_request->getParams();
			$fileExt 				= "pdf";
			
			$sourceFileName = $adapter->getFileName();
			//$sourceFileName = substr(basename($adapter->getFileName()), 0);
			//die($sourceFileName);
			if($sourceFileName == null)
			{
				$sourceFileName = null;
				$fileType = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter->getFileName()), 0);
				if($_FILES["document"]["type"])
				{
					$adapter->setDestination($attahmentDestination);
					$maxFileSize 			= $this->getSetting('Fe_attachment_maxbyte');
					$fileType = $adapter->getMimeType();
					$size = $_FILES["document"]["size"];
				}
				else
				{
					$fileType = null;
					$size = null;
				}
			}
			$paramsName['sourceFileName'] 	= $sourceFileName;
			
			$helpTopic 			= $this->_getParam('helpTopic');
			if($helpTopic == null)
			{
				$helpTopic = null;
			}
			$paramsName['helpTopic']  = $helpTopic;
			

			$filtersName = array(
									'sourceFileName'		=> array('StringTrim'),
									'helpTopic'	=> array('StripTags', 'StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															array('Db_NoRecordExists',array(
																								'table'=>'T_HELP',
																								'field'=>'HELP_FILENAME',
																								'exclude'   => array(
																														'field' => 'HELP_ISDELETED',
																														'value' => '1'
																													)
																							)
																),
															'messages' => array(
																					"Error : File Upload cannot be left blank or File Size too large",
																					"Error : File ".$sourceFileName." Already Exist"
																				)
														),
									'helpTopic' => array(
																'NotEmpty',
																array('StringLength', array('max' => 50)),
																new Zend_Validate_Regex('/^[a-zA-Z 0-9]*$/'),
																'messages' => array(
																						"Error: Help Topic cannot be left blank",
																						"Error: Help Topic size more than 50 char",
																						"Error: Help Topic Format Must Alpha Numerik",
																					)
															),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			if($zf_filter_input_name->isValid())
			{
				//$name = $_FILES['document']['name'];
				//die($name);
				//$fileTypeDis = $adapter->getFileInfo();
				//die($);
				$fileTypeMessage = explode('/',$fileType);
				$fileType =  $fileTypeMessage[1];
				$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
				$extensionValidator->setMessage("Extension file must be *.pdf");

				$maxFileSize = "1024000";
				$size = number_format($size);
				
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
				$sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");
				
				$adapter->setValidators(array($extensionValidator, $sizeValidator));
				
				if($adapter->isValid())
				{
					
					$newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
					$adapter->addFilter('Rename', $newFileName);
					//$fileType = $adapter->getMimeType();
					if($adapter->receive())
					{
						try
						{
							$this->_db->beginTransaction();
							$insertArr = array(
								'UPLOADED_BY'			=> $this->_userIdLogin, 
								'UPLOAD_DATETIME' 		=> new Zend_Db_Expr('now()'), 
								'HELP_TOPIC' 			=> $params['helpTopic'],
								'HELP_DESCRIPTION'		=> $this->_request->getParam('description'), 
								'HELP_FILENAME' 		=> $sourceFileName,
								'HELP_SYS_FILENAME'		=> $newFileName,
							);			
							$select = $this->_db->insert('T_HELP', $insertArr);
							//die($select);
							$this->_db->commit();
							
							$this->setbackURL('/helplist/index/index/');
							$this->_redirect('/notification/success');
							
							//$this->_redirect('/notification/success/index');
						}
						catch(Exception $e)
						{
							$this->_db->rollBack();
							$errorRemark = $this->getErrorRemark('82');
							$this->view->errorMsg = array(array($errorRemark));
						//	SGO_Helper_GeneralLog::technicalLog($e);
						}
					}					
				}
				else
				{
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}
			}
			else
			{
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName']))? $errors['sourceFileName'] : null;
				$this->view->helpTopicErr 		= (isset($errors['helpTopic']))? $errors['helpTopic'] : null;
				//$this->view->helpTopic 			= $params['helpTopic'];
			}
		}
		
		
		//$this->view->max_file_size = $maxFileSize;
		//$this->view->file_extension = $fileExt;
		
		//$disable_note_len 	 = (isset($params['description']))  ? strlen($params['description'])  : 0;	
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
		
		$disable_note_len 	 = 100; //- $disable_note_len;		
		$note_len 	 = 100 - $note_len;	

		$this->view->disable_note_len	= $disable_note_len;
		$this->view->note_len			= $note_len;
		
	}
}
