<?php


require_once 'Zend/Controller/Action.php';


class remittancechanges_DetailController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$arraycharges = array_combine(array_values($this->_monthlytype['code']),array_values($this->_monthlytype['desc']));
		$custid = $this->_getParam('custid');
		$docErr = "*No changes allowed for this record while awaiting approval for previous change";
		$select = $this->_db->select()
				->from(array('A' => 'M_CUSTOMER'),array('*'));
		$select -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $this->_db->fetchRow($select);
		$this->view->result = $result;
		
////////Realtime charges/////////////////////////////////////////////////////////////////////////////		
		
		$select3 = $this->_db->select()
							->from('TEMP_CHARGES_REMITTANCE');
		$select3 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select3 -> where("CHARGE_TYPE = '6'");
		$cektrf = $select3->query()->FetchAll();
// 		Zend_Debug::dump($select3);die;
		$select4 = $this->_db->select()
			        	->from(array('A' => 'M_CHARGES_REMITTANCE'),array('*'));
		$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$select4 -> where("A.CHARGE_TYPE = '6'");
// 		$select4 -> where("FLOOR(A.CHARGE_AMT) > 0");
		$result4 = $select4->query()->FetchAll();
		
		if($cektrf)
		{
			$this->view->disabletrf = true;
			$this->view->trferror = $docErr;
		}
		
		$i = 0;
		foreach($result4 as $trflist)
		{
			$td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
			if($i==0)
			{
				$templatetrfdetail = 
				'<tr>
					<td class="'.$td_css.'">'.$trflist['CHARGE_CCY'].'</td>
					<td class="'.$td_css.'">'.$trflist['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($trflist['CHARGE_AMT']).'</td>
			  	</tr>';
			}
			else
			{
				$templatetrfdetail = $templatetrfdetail.
				'<tr>
					<td class="'.$td_css.'">'.$trflist['CHARGE_CCY'].'</td>
					<td class="'.$td_css.'">'.$trflist['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($trflist['CHARGE_AMT']).'</td>
			  	</tr>';
			}
			$i++;
		}
		
		if(isSet($templatetrfdetail))
		{
			$templatetransfer =
			'<table class="table-sm table-striped table-responsive">
				<tr>
					<th valign="top">CCY</th>
					<th valign="top">Amount</th>
				</tr>'
				.$templatetrfdetail.
			'</table>';
		
			$this->view->templatetrf = $templatetransfer;
		}

/////////////////////////////////////////////////////////////////////////////////////////////////////


//////PDF///////////////////////////////////////////////////////////////////////////////////////////

		$pdf = $this->_getParam('pdf');
		if($pdf)
		{
			$template1 ='
			<div class="tabheader" id="tabheader">Company Charges and Administration Fee Detail</div>
			<br /><br />
			<h2>Detail Company Charges and Administration Fee</h2>
			<table border="0" cellspacing="0" cellpadding="0" class="tableform" width="600">
				<tr>
					<td class="tdform-even">&nbsp; Company Code</td>
					<td class="tdform-even">&nbsp;</td>
					<td class="tdform-odd">'.$result['CUST_ID'].'</td>
				</tr>
				<tr>
					<td class="tdform-even">&nbsp; Company Name</td>
					<td class="tdform-even">&nbsp;</td>
					<td class="tdform-odd">'.$result['CUST_NAME'].'</td>
				</tr>
				<tr>
					<td class="tdform-even">&nbsp; Current Charges Status</td>
					<td class="tdform-even">&nbsp;</td>
					<td class="tdform-odd">'.$chargestatuspdf.'</td>
				</tr>
				<tr>
					<td class="tdform-even">&nbsp; Current Administration Fee Status</td>
					<td class="tdform-even">&nbsp;</td>
					<td class="tdform-odd">'.$monthlyfeestatuspdf.'</td>
				</tr>
			</table>
			<br />
			<i>*Charges and Monthly Fee Status can be set from customer management menu</i>
			<br />
			<br />
			<br />';

			if($templatemonthly)
			{
				$template3 = '
				<h2>Monthly Fee</h2>
				'.'<b>'.$monthlytype.'</b><br/>'
				.$templatemonthly.'<br /><br /><br />';
			}
			else
			{
				$template3 = '
				<h2>Monthly Fee</h2>
				Monthly fee Charges not yet set<br /><br /><br />';
			}
			
			if($templaterealtime)
			{
				$template2 = '
				<h2>Inhouse Charges</h2>
				'.$templaterealtime.'<br /><br /><br />';
			}
			else
			{
				$template2 = '
				<h2>Inhouse Charges</h2>
				Realtime Charges not yet set<br /><br /><br />';
			}
			
			if($templateservice)
			{
				$template5 = '
				<h2>Service Charges</h2>
				'.$templateservice.'<br /><br /><br />';
			}
			else
			{
				$template5 = '
				<h2>Service Charges</h2>
				Service Charges scheme not yet set<br /><br /><br />';
			}
			
			$datapdf = "<tr><td>".$template1.$template3.$template2.$template5."</td></tr>";
			//Zend_Debug::dump($datapdf); die;
			$this->_helper->download->pdf(null,null,null,'Company Charges and Administration Fee',$datapdf);   
		}
		Application_Helper_General::writeLog('CHLS','View Company charges detail ('.$custid.')');
	}
}
