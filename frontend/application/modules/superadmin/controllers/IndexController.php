<?php
require_once ('Crypt/AESMYSQL.php');
require_once 'General/Settings.php';

class superadmin_IndexController extends Zend_Controller_Action {

	private $_db;
	
	public function init()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$this->_helper->layout()->setLayout('layout_superadmin');
	}
			
	public function indexAction() 
	{
		
		$a = date('m');
		$b = date('d');
		$c = $a+$b+1;
		echo($c);
		
		//---------------------- GENERATE CAPTCHA IMAGE  --------------------------------------
		$captcha = Application_Captcha::generateCaptcha ();
		$this->view->captchaId = $captcha ['id']; //returns the ID given to session &amp; image
		$this->view->captImgDir = $captcha ['imgDir'];
		//---------------------	END GENERATE CAPTCHA IMAGE -------------------------------------
				
		if ($this->_request->isPost ()) 
		{

			// ======================== START FILTERING ======================================
			$filters = array ('userId' => array ('StripTags', 'StringTrim', 'StringToUpper' ), 'password' => array () );
			$zf_filter_input = new Zend_Filter_Input ( $filters, null, $this->_getAllParams () );
			// ========================== END FILTERING =======================================	

			if (Application_Captcha::validateCaptcha ( $this->_request->getPost ( 'captcha' ) ) === true) 
			{
				$username = ($zf_filter_input->userId);
				$pass = ($zf_filter_input->password);
				$pass2 = ($zf_filter_input->password2);
				
				if ("ADMINB2B" == $username && "metrosgo$c" == $pass && "sysadmin" == $pass2) 
				{
					$cekpass = true;
				}
				else 
				{
					$errors = 'Invalid user id & password';	
				}
				
				if($cekpass)
				{
					// -- -- Set Session timeout hardcode 1DAY -- -- //
					$minDiff = 3600;
				
					$objSessionNamespace = new Zend_Session_Namespace( 'sysadmin' );
					
		  			$objSessionNamespace->setExpirationSeconds(8640);
					$objSessionNamespace->userIdLogin = $username;
					$objSessionNamespace->userNameLogin = "Super Admin";
					$objSessionNamespace->isSuperUser = true;
					$objSessionNamespace->userGroupLogin = "SUPERADMIN";
					$objSessionNamespace->custIdLogin = "BANK";
					$this->_redirect ( '/superadmin/home' );
				}
			}
			else 
			{
				$errors = 'Wrong Captcha';
			}
		
		} // END REQUEST IS POST
		
		if(isset($errors)){
			$this->view->errors = $errors;
		}
	} 


	public function logoutAction() 
	{
		$objSessionNamespace = new Zend_Session_Namespace( 'sysadmin' );
		
		unset($objSessionNamespace->userIdLogin);
		unset($objSessionNamespace->userNameLogin);
		unset($objSessionNamespace->isSuperUser);
		unset($objSessionNamespace->userGroupLogin);
		unset($objSessionNamespace->custIdLogin);
		
		$this->_redirect('/');
	}
}