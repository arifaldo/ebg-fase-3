<?php

require_once 'Zend/Controller/Action.php';

class binsetup_SuggestiondetailController extends binsetup_Model_Binsetup
{
  	protected $_moduleDB = 'CST';

	public function indexAction()
  	{
  		$this->_helper->layout()->setLayout('popup');
  		
  		$params = $this->_request->getParams();
  		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params)) $fullDesc = SGO_Helper_GeneralFunction::displayFullDesc($params);
  		else $fullDesc = null;

  	    $this->view->suggestionType = $this->_suggestType;
  		
  	    
  	    
  	    //cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
  	    $changes_id = strip_tags( trim($this->_getParam('changes_id')) );
  	      
	    $select = $this->_db->select()
		                     ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'))
		                     ->where("A.CHANGES_ID = ?",$changes_id)
		                     ->where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
                         // echo $select;die;
		$global_changes = $this->_db->fetchRow($select);
		 
		if(empty($global_changes))  $this->_redirect('/notification/invalid/index');
		//END cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
  	    
  	    
  		if(array_key_exists('changes_id', $params))
  		{  
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
  					// array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
					array('Db_RecordExists', array('table' => 'TEMP_CUSTOMER_BIN', 'field' => 'CHANGES_ID')),
					'messages' => array(
						'Can not be empty',
						'Invalid format',
						// 'Change id is not already exists in Master Global Changes',
						'Change id is not already exists in Temp Global Changes',
					                   ),
  				                    ),
  			                     );

  			
  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
  			
	  		if($zf_filter_input->isValid())
	  		{
	  		    $this->view->status_type = $this->_masterglobalstatus;
	  		    
	  			$changeId  = $zf_filter_input->changes_id;
	  			$resultdata = $this->getTempCustomer($changeId);
	  			$keyValue  = $resultdata['CUST_ID'];
	  			
          $resultCustData = $this->getTempCustName($resultdata['CHANGES_ID']);

	  			//suggest data
	  			$this->view->changes_id     = $resultdata['CHANGES_ID'];
	  			$this->view->changes_type   = $resultdata['CHANGES_TYPE'];
	  			$this->view->changes_status = $resultdata['CHANGES_STATUS'];
	  			$this->view->read_status    = $resultdata['READ_STATUS'];
	  			$this->view->created        = $resultdata['CREATED'];
	  			$this->view->created_by     = $resultdata['CREATED_BY'];
	  			
 				//temp customer data
          $this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
          $this->view->cust_name    = $resultCustData['CUST_NAME'];
          $this->view->cust_bin_status   = strtoupper($resultdata['CUST_BIN_STATUS']);
          $this->view->cust_bin   = strtoupper($resultdata['CUST_BIN']);
          
          
          $this->view->cust_created     = $resultdata['BIN_CREATED'];
          $this->view->cust_createdby   = $resultdata['BIN_CREATEDBY'];
          $this->view->cust_suggested   = $resultdata['BIN_SUGGESTED'];
          $this->view->cust_suggestedby = $resultdata['BIN_SUGGESTEDBY'];
          $this->view->cust_updated     = $resultdata['BIN_UPDATED'];
          $this->view->cust_updatedby   = $resultdata['BIN_UPDATEDBY'];
          $this->view->cust_lastsync   = $resultdata['BIN_LAST_SYNC'];

  		    $binTypeArr = array(
            '1' => 'eCollection',
            '2' => 'VA Debit'
          );

          $optStatus = array(
    //'' => '-- Please Select --',
            '1' => $this->language->_('No'),
            '0' => $this->language->_('Yes')
          );

          $optH2H = array(
    //'' => '-- Please Select --',
            '0' => $this->language->_('No'),
            '1' => $this->language->_('Yes')
          );

          $optSkip = array(
            '0' => $this->language->_('Yes'),
            '1' => $this->language->_('No')
          );

          //------------------------ecollection opt--------------------------------
          $optVA = array(
            '0' => $this->language->_('Dynamic Close'),
            '1' => $this->language->_('Static Open'),
            '2' => $this->language->_('Static Close')
          );

          $optExpTime = array(
            '0' => $this->language->_('Input Later'),
            '1' => $this->language->_('Predefined')
          );

          $optExTimeType = array(
            '1' => $this->language->_('Hour(s)'),
            '2' => $this->language->_('Day(s)')
          );

          $optPayment = array(
            '0' => $this->language->_('Yes'),
            '1' => $this->language->_('No'),
            '2' => $this->language->_('Over'),
          );

          $optBill = array(
            '0' => $this->language->_('Total'),
            '3' => $this->language->_('Selected'),
            '1' => $this->language->_('FIFO'),
            '2' => $this->language->_('LIFO')
          );

          //----------------------Va Debit opt-------------------------------

          $optRemainingCredit = array(
            '0' => $this->language->_('Accumulate'),
            '1' => $this->language->_('Auto Reset'),
          );

          $dayArr = array(
            '0' => 'Monday',
            '1' => 'Tuesday',
            '2' => 'Wednesday',
            '3' => 'Thursday',
            '4' => 'Friday',
            '5' => 'Saturday',
            '6' => 'Sunday',
          );

          $monthArr = array(
            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
          );

          $this->view->resultData = $resultdata;
        
          // print_r($resultdata);die;
          $this->view->ccy   = $resultdata['CCY'] != '' ? $resultdata['CCY'] : '-';
          $this->view->cif   = $resultdata['CUST_CIF'] != '' ? $resultdata['CUST_CIF'] : '-';
          $this->view->bank   = $resultdata['BANK_NAME'] != '' ? $resultdata['BANK_NAME'] : '-';
          $this->view->branch   = $resultdata['BRANCH_NAME'] != '' ? $resultdata['BRANCH_NAME'] : '-';
          $this->view->account_alias   = ($resultCustData['ACCT_ALIAS_NAME'] != '' || $resultCustData['ACCT_ALIAS_NAME'] != '&nbsp;') ? $resultCustData['ACCT_ALIAS_NAME'] : '-';
          $this->view->account   = $resultdata['ACCT_NO'] != '' ? $resultdata['ACCT_NO'] : '-';
          $this->view->account_name   = $resultdata['ACCT_NAME'] != '' ? $resultdata['ACCT_NAME'] : '-';
          $this->view->h2h   = $resultdata['HOSTTOHOST'] != '' ? $optH2H[$resultdata['HOSTTOHOST']] : 'No';
          $this->view->url_inquiry   = $resultdata['URL_INQUIRY'] != '' ? $resultdata['URL_INQUIRY'] : '-';            
          $this->view->url_payment   = $resultdata['URL_PAYMENT'] != '' ? $resultdata['URL_PAYMENT'] : '-';    
 
          $this->view->bintype        = $resultdata['BIN_TYPE'] != '' ? $binTypeArr[$resultdata['BIN_TYPE']] : '-';
          $this->view->allowskip        = $resultdata['SKIP_ERROR'] != '' ? $optSkip[$resultdata['SKIP_ERROR']] : '-';

          //ecollection
          $this->view->va_type   = $resultdata['VA_TYPE'] != '' ? $optVA[$resultdata['VA_TYPE']] : '-';
          $this->view->exp_time   = $resultdata['EXP_TIME'] != '' ? $optExpTime[$resultdata['EXP_TIME']] : '-';

          if ($resultdata['EXP_TIME'] == '1') {
            $this->view->exp_time .= ' ('.$resultdata['EX_TIME'].' '.$optExTimeType[$resultdata['EX_TIME_TYPE']].')';
          }

          $this->view->partial_type   = $resultdata['PARTIAL_TYPE'] != '' ? $optPayment[$resultdata['PARTIAL_TYPE']] : '-';
          $this->view->bill_method   =  $resultdata['BILLING_TYPE'] != '' ? $optBill[$resultdata['BILLING_TYPE']] : '-';
          $this->view->vp_max   = $resultdata['VP_MAX'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['VP_MAX']) : '-';
          $this->view->vp_min   = $resultdata['VP_MIN'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['VP_MIN']) : '-';
          $this->view->seller_fee   = $resultdata['SELLER_FEE'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['SELLER_FEE']) : '-';
          $this->view->buyer_fee   = $resultdata['BUYER_FEE'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['BUYER_FEE']) : '-';

          //Va Debit
          $this->view->va_credit_max   = $resultdata['VA_CREDIT_MAX'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['VA_CREDIT_MAX']) : '-';
          $this->view->va_daily_limit   = $resultdata['VA_DAILY_LIMIT'] != '' ? 'IDR '.Application_Helper_General::displayMoney($resultdata['VA_DAILY_LIMIT']) : '-';
          $this->view->remainingCredit   = $resultdata['REMAINING_AVAILABLE_CREDIT'] != '' ? $optRemainingCredit[$resultdata['REMAINING_AVAILABLE_CREDIT']] : '-';

          if ($resultdata['BIN_TYPE'] == '2' && ($resultdata['HOSTTOHOST'] == '0' || $resultdata['HOSTTOHOST'] == '')) {
            if ($resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
              if ($resultdata['EX_TIME_TYPE'] == '2') {
                $exTime = 'Weekly every '. $dayArr[$resultdata['EX_TIME']];
              }
              else if ($resultdata['EX_TIME_TYPE'] == '3') {
                $exTime = 'Monthly on '. $resultdata['EX_TIME'];
              }
              else if ($resultdata['EX_TIME_TYPE'] == '4') {
                $explodeTime = explode('_', $resultdata['EX_TIME']);

                $exTime = 'Yearly every '. $explodeTime[0].' '.$monthArr[$explodeTime[1]];
              }

              if ($resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
                $this->view->remainingCredit .= ' ('.$exTime.')';
              }
            }
          }

  		    //master customer data
  		    $m_resultdata = $this->getCustomer(strtoupper($resultdata['CUST_ID']),$resultdata['CUST_BIN']);

          $this->view->m_resultData = $m_resultdata;
  		    
  		    $this->view->m_cust_id      	= strtoupper($m_resultdata['CUST_ID']);
   			  $this->view->m_cust_name    	= $m_resultdata['CUST_NAME'];
  		    $this->view->m_cust_bin    		= $m_resultdata['CUST_BIN'];
  		    $this->view->m_cust_status   	= $m_resultdata['CUST_BIN_STATUS'];
  		    
  		    $this->view->m_cust_created     = $m_resultdata['BIN_CREATED'];
  		    $this->view->m_cust_createdby   = $m_resultdata['BIN_CREATEDBY'];
  		    $this->view->m_cust_suggested   = $m_resultdata['BIN_SUGGESTED'];
  		    $this->view->m_cust_suggestedby = $m_resultdata['BIN_SUGGESTEDBY'];
  		    $this->view->m_cust_updated     = $m_resultdata['BIN_UPDATED'];
  		    $this->view->m_cust_updatedby   = $m_resultdata['BIN_UPDATEDBY'];


          $this->view->m_ccy   = $m_resultdata['CCY'] != '' ? $m_resultdata['CCY'] : '-';
          $this->view->m_cif   = $m_resultdata['CUST_CIF'] != '' ? $m_resultdata['CUST_CIF'] : '-';
          $this->view->m_bank   = $m_resultdata['BANK_NAME'] != '' ? $m_resultdata['BANK_NAME'] : '-';
          $this->view->m_branch   = $m_resultdata['BRANCH_NAME'] != '' ? $m_resultdata['BRANCH_NAME'] : '-';
          $this->view->m_account_alias   = ($m_resultdata['ACCT_ALIAS_NAME'] || $m_resultdata['ACCT_ALIAS_NAME'] != '&nbsp;') ? $m_resultdata['ACCT_ALIAS_NAME'] : '-';
          $this->view->m_account   = $m_resultdata['ACCT_NO'] != '' ? $m_resultdata['ACCT_NO'] : '-';
          $this->view->m_account_name   = $m_resultdata['ACCT_NAME'] != '' ? $m_resultdata['ACCT_NAME'] : '-';
          $this->view->m_h2h   = $m_resultdata['HOSTTOHOST'] != '' ? $optH2H[$m_resultdata['HOSTTOHOST']] : 'No';
          $this->view->m_url_inquiry   = $m_resultdata['URL_INQUIRY'] != '' ? $m_resultdata['URL_INQUIRY'] : '-';            
          $this->view->m_url_payment   = $m_resultdata['URL_PAYMENT'] != '' ? $m_resultdata['URL_PAYMENT'] : '-';    
 
          $this->view->m_bintype        = $m_resultdata['BIN_TYPE'] != '' ? $binTypeArr[$m_resultdata['BIN_TYPE']] : '-';
          $this->view->m_allowskip        = $m_resultdata['SKIP_ERROR'] != '' ? $optSkip[$m_resultdata['SKIP_ERROR']] : '-';

          //ecollection
          $this->view->m_va_type   = $m_resultdata['VA_TYPE'] != '' ? $optVA[$m_resultdata['VA_TYPE']] : '-';
          $this->view->m_exp_time   = $m_resultdata['EXP_TIME'] != '' ? $optExpTime[$m_resultdata['EXP_TIME']] : '-';

          if ($m_resultdata['EXP_TIME'] == '1') {
            $this->view->m_exp_time .= ' ('.$m_resultdata['EX_TIME'].' '.$optExTimeType[$m_resultdata['EX_TIME_TYPE']].')';
          }

          $this->view->m_partial_type   = $m_resultdata['PARTIAL_TYPE'] != '' ? $optPayment[$m_resultdata['PARTIAL_TYPE']] : '-';
          $this->view->m_bill_method   =  $m_resultdata['BILLING_TYPE'] != '' ? $optBill[$m_resultdata['BILLING_TYPE']] : '-';
          $this->view->m_vp_max   = $m_resultdata['VP_MAX'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['VP_MAX']) : '-';
          $this->view->m_vp_min   = $m_resultdata['VP_MIN'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['VP_MIN']) : '-';
          $this->view->m_seller_fee   = $m_resultdata['SELLER_FEE'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['SELLER_FEE']) : '-';
          $this->view->m_buyer_fee   = $m_resultdata['BUYER_FEE'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['BUYER_FEE']) : '-';

          //Va Debit
          $this->view->m_va_credit_max   = $m_resultdata['VA_CREDIT_MAX'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['VA_CREDIT_MAX']) : '-';
          $this->view->m_va_daily_limit   = $m_resultdata['VA_DAILY_LIMIT'] != '' ? 'IDR '.Application_Helper_General::displayMoney($m_resultdata['VA_DAILY_LIMIT']) : '-';
          $this->view->m_remainingCredit   = $m_resultdata['REMAINING_AVAILABLE_CREDIT'] != '' ? $optRemainingCredit[$m_resultdata['REMAINING_AVAILABLE_CREDIT']] : '-';

          if ($m_resultdata['BIN_TYPE'] == '2' && ($m_resultdata['HOSTTOHOST'] == '0' || $m_resultdata['HOSTTOHOST'] == '')) {
            if ($m_resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
              if ($m_resultdata['EX_TIME_TYPE'] == '2') {
                $exTime = 'Weekly every '. $dayArr[$m_resultdata['EX_TIME']];
              }
              else if ($m_resultdata['EX_TIME_TYPE'] == '3') {
                $exTime = 'Monthly on '. $m_resultdata['EX_TIME'];
              }
              else if ($m_resultdata['EX_TIME_TYPE'] == '4') {
                $explodeTime = explode('_', $m_resultdata['EX_TIME']);

                $exTime = 'Yearly every '. $explodeTime[0].' '.$monthArr[$explodeTime[1]];
              }

              $this->view->m_remainingCredit .= ' ('.$exTime.')';
            }
          }
	  		}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			$errorRemark = SGO_Helper_GeneralFunction::displayErrorRemark($errors);
	  			Application_Helper_General::writeLog('BNLS','View customer BIN changes list');

	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
      			$this->_redirect('/popuperror/index/index');
	  		}
  		}
  		else
  		{
  			$errorRemark = 'Changes Id not found';
			//Application_Helper_General::writeLog('CCCL','');

	  		$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
      		$this->_redirect('/popuperror/index/index');
  		}
  		if(!$this->_request->isPost()){
  		Application_Helper_General::writeLog('BNLS','View customer BIN changes list');
  		}
  		
  		
	}
}



