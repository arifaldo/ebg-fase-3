<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
class accountaggregation_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$model = new accountaggregation_Model_Accountaggregation();

		$fields1 = array(
					'acct'  => array('field' => 'ACCT_NO',
										   'label' => $this->language->_('Account No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'ccy'   => array('field'    => 'CCY_ID',
										  'label'    => $this->language->_('CCY'),
										  'sortable' => true),
					'available'   => array('field'    => 'AVAILABLE_BALANCE',
										  'label'    => $this->language->_('Available Balance'),
										  'sortable' => true),
		);

		$fields2 = array(
					'depo_acct'  => array('field' => '',
										   'label' => $this->language->_('Deposit No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'depo_ccy'   => array('field'    => '',
										  'label'    => $this->language->_('CCY'),
										  'sortable' => true),
					'amount'   => array('field'    => '',
										  'label'    => $this->language->_('Available Balance'),
										  'sortable' => true),
		);

		$fields3 = array(
					'cardno'  => array('field' => '',
										   'label' => $this->language->_('Card No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'creditlimit'   => array('field'    => '',
										  'label'    => $this->language->_('Credit Limit'),
										  'sortable' => true),
					'closingbal'   => array('field'    => '',
										  'label'    => $this->language->_('Closing Balance'),
										  'sortable' => true),
		);

		

		$fields4 = array(
					'loan_acct'  => array('field' => '',
										   'label' => $this->language->_('Account No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'plafond'   => array('field'    => '',
										  'label'    => $this->language->_('Plafond'),
										  'sortable' => true),
					'outstanding'   => array('field'    => '',
										  'label'    => $this->language->_('Outstanding'),
										  'sortable' => true),
		);

		$sortByParam  = $this->_getParam('sortby');
		$sortDirParam = $this->_getParam('sortdir');
		
		
		//check param for personal table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields1)))){
			$sortBy1  =  $fields1[$sortByParam]['field'];
			$sortDir1 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy1  = $fields1[key($fields1)]['field'];
			$sortDir1 = 'asc';
		}
		
		$sorting1 = $sortBy1.' '.$sortDir1;
// 		$temp = $model->getUserAccount(array('userId'=>$this->_userIdLogin),$sorting1);

		$temp = $this->_db->fetchAll(
				$this->_db->select()
				->from(array('A' => 'M_CUSTOMER_ACCT'))
				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				->where("A.ACCT_STATUS = 1")
				->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
				->where("C.MAXLIMIT > 0")
				->order('B.GROUP_NAME DESC')
				->order('A.ORDER_NO ASC')
		);
		
		$isGroupPersonal = false;
		if(count($temp))
		{
			foreach($temp as $row)
			{
				if($row['GROUP_ID'])
				{
					$isGroupPersonal = true;
				}
			}
		}
		
		$tempDeposit = $this->_db->fetchAll(
				$this->_db->select()
				->from(array('A' => 'M_CUSTOMER_ACCT'))
				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				->where("A.ACCT_STATUS = 1")
				->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
				->where("C.MAXLIMIT > 0")
				->order('B.GROUP_NAME DESC')
				->order('A.ORDER_NO ASC')
		);
		
		$isGroupDeposit = false;
		if(count($tempDeposit))
		{
			foreach($tempDeposit as $row)
			{
				if($row['GROUP_ID'])
				{
					$isGroupDeposit = true;
				}
			}
		}
		
		$tempLoan = $this->_db->fetchAll(
				$this->_db->select()
				->from(array('A' => 'M_CUSTOMER_ACCT'))
				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				->where("A.ACCT_STATUS = 1")
				->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
				->where("C.MAXLIMIT > 0")
				->order('B.GROUP_NAME DESC')
				->order('A.ORDER_NO ASC')
		);
		
// 		print_r($tempLoan);die;
		$isGroupLoan = false;
		if(count($tempLoan))
		{
			foreach($tempLoan as $row)
			{
				if($row['GROUP_ID'])
				{
					$isGroupLoan = true;
				}
			}
		}
		
// 		if (count($temp) > 0) 
// 		{
// 			$datapers = array();
// 			foreach ( $temp as $key=>$row ) {
// 				$Account = new Account($row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']));
// 				$Account->setFlag(false);
// 				$Account->checkBalance();
// 				$datapers[$key]['ACCT_NO'] = $row['ACCT_NO'];
// 				$datapers[$key]['CCY_ID'] = $row['CCY_ID'];
// 				$datapers[$key]['PRODUCT_NAME'] = $row['PRODUCT_NAME'];
				
// 				$datapers[$key]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
// 			}
// 		}

		//check param for deposit table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields2)))){
			$sortBy2  =  $fields2[$sortByParam]['field'];
			$sortDir2 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy2  = $fields2[key($fields2)]['field'];
			$sortDir2 = 'asc';
		}

		//any query + logic for deposit

		//check param for cc table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields3)))){
			$sortBy3  =  $fields3[$sortByParam]['field'];
			$sortDir3 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy3  = $fields3[key($fields3)]['field'];
			$sortDir3 = 'asc';
		}

		//any query + logic for cc

		//check param for loan table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields4)))){
			$sortBy4  =  $fields4[$sortByParam]['field'];
			$sortDir4 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy4  = $fields4[key($fields4)]['field'];
			$sortDir4 = 'asc';
		}

		//any query + logic for deposit




		$logDesc = 'Viewing';

		
		Application_Helper_General::writeLog('BAIQ',$logDesc);
// 		print_r($temp);die;
		$this->view->custId = $this->_custIdLogin;
		$this->view->isGroupPersonal = $isGroupPersonal;
		$this->view->isGroupDeposit = $isGroupDeposit;
		$this->view->isGroupLoan = $isGroupLoan;
		
		
		$this->view->userId = $this->_userIdLogin;
		$this->view->personal = $temp;
		$this->view->creditcard = $datacc;
		
		
// 		$this->view->deposit = $tempLoan;
		$this->view->deposit = $tempDeposit;
		$this->view->loan = $tempLoan;
		$this->view->fields1 = $fields1; //personal
		$this->view->fields2 = $fields2; //deposit
		$this->view->fields3 = $fields3; //credit card
		$this->view->fields4 = $fields4; //loan

		$this->view->sortBy1 = $sortBy1;
		$this->view->sortBy2 = $sortBy2;
		$this->view->sortBy3 = $sortBy3;
		$this->view->sortBy4 = $sortBy4;

		$this->view->sortDir1 = $sortDir1;
		$this->view->sortDir2 = $sortDir2;
		$this->view->sortDir3 = $sortDir3;
		$this->view->sortDir4 = $sortDir4;
	}
	
	
}
