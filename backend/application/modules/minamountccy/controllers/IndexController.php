<?php

require_once 'Zend/Controller/Action.php';

class Minamountccy_IndexController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$fields = array(
			'available'      => array(
				'field' => 'CCY_STATUS',
				'label' => $this->language->_('Available'),
				'sortable' => false
			),
			'currency'           => array(
				'field' => 'CCY_ID',
				'label' => $this->language->_('Currency'),
				'sortable' => false
			),
			'currency_name'  => array(
				'field' => 'CCY_DESCRIPTION',
				'label' => $this->language->_('Currency Name'),
				'sortable' => false
			),
			'min_transfer_amount'     => array(
				'field' => '',
				'label' => $this->language->_('Minimum Transfer Amount'),
				'sortable' => false
			),
		);

		$submitArr = array(
			'submit' => array('StripTags', 'StringTrim'),
		);

		$zf_submit = new Zend_Filter_Input($submitArr, array(), $this->_request->getParams());
		$submit = $zf_submit->getEscaped('submit');
		$select2 = $this->_db->select()
			->from(array('A' => 'M_CURRENCY'), array())
			->joinleft(
				array('B' => 'M_MINAMT_CCY'),
				'A.CCY_ID = B.CCY_ID',
				array('B.CCY_STATUS', 'A.CCY_DESCRIPTION', 'A.CCY_ID', 'A.CCY_NUM', 'B.MIN_TX_AMT', '')
			);
		//		$select = $this->_db->select()
		//					        ->from(array('X' => 'M_MINAMT_CCY'));
		$result = $select2->query()->FetchAll();


		$cek = $this->_db->select()
			->from('TEMP_MINAMT_CCY')
			->query()->fetchall();

		if (!$cek) {
			$error = '';
			if ($submit == TRUE && $this->view->hasPrivilege('CRUD')) {
				//Zend_Debug::dump($this->_getAllParams());
				$this->_db->beginTransaction();
				try {
					$temp = 0;
					//$info = 'Changes Minimum Amount Currency';
					$info = $this->language->_('Changes Minimum Amount Currency');
					$change_id = $this->suggestionWaitingApproval('Minimum Amount and Currency Available', $info, $this->_changeType['code']['edit'], null, 'M_MINAMT_CCY', 'TEMP_MINAMT_CCY', 'Currency Setting', 'Perubahan Data', null, null, null, '');
					foreach ($result as $row) {
						$c = 'c' . $row['CCY_ID'];
						$t = 't' . $row['CCY_ID'];
						$amt = Application_Helper_General::convertDisplayMoney($this->_getParam($t));
						$min_amt = str_replace('.', '', $amt);
						$cek_angka = (Zend_Validate::is($min_amt, 'Digits')) ? true : false;
						$id = $row['CCY_ID'];
						$checkbox = ($this->_getParam($c)) ? "A" : "I";

						if ($cek_angka || $this->_getParam($t) != "0.00" || $this->_getParam($t) != "") {
							//KALO DICENTANG
							if ($checkbox == "A") { // DATA BENER //
								if ($this->_getParam($t) != "0.00" && $this->_getParam($t) != "" && $min_amt <= 999999999999999 && $min_amt > 0) {
									$data = array(
										'CHANGES_ID' => $change_id,
										'CCY_ID' => $row['CCY_ID'],
										'CCY_NUM' => $row['CCY_NUM'],
										'DESCRIPTION' => $row['CCY_DESCRIPTION'],
										'MIN_TX_AMT' => $amt,
									);

									$this->_db->insert('TEMP_MINAMT_CCY', $data);
									$temp += 1;

									$xxx = 'x' . $id;
									$centang = 'centang' . $id;
									$this->view->$centang = $checkbox;

									$tamt = 't' . $id;
									$this->view->$tamt = $amt;
								} else {
									// CENTANG TAPI VALUE SALAH //
									$id = $row['CCY_ID'];
									$docErr = 'Error in processing form values. Please correct values and re-submit.';
									$this->view->report_msg = $docErr;

									$xxx = 'x' . $id;
									$centang = 'centang' . $id;
									$this->view->$centang = $checkbox;

									$tamt = 't' . $id;
									$this->view->$tamt = $amt;
									if ($min_amt <= 0)
										$err = 'Invalid Amount. Amount must be greater than 0.00';
									else if ($min_amt > 999999999999999)
										$err = 'Invalid Amount. Amount must be lower than 9,999,999,999,999.99';
									else
										$err = 'Invalid Amount. Amount must be numeric';

									$this->view->$xxx = $err;
									$error = true;
								}
							}
							// KALO TIDAK DICENTANG
							else if ($checkbox == "I") {
								if ($this->_getParam($t) == "0.00" || $this->_getParam($t) == "") {
									// TIDAK DICENTANG DAN VALUE KOSONG //
									$xxx = 'x' . $id;
									$centang = 'centang' . $id;
									$this->view->$centang = $checkbox;

									$tamt = 't' . $id;
									$this->view->$tamt = $amt;
								} else if ($this->_getParam($t) != "0.00" && $this->_getParam($t) != "" && $min_amt <= 999999999999999 && $min_amt > 0) {
									// TIDAK DICENTANG DAN VALUE BENER //
									$id = $row['CCY_ID'];
									$docErr = 'Error in processing form values. Please correct values and re-submit.';
									$this->view->report_msg = $docErr;

									$xxx = 'x' . $id;
									$centang = 'centang' . $id;
									$this->view->$centang = $checkbox;

									$tamt = 't' . $id;
									$this->view->$tamt = $amt;

									$err = 'Please check selection';
									$this->view->$xxx = $err;
									$error = true;
								} else {
									$id = $row['CCY_ID'];
									$docErr = 'Error in processing form values. Please correct values and re-submit.';
									$this->view->report_msg = $docErr;

									$xxx = 'x' . $id;
									$centang = 'centang' . $id;
									$this->view->$centang = $checkbox;

									$tamt = 't' . $id;
									$this->view->$tamt = $amt;

									if ($min_amt <= 0)
										$err = 'Invalid Amount. Amount must be greater than 0.00';
									else if ($min_amt > 999999999999999)
										$err = 'Invalid Amount. Amount must be lower than 9,999,999,999,999.99';
									else
										$err = 'Invalid Amount. Amount must be numeric';

									$this->view->$xxx = $err;
									$error = true;
								}
							}
						} else {
							$id = $row['CCY_ID'];
							$docErr = 'Error in processing form values. Please correct values and re-submit.';
							$this->view->report_msg = $docErr;
							$xxx = 'x' . $id;
							$err = 'Invalid Amount. Amount must be numeric';
							$this->view->$xxx = $err;
							$centang = 'centang' . $id;
							$this->view->$centang = $checkbox;

							$error = true;
						}
					}
					if (!$error && $temp > 0) {
						Application_Helper_General::writeLog('CRUD', 'Update Minimum Amount and Currency Available');
						$this->_db->commit();
						$this->view->success = true;
						$msg = 'Currency Amount Change Request Saved';
						$this->view->report_msg = $msg;
					}
				} catch (Exception $e) {
					$this->_db->rollBack();
				}
			}
		} else {
			if ($submit == 'Submit') {
				foreach ($result as $row) {
					$c = 'c' . $row['CCY_ID'];
					$t = 't' . $row['CCY_ID'];
					$id = $row['CCY_ID'];

					$amt = Application_Helper_General::convertDisplayMoney($this->_getParam($t));
					$checkbox = ($this->_getParam($c)) ? "A" : "I";

					$centang = 'centang' . $id;
					$this->view->$centang = $checkbox;

					$tamt = 't' . $id;
					$this->view->$tamt = $amt;
				}
			}
			$this->view->error = true;
			$docErr = ('No changes allowed for this record while awaiting approval for previous change.');
			$this->view->report_msg = $docErr;
		}
		$this->view->result = $result;
		$this->view->fields = $fields;
		if (!$this->_request->isPost())
			Application_Helper_General::writeLog('CRLS', 'View Minimum Amount and Currency Available');
	}
}
