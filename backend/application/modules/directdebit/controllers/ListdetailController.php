<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';

class directdebit_ListdetailController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
		$password = $sessionNamespace->token; 
		$this->view->token = $sessionNamespace->token;
		$AESMYSQL = new Crypt_AESMYSQL();
		$NUM_ID      = urldecode($this->_getParam('numid'));
		$DIRECT_ID = $AESMYSQL->decrypt($NUM_ID, $password);

		
		if($DIRECT_ID){

			$select = $this->_db->select()
					 ->from(array('C' => 'T_DIRECTDEBIT'),array('*','company'	=> new Zend_Db_Expr("CONCAT(COMP_NAME , ' (' , COMP_ID , ')  ' )")))
					 ->joinleft(array('X'=>'M_DOMESTIC_BANK_TABLE'), 'X.SWIFT_CODE = C.DEBIT_BANK',array('BANK_NAME'))
					 ->where('DIRECT_ID = ?', $DIRECT_ID)
					 ->query()->FetchAll();

			// echo "<pre>";
			// var_dump($DIRECT_ID);
			// var_dump($select);
			if($select){
				$this->view->id = $select[0]['DIRECT_ID'];
				$this->view->COMP_NAME = $select[0]['COMP_NAME'];
				$this->view->COMP_ID = $select[0]['COMP_ID'];
				$this->view->CLIENT_REFF = $select[0]['CLIENT_REFF'];
				$this->view->CLIENT_NAME = $select[0]['CLIENT_NAME'];
				$this->view->DEBIT_ACCT = $select[0]['DEBIT_ACCT'];
				$this->view->ACCT_NAME = $select[0]['ACCT_NAME'];
				$this->view->DEBIT_BANK = $select[0]['DEBIT_BANK'];
				$this->view->DIR_STATUS = $select[0]['DIR_STATUS'];
				$this->view->DIR_CCY = $select[0]['DIR_CCY'];
				
				if(!empty($select['0']['DOC_ID'])){
					$selectud = $this->_db->select()
							->distinct()
							->from(	
									array('TFS'=>'T_FILE_SUBMIT'),
									array(
											'FILE_ID'			=>'TFS.FILE_ID',
											'FILE_NAME'			=>'TFS.FILE_NAME',
											'FILE_DESCRIPTION'	=>'TFS.FILE_DESCRIPTION',
											'DOCUMENT_NUMBER'	=>'TFS.DOCUMENT_NUMBER',											
											'FILE_UPLOADED_TIME'=>'TFS.FILE_UPLOADED_TIME',
									))
							->where('TFS.FILE_ID = ?',$select['0']['DOC_ID']);
							//echo $selectud;
							
						$dataud = $this->_db->fetchAll($selectud);
						$this->view->dataud = $dataud;
				}

				$selectHistory = $this->_db->select()
								->from(array('H' => 'T_PSLIP_HISTORY'), array('*'))
								->where('H.PS_NUMBER = ?', $select['0']['CLIENT_REFF'] )
								->order('DATE_TIME DESC');

				$dataHistory = $this->_db->fetchAll($selectHistory);
				$this->view->dataHistory = $dataHistory;
			
			}
		}

		$select3 = $this->_db->select()
                  ->from(array('A' => 'T_FILE_SUBMIT')); 
        
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;  

	    $AESMYSQL = new Crypt_AESMYSQL();
	    $PS_NUMBER      = urldecode($this->_getParam('FILE_ID'));
	    $FILE_ID = $AESMYSQL->decrypt($PS_NUMBER, $password);

		if($FILE_ID)
		{	


			$select3->where('FILE_ID =?',$FILE_ID);
			$data = $this->_db->fetchRow($select3);
			
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['FILE_NAME'],$attahmentDestination.$data['FILE_SYSNAME']);
			
			$updateArr = array();
			$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED']+1;
			$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;
			
			$whereArr = array('FILE_ID = ?'=>$FILE_ID);
			
			$fileupdate = $this->_db->update('T_FILE_SUBMIT',$updateArr,$whereArr);
			//echo "file downloaded: ".$data['FILE_DOWNLOADED'];
			Application_Helper_General::writeLog('VDEL','Download File Underlying Document '.$data['FILE_NAME']);
		}
		else{
			Application_Helper_General::writeLog('VDEL','View File Underlying Document');
		}
    }
}