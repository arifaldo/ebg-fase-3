<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class Beneficiaryuserlist_IndexController extends Application_Main
{

	public function initController()
	{
		$companyCode = $this->_db->fetchAll(
					$this->_db->select()
					   ->from('M_CUSTOMER',array('CUST_ID'))
					   );

		$listCompCode = array(''=>'-- '.$this->language->_('Any Value').' --');
		$listCompCode += Application_Helper_Array::listArray($companyCode,'CUST_ID','CUST_ID');
		$this->view->listcompcode = $listCompCode;

		$listCcy = array(''=>'-- '.$this->language->_('Any Value').' --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->listccy = $listCcy;
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$fields = array(
						'comp_code'  => array('field' => 'A.CUST_ID',
											   'label' => $this->language->_('Company Code'),
											   'sortable' => true),
						'comp_name'  => array('field' => 'CUST_NAME',
											   'label' => $this->language->_('Company Name'),
											   'sortable' => true),
						'user_id'  => array('field' => 'A.USER_ID',
											   'label' => $this->language->_('User ID'),
											   'sortable' => true),
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => $this->language->_('Beneficiary Name'),
											   'sortable' => true),
 						'bank_name' => array('field' => 'BANK_NAME',
 												'label' => 'Bank Name',
 												'sortable' => true),
						'ccy'   => array('field'    => 'CURR_CODE',
											  'label'    => $this->language->_('Currency'),
											  'sortable' => true)
				);

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','user_id');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'COMP_CODE' 	=> array('StringTrim','StripTags','StringToUpper'),
							'COMP_NAME'    	=> array('StringTrim','StripTags','StringToUpper'),
							'USER_ID'    	=> array('StringTrim','StripTags','StringToUpper'),
							'BENEF_ACCT'    => array('StringTrim','StripTags'),
							'BENEF_NAME'    => array('StringTrim','StripTags','StringToUpper'),
							'CCY'    		=> array('StringTrim','StripTags','StringToUpper'),
		);

		$filterlist = array("COMP_CODE","COMP_NAME","USER_ID","BENEF_ACCT","BENEF_NAME","CCY");

		$this->view->filterlist = $filterlist;

		 $dataParam = array("COMP_CODE","COMP_NAME","USER_ID","BENEF_ACCT","BENEF_NAME","CCY");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}


		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$select = $this->_db->select()
					   ->from(array('A' => 'M_BENEFICIARY_USER'),array('CUST_ID','C.CUST_NAME','USER_ID','B.BENEFICIARY_ACCOUNT','B.BENEFICIARY_NAME','B.BENEFICIARY_ALIAS','B.CURR_CODE','B.BANK_NAME'))
					   ->joinLeft(array('B' => 'M_BENEFICIARY'),'A.BENEFICIARY_ID = B.BENEFICIARY_ID',array())
					   ->joinLeft(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array());

	    $select->order($sortBy.' '.$sortDir);

		if($filter == TRUE)
		{
			$fCompCode = $zf_filter->getEscaped('COMP_CODE');
			$fCompName = $zf_filter->getEscaped('COMP_NAME');
			$fUser = $zf_filter->getEscaped('USER_ID');
			$fAcct = $zf_filter->getEscaped('BENEF_ACCT');
			$fName = $zf_filter->getEscaped('BENEF_NAME');
			$fCcy = $zf_filter->getEscaped('CCY');

	        if($fCompCode)$select->where('UPPER(B.CUST_ID) = '.$this->_db->quote(strtoupper($fCompCode)));
	        if($fCompName)$select->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fCompName).'%'));
	        if($fUser)$select->where('UPPER(USER_ID) LIKE '.$this->_db->quote('%'.strtoupper($fUser).'%'));
	        if($fAcct)$select->where('BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($fAcct).'%'));
	        if($fName)$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));
	        if($fCcy)$select->where('UPPER(CURR_CODE) = '.$this->_db->quote(strtoupper($fCcy)));

			$this->view->comp_code = $fCompCode;
			$this->view->comp_name = $fCompName;
			$this->view->user_id = $fUser;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->ccy = $fCcy;

		}
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			//get label from header table
			$arrLabel = Application_Helper_Array::simpleArray($fields, "label");

			$data = $this->_db->fetchAll($select);
			//remove column BENEFICIARY_ALIAS
			foreach($data as $key => $val){
				unset($data[$key]['BENEFICIARY_ALIAS']);
			}

			if($csv)
			{
				Application_Helper_General::writeLog('RPBU','Download CSV Beneficiary User List Report');
				$this->_helper->download->csv($arrLabel,$data,null,'Beneficiary User List');
			}
			else if($pdf)
			{
				Application_Helper_General::writeLog('RPBU','Download PDF Beneficiary User List Report');
				$this->_helper->download->pdf($arrLabel,$data,null,'Beneficiary User List');
			}
			else if($this->_request->getParam('print') == 1){
				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Beneficiary User List', 'data_header' => $fields));
			}
		}
		$this->paging($select);
		
		$setting = new Settings();
	    $MasterBankName = $setting->getSetting('master_bank_name');
		$this->view->bankname = $MasterBankName;

		$this->view->fields = $fields;
		$this->view->filter = $filter;

		Application_Helper_General::writeLog('RPBU','View Beneficiary User List Report');
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }
	}
}
