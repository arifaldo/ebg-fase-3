<?php

class Emailconf_EditController extends Application_Main {
	
	protected $_moduleDB  = 'ETS';
	
	public function indexAction() {
		$error_remark = null;
		$data_settingid = "'tmpl_new_cust_act_print','tmpl_new_cust_act_email','tmpl_cust_user_pass_print','tmpl_cust_user_pass_email',
							'tmpl_backend_usr_act_print','tmpl_backend_usr_act_email','tmpl_backend_user_reset_pass_email','tmpl_backend_user_reset_pass_print'";
       
		$isPendingAvailable = $this->_db->fetchAll(
									$this->_db->select()->from (
									 	array ('TEMP_SETTING'), 
									 	array ('SETTING_VALUE'))
									 	->where('SETTING_ID IN ('.$data_settingid.')')
										//->where('SETTING_ID = ?','Be_email')
							 );
		
				 
		if($isPendingAvailable==true)
		{
			$error_remark = $this->getErrorRemark('03','Template Email');
			//insert log
			try {
	  			$this->_db->beginTransaction();
	  			if($this->_request->isPost()){
	    			$action = 'E'; $fulldesc = $this->displayFullDesc($this->_request->getPost());
	  			}else{ $action = 'V'; $fulldesc = null; }
	  			$this->backendLog($action,$this->_moduleDB,$this->_moduleDB,$fulldesc,$error_remark);
	  			$this->_db->commit();
			}
    		catch(Exception $e){
 	  			$this->_db->rollBack();
	  			Application_Helper_GeneralLog::technicalLog($e);
			}
			
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      		$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      		$this->_redirect($this->_backURL);
		}

	
		if ($this->_request->isPost ()) {
			$filters = array('welcometext' => array('StringTrim', 'StripTags'));			
			$validators =  array('welcometext' => array('allowEmpty'=>true, array('StringLength', array('min' => 0, 'max' => 5000)), 'messages' => $this->getErrorRemark('04',$this->_moduleDB)));
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getPost(),$this->_optionsValidator);
		
			if ($zf_filter_input->isValid ()) {	
						try {
							$this->_db->beginTransaction();
        										
							$arrayTGlobalChanges = array (	'CHANGES_FLAG' => 'B', 
													'CUST_ID' => 'BANK',
													'DISPLAY_TABLENAME' => 'Backend Email Template', 
													'CHANGES_INFORMATION' => '',
													'CHANGES_TYPE' => 'E',
													'CHANGES_STATUS' => 'WA', 
													'CHANGES_REASON' => '',
													'MAIN_TABLENAME' => 'M_SETTING',
													'TEMP_TABLENAME' => 'TEMP_SETTING',
													'KEY_FIELD' => 'MODULE_ID',
													'KEY_VALUE' => $this->_moduleDB , 
													'CREATED' => new Zend_Db_Expr("now()"),
													'CREATED_BY' => $this->_userIdLogin,
													'LASTUPDATED' => new Zend_Db_Expr("now()"),
													'MODULE' => 'emailtemplatesetting'
												);
							$this->_db->insert( 'T_GLOBAL_CHANGES', $arrayTGlobalChanges );							
									
							$latestId = $this->_db->fetchOne('SELECT @@identity AS LASTID');									
							
							$data_settingid = "'tmpl_new_cust_act_print','tmpl_new_cust_act_email','tmpl_cust_user_pass_print','tmpl_cust_user_pass_email'";
							
							$arrayTempSetting = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'tmpl_new_cust_act_print',
													'SETTING_VALUE' => $this->_request->getParam ( 'text1' ),
													'MODULE_ID' => $this->_moduleDB										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting );
							
							$arrayTempSetting2 = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'tmpl_new_cust_act_email',
													'SETTING_VALUE' => $this->_request->getParam ( 'text2' ),
													'MODULE_ID' => $this->_moduleDB										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting2 );
							
							$arrayTempSetting3 = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'tmpl_cust_user_pass_print',
													'SETTING_VALUE' => $this->_request->getParam ( 'text3' ),
													'MODULE_ID' => $this->_moduleDB										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting3);
							
							$arrayTempSetting4 = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'tmpl_cust_user_pass_email',
													'SETTING_VALUE' => $this->_request->getParam ( 'text4' ),
													'MODULE_ID' => $this->_moduleDB										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting4 );
							
							//exit();
							// back end insert DB
							
								$data_settingid = "'tmpl_new_cust_act_print','tmpl_new_cust_act_email','tmpl_cust_user_pass_print','tmpl_cust_user_pass_email',
							'tmpl_backend_usr_act_print','tmpl_backend_usr_act_email','tmpl_backend_user_reset_pass_email','tmpl_backend_user_reset_pass_print'";
							
							$arrayTempSetting5 = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'tmpl_backend_usr_act_print',
													'SETTING_VALUE' => $this->_request->getParam ( 'text5' ),
													'MODULE_ID' => $this->_moduleDB										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting5 );
							
							$arrayTempSetting6 = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'tmpl_backend_usr_act_email',
													'SETTING_VALUE' => $this->_request->getParam ( 'text6' ),
													'MODULE_ID' => $this->_moduleDB										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting6 );
							
							$arrayTempSetting7 = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'tmpl_backend_user_reset_pass_email',
													'SETTING_VALUE' => $this->_request->getParam ( 'text7' ),
													'MODULE_ID' => $this->_moduleDB										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting7);
							
							$arrayTempSetting8 = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'tmpl_backend_user_reset_pass_print',
													'SETTING_VALUE' => $this->_request->getParam ( 'text8' ),
													'MODULE_ID' => $this->_moduleDB										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting8 );
							
							
							$this->_db->commit ();
							
						} catch ( Exception $e ) {
							$this->_db->rollBack ();
							SGO_Helper_GeneralLog::technicalLog($e);					
							$error_remark = $this->getErrorRemark('82');
						}
						
						//insert log
						try {
	  						$this->_db->beginTransaction();
	  						$fulldesc = $this->displayFullDesc($this->_request->getPost());
	  						$this->backendLog('E',$this->_moduleDB,$this->_moduleDB,$fulldesc,$error_remark);
	  						$this->_db->commit();
						}
    					catch(Exception $e){
 	  						$this->_db->rollBack();
	  						SGO_Helper_GeneralLog::technicalLog($e);
						}
						
						if($error_remark){ $class = 'F'; $msg = $error_remark; }
						else{ $class = 'S'; $msg = $this->getErrorRemark('00', 'Email Template Setting'); }
						$this->_helper->getHelper('FlashMessenger')->addMessage($class);
      					$this->_helper->getHelper('FlashMessenger')->addMessage($msg);
      					$this->_redirect($this->_backURL);
				}else{
					$errors = $zf_filter_input->getMessages ();
					if(count($errors))$error_remark = SGO_Helper_GeneralFunction::displayErrorRemark($errors);
					$this->view->suggestedText = ($zf_filter_input->isValid('welcometext'))? $zf_filter_input->welcometext : $this->_getParam('welcometext');
					$this->view->wltError = (isset($errors['welcometext']))? $errors['welcometext'] : null;
					$this->view->error = 1;
				}
							
		}else{
			$suggestedText = $this->_db->fetchAll	(
							$this->_db->select()->from (
							 	array ('M_SETTING'), 
							 	array ('SETTING_VALUE'))
							 	->where('SETTING_ID IN ('.$data_settingid.')')
							);
			for($i=0;$i<8;$i++)
			{
				if(!empty($suggestedText[$i]["SETTING_VALUE"]))
				{
					$suggesttext[$i] = $suggestedText[$i]["SETTING_VALUE"];
				}else{
					$suggesttext[$i] = " ";
				}
			
			}
			$this->view->VarSuggestedText = $suggesttext;
		}
		
		//insert log
		try {
	  		$this->_db->beginTransaction();
	  		if($this->_request->isPost()){
	    		$action = 'E'; $fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($this->_request->getPost());
	  		}else{ $action = 'V'; $fulldesc = null; }
	  		$this->backendLog($action,$this->_moduleDB,$this->_moduleDB,$fulldesc,$error_remark);
	  		$this->_db->commit();
		}
    	catch(Exception $e){
 	  		$this->_db->rollBack();
	  		SGO_Helper_GeneralLog::technicalLog($e);
		}
	}		
}

?>