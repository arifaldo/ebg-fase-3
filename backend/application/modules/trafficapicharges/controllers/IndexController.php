<?php


require_once 'Zend/Controller/Action.php';


class trafficapicharges_IndexController extends Application_Main
{
	public function indexAction()
	{

		$setting = new Settings();			  	 
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');

		$companyCode = $this->language->_('Company Code');
		$companyName = $this->language->_('Company');
		$balanceInquiry = $this->language->_('Balance Inquiry');
		$accountStatement = $this->language->_('Account Statement');
		$inhouse = $this->language->_('Inhouse Transfer');
		$skn = $this->language->_('SKN Transfer');
		$rtgs = $this->language->_('RTGS Transfer');
		$online = $this->language->_('ONLINE Transfer');
		$inhouseBenefInquiry = $this->language->_('Inhouse Beneficiary Inquiry');
		$interBenefInquiry = $this->language->_('Beneficiary Name Inquiry');
		$kursRate = $this->language->_('Kurs Rate Inquiry');
		$suggestedDate = $this->language->_('Charges Suggested');
		$suggester = $this->language->_('Charges Suggested By');
		$approvedDate = $this->language->_('Charges Approved');
		$approver = $this->language->_('Charges Approved By');
		$ccy = $this->language->_('CCY');

		$fields = array	(
							// 'Company Code'  			=> array	(
							// 										'field' => 'B.CUST_ID',
							// 										'label' => $companyCode,
							// 										'sortable' => true
							// 									),
							'Company'  				=> array	(
																	'field' => 'CUST_ID',
																	'label' => $companyName,
																	'type' => null,
																	'sortable' => true
																),
							'Balance Inquiry'  			=> array	(
																	'field' => 'BALANCE_INQUIRY',
																	'label' => $balanceInquiry,
																	'type' => 'money',
																	'sortable' => true
																),
							'Account Statement'  			=> array	(
																	'field' => 'ACCOUNT_STATEMENT',
																	'label' => $accountStatement,
																	'type' => 'money',
																	'sortable' => true
																),
							'Inhouse Transfer'  			=> array	(
																	'field' => 'INHOUSE_TRANSFER',
																	'label' => $inhouse,
																	'type' => 'money',
																	'sortable' => true
																),
							'SKN Transfer'  			=> array	(
																	'field' => 'SKN_TRANSFER',
																	'label' => $skn,
																	'type' => 'money',
																	'sortable' => true
																),
							'RTGS Transfer'  			=> array	(
																	'field' => 'RTGS_TRANSFER',
																	'label' => $rtgs,
																	'type' => 'money',
																	'sortable' => true
																),
							'ONLINE Transfer'  			=> array	(
																	'field' => 'ONLINE_TRANSFER',
																	'label' => $online,
																	'type' => 'money',
																	'sortable' => true
																),
							// 'Inhouse Beneficiary Inquiry'  			=> array	(
							// 										'field' => 'INHOUSE_BENEF_INQUIRY',
							// 										'label' => $inhouseBenefInquiry,
							// 										'type' => 'money',
							// 										'sortable' => true
							// 									),
							'Interbank Beneficiary Inquiry'  			=> array	(
																	'field' => 'INTERBANK_BENEF_INQUIRY',
																	'label' => $interBenefInquiry,
																	'type' => 'money',
																	'sortable' => true
																),
							'Kurs Rate Inquiry'  			=> array	(
																	'field' => 'KURS_RATE_INQUIRY',
																	'label' => $kursRate,
																	'type' => 'money',
																	'sortable' => true
																),
							'Suggested Date'  		=> array	(
																	'field' => 'CHARGES_SUGGESTED',
																	'label' => $suggestedDate,
																	'type' => null,
																	'sortable' => true
																),
							'Suggested By'  			=> array	(
																	'field' => 'CHARGES_SUGGESTEDBY',
																	'label' => $suggester,
																	'type' => null,
																	'sortable' => true
																),
							'Approved Date'  			=> array	(
																	'field' => 'CHARGES_APPROVED',
																	'label' => $approvedDate,
																	'type' => null,
																	'sortable' => true
																),
							'Approved By'  			=> array	(
																	'field' => 'CHARGES_APPROVEDBY',
																	'label' => $approver,
																	'type' => null,
																	'sortable' => true
																)
						);

		$filterlist = array("CUST_ID","SUGGESTOR","SUGGESTED_DATE");
		$this->view->filterlist = $filterlist;

		$filterArr = array('filter' 			=> array('StripTags','StringTrim'),
	                       'custid'    			=> array('StripTags','StringTrim'),
						   'suggestor'  		=> array('StripTags','StringTrim','StringToUpper'),
						   'fDateFrom'  		=> array('StripTags','StringTrim'),
						   'fDateTo'  			=> array('StripTags','StringTrim'),
	                      );

	    $validator = array('filter' 			=> array(),
	                       'custid'    			=> array(),
						   'suggestor'  		=> array(),
						   'fDateFrom'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo'  			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );

	    $dataParam = array("CUST_ID","SUGGESTOR","SUGGESTED_DATE");
		$dataParamValue = array();
	    foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if($dtParam==$value){
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}
		}


	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $custid = html_entity_decode($zf_filter->getEscaped('custid'));
		$suggestor = html_entity_decode($zf_filter->getEscaped('suggestor'));
		$datefrom = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$dateto = html_entity_decode($zf_filter->getEscaped('fDateTo'));
		//Zend_Debug::dump($this->_masterglobalstatus); die;

		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');

		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		/*if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}*/

		$selectglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_API'),array('*'));
		$selectglobal ->where("A.CUST_ID = 'GLOBAL'");
		$arrglobal = $this->_db->fetchAll($selectglobal);

		$this->view->globalcharge = $arrglobal;		

		// echo "<pre>";
		// var_dump($arrglobal);die;


		$select2 = $this->_db->select()->distinct()
						->from(array('B' => 'M_CUSTOMER'),array('B.CUST_ID',
																'B.CUST_NAME',
																'B.CUST_SUGGESTED',
																'B.CUST_SUGGESTEDBY'))
						->joinLeft(array('C' => 'M_CHARGES_API'),'B.CUST_ID = C.CUST_ID',
								array(
										'coalesce(C.BALANCE_INQUIRY," - ") AS BALANCE_INQUIRY', 
										'coalesce(C.ACCOUNT_STATEMENT," - ") AS ACCOUNT_STATEMENT', 
										'coalesce(C.INHOUSE_TRANSFER," - ") AS INHOUSE_TRANSFER',
										'coalesce(C.SKN_TRANSFER," - ") AS SKN_TRANSFER',
										'coalesce(C.RTGS_TRANSFER," - ") AS RTGS_TRANSFER',
										'coalesce(C.ONLINE_TRANSFER," - ") AS ONLINE_TRANSFER',
										// 'coalesce(C.INHOUSE_BENEF_INQUIRY," - ") AS INHOUSE_BENEF_INQUIRY',
										'coalesce(C.INTERBANK_BENEF_INQUIRY," - ") AS INTERBANK_BENEF_INQUIRY',
										'coalesce(C.KURS_RATE_INQUIRY," - ") AS KURS_RATE_INQUIRY',
										'coalesce(C.CHARGES_SUGGESTED," - ") AS CHARGES_SUGGESTED',
										'coalesce(C.CHARGES_SUGGESTEDBY," - ") AS CHARGES_SUGGESTEDBY',
										'coalesce(C.CHARGES_APPROVED," - ") AS CHARGES_APPROVED',
										'coalesce(C.CHARGES_APPROVEDBY," - ") AS CHARGES_APPROVEDBY'
								))
						->where("B.CUST_STATUS != '3'")
						->order('B.CUST_ID ASC');
						
		$arr = $this->_db->fetchAll($select2);

		// foreach ($arr as $key => $value) {
			// if($value==)
		// }

		// echo '<pre>';
		// print_r($arr);die();

		//if($filter == null || $filter == 'Set Filter')
		if($filter == true)
		{
		 $this->view->fDateTo    = $dateto;
		 $this->view->fDateFrom  = $datefrom;
		//Zend_Debug::dump($custid); die;

		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat); 
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);
	            }

	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
					//Zend_Debug::dump($dateto); die;
	            }

		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(B.CHARGES_SUGGESTED) >= ".$this->_db->quote($datefrom));

	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.CHARGES_SUGGESTED) <= ".$this->_db->quote($dateto));

	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.CHARGES_SUGGESTED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}

		if($filter == true)
		{

			if($custid)
		    {
	       		$this->view->cekcustid = $custid;
	       		$select2->where("B.CUST_ID LIKE ".$this->_db->quote($custid));
		    }

			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
	       		$select2->where("B.CHARGES_SUGGESTEDBY LIKE ".$this->_db->quote('%'.$suggestor.'%'));
		    }
		}

		// echo $select2;die;
		// $select2->group('B.CUST_ID');
		//$select2->order($sortBy.' '.$sortDir);
		$select2->order($sortBy.' '.$sortDir);
    	$this->paging($select2,30);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	$header = Application_Helper_Array::simpleArray($fields, "label");
    	$arr = $this->_db->fetchAll($select2);

    	if($this->_request->getParam('print') == 1){

    		$fields1 = array	(
							// 'Company Code'  			=> array	(
							// 										'field' => 'B.CUST_ID',
							// 										'label' => $companyCode,
							// 										'sortable' => true
							// 									),
							'Company'  				=> array	(
																	'field' => 'CUST_ID',
																	'label' => $companyName,
																	'type' => null,
																	'sortable' => true
																),
							'CCY'  					=> array	(
																	'field' => 'CCY',
																	'label' => $ccy,
																	'type' => null,
																	'sortable' => true
																),
							'Balance Inquiry'  			=> array	(
																	'field' => 'BALANCE_INQUIRY',
																	'label' => $balanceInquiry,
																	'type' => 'money',
																	'sortable' => true
																),
							'Account Statement'  			=> array	(
																	'field' => 'ACCOUNT_STATEMENT',
																	'label' => $accountStatement,
																	'type' => 'money',
																	'sortable' => true
																),
							'Inhouse Transfer'  			=> array	(
																	'field' => 'INHOUSE_TRANSFER',
																	'label' => $inhouse,
																	'type' => 'money',
																	'sortable' => true
																),
							'SKN Transfer'  			=> array	(
																	'field' => 'SKN_TRANSFER',
																	'label' => $skn,
																	'type' => 'money',
																	'sortable' => true
																),
							'RTGS Transfer'  			=> array	(
																	'field' => 'RTGS_TRANSFER',
																	'label' => $rtgs,
																	'type' => 'money',
																	'sortable' => true
																),
							'ONLINE Transfer'  			=> array	(
																	'field' => 'ONLINE_TRANSFER',
																	'label' => $online,
																	'type' => 'money',
																	'sortable' => true
																),
							// 'Inhouse Beneficiary Inquiry'  			=> array	(
							// 										'field' => 'INHOUSE_BENEF_INQUIRY',
							// 										'label' => $inhouseBenefInquiry,
							// 										'type' => 'money',
							// 										'sortable' => true
							// 									),
							'Interbank Beneficiary Inquiry'  			=> array	(
																	'field' => 'INTERBANK_BENEF_INQUIRY',
																	'label' => $interBenefInquiry,
																	'type' => 'money',
																	'sortable' => true
																),
							'Kurs Rate Inquiry'  			=> array	(
																	'field' => 'KURS_RATE_INQUIRY',
																	'label' => $kursRate,
																	'type' => 'money',
																	'sortable' => true
																),
							'Suggested Date'  		=> array	(
																	'field' => 'CHARGES_SUGGESTED',
																	'label' => $suggestedDate,
																	'type' => null,
																	'sortable' => true
																),
							'Suggested By'  			=> array	(
																	'field' => 'CHARGES_SUGGESTEDBY',
																	'label' => $suggester,
																	'type' => null,
																	'sortable' => true
																),
							'Approved Date'  			=> array	(
																	'field' => 'CHARGES_APPROVED',
																	'label' => $approvedDate,
																	'type' => null,
																	'sortable' => true
																),
							'Approved By'  			=> array	(
																	'field' => 'CHARGES_APPROVEDBY',
																	'label' => $approver,
																	'type' => null,
																	'sortable' => true
																)
						);

    		foreach($arr as $key=>$row)
			{
				$arr[$key]['CUST_ID'] = $row['CUST_NAME'].' ('.$row['CUST_ID'].')';
				$arr[$key]['CCY'] = 'IDR';

				if($row['BALANCE_INQUIRY'] == ' - '){
	              $arr[$key]['BALANCE_INQUIRY'] = $arrglobal[0]['BALANCE_INQUIRY'];
	            }

	            if($row['ACCOUNT_STATEMENT'] == ' - '){
	              $arr[$key]['ACCOUNT_STATEMENT'] = $arrglobal[0]['ACCOUNT_STATEMENT'];
	            }

	            if($row['INHOUSE_TRANSFER'] == ' - '){
	              $arr[$key]['INHOUSE_TRANSFER'] = $arrglobal[0]['INHOUSE_TRANSFER'];
	            }

	            if($row['SKN_TRANSFER'] == ' - '){
	              $arr[$key]['SKN_TRANSFER'] = $arrglobal[0]['SKN_TRANSFER'];
	            }

	            if($row['RTGS_TRANSFER'] == ' - '){
	              $arr[$key]['RTGS_TRANSFER'] = $arrglobal[0]['RTGS_TRANSFER'];
	            }

	            if($row['ONLINE_TRANSFER'] == ' - '){
	              $arr[$key]['ONLINE_TRANSFER'] = $arrglobal[0]['ONLINE_TRANSFER'];
	            }

	            if($row['INTERBANK_BENEF_INQUIRY'] == ' - '){
	              $arr[$key]['INTERBANK_BENEF_INQUIRY'] = $arrglobal[0]['INTERBANK_BENEF_INQUIRY'];
	            }

	            if($row['KURS_RATE_INQUIRY'] == ' - '){
	              $arr[$key]['KURS_RATE_INQUIRY'] = $arrglobal[0]['KURS_RATE_INQUIRY'];
	            }

	            if($row['CHARGES_SUGGESTED'] == ' - '){
	              $arr[$key]['CHARGES_SUGGESTED'] = $arrglobal[0]['CHARGES_SUGGESTED'];
	            }

	            if($row['CHARGES_SUGGESTEDBY'] == ' - '){
	              $arr[$key]['CHARGES_SUGGESTEDBY'] = $arrglobal[0]['CHARGES_SUGGESTEDBY'];
	            }

	            if($row['CHARGES_SUGGESTED'] == ' - '){
	              $arr[$key]['CHARGES_SUGGESTED'] = $arrglobal[0]['CHARGES_SUGGESTED'];
	            }

	            if($row['CHARGES_APPROVED'] == ' - '){
	              $arr[$key]['CHARGES_APPROVED'] = $arrglobal[0]['CHARGES_APPROVED'];
	            }

	            if($row['CHARGES_APPROVEDBY'] == ' - '){
	              $arr[$key]['CHARGES_APPROVEDBY'] = $arrglobal[0]['CHARGES_APPROVEDBY'];
	            }
	            
				
			}

			// echo "<pre>";
			// var_dump($arrglobal);
			// die();

			$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Payment Report', 'data_header' => $fields1));
		}

    	if($csv || $pdf)
    	{
    		
    		foreach($arr as $key=>$value)
			{
				$arr[$key]["CUST_SUGGESTED"] = Application_Helper_General::convertDate($value["CUST_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}

	    	if($csv)
			{
				Application_Helper_General::writeLog('CHLS','Download CSV Company Charges List');
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}

			if($pdf)
			{
				Application_Helper_General::writeLog('CHLS','Download PDF Company Charges List');
				$this->_helper->download->pdf($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('CHLS','View Company Charges List');
    	}
	}
}
