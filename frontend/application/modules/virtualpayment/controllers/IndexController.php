<?php

class virtualpayment_IndexController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';
	
	private function generateRandomString($length = 10) {
		$char = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $char[rand(0, strlen($char) - 1)];
		}
		return date('Ymd').$randomString;
	}
	
	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
	}
	
	public function indexAction (){
		$uploaded = $this->_request->getParam('uploaded');
		
		if (!(empty($uploaded))){
			$this->view->uploadText = 'File was successfully uploaded';
		}
		
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
				'va'	=> array(
						'field'	=> 'VA',
						'label'	=> 'VA',
						'sortable'	=> true
				),
				'institusi'	=> array(
						'field' => 'INSTITUSI',
						'label' => $this->language->_('Institusi'),
						'sortable'	=> true
				),
				'invoice'	=> array(
						'field' => 'INVOICE_NO',
						'label' => $this->language->_('Invoice'),
						'sortable' => true
				),
				'invoiceName'	=> array(
						'field' => 'INVOICE_NAME',
						'label' => $this->language->_('Invoice Name'),
						'sortable' => true
				),
				'desc' => array(
						'field' => 'DESCRIPTION',
						'label' => $this->language->_('Description'),
						'sortable' => true
				),
				'amount' => array(
						'field' => 'AMOUNT',
						'label' => $this->language->_('Amount'),
						'sortable' => true
				),
		);
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($fields, 'field');
		
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[0]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
				'institusi'			=> array(),
				'invoice'			=> array(),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
		
			$select = $this->_db->select()
			->from(
					array('X_BILLER_OFFLINE_UPLOAD'),
					array('VA','INSTITUSI','INVOICE','INVOICE_NAME','DESCRIPTION','AMOUNT')
			)
			->order($sortBy.' '.$sortDir)
			;
		
			if (!empty($institusi)){
				$select->where('INSTITUSI = ?', $institusi);
			}
			
			if (!empty($invoice)){
				$select->where('INVOICE = ?', $invoice);
			}
				
			// 			echo $select;
			$data = $this->_db->fetchAll($select);
		}
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
		$this->paging($data);
		
		$csv = $this->_request->getParam('csv',NULL);
		$pdf = $this->_request->getParam('pdf',NULL);
		
		if ($csv || $pdf){
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			if($csv){
				$this->_helper->download->csv($header,$data,null,'List View Payment');
				Application_Helper_General::writeLog('DARC','Export CSV View Payment');
			}elseif($pdf){
				$this->_helper->download->pdf($header,$data,null,'List View Payment');
				Application_Helper_General::writeLog('DARC','Export PDF View Payment');
			}
		}else{
			Application_Helper_General::writeLog('DARC','View FSCM Payment Report');
		}
	}
	
	public function taxconfirmAction(){
		$taxSess = new Zend_Session_Namespace('taxSess');
		
		$token = new Service_Token($this->_custIdLogin, $this->_userIdLogin);
		$this->view->challengeCode 	= $token->generateChallengeCode();
		
		$filter = array(
			'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => false);
		$validators = array(
			'userOnBehalf'		=> array('alnum','presence' => 'required',),
			'challengeCode'			=> array('Digits','presence' => 'required',),
			'responseCode'	=> array('Digits','presence' => 'required',),
		);
		$params 	= $this->_request->getParams();
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		foreach ($taxSess->list as $key => $val){
			$$key = $val;
			$this->view->{$key} = $val;
		}
		foreach ($taxSess->param as $key => $val){
			$$key = $val;
			$this->view->{$key} = $val;
		}
		
		$this->view->fee = 15000;
		$this->view->custName = $acctName[$sourceAccount];
		
		$this->view->userIdLogin 	= $this->_userIdLogin;
		$this->view->custIdLogin 	= $this->_custIdLogin;
		
		$check = $this->_request->getParam('userOnBehalf');
		
		if (!(empty($check))){
			if ($_POST){
				if ($filter->isValid()){
					$token 		= new Service_Token($this->_custIdLogin, $this->_userIdLogin);
					$verToken 	= $token->verify($filter->getEscaped('challengeCode'), $filter->getEscaped('$responseCode'));
			
					if ($verToken['ResponseCode'] != '00'){
						$this->view->error = array(
							'error' => array(
								'error' => $verToken['ResponseDesc']
							)
						);
					}else{
						$bindInsert = $taxSess->content;
						$this->_db->beginTransaction();
						try{
							$this->_db->insert('X_PAYMENT_DETAIL',$bindInsert);
							$this->_db->commit();
							$this->_redirect($this->_request->getModuleName().'/'.$this->_request->getControllerName().'/summary');
						}catch(Exception $e){
							$this->_db->rollBack();
							Zend_Debug::dump($e->getMessage());
							//Application_Helper_General::writeLog('XXXX','TAX Error: '.$e->getMessage());
						}
					}
				}else{
					$this->view->error = $filter->getMessages();
				}
			}
		}
	}
	
	public function taxAction (){	
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$acctList = array();
		$acctName = array();
		$sql = $this->_db->select()
			->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_NAME','ACCT_ALIAS_NAME','ACCT_DESC','CCY_ID'))
			->where('CUST_ID = ?',$this->_custIdLogin)
		;
		$sql = $this->_db->fetchAll($sql);
		if (count($sql) > 0){
			foreach ($sql as $arr => $key){
				$acctList[$key['ACCT_NO']] = $key['ACCT_NO'].' ['.$key['CCY_ID'].'] - '.$key['ACCT_NAME'].'/ '.$key['ACCT_ALIAS_NAME'].' ('.$key['ACCT_DESC'].')';
				$acctName[$key['ACCT_NO']] = $key['ACCT_NAME'];
			}
		}
		$this->view->acctList = $acctList;
		$this->view->acctName = $acctName;
		
		$taxList = array();
		$sql = $this->_db->select()
		->from(array('X_M_INSTITUSI'),array('INSTITUSI','NAMA'))
		;
		$sql = $this->_db->fetchAll($sql);
		if (count($sql) > 0){
			foreach ($sql as $arr => $key){
				$taxList[$key['INSTITUSI']] = $key['NAMA'];
			}
		}
		$this->view->taxList = $taxList;
		
		$monthList = array();
		$index = date('n');
		for($i = ($index-3); $i <= $index; $i ++){
			$monthList[$i] = date('F', mktime(0, 0, 0, $i, 1, date('Y')));
		}
		$this->view->monthList = $monthList;
		
		$yearsList = array();
		$index = '2014';
		for($i = ($index-3); $i <= $index; $i ++){
			$yearsList[$i] = $i;
		}
		$this->view->yearsList = $yearsList;
		
		$params 	= $this->_request->getParams();
// 		Zend_Debug::dump($params);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$filter = array(
			'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => false);
		
		$validators = array(
			'sourceAccount'		=> array('Digits','presence' => 'required',),
			'typeOfTax'			=> array('Digits','presence' => 'required',),
			'addToPaymentList'	=> array('allowEmpty' => true),
			'paymentChoice'		=> array('Digits','presence' => 'required'),
			'payment'			=> array('presence' => 'required'),
			'period'			=> array('Digits','presence' => 'required',),
			'years'				=> array('Digits','presence' => 'required',),
			'amount'			=> array('Digits','presence' => 'required',),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		$data = array();
		
		if ($_POST){
			if ($filter->isValid()){
				foreach ($stringParam as $key => $val){
					$$key = $val;
					$this->view->{$key} = $val;
				}
				
				$this->view->fee = 15000;
				$this->view->custName = $acctName[$filter->getEscaped('sourceAccount')];
				
				$this->view->userIdLogin 	= $this->_userIdLogin;
				$this->view->custIdLogin 	= $this->_custIdLogin;
				
				$bindInsert = array();
				$bindInsert['COMP_ID'] = '1';
				$bindInsert['INSTITUSI'] = $filter->getEscaped('typeOfTax');
				$bindInsert['INVOICE_NO'] = '1';
				$bindInsert['INVOICE_NAME'] = '1';
				$bindInsert['AMOUNT'] = $filter->getEscaped('amount');
				$bindInsert['TRX_DATE'] = date('Y-m-d');
				$bindInsert['LAST_UPDATE'] = date('Y-m-d H:i:s');
				$bindInsert['SYNC'] = '1';
				$bindInsert['PAYMENT_TYPE'] = '1';
				$bindInsert['PAID_STATUS'] = '1';
				$bindInsert['PAYMENT_CODE'] = rand(1000000000000000,9999999999999999);
				$bindInsert['PAID_FROM'] = '-';
				$bindInsert['INVOICE_DESC'] = '1';
				$bindInsert['CUSTOMER_ID'] = $this->_custIdLogin;
				$bindInsert['AREA'] = '1';
				
				$taxSess = new Zend_Session_Namespace('taxSess');
				$taxSess->mode = 'Add';
				$taxSess->content = $bindInsert;
				$taxSess->list = array(
					'acctList' => $acctList,
					'acctName' => $acctName,
					'taxList' => $taxList,
					'monthList' => $monthList,
					'yearsList' => $yearsList,
				);
				$taxSess->param = $stringParam;
				
				$this->_forward('taxconfirm');
			}else{
				foreach ($stringParam as $key => $val){
					$$key = $val;
					$this->view->{$key} = $val;
				}
				$this->view->error = $filter->getMessages();
				
			}
		}
		Application_Helper_General::writeLog('DARC','Viewing Tax');
	}
	
	public function viewAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$this->view->user = $this->_userIdLogin;
		
		$data = array();
		
		$params = $this->_request->getParams();
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => false);
		$validators = array(
			'id' => array('alnum','presence' => 'required'),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		if ($filter->isValid()){
			
			$id = $filter->getEscaped('id');
					
			$select = $this->_db->select()
				->from(array('XPD'=>'X_PAYMENT_DETAIL'))
				->where('INVOICE_NO = ?', $id)
				->joinLeft(array('XBOU'=>'X_BILLER_OFFLINE_UPLOAD'), 'XBOU.INVOICE = XPD.INVOICE_NO')
				->limit(1)
			;
			$data = $this->_db->fetchRow($select);
		}
		
		$this->view->data = $data;		
	}
	
	public function swapoutAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
	
		$data = array();
	
		$params = $this->_request->getParams();
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		$validators = array(
				'id' => array('alnum'),
		);
	
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
	
		if ($filter->isValid()){
			$select = $this->_db->select()
			->from(array('XMI'=>'X_M_INSTITUSI'))
			->group('INSTITUSI')
			;
// 			echo $select;
			$data = $this->_db->fetchAll($select);
		}
	
		$this->view->data = $data;
		Application_Helper_General::writeLog('DARC','Viewing Swap Out Account');
	}

	/*
	public function reportAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
				'create'	=> array(
						'field'	=> 'PS_CREATED',
						'label'	=> 'Created Date',
						'sortable'	=> true
				),
				'update'	=> array(
						'field' => 'PS_UPDATED',
						'label' => 'Last Updated Date',
						'sortable'	=> true
				),
				'date'	=> array(
						'field' => 'PS_EFDATE',
						'label' => 'Payment Date',
						'sortable' => true
				),
				'reff'	=> array(
						'field' => 'PS_NUMBER',
						'label' => 'Payment Reff',
						'sortable' => true
				),
				'source' => array(
						'field' => 'SOURCE_ACCOUNT',
						'label' => 'Source Account',
						'sortable' => true
				),
				'sourceAlias'	=> array(
						'field' => 'SOURCE_ACCOUNT_NAME',
						'label' => 'Source Name/ Alias',
						'sortable' => true
				),
				'beneficiary'	=> array(
						'field' => 'BENEFICIARY_ACCOUNT',
						'label' => 'Beneficiary Account',
						'sortable' => true
				),
				'beneficiaryAlias' => array	(
						'field' => 'BENEFICIARY_ACCOUNT_NAME',
						'label' => 'Beneficiary Name/ Alias.',
						'sortable' => true
				),
				'ccy'	=> array	(
						'field' => 'BENEFICIARY_ACCOUNT_CCY',
						'label' => 'CCY',
						'sortable' => false
				),
				'ccyAmount'	=> array	(
						'field' => 'TRA_AMOUNT',
						'label' => 'Amount',
						'sortable' => true
				),
				'id'	=> array	(
						'field' => 'TRANSACTION_ID',
						'label' => 'Transaction ID',
						'sortable' => true
				),
				'subject'	=> array	(
						'field' => 'PS_SUBJECT',
						'label' => 'Payment Subject',
						'sortable' => true
				),
				'message'	=> array	(
						'field' => 'TRA_MESSAGE',
						'label' => 'Message',
						'sortable' => true
				),
				'status'	=> array	(
						'field' => 'TRA_STATUS',
						'label' => 'Status',
						'sortable' => true
				),
				'type'	=> array	(
						'field' => 'PS_TYPE',
						'label' => 'Payment Type',
						'sortable' => true
				),
		);
		
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($fields, 'field');
		
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[1]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$select = $this->_db->select()
			->from(
				array('M_CUSTOMER_ACCT'),
				array('ACCT_NO','ACCT_ALIAS_NAME'=>'CONCAT(ACCT_NO," ","[",CCY_ID,"]"," - ",ACCT_NAME,"/ ",ACCT_ALIAS_NAME)')
			)
			->group('ACCT_NO')
			->where('NOT ACCT_ALIAS_NAME = ?', '')
		;
		
		$opt = $this->_db->fetchAll($select);
		$optSourceAccount = array(''=>'-- Please Select --');
		foreach ($opt as $key){
			$optSourceAccount[$key['ACCT_NO']] = $key['ACCT_ALIAS_NAME'];
		}
		
		$optArr = Zend_Registry::get('payment');
		$opt = array_flip($optArr['type']['code']);
		
		$optPaymentType = array(''=>'-- Please Select --');
		foreach ($opt as $key => $val){
			$optPaymentType[$key] = $optArr['type']['desc'][$val];
		}
		unset($optPaymentType['4']);
		unset($optPaymentType['5']);
		unset($optPaymentType['8']);
		unset($optPaymentType['9']);
		unset($optPaymentType['10']);
		
		$optArr = Zend_Registry::get('transfer');
		$opt = array_flip($optArr['status']['code']);
		
		foreach ($opt as $key => $val){
			$optTransferStatus[$key] = $optArr['status']['desc'][$val];
		}
		
		unset($optTransferStatus['1']);
		unset($optTransferStatus['2']);
		unset($optTransferStatus['9']);
		unset($optTransferStatus['13']);
		unset($optTransferStatus['0']);
		
		//remap array index
		$opt = $optTransferStatus;
		$optTransferStatus = array(''=>'-- Please Select --');
		
		$i = 0;
		foreach ($opt as $key => $val){
			$i ++;
			$optTransferStatus[$i] = $val;
		}
		
		$optArr = Zend_Registry::get('transfer');
		$opt = array_flip($optArr['type']['code']);
		
		$optTransferType = array(''=>'-- Please Select --');
		foreach ($opt as $key => $val){
			$optTransferType[$key] = $optArr['type']['desc'][$val];
		}
		
		unset($optTransferType['3']);
		unset($optTransferType['4']);
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
				'paymentDateStart' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'paymentDateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'paymentReff'		=> array('alnum'),
				'beneficiaryAccount'=> array('Digits'),
				'sourceAccount' 	=> array(array('InArray', array('haystack' => array_keys($optSourceAccount)))),
				'paymentType' 		=> array(array('InArray', array('haystack' => array_keys($optPaymentType)))),
				'status' 			=> array(array('InArray', array('haystack' => array_keys($optTransferStatus)))),
				'transferType' 		=> array(array('InArray', array('haystack' => array_keys($optTransferType)))),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
		
			$select = $this->_db->select()
				->from(
						array('T'=>'T_TRANSACTION'),
						array(
							'P.PS_CREATED',
							'P.PS_UPDATED',
							'P.PS_EFDATE',
							'P.PS_NUMBER',
							
							'T.SOURCE_ACCOUNT',
							'SOURCE_ACCOUNT_NAME'=>'CONCAT(T.SOURCE_ACCOUNT_NAME,"/ ",T.SOURCE_ACCOUNT_ALIAS_NAME)',
							'T.BENEFICIARY_ACCOUNT',
							'T.BENEFICIARY_ACCOUNT_NAME',
							'T.BENEFICIARY_ACCOUNT_CCY',
							'T.TRA_AMOUNT',
							'T.TRANSACTION_ID',
							'P.PS_SUBJECT',
							'T.TRA_MESSAGE',
							'T.TRA_STATUS',
								
							
							'P.PS_TYPE',						)
				)
				->join(array('P'=>'T_PSLIP'),'P.PS_NUMBER = T.PS_NUMBER',array())
				->order($sortBy.' '.$sortDir)
			;
		
			if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$select->where('P.PS_EFDATE >= "'.$paymentDateStart.'" AND P.PS_EFDATE <= "'.$paymentDateEnd.'"');
			}else{
				if (!empty($paymentDateStart)){
					$select->where('P.PS_EFDATE >= "'.$paymentDateStart.'"');
				}
		
				if (!empty($paymentDateEnd)){
					$select->where('P.PS_EFDATE <= "'.$paymentDateEnd.'"');
				}
			}
		
			if (!empty($sourceAccount)){
				$select->where('T.SOURCE_ACCOUNT = ?', $sourceAccount);
			}
			
			if (!empty($paymentReff)){
				$select->where('P.PS_NUMBER = ?', $paymentReff);
			}
			
			if (!empty($beneficiaryAccount)){
				$select->where('T.BENEFICIARY_ACCOUNT = ?', $beneficiaryAccount);
			}
			
			if (!empty($paymentType)){
				$select->where('P.PS_TYPE = ?', $paymentType);
			}
			
// 			if (!empty($transferType)){
// 				$select->where('PAYMENT_TYPE = ?', $transferType);
// 			}

			if (!empty($status)){
				$select->where('T.TRA_STATUS = ?', $status);
			}
				
// 			echo $select;
			$data = $this->_db->fetchAll($select);
		}
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->optSourceAccount 	= $optSourceAccount;
		$this->view->optTransferStatus 	= $optTransferStatus;
		$this->view->optPaymentType 	= $optPaymentType;
		$this->view->optTransferType 	= $optTransferType;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
		$this->paging($data);
		
		$chart = $this->_request->getParam('chart', false);
		
		if ($csv || $pdf || $chart){
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			if($csv){
				$this->_helper->download->csv($header,$data,null,'Transaction Report');
				Application_Helper_General::writeLog('DTRX','Export CSV View Transaction Report');
			}elseif($pdf){
				$this->_helper->download->pdf($header,$data,null,'Transaction Report');
				Application_Helper_General::writeLog('DTRX','Export PDF View Transaction Report');
			}elseif($chart){
				
				$data = $this->_db->query('
					SELECT
					XP.INSTITUSI,
					XM.NAMA,
					XP.AMOUNT
					FROM
					X_PAYMENT_DETAIL XP
					LEFT JOIN X_M_INSTITUSI XM ON XM.INSTITUSI = XP.INSTITUSI
					GROUP BY XP.INSTITUSI
				')->fetchAll();				
				$this->view->summary = $data;
				
				$data = $this->_db->query('
					SELECT
					XP.INSTITUSI,
					XM.NAMA,
					YEAR(XP.TRX_DATE) AS "YEAR",
					MONTHNAME(XP.TRX_DATE) AS "MONTH",
					CONCAT("WEEK"," ", WEEK(XP.TRX_DATE)) AS "WEEK",
					XP.AMOUNT
					FROM 
					X_PAYMENT_DETAIL XP
					LEFT JOIN X_M_INSTITUSI XM ON XM.INSTITUSI = XP.INSTITUSI
					GROUP BY XP.INSTITUSI
				')->fetchAll();
				$this->view->group = $data;
				
				$this->_helper->viewRenderer('index/chart', null, true);
				$this->view->render();
				Application_Helper_General::writeLog('DTRX','View Transaction Report Chart');
			}
		}else{
			Application_Helper_General::writeLog('DTRX','View Transaction Report');
		}
	}
	*/
	
	public function summaryAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
				'payReff'	=> array(
						'field'	=> 'INVOICE_NO',
						'label'	=> 'Payment Ref',
						'sortable'	=> false
				),
				'institusi'	=> array(
						'field' => 'INSTITUSI',
						'label' => 'Institusi',
						'sortable'	=> true
				),
				'invoice'	=> array(
						'field' => 'INVOICE_NO',
						'label' => 'Invoice',
						'sortable' => true
				),
				'invoiceName'	=> array(
						'field' => 'INVOICE_NAME',
						'label' => 'Invoice Name',
						'sortable' => true
				),
				'amount' => array(
						'field' => 'AMOUNT',
						'label' => 'Amount',
						'sortable' => true
				),
				'trxDate'	=> array(
						'field' => 'TRX_DATE',
						'label' => 'Transaction Date',
						'sortable' => true
				),
				'lastUpdate'	=> array(
						'field' => 'LAST_UPDATE',
						'label' => 'Last Update',
						'sortable' => true
				),
				'sync' 					 => array	(
						'field' => 'SYNC',
						'label' => 'Sync.',
						'sortable' => true
				),
				'paymentType'	=> array	(
						'field' => 'PAYMENT_TYPE',
						'label' => 'Payment Type',
						'sortable' => true
				),
				'paid'	=> array	(
						'field' => 'PAID_STATUS',
						'label' => 'Paid Status',
						'sortable' => true
				),
		);
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($fields, 'field');
		
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[1]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$optSync = array(
				'' => '-- Please Select --',
				'Y' => 'Yes',
				'N' => 'No',
		);
		
		$select = $this->_db->select()
		->from(
				array('X_M_INSTITUSI'),
				array('INSTITUSI','NAMA')
		)
		->group('NAMA')
		;
		$opt = $this->_db->fetchAll($select);
		$optPaymentType = array(''=>'-- Please Select --');
		foreach ($opt as $key){
			$optPaymentType[$key['INSTITUSI']] = $key['NAMA'];
		}
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
				'paymentDateStart' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'paymentDateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'lastUpdateStart'	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'lastUpdateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'paymentStart' 		=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'institusi'			=> array(),
				'sync' 				=> array(array('InArray', array('haystack' => array_keys($optSync)))),
				'paymentType' 		=> array(array('InArray', array('haystack' => array_keys($optPaymentType)))),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
				
			$select = $this->_db->select()
			->from(
					array('X_PAYMENT_DETAIL'),
					array('PAYREFF' => 'INVOICE_NO','INSTITUSI','INVOICE_NO','INVOICE_NAME','AMOUNT','TRX_DATE','LAST_UPDATE','SYNC','PAYMENT_TYPE','PAID_STATUS')
			)
			->order($sortBy.' '.$sortDir)
			;
			
			if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//$select->where('TRX_DATE >= "'.$paymentDateStart.'" AND TRX_DATE <= "'.$paymentDateEnd.'"');
			}else{
				if (!empty($paymentDateStart)){
					$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
				}
		
				if (!empty($paymentDateEnd)){
					$select->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				}
			}
				
			if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
				$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
			}else{
				if (!empty($lastUpdateStart)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
				}
					
				if (!empty($lastUpdateEnd)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}
			}
				
			if (!empty($institusi)){
				$select->where('INSTITUSI = ?', $institusi);
			}
			if (!empty($sync)){
				$select->where('SYNC = ?', $sync);
			}
			if (!empty($paymentType)){
				$select->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
			}
			
 			//echo $select;
 			//die("123");
			$datadb = $this->_db->fetchAll($select);
			$data = array();
			
			//Zend_Debug::dump($datadb);
			//die("123");			
			
			foreach ($datadb as $row => $arr){
				foreach ($arr as $key => $val){
					if ($key == 'PAYREFF'){
						$data[$row][$key] = $this->generateRandomString(9);
					}else{
						$data[$row][$key] = $val;
					}
				}
			}
		}
		
		//Zend_Debug::dump($data);
		//die("123");
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->optSync 		= $optSync;
		$this->view->optPaymentType = $optPaymentType;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		//$chart = $this->_request->getParam('chart', false);
		$pdf = $this->_request->getParam('pdf', false);
		$csv = $this->_request->getParam('csv', false);
		
		$this->paging($data);
		
		
		//Zend_Debug::dump($chart);
		if ($csv || $pdf){
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			if($csv){
				$this->_helper->download->csv($header,$data,null,'List View Payment');
				Application_Helper_General::writeLog('DARC','Export CSV View Payment');
			}elseif($pdf){
				$this->_helper->download->pdf($header,$data,null,'List View Payment');
				Application_Helper_General::writeLog('DARC','Export PDF View Payment');
			
			}else{
				Application_Helper_General::writeLog('DARC','View FSCM Payment Report');
			}
		}
		Application_Helper_General::writeLog('DARC','Viewing Payment Report Summary');
	}
	
	public function chartAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
			'payReff'	=> array(
				'field'	=> 'INVOICE_NO',
				'label'	=> 'Payment Ref',
				'sortable'	=> false
			),
			'institusi'	=> array(
				'field' => 'INSTITUSI',
				'label' => 'Institusi',
				'sortable'	=> true
			),
			'invoice'	=> array(
				'field' => 'INVOICE_NO',
				'label' => 'Invoice',
				'sortable' => true
			),
			'invoiceName'	=> array(
				'field' => 'INVOICE_NAME',
				'label' => 'Invoice Name',
				'sortable' => true
			),
			'amount' => array(
				'field' => 'AMOUNT',
				'label' => 'Amount',
				'sortable' => true
			),
			'trxDate'	=> array(
				'field' => 'TRX_DATE',
				'label' => 'Transaction Date',
				'sortable' => true
			),
			'lastUpdate'	=> array(
				'field' => 'LAST_UPDATE',
				'label' => 'Last Update',
				'sortable' => true
			),
			'sync' 					 => array	(
				'field' => 'SYNC',
				'label' => 'Sync.',
				'sortable' => true
			),
			'paymentType'	=> array	(
				'field' => 'PAYMENT_TYPE',
				'label' => 'Payment Type',
				'sortable' => true
			),
			'paid'	=> array	(
				'field' => 'PAID_STATUS',
				'label' => 'Paid Status',
				'sortable' => true
			),
			'paymentDateStart'	=> array	(
				'field' => 'PAYMENT_DATE_START',
				'label' => 'Payment Date Start',
				'sortable' => true
			),
		);
		
		$params 	= $this->_request->getParams();
		$field 		= Application_Helper_Array::simpleArray($fields, 'field');
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[1]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$optSync = array(
			'' => '-- Please Select --',
			'Y' => 'Yes',
			'N' => 'No',
		);
		
		$select = $this->_db->select()
			->from(
				array('X_M_INSTITUSI'),
				array('INSTITUSI','NAMA')
			)
			->group('NAMA')
		;
		
		$opt = $this->_db->fetchAll($select);
		$optPaymentType = array(''=>'-- Please Select --');
		foreach ($opt as $key){
			$optPaymentType[$key['INSTITUSI']] = $key['NAMA'];
		}
		
		$filter = array(
			'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
			'paymentDateStart' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'paymentDateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'lastUpdateStart'	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'lastUpdateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'paymentStart' 		=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'institusi'			=> array(),
			'sync' 				=> array(array('InArray', array('haystack' => array_keys($optSync)))),
			'paymentType' 		=> array(array('InArray', array('haystack' => array_keys($optPaymentType)))),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
				
			$select = $this->_db->select()
			->from(
				array('X_PAYMENT_DETAIL'),
				array('PAYREFF' => 'INVOICE_NO','INSTITUSI','INVOICE_NO','INVOICE_NAME','AMOUNT','TRX_DATE','LAST_UPDATE','SYNC','PAYMENT_TYPE','PAID_STATUS')
			)
			->order($sortBy.' '.$sortDir)
			;
				
			if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
			}else{
				if (!empty($paymentDateStart)){
					$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
				}
		
				if (!empty($paymentDateEnd)){
					$select->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				}
			}
				
			if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
				$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
			}else{
				if (!empty($lastUpdateStart)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
				}
					
				if (!empty($lastUpdateEnd)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}
			}
				
			if (!empty($institusi)){
				$select->where('INSTITUSI = ?', $institusi);
			}
			if (!empty($sync)){
				$select->where('SYNC = ?', $sync);
			}
			if (!empty($paymentType)){
				$select->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
			}
			
			$datadb = $this->_db->fetchAll($select);
			$data = array();
			
			foreach ($datadb as $row => $arr){
				foreach ($arr as $key => $val){
					if ($key == 'PAYREFF'){
						$data[$row][$key] = $this->generateRandomString(9);
					}else{
						$data[$row][$key] = $val;
					}
				}
			}
		}
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->optSync 		= $optSync;
		$this->view->optPaymentType = $optPaymentType;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
//======================= QUERY GRAPHIC PEMDA Papua ==================================		
				$select = $this->_db->select()
					->from(
						array('XP'=>'X_PAYMENT_DETAIL'),
						array('XP.INSTITUSI')
					)
					->where('XP.PAID_STATUS = "Y"')
					//->order($sortBy.' '.$sortDir)
					->joinLeft(array('XM'=>'X_M_INSTITUSI'),'XM.INSTITUSI = XP.INSTITUSI', array('AREA'=>'XM.NAMA','XM.NAMA','AMOUNT'=>'SUM(XP.AMOUNT)'))
					->group(array('XP.INSTITUSI'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$select->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$select->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$select->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$select->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}

				$data = $this->_db->fetchAll($select);
				$this->view->summary = $data;
				
				//echo "</br>".$select;
				//Zend_Debug::dump($data);
				//die("");
				
//======================= QUERY GRAPHIC RESTRIBUSI PASAR ==================================
				$pasar = $this->_db->select()
					->from(array('X' => 'X_PAYMENT_DETAIL'),
						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					->where('X.PAYMENT_TYPE = \'Retribusi Pasar\'')
					->where('X.PAID_STATUS = "Y"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.AREA'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$pasar->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$pasar->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$pasar->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$pasar->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$pasar->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$pasar->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$pasar->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$pasar->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$pasar->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($pasar);
				$this->view->pasar = $data;
				
				//echo "</br>".$pasar;
				//Zend_Debug::dump($data);
				//die("123");
				
//======================= QUERY GRAPHIC RESTRIBUSI RUSUN ==================================
				$rusun = $this->_db->select()
					->from(array('X' => 'X_PAYMENT_DETAIL'),
						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					->where('X.PAYMENT_TYPE = \'Retribusi Rusun\'')
					->where('X.PAID_STATUS = "Y"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.AREA'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rusun->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rusun->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rusun->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rusun->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rusun->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rusun->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rusun->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rusun->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rusun->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rusun);
				$this->view->rusun = $data;
				//Zend_Debug::dump($data);
				//die("123");

//======================= QUERY GRAPHIC SAMSAT Papua ==================================				
				$samsat = $this->_db->select()
					->from(array('X' => 'X_PAYMENT_DETAIL'),
						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					->where('X.PAYMENT_TYPE = \'RETRIBUSI SAMSAT\'')
					->where('X.PAID_STATUS = "Y"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.AREA'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$samsat->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$samsat->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$samsat->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$samsat->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$samsat->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$samsat->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$samsat->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$samsat->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$samsat->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($samsat);
				$this->view->samsat = $data;
//				Zend_Debug::dump($data);
//				die("123");
				
//======================= QUERY GRAPHIC KIR Papua ==================================				
				$kir = $this->_db->select()
					->from(array('X' => 'X_PAYMENT_DETAIL'),
						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					->where('X.PAYMENT_TYPE = \'RETRIBUSI KIR\'')
					->where('X.PAID_STATUS = "Y"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.AREA'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$kir->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$kir->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$kir->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$kir->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$kir->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$kir->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$kir->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$kir->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$kir->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($kir);
				$this->view->kir = $data;
				//Zend_Debug::dump($data);
				//die("123");				
				
//======================= QUERY GRAPHIC RSUD Papua ==================================				
				$rsud = $this->_db->select()
					->from(array('X' => 'X_PAYMENT_DETAIL'),
						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					->where('X.PAYMENT_TYPE = \'RSUD\'')
					->where('X.PAID_STATUS = "Y"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.AREA'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rsud);
				$this->view->rsud = $data;
				//Zend_Debug::dump($data);
				//die("123");				
				
//======================= QUERY GRAPHIC DISHUB ==================================				
				$rsud = $this->_db->select()
					->from(array('X' => 'X_PAYMENT_DETAIL'),
						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					->where('X.PAYMENT_TYPE = \''.strtoupper('Retribusi Perhubungan').'\'')
					->where('X.PAID_STATUS = "Y"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.AREA'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rsud);
				$this->view->dishub = $data;
				
//======================= QUERY GRAPHIC DKP ==================================				
				$rsud = $this->_db->select()
					->from(array('X' => 'X_PAYMENT_DETAIL'),
						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					->where('X.PAYMENT_TYPE = \''.strtoupper('Retribusi Perikanan').'\'')
					->where('X.PAID_STATUS = "Y"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.AREA'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				//echo $rsud;
				$data = $this->_db->fetchAll($rsud);
				$this->view->kp = $data;
				//Zend_Debug::dump($data);
				//die("123");
				
				//======================= QUERY GRAPHIC DISHUB ==================================
				$rsud = $this->_db->select()
				->from(array('X' => 'X_PAYMENT_DETAIL'),
						array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.INVOICE_NAME)')
				)
				->where('X.PAYMENT_TYPE = \''.strtoupper('Retribusi Perhubungan').'\'')
				->where('X.PAID_STATUS = "Y"')
				//->order($sortBy.' '.$sortDir)
				//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
				->group(array('X.AREA'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
					$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart).' AND LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
					//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart));
					}
						
					if (!empty($paymentDateEnd)){
						$rsud->where('LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
				
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rsud);
				$this->view->weight = $data;

//======================= QUERY GRAPHIC COA ==================================				
				$rsud = $this->_db->select()
					->from(
						array('X' => 'X_PAYMENT_DETAIL'),
						array(
							'X.INSTITUSI', 
							'AREA'			=>	'X.PAYMENT_TYPE', 
							'AMOUNT'		=>	'SUM(X.AMOUNT)',
							'MONTHXYEAR'	=>	'CONCAT(MONTHNAME(X.LAST_UPDATE)," ",YEAR(X.LAST_UPDATE))'
						)
						   
					)
					//->where('X.PAYMENT_TYPE = \'Rertribusi Perikanan\'')
					->where('X.PAID_STATUS = "Y"')
					->where('NOT ISNULL (MONTHNAME(X.LAST_UPDATE))')
					->where('YEAR(X.LAST_UPDATE) = "2014"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.PAYMENT_TYPE', 'MONTHXYEAR'));
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart).' AND LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
// 				echo $rsud;
				$data = $this->_db->fetchAll($rsud);
				$this->view->coa = $data;
				//Zend_Debug::dump($data);
				//die("123");	
							
//======================= QUERY GRAPHIC DEVICE ==================================				
				$rsud = $this->_db->select()
					->from(array('X' => 'X_PAYMENT_DETAIL'),
						   array(
						   	'X.INSTITUSI', 
						   	'AREA'=>'X.PAID_FROM', 
						   	'AMOUNT'=>'SUM(X.AMOUNT)',
						   	'MONTHXYEAR'	=>	'CONCAT(MONTHNAME(X.LAST_UPDATE)," ",YEAR(X.LAST_UPDATE))'
						   )
					   )
					//->where('X.PAYMENT_TYPE = \'Rertribusi Perikanan\'')
					->where('X.PAID_STATUS = "Y"')
					->where('NOT ISNULL (MONTHNAME(X.LAST_UPDATE))')
					->where('YEAR(X.LAST_UPDATE) = "2014"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('X.PAID_FROM','MONTHXYEAR'));
					
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart).' AND LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rsud);
				$this->view->dev = $data;
				//Zend_Debug::dump($data);
				//die("123");				
				$data = $this->_db->query('
					SELECT
					XP.INSTITUSI,
					XM.NAMA,
					MONTHNAME(XP.TRX_DATE) AS "MONTH",
					WEEK(XP.TRX_DATE) AS "WEEK",
					SUM(XP.AMOUNT) AS AMOUNT
					FROM 
					X_PAYMENT_DETAIL XP
					LEFT JOIN X_M_INSTITUSI XM ON XM.INSTITUSI = XP.INSTITUSI
					GROUP BY 
					XP.INSTITUSI, 
					XM.NAMA, 
					WEEK
				')->fetchAll();
				$this->view->group = $data;
			
			Application_Helper_General::writeLog('DTRX','View Transaction Report Chart');
		}
	
	public function detailAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
				'payReff'	=> array(
						'field'	=> 'INVOICE_NO',
						'label'	=> 'Payment Ref',
						'sortable'	=> false
				),
				'institusi'	=> array(
						'field' => 'INSTITUSI',
						'label' => 'Institusi',
						'sortable'	=> true
				),
				'invoice'	=> array(
						'field' => 'INVOICE_NO',
						'label' => 'Invoice',
						'sortable' => true
				),
				'invoiceName'	=> array(
						'field' => 'INVOICE_NAME',
						'label' => 'Invoice Name',
						'sortable' => true
				),
				'amount' => array(
						'field' => 'AMOUNT',
						'label' => 'Amount',
						'sortable' => true
				),
				'trxDate'	=> array(
						'field' => 'TRX_DATE',
						'label' => 'Transaction Date',
						'sortable' => true
				),
				'lastUpdate'	=> array(
						'field' => 'LAST_UPDATE',
						'label' => 'Last Update',
						'sortable' => true
				),
				'sync' 					 => array	(
						'field' => 'SYNC',
						'label' => 'Sync.',
						'sortable' => true
				),
				'paymentType'	=> array	(
						'field' => 'PAYMENT_TYPE',
						'label' => 'Payment Type',
						'sortable' => true
				),
				'paid'	=> array	(
						'field' => 'PAID_STATUS',
						'label' => 'Paid Status',
						'sortable' => true,
				),		
				'paid_from'	=> array	(
						'field' => 'PAID_FROM',
						'label' => 'Paid From',
						'sortable' => true
				),
		);
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($fields, 'field');
		
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[1]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$optSync = array(
				'' => '-- Please Select --',
				'Y' => 'Yes',
				'N' => 'No',
		);
		
		$select = $this->_db->select()
		->from(
				array('X_M_INSTITUSI'),
				array('INSTITUSI','NAMA')
		)
		->group('NAMA')
		;
		$opt = $this->_db->fetchAll($select);
		$optPaymentType = array(''=>'-- Please Select --');
		foreach ($opt as $key){
			$optPaymentType[$key['INSTITUSI']] = $key['NAMA'];
		}
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
				'paymentDateStart' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'paymentDateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'lastUpdateStart'	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'lastUpdateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'paymentStart' 		=> array(new Zend_Validate_Date($this->_dateDBFormat)),
				'institusi'			=> array(),
				'sync' 				=> array(array('InArray', array('haystack' => array_keys($optSync)))),
				'paymentType' 		=> array(array('InArray', array('haystack' => array_keys($optPaymentType)))),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
				
			$select = $this->_db->select()
			->from(
					array('X_PAYMENT_DETAIL'),
					array('PAYREFF' => 'INVOICE_NO','INSTITUSI','INVOICE_NO','INVOICE_NAME','AMOUNT','TRX_DATE','LAST_UPDATE','SYNC','PAYMENT_TYPE','PAID_STATUS','PAID_FROM')
			)
			->order($sortBy.' '.$sortDir)
			;
				
			if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
			}else{
				if (!empty($paymentDateStart)){
					$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
				}
		
				if (!empty($paymentDateEnd)){
					$select->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				}
			}
				
			if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
				$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
			}else{
				if (!empty($lastUpdateStart)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
				}
					
				if (!empty($lastUpdateEnd)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}
			}
				
			if (!empty($institusi)){
				$select->where('INSTITUSI = ?', $institusi);
			}
			if (!empty($sync)){
				$select->where('SYNC = ?', $sync);
			}
			if (!empty($paymentType)){
				$select->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
			}
			
// 			echo $select;
			$datadb = $this->_db->fetchAll($select);
			$data = array();
			foreach ($datadb as $row => $arr){
				foreach ($arr as $key => $val){
					if ($key == 'PAYREFF'){
						$data[$row]['ID'] = $val;
						$data[$row][$key] = $this->generateRandomString(9);
					}else{
						$data[$row][$key] = $val;
					}
				}
			}
		}
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->optSync 		= $optSync;
		$this->view->optPaymentType = $optPaymentType;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
		$this->paging($data);
		
		$csv = $this->_request->getParam('csv',NULL);
		$pdf = $this->_request->getParam('pdf',NULL);
		
		if ($csv || $pdf){
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			if($csv){
				$this->_helper->download->csv($header,$data,null,'List View Payment');
				Application_Helper_General::writeLog('DARC','Export CSV View Payment');
			}elseif($pdf){
				$this->_helper->download->pdf($header,$data,null,'List View Payment');
				Application_Helper_General::writeLog('DARC','Export PDF View Payment');
			}
		}else{
			Application_Helper_General::writeLog('DARC','View FSCM Payment Report');
		}
		Application_Helper_General::writeLog('DARC','Viewing Payment Report Detail');
	}
}
