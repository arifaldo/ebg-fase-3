<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'Service/Token.php'; //added new

class payment_EducationController extends Application_Main
{
	
	public function initModel()
    { 
		$this->model 					= new payment_Model_Payment();
		$this->param['SERVICE_TYPE'] 	= 10;
		$this->param['PROVIDER_TYPE'] 	= 1;
		$this->param['fetch'] 			= 'fetchAll';
		$this->param['USER_ID'] 		= $this->_userIdLogin;
		$this->CustomerUser 			= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    }
	
	public function indexAction() 
	{	
		$this->_helper->layout()->setLayout('newlayout');

		$biller_id = $this->_db->fetchAll(
						$this->_db->select()
					->from(array('A'=>'M_SERVICE_PROVIDER'),array('A.PROVIDER_ID'))
					->joinLeft(array('B' => 'M_SERVICES_TYPE'),'A.SERVICE_TYPE = B.SERVICE_ID',array())
					->where('B.SERVICE_ID = 1')
				);
		
		$select5 = $this->_db->fetchAll(
						$this->_db->select()
					->from(array('A'=>'T_PSLIP'),array('B.BILLER_ORDER_ID','A.PS_EFDATE','A.PS_TOTAL_AMOUNT'))
					->joinLeft(array('B' => 'T_TRANSACTION'),'A.PS_NUMBER = B.PS_NUMBER',array())
					->where('A.CUST_ID = '.$this->_db->quote($this->_custIdLogin))
					->where('PS_BILLER_ID IN (?)',array($biller_id))
					->order('PS_EFDATE DESC')
					->limit(5)
				);
		// die($select5);
		$this->view->historyData = $select5;
		
		$sessionNamespace 		= new Zend_Session_Namespace('creditcard');
		$paramSession 			= $sessionNamespace->paramSession;
		
		$paramac = array(
				'CCY_IN' => 'IDR'
		);
		$AccArr 	  			= $this->CustomerUser->getAccounts($paramac);
		$arrgroup 				= Application_Helper_Array::listArray($AccArr,'ACCT_NO','ACCT_NO');
		$this->view->AccArr 	= $AccArr;
		
		$this->view->radioCheck	= 1;
		$anyValue = '-- '.$this->language->_('Any Value'). ' --';
		$arr 						= $this->model->getProviderId(10,1);
		$providerArr 				= array(''=>$anyValue);
		$providerArr 				+= Application_Helper_Array::listArray($arr,'PROVIDER_ID','PROVIDER_NAME');
		$this->view->providerArr 	= $providerArr;
		
		$list 						= $this->model->cekList($this->param);
		$this->view->paymentList 	= $list;
		
		//added token
		$userData = $this->_db->select()
		->from(array('M_USER'),array('USER_ID','TOKEN_TYPE','TOKEN_ID','USER_MOBILE_PHONE'))
		->where('USER_ID = ?', $this->_userIdLogin)
		->limit(1)
		;
		$userData = $this->_db->fetchRow($userData);

		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);

		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();
		$this->view->challengeCode = $challengeCode;
		$this->view->challengeCodeReq = $challengeCode;
		
		$submit = $this->_getParam('submit');
		$submit1 = $this->_getParam('submit1');
		
		if($submit == TRUE)
		{
			$this->cancel();
		}
		else if($submit1 == TRUE)
		{
			$msg = array();
			$validator = new Payment(null,null,$this->_userIdLogin);
			$education = $validator->education($this->_request->getParams(), $msg, $msg1);
			
			$param['operator']		= $this->_getParam('operator');
			$param['orderId']		= $this->_getParam('orderId');
			$param['payment']		= $this->_getParam('payment');
			
			$param['ACCTSRC']		= $this->_getParam('ACCTSRC');
			$param['cek']			= $this->_getParam('cek');
			$param['radioCheck']	= $this->_getParam('radioCheck');
			$param['amount']	 	= Application_Helper_General::convertDisplayMoney($this->_getParam('amount'));
			$param['amountconvert']	 	= $this->_getParam('amount');
			$listProviderName = $this->model->getProviderName($param['operator'], 1);
			$providerName = $listProviderName['PROVIDER_NAME'];
			
			$sgo_product_code_data	= $this->model->getSgoProductCode($param['operator']);
			$sgo_product_code = $sgo_product_code_data['0']['SGO_PRODUCT_CODE'];
			
			$amount = (($param['amount'] < 1)) ? '0' : $param['amount'];

			$settings = new Application_Settings();
			$ccyList  = $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
			
			$paramPayment = array(	"_dateFormat"	=> $this->_dateDisplayFormat,
									"_dateDBFormat"	=> $this->_dateDBFormat,
								);

			$paramTrxArr[0] = array("TRA_AMOUNT" 	=> $amount,
									"ACCTSRC" 		=> $param['ACCTSRC'],	
								 );		
//			$paramSettingID = array('range_futuredate', 'auto_release_payment');								 
//							
//			$settings->setSettings(null, $paramSettingID);	// Zend_Registry => 'APPSETTINGS'				 
			
			
			$validate   = new ValidatePaymentSingle($this->_userIdLogin);
							
			$resultVal	= $validate->checkCreatePaymentPurchase($paramPayment, $paramTrxArr);
			
			$filter_acct 	= new Application_Filtering();
			$challengeCodeReq = $filter_acct->filter($this->_getParam('challengeCodeReq'), "challengeCodeReq");
			$responseCodeReq  = $filter_acct->filter($this->_getParam('responseCodeReq'), "responseCodeReq");
			
			//validasi hard token page 1 -- begin
			if($tokenType == '1'){ //sms token
				$resultToken = TRUE;
			}
			elseif($tokenType == '2'){ //hard token
				if(empty($responseCodeReq)){
					$errM = 'Error : Response Token cannot be left blank.';
				}
				else{
					$resHard = $HardToken->verifyHardToken($challengeCodeReq, $responseCodeReq);
					$resultToken = $resHard['ResponseCode'] == '0000';
					//set user lock token gagal
					if ($resHard['ResponseCode'] != '0000')
					{
						$tokenFailed = $this->CustomerUser->setFailedTokenMustLogout();
					}
				}
			}
			elseif($tokenType == '3'){ //mobile token
				$resultToken = TRUE;
			}
			else{$resultToken = TRUE;}
			//validasi hard token page 1 -- end
			
			
			if($education == TRUE && $resultVal == TRUE && $resultToken == TRUE)
			{
				$validateACCTSRC = new ValidateAccountSource($param['ACCTSRC'], $this->_userIdLogin); 

				$this->param['ACCT_NO'] = $param['ACCTSRC'];
				$detailCustomer	= $this->model->getCustomerData($this->param);
				$charges = $this->getCharges($param['operator']);
				if($charges)
				{
					$fee = $charges[$detailCustomer['CCY_ID']]['CHARGES_AMT'];
				}
				else
				{
					$fee = 0;
				}

				$params['amount']	=  0;
				/*
					TO DO GET AMOUNT FROM BILLER
				*/
				$total = $param['amount'] + $fee;
				$check = $validateACCTSRC->check($total);

				if($check)
				{
					
					$paramsearch['PROVIDER_ID'] 		= $param['operator'];
					$paramsearch['fetch'] 				= 'fetchRow';
					$arr 								= $this->model->getProvider($paramsearch);
					if(isset($param['cek']))
					{
						$paramsearch['USER_ID'] 		= $this->_userIdLogin;
						$paramsearch['REF_NO'] 			= $param['orderId'];
						$result 						= $this->model->cekList($paramsearch);
						
						if(!$result)
						{
							$paramSession['cek']			= $param['cek'];
						}
					}
					$paramSession['operator']			= $param['operator'];
					$paramSession['operatorNama']		= $providerName;
					
					if($param['radioCheck']==1)
					{
						$paramSession['orderId']			= $param['orderId'];
					}
					else if($param['radioCheck']==2)
					{
						$paramSession['orderId']			= $param['payment'];
					}
					
					$paramSession['ACCTSRC']			= $param['ACCTSRC'];
					$paramSession['ccy']				= $detailCustomer['CCY_ID'];
					$paramSession['ACCT_NAME']			= $detailCustomer['ACCT_NAME'];
					$paramSession['ACCT_ALIAS_NAME']	= $detailCustomer['ACCT_ALIAS_NAME'];
					$paramSession['ACCT_TYPE']			= $detailCustomer['ACCT_TYPE'];
					$paramSession['FREEZE_STATUS']		= $detailCustomer['FREEZE_STATUS'];
					$paramSession['MAXLIMIT']			= $detailCustomer['MAXLIMIT'];
					$paramSession['amount']				= $param['amount'];
					$paramSession['fee']				= $fee;
					$paramSession['total']				= $total;
					$paramSession['sgoProductCode']		= $sgo_product_code;
					//$paramSession['orderId']			= $param['orderId'];
					
					$sessionNamespace->paramSession = $paramSession;
					
					$this->_redirect('/payment/education/next');
				}
				else
				{
					$errMsg 			= $validateACCTSRC->getErrorMsg();
					$error				= true;
				}
			}
			else
			{
				$this->view->checkmakerlimit = $msg1;
				$errors 	= $msg;
				$error		= true;
				
				if($education == FALSE){
					
				}
				else{
					//check amount error core
					$errMessage 	= '';
					$errorMsg 		= (!empty($errorMsg)) ? $errorMsg : $validate->getErrorMsg();
					$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
					$validate->__destruct();
					unset($validate);
					if(!empty($errorMsg) || !empty($errorTrxMsg))
						$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));
					
					$this->view->error 		= true;
					$this->view->ERROR_MSG	= $errMessage;
				}
				
				if(count($msg) < 1){
					$this->view->ERROR_MSG_TOKEN = $errM;
				}
				else{
					foreach($msg as $key=>$err)
					{
						$xxx = 'x'.$key;
						foreach($err as $msg)
						{
							$this->view->$xxx = $msg;
						}
					}
				}

				if(empty($responseCodeReq)){}else{
					$this->view->ERROR_MSG_TOKEN = 'Error : Invalid Token APPLI 2';
				}
			}
			
			if($error)
			{
				$this->view->error 					= $error;
				$this->view->operatorErr 			= (isset($errors['operator']))? $errors['operator'] : null;
				$this->view->orderIdErr 			= (isset($errors['orderId']))? $errors['orderId'] : null;
				$this->view->paymentErr 			= (isset($errors['payment']))? $errors['payment'] : null;
				$this->view->ACCTSRCErr 			= (isset($errors['ACCTSRC']))? $errors['ACCTSRC'] : null;
				$this->view->amountErr 				= (isset($errors['amount']))? $errors['amount'] : null;
				$this->view->xACCTSRC 				= (isset($errMsg))? $errMsg : null;
				
				$this->view->operator				= $param['operator'];
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				$this->view->cek					= $param['cek'];
				$this->view->radioCheck				= $param['radioCheck'];
				$this->view->amount					= $param['amountconvert'];
				
				if($param['radioCheck']==1)
				{
					$this->view->orderId					= $param['orderId'];
				}
				else if($param['radioCheck']==2)
				{
					$this->view->payment					= $param['payment'];
				}
				
			}
		}
		else
		{
			unset($_SESSION['creditcard']);
		}
	Application_Helper_General::writeLog('MEDU','Education Payment');
	}
	
	public function nextAction() 
	{
		$this->view->userId	= $this->_userIdLogin; 
		$SinglePayment 		= new SinglePayment( null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('creditcard');
		$paramSession 		= $sessionNamespace->paramSession;
		$this->view->paramSession = $paramSession;
		//$model = new payment_Model_Payment();
		
		if(!$paramSession || $this->_getParam('submit') == true)
		{
			$this->cancel();
		}
		
		//agung tambahan benef account
		$select = $this->_db->select()
					 ->from(array('P' => 'M_PROVIDER_ACCT'));
		$select->where('P.PROVIDER_ID=?','1051');	
		$data = $this->_db->fetchRow($select);
		$CCY_ID = $data['CCY_ID'];
		$ACCT_NO = $data['ACCT_NO'];
		$ACCT_NAME = $data['ACCT_NAME'];
		$ACCT_ALIAS_NAME = $data['ACCT_ALIAS_NAME'];
		
		//added token
		$userData = $this->_db->select()
		->from(array('M_USER'),array('USER_ID','TOKEN_TYPE','TOKEN_ID'))
		->where('USER_ID = ?', $this->_userIdLogin)
		->limit(1)
		;
		$userData = $this->_db->fetchRow($userData);
		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		
		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		
		if($this->_getParam('submit1') == TRUE)
		{
			$PS_NUMBER 		= "P".$SinglePayment->generatePaymentID(1);
			$PS_EFDATE		= $this->model->date();
			
			$SinglePayment 	= new SinglePayment( $PS_NUMBER, $this->_userIdLogin);
			
			$setting 	= new Settings();
			$bank_code 	= $setting->getSetting('bank_code');
			
			$param	= array(
								'PS_CCY'			=> $paramSession['ccy'],
								'PS_TYPE'			=> 17,
								'PS_EFDATE'			=> Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat),
								'TRA_AMOUNT'		=> $paramSession['total'],
								'SOURCE_ACCOUNT'	=> $paramSession['ACCTSRC'],
								'TRA_MESSAGE'		=> $this->language->_('Education Payment'). ' = '.$paramSession['operatorNama'].' no = '.$PS_NUMBER.' '.$this->language->_('Customer ID').' = '.$this->_custIdLogin.' '.$this->language->_('User ID').' = '.$this->_userIdLogin,
								'_addBeneficiary'	=> $this->view->hasPrivilege('BADA'),
								'_beneLinkage'		=> $this->view->hasPrivilege('BLBU'),
								'_priviCreate'		=> 'MEDU',
								'PS_CATEGORY'		=> 'Payment',
								'PS_CATEGORY_EMAIL'	=> 'Education', //menu transaks
								'CHARGES'			=> $paramSession['fee'], //charges
								'PROVIDER_NAME'		=> $paramSession['operatorNama'],
			
								'PROVIDER_ID'		=> $paramSession['operator'],
								'REF_NO'			=> $paramSession['orderId'],
								'USER_BANK_CODE'	=> $bank_code,
								'BENEFICIARY_ACCOUNT_CCY' => $CCY_ID, //agung - added CCY_account
								'BENEFICIARY_ACCOUNT'	=> $ACCT_NO, //agung - added benef_account
								'BENEFICIARY_ACCOUNT_NAME'	=> $ACCT_NAME, //agung - added benef_account_name
								'BENEFICIARY_ALIAS_NAME'	=> $ACCT_ALIAS_NAME, //agung - added benef_account_alias_name
								'USER_ID'	=> $this->_userIdLogin, //agung - added benef_account_alias_name
								'DETAIL'			=>	array(
																'0' => array(
																				'PS_FIELDNAME' 	=> 'Provider Name',
																				'PS_FIELDTYPE'	=> '3',
																				'PS_FIELDVALUE'	=> $paramSession['operatorNama']
																			),
																'1' => array(
																						'PS_FIELDNAME' 	=> 'Customer Name',
																						'PS_FIELDTYPE'	=> '3',
																						'PS_FIELDVALUE'	=> $paramSession['ACCT_NAME']
																					),
																'2' => array(
																						'PS_FIELDNAME' 	=> "Students No",
																						'PS_FIELDTYPE'	=> '3',
																						'PS_FIELDVALUE'	=> $paramSession['orderId']
																					),
																'3' => array(
																						'PS_FIELDNAME' 	=> 'Amount',
																						'PS_FIELDTYPE'	=> '1',
																						'PS_FIELDVALUE'	=> $paramSession['amount']
																					),
																'4' => array(
																						'PS_FIELDNAME' 	=> 'Fee',
																						'PS_FIELDTYPE'	=> '1',
																						'PS_FIELDVALUE'	=> $paramSession['fee']
																					),
																'5' => array(
																						'PS_FIELDNAME' 	=> 'Total Payment',
																						'PS_FIELDTYPE'	=> '1',
																						'PS_FIELDVALUE'	=> $paramSession['total']
																					),
																'6' => array(
																						'PS_FIELDNAME' 	=> 'Type Of Transaction',
																						'PS_FIELDTYPE'	=> '1',
																						'PS_FIELDVALUE'	=> 'Education Payment'
																					),
															)
							);
			if(isset($paramSession['cek']))
			{
				$paramcek['cek'] = $paramSession['cek'];
				$param +=$paramcek;
			}

		//token validasi here added agung
			$filters = array('*' => array('HtmlEntities', 'StripTags', 'StringTrim'));
				$validators = array(
					'responseCode' => array(
						'notEmpty',
						'alnum',
						'presence' => 'required',
					)
				);
		
			$filter = new Zend_Filter_Input($filters, $validators, $this->_getAllParams(), array('breakChainOnFailure' => true));
			//Zend_Debug::dump($filter);
				if ($filter->isValid()){
	
					$responseCode = $filter->getEscaped('responseCode');

					$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);
					if (isset($userMobilePhone) || !(empty($userMobilePhone))){
						
						$Settings = new Settings();
						$message = $Settings->getSetting('token_confirm_message');
							
						$trans = array("[Response_Code]" => $responseCode);
						$message = strtr($message, $trans);
						
						$token = new Service_Token(NULL,$this->_userIdLogin);
						$token->setMsisdn($userData['USER_MOBILE_PHONE']);
						$token->setMessage($message);
						
						if($tokenType == '1'){ //sms token
							$res = $token->confirm();
							$resultToken = $res['ResponseCode'] == '0000';
						}
						elseif($tokenType == '2'){ //hard token
							$resHard = $HardToken->verifyHardToken($challengeCode, $responseCode);
							$resultToken = $resHard['ResponseCode'] == '0000';
							//set user lock token gagal
							if ($resultToken['ResponseCode'] != '0000')
							{
								$tokenFailed = $this->CustomerUser->setFailedTokenMustLogout();
							}	
						}
						elseif($tokenType == '3'){ //mobile token
						}	
							
						if ($resultToken){
							$return = $SinglePayment->createPembelianPembayaran($param);
						}else{
							$error_msg	 			= 'Invalid Token';
							$this->view->error 		= true;
							$this->view->ERROR_MSG	= $error_msg;	
						}
					}
				}
				else{
							$error_msg	 			= 'Token cannot be left blank';
							$this->view->error 		= true;
							$this->view->ERROR_MSG	= $error_msg;	
				}
			
			if($return)
			{				
				$params['orderId'] = $paramSession['orderId'];
				$params['productCode'] = $paramSession['sgoProductCode'];
				$params['amount'] = $paramSession['total'];
				$params['sourceAccountNo'] = $paramSession['ACCTSRC'];
				$params['sourceAccountName'] = $paramSession['ACCT_NAME'];
				$params['destinationAccountNo'] = $ACCT_NO;
				$params['destinationAccountName'] = $ACCT_NAME;

				$biller = new Service_Biller();
				$res = $biller->PaymentNotification($params);
							
				if ($res['ResponseCode'] == '00'){
					$paramSession['PSNumber'] 		= $PS_NUMBER;
					$paramSession['date']			= $PS_EFDATE;
					$paramSession['paymentMessage']	= 'Payment Success';
					$sessionNamespace->paramSession = $paramSession;
					
					$this->_redirect('/payment/education/confirm');	
				}else{
					$SinglePayment->setPaymentException($param);
					$paramSession['PSNumber'] 		= $PS_NUMBER;
					$paramSession['date']			= $PS_EFDATE;
					$paramSession['paymentMessage']	= 'Payment Success, Please Call Customer Care';
					$sessionNamespace->paramSession = $paramSession;
					
					$this->_redirect('/payment/education/confirm');
				}
				
			}
		}
		else if ($this->_getParam('submit') == true)
		{
			$this->_redirect('/payment/education/index');
		}
	}
	
	public function confirmAction() 
	{
		$pdf = $this->_getParam('pdf');
		$sessionNamespace 	= new Zend_Session_Namespace('creditcard');
		$paramSession 		= $sessionNamespace->paramSession;
		$this->view->ERROR_MSG_N	= $paramSession['paymentMessage'];
		$this->view->paramSession = $paramSession;
		
		if($pdf)
			{
//				$getPaymentDetail 	= new paymentreport_Model_Paymentreport();
//				$pslipdetail = $getPaymentDetail->getPslipDetail($paramSession['PSNumber']);
//				$htmldataDetailDetail = '';
//				foreach($pslipdetail as $pslipdetaillist)
//				{
//				   	if($pslipdetaillist['PS_FIELDTYPE'] == 1)
//				   	{
//				   		$value = Application_Helper_General::displayMoney($pslipdetaillist['PS_FIELDVALUE']);
//				   	}
//					elseif($pslipdetaillist['PS_FIELDTYPE'] == 2)
//				   	{
//				   		$value = Application_Helper_General::convertDate($pslipdetaillist['PS_FIELDVALUE'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
//				   	}
//				   	elseif($pslipdetaillist['PS_FIELDTYPE'] == 3)
//				   	{
//				   		$value = $pslipdetaillist['PS_FIELDVALUE'];
//				   	}
//				   	else
//				   	{
//				   		$value = '';
//				   	}
//				   	
//				   	$htmldataDetailDetail .='
//					<tr>
//						<td class="tbl-evencontent">&nbsp; '.$pslipdetaillist['PS_FIELDNAME'].'</td>
//						<td class="tbl-evencontent">:</td>
//						<td class="tbl-evencontent">'.$value.'</td>
//					</tr>';
//				}
//				
//				$htmldataDetail = 
//				'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="400px">
//					<tr>
//						<td class="tbl-evencontent">&nbsp; Payment Ref</td>
//						<td class="tbl-evencontent">:</td>
//						<td class="tbl-evencontent">'.$paramSession['PSNumber'].'</td>
//					</tr>
//					<tr>
//						<td class="tbl-evencontent">&nbsp; Date Time </td>
//						<td class="tbl-evencontent">:</td>
//						<td class="tbl-evencontent">'.$paramSession['date'].'</td>
//					</tr>
//					<tr>
//						<td class="tbl-evencontent">&nbsp; Source Account</td>
//						<td class="tbl-evencontent">:</td>
//						<td class="tbl-evencontent">'.$paramSession['ACCTSRC'].'</td>
//					</tr>
//					'.$htmldataDetailDetail.'
//				</table>';
//		
//						
//				$datapdf 	= 
//							"<h2>View Payment</h2><br/><br/>
//							<h2>Transfer From</h2><hr></hr>
//							".$htmldataDetail;
//				
//				$datapdf = "<tr><td>".$datapdf."</td></tr>";
//				$this->_helper->download->pdf(null,null,null,'Payment Report Detail',$datapdf);	

				$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($paramSession['PSNumber'], $paramSession);
				$this->_helper->download->pdf(null,null,null,'Payment Report Detail',$datapdf);
			
			}
	}
	
	public function cancel() 
	{
		unset($_SESSION['creditcard']);
		$this->_redirect("/home/index");
	}
}
?>