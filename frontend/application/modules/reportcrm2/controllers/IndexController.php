<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';
class reportcrm2_IndexController extends Application_Main {


    public function indexnewAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        
        
        
        
        $frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'TABLELIST';
        
        $tableList = $cache->load($cacheID);
        //var_dump($select_int);
        if(empty($tableList)){
                $select = $this->_db->select()
                            ->from('information_schema.tables', array('table_name'))
                            ->where('table_schema = ?', 'digitalbanking360');

            $tableList = $this->_db->fetchAll($select);
            
            $cache->save($tableList,$cacheID);
        }
        
        
        $filterlist = array('PS_NUMBER', 'PS_SUBJECT', 'SOURCE_BANK','SOURCE_ACCOUNT', 'BENEFICIARY_ACCOUNT','BENEFICIARY_BANK', 'PS_STATUS', 'PS_TYPE', 'TRANSFER_TYPE');

        $this->view->filterlist = $filterlist;

        $filterlist2 = array('PS_NUMBER', 'PS_SUBJECT','PS_CREATED','PS_UPDATED','PS_EFDATE','PS_CREATEDBY','TRANSACTION_ID','SOURCE_BANK','SOURCE_ACCOUNT', 'BENEFICIARY_CATEGORY', 'BENEFICIARY_ACCOUNT', 'BENEFICIARY_BANK', 'TRA_AMOUNT', 'PS_STATUS', 'PS_TYPE', 'TRANSFER_TYPE');

        $this->view->filterlist2 = $filterlist2;
        $this->view->custID = $this->_custIdLogin;

       
        
        
        
        
        $frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'TABLECOLOMN';
        
        $tempColumn = $cache->load($cacheID);
        //var_dump($select_int);
        if(empty($tempColumn)){
                $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'digitalbanking360')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                                    ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "PS_CREATED",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "PS_PERIODIC",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
            // echo $select;die;
            $tempColumn = $this->_db->fetchAll($select);
            
            $cache->save($tempColumn,$cacheID);
        }

        // echo "<pre>";
        // print_r($tempColumn);
         

        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFTIME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('PS_EFTIME ');
            // }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            // }else if($value['COLUMN_NAME'] == 'CUST_ID'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            // }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            // }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            // }else if($value['COLUMN_NAME'] == 'PS_CCY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            // }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            // }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'BENEF_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('BENEF_ACCT_BANK_CODE ');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS2'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('BENEFICIARY_ADDRESS2 ');
            // }else if($value['COLUMN_NAME'] == 'RATE'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            // }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            // }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            // }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            // }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            // }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            // }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            // }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            // }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            // }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            // }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            // }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                // $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            // }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            // }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            // }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            // }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
            //     $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                // $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
                // $tempColumn[$key]['COLOMN'] = "0";
                continue;
            }

        }

        $tempColumn['fx']['DATA_TYPE'] = 'function';
        $tempColumn['fx']['COLUMN_NAME'] = 'autonumber';
        $tempColumn['fx']['COLOMN'] = 'Auto Numbering (1,2,3,...)';

        // echo '<pre>';
        // print_r($tempColumn);die;

        $this->view->colomndata = $tempColumn;

        $this->view->countColomndata = count($tempColumn);
        $this->view->halfCountColomndata = count($tempColumn) / 2;

        $this->view->tableList = $tableList;

        //get tag from m_mapping
        $selectTag = $this->_db->select()
                     ->distinct()
                     ->from(array('T' => 'M_MAPPING'), 'T.TAG');

        $dataTag = $this->_db->fetchAll($selectTag);

        $opt = '<option value="autonumber">Auto Numbering</option>
                <option value="uid">Unique ID</option>';

        foreach ($dataTag as $key => $value) {
            $opt .= '<option value="'.$value['TAG'].'_mapping">'.$value['TAG'].' Auto Mapping</option>'; 
        }

        $this->view->functionOpt = $opt;
        
        if($this->_request->isPost()){


            $params     = $this->_request->getParams();
            // print_r($params);die;
            $filters    = array(
                                'report_type' => array('StringTrim','StripTags','HtmlEntities'),
                                'save_as' => array('StringTrim','StripTags','HtmlEntities'),
                                'tablecols' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities'),
                                'datalimit' => array('StringTrim','StripTags','HtmlEntities'),
                                'wherecol' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereopt' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereval' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_name' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_email' => array('StringTrim','StripTags','HtmlEntities'),
                                'label' => array('StringTrim','StripTags','HtmlEntities'),
                                'colomn' => array('StringTrim','StripTags','HtmlEntities'),
                                'type' => array('StringTrim','StripTags','HtmlEntities'),
                                'periodic' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_schedule' => array('StringTrim','StripTags','HtmlEntities'),
                                'repeat_every' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_date' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_day' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_time' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_month' => array('StringTrim','StripTags','HtmlEntities'),
                                'repeat_end' => array('StringTrim','StripTags','HtmlEntities'),
                                // 'sortby' => array('StringTrim','StripTags','HtmlEntities'),
                                'sort1' => array('StringTrim','StripTags','HtmlEntities'),
                                'sort1Direction' => array('StringTrim','StripTags','HtmlEntities'),
                                'sort2' => array('StringTrim','StripTags','HtmlEntities'),
                                'sort2Direction' => array('StringTrim','StripTags','HtmlEntities'),
                                'repeat_start' => array('StringTrim','StripTags','HtmlEntities'),
                                'repeat_end' => array('StringTrim','StripTags','HtmlEntities')


            );
 
            $validators =  array(
                                'report_type' => array(),
                                'save_as'      => array(),
                                'tablecols'      => array('allowEmpty'=>true),
                                'sortasc'        => array('allowEmpty'=>true),
                                'sortdesc'       => array('allowEmpty'=>true),
                                'wherecol'       => array('allowEmpty'=>true),
                                'whereopt'       => array('allowEmpty'=>true),
                                'whereval'          => array('allowEmpty'=>true
                                                            // new Zend_Validate_Regex(array('pattern' => '/^[0-9A-Za-z\\s-_.]+$/')),
                                                            // 'messages' => array('Invalid report condition')
                                                    ),
                                'datalimit'         => array('allowEmpty' => true,
                                                            'Digits',
                                                            'messages' => array('Invalid data limit format')
                                                    ),
                                'report_name'       => array('NotEmpty',
                                                            array('StringLength',array('max'=>200)),
                                                            'messages' => array('Can not be empty',
                                                                            'Report name length cannot be more than 200',
                                                                        )
                                                    ),
                                'label'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),

                                'colomn'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'type'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'report_email'      => array('allowEmpty'=>true,
                                                            array('StringLength',array('max'=>128)),
                                                            'messages' => array(//'Can not be empty',
                                                                            'Email length cannot be more than 128',
                                                                        )
                                                    ),
                                'report_schedule'   => array('allowEmpty'=>true,
                                                            'Alpha',
                                                            'messages' => array(//'Can not be empty',
                                                                                'Invalid schedule'
                                                                        )
                                                    ),
                                'periodic'       => array('allowEmpty'=>true),
                                'repeat_every'       => array('allowEmpty'=>true),
                                'report_date'       => array('allowEmpty'=>true),
                                'report_day'       => array('allowEmpty'=>false),
                                'report_time'       => array('allowEmpty'=>true),
                                'report_month'       => array('allowEmpty'=>true),
                                'repeat_end'       => array('allowEmpty'=>true),
                                // 'sortby'       => array('allowEmpty'=>true),
                                'sort1'       => array('allowEmpty'=>true),
                                'sort1Direction'       => array('allowEmpty'=>true),
                                'sort2'       => array('allowEmpty'=>true),
                                'sort2Direction'       => array('allowEmpty'=>true),
                                'repeat_start'       => array('allowEmpty'=>true),
                                'repeat_end'       => array('allowEmpty'=>true)


            );
            
            
            
            $dataParam = array("PS_SUBJECT","SOURCE_ACCOUNT", 'BENEFICIARY_ACCOUNT', "PS_NUMBER", "PS_STATUS", "PS_TYPE", "TRANSFER_TYPE");
            $dataParamValue = array();
            foreach ($dataParam as $dtParam) {

                // print_r($dtParam);die;
                if (!empty($this->_request->getParam('wherecol'))) {
                    $dataval = $this->_request->getParam('whereval');
                    foreach ($this->_request->getParam('wherecol') as $key => $value) {
                        if ($dtParam == $value) {
                            $dataParamValue[$dtParam] = $dataval[$key];
                        }
                    }
                }

                // $dataPost = $this->_request->getPost($dtParam);
                // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
            }

            if (!empty($this->_request->getParam('createdate'))) {
                $createarr = $this->_request->getParam('createdate');
                $dataParamValue['PS_CREATED'] = $createarr[0];
                $dataParamValue['PS_CREATED_END'] = $createarr[1];
            }
            if (!empty($this->_request->getParam('updatedate'))) {
                $updatearr = $this->_request->getParam('updatedate');
                $dataParamValue['PS_UPDATED'] = $updatearr[0];
                $dataParamValue['PS_UPDATED_END'] = $updatearr[1];
            }

            if (!empty($this->_request->getParam('efdate'))) {
                $efdatearr = $this->_request->getParam('efdate');
                $dataParamValue['PS_EFDATE'] = $efdatearr[0];
                $dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
            }

            $zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

            $cek_multiple_email = true;

            if($zf_filter->report_email)
            {
                $validate = new Validate();
                $cek_multiple_email = $validate->isValidEmailMultiple($zf_filter->report_email);
            }
            // echo 'here';die;
            if($zf_filter->isValid() && $cek_multiple_email == true)
            {   
                // print_r($zf_filter->sortasc);die;
                $selectedCols = implode(",", $zf_filter->colomn);
                // $sortAsc = implode(",", $zf_filter->sortasc);
                // $sortDesc = implode(",", $zf_filter->sortdesc);
                // print_r($selectedCols);

                $whereCols = $zf_filter->wherecol;
                $whereOpts = $zf_filter->whereopt;
                $whereVals = $zf_filter->whereval;

                $optsArr = array("EQUAL" => "=",
                                "NOT EQUAL" => "<>",
                                "LESS THAN" => "<",
                                "GREATER THAN" => ">",
                                "LESS THAN OR EQUAL TO" => "<=",
                                "GREATER THAN OR EQUAL TO" => ">="
                );

                // echo "<pre>";
                // print_r($whereCols);
                // print_r($whereVals);

                if(!empty($whereCols)){
                    $tempWhere = array();
                    $index = 0;
                    // $withoutDuplicates = array_unique(array_map("strtoupper", $whereCols));
                    // print_r($withoutDuplicates);
                    $row = 0;
                    // print_r($whereVals);
                    // print_r($whereCols);
                    foreach($whereCols as $key => $val){
                        $tempName = explode("-", $val);
                        $colName = $tempName[0];
                        // if($key%2==0){


                            // print_r($tempName);
                        if($whereOpts[$key] != "LIKE"){
                            $opt = $optsArr[$whereOpts[$index]];
                            
                            // print_r($opt);
                            $whereval = $whereCols[$key+1];
                        }
                        else{
                            $opt = $whereOpts[$index];
                            $whereval = "%".$whereCols[$key+1]."%";
                        }
                        // if($whereVals==''){
                        //     $whereval = 0;
                        // }
                        $duplicate = in_array($val, $whereCols);
                        // print_r($duplicate);
                        if($duplicate){
                            // print_r($whereval);
                            if(empty($whereVals[$row])){
                                $whereVals[$row] = 0;
                            }
                            if($row == '0'){
                                if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    //$val = Application_Helper_General::convertDate($whereVals[$row], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    
                                    if($colName == 'PS_CREATED'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_CREATED'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_CREATED_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    if($colName == 'PS_EFDATE'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_EFDATE_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    if($colName == 'PS_UPDATED'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_UPDATED'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_UPDATED_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    
                                    //$tempWhere[$row] .= " ".$colName." =< ".$val." AND ".$colName." => ".$val_end." ;";
                                    $tempWhere[$row] .= $colName." >= ".$val.";".$colName." <= ".$val_end.";";
                                    // $val = $whereVals[$row];
                                }else{
                                    $val = $whereVals[$row];
                                    var_dump($colName);echo $index;echo '-';
                                    // print_r($val);
                                    if($colName != '' && $val != ''){
                                        $tempWhere[$row] = $colName." = ".$val." ;";
                                    }else{
                                        $tempWhere[$row] = ";";
                                    }
                                }
                                
                            }else{
                                if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    //$val = Application_Helper_General::convertDate($whereVals[$row], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    if($colName == 'PS_CREATED'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_CREATED'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_CREATED_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    if($colName == 'PS_EFDATE'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_EFDATE_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    if($colName == 'PS_UPDATED'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_UPDATED'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_UPDATED_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    
                                    //$tempWhere[$row-1] .= " ".$colName." =< ".$val." AND ".$colName." => ".$val_end." ;";
                                    $tempWhere[$row-1] .= $colName." >= ".$val.";".$colName." <= ".$val_end." ; ";
                                     // $val = $whereVals[$row];
                                }else{
                                    //if($colName != '')
                                    $val = $whereVals[$row]; 
                                var_dump($colName);echo $index;echo '--';
                                    $tempWhere[$row-1] .= $colName." = ".$val." ;";
                                } 
                                 
                            }
                            // echo "dupl";
                            // print_r($tempWhere);
                            $row++;
                        }else{
                            if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    //$val = Application_Helper_General::convertDate($whereval, $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    // $val = $whereVals[$row];
                                    if($colName == 'PS_CREATED'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_CREATED'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_CREATED_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    if($colName == 'PS_EFDATE'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_EFDATE_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    if($colName == 'PS_UPDATED'){
                                        $val = Application_Helper_General::convertDate($dataParamValue['PS_UPDATED'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                        $val_end = Application_Helper_General::convertDate($dataParamValue['PS_UPDATED_END'], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    }
                                    
                                    
                                    $tempWhere[$row] .= $colName." >= ".$val.";".$colName." <= ".$val_end.";";
                                }else{
                                    $val = $whereVals[$row];
                                    // var_dump($colName);echo $index;echo '---';
                                    $tempWhere[$row] = $colName." = ".$val." ;";
                                }
                            // echo "nondup";
                            // print_r($tempWhere);
                            $row++;
                        }

                        $index++;
                        // }
                        // $index++;
                    }

                     //print_r($tempWhere);die; 

                    $wheres = implode(";",$tempWhere);
                    $wheres = str_replace(';;', ';', $wheres);
                }
                // print_r($zf_filter->report_data);




                // print_r($wheres);
               //  $report = $this->_request->getParam('report_radio');
               // if(empty($report)){
               //      $    
               // }

                if(empty($zf_filter->report_email)){
                    $schedule = null;
                }
                else{
                    if(empty($zf_filter->report_schedule))
                        $schedule = NULL;
                    else
                        $schedule = strtolower($zf_filter->report_schedule);
                }

                if(empty($zf_filter->datalimit))
                    $limit = 0;
                else
                    $limit = $zf_filter->datalimit;


                if(empty($zf_filter->repeat_start))
                    $reportstart = NULL;
                else
                    $reportstart = $zf_filter->repeat_start;
                

                if(empty($zf_filter->repeat_end))
                    $reportend = NULL;
                else
                    $reportend = $zf_filter->repeat_end;
			//	echo '<pre>';
               // var_dump($params);die;
                if($zf_filter->report_type == '2'){
					if($schedule == 'one'){
                    $reportstart = $this->_request->getParam('updatedate2')['0'];
                    $reportend = $this->_request->getParam('updatedate2')['1'];
					}else{
						$reportstart = $this->_request->getParam('repeat_start');
						$reportend = $this->_request->getParam('repeat_end');
					}
                }

                $insArr = array(
                                "REPORT_TYPE" => $zf_filter->report_type,
                                "REPORT_NAME" => $zf_filter->report_name,
                                "REPORT_FILE" => $zf_filter->save_as,
                                // "REPORT_TABLE" => $zf_filter->tablename,
                                "REPORT_COLUMNS" => $selectedCols,
                                "REPORT_WHERE" => $wheres,
                                // "REPORT_SORT_ASC" => $sortAsc,
                                // "REPORT_SORT_DESC" => $sortDesc,
                                "REPORT_LIMIT" => $limit,
                                "REPORT_EMAIL" => $zf_filter->report_email,
                                //"PERIODIC" => $zf_filter->periodic,
                                "PERIODIC" => 1,
                                "REPORT_SCHEDULE" => $schedule,
                                "REPORT_CUST" => $this->_custIdLogin,
                                "REPORT_CREATED" => new Zend_Db_Expr('now()'),
                                "REPORT_CREATEDBY" => $this->_userIdLogin,
                                "REPORT_START" => $reportstart,
                                "REPORT_END" => $reportend
                                //"REPORT_DATA" => $zf_filter->report_data

                );
                //  print_r($insArr);
                // die;
                // try{
                //     // echo $insArr['REPORT_CREATED'];
                //     echo new Zend_Db_Expr('now()');
                //     // if($insArr['REPORT_CREATED'] == 'now()'){
                //     //     $this->_db->insert('T_REPORT_GENERATOR',$insArr);
                //     // }
                                        

                // }catch(Exception $e){
                //     print_r($e);die;
                // }

                // print_r($insArr);die;

//                 if($zf_filter->sortby == '1'){
// // "REPORT_SORT_ASC" => $sortAsc,
//                     $insArr['REPORT_SORT_ASC'] =  $zf_filter->sortasc;
//                 }else if($zf_filter->sortby == '2'){
//                     $insArr['REPORT_SORT_DESC'] =  $zf_filter->sortdesc;
//                 }

                //if periodic != 0 (custom)
                //if ($zf_filter->periodic != 0) {

                    if($schedule == 'monthly'){
                        $insArr['REPEAT_EVERY'] =  $this->_request->getParam('selectrepeat');
                        //$insArr['REPORT_DATE'] =  $zf_filter->report_date;
                    }else if($schedule == 'weekly'){

                        $report_day = '';
                        $count = count($zf_filter->report_day);
                        $i = 1;
                        foreach ($zf_filter->report_day as $value) {
                            if ($i !== $count) {
                                $report_day .= $value.',';
                            }
                            else{
                                $report_day .= $value;
                            }
                            $i++;
                        }

                        $insArr['REPEAT_EVERY'] =  $zf_filter->repeat_every;
                        $insArr['REPORT_DAY'] =  $report_day;
                    }else if($schedule == 'daily'){
                        // $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                        
                    }else if($schedule == 'yearly'){
                        $insArr['REPORT_MONTH'] =  $zf_filter->report_month;
                        $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                    }
					
					

                 //   $insArr['REPEAT_END'] =  $zf_filter->repeat_end;
               // }
				
				if($schedule != 'one'){ 
						$insArr['REPORT_TIME'] =  $this->_request->getParam('PS_EFTIMEF');
				}

                if (!empty($zf_filter->sort1)) {
                    $insArr['REPORT_SORT1'] = $zf_filter->sort1.' '.$zf_filter->sort1Direction;
                }

                if (!empty($zf_filter->sort2)) {
                    $insArr['REPORT_SORT2'] = $zf_filter->sort2.' '.$zf_filter->sort2Direction;
                }
                
                if (!empty($this->_request->getParam('change_id'))) {
                    $source = $this->_request->getParam('change_id');
                    $sourcelist = '';
                    foreach($source as $key => $vals){
                        //var_dump($vals);
                        $sourcelist .= $vals.',';
                    }
                    $insArr['REPORT_SOURCE'] = $sourcelist;
                }
        //die;
                try{
                    $this->_db->beginTransaction();
                    //echo '<pre>';
                     //print_r($insArr);die;
                    $this->_db->insert('T_REPORT_GENERATOR',$insArr);

                    $lastId = $this->_db->lastInsertId();
					//var_dump($lastId);die;
                    // print_r($zf_filter->label);die;
                    if(!empty($zf_filter->label)){
                        foreach ($zf_filter->label as $key => $value) {
                                    // print_r($value);
                                if($zf_filter->type[$key]=='function'){
                                    $type = 5;
                                }
                                else if($zf_filter->type[$key]=='datetime'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='date'){
                                    $type = 3;
                                }elseif($zf_filter->type[$key]=='varchar'){
                                    $type = 1;
                                }elseif($zf_filter->type[$key]=='text'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='decimal' || $zf_filter->type[$key]=='int'){
                                    $type = 2;
                                }else{
                                    $type = 1;
                                }

                                $colmnArr = array("COLM_NAME" => $value,
                                    "COLM_FIELD" => $zf_filter->colomn[$key],
                                    "COLM_REPORD_ID" => $lastId,
                                    "COLM_INDEX" => $key,
                                    "COLM_TYPE" => $type
                                );
                                // print_r($colmnArr);die;
                                $this->_db->insert('T_REPORT_COLOMN',$colmnArr);
                        }

                    }
					
					if($schedule == 'daily' || $schedule == 'weekly'){
						
						foreach($this->_request->getParam('report_day') as $key => $valday  ){
								 $colmnArr = array(
                                    "REPORT_ID" => $lastId,
                                    "DAY_ID" => $valday
                                );
                                // print_r($colmnArr);die;
                                $this->_db->insert('T_REPORT_DAY',$colmnArr);		
						}
					}

                    Application_Helper_General::writeLog('ADRG','New report has been added, Report Name : '.$zf_filter->report_name. ' Creator : '.$this->_custIdLogin." | ".$this->_userIdLogin);
                    if($schedule == 'one'){
                        $queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
                        $insArr['ID'] = $lastId;
                        $emailQueueProducer = $queueService->getQueueByProfileName("REPORTGEN_PRODUCER");
                        $datarab = json_encode($insArr);
                        $emailQueueProducer->insertQueueItem($datarab);
                    }
                    $this->_db->commit();
                    $this->setbackURL('/reportcrm2/report');
                    $this->_redirect('/notification/success/index');
                }
                catch(Exception $e){
                     print_r($e);die;
                    $this->_db->rollBack();
                    $error_remark = $this->language->_('An Error Occured. Please Try Again');
                }
            // die;
                if(isset($error_remark))
                {
                    // die;
                    Application_Helper_General::writeLog('ADRG','Add Report');
                    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
                    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
                    $this->_redirect('/reportcrm2');
                }
            }
            else{
                $this->view->tablename = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $this->view->save_as = ($zf_filter->isValid('save_as'))? $zf_filter->save_as : $this->_getParam('save_as');
                $this->view->tablecols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $this->view->sortasc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $this->view->sortdesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');
                $this->view->datalimit = ($zf_filter->isValid('datalimit'))? $zf_filter->datalimit : $this->_getParam('datalimit');
                $this->view->wherecol = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                $this->view->whereopt = ($zf_filter->isValid('whereopt'))? $zf_filter->whereopt : $this->_getParam('whereopt');
                $this->view->whereval = ($zf_filter->isValid('whereval'))? $zf_filter->whereval : $this->_getParam('whereval');
                $this->view->report_name = ($zf_filter->isValid('report_name'))? $zf_filter->report_name : $this->_getParam('report_name');
                $this->view->report_email = ($zf_filter->isValid('report_email'))? $zf_filter->report_email : $this->_getParam('report_email');
                $this->view->report_schedule = ($zf_filter->isValid('report_schedule'))? $zf_filter->report_schedule : $this->_getParam('report_schedule');
                
                $this->view->repeat_start = ($zf_filter->isValid('repeat_start'))? $zf_filter->repeat_start : $this->_getParam('repeat_start');
                $this->view->repeat_end = ($zf_filter->isValid('repeat_end'))? $zf_filter->repeat_end : $this->_getParam('repeat_end');

                $error = $zf_filter->getMessages();
            //print_r($error);die;
                $errorArray = null;
                foreach($error as $keyRoot => $rowError)
                {
                   foreach($rowError as $errorString)
                   {
                      $errorArray[$keyRoot] = $errorString;
                   }
                }

                if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['report_email'] = 'Invalid email format';

                $this->view->error_msg = $errorArray;

                $tblName = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'digitalbanking360')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")');

                $tempColumn = $this->_db->fetchAll($select);

                $this->view->columnList = $tempColumn;

                $tblCols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $tblAsc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $tblDesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');

                $leftCols = array();
                $leftAsc = array();
                $leftDesc = array();
                foreach($tempColumn as $row){
                    if(!in_array($row['COLUMN_NAME'], $tblCols))
                        $leftCols[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblAsc))
                        $leftAsc[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblDesc))
                        $leftDesc[] = $row['COLUMN_NAME'];
                }

                $this->view->leftcols = $leftCols;
                $this->view->leftasc = $leftAsc;
                $this->view->leftdesc = $leftDesc;

                $whereCols = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                if(!empty($whereCols)){
                    $wherectr = count($whereCols)+1;
                }
                else{
                    $wherectr = 1;
                }

                $this->view->wherectr = $wherectr;
            }
        }
        else{
            $this->view->wherectr = 1;
        }
        
        
        if (!empty($dataParamValue)) {

            $this->view->createdStart = $dataParamValue['PS_CREATED'];
            $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
            $this->view->efdateStart = $dataParamValue['PS_UPDATED'];
            $this->view->efdateEnd = $dataParamValue['PS_UPDATED_END'];
            $this->view->efdateStart = $dataParamValue['PS_EFDATE'];
            $this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];


            unset($dataParamValue['PS_CREATED_END']);
            unset($dataParamValue['PS_EFDATE_END']);
            unset($dataParamValue['PS_UPDATED_END']);

            foreach ($dataParamValue as $key => $value) {
                $wherecol[] = $key;
                $whereval[] = $value;
            }
            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }
        
        
    }


    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $select = $this->_db->select()
                            ->from('information_schema.tables', array('table_name'))
                            ->where('table_schema = ?', 'digitalbanking360');

        $tableList = $this->_db->fetchAll($select);

        $report =  $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
                             ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                             ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin))
                     );
        foreach ($report as $key => $value) {
            $report[$key]['REPORT_CREATED'] = Application_Helper_General::convertDate($value['REPORT_CREATED'],$this->displayDateTimeFormat,$this->defaultDateFormat);
        }
        $this->view->report = $report;

         $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'digitalbanking360')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                                    ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "PS_CREATED",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "PS_PERIODIC",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
            // echo $select;die;
        $tempColumn = $this->_db->fetchAll($select);

        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }

        }

        $tempColumn['fx']['DATA_TYPE'] = 'function';
        $tempColumn['fx']['COLUMN_NAME'] = 'autonumber';
        $tempColumn['fx']['COLOMN'] = 'Auto Numbering (1,2,3,...)';

        // echo '<pre>';
        // print_r($tempColumn);die;

        $this->view->colomndata = $tempColumn;

        $this->view->countColomndata = count($tempColumn);
        $this->view->halfCountColomndata = count($tempColumn) / 2;

        $this->view->tableList = $tableList;

        //get tag from m_mapping
        $selectTag = $this->_db->select()
                     ->distinct()
                     ->from(array('T' => 'M_MAPPING'), 'T.TAG');

        $dataTag = $this->_db->fetchAll($selectTag);

        $opt = '<option value="autonumber">Auto Numbering</option>
                <option value="uid">Unique ID</option>';

        foreach ($dataTag as $key => $value) {
            $opt .= '<option value="'.$value['TAG'].'_mapping">'.$value['TAG'].' Auto Mapping</option>'; 
        }

        $this->view->functionOpt = $opt;
        
        if($this->_request->isPost()){

            $params     = $this->_request->getParams();
            // print_r($params);die;
            $filters    = array('save_as' => array('StringTrim','StripTags','HtmlEntities'),
                                'tablecols' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities'),
                                'datalimit' => array('StringTrim','StripTags','HtmlEntities'),
                                'wherecol' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereopt' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereval' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_name' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_email' => array('StringTrim','StripTags','HtmlEntities'),
                                'label' => array('StringTrim','StripTags','HtmlEntities'),
                                'colomn' => array('StringTrim','StripTags','HtmlEntities'),
                                'type' => array('StringTrim','StripTags','HtmlEntities'),
                                'periodic' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_schedule' => array('StringTrim','StripTags','HtmlEntities'),
                                'repeat_every' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_date' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_day' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_time' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_month' => array('StringTrim','StripTags','HtmlEntities'),
                                'repeat_end' => array('StringTrim','StripTags','HtmlEntities'),
                                // 'sortby' => array('StringTrim','StripTags','HtmlEntities'),
                                'sort1' => array('StringTrim','StripTags','HtmlEntities'),
                                'sort1Direction' => array('StringTrim','StripTags','HtmlEntities'),
                                'sort2' => array('StringTrim','StripTags','HtmlEntities'),
                                'sort2Direction' => array('StringTrim','StripTags','HtmlEntities'),
                                'repeat_start' => array('StringTrim','StripTags','HtmlEntities'),
                                'repeat_end' => array('StringTrim','StripTags','HtmlEntities')
                                
 

            );

            $validators =  array('save_as'      => array(),
                                'tablecols'      => array(),
                                'sortasc'        => array(),
                                'sortdesc'       => array(),
                                'wherecol'       => array(),
                                'whereopt'       => array(),
                                'whereval'          => array('allowEmpty'=>true
                                                            // new Zend_Validate_Regex(array('pattern' => '/^[0-9A-Za-z\\s-_.]+$/')),
                                                            // 'messages' => array('Invalid report condition')
                                                    ),
                                'datalimit'         => array('allowEmpty' => true,
                                                            'Digits',
                                                            'messages' => array('Invalid data limit format')
                                                    ),
                                'report_name'       => array('NotEmpty',
                                                            array('StringLength',array('max'=>200)),
                                                            'messages' => array('Can not be empty',
                                                                            'Report name length cannot be more than 200',
                                                                        )
                                                    ),
                                'label'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),

                                'colomn'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'type'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'report_email'      => array('allowEmpty'=>true,
                                                            array('StringLength',array('max'=>128)),
                                                            'messages' => array(//'Can not be empty',
                                                                            'Email length cannot be more than 128',
                                                                        )
                                                    ),
                                'report_schedule'   => array('allowEmpty'=>true,
                                                            'Alpha',
                                                            'messages' => array(//'Can not be empty',
                                                                                'Invalid schedule'
                                                                        )
                                                    ),
                                'periodic'       => array('allowEmpty'=>true),
                                'repeat_every'       => array('allowEmpty'=>true),
                                'report_date'       => array('allowEmpty'=>true),
                                'report_day'       => array('allowEmpty'=>false),
                                'report_time'       => array('allowEmpty'=>true),
                                'report_month'       => array('allowEmpty'=>true),
                                'repeat_end'       => array('allowEmpty'=>true),
                                // 'sortby'       => array('allowEmpty'=>true),
                                'sort1'       => array('allowEmpty'=>true),
                                'sort1Direction'       => array('allowEmpty'=>true),
                                'sort2'       => array('allowEmpty'=>true),
                                'sort2Direction'       => array('allowEmpty'=>true),
                                'repeat_start'       => array('allowEmpty'=>true),
                                'repeat_end'       => array('allowEmpty'=>true)


            );

            $zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

            $cek_multiple_email = true;

            if($zf_filter->report_email)
            {
                $validate = new Validate();
                $cek_multiple_email = $validate->isValidEmailMultiple($zf_filter->report_email);
            }
            // echo 'here';die;
            if($zf_filter->isValid() && $cek_multiple_email == true)
            {   
                // print_r($zf_filter->sortasc);die;
                $selectedCols = implode(",", $zf_filter->tablecols);
                // $sortAsc = implode(",", $zf_filter->sortasc);
                // $sortDesc = implode(",", $zf_filter->sortdesc);

                $whereCols = $zf_filter->wherecol;
                $whereOpts = $zf_filter->whereopt;
                $whereVals = $zf_filter->whereval;

                $optsArr = array("EQUAL" => "=",
                                "NOT EQUAL" => "<>",
                                "LESS THAN" => "<",
                                "GREATER THAN" => ">",
                                "LESS THAN OR EQUAL TO" => "<=",
                                "GREATER THAN OR EQUAL TO" => ">="
                );

                echo "<pre>";
                print_r($whereCols);
                print_r($whereVals);

                if(!empty($whereCols)){
                    $tempWhere = array();
                    $index = 0;
                    // $withoutDuplicates = array_unique(array_map("strtoupper", $whereCols));
                    // print_r($withoutDuplicates);
                    $row = 0;
                    // print_r($whereVals);
                    // print_r($whereCols);
                    foreach($whereCols as $key => $val){
                        $tempName = explode("-", $val);
                        $colName = $tempName[0];
                        // if($key%2==0){


                            // print_r($test);
                        if($whereOpts[$key] != "LIKE"){
                            $opt = $optsArr[$whereOpts[$index]];
                            // print_r($whereVals);
                            $whereval = $whereCols[$key+1];
                        }
                        else{
                            $opt = $whereOpts[$index];
                            $whereval = "%".$whereCols[$key+1]."%";
                        }
                        // if($whereVals==''){
                        //     $whereval = 0;
                        // }
                        $duplicate = in_array($val, $whereCols);
                        // print_r($duplicate);
                        if($duplicate){
                            // print_r($whereval);
                            if(empty($whereVals[$row])){
                                $whereVals[$row] = 0;
                            }
                            if($row == '0'){
                                if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    $val = Application_Helper_General::convertDate($whereVals[$row], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    // $val = $whereVals[$row];
                                }else{
                                    $val = $whereVals[$row];
                                }
                                $tempWhere[$row] = $colName." ".$opt." ".$val." ;";
                            }else{
                                if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    $val = Application_Helper_General::convertDate($whereVals[$row], $this->_dateDBFormat, $this->_dateDisplayFormat);
                                     // $val = $whereVals[$row];
                                }else{
                                    $val = $whereVals[$row];
                                }
                                $tempWhere[$row-1] .= " ".$colName." ".$opt." ".$val." ;";
                            }
                            // print_r($tempWhere);
                            $row++;
                        }else{
                            if($colName == 'PS_CREATED' || $colName == 'PS_EFDATE' || $colName == 'PS_UPDATED'){
                                    $val = Application_Helper_General::convertDate($whereval, $this->_dateDBFormat, $this->_dateDisplayFormat);
                                    // $val = $whereVals[$row];
                                }else{
                                    $val = $whereVals[$row];
                                }
                            $tempWhere[$row] = $colName." ".$opt." ".$val." ;";
                            // print_r($tempWhere);
                            $row++;
                        }

                        $index++;
                        // }
                        // $index++;
                    }

                    // print_r($tempWhere);

                    $wheres = implode(";",$tempWhere);
                    $wheres = str_replace(';;', ';', $wheres);
                }
                // print_r($zf_filter->report_data);




                // print_r($wheres);
               //  $report = $this->_request->getParam('report_radio');
               // if(empty($report)){
               //      $    
               // }

                if(empty($zf_filter->report_email)){
                    $schedule = null;
                }
                else{
                    if(empty($zf_filter->report_schedule))
                        $schedule = NULL;
                    else
                        $schedule = strtolower($zf_filter->report_schedule);
                }

                if(empty($zf_filter->datalimit))
                    $limit = 0;
                else
                    $limit = $zf_filter->datalimit;


                $insArr = array("REPORT_NAME" => $zf_filter->report_name,
                                "REPORT_FILE" => $zf_filter->save_as,
                                // "REPORT_TABLE" => $zf_filter->tablename,
                                "REPORT_COLUMNS" => $selectedCols,
                                "REPORT_WHERE" => $wheres,
                                // "REPORT_SORT_ASC" => $sortAsc,
                                // "REPORT_SORT_DESC" => $sortDesc,
                                "REPORT_LIMIT" => $limit,
                                "REPORT_EMAIL" => $zf_filter->report_email,
                                "PERIODIC" => $zf_filter->periodic,
                                "REPORT_SCHEDULE" => $schedule,
                                "REPORT_CUST" => $this->_custIdLogin,
                                "REPORT_CREATED" => new Zend_Db_Expr("GETDATE()"),
                                "REPORT_CREATEDBY" => $this->_userIdLogin
                                //"REPORT_DATA" => $zf_filter->report_data

                );
                // print_r($insArr);die;
                

//                 if($zf_filter->sortby == '1'){
// // "REPORT_SORT_ASC" => $sortAsc,
//                     $insArr['REPORT_SORT_ASC'] =  $zf_filter->sortasc;
//                 }else if($zf_filter->sortby == '2'){
//                     $insArr['REPORT_SORT_DESC'] =  $zf_filter->sortdesc;
//                 }

                //if periodic != 0 (custom)
                if ($zf_filter->periodic != 0) {

                    if($schedule == 'monthly'){
                        $insArr['REPEAT_EVERY'] =  $zf_filter->repeat_every;
                        $insArr['REPORT_DATE'] =  $zf_filter->report_date;
                    }else if($schedule == 'weekly'){

                        $report_day = '';
                        $count = count($zf_filter->report_day);
                        $i = 1;
                        foreach ($zf_filter->report_day as $value) {
                            if ($i !== $count) {
                                $report_day .= $value.',';
                            }
                            else{
                                $report_day .= $value;
                            }
                            $i++;
                        }

                        $insArr['REPEAT_EVERY'] =  $zf_filter->repeat_every;
                        $insArr['REPORT_DAY'] =  $report_day;
                    }else if($schedule == 'daily'){
                        // $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                        $insArr['REPORT_TIME'] =  $zf_filter->report_time;
                    }else if($schedule == 'yearly'){
                        $insArr['REPORT_MONTH'] =  $zf_filter->report_month;
                        $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                    }

                    $insArr['REPEAT_END'] =  $zf_filter->repeat_end;
                }

                if (!empty($zf_filter->sort1)) {
                    $insArr['REPORT_SORT1'] = $zf_filter->sort1.' '.$zf_filter->sort1Direction;
                }

                if (!empty($zf_filter->sort2)) {
                    $insArr['REPORT_SORT2'] = $zf_filter->sort2.' '.$zf_filter->sort2Direction;
                }

                try{
                    $this->_db->beginTransaction();

                    $this->_db->insert('T_REPORT_GENERATOR',$insArr);

                    $lastId = $this->_db->lastInsertId();
                    // print_r($zf_filter->label);die;
                    if(!empty($zf_filter->label)){
                        foreach ($zf_filter->label as $key => $value) {
                                    // print_r($value);
                                if($zf_filter->type[$key]=='function'){
                                    $type = 5;
                                }
                                else if($zf_filter->type[$key]=='datetime'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='date'){
                                    $type = 3;
                                }elseif($zf_filter->type[$key]=='varchar'){
                                    $type = 1;
                                }elseif($zf_filter->type[$key]=='text'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='decimal' || $zf_filter->type[$key]=='int'){
                                    $type = 2;
                                }else{
                                    $type = 1;
                                }

                                $colmnArr = array("COLM_NAME" => $value,
                                    "COLM_FIELD" => $zf_filter->colomn[$key],
                                    "COLM_REPORD_ID" => $lastId,
                                    "COLM_INDEX" => $key,
                                    "COLM_TYPE" => $type
                                );
                                // print_r($colmnArr);die;
                                $this->_db->insert('T_REPORT_COLOMN',$colmnArr);
                        }

                    }

                    Application_Helper_General::writeLog('ADRG','New report has been added, Report Name : '.$zf_filter->report_name. ' Creator : '.$this->_custIdLogin." | ".$this->_userIdLogin);

                    $this->_db->commit();
                    $this->setbackURL('/reportcrm2/report');
                    $this->_redirect('/notification/success/index');
                }
                catch(Exception $e){
                    // print_r($e);die;
                    $this->_db->rollBack();
                    $error_remark = $this->language->_('An Error Occured. Please Try Again');
                }
            // die;
                if(isset($error_remark))
                {
                    // die;
                    Application_Helper_General::writeLog('ADRG','Add Report');
                    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
                    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
                    $this->_redirect('/reportcrm2');
                }
            }
            else{
                $this->view->tablename = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $this->view->save_as = ($zf_filter->isValid('save_as'))? $zf_filter->save_as : $this->_getParam('save_as');
                $this->view->tablecols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $this->view->sortasc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $this->view->sortdesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');
                $this->view->datalimit = ($zf_filter->isValid('datalimit'))? $zf_filter->datalimit : $this->_getParam('datalimit');
                $this->view->wherecol = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                $this->view->whereopt = ($zf_filter->isValid('whereopt'))? $zf_filter->whereopt : $this->_getParam('whereopt');
                $this->view->whereval = ($zf_filter->isValid('whereval'))? $zf_filter->whereval : $this->_getParam('whereval');
                $this->view->report_name = ($zf_filter->isValid('report_name'))? $zf_filter->report_name : $this->_getParam('report_name');
                $this->view->report_email = ($zf_filter->isValid('report_email'))? $zf_filter->report_email : $this->_getParam('report_email');
                $this->view->report_schedule = ($zf_filter->isValid('report_schedule'))? $zf_filter->report_schedule : $this->_getParam('report_schedule');

                $error = $zf_filter->getMessages();
           // print_r($error);die;
                $errorArray = null;
                foreach($error as $keyRoot => $rowError)
                {
                   foreach($rowError as $errorString)
                   {
                      $errorArray[$keyRoot] = $errorString;
                   }
                }

                if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['report_email'] = 'Invalid email format';

                $this->view->error_msg = $errorArray;

                $tblName = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'digitalbanking360')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")');

                $tempColumn = $this->_db->fetchAll($select);

                $this->view->columnList = $tempColumn;

                $tblCols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $tblAsc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $tblDesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');

                $leftCols = array();
                $leftAsc = array();
                $leftDesc = array();
                foreach($tempColumn as $row){
                    if(!in_array($row['COLUMN_NAME'], $tblCols))
                        $leftCols[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblAsc))
                        $leftAsc[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblDesc))
                        $leftDesc[] = $row['COLUMN_NAME'];
                }

                $this->view->leftcols = $leftCols;
                $this->view->leftasc = $leftAsc;
                $this->view->leftdesc = $leftDesc;

                $whereCols = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                if(!empty($whereCols)){
                    $wherectr = count($whereCols)+1;
                }
                else{
                    $wherectr = 1;
                }

                $this->view->wherectr = $wherectr;
            }
        }
        else{
            $this->view->wherectr = 1;
        }
    }


    public function columnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('COLUMN_NAME'))
                            ->where('table_schema = ?', 'digitalbanking360')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")');
                            // echo $select;die;
        $data = $this->_db->fetchAll($select);
        foreach($data as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."'>".$row['COLUMN_NAME']."</option>";
        }

        echo $optHtml;
    }


     public function previewAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $colomn = $this->_getParam('colomn');
        $limit = $this->_getParam('limit');
        $selectdata = '';
        foreach ($colomn as $key => $value) {
            if(!empty($colomn[$key+1])){
                $selectdata .= $value.' , ';    
            }else{
                $selectdata .= $value;
            }
            
        }

         $paramPayment = array("WA"              => false,
                              "ACCOUNT_LIST"    => $this->_accountList,
                              "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
                             );
        
        // get payment query
        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        $select   = $CustomerUser->getPayment($paramPayment);
        // $select = 'SELECT '.$selectdata.' FROM T_TRANSACTION as A LEFT JOIN T_PSLIP as B ON A.PS_NUMBER = B.PS_NUMBER';

        // $select = $this->_db->select()
        //                     ->from('information_schema.columns', array('COLUMN_NAME'))
        //                     ->where('table_schema = ?', 'bank_mayapada_cm_demo')
        //                     ->where('table_name in ("T_PSLIP","T_TRANSACTION")');
        //                     // echo $select;die;
        // $data = $this->_db->fetchAll($select);
        // foreach($data as $row){
        //     $optHtml.="<option value='".$row['COLUMN_NAME']."'>".$row['COLUMN_NAME']."</option>";
        // }



        if(!empty($limit)){
            $select->limit($limit);
        }
               
       // echo '<pre>';
       //  echo $select;die;
        $data = $this->_db->fetchAll($select);

        $optHtml = '<table class="table table-bordered table-condensed flip-content"><tr>';
        foreach ($colomn as $key => $value) {
            $optHtml .= '<th>'.$value.'</th>';
        }
        $optHtml .= '</tr>';
        $total = count($data);

        for ($i=0; $i < $total ; $i++) { 
            $optHtml .= '<tr>';
            foreach ($colomn as $key => $value) {
                $optHtml .= '<td>'.$data[$i][$value].'</td>';
            }
            $optHtml .= '</tr>';
        }
        // foreach ($data as $key => $value) {
            
        // }

        echo $optHtml;
    }

    

    public function wherecolumnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                            ->where('table_schema = ?', 'digitalbanking360')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                            ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "PS_NUMBER",
                                    "LLD_DESC",
                                    "TRANSACTION_ID",
                                    "PS_SUBJECT",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "PS_CREATED",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "TRA_MESSAGE",
                                    "TRA_ADDITIONAL_MESSAGE",
                                    "PS_PERIODIC",
                                    "CUST_ID",
                                    "RATE",
                                    "RATE_BUY",
                                    "BOOK_RATE",
                                    "BOOK_RATE_BUY",
                                    "PROVISION_FEE",
                                    "FULL_AMOUNT_FEE",
                                    "TOTAL_CHARGES",
                                    "RATE_BUY",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "TRANSFER_FEE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
                            // ->where('DATA_TYPE != ? ','datetime')
                            // ->where('DATA_TYPE != ? ','date');

        $tempColumn = $this->_db->fetchAll($select);
        $optHtml = "<option value=''>-- ".$this->language->_('Any Value')." --</option>";
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }

        }
        foreach($tempColumn as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."-".$row['DATA_TYPE']."'>".$row['COLOMN']."</option>";
        }

        echo $optHtml;
    }


     public function wherecolumnnewAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                            ->where('table_schema = ?', 'digitalbanking360')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                            ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "PS_NUMBER",
                                    "LLD_DESC",
                                    "TRANSACTION_ID",
                                    "PS_SUBJECT",
                                    "LOG",
                                    "REFF_ID",
                                    "PS_CREATED",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "TRA_MESSAGE",
                                    "TRA_ADDITIONAL_MESSAGE",
                                    "PS_PERIODIC",
                                    "CUST_ID",
                                    "RATE",
                                    "RATE_BUY",
                                    "BOOK_RATE",
                                    "BOOK_RATE_BUY",
                                    "PROVISION_FEE",
                                    "FULL_AMOUNT_FEE",
                                    "TOTAL_CHARGES",
                                    "RATE_BUY",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "TRANSFER_FEE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
                            // ->where('DATA_TYPE != ? ','datetime')
                            // ->where('DATA_TYPE != ? ','date');

        $tempColumn = $this->_db->fetchAll($select);
        $optHtml = "<option value=''>-- ".$this->language->_('Any Value')." --</option>";
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }

        }
        foreach($tempColumn as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."-".$row['DATA_TYPE']."'>".$row['COLOMN']."</option>";
        }

        echo $optHtml;
    }




    public function wherecolumnpsstatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $arrPayStatus   = array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
        foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }


        // $tempColumn = $this->_db->fetchAll($select);
        $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($optpayStatusRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }

     public function wherecolumnpstypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $payType    = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
        foreach($payType as $key => $value){
//          if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);

             $optpaytypeRaw[$key] = $this->language->_($value);
        }


        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($optpaytypeRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }

    public function wheresourceaccountAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $arrAccount     = $CustomerUser->getAccounts();

        if(is_array($arrAccount) && count($arrAccount) > 0){
            foreach($arrAccount as $key => $value){

                $val        = $arrAccount[$key]["ACCT_NO"];
                $ccy        = $arrAccount[$key]["CCY_ID"];
                $acctname   = $arrAccount[$key]["ACCT_NAME"];
                //$acctalias    = $arrAccount[$key]["ACCT_ALIAS_NAME"];
                $accttype   = ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';  // 10 : saving, 20 : giro;

                $arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';

            }
        }
        else { $arrAccountRaw = array();}

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($arrAccountRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }




     public function whereuserAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $cust_id = (string)$this->_custIdLogin;
        $cust = new Customer($cust_id);
        $userList = $cust->getUserList();
        $tblName = $this->_getParam('id');
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        // alert($tblName);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($userList)){
        foreach($userList as $key => $row){
            if($tblName==$row['USER_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['USER_ID']."' ".$select.">".$row['USER_ID']."</option>";
        }
        }

        echo $optHtml;
    }

    // public function whereuserAction()
    // {
    //     $this->_helper->viewRenderer->setNoRender();
    //     $this->_helper->layout()->disableLayout();
    //     $cust_id = (string)$this->_custIdLogin;
    //     $cust = new Customer($cust_id);
    //     $userList = $cust->getUserList();
    //     $tblName = $this->_getParam('id');
    //     // $tempColumn = $this->_db->fetchAll($select);
    //     // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
    //     // $optPayStatus   = $opt + $optpayStatusRaw;
    //     // print_r($arrAccountRaw);die;
    //     // alert($tblName);
    //     $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
    //     if(!empty($userList)){
    //     foreach($userList as $key => $row){
    //         if($tblName==$row['USER_ID']){
    //             $select = 'selected';
    //         }else{
    //             $select = '';
    //         }
    //         $optHtml.="<option value='".$row['USER_ID']."' ".$select.">".$row['USER_ID']."</option>";
    //     }
    //     }

    //     echo $optHtml;
    // }


     public function approvestatAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $aprovalstatus = $this->_aprovalstatus;
        $aprovalstatus = array(''=>'-- '. $this->language->_('All') .' --');
        $aprovalstatus += Application_Helper_Array::globalvarArray($this->_aprovalstatus);
        foreach($aprovalstatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }

        unset($aprovalstatus[2]);
        unset($aprovalstatus[3]);
        unset($aprovalstatus[6]);

        unset($optpayStatusRaw[2]);
        unset($optpayStatusRaw[3]);
        unset($optpayStatusRaw[6]);

         foreach ($optpayStatusRaw as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

   public function categoryAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
         $tblName = $this->_getParam('id');

    $arrquestioncategory    = array_combine($this->_questioncategory["code"],$this->_questioncategory["desc"]);
        $questioncategoryArray  = array( '' => '--- '.$this->language->_('Please Select').' --- ');
        $questioncategoryArray += array_combine(array_values($this->_questioncategory['code']),array_values($this->_questioncategory['desc']));
        //$this->view->questioncategoryArray        = $questioncategoryArray;

        foreach($questioncategoryArray as $key => $value){ if($key != 5) $optpayStatusRaw[$key] = $this->language->_($value); }

        $optPayType = $optpayStatusRaw;
        // print_r($optPayType);die;
        foreach ($optPayType as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
        // $this->view->questioncategoryArray  = $optPayType;


    }

    public function ccyAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       $selectidr = '';
       $selectusd = '';
       if(!empty($tblName)){
            if($tblName=='IDR'){
                $selectidr = 'selected';
            }else{
                $selectusd = '';
            }

       }

        // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        $optHtml.="<option value='IDR' ".$selectidr.">IDR</option>";
        $optHtml.="<option value='USD' ".$selectusd.">USD</option>";


        echo $optHtml;
    }


    public function actlistAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $activity = $this->_db->select()->distinct()
                                ->from(array('A' => 'M_FPRIVILEGE'),array('FPRIVI_ID', 'FPRIVI_DESC'))
                                ->order('FPRIVI_DESC ASC')
                                ->query()->fetchAll();

        $login = array('FPRIVI_ID'=>'FLGN','FPRIVI_DESC'=>'Login');
        $logout = array('FPRIVI_ID'=>'FLGT','FPRIVI_DESC'=>'Logout');
        $changepass = array('FPRIVI_ID'=>'CHMP','FPRIVI_DESC'=>'Change My Password');
        $resetpass = array('FPRIVI_ID'=>'RFPW','FPRIVI_DESC'=>'Reset Forgot Password');
        array_unshift($activity,$changepass);
        array_unshift($activity,$logout);
        array_unshift($activity,$login);
        array_unshift($activity,$resetpass);

        $activityarr = Application_Helper_Array::listArray($activity,'FPRIVI_ID','FPRIVI_DESC');
        asort($activityarr);
        // print_r($activityarr);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach ($activityarr as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function sugesttypeAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $changesTypeCodeArr   = $this->_suggestType['code'];

        unset($this->_suggestType['code']['activate']);
        unset($this->_suggestType['desc']['activate']);
        unset($this->_suggestType['code']['deactivate']);
        unset($this->_suggestType['desc']['deactivate']);
        $options = array_combine(array_values($this->_suggestType['code']),array_values($this->_suggestType['desc']));

        // print_r($options);die;
        foreach ($options as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }


     public function trastatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        // $arrPayStatus   = array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
        $statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
        foreach($statusarr as $key => $value){ $optpayStatusRaw[$key] = $this->language->_($value); }


        // $tempColumn = $this->_db->fetchAll($select);
        $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($optpayStatusRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }


    

    public function sugeststatusAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $suggestionStatusCP = array( "" => '--'.$this->language->_('Any Value')."--",
                                            "UR"=> $this->language->_('Unread Suggestion') ,
                                            "RS"=> $this->language->_('Read Suggestion'),
                                            "RR"=> $this->language->_('Request Repaired'),
                                            "RP"=> $this->language->_('Repaired Suggestion'),
        );
    $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
     foreach ($suggestionStatusCP as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }


    public function areaAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $areaArr = $select = $this->_db->select()
                           ->distinct()
                           ->from('M_SERVICE_AREA',array('AREA_NAME'))
                           ->query()->fetchAll();
        //$this->view->branchArr = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($model->getBranch(),"CODE","NAME");
        $area = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($areaArr,"AREA_NAME","AREA_NAME");
        // print_r($area);die;
        foreach ($area as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function sugestdataAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $listSuggestData  = array(
        //'General Setting'=>$this->language->_('General Setting'),
        //'Minimum Amount and Currency Available'=>$this->language->_('Minimum Amount and Currency Available'),
        // 'Holiday Setting'=>$this->language->_('Holiday Setting'),
        // 'Customer'=>$this->language->_('Customer'),
        // 'Bank Account'=>$this->language->_('Bank Account'),
        // 'User Account'=>$this->language->_('User Account'),
        // 'User Limit'=>$this->language->_('User Limit'),
        // 'User Daily Limit'=>$this->language->_('User Daily Limit'),
        // 'Approver Group'=>$this->language->_('Approver Group'),
        // 'Approver Group Boundary'=>$this->language->_('Approver Group Boundary'),
        // 'Backend User'=>$this->language->_('Backend User'),
        // 'Backend Group'=>$this->language->_('Backend Group'),
        // 'Charges'=>$this->language->_('Charges'),
        // 'Charges Template'=>$this->language->_('Charges Template'),
        'User Account' => $this->language->_('User Account'),
        'User List' => $this->language->_('User List'),
        'User Limit'=>$this->language->_('User Limit'),
        'User Daily Limit'=>$this->language->_('User Daily Limit'),
        'Approver Group'=>$this->language->_('Approver Group'),
        'Approver Group Boundary'=>$this->language->_('Approver Group Boundary'),
        // 'COA Account'=>'COA Account',
        // 'Charges Template'=>'Charges Template',
        // 'Charges'=>'Charges',
        // 'System Balance'=>'System Balance',
        // 'Global Scheme Parameter'=>'Global Scheme Parameter',
        // 'Scheme Configuration'=>'Scheme Configuration',
        // 'Physical Document Configuration'=>'Physical Document Configuration',
        // 'Root Community'=>'Root Community',
        // 'Principal'=>'Principal',
        // 'Community'=>'Community',
        // 'Member'=>'Member',
        // 'Assign User to Community'=>'Assign User to Community',
    );
    ksort($listSuggestData);
    $listSuggestData = array('all'=>'--'.$this->language->_('Any Value').'--')+$listSuggestData;

     // $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach ($listSuggestData as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }

    public function benetypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       $selectidr = '';
       $selectusd = '';
       if(!empty($tblName)){
            if($tblName=='IDR'){
                $selectidr = 'selected';
            }else{
                $selectusd = '';
            }

       }
       if($tblName == '1'){$select =  'selected';}
        $type =$this->language->_('In House');
        $optHtml.="<option value = '1' ".$select.">".$type."</option>";
        if($tblName == '2'){$select =  'selected';}
        $type =$this->language->_('Domestic');
        $optHtml.="<option value = '2' ".$select.">".$type."</option>";
        if($tblName == '3'){$select =  'selected';}
        $type =$this->language->_('Remittance');
        $optHtml.="<option value = '3' ".$select.">".$type."</option>";
        if($tblName == '4'){$select =  'selected';}
        $type =$this->language->_('Local Remittance');
        $optHtml.="<option value = '4' ".$select.">".$type."</option>";


        echo $optHtml;
    }


      public function benefAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

//      if ($priviBeneLinkage == true)
//      {
            $select = $this->_db->select()
                                ->from(array('B'            => 'M_BENEFICIARY_USER'),
                                       array('ACBENEF_ID'   => 'B.BENEFICIARY_ID')
                                       )
                                ->join(array('A'=>'M_BENEFICIARY'), 'A.BENEFICIARY_ID = B.BENEFICIARY_ID',array('A.BENEFICIARY_ACCOUNT'))
                                ->where("B.CUST_ID  = ?" , (string)$this->_custIdLogin)
                                ->where("B.USER_ID  = ?" , (string)$this->_userIdLogin);
         // echo "<pre>";
     // echo $select->__toString();
//          die;
            $ACBENEFArr = $this->_db->fetchAll($select);
            // $ACBENEFArr = Application_Helper_Array::simpleArray($ACBENEFArr, "ACBENEF_ID");
            // print_r($ACBENEFArr);
            // bila empty, user bene linkage cannot see all payment
            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['ACBENEF_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
            $optHtml.="<option value='".$value['ACBENEF_ID']."' ".$select.">".$value['BENEFICIARY_ACCOUNT']."</option>";
        }


        // $optHtml.="<option value='USD'>USD</option>";


        echo $optHtml;
    }

    //addtional for checkque


    public function whereproducttypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $productType = array(
            "1"=> $this->language->_('Cheque') ,
            "2"=> $this->language->_('Bilyet Giro')
        );


        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($productType as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function wherestatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $status = array(
            "AP"=> $this->language->_('Approve') ,
            "RJ"=> $this->language->_('Reject'),
            "WA"=> $this->language->_('Waiting Approve')
        );

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($status as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function sourcebankAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $arrAccount     = $CustomerUser->getAccounts();

        if(is_array($arrAccount) && count($arrAccount) > 0){
            foreach($arrAccount as $key => $value){

                $val        = $arrAccount[$key]["ACCT_NO"];
                $ccy        = $arrAccount[$key]["CCY_ID"];
                $acctname   = $arrAccount[$key]["ACCT_NAME"];
                //$acctalias    = $arrAccount[$key]["ACCT_ALIAS_NAME"];
                $accttype   = ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';  // 10 : saving, 20 : giro;

                $arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';

            }
        }
        else { $arrAccountRaw = array();}

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach($arrAccountRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }


        //open banking acct
        $complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                       
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );   
        $comp = "'";
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";

        $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_APIKEY'))
                         ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                          ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                         ->where("A.CUST_ID IN (".$comp.")")
                         ->order('A.APIKEY_ID ASC')
                );

        $listbank = array();
        foreach ($acctlist as $key => $value) {
            $listbank[] = $value['BANK_NAME'];
        }
        
        $listBank = array_unique($listbank);
        foreach ($listBank as $key => $value) {

            if($tblName==$value){
                $select = 'selected';
            }else{
                $select = '';
            }
            // $optHtml.="<option value='".$value['ACCT_NO']."' ".$select.">".$value['ACCT_NO']." [".$value['ACCT_BANK']."] - ".$value['ACCT_NAME']."</option>";
            $optHtml.="<option value='".$value."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
    }

    public function benefbankAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

         //open banking acct
        $complist = $this->_db->fetchAll(
            $this->_db->select()
                 ->from(array('A' => 'M_USER'),array('CUST_ID'))
               
                 ->where("A.USER_ID = ? ", $this->_userIdLogin)
        );   
        $comp = "'";
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";

        $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                        ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                        ->where("A.CUST_ID IN (".$comp.")")
                        ->order('A.APIKEY_ID ASC')
                );

        $listbank = array();
        foreach ($acctlist as $key => $value) {
            $listbank[] = $value['BANK_NAME'];
        }

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        
        $listBank = array_unique($listbank);
        foreach ($listBank as $key => $value) {

            if($tblName==$value){
                $select = 'selected';
            }else{
                $select = '';
            }
            // $optHtml.="<option value='".$value['ACCT_NO']."' ".$select.">".$value['ACCT_NO']." [".$value['ACCT_BANK']."] - ".$value['ACCT_NAME']."</option>";
            $optHtml.="<option value='".$value."' ".$select.">".$value."</option>";
        }


        echo $optHtml;
    }

    public function benefcategoryAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $cat = array(
            "Individual"=> $this->language->_('Individual') ,
            "Governmant"=> $this->language->_('Governmant'),
            "Company"=> $this->language->_('Company')
        );

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($cat as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function benefcitizenshipAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $cat = array(
            "Resident"=> $this->language->_('Resident') ,
            "Non Resident"=> $this->language->_('Non Resident'),
        );

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($cat as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function benefnationalityAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $cat = array(
            "W"=> $this->language->_('WNI'),
            "N"=> $this->language->_('WNA'),
        );

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($cat as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function benefidtypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $cat = array(
            "KTP"=> $this->language->_('KTP'),
            "SIM"=> $this->language->_('SIM'),
            "PAS"=> $this->language->_('PASPOR'),
            "KIT"=> $this->language->_('KITAS'),
        );

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($cat as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function benefemailAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

            $select = $this->_db->select()
                     ->from(array('M_BENEFICIARY'));
                    
            $ACBENEFArr = $this->_db->fetchAll($select);

            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }

        $mail = array();
        foreach($ACBENEFArr as $key => $value){
            if($value['BENEFICIARY_EMAIL']==''){
                continue;
            }
            $mail[] = $value['BENEFICIARY_EMAIL'];
            
        }
        
        $mail = array_unique($mail);

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($mail as $val) {
            $optHtml.="<option value='".$val."'>".$val."</option>";
        }
        echo $optHtml;

    }

    public function benefcitycodeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

            $select = $this->_db->select()
                     ->from(array('M_BENEFICIARY'));
                    
            $ACBENEFArr = $this->_db->fetchAll($select);

            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }

        $code = array();
        foreach($ACBENEFArr as $key => $value){
            if($value['BENEFICIARY_CITY_CODE']==''){
                continue;
            }
            $code[] = $value['BENEFICIARY_CITY_CODE'];
            
        }
        
        $code = array_unique($code);

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($code as $val) {
            $optHtml.="<option value='".$val."'>".$val."</option>";
        }
        echo $optHtml;

    }

    public function benefbankcodeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

            $select = $this->_db->select()
                     ->from(array('M_BENEFICIARY'));
                    
            $ACBENEFArr = $this->_db->fetchAll($select);

            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }

        $code = array();
        foreach($ACBENEFArr as $key => $value){
            if($value['BENEF_ACCT_BANK_CODE']==''){
                continue;
            }
            $code[] = $value['BENEF_ACCT_BANK_CODE'];
            
        }
        
        $code = array_unique($code);

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($code as $val) {
            $optHtml.="<option value='".$val."'>".$val."</option>";
        }
        echo $optHtml;

    }

    public function benefaddressAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

            $select = $this->_db->select()
                     ->from(array('M_BENEFICIARY'));
                    
            $ACBENEFArr = $this->_db->fetchAll($select);

            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }

        $code = array();
        foreach($ACBENEFArr as $key => $value){
            if($value['BENEFICIARY_ADDRESS']==''){
                continue;
            }
            $code[] = $value['BENEFICIARY_ADDRESS'];
            
        }
        
        $code = array_unique($code);

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($code as $val) {
            $optHtml.="<option value='".$val."'>".$val."</option>";
        }
        echo $optHtml;

    }

    public function benefaddress2Action()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

            $select = $this->_db->select()
                     ->from(array('M_BENEFICIARY'));
                    
            $ACBENEFArr = $this->_db->fetchAll($select);

            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }

        $code = array();
        foreach($ACBENEFArr as $key => $value){
            if($value['BENEFICIARY_ADDRESS2']==''){
                continue;
            }
            $code[] = $value['BENEFICIARY_ADDRESS2'];
            
        }
        
        $code = array_unique($code);

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($code as $val) {
            $optHtml.="<option value='".$val."'>".$val."</option>";
        }
        echo $optHtml;

    }

    public function transactionidAction()
    {

    }

}
