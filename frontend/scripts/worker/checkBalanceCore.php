<?php
//die('here');
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	
	
	//$thisClass = $this;
	$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
		
		$datapers = array();
		$logName = 'Service_SMS::sendSMS';
		$pathFile = null;
		$printToDisplay = true;
		$logFileType = "ALL";
		
		
	
		$loggerService = SGO_Extendedmodule_Loggerservice_Service::getInstance();
		$logger = $loggerService->getLogger($logName, $pathFile, $printToDisplay, $logFileType);
		//var_dump($logger);
		//var_dump($queueService);die;
		$sms = array();
		$callback = function($sms) use ($logger,$queueService) {
			
			
			
			
			$request = json_decode($sms->body, true);
			
			// $frontendOptions = array ('lifetime' => 600,
   //                                'automatic_serialization' => true );
			// $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
			// $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
			// $cacheID = 'BALANCE'.$request['cust'];
			// $data = $cache->load($cacheID);
			
			//var_dump($request);die;
			$hasilmod = $request['ACCT_NO'] % 2; 
			// if($request['BANK_CODE'] == '014' && $hasilmod == 0){
			
			$db = Zend_Db_Table::getDefaultAdapter();
			// var_dump($request);
			

            $systemBalance = new SystemBalance($request['CUST_ID'],$request['ACCT_NO'],Application_Helper_General::getCurrNum($request['CCY_ID']));
            $systemBalance->setFlag(false);
            $systemBalance->checkBalance();

            if($systemBalance->getEffectiveBalance() != 'N/A'){

                $temmpbalance =  $db->select()
	              ->from(array('A' => 'M_BALANCE'))
	              ->where("A.ACCT_NO = ".$db->quote($request['ACCT_NO']))
	              ->where("A.BANK_CODE is null ")
				  ->where("A.CUST_ID = ".$db->quote($request['CUST_ID']));
				 
				//echo $temmpbalance;
				  $balance = $db->fetchAll($temmpbalance);
				
	
	             if(empty($balance)){
	                  $param = array(
	                     'ACCT_NO' => $request['ACCT_NO'],
	                     'BANK_CODE' => null,
						 'ACCT_NAME' => null,
	                     'CUST_ID' => $request['CUST_ID'],
	                     'CCY' => $request['CCY_ID'],
	                     'BALANCE' => (int)$systemBalance->getEffectiveBalance(),
	                     'UPDATED' => new Zend_Db_Expr("now()"),
	                     'RABBIT_ID' => (string)$request['id']
	                 );
	                 $db->insert('M_BALANCE',$param);
	             }else{
	                $updateArr['BALANCE'] = (int)$systemBalance->getEffectiveBalance();
				//	$updateArr['ACCT_NAME']	= $result->account_name;
	                $updateArr['UPDATED'] = new Zend_Db_Expr('now()');
	                $updateArr['RABBIT_ID']	= (string)$request['id'];
	                $whereArr = array( #'CUST_ID = ?' => (string)$custId,
	                        'ACCT_NO = ?' => (string)$request['ACCT_NO'],
	                        //'BANK_CODE = ? ' => NULL,
	                        'CUST_ID = ? ' => (string)$request['CUST_ID']
	                    );
						//var_dump($updateArr);
	//var_dump($whereArr);
	                try{
	                	$balanceupdate = $db->update('M_BALANCE',$updateArr,$whereArr);
	                }catch (Exception $e) {
	                 //   print_r($e);die;
					}
	            }
				
				
								$where['CUST_ID = ?'] = $request['CUST_ID'];
						  		  			//	$where['USER_ID = ?'] = $request['user'];
   								 				$db->delete('T_RABBIT_LOGNA',$where);	
   								$updateLog['DONE'] = new Zend_Db_Expr('now()');
				                $whereArrlog = array( #'CUST_ID = ?' => (string)$custId,
				                        'ID = ? ' => (string)$request['id']
				                    );

				                try{
				                	$balanceupdate = $db->update('T_RABBIT_FLAGNA',$updateLog,$whereArrlog);
				                }catch (Exception $e) {
				                   // print_r($e);die;
								}
								
   								$updateLog['DONE'] = new Zend_Db_Expr('now()');
				                $whereArrlog = array( #'CUST_ID = ?' => (string)$custId,
				                        'ID = ? ' => (string)$request['id']
				                    );

				                try{
				                	$balanceupdate = $db->update('T_RABBIT_FLAG',$updateLog,$whereArrlog);
				                }catch (Exception $e) {
				                 //   print_r($e);die;
								}
				
				
				

            }
			
		};
		//$queueConsumer = '';
		//var_dump($queueConsumer);
		$queueConsumer = $queueService->getQueueByProfileName("BALANCE_CORE");
		
		try {
			$queueConsumer->waitAndProcess($callback);
		} catch (Exception $e) {
			$param = [];
			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e->getMessage(),NULL,FALSE),$param);
			SGO_Helper_Supervisor::doExitByExitCodeName("RESTART");
		}

?>
