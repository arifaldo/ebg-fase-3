<?php

class myaccount_Model_Myaccount extends Application_Main
{

  public function getUserData($cust_id, $user_id)
  {
    $select = $this->_db->select()
      ->from(array('T' => 'M_USER'))
      ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
      ->where('UPPER(USER_ID)=' . $this->_db->quote((string)$user_id))
      ->query()->fetch();

    return $select;
  }

  public function getPrivilege($cust_id, $user_id)
  {

    // $select  = $this->_db->select()
    // ->from(array('u'=>'M_FPRIVI_USER'))
    // ->where('UPPER(u.FUSER_ID)='.$this->_db->quote((string)$fuserId))
    // ->query()->fetchAll();

    //   $select = $this->_db->select()
    //                        ->from('M_FPRIVILEGE',array('FPRIVI_ID','FPRIVI_DESC','FPRIVI_MODULEID'))
    //                        ->order('FPRIVI_DESC ASC')
    //                        ->query()->fetchAll();


    //    $select = $this->_db->select()
    //    ->from('M_FPRIVILEGE',array('FPRIVI_ID','FPRIVI_DESC','FPRIVI_MODULEID'))
    //    ->order('FPRIVI_DESC ASC')
    //    ->query()->fetchAll();

    $fuserId    = $cust_id . $user_id;

    $select = $this->_db->select()
      ->from(array('A' => 'M_FPRIVILEGE'), array('FPRIVI_ID', 'FPRIVI_DESC', 'FPRIVI_MODULEID'))
      ->joinLeft(array('B' => 'M_FPRIVI_USER'), 'A.FPRIVI_ID = B.FPRIVI_ID', array())
      ->where('UPPER(B.FUSER_ID)=' . $this->_db->quote((string)$fuserId))
      ->order('A.FPRIVI_DESC ASC')
      ->query()->fetchAll();



    // ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' OR UPPER(CUST_ID)='.$this->_db->quote('BANK'))
    // ->where('UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
    return $select;
  }

  public function getPrivileges($tpye = null)
  {
    // var_dump($type);
    $select = $this->_db->select()
      ->from('M_FPRIVILEGE', array('FPRIVI_ID', 'FPRIVI_DESC', 'FPRIVI_MODULEID'));

    $setting = new Settings();
    $type = $setting->getSetting('system_type');
    // var_dump($type);
    if ($type == '1') {
      $select->where('FPRIVI_MODE IN (0,1)');
    } else if ($type == '2') {
      $select->where('FPRIVI_MODE IN (0,2)');
    }
    $select->order('FPRIVI_DESC ASC');

    //echo $select;
    $select = $select->query()->fetchAll();

    // ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' OR UPPER(CUST_ID)='.$this->_db->quote('BANK'))
    // ->where('UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
    return $select;
  }


  //mengurutkan privi grouping
  public function modifPrivi($privi)
  {
    $privi_final = array();
    $privi_final['ACGR'] = (isset($privi['ACGR'])) ? $privi['ACGR'] : '';
    $privi_final['ACIF'] = (isset($privi['ACIF'])) ? $privi['ACIF'] : '';
    $privi_final['BEAC'] = (isset($privi['BEAC'])) ? $privi['BEAC'] : '';
    $privi_final['CHRL'] = (isset($privi['CHRL'])) ? $privi['CHRL'] : '';
    $privi_final['CMPW'] = (isset($privi['CMPW'])) ? $privi['CMPW'] : '';
    $privi_final['CMPY'] = (isset($privi['CMPY'])) ? $privi['CMPY'] : '';
    $privi_final['CMRP'] = (isset($privi['CMRP'])) ? $privi['CMRP'] : '';
    $privi_final['CQMG'] = (isset($privi['CQMG'])) ? $privi['CQMG'] : '';
    $privi_final['SMBG'] = (isset($privi['SMBG'])) ? $privi['SMBG'] : '';
    $privi_final['IPDT'] = (isset($privi['IPDT'])) ? $privi['IPDT'] : '';
    $privi_final['SWBG'] = (isset($privi['SWBG'])) ? $privi['SWBG'] : '';

    // $privi_final['SCPY'] = (isset($privi['SCPY'])) ? $privi['SCPY'] : '';
    // $privi_final['SCDC'] = (isset($privi['SCDC'])) ? $privi['SCDC'] : '';
    // $privi_final['SCPD'] = (isset($privi['SCPD'])) ? $privi['SCPD'] : '';
    // $privi_final['SCRP'] = (isset($privi['SCRP'])) ? $privi['SCRP'] : '';
    $privi_final['LABE'] = (isset($privi['LABE'])) ? $privi['LABE'] : '';
    $privi_final['OTRP'] = (isset($privi['OTRP'])) ? $privi['OTRP'] : '';
    $privi_final['USRL'] = (isset($privi['USRL'])) ? $privi['USRL'] : '';
    $privi_final['UTLT'] = (isset($privi['UTLT'])) ? $privi['UTLT'] : '';
    $privi_final['OPBK'] = (isset($privi['OPBK'])) ? $privi['OPBK'] : '';
    $privi_final['OPAD'] = (isset($privi['OPAD'])) ? $privi['OPAD'] : '';

    return $privi_final;
  }

  public function getTemplate()
  {
    $select = $this->_db->select()
      ->from('M_FTEMPLATE')
      ->query()->fetchAll();
    return $select;
  }

  public function getPriviTemplate()
  {
    $select = $this->_db->select()
      ->from('M_FPRIVILEGE_TEMPLATE')
      ->query()->fetchAll();
    return $select;
  }

  public function getModuleDescArr()
  {
    $select = $this->_db->select()
      ->from('M_MODULE')
      ->query()->fetchAll();

    $module_desc = array();
    foreach ($select as $row) {
      $module_desc[$row['MODULE_ID']] = $row['MODULE_DESC'];
    }

    return $module_desc;
  }
}
