<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/Settings.php';

class backendgroup_DetailController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();
        $groupid = $AESMYSQL->decrypt($this->_getParam('groupid'), $password);

		//Zend_Debug::dump($change_id);die;
		$this->view->groupid = $groupid;
		$suspend = $this->_getParam('suspend');
		$unsuspend = $this->_getParam('unsuspend');
		$delete = $this->_getParam('delete');
		$privil = $this->_db->select()
							 ->FROM ('M_BPRIVILEGE');
							 
		
		$setting = new Settings();
  
  		$type = $setting->getSetting('system_type');
		 if($type=='1'){
	        $privil->where('BPRIVI_MODE IN (0,1)');
	      }else if($type=='2'){
	        $privil->where('BPRIVI_MODE IN (0,2)');
	      }
	      		$privil->ORDER('BPRIVI_DESC ASC');
		//echo $privil;
		
		$privil = $this->_db->fetchAll($privil);
		//$privil = $privil->query->fetchall();

		$select = $this->_db->select()
					        ->from(array('TEMP_BGROUP'));
		$select -> where("BGROUP_ID LIKE ".$this->_db->quote($groupid));
		$result = $select->query()->FetchAll();

		$caseStatus = "(CASE BGROUP_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";
		//Zend_Debug::dump($this->_changeType['code']['suspend']);die;
			$select3 = $this->_db->select()
					        ->from(array('M_BGROUP'),
					        array('BGROUP_ID','BGROUP_CALL','BGROUP_DESC','UPDATED', 'UPDATEDBY','CREATED','CREATEDBY','BGROUP_STATUS'=>$caseStatus));
			$select3 -> where("BGROUP_ID LIKE ".$this->_db->quote($groupid));
			// echo $select3;die;
			$result3 = $select3->query()->FetchAll();
			$groupname = $result3[0]['BGROUP_DESC'];
			$status = $result3[0]['BGROUP_STATUS'];
			// $callcenter 		= $result3[0]['BGROUP_CALL'];
			$createddate = $result3[0]['CREATED'];
			$lastupdated = $result3[0]['UPDATED'];
			$updatedby = $result3[0]['UPDATEDBY'];
			$createdby = $result3[0]['CREATEDBY'];
			//Zend_Debug::dump($status);die;
			$select4 = $this->_db->select()
					        ->from(array('M_BPRIVI_GROUP'));
			$select4 -> where("BGROUP_ID LIKE ".$this->_db->quote($groupid));
			$result4 = $select4->query()->FetchAll();

			foreach($privil as $row)
			{
				// print_r($result4);die;
				foreach($result4 as $row2)
				{
					if($row2['BPRIVI_ID'] == $row['BPRIVI_ID'])
					{
						$this->view->$row['BPRIVI_ID'] = '1';
						// break;
					}
				}
			}
		// print_r($privil);die;
		// $this->view->callcenter 	= $callcenter;
		$this->view->groupid = $groupid;
		$this->view->groupname = $groupname;
		$this->view->status = $status;
		$this->view->createddate = $createddate;
		$this->view->lastupdated = $lastupdated;
		$this->view->updatedby = $updatedby;
		$this->view->createdby = $createdby;
		$this->view->privil = $privil;
		// print_r($result4);die;
		$this->view->prev = $result4;
		$this->view->modulename = $this->_request->getModuleName();

		if($result)
		{
			$change = "disabled";
			$this->view->change = $change;
		}

		$originalgrouparr = array('BAAPPV','BAMAKR');
		$originalgroup = in_array($groupid,$originalgrouparr);

		if($suspend && $status == 'Approved' && $this->view->hasPrivilege('BGSP') && !$originalgroup )
		{
			$this->_db->beginTransaction();
			try
			{
				Application_Helper_General::writeLog('BGSP','Suspend Backend group ('.$groupid.')');
				$info = "Backend Group";
				$change_id = $this->suggestionWaitingApproval('Backend Group',$info,$this->_changeType['code']['suspend'],null,'M_BGROUP','TEMP_BGROUP',$groupid,$groupname);
				$data = array(
										'CHANGES_ID' => $change_id,
										'BGROUP_ID' => $groupid,
										'BGROUP_DESC' => $groupname,
										'BGROUP_STATUS' => '2',
										'CREATED' => new Zend_Db_Expr('now()'),
										'CREATEDBY' => $this->_userIdLogin,
										);
				$this->_db->insert('TEMP_BGROUP',$data);
				$temp = 1;
			}
			catch(Exception $e)
			{
				$temp = 0;
				$this->_db->rollBack();
			}

			if($temp == 1)
			{
				$this->_db->commit();
				$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
				$this->_redirect('/notification/submited/index');
			}
			if($temp == 0)
			{
				$this->_db->rollBack();
			}
		}

		if($unsuspend && $status == 'Suspended' && $this->view->hasPrivilege('BGSP') && !$originalgroup)
		{
			$this->_db->beginTransaction();
			try
			{
				Application_Helper_General::writeLog('BGSP','Unsuspend Backend group ('.$groupid.')');
				$info = "Backend Group";
				$change_id = $this->suggestionWaitingApproval('Backend Group',$info,$this->_changeType['code']['unsuspend'],null,'M_BGROUP','TEMP_BGROUP',$groupid,$groupname);
				$data = array(
										'CHANGES_ID' => $change_id,
										'BGROUP_ID' => $groupid,
										'BGROUP_DESC' => $groupname,
										'BGROUP_STATUS' => '1',
										'CREATED' => new Zend_Db_Expr('now()'),
										'CREATEDBY' => $this->_userIdLogin,
										);
				$this->_db->insert('TEMP_BGROUP',$data);
				$temp = 1;
			}
			catch(Exception $e)
			{
				$temp = 0;
				$this->_db->rollBack();
			}

			if($temp == 1)
			{
				$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
				$this->_db->commit();
				$this->_redirect('/notification/submited/index');
			}
			if($temp == 0)
			{
				$this->_db->rollBack();
			}
		}

		if($delete && $this->view->hasPrivilege('BGDL') && !$originalgroup)
		{
			$this->_db->beginTransaction();
			try
			{
				Application_Helper_General::writeLog('BGDL','Delete Backend group ('.$groupid.')');
				$info = "Backend Group";
				$change_id = $this->suggestionWaitingApproval('Backend Group',$info,$this->_changeType['code']['delete'],null,'M_BGROUP','TEMP_BGROUP',$groupid,$groupname);
				$data = array(
										'CHANGES_ID' => $change_id,
										'BGROUP_ID' => $groupid,
										'BGROUP_DESC' => $groupname,
										'BGROUP_STATUS' => '3',
										'CREATED' => new Zend_Db_Expr('now()'),
										'CREATEDBY' => $this->_userIdLogin,
										);
				$this->_db->insert('TEMP_BGROUP',$data);
				$temp = 1;
			}
			catch(Exception $e)
			{
				$temp = 0;
				$this->_db->rollBack();
			}

			if($temp == 1)
			{
				$this->_db->commit();
				$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
				$this->_redirect('/notification/submited/index');
			}
			if($temp == 0)
			{
				$this->_db->rollBack();
			}
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('BGLS','View Backend Group Detail ('.$groupid.')');
		}
	}
}
