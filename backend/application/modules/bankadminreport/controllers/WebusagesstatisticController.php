<?php
require_once 'Zend/Controller/Action.php';

class Bankadminreport_WebusagesstatisticController extends Application_Main
{	

	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{			
		$this->_helper->layout()->setLayout('newlayout');

		$arrCust 	 = $this->getCustomerCode();

		$arrFPrivi   = $this->getFPriviCode();
		$arrShowType = $this->showType();
		
		$this->view->arrCust 		= $arrCust;
		$this->view->arrFPrivi 		= $arrFPrivi;
		$this->view->arrShowType 	= $arrShowType;
		
		$day = $this->language->_('Date');
		$hitPerDay = $this->language->_('Hit per Day');
		$week = $this->language->_('Date');
		$hitPerWeek = $this->language->_('Hit per Week');
		
		$month = $this->language->_('Date');
		$hitPerMonth = $this->language->_('Hit per Month');
		
		
		$fieldsdaily = array(

								'days'  		=> array(	
														'field' => 'days',
														'label' => 'Day',
														'sortable' => true
													),
								'day'  		=> array(	
														'field' => 'day',
														'label' => $day,
														'sortable' => true
													),
								'hitperday' => array(
														'field' => 'hitperday',
														'label' => $hitPerDay,
														'sortable' => true
													)
						);
		
		$fieldsweekly = array(
								'week'  	=> array	(	
														'field' => 'week',
														'label' => $week,
														'sortable' => true
													),
								'hitperweek' => array(
														'field' => 'hitperweek',
														'label' => $hitPerWeek,
														'sortable' => true
													)
						);
		
		$fieldsmonthly = array	(
		
							'month'  	=> array	(	
													'field' => 'month',
													'label' => $month,
													'sortable' => true
												),
							'hitpermonth'  => array(
													'field' => 'hitpermonth',
													'label' => $hitPerMonth,
													'sortable' => true
												)
						);

		$filterlist = array('PS_CREATED','COMP_ID','ACTION','SHOW','COMP_NAME');
		
		$this->view->filterlist = $filterlist;
		
		//validasi page, jika input page bukan angka               
		$page 		 = $this->_getParam('page');
		$csv 		 = $this->_getParam('csv');
		$pdf 		 = $this->_getParam('pdf');
		
		$filter 	 = $this->_getParam('filter');
		$clearfilter = $this->_getParam('clearfilter');
		$fSHOWTYPE 	 = $this->_getParam('SHOW');
				
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		if($fSHOWTYPE == '' OR $fSHOWTYPE == '1'){ $fields = $fieldsdaily; }
		else if($fSHOWTYPE == '2'){ $fields = $fieldsweekly; }
		else if($fSHOWTYPE == '3'){ $fields = $fieldsmonthly; }
		
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$filterArr = array('COMP_ID'     	=> array('StripTags','StringTrim','StringToUpper'),
						   'COMP_NAME'   	=> array('StripTags','StringTrim','StringToUpper'),
						   'PS_CREATED'  		=> array('StripTags','StringTrim'),
						   'PS_CREATED_END'  		=> array('StripTags','StringTrim'),
						   'ACTION' 		=> array('StripTags','StringTrim'),
						   'SHOW' 		=> array('StripTags','StringTrim'),

						  );
		
		// if POST value not null, get post, else get param
		
    	$dataParam = array('COMP_ID','COMP_NAME','ACTION','SHOW');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_CREATED"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		// print_r($dataParamValue);
		// die;	
		// print_r($this->_request->getParam('whereval'));die;
			if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}
		
		//echo "<pre>";
		//print_r ($dataParamValue);
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($arrCust)))),
						'COMP_NAME' 	=> array(),	
						'PS_CREATED' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_CREATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'ACTION' 	=> array(array('InArray', array('haystack' => array_keys($arrFPrivi)))),	
						'SHOW' 		=> array(array('InArray', array('haystack' => array_keys($arrShowType)))),							
						);
		
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter 	= $this->_getParam('filter');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');
		
//		$fCOMPANYID      = $zf_filter->getEscaped('COMPANYID');
//		$fCOMPANYNAME    = $zf_filter->getEscaped('COMPANYNAME');
		$fCOMPANYID      = $this->_request->getParam('COMP_ID');
		$fCOMPANYNAME    = $this->_request->getParam('COMP_NAME');
		
		if($filter == NULL && $clearfilter != 1){
			
			$fDATETO 	= date("d/m/Y");
			
			$Y = date('Y');
			$d = date('d');
			$m = date('m',strtotime("-1 month")); //last month
			if($m == '12') $Y = $Y -1;
			
			$fDATEFROM = $d.'/'.$m.'/'.$Y;

		}
		else{
			if($filter != NULL){
				$fDATEFROM 	= html_entity_decode($zf_filter->getEscaped('PS_CREATED'));
				$fDATETO 	= html_entity_decode($zf_filter->getEscaped('PS_CREATED_END'));
			}
			else if($clearfilter == 1){
				$fDATEFROM 	= "";
				$fDATETO 	= "";
			}
		}
		
//		$fPRIVICODE  = $zf_filter->getEscaped('PRIVICODE');
//		$fSHOWTYPE   = $zf_filter->getEscaped('SHOWTYPE');
		
		$fPRIVICODE  = $this->_request->getParam('ACTION');
		$fSHOWTYPE   = $this->_request->getParam('SHOW');
		
		
		if($fSHOWTYPE =='' OR $fSHOWTYPE == 1){
			
			/*
				select DATE("LOG_DATE") as days,
				count("LOG_DATE") as hit
				from T_FACTIVITY
				group by DATE("LOG_DATE")
				ORDER BY DATE("LOG_DATE") ASC
			*/
			
			$select = $this->_db->select()
								->from(	array(	'FA'		=>'T_FACTIVITY'),
										array(	'days'		=>'DATE(LOG_DATE)',
												'day'		=>'DATE(LOG_DATE)',
												'hitperday'	=>'count(LOG_DATE)'
												))
								->joinLeft( array('C'=>'M_CUSTOMER'),'FA.CUST_ID = C.CUST_ID',array())
								->group('DATE(LOG_DATE)')
								->order('LOG_DATE DESC');
			//echo $select;
			
		}
		else if( $fSHOWTYPE == 2){
			
			/*
				SELECT DISTINCT 
				DATEPART(WK, "LOG_DATE") week,
				COUNT("LOG_DATE") as hit

				FROM T_FACTIVITY
				GROUP BY DATEPART(WK, "LOG_DATE")
				ORDER BY DATEPART(WK, "LOG_DATE") ASC
			*/
		
			$select = $this->_db->select()
								->from(	array(	'FA'		=>'T_FACTIVITY'),
										array(	'week'		=>'WEEK(LOG_DATE)',
												'hitperweek'=>'count(LOG_DATE)'
												))
								->joinLeft( array('C'=>'M_CUSTOMER'),'FA.CUST_ID = C.CUST_ID',array())
								->group('WEEK(LOG_DATE)')
								->order('LOG_DATE DESC');
			
		
		}
		else if( $fSHOWTYPE == 3){ 
			$select = $this->_db->select()
								->from(	array(	'FA'		 =>'T_FACTIVITY'),
										array(	'month'		 =>'MONTH(LOG_DATE)',
												'year'		 =>'YEAR(LOG_DATE)',
												'hitpermonth'=>'count(LOG_DATE)'
												))
								->joinLeft( array('C'=>'M_CUSTOMER'),'FA.CUST_ID = C.CUST_ID',array())
								->group(array('YEAR(LOG_DATE)','MONTH(LOG_DATE)'))
								->order('LOG_DATE DESC');
			
			// echo "<pre>";
			// echo $select->__toString();
			// die;
		
		}
		
		/*
			TA.ACTION_DESC 		= PRIVICODE
			TA.ACTION_FULLDESC 	= PRIVICODE DESCRIPTION
		*/
		
		if($fCOMPANYID)		{ 
			$select->where('UPPER(FA.CUST_ID) = '.$this->_db->quote(strtoupper($fCOMPANYID)));
			
			// echo "<pre>";
			// echo $select->__toString();
			// die;
			
		}
		if($fCOMPANYNAME)	{ $select->where('UPPER(C.CUST_NAME) LIKE '.$this->_db->quote(strtoupper('%'.$fCOMPANYNAME.'%')));}
		if($fPRIVICODE)		{ $select->where('UPPER(FA.ACTION_DESC) = '.$this->_db->quote(strtoupper($fPRIVICODE)));}
		
		if($fDATEFROM){
			
			$FormatDateFrom = new Zend_Date($fDATEFROM, $this->_dateDisplayFormat);
			$dateFrom   	= $FormatDateFrom->toString($this->_dateDBFormat);	
			
			if($fSHOWTYPE == "" OR $fSHOWTYPE == 1){ $select->where('DATE(FA.LOG_DATE) >= '.$this->_db->quote(strtoupper($dateFrom))); }
			elseif($fSHOWTYPE == 2){ 
				
				$select->where('DATE(FA.LOG_DATE) >= '.$this->_db->quote(strtoupper($dateFrom)));
				
				// $select->where('DATEPART(WK, "LOG_DATE") >= DATEPART(WK,'."'$dateFrom'".')');
			}
			elseif($fSHOWTYPE == 3){ 
			
				$select->where('DATE(FA.LOG_DATE) >= '.$this->_db->quote(strtoupper($dateFrom)));
				
				// $select->where('DATEPART(YEAR, "LOG_DATE") >= DATEPART(YEAR,'."'$dateFrom'".')');
				// $select->where('DATEPART(MM, "LOG_DATE") >= DATEPART(MM,'."'$dateFrom'".')');
			}
			
		
		}
		
		if($fDATETO){
			
			$FormatDateTo 	= new Zend_Date($fDATETO, $this->_dateDisplayFormat);
			$dateTo  		= $FormatDateTo->toString($this->_dateDBFormat);	
			
			if($fSHOWTYPE == "" OR $fSHOWTYPE == 1){ 
				
				$select->where('DATE(FA.LOG_DATE) <= '.$this->_db->quote(strtoupper($dateTo))); 
			}
				
			elseif($fSHOWTYPE == 2){ 
				
				$select->where('DATE(FA.LOG_DATE) <= '.$this->_db->quote(strtoupper($dateTo))); 
				// $select->where('DATEPART(WK, "LOG_DATE") <= DATEPART(WK,'."'$dateTo'".')'); 
			}
			elseif($fSHOWTYPE == 3){ 
				
				$select->where('DATE(FA.LOG_DATE) <= '.$this->_db->quote(strtoupper($dateTo)));
				
				// $select->where('DATEPART(YEAR, "LOG_DATE") <= DATEPART(YEAR,'."'$dateTo'".')');
				// $select->where('DATEPART(MM, "LOG_DATE") <= DATEPART(MM,'."'$dateTo'".')');
				
			}
			
		}
		
		// echo "<pre>";
		// echo $select->__toString();
		// die;
		
		$select->order($sortBy.' '.$sortDir);
		$dataSQL = $this->_db->fetchAll($select);
		
		// Zend_Debug::dump($select->__toString());die;
		//-- get total hit  & average -- //
		
			$total = 0;
			foreach($dataSQL as $key =>$dt){
				
				if($fSHOWTYPE == "" OR $fSHOWTYPE == "1"){ $total = $total + $dt["hitperday"]; }
				elseif($fSHOWTYPE == "2"){ $total = $total + $dt["hitperweek"]; }
				elseif($fSHOWTYPE == "3"){ $total = $total + $dt["hitpermonth"]; }
			
			}
			
			@$avg = round($total / count($dataSQL),2);
			
		//-- end of get total hit & average -- //
		
		if ($csv || $pdf || $this->_request->getParam('print'))
		{	$headers  = Application_Helper_Array::simpleArray($fields, "label");}
		else
		{
			$this->paging($dataSQL);
			$dataSQL = $this->view->paginator;
		}
		
		// echo "<pre>";
		// print_r ($dataSQL);
		//die;
		if(!empty($dataSQL) && count($dataSQL) > 0){
		
			foreach ($dataSQL as $d => $dt){					
				
				foreach ($fields as $key => $field){
					$value = $dt[$key];
					
					if($fSHOWTYPE == "" OR $fSHOWTYPE == "1"){ 
						if($key== "day"){ 
							$value = Application_Helper_General::convertDate($value,"dd MMM Y",$this->view->defaultDateFormat); 
						}
						if($key == "days"){
							$value = Application_Helper_General::convertDate($value, Zend_Date::WEEKDAY,$this->view->defaultDateFormat); 
						}

						if($key == "hitperday"){
							$value = number_format($value,0,",",".");
						}
						
					}
					elseif($fSHOWTYPE == "2"){
						if($key== "week"){ $value = 'Week '.$value;}
						
					}
					elseif($fSHOWTYPE == "3"){ 
						if($key== "month"){ 
							$monthname = $this->changeMonthName($dt["month"]);
							$value = $monthname." ".$dt["year"]; 
						}
					}
					
					$data[$d][$key] = $value;
				}
			}
			
			//count totalhit and average:
			
			$dataPDFCSV = $data;
			$dataPDFCSV[$d+1]['number'] = false;
			$dataPDFCSV[$d+1][0] = $this->language->_('Total Hit');
			$dataPDFCSV[$d+1][3] = $total;

			$dataPDFCSV[$d+2]['number'] = false;
			$dataPDFCSV[$d+2][0] = $this->language->_('Average Hit');
			$dataPDFCSV[$d+2][3] = $avg;
			
			
		}
		//else{ $data = array(); }
		else{ $dataPDFCSV = $data = array(); }
		//if ($csv || $pdf){	$headers = Application_Helper_Array::simpleArray($fields, "label");}
		
		if($csv){
			if(!empty($dataSQL) && count($dataSQL) > 0){
				foreach ($dataSQL as $d => $dt){					
					foreach ($fields as $key => $field){
						$value = $dt[$key];
						if($fSHOWTYPE == "" OR $fSHOWTYPE == "1"){ 
							if($key== "day"){ $value = Application_Helper_General::convertDate($value,"dd MMM Y",$this->view->defaultDateFormat); }
							if($key == "days"){
								$value = Application_Helper_General::convertDate($value,"l",$this->view->defaultDateFormat); 
							}

							if($key == "hitperday"){
								$value = number_format($value,0,",",".");
							}
							
						}
						elseif($fSHOWTYPE == "2"){
							if($key== "week"){ $value = 'Week '.$value;}
							
						}
						elseif($fSHOWTYPE == "3"){ 
							if($key== "month"){ 
								$monthname = $this->changeMonthName($dt["month"]);
								$value = $monthname." ".$dt["year"]; 
							}
						}
						
						$data[$d][$key] = $value;
					}
				}
				
				//count totalhit and average:
				
				$dataPDFCSV = $data;
//				$dataPDFCSV[$d+1]['number'] = false;
				$dataPDFCSV[$d+1][0] = $this->language->_('Total Hit');
				$dataPDFCSV[$d+1][3] = $total;
	
//				$dataPDFCSV[$d+2]['number'] = false;
				$dataPDFCSV[$d+2][0] = $this->language->_('Average Hit');
				$dataPDFCSV[$d+2][3] = $avg;
			}
			//else{ $data = array(); }
			else{ $dataPDFCSV = $data = array(); }
			
			$this->_helper->download->csv($headers,$dataPDFCSV,null,$this->language->_('Web Usages Report'));  
			Application_Helper_General::writeLog('ARWS','Export CSV Web Usages Report');
		}
		elseif($pdf){
		
			$this->_helper->download->pdf($headers,$dataPDFCSV,null,$this->language->_('Web Usages Report'));  
			Application_Helper_General::writeLog('ARWS','Export PDF Web Usages Report');
		}
		elseif($this->_request->getParam('print') == 1){
		if(!empty($dataSQL) && count($dataSQL) > 0){
				foreach ($dataSQL as $d => $dt){					
					foreach ($fields as $key => $field){
						$value = $dt[$key];
						if($fSHOWTYPE == "" OR $fSHOWTYPE == "1"){ 
							if($key== "day"){ $value = Application_Helper_General::convertDate($value,"dd MMM Y",$this->view->defaultDateFormat); }
							// if($key == "days"){
							// 	$value = Application_Helper_General::convertDate($value,"l",$this->view->defaultDateFormat); 
							// }
							if($key == "hitperday"){
								$value = number_format($value,0,",",".");
							}
							$key1 = 'day';
							$key2 = 'hitperday';
						}
						elseif($fSHOWTYPE == "2"){
							if($key== "week"){ $value = 'Week '.$value;}
							$key1 = 'week';
							$key2 = 'hitperweek';
						}
						elseif($fSHOWTYPE == "3"){ 
							if($key== "month"){ 
								$monthname = $this->changeMonthName($dt["month"]);
								$value = $monthname." ".$dt["year"]; 
							}
							$key1 = 'month';
							$key2 = 'hitpermonth';
						}
						
						$data[$d][$key] = $value;
					}
				}
				
				//count totalhit and average:
				
				$dataPDFCSV = $data;
//				$dataPDFCSV[$d+1]['number'] = false;
				$dataPDFCSV[$d+1][$key1] = $this->language->_('Total Hit');
				$dataPDFCSV[$d+1][$key2] = $total;
	
//				$dataPDFCSV[$d+2]['number'] = false;
				$dataPDFCSV[$d+2][$key1] = $this->language->_('Average Hit');
				$dataPDFCSV[$d+2][$key2] = $avg;
			}
			//else{ $data = array(); }
			else{ $dataPDFCSV = $data = array(); }
			
			unset($fields['action']);

			// echo "<pre>";
			// var_dump($data);
			// die();

			foreach($dataPDFCSV as $key=>$row)
			{
				$dataPDFCSV[$key]['days'] = Application_Helper_General::convertDate($row['days'], Zend_Date::WEEKDAY);
				
			}

			$this->_forward('print', 'index', 'widget', array('data_content' => $dataPDFCSV, 'data_caption' => 'Web Usages Report', 'data_header' => $fields));
			Application_Helper_General::writeLog('ARWS','Print Web Usages Report');
		}
		else{	
		
			$stringParam = array( 	'COMP_ID'		=>'fCOMPANYID',
									'COMP_NAME'	=>'fCOMPANYNAME',
									'PS_CREATED'		=>'fDATEFROM',
									'PS_CREATED_END'		=>'fDATETO',
									'ACTION'		=>'fPRIVICODE',
									'SHOW'		=>'fSHOWTYPE',
										
									'clearfilter'		=> $clearfilter,
									'filter'			=> $filter,
								);
				
			$this->view->COMPANYID = $fCOMPANYID;
			$this->view->COMPANYNAME = $fCOMPANYNAME;
			$this->view->DATEFROM 	 = $fDATEFROM;
			$this->view->DATETO 	 = $fDATETO;
			$this->view->PRIVICODE 	 = $fPRIVICODE;
			$this->view->SHOWTYPE 	 = $fSHOWTYPE;

			$this->view->fields 		= $fields;
			$this->view->filter 		= $filter;
			$this->view->clearfilter	= $clearfilter;
								
			$this->view->sortBy 		= $sortBy;
			$this->view->sortDir 		= $sortDir;
			$this->view->data 			= $data;
			//$this->paging($data,30); 
				
			$this->view->arrCust 		= $arrCust;
			$this->view->arrFPrivi 		= $arrFPrivi;
			$this->view->arrShowType 	= $arrShowType;
			
			$this->view->total 	= number_format($total,0,",",",");
			$this->view->avg 	= $avg;
		}
		
		Application_Helper_General::writeLog('ARWS','View Web (Hit) Statistic');

		if(!empty($dataParamValue)){
	    		$this->view->createStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createEnd = $dataParamValue['PS_CREATED_END'];

	    	  	unset($dataParamValue['PS_CREATED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }

	}	
	
	function changeMonthName($monthnumber){
	
		$arrmonth = array(
						'1'=>'January',
						'2'=>'February',
						'3'=>'March',
						'4'=>'April',
						'5'=>'May',
						'6'=>'June',
						'7'=>'July',
						'8'=>'August',
						'9'=>'September',
						'10'=>'October',
						'11'=>'November',
						'12'=>'December'
						
					);
		foreach($arrmonth as $key => $value){
			if($key == $monthnumber){ $result = $value; }
		}
		return $result;
	}
	
	function showType(){
	
		$arr = array(	'1'=>$this->language->_('Daily'),
						'2'=>$this->language->_('Weekly'),
						'3'=>$this->language->_('Monthly')
					);
		return $arr;
		
	}
	
	function getCustomerCode(){
		
       $select = $this->_db->select()
      	                   ->from(	array('M'			=>'M_CUSTOMER'),
									array('custid'		=>'CUST_ID') )
      	                   ->query()->fetchAll();
		$result =  array();				   
		foreach($select as $key => $val){
			
			$a = $select[$key]["custid"];
			$result[$a] = $a;
			
		}
		return $result;
	  
    }
	
	function getFPriviCode(){
		 $select = $this->_db->select()
      	                   ->from(	array(	'M'			=>'M_FPRIVILEGE'),
									array(	'privicode'	=>'FPRIVI_ID',
											'prividesc'	=>'FPRIVI_DESC') )
      	                   ->query()->fetchAll();
						   
		foreach($select as $key => $val){
			
			$privicode = $select[$key]["privicode"];
			$prividesc = $select[$key]["prividesc"];
			$result[$privicode] = $prividesc;
			
		}
		return $result;
	
	}
	
}
