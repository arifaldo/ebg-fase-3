<?php

class linefacility_Model_Linefacility
{
	protected $_db;

	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getData($filterParam = null, $filter = null)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'M_CUST_LINEFACILITY'),
				array('*')
			)
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME', 'COLLECTIBILITY_CODE')
			);

		if ($filter == TRUE) {
			if ($filterParam['fCompanyCode']) {
				$select->where('A.CUST_ID LIKE ' . $this->_db->quote('%' . $filterParam['fCompanyCode'] . '%'));
			}
			if ($filterParam['fCompanyName']) {
				$select->where('B.CUST_NAME LIKE ' . $this->_db->quote('%' . $filterParam['fCompanyName'] . '%'));
			}
			if ($filterParam['fCollectCode']) {
				$select->where('B.COLLECTIBILITY_CODE = ?', $filterParam['fCollectCode']);
			}
		}
		return $this->_db->fetchAll($select);
	}

	public function getDataById($id)
	{
		$data = $this->_db->select()
			->from(array('A' => 'M_CUST_LINEFACILITY'), array('*'))
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME', 'CUST_MODEL', 'COLLECTIBILITY_CODE')
			)
			->where('A.ID = ?', $id);
		return $this->_db->fetchRow($data);
	}

	public function getDataByCustId($cust_id)
	{
		$data = $this->_db->select()
			->from(array('A' => 'M_CUST_LINEFACILITY'), array('*'))
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME', 'COLLECTIBILITY_CODE')
			)
			->where('A.CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($data);
	}

	public function getCreditQuality()
	{
		$select = $this->_db->select()
			->from('M_CREDIT_QUALITY')
			->query()->fetchAll();
		return $select;
	}

	public function getAgreement()
	{
		$select = $this->_db->select()
			->from('M_AGREEMENT')
			->query()->fetchAll();
		return $select;
	}

	public function getCustomer()
	{
		$cust_id = $this->_db->select()
			->from(array('M_CUST_LINEFACILITY'), array('CUST_ID'))
			->query()->fetchAll();
		$select = $this->_db->select()
			->from('M_CUSTOMER')
			->where('CUST_MODEL != 3') // 3 = Special Obligee
			->where('CUST_ID NOT IN (?)', $cust_id)
			->query()->fetchAll();
		return $select;
	}

	public function getCustomerById($cust_id)
	{
		$select = $this->_db->select()
			->from('M_CUSTOMER')
			->where('CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($select);
	}

	public function getCustAcctById($cust_id, $acctType = null)
	{
		$data = $this->_db->select()
			->from(
				array('A' => 'M_CUSTOMER_ACCT'),
				array(
					'ACCT_NO'	=> 'A.ACCT_NO',
					'ACCT_NAME'	=> 'A.ACCT_NAME',
					'CCY_ID'	=> 'A.CCY_ID',
					'ACCT_DESC'	=> 'A.ACCT_DESC'
				)
			)
			->where('A.CUST_ID = ?', $cust_id);
		if ($acctType) {
			$data = $data->where("ACCT_TYPE = ?", $acctType);
		}
		return $this->_db->fetchAll($data);
	}

	public function getCustAcctByAcct($acct_no)
	{
		$data = $this->_db->select()
			->from(
				array('A' => 'M_CUSTOMER_ACCT'),
				array(
					'ACCT_NO'	=> 'A.ACCT_NO',
					'ACCT_NAME'	=> 'A.ACCT_NAME',
					'CCY_ID'	=> 'A.CCY_ID',
					'ACCT_DESC'	=> 'A.ACCT_DESC'
				)
			)
			->where('A.ACCT_NO = ?', $acct_no);
		return $this->_db->fetchRow($data);
	}

	public function getTempLinefacility($cust_id)
	{
		$select = $this->_db->select()
			->from(array('A' => 'TEMP_CUST_LINEFACILITY'), array('*'))
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME', 'COLLECTIBILITY_CODE')
			)
			->where('A.CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($select);
	}
}
