<?php

require_once 'Zend/Controller/Action.php';

class Generalsetting_IndexController extends Application_Main
{

    public function indexAction()
	{
		// $this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()
					->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					->where('MODULE_ID = ?' , 'GNS')
					->query()->FetchAll();
		$setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		foreach($setting as $key=>$value)
		{
			if(!empty($key))
				$this->view->$key = $value;
		}

		// convert to validate
//			$threshold_rtgs = $this->_getParam('threshold_rtgs');
//			$threshold_rtgs_val =	Application_Helper_General::convertDisplayMoney($threshold_rtgs);
//			$this->_setParam('threshold_rtgs',$threshold_rtgs_val);

			$threshold_lld = $this->_getParam('threshold_lld');
			$threshold_lld_val =	Application_Helper_General::convertDisplayMoney($threshold_lld);
			$this->_setParam('threshold_lld',$threshold_lld_val);

			$admin_fee_company = $this->_getParam('admin_fee_company');
			$admin_fee_company_val =	Application_Helper_General::convertDisplayMoney($admin_fee_company);
			$this->_setParam('admin_fee_company',$admin_fee_company_val);

			$admin_fee_account = $this->_getParam('admin_fee_account');
			$admin_fee_account_val =	Application_Helper_General::convertDisplayMoney($admin_fee_account);
			$this->_setParam('admin_fee_account',$admin_fee_account_val);

			$global_charges_skn = $this->_getParam('global_charges_skn');
			$global_charges_skn_val =	Application_Helper_General::convertDisplayMoney($global_charges_skn);
			$this->_setParam('global_charges_skn',$global_charges_skn_val);

			$global_charges_rtgs = $this->_getParam('global_charges_rtgs');
			$global_charges_rtgs_val =	Application_Helper_General::convertDisplayMoney($global_charges_rtgs);
			$this->_setParam('global_charges_rtgs',$global_charges_rtgs_val);

			$threshold_lld_remittance = $this->_getParam('threshold_lld_remittance');
			$threshold_lld_remittance_val =	Application_Helper_General::convertDisplayMoney($threshold_lld_remittance);
			$this->_setParam('threshold_lld_remittance',$threshold_lld_remittance_val);

			$remittance_limit_permonthly = $this->_getParam('remittance_limit_permonthly');
			$remittance_limit_permonthly_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_permonthly);
			$this->_setParam('remittance_limit_permonthly',$remittance_limit_permonthly_val);


			$min_amount_skn = $this->_getParam('min_amount_skn');
			$min_amount_skn_val =	Application_Helper_General::convertDisplayMoney($min_amount_skn);
			$this->_setParam('min_amount_skn',$min_amount_skn_val);

			$max_amount_skn = $this->_getParam('max_amount_skn');
			$max_amount_skn_val =	Application_Helper_General::convertDisplayMoney($max_amount_skn);
			$this->_setParam('max_amount_skn',$max_amount_skn_val);

			$min_amount_rtgs = $this->_getParam('min_amount_rtgs');
			$min_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($min_amount_rtgs);
			$this->_setParam('min_amount_rtgs',$min_amount_rtgs_val);

			$max_amount_rtgs = $this->_getParam('max_amount_rtgs');
			$max_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($max_amount_rtgs);
			$this->_setParam('max_amount_rtgs',$max_amount_rtgs_val);

			$escrow_acct_idr = $this->_getParam('escrow_acct_idr');
			// $escrow_acct_idr_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_idr);
			$this->_setParam('escrow_acct_idr',$escrow_acct_idr);

			$escrow_acct_usd = $this->_getParam('escrow_acct_usd');
			// $escrow_acct_usd_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_usd);
			$this->_setParam('escrow_acct_usd',$escrow_acct_usd);

			$remittance_limit_per_month = $this->_getParam('remittance_limit_per_month');
			$remittance_limit_per_month_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_per_month);
			$this->_setParam('remittance_limit_per_month',$remittance_limit_per_month_val);

			/* $SKN_cost = $this->_getParam('SKN_cost');
			$SKN_cost_val =	Application_Helper_General::convertDisplayMoney($SKN_cost);
			$this->_setParam('SKN_cost',$SKN_cost_val);

			$RTGS_cost = $this->_getParam('RTGS_cost');
			$RTGS_cost_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_cost',$RTGS_cost_val); */

			/* $SKN_charge = $this->_getParam('SKN_charge');
			$SKN_charge_val =	Application_Helper_General::convertDisplayMoney($SKN_charge);
			$this->_setParam('SKN_charge',$SKN_charge_val);

			$RTGS_charge = $this->_getParam('RTGS_charge');
			$RTGS_charge_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_charge',$RTGS_charge_val); */

			//FILTER
			{
			$filters = array(
							'isenabled' => array('StringTrim','StripTags'),
							'auto_release_payment' => array('StringTrim','StripTags'),
							// 'auto_monthlyfee' => array('StringTrim','StripTags'),
							'remittance_limit_per_month' => array('StringTrim','StripTags'),
							'disable_note' => array('StringTrim','StripTags'),
							'note' => array('StringTrim','StripTags'),
							'paging_frontend' => array('StringTrim','StripTags'),
							'max_import_bene' => array('StringTrim','StripTags'),
							'max_import_single_payment' => array('StringTrim','StripTags'),
							'range_futuredate' => array('StringTrim','StripTags'),
							'archiveafter' => array('StringTrim','StripTags'),
							'cut_off_time_rtgs' => array('StringTrim','StripTags'),
							'cut_off_time_skn' => array('StringTrim','StripTags'),
							'cut_off_time_bulkpayments' => array('StringTrim','StripTags'),
							'cut_off_time_bi' => array('StringTrim','StripTags'),
							'cut_off_time_remittance' => array('StringTrim','StripTags'),
			                'cut_off_running_balance' => array('StringTrim','StripTags'),
			                'start_running_balance' => array('StringTrim','StripTags'),
							'starting_time_remittance' => array('StringTrim','StripTags'),
//							'threshold_rtgs' => array('StringTrim','StripTags'),
							'threshold_lld' => array('StringTrim','StripTags'),
							'threshold_lld_remittance' => array('StringTrim','StripTags'),
							'remittance_limit_permonthly' => array('StringTrim','StripTags'),
							'min_amount_skn' => array('StringTrim','StripTags'),
							'max_amount_skn' => array('StringTrim','StripTags'),
							'min_amount_rtgs' => array('StringTrim','StripTags'),
							'max_amount_rtgs' => array('StringTrim','StripTags'),
            			    'escrow_acct_usd' => array('StringTrim','StripTags'),
            			    'escrow_acct_idr' => array('StringTrim','StripTags'),
            			    'cancel_local_remittance' => array('StringTrim','StripTags'),

							/*'admin_fee_company' => array('StringTrim','StripTags'),
							'admin_fee_account' => array('StringTrim','StripTags'),
							'global_charges_skn' => array('StringTrim','StripTags'),
							'global_charges_rtgs' => array('StringTrim','StripTags'),*/

							// 'SKN_cost' => array('StringTrim','StripTags'),
							// 'SKN_charge' => array('StringTrim','StripTags'),
							// 'RTGS_cost' => array('StringTrim','StripTags'),
							// 'RTGS_charge' => array('StringTrim','StripTags'),
							'max_row_multi_transaction' => array('StringTrim','StripTags'),
							'max_import_bulk' => array('StringTrim','StripTags'),
							'pwd_expired_day' => array('StringTrim','StripTags'),
							'timeoutafter' => array('StringTrim','StripTags'),
							'locknologinafter' => array('StringTrim','StripTags'),
							'lockfailloginafter' => array('StringTrim','StripTags'),
							'lockfailtokenafter' => array('StringTrim','StripTags'),
							'password_history' => array('StringTrim','StripTags'),
							'min_length_userid' => array('StringTrim','StripTags'),
							'max_length_userid' => array('StringTrim','StripTags'),
							'minfpassword' => array('StringTrim','StripTags'),
							'maxfpassword' => array('StringTrim','StripTags'),
							'b_timeoutafter' => array('StringTrim','StripTags'),
							'b_locknologinafter' => array('StringTrim','StripTags'),
							'b_lockfailloginafter' => array('StringTrim','StripTags'),
							'bpassword_history' => array('StringTrim','StripTags'),
							'min_length_buserid' => array('StringTrim','StripTags'),
							'max_length_buserid' => array('StringTrim','StripTags'),
							'minbpassword' => array('StringTrim','StripTags'),
							'maxbpassword' => array('StringTrim','StripTags'),
							'email_submission' => array('StringTrim','StripTags'),
							'email_exception' => array('StringTrim','StripTags'),
							'email_supplychain' => array('StringTrim','StripTags'),
							'email_valas' => array('StringTrim','StripTags'),
							);
			}

			//VALIDATE
			{
			$validators = array(
									'isenabled' => array(	'NotEmpty',
															'messages' => array
															('Can not be empty')),
                    			    'cancel_local_remittance' => array(	'NotEmpty',
                                        			        'messages' => array
                                        			        ('Can not be empty')),
									'auto_release_payment' => array(	'NotEmpty',
															'messages' => array
															('Can not be empty')),
									/* 'auto_monthlyfee' => array(	'NotEmpty',
															'messages' => array
															('Can not be empty')), */
									'disable_note' => array(	'NotEmpty',
															'messages' => array
															('Can not be empty')),
									'note' => array('NotEmpty',
															'messages' => array
															('Can not be empty')),
									'paging_frontend' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'max_import_bene' => array(
															'NotEmpty',
															'Digits',
															new Zend_Validate_Between(array('min'=>1, 'max'=> 1000)),
															'messages' => array
															(
															'Can not be empty',
															'Must be numeric values',
															'Value between 1 - 1000',
															)),
									'max_import_single_payment' => array('NotEmpty',
															'Digits',
															new Zend_Validate_Between(array('min'=>1, 'max'=> 1000)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 1000')),
									'range_futuredate' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'archiveafter' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'cut_off_time_rtgs' => array('NotEmpty',
															new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
															new Zend_Validate_Date(array('format' =>'HH:mm:ss')),
															array('StringLength',array('min'=>8,'max'=>8)),
															'messages' => array
															('Can not be empty',
															'Invalid Time Format',
															'Invalid Time Format',
															'Invalid Time Format'
															)),
									'cut_off_time_skn' => array('NotEmpty',
															new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
															new Zend_Validate_Date(array('format' =>'HH:mm:ss')),
															array('StringLength',array('min'=>8,'max'=>8)),
															'messages' => array
															('Can not be empty',
															'Invalid Time Format',
															'Invalid Time Format',
															'Invalid Time Format'
														)),
									'cut_off_time_bulkpayments' => array('NotEmpty',
															new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
															new Zend_Validate_Date(array('format' =>'HH:mm:ss')),
															array('StringLength',array('min'=>8,'max'=>8)),
															'messages' => array
															('Can not be empty',
															'Invalid Time Format',
															'Invalid Time Format',
															'Invalid Time Format'
															)),
									'cut_off_time_bi' => array('NotEmpty',
															new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
															new Zend_Validate_Date(array('format' =>'HH:mm:ss')),
															array('StringLength',array('min'=>8,'max'=>8)),
															'messages' => array
															('Can not be empty',
															'Invalid Time Format',
															'Invalid Time Format',
															'Invalid Time Format'
															)),
									'remittance_limit_per_month' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
																	//$this->language->_('Amount must be greater than 0.00'),
																	$this->language->_('Value between 1 - 9999999999.99'),
																	$this->language->_('Value between 1 - 9999999999.99'),)),
                    			    'escrow_acct_usd' => array('NotEmpty',
                                        			        'messages' => array
                                        			        ($this->language->_('Can not be empty'),
                                        			            )),
                    			    'escrow_acct_idr' => array('NotEmpty',
                                        			        'messages' => array
                                        			        ($this->language->_('Can not be empty'),
                                        			            )),
									'starting_time_remittance' => array('NotEmpty',
															new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
															new Zend_Validate_Date(array('format' =>'HH:mm:ss')),
															array('StringLength',array('min'=>8,'max'=>8)),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Invalid Time Format'),
															$this->language->_('Invalid Time Format'),
															$this->language->_('Invalid Time Format')
															)),
									'cut_off_time_remittance' => array('NotEmpty',
															new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
															new Zend_Validate_Date(array('format' =>'HH:mm:ss')),
															array('StringLength',array('min'=>8,'max'=>8)),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Invalid Time Format'),
															$this->language->_('Invalid Time Format'),
															$this->language->_('Invalid Time Format')
											)),
                    			    'start_running_balance' => array('NotEmpty',
                                        			        new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
                                        			        new Zend_Validate_Date(array('format' =>'HH:mm:ss')),
                                        			        array('StringLength',array('min'=>8,'max'=>8)),
                                        			        'messages' => array
                                        			        ($this->language->_('Can not be empty'),
                                        			            $this->language->_('Invalid Time Format'),
                                        			            $this->language->_('Invalid Time Format'),
                                        			            $this->language->_('Invalid Time Format')
                                        			        )),
                    			     'cut_off_running_balance' => array('NotEmpty',
                                    			            new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/'),
                                    			            new Zend_Validate_Date(array('format' =>'HH:mm:ss')),
                                    			            array('StringLength',array('min'=>8,'max'=>8)),
                                    			            'messages' => array
                                    			            ($this->language->_('Can not be empty'),
                                    			                $this->language->_('Invalid Time Format'),
                                    			                $this->language->_('Invalid Time Format'),
                                    			                $this->language->_('Invalid Time Format')
                                    			            )),
//									'threshold_rtgs' => array('NotEmpty',
//															array('Between', array('min'=>1,'max'=>9999999999999.99)),
//															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
//															'messages' => array
//															('Can not be empty',
//															'Value between 1 - 9999999999999.99',
//															'Must be numeric values',	)),
									'threshold_lld' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 1 - 9999999999999.99',
															'Must be numeric values',	)),

									'threshold_lld_remittance' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 1 - 9999999999999.99',
															'Must be numeric values',	)),
// 									'remittance_limit_per_month' => array('NotEmpty',
// 															array('Between', array('min'=>1,'max'=>9999999999999.99)),
// 															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
// 															'messages' => array
// 															('Can not be empty',
// 															'Value between 1 - 9999999999999.99',
// 															'Must be numeric values',	)),

									'min_amount_skn' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),

									'max_amount_skn' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),

									'min_amount_rtgs' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'max_amount_rtgs' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),


									/*'admin_fee_company' => array('NotEmpty',
															array('Between', array('min'=>0,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 0.00 - 9999999999999.99',
															'Must be numeric values',	)),
									'admin_fee_account' => array('NotEmpty',
															array('Between', array('min'=>0,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 0.00 - 9999999999999.99',
															'Must be numeric values',	)),

									'global_charges_skn' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 1 - 9999999999999.99',
															'Must be numeric values',	)),

									'global_charges_rtgs' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Value between 1 - 9999999999999.99',
															'Must be numeric values',	)),*/


									/* 'SKN_cost' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 9999999999999.99',
															'Value between 1 - 9999999999999.99',)),
									'RTGS_cost' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 9999999999999.99',
															'Value between 1 - 9999999999999.99',)),*/
									/* 'SKN_charge' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 9999999999999.99',
															'Value between 1 - 9999999999999.99',)),
									'RTGS_charge' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 9999999999999.99',
															'Value between 1 - 9999999999999.99',)), */
									'max_row_multi_transaction' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'max_import_bulk' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															array('Between', array('min'=>1,'max'=>5000)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)',
															'Value between 1 - 5000')),
									'pwd_expired_day' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'timeoutafter' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'locknologinafter' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'lockfailloginafter' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'lockfailtokenafter' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'password_history' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'min_length_userid' => array('NotEmpty',
															'Digits',
															array('Between', array('min'=>1,'max'=>$this->_getParam('max_length_userid'))),
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - (Frontend) Max length of User ID',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'max_length_userid' => array('NotEmpty',
															'Digits',
															array('Between', array('min'=>$this->_getParam('min_length_userid'),'max'=>16)),
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between (Frontend) Min length of User ID - 16',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'minfpassword' => array('NotEmpty',
															'Digits',
															array('Between', array('min'=>1,'max'=>$this->_getParam('maxfpassword'))),
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - (Frontend) Max length of password',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'maxfpassword' => array('NotEmpty',
															'Digits',
															array('Between', array('min'=>$this->_getParam('minfpassword'),'max'=>1000)),
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value must be greater than (Frontend) Min length of password',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'b_timeoutafter' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'b_locknologinafter' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'b_lockfailloginafter' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'bpassword_history' => array('NotEmpty',
															'Digits',
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'min_length_buserid' => array('NotEmpty',
															'Digits',
															array('Between', array('min'=> 1 ,'max'=>$this->_getParam('max_length_buserid'))),
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - (Backend) Max length of User ID',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'max_length_buserid' => array('NotEmpty',
															'Digits',
															array('Between', array('min'=>$this->_getParam('min_length_buserid'),'max'=>16)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between (Backend) Min length of User ID - 16',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'minbpassword' => array('NotEmpty',
															'Digits',
															array('Between', array('min'=> 1 ,'max'=> $this->_getParam('maxbpassword'))),
															array('StringLength',array('min'=>1,'max'=>13)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - (Backend) Max length of password',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'maxbpassword' => array('NotEmpty',
															'Digits',
															array('Between', array('min'=>$this->_getParam('minbpassword'),'max'=>1000)),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value must be greater than (Backend) Min length of password',
															'Too many significant digits. Maximum digit allowed 13 digit(s)')),
									'email_exception' => array('NotEmpty',
															new Zend_Validate_Regex('/^([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4})(\s*;\s*([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4}))*$/'),
															'messages' => array
															('Can not be empty',
																'Invalid email address',)),
									'email_valas'    => array('NotEmpty',
															//new Application_Validate_EmailAddress(),
															array('StringLength',array('min'=>1,'max'=>128)),
															new Zend_Validate_Regex('/^([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4})(\s*;\s*([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4}))*$/'),
															'messages' => array(
															$this->language->_('Can not be empty'),
															$this->language->_('Email lenght cannot be more than 128'),
															$this->language->_('Invalid email address'))),
									'email_submission' => array('NotEmpty',
															new Zend_Validate_Regex('/^([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4})(\s*;\s*([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4}))*$/'),
															'messages' => array
															(
																'Can not be empty',
																'Invalid email address',
															)),
									'email_supplychain' => array('NotEmpty',
															'messages' => array
															('Can not be empty',)),
															);
			}

				$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				$arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');

				$cek = $this->_db->select()
									->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID'))
									->WHERE ('MODULE_ID = ?','GNS')
									->query()->FetchAll();
				if($cek == null)
				{
					if($this->_request->isPost() && $this->view->hasPrivilege('STUD'))
					{
						//validasi global admin fee
						if($admin_fee_company > 0 && $admin_fee_account > 0){
							//echo'ga bisa masuk';
							$result_val = false;
							$this->view->errorfee = 'Admin fee per account and admin fee per company can not be filled both, please choose one.';
						}
						else{
							//echo'bisa masuk';
							$result_val = true;
						}

						if($zf_filter->isValid() && $result_val === true)
						{
							$this->_db->beginTransaction();
							try{

								$info = "GENERAL SETTING";
								$change_id = $this->suggestionWaitingApproval('General Setting',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','','');
								//$change_id = 1;

								foreach($setting as $key=>$value){
										if (in_array($key, $arrfck)) {
											$valuee = $this->_getParam($key);
											$data = array(	'CHANGES_ID' => $change_id,
															'SETTING_ID' => $key,
															'SETTING_VALUE' => ($valuee),
															'MODULE_ID' => 'GNS',
															);
										}
										else
										{
											$data = array(	'CHANGES_ID' => $change_id,
													'SETTING_ID' => $key,
													'SETTING_VALUE' => ($zf_filter->$key),
													'MODULE_ID' => 'GNS',
													);
										}
											$this->_db->insert('TEMP_SETTING',$data);
								}

										Application_Helper_General::writeLog('STUD','Update General Setting');
										$this->_db->commit();
										$this->view->success = true;
										$msg = "Settings Change Request Saved";
										$this->view->report_msg = $msg;
								}
								catch(Exception $e)
								{
										$this->_db->rollBack();
								}
						}
						else{
							$docErr = 'Error in processing form values. Please correct values and re-submit.';
							$this->view->report_msg = $docErr;

							foreach($zf_filter->getMessages() as $key=>$err){
							$xxx = 'x'.$key;
							$this->view->$xxx = $this->displayError($err);
							}
						}
					}
				}else{
					$this->view->error = true;
					//$this->fillParam($zf_filter_input);
					$this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
				}


		if($this->_request->isPost()){

			foreach($setting as $key=>$value)
			{
				if (in_array($key, $arrfck)){
					$this->view->$key =   $this->_getParam($key);
				}
				else{
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
				}
			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		}
		else{
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			Application_Helper_General::writeLog('STLS','View General Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 200 - $disable_note_len;
		$note_len 	 = 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;


	}
}
