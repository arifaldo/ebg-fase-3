<?php
require_once 'Zend/Controller/Action.php';
class billeronboardaccount_SuggestiondetailController extends Application_Main
{
	protected $_moduleDB = 'RCM';

  	public function indexAction()
  	{
		$biller = new billeronboard_Model_Billeronboard();
  		$this->_helper->layout()->setLayout('popup');
  		$params = $this->_request->getParams();

  		if(is_array($params) && count($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;

  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
					'messages' => array(
						'No Suggestion ID',
						'Wrong ID Format',
					),
  				),
  			);

  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
				$tempData = $masterData = null;
	  			$changeId = $zf_filter_input->changes_id;
				$changeInfo = array();
				$changeInfo = $this->getGlobalChanges($changeId);

				if(empty($changeInfo))
				{
					$this->_redirect('/notification/invalid/index');
				}
				else{
					if(!in_array($changeInfo['CHANGES_STATUS'],array('RR','WA')))
					{
						$this->_redirect('/notification/invalid/index');
					}

					$tempData = $biller->getTempacct($changeId);
					$arrStatus = array('1' => 'Approved',
												'2' => 'Suspended',
												'3' => 'Deleted');

					$tempData['ACCT_STATUS']	=	$arrStatus[$tempData['ACCT_STATUS']];

					if(!in_array($changeInfo['CHANGES_TYPE'],array('N','S','U','L')))// NEW,SUSPEND,UNSUSPEND,DELETE
					{
						$masterData = $biller->getBillerAcc(array('ACCT_NO'=>$tempData['ACCT_NO'],'detail'=>true));
						$this->view->master_data = $masterData[0];
					}

					$this->view->changeType = $changeInfo['CHANGES_TYPE'];
					$this->view->suggested_by = $changeInfo['CREATED_BY'];
					$this->view->suggestion_date = $changeInfo['CREATED'];
					$this->view->temp_data = $tempData;
					$this->view->changes_id = $changeId;
				}
	  		}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
	  		}
  		}
  		else
  		{
  			$errorRemark = 'No Suggestion ID';
	  		$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
    	}
    	if(!$this->_request->isPost()){
			Application_Helper_General::writeLog('VBAC','Viewing Suggestion Detail E-Commerce Account List for '.$tempData['PROVIDER_NAME']);
    	}
    }	
}