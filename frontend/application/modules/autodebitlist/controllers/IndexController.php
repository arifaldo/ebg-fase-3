<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class autodebitlist_IndexController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;
 
        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
 
        $select = $this->_db->select()
                             ->from(array('A' => 'T_DIRECTDEBIT'),array('*'))
                            //  ->joinleft(array('B'=>'M_BUSER'), 'A.DIR_APPROVEDBY = B.BUSER_ID',array('B.BGROUP_ID'))
                            //  ->join(array('C'=>'M_BGROUP'), 'C.BGROUP_ID = B.BGROUP_ID',array('C.BGROUP_DESC'))
                             ->where('A.COMP_ID = '.$this->_db->quote((string)$this->_custIdLogin));
                             
        
        $select->query()->fetchAll();
        $this->paging($select);

        $conf = Zend_Registry::get('config');
        $this->view->bankname = $conf['app']['bankname'];

    }
}