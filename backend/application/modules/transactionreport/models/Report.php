<?php


class transactionreport_Model_Report extends Application_Main
{	

	/*
		T_TX_DETAIL.FLAG = menentukan disburse ke berapa. 1 -> disburse pertama, 2 -> disburse keduab
		
		DISB FEE :
			kalo ada 2 disb, tampilkan disb fee cuman di line kedua doang!
			
		INTEREST FEE :
			interest fee
		TRANSFER FEE :
		
	*/
	
	
    // constructor
    public function initModel(){ 
	
    }
	
	public function getDocumentType($docnoid){
		
		$string = NULL;
		$long = '50';
	
		$selectdistinct = $this->_db->select()
									->distinct()
									->from('M_DOC_INI_DOC_CHECKLIST',array('DOC_TYPE'))
									->where('DOC_NO_ID = ? ',$docnoid);
														
		$dataSQL = $this->_db->fetchall($selectdistinct);
		
									
		if(is_array($dataSQL) && count($dataSQL) > 1){
		
			foreach($dataSQL as $keyr => $valr){
				foreach($valr as $keyrr => $valrr){
												
						$string .= $valr[$keyrr].', ';
												
				}
			}
			
			$short_string = (strlen($string) > $long ? substr($string, 0, $long) . "&hellip;" : $string);
			
		}
		else if(is_array($dataSQL) && count($dataSQL) > 0){
		
			foreach($dataSQL as $keyr => $valr){
				foreach($valr as $keyrr => $valrr){
					$string .= $valr[$keyrr].', ';
				}
			}
										
			$short_string = (strlen($string) > $long ? substr($string, 0, $long) . "&hellip;" : $string);
			
		}
		else $short_string = "-";
		
		return $short_string;
		
	}
	
	public function getSumTx($payRef){
		
		$sumamount =  $this->_db->select()
										->from('T_TX_DETAIL',array('sumamount'=>'SUM(TRA_AMOUNT)'))
										->where('PS_NUMBER =?',$payRef);
				
		$sumamount = $this->_db->fetchOne($sumamount);
		
		return $sumamount;
		
	}
	
	public function getLoanIDSettlement($psnumber){
		
		$select = $this->_db->select()
							->from('T_SETTLEMENT',array('loanid'=>'LOAN_ID'))
							->where('PS_NUMBER =?',$psnumber);
		$result = $this->_db->fetchOne($select);
		
		return $result;
	}
	
	public function getSettleAllocation($psnumber,$loanid){
		
		$select = $this->_db->select()
								->from('T_SETTLEMENT_DETAIL',array(
																	'principal'		=>'SETTLEMENT_PRINCIPAL',
																	'intAT'			=>'SETTLEMENT_INT_AT',
																	'intGP'			=>'SETTLEMENT_INT_GP',
																	'intPEN'		=>'SETTLEMENT_INT_PEN',
																	))
								->where("LOAN_ID = ".$this->_db->quote($loanid)." AND PS_NUMBER =? ",$psnumber);
								
		$result = $this->_db->fetchAll($select);
		if(is_array($result) && count($result) > 0){
			
			foreach($result as $key => $val){
				
				$data[0] = $val["principal"];
				$data[1] = $val["intAT"];
				$data[2] = $val["intGP"];
				$data[3] = $val["intPEN"];
			
			}
		
		}
		else $data = array();
		
		return $data;
		
	
	}
	
	public function getTtxFee($feetypelist,$payRef){
				
		$sTtx = $this->_db->select()
						->from(	array('TF'=>'T_TX_FEE'),
								array('chargestatus'=>'TRA_STATUS'))
						->where("TF.PS_NUMBER = ".$this->_db->quote($payRef)." AND TF.TRA_STATUS != 0 AND FEE_TYPE IN (?) ",$feetypelist);
		$rr = $this->_db->fetchOne($sTtx);
		
		// echo $sTtx->__toString();
		// die();
		
		return $rr;
		
	
	}
	
	public function isSecondDisbArr(){
	
	     $select = $this->_db->select()
  	                         ->from(array('T' => 'T_TX_DETAIL'),array('PS_NUMBER','FLAG'))
	  	                     ->where('T.FLAG=?',2)  //2 = pencairan ke-2
	  	                     ->query()->fetchAll();

	  	 if(!empty($select))  $result = Application_Helper_Array::listArray($select,'PS_NUMBER','FLAG'); 
	  	 else $result = array();

	  	 return $result;
	}
	
	public function getChargesAmountLoan($loanid,$membercode,$arrfee,$disbamount){
		
	
		
		/*
			TYPE:
				1. Scheme
				2. Community
				3. Member
			
			FEE TYPE:
				1. Percentage
				2. Fix Amount
			
			FEE_TYPE:
				4. DISB
				5. AT
				6. PEN
				7. GP
			
			FEE_OPTION:
				1 => 4
				2 => 5
				3 => 6
				4 => 7
		*/
		
		// echo "$loanid,$membercode,$disbamount";
		// print_r($arrfee);
		
		$arrfeeoption = array(	
								'4'=>'1',
								'5'=>'2',
								'6'=>'3',
								'7'=>'4',
								);
		
		$select = $this->_db->select()
							->from('T_TX_FEE',array('Famount'	=>'TRA_AMOUNT',
													'Fstatus'	=>'TRA_STATUS',
													'Ffeetype'	=>'FEE_TYPE',
													'Fccy'		=>'SOURCE_ACCOUNT_CCY'
													))
							->where("LOAN_ID = '$loanid' AND FEE_TYPE IN (?) ",$arrfee);
							
		$rr = $this->_db->fetchAll($select);
		
		if(!empty($rr)){
			foreach($rr as $key => $value){
			
				$Ffeetype 	= $value["Ffeetype"];
				$Fstatus 	= $value["Fstatus"];
				$Famount 	= $value["Famount"];
				$Fccy 	= $value["Fccy"];
				
				if($Fstatus == '3') {
					$data[$Ffeetype]['CCY'] 	= $Fccy;
					$data[$Ffeetype]['AMOUNT'] 	= $Famount;
				}
				else{
				
					if($Ffeetype == '4'){
						$data[$Ffeetype]['CCY'] 	= $Fccy;
						$data[$Ffeetype]['AMOUNT'] 	= $Famount;
					}
					else{
						
						//------------------- START ELSE ---------------------------
						
						foreach($arrfeeoption as $key => $value){
							if($key == $Ffeetype) $feeopt = $value;
						
						}
						
						$select = $this->_db->select()
											->from(	array('CH'=>'M_CHARGES_DETAIL'),
													array(	'feetype'	=>'FEE_TYPE',
															'feeccy'	=>'FEE_CCY',
															'feerate'	=>'FEE_RATE',
															'feeamt'	=>'FEE_AMT',
															'feemin'	=>'FEE_MIN',
															'feemax'	=>'FEE_MAX',
															'feeoption'	=>'FEE_OPTION'
															))
											->where("CODE = '$membercode' AND TYPE = '3' AND FEE_OPTION =? ",$feeopt);
						$r = $this->_db->fetchRow($select);
						
						// echo "<pre>";
						// print_r($result);
						// die;
						
						// echo "<pre>";
						// echo $select->__toString();
						// die;
						
						if(!empty($r)){
							//foreach($result as $key => $r){
								
								$feetype 	= $r["feetype"];
								$feeccy 	= $r["feeccy"];
								$feeamount 	= $r["feeamt"];
								
								$feerate 	= $r["feerate"];
								$feemin 	= $r["feemin"];
								$feemax 	= $r["feemax"];
								
								$feeoption	= $r["feeoption"];
								
								if($feetype == 1){
									
									if(!empty($disbamount)){
									
										$fee = ($disbamount * $feerate)/100;
										if($feeccy == 'IDR') 	$rr = floor($fee);
										else 					$rr = $this->formatChargeUsd($fee);
										
										if		($rr <= $feemin) 				 $amt = $feemin;
										else if	($rr >= $feemax )				 $amt = $feemax;
										else if	($rr > $feemin && $rr < $feemax) $amt = $rr;
										
										//echo "fee: $fee, rr: $rr, amt: $amt<br />";
																
										$data[$Ffeetype]['CCY'] 		= $feeccy;
										$data[$Ffeetype]['AMOUNT'] 	= $amt;
										
									}
									else {
										$amounttext = '0.00';
										
										$data[$Ffeetype]['CCY'] 		= $feeccy;
										$data[$Ffeetype]['AMOUNT'] 	= $amounttext;
									}
								
								}
								else {
								
									$data[$Ffeetype]['CCY'] 		= $feeccy;
									$data[$Ffeetype]['AMOUNT'] 		= $feeamount;
									
								}
								
							//}
			
						}	
						
						
						//------------------- END OF ELSE---------------------------
						
						
						
					
					}
				
				}
			
			}
			
		}
		
		// echo "<pre>";
		// print_r($data);
		// die;
		
		return $data;
	
	}
	
	public function getChargesAccountSettlement($disbref,$payref=null){
		
		/*
		charges account: 
			
			t_accounts ambil psnumber, join ttx, dapet membercode, join M_MEMBER ambil rolenya! buyer or seller,
			
			T = account ownernya ambil punyanya si buyer!
			buyernya itu principal ato member!
			baru masukkin account owner P/M.
			
			select from t_accounts where account_owner = T, psnumber = psnumberdisb
			
		*/
		
		// $select = $this->_db->select()
							// ->from(		array('T'=>'T_TX'),array())
							// ->joinLeft(	array('M'=>'M_MEMBER'),'T.MEMBER_CODE = M.MEMBER_CODE',
										// array('memberrole'=>'MEMBER_ROLE'))
							// ->where('T.PS_NUMBER =? ',$this->_payRef);
		// $MEMBERROLE = $this->_db->fetchOne($select);
		/*
			memberrole: B - Buyer; S - Seller
		*/
		
		// if(Zend_Auth::getInstance()->hasIdentity()){
		
			// $auth 				= Zend_Auth::getInstance()->getIdentity();
			// $LIST 				= $auth->listCommunityByCustLogin;
			// $INCOMMUNITY_ROLE 	= $LIST[$this->_communityCode]["INCOMMUNITY_ROLE"]; // P / M
		// }
		
		// if($MEMBERROLE == 'B') $ACCOUNT_OWNER = $INCOMMUNITY_ROLE;
		// else{
			
			// if($INCOMMUNITY_ROLE == 'P') 	$ACCOUNT_OWNER = 'M';
			// else 							$ACCOUNT_OWNER = 'P';
		// }
		$ACCOUNT_OWNER = $this->_db->fetchOne("SELECT TX_MAKER FROM T_TX WHERE PS_NUMBER='".$payref."'");
		
		$selectacct = $this->_db->select()
								->from('T_ACCOUNTS',array(	'acctsource'=>'ACCT_NO',
															'acctccy'	=>'ACCT_CCY',
															'acctname'	=>'ACCT_NAME',
															'acctalias'	=>'ACCT_ALIAS',														
														))
														
								->where("ACCT_OWNER = '$ACCOUNT_OWNER' AND ACCT_GROUP = 'C' AND ACCT_SOURCE = '2' AND PS_NUMBER =?",$disbref);
		
		$selectacct = $this->_db->fetchRow($selectacct);
				
		if($selectacct)	$chargesaccount = $this->getAcctDescByAccount($selectacct["acctsource"]);
		else			$chargesaccount = "-";
		
		return $chargesaccount;
	
	}
	
	public function formatChargeUsd($number){
	 
		 $number_explode = explode('.',$number);
		 $desimal = '';
		  
		 if(isset($number_explode[1])){ 
		 
			$desimal = substr($number_explode[1], 0, 2);
			$result  = $number_explode[0].'.'.$desimal;
		 }
		 else $result = $number;
		  
		 return $result;
	}
	
	public function getChargesAmount($membercode,$feetype,$opt,$disbAmt){
		
		
		//return "chargesamount";
		
		/*
			TYPE:
				1. Scheme
				2. Community
				3. Member
			
			FEE TYPE:
				1. Percentage
				2. Fix Amount
			
			FEE_TYPE:
				4. DISB
				5. AT
				6. PEN
				7. GP
			
			FEE_OPTION:
				1 => 4
				2 => 5
				3 => 6
				4 => 7
		*/
		
		$arrfeeoption = array(	
								'4'=>'1',
								'5'=>'2',
								'6'=>'3',
								'7'=>'4',
								);
		foreach($arrfeeoption as $key => $value){
			if($key == $feetype) $feeopt = $value;
		
		}
		
		$select = $this->_db->select()
							->from(	array('CH'=>'M_CHARGES_DETAIL'),
									array(	'feetype'	=>'FEE_TYPE',
											'feeccy'	=>'FEE_CCY',
											'feerate'	=>'FEE_RATE',
											'feeamt'	=>'FEE_AMT',
											'feemin'	=>'FEE_MIN',
											'feemax'	=>'FEE_MAX',
											))
							->where("CODE = '$membercode' AND TYPE = '3' AND FEE_OPTION = '$feeopt'");
		$r = $this->_db->fetchRow($select);
		
		// echo "<pre>";
		// echo $select->__toString();
		// die;
		
		if(!empty($r)){
			$feetype = $r["feetype"];
			
			if($feetype == 1){
				
				$feeccy 	= $r["feeccy"];
				$feeamount 	= $r["feeamt"];
				$feerate 	= $r["feerate"];
				$feemin 	= $r["feemin"];
				$feemax 	= $r["feemax"];
				
				//return "test: $feeamount";
				//$disbAmt = 10.00;
				
				//return $feeamount;
				
				if(!empty($disbAmt)){
					
					//return $feerate;
					
					$fee = ($disbAmt * $feerate)/100;
					if($feeccy == 'IDR') 	$rr = floor($fee);
					else 					$rr = $this->formatChargeUsd($fee);
					
					if		($rr <= $feemin) 				 $amt = $feemin;
					else if	($rr >= $feemax )				 $amt = $feemax;
					else if	($rr > $feemin && $rr < $feemax) $amt = $rr;
					
					//echo "fee: $fee, rr: $rr, amt: $amt<br />";
					$amounttext = $amt;
					$amount 	= $feeccy.' '.Application_Helper_General::displayMoney($amt);
				}
				else {
					$amounttext = '0.00';
					$amount		= $feeccy.' '.Application_Helper_General::displayMoney($amounttext);
				}
				
			}
			else{
				
				
				$feeamount 	= $r["feeamt"];
				$feeccy		= $r["feeccy"];
				
				$amounttext = $feeamount;
				$amount 	= $feeccy.' '.Application_Helper_General::displayMoney($feeamount);
			}
		}
		else{
			$amount 	= '-';
			$amounttext = '0.00';
		}
		
		if($opt == 1) 	return $amount;
		else 			return $amounttext;
	}
	
	public function getDisbursementIdSettlement($payRef){
	
		$select = $this->_db->select()
							->from(array('S'=>'T_SETTLEMENT'),array())
							->joinLeft(array('L'=>'T_LOAN'),'S.LOAN_ID = L.LOAN_ID',array())
							->joinLeft(array('T'=>'T_TX'),'L.PS_NUMBER = T.PS_NUMBER',array('psnumberdisb'=>'PS_NUMBER'))
							->where('S.PS_NUMBER =? ',$payRef);
							
		$disbref = $this->_db->fetchOne($select);
		
		return $disbref;
	
	}
	
	public function getLoanCharges($disbAmt,$payRef){
		
		$selectloan = $this->_db->select()
								->from('T_LOAN',array('countloan'=>'count(*)'))
								->where("LOAN_STATUS != '' AND PS_NUMBER =?",$payRef);
								
		$countloan = $this->_db->fetchOne($selectloan);
		
		if($countloan == 1){
			
			// Table Detail Header
			$fieldscharges = array(	"CHARGESTYPE"	=>'Charges Type',
									"MATURITYDATE"	=>'Maturity Date',
									"CHARGEACCOUNT"	=>'Charge Account',
									"CHARGEAMOUNT"	=>'Charge Amount',
									"PAYMENTDATE"	=>'Payment Date',
									"STATUS"		=>'Status',
								);
			
		}
		else{
			$fieldscharges = array(	"INSTALLMENTNO"	=>'Installment No',
									"CHARGESTYPE"	=>'Charges Type',
									"MATURITYDATE"	=>'Maturity Date',
									"CHARGEACCOUNT"	=>'Charge Account',
									"CHARGEAMOUNT"	=>'Charge Amount',
									"PAYMENTDATE"	=>'Payment Date',
									"STATUS"		=>'Status',
								);
		
		}
		// echo "<pre>";
		// print_r ($fieldscharges);
		// echo $selectloan->__toString();
		
		$arrTraStatus 	= array_combine($this->_transferstatus['code'],$this->_transferstatus['desc']);
					
		$caseTraStatus = "(CASE F.TRA_STATUS ";
		foreach($arrTraStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
		$caseTraStatus .= " END)";
		
		$select = $this->_db->select()
							->from(		array('F'=>'T_TX_FEE'),array())
							->joinLeft(	array('L'=>'T_LOAN'),'F.LOAN_ID = L.LOAN_ID',
										array(
												'INSTALLMENTNO'	=>'L.INSTALLMENT_OF',
												'CHARGESTYPE'	=>"(CASE F.FEE_TYPE WHEN '4' THEN 'Early Charges' WHEN '5' THEN 'Additional Tenor Charges' WHEN '6' THEN 'Grace Period Charges' WHEN '7' THEN 'Penalty Period Charges' END )",
												
												'MATURITYDATE'	=>'F.TRA_DATE',
												'CHARGEACCOUNT'	=>'F.SOURCE_ACCOUNT',
												'CCY'			=>'F.SOURCE_ACCOUNT_CCY',
												'CHARGEAMOUNT'	=>'F.TRA_AMOUNT',
												'PAYMENTDATE'	=>'F.PAYMENT_DATE',
												
												'STATUS'		=>$caseTraStatus,
												'TRASTATUSCODE'	=>'F.TRA_STATUS',
												'FEETYPE'		=>'F.FEE_TYPE',
												
												'MEMBERCODE'		=>'L.MEMBER_CODE',
												'DOCMATURITYDATE'	=>'L.DOC_MATURITY_DATE',
												'LOANSTARTDATE'		=>'L.LOAN_START_DATE',
												'ATMATURITYDATE'	=>'L.ADD_TENOR_MATURITY_DATE',
												'GPMATURITYDATE'	=>'L.GRACE_PERIOD_MATURITY_DATE',
												
												
											))
							->where("F.TRA_STATUS != '0' AND F.PS_NUMBER =?",$payRef);
		$result = $this->_db->fetchAll($select);
		
		// echo "<pre>";
		// print_r($result);
		// echo $select->__toString();
		// die;
		
		$data = array();
		
		$data = array();
		if($countloan == 1) $start = 'DOCMATURITYDATE';
		else				$start = 'LOANSTARTDATE';
		

		$today = strtotime(date("Y-m-d")); 
		
		if(!empty($result)){
			foreach($result as $key => $value){
					
					/*
						feetype: 
							4. disb,
							5. AT
							6. GP
							7. PEN
					*/
					
				$feetype 		= $value["FEETYPE"];
					
				$loanstartdate 	= strtotime($value[$start]);
				$ATmaturitydate = strtotime($value["ATMATURITYDATE"]);
				$GPmaturitydate = strtotime($value["GPMATURITYDATE"]);
									
				foreach($value as $d => $dt){
						
					if($feetype == 4){
						
						$row = $value[$d];
						$data[$key][$d] = $row;
							
					}
							
							
					else if($today >= $loanstartdate){
							
						//if($loanstartdate >= $today && $ATmaturitydate < $today){
						//if($today > $loanstartdate &&  $today <= $ATmaturitydate){
						if($today >= $loanstartdate &&  $today <= $ATmaturitydate){
								
							if($feetype != 6 && $feetype != 7){
									
								$row = $value[$d];
								$data[$key][$d] = $row;
							}
							
						}
						//else if($ATmaturitydate >= $today && $GPmaturitydate < $today){
						//else if($today > $ATmaturitydate && $today <= $GPmaturitydate){
						else if($today >= $ATmaturitydate && $today <= $GPmaturitydate){
								
							if($feetype != 7){
									
								$row = $value[$d];
								$data[$key][$d] = $row;
							}
								
						}							
							//else if($today >= $GPmaturitydate){
						//else if($today > $GPmaturitydate){
						else if($today >= $GPmaturitydate){
								
							$row = $value[$d];
							$data[$key][$d] = $row;
								
						}
							
					}
						
				}
				
			}
				
		} else $data = array();
		
		
		
		
		$totalcharges = 0;
		/*
			 0 = Closed
			 1 = Untransfered
			 3 = Success
			 4 = Failed
			 9 = In Progress
			13 = Exception
		
		*/
		// echo "<pre>";
		// print_r ($result);
		
		$totalLoan = 0;
		if(is_array($data) && count($data) > 0){
		
			foreach ($data as $p => $pChargeFee)
			{			
				//create table detail data
				foreach ($fieldscharges as $key => $field)
				{
					$value 			= $pChargeFee[$key];
					$installmentno	= $pChargeFee["INSTALLMENTNO"];
					$ccy			= $pChargeFee["CCY"];
					$statuscharges	= $pChargeFee["TRASTATUSCODE"];
					$membercode		= $pChargeFee["MEMBERCODE"];
					
					if($key == "INSTALLMENTNO"){ $value = 'Installment '.$installmentno; }
					else if($key == "CHARGEAMOUNT"){
						
						if($statuscharges == '3'){
							$value 		= $ccy.' '.Application_Helper_General::displayMoney($value);
							$amount 	= $pChargeFee["CHARGEAMOUNT"];
							
							//echo "$amount - $value";
						}
						else{
						
							$membercode = $pChargeFee["MEMBERCODE"];
							$feetype  	= $pChargeFee["FEETYPE"];
							
							$value 	= $this->getChargesAmount($membercode,$feetype,1,$disbAmt);
							$amount = $this->getChargesAmount($membercode,$feetype,2,$disbAmt);
							
							
						}					
						
						$totalLoan	= $totalLoan + $amount;
						
					}
					else if($key == "CHARGEACCOUNT")$value = $this->getAcctDescByAccount($value);
					else if($key == "MATURITYDATE") $value = Application_Helper_General::convertDate($value,$this->_dateViewFormat);
					else if($key == "PAYMENTDATE") 	$value = Application_Helper_General::convertDate($value,$this->_dateViewFormat);
						
					$value = ($value == "")? "&nbsp;": $value;
					
					$tableDtl[$p][$key] = $value;	
				}
				
			}
		}else $tableDtl = array();
		
		//echo "totalloan: ".$totalLoan;
		
		
		$this->view->fieldscharges 		= $fieldscharges;
		$this->view->tableDtlCharges 	= $tableDtl;
		$this->view->totalLoan			= $totalLoan;
		$this->view->ccyLoan 			= $ccy;
		
	
	}
	
	public function getAllCurrency(){
	
		$selectcur = $this->_db->select()
								->from(array('A' => 'M_MINAMT_CCY'),array('CCY_ID'))
								->order('CCY_ID ASC')
								-> query() ->fetchAll();
						  
		if(is_array($selectcur) && count($selectcur) > 0){
			foreach($selectcur as $key => $val){
			
				$a = $selectcur[$key]["CCY_ID"];
				$result[$a] = $a;
			}		
		}
		else $result = array();
		
		return $result;
		
	}
	
	public function getAllCustomer(){ // custid - custid
		
      $select = $this->_db->select()
      	                   ->from('M_CUSTOMER',array('custid'=>'CUST_ID','custname'=>'CUST_NAME'))
						   ->order('CUST_ID ASC')
      	                   ->query()->fetchAll();
		
		if(is_array($select) && count($select) >0){
			foreach($select as $key => $val){
				
				$b = $select[$key]["custname"];
				$c = $select[$key]["custid"];
				$result[$c] = $c;
				
			}
		}
		else $result = array();
		
		return $result;
	  
    }
	
	public function getAllCommunity(){ // communitycode - communitycode
		
		$select = $this->_db->select()
      	                   ->from(array('A'=>'M_COMMUNITY'),array(	'commcode'=>'COMMUNITY_CODE',
																	'commname'=>'COMMUNITY_NAME'))
      	                   ->query()->fetchAll();
		
		if(is_array($select) && count($select) > 0){
			foreach($select as $key => $val){
				
				$a = $select[$key]["commcode"];
				$b = $select[$key]["commname"];
				$result[$a] = $a;
				
			}
		}
		else $result = array();
		
		return $result;
    }
	
	public function getAllMember(){ // membercode - membercode
		
       $select = $this->_db->select()
      	                   ->from(array('M'=>'M_MEMBER'),array(	'membercode'	=>'MEMBER_CODE',
																'membercompany'	=>'MEMBER_COMPANY'))
      	                   ->query()->fetchAll();
		
		if(is_array($select) && count($select) > 0){
			foreach($select as $key => $val){
				
				$a = $select[$key]["membercode"];
				$result[$a] = $a;
				
			}
		}
		else $result = array();
		
		return $result;
	  
    }
	
	public function getAllRootCommunity(){ 
		
		$arrRR = $this->_db->select()
      	                   ->from('M_ROOT_COMM',array('rootcode'=>'ROOT_CODE','rootname'=>'ROOT_NAME'))
      	                   ->query()->fetchAll();
						   
		if(is_array($arrRR) && count($arrRR) > 0){
			foreach($arrRR as $key => $val){
				
				$a = $arrRR[$key]["rootcode"];
				$b = $arrRR[$key]["rootname"];
				$arrRoot[$a] = $a;
				
			}
		}
		else $arrRoot = array();
		return $arrRoot;
	  
    }
	
	public function getAllPrinciple(){ 
		
       $select = $this->_db->select()
      	                   ->from(array('A'=>'M_ANCHOR'),array(	'anchorid'=>'ANCHOR_ID',
																'anchorcust'=>'ANCHOR_CUST'))
      	                   ->joinLeft(array('CS'=>'M_CUSTOMER'),'CS.CUST_ID = A.ANCHOR_CUST',array('custname'=>'CUST_NAME'))
      	                   ->query()->fetchAll();
						   
		if(is_array($select) && count($select) > 0){
		
			foreach($select as $key => $val){
				
				$a = $select[$key]["anchorcust"];
				$b = $select[$key]["custname"];
				$result[$a] = $a;
				
			}
		}
		else $result = array();
		
		return $result;
	  
    }
	
	public function getAccountRaw($psnumber){ // get raw result account transaction
		
		$select = $this->_db->select()
						->distinct()
						->from('T_TX_DETAIL',
								array(	'acctsource'=>'SOURCE_ACCOUNT',
										'acctccy'	=>'SOURCE_ACCOUNT_CCY',
										'acctname'	=>'SOURCE_ACCOUNT_NAME',
										'acctalias'	=>'SOURCE_ACCOUNT_ALIAS_NAME',
										'accttype'	=> new Zend_Db_Expr("	CASE WHEN FLAG <= '2' THEN 'PRK' 
																			ELSE 'GIRO' END"),
										'benesource'=>'BENEFICIARY_ACCOUNT',
										'beneccy'	=>'BENEFICIARY_ACCOUNT_CCY',
										'benename'	=>'BENEFICIARY_ACCOUNT_NAME',
										'benealias'	=>'BENEFICIARY_ALIAS_NAME',
										
								))
						->where('DISPLAY_FLAG = 1 AND PS_NUMBER =?',$psnumber);
		$select = $this->_db->fetchRow($select);
		
		return $select;
	}
	
	public function getBeneficiaryName($psnumber){ // get full name Beneficiary Name
	
		$result = $this->getAccountRaw($psnumber);
		
		if($result){
			$beneacct 	= $result["benesource"] ? $result["benesource"] :'';
			$beneccy  	= ' ['.$result["beneccy"].'] ' ? ' ['.$result["beneccy"].'] ':'';
			$benename 	= $result["benename"] ? $result["benename"]:'';
			$benealias 	= ' / '.$result["benealias"] ? ' / '.$result["benealias"]:'';
			
			$beneaccount 	= $beneacct.$beneccy.' - '.$benename.$benealias;
		}
		else $beneaccount = '-';
		
		return $beneaccount;
		
	}
		
	public function getChargesAccountDisb($disbref){
		
		$select = $this->_db->select()
							->from(array('T'=>'T_TX'),array(
														'disbtype'	=>'DISBURSEMENT_TYPE',
														'schemecode'=>'BUSS_SCHEME_CODE'
													))
							->where('PS_NUMBER =? ',$disbref);
		$transaction = $this->_db->fetchRow($select);
		
		$acct_owner = 'M';
		
        if($transaction['disbtype'] == 1)  //1 = early disb
        {
			if($transaction['schemecode'] == 2 || $transaction['schemecode'] == 3)
            {
                $acct_owner = 'P';
            }
         }
		
		$selectacct = $this->_db->select()
								->from('T_ACCOUNTS',array(	'acctsource'=>'ACCT_NO',
															'acctccy'	=>'ACCT_CCY',
															'acctname'	=>'ACCT_NAME',
															'acctalias'	=>'ACCT_ALIAS',														
														))
														
								->where("ACCT_OWNER = ".$this->_db->quote($acct_owner)." AND ACCT_GROUP = 'C' AND ACCT_SOURCE = '2' AND PS_NUMBER =?",$disbref);
		
		$selectacct = $this->_db->fetchRow($selectacct);
				
		if($selectacct)	{ $chargesaccount = $this->getAcctDescByAccount($selectacct["acctsource"]); }
		else			{ $chargesaccount = "-"; }
		
		return $chargesaccount;
	
	}
	
	public function getAcctDescByAccount($acctsource){
		
		$tx			= new SCM_Model_Tx($this->_db);
		$acctsrc 	= $tx->getAcctDesc($acctsource);
		
		return $acctsrc;
	
	}
	
	public function getAcctSourceName($psnumber){ // get full name Source Account
		
		/*
			Account Desc = Account No [CCY] - Account Name / Account Alias ( Account Type )
		*/
		
		$result = $this->getAccountRaw($psnumber);
		
		if($result){
			if($result["acctsource"] == '0000000000')	{ $acctsrc = 'OFFLINE';}
			else										{ $acctsrc 	= $this->getAcctDescByAccount($result["acctsource"]);}
		}
		
		else $acctsrc = '-';
		
		return $acctsrc;
	
	}
	
	public function getLastStatusTransaction($psnumber){
		
		/*
			FLAG:
				1. DISB KE-1
				2. DISB KE-2
				3. GIRO
				4. PAYMENT ALOCATION
				
		
		*/
		
		$select = $this->_db->select()
							->from('T_TX_DETAIL',array('trastatus'=>'TRA_STATUS'))
							->where("(FLAG != 3 AND FLAG != 4) AND PS_NUMBER =?",$psnumber)
							->order('FLAG DESC');
		$lasttrastatus = $this->_db->fetchOne($select);
		
		// echo $select->__toString();
		// die;
		
		return $lasttrastatus;
	}
	
	public function getLastDisbFee($psnumber){
		
		/*	if status transaksi 0 disbfee = 0.00 (closed)
			if status transaksi 3 disbfee = select from t_tx_fee
			if status selain 0 / 3 disbfee = select from chris
		*/
		
		$status = $this->getLastStatusTransaction($psnumber);
		
		if(is_array($status) && count($status) > 0){
			if($status == 0)		$disbfee = '0.00';
			else if($status == 3){
				
				$disbfee = $this->_db->select()
									->from(	array('T_TX_FEE'),array('amount'=>'TRA_AMOUNT'))
									->where("PS_NUMBER = '$psnumber' AND FEE_TYPE = '4'");
									
				$disbfee = $this->_db->fetchOne($disbfee);
				
				if($disbfee) 	$disbfee = $disbfee;
				else 			$disbfee = '0.00';
				
			}
			else{
				
				 /*return :
					
					result[1] = fee disbursement
					result[2] = fee additional tenor
					result[3] = fee grace period
					result[4] = fee penalty 
				*/
				
				$releaseDisb	= new SCM_Model_ReleaseDisbursement($this->_db);
				
				$select = $releaseDisb->getTx();
				$select->where('t.PS_NUMBER = ?' , (string) $this->_payRef);
				$param 	= $this->_db->fetchRow($select);
				
				$SCMFee = $releaseDisb->insertTxFee($param,false);
				$disbfee = $SCMFee[1];
				if(!$disbfee){ $disbfee = '0.00';}
			}
		}
		else $disbfee = '0.00';
		//echo "<pre>";
		//echo $select->__toString();
		//echo "feescm: ";
		//print_r($SCMFee);
		//die;	 
		
		return $disbfee;
		
		
	}
	
	public function getTransferFeeSettlement($tra_id)
	{
		
		$select	= $this->_db->select()
							->from('T_TX_DETAIL',array(	'trastatus'=>'TRA_STATUS',
														'acctsource'=>'SOURCE_ACCOUNT',
														'PS_NUMBER'=>'PS_NUMBER'))
							->where('TRANSACTION_ID =?',$tra_id);
		$result = $this->_db->fetchRow($select);
		$trastatus  = $result["trastatus"];
		$acctsource = $result["acctsource"];
		$PS_NUMBER = $result["PS_NUMBER"];
		$select	= $this->_db->select()
							->from('T_TX',array('TX_STATUS'=>'TX_STATUS'))
							->where('PS_NUMBER =?',$result["PS_NUMBER"]);
		$result = $this->_db->fetchRow($select);
		
		$txStatus = $result["TX_STATUS"];
		if($txStatus == '14' ||$txStatus == '4')
		{
			$trafeeamount = ' - ';
		}
		else
		{
			if($trastatus == 3){ // trastatus = 3 - success
			
				$select = $this->_db->select()
									->from(		array('CH'=>'T_CHARGES_LOG'),array('amount'=>'AMOUNT'))
									->joinLeft(	array('TX'=>'T_TX_DETAIL'),'CH.TRANSACTION_ID = TX.TRANSACTION_ID',array())
									->where("	CH.TRANSACTION_ID 	= '$tra_id' 
												
												AND CH.PAYMENT_TYPE = '9'
												AND CH.CHARGES_TYPE = TX.TRANSFER_TYPE
												"); // 1: success; 0 : failed AND CH.STATUS = '1'
				
				$trafeeamount = $this->_db->fetchOne($select);
				
				if($trafeeamount != "") $trafeeamount = $trafeeamount;
				else $trafeeamount = "0.00";
			}
			else { // select from M_CHARGES_WITHIN where bussines_type = 2 & cust_id = $this->cust_id; 2: disbursement, 3: settlement;
				
				$select = $this->_db->select()
									->from(array('CHW'=>'M_CHARGES_WITHIN'),array('amount'=>'AMOUNT'))
									->where("BUSINESS_TYPE = '3' AND ACCT_NO = '$acctsource'");
				
				$trafee = $this->_db->fetchOne($select);
				if(!$trafee) 	$trafeeamount = "0.00";
				else			$trafeeamount = $trafee;
				
			}
			$trafeeamount = Application_Helper_General::displayMoney($trafeeamount);
			//return "$acctsource,$this->_customerid";
		}
		return $trafeeamount;
	
	}
	
	public function getFlagTrx($tra_id){
		
		$select = $this->_db->select()
							->from('T_TX_DETAIL',array('flag'=>'FLAG'))
							->where('TRANSACTION_ID =?',$tra_id);
							
		$flagTrx = $this->_db->fetchOne($select);
		
		return $flagTrx;
		
	}
	
	public function getOtherTransaction($psnumber,$tra_id){
		
		$select = $this->_db->select()
							->from('T_TX_DETAIL',array('traid'=>'TRANSACTION_ID'))
							->where("PS_NUMBER = '$psnumber' AND TRANSACTION_ID != '$tra_id'");
		$xTraId = $this->_db->fetchOne($select);
		
		return $xTraId;
		
	}
	
	public function getPSNumberTransaction($tra_id){
	
		$select = $this->_db->select()
							->from('T_TX_DETAIL',array('psnumber'=>'PS_NUMBER'))
							->where('TRANSACTION_ID =?',$tra_id);
							
		$psnumber = $this->_db->fetchOne($select);
		
		return $psnumber;
	
	}
	
	public function getTxFee($tra_id){
		
		$select	= $this->_db->select()
							->from('T_TX_DETAIL',array(	'trastatus'		=>'TRA_STATUS',
														'acctsource'	=>'SOURCE_ACCOUNT',
														'PS_NUMBER'		=>'PS_NUMBER',
														'TRANSFER_TYPE' =>'TRANSFER_TYPE'))
							->where('TRANSACTION_ID =?',$tra_id);
		$result = $this->_db->fetchRow($select);
		
		$trastatus  	= $result["trastatus"];
		$acctsource 	= $result["acctsource"];
		$TRANSFER_TYPE 	= $result["TRANSFER_TYPE"];
		
		$select	= $this->_db->select()
							->from('T_TX',array('TX_STATUS'		=>'TX_STATUS',
												'MEMBER_CUST_ID'=>'MEMBER_CUST_ID'))
							->where('PS_NUMBER =?',$result["PS_NUMBER"]);
		$result = $this->_db->fetchRow($select);
		
		$txStatus = $result["TX_STATUS"];
		//Zend_Debug::dump($txStatus);
		if($txStatus == '14' ||$txStatus == '4')
		{
			$trafeeamount = ' - ';
		}
		else
		{
			if($trastatus == 3)
			{ // trastatus = 3 - success
			
				$select = $this->_db->select()
									->from(		array('CH'=>'T_CHARGES_LOG'),array('amount'=>'AMOUNT'))
									->joinLeft(	array('TX'=>'T_TX_DETAIL'),'CH.TRANSACTION_ID = TX.TRANSACTION_ID',array())
									->where("	CH.TRANSACTION_ID 	= '$tra_id' 
												
												AND CH.PAYMENT_TYPE = '8'
												AND CH.CHARGES_TYPE = TX.TRANSFER_TYPE
												"); // 1: success; 0 : failed AND CH.STATUS = '1'
				
				$trafeeamount = $this->_db->fetchOne($select);
				
				if($trafeeamount != "") $trafeeamount = $trafeeamount;
				else $trafeeamount = "0.00";
			}
			else 
			{ // select from M_CHARGES_WITHIN where bussines_type = 2 & cust_id = $this->cust_id; 2: disbursement, 3: settlement;
				/*cek transaksi domestik ato bukan
					cek di ttx_detail. tra_type = 1/2 dari m_charges_other where CUST_ID = $result["MEMBER_CUST_ID"] and CHARGES_TYPE = $TRANSFER_TYPE get CHARGES_AMT
					if tra_type == 0 dari M_CHARGES_WITHIN
				*/
				
				if($TRANSFER_TYPE == 0)
				{
					$select = $this->_db->select()
										->from(array('CHW'=>'M_CHARGES_WITHIN'),array('amount'=>'AMOUNT'))
										->where("BUSINESS_TYPE = '2' AND ACCT_NO = '$acctsource'");
					
					$trafee = $this->_db->fetchOne($select);
					if(!$trafee) 	$trafeeamount = "0.00";
					else			$trafeeamount = $trafee;
				}
				else
				{
					$select = $this->_db->select()
										->from(array('MCO'=>'M_CHARGES_OTHER'),array('amount'=>'CHARGES_AMT'))
										->where("CUST_ID = ".$this->_db->quote($result["MEMBER_CUST_ID"])." AND CHARGES_TYPE = '$TRANSFER_TYPE'");
					$trafee = $this->_db->fetchOne($select);	
					$trafeeamount = $trafee;					
				}
				
			}
			$trafeeamount = Application_Helper_General::displayMoney($trafeeamount);
		}
		//return "$acctsource,$this->_customerid";
		
		return $trafeeamount;
	
	}
	
	public function getTraDateTransaction($tra_id){
		
		$select = $this->_db->select()
							->from('T_TX_DETAIL',array('tradate'=>'TRA_DATE'))
							->where('TRANSACTION_ID =?',$tra_id);
							
		$tradate = $this->_db->fetchOne($select);
		
		return $tradate;
	
	}
	
	public function checkTransactionSameDate($xTraId,$tradate){
		
		$select = $this->_db->select()
							->from(array('T'=>'T_TX_DETAIL'),array('tradate'=>'TRA_DATE'))
							->where("T.TRANSACTION_ID = '$xTraId' AND convert(date, T.TRA_DATE) = convert(date, ? )",$tradate);
		
		$result = $this->_db->fetchOne($select);
		return $result;
	
	}
	
	public function getTransferFee($tra_id){
		
		$flagTtx = $this->getFlagTrx($tra_id);
		
		//return "$flagTtx,$tra_id";
		
		if($flagTtx == 1)			$amount = $this->getTxFee($tra_id); 		// PENCAIRAN PERTAMA
		else if($flagTtx == 2){
			
			$psnumber 	= $this->getPSNumberTransaction($tra_id);
			$xTraId 	= $this->getOtherTransaction($psnumber,$tra_id);
			$xflag 		= $this->getFlagTrx($xTraId);
			
			if($xflag == 1){
				$tradate = $this->getTraDateTransaction($tra_id);
				
				if(!empty($tradate)){
				
					$isDisbSamedate = $this->checkTransactionSameDate($xTraId,$tradate);
					
					if($isDisbSamedate)		$amount = '0.00';
					else					$amount = $this->getTxFee($tra_id);
				}
			}
			else if($xflag == 3)	$amount = $this->getTxFee($tra_id); // other disbursement use GIRO account
			else					$amount = $this->getTxFee($tra_id); // There's just one transaction ref#
		
		}
		else if($flagTtx == 3) 	$amount = $this->getTxFee($tra_id); // GIRO
		
		return $amount;

	}

	public function getTransferFeePayAlocation($tra_id){
	
		$select = $this->_db->select()
							->from(		array('CH'=>'T_CHARGES_LOG'),array('amount'=>'AMOUNT'))
							->joinLeft(	array('TX'=>'T_TX_DETAIL'),'CH.TRANSACTION_ID = TX.TRANSACTION_ID',array())
							->where("	CH.TRANSACTION_ID 	= '$tra_id' 
											
										AND CH.PAYMENT_TYPE = '8'
										AND CH.CHARGES_TYPE = TX.TRANSFER_TYPE
									"); // 1: success; 0 : failed AND CH.STATUS = '1'
			
		$trafeeamount = $this->_db->fetchOne($select);
			
		if($trafeeamount != "") $trafeeamount = $trafeeamount;
		else $trafeeamount = "0.00";
		
		return $trafeeamount;
	}
}
?>