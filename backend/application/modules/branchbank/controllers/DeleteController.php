<?php

require_once 'Zend/Controller/Action.php';

class Branchbank_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');  
	
	    $filters = array('id' => array('StringTrim', 'StripTags'),
						'branch_code' => array('StringTrim','StripTags'),
						);
							 
		$validators =  array(
						'id'      => array('NotEmpty',
                                    			        'messages' => array(
                                    			            $this->language->_('Can not be empty')
                                    			        )
                                    			    ),
							'branch_code'      => array('NotEmpty',
                                    			        'messages' => array(
                                    			            $this->language->_('Can not be empty')
                                    			        )
                                    			    ),
					        );
			
		if(array_key_exists('id',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
				    $this->_db->beginTransaction();
				    
					$id  = $zf_filter_input->getEscaped('id');
					$branch_code  = $this->_getParam('branch_code');//$zf_filter_input->getEscaped('branch_code');
					
					$branch_name = $this->_db->fetchAll($this->_db->select()
					        ->from(array('A' => 'M_BRANCH'),array('A.BRANCH_NAME'))
							->where("A.BRANCH_CODE IN (?)",$branch_code));
					
					
//					$this->_db->delete('M_DOMESTIC_BANK_TABLE','BANK_ID = ?', $bankId);
					$this->_db->delete('M_BRANCH','ID = '.$this->_db->quote($id));
					
					
					$branch_name_coverageArr = $this->_db->fetchAll($this->_db->select()
					        ->from(array('A' => 'M_BRANCH_COVERAGE'),array('A.BRANCH_CODE'))
							->where("A.BRANCH_CODE IN (?)",$branch_code));
							
					
					foreach($branch_name_coverageArr as $row){
						
							if(!empty($row)){
						
									$this->_db->delete('M_BRANCH_COVERAGE','BRANCH_CODE = '.$branch_code);
									
							}
					}
								
					
					
					//die;
							
						
							
							
							
					$this->_db->commit();
					
							
						 
					Application_Helper_General::writeLog('MNBB','Delete Branch Bank. Branch Name : ['.$branch_name[0]['BRANCH_NAME'].']');
					$this->_redirect('/notification/success/index');
					
					/*$msg_success = 'Record Deleted';
					
        	        $this->_helper->getHelper('FlashMessenger')->addMessage('S');
			        $this->_helper->getHelper('FlashMessenger')
						   ->addMessage(($msg_success !="")? $msg_success : null);
			   
			 
			 
			        $this->backendLog('A', $this->_moduleDB, $bankId  , null, null);
			        */
				    
						
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
					$this->_redirect('/'.$this->_request->getModuleName().'/index');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						Application_Helper_General::writeLog('COUD','Update Branch Bank list');
						
						$this->_redirect('/'.$this->_request->getModuleName().'/index');
						//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
						
					}					
				}	
			} 
			
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('COUD','Update Branch Bank list');
			
			//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
			$this->_redirect('/'.$this->_request->getModuleName().'/index');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
