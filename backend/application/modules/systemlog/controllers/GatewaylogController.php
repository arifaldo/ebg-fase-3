<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

// require_once 'SGO/Extendedmodule/vendor/elasticsearch/elasticsearch/src/Elasticsearch/ClientBuilder.php';
require_once 'SGO/Extendedmodule/vendornew/autoload.php';
// require_once 'SGO/Extendedmodule/Getest/lib/class.geetestlib.php';

class systemlog_GatewaylogController extends Application_Main
{
	
	public function initModel()
	{	
		
		$conf 		= Zend_Registry::get('config');
		$host 		= $conf['db2']['params']['host'];
		$username	= $conf['db2']['params']['username'];
		$pass		= $conf['db2']['params']['password'];
		$db			= $conf['db2']['params']['dbname'];
		$port		= $conf['db2']['params']['port'];
		//print_r($username);die;	
		$key = $conf['enc']['dec']['key'];
		$iv = $conf['enc']['dec']['iv'];
		
		// $usernameval = Application_Helper_General::decrypt($key,$iv,$username);
		// $passval = Application_Helper_General::decrypt($key,$iv,$pass);
	
// 		$config = array(
// 	       'host'      	=> $host,
// 	       'username'  	=> $usernameval,
// 	       'password'  	=> $passval,
// 	       'dbname'    	=> $db,
// 	       'port'  		=> $port
// 	     );
		$config = array(
							       'host'      => '192.168.86.23',
							       'username'  => 'ibsgolive',
							       'password'  => 'F9t%<HCj3L9tPZf?mv)]C#K',
							       'dbname'    => 'sgolivebankdev',
							       'port'  => '3306'
							     );
	     $this->_db = new Zend_Db_Adapter_Pdo_Mysql($config);
 //print_r($this->_db);die;
	}
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		//$model = new systemlog_Model_Systemlog();

		//$activity = $model->getFPrivilege();
		
		$login = array('FPRIVI_ID'=>'FLGN','FPRIVI_DESC'=>'Login');
		$logout = array('FPRIVI_ID'=>'FLGT','FPRIVI_DESC'=>'Logout');
		$changepass = array('FPRIVI_ID'=>'CHMP','FPRIVI_DESC'=>'Change My Password');
		/*array_unshift($activity,$changepass);
		array_unshift($activity,$logout);
		array_unshift($activity,$login);*/
					
		$MYPCHANNEL  	= 'MYPCHANNEL';
		$ECHIDNA 	= 'ECHIDNA';
		$VLINK 		= 'VLINK';
		$MAYAPADA	= 'MAYAPADA';
		$SGO		= 'SGO';
		$IST		= 'IST';
		$ISTv2		= 'ISTv2';
				
		$uuid_lg = $this->language->_('UUID');
		$sequenceid_lg = $this->language->_('Sequence ID');
		$serviceid_lg = $this->language->_('Service ID / Sequence ID');
		$service_messagereq_lg = $this->language->_('Service Message Request');
		$log_reqdate_lg = $this->language->_('Log Request Date');
		$service_messageres_lg = $this->language->_('Service Message Response');
		$log_resdate_lg = $this->language->_('Log Response Date');
		$uuid_lg = $this->language->_('UUID / Error Code');
		$error_code_lg = $this->language->_('Error Code');
		$error_message_lg = $this->language->_('Error Message');
		$error_desc_lg = $this->language->_('Error Description');
		
		$fields = array(						
						// 'SEQUENCE_ID'     			=> array('field' => 'SEQUENCE_ID',
						// 					      		'label' => $sequenceid_lg,
						// 					      		'sortable' => true),
						'SERVICE_ID'     			=> array('field' => 'SERVICE_ID',
											      		'label' => $serviceid_lg,
											      		'sortable' => true),
						'SERVICE_MESSAGE_REQUEST'  	=> array('field' => 'SERVICE_MESSAGE_REQUEST',
											      		'label' => $service_messagereq_lg,
											      		'sortable' => true),
						'LOG_REQUEST_DATETIME'		=> array('field' => 'LOG_REQUEST_DATETIME',
											      		'label' => $log_reqdate_lg,
											      		'sortable' => true),
						'SERVICE_MESSAGE_RESPONSE'   => array	(	
														'field'    => 'SERVICE_MESSAGE_RESPONSE',
														'label'    => $service_messageres_lg,
														'sortable' => true
														),
						'LOG_RESPONSE_DATETIME'   	=> 	array	(	
														'field'    => 'LOG_RESPONSE_DATETIME',
														'label'    => $log_resdate_lg,
														'sortable' => true
													),
						'UUID' 						=> 	array	(	
														'field'    => 'UUID',
														'label'    => $uuid_lg,
														'sortable' => true
													),
						// 'ERROR_CODE' 				=> 	array	(	
						// 								'field'    => 'ERROR_CODE',
						// 								'label'    => $error_code_lg,
						// 								'sortable' => true
						// 							),
						// 'ERROR_MESSAGE' 			=> 	array	(	
						// 								'field'    => 'ERROR_MESSAGE',
						// 								'label'    => $error_message_lg,
						// 								'sortable' => true
						// 							),
						'ERROR_DESCRIPTION' 		=> 	array	(	
														'field'    => 'ERROR_DESCRIPTION',
														'label'    => $error_desc_lg,
														'sortable' => true
													)
				      );

		$filterlist = array('PS_LOG_DATE','SERVICE_ID','SEQUENCE_ID','UUID');
		
		$this->view->filterlist = $filterlist;
				      
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','LOG_REQUEST_DATETIME');
		$sortDir = $this->_getParam('sortdir','desc');
		$filter_clear    = $this->_getParam('filter_clear');
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
					        
		$filterArr = array('filter'				=> array('StripTags','StringTrim'),	                       
	                       'SEQUENCE_ID'		  		=> array('StripTags','StringTrim','StringToUpper'),
	    				   'UUID'    			=> array('StripTags','StringTrim','StringToUpper'),	    				   
	                       'SERVICE_ID' 	=> array('StripTags','StringToUpper'),
						   'PS_LOG_DATE' 			=> array('StripTags','StringTrim'),
						   'PS_LOG_DATE_END' 			=> array('StripTags','StringTrim'),
	                       //'Description'    	=> array('StripTags','StringTrim','StringToUpper'),
	    				   );
	                      
	    $validator = array('filter'			 	=> array(),	                       
	                       'SEQUENCE_ID'  			=> array(),
	    				   'UUID'    			=> array(),	    				  
	                       'SERVICE_ID' 	=> array(),
						   'PS_LOG_DATE' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'PS_LOG_DATE_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	    				   //'Description'    	=> array(),
	                      );

	    $dataParam = array('SERVICE_ID','SEQUENCE_ID','UUID');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_LOG_DATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
			}
		}
		// print_r($dataParamValue);
		// die;	
		// print_r($this->_request->getParam('whereval'));die;
			if(!empty($this->_request->getParam('logdate'))){
				$logarr = $this->_request->getParam('logdate');
					$dataParamValue['PS_LOG_DATE'] = $logarr[0];
					$dataParamValue['PS_LOG_DATE_END'] = $logarr[1];
			}
	                      
		$fParam = array();
		$datefrom = "";
		$dateto = "";
	                   
	  	$zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
    // $filter 		= $zf_filter->getEscaped('filter');
 		$filter 		= $this->_getParam('filter');
		$sequenceid	= $fParam['sequenceid']	= html_entity_decode($zf_filter->getEscaped('SEQUENCE_ID'));
		$uuid 		= $fParam['uuid']		= html_entity_decode($zf_filter->getEscaped('UUID'));
		$service 	= $fParam['service']	= html_entity_decode($this->_getParam('SERVICE_ID'));
		$datefrom 	= $fParam['datefrom']	= html_entity_decode($zf_filter->getEscaped('PS_LOG_DATE'));
		$dateto	 	= $fParam['dateto']		= html_entity_decode($zf_filter->getEscaped('PS_LOG_DATE_END'));
				
		if($filter == null)
		{	
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}
		 
		if($filter == null || $filter == TRUE)
		{
			$this->view->fDateTo    = $dateto;
			$this->view->fDateFrom  = $datefrom;
		}
			
		if(!empty($datefrom))
		{
			$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
			$fParam['datefrom']  = $FormatDate->toString($this->_dateDBFormat);	
		}
		
		if(!empty($dateto))
		{
			$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
			$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
		}
		
		$select = $this->_db->select()->distinct()
										->from(
													array('L_SERVICECALL'),
													array('SERVICE_ID'))
										//->where('ORDER_NO = 2')
										//->where('ORDER_NO = 2')
										->where('ENTITY_ID = "'.$MYPCHANNEL.'" or ENTITY_ID = "'.$ECHIDNA.'" or ENTITY_ID = "'.$VLINK.'" or ENTITY_ID = "'.$MAYAPADA.'" or ENTITY_ID = "'.$SGO.'" or ENTITY_ID = "'.$IST.'" or ENTITY_ID = "'.$ISTv2.'"')
										;	
											
		if(!empty($fParam['datefrom']) && empty($fParam['dateto']))
				$select->where("DATE(LOG_REQUEST_DATETIME) >= ".$this->_db->quote($fParam['datefrom']));

		if(empty($fParam['datefrom']) && !empty($fParam['dateto']))
				$select->where("DATE(LOG_REQUEST_DATETIME) <= ".$this->_db->quote($fParam['dateto']));

		if(!empty($fParam['datefrom']) && !empty($fParam['dateto']))
				// $select->where("DATE(LOG_REQUEST_DATETIME) BETWEEN ".$this->_db->quote($fParam['datefrom'])." and ".$this->_db->quote($fParam['dateto']));
				//echo $select;die;					
			$select->limit(20);




		$hosts = [
		'192.168.86.29:9200'
		];
		
		try{//echo "<pre>";

		$clientBuilder = Elasticsearch\ClientBuilder::create();   // Instantiate a new ClientBuilder
		$clientBuilder->setHosts($hosts);
		$client = $clientBuilder->build();
		
		//echo "GET".PHP_EOL;
		$params = [
	  		'index' => 'bungaku*',
	   		/*'body' => [
	        		'query' => [
	           		 'match' => [
	              	  'message' => 'PAIDBAQ3736410429'
	         		   ]
	       		 ]
	 		   ]*/
			];

		//print_r($filter);die;
		if(!empty($filter['seq'])){
		$params = [
	 	 'index' => 'bungaku*',
	  	 'body' => [
	   	     'query' => [
	     	       'match' => [
	        		        'message' => $filter['seq']
	        	   	 ]
		        ]
	 	   ]
		];

		}

		if(!empty($filter['uuid'])){
		$params = [
	 	 'index' => 'bungaku*',
	  	 'body' => [
	   	     'query' => [
	     	       'match' => [
	        		        'message' => $filter['uuid']
	        	   	 ]
		        ]
	 	   ]
		];

		}

		
	$response = $client->search($params);

		//echo '<pre>';
		//print_r($params);die;
		//$response = $client->search($params);
		//echo "SEARCH".PHP_EOL;
		//print_r($response);
		//die;

	}catch(Exception $e){
		//echo 'catch';
		//echo "<PRE>";
		//var_dump($e);
	}




	///////////////////get resulst///////////

	$data = $response['hits']['hits'];
// 
	// echo "<pre>";
	// var_dump($data);die;
	$this->view->data = $data;

	if($this->_request->getParam('print') == 1){

		// echo "<pre>";
		// var_dump($data);
		// die();
		foreach($data as $key=>$row)
		{
			$detail = json_decode($row['_source']['message'],true);
			$data[$key]['SERVICE_ID'] = $detail['Data']['SERVICE_ID'].' / '.$detail['Data']['SEQUENCE_ID'];
			$data[$key]['SERVICE_MESSAGE_REQUEST'] = '<textarea class="form-control" readonly rows="5" cols="20" style="overflow:auto;resize:none;color:black;font-family: Arial, Helvetica, sans-serif;">'.$detail['Data']['SERVICE_MESSAGE_REQUEST'].'</textarea>';
			$data[$key]['LOG_REQUEST_DATETIME'] = Application_Helper_General::convertDate($detail['Data']['LOG_REQUEST_DATETIME'],"dd MMM yyyy HH:mm:ss ");
			$data[$key]['SERVICE_MESSAGE_RESPONSE'] = '<textarea class="form-control" readonly rows="5" cols="20" style="overflow:auto;resize:none;color:black;font-family: Arial, Helvetica, sans-serif;">'.$detail['Data']['SERVICE_MESSAGE_RESPONSE'].'</textarea>';
			$data[$key]['LOG_RESPONSE_DATETIME'] = Application_Helper_General::convertDate($detail['Data']['LOG_RESPONSE_DATETIME'],"dd MMM yyyy HH:mm:ss ");
			$data[$key]['UUID'] = $detail['Data']['UUID'].' / '.$detail['Data']['ERROR_CODE'].':'.$detail['Data']['ERROR_MESSAGE'];
			$data[$key]['ERROR_DESCRIPTION'] = '<textarea class="form-control" readonly rows="5" cols="20" style="overflow:auto;resize:none;color:black;font-family: Arial, Helvetica, sans-serif;">'.$detail['Data']['ERROR_DESCRIPTION'].'</textarea>';
			
		}


		$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Gateway Log', 'data_header' => $fields));
	}

		// $arrservice 				= $this->_db->fetchAll($select);
		$this->view->arrservice 	= $arrservice;
		
		if($filter == TRUE)
		{
			
			if($service)
			{
				$this->view->service = $service;				
			}
			
			if($sequenceid)
			{
			   $this->view->sequenceid = $sequenceid;
			}
			
			if($uuid)
			{
			   $this->view->uuid = $uuid;
			}
			
		}
		if($filter_clear == TRUE){
			$datefrom = '00/00/0000';
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';
			
			$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
			$fParam['datefrom']  = $FormatDate->toString($this->_dateDBFormat);	
			//$fParam['datefrom'] = '';
			
			$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
			$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
			//$fParam['dateto'] = '';
			
		}
		
		$select2 = $this->_db->select()->from(
										array('L_SERVICECALL'),
										array(			
												'SEQUENCE_ID',												
												'SERVICE_ID',																													
												'SERVICE_MESSAGE_REQUEST',
												'LOG_REQUEST_DATETIME',
												'SERVICE_MESSAGE_RESPONSE',
												'LOG_RESPONSE_DATETIME',
												'UUID',
												'ERROR_CODE',
												'ERROR_MESSAGE',
												'ERROR_DESCRIPTION'														
											))
										//->where('ORDER_NO = 2')
										->where('ENTITY_ID = "'.$MYPCHANNEL.'" or ENTITY_ID = "'.$ECHIDNA.'" or ENTITY_ID = "'.$VLINK.'" or ENTITY_ID = "'.$MAYAPADA.'" or ENTITY_ID = "'.$SGO.'" or ENTITY_ID = "'.$IST.'" or ENTITY_ID = "'.$ISTv2.'"')
									;

		if($filter == null || $filter == true)
		{
			if(!empty($fParam['datefrom']) && empty($fParam['dateto']))
					$select2->where("DATE(LOG_REQUEST_DATETIME) >= ".$this->_db->quote($fParam['datefrom']));

			if(empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(LOG_REQUEST_DATETIME) <= ".$this->_db->quote($fParam['dateto']));

			if(!empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(LOG_REQUEST_DATETIME) BETWEEN ".$this->_db->quote($fParam['datefrom'])." and ".$this->_db->quote($fParam['dateto']));
		}

		if($filter == TRUE)
		{
			if($fParam['service'])
			{
				$select2->where("SERVICE_ID LIKE ".$this->_db->quote($fParam['service']));
			}

			if($fParam['sequenceid'])
			{
			   $select2->where("SEQUENCE_ID LIKE ".$this->_db->quote('%'.$fParam['sequenceid'].'%'));
			}

			if($fParam['uuid'])
			{
			   $select2->where("UUID LIKE ".$this->_db->quote('%'.$fParam['uuid'].'%'));
			}
		}

		$select2->order($sortBy.' '.$sortDir);
		if($filter_clear == TRUE){
			$select2->limit(1000);
		}
		//echo $select2; die;
		$datanew = $this->_db->fetchAll($select2);
				
		//$data = $model->getTransactionLog($fParam,$sortBy,$sortDir,$filter);

		$this->paging($datanew);


		if($pdf || $csv)
		{
			
			$dataexport = array();
			foreach($data as $key=>$row)
		{
			$detail = json_decode($row['_source']['message'],true);
			$dataexport[$key]['SERVICE_ID'] = $detail['Data']['SERVICE_ID'].' / '.$detail['Data']['SEQUENCE_ID'];
			$dataexport[$key]['SERVICE_MESSAGE_REQUEST'] = '<textarea class="form-control" readonly rows="5" cols="20" style="overflow:auto;resize:none;color:black;font-family: Arial, Helvetica, sans-serif;">'.$detail['Data']['SERVICE_MESSAGE_REQUEST'].'</textarea>';
			$dataexport[$key]['LOG_REQUEST_DATETIME'] = Application_Helper_General::convertDate($detail['Data']['LOG_REQUEST_DATETIME'],"dd MMM yyyy HH:mm:ss ");
			$dataexport[$key]['SERVICE_MESSAGE_RESPONSE'] = '<textarea class="form-control" readonly rows="5" cols="20" style="overflow:auto;resize:none;color:black;font-family: Arial, Helvetica, sans-serif;">'.$detail['Data']['SERVICE_MESSAGE_RESPONSE'].'</textarea>';
			$dataexport[$key]['LOG_RESPONSE_DATETIME'] = Application_Helper_General::convertDate($detail['Data']['LOG_RESPONSE_DATETIME'],"dd MMM yyyy HH:mm:ss ");
			$dataexport[$key]['UUID'] = $detail['Data']['UUID'].' / '.$detail['Data']['ERROR_CODE'].':'.$detail['Data']['ERROR_MESSAGE'];
			$dataexport[$key]['ERROR_DESCRIPTION'] = '<textarea class="form-control" readonly rows="5" cols="20" style="overflow:auto;resize:none;color:black;font-family: Arial, Helvetica, sans-serif;">'.$detail['Data']['ERROR_DESCRIPTION'].'</textarea>';
			
		}
			
			if($csv)
			{
				//var_dump($data);
				//die('here');
				Application_Helper_General::writeLog('SYLG','Download CSV Gateway Log');				
				$this->_helper->download->csv(array('Sequence ID','Service ID', 'Service Message Request', 'Log Request Date', 'Service Message Response', 'Log Response Date','UUID','Error Code','Error Message','Error Description'),$dataexport,null,'Gateway Log');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('SYLG','Download PDF Gateway Log');
				$this->_helper->download->pdf(array('Sequence ID','Service ID', 'Service Message Request', 'Log Request Date', 'Service Message Response', 'Log Response Date','UUID','Error Code','Error Message','Error Description'),$dataexport,null,'Gateway Log');
			}
		}
		else
		{			
			Application_Helper_General::writeLog('SYLG','View Gateway Log');
		}
	 
		$this->view->fields = $fields;
		$this->view->filter = $filter;


	    if(!empty($dataParamValue)){
	    		$this->view->logdateStart = $dataParamValue['PS_LOG_DATE'];
	    		$this->view->logdateEnd = $dataParamValue['PS_LOG_DATE_END'];

			    unset($dataParamValue['PS_LOG_DATE_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}
}
?>
