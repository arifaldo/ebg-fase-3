<?php
Class insurancebranch_Model_Insurancebranch {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getData($fParam,$sortBy = null,$sortDir = null,$filter = null,$custId)
    {
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_INS_BRANCH'),
							array(
								'INS_BRANCH_NAME',
								'INS_BRANCH_EMAIL',
								'INS_BRANCH_ACCT',
								'INS_BRANCH_CCY',
								'ACCT_TYPE',
								'LAST_APPROVED')
							);
		//var_dump($fParam);
		if($custId){
			$select->where('CUST_ID = ?',$custId); 
		}
		//
		
		// if($filter == 'Set Filter')
		{  			
	        if(isset($fParam['fbranch_name'])) if(!empty($fParam['fbranch_name'])) $select->where('UPPER(INS_BRANCH_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fParam['fbranch_name']).'%'));
	        if(isset($fParam['fbranch_email'])) if(!empty($fParam['fbranch_email'])) $select->where('UPPER(INS_BRANCH_EMAIL) = '.$this->_db->quote(strtoupper($fParam['fbranch_email'])));
	        if(isset($fParam['fbranch_acct'])) if(!empty($fParam['fbranch_acct']))    $select->where('INS_BRANCH_ACCT LIKE '.$this->_db->quote('%'.strtoupper($fParam['fbranch_acct']).'%'));
			if(isset($fParam['fbranch_acct_type'])) if(!empty($fParam['fbranch_acct_type']))    $select->where('ACCT_TYPE LIKE '.$this->_db->quote('%'.strtoupper($fParam['fbranch_acct_type']).'%'));


		}
		if( !empty($sortBy) && !empty($sortDir) )
			$select->order($sortBy.' '.$sortDir);
		
		 //echo $select;
		// print_r($fParam);die;
       return $this->_db->fetchall($select);
    }
  
}