<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'General/Charges.php';
require_once ('../../frontend/application/modules/purchasing/models/Purchasing.php');

class localtransreport_DetailController extends Application_Main
{
	
	public function initController(){
			  $this->_helper->layout()->setLayout('popup');
	}
	
	protected $_controllerList 	= "index";
	
	public function indexAction() 
	{
		$filter 			= new Application_Filtering();
		$PS_NUMBER 			= $filter->filter($this->_getParam('transactionid')				, "PS_NUMBER");
		$pdf 				= $filter->filter($this->_getParam('pdf')					, "BUTTON");
		$this->_paymentRef 	= $PS_NUMBER;
		
		$sessionNamespace = new Zend_Session_Namespace('URL_CP_PR');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ? 
									   $sessionNamespace->URL : '/'.$this->view->modulename.'/'.$this->_controllerList.'/index';
		
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		


		$filterArr = array('PS_NUMBER'    	=> array('StripTags'),
	                       
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array(	"PS_NUMBER");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		// 'name' => array(
		// 													'NotEmpty',
		// 													'messages' => array(
		// 																			$this->language->_('Can not be empty'),
		// 																		)
		// 												),	
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						
						
						'PS_NUMBER' 	=> array('NotEmpty',
															'messages' => array(
																					$this->language->_('Branch Can not be empty'),
																				)),	
						
						
                    	);
		
		$zf_filter  		= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		// print_r($PS_NUMBER);die;	
		if(empty($PS_NUMBER)){
			$PS_NUMBER 		= html_entity_decode($zf_filter->getEscaped('PS_NUMBER'));
			
		}
		
		
		



		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";
		
		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($arrPayType as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";
		
		$select	= $this->_db->select()
							->from(		array(	'P' 		=> 'T_PSLIP'),
										array(	'payStatus'	=>$casePayStatus,
												'PS_STATUS'	=>'P.PS_STATUS',
												'compCode'	=>'C.CUST_ID',
												'compName'	=>'C.CUST_NAME',
												'paySubject'=>'P.PS_SUBJECT',
												'created'	=>'P.PS_CREATED',
												'updated'	=>'P.PS_UPDATED',
												'efdate'	=>'P.PS_EFDATE',
												'sumamount'	=>'P.PS_TOTAL_AMOUNT',
												'PS_CATEGORY'	=>'P.PS_CATEGORY',
												'ccy'			=>'P.PS_CCY',
												'transmission'	=>'',
												'reference'		=>'',
												'traceno'		=>'P.UUID',
												'releaser'		=>'P.PS_RELEASER_USER_LOGIN',
												'challengecode'	=>'P.PS_RELEASER_CHALLENGE',
												'PS_TYPE'		=>'P.PS_TYPE',
												'bookrate'		=>'T.BOOK_RATE',
												'bookbuy'		=>'T.BOOK_RATE_BUY',
												'amount'		=>'T.TRA_AMOUNT',
												'transferfee'	=>'T.TRANSFER_FEE',
												'provfee'		=>'T.PROVISION_FEE',
												'fafee'			=>'T.FULL_AMOUNT_FEE',
												'sourceccy'		=>'T.SOURCE_ACCOUNT_CCY',
												'beneccy'		=>'T.BENEFICIARY_ACCOUNT_CCY',
												'payType'		=>$casePayType,
												'BALANCE_TYPE'=>new Zend_Db_Expr("(SELECT BALANCE_TYPE FROM T_PERIODIC_DETAIL WHERE PS_PERIODIC = P.PS_PERIODIC limit 1)"),
												'sourceresident'=>'CA.ACCT_RESIDENT',
												'sourcecitizen'=>'CA.ACCT_CITIZENSHIP',
												'sourcecategory'=>'CA.ACCT_CATEGORY',
												// 'sourceidtype'=>'CA.ACCT_ID_TYPE',
												// 'sourceidnum'=>'CA.ACCT_ID_NUM',
												'benefresident'=>'T.BENEFICIARY_RESIDENT',
												'benefcitizen'=>'T.BENEFICIARY_CITIZENSHIP',
												'benefcategory'=>'T.BENEFICIARY_CATEGORY',
												// 'benefidtype'=>'T.BENEFICIARY_ID_TYPE',
												// 'benefidnum'=>'T.BENEFICIARY_ID_NUMBER',
												'lldidentity'=>'T.LLD_IDENTITY',
												'lldpurpose'=>'T.LLD_TRANSACTION_PURPOSE',
												'lldrel'=>'T.LLD_TRANSACTOR_RELATIONSHIP',
												'T.EQUIVALENT_AMOUNT_IDR',
												'T.EQUIVALENT_AMOUNT_USD',
												'T.TRACE_NO','T.*',
												'PS_REMAIN'		=>'P.PS_REMAIN'
												
											))
							->joinLeft(	array(	'C' => 'M_CUSTOMER' ),'P.CUST_ID = C.CUST_ID',array())
							->joinLeft(	array(	'T' => 'T_TRANSACTION' ),'T.PS_NUMBER = P.PS_NUMBER',array('T.TRA_AMOUNT'))
							->joinLeft( array( 'CA' => 'M_CUSTOMER_ACCT'), 'T.SOURCE_ACCOUNT = CA.ACCT_NO', array())
							->where('T.TRANSACTION_ID =? ',$PS_NUMBER);
							// echo $select;die;
		if(!empty($PS_NUMBER)){
			$pslip = $this->_db->fetchRow($select);	
		}else{
			$pslip = array();
		}
		
		// echo'<pre>';print_r($pslip);die;
		$PSSTATUS  = $pslip["PS_STATUS"];
		$payStatus = $pslip["payStatus"];
			
		if($PSSTATUS == 5){
						
			// COMPLETED WITH (_) TRANSACTION (S) FAILED 
			// TRA_STATUS FAILED (4)
						
			$select = $this->_db->select()
								->from('T_TRANSACTION',array('countfailed'=>'count(TRANSACTION_ID)'))
								->where("TRA_STATUS = '4' AND PS_NUMBER = ?",$PS_NUMBER);
			$countFailed = $this->_db->fetchOne($select);
						
			if($countFailed == 0) 	$value = $payStatus;
			else 					$value = 'Completed with '.$countFailed.' Failed Transaction(s)';
						
		}
		else $value = $payStatus;
		
		$persenLabel = $pslip["BALANCE_TYPE"] == '2' ? ' %' : '';
		// View Data
		
		$this->_tableMst[0]["label"] = "Company Code";
		$this->_tableMst[1]["label"] = "Company Name";
		$this->_tableMst[2]["label"] = "Payment Ref#";
		$this->_tableMst[3]["label"] = "Payment Type";
		$this->_tableMst[4]["label"] = "Transaction Ref#";
		$this->_tableMst[5]["label"] = "Transfer Type";
		$this->_tableMst[6]["label"] = "Payment Subject";
		
		$this->_tableMst[7]["label"] = "Updated Date";
		$this->_tableMst[8]["label"] = "Payment Date";

		$this->_tableMst[9]["label"] = "Source Account";
		$this->_tableMst[10]["label"] = "Beneficiary Name";
		$this->_tableMst[11]["label"] = "Beneficiary NRC";
		$this->_tableMst[12]["label"] = "Beneficiary Phone";
		$this->_tableMst[13]["label"] = "Bank Name";
		$this->_tableMst[14]["label"] = "Bank Address";
		$this->_tableMst[15]["label"] = "Bank City";
		// $this->_tableMst[16]["label"] = "Country";
		$this->_tableMst[17]["label"] = "Currency / Amount";
		$this->_tableMst[18]["label"] = "Full Amount Fee";
		$this->_tableMst[19]["label"] = "Total";
		$this->_tableMst[20]["label"] = "Message";
		$this->_tableMst[21]["label"] = "Additional Message";
		$this->_tableMst[22]["label"] = "Status";
		// $this->_tableMst[9]["label"] = "Transmission";
		// $this->_tableMst[10]["label"] = "User Reference";
		//$this->_tableMst[11]["label"] = "Bank Response";
		//$this->_tableMst[12]["label"] = "Bank Resp. Code";
		// $this->_tableMst[11]["label"] = "Trace No.";
		// $this->_tableMst[12]["label"] = "Releaser";
		// $this->_tableMst[13]["label"] = "Challenge Code";
		// $this->_tableMst[14]["label"] = "Total Payment";
		// $this->_tableMst[15]["label"] = "Payment Type";
		
		$this->_tableMst[0]["value"] = $pslip['compCode'];
		$this->_tableMst[1]["value"] = $pslip['compName'];
		$this->_tableMst[2]["value"] = $pslip['PS_NUMBER'];
		$this->_tableMst[3]["value"] = 'Local Remittance';
		$this->_tableMst[4]["value"] = $PS_NUMBER;
if($pslip['TRANSFER_TYPE']=='9'){
	$transtype = 'FA';
}else{
	$transtype = 'No FA';
}

		$this->_tableMst[5]["value"] = $transtype;
		$this->_tableMst[6]["value"] = $pslip['paySubject'];
		$this->_tableMst[7]["value"] = Application_Helper_General::convertDate($pslip['updated'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		$this->_tableMst[8]["value"] = Application_Helper_General::convertDate($pslip['efdate'],$this->_dateViewFormat);

		$this->_tableMst[9]["value"] = $pslip['SOURCE_ACCOUNT'].' ('.$pslip['SOURCE_ACCOUNT_CCY'].') '.$pslip['SOURCE_ACCOUNT_NAME'];
		$this->_tableMst[10]["value"] = $pslip['BENEFICIARY_ACCOUNT_NAME'];
		$this->_tableMst[11]["value"] = $pslip['BENEFICIARY_ID_NUMBER'];
		$this->_tableMst[12]["value"] = $pslip['BENEFICIARY_MOBILE_PHONE_NUMBER'];
		$this->_tableMst[13]["value"] = $pslip['BENEFICIARY_BANK_NAME'];
		$this->_tableMst[14]["value"] = $pslip['BENEFICIARY_BANK_ADDRESS1'];
		$this->_tableMst[15]["value"] = $pslip['BENEFICIARY_BANK_CITY'];
		// $this->_tableMst[16]["value"] = $pslip['BENEFICIARY_BANK_COUNTRY'];
		$this->_tableMst[17]["value"] = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['amount']);
		$this->_tableMst[18]["value"] = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['FULL_AMOUNT_FEE']);
		if($pslip['TRANSFER_TYPE']=='9'){
		$total = $pslip['FULL_AMOUNT_FEE']+$pslip['amount'];
		}else{
			$total = $pslip['amount']-$pslip['FULL_AMOUNT_FEE'];
		}
		$this->_tableMst[19]["value"] = $pslip['ccy'].' '.Application_Helper_General::displayMoney($total);
		$this->_tableMst[20]["value"] = $pslip['TRA_MESSAGE'];
		$this->_tableMst[21]["value"] = $pslip['TRA_ADDITIONAL_MESSAGE'];



		
		
		//die;
		
		
		
		$this->view->PS_NUMBER 			= $PS_NUMBER;
		$this->view->tableMst 			= $this->_tableMst;
		$this->view->totalTrx 			= (isset($pslip["numtrx"])) ? $pslip["numtrx"] : '';
		$this->view->totalAmt 			= (isset($pslip["amount"])) ? $pslip["amount"] : '';
		$this->view->pdf 				= ($pdf)? true: false;
		$this->view->arrStatus  		= array(	'5'=>'Success',
						'4'=>'Cancel Payment'
					);
		
		if($pslip['PS_STATUS'] == '5' || $pslip['PS_STATUS'] == '4'){
			$this->view->pstatus  		= $pslip['PS_STATUS'];	
		}else{
			$this->view->pstatus  		= false;	
		}
		// Zend_Debug::dump($pslip);
		// print_r($pslip);die;
		$this->view->psnumb  		= $pslip['PS_NUMBER'];
		if($this->view->tableDtl)
		{
			$tableDtl = $this->view->tableDtl;
			
//			echo($PSSTATUS);die;
			if($PSSTATUS == '1' || $PSSTATUS == '2' || $PSSTATUS == '3' || $PSSTATUS == '7')
			{

				$no=0;
				foreach($tableDtl as $row)
				{
					if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["domestic"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
					{
						$transferType = $row['TRANS_TYPE_FULL'];
					}
					else
					{
						$transferType = $row['TRANSFER_TYPE'];
					}

					//echo($row['SOURCE_ACCOUNT']." -- ".$pslip["compCode"]." == ".$pslip["PS_TYPE"]." - - ".$pslip["compCode"]." - - ".$transferType."<br>");
				
					if($pslip["PS_TYPE"] !== $this->_paymenttype["code"]["remittance"]){
						$chargesObj = Charges::factory($pslip["compCode"],$transferType);
						$paramCharges = array('accsrc'=>$row['SOURCE_ACCOUNT'],'transferType'=>$transferType);
						$chargesAmt = $chargesObj->getCharges($paramCharges);
					}
				
					//Zend_Debug::dump($chargesAmt);
					//$this->view->tableDtl[$no]['TRANSFER_FEE']=$chargesAmt;
					//$this->view->tableDtl[$no]['TRANSFER_FEE']= Application_Helper_General::displayMoney($chargesAmt);
					
					$no++;
				}
			}
			/*else if($PSSTATUS == '5' || $PSSTATUS == '6' || $PSSTATUS == '9')
			{

			}*/
			else if($PSSTATUS == '4' || $PSSTATUS == '14')
			{
				$no=0;
				foreach($tableDtl as $row)
				{
					// $this->view->tableDtl[$no]['TRANSFER_FEE']= ' - ';
					$no++;
				}
			}
			
			$no=0;
			foreach($tableDtl as $row)
			{
									//
				if($tableDt1[$no]['TRANSFER_TYPE']=='PB'){
					$tableDt1[$no]['TRANSFER_TYPE'] = 'Mayapada';
				}
				unset($this->view->tableDtl[$no]['SOURCE_ACCOUNT']);
					$no++;
			}
			//print_r($tableDtl);die;
			unset($this->view->fields['SOURCE_ACCOUNT']);
			//Zend_Debug::dump($this->view->tableDtl);
		}

		$filterArr = array('STATUS'    	=> array('StripTags'),
						'PS_NUMBER'    	=> array('StripTags'),
	                       
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array(	"STATUS","PS_NUMBER");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		if(!empty($dataParamValue['STATUS']) && $dataParamValue['STATUS'] != '0' && !empty($pslip)){
			
				$paymentData = array('PS_STATUS' => $dataParamValue['STATUS']);

			$whereBeneficiary = array(
						'PS_NUMBER = ?'	 =>  $dataParamValue['PS_NUMBER']
			);
			
			$this->_db->update('T_PSLIP', $paymentData,$whereBeneficiary);


			$transdata = array('TRA_STATUS' => '3');

			$wheretrans = array(
						'PS_NUMBER = ?'	 =>  $dataParamValue['PS_NUMBER']
			);
			
			$this->_db->update('T_TRANSACTION', $transdata,$wheretrans);

			// print_r($dataParamValue['STATUS']);die;
			// if($dataParamValue['STATUS']=='5'){
				$this->_redirect('/notification/popupsuccess/index');
			// }
		}
		// print_r($dataParamValue);die;
		

		
		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render('/detail/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Payment Detail',$outputHTML); 
			Application_Helper_General::writeLog('RPPY','Download PDF Payment Detail Report');
		}
		else
		{
			Application_Helper_General::writeLog('RPPY','View Payment Detail Report');
		}
		
		
	}
	
	
	
	
	
}