<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';

class Remittancechar_SuggestiondetailController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{
		// 		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();

// 		if(count($temp)>1){
//       		if($temp[0]=='F' || $temp[0]=='S'){
//       			if($temp[0]=='F')
//         			$this->view->error = 1;
//         		else
//         			$this->view->success = 1;
//       			$msg = ''; unset($temp[0]);
//       			foreach($temp as $value)
//       			{
//       				if(!is_array($value))
//       					$value = array($value);
//       				$msg .= $this->view->formErrors($value);
//       			}
//         		$this->view->report_msg = $msg;
//      		}	
//     	}
		$this->_helper->layout()->setLayout('newlayout');
		$this->_helper->layout()->setLayout('popup');

		$buser_id = $this->_userIdLogin;
		$this->view->buser_id 	= $buser_id;

		$settings =  new Settings();

		
		$modelremitance = new remittancechar_Model_Remittancechar();
		
// 		$this->view->tempcek = $model->cekTemp();
// 		$this->view->paginator = $model->getRemittanceType($charge_type);
// 		$this->view->transferfee = $model->getRemittanceTypeIndexTemp('3');
		$params = $this->_request->getParams();
		$this->view->transferfee = $modelremitance->getRemittanceTypeIndexTemp('3',$params['changes_id']);
// 		$transferfee = $model->getRemittanceTypeIndexTemp('3',$params['changes_id']);
// 		print_r($transferfee);die;
// 		print_r($params);die;
		$this->view->fullamount = $modelremitance->getRemittanceTypeIndexTemp('4',$params['changes_id']);
		$this->view->provfee = $modelremitance->getRemittanceTypeIndexTemp('5',$params['changes_id']);
// 		
		$this->view->changes_id =    $params['changes_id'];
		
		if(array_key_exists('changes_id', $params))
		{
			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
			$validators = array(
					'changes_id' => array(
							'NotEmpty',
							'Digits',
							'messages' => array(
									'No Suggestion ID',
									'Wrong ID Format',
							),
					),
			);
		
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				$tempData = $masterData = null;
				$changeId = $zf_filter_input->changes_id;
				$changeInfo = array();
				$changeInfo = $this->getGlobalChanges($changeId);
		
		
				if(empty($changeInfo))
				{
					$this->_redirect('/notification/invalid/index');
				}
				else{
					
					if(!in_array($changeInfo['CHANGES_STATUS'],array('RR','WA')))
					{
						$this->_redirect('/notification/success/index');
					}
// 					$providerType = array ( 1 => 'Payment', 2 => 'Purchase');
					
					
					$this->view->suggested_by = $changeInfo['CREATED_BY'];
					$this->view->suggestion_date = $changeInfo['CREATED'];
					$this->view->temp_data = $tempData;
					$this->view->changes_id = $changeId;
// 					$this->_redirect('/notification/success/index');
				}
			}
			else
			{
				$errors = $zf_filter_input->getMessages();
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
			}
		}
		else
		{
			$errorRemark = 'No Suggestion ID';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
		}
		
		Application_Helper_General::writeLog('CHCL','View Remittance Charges Changes List');
	}

}

