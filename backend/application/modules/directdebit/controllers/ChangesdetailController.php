<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';

class directdebit_ChangesdetailController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;


		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
		$password = $sessionNamespace->token; 
		$this->view->token = $sessionNamespace->token;
		$AESMYSQL = new Crypt_AESMYSQL();
		$NUM_ID      = urldecode($this->_getParam('changes_id'));
		$DIRECT_ID = $AESMYSQL->decrypt($NUM_ID, $password);

		
		if($DIRECT_ID){

			$select = $this->_db->select()
					 ->from(array('C' => 'TEMP_DIRECTDEBIT'),array('*','company'	=> new Zend_Db_Expr("CONCAT(COMP_NAME , ' (' , COMP_ID , ')  ' )")))
					 ->join(array('D' => 'T_GLOBAL_CHANGES'), 'D.CHANGES_ID = C.CHANGES_ID',array('D.CHANGES_INFORMATION'))
					 ->join(array('B' => 'M_BUSER'), 'B.BUSER_ID = C.DIR_SUGESTEDBY',array('B.BUSER_NAME'))
					 ->where('C.CHANGES_ID = ?', $DIRECT_ID)
					 ->query()->FetchAll();

			// echo "<pre>";
			// var_dump($DIRECT_ID);
			// var_dump($select);
			
			if($select){
                $data = $select[0];

				$this->view->COMP_NAME = $select[0]['COMP_NAME'];
				$this->view->COMP_ID = $select[0]['COMP_ID'];
				$this->view->CLIENT_REFF = $select[0]['CLIENT_REFF'];
				$this->view->CLIENT_NAME = $select[0]['CLIENT_NAME'];
				$this->view->DOCUMENT_REFF = $data['DOCUMENT_REFF'];
				$this->view->BUSER_NAME = $data['BUSER_NAME'];
				$this->view->DOC_ID = $data['DOC_ID'];
				$this->view->DIR_SUGESTEDBY = $data['DIR_SUGESTEDBY'];
				$this->view->DIR_SUGESTED = $data['DIR_SUGESTED'];
				$this->view->CHANGES_INFORMATION = $data['CHANGES_INFORMATION'];
				$this->view->DIR_CCY = $select[0]['DIR_CCY'];
				$this->view->DEBIT_ACCT = $select[0]['DEBIT_ACCT'];
				$this->view->DOC_ID = $select[0]['DOC_ID'];

				if($this->_getParam('approve') == 'Approve' ){
					$actor = $this->_userIdLogin;
					$bgroup = $this->_db->select()
						->from('TEMP_DIRECTDEBIT')
						->where($this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']))
						->query()
						->fetch(Zend_Db::FETCH_ASSOC);
						
					if(!count($bgroup)){
						$this->_errorCode = '22';
						$this->_errorMsg = 'Query failed(directdebit)';
						return false;
						return false;
					}

					// check if delete or not
					if($data['DIR_STATUS'] == '2'){
						$this->_db->beginTransaction();
						$dataUpdate =  array(					
							'DIR_STATUS' 		=> '2',
							'DIR_APPROVED' 	=> new Zend_Db_Expr('now()'), 
							'DIR_APPROVEDBY' 	=> $actor
						);							

						$where = array('CLIENT_REFF = ?' => $data['CLIENT_REFF']);
						$result = $this->_db->update('T_DIRECTDEBIT',$dataUpdate,$where);
						$this->_db->commit();
					}else{
						$newstatus = '1';
						//insert to master table M_BGROUP
						$insertArr = array_diff_key($bgroup,array('CHANGES_ID'=>''));
						$insertArr['DIR_APPROVED'] = new Zend_Db_Expr('now()');
						$insertArr['DIR_STATUS'] = $newstatus; 
						$insertArr['DIR_TYPE'] = '1'; 
						$insertArr['DIR_APPROVEDBY'] = $actor;

						$currentD = $this->_db->select()
									->from(array('C' => 'T_DIRECTDEBIT'));

						$currentD->where("CLIENT_REFF = ?",$data['CLIENT_REFF']);
						$currentD = $this->_db->fetchRow($currentD);

						if($currentD){
							$where = array('CLIENT_REFF = ?' => $data['CLIENT_REFF']);
							$result = $this->_db->update('T_DIRECTDEBIT',$insertArr,$where);
						}else{
							$bgroupins = $this->_db->insert('T_DIRECTDEBIT',$insertArr);
							if(!(boolean)$bgroupins) {
								$this->_errorCode = '82';
								$this->_errorMsg = 'Query failed(notional)';
								return false;
							}
						}


					}
					
					$bgroupdelete = $this->_db->delete('TEMP_DIRECTDEBIT',$this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']));			
					$rowUpdated = $this->_db->update(
					'T_GLOBAL_CHANGES',
					array(
						'CHANGES_STATUS' => 'AP',						
						'LASTUPDATED' => new Zend_Db_Expr("now()"),
					),
						$this->_db->quoteInto('CHANGES_ID = ?', $data['CHANGES_ID'])
					);
					
					$rowHistory = $this->_db->insert(
						'T_PSLIP_HISTORY',
						array(
							'PS_REASON' => 'Approve',
							'USER_LOGIN' => $actor,
							'PS_NUMBER' => $data['CLIENT_REFF'],
							'DATE_TIME' => new Zend_Db_Expr("now()"),
						)
					);
			
					$this->setbackURL('/'.$this->_request->getModuleName().'/approve');
					$this->_redirect('/notification/success');
				}

				if($this->_getParam('reject') == 'Reject' ){
                    
                        
					$bgroupdelete = $this->_db->delete('TEMP_DIRECTDEBIT',$this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']));
		  
					$this->setbackURL('/'.$this->_request->getModuleName().'/approve');
					$this->_redirect('/notification/success');
				}
			}

		}

		$select3 = $this->_db->select()
                  ->from(array('A' => 'T_FILE_SUBMIT')); 
        
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;  

	    $AESMYSQL = new Crypt_AESMYSQL();
	    $PS_NUMBER      = urldecode($this->_getParam('FILE_ID'));
	    $FILE_ID = $AESMYSQL->decrypt($PS_NUMBER, $password);


		if($FILE_ID)
		{	


			$select3->where('FILE_ID =?',$FILE_ID);
			$data = $this->_db->fetchRow($select3);
			
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['FILE_NAME'],$attahmentDestination.$data['FILE_SYSNAME']);
			
			$updateArr = array();
			$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED']+1;
			$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;
			
			$whereArr = array('FILE_ID = ?'=>$FILE_ID);
			
			$fileupdate = $this->_db->update('T_FILE_SUBMIT',$updateArr,$whereArr);
			//echo "file downloaded: ".$data['FILE_DOWNLOADED'];
			Application_Helper_General::writeLog('VDEL','Download File Underlying Document '.$data['FILE_NAME']);
		}
		else{
			Application_Helper_General::writeLog('VDEL','View File Underlying Document');
		}

    }
}