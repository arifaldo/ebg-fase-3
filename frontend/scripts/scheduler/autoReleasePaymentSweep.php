<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	
	$truecounter = 0;
	$falsecounter = 0;
	$cronResult = "";

	// INNER JOIN KE T_TRANSACTION
	// PS STATUS  7
	// EFTDATE (today)
	// PS CATEGORY (SINGLE PAYMENT)

	$flag = realpath(dirname(__FILE__)).'/inprogressReleasePaymentSweep'; //path to flag file

	$res = 'Execute Release Payment Sweep';
	$x = 0;

	echo 'Release Payment Sweep Executor'.PHP_EOL;

	if (file_exists($flag)) {
		echo 'Queue inprogress at '.date('Y-m-d H:i:s').PHP_EOL;
	}else{

		try{
			$fp = fopen($flag, 'w');
			@fclose($fp);
			echo 'Create flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
		}catch(Exception $e){
			$stringFailed = 'Can\'t create flag at: '.date('Y-m-d H:i:s');
			Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
			echo $stringFailed.PHP_EOL;

			die();
		}

	$data = $db->select	()
				->FROM	(array('P' => 'T_PSLIP'),array('PS_NUMBER','CUST_ID'))
//				->JOIN	(array('T' => 'T_TRANSACTION'),'T.PS_NUMBER = P.PS_NUMBER',array('*'))
				->WHERE('P.PS_STATUS = 7')						
				->WHERE('DATE(P.PS_EFDATE) = DATE(now())')
				->WHERE('HOUR(P.PS_EFTIME) = HOUR(NOW()) ')
				->WHERE('MINUTE(P.PS_EFTIME) between MINUTE(NOW())-5 and MINUTE(NOW()) +5') 

				
				->WHERE("P.PS_CATEGORY = 'SWEEP PAYMENT' ")
				// echo $data;
				->QUERY()->FETCHALL();

	

// 	if(empty($data)){
// 		$data = $db->select	()
// 				->FROM	(array('P' => 'T_PSLIP'),array('PS_NUMBER','CUST_ID'))
// //				->JOIN	(array('T' => 'T_TRANSACTION'),'T.PS_NUMBER = P.PS_NUMBER',array('*'))
// 				->WHERE('P.PS_STATUS = 7')						
// 				->WHERE('P.PS_TYPE = 22')						
// 				->WHERE('DATE(P.PS_EFDATE) = DATE(now())')
// 				//->WHERE('HOUR(P.PS_EFTIME) = HOUR(NOW()) ')
// 				->WHERE("P.PS_CATEGORY = 'SWEEP PAYMENT' ")
// 				// echo $data;
// 				->QUERY()->FETCHALL();
// 	}
				 
				//var_dump($data);die('here');
	if(!empty($data))
	{
		//var_dump($data);die;
		foreach($data as $row)
		{
			 //echo "here";
			$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], 'System');
			$resultRelease = $Payment->releasePayment();
					
			if ($resultRelease['status'] == '00')
			{
				$truecounter++;
			}
			else
			{
				$falsecounter++;
			}
		}
		
		if($truecounter>0)
		{
			$cronResult .= " ".$truecounter." payment(s) success";
		}
		if($falsecounter>0)
		{
			$cronResult .= " ".$falsecounter." payment(s) failed";
		}		
	}
	else
	{
		$cronResult .= "No data released";
	}

			try{
				unlink($flag);
				echo 'Delete flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
			}catch(Exception $e){
				$stringFailed = 'Can\'t delete flag at: '.date('Y-m-d H:i:s');
				Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
				echo $stringFailed.PHP_EOL;

				die();
			}
	}

	$filename = basename(__FILE__);
	Application_Helper_General::cronLog($filename, $cronResult, 1);
?>