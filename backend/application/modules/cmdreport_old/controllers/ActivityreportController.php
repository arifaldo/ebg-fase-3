<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class cmdreport_ActivityreportController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()->distinct()
			->from(
				array('A' => 'M_CUSTOMER'),
				array('CUST_ID')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();
		//Zend_Debug::dump($select); die;
		$this->view->var = $select;

		$activity = $this->_db->select()->distinct()
			->from(array('A' => 'M_FPRIVILEGE'), array('FPRIVI_ID', 'FPRIVI_DESC'))
			->order('FPRIVI_DESC ASC')
			->query()->fetchAll();

		$login = array('FPRIVI_ID' => 'FLGN', 'FPRIVI_DESC' => 'Login');
		$logout = array('FPRIVI_ID' => 'FLGT', 'FPRIVI_DESC' => 'Logout');
		$changepass = array('FPRIVI_ID' => 'CHMP', 'FPRIVI_DESC' => 'Change My Password');
		//$resetpass = array('FPRIVI_ID'=>'RFPW','FPRIVI_DESC'=>'Reset Forgot Password');
		array_unshift($activity, $changepass);
		array_unshift($activity, $logout);
		array_unshift($activity, $login);
		//array_unshift($activity,$resetpass);

		$activityarr = Application_Helper_Array::listArray($activity, 'FPRIVI_ID', 'FPRIVI_DESC');
		asort($activityarr);
		//Zend_Debug::dump($activityarr);die;
		$this->view->activity = $activityarr;

		$fields = array(
			'B.CUST_ID'      	=> array(
				'field' => 'COMPANY',
				'label' => $this->language->_('Company'),
				'sortable' => true
			),
			'USER_ID'           => array(
				'field' => 'USER_ID',
				'label' => $this->language->_('User ID'),
				'sortable' => true
			),
			'LOG_DATE'      	=> array(
				'field' => 'LOG_DATE',
				'label' => $this->language->_('Activity Date'),
				'sortable' => true
			),
			'FPRIVI_DESC'  	=> array(
				'field' => 'FPRIVI_DESC',
				'label' => $this->language->_('Activity Type'),
				'sortable' => true
			),
			'ACTION_FULLDESC'     => array(
				'field' => 'ACTION_FULLDESC',
				'label' => $this->language->_('Description'),
				'sortable' => true
			)
		);

		$filterlist = array('PS_ACTIVITY', 'COMP_ID', 'COMP_NAME', 'USER_ID', 'ACTIVITY_TYPE');

		$this->view->filterlist = $filterlist;


		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'LOG_DATE');
		$sortDir = $this->_getParam('sortdir', 'desc');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$select2 = $this->_db->select()
			->from(array('B' => 'T_FACTIVITY'), array())
			->joinleft(array('C' => 'M_CUSTOMER'), 'B.CUST_ID = C.CUST_ID', array())
			->joinleft(array('D' => 'M_FPRIVILEGE'), 'B.ACTION_DESC = D.FPRIVI_ID', array(
				'C.CUST_ID',
				'C.CUST_NAME',
				'B.USER_ID',
				'COMPANY'	=> new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )"),
				'B.LOG_DATE',
				'FPRIVI_DESC' => new Zend_Db_Expr("(CASE B.ACTION_DESC 
																							        	  								   	WHEN 'FLGN' THEN 'Login' 
																							        	  									WHEN 'FLGT' THEN 'Logout' 
																							        	  									WHEN 'CHMP' THEN 'Change My Password' 
																							        	  									WHEN 'RFPW' THEN 'Reset Forgot Password' 
																							        	  									ELSE D.FPRIVI_DESC 
																							        	  									END)"),
				'B.ACTION_FULLDESC'
			));
		//$select2 -> where("B.CUST_ID = 'SGO006'");


		$filterArr = array(
			'filter'				=> array('StripTags', 'StringTrim'),
			'COMP_ID'    	=> array('StripTags'),
			'SEARCH_CUST'    	=> array('StripTags', 'StringTrim', 'StringToUpper'),
			'COMP_NAME'  		=> array('StripTags', 'StringTrim', 'StringToUpper'),
			'description'  		=> array('StripTags', 'StringTrim', 'StringToUpper'),
			'USER_ID'    			=> array('StripTags', 'StringTrim', 'StringToUpper'),
			'SEARCH_ACTIVITY' 	=> array('StripTags', 'StringToUpper'),
			'ACTIVITY_TYPE' => array('StripTags'),
			'PS_ACTIVITY' 			=> array('StripTags', 'StringTrim'),
			'PS_ACTIVITY_END' 			=> array('StripTags', 'StringTrim'),
		);

		$dataParam = array('COMP_ID', 'COMP_NAME', 'USER_ID', 'ACTIVITY_TYPE');
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "PS_ACTIVITY") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}

		//echo "<pre>";
		//print_r ($dataParamValue);

		if (!empty($this->_request->getParam('actdate'))) {
			$actarr = $this->_request->getParam('actdate');
			$dataParamValue['PS_ACTIVITY'] = $actarr[0];
			$dataParamValue['PS_ACTIVITY_END'] = $actarr[1];
		}

		/*var_dump($dataParamValue);
*/
		$validator = array(
			'filter'			 	=> array(),
			'SEARCH_CUST'   	 	=> array(),
			'COMP_ID'			=> array(),
			'COMP_NAME'  		=> array(),
			'description'  		=> array(),
			'USER_ID'    			=> array(),
			'SEARCH_ACTIVITY' 	=> array(),
			'ACTIVITY_TYPE'		=> array(),
			'PS_ACTIVITY' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_ACTIVITY_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);


		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue);
		$filter = $this->_request->getParam('filter');
		$custid = html_entity_decode($zf_filter->getEscaped('COMP_ID'));
		$custname = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
		$description = html_entity_decode($zf_filter->getEscaped('description'));
		$userid = html_entity_decode($zf_filter->getEscaped('USER_ID'));
		$active = html_entity_decode($zf_filter->getEscaped('ACTIVITY_TYPE'));
		$datefrom = html_entity_decode($zf_filter->getEscaped('PS_ACTIVITY'));
		$dateto = html_entity_decode($zf_filter->getEscaped('PS_ACTIVITY_END'));
		$clearfilter	= $this->_getParam('clearfilter');

		if ($filter == null && $clearfilter != 1) {
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->actdateStart  = (date("d/m/Y"));
			$this->view->actdateEnd  = (date("d/m/Y"));
			//Zend_Debug::dump($this->view->fDateFrom); die;
		} else {

			if ($filter != NULL) {
				$datefrom 		= html_entity_decode($zf_filter->getEscaped('PS_ACTIVITY'));
				$dateto 		= html_entity_decode($zf_filter->getEscaped('PS_ACTIVITY_END'));
			} else if ($clearfilter == 1) {
				$datefrom 	= "";
				$dateto 	= "";
			}
		}

		if ($filter == null || $filter == TRUE) {
			$this->view->actdateStart    = $dateto;
			$this->view->actdateEnd  = $datefrom;
			//Zend_Debug::dump($custid); die;

			if (!empty($datefrom)) {
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($dateto)) {
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto    = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($datefrom) && empty($dateto))
				$select2->where("DATE(B.LOG_DATE) >= " . $this->_db->quote($datefrom));

			if (empty($datefrom) && !empty($dateto))
				$select2->where("DATE(B.LOG_DATE) <= " . $this->_db->quote($dateto));

			if (!empty($datefrom) && !empty($dateto))
				$select2->where("DATE(B.LOG_DATE) between " . $this->_db->quote($datefrom) . " and " . $this->_db->quote($dateto));
		}

		if ($filter == TRUE) {
			if ($active) {
				$this->view->active = $active;
				if (($active != 'FLGN' && $active != 'FLGT' && $active != 'CHMP')) {
					$select2->where("D.FPRIVI_ID LIKE " . $this->_db->quote($active));
				} else {
					$select2->where("B.ACTION_DESC LIKE " . $this->_db->quote($active));
				}
			}

			if ($custid) {
				$this->view->cekcustid = $custid;
				$select2->where("C.CUST_ID LIKE " . $this->_db->quote('%' . $custid . '%'));
			}

			if ($custname) {
				$this->view->companyName = $custname;
				$select2->where("C.CUST_NAME LIKE " . $this->_db->quote('%' . $custname . '%'));
			}

			if ($userid) {
				$this->view->UserID = $userid;
				$select2->where("B.USER_ID LIKE " . $this->_db->quote('%' . $userid . '%'));
			}

			if ($description) {
				$this->view->description = $description;
				$select2->where("B.ACTION_FULLDESC LIKE " . $this->_db->quote('%' . $description . '%'));
			}
		}
		$select2->where('B.USER_ID != ?', 'Backend');
		$select2->order($sortBy . ' ' . $sortDir);
		$this->paging($select2);
		// echo $select2;die;

		if ($pdf || $csv || $this->_request->getParam('print')) {
			$arrLabel = Application_Helper_Array::simpleArray($fields, "label");

			$arr = $this->_db->fetchAll($select2);
			$newArr = [];

			foreach ($arr as $key => $value) {
				$newArr[] = [
					'No' => $key + 1,
					'Company' => $value['CUST_NAME'],
					'User ID' => $value['USER_ID'],
					'Activity Date' => $value['LOG_DATE'],
					'Activity Type' => $value['FPRIVI_DESC'],
					'Deskripsi' => $value['ACTION_FULLDESC']
				];
			}

			foreach ($arr as $key => $value) {
				$arr[$key]["LOG_DATE"] = Application_Helper_General::convertDate($value["LOG_DATE"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}

			$newArrLabel = [
				'No',
				'Company',
				'User ID',
				'Activity Date',
				'Activity Type',
				'Deskripsi',
			];

			if ($csv) {
				Application_Helper_General::writeLog('RPCT', 'Download CSV Frontend User Activity Report');
				// Application_Helper_General::writeLog('RPCT', 'Download CSV Customer Activity Report');
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv($newArrLabel, $newArr, null, 'Frontend User Activity Report');
				// $this->_helper->download->csv($arrLabel, $arr, null, 'Customer Activity Report');
			}

			if ($pdf) {
				Application_Helper_General::writeLog('RPCT', 'Download PDF Frontend User Activity Report');
				$this->_helper->download->pdf($arrLabel, $arr, null, 'Frontend User Activity Report');
			}
			if ($this->_request->getParam('print') == 1) {
				$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Frontend User Activity Report', 'data_header' => $fields));
				Application_Helper_General::writeLog('RPCT', 'Print Frontend User Activity Report');
			}
		} else {
			Application_Helper_General::writeLog('RPCT', 'View Frontend User Activity Report');
		}
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if (!empty($dataParamValue)) {
			$this->view->actdateStart = $dataParamValue['PS_ACTIVITY'];
			$this->view->actdateEnd = $dataParamValue['PS_ACTIVITY_END'];

			unset($dataParamValue['PS_ACTIVITY_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
			// print_r($whereval);die;
		}
	}
}
