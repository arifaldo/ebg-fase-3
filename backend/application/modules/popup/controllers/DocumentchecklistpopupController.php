<?php


class popup_DocumentchecklistpopupController extends Application_Main
{
	 protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction() 
	{
		$scheme_type = $this->_getParam('scheme_type');
		$arrdoc_no_id = $this->_getParam('doc_no_id');
		
		$select = $this->_db->select()
					->FROM('M_DOC_CONFIGURATION',array('DOC_ID','BUSS_SCHEME_CODE','DOC_TYPE','DOC_ORIGIN'))			
					->WHERE("BUSS_SCHEME_CODE = ?",$scheme_type)
					->WHERE("DOC_STATUS = 1");
					
		$result = $this->_db->fetchall($select);

		$arrdoc_no_id = explode (',',$arrdoc_no_id);
		$this->view->arrdoc_no_id = $arrdoc_no_id;

		$this->view->result = $result;
	}

}
