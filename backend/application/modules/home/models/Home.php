<?php

Class home_Model_Home
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function checkIsRequireChangePwd($user_id,$timeoutpwd,$daybefore)
    {
	/*$select = $this->_db->select()
						 ->from(array('A'=>'M_BUSER'),array( 'datetime'		=>'BUSER_LASTCHANGEPASSWORD',
															 'dateadd'		=>'DATEADD(day,'.$timeoutpwd.', BUSER_LASTCHANGEPASSWORD)'
															))
						 ->where("	A.BUSER_STATUS = '1' 
									AND (A.BUSER_ISREQUIRE_CHANGEPWD is NULL or A.BUSER_ISREQUIRE_CHANGEPWD != '1')
									AND A.CREATED is not NULL
									AND BUSER_ID = '".$this->_userIdLogin."'
									AND DATEADD(day,$dayleft,convert(date, A.BUSER_LASTCHANGEPASSWORD)) = convert(date, GETDATE())"
								);*/

		$select = $this->_db->select()
						 ->from(array('A'=>'M_BUSER'),array( 
														'datetime'		=>'BUSER_LASTCHANGEPASSWORD',
														'dateadd'		=> 'ADDDATE(BUSER_LASTCHANGEPASSWORD,'.$timeoutpwd.')'
													))
						 ->where("	A.BUSER_STATUS = '1' 
									AND (A.BUSER_ISREQUIRE_CHANGEPWD is NULL or A.BUSER_ISREQUIRE_CHANGEPWD != '1')
									AND A.CREATED is not NULL
									AND BUSER_ID = ".$this->_db->quote($user_id)."
									AND DATEDIFF( ADDDATE(BUSER_LASTCHANGEPASSWORD,".$timeoutpwd."),now() ) < $daybefore
								");
									//AND DATEDIFF(DATE(ADDDATE(BUSER_LASTCHANGEPASSWORD,15)),now() ) <5
       return $this->_db->fetchRow($select);
    }
  
    public function getChangesData($whPriviID)
    {
		$select = $this->_db->SELECT()
							->FROM(array('G' => 'T_GLOBAL_CHANGES'),array('DISPLAY_TABLENAME'));
		$select->where('(CHANGES_STATUS = '.$this->_db->quote('WA').' OR CHANGES_STATUS = '.$this->_db->quote('RR').')');
		$select->where("G.MODULE IN (".$whPriviID.")");
		$select->order("DISPLAY_TABLENAME ASC");
		
       return $this->_db->FETCHALL($select);
    }

}