<?php
Class mobile_Model_Mobile {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getData($fParam,$sortBy = null,$sortDir = null,$filter = null)
    {
		$select = $this->_db->select()
					        ->from(array('A' => 'T_PSLIP'),array('PS_NUMBER','PS_UPDATED'))
					        ->where("PS_NUMBER=?", 'aa'); 
		// print_r($fParam);
		// if($filter == 'Set Filter')
		// {  			
	 //        if($fParam['fcode'] != '')  $select->where('CODE LIKE '.$this->_db->quote('%'.$fParam['fcode'].'%'));
	 //        if(isset($fParam['fid'])) if(!empty($fParam['fid'])) $select->where('UPPER(ID) LIKE '.$this->_db->quote('%'.strtoupper($fParam['fid']).'%'));
	 //        if(isset($fParam['fdescription'])) if(!empty($fParam['fdescription'])) $select->where('UPPER(DESCRIPTION) LIKE '.$this->_db->quote('%'.strtoupper($fParam['fdescription']).'%'));
	        
		// }
		if( !empty($sortBy) && !empty($sortDir) )
			// echo $select;die;
			$select->order($sortBy.' '.$sortDir);
       return $this->_db->fetchall($select);
    }
  
    public function getDetail($BANK_ID)
    {
		$data = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('T_PSLIP')) 
									 ->where("PS_NUMBER=?", $BANK_ID)
							);
		
       return $data;
    }
  
    public function insertData($content)
    {
		$this->_db->insert("T_PSLIP",$content);
    }
  
    public function updateData($PRODUCT_ID,$content)
    {
		$whereArr  = array('PS_NUMBER = ?'=>$PRODUCT_ID);
		$this->_db->update('T_PSLIP',$content,$whereArr);
    }
  
    public function deleteData($PRODUCT_ID)
    {
//		$this->_db->delete('M_PRODUCT_TYPE','PRODUCT_ID = ?', $PRODUCT_ID);
		$this->_db->delete('T_PSLIP','PS_NUMBER = '.$this->_db->quote($PRODUCT_ID));
    }
}