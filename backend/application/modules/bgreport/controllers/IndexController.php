<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'Zend/Loader/PluginLoader.php';
require_once 'Zend/Controller/Action/Helper/Abstract.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/Writer/Excel2007.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class bgreport_IndexController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');
		$this->view->systemType = $system_type;

		Application_Helper_General::writeLog('RBGL', 'View Customer BG List');

		$this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

		$fields = array(
			'regno'    		=> array(
				'field' 	=> 'BG_REG_NUMBER',
				'label'	 	=> $this->language->_('Reg No# /  BG No#'),
			),
			'subject'     	=> array(
				'field'    	=> 'BG_SUBJECT',
				'label'    	=> $this->language->_('Subjek'),
			),
			'principal'     => array(
				'field'    	=> 'CUST_NAME',
				'label'    	=> $this->language->_('Principal'),
			),
			'obligee'		=> array(
				'field'    	=> 'RECIPIENT_NAME',
				'label'    	=> $this->language->_('Obligee'),
			),
			'branch'     	=> array(
				'field'   	=> 'BRANCH_NAME',
				'label'    	=> $this->language->_('Bank Cabang'),
			),
			'countertype'	=> array(
				'field'		=> 'COUNTERTYPE',
				'label'    	=> $this->language->_('Jenis Kontra'),
			),
			'changetype'    => array(
				'field'    	=> 'CHANGETYPE',
				'label'    	=> $this->language->_('Change Type'),
			),
			'status'     	=> array(
				'field'    	=> 'STATUS',
				'label'    	=> $this->language->_('Status'),
			)
		);

		$config = Zend_Registry::get('config');
		$BgType = $config["bg"]["status"]["desc"];
		$BgCode = $config["bg"]["status"]["code"];
		$arrStatus = array_combine(array_values($BgCode), array_values($BgType));
		$this->view->arrStatus = $arrStatus;

		$bgClosingTypeCode = $config["bgclosing"]["changetype"]["code"];
		$bgClosingTypeDesc = $config["bgclosing"]["changetype"]["desc"];
		$this->view->arrClosingType = array_combine($bgClosingTypeCode, $bgClosingTypeDesc);

		$BgcomplationType = $config["bg"]["complation"]["desc"];
		$BgcomplationCode = $config["bg"]["complation"]["code"];
		$arrStatusSettlement = array_combine(array_values($BgcomplationCode), array_values($BgcomplationType));
		$this->view->arrStatusSettlement = $arrStatusSettlement;

		$arrChangeType = array(
			'0' => 'New',
			'1' => 'Amendment Isi',
			'2' => 'Amendment Format'
		);
		$this->view->arrChangeType = $arrChangeType;

		$arrWarrantyType = array(
			'1' => 'Cash Collateral',
			'2' => 'Line Facility',
			'3' => 'Asuransi'
		);
		$this->view->arrWarrantyType = $arrWarrantyType;

		//print 
		$caseStatus = "(CASE BG_STATUS ";
		foreach ($arrStatus as $key => $val) {
			$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseStatus .= " END)";

		$caseCounter = "(CASE COUNTER_WARRANTY_TYPE ";
		foreach ($arrWarrantyType as $key => $val) {
			$caseCounter .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseCounter .= " END)";

		$caseType = "(CASE CHANGE_TYPE ";
		foreach ($arrChangeType as $key => $val) {
			$caseType .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseType .= " END)";

		$select = $this->_db->select()
			->from(
				array('A' => 'T_BANK_GUARANTEE'),
				array(
					'A.BG_REG_NUMBER',
					'A.BG_NUMBER',
					'A.BG_SUBJECT',
					'C.CUST_NAME',
					'A.RECIPIENT_NAME',
					'B.BRANCH_NAME',
					'COUNTERTYPE' => $caseCounter,
					'CHANGETYPE' => $caseType,
					'STATUS' => $caseStatus,
					'A.BG_INSURANCE_CODE',
					'A.COUNTER_WARRANTY_TYPE',
					'A.CHANGE_TYPE',
					'A.TIME_PERIOD_END',
					'A.BG_STATUS',
					'COMPLETION_STATUS' => 'A.COMPLETION_STATUS',
					'A.CLOSING_TYPE'
				)
			)
			->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
			->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = A.CUST_ID', array('CUST_NAME'));

		$auth = Zend_Auth::getInstance()->getIdentity();
		if ($auth->userHeadQuarter == "NO") {
			$select->where('B.ID = ?', $auth->userBranchId); // Add Bahri
		}

		// Filter ----------------------------------------------------------
		$this->view->optionWarranty = $this->filterwarranty();
		$this->view->optionBgStatus = $this->filterbgstatus();
		$this->view->optionBranch = $this->filterbranch();
		$this->view->optionPrincipal = $this->filterprincipal();

		$filterlist = array(
			'Subjek'      			=> 'BG_SUBJECT',
			'Nomor BG'    			=> 'BG_NUMBER',
			'Principal'   			=> 'CUST_ID',
			'Obligee'     			=> 'OBLIGEE_NAME',
			'Bank Cabang' 			=> 'BRANCH_NAME',
			'Kontra'      			=> 'COUNTER_WARRANTY_TYPE',
			'Status Bank Garansi'   => 'BG_STATUS',
		);

		$this->view->filterlist = $filterlist;

		$filterArr = array(
			'BG_SUBJECT'            => array('StringTrim', 'StripTags'),
			'BG_NUMBER'             => array('StringTrim', 'StripTags'),
			'CUST_ID'               => array('StringTrim', 'StripTags'),
			'OBLIGEE_NAME'          => array('StringTrim', 'StripTags'),
			'BRANCH_NAME'           => array('StringTrim', 'StripTags'),
			'COUNTER_WARRANTY_TYPE' => array('StringTrim', 'StripTags'),
			'BG_STATUS'             => array('StringTrim', 'StripTags'),
		);

		$dataParam = array(
			'BG_SUBJECT',
			'BG_NUMBER',
			'CUST_ID',
			'OBLIGEE_NAME',
			'BRANCH_NAME',
			'COUNTER_WARRANTY_TYPE',
			'BG_STATUS',
		);

		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}
		}

		$validators = array(
			'BG_SUBJECT'            => array(),
			'BG_NUMBER'             => array(),
			'CUST_ID'               => array(),
			'OBLIGEE_NAME'          => array(),
			'BRANCH_NAME'           => array(),
			'COUNTER_WARRANTY_TYPE' => array(),
			'BG_STATUS'             => array(),
		);

		$options = array('allowEmpty' => true);
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');

		$bgSubject = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
		$bgNumber = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));
		$custId = html_entity_decode($zf_filter->getEscaped('CUST_ID'));
		$obligee = html_entity_decode($zf_filter->getEscaped('OBLIGEE_NAME'));
		$branchName = html_entity_decode($zf_filter->getEscaped('BRANCH_NAME'));
		$counterWarranty = html_entity_decode($zf_filter->getEscaped('COUNTER_WARRANTY_TYPE'));
		$bgStatus = html_entity_decode($zf_filter->getEscaped('BG_STATUS'));

		if ($filter == TRUE) {
			if ($bgSubject) {
				$select->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $bgSubject . '%'));
			}
			if ($bgNumber) {
				$select->where("A.BG_NUMBER  LIKE ?", '%' . $bgNumber . '%');
			}
			if ($custId) {
				$select->where("C.CUST_NAME  LIKE ?", '%' . $custId . '%');
			}
			if ($obligee) {
				$select->where("A.RECIPIENT_NAME  LIKE ?", '%' . $obligee . '%');
			}
			if ($branchName) {
				$select->where("B.BRANCH_NAME  LIKE ?", '%' . $branchName . '%');
			}
			if ($counterWarranty) {
				$select->where("A.COUNTER_WARRANTY_TYPE  LIKE ?", '%' . $counterWarranty . '%');
			}
			if ($bgStatus) {
				$select->where("A.BG_STATUS = ? ", $bgStatus);
			}
		}

		$this->view->filter = $filter;
		$this->view->subject = $bgSubject;
		$this->view->fbgnumb = $bgNumber;
		$this->view->status = $bgStatus;
		// Filter ----------------------------------------------------------
		
		$xls = $this->_getParam('xls');
		$xlsx = $this->_getParam('xlsx');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		if ($csv || $pdf || $this->_request->getParam('print') || $xls || $xlsx) {
			$arr = $this->_db->fetchAll($select);

			foreach ($arr as $key => $val) {
				unset($arr[$key]['TIME_PERIOD_END']);
				unset($arr[$key]['BG_REG_NUMBER']);
				unset($arr[$key]['BG_INSURANCE_CODE']);
				unset($arr[$key]['COUNTER_WARRANTY_TYPE']);
				unset($arr[$key]['CHANGE_TYPE']);
				unset($arr[$key]['BG_STATUS']);
				unset($arr[$key]['COMPLETION_STATUS']);
			}

			$header = Application_Helper_Array::simpleArray($fields, 'label');

			if ($csv) {
				$this->_helper->download->csv($header, $arr, null, $this->language->_('Customer List BG'));
				Application_Helper_General::writeLog('RBGR', 'Download CSV Customer List BG');
			} else if ($pdf) {
				foreach ($arr as $key => $val) {
					unset($arr[$key]['TIME_PERIOD_END']);
					unset($arr[$key]['BG_REG_NUMBER']);
					unset($arr[$key]['BG_INSURANCE_CODE']);
					unset($arr[$key]['COUNTER_WARRANTY_TYPE']);
					unset($arr[$key]['CHANGE_TYPE']);
					unset($arr[$key]['BG_STATUS']);
				}

				$this->_helper->download->pdf($header, $arr, null, $this->language->_('Customer List BG'));
				Application_Helper_General::writeLog('RBGR', 'Download PDF Customer List BG');

			} else if ($this->_request->getParam('print') == 1) {
				$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Customer List BG', 'data_header' => $fields));

			} else if ($xls) {
				$this->_helper->download->xls($header, $arr, null, $this->language->_('Customer List BG Xls'));

			} else if ($xlsx) {
				$bgtypedesc     = $config["bg"]["type"]["desc"];
				$bgtypecode     = $config["bg"]["type"]["code"];
				$arrbgtype = array_combine(array_values($bgtypecode), array_values($bgtypedesc));

				$arrStatusPrint = array(
					'15' => 'Aktif',
					'16' => 'Berakhir',
				);

				$caseUsagePurpose = "(CASE ";
				foreach ($arrbgtype as $key => $val) {
					$caseUsagePurpose .= " WHEN A.USAGE_PURPOSE = '" . $key . "' THEN '" . $val . "'";
				}
				$caseUsagePurpose .= " END)";

				$caseStatusPrint = "(CASE  ";
				foreach ($arrStatusPrint as $key => $val) {
					$caseStatusPrint .= " WHEN A.BG_STATUS = '" . $key . "' THEN '" . $val . "'";
				}
				$caseStatusPrint .= " END)";

				$caseCounter = "(CASE  ";
				foreach ($arrWarrantyType as $key => $val) {
					$caseCounter .= " WHEN A.COUNTER_WARRANTY_TYPE = '" . $key . "' THEN '" . $val . "'";
				}
				$caseCounter .= " END)";

				$headerXlsx = array(
					"Tgl Penerbitan", // COL1
					"Kantor Cabang",  // COL2
					"Jenis Garansi Bank", // COL3
					"Nama Pemohon", // COL4
					"No. Bank Garansi", // COL5
					"No. Seri Kertas", // COL6
					"Pemilik Proyek", // COL7
					"Nama Proyek", // COL8
					"Valuta", // COL9
					"Nilai BG", // COL10
					"Tgl Penjaminan", // COL11
					"Tgl Jatuh Tempo", // COL12
					"Jangka Waktu (hari)", // COL13
					"Tgl Batas Klaim", // COL14
					"Kontra Garansi", // COL15
					"Nama Asuransi", // COL16
					"Jenis Rekening", // COL17
					"No Kontra Garansi", // COL18
					"Tgl Kontra Garansi", // COL19
					"Jaminan / MD", // COL20 
					"Keterangan" // COL21
				);

				$sqldataxlsx = "SELECT
							A.BG_ISSUED AS COL1,
							B.BRANCH_NAME AS COL2,
							A.USAGE_PURPOSE_DESC AS COL3,
							C.CUST_NAME AS COL4,
							A.BG_NUMBER AS COL5,
							GROUP_CONCAT(UPPER(D.PAPER_ID) ORDER BY D.PAPER_ID ASC) AS COL6,
							A.RECIPIENT_NAME AS COL7,
							GROUP_CONCAT(H.DOC_NAME ORDER BY H.DOC_NUMBER ASC) AS COL8,
							I.PS_FIELDVALUE AS COL9,
							A.BG_AMOUNT AS COL10,
							A.TIME_PERIOD_START AS COL11,
							A.TIME_PERIOD_END AS COL12,
							DATEDIFF( A.TIME_PERIOD_END, A.TIME_PERIOD_START )+ 1 AS COL13,
							A.BG_CLAIM_DATE AS COL14,
							" . $caseCounter . " AS COL15,
							E.CUST_NAME AS COL16,
							A.BG_REG_NUMBER AS COL17,
							F.PS_FIELDVALUE AS COL18,
							G.PS_FIELDVALUE AS COL19,
							(
								CASE 
									WHEN A.COUNTER_WARRANTY_TYPE = '1' THEN '100'
									WHEN A.COUNTER_WARRANTY_TYPE = '3' THEN (
										SELECT
											COALESCE(
												(SELECT PS_FIELDVALUE
													FROM T_BANK_GUARANTEE_DETAIL
													WHERE PS_FIELDNAME = 'Marginal Deposit Percentage'
													AND BG_REG_NUMBER = A.BG_REG_NUMBER
													LIMIT 1),
												(SELECT PS_FIELDVALUE
													FROM TEMP_BANK_GUARANTEE_DETAIL
													WHERE PS_FIELDNAME = 'Marginal Deposit Percentage'
													AND BG_REG_NUMBER = A.BG_REG_NUMBER
													LIMIT 1),
												(SELECT PS_FIELDVALUE
													FROM T_BANK_GUARANTEE_DETAIL
													WHERE PS_FIELDNAME = 'Marginal Deposit Percentage'
													AND BG_REG_NUMBER = (
														SELECT BG_REG_NUMBER
														FROM T_BANK_GUARANTEE
														WHERE BG_NUMBER = A.BG_NUMBER_NEW
														LIMIT 1) 
												LIMIT 1)
											)
									)
									ELSE '0'
								END
							) AS COL20,
							" . $caseStatusPrint . " AS COL21,
							A.COUNTER_WARRANTY_TYPE AS COL22
						FROM
							T_BANK_GUARANTEE A
							LEFT JOIN M_BRANCH B ON A.BG_BRANCH = B.BRANCH_CODE
							LEFT JOIN M_CUSTOMER C ON A.CUST_ID = C.CUST_ID
							LEFT JOIN M_PAPER D ON A.BG_NUMBER = D.NOTES
							LEFT JOIN M_CUSTOMER E ON A.BG_INSURANCE_CODE = E.CUST_ID
							LEFT JOIN T_BANK_GUARANTEE_DETAIL F ON A.BG_REG_NUMBER = F.BG_REG_NUMBER AND F.PS_FIELDNAME = 'Counter Guarantee Number'
							LEFT JOIN T_BANK_GUARANTEE_DETAIL G ON A.BG_REG_NUMBER = G.BG_REG_NUMBER AND G.PS_FIELDNAME = 'Counter Guarantee Granted Date'
							LEFT JOIN T_BANK_GUARANTEE_DETAIL I ON A.BG_REG_NUMBER = I.BG_REG_NUMBER AND I.PS_FIELDNAME = 'Currency'
							LEFT JOIN T_BANK_GUARANTEE_UNDERLYING H ON A.BG_REG_NUMBER = H.BG_REG_NUMBER
							WHERE A.BG_REG_NUMBER <> ''
							";

				if ($filter == TRUE) {
					if ($bgSubject) {
						$sqldataxlsx .= "AND A.BG_SUBJECT LIKE " . "'%" . $bgSubject . "%'" . " ";
					}
					if ($bgNumber) {
						$sqldataxlsx .= "AND A.BG_NUMBER LIKE " . "'%" . $bgNumber . "%'" . " ";
					}
					if ($custId) {
						$sqldataxlsx .= "AND C.CUST_NAME LIKE " . "'%" . $custId . "%'" . " ";
					}
					if ($obligee) {
						$sqldataxlsx .= "AND A.RECIPIENT_NAME LIKE " . "'%" . $obligee . "%'" . " ";
					}
					if ($branchName) {
						$sqldataxlsx .= "AND B.BRANCH_NAME LIKE " . "'%" . $branchName . "%'" . " ";
					}
					if ($counterWarranty) {
						$sqldataxlsx .= "AND A.COUNTER_WARRANTY_TYPE LIKE " . "'%" . $counterWarranty . "%'" . " ";
					}
					if ($bgStatus) {
						$sqldataxlsx .= "AND A.BG_STATUS = " . "'" . $bgStatus . "'" . " ";
					}
				}

				if ($auth->userHeadQuarter == "NO") {
					$sqldataxlsx .= " AND B.ID = " . "'" .  $auth->userBranchId . "'" . " ";
				}
				
				$sqldataxlsx .= "GROUP BY A.BG_NUMBER";
				$dataxlsx = $this->_db->fetchAll($sqldataxlsx);

				foreach ($dataxlsx as $key => $value) {
					$dataxlsx[$key]["COL1"] = date('d-m-Y', strtotime($value["COL1"]));
					$dataxlsx[$key]["COL3"] = ucwords(strtolower($value["COL3"]));
					// $dataxlsx[$key]["COL10"] = number_format($value["COL10"], 2);
					$dataxlsx[$key]["COL11"] = date('d-m-Y', strtotime($value["COL11"]));
					$dataxlsx[$key]["COL12"] = date('d-m-Y', strtotime($value["COL12"]));
					$dataxlsx[$key]["COL14"] = date('d-m-Y', strtotime($value["COL14"]));
					$dataxlsx[$key]["COL19"] = $value["COL19"] ? date('d-m-Y', strtotime($value["COL19"])) : '';
					$dataxlsx[$key]["COL20"] = $value["COL20"] ? number_format($value["COL20"], 2) : number_format(0, 2);

					if ($value['COL22'] == 3) {
						$dataxlsx[$key]["COL18"] = "" . $value["COL18"];
						unset($dataxlsx[$key]['COL22']);
						$dataxlsx[$key]['COL17'] = '';
						
					} else {
						$dataxlsx[$key]["COL18"] = "";
						unset($dataxlsx[$key]['COL22']);
					}

					$girodep = $this->_db->select()
						->from(array('TBG' => 'T_BANK_GUARANTEE'), ['*'])
						->joinLeft(array('A' => 'T_BANK_GUARANTEE_SPLIT'), "TBG.BG_REG_NUMBER = A.BG_REG_NUMBER",  array('*'))
						->joinLeft(
							array('B' => 'M_CUSTOMER_ACCT'),
							'B.ACCT_NO = A.ACCT',
							array('B.ACCT_TYPE', 'B.ACCT_NAME', 'B.ACCT_NO')
						)
						->where('TBG.BG_NUMBER = ?', trim($value['COL5']))
						->where('A.ACCT_DESC IS NULL')
						->order('A.AMOUNT DESC')
						->query()->fetchAll();

					$girodep = $girodep[0];

					switch (true) {
						case (($girodep['ACCT_TYPE'] == '20' || strtolower($girodep['ACCT_TYPE']) == 'd')):
							$dataxlsx[$key]['COL17'] = 'GIRO';
							break;
						case (($girodep['ACCT_TYPE'] == '10' || strtolower($girodep['ACCT_TYPE']) == 's')):
							$dataxlsx[$key]['COL17'] = 'SAVING';
							break;
						case (($girodep['ACCT_TYPE'] == '30' || strtolower($girodep['ACCT_TYPE']) == 't')):
							$dataxlsx[$key]['COL17'] = 'DEPOSITO';
							break;
						default:
							$dataxlsx[$key]['COL17'] = '';
							break;
					}
				}

				$this->xlsx($headerXlsx, $dataxlsx, null, $this->language->_('Customer List BG'));
			}
		}

		$select->order('A.BG_ISSUED DESC');
		$select = $select->query()->fetchAll();
		$this->paging($select);

		$arrType = array(
			1 => 'Standard',
			2 => 'Custom'
		);

		$arrLang = array(
			1 => 'Indonesian',
			2 => 'English',
			3 => 'Bilingual'
		);

		$this->view->langArr = $arrLang;
		$this->view->formatArr = $arrType;
		$this->view->fields = $fields;

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[] = $key;
				$whereval[] = $value;
			}
			$this->view->wherecol = $wherecol;
			$this->view->whereval = $whereval;
		}
	}

	public function testAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();


		$this->_helper->download->csv([], [], null, $this->language->_('Signing Stage List') . " - " . date("dMYHis"));
	}

	public function exportcsvAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$bgNumber = $this->_getParam('bgNumber');

		$response = Zend_Controller_Front::getInstance()->getResponse();
		$response->setHeader('Content-Disposition', 'attachment; filename="Customer List BG.csv"');
		$response->setHeader('Content-Type', 'text/csv');

		$select = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
			->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = A.CUST_ID', array('CUST_NAME'))
			->where('A.BG_NUMBER IN (?)', $bgNumber);
		$select = $select->query()->fetchAll();

		$arrWarrantyType = array(
			'1' => 'Full Cover',
			'2' => 'Line Facility',
			'3' => 'Insurance'
		);

		$config        = Zend_Registry::get('config');
		$BgType     = $config["bg"]["status"]["desc"];
		$BgCode     = $config["bg"]["status"]["code"];
		$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

		$output = fopen('php://output', 'w');

		$heading = array(
			'BG Number / Subject',
			'Principal Name',
			'Obligee Name',
			'Bank Branch',
			'Counter Type',
			'Status'
		);

		fputcsv($output, $heading);

		foreach ($select as $row) {
			$subData = [];
			$subData['BG_REG_NUMBER'] = $row['BG_REG_NUMBER'] . ' / ' . $row['BG_SUBJECT'];
			$subData['CUST_NAME'] = $row['CUST_NAME'];
			$subData['RECIPIENT_NAME'] = $row['RECIPIENT_NAME'];
			$subData['BRANCH_NAME'] = $row['BRANCH_NAME'];

			if (empty($row['BG_INSURANCE_CODE'])) {
				$subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']];
			} else {
				$subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']] . ' (' . $row['BG_INSURANCE_CODE'] . ')';
			}

			$subData['TIME_PERIOD_START'] = Application_Helper_General::convertDate($row['TIME_PERIOD_START'], $this->viewDateFormat, $this->defaultDateFormat);
			$subData['TIME_PERIOD_END'] = Application_Helper_General::convertDate($row['TIME_PERIOD_END'], $this->viewDateFormat, $this->defaultDateFormat);
			$subData['BG_STATUS'] = $arrStatus[$row['BG_STATUS']];

			fputcsv($output, $subData);
		}

		fclose($output);
	}

	function filterprincipal()
	{
		$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
		$select_principal = $this->_db->select()
			->from('M_CUSTOMER')
			->order('CUST_NAME ASC')
			->query()->fetchAll();

		foreach ($select_principal as $key => $row) {
			$optHtml .= "<option value='" . $row['CUST_NAME'] . "'>" . $row['CUST_NAME'] . " (". $row['CUST_ID'] .")"."</option>";
		}
		return $optHtml;
	}

	public function filterbranch()
	{
		$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
		$select_branch = $this->_db->select()
			->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
			->order('A.BRANCH_NAME ASC')
			->query()->fetchAll();
		$save_branch = [];

		foreach ($select_branch as $key => $value) {
			$save_branch[$key] = $value["BRANCH_NAME"];
		}
		foreach ($save_branch as $key => $row) {
			$optHtml .= "<option value='" . $row . "'>" . $row . "</option>";
		}

		return $optHtml;
	}

	public function filterwarranty()
	{
		$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
		$warranty = [
			1 => 'Full Cover',
			2 => 'Line Facility',
			3 => 'Asuransi'
		];
		foreach ($warranty as $key => $row) {
			$optHtml .= "<option value='" . $key . "'>" . $row . "</option>";
		}
		return $optHtml;
	}
	
	public function filterbgstatus()
	{
		$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
		$bg_status = [
			15 => 'Aktif',
			16 => 'Berakhir',
		];
		foreach ($bg_status as $key => $row) {
			$optHtml .= "<option value='" . $key . "'>" . $row . "</option>";
		}
		return $optHtml;
	}

	public function xlsx($header, $data, $select, $fileName)
    {

        require_once 'Zend/Layout.php';
        $layout = Zend_Layout::getMvcInstance();
        if ($layout instanceof Zend_Layout) {
            $layout->disableLayout();
        }
        require_once 'Zend/Controller/Action/HelperBroker.php';
        Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNoRender(true);
        $response = Zend_Controller_Front::getInstance()->getResponse();
        $response->setHeader('Content-Type', 'application/force-download');
        $response->setHeader('Content-Type', 'application/octet-stream');
        $response->setHeader('Content-Type', 'application/download');
        $response->setHeader('Content-Description', 'Download File Xlsx');
        $response->setHeader('Content-Disposition', 'attachment; filename="' . basename($fileName) . '.xlsx"');
        $response->setHeader('Content-Transfer-Encoding', 'binary');
        header("Pragma: public");

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->fromArray($header, NULL, 'A1'); // Header
        $objPHPExcel->getActiveSheet()->fromArray($data, NULL, 'A2'); // Data
		
		// format number untuk kolom Jaminan / MD - COL20 & COL10 - nominal BG
		$sheet = $objPHPExcel->getActiveSheet();
		$columnsToFormat = array('T', 'J');
		$numberFormat = '0.00';
		$currencyFormat = '#,##0.00';

		// Atur format angka untuk kolom T dan J
		foreach ($columnsToFormat as $column) {
			$range = $column . '2:' . $column . $sheet->getHighestRow();
			$formatCode = ($column == 'J') ? $currencyFormat : $numberFormat;
			$sheet->getStyle($range)->getNumberFormat()->setFormatCode($formatCode);
		}
		
		// setting auto size width setiap kolom
		$colCount = $sheet->getHighestDataColumn();
		for ($col = 'A'; $col <= $colCount; $col++) {
			$sheet->getColumnDimension($col)->setAutoSize(true);
		}

		$colStart = 'A';
		$colEnd = 'U';

		// Atur garis batas untuk border
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
				),
			),
		);
		$sheet->getStyle($colStart . '1:' . $colEnd . $sheet->getHighestRow())->applyFromArray($styleArray);

		// Warna latar belakang
		$color = 'b0b0b0';
		$sheet->getStyle($colStart . '1:' . $colEnd . '1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$sheet->getStyle($colStart . '1:' . $colEnd . '1')->getFill()->getStartColor()->setRGB($color);

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }
}
