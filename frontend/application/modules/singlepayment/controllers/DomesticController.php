<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'CMD/Beneficiary.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'Crypt/AESMYSQL.php';
class singlepayment_DomesticController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}
		// $this->view->hidetoken = true;

		$selectuser = $this->_db->select()
			->from(array('A' => 'M_USER'));
		$selectuser->where("A.CUST_ID = " . $this->_db->quote($this->_custIdLogin));
		// echo $selectuser;die;
		$this->view->dataact = $selectuser->query()->fetchAll();


		$USE_CONFIRM_PAGE = true;

		$periodicEveryArr = array(
			'1' => $this->language->_('Monday'),
			'2' => $this->language->_('Tuesday'),
			'3' => $this->language->_('Wednesday'),
			'4' => $this->language->_('Thursday'),
			'5' => $this->language->_('Friday'),
			'6' => $this->language->_('Saturday'),
			'7' => $this->language->_('Sunday'),
		);
		$periodicEveryDateArr = range(1, 28);

		$TRANSFER_TYPE = $this->_request->getParam('TRANSFER_TYPE');
		if (empty($TRANSFER_TYPE))
			$TRANSFER_TYPE = 'ONLINE';

		// Set variables needed in view
		$paramSettingID = array(
			'range_futuredate', //'auto_release', 'cut_off_time_inhouse'
			'cut_off_time_skn', 'cut_off_time_rtgs',
			'threshold_rtgs', 'threshold_lld'
		);

		$settings 			= new Application_Settings();
		$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
		$ccyList  			= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'

		//for skn rtgs
		$lldTypeArr  		= $settings->getLLDDOMType();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
		$residentArr 		= array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
		$citizenshipArr		= array("W" => "WNI", "N" => "WNA");

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only


		$futureDate = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);

			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		$this->view->googleauth = true;
		
		$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID','TOKEN_TYPE')
			)
			->where('USER_ID = ?', $this->_userIdLogin)
			->where('CUST_ID = ?', $this->_custIdLogin)
			->limit(1);

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		//var_dump($userOnBehalf);
		//die('here');
		$tokenType = $tokenIdUser['TOKEN_TYPE'];
		if($tokenIdUser['TOKEN_TYPE'] == '6'){
			$this->view->googleauth = true;
			//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
			$settingObj = new Settings();
			$maxtoken = $settingObj->getSetting("max_failed_token");
			$this->view->tokenfail = (int)$maxtoken-1;
			$this->view->divchlcode = true;
			$this->view->token = true;
			$this->view->googleauth = false;
			$random_number = random_int(10000000, 99999999);
			$this->view->chlcode = $random_number;
			if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
				//die;
				$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
				
				
				
				$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
				$this->view->tokenfail = $tokenfail;
			}
			//echo '<pre>';
			//var_dump($params);
			//var_dump($release);die;
			
		}
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];



		$anyValue = '-- ' . $this->language->_('Select City') . ' --';
		$select = $this->_db->select()
			->from(array('A' => 'M_CITY'), array('*'));
		$select->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);

		$cityCodeArr 			= array('' => $anyValue);
		$cityCodeArr 			+= Application_Helper_Array::listArray($arr, 'CITY_CODE', 'CITY_NAME');
		$this->view->cityCodeArr 	= $cityCodeArr;


		$Settings = new Settings();
		$address_mandatory = $Settings->getSettingNew('address_mandatory');
		
		$max_amount_ol = $Settings->getSettingNew('max_amount_ol');
		$min_amount_skn = $Settings->getSettingNew('min_amount_skn');
		$max_amount_skn = $Settings->getSettingNew('max_amount_skn');
		
		$min_amount_rtgs = $Settings->getSettingNew('min_amount_rtgs');
		$max_amount_rtgs = $Settings->getSettingNew('max_amount_rtgs');
		
		$this->view->max_amount_ol = $max_amount_ol;
		$this->view->max_amount_skn = $max_amount_skn;
		
		$this->view->min_amount_skn = $min_amount_skn;
		
		$this->view->min_amount_rtgs = $min_amount_rtgs;
		$this->view->max_amount_rtgs = $max_amount_rtgs;
		
		$this->view->address_mandatory = $address_mandatory;

		// Add by Hamdan
		//$tranferdatetype = $this->_request->getParam('tranferdatetype');
		//$this->view->tranferdatetype = $tranferdatetype;
 
		$date_val	= date('d/m/Y');
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		if($tranferdatetype == 2){
			$PS_EFDATE_VAL = $this->_request->getParam('PS_FUTUREDATE');
		}else{
			$PS_EFDATE_VAL = $this->_request->getParam('PS_EFDATE');
		}
		$this->view->paymentDate = $PS_EFDATE_VAL;
		if ($PS_EFDATE_VAL > $date_val) {
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		} elseif ($PS_EFDATE_VAL == $date_val) {
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		} else {
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}


		$PS_PERIODIC_EVERY = $this->_request->getParam('PERIODIC_EVERY');
		$PS_PERIODIC_EVERY_DATE = $this->_request->getParam('PERIODIC_EVERYDATE');
		$PS_ENDDATE_VAL = ($this->_request->getParam('PS_ENDDATEPERIODIC')) ? $this->_request->getParam('PS_ENDDATEPERIODIC') : date('d/m/Y');
		$this->view->endDatePeriodic = $PS_ENDDATE_VAL;

		$jenisTransfer =  $this->_request->getParam('tranferdatetype');
		$jenisPeriodic =  $this->_request->getParam('tranferdateperiodictype');

		if ($jenisTransfer == '1') {
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		} elseif ($jenisTransfer == '2') {
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		} elseif ($jenisTransfer == '3') {
			$periodictranfer = $this->language->_('Periodic Transfer');
			$this->view->paymentType = $periodictranfer;

			if ($jenisPeriodic == '5') {
				$this->view->periodicValue = $PS_PERIODIC_EVERY;
			} elseif ($jenisPeriodic == '6') {
				$this->view->periodicValue = $PS_PERIODIC_EVERY_DATE;
			}
		}
		// End

		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		if (empty($tranferdatetype)) {
			$tranferdatetype = 1;
		}
		$this->view->tranferdatetype = $tranferdatetype;

		$filter 		= new Application_Filtering();
		//$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($this->_getParam('PS_NUMBER'));
		$this->view->encps_number = $this->_getParam('PS_NUMBER');
		$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);
//		var_dump($PS_NUMBER);die;
		$PS_EFDATE 		= Application_Helper_General::convertDate($this->getCurrentDate());
		$isConfirmPage 	= $this->_getParam('confirmPage');


		if ($this->_getParam('process') == 'back') {
			Zend_Session::namespaceUnset('TW');
		}

		$sessionNamespace = new Zend_Session_Namespace('TW');


		$process 	= $this->_getParam('process');
		$submitBtn 	= ($this->_request->isPost()) ? true : false;

		// print_r($this->_request->getParams());
		// get ps_number data for repair payment
		if (!empty($PS_NUMBER) && !$this->_request->isPost()) {

			$paramList = array(
				"WA" 			=> false,
				"ACCOUNT_LIST" 	=> $this->_accountList,
				"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
			);
			$select   = $CustomerUser->getPayment($paramList, false);	// not distinct, cause text cannot be selected as DISTINCT, it's not comparable
			$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

			if ($TRANSFER_TYPE == 'ONLINE') {
				$select->columns(
					array(
						"tra_message"			=> "T.TRA_MESSAGE",
						"tra_refno"				=> "T.TRA_REFNO",
						"acbenef_email"			=> "T.BENEFICIARY_EMAIL",
						"bank_name"				=> "T.BENEFICIARY_BANK_NAME",
						"bank_code"				=> "T.BANK_CODE",
						"transfer_type"			=> "T.TRANSFER_TYPE",
						//"acbenef_bankname"		=> "T.BENEFICIARY_ACCOUNT_NAME",
						//"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
					)
				);
			} else {
				$select->columns(
					array(
						"tra_message"			=> "T.TRA_MESSAGE",
						"tra_refno"				=> "T.TRA_REFNO",
						"acbenef_email"			=> "T.BENEFICIARY_EMAIL",
						"acbenef_address"		=> "T.BENEFICIARY_ADDRESS",
						"acbenef_citizenship"	=> "T.BENEFICIARY_CITIZENSHIP",
						"acbenef_resident"		=> "T.BENEFICIARY_RESIDENT",
						"bank_name"				=> "T.BENEFICIARY_BANK_NAME",
						//"bank_city"				=> "T.BENEFICIARY_BANK_CITY",
						"clr_code"				=> "T.CLR_CODE",
						"lld_desc"				=> "T.LLD_DESC",
						"lld_code"				=> "T.LLD_CODE",
						"transfer_type"			=> "T.TRANSFER_TYPE",
						//"acbenef_bankname"		=> "T.BENEFICIARY_ACCOUNT_NAME",
						"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
						"acbenef_cat"				=> "T.BENEFICIARY_CATEGORY",
						"acbenef_id_type"		=> "T.BENEFICIARY_ID_TYPE",
						"acbenef_id_number"		=> "T.BENEFICIARY_ID_NUMBER",
						"acbenef_city_code"		=> "T.BENEFICIARY_CITY_CODE",
					)
				);
			}

			$pslipData 			= $this->_db->fetchRow($select);
			if (!empty($pslipData)) {
				//tambahn pentest
				$PS_EFDATE_ORI  		= date("d/m/Y", strtotime($pslipData['efdate']));
				if ($PS_EFDATE_ORI > date('d/m/Y')) {
					$TransferDate 	=  "2"; //future
					$PS_EFDATE = $PS_EFDATE_ORI;
				} else {
					$TransferDate	=  "1"; //imediate
					$PS_EFDATE = date('d/m/Y');
				}

				$PS_SUBJECT 		= $pslipData['paySubj'];

				$TRA_AMOUNT  		= Application_Helper_General::displayMoney($pslipData['amount']);
				$TRA_MESSAGE 		= $pslipData['tra_message'];

				if ($TRANSFER_TYPE != 'ONLINE') {
					$TRA_REFNO 			= $pslipData['tra_refno'];
					$ACBENEF_ADDRESS  	= $pslipData['acbenef_address'];
					$ACBENEF_CITIZENSHIP = $pslipData['acbenef_citizenship'];
					$ACBENEF_RESIDENT	= $pslipData['acbenef_resident'];
					$CLR_CODE			= $pslipData['clr_code'];
					$LLD_DESC			= $pslipData['lld_desc'];
					$LLD_CODE			= $pslipData['lld_code'];

					$LLD_CATEGORY		= $pslipData['acbenef_cat'];
					$LLD_BENEIDENTIF	= $pslipData['acbenef_id_type'];
					$LLD_BENENUMBER		= $pslipData['acbenef_id_number'];
					$CITY_CODE			= $pslipData['acbenef_city_code'];
				} else {
					$BANK_CODE			= $pslipData['bank_code'];
				}
				
				//echo '<pre>';
				//var_dump($pslipData);die;
				$ACCTSRC 			= $pslipData['accsrc'];
				$ACBENEF  			= $pslipData['acbenef'];
				$ACBENEF_CCY  		= $pslipData['acbenef_ccy'];
				$ACBENEF_BANKNAME	= $pslipData['acbenef_bankname'];
				$ACBENEF_ALIAS  	= $pslipData['acbenef_alias'];
				$ACBENEF_EMAIL  	= $pslipData['acbenef_email'];

				$BANK_NAME			= $pslipData['bank_name'];

				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE'];
				$TRANSFER_TYPE  	= $pslipData['transfer_type'];	// 1, 2

				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"]) {
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				} elseif ($PS_TYPE != $this->_paymenttype["code"]["domestic"]) {
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}

				$trfTypeArr = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);	// Array([2] => SKN, [1] => RTGS)
				$TRANSFER_TYPE = $trfTypeArr[$TRANSFER_TYPE];	// SKN, RTGS

				if (!empty($LLD_CODE)) {
					$LLD_CODEarr = Application_Helper_General::stringFormatToArray($LLD_CODE);
					$LLD_IDENTICAL 			= (!empty($LLD_CODEarr["ID"])) ? $LLD_CODEarr["ID"] : "";
					$LLD_RELATIONSHIP 		= (!empty($LLD_CODEarr["TR"])) ? $LLD_CODEarr["TR"] : "";
					$LLD_PURPOSE 			= (!empty($LLD_CODEarr["TP"])) ? $LLD_CODEarr["TP"] : "";
					$LLD_DESCRIPTION 		= (!empty($LLD_CODEarr["TD"])) ? $LLD_CODEarr["TD"] : "";
					$LLD_SENDERIDENTIF 		= (!empty($LLD_CODEarr["ST"])) ? $LLD_CODEarr["ST"] : "";
					$LLD_SENDERNUMBER 		= (!empty($LLD_CODEarr["SN"])) ? $LLD_CODEarr["SN"] : "";
				}
			} else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
		}

		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP')) {
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}

		//tambahn pentest
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;

		if (!empty($pslipData)) {
			if ($TransferDate == '1') {
				$this->view->PS_EFDATEFUTURE = $futureDate;
			} elseif ($tranferdatetype == '3') {
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII; //						
			} else {
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			}
		} else {
			$this->view->PS_EFDATEFUTURE = $futureDate;
		}

		//$TRANSFER_TYPE ='SKN';
		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
		//
		if ($this->_request->isPost() ) {
			//var_dump($process);die; 
			// print_r($this->_request->getParams());die;
			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');

			if ($this->_getParam('randomTransact') == $sessionNameRand->randomTransact) {
				try {
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					$blocksize = 256;  // can be 128, 192 or 256
					$ACBENEF =  SGO_Helper_AES::decrypt($this->_request->getParam('ACBENEF'), $passwordRand, $blocksize);
					$ACCTSRC =  SGO_Helper_AES::decrypt($this->_request->getParam('ACCTSRC'), $passwordRand, $blocksize);
					$TRA_AMOUNT =  SGO_Helper_AES::decrypt($this->_request->getParam('TRA_AMOUNT'), $passwordRand, $blocksize);
				} catch (Exception $e) {
					$ACBENEF = '';
					$ACCTSRC = '';
					$TRA_AMOUNT = '';
				}
				$SAVEBENE 			= $this->_getParam('save_bene');
				$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PS_SUBJECT'), "PS_SUBJECT");
				$PS_EFDATE 			= $filter->filter($this->_request->getParam('PS_EFDATE'), "PS_DATE");
				$TRA_AMOUNT 		= $filter->filter($TRA_AMOUNT, "AMOUNT");
				$TRA_MESSAGE 		= $filter->filter($this->_request->getParam('TRA_MESSAGE'), "TRA_MESSAGE");
				$ACCTSRC 			= $filter->filter($ACCTSRC, "ACCOUNT_NO");
				$ACBENEF 			= $filter->filter($ACBENEF, "ACCOUNT_NO");
				$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
				$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
				$ACBENEF_EMAIL 		= $filter->filter($this->_request->getParam('ACBENEF_EMAIL'), "EMAIL");
				$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");

				$ACBENEF_CITIZEN 		= $filter->filter($this->_request->getParam('ACBENEF_CITIZENSHIP_SELECT'), "SELECTION");
				$ACBENEF_RESIDENT 		= $filter->filter($this->_request->getParam('ACBENEF_RESIDENT_SELECT'), "SELECTION");
				$ACBENEF_CATEGORY 		= $filter->filter($this->_request->getParam('LLD_CATEGORY_SELECT'), "SELECTION");
				$ACBENEF_IDTYPE 		= $filter->filter($this->_request->getParam('LLD_BENEIDENTIF_SELECT'), "SELECTION");

				$ACBENEF_CITY 		= $filter->filter($this->_request->getParam('CITY_CODE_SELECT'), "SELECTION");


				$BANK_NAME			= $filter->filter($this->_request->getParam('BANK_NAME'), "BANK_NAME");

				$TRANSFER_TYPE 		= $filter->filter($this->_request->getParam('TRANSFER_TYPE'), "SELECTION");

				if ($TRANSFER_TYPE == 'ONLINE') {
					$BANK_CODE			= $filter->filter($this->_request->getParam('BANK_CODE'), "BANK_CODE");
					$TRANSFER_FEE 		= $filter->filter($this->_request->getParam('TRANSFER_FEE'), "TRANSFER_FEE");
					$TRANSFER_FEE_num 	= Application_Helper_General::convertDisplayMoney($TRANSFER_FEE);
				} else {
					$TRANSFER_FEE 		= $filter->filter($this->_request->getParam('TRANSFER_FEE'), "TRANSFER_FEE");
					$TRA_REFNO 			= $filter->filter($this->_request->getParam('TRA_REFNO'), "TRA_REFNO");

					$ACBENEF_ADDRESS	= $filter->filter($this->_request->getParam('ACBENEF_ADDRESS'), "ADDRESS");

					$ACBENEF_RESIDENT = $filter->filter($this->_request->getParam('ACBENEF_RESIDENT'), "SELECTION");
					$this->view->ACBENEF_RESIDENT = (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT	: 'R';

					$ACBENEF_CITIZENSHIP = $filter->filter($this->_request->getParam('ACBENEF_CITIZENSHIP'), "SELECTION");
					$this->view->ACBENEF_CITIZENSHIP = (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: 'W';

					$CITY_CODE = $filter->filter($this->_request->getParam('CITY_CODE'), "SELECTION");
					$this->view->CITY_CODE = (isset($CITY_CODE))	? $CITY_CODE	: '';


					$CLR_CODE			= $filter->filter($this->_request->getParam('CLR_CODE'), "BANK_CODE");
					$LLD_CATEGORY 		= $filter->filter($this->_request->getParam('LLD_CATEGORY'), "LLD_CODE");
					$LLD_IDENTICAL 		= $filter->filter($this->_request->getParam('LLD_IDENTICAL'), "LLD_CODE");
					$LLD_RELATIONSHIP 	= $filter->filter($this->_request->getParam('LLD_RELATIONSHIP'), "LLD_CODE");
					$LLD_PURPOSE 		= $filter->filter($this->_request->getParam('LLD_PURPOSE'), "LLD_CODE");
					$LLD_DESCRIPTION 	= $filter->filter($this->_request->getParam('LLD_DESCRIPTION'), "LLD_DESC");
					$LLD_BENEIDENTIF 	= $filter->filter($this->_request->getParam('LLD_BENEIDENTIF'), "LLD_CODE");
					$LLD_BENENUMBER 	= $filter->filter($this->_request->getParam('LLD_BENENUMBER'), "LLD_CODE");
					$LLD_SENDERIDENTIF 	= $filter->filter($this->_request->getParam('LLD_SENDERIDENTIF'), "LLD_CODE");
					$LLD_SENDERNUMBER 	= $filter->filter($this->_request->getParam('LLD_SENDERNUMBER'), "LLD_CODE");

					$TRANSFER_FEE_num 	= Application_Helper_General::convertDisplayMoney($TRANSFER_FEE);
				}


				$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

				$PERIODIC_EVERY 	= $filter->filter($this->_request->getParam('PERIODIC_EVERY'), "SELECTION");
				$PERIODIC_EVERYDATE = $filter->filter($this->_request->getParam('PERIODIC_EVERYDATE'), "SELECTION");
				$TrfDateType 		= $filter->filter($this->_request->getParam('tranferdatetype'), "SELECTION");
				$TrfPeriodicType 	= $filter->filter($this->_request->getParam('tranferdateperiodictype'), "SELECTION");
				$PS_ENDDATE			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC'), "PS_DATEPERIODIC");

				$PS_EVERY_PERIODIC_UOM 	= $TrfPeriodicType;
				$START_DATE 			= $filter->filter(date('d/m/Y'), "PS_DATE");
				$EXPIRY_DATE 			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC'), "PS_DATE");

				$TRA_NOTIF		= $this->_request->getParam('notif');
				$TRA_EMAIL		= $this->_request->getParam('email_notif');
				$TRA_SMS		= $this->_request->getParam('sms_notif');
				$BENE_ID		= $this->_request->getParam('BENE_ID');

				$filter->__destruct();
				unset($filter);
 
				// post submit payment
				if ($submitBtn && $process != 'back') {
					// die('here');

					

						// NextDate			

						if ($TrfDateType=='3'){
											
						$repetition = $this->_getParam('repetition');
						$efDate = $this->_getParam('PS_EFDATE1');
						$PS_EFDATE 			= $this->_request->getParam('PS_EFDATE1');
						//daily
						if ($repetition == 1) {
							$repeatOn = $this->_getParam('report_day');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
						}
						//weekly
						else if ($repetition == 2) {
							$repeatOn = $this->_getParam('report_day');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

							if (empty($repeatOn)) {
								$this->view->error = true;
								$confirmPage = false;
								$this->view->error_msg = 'Repeat On Be Selected';
							}
						}
						//monthly
						else if ($repetition == 3) {
							$repeatEvery = $this->_getParam('selectrepeat');
							// $repeatOn = $this->_getParam('PS_REPEATON');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

							if (empty($repeatEvery)) {
								$this->view->error = true;
								$confirmPage = false;
								$this->view->error_msg = 'Repeat Every Must Be Selected';
							}
							// if (empty($repeatOn)) {
							// 	$this->view->error = true;
							// 	$confirmPage = false;
							// 	$error_msg[] = 'Repeat On Be Selected';
							// }
						}

						if (empty($endDate)) {
							$this->view->error = true;
							$confirmPage = false;
							$this->view->error_msg = 'End Date Must Be Selected';
						}

						//periodic
						$this->view->efDate = $efDate;
						$this->view->repetition = $repetition;
						$this->view->endDate = $endDate;
						$this->view->repeatEvery = $repeatEvery;
						$this->view->repeatOn = $repeatOn;
						$this->view->endDate = $endDate;
						$this->view->repeat_day = $repeatOn;


					}
						/*											
						if ($PS_EVERY_PERIODIC_UOM == 5) { //every day

							$dateNow = mktime(0, 0, 0, date("n"), date("j"), date("Y"));
							$every = (int) $PERIODIC_EVERY;
							$d = date("w", $dateNow);

							$addDay = $every;
							if ($every > $d) {
								$addDay = $addDay;
							} else {
								$addDay = $addDay + 7;
							}
							$nextDate  =  (int) $addDay - (int) $d;
							$NEXT_DATE = date("Y-m-d", strtotime("+$nextDate day"));
							$PERIODIC_EVERY_VAL = $PERIODIC_EVERY;
						} elseif ($PS_EVERY_PERIODIC_UOM == 6) { //every date

							$dateNextMonth = mktime(0, 0, 0, date("n"), date("j") + 1, date("Y"));

							$dateNow = date("j");
							$maxDays = date('t', $dateNextMonth);
							//echo $maxDays; die;
							$every = (int) $PERIODIC_EVERYDATE;

							if ($every > $dateNow) {
								$addMonth = 0;
							} else {
								$addMonth = 1;
							}

							if ($maxDays >=  $every) {
								$every = $every;
							} else {
								$every = $maxDays;
							}

							$nextDate = mktime(0, 0, 0, date("n") + $addMonth, $every, date("Y"));
							$NEXT_DATE = date("Y-m-d", $nextDate);
							$PERIODIC_EVERY_VAL = $PERIODIC_EVERYDATE;
						}
						//echo $NEXT_DATE."<br>";
						$END_DATE = join('-', array_reverse(explode('/', $EXPIRY_DATE)));
						$PS_EFDATE = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
						$PS_EFDATE_ORII = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
						//echo $ND_DATE;
								*/
						/*if ($TrfPeriodicType==5 && empty($PERIODIC_EVERY) ){
							$errorMsg		 		= $this->language->_('Period can not be left blank.');
							$this->view->error 	= true;
							$this->view->ERROR_MSG	= $errorMsg;
						}elseif ($TrfPeriodicType==6 && empty($PERIODIC_EVERYDATE)){							
							$errorMsg		 		= $this->language->_('Period can not be left blank.');
							$this->view->error 	= true;		
							$this->view->ERROR_MSG	= $errorMsg;				
						}elseif (strtotime($NEXT_DATE) > strtotime($END_DATE)){						
							$errorMsg		 	= $this->language->_('End Date must grather than'). " ". $NEXT_DATE;
							$this->view->error 	= true;		
							$this->view->ERROR_MSG	= $errorMsg;					
						}*/
					//}
					else if($TrfDateType == '2'){
						$PS_EFDATE = $this->_request->getParam('PS_FUTUREDATE');
							if(empty($PS_EFDATE)){
									$PS_EFDATE = $this->_request->getParam('PS_EFDATE');
							}
					} else {
						$PS_EFDATE = $PS_EFDATE;
					}

					//cek date kosong
					if (!$PS_EFDATE) {

						$error_msg		 		= 'Payment Date can not be left blank.';
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $error_msg;
					} elseif ($TrfDateType == '3' && $TrfPeriodicType == 5 && empty($PERIODIC_EVERY)) {
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;

						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					} elseif ($TrfDateType == '3' && $TrfPeriodicType == 6 && empty($PERIODIC_EVERYDATE)) {
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;

						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					} elseif ($TrfDateType == '3' && strtotime($NEXT_DATE) > strtotime($END_DATE)) {
						$errorMsg		 		= $this->language->_('End Date must be greater than') . " " . $NEXT_DATE;
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;

						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					} else {

						$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
						if (!$validateDateFormat->isValid($PS_EFDATE)) {
							$error_msg = 'Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
							$this->view->error 		= true;
							$this->view->ERROR_MSG	= $error_msg;
						} else {
							//new kebutuhan pentest (revisi security)
							if ($isConfirmPage != 1) {

								if ($this->_custSameUser) {

									if (!$this->view->hasPrivilege('PRLP')) {
										// die('here');
										$errMessage = $this->language->_("Error: You don't have privilege to release payment");
										$this->view->error = true;
										$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
									} else {

										$challengeCode		= $this->_getParam('challengeCode');

										$inputtoken1 		= $this->_getParam('inputtoken1');
										$inputtoken2 		= $this->_getParam('inputtoken2');
										$inputtoken3 		= $this->_getParam('inputtoken3');
										$inputtoken4 		= $this->_getParam('inputtoken4');
										$inputtoken5 		= $this->_getParam('inputtoken5');
										$inputtoken6 		= $this->_getParam('inputtoken6');
										$inputtoken7 		= $this->_getParam('inputtoken7');
										$inputtoken8 		= $this->_getParam('inputtoken8');

										$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6. $inputtoken7 . $inputtoken8;

									if($tokenType == 6){
						
													$select3 = $this->_db->select()
														 ->from(array('C' => 'M_USER'));
													$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
													$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
													$data2 = $this->_db->fetchRow($select3);
											
													$request = array();
													$request['mod'] = 'prosesCR';
													$request['tokenid'] = $data2['TOKEN_ID'];
													$request['otp'] = $responseCode;
													$request['chl'] = $this->_request->getParam('chlcode');
													
													$request['token'] = 'mcodex';
													$clientUser  =  new SGO_Soap_ClientUser();
													$success = $clientUser->callapi('token',$request);
													$result  = $clientUser->getResult();
													
													if($result != 'CORRECT'){
														$tokenFailed = $CustUser->setLogToken(); //log token activity

														$error = true;
														$this->view->error = true;
														$errorMsg[] = $this->language->_('Invalid Token Auth Code');	//$verToken['ResponseDesc'];
														$this->view->ERROR_MSG =  $this->language->_('Invalid Token Auth Code');

														if ($tokenFailed === true) {
															$this->_redirect('/default/index/logout');
														}
													}else{
														//$step = $step+1;
														$tokensuccess = true;
													}
													
													//die;
										
									}else{


										$HardToken 	= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
										$resHard = $HardToken->verifyHardToken($challengeCode, $responseCode);
										$resultToken = $resHard['ResponseCode'] == '0000';

										if ($resHard['ResponseCode'] != '0000') {
											$tokenFailed = $CustUser->setLogToken(); //log token activity

											$this->view->error = true;
											$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

											if ($tokenFailed === true) {
												$this->_redirect('/default/index/logout');
											}
										}
										
									}
										
										
									}

									// var_dump($sessionNameConfrim);die;

								}
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct = $ACBENEF;
								$sessionNameConfrim->sourceAcct = $ACCTSRC;
								$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
							} else {
								if ($this->_custSameUser) {

									$inputtoken1 		= $this->_getParam('inputtoken1');
									$inputtoken2 		= $this->_getParam('inputtoken2');
									$inputtoken3 		= $this->_getParam('inputtoken3');
									$inputtoken4 		= $this->_getParam('inputtoken4');
									$inputtoken5 		= $this->_getParam('inputtoken5');
									$inputtoken6 		= $this->_getParam('inputtoken6');
									$inputtoken7 		= $this->_getParam('inputtoken7');
									$inputtoken8 		= $this->_getParam('inputtoken8');

									$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6. $inputtoken7 . $inputtoken8;

									// $HardToken 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
									// $resHard = $HardToken->validateOtp($responseCode);
									// $resultToken = $resHard['ResponseCode'] == '0000';
									// // print_r($resHard);die;
									// if ($resHard['ResponseCode'] != '00'){
									// 	$tokenFailed = $CustUser->setLogToken(); //log token activity

									// 	$this->view->error = true;
									// 	$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

									// 	if ($tokenFailed === true)
									// 	{
									// 		$this->_redirect('/default/index/logout');
									// 	}
									// }
								if($tokenType == 6){
						
								$select3 = $this->_db->select()
									 ->from(array('C' => 'M_USER'));
								$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
								$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
								$data2 = $this->_db->fetchRow($select3);
						
								$request = array();
								$request['mod'] = 'prosesOTP';
								$request['tokenid'] = $data2['TOKEN_ID'];
								$request['otp'] = $responseCode;
								//$request['chl'] = $this->_request->getParam('chlcode');
								
								$request['token'] = 'mcodex';
								$clientUser  =  new SGO_Soap_ClientUser();
								$success = $clientUser->callapi('token',$request);
								$result  = $clientUser->getResult();
								
								if($result != 'CORRECT'){
									$tokenFailed = $CustUser->setLogToken(); //log token activity
									
									$error = true;
									$this->view->error = true;
									$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
									$this->view->ERROR_MSG = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

									if ($tokenFailed === true) {
										$this->_redirect('/default/index/logout');
									}
								}else{
									//$step = $step+1;
									$tokensuccess = true;
								}
								
								//die;
					
							}else{
									if (!empty($tokenGoogle)) {

										$pga = new PHPGangsta_GoogleAuthenticator();
										// var_dump($data2['GOOGLE_CODE']);
										$setting 		= new Settings();
										$google_duration 	= $setting->getSetting('google_duration');
										$resultcapca = $pga->verifyCode($tokenGoogle, $responseCode, $google_duration);
										// var_dump($resultcapca);
										//  	var_dump($responseCode);
										//  	var_dump($tokenGoogle);die;
										if ($resultcapca) {
											$resultToken = $resHard['ResponseCode'] == '0000';
										} else {
											$tokenFailed = $CustUser->setLogToken(); //log token activity

											$error = true;
											$this->view->error = true;
											$errorMsg[] = $this->language->_('Invalid Google Auth Code');	//$verToken['ResponseDesc'];
											$this->view->ERROR_MSG =  $this->language->_('Invalid Google Auth Code');

											// if ($tokenFailed === true)
											// {
											// 	$this->_redirect('/default/index/logout');
											// }
										}
									} else {

										$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
										$verToken 	= $Token->verify($challengeCode, $responseCode);

										if ($verToken['ResponseCode'] != '00') {
											$tokenFailed = $CustUser->setLogToken(); //log token activity

											$error = true;
											$this->view->error = true;
											$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
											$this->view->ERROR_MSG =  $this->language->_('Invalid Response Code');

											if ($tokenFailed === true) {
												$this->_redirect('/default/index/logout');
											}
										}
									}
									
								}
									
								}
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct;
								$sessionNameConfrim->sourceAcct;
								$sessionNameConfrim->traAmount;
							}


							$paramPayment = array(
								"CATEGORY" 					=> "SINGLE DOM",
								"FROM" 						=> "F",				// F: Form, I: Import
								"PS_NUMBER"					=> $PS_NUMBER,
								"PS_SUBJECT"				=> $PS_SUBJECT,
								"PS_EFDATE"					=> $PS_EFDATE,
								"_dateFormat"				=> $this->_dateDisplayFormat,
								"_dateDBFormat"				=> $this->_dateDBFormat,
								"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
								"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
								"_createPB"					=> false,								// cannot create PB trx
								"_createDOM"				=> $this->view->hasPrivilege('CDFT'),	// privi CDFT (Create Domestic Fund Transfer)
								"_createREM"				=> false,								// cannot create REM trx
							);


							if (empty($CITY_CODE)) {
								$CITY_CODE = $ACBENEF_CITY;
							}



							if (empty($LLD_BENEIDENTIF)) {
								$LLD_BENEIDENTIF = $ACBENEF_IDTYPE;
							}


							if (empty($LLD_CATEGORY)) {
								$LLD_CATEGORY = $ACBENEF_CATEGORY;
							}

							if (empty($ACBENEF_CITIZENSHIP)) {
								$ACBENEF_CITIZENSHIP = $ACBENEF_CITIZEN;
							}

							$paramTrxArr[0] = array(
								"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
								"TRA_AMOUNT" 				=> $sessionNameConfrim->traAmount,
								"TRA_MESSAGE" 				=> $TRA_MESSAGE,
								"TRA_REFNO" 				=> $TRA_REFNO,
								"ACCTSRC" 					=> $sessionNameConfrim->sourceAcct,
								"ACBENEF" 					=> $sessionNameConfrim->benefAcct,
								"ACBENEF_CCY" 				=> $ACBENEF_CCY,
								"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,

								// for Beneficiary data, except (bene CCY and email), must be passed by reference
								"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
								"ReffId" 					=> &$ReffId,
								"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
								"ACBENEF_CITIZENSHIP" 		=> &$ACBENEF_CITIZENSHIP,		// W:WNI, N: WNA
								"ACBENEF_RESIDENT" 			=> &$ACBENEF_RESIDENT,			// 1/R: RESIDENT, 2/NR: NON-RESIDENT
								"ACBENEF_ADDRESS1" 			=> &$ACBENEF_ADDRESS,
								//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
								//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

								//	"ORG_DIR" 					=> $ORG_DIR,
								"BANK_CODE" 				=> $CLR_CODE,
								//	"BANK_NAME" 				=> $BANK_NAME,
								//	"BANK_BRANCH" 				=> $BANK_BRANCH,
								//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
								//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
								//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,

								"LLD_IDENTICAL" 			=> $LLD_IDENTICAL,
								//	"LLD_CITIZENSHIP"			=> $LLD_CITIZENSHIP,
								"LLD_CATEGORY" 				=> $LLD_CATEGORY,
								"LLD_RELATIONSHIP" 			=> $LLD_RELATIONSHIP,
								"LLD_PURPOSE" 				=> $LLD_PURPOSE,
								"LLD_DESCRIPTION" 			=> $LLD_DESCRIPTION,
								"LLD_BENEIDENTIF" 			=> $LLD_BENEIDENTIF,
								"LLD_BENENUMBER" 			=> $LLD_BENENUMBER,
								"LLD_SENDERIDENTIF" 		=> $LLD_SENDERIDENTIF,
								"LLD_SENDERNUMBER" 			=> $LLD_SENDERNUMBER,
								"CITY_CODE" 				=> $CITY_CODE, //yang akan dikirimkan ke vlink field 3
								"PS_EMAIL"					=> $TRA_EMAIL,
								"PS_NOTIF"					=> $TRA_NOTIF,
								"PS_SMS"					=> $TRA_SMS,
							);

							if ($TRANSFER_TYPE == 'ONLINE') {
								$paramPayment['_createDOM'] = true;

								unset($paramTrxArr[0]['ReffId']);
								unset($paramTrxArr[0]['LLD_BENEIDENTIF']);
								unset($paramTrxArr[0]['LLD_BENENUMBER']);
								unset($paramTrxArr[0]['LLD_SENDERIDENTIF']);
								unset($paramTrxArr[0]['LLD_SENDERNUMBER']);
								unset($paramTrxArr[0]['CITY_CODE']);

								$paramTrxArr[0]['TRA_REFNO'] = "";
								$paramTrxArr[0]['ACBENEF_CITIZENSHIP'] = "";
								$paramTrxArr[0]['ACBENEF_RESIDENT'] = "";
								$paramTrxArr[0]['ACBENEF_ADDRESS1'] = "";
								$paramTrxArr[0]['BANK_CODE'] = $BANK_CODE;

								$paramTrxArr[0]['LLD_IDENTICAL'] = "";
								$paramTrxArr[0]['LLD_CATEGORY'] = "";
								$paramTrxArr[0]['LLD_RELATIONSHIP'] = "";
								$paramTrxArr[0]['LLD_PURPOSE'] = "";
								$paramTrxArr[0]['LLD_DESCRIPTION'] = "";
							}


							if ($isConfirmPage != 1) {
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME = $paramTrxArr[0]['ACBENEF_BANKNAME'];
							} else {
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME;
							}

							if (empty($sessionNameConfrim->SAVEBENE)) {
								$sessionNameConfrim->SAVEBENE = $SAVEBENE;
							}

							// var_dump($paramPayment);
							// var_dump($paramTrxArr);die;
							$resWs = array();
							$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);

							if ($TRANSFER_TYPE != 'ONLINE' && $this->view->error == false)
								$validate->setFlagConfirmPage(($isConfirmPage == 1) ? TRUE : FALSE); //tujuan untuk set supaya jangan manggil inquiry lg di page ke 2
							$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);

							if ($TRANSFER_TYPE != 'ONLINE' && $this->view->error == false) {
								$sourceAccountType = $resWs['accountType'];
								$infoWarning = $resWs['infoHoliday']['infoWarning']; //permintaan mayapada hari libur dikasih warning saja
								$reffId = $paramTrxArr[0]['ReffId'];
								if ($isConfirmPage != 1) {
									$this->view->reffIdSend = $reffId;
									$this->view->sourceAcctTypeGet = $sourceAccountType;
								} else {
									$reffIdGet 		= $this->_getParam('reffId');
									$this->view->reffIdSend = $reffIdGet;

									$sourceAcctTypeGet 		= $this->_getParam('sourceAcctType');
									$this->view->sourceAcctTypeGet = $sourceAcctTypeGet;
								}
							}

							if ($validate->isError() === false && $this->view->error == false)	// payment data is valid
							{
								$payment 		= $validate->getPaymentInfo();

								if ($USE_CONFIRM_PAGE == true && $isConfirmPage == false) {
									$isConfirmPage 	= true;
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
									$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];

									if ($TRANSFER_TYPE == 'ONLINE')
										$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$ACCTSRC]["ACCT_ALIAS"];
									else
										$ACCTSRC_ALIAS = "";

									$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
									$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);

									$validate->__destruct();
									unset($validate);

									require_once 'General/Charges.php';
									$trfType		= $TRANSFER_TYPE;
									
									
									
						
									if ($TRANSFER_TYPE == "ONLINE"){
										$trfType		= 5;
										$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
										$paramCharges 	= array("accsrc" => $sessionNameConfrim->sourceAcct, "transferType" => 'ONLINE');
										$chargesAMT = $chargesObj->getCharges($paramCharges);
									}else{
										//$trfType		= 5;
										$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
										$paramCharges 	= array("accsrc" => $sessionNameConfrim->sourceAcct, "transferType" => $trfType);
										$chargesAMT 	= $chargesObj->getCharges($paramCharges);
									}
									$chargesCCY 	= $ACCTSRC_CCY;
								} else {
									$validate->__destruct();
									unset($validate);

									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];

									$param = array();
									$param['PS_SUBJECT'] 					= $PS_SUBJECT;
									$param['PS_CCY'] 						= $ACCTSRC_CCY;
									$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
									$param['TRA_AMOUNT'] 					= $sessionNameConfrim->traAmount;
									$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;

									$param['SOURCE_ACCOUNT'] 				= $sessionNameConfrim->sourceAcct;
									$param['BENEFICIARY_ACCOUNT'] 			= $sessionNameConfrim->benefAcct;
									$param['BENEFICIARY_ACCOUNT_CCY'] 		= $ACBENEF_CCY;
									$param['BENEFICIARY_ACCOUNT_NAME'] 		= $ACBENEF_BANKNAME;
									$param['BENEFICIARY_ALIAS_NAME'] 		= $ACBENEF_ALIAS;
									$param['BENEFICIARY_BANK_NAME'] 		= $BANK_NAME;
									$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;


									$param['ACBENEF_CITIZEN'] 				= $ACBENEF_CITIZEN;
									$param['ACBENEF_RESIDENT'] 				= $ACBENEF_RESIDENT;
									$param['ACBENEF_CATEGORY'] 				= $ACBENEF_CATEGORY;
									$param['ACBENEF_IDTYPE'] 				= $ACBENEF_IDTYPE;

									$param['ACBENEF_CITY']					= $ACBENEF_CITY;




									$param['TRANSFER_TYPE']		 			= $TRANSFER_TYPE;
									$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
									$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
									$param['PS_SAVEBENE']					= $sessionNameConfrim->SAVEBENE;
									$param['TRANS_TYPE']					= $TRANSFER_TYPE;

									if ($TRANSFER_TYPE != 'ONLINE') {
										$param['BENEFICIARY_ADDRESS'] 			= $ACBENEF_ADDRESS;
										$param['BENEFICIARY_CITIZENSHIP'] 		= $ACBENEF_CITIZENSHIP;
										$param['BENEFICIARY_RESIDENT'] 			= $ACBENEF_RESIDENT;
										$param['LLD_CATEGORY']		 			= $LLD_CATEGORY;
										$param['LLD_IDENTICAL']		 			= $LLD_IDENTICAL;
										$param['LLD_RELATIONSHIP']		 		= $LLD_RELATIONSHIP;
										$param['LLD_PURPOSE']		 			= $LLD_PURPOSE;
										$param['LLD_DESCRIPTION']		 		= $LLD_DESCRIPTION;
										$param['LLD_BENEIDENTIF']		 		= $LLD_BENEIDENTIF;
										$param['LLD_BENENUMBER']		 		= $LLD_BENENUMBER;
										$param['LLD_SENDERIDENTIF']		 		= $LLD_SENDERIDENTIF;
										$param['LLD_SENDERNUMBER']		 		= $LLD_SENDERNUMBER;
										$param['CITY_CODE']		 				= $CITY_CODE;

										$param['_priviCreate'] 					= 'CDFT';
										$param['sourceAccountType'] 			= $sourceAcctTypeGet;
										$param['ReffId'] 						= $reffIdGet;
										$param['TRANSFER_FEE'] 					= $TRANSFER_FEE_num;
										$param['TRA_REFNO'] 					= $TRA_REFNO;
										$param['CLR_CODE']		 				= $CLR_CODE;
										$param['TRA_AMOUNTEQ'] = '0';   
										$param['PS_EMAIL'] = $TRA_EMAIL;
										$param['PS_NOTIF'] = $TRA_NOTIF;
										$param['PS_SMS'] = $TRA_SMS;
									} else {
										$param['TRANSFER_FEE'] 					= $TRANSFER_FEE_num;
										$param['_priviCreate'] 					= 'CROL';
										$param['TRA_REFNO'] 					= "";
										$param['CLR_CODE']						= $BANK_CODE;
										$param['TRA_AMOUNTEQ'] = '0'; 
										$param['PS_EMAIL'] = $TRA_EMAIL;
										$param['PS_NOTIF'] = $TRA_NOTIF;
										$param['PS_SMS'] = $TRA_SMS;
									}

									try {
										$SinglePayment = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
										if (!empty($PS_NUMBER)) {
																	$sendProvider->isRepair = true;
																	$sendProvider	 = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
																	$param['isRepair'] = true;
																	$param['PS_NUMBER'] = $PS_NUMBER;
										}

										//ada by Hamdan for Periodic
										if ($TrfDateType == 3) {



											if ($repetition == 1) {
												
												//$repeatOn = $sessionNameConfrim->report_day;
												$repeatOn = $this->_getParam('report_day');
												$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
												//$endDate = $sessionNameConfrim->endDate;
												$efDate = $this->_getParam('PS_EFDATE1');

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
												$nextDate = $nextDate->format('Y-m-d');

												$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
											//var_dump($datenumb);die;												
																		
																			//var_dump($efDate);
																			//var_dump($dataday);
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value];
																				//var_dump($string);die;
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																				
																				$nextDatenow = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDatenow = $nextDatenow->format('Y-m-d');
																				$datenumb = date("w", strtotime($nextDatenow));
																				//var_dump($nextDatenow);
																				//var_dump($datenumb);
																				//var_dump($value);
																				//echo '-';
																				if ($datenumb == $value) {
																					$nextDateArr[] = $nextDatenow;
																				//	var_dump($nextDate);die('1');
																					break;

																				}
																				$nextDateArr[] = $nextDate;
																				//var_dump($nextDate);die('2');
																			//	break;
																		//	}
																		//	}
																			
																			
																			
																		}
																		//var_dump($nextDateArr);die;
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																			//if(!empty($nextDateArr['1'])){
																			//	$nextDate = $nextDateArr['1'];
																		//	}else{
																				$nextDate = $nextDateArr['0'];
																		//	}
																			
																			//$param['PS_EFDATE'] = $nextDateArr['0'];
																	}
																	
												
											}
											//weekly
											else if ($repetition == 2) {

												$repeatOn = $this->_getParam('report_day');
												$endDate = $this->_getParam('PS_EFDATE1');

												$repeatEvery = $repeatOn['0'];

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
												$nextDate = $nextDate->format('Y-m-d');

												$dataday = $repeatOn;
												$arrday = array(
													'0' => 'sunday',
													'1' => 'monday',
													'2' => 'tuesday',
													'3' => 'wednesday',
													'4' => 'thursday',
													'5' => 'friday',
													'6' => 'saturday'
												);

												// get number of day in a week of startdate
												$datenumb = date("w", strtotime($nextDate));

												if (!empty($dataday)) {
													foreach ($dataday as $key => $value) {

														if ($datenumb == $value) {
															$nextDate = $nextDate;
															break;
														}
														else {
															$string = 'next ' . $arrday[$dataday[0]];
															// var_dump($string);die();
															$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
															$nextDate->modify($string);
															$nextDate = $nextDate->format('Y-m-d');

															break;
														}
													}
												}
											}
											//monthly
											else if ($repetition == 3) {
												$repeatEvery = $this->_getParam('selectrepeat');
												// $repeatOn = $this->_getParam('PS_REPEATON');
												$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
												//$repeatEvery = $sessionNameConfrim->repeatEvery;
												//$endDate = $sessionNameConfrim->endDate;

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $this->_getParam('PS_EFDATE1'));
												$nextDate = $nextDate->format('Y-m-d');

												// get date of startdate
												$datenumb = date("d", strtotime($nextDate));
											
												if($repeatEvery == 'last'){
												
												$nextDate = date("Y-m-t", strtotime($nextDate));
												
												//die;
												}else{
												
													if ($datenumb == $repeatEvery) {
														$nextDate = $nextDate;
													}
													else if($datenumb < $repeatEvery){
														$nextDateExplode = explode('-', $nextDate);

														$nextDateExplode[2] = $repeatEvery;

														$nextDate = implode('-', $nextDateExplode);
													}
													else if($datenumb > $repeatEvery) {

														$nextDateExplode = explode('-', $nextDate);

														$nextDateExplode[2] = $repeatEvery;

														$nextDate = implode('-', $nextDateExplode);

														$nextDate = date('Y-m-d', strtotime("+1 month", strtotime($nextDate)));
													}
												}
											}else{
											
											$nextDate = $PS_EFDATE;
										}

										if ($TrfDateType == 3) {
											// echo 'here';
											//$efDate = $this->_getParam('PS_EFDATE1');
											$start = $this->_getParam('PS_EFDATE1');
											$STR_DATE = DateTime::createFromFormat('d/m/Y', $start);
											$START_DATE = $STR_DATE->format('Y-m-d');

											$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $this->_getParam('PS_ENDDATEPERIODIC'));
											$expDate = $EXPIRY_DATE->format('Y-m-d');
 
											if (!empty($TrfDateType == 3 && !empty($this->_getParam('PS_ENDDATEPERIODIC')))) {
												$EF_DATE = DateTime::createFromFormat('d/m/Y', $this->_getParam('PS_ENDDATEPERIODIC'));
												$endDate = $EF_DATE->format('Y-m-d');
												// var_dump($endDate);die;
											}

											if (empty($this->_getParam('PS_ENDDATEPERIODIC'))) {
												$endDate = join('-', array_reverse(explode('/', date('d/m/Y'))));
												$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
												$endDate = $EF_DATE->format('Y-m-d');
											}
										}

											// Insert T_PERIODIC
											//										$param['PS_PERIODIC_TRANSFER'] = 1;
											$START_DATE = join('-', array_reverse(explode('/', $START_DATE)));
											$EXPIRY_DATE = join('-', array_reverse(explode('/', $EXPIRY_DATE)));
											//$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
											//$endDate = $EF_DATE->format('Y-m-d');
											
											$insertPeriodic = array(
												'PS_EVERY_PERIODIC' 	=> (int) $repeatEvery,
												'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
												'PS_EVERY_PERIODIC_UOM' => $repetition, // 1: daily, 2: weekly, 3: monthly, 4: yearly, 5: every day of, 6: every date of
												'PS_PERIODIC_STARTDATE' => $START_DATE,
												'PS_PERIODIC_ENDDATE'	=> $endDate,
												'PS_PERIODIC_NEXTDATE'	=> Application_Helper_General::convertDate($nextDate, $this->_dateDBFormat, $this->_dateDisplayFormat),
												'PS_PERIODIC_STATUS' 	=> 2,	// 2 INPROGRESS KALO BELUM BERAKHIR, 1 COMPLETE KALO SUDAH HABIS END DATE, 0 PERIODIC INI DI CANCEL
												'USER_ID' 				=> $this->_userIdLogin,
												'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
											);
											//Zend_Debug::dump($insertPeriodic); die;
											$this->_db->insert('T_PERIODIC', $insertPeriodic);
											$psPeriodicID =  $this->_db->lastInsertId();
											$param['PS_PERIODIC'] = $psPeriodicID;


											// Insert T_PERIODIC_DETAIL
											$select	= $this->_db->select()
												->from(
													array('A'	 		=> 'M_CUSTOMER_ACCT'),
													array(
														'ACCT_NAME' 	=> 'A.ACCT_NAME',
														'ACCT_ALIAS' 	=> 'A.ACCT_ALIAS_NAME',
														'ACCT_CCY' 	=> 'A.CCY_ID',
													)
												)
												->where("A.CUST_ID 			 = ?", $this->_custIdLogin)
												->where("A.ACCT_NO		 	 = ?", $param['SOURCE_ACCOUNT']);

											$accsrc = $this->_db->fetchRow($select);

											$sourceAccountName 		=  $accsrc['ACCT_NAME'];
											$sourceAccountAliasName =  $accsrc['ACCT_ALIAS'];
											$sourceAccountCCY 		=  $accsrc['ACCT_CCY'];

											//Zend_Debug::dump($param); die;
											$insertPeriodicDetail = array(
												'PS_PERIODIC' 				=> $psPeriodicID,
												'SOURCE_ACCOUNT' 			=> $param['SOURCE_ACCOUNT'], // 1: daily, 2: weekly, 3: monthly, 4: yearly, 5: every day of, 6: every date of
												'SOURCE_ACCOUNT_CCY' 		=> $sourceAccountCCY,
												'SOURCE_ACCOUNT_NAME' 		=> $sourceAccountName,
												'SOURCE_ACCOUNT_TYPE' 		=> $param['sourceAccountType'],
												'SOURCE_ACCOUNT_BANK_CODE' 	=> "",
												'SOURCE_ACCOUNT_BANK_NAME' 	=> "",
												'BENEFICIARY_ACCOUNT' 		=> $param['BENEFICIARY_ACCOUNT'],
												'BENEFICIARY_ACCOUNT_CCY' 	=> $param['BENEFICIARY_ACCOUNT_CCY'],
												'BENEFICIARY_ACCOUNT_NAME' 	=> $param['BENEFICIARY_ACCOUNT_NAME'],
												'BENEFICIARY_EMAIL' 		=> $param['BENEFICIARY_EMAIL'],
												'BENEFICIARY_BANK_CODE' 	=> "",
												'BENEFICIARY_BANK_NAME' 	=> "",
												'TRA_AMOUNT' 				=> $param['TRA_AMOUNT'],
												'TRA_MESSAGE' 				=> $param['TRA_MESSAGE'],
												'TRANSFER_TYPE' 			=> 0,	// 0 : Inhouse, 1: RTGS, 2: SKN														
											);
											$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

											$report_day = $this->_getParam('report_day');
											//var_dump($data);die;
											if (!empty($report_day)) {

												$filter_day = array_unique($report_day);
												foreach ($filter_day as $keyday => $valday) {
													$insertPeriodicday = array(
														'PERIODIC_ID' => $psPeriodicID,
														'DAY_ID'		=> $valday
													);

													$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
												}
											}
											
										}
										// End

										if($TrfDateType == 3){
											$param['PS_EFDATE'] 	= Application_Helper_General::convertDate($nextDate, $this->_dateDBFormat, $this->_dateDisplayFormat);
										}
										//echo '<pre>';
										//var_dump($param);die;
										$result = $SinglePayment->createPaymentDomestic($param);

										$ns = new Zend_Session_Namespace('FVC');
										//$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';

										if (!empty($PS_NUMBER))
											$ns->backURL = '/paymentworkflow/requestrepair/index/m/1';
										else
											$ns->backURL = '/' . $this->view->modulename . '/' . $this->view->controllername . '/index';


										//	print_r($result);die;
										if ($result) {
											if (!empty($sessionNameConfrim->SAVEBENE)) {




												$content 						= array();
												$content['BENEFICIARY_ACCOUNT'] = $param['BENEFICIARY_ACCOUNT'];
												$content['BENEFICIARY_NAME'] = $param['BENEFICIARY_ACCOUNT_NAME'];
												$content['CURR_CODE'] =  $param['BENEFICIARY_ACCOUNT_CCY'];
												$content['BENEFICIARY_RESIDENT'] = $param['ACBENEF_RESIDENT'];
												$content['BENEFICIARY_ID_NUMBER'] = $param['LLD_BENENUMBER'];
												$content['BENEFICIARY_CITIZENSHIP'] = $param['ACBENEF_CITIZEN'];
												$content['BENEFICIARY_ID_TYPE'] = $param['ACBENEF_IDTYPE'];
												$content['BENEFICIARY_CATEGORY'] = $param['ACBENEF_CATEGORY'];
												$content['BENEFICIARY_EMAIL'] = $param['BENEFICIARY_EMAIL'];
												$content['BENEFICIARY_ALIAS'] = $param['BENEFICIARY_ALIAS_NAME'];
												$content['BENEFICIARY_CREATED'] = new Zend_Db_Expr("now()");
												$content['BENEFICIARY_CREATEDBY'] = $this->_userIdLogin;
												$content['BENEFICIARY_UPDATED'] = new Zend_Db_Expr("now()");
												$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
												$content['BENEFICIARY_SUGGESTED'] = new Zend_Db_Expr("now()");
												$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
												$content['CUST_ID'] 			  = $this->_custIdLogin;
												$content['BANK_CITY']			  = $param['ACBENEF_CITY'];
												if ($param['TRANS_TYPE'] != 'ONLINE') {
													$content['BENEFICIARY_TYPE'] = '2';
												} else {
													$content['BENEFICIARY_TYPE'] = '8';
												}

												$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
												$content['USER_ID_LOGIN'] = $this->_userIdLogin;
												$content['CUST_ID_LOGIN'] = $this->_custIdLogin;
												// $content['PS_NUMBER'] = $result;
												// print_r($content);die;

												$selectUsers = $this->_db->select()
													->from(array('MAB' => 'M_BENEFICIARY'))
													->join(array('MABG' => 'M_BENEFICIARY_USER'), 'MAB.BENEFICIARY_ID = MABG.BENEFICIARY_ID', array('MAB.BENEFICIARY_ACCOUNT'))
													->where('MABG.USER_ID = ?', $this->_userIdLogin)
													->where('MABG.CUST_ID = ?', $this->_custIdLogin)
													->where('MAB.BENEFICIARY_ACCOUNT = ?', $param['BENEFICIARY_ACCOUNT'])
													// echo $selectUser;die;
													->query()->fetchall();
												// print_r($selectUsers);die;
												if (empty($selectUsers)) {
													$Beneficiary = new Beneficiary();
													// $content
													// $add = $Beneficiary->add($content);	
												}
											}

											if ($this->_custSameUser) {
												//		die('sini');
												$paramSQL = array(
													"WA" 				=> false,
													"ACCOUNT_LIST" 	=> $this->_accountList,
													"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
												);

												// get payment query
												$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
												$select   = $CustUser->getPayment($paramSQL);
												$select->where('P.PS_NUMBER = ?', (string) $result);
												// echo $select;
												$pslip = $this->_db->fetchRow($select);
												$settingObj = new Settings();
												$setting = array(
													"COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
													"COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
													"COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
													"COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
													"COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
													'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
													"range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
													"auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
													"_dateFormat" 			=> $this->_dateDisplayFormat,
													"_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
													"_transfertype" 		=> array_flip($this->_transfertype["code"]),
												);

												$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
												$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

												$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
												$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

												$app = Zend_Registry::get('config');
												$appBankname = $app['app']['bankname'];

												$selectTrx = $this->_db->select()
													->from(
														array('TT' => 'T_TRANSACTION'),
														array(
															'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
															'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
															'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
															//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
															'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
															'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
															'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
															'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
															'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
															'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
															'TRA_REFNO'				=> 'TT.TRA_REFNO',
															'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
															'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
															'TRA_STATUS'			=> 'TT.TRA_STATUS',
															'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
															'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
															'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
															'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
															'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
															'CLR_CODE'				=> 'TT.CLR_CODE',
															'TT.RATE',
															'TT.PROVISION_FEE',
															'TT.NOSTRO_NAME',
															'TT.FULL_AMOUNT_FEE',
															'C.PS_CCY', 'C.CUST_ID',
															'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
															'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
															'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
															'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
															'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
					        																FROM T_PERIODIC_DETAIL Y
					        																inner join T_PSLIP Z
					        																on Y.PS_PERIODIC = Z.PS_PERIODIC
					        																where
					        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
														)
													)
													->joinLeft(array('C' => 'T_PSLIP'), 'C.PS_NUMBER = TT.PS_NUMBER', array())
													->where('TT.PS_NUMBER = ?', $result);
												// echo $selectTrx;
												$paramTrxArr = $this->_db->fetchAll($selectTrx);

												$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $result);
												$paramPayment = array_merge($pslip, $setting);
												// echo '<pre>';
												// print_r($paramPayment);
												// print_r($paramTrxArr);
												// die;
												$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
												$infoWarnOri = (!empty($check['infoWarning']) ? '*) ' . $check['infoWarning'] : '');
												$sessionNameConfrim->infoWarnOri = $infoWarnOri;

												if ($validate->isError() === true) {
													$error = true;
													$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
												}

												$Payment = new Payment($result, $this->_custIdLogin, $this->_userIdLogin);
												// if ($this->_hasPriviReleasePayment){
												$resultRelease = $Payment->releasePayment();
												//print_r($resultRelease);die;
												$this->view->ps_numb = $result;
												$this->view->hidetoken = true;
												if ($resultRelease['status'] == '00') {
													$ns = new Zend_Session_Namespace('FVC');
													$ns->backURL = $this->view->backURL;
													$this->view->releaseresult = true;
													// $this->_redirect('/notification/success/index');
												} else {
													die('here');
													$this->view->releaseresult = false;
													$this->_helper->getHelper('FlashMessenger')->addMessage($result);
													//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
													//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
													$this->_redirect('/notification/index/release');
												}
												// }
											} else {
												//die('cek');
												$this->_redirect('/notification/success/index');
											}
										} else {
											// if(!empty($sessionNameConfrim->SAVEBENE)){




											// 	$content 						= array();
											// 	$content['BENEFICIARY_ACCOUNT'] = $param['BENEFICIARY_ACCOUNT'];
											// 	$content['BENEFICIARY_NAME'] = $param['BENEFICIARY_ACCOUNT_NAME'];
											// 	$content['CURR_CODE'] =  $param['BENEFICIARY_ACCOUNT_CCY'];
											// 	$content['BENEFICIARY_RESIDENT'] = $param['ACBENEF_RESIDENT'];
											// 	$content['BENEFICIARY_ID_NUMBER'] = $param['LLD_BENENUMBER'];
											// 	$content['BENEFICIARY_CITIZENSHIP'] = $param['ACBENEF_CITIZEN'];
											// 	$content['BENEFICIARY_ID_TYPE'] = $param['ACBENEF_IDTYPE'];
											// 	$content['BENEFICIARY_CATEGORY'] = $param['ACBENEF_CATEGORY'];
											// 	$content['BENEFICIARY_EMAIL'] = $param['BENEFICIARY_EMAIL'];
											// 	$content['BENEFICIARY_ALIAS'] = $param['BENEFICIARY_ALIAS_NAME'];
											// 	$content['BENEFICIARY_CREATED'] = new Zend_Db_Expr("now()");
											// 	$content['BENEFICIARY_CREATEDBY'] = $this->_userIdLogin;
											// 	$content['BENEFICIARY_UPDATED'] = new Zend_Db_Expr("now()");
											// 	$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
											// 	$content['BENEFICIARY_SUGGESTED'] = new Zend_Db_Expr("now()");
											// 	$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
											// 	$content['CUST_ID'] 			  = $this->_custIdLogin;

											// 	$content['BANK_CITY']			  = $param['ACBENEF_CITY'];
											// 	if($param['TRANS_TYPE'] != 'ONLINE'){
											// 		$content['BENEFICIARY_TYPE'] = '2';	
											// 	}else{
											// 		$content['BENEFICIARY_TYPE'] = '8';
											// 	}


											// 	$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
											// 	$content['USER_ID_LOGIN'] = $this->_userIdLogin;
											// 	$content['CUST_ID_LOGIN'] = $this->_custIdLogin;
											// 	// $content['PS_NUMBER'] = $result;
											// 	// print_r($content);die;

											// 	 $selectUsers = $this->_db->select()
											//                       ->from(array('MAB'=>'M_BENEFICIARY'))
											//               						 ->join(array('MABG'=>'M_BENEFICIARY_USER'), 'MAB.BENEFICIARY_ID = MABG.BENEFICIARY_ID', array('MAB.BENEFICIARY_ACCOUNT'))
											//                       ->where('MABG.USER_ID = ?', $this->_userIdLogin)
											//                       ->where('MABG.CUST_ID = ?', $this->_custIdLogin)
											//                       ->where('MAB.BENEFICIARY_ACCOUNT = ?', $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_ACCOUNT'])
											//                       // echo $selectUser;die;
											//                       ->query()->fetchall();
											//                       // print_r($selectUsers);die;
											//                       if(empty($selectUsers)){
											//                       		$Beneficiary = new Beneficiary();
											// 			// $content
											// 			$add = $Beneficiary->add($content);	
											//                       }


											// }
											$this->_redirect('/notification/success/index');
										}	// TODO: what to do, if failed create payment
									} catch (Exception $e) {
										Application_Helper_General::exceptionLog($e);
									}
								}
							} else {
								$errorMsg 		= $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

								$validate->__destruct();
								unset($validate);

								if (empty($errMessage)) {
									$errMessage 	= (!empty($errorMsg)) ? $errorMsg : reset(reset(reset($errorTrxMsg)));
								}

								if (!empty($errMessage)) {
									$this->view->error 		= true;
									$this->view->ERROR_MSG	= $errMessage;
								}
							}
						}
					}
				} else {
					//tambahn pentest
					$TRA_AMOUNT  		= $this->_getParam('TRA_AMOUNT');

					$PS_EFDATE_ORI_now  		= $this->_getParam('PS_EFDATE');
					if ($PS_EFDATE_ORI_now > date('d/m/Y')) {
						$TransferDate 	=  "2";
					} else {
						$TransferDate	=  "1";
						$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
					}

					$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
					$isConfirmPage 	= false;
				}
			} else {
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/singlepayment/domestic');
			}
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;

			//tambahn pentest
			if ($tranferdatetype == '1') {
				$this->view->PS_EFDATE 			= $PS_EFDATE;
			} elseif ($tranferdatetype == '3') {
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII; //						
			} else {
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			}
		} else {
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;
		}

		$this->view->infoWarning = $infoWarning;

		$TRA_MESSAGE_len = (isset($TRA_MESSAGE)) ? strlen($TRA_MESSAGE) : 0;
		$TRA_REFNO_len 	 = (isset($TRA_REFNO))  ? strlen($TRA_REFNO)  : 0;

		$TRA_MESSAGE_len = 120 - $TRA_MESSAGE_len;
		$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;

		$settingObj = new Settings();
		$this->view->COT_SKN			= $settingObj->getSetting("cut_off_time_skn", "00:00:00");
		$this->view->COT_RTGS			= $settingObj->getSetting("cut_off_time_rtgs", "00:00:00");
		$this->view->COT_BI				= $settingObj->getSetting("cut_off_time_bi", "00:00:00");
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld", 0);
		$this->view->BENE_ID			= $BENE_ID;
		$this->view->AccArr 			= $AccArr;
		$this->view->transferTypeArr 	= array(
			$this->_transfertype["desc"]["SKN"]  => $this->_transfertype["desc"]["SKN"],
			$this->_transfertype["desc"]["RTGS"] => $this->_transfertype["desc"]["RTGS"],
			'ONLINE' => 'ONLINE'
		);
		$this->view->citizenshipArr 	= $citizenshipArr;
		$this->view->residentArr 		= $residentArr;
		$this->view->lldCategoryArr 	= $lldCategoryArr;
		$this->view->lldIdenticalArr 	= $lldIdenticalArr;
		$this->view->lldRelationshipArr = $lldRelationshipArr;
		$this->view->lldPurposeArr 		= $lldPurposeArr;
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		$this->view->lldSenderIdentifArr  = $lldSenderIdentifArr;

		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		$this->view->PS_EFDATE 			= $PS_EFDATE;

		$this->view->TransferDate		= (isset($TransferDate))		? $TransferDate			: '1';

		if ($PS_NUMBER) {
			$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT))			? $TRA_AMOUNT			: '';
		} else {
			$traamount = Application_Helper_General::displayMoney($sessionNameConfrim->traAmount);
			$this->view->TRA_AMOUNT 		= (isset($traamount))			? $traamount : '';
		}

		$this->view->TRA_MESSAGE 		= (isset($TRA_MESSAGE))			? $TRA_MESSAGE			: '';
		$this->view->TRA_REFNO 			= (isset($TRA_REFNO))			? $TRA_REFNO			: '';
		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;
		$this->view->TRA_REFNO_len		= $TRA_REFNO_len;

		$this->view->ACCTSRC_view		= (isset($ACCTSRC_view))		? $ACCTSRC_view			: '';

		if (!empty($pslipData)) {
			$this->view->ACCTSRC 			= (isset($ACCTSRC))				? $ACCTSRC				: '';
			$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
		} else {
			$this->view->ACCTSRC 			= (isset($sessionNameConfrim->sourceAcct))				? $sessionNameConfrim->sourceAcct				: '';
			$this->view->ACBENEF 			= (isset($sessionNameConfrim->benefAcct))				? $sessionNameConfrim->benefAcct				: '';
		}


		if (!empty($pslipData)) {
			$this->view->ACBENEF_BANKNAME	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
		} else {

			if ($tranferdatetype == 3) {
				$this->view->ACBENEF_BANKNAME	= $this->_getParam('ACBENEF_BANKNAME');
			} else {
				$this->view->ACBENEF_BANKNAME	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
			}
		}
		$this->view->notif 		= (isset($TRA_NOTIF))			? $TRA_NOTIF			: '';
		$this->view->sms_notif 		= (isset($TRA_SMS))			? $TRA_SMS			: '';
		$this->view->email_notif 		= (isset($TRA_EMAIL))			? $TRA_EMAIL			: '';
		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->ACBENEF_EMAIL 		= (isset($ACBENEF_EMAIL))		? $ACBENEF_EMAIL		: '';
		$this->view->CURR_CODE 			= (isset($ACBENEF_CCY))			? $ACBENEF_CCY			: '';
		$this->view->ACBENEF_ADDRESS 	= (isset($ACBENEF_ADDRESS))		? $ACBENEF_ADDRESS		: '';
		$this->view->ACBENEF_CITIZENSHIP = (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: '';
		$this->view->CITY_CODE			= (isset($CITY_CODE))			? $CITY_CODE	: '';

		$this->view->ACBENEF_RESIDENT	= (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT		: '';
		$this->view->BANK_NAME			= (isset($BANK_NAME))			? $BANK_NAME			: '';
		$this->view->BANK_CODE 			= (isset($BANK_CODE))			? $BANK_CODE			: "";

		$this->view->TRANSFER_TYPE 		= (isset($TRANSFER_TYPE))		? $TRANSFER_TYPE		: "ONLINE";
		$this->view->CLR_CODE 			= (isset($CLR_CODE))			? $CLR_CODE				: "";   // "0140601";  // 0000000
		$this->view->LLD_CATEGORY 		= (isset($LLD_CATEGORY))		? $LLD_CATEGORY			: "";
		$this->view->LLD_IDENTICAL 		= (isset($LLD_IDENTICAL))		? $LLD_IDENTICAL		: "";
		$this->view->LLD_RELATIONSHIP 	= (isset($LLD_RELATIONSHIP))	? $LLD_RELATIONSHIP		: "";
		$this->view->LLD_PURPOSE 		= (isset($LLD_PURPOSE))			? $LLD_PURPOSE			: "";
		$this->view->LLD_DESCRIPTION 	= (isset($LLD_DESCRIPTION))		? $LLD_DESCRIPTION		: "";
		$this->view->LLD_BENEIDENTIF 	= (isset($LLD_BENEIDENTIF))		? $LLD_BENEIDENTIF		: "";
		$this->view->LLD_BENENUMBER 	= (isset($LLD_BENENUMBER))		? $LLD_BENENUMBER		: "";
		$this->view->LLD_SENDERIDENTIF 	= (isset($LLD_SENDERIDENTIF))	? $LLD_SENDERIDENTIF	: "";
		$this->view->LLD_SENDERNUMBER 	= (isset($LLD_SENDERNUMBER))	? $LLD_SENDERNUMBER		: "";
		$this->view->CHARGES_AMT 		= (isset($chargesAMT))			? Application_Helper_General::displayMoney($chargesAMT)	: '';
		$this->view->CHARGES_CCY 		= (isset($chargesCCY))			? $chargesCCY			: '';

		$this->view->confirmPage		= $isConfirmPage;
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;

		$this->view->PERIODIC_EVERY 	= (isset($PERIODIC_EVERY))		? $PERIODIC_EVERY		: '0';
		$this->view->PERIODIC_EVERYDATE = (isset($PERIODIC_EVERYDATE))	? $PERIODIC_EVERYDATE	: '0';
		$this->view->TrfDateType 		= (isset($TrfDateType))			? $TrfDateType			: '1';
		$this->view->TrfPeriodicType 	= (isset($TrfPeriodicType))		? $TrfPeriodicType		: ''; //$TrfPeriodicType;

		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;

		if ($TRANSFER_TYPE != 'ONLINE' && $ACBENEF_RESIDENT != 'R') {
			$this->view->LLD_HI = '-';
		} else {
			$this->view->LLD_HI = 'none';
		}

		if ($isConfirmPage == '1') {
		} else {
			Application_Helper_General::writeLog('CDFT', 'Viewing Create Single Payment Domestic');
		}

		/*------------------------------------approval matrix data--------------------------------------------------------*/

		$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;

		/*--------------------------------END approval matrix data------------------------------------*/


		//cek privilage
		$selectpriv = $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'), array('FPRIVI_ID'))
			->where('A.FUSER_ID = ?', (string) $this->_custIdLogin . $this->_userIdLogin)
			->query()->fetchAll();

		foreach ($selectpriv as $key => $value) {
			foreach ($value as $row => $data) {
				if ($data == 'MTSP') {
					$usermtsp = true;
				}
				if ($data == 'UTSP') {
					$userutsp = true;
				}
			}
		}

		if (!empty($usermtsp)) {

			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin);
			$select->where('A.T_TYPE = "2"');
			$select->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		} else if (!empty($userutsp)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin)
				->where('A.T_TARGET LIKE ?', (string) '%' . $this->_userIdLogin . '%')
				->where('A.T_TYPE = "2"')
				->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		}

		if (!empty($usermtsp) && !empty($userutsp)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin);
			$select->where('A.T_TYPE = "2"');
			$select->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		}

		if ($this->view->hasPrivilege('UTSP')) {
			$templatePriv = 1;
		} else {
			$templatePriv = 0;
		}

		if ($this->view->hasPrivilege('CDFT')) {
			$priv = 1;
		} else {
			$priv = 0;
		}

		if ($this->view->hasPrivilege('MTSP')) {
			$usetemp = 1;
		} else {
			$usetemp = 0;
		}

		$this->view->usetemplate = $usetemp;
		$this->view->btntemplatePriv = $priv;
		$this->view->templatePriv = $templatePriv;
			//Recrate Ongoing
			if ($this->_request->getParam('recreate')) {
				$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
				$filter       = new Application_Filtering();
				$AESMYSQL = new Crypt_AESMYSQL();
				$password = $sessionNamespace->token;
				$PS_NUMBER = urldecode($filter->filter($this->_getParam('recreate'), "recrate"));
				$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);
	
				$recreate  = $this->_db->select()
					->from(array('P' => 'T_PSLIP'))
					->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER')
					->where('P.PS_NUMBER = ?', $PS_NUMBER);
				$recreate = $this->_db->fetchRow($recreate);
				// echo '<pre>';
				// var_dump($recreate['TRA_AMOUNT']);die;

				// echo "<pre>";
				// print_r($recreate);die();
	
				$this->view->PS_SUBJECT = $recreate['PS_SUBJECT'];
				$this->view->TRA_AMOUNT = $recreate['TRA_AMOUNT'];
				$this->view->ACCTSRC = $recreate['SOURCE_ACCOUNT'];
				$this->view->ACBENEF = $recreate['BENEFICIARY_ACCOUNT'];
				$this->view->ACBENEF_BANKNAME = $recreate['BENEFICIARY_ACCOUNT_NAME'];
				$this->view->ACBENEF_EMAIL = $recreate['BENEFICIARY_EMAIL'];
				$this->view->TRA_MESSAGE = $recreate['TRA_MESSAGE'];
				if($recreate['TRANSFER_TYPE']=="5"){
					$TRA_TYPE = "ONLINE";
				}elseif ($recreate['TRANSFER_TYPE']=="2") {
					$TRA_TYPE = "SKN";
				}elseif ($recreate['TRANSFER_TYPE']=="1") {
					$TRA_TYPE = "RTGS";
				}else{
					$TRA_TYPE = '';
				}
				$this->view->TRANSFER_TYPE = $TRA_TYPE;

				$this->view->ACBENEF_ADDRESS = $recreate['BENEFICIARY_ADDRESS'];
				$this->view->CITY_CODE = $recreate['BENEFICIARY_CITY_CODE'];
				$this->view->LLD_CATEGORY = $recreate['BENEFICIARY_CATEGORY'];
				$this->view->LLD_BENEIDENTIF = $recreate['BENEFICIARY_ID_TYPE'];
				$this->view->LLD_BENENUMBER = $recreate['BENEFICIARY_ID_NUMBER'];

				$this->view->BANK_CODE = $recreate['BANK_CODE'];
				$this->view->BANK_NAME = $recreate['BENEFICIARY_BANK_NAME'];
				$this->view->CLR_CODE = $recreate['CLR_CODE'];
				$this->view->SWIFT_CODE = $recreate['SWIFT_CODE'];
				$this->view->ACBENEF_ALIAS = $recreate['BENEFICIARY_BANK_CODE'];
				$this->view->ACBENEF_ALIAS = $recreate['BENEFICIARY_ALIAS_NAME'];
	
				$this->view->notif = $recreate['PS_NOTIF'];
				$this->view->email_notif = $recreate['PS_EMAIL'];
				$this->view->sms_notif = $recreate['PS_SMS'];
			}
			/////////////////
	}

	public function templateaddAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$tratype = $this->_getParam('tratype');
		$bene = $this->_getParam('bene');
		$beneaddress = $this->_getParam('beneaddress');
		$citizenship = $this->_getParam('citizenship');
		$nationality = $this->_getParam('nationality');
		$benecategory = $this->_getParam('benecategory');
		$beneidtype = $this->_getParam('beneidtype');
		$beneidnum = $this->_getParam('beneidnum');
		$bank = $this->_getParam('bank');
		$city = $this->_getParam('city');
		$email_domestic = $this->_getParam('email_domestic');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> '2',
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_TRATYPE' 			=> $tratype,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_BENEADDRESS' 		=> $beneaddress,
			'TEMP_CITIZENSHIP' 		=> $citizenship,
			'TEMP_NATIONALITY' 		=> $nationality,
			'TEMP_BENCATEGORY' 		=> $benecategory,
			'TEMP_BENEIDTYPE' 		=> $beneidtype,
			'TEMP_BENEIDNUM' 		=> $beneidnum,
			'TEMP_BANK' 			=> $bank,
			'TEMP_CITY' 			=> $city,
			'TEMP_EMAIL_DOMESTIC' 	=> $email_domestic,

			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_LOCKED'			=> $locked,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}


	public function templaterefreshAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$selectpriv = $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'), array('FPRIVI_ID'))
			->where('A.FUSER_ID = ?', (string) $this->_custIdLogin . $this->_userIdLogin)
			->query()->fetchAll();

		foreach ($selectpriv as $key => $value) {
			foreach ($value as $row => $data) {
				if ($data == 'MTSP') {
					$usermtsp = true;
				}
				if ($data == 'UTSP') {
					$userutsp = true;
				}
			}
		}

		if (!empty($usermtsp)) {

			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin);
			$select->where('A.T_TYPE = "2"');
			$select->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		} else if (!empty($userutsp)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin)
				->where('A.T_TARGET LIKE ?', (string) '%' . $this->_userIdLogin . '%')
				->where('A.T_TYPE = "2"')
				->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		}

		if (!empty($usermtsp) && !empty($userutsp)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin);
			$select->where('A.T_TYPE = "2"');
			$select->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		}
		
		
		 if (!empty($templateArr)) {
                  foreach ($templateArr as $key => $value) {
                    /*print_r($value);die;*/
                    echo '<div class="row">';
                    echo '<div class="alert alert-primary" role="alert" style="width: 90%">';

                    echo '<a href="#" id="edittemplatelink" data-tempid="' . $value['T_ID'] . '" data-tempname="' . $value['T_NAME'] . '" data-tempsubject="' . $value['TEMP_SUBJECT'] . '" data-tempbene="' . $value['TEMP_BENE'] . '" data-tempsource="' . $value['TEMP_SOURCE'] . '" data-tempamount="' . $value['TEMP_AMOUNT'] . '" data-tempnotif="' . $value['TEMP_NOTIF'] . '" data-tempbenename="' . $value['TEMP_BENE_NAME'] . '" data-tempemail="' . $value['TEMP_EMAIL'] . '" data-tempsms="' . $value['TEMP_SMS'] . '" data-templocked="' . $value['TEMP_LOCKED'] . '" data-temptratype="' . $value['TEMP_TRATYPE'] . '" data-tempbeneaddress="' . $value['TEMP_BENEADDRESS'] . '" data-tempcitizenship="' . $value['TEMP_CITIZENSHIP'] . '" data-tempnationality="' . $value['TEMP_NATIONALITY'] . '" data-tempbenecategory="' . $value['TEMP_BENECATEGORY'] . '" data-tempbeneidtype="' . $value['TEMP_BENEIDTYPE'] . '" data-tempbeneidnum="' . $value['TEMP_BENEIDNUM'] . '" data-tempbank="' . $value['TEMP_BANK'] . '" data-tempcity="' . $value['TEMP_CITY'] . '" data-tempemaildomestic="' . $value['TEMP_EMAIL_DOMESTIC'] . '" class="alert-link edittemplatelink">' . $value['T_NAME'] . '</a> <span style="float:right">';

                    echo '</div>';
                    echo '</div>';
                    echo '<script>';
                    echo "$('.edittemplatelink').click(function(){document.getElementById('edittemplatediv').style.display='block',$('.templatelink').show(),$('.closeBtn').css('margin-right','25%'),document.getElementById('edittempname').value=$(this).attr('data-tempname'),document.getElementById('edittempsubject').value=$(this).attr('data-tempsubject'),document.getElementById('edittempbenef').value=$(this).attr('data-tempbene');var e=(t=$(this).attr('data-tempamount')).split(' ');if(e.length>1)var t=e[1],d=e[0];else t=$(this).attr('data-tempamount'),d='';document.getElementById('edittempamount').value=t,document.getElementById('hiddenid').value=$(this).attr('data-tempid'),document.getElementById('hiddenbenename').value=$(this).attr('data-tempbenename'),document.getElementById('locked').value=$(this).attr('data-templocked'),document.getElementById('edittempsourceacct').value=$(this).attr('data-tempsource'),'2'==$(this).attr('data-tempcross')?(document.getElementById('edittempnocross').checked=!0,$('.tempvalas').hide(),$('#currinhouse').show()):(document.getElementById('edittempyescross').checked=!0,$('.tempvalas').show(),$('#currinhouse').hide(),document.getElementById('edittemptranspose').value=$(this).attr('data-temppurpose'),'1'==$(this).attr('data-tempexchange')?document.getElementById('edittempcounterrate').checked=!0:document.getElementById('edittempspecialrate').checked=!0,document.getElementById('edittemprefnum').value=$(this).attr('data-temperefnum'),document.getElementById('edittempcustnum').value=$(this).attr('data-tempcustref'),document.getElementById('edittempccy').value=d),'1'==$(this).attr('data-tempnotif')?(document.getElementById('edittempnonotif').checked=!0,$('#edittemptremailnotif').hide(),$('#edittemptrsmsnotif').hide()):(document.getElementById('edittempyesnotif').checked=!0,$('#edittemptremailnotif').show(),$('#edittemptrsmsnotif').show(),document.getElementById('edittempemailnotif').value=$(this).attr('data-tempemail'),document.getElementById('edittempsmsnotif').value=$(this).attr('data-tempsms'));var m=$(this).attr('data-templocked').split(',');for(i=0;i<m.length-1;i++)if('subject'==m[i])var a=!0;else if('cross'==m[i])var n=!0;else if('source'==m[i])var l=!0;else if('purpose'==m[i])var s=!0;else if('bene'==m[i])var c=!0;else if('amount'==m[i])var o=!0;else if('exrate'==m[i])var u=!0;else if('notif'==m[i])var p=!0;else if('notifexpand'==m[i])var r=!0;document.getElementById('edittempsubject').disabled=a,document.getElementById('edittempnocross').disabled=n,document.getElementById('edittempyescross').disabled=n,document.getElementById('edittempsourceacct').disabled=l,document.getElementById('edittemptranspose').disabled=s,document.getElementById('edittempbenef').disabled=c,document.getElementById('edit_tempaccountadd').disabled=c,document.getElementById('edittempccy').disabled=o,document.getElementById('edittempamount').disabled=o,document.getElementById('edittempcounterrate').disabled=u,document.getElementById('edittempspecialrate').disabled=u,document.getElementById('edittemprefnum').disabled=u,document.getElementById('edittempcustnum').disabled=u,document.getElementById('edittempnonotif').disabled=p,document.getElementById('edittempyesnotif').disabled=p,document.getElementById('edittempemailnotif').disabled=r,document.getElementById('edittempsmsnotif').disabled=r});";
					echo '</script>';
                  }
                } else {
                  echo '<div class="row">';
                  echo '<div class="alert alert-primary" role="alert" style="width: 90%">';
                  echo "<h6>----No Data----</h6>";
                  echo '</div>';
                  echo '</div>';
                }

		// $this->_db->commit();
	}

	public function generateTransactionID(){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(5));
		$trxId = 'PR'.$currentDate.$this->_custIdLogin.$seqNumber;

		return $trxId;
	}
}
