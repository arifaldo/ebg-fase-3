<?php
/**
 * Temprootcommunity model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempspecialcharges extends Changemanagement_Model_Tempchanges {
	protected $_moduleId = 'BGR';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		//query from TEMP_ROOT_COMM
		$select = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_CHARGES_WITHIN')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
									 
        $select2 = $this->dbObj->fetchRow(
									$this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
									 
		
				
		$custid = $select['0']['CUST_ID'];
     
		$selecttemp = $this->dbObj->select()
						->from(array('B' => 'TEMP_CHARGES_WITHIN'),array('*'))
						->where("B.CUST_ID = ? ",$custid);
		$temp = $this->dbObj->fetchAll($selecttemp);
		$duplicate = false;
		if(count($temp>1)){
			$selectdata = $this->dbObj->select()->distinct()
					->from(array('M_CHARGES_WITHIN'),array('CUST_ID'))
					->where("CUST_ID LIKE ?" ,'%SPECIAL%')
				 	-> query() ->fetchAll();
			$totaldata = count($selectdata);
			$totaldata = $totaldata+1;
			$cust_id = 'SPECIAL'.sprintf("%02d", $totaldata);
			//$custid = 'SPECIAL'
			$duplicate = true;
		}
		//var_dump(count($temp));die;

       
	   //die;
	   //echo '<pre>';
       if($select){
	       	//insert to master table M_BPRIVI_GROUP
	       	foreach ($select as $list) {
	       		$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>''));
	       		$insertArr['CHARGES_APPROVED'] = new Zend_Db_Expr('now()');
				$insertArr['CHARGES_APPROVEDBY'] = $actor;
				$insertArr['Column 8'] = '-';
				if($duplicate){
					$insertArr['CUST_ID'] = $cust_id;
				}
				//var_dump($insertArr);
				$assign = $this->dbObj->insert('M_CHARGES_WITHIN',$insertArr);
	       	}
			
       }

       
									 
        $select2 = $this->dbObj->fetchRow(
									$this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									);
		$custid = $select['0']['CUST_ID'];
	
      

       
	   //die; 
	   
	   $selectother = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_CHARGES_OTHER')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
					//echo $select;die;
					//var_dump($selectother);die;
	   
       if($selectother){
	       	foreach ($selectother as $list) {
	       		$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$insertArr['CHARGES_APPROVED'] = new Zend_Db_Expr('now()');
				$insertArr['CHARGES_APPROVEDBY'] = $actor;
				if($duplicate){
					$insertArr['CUST_ID'] = $cust_id;
				}
				if(empty($insertArr['CHARGES_NO'])){
					$insertArr['CHARGES_NO'] = '1';
				}
				
				try{
				$assign = $this->dbObj->insert('M_CHARGES_OTHER',$insertArr);
				}catch(exception $e)
				{
					print_r($e);die;
				}
	       	}
			
       }
		
		//die('here');
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */ 
	public function approveEdit($actor = null) {
		//query from TEMP_ROOT_COMM
		$select = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_CHARGES_WITHIN')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
									 
        $select2 = $this->dbObj->fetchRow(
									$this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
		$custid = $select['0']['CUST_ID'];
     
        //$whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
       $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'BUSINESS_TYPE =?'		=>	'1'); 
       //Zend_Debug::dump($commid); die;
       $assign = $this->dbObj->delete('M_CHARGES_WITHIN',$whereArr);


       
	   //die;
	   //echo '<pre>';
       if($select){
	       	//insert to master table M_BPRIVI_GROUP
	       	foreach ($select as $list) {
	       		$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>''));
	       		$insertArr['CHARGES_APPROVED'] = new Zend_Db_Expr('now()');
				$insertArr['CHARGES_APPROVEDBY'] = $actor;
				$insertArr['Column 8'] = '-';
				//var_dump($insertArr);
				$assign = $this->dbObj->insert('M_CHARGES_WITHIN',$insertArr);
	       	}
			
       }

       
									 
        $select2 = $this->dbObj->fetchRow(
									$this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									);
		$custid = $select['0']['CUST_ID'];
	
       $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'CHARGES_TYPE =?'		=>	'1'); 
       //Zend_Debug::dump($commid); die;
       $assign = $this->dbObj->delete('M_CHARGES_OTHER',$whereArr);
       $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'CHARGES_TYPE =?'		=>	'2'); 
       //Zend_Debug::dump($commid); die;
       $assign = $this->dbObj->delete('M_CHARGES_OTHER',$whereArr);
	   
	   $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'CHARGES_TYPE =?'		=>	'8'); 
       //Zend_Debug::dump($commid); die;
       $assign = $this->dbObj->delete('M_CHARGES_OTHER',$whereArr);

       
	   //die; 
	   
	   $selectother = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_CHARGES_OTHER')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
					//echo $select;die;
					//var_dump($selectother);die;
	   
       if($selectother){
	       	foreach ($selectother as $list) {
	       		$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$insertArr['CHARGES_APPROVED'] = new Zend_Db_Expr('now()');
				$insertArr['CHARGES_APPROVEDBY'] = $actor;
				if(empty($insertArr['CHARGES_NO'])){
					$insertArr['CHARGES_NO'] = '1';
				}
				
				try{
				$assign = $this->dbObj->insert('M_CHARGES_OTHER',$insertArr);
				}catch(exception $e)
				{
					print_r($e);die;
				}
	       	}
			
       }
		
		//die('here');
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate() {
		
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate() {
		
	}
	
	public function approveDelete() {
		//will never be called
		
		$select = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_CHARGES_WITHIN')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
									 
        $selectCurr = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('M_CHARGES_WITHIN')
									 ->where('CUST_ID = ?',$select['0']['CUST_ID'])
									 );
									 
		$custid = $select['0']['CUST_ID'];
		
		
		$rowUpdated = $this->dbObj->update(
						'M_CUSTOMER',
						array(
							'CUST_CHARGESID' => NULL
						),
						$this->dbObj->quoteInto('CUST_CHARGESID = ?', $selectCurr['0']['CHARGES_ID'])
					);
					
					
		$assigndelete = $this->dbObj->delete('M_CHARGES_WITHIN',$this->dbObj->quoteInto('CUST_ID = ?',$select['0']['CUST_ID']));
		$assigndelete = $this->dbObj->delete('M_CHARGES_OTHER',$this->dbObj->quoteInto('CUST_ID = ?',$custid));
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit($actor = null) {

		//delete from TEMP_ROOT_COMM
		$assigndelete = $this->dbObj->delete('TEMP_CHARGES_WITHIN',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$assigndelete = $this->dbObj->delete('TEMP_CHARGES_OTHER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$rootcommunitydelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Backend Group)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteDelete() {
		//will never be called
		$assigndelete = $this->dbObj->delete('TEMP_CHARGES_WITHIN',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$assigndelete = $this->dbObj->delete('TEMP_CHARGES_OTHER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$rootcommunitydelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Backend Group)';
//			return false;
//		}
		
		return true;
	}
}