<?php
require_once 'Zend/Controller/Action.php';

class Eformworkflow_GuarantedtransactionController extends Application_Main
{
    public function indexAction()
    {
        $bg_reg_number = $this->_getParam('bgregnumber');

        // Guaranted Transaction -----------------------

        $getGuarantedTransanctions = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
            ->where('BG_REG_NUMBER = ?', $bg_reg_number)
            ->query()->fetchAll();

        if (!$getGuarantedTransanctions) {
            $getGuarantedTransanctions = $this->_db->select()
                ->from('T_BANK_GUARANTEE_UNDERLYING')
                ->where('BG_REG_NUMBER = ?', $bg_reg_number)
                ->query()->fetchAll();
        }

        $this->view->guarantedTransanctions = $getGuarantedTransanctions;

        $conf = Zend_Registry::get('config');

        $bgdocType         = $conf["bgdoc"]["type"]["desc"];
        $bgdocCode         = $conf["bgdoc"]["type"]["code"];

        $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
        $this->view->arrbgdoc = $arrbgdoc;

        // end Guaranted Transaction -------------------
    }
}
