<?php

require_once 'Zend/Controller/Action.php';

class Corporatedcard_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index'); 
		
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;
	
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
    	
    	$this->view->report_msg = array();

		if($this->_request->isPost())
		{
			
			$filters = array(
							'regid' => array('StringTrim','StripTags'),
							 'cust_card' => array('StringTrim','StripTags'),
							 'edc' => array('StringTrim','StripTags'),
							 'purchase' => array('StringTrim','StripTags')
							);

			$validators = array(
								'regid'      => array('NotEmpty',
													 'messages' => array(
																		 $this->language->_('Can not be empty')
																         )
														),
								'cust_card'      => array('NotEmpty',
													 'messages' => array(
																		 $this->language->_('Can not be empty')
																         )
														),
                			    
                			    'edc'      => array(),
                			    'purchase'      => array(),
                			    
							   );
		
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//Zend_Debug::Dump($this->_request->getParams());die;
			$errorLim = false;
			if($zf_filter_input->edc == NULL &&  $zf_filter_input->purchase == NULL ){

                  $errorLim = true;
                  $error_remark = "al least one of the EDC or ATM must be on";
              }
			if($zf_filter_input->isValid() && !$errorLim)
			{  
				
				$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							//->where('A.CUST_ID = ?',$cust_id)
							->where('A.REG_NUMBER = ?',$zf_filter_input->regid);
				$data = $this->_db->fetchRow($select);	
				
				$content = array(
								//'BRANCH_CODE' 	 => $zf_filter_input->COMPANY_NAME,
								'REG_NUMBER' => $zf_filter_input->regid,
								'VA_NUMBER' => $data['VA_NUMBER'],
								'VA_NAME' => $data['VA_NAME'],
								'CUST_ID' 	 => $data['CUST_ID'],
								'DEBIT_TYPE' 	 => $zf_filter_input->cust_card,
            				    'DEBIT_EDC' 	 => $zf_filter_input->edc,
            				    'DEBIT_ATM' 	 => $zf_filter_input->purchase,
								'DEBIT_DATE'		=> new Zend_Db_Expr('now()'),
								'DEBIT_SUGGESTED'		=> new Zend_Db_Expr('now()'),
								'DEBIT_SUGGESTEDBY' => $this->_userIdLogin,
								'DEBIT_STATUS'		=> $data['DEBIT_STATUS']//3
						       );	
							   
			    //Zend_Debug::dump($content);
	  			//die;
							   

				try 
				{
				    //-----insert--------------
					$this->_db->beginTransaction(); 
					$change_id = $this->suggestionWaitingApproval('Corporate Debit Card',$info,$this->_changeType['code']['edit'],null,'T_CUST_DEBIT','TEMP_CUST_DEBIT',$data['CUST_ID'],$data['CUST_NAME'],$data['CUST_ID'],$data['CUST_NAME']);
					$content['CHANGES_ID'] = $change_id;
					$this->_db->insert('TEMP_CUST_DEBIT', $content);
// 					die;
					$this->_db->commit();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					Application_Helper_General::writeLog('COUD','Edit Branch Bank. Branch Name : ['.$zf_filter_input->branch_name.']');
					$this->view->success = true;
					$this->view->report_msg = array();
					
					$this->_redirect('/notification/submited/index');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();

				    foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);
					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{  
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
				
				if(!empty($error_remark)){
					//die('here');
					$errorArray = array(true);
					$this->view->report_msg = $errorArray;
					$this->view->errormsg = 'Error :'.$this->language->_($error_remark);
				}else{
					$this->view->errormsg = 'Error :'.$this->language->_('in processing form values. Please correct values and re-submit.');	
				}

			}
		}

		$id = $this->_getParam('regid');
			//$bank_id = (Zend_Validate::is($bank_id,'Digits'))? $bank_id : null;
			
			if($id)
			{
			  //$id  = $zf_filter_input->getEscaped('regid'); 
					$this->view->regid = $id;
					$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							//->where('A.CUST_ID = ?',$cust_id)
							->where('A.REG_NUMBER = ?',$id);
							
					$data = $this->_db->fetchRow($select);			
					if($data['DEBIT_TYPE'] == '1'){
						$this->view->checktype1 = 'checked';
						$this->view->debit_type = 1;
						$data['DEBIT_TYPE'] = 'Corporate';
					}else{
						$data['DEBIT_TYPE'] = 'Card Holder';
						$this->view->checktype2 = 'checked';
						$this->view->debit_type = 2;
					}
					
					if($data['DEBIT_ATM'] == '1'){
						$this->view->purchase = 1;
						$data['DEBIT_ATM'] = 'On';
						
					}else{
						$this->view->purchase = null;
						$data['DEBIT_ATM'] = 'Off';
					}
					
					if($data['DEBIT_EDC'] == '1'){
						$data['DEBIT_EDC'] = 'On';
						$this->view->edc = 1;
					}else{
						$this->view->edc = null;
						$data['DEBIT_EDC'] = 'Off';
					}
					
					
					$this->view->data = $data; 
			}
			else
			{
			   $error_remark = 'Branch Bank not found';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   //$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			   $this->_redirect('/'.$this->_request->getModuleName().'/index');	
			}
	}

}