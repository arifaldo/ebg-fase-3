<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class customer_SuggestiondetailController extends customer_Model_Customer
{
    public function indexAction()
    {

        $this->_helper->layout()->setLayout('newpopup');

        //print_r($this->_getParam());

        $changes_id = $this->_getParam('changes_id');
        $changes_id = (Zend_Validate::is($changes_id, 'Digits')) ? $changes_id : 0;

        $this->view->suggestionType = $this->_suggestType;

        if ($changes_id) {

            $getCompanyType = $this->getCompanyType();
            $companyTypeArr = Application_Helper_Array::listArray($getCompanyType, 'COMPANY_TYPE_CODE', 'COMPANY_TYPE_DESC');

            $getBusinessEntity = $this->getBusinessEntity();
            $businessEntityArr = Application_Helper_Array::listArray($getBusinessEntity, 'BUSINESS_ENTITY_CODE', 'BUSINESS_ENTITY_DESC');

            $getDebitur = $this->getDebitur();
            $debiturArr = Application_Helper_Array::listArray($getDebitur, 'DEBITUR_CODE', 'DEBITUR_DESC');

            $getCreditQuality = $this->getCreditQuality();
            $creditQualityArr = Application_Helper_Array::listArray($getCreditQuality, 'CODE', 'DESCRIPTION');

            $getCity = $this->getCity();
            $cityArr = Application_Helper_Array::listArray($getCity, 'CITY_CODE', 'CITY_NAME');

            $getCountry = $this->getCountry();
            $countryArr = Application_Helper_Array::listArray($getCountry, 'COUNTRY_CODE', 'COUNTRY_NAME');

            $custModelArr = [
                1 => 'Applicant',
                2 => 'Insurance',
                3 => 'Special Obligee'
            ];

            $select = $this->_db->select()
                ->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
                ->where('CHANGES_ID = ' . $this->_db->quote($changes_id))
                ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
            $result = $this->_db->fetchOne($select);

            if (empty($result)) {
                $this->_redirect('/notification/invalid/index');
            } else {

                $select = $this->_db->select()
                    ->from(
                        array('A' => 'TEMP_CUSTOMER'),
                        array('*')
                    )
                    ->joinLeft(
                        array('G' => 'T_GLOBAL_CHANGES'),
                        'G.CHANGES_ID = A.CHANGES_ID',
                        array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
                    )
                    ->where('A.CHANGES_ID = ?', $changes_id);
                $resultData = $this->_db->fetchRow($select);

                //Zend_Debug::dump($resultData);

                if ($resultData) {
                    $this->view->cust_id            = $resultData['CUST_ID'];
                    $this->view->changes_id         = $resultData['CHANGES_ID'];
                    $this->view->changes_type       = $resultData['CHANGES_TYPE'];
                    $this->view->changes_status     = $resultData['CHANGES_STATUS'];
                    $this->view->read_status        = $resultData['READ_STATUS'];

                    // DATA TEMP CUSTOMER
                    $this->view->cust_model         = $custModelArr[$resultData['CUST_MODEL']];
                    $this->view->cust_cif           = $resultData['CUST_CIF'];
                    $this->view->cust_name          = $resultData['CUST_NAME'];
                    $this->view->cust_npwp          = $resultData['CUST_NPWP'];
                    $this->view->cust_type          = $companyTypeArr[$resultData['CUST_TYPE']];
                    $this->view->business_type      = $businessEntityArr[$resultData['BUSINESS_TYPE']];
                    $this->view->go_public          = $resultData['GO_PUBLIC'];
                    $this->view->grup_bumn          = $resultData['GRUP_BUMN'] == "1" ? "Y" : "N";
                    $this->view->debitur_code       = $debiturArr[$resultData['DEBITUR_CODE']];
                    $this->view->collect_code       = $creditQualityArr[$resultData['COLLECTIBILITY_CODE']];
                    $this->view->cust_address       = $resultData['CUST_ADDRESS'];
                    $this->view->cust_village       = $resultData['CUST_VILLAGE'];
                    $this->view->cust_district      = $resultData['CUST_DISTRICT'];
                    $this->view->cust_city          = $cityArr[$resultData['CUST_CITY']];
                    $this->view->cust_zip           = $resultData['CUST_ZIP'];
                    $this->view->country_code       = $countryArr[$resultData['COUNTRY_CODE']];
                    $this->view->cust_approver      = $resultData['CUST_APPROVER'];

                    $this->view->cust_contact       = $resultData['CUST_CONTACT'];
                    $this->view->cust_contact_phone = $resultData['CUST_CONTACT_PHONE'];
                    $this->view->cust_email         = $resultData['CUST_EMAIL'];
                    $this->view->cust_phone         = $resultData['CUST_PHONE'];
                    $this->view->cust_website       = $resultData['CUST_WEBSITE'];

                    // DATA TEMP CUSTOMER ACCOUNT
                    $select = $this->_db->select()
                        ->from('TEMP_CUSTOMER_ACCT')
                        ->where('CHANGES_ID = ?', $changes_id);
                    $accountInfo = $this->_db->fetchAll($select);

                    if (empty($accountInfo)) {
                        $select = $this->_db->select()
                            ->from('M_CUSTOMER_ACCT')
                            ->where('CUST_ID = ?', $resultData['CUST_ID']);
                        $accountInfo = $this->_db->fetchAll($select);
                        $this->view->accountInfo = $accountInfo;
                    } else {
                        $this->view->accountInfo = $accountInfo;
                    }


                    $cust_id = $resultData['CUST_ID'];

                    // DATA MASTER CUSTOMER
                    $select = $this->_db->select()
                        ->from('M_CUSTOMER')
                        ->where('CUST_ID = ?', $cust_id);
                    $m_customer = $this->_db->fetchRow($select);

                    if ($m_customer) {
                        $this->view->m_cust_model         = $custModelArr[$m_customer['CUST_MODEL']];
                        $this->view->m_cust_id            = $m_customer['CUST_ID'];
                        $this->view->m_cust_cif           = $m_customer['CUST_CIF'];
                        $this->view->m_cust_name          = $m_customer['CUST_NAME'];
                        $this->view->m_cust_npwp          = $m_customer['CUST_NPWP'];
                        $this->view->m_cust_type          = $companyTypeArr[$m_customer['CUST_TYPE']];
                        $this->view->m_business_type      = $businessEntityArr[$m_customer['BUSINESS_TYPE']];
                        $this->view->m_go_public          = $m_customer['GO_PUBLIC'];
                        $this->view->m_grup_bumn          = $m_customer['GRUP_BUMN'] == "1" ? "Y" : "N";
                        $this->view->m_debitur_code       = $debiturArr[$m_customer['DEBITUR_CODE']];
                        $this->view->m_collect_code       = $creditQualityArr[$m_customer['COLLECTIBILITY_CODE']];
                        $this->view->m_cust_address       = $m_customer['CUST_ADDRESS'];
                        $this->view->m_cust_village       = $m_customer['CUST_VILLAGE'];
                        $this->view->m_cust_district      = $m_customer['CUST_DISTRICT'];
                        $this->view->m_cust_city          = $cityArr[$m_customer['CUST_CITY']];
                        $this->view->m_cust_zip           = $m_customer['CUST_ZIP'];
                        $this->view->m_country_code       = $countryArr[$m_customer['COUNTRY_CODE']];
                        $this->view->m_cust_approver      = $m_customer['CUST_APPROVER'];
                        $this->view->m_cust_contact       = $m_customer['CUST_CONTACT'];
                        $this->view->m_cust_contact_phone = $m_customer['CUST_CONTACT_PHONE'];
                        $this->view->m_cust_email         = $m_customer['CUST_EMAIL'];
                        $this->view->m_cust_phone         = $m_customer['CUST_PHONE'];
                        $this->view->m_cust_website       = $m_customer['CUST_WEBSITE'];
                    }

                    // DATA MASTER CUSTOMER ACCOUNT
                    $select = $this->_db->select()
                        ->from('M_CUSTOMER_ACCT')
                        ->where('CUST_ID = ?', $cust_id);
                    $m_accountInfo = $this->_db->fetchAll($select);
                    $this->view->m_accountInfo = $m_accountInfo;

                    $submit_approve         = $this->_getParam('submit');
                    $submit_reject          = $this->_getParam('submit_reject');
                    $submit_request_repair  = $this->_getParam('submit_request_repair');

                    if ($submit_approve == $this->language->_('Approve')) {

                        // $core = array();
                        // $svcAccountCIF = new Service_Account(null, null, null, null, null, $resultData['CUST_CIF']);
                        // $result = $svcAccountCIF->inquiryCIFAccount();
                        // $core =  $result['accounts'];
                        // if($result['response_code'] != '0000'){
                        //     $this->view->error = true;
                        //     $this->view->ERROR_MSG = $result['ResponseDesc'];
                        // }else{

                        try {
                            $this->_db->beginTransaction();
                            // INSERT/UPDATE DATA
                            $data1 = [
                                'CUST_ID'               => $cust_id,
                                'CUST_CIF'              => $resultData['CUST_CIF'],
                                'CUST_NAME'             => $resultData['CUST_NAME'],
                                'CUST_MODEL'            => $resultData['CUST_MODEL'],
                                'CUST_TYPE'             => $resultData['CUST_TYPE'],
                                'CUST_CONTACT'          => $resultData['CUST_CONTACT'],
                                'CUST_CONTACT_PHONE'    => $resultData['CUST_CONTACT_PHONE'],
                                'CUST_PHONE'            => $resultData['CUST_PHONE'],
                                'CUST_EMAIL'            => $resultData['CUST_EMAIL'],
                                'CUST_ADDRESS'          => $resultData['CUST_ADDRESS'],
                                'CUST_CITY'             => $resultData['CUST_CITY'],
                                'CUST_ZIP'              => $resultData['CUST_ZIP'],
                                'COUNTRY_CODE'          => $resultData['COUNTRY_CODE'],
                                'CUST_WEBSITE'          => $resultData['CUST_WEBSITE'],
                                'CUST_STATUS'           => $resultData['CUST_STATUS'],
                                'CUST_CREATED'          => $resultData['CUST_CREATED'],
                                'CUST_CREATEDBY'        => $resultData['CUST_CREATEDBY'],
                                'CUST_UPDATED'          => new Zend_Db_Expr('now()'),
                                'CUST_UPDATEDBY'        => $this->_userIdLogin,
                                'CUST_SUGGESTED'        => $resultData['CUST_SUGGESTED'],
                                'CUST_SUGGESTEDBY'      => $resultData['CUST_SUGGESTEDBY'],
                                'CUST_APPROVER'         => $resultData['CUST_APPROVER'],
                                'CUST_NPWP'             => $resultData['CUST_NPWP'],
                                'CUST_VILLAGE'          => $resultData['CUST_VILLAGE'],
                                'CUST_DISTRICT'         => $resultData['CUST_DISTRICT'],
                                'BUSINESS_TYPE'         => $resultData['BUSINESS_TYPE'],
                                'GO_PUBLIC'             => $resultData['GO_PUBLIC'],
                                'GRUP_BUMN'             => $resultData['GRUP_BUMN'],
                                'DEBITUR_CODE'          => $resultData['DEBITUR_CODE'],
                                'COLLECTIBILITY_CODE'   => $resultData['COLLECTIBILITY_CODE'],
                            ];
                            $where1 = ['CUST_ID = ?' => $cust_id];

                            if ($resultData['CHANGES_TYPE'] == 'N') {
                                $this->_db->insert('M_CUSTOMER', $data1);
                            } else {
                                $this->_db->update('M_CUSTOMER', $data1, $where1);
                            }

                            $select = $this->_db->select()
                                ->from('TEMP_CUSTOMER_ACCT')
                                ->where('CHANGES_ID = ?', $changes_id);
                            $accountInfo = $this->_db->fetchAll($select);

                            if ($accountInfo) {

                                foreach ($accountInfo as $row) {
                                    $data2 = [
                                        'ACCT_NO'           => $row['ACCT_NO'],
                                        'CUST_ID'           => $cust_id,
                                        'CCY_ID'            => $row['CCY_ID'],
                                        'ACCT_CIF'          => $row['ACCT_CIF'],
                                        'ACCT_NAME'         => $row['ACCT_NAME'],
                                        'GROUP_ID'          => $row['GROUP_ID'],
                                        'ACCT_TYPE'         => $row['ACCT_TYPE'],
                                        'ACCT_DESC'         => $row['ACCT_DESC'],
                                        'ACCT_ALIAS_NAME'   => $row['ACCT_ALIAS_NAME'],
                                        'ACCT_EMAIL'        => $row['ACCT_EMAIL'],
                                        'ORDER_NO'          => $row['ORDER_NO'],
                                        'ACCT_STATUS'       => $row['ACCT_STATUS'],
                                        'FREEZE_STATUS'     => $row['FREEZE_STATUS'],
                                        'ISSYSTEMBALANCE'   => $row['ISSYSTEMBALANCE'],
                                        'PLAFOND'           => $row['PLAFOND'],
                                        'ISPLAFONDDATE'     => $row['ISPLAFONDDATE'],
                                        'PLAFOND_START'     => $row['PLAFOND_START'],
                                        'PLAFOND_END'       => $row['PLAFOND_END'],
                                        'ACCT_CREATED'      => $row['ACCT_CREATED'],
                                        'ACCT_CREATEDBY'    => $row['ACCT_CREATEDBY'],
                                        'ACCT_UPDATED'      => new Zend_Db_Expr('now()'),
                                        'ACCT_UPDATEDBY'    => $this->_userIdLogin,
                                        'ACCT_SUGGESTED'    => $row['ACCT_SUGGESTED'],
                                        'ACCT_SUGGESTEDBY'  => $row['ACCT_SUGGESTEDBY']
                                    ];
                                    $this->_db->insert('M_CUSTOMER_ACCT', $data2);
                                }
                            }

                            $this->_db->delete('TEMP_CUSTOMER', $where1);
                            $this->_db->delete('TEMP_CUSTOMER_ACCT', $where1);

                            $data3  = [
                                'CHANGES_STATUS'    => 'AP',
                                'LASTUPDATED'       => new Zend_Db_Expr('now()')
                            ];
                            $where3 = ['CHANGES_ID = ?' => $changes_id];
                            $this->_db->update('T_GLOBAL_CHANGES', $data3, $where3);

                            $this->_db->commit();

                            $this->view->is_approve = true;
                        } catch (Exception $error) {

                            $this->_db->rollBack();
                            echo '<pre>';
                            print_r($error->getMessage());
                            echo '</pre><br>';
                            die;
                        }
                        // }
                    } elseif ($submit_reject) {

                        try {


                            $this->_db->beginTransaction();

                            $where1 = ['CUST_ID = ?' => $cust_id];
                            $this->_db->delete('TEMP_CUSTOMER', $where1);
                            $this->_db->delete('TEMP_CUSTOMER_ACCT', $where1);
                            $noteReject =  $this->_getParam("PS_REASON_REPAIR");
                            $data2  = [
                                'CHANGES_STATUS'    => 'RJ',
                                'CHANGES_REASON'    => $noteReject,
                                'LASTUPDATED'       => new Zend_Db_Expr('now()')
                            ];
                            $where2 = ['CHANGES_ID = ?' => $changes_id];
                            $this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

                            $this->_db->commit();

                            $this->view->is_reject = true;
                        } catch (Exception $error) {
                            $this->_db->rollBack();
                            echo '<pre>';
                            print_r($error->getMessage());
                            echo '</pre><br>';
                            die;
                        }
                    } elseif ($submit_request_repair) {

                        try {
                            $this->_db->beginTransaction();

                            $data1  = [
                                'CHANGES_STATUS'    => 'RR',
                                'LASTUPDATED'       => new Zend_Db_Expr('now()')
                            ];
                            $where1 = ['CHANGES_ID = ?' => $changes_id];
                            $this->_db->update('T_GLOBAL_CHANGES', $data1, $where1);

                            $this->_db->commit();

                            $this->view->is_request_repair = true;
                        } catch (Exception $error) {
                            $this->_db->rollBack();
                            echo '<pre>';
                            print_r($error->getMessage());
                            echo '</pre><br>';
                            die;
                        }
                    } else {

                        Application_Helper_General::writeLog('CCCL', 'View Customer Changes CUST_ID: ' . $cust_id);
                    }
                }
            }
        } else {
            $this->_redirect('/popuperror/index/index');
        }
    }

    public function repairAction()
    {
        $this->_helper->layout()->setLayout('newpopup');

        $changes_id = $this->_getParam('changes_id');
        $changes_id = (Zend_Validate::is($changes_id, 'Digits')) ? $changes_id : 0;

        if ($changes_id) {

            $this->view->cust_typeArr           = $this->getCompanyType();
            $this->view->business_typeArr       = $this->getBusinessEntity();
            $this->view->debitur_codeArr        = $this->getDebitur();
            $this->view->collectibility_codeArr = $this->getCreditQuality();
            $this->view->cust_cityArr           = $this->getCity();
            $this->view->country_codeArr        = $this->getCountry();

            $custModelArr = [
                1 => 'Applicant',
                2 => 'Insurance',
                3 => 'Special Obligee'
            ];
            $this->view->custModelArr = $custModelArr;

            $select = $this->_db->select()
                ->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
                ->where('CHANGES_ID = ' . $this->_db->quote($changes_id))
                ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
            $result = $this->_db->fetchOne($select);

            if (empty($result)) {
                $this->_redirect('/notification/invalid/index');
            } else {
                $select = $this->_db->select()
                    ->from(
                        array('A' => 'TEMP_CUSTOMER'),
                        array('*')
                    )
                    ->joinLeft(
                        array('G' => 'T_GLOBAL_CHANGES'),
                        'G.CHANGES_ID = A.CHANGES_ID',
                        array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
                    )
                    ->where('A.CHANGES_ID = ?', $changes_id);
                $resultData = $this->_db->fetchRow($select);
                $this->view->data = $resultData;

                $cust_id = $resultData['CUST_ID'];

                if ($resultData) {
                    if ($this->_request->isPost()) {
                        $params = $this->_request->getParams();

                        $filters = array('*' => array('StringTrim', 'StripTags'));

                        $validators = array(
                            'cust_model'  => array(
                                'allowEmpty' => true
                            ),
                            'cust_id'    => array(
                                'allowEmpty' => true
                            ),
                            'cust_cif'   => array(
                                'allowEmpty' => true
                            ),
                            'cust_name'  => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Company Name cannot be empty'))
                            ),
                            'cust_npwp'  => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Company NPWP cannot be empty'))
                            ),
                            'cust_type'  => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Company Type cannot be empty'))
                            ),
                            'business_type'  => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Business Type cannot be empty'))
                            ),
                            'go_public'  => array(
                                'allowEmpty' => true
                            ),
                            'cust_address'   => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Address cannot be empty'))
                            ),
                            'cust_village'   => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Kelurahan cannot be empty'))
                            ),
                            'cust_district'  => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Kecamatan cannot be empty'))
                            ),
                            'cust_city'  => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Kabupaten/City cannot be empty'))
                            ),
                            'cust_zip'   => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Zip cannot be empty'))
                            ),
                            'country_code'    => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Country cannot be empty'))
                            ),
                            'cust_email' => array(
                                'NotEmpty',
                                'messages' => array($this->language->_('Contact Email cannot be empty'))
                            ),
                            'cust_website'  => array(
                                'allowEmpty' => true
                            ),
                            'req_id'     => array(
                                'allowEmpty' => true
                            ),
                            'cust_same_user'  => array(
                                'allowEmpty' => true
                            ),
                            'cust_approver'  => array(
                                'allowEmpty' => true
                            ),
                        );

                        if ($resultData['CUST_MODEL'] == 1) {
                            $validators += array(
                                'debitur_code'   => array(
                                    'NotEmpty',
                                    'messages' => array($this->language->_('Golongan Debitur Code cannot be empty'))
                                ),
                                'cust_contact'   => array('allowEmpty' => true),
                                'cust_contact_phone'     => array(
                                    'allowEmpty' => true,
                                    array('StringLength', array('min' => 1, 'max' => 128)),
                                    'messages' => array(
                                        $this->language->_('Contact Number maximum characters allowed is') . ' 128'
                                    )
                                ),
                                'cust_phone'       => array(
                                    'allowEmpty' => true,
                                    array('StringLength', array('min' => 1, 'max' => 128)),
                                    'messages' => array(
                                        $this->language->_('Contact Number maximum characters allowed is') . ' 128'
                                    )
                                ),
                            );
                        } else {
                            $validators += array(
                                'cust_phone' => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 128)),
                                    'messages' => array(
                                        $this->language->_('Company Phone cannot be empty'),
                                        $this->language->_('Company Phone maximum characters allowed is') . ' 128'
                                    )
                                ),
                            );
                        }

                        if ($resultData['CUST_MODEL'] == 2) {
                            $validators += array(
                                'cust_contact'   => array(
                                    'NotEmpty',
                                    'messages' => array($this->language->_('Contact Person cannot be empty'))
                                ),
                                'cust_contact_phone' => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 128)),
                                    'messages' => array(
                                        $this->language->_('Contact Number cannot be empty'),
                                        $this->language->_('Contact Number maximum characters allowed is') . ' 128'
                                    )
                                ),
                            );
                        }

                        if ($resultData['CUST_MODEL'] == 3) {
                            $validators += array(
                                'cust_contact'   => array('allowEmpty' => true),
                                'cust_contact_phone'     => array(
                                    'allowEmpty' => true,
                                    array('StringLength', array('min' => 1, 'max' => 128)),
                                    'messages' => array(
                                        $this->language->_('Contact Number maximum characters allowed is') . ' 128'
                                    )
                                ),
                            );
                        }

                        if ($resultData['CUST_MODEL'] != 3) {
                            $validators += array(
                                'collectibility_code'    => array(
                                    'NotEmpty',
                                    'messages' => array($this->language->_('Collectibility Code cannot be empty'))
                                ),
                            );
                        }

                        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

                        if ($zf_filter_input->isValid()) {
                            // Validasi multiple email
                            if ($zf_filter_input->cust_email) {
                                $validate = new validate;
                                $cek_multiple_email = $validate->isValidEmailMultiple($zf_filter_input->cust_email);
                            } else {
                                $cek_multiple_email = true;
                            }

                            foreach ($validators as $key => $value) {
                                if ($zf_filter_input->$key) {
                                    $cust_data[strtoupper($key)] = $zf_filter_input->$key;
                                }
                            }

                            try {
                                $this->_db->beginTransaction();

                                $cust_data['CUST_MODEL']        = $resultData['CUST_MODEL'];
                                $cust_data['CUST_STATUS']       = $resultData['CUST_STATUS'];
                                $cust_data['CUST_CREATED']      = $resultData['CUST_CREATED'];
                                $cust_data['CUST_CREATEDBY']    = $resultData['CUST_CREATEDBY'];
                                $cust_data['CUST_UPDATED']      = $resultData['CUST_UPDATED'];
                                $cust_data['CUST_UPDATEDBY']    = $resultData['CUST_UPDATEDBY'];
                                $cust_data['CUST_SUGGESTED']    = date('Y-m-d H:i:s');
                                $cust_data['CUST_SUGGESTEDBY']  = $this->_userIdLogin;

                                if (!empty($zf_filter_input->same_release)) {
                                    $cust_data['CUST_SAME_USER'] = $zf_filter_input->same_release;
                                }

                                try {
                                    $this->updateTempCustomer($changes_id, $cust_data);

                                    $where = ['CHANGES_ID = ?' => $changes_id];
                                    $this->_db->delete('TEMP_CUSTOMER_ACCT', $where);

                                    foreach ($zf_filter_input->req_id as $key => $value) {
                                        $content = [
                                            'CHANGES_ID'        => $changes_id,
                                            'CUST_ID'           => $cust_id,
                                            'ACCT_NO'           => $value,
                                            'CCY_ID'            => Application_Helper_General::getCurrCode($this->_getParam('ccy' . $value)),
                                            'ACCT_EMAIL'        => null,
                                            'ACCT_STATUS'       => 1,
                                            'ACCT_SUGGESTED'    => date('Y-m-d H:i:s'),
                                            'ACCT_SUGGESTEDBY'  => $this->_userIdLogin,
                                            'PLAFOND'           => null,
                                            'ACCT_SOURCE'       => null,
                                            'ACCT_NAME'         => $this->_getParam('accountName' . $value),
                                            'ACCT_TYPE'         => $this->_getParam('productType' . $value),
                                            'ACCT_DESC'         => $this->_getParam('acct_desc' . $value),
                                            // 'ACCT_ALIAS_NAME'    => isset($acct_data['ACCT_ALIAS_NAME']) ? $acct_data['ACCT_ALIAS_NAME'] : null,
                                            'ACCT_ALIAS_NAME'  => $this->_getParam('accountName' . $value),
                                            'ORDER_NO'         => $key,
                                            // 'ACCT_NAME'          => isset($acct_data['ACCT_NAME']) ? $acct_data['ACCT_NAME'] : null,
                                            'GROUP_ID'          => null,
                                            'ACCT_RESIDENT'     => null,
                                            'ACCT_CITIZENSHIP'  => null,
                                            'ACCT_CATEGORY'     => null,
                                            'ACCT_ID_TYPE'      => null,
                                            'ACCT_ID_NUMBER'    => null
                                        ];
                                        // $this->print($content);
                                        $this->_db->insert('TEMP_CUSTOMER_ACCT', $content);
                                    }

                                    $data2  = [
                                        'CHANGES_STATUS' => 'WA',
                                        'READ_STATUS'   => 2,
                                        'LASTUPDATED'   => new Zend_Db_Expr('now()')
                                    ];
                                    $where2 = ['CHANGES_ID = ?' => $changes_id];
                                    $this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);
                                } catch (Exception $e) {
                                    $this->print($e);
                                    die('catch');
                                }

                                $this->_db->commit();

                                Application_Helper_General::writeLog('CCCL', 'Repaire Customer Changes CUST_ID: ' . $cust_id);
                                $this->_redirect('/popup/successpopup/index');
                            } catch (Exception $e) {
                                $this->_db->rollBack();
                                $error_remark = 'exception';
                            }
                        } else {
                            $errors = $zf_filter_input->getMessages();
                            $errDesc['cust_name']           = (isset($errors['cust_name'])) ? $errors['cust_name'] : null;
                            $errDesc['cust_npwp']           = (isset($errors['cust_npwp'])) ? $errors['cust_npwp'] : null;
                            $errDesc['cust_type']           = (isset($errors['cust_type'])) ? $errors['cust_type'] : null;
                            $errDesc['business_type']       = (isset($errors['business_type'])) ? $errors['business_type'] : null;
                            $errDesc['go_public']           = (isset($errors['go_public'])) ? $errors['go_public'] : null;
                            $errDesc['cust_address']        = (isset($errors['cust_address'])) ? $errors['cust_address'] : null;
                            $errDesc['debitur_code']        = (isset($errors['debitur_code'])) ? $errors['debitur_code'] : null;
                            $errDesc['collectibility_code'] = (isset($errors['collectibility_code'])) ? $errors['collectibility_code'] : null;
                            $errDesc['cust_village']        = (isset($errors['cust_village'])) ? $errors['cust_village'] : null;
                            $errDesc['cust_district']       = (isset($errors['cust_district'])) ? $errors['cust_district'] : null;
                            $errDesc['cust_city']           = (isset($errors['cust_city'])) ? $errors['cust_city'] : null;
                            $errDesc['cust_zip']            = (isset($errors['cust_zip'])) ? $errors['cust_zip'] : null;
                            $errDesc['country_code']        = (isset($errors['country_code'])) ? $errors['country_code'] : null;
                            $errDesc['cust_contact']        = (isset($errors['cust_contact'])) ? $errors['cust_contact'] : null;
                            $errDesc['cust_contact_phone']  = (isset($errors['cust_contact_phone'])) ? $errors['cust_contact_phone'] : null;
                            $errDesc['cust_email']          = (isset($errors['cust_email'])) ? $errors['cust_email'] : null;
                            $errDesc['cust_phone']          = (isset($errors['cust_phone'])) ? $errors['cust_phone'] : null;
                            $errDesc['cust_website']        = (isset($errors['cust_website'])) ? $errors['cust_website'] : null;

                            $this->view->errDesc            = $errDesc;
                            $this->view->cust_name          = $zf_filter_input->cust_name;
                            $this->view->cust_npwp          = $zf_filter_input->cust_npwp;
                            $this->view->cust_type          = $zf_filter_input->cust_type;
                            $this->view->business_type      = $zf_filter_input->business_type;
                            $this->view->go_public          = $zf_filter_input->go_public;
                            $this->view->cust_address       = $zf_filter_input->cust_address;
                            $this->view->debitur_code       = $zf_filter_input->debitur_code;
                            $this->view->collectibility_code = $zf_filter_input->collectibility_code;
                            $this->view->cust_village       = $zf_filter_input->cust_village;
                            $this->view->cust_district      = $zf_filter_input->cust_district;
                            $this->view->cust_city          = $zf_filter_input->cust_city;
                            $this->view->cust_zip           = $zf_filter_input->cust_zip;
                            $this->view->country_code       = $zf_filter_input->country_code;
                            $this->view->cust_contact       = $zf_filter_input->cust_contact;
                            $this->view->cust_contact_phone = $zf_filter_input->cust_contact_phone;
                            $this->view->cust_email         = $zf_filter_input->cust_email;
                            $this->view->cust_phone         = $zf_filter_input->cust_phone;
                            $this->view->cust_website       = $zf_filter_input->cust_website;
                        }
                    } else {
                        Application_Helper_General::writeLog('CCCL', 'View Repair Customer Changes CUST_ID: ' . $cust_id);
                    }
                }
            }
        } else {
            $this->_redirect('/popuperror/index/index');
        }
    }
}
