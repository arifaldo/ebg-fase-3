<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class display_DepositController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash; 
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
		
		//select m_user - begin
		
		$userData = $CustomerUser->getUser($this->_userIdLogin);
		
		$fields = array(
						'SOURCE_ACCT'      	=> array('field' => 'SOURCE_ACCT',
													'label' => $this->language->_('Source Account'),
													'sortable' => true),
						'DEPOSIT_AMOUNT'      	=> array('field' => 'DEPOSIT_AMOUNT',
													'label' => $this->language->_('Deposit Amount'),
													'sortable' => true),
						'ARO'  		=> array('field' => 'ARO',
													'label' => $this->language->_('ARO (Automatic Roll Over)'),
													'sortable' => true),
						'DEPOSIT_MONTH'  	=> array('field' => 'DEPOSIT_MONTH',
													'label' => $this->language->_('Deposit Month'),
													'sortable' => true),
						'AIM_OBJECTIVE'  			=> array('field' => 'AIM_OBJECTIVE',
													'label' => $this->language->_('Aim Objective'),
													'sortable' => true),
						'DEPOSIT_PURPOSE'  			=> array('field' => 'DEPOSIT_PURPOSE',
													'label' => $this->language->_('Deposit Purpose'),
													'sortable' => true),
						'AVERAGE_TRANSACTION'  			=> array('field' => 'AVERAGE_TRANSACTION',
													'label' => $this->language->_('Average Transaction'),
													'sortable' => true),
						'COMPANY_NAME'  			=> array('field' => 'COMPANY_NAME',
													'label' => $this->language->_('Company Name'),
													'sortable' => true),
						'DEPOSIT_CREATEDBY'  			=> array('field' => 'DEPOSIT_CREATEDBY',
													'label' => $this->language->_('Created By'),
													'sortable' => true),
						'DEPOSIT_CREATED'  	=> array('field' => 'DEPOSIT_CREATED',
													'label' => $this->language->_('Created Date'),
													'sortable' => true),
						'STATUS'  					=> array('field' => 'STATUS',
													'label' => $this->language->_('Status'),
													'sortable' => true),
						'DEPOSIT_UPDATEDBY'  		=> array('field' => 'DEPOSIT_UPDATEDBY',
													'label' => $this->language->_('Updated By'),
													'sortable' => true),
						'DEPOSIT_UPDATED'  					=> array('field' => 'DEPOSIT_UPDATEDBY',
													'label' => $this->language->_('Updated Date'),
													'sortable' => true),
						'ACTION'  					=> array('field' => 'ACTION',
													'label' => $this->language->_('Action'),
													'sortable' => true)
				      );

		$filterlist = array('OPENING_DATE','DEPOSIT_CREATED','SOURCE_ACCT','DEPOSIT_MONTH');

		$this->view->filterlist = $filterlist;
				      
		$page    = $this->_getParam('page');
		
		// $sortBy  = $this->_getParam('sortby','B.PS_UPDATED');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('PS_UPDATED');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		// $sortDir = $this->_getParam('sortdir','desc');
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		
		$select2 = $this->_db->select()
					        ->from(array('D' => 'T_DEPOSIT'))
					        ->join(array('C' => 'M_CUSTOMER'), 'D.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'));
		$select2->where("D.CUST_ID = ?",$this->_custIdLogin);
		
		$filterArr = array(
           'SOURCE_ACCT'		=> array('StripTags','StringTrim','StringToUpper'),	                                              
		   'DEPOSIT_MONTH'			=> array('StripTags')
          );
	                      
		$dataParam = array('OPENING_DATE','DEPOSIT_CREATED',"SOURCE_ACCT","DEPOSIT_MONTH","DEPOSIT_STATUS");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}
		}

		if(!empty($this->_request->getParam('createdate'))){
			$createdatearr = $this->_request->getParam('createdate');
			$dataParamValue['DEPOSIT_CREATED'] = $createdatearr[0];
			$dataParamValue['DEPOSIT_CREATED_END'] = $createdatearr[1];
		}

		if(!empty($this->_request->getParam('opendate'))){
			$opendatearr = $this->_request->getParam('opendate');
			$dataParamValue['OPENING_DATE'] = $opendatearr[0];
			$dataParamValue['OPENING_DATE_END'] = $opendatearr[1];
		}
	                      
		$validator = array(                  
		   'SOURCE_ACCT'	=> array(),	 
		   'DEPOSIT_MONTH'			=> array(),						   
		   'DEPOSIT_CREATED'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		   'DEPOSIT_CREATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		   'OPENING_DATE'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		   'OPENING_DATE_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	      );
	   
	     $zf_filter 	= new Zend_Filter_Input($filterArr,$validator,$dataParamValue);

	     if ($zf_filter->isValid()) {
	     	$filter 		= TRUE;
	     }
	    
	     
	     $sourceacct 	= html_entity_decode($zf_filter->getEscaped('SOURCE_ACCT'));	 
		 $depositmonth	= html_entity_decode($zf_filter->getEscaped('DEPOSIT_MONTH'));		 
		 $createdatefrom 		= html_entity_decode($zf_filter->getEscaped('DEPOSIT_CREATED'));
		 $createdateto 		= html_entity_decode($zf_filter->getEscaped('DEPOSIT_CREATED_END'));
		 $opendatefrom 		= html_entity_decode($zf_filter->getEscaped('OPENING_DATE'));
		 $opendateto 		= html_entity_decode($zf_filter->getEscaped('OPENING_DATE_END'));
		 
		 
		if($filter == null)
		{	
			$createdatefrom = (date("d/m/Y"));
			$createdateto = (date("d/m/Y"));
			$opendatefrom = (date("d/m/Y"));
			$opendateto = (date("d/m/Y"));
			$this->view->createdatefrom  = (date("d/m/Y"));
			$this->view->createdateto  = (date("d/m/Y"));
			$this->view->opendatefrom  = (date("d/m/Y"));
			$this->view->opendateto  = (date("d/m/Y"));
		}
		
		if($filter_clear == '1'){
			$this->view->createdatefrom  = '';
			$this->view->createdateto  = '';
			$this->view->opendatefrom  = '';
			$this->view->opendateto  = '';
			$createdatefrom = '';
			$createdateto = '';
			$opendatefrom = '';
			$opendateto = '';
			
		}
		
		if($filter == null || $filter ==TRUE)
		{
			$this->view->createdatefrom  = $createdatefrom;
			$this->view->createdateto  = $createdateto;
			$this->view->opendatefrom  = $opendatefrom;
			$this->view->opendateto  = $opendateto; 
		if(!empty($createdatefrom))
	            {
	            	$FormatDate = new Zend_Date($createdatefrom, $this->_dateDisplayFormat);
					$createdatefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($createdateto))
	            {
	            	$FormatDate = new Zend_Date($createdateto, $this->_dateDisplayFormat);
					$createdateto    = $FormatDate->toString($this->_dateDBFormat);
	            }
		
		if(!empty($createdatefrom) && empty($createdateto))
	            $select2->where("D.DEPOSIT_CREATED >= ".$this->_db->quote($createdatefrom));
	            
	   	if(empty($createdatefrom) && !empty($createdateto))
	            $select2->where("D.DEPOSIT_CREATED <= ".$this->_db->quote($createdateto));
	            
	    if(!empty($createdatefrom) && !empty($createdateto))
	            $select2->where("D.DEPOSIT_CREATED between ".$this->_db->quote($createdatefrom)." and ".$this->_db->quote($createdateto));


	    if(!empty($opendatefrom))
	            {
	            	$FormatDate = new Zend_Date($opendatefrom, $this->_dateDisplayFormat);
					$opendatefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($opendateto))
	            {
	            	$FormatDate = new Zend_Date($opendateto, $this->_dateDisplayFormat);
					$opendateto    = $FormatDate->toString($this->_dateDBFormat);
	            }
		
		if(!empty($opendatefrom) && empty($opendateto))
	            $select2->where("D.OPENING_DATE >= ".$this->_db->quote($opendatefrom));
	            
	   	if(empty($opendatefrom) && !empty($opendateto))
	            $select2->where("D.OPENING_DATE <= ".$this->_db->quote($opendateto));
	            
	    if(!empty($opendatefrom) && !empty($opendateto))
	            $select2->where("D.OPENING_DATE between ".$this->_db->quote($opendatefrom)." and ".$this->_db->quote($opendateto));

		}
		
		if($filter == TRUE)
	    {		
	    	
	    	if($sourceacct!=null)
			{ 				
				$this->view->sourceacct = $sourceacct;
				$select2->where("D.SOURCE_ACCT LIKE ".$this->_db->quote($sourceacct));
			}
	    	
			if($depositmonth != null)
			{
			   $this->view->depositmonth = $depositmonth;
		       $select2->where("D.DEPOSIT_MONTH LIKE ".$this->_db->quote($depositmonth));
			}
		}

		$select2->order($sortBy.' '.$sortDir);
		
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$arr = $this->_db->fetchAll($select2);
			foreach ($arr as $key => $value)
			{
				unset($arr[$key]['CUST_ID']);
				//echo $key;
				$arr[$key]["PS_REQUESTED"] = Application_Helper_General::convertDate($value["PS_REQUESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

				$arr[$key]["SOURCE_ACCOUNT"] = $arr[$key]["SOURCE_ACCOUNT"]." [".$arr[$key]["SOURCE_ACCOUNT_CCY"]."] / ".$arr[$key]["SOURCE_ACCOUNT_NAME"];
				unset($arr[$key]["SOURCE_ACCOUNT_CCY"]);
				unset($arr[$key]["SOURCE_ACCOUNT_NAME"]);

				$arrProductType = array('1'=>'Cheque','2'=>'Bilyet Giro');
				$arr[$key]["PRODUCT_TYPE"] = $arrProductType[$arr[$key]["PRODUCT_TYPE"]];

				unset($arr[$key]['PS_APPROVEBY']);

				$requestby = $arr[$key]['PS_REQUESTBY'];
				unset($arr[$key]['PS_REQUESTBY']);

				$arr[$key]["ADMIN_FEE"] = "IDR ".Application_Helper_General::displayMoney($arr[$key]["ADMIN_FEE"]);
				unset($arr[$key]["SUGGEST_STATUS"]);

				$arr[$key]["PS_REQUESTBY"] = $requestby;

				
				// $arr[$key]["SOURCE_ACCOUNT_NAME"]= $value["SOURCE_ACCOUNT_NAME"];
				// $arr[$key]["BENEFICIARY_ACCOUNT_NAME"]= $value["BENEFICIARY_ACCOUNT_NAME"];
				// $arr[$key]["BENEFICIARY_ACCOUNT_CCY"]= $value["BENEFICIARY_ACCOUNT_CCY"].' / '.$value["TRA_AMOUNT"];
				// $arr[$key]["PS_TYPE"]= $this->language->_($value["PS_TYPE"]).' ('.$value["TRANSFER_TYPE"].')';
				
				// $amount = Application_Helper_General::displayMoney( $value['TRA_AMOUNT'] );
				
				// $updated = Application_Helper_General::convertDate($value["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				// $efdate = Application_Helper_General::convertDate($value["PS_EFDATE"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
				// $arr[$key]["AMOUNT_EQ"]= $amounteq;
				
				// $status = $this->language->_($value["TRA_STATUS"]);
				
				// $pstype = $this->language->_($value["PS_TYPE"]);
				
				// $arr[$key]["PS_UPDATED"] = $updated;
				// $arr[$key]["PS_EFDATE"] = $efdate;
				// $arr[$key]["TRA_STATUS"] = $status;
				// $arr[$key]["PS_TYPE"] = $pstype;
				
			}
// 			echo "<pre>";
// 			print_r($arr);die;
			$header = Application_Helper_Array::simpleArray($fields, 'label');
			//Zend_Debug::dump($arr);die;
			if($csv)
			{
				Application_Helper_General::writeLog('DTRX','Download CSV Transaction Report');
				//Zend_Debug::dump($arr);die;
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->csv($header,$arr,null,'Executed Transaction');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('DTRX','Download PDF Transaction Report');
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->pdf($header,$arr,null,'Executed Transaction');
			}
			if($this->_request->getParam('print') == 1){
				
	                $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields));
	        }
		}
		else
		{
			Application_Helper_General::writeLog('DTRX','View Transaction Report');
		}
// 		echo '<pre>';
// 		print_r($select2->query());die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($select2);

		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}
}