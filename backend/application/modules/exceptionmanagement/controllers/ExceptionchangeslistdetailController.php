<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';

/*
* ALL COMPLETED PAYMENT ( FAILED / SUCCESS : FLAG SENDFILE_STATUS = 4 )
*/
class Exceptionmanagement_ExceptionchangeslistdetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$transferstatus = $this->_transferstatus;
	$transferstatuscode = $transferstatus['code'];
	$transferstatusdesc = $transferstatus['desc'];
	$this->view->transferstatuscode = array_flip($transferstatuscode);
	$this->view->transferstatusdesc = $transferstatusdesc;
	$this->view->transferstatus = $transferstatus;
	$approvalstatus = $this->_aprovalstatus;
	
	$sugstatusarr = array_combine(array_values($approvalstatus['code']),array_values($approvalstatus['desc']));
	$paycashmstatusarr = array_combine(array_values($this->_paymentstatus['code']),array_values($this->_paymentstatus['desc']));
	$trastatusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
	$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
	// $feearr = array_combine(array_values($this->_feetype['code']),array_values($this->_feetype['desc']));
  	$suggestid = $this->_getParam('suggestid');
  	$slipnumb = $this->_getParam('slipnumb');
  	$extype = $this->_getParam('extype');
  	$approve = $this->_getParam('approve');
  	$reject = $this->_getParam('reject');
  	
  	if($approve)
  	{
  		Application_Helper_General::writeLog('EXCA','Approve Payment Exception ('.$slipnumb.')');
  	}
  	if($reject)
  	{
  		Application_Helper_General::writeLog('EXCA','Reject Payment Exception ('.$slipnumb.')');
  	}
  	if(!$approve || !$reject)
  	{
  		//Application_Helper_General::writeLog('EXCL','Viewing Payment Exception Changes List Detail ('.$slipnumb.')');	
  	}
  	
  	$selectCek = $this->_db->select()
							->from(array('A' => 'T_PSLIP_EXCEPTION_REPAIR'),array('*'))
							->joinLeft(array('B' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'),'A.SUGGEST_ID = B.SUGGEST_ID',array('*'))
							->joinLeft(array('C' => 'T_PSLIP'),'A.PS_NUMBER = C.PS_NUMBER',array('PS_CCY','ESCROW_ACC','ESCROW_ACC_TYPE','SENDFILE_STATUS'))
							->joinLeft(array('D' => 'T_TRANSACTION'),'B.TRANSACTION_ID = D.TRANSACTION_ID',array('SOURCE_ACCOUNT'))
							->where("A.SUGGEST_ID LIKE ".$this->_db->quote($suggestid))
							->where("A.EXCEPTION_TYPE LIKE ".$this->_db->quote($extype))
							->where("A.SUGGEST_STATUS LIKE '0' OR A.SUGGEST_STATUS LIKE '1'");
	
	$cek = $this->_db->fetchAll($selectCek);

  		if($reject && $this->view->hasPrivilege('EXCA'))
  		{
  			$updatesugReject['SUGGEST_STATUS'] = 'RJ';
  			$wheresugReject['SUGGEST_ID = ?'] = $suggestid;
  			$query = $this->_db->update ( "T_PSLIP_EXCEPTION_REPAIR", $updatesugReject, $wheresugReject );
  			$this->_redirect('/popup/successpopup/index');
  		}
  		
  		if($approve && $this->view->hasPrivilege('EXCA'))
  		{
			if($cek)
			{
				// $this->_db->beginTransaction();
				try
				{
					$updatesugApprove['SUGGEST_STATUS'] = 'AP';
		  			$wheresugApprove['SUGGEST_ID = ?'] = $suggestid;
		  			$query = $this->_db->update ( "T_PSLIP_EXCEPTION_REPAIR", $updatesugApprove, $wheresugApprove );
					
					if($cek[0]['TYPE']==4)
					{
						if($cek[0]['SENDFILE_STATUS']==0) // exception from source to escrow
						{
							if($cek[0]['PS_STATUS']==5) // if success
							{
								$updateApprove['PS_STATUS'] = 8; // in progress
								$updateApprove['SENDFILE_STATUS'] = 1;
								$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
								
								foreach($cek as $row)
								{
									$updatedetailApprove['TRA_STATUS'] = 2; // processed
									$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
									$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
								}
								
							}
							else // failed
							{
								$updateApprove['PS_STATUS'] = 6; // failed
								$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
						
								foreach($cek as $row)
								{
									$cekDomestic = $this->_db->select()
														->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
														->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
														->where("A.TRANSFER_TYPE != 0");
														
									$cekDomestic = $this->_db->fetchRow($cekDomestic);
									if($cekDomestic) 
									{
										$updatedetailApprove['EFT_STATUS'] = '2';
									}
									$updatedetailApprove['TRA_STATUS'] = '4';
									$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
									$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
								}
							
							}
						}
						else if($cek[0]['SENDFILE_STATUS']==2) // exception from escrow to beneficiary
						{
//							if($cek[0]['PS_STATUS']==5) // if success
//							{
//								$isExceptionReversal = false;
//								$numTrxFailed = 0;
//								foreach($cek as $row)
//								{
//									if($row['TRA_STATUS'] == '4') // ff failed, transfer reversal
//									{
//										$numTrxFailed += 1;
//									}
//									
//									$cekDomestic = $this->_db->select()
//														->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
//														->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
//														->where("A.TRANSFER_TYPE != 0");
//														
//									$cekDomestic = $this->_db->fetchRow($cekDomestic);
//									if($cekDomestic && $row['TRA_STATUS'] == '4') 
//									{
//										$updatedetailApprove['EFT_STATUS'] = '2';
//									}
//									
//									$updatedetailApprove['TRA_STATUS'] = $row['TRA_STATUS'];
//									$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
//									$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
//								}
//								
//								if($numTrxFailed>0) // DO REVERSAL
//								{
//									$updateApprove['SENDFILE_STATUS'] = 3;
//									$whereApprove['PS_NUMBER = ?'] = $slipnumb;
//									$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
//									
//									$pslip = array(
//													'ESCROW_ACC' => $row['ESCROW_ACC'],
//													'PS_CCY' => $row['PS_CCY'],
//													'ESCROW_ACC_TYPE' => $row['ESCROW_ACC_TYPE']
//												);
//									$amountRev = $this->getTotalAmountReversal($slipnumb);
//									
//									$Payment = new Payment($slipnumb);
//									
//									$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
//									if($resultReversal['ResponseCode'] != '00')
//									{
//										$isExceptionReversal = true;
//									}
//								}
//								
//								if($isExceptionReversal)
//									$updateApprove['PS_STATUS'] = 9; // exception reversal
//								else
//								{
//									$updateApprove['PS_STATUS'] = 5; // complete
//									$updateApprove['SENDFILE_STATUS'] = 4;
//								}
//								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
//								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
//								
//							}
//							else // failed
//							{
//								$updateApprove['PS_STATUS'] = 6; // failed
//								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
//								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
//						
//								foreach($cek as $row)
//								{
//									$cekDomestic = $this->_db->select()
//														->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
//														->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
//														->where("A.TRANSFER_TYPE != 0");
//														
//									$cekDomestic = $this->_db->fetchRow($cekDomestic);
//									if($cekDomestic) 
//									{
//										$updatedetailApprove['EFT_STATUS'] = '2';
//									}
//									$updatedetailApprove['TRA_STATUS'] = '4';
//									$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
//									$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
//								}
//
//								// DO REVERSAL
//								{
//									$updateApprove['SENDFILE_STATUS'] = 3;
//									$whereApprove['PS_NUMBER = ?'] = $slipnumb;
//									$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
//									
//									$pslip = array(
//													'ESCROW_ACC' => $row['ESCROW_ACC'],
//													'PS_CCY' => $row['PS_CCY'],
//													'ESCROW_ACC_TYPE' => $row['ESCROW_ACC_TYPE']
//												);
//									$amountRev = $this->getTotalAmountReversal($slipnumb);
//
//									$Payment = new Payment($slipnumb);
//									$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
//									if($resultReversal['ResponseCode'] != '00')
//									{
//										$isExceptionReversal = true;
//									}
//								}
//								
//								if($isExceptionReversal)
//									$updateApprove['PS_STATUS'] = 9; // exception reversal
//								else
//								{
//									$updateApprove['PS_STATUS'] = 6; // complete
//									$updateApprove['SENDFILE_STATUS'] = 4;
//								}
//								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
//								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
//
//							}

							
							if($cek[0]['PS_STATUS']==5) // if success
							{
								$isExceptionReversal = false;
								$numTrxFailed = 0;
								foreach($cek as $row)
								{
									if($row['TRA_STATUS'] == '4') // ff failed, transfer reversal
									{
										$numTrxFailed += 1;
									}
									
									$cekDomestic = $this->_db->select()
														->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
														->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
														->where("A.TRANSFER_TYPE != 0");
														
									$cekDomestic = $this->_db->fetchRow($cekDomestic);
									if($cekDomestic && $row['TRA_STATUS'] == '4') 
									{
										$updatedetailApprove['EFT_STATUS'] = '2';
									}
									
									$updatedetailApprove['TRA_STATUS'] = $row['TRA_STATUS'];
									$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
									$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
								}
								
								if($numTrxFailed>0) // DO REVERSAL
								{
									$updateApprove['SENDFILE_STATUS'] = 3;
									$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
									$whereApprove['PS_NUMBER = ?'] = $slipnumb;
									$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
									
									$pslip = array(
													'ESCROW_ACC' => $row['ESCROW_ACC'],
													'PS_CCY' => $row['PS_CCY'],
													'ESCROW_ACC_TYPE' => $row['ESCROW_ACC_TYPE']
												);
									$amountRev = $this->getTotalAmountReversal($slipnumb);
									
									$Payment = new Payment($slipnumb);
									
//									$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
//									if($resultReversal['ResponseCode'] != '00')
//									{
//										$isExceptionReversal = true;
//									}
								}
								
								if($isExceptionReversal)
									$updateApprove['PS_STATUS'] = 9; // exception reversal
								else
								{
									$updateApprove['PS_STATUS'] = 5; // complete
									$updateApprove['SENDFILE_STATUS'] = 4;
								}
								$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
								
							}
							else // failed
							{
								$updateApprove['PS_STATUS'] = 6; // failed
								$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
						
								foreach($cek as $row)
								{
									$cekDomestic = $this->_db->select()
														->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
														->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
														->where("A.TRANSFER_TYPE != 0");
														
									$cekDomestic = $this->_db->fetchRow($cekDomestic);
									if($cekDomestic) 
									{
										$updatedetailApprove['EFT_STATUS'] = '2';
									}
									$updatedetailApprove['TRA_STATUS'] = '4';
									$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
									$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
								}

								// DO REVERSAL
								{
									$updateApprove['SENDFILE_STATUS'] = 3;
									$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
									$whereApprove['PS_NUMBER = ?'] = $slipnumb;
									$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
									
									$pslip = array(
													'ESCROW_ACC' => $row['ESCROW_ACC'],
													'PS_CCY' => $row['PS_CCY'],
													'ESCROW_ACC_TYPE' => $row['ESCROW_ACC_TYPE']
												);
									$amountRev = $this->getTotalAmountReversal($slipnumb);

									$Payment = new Payment($slipnumb);
//									$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
//									if($resultReversal['ResponseCode'] != '00')
//									{
//										$isExceptionReversal = true;
//									}
								}
								
								if($isExceptionReversal)
									$updateApprove['PS_STATUS'] = 9; // exception reversal
								else
								{
									$updateApprove['PS_STATUS'] = 6; // complete
									$updateApprove['SENDFILE_STATUS'] = 4;
								}
								$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );

							}
						}
						else if($cek[0]['SENDFILE_STATUS']==3) // exception on reversal
						{
							if($cek[0]['PS_STATUS']==1) // if resend reversal
							{
								$pslip = array(
												'ESCROW_ACC' => $cek[0]['ESCROW_ACC'],
												'PS_CCY' => $cek[0]['PS_CCY'],
												'ESCROW_ACC_TYPE' => $cek[0]['ESCROW_ACC_TYPE']
											);
								$amountRev = $this->getTotalAmountReversal($slipnumb);

								$Payment = new Payment($slipnumb);
								$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
								if($resultReversal['ResponseCode'] == '00')
								{
									$updateApprove['PS_STATUS'] = 5; // complete
									$updateApprove['SENDFILE_STATUS'] = 4;
								}
								else{
									$updateApprove['PS_STATUS'] = 9; // exception reversal
								}
								
								$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');	
								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
								
							}
							else if($cek[0]['PS_STATUS']==5) // if success
							{
								$updateApprove['PS_STATUS'] = 5; // complete
								$updateApprove['SENDFILE_STATUS'] = 4;
								$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
							}
						}
					}
					else
					{
						$updateApprove['PS_STATUS'] = $cek[0]['PS_STATUS'];
						$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
						$whereApprove['PS_NUMBER = ?'] = $slipnumb;
						$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
						
						$cekDomestic = $this->_db->select()
											->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
											->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($cek[0]['TRANSACTION_ID']))
											->where("A.TRANSFER_TYPE NOT LIKE '0'");
											
						$cekDomestic = $this->_db->fetchRow($cekDomestic);
						
						foreach($cek as $row)
						{
							if($cekDomestic && $row['TRA_STATUS'] == '4') 
							{
								$updatedetailApprove['EFT_STATUS'] = '2';
							}
							$updatedetailApprove['TRA_STATUS'] = $row['TRA_STATUS'];
							$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
							$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
						}
						
						$insertHistory['DATE_TIME'] = new Zend_Db_Expr('now()');
						$insertHistory['PS_NUMBER'] = $slipnumb;
						$insertHistory['USER_LOGIN'] = $this->_userIdLogin;
						$insertHistory['CUST_ID'] = $cek[0]['CUST_ID'];
						$insertHistory['HISTORY_STATUS'] = '5';
						$insertHistory['PS_REASON'] = 'Payment Exception Repair. Request Approved';
						$this->_db->insert('T_PSLIP_HISTORY', $insertHistory);
						
						// $selectQuery	= "SELECT PS_STATUS,TRA_AMOUNT,SOURCE_ACCOUNT FROM T_PSLIP P INNER JOIN T_TRANSACTION T ON P.PS_NUMBER = T.PS_NUMBER 
						// WHERE P.CUST_ID = ".$this->_db->quote($cek[0]['CUST_ID'])." AND T.PS_NUMBER = ".$this->_db->quote($slipnumb);
			
						// $resultQuery =  $this->_db->fetchAll($selectQuery);
				
						// if(is_array($resultQuery)){
							// foreach ($resultQuery as $key => $data) {
								// if($data['PS_STATUS']!='3'){
									// $sourceAccount =  $data['SOURCE_ACCOUNT'];
									// $holdAmountRelease = $data['TRA_AMOUNT'];
									// $systemBalance =  new systemBalance($cek[0]['CUST_ID'], $sourceAccount);
									// $systemBalance->releaseAmountHold($holdAmountRelease,$slipnumb);
								// }
							// }
						// }
					}
					// $this->_db->commit();
					$this->_redirect('/popup/successpopup/index');
					
				}
				catch(Exception $e)
				{
					// $this->_db->rollBack();					
				}
			}
				 			
			
  		}
  		
  		$caseStatus = "(CASE B.PS_STATUS ";
  		foreach($paycashmstatusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";
  			
  		$caseStatus2 = "(CASE C.TRA_STATUS ";
  		foreach($trastatusarr as $key=>$val)
  		{
   			$caseStatus2 .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus2 .= " END)";
  			
  		$caseStatus3 = "(CASE E.PS_STATUS ";
  		foreach($paycashmstatusarr as $key=>$val)
  		{
   			$caseStatus3 .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus3 .= " END)";
  			
  		$caseStatus4 = "(CASE F.TRA_STATUS ";
  		foreach($trastatusarr as $key=>$val)
  		{
   			$caseStatus4 .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus4 .= " END)";
  			
  		$caseStatus5 = "(CASE E.SUGGEST_STATUS ";
  		foreach($sugstatusarr as $key=>$val)
  		{
   			$caseStatus5 .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus5 .= " END)";

  		$select = $this->_db->select()->distinct()
					        ->from(array('B' => 'T_PSLIP'),array())
					        ->join(array('C' => 'T_TRANSACTION'), 'B.PS_NUMBER = C.PS_NUMBER',array())
					        ->join(array('D' => 'M_CUSTOMER'), 'B.CUST_ID = D.CUST_ID',array())
					        ->join(array('E' => 'T_PSLIP_EXCEPTION_REPAIR'), 'B.PS_NUMBER = E.PS_NUMBER',array())
					        ->joinLeft(array('F' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'), 'C.TRANSACTION_ID = F.TRANSACTION_ID AND E.SUGGEST_ID = F.SUGGEST_ID',array('payref'		=>	'B.PS_NUMBER',
																					        																	'paystatus'		=>	$caseStatus,
																					        																	'custid'		=>	'D.CUST_ID',
																				        																		'custname'		=>	'D.CUST_NAME',
																				        																		'paysubject'	=>	'B.PS_SUBJECT',
																					        																	'benefacct'		=>	'C.BENEFICIARY_ACCOUNT',
															        																							'benefccy'		=>	'C.BENEFICIARY_ACCOUNT_CCY',
															        																							'benefname'		=>	'C.BENEFICIARY_ACCOUNT_NAME',
															        																							'benefalname'	=>	'C.BENEFICIARY_ALIAS_NAME',
																					        																	'created'		=>	'B.PS_CREATED',
																					        																	'updated'		=>	'B.PS_UPDATED',
																					        																	'paydate'		=>	'B.PS_EFDATE',
																					        																	'releaser'		=>	'B.PS_RELEASER_USER_LOGIN',
																					        																	'ccode'			=>	'B.PS_RELEASER_CHALLENGE',
																	        																					'benefccy'		=>	'C.BENEFICIARY_ACCOUNT_CCY',
																	        																					'payamt'		=>	'B.PS_TOTAL_AMOUNT',
																	        																					'transid'		=>	'C.TRANSACTION_ID',
																	        																					'sourceacct'	=>	'C.SOURCE_ACCOUNT',
																	        																					'sourceccy'		=>	'C.SOURCE_ACCOUNT_CCY',
															        																							'sourcename'	=>	'C.SOURCE_ACCOUNT_NAME',
															        																							'sourcealname'	=>	'C.SOURCE_ACCOUNT_ALIAS_NAME',
																	        																					'transamt'		=>	'C.TRA_AMOUNT',
																	        																					'message'		=>	'C.BANK_RESPONSE',
																	        																					'trastatus'		=>	$caseStatus2,
															        																							'suggestor'		=>	'E.SUGGEST_USER',
															        																							'suggestdate'	=>	'E.SUGGEST_DATE',
															        																							'granter'		=>	'E.SUGGEST_GRANTER',
															        																							'grantdate'		=>	'E.SUGGEST_GRANTDATE',
															        																							'rejector'		=>	'E.SUGGEST_REJECTOR',
															        																							'rejectdate'	=>	'E.SUGGEST_REJECTDATE',
															        																							'paysugstatus'	=>	$caseStatus3,
															        																							'trasugstatus'	=>	$caseStatus4,
															        																							'sugstatus'		=>	$caseStatus5,
															        																							'sugstatuscode'	=>	'E.SUGGEST_STATUS',
															        																							'psstatus'	=>	'E.PS_STATUS',
															        																							'type'			=>	'E.TYPE',
																																								'reversal'			=>	'B.REVERSAL_STATUS',
																																								'sendfile'			=>	'B.SENDFILE_STATUS'));
		$select -> where("B.PS_NUMBER LIKE ".$this->_db->quote($slipnumb));
		$select -> where("B.PS_STATUS LIKE '9'");
		$select -> where("E.SUGGEST_ID LIKE ".$this->_db->quote($suggestid));
  		$select -> where("E.SUGGEST_STATUS = '0' OR E.SUGGEST_STATUS = '1'");
  		// echo $select;
		$result = $select->query()->FetchAll();
		$cektotal = 0;
		foreach($result as $total)
		{
			$cektotal += $total['transamt'];
		}
		
		/* $this->view->escrowAcct = '';
		if($result[0]['type'] == 4 && $result[0]['sendfile'] == 0)
		{
			$escrow = $this->_db->fetchRow(
								$this->_db->select()
													->from('M_MASTER',array('M_KEY1','M_KEY2','M_KEY3'))
													->where("M_KEY = 'ESCROW'")
			);
			$this->view->escrowAcct = $escrow['M_KEY2']." [".$escrow['M_KEY1']."] - ".$escrow['M_KEY3'];
		} */
		$this->view->result = $result;
		$this->view->total = $cektotal;
		$this->_helper->viewRenderer('exceptionchangeslistdetail/cashm', null, true);
  	
  	
		if($result[0]['sugstatuscode'] == 0)
		{
			$updatesugStatus['SUGGEST_STATUS'] = 1;
			$wheresugStatus['SUGGEST_ID = ?'] = $suggestid;
			$query = $this->_db->update ( "T_PSLIP_EXCEPTION_REPAIR", $updatesugStatus, $wheresugStatus );
		}
	}
	
	private function getTotalAmountReversal($ps_number)
	{
		$sql = "SELECT SUM(TRA_AMOUNT) AS TOTAL FROM T_TRANSACTION 
					WHERE PS_NUMBER='".$ps_number."' AND TRA_STATUS=4";
		return $this->_db->fetchOne($sql);
	}
}