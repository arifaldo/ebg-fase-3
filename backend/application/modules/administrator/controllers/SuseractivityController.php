<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class administrator_SuseractivityController extends Application_Main
{
	public function indexAction()
	{
		$fields = array(
						'LOG_DATE'      	=> array('field' => 'LOG_DATE',
											      'label' => 'Date',
											      'sortable' => true),
						'SUSER_ID'      	=> array('field' => 'SUSER_ID',
											      'label' => 'Super User Login',
											      'sortable' => true),
						'ACTION_FULLDESC'  	=> array('field' => 'ACTION_FULLDESC',
											      'label' => 'Activity Type',
											      'sortable' => true));
				      
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','LOG_DATE');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array('filter' => array('StripTags','StringTrim'),
						   'fDateFrom' => array('StripTags','StringTrim'),
						   'fDateTo' => array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array('filter'			 	=> array(),
						   'fDateFrom' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );               
	    
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
		 $datefrom = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		 $dateto = html_entity_decode($zf_filter->getEscaped('fDateTo'));
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		
		$select2 = $this->_db->select()
					        ->from(array('A' => 'T_SACTIVITY'),array('LOG_DATE',
					        										 'SUSER_ID',
					        										 'ACTION_FULLDESC'));
					        
	if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}
		
		if($filter =='Set Filter')
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
	            }
		
		
		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(LOG_DATE) >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(LOG_DATE) <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(LOG_DATE) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}
		$select2->order($sortBy.' '.$sortDir);   
		$this->paging($select2);
		$result = $select2->query()->FetchAll();
		$arr = $this->_db->fetchAll($select2);
		//Zend_Debug::dump($arr); die;
		
		if($csv)
		{
			$this->_helper->download->csv(array('Date','Super User Login','Activity Type'),$result,null,'Super User Activity Log');
		}
		
		if($pdf)
		{
			$this->_helper->download->pdf(array('Date','Super User Login','Activity Type'),$result,null,'Super User Activity Log');
		}
	 
	    $result = $select2->query()->FetchAll();
	//Zend_Debug::dump($result);die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		}
	}
?>
