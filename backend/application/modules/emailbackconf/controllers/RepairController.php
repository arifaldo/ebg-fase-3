<?php
require_once 'Zend/Controller/Action.php';

class Welcome_RepairController extends Application_Main {
	protected $_changesID;
	protected $_changesInfo;
	protected $_ccyArr=array();
	protected $_moduleDB  = 'WLT';
	
	public function indexAction() {
		$this->_helper->viewRenderer->setNoRender();
		$clause = $this->_db->quoteInto('CUST_ID = ?',(string)$this->_custIdLogin);
		$filters = array('changes_id' => array('StringTrim'));
							 
		$validators =  array(
			'changes_id' => array('NotEmpty','Digits', //group code may not be empty
									array('Db_RecordExists',array('table'=>'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude'=>$clause)),
									array('Db_RecordExists', array('table' => 'TEMP_SETTING', 'field' => 'CHANGES_ID')),
								   'messages' => array(
								   					   $this->getErrorRemark('01','Suggestion ID'), 
								   					   $this->getErrorRemark('04','Suggestion ID'),
								   					   $this->getErrorRemark('22','Suggestion ID'),
								   					   $this->getErrorRemark('22','Suggestion ID'),
								   					   ) 
								   ),
			);
					
		if (array_key_exists('changes_id',$this->_request->getParams())){

			
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
		
			if($zf_filter_input->isValid()) {
				
				$this->_changesID  = ($zf_filter_input->changes_id)? $zf_filter_input->changes_id : $this->_request->getParam('changes_id');
			
        		$this->_changesInfo = 
							$this->_db->fetchRow(
								$this->_db->select()
												->from(array('G' => 'T_GLOBAL_CHANGES'), '*')
												->where("KEY_VALUE=?", "WLT")
												->where("G.CHANGES_ID=?", $this->_changesID)
							);

//				if($this->_changesInfo['CHANGES_TYPE']==$this->_changeType['code']['new']){
//					$this->_redirect("/".$this->_request->getModuleName()."/repair/repairnew/changes_id/$this->_changesID");
				if($this->_changesInfo['CHANGES_TYPE']==$this->_changeType['code']['edit']){
					$this->_redirect("/".$this->_request->getModuleName()."/repair/repairedit/changes_id/$this->_changesID");
//				}elseif($this->_changesInfo['CHANGES_TYPE']==$this->_changeType['code']['activate']){
//					$this->_redirect("/".$this->_request->getModuleName()."/repair/repairedit/changes_id/$this->_changesID");
//				}elseif($this->_changesInfo['CHANGES_TYPE']==$this->_changeType['code']['deactivate']){
//					$this->_redirect("/".$this->_request->getModuleName()."/repair/repairedit/changes_id/$this->_changesID");
				}else{
					$errorMsg = $this->getErrorRemark('22','Suggestion ID');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
					$this->_redirect('/'.$this->_request->getModuleName().'/repair/failed');
				}
				
			}
			else{
				$this->view->error = true;				 
				$errors = $zf_filter_input->getMessages();
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				
				foreach($errors as $key => $error){
					foreach($error as $key2 => $errorMsg){
						$this->_helper->getHelper('FlashMessenger')
								->addMessage("$errorMsg");
					}					
				}
					
				$this->_redirect('/'.$this->_request->getModuleName().'/repair/failed');
			}
		}
		else{
			$errorMsg = $this->getErrorRemark('01','Suggestion ID');
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
			$this->_redirect('/'.$this->_request->getModuleName().'/repair/failed');
		}
	}
	
	public function repairnewAction(){
	}
	
	public function repaireditAction(){
		$error_remark = null;
		$clause = "CHANGES_STATUS IN ('WA','RR')";
		$this->view->disableFlag = true;
		//$this->_helper->layout()->setLayout('popup');
		$filters = array('changes_id' => array('StringTrim'));
		$validators =  array('changes_id' => array(
										'NotEmpty', //group code may not be empty
										'Digits',
										array('Db_RecordExists',array('table'=>'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude'=>$clause)),
								   		array('Db_RecordExists', array('table' => 'TEMP_SETTING', 'field' => 'CHANGES_ID')),
								   		'messages' => array(
								   							$this->getErrorRemark('01','Suggestion ID'), 
								   							$this->getErrorRemark('04','Suggestion ID'),
								   					   		$this->getErrorRemark('22','Suggestion ID'),
								   					   		$this->getErrorRemark('22','Suggestion ID'),
								   					   )
								   ),
						);
					
		if (array_key_exists('changes_id',$this->_request->getParams())){

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
		
			if($zf_filter_input->isValid()) {
				$this->_changesID  = ($zf_filter_input->changes_id)? $zf_filter_input->changes_id : $this->_request->getParam('changes_id');
				$this->_changesInfo = 
							$this->_db->fetchRow(
								$this->_db->select()
												->from('T_GLOBAL_CHANGES')
												->where("CHANGES_TYPE='E'")
												->where("CHANGES_FLAG='B'")
                             					->where('CREATED_BY=?',(string)$this->_userIdLogin)
												->where('CHANGES_ID=?', $this->_changesID)
												->where("KEY_FIELD='MODULE_ID'")
												->where("KEY_VALUE='WLT'")
							);
							
				if(!count($this->_changesInfo)){
					$errorMsg = $this->getErrorRemark('22','Suggestion ID');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
					$this->_redirect('/'.$this->_request->getModuleName().'/repair/failed');
				}
			
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());
				$zf_filter_input->setDefaultEscapeFilter(array('StringTrim','StripTags'));
			
				if($zf_filter_input->isValid()) {
				    $select = $this->_db->select()
						             ->from(array('TEMP_SETTING'))
									->where("CHANGES_ID = ?", $this->_changesID)
									->where("MODULE_ID = ?", 'WLT')
	                    			->query()->fetchAll();
	                    			
	                    $changesId = $select[0]['CHANGES_ID'];			
	                	$this->view->suggestedText = $select[0]['SETTING_VALUE'];
	                	$this->view->welcomeTempId = $select[0]['TEMP_ID'];						        
							        
					if($this->_request->isPost()){
						$filters = array('welcometext' => array('StringTrim', new Zend_Filter_StripTags()));			
						$validators =  array('welcometext' => array('allowEmpty'=>true, array('Between', array('min' => 0, 'max' => 5000)), 'messages' => $this->getErrorRemark('04','WLT')));
						$customError = array();
									
						$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getPost(),$this->_optionsValidator);
						//$zf_filter_input->setDefaultEscapeFilter(new Zend_Filter_StripTags());
						
						$newWelcomeText = $zf_filter_input->getEscaped('welcometext');
						$welcomeTempId = $this->_request->getParam('welcometempid');
						
						if($zf_filter_input->isValid()) {
			
							try {
								$this->_db->beginTransaction();
								$this->_db->update('T_GLOBAL_CHANGES', array('CHANGES_STATUS'=>'WA'), "CHANGES_ID=$changesId");	
								$this->_db->update('TEMP_SETTING', array('SETTING_VALUE'=>$newWelcomeText), "TEMP_ID=$welcomeTempId");
								$this->_db->commit();
							}catch(Exception $e) {
								$this->_db->rollBack();
								SGO_Helper_GeneralLog::technicalLog($e);
								$error_remark = $this->getErrorRemark('82');
							}
							//if we got here, this means no problem during try block, commit transaction
							
							if($error_remark){
								$this->_helper->getHelper('FlashMessenger')->addMessage('F');
								$this->_helper->getHelper('FlashMessenger')
										->addMessage($error_remark);
							}else{
								$this->_helper->getHelper('FlashMessenger')->addMessage('S');
								$this->_helper->getHelper('FlashMessenger')
										->addMessage($this->getErrorRemark('00','Welcome Text'));
							}
							
							//insert log
							try {
	  							$this->_db->beginTransaction();
	  							$fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($this->_request->getPost());
	  							$this->backendLog('R',$this->_moduleDB,$this->_moduleDB,$fulldesc,$error_remark);
	  							$this->_db->commit();
							}
    						catch(Exception $e){
 	  							$dbObj->rollBack();
	  							SGO_Helper_GeneralLog::technicalLog($e);
							}
	                        $this->_redirect($this->_backURL);
	                        
							$this->view->disableFlag = true;
                        	$this->view->disableSuccess = true;
							$this->view->suggestedText = $newWelcomeText;
							$msg_success = $this->getErrorRemark('00', 'Welcome Text');
							$this->_helper->getHelper('FlashMessenger')->addMessage($msg_success);
						}else{
							$errors = $zf_filter_input->getMessages ();
							if(count($errors))$error_remark = SGO_Helper_GeneralFunction::displayErrorRemark($errors);
						}
					}
					
					$this->view->wltError = (isset($errors['welcometext']))? $errors['welcometext'] : null;
					$this->view->customError = (isset($customError['custom']))? $customError['custom'] : null;
						
				}else{
				$select = $this->_db->select()
					             ->from(array('TEMP_SETTING'))
								->where("TEMP_ID = ?",$this->_changesInfo['KEY_VALUE'])
                    			->query()->fetchAll();
						        if(is_array($select))
						        	$this->view->suggestedText = $select[0]['SETTING_VALUE'];
				
				$this->view->error = true;				 
				$errors = $zf_filter_input->getMessages();
			
				//assign error messages
				$this->view->groupCodeErr = (isset($errors['group_code']))? $errors['group_code'] : null;
								
			}

       		//insert log
			try {
	  			$this->_db->beginTransaction();
	  			if($this->_request->isPost()){
	    			$action = 'R'; $fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($this->_request->getPost());
	  			}else{ $action = 'V'; $fulldesc = 'CHANGES_ID:'.$this->_changesID; }
	  			$this->backendLog($action,$this->_moduleDB,$this->_moduleDB,$fulldesc,$error_remark);
	  			$this->_db->commit();
			}
    		catch(Exception $e){
 	  			$this->_db->rollBack();
	  			SGO_Helper_GeneralLog::technicalLog($e);
			}
			$this->view->inputParam = $this->getRequest()->getParams();
			}else{
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error){
					foreach($error as $key2 => $errorMsg){
						$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
					}					
				}	
				$this->_redirect('/'.$this->_request->getModuleName().'/repair/failed');
			}
		}else{
			$errorMsg = $this->getErrorRemark('01','Suggestion ID');
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
			$this->_redirect('/'.$this->_request->getModuleName().'/repair/failed');
		}
	}
	
	public function successAction(){
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction(){
		$this->_redirect($this->_backURL);
	}
}
