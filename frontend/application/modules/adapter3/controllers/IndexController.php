<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/Payment.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class adapter3_IndexController extends Application_Main
{
    protected $_moduleDB = 'RTF'; //masih harus diganti

    protected $_destinationUploadDir = '';
    protected $_maxRow = '';

    public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

    public function indexAction()
    {
        $this->setbackURL();
        $this->_helper->_layout->setLayout('newlayout');
        $settingObj = new Settings();
        $this->view->THRESHOLD_LLD      = $settingObj->getSetting("threshold_lld"   , 0);

        $this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        $AccArr       = $CustomerUser->getAccounts();
        $this->view->AccArr =  $AccArr;
        $listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');

        if($this->_request->isPost() )
        {
            $param = $this->_request->getParams();
            // $data = $this->_request->getParams();
// print_r($data);die;


            $filter = new Application_Filtering();
            $confirm = false;
            $error_msg[0] = "";

            $PS_SUBJECT     = $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
            // $PS_EFDATE      = $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
            // $ACCTSRC        = $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");

            // if(!$ACCTSRC)
            // {
            //     $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Source Account cannot be left blank.').'';
            //     $this->view->error      = true;
            //     $this->view->report_msg = $this->displayError($error_msg);
            // }
            // else if(!$PS_EFDATE)
            // {
            //     $error_msg[0] = 'Error: Payment Date can not be left blank.';
            //     $this->view->error      = true;
            //     $this->view->report_msg = $this->displayError($error_msg);
            // }
            // else
            // {

                if ($param['fixLengthSubmit']) {

                    $sessionNamespace = new Zend_Session_Namespace('fixLengthData');
                    $fileName = $sessionNamespace->fileName;
                    $fixLengthType = $sessionNamespace->fixLengthType;
                    $fileContents = $sessionNamespace->fileContents;

                    $label = $param['label'];

                    // if with header
                    if ($fixLengthType == 1) {
                        //karena yg diambil hanya order dri row ke 2 saja
                        $contentOrder = $param['row2'];
                        $startArrIndex = 1;
                        $surplusIndex = 0;
                    }
                    else if ($fixLengthType == 3) {
                        //karena yg diambil hanya order dri row ke 1 saja
                        $contentOrder = $param['row1'];
                        $startArrIndex = 0;
                        $surplusIndex = 1;
                    }

                    $contentOrderArr = explode(',', $contentOrder);

                    if (count($contentOrderArr) > 1) {
                        foreach ($fileContents as $key => $value) {
                            if ($key >= $startArrIndex) {
                                foreach ($contentOrderArr as $key2 => $value2) {
                                    //first order, startIndex from 0
                                    if ($key2 == 0) {
                                        $startIndex = 0;
                                        $endIndex = (int) $value2 + 1;

                                        $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                    }
                                    else{
                                        $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                        $endIndex = (int) ($value2 - ($startIndex - 1));

                                        $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                    }
                                }       
                            }   
                        }
                    }

                    foreach ($label as $key => $labels) {
                        $data[0][] = $labels;
                    }

                    $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                    $sessionNamespace->content = $data;
                    $sessionNamespace->fileName = $fileName;
                    $sessionNamespace->mtFile = false;
                    $this->_redirect('/adapter3/index/confirm');  
                }


                $paramSettingID = array('range_futuredate', 'auto_release_payment');

                $settings = new Application_Settings();
                $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
                $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'txt', 'json', 'xml', 'xls', 'xlsx'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv, *.json, *.xml, *.txt, *.xls, *.xlsx'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));

                $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);

                $extension = explode('.', $sourceFileName);

                $extensionName = $extension[1];

                $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                $adapter->addFilter ( 'Rename',$newFileName  );

                $mtFile = false;

                if ($adapter->isValid ())
                    {

                    if ($adapter->receive()) {
                         
                        $file_contents = file_get_contents($newFileName);

                        //if csv occured
                        if ($extensionName === 'csv') {

                             //PARSING CSV HERE
                            //if advanced option
                            if ($param['advancedOption'] == 1 && $param['radType'] == 1) {
                                $data = $this->_helper->parser->parseCSV($newFileName, $param['delimitedWith']);
                            }
                            //if fix length
                            else if ($param['advancedOption'] == 1 && $param['radType'] == 2) {

                                $file_contents = file($newFileName);

                                if (!empty($file_contents)) {
                                    $this->view->fixLength = true;

                                    //delete space in file
                                    foreach ($file_contents as $contents) {
                                        $newFileContents[] = str_replace(' ', '_', $contents);
                                    }
                                    $this->view->fileContents = $newFileContents;

                                    $this->view->fixLengthType = $param['fixLengthType'];

                                    $sessionNamespace = new Zend_Session_Namespace('fixLengthData');
                                    $sessionNamespace->fileName = $sourceFileName;
                                    $sessionNamespace->fixLengthType = $param['fixLengthType'];
                                    $sessionNamespace->fileContents = $file_contents;
                                }
                                else{
                                    $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File are empty').'.';
                                    $this->view->error      = true;
                                    $this->view->report_msg = $this->displayError($error_msg);
                                }                                
                            }
                            else{
                                $data = $this->_helper->parser->parseCSV($newFileName);
                            }
                            
                        }
                        //if txt occured
                        else if ($extensionName === 'txt') {

                           $lines = file($newFileName);
                        
                           $checkMt940 = false;
                           $checkMt101 = false;
                           foreach ($lines as $line) {
                               if (strpos($line, '{1:') !== false) {
                                   $checkMt940 = true;
                               }
                               else if (strpos($line, ':20:') !== false) {
                                   $checkMt101 = true;
                               }
                           }

                           //if mt940 format
                           if ($checkMt940) {
                               
                                $data = $this->_helper->parser->mt940($newFileName);
                                $mtFile = true;
                           }
                           else if($checkMt101){
                                $data = $this->_helper->parser->mt101($newFileName);
                                $mtFile = true;
                           }
                           else{

                                if ($param['advancedOption'] == 1 && $param['radType'] == 1) {
                                    $delimited = $param['delimitedWith'];
                                     //parse csv jg bisa utk txt
                                    $data = $this->_helper->parser->parseCSV($newFileName, $delimited);
                                }
                                //if fix length
                                else if ($param['advancedOption'] == 1 && $param['radType'] == 2) {

                                    if (!empty($lines)) {
                                        $this->view->fixLength = true;

                                        //delete space in file
                                        foreach ($lines as $contents) {
                                            $newFileContents[] = str_replace(' ', '_', $contents);
                                        }
                                        $this->view->fileContents = $newFileContents;

                                        $this->view->fixLengthType = $param['fixLengthType'];

                                        $sessionNamespace = new Zend_Session_Namespace('fixLengthData');
                                        $sessionNamespace->fileName = $sourceFileName;
                                        $sessionNamespace->fixLengthType = $param['fixLengthType'];
                                        $sessionNamespace->fileContents = $file_contents;
                                    }
                                    else{
                                        $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File are empty').'.';
                                        $this->view->error      = true;
                                        $this->view->report_msg = $this->displayError($error_msg);
                                    }                                
                                }
                                else{
                                    //parse csv jg bisa utk txt
                                    $data = $this->_helper->parser->parseCSV($newFileName, ',');
                                }
                           }
                        }
                        //if json occured
                        else if ($extensionName === 'json') {

                            $datajson = json_decode($file_contents, 1);
                            $i = 0;
                            foreach ($datajson as $key => $value) {
                                if ($i == 0) {
                                    $data[$i] = array_keys($value);
                                    $data[$i + 1] = array_values($value);
                                }
                                else{
                                    $data[$i+1] = array_values($value);
                                }

                                $i++;
                            }
                        }
                        //if xml occured
                        else if($extensionName === 'xml'){

                            header('Content-Type: text/plain');
                            $file_contents = file_get_contents($newFileName);

                            $xml = (array) simplexml_load_string($file_contents);

                            // print_r($xml);die();
                             $i = 0;
                            foreach ($xml as $key => $value) {
                                foreach ($value as $key2 => $value2) {
                                    if ($i == 0) {
                                        $data[$i] = array_keys((array)$value2);
                                        $data[$i + 1] = array_values((array)$value2);
                                    }
                                    else{
                                        $data[$i+1] = array_values((array)$value2);
                                    }

                                    $i++;
                                }
                            }

                            for($i=0; $i<count($data); $i++){
                                if ($i > 0) {
                                    foreach ($data[$i] as $key => $value) {
                                        if (empty($value)) {
                                            $data[$i][$key] = null;
                                        }
                                    }
                                }
                            }
                        }
                        else if($extensionName === 'xls' || $extensionName === 'xlsx'){
                            try {
                                $inputFileType = IOFactory::identify($newFileName);
                                $objReader = IOFactory::createReader($inputFileType);
                                $objPHPExcel = $objReader->load($newFileName);
                            } catch(Exception $e) {
                                die('Error loading file "'.pathinfo($newFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                            }

                            $sheet = $objPHPExcel->getSheet(0);
                            $highestRow = $sheet->getHighestRow();
                            $highestColumn = $sheet->getHighestColumn();
                             
                            for ($row = 1; $row <= $highestRow; $row++){                        
                                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

                                $data[$row - 1] = $rowData[0];
                            }
                        }
                       
                        @unlink($newFileName);

                        //if not fix length
                        if ($param['radType'] != 2) {
                            
                            if(!empty($data))
                            {
                                $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                                $sessionNamespace->content = $data;
                                $sessionNamespace->fileName = $sourceFileName;
                                $sessionNamespace->mtFile = $mtFile;
                                $this->_redirect('/adapter3/index/confirm');  
                            }
                            else //kalo total record = 0
                            {
                                $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formats or Delimited String').'.';
                                $this->view->error      = true;
                                $this->view->report_msg = $this->displayError($error_msg);
                            }
                        }
                    }
                }
                else
                {
                    $this->view->error = true;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                        if($key=='fileUploadErrorNoFile')
                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                        else
                            $error_msg[0] = $val;
                        break;
                    }
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;
                }   

            if($confirm)
            {
                $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                $sessionNamespace->content = $csvData2;
                $this->_redirect('/adapter3/index/confirm');
            }

            $this->view->PSSUBJECT = $PS_SUBJECT;
            $this->view->ACCTSRC = $ACCTSRC;
            $this->view->PSEFDATE = $PS_EFDATE;
        }
        Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
        Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
    }

    public function confirmAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
        $data = $sessionNamespace->content;
        $fileSourceName = $sessionNamespace->fileName;
        $mtFile = $sessionNamespace->mtFile;
        $this->view->refresh = '0';
        $this->view->data = $data;
        $this->view->mtFile = $mtFile;

        //if not mt file (mt940, mt101)
        if (!$mtFile) {
            $data[0] = array_filter($data[0]);
            //echo '<pre>';
            //var_dump($data[0]);
            $this->view->total = count($data[0]);
            $this->view->headerData = $data[0];
            //var_dump(count($data[0]));die;
            $this->view->headerCount = count($data[0]);

            $halfHeaderCount = (int) (count($data[0]) / 2);

            $this->view->halfHeaderCount = $halfHeaderCount;

            unset($data[0]);
            $this->view->valueData  = $data;   

            $selectTag = $this->_db->select()
                     ->distinct()
                     ->from(array('T' => 'M_MAPPING'), 'T.TAG');

            $dataTag = $this->_db->fetchAll($selectTag);

            $opt = '<option value="autonumber">Auto Numbering</option>
                    <option value="uid">Unique ID</option>';

            foreach ($dataTag as $key => $value) {
                $opt .= '<option value="'.$value['TAG'].'_mapping">'.$value['TAG'].' Auto Mapping</option>'; 
            }

            $this->view->functionOpt = $opt;
        }
        else{
            $data[0] = array_filter($data[0]);
            echo '<pre>';
            var_dump($data[0]);die;
             $this->view->headerData = array_keys($data[0]);
        }

        if($this->_request->isPost()  )
        {

            $params = $this->_request->getParams();

            //-------------------------------------code baru----------------------------------------

            $filter = array(
               'fileType' => array('StripTags','StringTrim'),
               // 'headerOrder' => array('StripTags','StringTrim'),
            );
            
            $options = array('allowEmpty' => FALSE);
            
            $validators = array(
                'fileType'      => array(),
                // 'headerOrder'   => array()
            );

            $filter  = new Zend_Filter_Input($filter, $validators, $params, $options);

            $headerCounter = $params['headerCounter'];

            if ($filter->isValid() && $headerCounter >= 0){

                //refresh variable value yang dipakai diatas
                unset($value);

                $type = $filter->fileType;
                $fileName = $params['fileName'];

                if (empty($fileName)) {
                    $explodeFileName = explode('.', $fileSourceName);
                    $fileName = $explodeFileName[0].'_new setup file';
                }

                // print_r($data);die();

                if (!$mtFile) {
                    $label = $params['label'];
                    $content = $params['content'];
                    $function = $params['function'];

                    foreach ($label as $key => $value) {

                        $labels[] = $label[$key];

                        $headerContent = explode(',', $content[$key]);

                        foreach ($data as $key2 => $value2) {

                            if (count($headerContent) >= 2) {

                                $contentString = '';
                                $counterString = 0;
                                foreach ($headerContent as $key3 => $values3) {

                                    //kasih spasi
                                    if ($counterString !== count($headerContent)) {   
                                        $contentString .= $data[$key2][$values3].'  ';  
                                    }
                                    else{
                                        $contentString .= $data[$key2][$values3];
                                    }

                                    $counterString++;
                                }

                                if ($type == 'json' || $type == 'xml'){

                                    $contents[$key2-1][$value] = $contentString;
                                }else{

                                    $contents[$key2-1][] = $contentString;
                                }
                            }
                            else{
                                if ($type == 'json' || $type == 'xml'){

                                    $contents[$key2-1][$value] = $data[$key2][$content[$key]];
                                }
                                else{

                                    $contents[$key2-1][] = $data[$key2][$content[$key]];
                                }
                            }
                        }
                    }
                }

                //if mt file (mt940, mt101)
                else{

                    $labels = array_keys($data[0]);

                    if ($type == 'json' || $type == 'xml'){

                        $contents = $data;

                    }
                    else{

                        foreach ($data as $key => $value) {
                            foreach ($value as $key2 => $value2) {
                                $contents[$key][] = $value2;
                            }
                        }
                    }
                }

                // print_r($labels);
                // echo '<br>';
                // print_r($contents);
                // echo '<br>';
                // die(); 



                //-------------------------------------code lama----------------------------------------
                
                $header = $labels;
                $dataValue = $contents;

                if($type == 'json'){
                    $json_dataValue = json_encode($dataValue);
                }
                else if($type == 'xml'){

                    foreach ($dataValue as $key => $value) {
                        foreach ($value as $key2 => $value2) {
                            if (strpos($key2, ' ') !== false) {
                                $newKey = str_replace(' ', '_', $key2);
                                $newDataValue[$key][$newKey] = $value2;
                                unset($dataValue[$key][$key2]);
                                $merged_dataValue[$key] = array_merge($newDataValue[$key], $dataValue[$key]);
                            }
                        }
                    }

                    if (!empty($merged_dataValue)) {
                        $final_dataValue = $merged_dataValue;
                    }
                    else{
                        $final_dataValue = $dataValue;
                    }

                    // creating object of SimpleXMLElement
                    $xml_data = new SimpleXMLElement('<?xml version="1.0"?><Data></Data>');

                    // function call to convert array to xml
                    $this->_helper->parser->array_to_xml($final_dataValue,$xml_data);

                    $xml_dataValue = $xml_data->asXML();
                }
                
                // echo "<pre>";
                // var_dump($header);
                // var_dump($dataValue);
                // die();

                try{
                    $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                    if ($type == 'csv') {
                        $result = $this->_helper->download->csv($header,$dataValue,null,$fileName, 1);     
                    }
                    else if($type == 'json'){
                        $result = $this->_helper->download->json($json_dataValue, $fileName);
                    }
                    else if($type == 'xml'){
                        $result = $this->_helper->download->xml($xml_dataValue, $fileName);
                    }
                    else if($type == 'txt'){
                        $result = $this->_helper->download->txt($header,$dataValue,null,$fileName);  
                    }
                     else if($type == 'xls'){
                        $result = $this->_helper->download->xls($header,$dataValue,null,$fileName); 
                    }
                     else if($type == 'xlsx'){
                        $result = $this->_helper->download->xlsx($header,$dataValue,null,$fileName); 
                    }
                    
                    //$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index');
                
                }catch ( Exception $e ) {
                // print_r($e);die;
                    $this->view->error = true;
                    $error_msg = 'Error occured while download, please contact administrator';
                    $this->view->report_msg = $this->displayError($error_msg);
                }
                
            }
            else{

                $this->view->error = true;
                if ($headerCounter < 0) {
                    $error_msg[] = 'Column selection cannot be left blank';
                }
                else{
                    $error_msg[] = $filter->getMessages();
                }

                $this->view->report_msg = $this->displayError($error_msg);
            }
        }

    }

    public function wherecolumnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
        $data = $sessionNamespace->content;
        // print_r($data);die('here');
        // $tblName = $this->_getParam('id');
//         $payType    = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
//         foreach($payType as $key => $value){ 
// //          if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);
                
//              $optpaytypeRaw[$key] = $this->language->_($value); 
//         }
        

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($data[0] as $key => $row){
            $optHtml.="<option value='".$key."'>".$row."</option>";
        }

        echo $optHtml;
    }
}
