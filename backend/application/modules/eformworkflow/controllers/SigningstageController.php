<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_SigningstageController extends Application_Main
{
    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newlayout');

        $fields = array(
            'regno'     => array(
                'field'    => 'BG_REG_NUMBER',
                'label'    => $this->language->_('RegNumber') . ' / ' . $this->language->_('Subject'),
            ),
            'app' => array(
                'field' => 'CUST_ID',
                'label' => $this->language->_('Principal'),
            ),

            'obligee' => array(
                'field' => 'RECIPIENT_NAME',
                'label' => $this->language->_('Obligee'),
            ),
            'branch'     => array(
                'field'    => 'BG_BRANCH',
                'label'    => $this->language->_('Bank Branch'),
            ),

            'countertype'     => array(
                'field'    => 'COUNTER_WARRANTY_TYPE',
                'label'    => $this->language->_('Counter'),
            ),

            'type'  => array(
                'field'    => 'CHANGE_TYPE',
                'label'    => $this->language->_('Type')
            ),
            'amount' => array(
                'field' => 'BG_AMOUNT',
                'label' => $this->language->_('Status'),
            ),

        );

        $this->view->fields = $fields;

        $filterlist = array(
            "BG_NUMBER" => 'REG NUMBER',
            'BG_SUBJECT' => "SUBJECT",
            'BG_PERIOD' => "PERIOD TIME",
            "APPLICANT" => "APPLICANT",
            "OBLIGEE_NAME" => "OBLIGEE NAME",
            "BG_AMOUNT" => "AMOUNT",
            "BRANCH" => "BRANCH",
            "COUNTER_TYPE" => "COUNTER TYPE"
        );

        $this->view->filterlist = $filterlist;

        $page    = $this->_getParam('page');

        $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
        $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

        $this->view->currentPage = $page;
        $this->view->sortBy      = $sortBy;
        $this->view->sortDir     = $sortDir;

        // Add Bahri
        if ($this->view->hasPrivilege("ACCS")) { // Cash Collateral
            $warantyIn = ['1'];
        }
        if ($this->view->hasPrivilege("ANCS")) { // Non-Cash Collateral
            $warantyIn = ['2', '3'];
        }
        if ($this->view->hasPrivilege("ACCS") && $this->view->hasPrivilege("ANCS")) { // All
            $warantyIn = ['1', '2', '3'];
        }
        // END Bahri

        $selectbg = $this->_db->select()
            ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array(
                'REG_NUMBER' => 'BG_REG_NUMBER',
                'SUBJECT' => 'BG_SUBJECT',
                'CREATED' => 'BG_CREATED',
                //'TYPE' => (string)'TYPE',
                'TIME_PERIOD_START',
                'TIME_PERIOD_END',
                'CREATEDBY' => 'BG_CREATEDBY',
                'AMOUNT' => 'BG_AMOUNT',
                'COUNTER_WARRANTY_TYPE',
                'BG_INSURANCE_CODE',
                'FULLNAME' => 'T.USER_FULLNAME',
                'SP_OBGLEE_CODE',
                'RECIPIENT_NAME',
                'B.BRANCH_NAME',
                'C.CUST_NAME',
                'IS_AMENDMENT' => 'A.CHANGE_TYPE'
            ))
            ->join(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID AND T.CUST_ID = A.CUST_ID')
            ->join(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID')
            ->join(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE')
            ->where('A.BG_STATUS = 16') // -->> status harus diganti
            ->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn)
            ->order('A.BG_CREATED DESC');

        // select branch ------------------------------------------------------------
        $select_branch = $this->_db->select()
            ->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
            ->query()->fetchAll();

        $save_branch = [];

        foreach ($select_branch as $key => $value) {
            $save_branch[$value["BRANCH_CODE"]] = $value["BRANCH_NAME"];
        }

        $this->view->sel_branch = $save_branch;
        //  --------------------------------------------------------------------------

        // select counter type ------------------------------------------------------------
        $save_counter_type = [
            1 => 'FC',
            2 => 'LF',
            3 => 'Insurance'
        ];

        $this->view->counter_type = $save_counter_type;
        //  -------------------------------------------------------------------------- 

        $selectlc = $this->_db->select()
            ->from(array('A' => 'T_LC'), array(
                'REG_NUMBER' => 'LC_REG_NUMBER',
                'SUBJECT' => 'LC_CREDIT_TYPE',
                'CREATED' => 'LC_CREATED',
                'CCYID' => 'LC_CCY',
                'TIME_PERIOD_END' => 'LC_EXPDATE',
                'CREATEDBY' => 'LC_CREATEDBY',
                'AMOUNT' => 'LC_AMOUNT',
                'FULLNAME' => 'T.USER_FULLNAME'
            ))
            ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
            ->where('A.LC_STATUS = 1')
            ->order('A.LC_CREATED DESC');

        $conf = Zend_Registry::get('config');

        $this->view->bankname = $conf['app']['bankname'];

        $config     = Zend_Registry::get('config');
        $BgType     = $config["bg"]["status"]["desc"];
        $BgCode     = $config["bg"]["status"]["code"];

        $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

        $this->view->arrStatus = $arrStatus;

        $arrWaranty = array(
            1 => 'FC',
            2 => 'LF',
            3 => 'INS'
        );
        $this->view->arrWaranty = $arrWaranty;

        $arrType = array(
            1 => 'Standart',
            2 => 'Custom'
        );

        $arrLang = array(
            1 => 'Indonesian',
            2 => 'English',
            3 => 'Bilingual'
        );
        $this->view->langArr = $arrLang;
        $this->view->formatArr = $arrType;

        $filterArr = array(
            'BG_NUMBER'   => array('StripTags', 'StringTrim', 'StringToUpper'),
            'BG_SUBJECT'      => array('StripTags', 'StringTrim', 'StringToUpper')
        );

        $dataParam = array("BG_NUMBER", "BG_SUBJECT", "BG_PERIOD", "APPLICANT", "OBLIGEE_NAME", "BG_AMOUNT", "BRANCH", "COUNTER_TYPE");
        $dataParamValue = array();

        foreach ($dataParam as $dtParam) {
            if (!empty($this->_request->getParam('wherecol'))) {
                $dataval = $this->_request->getParam('whereval');
                foreach ($this->_request->getParam('wherecol') as $key => $value) {
                    if ($dtParam == $value) {

                        if (empty($dataParamValue[$dtParam])) {
                            $dataParamValue[$dtParam] = [];
                        }
                        array_push($dataParamValue[$dtParam], $dataval[$key]);
                    }
                }
            }
        }

        if (!empty($this->_request->getParam('efdate'))) {
            $efdatearr = $this->_request->getParam('efdate');
            $dataParamValue['BG_PERIOD'] = $efdatearr[0];
            $dataParamValue['BG_PERIOD_END'] = $efdatearr[1];
        }

        $validator = array(
            'BG_PERIOD'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
            'BG_PERIOD_END'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
        );

        $zf_filter   = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

        if ($zf_filter->isValid()) {
            $filter     = TRUE;
        }

        $bgNubmer  = $dataParamValue["BG_NUMBER"];
        $applicant = $dataParamValue["APPLICANT"];
        $OBLIGEE_NAME = $dataParamValue["OBLIGEE_NAME"];
        $amount = $dataParamValue["BG_AMOUNT"];
        $branch = $dataParamValue["BRANCH"];
        $counttype = $dataParamValue["COUNTER_TYPE"];
        $bgSubject  = $dataParamValue["BG_SUBJECT"];
        $datefrom    = html_entity_decode($zf_filter->getEscaped('BG_PERIOD'));
        $dateto    = html_entity_decode($zf_filter->getEscaped('BG_PERIOD_END'));

        if ($filter == null || $filter == TRUE) {
            $append_query = "";

            $this->view->fDateFrom = $datefrom;
            $this->view->fDateTo = $dateto;

            if (!empty($datefrom)) {
                $FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
                $datefrom  = $FormatDate->toString($this->_dateDBFormat);
            }
            if (!empty($dateto)) {
                $FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
                $dateto    = $FormatDate->toString($this->_dateDBFormat);
            }
            if (!empty($datefrom) && empty($dateto)) {
                if ($append_query != "") {
                    $append_query .= " OR ";
                }
                $append_query .= "A.TIME_PERIOD_START >= " . $this->_db->quote($datefrom);
            }

            if (empty($datefrom) && !empty($dateto)) {
                if ($append_query != "") {
                    $append_query .= " OR ";
                }
                $append_query .= "A.TIME_PERIOD_END <= " . $this->_db->quote($dateto);
            }

            if (!empty($datefrom) && !empty($dateto)) {
                if ($append_query != "") {
                    $append_query .= " OR ";
                }
                $append_query .= "A.TIME_PERIOD_START >= " . $this->_db->quote($datefrom) . " and A.TIME_PERIOD_END <= " . $this->_db->quote($dateto);
            }

            if (count($bgNubmer) > 0) {
                foreach ($bgNubmer as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " OR ";
                    }
                    $append_query .= "A.BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($bgSubject) > 0) {
                foreach ($bgSubject as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " OR ";
                    }
                    $append_query .= "A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($applicant) > 0) {
                foreach ($applicant as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " OR ";
                    }
                    $append_query .= "A.CUST_ID LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($OBLIGEE_NAME) > 0) {
                foreach ($OBLIGEE_NAME as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " OR ";
                    }
                    $append_query .= "A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($amount) > 0) {
                foreach ($amount as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " OR ";
                    }
                    $append_query .= "A.BG_AMOUNT LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($branch) > 0) {
                foreach ($branch as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " OR ";
                    }
                    $append_query .= "B.BRANCH_CODE LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($counttype) > 0) {
                foreach ($counttype as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " OR ";
                    }
                    $append_query .= "A.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if ($append_query != "") {
                $selectbg->where($append_query);
            }
        }

        $this->view->fields = $fields;
        $this->view->filter = $filter;

        $selectbg = $this->_db->fetchAll($selectbg);
        $selectlc = $this->_db->fetchAll($selectlc);
        $result = array_merge($selectbg, $selectlc);

        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token   = $rand;
        $this->view->token = $sessionNamespace->token;

        foreach ($result as $key => $value) {
            $get_reg_number = $value["REG_NUMBER"];

            $AESMYSQL = new Crypt_AESMYSQL();
            $rand = $this->token;

            $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
            $encpayreff = urlencode($encrypted_payreff);

            $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
        }

        $this->paging($result);

        if (!empty($dataParamValue)) {

            $this->view->efdateStart = $dataParamValue['BG_PERIOD'];
            $this->view->efdateEnd = $dataParamValue['BG_PERIOD_END'];

            foreach ($dataParamValue as $key => $value) {
                $duparr = explode(',', $value);
                if (!empty($duparr)) {

                    foreach ($duparr as $ss => $vs) {
                        $wherecol[]  = $key;
                        $whereval[] = $vs;
                    }
                } else {
                    $wherecol[]  = $key;
                    $whereval[] = $value;
                }
            }
            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }
    }
}
