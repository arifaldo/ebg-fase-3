<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class  cmdreport_BankAccListreportController extends Application_Main
{
	protected $_moduleDB = 'RTF';


	public function indexAction()
	{
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');

		$listId = $this->_db->select()->distinct()
			->from(
				array('M_CUSTOMER'),
				array('CUST_ID')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();

		$this->view->listCustId = array('' => '-- ' . $this->language->_('Any Value') . ' --');
		$this->view->listCustId += Application_Helper_Array::listArray($listId, 'CUST_ID', 'CUST_ID');

		$status = $this->_customerstatus;
		$options = array_combine(array_values($status['code']), array_values($status['desc']));
		$this->view->listStatus = array('' => '-- ' . $this->language->_('Any Value') . ' --');
		$this->view->listStatus += $options;

		$fields = array(
			'CompanyCode'  			=> array(
				'field' => 'C.CUST_ID',
				'label' => $this->language->_('Company Code'),
				'sortable' => true
			),
			'companyname'  			=> array(
				'field' => 'C.CUST_NAME',
				'label' => $this->language->_('Company Name'),
				'sortable' => true
			),
			'acct'  					=> array(
				'field' => 'CA.ACCT_NO',
				'label' => $this->language->_('Account Number'),
				'sortable' => true
			),
			'accountname'  					=> array(
				'field' => 'CA.ACCT_NAME',
				'label' => $this->language->_('Account Name'),
				'sortable' => true
			),
			'currency'  					=> array(
				'field' => 'CA.CCY_ID',
				'label' => $this->language->_('CCY'),
				'sortable' => true
			),
			'Type'  					=> array(
				'field' => 'CA.ACCT_DESC',
				'label' => $this->language->_('Type'),
				'sortable' => true
			),

			'Updated'  => array(
				'field' => 'CA.ACCT_UPDATED',
				'label' => $this->language->_('Approved Date'),
				'sortable' => true
			),
			'Status'  => array(
				'field' => 'CA.ACCT_STATUS',
				'label' => $this->language->_('Status'),
				'sortable' => true
			),
		);

		$filterlist = array("COMP_CODE", "COMP_NAME", "ACCT_NO", "ACCT_NAME", "STATUS", "CCY_ID", "ACCT_DESC", "ACCT_UPDATED");

		$this->view->filterlist = $filterlist;
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'C.CUST_ID');
		$sortDir = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		$filterArr = array(
			'filter' => array('StripTags', 'StringTrim'),
			'COMP_CODE'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'COMP_NAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'ACCT_NO'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'ACCT_NAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'CCY_ID'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'ACCT_DESC'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'ACCT_UPDATED'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'STATUS' => array('StripTags', 'StringToUpper'),
			'GROUP_NAME' => array('StripTags', 'Alnum', 'StringToUpper')
		);

		$dataParam = array("COMP_CODE", "COMP_NAME", "ACCT_NO", "ACCT_NAME", "STATUS", "CCY_ID", "ACCT_DESC");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		//$arrACCT_STATUS = array(0=>"Unapproved",1=>"Approved",2=>"Suspended",3=>"Deleted",4=>"Request Edit",5=>"Request  Delete");

		$caseStatus = "(CASE CA.ACCT_STATUS ";
		foreach ($options as $key => $val) {
			$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseStatus .= " END)";

		$select = $this->_db->select()
			->FROM(array('C' => 'M_CUSTOMER'), array('C.CUST_ID', 'C.CUST_NAME'))
			->JOINLEFT(array('CA' => 'M_CUSTOMER_ACCT'), 'C.CUST_ID = CA.CUST_ID', array('CA.ACCT_NO', 'CA.ACCT_NAME', 'CA.CCY_ID', 'CA.ACCT_DESC', 'CA.ACCT_EMAIL'))
			->JOINLEFT(array('G' => 'M_GROUPING'), 'G.GROUP_ID = CA.GROUP_ID', array(
				'G.GROUP_NAME',
				'CA.ACCT_CREATED',
				'CA.ACCT_SUGGESTED',
				'CA.ACCT_UPDATED',
				'ACCT_STATUS' => $caseStatus
			))
			->joinLeft(['MB' => 'M_BUSER'], 'C.CUST_CREATEDBY = MB.BUSER_ID', [''])
			->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', ['MBR.BRANCH_CODE']);

		if ($filter == TRUE || ($filter == TRUE && ($csv || $pdf))) {
			$companyid = html_entity_decode($zf_filter->getEscaped('COMP_CODE'));
			$companyname = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
			$accnumber = html_entity_decode($zf_filter->getEscaped('ACCT_NO'));
			$accname = html_entity_decode($zf_filter->getEscaped('ACCT_NAME'));
			$ccyid = html_entity_decode($zf_filter->getEscaped('CCY_ID'));
			$acctype = html_entity_decode($zf_filter->getEscaped('ACCT_DESC'));
			$approvedate = html_entity_decode($zf_filter->getEscaped('ACCT_UPDATED'));
			$stat = $zf_filter->getEscaped('STATUS');
			$groupname = html_entity_decode($zf_filter->getEscaped('GROUP_NAME'));

			$latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('latestSuggestionFrom'));
			$latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('latestSuggestionTo'));
			$latestSuggestor        = html_entity_decode($zf_filter->getEscaped('latestSuggestor'));
			$latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('latestApprovalFrom'));
			$latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('latestApprovalTo'));
			$latestApprover         = html_entity_decode($zf_filter->getEscaped('latestApprover'));

			if ($companyid) {
				$this->view->companyid = $companyid;
				$select->where("UPPER(C.CUST_ID) LIKE " . $this->_db->quote('%' . $companyid . '%'));
			}

			if ($companyname) {
				$this->view->companyname = $companyname;
				$select->where("UPPER(C.CUST_NAME) LIKE " . $this->_db->quote('%' . $companyname . '%'));
			}

			if ($accnumber) {
				$this->view->accnumber = $accnumber;
				$select->where("ACCT_NO LIKE " . $this->_db->quote('%' . $accnumber . '%'));
			}

			if ($accname) {
				$this->view->accname = $accname;
				$select->where("UPPER(CA.ACCT_NAME) LIKE " . $this->_db->quote('%' . $accname . '%'));
			}

			if ($ccyid) {
				$this->view->ccyid = $ccyid;
				$select->where("UPPER(CA.CCY_ID) LIKE " . $this->_db->quote('%' . $ccyid . '%'));
			}

			if ($acctype) {
				$this->view->acctype = $acctype;
				$select->where("UPPER(CA.ACCT_DESC) LIKE " . $this->_db->quote('%' . $acctype . '%'));
			}

			if ($approvedate) {
				$this->view->approvedate = $approvedate;
				$select->where("UPPER(CA.ACCT_UPDATED) LIKE " . $this->_db->quote('%' . $approvedate . '%'));
			}

			if ($stat !== null && $stat !== "") {
				$this->view->stat = ($stat);
				$select->where("CA.ACCT_STATUS LIKE " . $this->_db->quote($stat));
			}

			if ($groupname) {
				$this->view->groupname = $groupname;
				$select->where("G.GROUP_NAME LIKE " . $this->_db->quote('%' . $groupname . '%'));
			}

			//konversi date agar dapat dibandingkan
			$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestSuggestionFrom, $this->_dateDisplayFormat) :
				false;

			$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestSuggestionTo, $this->_dateDisplayFormat) :
				false;

			$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestApprovalFrom, $this->_dateDisplayFormat) :
				false;

			$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestApprovalTo, $this->_dateDisplayFormat) :
				false;

			if ($latestSuggestor)  $select->where('UPPER(USER_SUGGESTEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestSuggestor) . '%'));
			if ($latestApprover)   $select->where('UPPER(USER_UPDATEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestApprover) . '%'));
			if ($latestSuggestionFrom)  $select->where("CONVERT(date,CUST_SUGGESTED) >= CONVERT(DATE," . $this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)) . ")");
			if ($latestSuggestionTo)    $select->where("CONVERT(date,CUST_SUGGESTED) <= CONVERT(DATE," . $this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)) . ")");
			if ($latestApprovalFrom)    $select->where("CONVERT(date,CUST_UPDATED) >= CONVERT(DATE," . $this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)) . ")");
			if ($latestApprovalTo)      $select->where("CONVERT(date,CUST_UPDATED) <= CONVERT(DATE," . $this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)) . ")");

			if ($latestSuggestor)	$this->view->latestSuggestor  = $latestSuggestor;
			if ($latestApprover)	$this->view->latestApprover   = $latestApprover;

			if ($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
			if ($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
			if ($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
			if ($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
		}

		$select->order($sortBy . ' ' . $sortDir);
		//PAGING
		if ($csv || $pdf || $this->_request->getParam('print') == 1) {
			$data = array();
			$data = $this->_db->fetchall($select);
			// if(is_array($data))
			// {
			// $arr_date = array('ACCT_CREATED','ACCT_SUGGESTED','ACCT_UPDATED');
			// foreach($data as $index=>$row)
			// {
			// foreach($row as $key=>$value)
			// {
			// if( in_array($key,$arr_date) )
			// {
			// $data[$index][$key] = Application_Helper_General::convertDate($value,$this->_dateTimeDisplayFormat,$this->_dateDBDefaultFormat);
			// }
			// }
			// }
			// }
			if ($csv) {
				Application_Helper_General::writeLog('RPAC', 'Download CSV Bank Account List Report');
				$this->_helper->download->csv(array($this->language->_('Company Code'), $this->language->_('Company Name'), $this->language->_('Account Number'), $this->language->_('Account Name'), $this->language->_('Currency'), $this->language->_('Type'), $this->language->_('Email Address'), $this->language->_('Group Name'), $this->language->_('Created Date'), $this->language->_('Suggested Date'), $this->language->_('Approved Date'), $this->language->_('Status')), $data, null, $this->language->_('Bank Account List Report'));
			} else if ($pdf) {
				Application_Helper_General::writeLog('RPAC', 'Download PDF Bank Account List Report');
				$this->_helper->download->pdf(array($this->language->_('Company Code'), $this->language->_('Company Name'), $this->language->_('Account Number'), $this->language->_('Account Name'), $this->language->_('Currency'), $this->language->_('Type'), $this->language->_('Email Address'), $this->language->_('Group Name'), $this->language->_('Created Date'), $this->language->_('Suggested Date'), $this->language->_('Approved Date'), $this->language->_('Status')), $data, null, $this->language->_('Bank Account List Report'));
			} else if ($this->_request->getParam('print') == 1) {
				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Bank Account List Report', 'data_header' => $fields));
			} else {
				//Application_Helper_General::writeLog('RPAC','View Bank Account List Report');
			}
		}

		$auth = Zend_Auth::getInstance()->getIdentity();

		if ($auth->userHeadQuarter == "NO") {
			$select->where('MBR.BRANCH_CODE = ?', $auth->userBranchCode); // Add Bahri
		}

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->modulename = $this->_request->getModuleName();
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('RPAC', 'View Bank Account List Report');
		}

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
	}
}
