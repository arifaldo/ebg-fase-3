<?php
require_once ('Crypt/AESMYSQL.php');
require_once 'General/Settings.php';
require_once 'General/BankUser.php';

class superadmin_ChangespasswordController extends Zend_Controller_Action {
	
	private $_db;

	public function init(){
		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$this->_helper->layout()->setLayout('layout_superadmin');
		$auth = new Zend_Session_Namespace( 'SYSADMIN' );

		if($auth->isSuperUser != true || $auth->userGroupLogin != "SUPERADMIN")
		{
			$this->_redirect('/superadmin');
		}
	}
	
	public function indexAction()
	{

		if($this->_getParam('success'))
		{
			$success = "Password changed success";
			$this->view->success = $success;
		}
		
		$auth = new Zend_Session_Namespace( 'SYSADMIN' );
		if( $auth->userIdLogin )
		{
			$change 	= $this->_getParam('change');
			$this->view->change = $change;
			$suser_id	= $auth->userIdLogin;
		}

		if( (in_array($change,array(1,2))) && !empty($suser_id) )
		{
			if($this->_request->isPost())
			{
				$errDesc = array();
				$filters = array(
												'new_password'     => array('StripTags','StringTrim'),
												'confirm_password'     => array('StripTags','StringTrim'),
												'curr_pass1' => array('StripTags','StringTrim'),
												'curr_pass2' => array('StripTags','StringTrim'),
											);
				$zf_filter_input = new Zend_Filter_Input($filters,null,$this->_request->getPost());

				$new_pass	= $zf_filter_input->new_password;
				$conf_pass 	= $zf_filter_input->confirm_password;
				$curr_pass1	= $zf_filter_input->curr_pass1;	
				$curr_pass2	= $zf_filter_input->curr_pass2;

				$dbPass = $this->_db->FetchRow(
																			$this->_db->SELECT()
																								->FROM( 'M_SUSER',array('SUSER_PASSWORD1','SUSER_PASSWORD2') )
																								->WHERE('SUSER_ID = ?',$suser_id)
																			);
				
				$db_pass1 = $dbPass['SUSER_PASSWORD1'];
				$db_pass2 = $dbPass['SUSER_PASSWORD2'];

				$key = md5($suser_id);
				$encrypted_pwd = Crypt_AESMYSQL::encrypt($new_pass, $key);
				$encryptedNewPassword = md5($encrypted_pwd);

				$key = md5($suser_id);
				$encrypted_pwd = Crypt_AESMYSQL::encrypt($new_pass, $key);
				$encryptedNewPassword = md5($encrypted_pwd);

				$encrypted_pwd = Crypt_AESMYSQL::encrypt($curr_pass1, $key);
				$encryptedCurrPass1 = md5($encrypted_pwd);

				$encrypted_pwd = Crypt_AESMYSQL::encrypt($curr_pass2, $key);
				$encryptedCurrPass2 = md5($encrypted_pwd);

				$errDesc = array();
				if (!$curr_pass1){
					$errDesc['curr_pass1'] = "Error: Cannot be left blank. Please correct it.";
				}
				else if($encryptedCurrPass1 != $db_pass1){
					$errDesc['curr_pass1'] = "Error: Sorry, but the Current Password you entered is incorrect.";
				}

				if (!$curr_pass2){
					$errDesc['curr_pass2'] = "Error: Cannot be left blank. Please correct it.";
				}
				else if($encryptedCurrPass2 != $db_pass2){
					$errDesc['curr_pass2'] = "Error: Sorry, but the Current Password you entered is incorrect.";
				}

				if (!$new_pass){
					$errDesc['new_pass'] = "Error: New Password cannot be left blank. Please correct it.";
				}
				else if($encryptedNewPassword == $db_pass1){
					$errDesc['new_pass'] = "Error: New Password cannot be same with Any Current Password.";
				}
				else if($encryptedNewPassword == $db_pass2){
					$errDesc['new_pass'] = "Error: New Password cannot be same with Any Current Password.";
				}

				if (!$conf_pass){
					$errDesc['conf_pass'] = "Error: Confirm New Password cannot be left blank. Please correct it.";
				}elseif ($conf_pass != $new_pass){
					$errDesc['conf_pass'] = "Error: Sorry, but the two passwords you entered are not the same.";
				}
				if(count($errDesc)==0)
				{
					$update = 0;
					$where =  array();

					$params = array(
												'SUSER_PASSWORD'.$change => $encryptedNewPassword,
												'IS_REQPASS'.$change => 0,
											);
					$where['SUSER_ID = ?'] = $suser_id;
					$update = $this->_db->UPDATE('M_SUSER',$params,$where);

					if($update)
					{
						// -- -- WRITE LOG -- -- //
						$params = array(
														'LOG_DATE' => new Zend_Db_Expr('now()'),
														'SUSER_ID' => $suser_id,
														'ACTION_FULLDESC' => 'Changed Password '.$change
													);
						$update = $this->_db->INSERT('T_SACTIVITY',$params);

						$this->_redirect('/superadmin/changespassword/index/change/'.$change.'/success/true');
					}
					else
					{
						$this->view->error 	= true;
						$this->view->msg_failed = 'Failed to change password';
						$this->_redirect('/superadmin/changespassword/index/change/'.$change.'/success/false');
					}
				}
				else
				{							
					$this->view->error 	= true;
					$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';
					$this->view->errDesc = $errDesc;
				}
			}
		}
		else
		{
			$this->_redirect('/superadmin/home');
		}
	}
    
}