<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class bankforexlimit_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
	public function indexAction()
	{
	   
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
		$model = new bankforexlimit_Model_Bankforexlimit();
	
	    $fields = array(
						'CCY'      => array('field' => 'CCY',
											      'label' => $this->language->_('Currency'),
											      'sortable' => true),
	    				'BUY_AMOUTN'      => array('field' => 'BUY_AMOUNT',
											      'label' => $this->language->_('Max buy Limit in IDR'),
											      'sortable' => true),
						'SELL_AMOUNT'           => array('field' => 'SELL_AMOUNT',
											      'label' => $this->language->_('Max sell Limit in IDR'),
											      'sortable' => true),
						'CREATEDBY'           => array('field' => 'CREATEDBY',
											      'label' => $this->language->_('Created By'),
											      'sortable' => true),
	    				'CREATED'      => array('field' => 'CREATED',
							    				  'label' => $this->language->_('Created Date'),
							    				  'sortable' => true),
	    				'UPDATEBY'           => array('field' => 'UPDATEDBY',
					    						  'label' => $this->language->_('Updated By'),
					    						  'sortable' => true),
	    				'UPDATED'           => array('field' => 'UPDATED',
	    										  'label' => $this->language->_('Updated Date'),
	    										  'sortable' => true),
	    				'ACTION'           => array('field' => '',
							    				  'label' => $this->language->_('Action'),
							    				  'sortable' => false),
				      );
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','CCY');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'product_code'  => array('StringTrim','StripTags'),
							'plan_code'  => array('StringTrim','StripTags'),
							'product_name'      => array('StringTrim','StripTags','StringToUpper'),
		);
		
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	

		$fParam = array();
		
		$select = $model->getData($fParam,$sortBy,$sortDir,$filter);		
		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['PRODUCT_ID']);
		}
		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
				$this->_helper->download->csv(array($this->language->_('CCY'),$this->language->_('Max buy Limit in IDR'),$this->language->_('Max sell Limit in IDR'),$this->language->_('Created'),$this->language->_('Created By'),$this->language->_('Updated'),$this->language->_('Updated By')),$selectPdfCsv,null,'bankforexlimit');  
				Application_Helper_General::writeLog('PALS','Export to CSV');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf(array($this->language->_('CCY'),$this->language->_('Max buy Limit in IDR'),$this->language->_('Max sell Limit in IDR'),$this->language->_('Created'),$this->language->_('Created By'),$this->language->_('Updated'),$this->language->_('Updated By')),$selectPdfCsv,null,'bankforexlimit');   
				Application_Helper_General::writeLog('PALS','Export to PDF');
		}
		elseif($this->_request->getParam('print') == 1){
				unset($fields['action']);
	            $this->_forward('print', 'index', 'widget', array('data_content' => $selectPdfCsv, 'data_caption' => 'product_account', 'data_header' => $fields));
	       		Application_Helper_General::writeLog('PALS','Export to PDF');
		}
		else
		{		
				Application_Helper_General::writeLog('PALS','View Bank Forex List');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}
}