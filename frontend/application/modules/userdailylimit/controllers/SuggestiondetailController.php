<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'Crypt/AESMYSQL.php';
class userdailylimit_SuggestiondetailController extends Application_Main 
{
  public function indexAction()
  { 
   
    $change_id = $this->_getParam('changes_id');

     $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;  


    $AESMYSQL = new Crypt_AESMYSQL();
    $PS_NUMBER      = urldecode($this->_getParam('changes_id'));
    $change_id = $AESMYSQL->decrypt($PS_NUMBER, $password); 
    $change_id = (Zend_Validate::is($change_id,'Digits'))? $change_id : 0;
    $this->_helper->layout()->setLayout('popup');
    
    $this->view->suggestionType = $this->_suggestType;
    
    if($change_id)
    {
      $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID'))
                             ->where('CHANGES_ID='.$this->_db->quote($change_id))
                             //->where("CHANGES_FLAG='F'")
                             ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
//                             ->where('UPPER(MODULE)='.$this->_db->quote(strtoupper($this->_request->getModuleName())));
      $result = $this->_db->fetchOne($select);
      
      if(empty($result))  $this->_redirect('/notification/invalid/index');
      
      
      if($result)
      {
        //content send to view
        $select = $this->_db->select()
                               ->from(array('T' =>'TEMP_DAILYLIMIT'))
                               ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','GLOBALCHANGES_CREATED'=>'CREATED','GLOBALCHANGES_CREATEDBY'=>'CREATED_BY','CHANGES_STATUS','READ_STATUS'))
                       ->where('T.CHANGES_ID = ?', $change_id);
        $resultdata = $this->_db->fetchRow($select);
     
        if($resultdata['TEMP_ID'])
        {
            //suggest data
          $this->view->changes_id     = $resultdata['CHANGES_ID'];
        $this->view->changes_type   = $resultdata['CHANGES_TYPE'];
        $this->view->changes_status = $resultdata['CHANGES_STATUS'];
        $this->view->read_status    = $resultdata['READ_STATUS'];
        $this->view->created        = $resultdata['GLOBALCHANGES_CREATED'];
        $this->view->created_by     = $resultdata['GLOBALCHANGES_CREATEDBY'];
        
        
        
        
          //daily limit data  
            $this->view->cust_id  = $resultdata['CUST_ID'];
        
            $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_NAME'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$resultdata['CUST_ID']));
            $custName = $this->_db->fetchRow($select);
            $this->view->cust_name = $custName['CUST_NAME']; 
            
            $this->view->user_id  = $resultdata['USER_LOGIN'];
            
        $select = $this->_db->select()
                             ->from('M_USER',array('USER_FULLNAME'))
                             ->where('UPPER(USER_ID)='.$this->_db->quote((string)$resultdata['USER_LOGIN']))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$resultdata['CUST_ID']));
            $userName = $this->_db->fetchRow($select);
            $this->view->user_name = $userName['USER_FULLNAME'];
            
          
            $this->view->ccy_id      = $resultdata['CCY_ID'];
            $this->view->daily_limit = Application_Helper_General::displayMoney($resultdata['DAILYLIMIT']);
            $this->view->status      = $resultdata['DAILYLIMIT_STATUS'];
      
            
        
            $this->view->daily_created     = $resultdata['CREATED'];
            $this->view->daily_createdby   = $resultdata['CREATEDBY'];
            $this->view->daily_suggested   = $resultdata['SUGGESTED'];
            $this->view->daily_suggestedby = $resultdata['SUGGESTEDBY'];
            $this->view->daily_updated     = $resultdata['UPDATED'];
            $this->view->daily_updatedby   = $resultdata['UPDATEDBY'];  
            
            
          
            
            //master data
            $select = $this->_db->select()
                               ->from(array('T' =>'M_DAILYLIMIT'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$resultdata['CUST_ID']))
                       ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$resultdata['USER_LOGIN']))
                       ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$resultdata['CCY_ID']));
            $m_resultdata = $this->_db->fetchRow($select);
            
            
            
            
          $this->view->m_daily_limit = Application_Helper_General::displayMoney($m_resultdata['DAILYLIMIT']);
            $this->view->m_status      = $m_resultdata['DAILYLIMIT_STATUS'];
      
            
        
            $this->view->m_daily_created     = $m_resultdata['CREATED'];
            $this->view->m_daily_createdby   = $m_resultdata['CREATEDBY'];
            $this->view->m_daily_suggested   = $m_resultdata['SUGGESTED'];
            $this->view->m_daily_suggestedby = $m_resultdata['SUGGESTEDBY'];
            $this->view->m_daily_updated     = $m_resultdata['UPDATED'];
            $this->view->m_daily_updatedby   = $m_resultdata['UPDATEDBY']; 
            
            
            
            
            
            $this->view->status_type = $this->_masterglobalstatus;

//             var_dump($this->view->cust_id);
//             var_dump($this->view->user_id);
//             var_dump($this->view->ccy_id);
        
        }
        else{ $change_id = 0; }
      }
      else{ $change_id = 0; } 
    }
    
    
    
    
    
    if(!$change_id)
    {
      $error_remark = 'Changes ID is not found.';
      //insert log
     
      Application_Helper_General::writeLog('VDLC','View User Daily Limit Changes List');
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect('/popuperror/index/index'); 
    }
    
    //insert log
    Application_Helper_General::writeLog('VDLC','View User Daily Limit Changes List');
    $this->view->changes_id = $change_id;
  }

}