<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';

require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';
require_once("Service/Account.php");

class bgclosingworkflow_VerificationdetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$settings = new Settings();
		$conf = Zend_Registry::get('config');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$AESMYSQL = new Crypt_AESMYSQL();

		$system_type = $settings->getSetting('system_type');
		$stamp_fee = $settings->getSetting('stamp_fee');
		$adm_fee = $settings->getSetting('adm_fee');

		$claim_period = $settings->getSetting('max_claim_period');
		$getOtpTrx = $this->_db->select()->from("M_SETTING")->where("SETTING_ID = ?", "otp_exp_trx")->query()->fetchAll();

		$bankName = $conf['app']['bankname'];

		$toc_ind = $settings->getSetting('ftemplate_bg_ind');
		$toc_eng = $settings->getSetting('ftemplate_bg_eng');

		$ctdesc = $conf["bgclosing"]["changetype"]["desc"];
		$ctcode = $conf["bgclosing"]["changetype"]["code"];
		$suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

		$usergoogleAuth = $this->_db->select()
			->from(["MU" => "M_USER"], ["MU.GOOGLE_CODE", "MU.USER_FAILEDTOKEN"])
			->where("CUST_ID = ?", $this->_custIdLogin)
			->where("USER_ID = ?", $this->_userIdLogin)
			->where("LTRIM(RTRIM(GOOGLE_CODE)) <> ?", "")
			->query()->fetchAll();

		if (!empty($usergoogleAuth)) {
			$maxtoken = $settings->getSetting("max_failed_token");
			$this->view->googleauth = true;
			$this->view->tokenfail = (int)$maxtoken;
			if ($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0') {
				$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN'] + 1);
				$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
				$this->view->tokenfail = $tokenfail;
			}
			// untuk di tanyakan
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
			// untuk di tanyakan
		} else {
			// untuk di tanyakan
			$this->view->nogoauth = '1';
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
			// untuk di tanyakan
		}

		$get_cash_collateral = $this->_db->select()
			->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
			->where("CUST_ID = ?", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$numb = $this->_getParam('bgnumb');

		$this->view->systemType = $system_type;
		$this->view->stamp_fee = $stamp_fee;
		$this->view->adm_fee = $adm_fee;
		$this->view->BG_CLAIM_PERIOD = $claim_period;
		$this->view->timeExpOtp = $getOtpTrx[0]["SETTING_VALUE"];
		$this->view->ProvFee = 2000000;
		$this->view->masterbankname = $bankName;
		$this->view->toc_ind = $toc_ind;
		$this->view->toc_eng = $toc_eng;
		$this->view->userid = $this->_userIdLogin;
		$this->view->custid = $this->_custIdLogin;
		$this->view->googleauth = true;
		$this->view->cash_collateral = $get_cash_collateral[0];
		$this->view->suggestion_type = $suggestion_type;

		// decrypt numb
		$enc_pass = $settings->getSetting('enc_pass');
		$enc_salt = $settings->getSetting('enc_salt');

		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token = $rand;
		$password = $sessionNamespace->token;
		$BG_NUMBER = urldecode($numb);
		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
		$numb = $BG_NUMBER;

		$this->view->bgnumb_enc = urlencode($AESMYSQL->encrypt($numb, uniqid()));

		$this->view->token = $sessionNamespace->token;
		$this->view->numb = $numb;

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*', 'CHANGE_TYPE_CLOSE' => 'D.CHANGE_TYPE', 'BG_REG' => 'A.BG_REG_NUMBER', 'T_CUST_EMAIL' => 'A.CUST_EMAIL'))
				->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
				->where('A.BG_NUMBER = ?', $numb)
				->where('D.SUGGESTION_STATUS IN (?)', [4, 9])
				->query()->fetchAll();

			if (!empty($bgdata)) {
				$data = $bgdata['0'];

				$bgdatadetail = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $data['BG_REG'])
					->query()->fetchAll();

				$bgdatadetailclose = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$selectcomp = $this->_db->select()
					->from(array('A' => 'M_CUSTOMER'), array('*'))
					->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
					->where('A.CUST_ID = ?', $data['CUST_ID'])
					->query()->fetchAll();
				$this->view->compinfo = $selectcomp[0];

				$closeDocument = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$policyBoundary = $this->findPolicyBoundary(38, $data['BG_AMOUNT']);
				$approverUserList = $this->findUserBoundary(38, $data['BG_AMOUNT']);
				$releaserList = $this->findUserPrivi('VVCC');

				$bgpublishType = $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode = $conf["bgpublish"]["type"]["code"];
				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
				$this->view->publishDesc = $arrbgpublish[$data['BG_PUBLISH']];

				if ($data['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('NOTES = ?', $data['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();
					$totalKertas = count($getPaperPrint);
					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
					$this->view->isinsurance = true;
					$this->view->stamp_fee = $data['STAMP_FEE'];
					$this->view->adm_fee = $data['ADM_FEE'];
					$this->view->ProvFee = $data['PROVISION_FEE'];
				}
				$this->view->closeDocument = $closeDocument;
				$this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];
				$this->view->policyBoundary = $policyBoundary;

				if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceName = array_search("Insurance Name", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];
					$getInsuranceName = $bgdatadetail[$getInsuranceName];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();
					$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];

					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $getInsuranceName["PS_FIELDVALUE"];
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
					// end get linefacility
				}

				if ($data['COUNTER_WARRANTY_TYPE'] == '2') {
					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $this->_custIdLogin;
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
				}

				if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $this->_custIdLogin;
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
				}

				// ========================= History Workflow =========================
				$selectHistory	= $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY_CLOSE')
					->order('DATE_TIME ASC')
					->where("BG_NUMBER = ?", $numb);
				$history = $this->_db->fetchAll($selectHistory);

				$historyStatusCode = $conf["bgclosinghistory"]["status"]["code"];
				$historyStatusDesc = $conf["bgclosinghistory"]["status"]["desc"];
				$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

				$this->view->dataHistory = $history;
				$this->view->historyStatusArr = $historyStatusArr;

				$cust_approver = 1;

				$bg_submission_hisotrys = $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY_CLOSE')
					->where("BG_NUMBER = ?", $BG_NUMBER)
					->where("CUST_ID IN (?) ", [$selectcomp[0]["CUST_ID"], $data["BG_INSURANCE_CODE"]]);
				$bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

				$getBoundary = $this->_db->select()
					->from('M_APP_BOUNDARY')
					->where('CUST_ID = ?', $data['CUST_ID'])
					->where('BOUNDARY_MIN <= \'' . $data['BG_AMOUNT'] . '\'')
					->where('BOUNDARY_MAX >= \'' . $data['BG_AMOUNT'] . '\'')
					->query()->fetch();

				$checkBoundary = strpos($getBoundary['POLICY'], 'THEN');
				if ($checkBoundary === false) $checkBoundary = strpos($getBoundary['POLICY'], 'AND');
				$checkCount = 1;

				foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {
					// maker
					if ($bg_submission_hisotry['HISTORY_STATUS'] == 1) {
						$makerStatus = 'active';
						$makerIcon = '<i class="fas fa-check"></i>';
						$custlogin = $bg_submission_hisotry['USER_LOGIN'];
						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));
						$align = 'align="center"';
						$this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
					}

					// approver
					if ($checkBoundary === false) {
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
							$approverStatus = 'active';
							$approverIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);
							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));
							$align = 'align="center"';

							$this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
							$this->view->approverStatus = $approverStatus;
						}
					} else {
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
							$approverStatus = 'active';
							$approverIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);
							$customer = $this->_db->fetchAll($selectCust);

							if ($checkCount === 1) {
								$custFullname = $customer[0]['USER_FULLNAME'];
								$checkCount++;
							} else {
								$custFullname = $custFullname . "<br><span>" . $customer[0]['USER_FULLNAME'] . "</span>";
								$checkCount = 1;
							}

							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));
							$align = 'align="center"';

							$this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
							$this->view->approverStatus = $approverStatus;
						}
					}

					if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
						//if releaser
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 3) {
							$releaserStatus = 'active';
							$releaserIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);
							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));
							$align = 'align="center"';

							$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
							$this->view->releaserStatus = $releaserStatus;
						}
					} else {
						//if releaser
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 3) {
							$releaserStatus = 'active';
							$releaserIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);
							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));
							$align = 'align="center"';

							$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
							$this->view->releaserStatus = $releaserStatus;
						}
					}
				}

				foreach ($history as $row) {
					if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
						if ($row['HISTORY_STATUS'] == 8 || $row['HISTORY_STATUS'] == 24) {
							$makerStatus = 'active';
							$insuranceIcon = '<i class="fas fa-check"></i>';
							$insuranceStatus = 'active';
							$reviewStatus = '';

							$makerOngoing = '';
							$reviewerOngoing = '';
							$approverOngoing = '';

							$custlogin = $row['USER_LOGIN'];

							$selectCust    = $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin);
							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							$insuranceApprovedBy = $custFullname;

							$align = 'align="center"';
							$marginLeft = '';

							if ($cust_reviewer == 0 && $cust_approver == 0) {
								$align = '';
								$marginLeft = 'style="margin-left: 15px;"';
							}

							$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
							$this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
						}
					}

					if ($row['HISTORY_STATUS'] == 22) {
						$verifyStatus = 'active';
						$verifyIcon = '<i class="fas fa-check"></i>';

						$verifyOngoing = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
							$reviewerOngoing = '';
							$verifyOngoing = '';
							$approverOngoing = '';
							//$releaserOngoing = 'ongoing';
							$releaserOngoing = '';
						} else {
							//$reviewerOngoing = 'ongoing';
							$reviewerOngoing = '';
							$verifyOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						}

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_BUSER')
							->where("BUSER_ID = ?", $custlogin);
						// ->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['BUSER_NAME'];
						$custEmail 	  = $customer[0]['USER_EMAIL'];
						$custPhone	  = $customer[0]['USER_PHONE'];

						$verifyApprovedBy = $custFullname;

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

						$align = 'align="center"';
						$marginRight = '';

						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginRight = 'style="margin-right: 15px;"';
						}

						$this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $verifyApprovedBy . '</span></div>';
					}
					//if reviewer done

					//if approver done
					if ($row['HISTORY_STATUS'] == 16) {
						$makerStatus = 'active';
						$approveStatus = '';
						$reviewStatus = '';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = 'ongoing';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectuserapp	= $this->_db->select()
							->from(array('C' => 'T_APPROVAL'), array(
								'*'
							))
							->where("C.PS_NUMBER = ?", $BG_NUMBER)
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin);
						$userapprove = $this->_db->fetchAll($selectuserapp);

						//tampung data user yang sudah approve
						if (!empty($userapprove) && empty($userid)) {
							foreach ($userapprove as $vl) {
								$userid[] = $vl['USER_ID'];
							}
						}

						$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
					}
					//if releaser done
					if ($row['HISTORY_STATUS'] == 5) {
						$makerStatus = 'active';
						/*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
						$approveStatus = '';
						$reviewStatus = '';
						$releaseStatus = '';
						$releaseIcon = '';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						// $custEmail 	  = $customer[0]['USER_EMAIL'];
						// $custPhone	  = $customer[0]['USER_PHONE'];

						$releaserApprovedBy = $custFullname;

						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						//$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
					}
				}

				//approvernamecircle jika sudah ada yang approve
				if (!empty($userid)) {

					$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

					$flipAlphabet = array_flip($alphabet);

					$approvedNameList = array();
					$i = 0;
					
					foreach ($userid as $key => $value) {

						//select utk nama dan email
						$selectusername = $this->_db->select()
							->from(array('M_USER'), array(
								'*'
							))
							->where("USER_ID = ?", (string) $value)
							->where("CUST_ID = ?", (string) $this->_custIdLogin);

						$username = $this->_db->fetchAll($selectusername);

						//select utk cek user berada di grup apa
						$selectusergroup	= $this->_db->select()
							->from(array('C' => 'M_APP_GROUP_USER'), array(
								'*'
							))
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							->where("C.USER_ID 	= ?", (string) $value);

						$usergroup = $this->_db->fetchAll($selectusergroup);

						$groupuserid = $usergroup[0]['GROUP_USER_ID'];
						$groupusername = $usergroup[0]['USER_ID'];
						$groupuseridexplode = explode("_", $groupuserid);

						if ($groupuseridexplode[0] == "S") {
							$usergroupid = "SG";
						} else {
							$usergroupid = $alphabet[$groupuseridexplode[2]];
						}

						array_push($approvedNameList, $username[0]['USER_FULLNAME']);
						$efdate = $approveEfDate[$i];

						$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
						$i++;
					}
					$this->view->approverApprovedBy = $approverApprovedBy;

					//kalau sudah approve semua
					if (!$checkBoundary) {
						$approveStatus = '';
						$approverOngoing = '';
						$approveIcon = '';
						$releaserOngoing = 'ongoing';
					}
				}

				$selectsuperuser = $this->_db->select()
					->from(array('C' => 'T_APPROVAL'))
					->where("C.PS_NUMBER 	= ?", $BG_NUMBER)
					->where("C.GROUP 	= 'SG'");
				$superuser = $this->_db->fetchAll($selectsuperuser);

				if (!empty($superuser)) {
					$userid = $superuser[0]['USER_ID'];

					//select utk nama dan email
					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("USER_ID = ?", (string) $userid)
						->where("CUST_ID = ?", (string) $this->_custIdLogin);

					$username = $this->_db->fetchAll($selectusername);

					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

					$approveStatus = 'active';
					$approverOngoing = '';
					$approveIcon = '<i class="fas fa-check"></i>';
					$releaserOngoing = 'ongoing';
				}

				//define circle
				$makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . ' </button>';
				$insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . ' </button>';

				foreach ($reviewerList as $key => $value) {
					$textColor = '';
					if ($value == $reviewerApprovedBy) {
						$textColor = 'text-white-50';
					}

					$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}

				$reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '</button>';

				$verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $verifyStatus . '  hovertext" disabled>' . $verifyIcon . '</button>';

				$groupNameList = $approverUserList['GROUP_NAME'];
				unset($approverUserList['GROUP_NAME']);

				if ($approverUserList != '') {
					foreach ($approverUserList as $key => $value) {
						$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
						$i = 1;
						foreach ($value as $key2 => $value2) {

							$textColor = '';
							if (in_array($value2, $approvedNameList)) {
								$textColor = 'text-white-50';
							}

							if ($i == count($value)) {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
							} else {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
							}
							$i++;
						}
					}
				} else {
					$approverListdata = 'There is no Approver User';
				}
				// 
				$spandata = '';
				if (!empty($approverListdata) && !$error_msg2) {
					$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
				}

				$approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . ' ' . $spandata . '</button>';

				foreach ($releaserList as $key => $value) {
					$textColor = '';
					if ($value == $releaserApprovedBy) {
						$textColor = 'text-white-50';
					}

					$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				$releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '</button>';

				$this->view->policyBoundary = $policyBoundary;
				$this->view->makerNameCircle = $makerNameCircle;
				$this->view->reviewerNameCircle = $reviewerNameCircle;
				$this->view->verifyNameCircle = $verifyNameCircle;
				$this->view->insuranceNameCircle = $insuranceNameCircle;

				$this->view->approverNameCircle = $approverNameCircle;
				$this->view->releaserNameCircle = $releaserNameCircle;

				$this->view->makerStatus = $makerStatus;
				$this->view->verifyStatus = $verifyStatus;
				$this->view->approveStatus = $approveStatus;
				$this->view->reviewStatus = $reviewStatus;
				$this->view->releaseStatus = $releaseStatus;
				// ========================= History Workflow =========================

				$bgType = $conf["bg"]["type"]["desc"];
				$bgCode = $conf["bg"]["type"]["code"];
				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));
				$this->view->arrbgType = $arrbgType;

				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				$BgType = $conf["bgclosing"]["status"]["desc"];
				$BgCode = $conf["bgclosing"]["status"]["code"];
				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));
				$this->view->arrStatus = $arrStatus;

				$bgcgType = $conf["bgcg"]["type"]["desc"];
				$bgcgCode = $conf["bgcg"]["type"]["code"];
				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
				$this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->BG_REG_NUMBER = $data['BG_REG'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];
				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->contact_person = $data['RECIPIENT_CP'];
				$this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
				$this->view->contact_email = $data['RECIPIENT_EMAIL'];
				$this->view->data = $data;
				$this->view->fileName = $data['FILE'];
				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];
				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];
				$this->view->status = $data['BG_STATUS'];
				$this->view->claim_period = $settings->getSetting('claim_period');
				$this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
				$this->view->timeDifference = $this->calculateTimeSpan($data['CLAIM_FIRST_VERIFIED']);

				$bgdocType = $conf["bgdoc"]["type"]["desc"];
				$bgdocCode = $conf["bgdoc"]["type"]["code"];
				$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
				$this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];

				$this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
				$this->view->usage_purpose = $data['USAGE_PURPOSE'];
				$this->view->comment = $data['SERVICE'];

				if (!empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$select = $this->_db->select()
						->from(
							array('a' => 'T_BANK_GUARANTEE_HISTORY'),
							array(
								'a.USER_LOGIN', 'u_name' => 'b.USER_FULLNAME', 'b_name' => 'c.BUSER_NAME',
								'a.DATE_TIME', 'a.BG_REASON', 'a.HISTORY_STATUS', 'a.BG_REG_NUMBER'
							)
						)
						->joinLeft(
							array('b' => 'M_USER'),
							'a.USER_LOGIN = b.USER_ID AND a.CUST_ID = b.CUST_ID',
							array()
						)
						->joinLeft(array('c' => 'M_BUSER'), 'a.USER_LOGIN = c.BUSER_ID', array())
						->where('a.BG_REG_NUMBER = ?', (string) $data['BG_REG'])
						->where('a.HISTORY_STATUS = ?', (string) $data['BG_STATUS'])
						->group('a.HISTORY_ID')
						->order('a.DATE_TIME');
					$result = $this->_db->fetchAll($select);

					if (!empty($result)) {
						$data['REASON'] = $result['0']['BG_REASON'];
					}
					$this->view->reqrepair = true;
				}

				// REASON
				$cekHistory = $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY_CLOSE')
					->where('BG_NUMBER = ?', $data['BG_NUMBER'])
					->order('DATE_TIME DESC')
					->query()->fetch();

				if ($cekHistory['HISTORY_STATUS'] == '9' || $cekHistory['HISTORY_STATUS'] == '13') {
					$this->view->reason = $cekHistory['BG_REASON'] . ' (' . $cekHistory['USER_FROM'] . ' - ' .  $cekHistory['USER_LOGIN'] . ')';
				}

				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {
						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
								$insBranchCode =   $value['PS_FIELDVALUE'];
							}
						} else {
							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}

				if (!empty($bgdatadetailclose)) {
					foreach ($bgdatadetailclose as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Beneficiary is own account') {
							$this->view->rekeningPenerima =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'SWIFT CODE') {
							$domesticBanks = $this->_db->select()
								->from(array('A' => 'M_DOMESTIC_BANK_TABLE'), array('*'))
								->where('A.SWIFT_CODE = ?', $value['PS_FIELDVALUE'])
								->query()->fetchAll();

							$this->view->swiftCode = $domesticBanks[0]['BANK_NAME'];
							$this->view->swiftCodeParam = $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account CCY') {
							$this->view->valutaRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account Number') {
							$this->view->nomorRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account Name') {
							$this->view->namaRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Address') {
							$this->view->alamatRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Transaction CCY') {
							$this->view->valutaTrasaksi =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Transaction Amount') {
							$this->view->totalTransaksi =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Remark 1') {
							$this->view->berita =   $value['PS_FIELDVALUE'];
						}
					}
				}

				$this->view->bankname = $conf['app']['bankname'];

				$download = $this->_getParam('download');
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				// Marginal Deposit Eksisting ---------------
				$bgdatamdeks = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID =' . $this->_db->quote($data['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $data['BG_REG'])
					->query()->fetchAll();

				$mdeksisting = null;
				$saveEscrow = 0;

				if (count($bgdatamdeks) > 1) {
					foreach ($bgdatamdeks as $key => $value) {
						if (strtolower($value["M_ACCT_DESC"]) == "giro" || $value["M_ACCT_TYPE"] == "D") {
							continue;
						};
						$serviceAccount = new Service_Account($value['ACCT'], null);
						$accountInfo = $serviceAccount->inquiryAccontInfo();
						$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
						$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
						$listAccount = array_search($value['ACCT'], array_column($saveResult, "account_number"));

						if ($saveResult[$listAccount]["product_type"] == 'J1') {
							continue;
						} else {
							$mdeksisting += floatval($value["AMOUNT"]);
						}

						if (strtolower($value["ACCT_DESC"] == 'Escrow')) $saveEscrow += floatval($value["AMOUNT"]);
					}
				} else {
					$mdeksisting += floatval($bgdatamdeks[0]['AMOUNT']);
				}
				$this->view->totaleksisting = $mdeksisting;
				$this->view->escrow = $saveEscrow;

				// Pelepasan dana ---------------
				$mdclose = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$groupedArray = array();
				$totalClose = null;
				foreach ($mdclose as $row) {
					$index = $row['ACCT_INDEX'];
					if (!isset($groupedArray[$index])) {
						$groupedArray[$index] = array(
							'ownAccount' => '',
							'ccyAccount' => '',
							'numberAccount' => '',
							'nameAccount' => '',
							'ccyTransaction' => '',
							'amountTransaction' => '',
						);
					}
					if ($row['PS_FIELDNAME'] == 'Beneficiary is own account') {
						$groupedArray[$index]['ownAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account CCY') {
						$groupedArray[$index]['ccyAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account Number') {
						$groupedArray[$index]['numberAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account Name') {
						$groupedArray[$index]['nameAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Transaction CCY') {
						$groupedArray[$index]['ccyTransaction'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Transaction Amount') {
						$groupedArray[$index]['amountTransaction'] = $row['PS_FIELDVALUE'];
						$totalClose += $row['PS_FIELDVALUE'];
					}
				}
				$groupedArray = array_values($groupedArray);

				$this->view->totalclose = $totalClose;
				$this->view->mdclose = $groupedArray;

				$download = $this->_getParam('certfinal');
				if ($download == 1) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					return $this->_helper->download->file("BG_NO_" . $data["BG_NUMBER"] . ".pdf", $attahmentDestination . $data['DOCUMENT_ID'] . ".pdf");
				}

				if ($this->_request->isPost()) {
					$verify = $this->_getParam('release');
					$reject = $this->_getParam('reject');
					$repair = $this->_getParam('repair');

					$bgClose = $this->_db->select()
						->from('TEMP_BANK_GUARANTEE_CLOSE')
						->where('CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
						->order('LASTUPDATEDBY DESC')
						->query()->fetch();

					$bgclosingType = $conf["bgclosing"]["changetype"]["desc"];
					$bgclosingCode = $conf["bgclosing"]["changetype"]["code"];
					$arrbgclosingType = array_combine(array_values($bgclosingCode), array_values($bgclosingType));

					$tipeUsulan = $arrbgclosingType[$bgClose['CHANGE_TYPE']];

					$bgclosingStatus = $conf["bgclosing"]["status"]["desc"];
					$bgclosingstatusCode = $conf["bgclosing"]["status"]["code"];
					$arrbgclosingStatus = array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));

					$mSetting = $settings->getAllSetting();
					
					if ($verify) {
						$statusUsulanVerify = $arrbgclosingStatus[5];

						// Update data ke table TEMP_BANK_GUARANTEE_CLOSE
						$dataclose = [
							'SUGGESTION_STATUS' => '5',
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'BANK',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						];

						if ($data['CLAIM_FIRST_VERIFIED'] == '0000-00-00 00:00:00') {
							$dataclose['CLAIM_FIRST_VERIFIED'] = new Zend_Db_Expr("now()");
						}

						$whereclose['CLOSE_REF_NUMBER = ?'] = $data['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataclose, $whereclose);

						// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
						$historyInsert = [
							'DATE_TIME' => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER' => $data['CLOSE_REF_NUMBER'],
							'BG_NUMBER' => $data['BG_NUMBER'],
							'CUST_ID' => '-',
							'USER_FROM' => 'BANK',
							'USER_LOGIN' => $this->_userIdLogin,
							'BG_REASON' => 'NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '',
							'HISTORY_STATUS' => 8,
						];

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						// Simpan log aktifitas user ke table T_BACTIVITY
						if ($this->view->hasPrivilege("VPCC")) $privilege = 'VPCC';
						if ($this->view->hasPrivilege("VPNC")) $privilege = 'VPNC';

						if ($data['CHANGE_TYPE_CLOSE'] == '1') {
							Application_Helper_General::writeLog($privilege, 'Verifikasi Pengajuan Penutupan untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');
						} elseif ($data['CHANGE_TYPE_CLOSE'] == '3') {
							Application_Helper_General::writeLog($privilege, 'Verifikasi Pengajuan Klaim  untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');
						}

						// ----------------------------- Email notification -----------------------------
						// ----- Notifikasi email ke Reviewer / next approver -----
						$contractName = $this->handleDokumenUnderlying($data['BG_REG']);

						if ($bgClose['CHANGE_TYPE'] == 1) {
							$titleChangeType = 'PENUTUPAN';
						} else if ($bgClose['CHANGE_TYPE'] == 3) {
							$titleChangeType = 'KLAIM';
						} else {
							$titleChangeType = 'PENUTUPAN / KLAIM';
						}

						$dataContentEmail = [
							'[[title_change_type]]' => $titleChangeType,
							'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $data["BG_NUMBER"],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $data["USAGE_PURPOSE_DESC"],
							'[[cust_name]]' => $data["CUST_NAME"],
							'[[recipient_name]]' => $data['RECIPIENT_NAME'],
							'[[bg_amount]]' => $this->view->valutaTrasaksi . ' ' . number_format($data["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' => $arrbgcg[$data['COUNTER_WARRANTY_TYPE']],
							'[[change_type]]' => $tipeUsulan,
							'[[suggestion_status]]' => $statusUsulanVerify,
							'[[repair_notes]]' => '-',
							'[[master_bank_name]]' => $mSetting["master_bank_name"],
							'[[cust_cp]]' => $data['CUST_CP'],
							'[[recipient_name]]' => $data["RECIPIENT_NAME"],
							'[[transaction_amount]]' => $this->view->valutaTrasaksi . ' ' . number_format($this->view->totalTransaksi, 2),
							'[[master_bank_email]]' => $mSetting["master_bank_email"],
							'[[master_bank_telp]]' => $mSetting["master_bank_telp"],
							'[[master_app_name]]' => $mSetting["master_bank_app_name"],
							'[[recipient_cp]]' => $data['RECIPIENT_CP'],
							'[[bank_name]]' => $this->view->swiftCode,
							'[[beneficiary_account_number]]' => $this->view->nomorRekening,
							'[[beneficiary_account_name]]' => $this->view->namaRekening,
						];

						$data['COUNTER_WARRANTY_TYPE'] == '1'
							? $bprivi = ['RPCC'] // cash collateral
							: $bprivi = ['RPNC']; // non cash collateral
							
						$subjectToReviewer = $statusUsulanVerify . ' BG NO. ' . $data["BG_NUMBER"];
						$emailTemplateToReviewer = $mSetting['bemailtemplate_6A'];
						$emailContentToReviewer = strtr($emailTemplateToReviewer, $dataContentEmail);

						$busers = $this->buserEmail($bprivi, $data['BG_BRANCH']);
						if (!empty($busers)) {
							foreach ($busers as $buser) {
								Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], $subjectToReviewer, $emailContentToReviewer);
							}
						}

						// Klaim Obligee
						if ($data['CHANGE_TYPE_CLOSE'] == '3') {
							// ----- Notifikasi email ke Nasabah/Prinsipal -----
							$emailPrinciple = $data['T_CUST_EMAIL'];
							$subjectToPrinciple = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
							$emailTemplateToPrinciple = $mSetting['bemailtemplate_4A'];
	
							$emailContentToPrinciple = strtr($emailTemplateToPrinciple, $dataContentEmail);
							Application_Helper_Email::sendEmail($emailPrinciple, $subjectToPrinciple, $emailContentToPrinciple);
	
							// ----- Notifikasi email ke Obligee -----
							$emailObligee = $data['RECIPIENT_EMAIL'];
							$subjectToObligee = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
							$emailTemplateToObligee = $mSetting['bemailtemplate_4B'];
	
							$emailContentToObligee = strtr($emailTemplateToObligee, $dataContentEmail);
							Application_Helper_Email::sendEmail($emailObligee, $subjectToObligee, $emailContentToObligee);
							
							// ----- Notifikasi email ke Asuransi -----
							if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
								$emailInsurance = $insuranceBranch[0]["INS_BRANCH_EMAIL"];
								$subjectToInsurance = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
								$emailTemplateToInsurance = $mSetting['bemailtemplate_4C'];

								$branchFullName = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $insuranceBranch[0]["CUST_ID"])
									->query()->fetchAll();

								$dataContentToInsurance = array_merge($dataContentEmail, [
									'[[insurance_name]]' => $branchFullName[0]['CUST_NAME'],
									'[[insurance_branch_name]]' => $insuranceBranch[0]["INS_BRANCH_NAME"],
								]);

								$emailContentToInsurance = strtr($emailTemplateToInsurance, $dataContentToInsurance);
								Application_Helper_Email::sendEmail($emailInsurance, $subjectToInsurance, $emailContentToInsurance);
							}

							// ----- Notifikasi email ke Kacab Penerbitan -----
							$subjectToBranch = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
							$emailTemplateToBranch = $mSetting['bemailtemplate_4D'];

							$emailContentToBranch = strtr($emailTemplateToBranch, $dataContentEmail);
							Application_Helper_Email::sendEmail($selectbranch[0]['BRANCH_EMAIL'], $subjectToBranch, $emailContentToBranch);

							// ----- Notifikasi email ke Group  -----
							switch (true) {
								case $data['COUNTER_WARRANTY_TYPE'] == '1':
									// SEND EMAIL TO GROUP CASH COL
									if ($mSetting['email_group_cashcoll']) Application_Helper_Email::sendEmail($mSetting['email_group_cashcoll'], $subjectToBranch, $emailContentToBranch);
									break;

								case $data['COUNTER_WARRANTY_TYPE'] == '2':
									// SEND EMAIL TO GROUP LINE FACILITY
									if ($mSetting['email_group_linefacility']) Application_Helper_Email::sendEmail($mSetting['email_group_linefacility'], $subjectToBranch, $emailContentToBranch);
									break;

								case $data['COUNTER_WARRANTY_TYPE'] == '3':
									// SEND EMAIL TO GROUP ASURANSI
									if ($mSetting['email_group_insurance']) Application_Helper_Email::sendEmail($mSetting['email_group_insurance'], $subjectToBranch, $emailContentToBranch);
									break;

								default:
									break;
							}
						}
						// ----------------------------- Email notification -----------------------------

						// DELETE OLD APPROVAL
						$this->_db->delete('T_BGAPPROVAL', [
							'REG_NUMBER = ?' => $data['CLOSE_REF_NUMBER']
						]);

						$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						$this->_redirect('/notification/success/index');
					}

					if ($reject) {
						$statusUsulanReject = $arrbgclosingStatus[7];

						$notes = $this->_getParam('PS_REASON_REJECT');

						// Update data ke table TEMP_BANK_GUARANTEE_CLOSE
						$dataclose = [
							'SUGGESTION_STATUS' => '7',
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'BANK',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						];

						$whereclose['CLOSE_REF_NUMBER = ?'] = $data['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataclose, $whereclose);

						// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
						$historyInsert = [
							'DATE_TIME' => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER' => $data['CLOSE_REF_NUMBER'],
							'BG_NUMBER' => $data['BG_NUMBER'],
							'CUST_ID' => '-',
							'USER_FROM' => 'BANK',
							'USER_LOGIN' => $this->_userIdLogin,
							'BG_REASON' => $notes,
							'HISTORY_STATUS' => 7,
						];

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						// Simpan log aktifitas user ke table T_BACTIVITY
						if ($this->view->hasPrivilege("VCCS")) $privilege = 'VCCS';
						if ($this->view->hasPrivilege("VNCS")) $privilege = 'VNCS';

						if ($data['CHANGE_TYPE_CLOSE'] == '1') {
							Application_Helper_General::writeLog($privilege, 'Penolakan Pengajuan Penutupan untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . 'Catatan : ' . $notes . '');
						} elseif ($data['CHANGE_TYPE_CLOSE'] == '3') {
							Application_Helper_General::writeLog($privilege, 'Penolakan Pengajuan Klaim untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . 'Catatan : ' . $notes . '');
						}

						// ----------------------------- Email notification -----------------------------
						$contractName = $this->handleDokumenUnderlying($data['BG_REG']);
						if ($data['CHANGE_TYPE_CLOSE'] == 1) {
							$titleChangeType = 'PENUTUPAN';
						} else if ($data['CHANGE_TYPE_CLOSE'] == 3) {
							$titleChangeType = 'KLAIM';
						} else {
							$titleChangeType = 'PENUTUPAN / KLAIM';
						}
						
						$dataEmail = [
							'[[title_change_type]]' => $titleChangeType,
							'[[cust_cp]]' => $data['CUST_CP'],
							'[[cust_name]]' => $data["CUST_NAME"],
							'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $data["BG_NUMBER"],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $data["USAGE_PURPOSE_DESC"],
							'[[recipient_cp]]' => $data['RECIPIENT_CP'],
							'[[recipient_name]]' => $data["RECIPIENT_NAME"],
							'[[bg_amount]]' => $this->view->valutaTrasaksi . ' ' . number_format($data["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' => $arrbgcg[$data['COUNTER_WARRANTY_TYPE']],
							'[[change_type]]' => $tipeUsulan,
							'[[suggestion_status]]' => $statusUsulanReject,
							'[[repair_notes]]' => $notes,
							'[[master_bank_email]]' => $mSetting["master_bank_email"],
							'[[master_bank_telp]]' => $mSetting["master_bank_telp"],
							'[[master_app_name]]' => $mSetting["master_bank_app_name"],
							'[[master_bank_name]]' => $mSetting["master_bank_name"],
							'[[recipient_name]]' => $data['RECIPIENT_NAME'],
						];

						// ----- Notifikasi email ke Nasabah/Prinsipal -----
						$emailPrinciple = $data['T_CUST_EMAIL'];
						$subjectToPrinciple = 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN ' . $titleChangeType . ' BG NO. ' . $data["BG_NUMBER"];
						$emailTemplateToPrinciple = $mSetting['bemailtemplate_7B'];

						$emailContentToPrinciple = strtr($emailTemplateToPrinciple, $dataEmail);
						Application_Helper_Email::sendEmail($emailPrinciple, $subjectToPrinciple, $emailContentToPrinciple);

						// ----- Notifikasi email ke Obligee -----
						$emailObligee = $data['RECIPIENT_EMAIL'];
						$subjectToObligee = 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN ' . $titleChangeType . ' BG NO. ' . $data["BG_NUMBER"];
						$emailTemplateToObligee = $mSetting['bemailtemplate_7C'];

						$emailContentToObligee = strtr($emailTemplateToObligee, $dataEmail);
						Application_Helper_Email::sendEmail($emailObligee, $subjectToObligee, $emailContentToObligee);

						// Klaim Obligee & insurance
						if ($data['CHANGE_TYPE_CLOSE'] == 3) {
							// ----- Notifikasi email ke Kacab Penerbitan -----
							$subjectToBranch = 'PENOLAKAN PENGAJUAN ' . $titleChangeType . ' BG NO. ' . $data["BG_NUMBER"];
							$emailTemplateToBranch = $mSetting['bemailtemplate_7A'];

							$emailContentToBranch = strtr($emailTemplateToBranch, $dataEmail);
							Application_Helper_Email::sendEmail($selectbranch[0]['BRANCH_EMAIL'], $subjectToBranch, $emailContentToBranch);

							// ----- Notifikasi email ke Group  -----
							switch (true) {
								case $data['COUNTER_WARRANTY_TYPE'] == '1':
									// SEND EMAIL TO GROUP CASH COL
									if ($mSetting['email_group_cashcoll']) Application_Helper_Email::sendEmail($mSetting['email_group_cashcoll'], $subjectToBranch, $emailContentToBranch);
									break;

								case $data['COUNTER_WARRANTY_TYPE'] == '2':
									// SEND EMAIL TO GROUP LINE FACILITY
									if ($mSetting['email_group_linefacility']) Application_Helper_Email::sendEmail($mSetting['email_group_linefacility'], $subjectToBranch, $emailContentToBranch);
									break;

								case $data['COUNTER_WARRANTY_TYPE'] == '3':
									// SEND EMAIL TO GROUP ASURANSI
									if ($mSetting['email_group_insurance']) Application_Helper_Email::sendEmail($mSetting['email_group_insurance'], $subjectToBranch, $emailContentToBranch);
									break;

								default:
									break;
							}
							
							if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
								$emailInsurance = $insuranceBranch[0]["INS_BRANCH_EMAIL"];
								$subjectToInsurance = 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
								$emailTemplateToInsurance = $mSetting['bemailtemplate_7D'];

								$branchFullName = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $insuranceBranch[0]["CUST_ID"])
									->query()->fetchAll();
								
								$dataContentToInsurance = array_merge($dataEmail, [
									'[[insurance_name]]' => $branchFullName[0]['CUST_NAME'],
									'[[insurance_branch_name]]' => $insuranceBranch[0]["INS_BRANCH_NAME"],
								]);

								$emailContentToInsurance = strtr($emailTemplateToInsurance, $dataContentToInsurance);
								Application_Helper_Email::sendEmail($emailInsurance, $subjectToInsurance, $emailContentToInsurance);
							}
						}
						// ----------------------------- Email notification -----------------------------

						$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						$this->_redirect('/notification/success/index');
					}

					if ($repair) {
						if ($data['CHANGE_TYPE_CLOSE'] == '1') $statusUsulanRepair = $arrbgclosingStatus[8];
						if ($data['CHANGE_TYPE_CLOSE'] == '3') $statusUsulanRepair = $arrbgclosingStatus[10];

						$notes = $this->_getParam('PS_REASON_REPAIR');

						// Update data ke table TEMP_BANK_GUARANTEE_CLOSE
						$dataclose = [
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'BANK',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						];

						if ($data['CHANGE_TYPE_CLOSE'] == '1') $dataclose['SUGGESTION_STATUS'] = '8';
						if ($data['CHANGE_TYPE_CLOSE'] == '3') $dataclose['SUGGESTION_STATUS'] = '10';

						$whereclose['CLOSE_REF_NUMBER = ?'] = $data['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataclose, $whereclose);

						// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
						if ($data['CHANGE_TYPE_CLOSE'] == '3') {
							$historyStatus = 6;
						} else {
							$historyStatus = 5;
						}

						$historyInsert = [
							'DATE_TIME' => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER' => $data['CLOSE_REF_NUMBER'],
							'BG_NUMBER' => $data['BG_NUMBER'],
							'CUST_ID' => '-',
							'USER_FROM' => 'BANK',
							'USER_LOGIN' => $this->_userIdLogin,
							'BG_REASON' => $notes,
							'HISTORY_STATUS' => $historyStatus,
						];

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						// Simpan log aktifitas user ke table T_BACTIVITY
						if ($this->view->hasPrivilege("VCCS")) $privilege = 'VCCS';
						if ($this->view->hasPrivilege("VNCS")) $privilege = 'VNCS';

						if ($data['CHANGE_TYPE_CLOSE'] == '1') {
							Application_Helper_General::writeLog($privilege, 'Permintaan Perbaikan Pengajuan Penutupan untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . 'Catatan : ' . $notes . '');
						} elseif ($data['CHANGE_TYPE_CLOSE'] == '3') {
							Application_Helper_General::writeLog($privilege, 'Permintaan Perbaikan Pengajuan Klaim untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . 'Catatan : ' . $notes . '');
						}

						// ----------------------------- Email notification -----------------------------
						if ($data['CHANGE_TYPE_CLOSE'] == 1) {
							$titleChangeType = 'PENUTUPAN';
						} else if ($data['CHANGE_TYPE_CLOSE'] == 3) {
							$titleChangeType = 'KLAIM';
						} else {
							$titleChangeType = 'PENUTUPAN / KLAIM';
						}
						
						$contractName = $this->handleDokumenUnderlying($data['BG_REG']);

						$dataContentEmail = [
							'[[title_change_type]]' => $titleChangeType,
							'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $data["BG_NUMBER"],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $data["USAGE_PURPOSE_DESC"],
							'[[cust_name]]' => $data["CUST_NAME"],
							'[[recipient_cp]]' => $data['RECIPIENT_CP'],
							'[[recipient_name]]' => $data['RECIPIENT_NAME'],
							'[[bg_amount]]' => $this->view->valutaTrasaksi . ' ' . number_format($data["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' => $arrbgcg[$data['COUNTER_WARRANTY_TYPE']],
							'[[change_type]]' => $tipeUsulan,
							'[[suggestion_status]]' => $statusUsulanRepair,
							'[[repair_notes]]' => $notes,
							'[[master_bank_name]]' => $mSetting["master_bank_name"],
							'[[cust_cp]]' => $data['CUST_CP'],
							'[[master_bank_email]]' => $mSetting["master_bank_email"],
							'[[master_bank_telp]]' => $mSetting["master_bank_telp"],
							'[[master_app_name]]' => $mSetting["master_bank_app_name"],
						];

						if ($data['CHANGE_TYPE_CLOSE'] == '3') {
							// ----- Notifikasi email ke Sesama rekan bank (yang akan melakukan perbaikan) -----
							$subjectToRepair = $statusUsulanRepair . ' BG NO. ' . $data['BG_NUMBER'];
							$emailTemplateToRepair = $mSetting['bemailtemplate_6A'];
	
							$data['COUNTER_WARRANTY_TYPE'] == '1'
								? $bprivi = ['VPCC'] // cash collateral
								: $bprivi = ['VPNC', 'PADK']; // non cash collateral
	
							$bpriviGroup = $this->_db->select()
								->from('M_BPRIVI_GROUP')
								->where('BPRIVI_ID IN (?)', $bprivi)
								->query()->fetchAll();
	
							$bpriviGroups = array_column($bpriviGroup, 'BGROUP_ID');
	
							$busers = $this->_db->select()
								->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
								->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
								->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
								->where('MB.BGROUP_ID IN (?)', $bpriviGroups)
								->where('MBR.BRANCH_CODE = ?', $data['BG_BRANCH'])
								->query()->fetchAll();
	
							if (!empty($busers)) {
								foreach ($busers as $buser) {
									$emailContentToRepair = strtr($emailTemplateToRepair, $dataContentEmail);
									Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], $subjectToRepair, $emailContentToRepair);
								}
							}

							// Branch coverage
							try {
								$getBranchCoverage = $this->_db->select()
									->from('M_BRANCH_COVERAGE', ['BRANCH_COVERAGE_CODE', 'BRANCH_CODE'])
									->where('BRANCH_CODE = ?', $data['BG_BRANCH'])
									->query()->fetchAll();

								if (!empty($getBranchCoverage)) {
									$busersBts = $this->_db->select()
										->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
										->join(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', ['BRANCH_EMAIL'])
										->where('MB.BGROUP_ID IN (?)', $bpriviGroups)
										->where('MBR.BRANCH_CODE IN (?)', array_column($getBranchCoverage, 'BRANCH_COVERAGE_CODE'))
										->query()->fetchAll();
			
									if (!empty($busersBts)) {
										foreach ($busersBts as $buserBts) {
											$emailContentToBts = strtr($emailTemplateToRepair, $dataContentEmail);
											Application_Helper_Email::sendEmail($buserBts['BUSER_EMAIL'], $subjectToRepair, $emailContentToBts);
										}
									}
								}
			
							} catch (\Throwable $th) {
								//throw $th;
							}
						} else if ($data['CHANGE_TYPE_CLOSE'] == '3') {
							// ----- Notifikasi email ke Nasabah/Prinsipal -----
							$emailPrinciple = $data['T_CUST_EMAIL'];
							$subjectToPrinciple = $statusUsulanRepair . ' BG NO. ' . $data['BG_NUMBER'];
							$emailTemplateToPrinciple = $mSetting['bemailtemplate_6B'];

							$emailContentToPrinciple = strtr($emailTemplateToPrinciple, $dataContentEmail);
							Application_Helper_Email::sendEmail($emailPrinciple, $subjectToPrinciple, $emailContentToPrinciple);

						}

						// ----------------------------- Email notification -----------------------------

						$this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
						$this->_redirect('/notification/success/index');
					}

					$back = $this->_getParam('back');
					if ($back) {
						$this->_redirect('/bgclosingworkflow/verification');
					}
				}

				// Simpan log aktifitas user ke table T_BACTIVITY
				if ($this->view->hasPrivilege("VVCC")) $privilege = 'VVCC';
				if ($this->view->hasPrivilege("VVNC")) $privilege = 'VVNC';

				if ($data['CHANGE_TYPE_CLOSE'] == '1') {
					Application_Helper_General::writeLog($privilege, $this->language->_('Lihat Detail Pengajuan Penutupan dalam Proses Verifikasi Bank untuk BG No') . ' : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER']);
				} elseif ($data['CHANGE_TYPE_CLOSE'] == '3') {
					Application_Helper_General::writeLog($privilege, $this->language->_('Lihat Detail Pengajuan Klaim dalam Proses Verifikasi Bank untuk BG No') . ' : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER']);
				}
			}
		}
	}

	function handleDokumenUnderlying($bgRegNumber) {
		$dokumenUnderlying = $this->_db->select()
			->from('T_BANK_GUARANTEE_UNDERLYING')
			->where('BG_REG_NUMBER = ?', $bgRegNumber)
			->query()->fetchAll();
		
		if (empty($dokumenUnderlying)) {
			return '-';
		}
		
		$docName = array_column($dokumenUnderlying, 'DOC_NAME');
		$combinedDocNames = implode(', ', $docName);

		if (strlen($combinedDocNames) > 90) {
			$combinedDocNames = substr($combinedDocNames, 0, 90) . '...';
		}

		$combinedDocNames .= ' (total ' . count($dokumenUnderlying) . ' dokumen)';
		return $combinedDocNames;
	}

	function buserEmail($bprivi, $bgBranch) {
		$bpriviGroup = $this->_db->select()
			->from('M_BPRIVI_GROUP')
			->where('BPRIVI_ID IN (?)', $bprivi)
			->query()->fetchAll();

		$bpriviGroups = array_column($bpriviGroup, 'BGROUP_ID');

		$busers = $this->_db->select()
			->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
			->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
			->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
			->where('MB.BGROUP_ID IN (?)', $bpriviGroups)
			->where('MBR.BRANCH_CODE = ?', $bgBranch)
			->query()->fetchAll();
		
		return $busers;
	}

	function calculateTimeSpan($date)
	{
		$now = new DateTime();
		$dateObj = new DateTime($date);
		$interval = $now->diff($dateObj);

		$days = $interval->days;
		$hours = $interval->h;
		$minutes = $interval->i;

		if ($days > 0) {
			$time = $days . " hari " . $hours . " jam " . $minutes . " menit";
		} else if ($hours > 0) {
			$time = $hours . " jam " . $minutes . " menit";
		} else {
			$time = $minutes . " menit";
		}

		return $time;
	}

	public function downloadlampiranAction()
	{
		// decrypt numb
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token = $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();

		$bgfile     = urldecode($this->_getParam('bgfile'));

		$attahmentDestination = UPLOAD_PATH . '/document/closing/';

		return $this->_helper->download->file($bgfile, $attahmentDestination . $bgfile);
	}

	public function findPolicyBoundary($transfertype, $amount)
	{
		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);

		$datauser = $this->_db->fetchAll($selectuser);
		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{
		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);

		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}

		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;
		return $userlist;
	}

	public function findUserPrivi($privID)
	{
		$selectuser	= $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'))
			->joinLeft(array('B' => 'M_USER'), 'A.FUSER_ID = CONCAT(B.CUST_ID,B.USER_ID)', array('USER_FULLNAME'))
			->where("A.FPRIVI_ID 	= ?", (string) $privID)
			->where("B.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->order('B.USER_FULLNAME ASC');

		//echo $selectuser;die();
		return $datauser = $this->_db->fetchAll($selectuser);
	}

	public function inquirymarginaldepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$regNumber = $this->_getParam('reqNumber');

		$bgdata = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		$optHtml = '<table class="table table-sm mb-1" style="font-size: 1.1em;" id="guarantedTransactions">
						<thead style="background-color: #c0c2c4;">
							<td><i>' . $this->language->_('Hold Seq') . '</i></td>
							<td><i>' . $this->language->_('Nomor Rekening') . '</i></td>
							<td><i>' . $this->language->_('Nama Rekening') . '</i></td>
							<td><i>' . $this->language->_('Tipe') . '</i></td>
							<td><i>' . $this->language->_('Valuta') . '</i></td>
							<td><i>' . $this->language->_('Nominal') . '</i></td>
						</thead>
						<tbody>';

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($bgdata[0]['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		if (!empty($bgdatasplit)) {
			foreach ($bgdatasplit as $key => $row) {
				if (strtolower($row["M_ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D") continue;
				// GET SERVICE 
				$serviceAccount = new Service_Account($row['ACCT'], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($row['ACCT'], array_column($saveResult, "account_number"));

				$row['HOLD_SEQUENCE'] ? $holdSequence = $row['HOLD_SEQUENCE'] : $holdSequence = '-';
				if ($saveResult[$listAccount]["product_type"] == 'J1') {
					continue;
				}
				$optHtml .= '<tr class="table-secondary">
					<td>' . $holdSequence . '</td>
					<td>' . $row['ACCT'] . '</td>
					<td>' . $row['NAME'] . '</td>
					<td>' . $saveResult[$listAccount]["type_desc"] . " " . $saveResult[$listAccount]["product_type"] . '</td>
					<td>' . $saveResult[$listAccount]["currency"] . '</td>
					<td>' . Application_Helper_General::displayMoney($row['AMOUNT']) . '</td>
				</tr>';
			}
		} else {
			$optHtml .= '<tr><td colspan="7" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}
		$optHtml .= '</tbody></table>';
		$optHtml .= '<p style="font-size: 1.2em;" class="mb-1"><font class="text-danger">*</font><i>' . $this->language->_("Rekening Tipe Deposito dan Saving akan dilakukan pelepasan dana ditahan (tanpa pemindahan dana)") . '</i></p>';
		echo $optHtml;
	}

	public function inquiryaccountinfoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);
		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function inquiryrekeningklaimAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		$swiftcode = $this->_request->getParam('swiftcode');
		$acct_name = $this->_request->getParam('acct_name');

		if (strtolower($swiftcode) == 'btanidja') {
			if ($result['response_desc'] == 'Success') //00 = success
			{
				$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
				$result2 = $svcAccountCIF->inquiryCIFAccount();

				$filterBy = $result['account_number']; // or Finance etc.
				$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
					return ($var['account_number'] == $filterBy);
				});

				$singleArr = array();
				foreach ($new as $key => $val) {
					$singleArr = $val;
				}
			} else {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($result);
				return 0;
			}

			if ($singleArr['type_desc'] == 'Deposito') {
				$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountDeposito->inquiryDeposito();

				if ($result3["response_code"] != "0000") {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				$sqlRekeningJaminanExist = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
					->where('A.ACCT = ?', $result['account_number'])
					->query()->fetchAll();

				$result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;

				$get_product_type = current($new)["type"];

				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			} else {
				$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountBalance->inquiryAccountBalance();

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode($result3);
					return 0;
				}

				$get_product_type = current($new)["type"];
				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			}

			$return = array_merge($result, $singleArr, $result3);

			$data['data'] = false;
			is_array($return) ? $return :  $return = $data;

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($return);
		} else {
			$clientUser  =  new SGO_Soap_ClientUser();

			$getInfoBank = $this->_db->select()
				->from('M_DOMESTIC_BANK_TABLE')
				->where('SWIFT_CODE = ?', $swiftcode)
				->query()->fetch();

			$request = [
				'account_number' => $acct_no,
				'bank_code' => $getInfoBank['BANK_CODE']
			];

			$clientUser->callapi('otherbank', NULL, $request);
			$responseService = $clientUser->getResult();

			$result = [
				'valid' => true,
				'account_name' => $responseService['account_name']
			];

			if ($responseService['response_code'] != '0000') {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Rekening tidak ditemukan')
				];
			}

			if (strtolower($responseService['account_name']) !== strtolower($acct_name)) {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Nama dan Nomor Rekening tidak sesuai')
				];
			}

			$result = array_merge($result, array_intersect_key($responseService, ['response_code' => '']));
			echo json_encode($result);
			return;
		}
	}

	public function checkvalidasiAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$bgNumber = $this->_getParam('bgNumber');

		$data = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE')
			->where('BG_NUMBER = ?', $bgNumber)
			->where('CHANGE_TYPE = 3')
			->where('SUGGESTION_STATUS NOT IN (7,11)')
			->query()->fetch();

		if (!empty($data)) {
			$return['status'] = true;
		} else {
			$return['status'] = false;
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($return);
	}

	public function showfinaldocAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// decrypt numb

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

		$file = $this->_request->getParam("file");

		$get_document_id = $this->_db->select()
			->from("T_BANK_GUARANTEE", ["DOCUMENT_ID"])
			->where("BG_NUMBER = ?", $BG_NUMBER)
			->query()->fetch();

		$get_file_name = $get_document_id["DOCUMENT_ID"] . ".pdf";

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=" . $get_file_name);
		// @readfile('/app/bg/library/data/uploads/document/submit/' . $get_file_name);
		@readfile(UPLOAD_PATH . '/document/submit/' . $get_file_name);
	}

	public function removetempAction()
	{
		$this->_db->delete("TEMP_REQUEST_REBATE", "1");
	}

	public function getbusersbgcAction()
	{
		$getprivi = $this->_db->select()
			->from(["MBPG" => "M_BPRIVI_GROUP"], ["*"])
			->where("MBPG.BPRIVI_ID = ?", "SBGC")
			->query()->fetchAll();

		$savegroup = [];
		foreach ($getprivi as $key => $value) {
			array_push($savegroup, $value["BGROUP_ID"]);
		}

		$getbuser = $this->_db->select()
			->from(["MB" => "M_BUSER"], ["*"])
			->joinleft(["BRANCH" => "M_BRANCH"], "BRANCH.ID = MB.BUSER_BRANCH", ["BRANCH.BRANCH_NAME"])
			->where("MB.BGROUP_ID IN (?)", $savegroup);

		$getbuser->where("MB.BUSER_BRANCH = ?", "26");

		$getbuser->where("MB.BUSER_NAME LIKE ?", "%" . "KRI" . "%");

		$getbuser = $getbuser->query()->fetchAll();

		var_dump($getbuser);
		die();
	}

	private function cekProductType($acct)
	{
		$getProductType = $this->_db->select()
			->from('M_PRODUCT')
			->query()->fetchAll();

		$serviceCall = new Service_Account($acct, 'IDR');
		$getBalanceNotDeposito = $serviceCall->inquiryAccountBalance();
		$getBalanceDeposito = $serviceCall->inquiryDeposito();

		$productTypeAcct = ($getBalanceDeposito['response_code'] == '0000' || $getBalanceDeposito['response_code'] == '00') ? $getBalanceDeposito['product_type'] : $getBalanceNotDeposito['product_type'];

		$cekArray = array_filter($getProductType, function ($array) use ($productTypeAcct) {
			return $productTypeAcct == $array['PRODUCT_PLAN'];
		});

		$result = array_shift($cekArray);

		if (!$result) return [false, $productTypeAcct];

		return [true, $productTypeAcct];
	}
}
