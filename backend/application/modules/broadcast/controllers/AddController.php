<?php

require_once 'Zend/Controller/Action.php';

class broadcast_AddController extends Application_Main 
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		if($this->_request->isPost())
		{


			$params = $this->_request->getParams();
		
			/*print_r($params);die;*/
			$filtersName = array(
									// 'sourceFileName'		=> array('StringTrim'),
									'helpTopic'	=> array('StripTags', 'StringTrim'),
								);
			
			$validatorsName = array(
									
									// 'helpTopic' => array(
									// 							'NotEmpty',
									// 							array('StringLength', array('max' => 50)),
									// 							new Zend_Validate_Regex('/^[a-zA-Z 0-9]*$/'),
																
									// 									),
									// 							'messages' => array(
									// 													$this->language->_("Error : Help Topic cannot be left blank"),
									// 													$this->language->_("Error : Help Topic size more than 50 char"),
									// 													$this->language->_("Error : Help Topic Format Must Alpha Numeric"),
									// 												),
															
									'helpTopic' => array(),
									// 'text_content' => array(
									// 							'NotEmpty',
									// 							// array('StringLength', array('max' => 50)),
									// 							// new Zend_Validate_Regex('/^[a-zA-Z 0-9]*$/'),
																
									// 									),
									// 							'messages' => array(
									// 													$this->language->_("Error : Help Topic cannot be left blank"),
									// 													// $this->language->_("Error : Help Topic size more than 50 char"),
									// 													// $this->language->_("Error : Help Topic Format Must Alpha Numeric"),
									// 												),
															
								);

			if ($params['typesendid'] == 1) {
				if($params['sendtoid'] == '3'){
					$receiver = $params['receiver_name_phone'];
				}
				$text_content = $params['text_content'];
			}else{
				if($params['sendtoid'] == '3'){
					$receiver = $params['receiver_name_email'];
				}
				$text_content = $params['ckeditor'];
			}

			/*print_r($params);die();*/


			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $params, $this->_optionsValidator);

			if($zf_filter_input_name->isValid())
			{
				
						/*print_r($params);die;*/

						try
						{
							$this->_db->beginTransaction();
							// print_r($params);die;
							$insertArr = array(
								'B_TYPE'			=> $params['typesendid'], 
								'B_SUBJECT' 		=> $params['helpTopic'], 
								'B_EMAIL' 			=> $receiver,
								// 'B_EFDATE'			=> $params['news'],
								'B_SENDTYPE' 		=> $params['sendtoid'],
								'B_DATETYPE'		=> $params['postdatetype'],
								'B_CONTENT'			=> $text_content,
								'B_CREATEDBY'		=> $this->_userIdLogin, 
								'B_CREATED'			=> new Zend_Db_Expr('now()'), 
							);			

							if($params['postdatetype'] == '1'){
								$insertArr['B_EFDATE'] = new Zend_Db_Expr('now()');

							}else if($params['postdatetype'] == '2'){
								$insertArr['B_EFDATE'] = $params['from_date'];
								$insertArr['B_EFTIME'] = $params['from_time'];
							}

							if($params['postdatetype'] == '1'){
								$insertArr['B_STATUS'] = '1';
							}else{
								$insertArr['B_STATUS'] = '2';
							}


							$select = $this->_db->insert('M_BROADCAST', $insertArr);
							//die($select);
							$this->_db->commit();
							$id = $this->_db->lastInsertId();
							Application_Helper_General::writeLog('HLAD','Add Broadcast. ID: ['.$id.'], Subject : ['.$params['helpTopic'].']');
							$this->setbackURL('/broadcast');
							$this->_redirect('/notification/success');

						}
						catch(Exception $e)
						{
							// print_r($e);die;
							$this->_db->rollBack();
							$this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
						}
				
			}
			else
			{
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName']))? $errors['sourceFileName'] : null;
				$this->view->helpTopicErr 		= (isset($errors['helpTopic']))? $errors['helpTopic'] : null;
				$this->view->helpTopic 			= $params['helpTopic'];
				$this->view->news 			= $params['news'];
			}
		}
		
		
		//$this->view->max_file_size = $maxFileSize;
		//$this->view->file_extension = $fileExt;
		
		//$disable_note_len 	 = (isset($params['description']))  ? strlen($params['description'])  : 0;	
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
		
		$disable_note_len 	 = 100; //- $disable_note_len;		
		$note_len 	 = 100 - $note_len;	

		$this->view->disable_note_len	= $disable_note_len;
		$this->view->note_len			= $note_len;
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('HLAD','Viewing Add Help');
		}
	}
}
