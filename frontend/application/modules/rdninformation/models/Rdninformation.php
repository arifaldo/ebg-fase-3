<?php
class rdninformation_Model_Rdninformation
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}


	public function getUser($userId)
	{
		$result = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('C' => 'M_USER'))
					->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
	}

	public function getUserCIF($custId)
	{
		$result = $this->_db->fetchRow(
				$this->_db->select('CUST_CIF')
					->from(array('C' => 'M_CUSTOMER'))
					->where("CUST_ID = ".$this->_db->quote($custId))
			);
		return $result;
	}

	public function getProduct($plan, $code)
	{
		$data = $this->_db->select()
		->from(array('A' => 'M_PRODUCT_TYPE'))
		->where('PRODUCT_CODE = ?' , $code)
		->where('PRODUCT_PLAN = ?' , $plan)
		->query()
		//print_r($data);die;
		->FetchAll();

		return $data;
	}

	public function getProductAggregation($plan, $code)
	{
		$data = $this->_db->select()
		->from(array('A' => 'M_PRODUCT_AGGREGATION'),array('PRODUCT_CODE','PRODUCT_NAME'))
		->where('PRODUCT_CODE = ?' , $code)
		->where('PRODUCT_PLAN = ?' , $plan)
		->query()->FetchAll();

		return $data;
	}

	public function getCustAccount($param)
	{
		$select = $this->_db->select()
		->from(array('A' => 'M_CUSTOMER_ACCT'))
//		->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
		//->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
		->where("A.ACCT_STATUS = 1");
//		->where("C.MAXLIMIT > 0");
//		if(isset($param['userId'])) $select->where("C.USER_LOGIN = ".$this->_db->quote($param['userId']));
		if(isset($param['custId'])) $select->where("A.CUST_ID = ".$this->_db->quote($param['custId']));
		if(isset($param['acctNo'])) $select->where("C.ACCT_NO = ".$this->_db->quote($param['acctNo']));
		//if(isset($param['acctType'])) $select->where("A.ACCT_TYPE = ".$param['acctType']);
//		$select->order('C.USER_LOGIN ASC');
//		echo $select; die;
		 //		print_r($select->query());die;
		$result = $this->_db->fetchAll($select);
		return $result;
	}
	public function getUserAccount($param)
	{
		$select = $this->_db->select()
			->from(array('A' => 'M_USER_ACCT'))
			->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
		//	->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
			->where("A.ACCT_STATUS = 1")
			->where("C.MAXLIMIT > 0");
		if(isset($param['userId'])) $select->where("C.USER_LOGIN = ".$this->_db->quote($param['userId']));
		if(isset($param['acctNo'])) $select->where("C.ACCT_NO = ".$this->_db->quote($param['acctNo']));
		if(isset($param['acctType'])) $select->where("A.ACCT_TYPE = ".$param['acctType']);
		$select->group('A.ACCT_ID');
		$select->order('C.USER_LOGIN ASC');
// 		print_r($select->query());die;
//		echo $select; die;
		$result = $this->_db->fetchAll($select);
		return $result;
	}
}
