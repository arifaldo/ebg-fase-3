<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class customerbglist_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$page 			= $this->_getParam('page');
		$filter_clear 	= $this->_getParam('filter_clear');
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		//get filtering param
		$filters = array('*' => array('StringTrim', 'StripTags'));

		$validators = array('*' => array('allowEmpty' => true));

		$optionValidators = array('breakChainOnFailure' => false);

		// $zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optionValidators);

		// $filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');

		$filterlist = array("fSubject" => "Subjek Transaksi", "fBgNumber" => "BG Number", 'fStatus' => "PKS Status");
		$this->view->filterlist = $filterlist;

		$filterArr = array(
			'*'	=>  array('StringTrim', 'StripTags'),
		);

		$dataParam = array("fSubject", "fBgNumber", "fStatus");
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "Last Suggestion" || $value == "APPROVE_DATE" || $value == "Last Login") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}

			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}

		$zf_filter = new Zend_Filter_Input($filterArr, [], $dataParamValue);
		$filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');

		// if ($filter == TRUE) {
		// 	$fSubject 		= $this->_request->getParam('fSubject');
		// 	$fBgNumber 		= $this->_request->getParam('fBgNumber');
		// 	$fStartDate 	= html_entity_decode($zf_filter->getEscaped('fStartDate'));
		// 	$fEndDate		= html_entity_decode($zf_filter->getEscaped('fEndDate'));
		// 	$fStatus 		= $this->_request->getParam('fStatus');
		// }

		// if ($fStartDate) {
		// 	$FormatDate = new Zend_Date($fStartDate, $this->_dateDisplayFormat);
		// 	$fStartDate = $FormatDate->toString($this->_dateDBFormat);
		// }
		// if ($fEndDate) {
		// 	$FormatDate = new Zend_Date($fEndDate, $this->_dateDisplayFormat);
		// 	$fEndDate 	= $FormatDate->toString($this->_dateDBFormat);
		// }

		//$whereIn = [7, 20, 21, 22, 23];
		$whereIn = [15, 16];

		$select = $this->_db->select()
			->from(
				array('A' => 'T_BANK_GUARANTEE'),
				array(
					'BG_REG_NUMBER'			=> 'A.BG_REG_NUMBER',
					'BG_SUBJECT'			=> 'A.BG_SUBJECT',
					'BG_NUMBER'				=> 'A.BG_NUMBER',
					'BG_AMOUNT'				=> 'A.BG_AMOUNT',
					'BG_STATUS'                      => 'A.BG_STATUS',
					'BRANCH_NAME'			=> 'B.BRANCH_NAME',
					'COUNTER_WARRANTY_TYPE'	=> new Zend_Db_Expr("(
					CASE A.COUNTER_WARRANTY_TYPE
					WHEN '1' THEN 'Full Cover'
					WHEN '2' THEN 'Line Facility'
					WHEN '3' THEN 'Insurance'
					ELSE '-'
					END)"),
					'BG_INSURANCE_CODE'		=> 'A.BG_INSURANCE_CODE',
					'TIME_PERIOD_START'		=> 'A.TIME_PERIOD_START',
					'TIME_PERIOD_END'		=> 'A.TIME_PERIOD_END',
					'STATUS'				=> new Zend_Db_Expr("(
					CASE A.BG_STATUS
					WHEN '7' THEN 'Canceled'
					WHEN '20' THEN 'On Risk'
					WHEN '21' THEN 'Off Risk'
					WHEN '22' THEN 'Claimed On Process'
					WHEN '23' THEN 'Claimed'
					ELSE '-'
					END)"),
					"BG_STATUS"
				)
			)
			->joinLeft(
				array('B' => 'M_BRANCH'),
				'B.BRANCH_CODE = A.BG_BRANCH',
				array()
			)
			// ->where('A.BG_INSURANCE_CODE = ?', $this->_custIdLogin)
			->where('A.BG_INSURANCE_CODE = ?', "ASKRINDO01")
			->where('A.BG_STATUS IN (?)', $whereIn);

		if ($filter == TRUE) {
			if ($zf_filter->getEscaped("fSubject")) {
				$select->where('A.BG_SUBJECT LIKE ' . $this->_db->quote('%' . $zf_filter->getEscaped("fSubject") . '%'));
			}
			if ($zf_filter->getEscaped("fBgNumber")) {
				$select->where('A.BG_NUMBER LIKE ' . $this->_db->quote('%' . $zf_filter->getEscaped("fBgNumber") . '%'));
			}
			// if ($fStartDate) {
			// 	$select->where('DATE(A.TIME_PERIOD_START) >= ?', $fStartDate);
			// }
			// if ($fEndDate) {
			// 	$select->where('DATE(A.TIME_PERIOD_END) <= ?', $fEndDate);
			// }
			if ($zf_filter->getEscaped("fStatus")) {
				$select->where('A.BG_STATUS = ?', $zf_filter->getEscaped("fStatus"));
			}
		}
		$select->order('A.BG_CREATED DESC');
		$data = $this->_db->fetchAll($select);

		foreach ($data as $key => $value) {
			$getInsuranceBranch = $this->_db->select()
				->from(["TBD" => "T_BANK_GUARANTEE_DETAIL"])
				->joinLeft(["MIB" => "M_INS_BRANCH"], "MIB.INS_BRANCH_CODE = TBD.PS_FIELDVALUE")
				->where("TBD.BG_REG_NUMBER = ?", $value["BG_REG_NUMBER"])
				->query()->fetchAll();
			$data[$key]["INSURANCE_BRANCH"] = $getInsuranceBranch[0]["INS_BRANCH_NAME"];
		}
		$this->paging($data);

		$config    		= Zend_Registry::get('config');
		$BgType 		= $config["bg"]["status"]["desc"];
		$BgCode 		= $config["bg"]["status"]["code"];

		$statusArr = array_combine(array_values($BgCode), array_values($BgType));
		/*$statusArr = [
			'1' => 'Waiting for review',
			'2' => 'Waiting for approve',
			'3' => 'Waiting to release',
			'4' => 'Waiting for bank approval',
			'5' => 'Issued',
			'6' => 'Expired',
			'7' => 'Canceled',
			'8' => 'Claimed by applicant',
			'9' => 'Claimed by recipient',
			'10' => 'Request Repair',
			'11' => 'Reject'
		];*/

		$this->view->currentPage	= $page;
		$this->view->filter 		= $filter;
		$this->view->statusArr 		= $statusArr;
		$this->view->fSubject 		= $fSubject;
		$this->view->fBgNumber 		= $fBgNumber;
		$this->view->fStartDate 	= $fStartDate;
		$this->view->fEndDate 		= $fEndDate;
		$this->view->fStatus 		= $fStatus;

		if ($data) {

			$dataAll = [];

			if (!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1) {

				foreach ($data as $key => $row) {
					$subData = [];
					$subData['BG_REG_NUMBER'] = $row['BG_REG_NUMBER'] . ' / ' . $row['BG_SUBJECT'];
					$subData['BG_NUMBER'] = $row['BG_NUMBER'];
					$subData['BG_AMOUNT'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['BG_AMOUNT']);
					$subData['INSURANCE_BRANCH'] = $row["INSURANCE_BRANCH"];

					if (empty($row['BG_INSURANCE_CODE'])) {
						$subData['COUNTER_WARRANTY_TYPE'] = $row['COUNTER_WARRANTY_TYPE'];
					} else {
						$subData['COUNTER_WARRANTY_TYPE'] = $row['COUNTER_WARRANTY_TYPE'] . ' (' . $row['BG_INSURANCE_CODE'] . ')';
					}

					$subData['TIME_PERIOD_START'] = Application_Helper_General::convertDate($row['TIME_PERIOD_START'], $this->viewDateFormat, $this->defaultDateFormat);
					$subData['TIME_PERIOD_END'] = Application_Helper_General::convertDate($row['TIME_PERIOD_END'], $this->viewDateFormat, $this->defaultDateFormat);
					$subData['BG_STATUS'] = $statusArr[$row['BG_STATUS']];
					$dataAll[] = $subData;
				}
			}

			if ($this->_getParam('csv')) {

				$this->_helper->download->csv(array($this->language->_('Reg No# / Subjek Transaksi'), $this->language->_('BG Number'), $this->language->_('BG Amount'), $this->language->_('Insurance Branch'), $this->language->_('Counter Type'), $this->language->_('Start Date'), $this->language->_('End Date'), $this->language->_('Status')), $dataAll, null, 'Customer Bank Guarantee List');
			} else if ($this->_request->getParam('print') == 1) {

				$fields = array(
					'regno'     => array(
						'field'    => 'BG_REG_NUMBER',
						'label'    => $this->language->_('Reg No# /  BG No#'),
					),
					'number'     => array(
						'field'    => 'BG_NUMBER',
						'label'    => $this->language->_('BG Number'),
					),
					'bgamount'  => array(
						'field'    => 'BG_AMOUNT',
						'label'    => $this->language->_('BG Amount'),
					),
					'branch'  => array(
						'field'    => 'INSURANCE_BRANCH',
						'label'    => $this->language->_('Insurance Branch'),
					),
					'countertype'  => array(
						'field'    => 'COUNTER_WARRANTY_TYPE',
						'label'    => $this->language->_('Counter Type'),
					),
					'startdate' => array(
						'field' => 'TIME_PERIOD_START',
						'label' => $this->language->_('Date From'),
					),
					'enddate'   => array(
						'field'    => 'TIME_PERIOD_END',
						'label'    => $this->language->_('Date To'),
					),
					'format'   => array(
						'field'    => 'BG_STATUS',
						'label'    => $this->language->_('Status'),
					)
				);

				$this->_forward('print', 'index', 'widget', array('data_content' => $dataAll, 'data_caption' => 'Customer Bank Guarantee List', 'data_header' => $fields));
			}
		}
	}

	public function detailAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$AESMYSQL = new Crypt_AESMYSQL();

		$sessToken  = new Zend_Session_Namespace('Tokenenc');
		$password   = $sessToken->token;

		$decryption = urldecode($this->_request->getParam('bgnumb'));
		$decryption_bg = $AESMYSQL->decrypt($decryption, $password);


		// $Settings = new Settings();
		// $claim_period = $Settings->getSetting('max_claim_period');
		// $this->view->BG_CLAIM_PERIOD = $claim_period;


		$conf = Zend_Registry::get('config');

		$select = $this->_db->select()
			->from(
				array('TBG' => 'T_BANK_GUARANTEE'),
				array('*')
			)
			->joinLeft(
				array('MB' => 'M_BRANCH'),
				'MB.BRANCH_CODE = TBG.BG_BRANCH',
				array('BRANCH_NAME')
			)
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = TBG.CUST_ID',
				array(
					'CUST_ID',
					'CUST_NAME',
					'CUST_NPWP',
					'CUST_ADDRESS',
					'CUST_CITY',
					'CUST_FAX',
					'CUST_CONTACT',
					'CUST_PHONE',
				)
			)
			->joinLeft(
				array('MCST' => 'M_CUSTOMER'),
				'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
				array(
					"SP_OBLIGEE_NAME" => "MCST.CUST_NAME"
				)
			)
			->joinLeft(
				array('MCSTR' => 'M_CUSTOMER'),
				'MCSTR.CUST_ID = TBG.BG_INSURANCE_CODE',
				array(
					"BG_INSURANCE_NAME" => "MCSTR.CUST_NAME"
				)
			)
			->joinLeft(
				array('MCL' => 'M_CITYLIST'),
				'MCL.CITY_CODE = MC.CUST_CITY',
				array('CITY_NAME')
			)
			->joinLeft(
				array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
				'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
				array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
			)
			->joinLeft(
				array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
				'TBGS.BG_NUMBER = TBG.BG_REG_NUMBER',
				[
					"ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
				]
			)
			->where('TBG.BG_REG_NUMBER = ?', $decryption_bg)
			->where('TBG.BG_INSURANCE_CODE = ?', $this->_custIdLogin);

		$data = $this->_db->fetchRow($select);


		switch ($data["CHANGE_TYPE"]) {
			case '0':
				$this->view->suggestion_type = "New";
				break;
			case '1':
				$this->view->suggestion_type = "Amendment Changes";
				break;
			case '2':
				$this->view->suggestion_type = "Amendment Draft";
				break;
		}

		//echo '<pre>';print_r($data);
		$conf = Zend_Registry::get('config');
		// BG TYPE
		$bgType         = $conf["bg"]["type"]["desc"];
		$bgCode         = $conf["bg"]["type"]["code"];

		$arrbgType = array_combine(array_values($bgCode), array_values($bgType));

		$this->view->arrbgType = $arrbgType;

		//BG Counter Guarantee Type
		$bgcgType         = $conf["bgcg"]["type"]["desc"];
		$bgcgCode         = $conf["bgcg"]["type"]["code"];

		$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

		$this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];


		if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

			$bgdatasplit = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $decryption_bg)
				->query()->fetchAll();

			$this->view->fullmember = $bgdatasplit;
		}


		$bgdatadetail = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
			// ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('A.BG_REG_NUMBER = ?', $decryption_bg)
			->query()->fetchAll();

		if (!empty($bgdatadetail)) {
			foreach ($bgdatadetail as $key => $value) {

				if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
					if ($value['PS_FIELDNAME'] == 'Insurance Name') {
						$this->view->insuranceName =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
						$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount') {
						$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
						$this->view->paDateStart =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
						$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
					}
				} else {

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
						$this->view->owner1 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
						$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
						$this->view->owner2 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
						$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
						$this->view->owner3 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
						$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
					}
				}
			}
		}

		if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
			$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
			$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

			$insuranceBranch = $this->_db->select()
				->from("M_INS_BRANCH")
				->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
				->query()->fetchAll();

			$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
		}

		$principleData = [];
		if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
			foreach ($bgdatadetail as $key => $value) {
				$principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
			}

			$this->view->principleData = $principleData;
		}

		$get_linefacility = $this->_db->select()
			->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
			->where("CUST_ID = ?", $data["CUST_ID"])
			->query()->fetchAll();

		$this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
		$this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

		$this->view->linefacility = $get_linefacility[0];

		$this->view->acct = $data['FEE_CHARGE_TO'];

		$conf = Zend_Registry::get('config');
		$this->view->bankname = $conf['app']['bankname'];

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
		$AccArr = $CustomerUser->getAccountsBG($param);
		//var_dump($AccArr);die;

		if (!empty($AccArr)) {
			$this->view->src_name = $AccArr['0']['ACCT_NAME'];
		}

		$get_cash_collateral = $this->_db->select()
			->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
			->where("CUST_ID = ?", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$this->view->cash_collateral = $get_cash_collateral[0];

		$bgpublishType     = $conf["bgpublish"]["type"]["desc"];
		$bgpublishCode     = $conf["bgpublish"]["type"]["code"];

		$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

		$arrBankFormat = array(
			1 => 'Bank Standard',
			2 => 'Special Format (with bank approval)'
		);

		$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
		$this->view->bankFormatNumber = $data['BG_FORMAT'];

		$arrLang = array(
			1 => 'Indonesian',
			2 => 'English',
			3 => 'Billingual',
		);

		$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

		$checkOthersAttachment = $this->_db->select()
			->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
			// ->where("BG_REG_NUMBER = '268312345601F4201'")
			->where("BG_REG_NUMBER = ?", $data['BG_REG_NUMBER'])
			->order('A.INDEX ASC')
			->query()->fetchAll();

		if (count($checkOthersAttachment) > 0) {
			$this->view->othersAttachment = $checkOthersAttachment;
		}

		// Get data T_BANK_GUARANTEE_HISTORY
		$select = $this->_db->select()
			->from(
				array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
				array('*')
			)
			->where('TBGH.BG_REG_NUMBER = ?', $decryption_bg)
			->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
		$dataHistory = $this->_db->fetchAll($select);

		// Get data TEMP_BANK_GUARANTEE_SPLIT
		$select = $this->_db->select()
			->from(
				array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
				array('*')
			)
			->where('TBGS.BG_REG_NUMBER = ?', $decryption_bg);
		$dataAccSplit = $this->_db->fetchAll($select);

		$config = Zend_Registry::get('config');

		$docTypeCode = $config["bgdoc"]["type"]["code"];
		$docTypeDesc = $config["bgdoc"]["type"]["desc"];
		$docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

		$statusCode = $config["bg"]["status"]["code"];
		$statusDesc = $config["bg"]["status"]["desc"];
		$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

		$historyStatusCode = $config["history"]["status"]["code"];
		$historyStatusDesc = $config["history"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

		$counterTypeCode = $config["bgcg"]["type"]["code"];
		$counterTypeDesc = $config["bgcg"]["type"]["desc"];
		$counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

		//$config    		= Zend_Registry::get('config');
		$BgType     = $config["bg"]["status"]["desc"];
		$BgCode     = $config["bg"]["status"]["code"];

		$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

		// $arrStatus = array(
		//   '7'  => 'Canceled',
		//   '20' => 'On Risk',
		//   '21' => 'Off Risk',
		//   '22' => 'Claimed On Process',
		//   '23' => 'Claimed'
		// );

		$this->view->arrStatus = $arrStatus;

		$this->view->data               = $data;
		//Zend_Debug::dump($data);
		$this->view->dataHistory        = $dataHistory;
		$this->view->dataAccSplit       = $dataAccSplit;
		$this->view->docTypeArr         = $docTypeArr;
		$this->view->statusArr          = $statusArr;
		$this->view->historyStatusArr   = $historyStatusArr;
		$this->view->counterTypeArr     = $counterTypeArr;

		$download = $this->_getParam('download');
		if ($download == 1) {
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
		}
	}
}
