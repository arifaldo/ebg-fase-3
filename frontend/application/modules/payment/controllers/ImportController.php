<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';
require_once 'General/CustomerUser.php';

class payment_ImportController extends Application_Main
{
	protected 	$_moduleDB 	= 'RTF';
	protected	$_rowNum 	= 0;
	protected	$_errmsg 	= null;	
	protected	$params		= null;
	
	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  	
	} 
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		if($this->_request->isPost() )
		{			
			$adapter1 					= new Zend_File_Transfer_Adapter_Http();
			$params 					= $this->_request->getParams();
			$sourceFileName 			= $adapter1->getFileName();
			
			if($sourceFileName == null)
			{
				$sourceFileName = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter1->getFileName()), 0);
			}
			
			$paramsName['sourceFileName'] 	= $sourceFileName;
			
			$filtersName = array(
									'sourceFileName'		=> array('StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File size too big or left blank"
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			if($zf_filter_input_name->isValid())
			{
				$filter 	= new Application_Filtering();
				$adapter 	= new Zend_File_Transfer_Adapter_Http ();
// 				print_r($this->_custIdLogin);
// 				print_r($this->_userIdLogin);die;
				$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
				$max		= $this->getSetting('max_import_single_payment');
				
				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
													'Error: Extension file must be *.csv'
												);
				
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
											'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
										);
						
				$adapter->setValidators(array($extensionValidator,$sizeValidator));
					
				if ($adapter->isValid ()) 
				{
					$sourceFileName = $adapter->getFileName();
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
						
					$adapter->addFilter ( 'Rename',$newFileName  );
						
					if ($adapter->receive ())
					{
						$Csv = new Application_Csv ($newFileName,",");
						$csvData = $Csv->readAll ();
				
						@unlink($newFileName);
						
						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}
						
						if ($totalRecords){
							if($totalRecords <= $max)
							{
								$no =0;
								foreach ( $csvData as $columns )
								{
									if(count($columns)==10)
									{
										$params['PAYMENT SUBJECT'] 			= trim($columns[0]);
										$params['SOURCE ACCT NO'] 			= trim($columns[1]);
										$params['BENEFICIARY ACCT NO'] 		= trim($columns[2]);
//										$params['BENEFICIARY NAME'] 		= trim($columns[3]);
										$params['CCY'] 						= trim($columns[3]);
										$params['AMOUNT'] 					= trim($columns[4]);
										$params['MESSAGE'] 					= trim($columns[5]);
										$params['ADDITIONAL MESSAGE'] 		= trim($columns[6]);
//										$params['EMAIL ADDRESS'] 			= trim($columns[8]);
										$params['PAYMENT DATE'] 			= trim($columns[7]);
										$params['TRANSFER TYPE'] 			= trim($columns[8]);
										$params['BANK CODE'] 				= trim($columns[9]);
//										$params['BANK NAME'] 				= trim($columns[12]);
										// 									$params['BANK CITY'] 				= trim($columns[13]);
//										$params['CITIZENSHIP'] 				= trim($columns[13]);
											
										$PS_SUBJECT 		= $filter->filter($params['PAYMENT SUBJECT'],"PS_SUBJECT");
										$PS_EFDATE 			= $filter->filter($params['PAYMENT DATE'],"PS_DATE");
										$TRA_AMOUNT 		= $filter->filter($params['AMOUNT'],"AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($params['ADDITIONAL MESSAGE'],"TRA_REFNO");
										$ACCTSRC 			= $filter->filter($params['SOURCE ACCT NO'],"ACCOUNT_NO");
										$ACBENEF 			= $filter->filter($params['BENEFICIARY ACCT NO'],"ACCOUNT_NO");
										$ACBENEF_BANKNAME 	= $filter->filter($params['BENEFICIARY NAME'],"ACCOUNT_NAME");
//										$ACBENEF_ALIAS 		= $filter->filter($params['BENEFICIARY NAME'],"ACCOUNT_ALIAS");
//										$ACBENEF_EMAIL 		= $filter->filter($params['EMAIL ADDRESS'],"EMAIL");
										$ACBENEF_CCY 		= $filter->filter($params['CCY'],"SELECTION");
//										$ACBENEF_CITIZENSHIP= $filter->filter($params['CITIZENSHIP'], "SELECTION");
										// 									$BANK_CITY			= $filter->filter($params['BANK CITY'], "ADDRESS");
										$CLR_CODE			= $filter->filter($params['BANK CODE'], "BANK_CODE");
										$TRANSFER_TYPE 		= $filter->filter($params['TRANSFER TYPE'], "SELECTION");
//										$BANK_NAME 			= $filter->filter($params['BANK NAME'],"BANK_NAME");
											
										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
										
										if($TRANSFER_TYPE == 'RTGS'){
											$chargeType = '1';
											$select = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType);
											$resultSelecet = $this->_db->FetchAll($select);
											$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
											
											//$param['TRANSFER_FEE'] = $chargeAmt;
										}
										else if($TRANSFER_TYPE == 'SKN'){
											$chargeType1 = '2';
											$select1 = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType1);
											$resultSelecet1 = $this->_db->FetchAll($select1);
											$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
											
											//$param['TRANSFER_FEE'] = $chargeAmt1;
										}
										else{
											$chargeAmt = '0';
											//$param['TRANSFER_FEE'] = $chargeAmt2;
										}

		
										$paramPayment = array(
												"CATEGORY" 					=> "SINGLE PAYMENT",
												"FROM" 						=> "I",				// F: Form, I: Import
												"PS_NUMBER"					=> "",
												"PS_SUBJECT"				=> $PS_SUBJECT,
												"PS_EFDATE"					=> $PS_EFDATE,
												"_dateFormat"				=> $this->_dateUploadFormat,
												"_dateDBFormat"				=> $this->_dateDBFormat,
												"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
												"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
												"_createPB"					=> $this->view->hasPrivilege('CRIP'),								// cannot create PB trx
												"_createDOM"				=> $this->view->hasPrivilege('CRDI'),	// privi CDFT (Create Domestic Fund Transfer)
												"_createREM"				=> $this->view->hasPrivilege('CRIR'),								// cannot create REM trx
										);
							
										$paramTrxArr[0] = array(
												"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
												"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
												"TRANSFER_FEE" 				=> $chargeAmt,
												"TRA_MESSAGE" 				=> $TRA_MESSAGE,
												"TRA_REFNO" 				=> $TRA_REFNO,
												"ACCTSRC" 					=> $ACCTSRC,
												"ACBENEF" 					=> $ACBENEF,
												"ACBENEF_CCY" 				=> $ACBENEF_CCY,
												"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
												"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
//												"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
												"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
												// 																"ACBENEF_ADDRESS1" 			=> $BANK_CITY,
//												"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,
												"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
												"BANK_CODE" 				=> $CLR_CODE,
//												"BENEFICIARY_BANK_NAME"		=> $BANK_NAME,
//												"LLD_IDENTICAL" 			=> "",
//												"LLD_CATEGORY" 				=> "",
//												"LLD_RELATIONSHIP" 			=> "",
//												"LLD_PURPOSE" 				=> "",
//												"LLD_DESCRIPTION" 			=> "",
												"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
												"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
												"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
												"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
												"BANK_NAME" 	=> $BANK_NAME,
										
										);
											
										$arr[$no]['paramPayment'] = $paramPayment;
										$arr[$no]['paramTrxArr'] = $paramTrxArr;
									}
									else
									{
										$this->view->error 		= true;
										break;
									}
									$no++;
								}
									
								if(!$this->view->error)
								{
									$resWs = array();
									$err 	= array();
									
									$resultVal	= $validate->checkCreate($arr, $resWs);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();
									
									//Zend_Debug::dump($resWs);die;
									$i = 0;
									foreach($resWs as $key=>$dataAcctType){
										//Zend_Debug::dump($dataAcctType);	
										$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
									}
									
										
									//die;
									
									$sourceAccountType 	= $resWs['accountType'];
									
									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
									$content['sourceAccountType'] 	= $sourceAccountType;
															
									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;
										
									$this->_redirect('/singlepayment/import/confirm');
								}
									
							}
							else
							{
								$this->view->error2 = true;
								$this->view->max 	= $max;
							}
						}else{
							$this->view->error = true;
						}
					}
				}
				else
				{
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}
			}
			else
			{
				$this->view->error3		= true;
			}
		}
		Application_Helper_General::writeLog('CRDI','Viewing Create Single Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CRIP','Viewing Create Single Payment In House by Import File (CSV)');
	}
	
	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
		$data = $sessionNamespace->content;
		if(!$data["payment"]["countTrxCCY"])
		{
			$this->_redirect("/authorizationacl/index/nodata");
		}
		
		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];
		
		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}
		
		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}
		
		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}
		
		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}
		
		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;
		
		$fields = array(
						'PaymentType'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Type'),
														'sortable' => false
													),
						'CCY'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('CCY'),
														'sortable' => false
													),
						'Payment Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Success'),
														'sortable' => false
													),
						'Total Amount Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Success'),
														'sortable' => false
													),
						'Payment Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Failed'),
														'sortable' => false
													),
						'Total Amount Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Failed'),
														'sortable' => false
													),
                        );
		$this->view->fields = $fields;
		
		if($this->_request->isPost() )
		{			
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmImportCredit']); 
				$this->_redirect('/singlepayment/import');
			}
			
			foreach($data["arr"] as $row)
			{
				$param['PS_SUBJECT'] 				= $row['paramPayment']['PS_SUBJECT'];
				$param['PS_EFDATE']  				= Application_Helper_General::convertDate($row['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateUploadFormat);
				$param['PS_TYPE'] 					= $this->_paymenttype['code']['bulkcredit'];
				$param['PS_CCY']  					= $row['paramTrxArr'][0]['ACBENEF_CCY'];
				$param['SOURCE_ACCOUNT']			= $row['paramTrxArr'][0]['ACCTSRC'];
				$param['BENEFICIARY_ACCOUNT'] 		= $row['paramTrxArr'][0]['ACBENEF'];
				$param['BENEFICIARY_ACCOUNT_CCY'] 	= $row['paramTrxArr'][0]['ACBENEF_CCY'];
				$param['BENEFICIARY_ACCOUNT_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_BANKNAME'];
//				$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_ALIAS'];
				$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_BANKNAME'];
				$param['BENEFICIARY_EMAIL'] 		= $row['paramTrxArr'][0]['ACBENEF_EMAIL'];
				$param['BENEFICIARY_ADDRESS'] 		= $row['paramTrxArr'][0]['ACBENEF_ADDRESS1'];
				$param['BENEFICIARY_CITIZENSHIP'] 	= $row['paramTrxArr'][0]['ACBENEF_CITIZENSHIP'];
				$param['CLR_CODE'] 					= $row['paramTrxArr'][0]['BANK_CODE'];
//				$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BENEFICIARY_BANK_NAME'];
				$param['TRANSFER_TYPE'] 			= $row['paramTrxArr'][0]['TRANSFER_TYPE'];
				$param['TRA_AMOUNT'] 				= $row['paramTrxArr'][0]['TRA_AMOUNT'];
				$param['TRANSFER_FEE'] 				= $row['paramTrxArr'][0]['TRANSFER_FEE'];
				$param['TRA_MESSAGE'] 				= $row['paramTrxArr'][0]['TRA_MESSAGE'];
				$param['TRA_REFNO'] 				= $row['paramTrxArr'][0]['TRA_REFNO'];
				$param['_addBeneficiary'] 			= $row['paramPayment']['_addBeneficiary'];
				$param['_beneLinkage'] 				= $row['paramPayment']['_beneLinkage'];
				$param["_dateFormat"]				= $row['paramPayment']['_dateFormat'];
				$param["_dateDBFormat"]				= $row['paramPayment']['_dateDBFormat'];
				$param["_createPB"]					= $row['paramPayment']['_createPB'];
				$param["_createDOM"]				= $row['paramPayment']['_createDOM'];
				$param["_createREM"]				= $row['paramPayment']['_createREM'];
				$param["sourceAccountType"]			= $row['paramTrxArr'][0]['ACCOUNT_TYPE'];
				
				$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BANK_NAME'];
				$param['LLD_CATEGORY'] 				= $row['paramTrxArr'][0]['BENEFICIARY_CATEGORY'];
				$param['CITY_CODE'] 				= $row['paramTrxArr'][0]['BENEFICIARY_CITY_CODE'];
				$param['LLD_BENEIDENTIF'] 			= $row['paramTrxArr'][0]['BENEFICIARY_ID_TYPE'];
				$param['LLD_BENENUMBER'] 			= $row['paramTrxArr'][0]['BENEFICIARY_ID_NUMBER'];
				
				try 
				{
					$SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);					
					$result = $SinglePayment->createPayment($param);
					
				}
				catch(Exception $e)
				{
					Application_Helper_General::exceptionLog($e);
					$errr = 1;
				}
			}
			unset($sessionNamespace->content);
			
			$this->_helper->getHelper('FlashMessenger')->addMessage('/'.$this->view->modulename.'/'.$this->view->controllername.'/index');
			
			if ($errr != 1)
			{	$this->_redirect('/notification/success/index');	}
			else
			{	$this->_redirect('/notification/success/index');	}	// TODO: what to do, if failed create payment
			
		}
	}
}
