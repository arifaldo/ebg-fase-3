<?php
class language_ImportController extends Application_Main {
	public function indexAction(){
		$model = new language_Model_Index();
		//$filename = LIBRARY_PATH.'data/languages/backend/import/language.tmx';
		
		$filename = LIBRARY_PATH.'data/languages/frontend/import/language.tmx';
		if (file_exists($filename)){
			$file = simplexml_load_file($filename);
			
			foreach ($file->body->tu as $data => $arr){
				foreach ($arr[0]->attributes() as $key => $val){
					$this->_db->beginTransaction();
					
					$i = 0;
					$bindMaster = array(
						'M_TRANSLATE_UI_ID' 	=> 1,
						'TEXT' 					=> $val[0],
						'CREATED' 				=> date('Y-m-d H:i:s'),
						'CREATEDBY' 			=> $this->_userIdLogin,
						'UPDATEDBY' 			=> $this->_userIdLogin,
					);
					$this->_db->insert('M_TRANSLATE', $bindMaster);
					$lastId = $this->_db->lastInsertId();
					
					foreach ($arr as $key2 => $val2){
						$bindDetail = array(
							'M_TRANSLATE_ID' 		=> $lastId,
							'M_TRANSLATE_LANG_ID' 	=> ($i+1),
							'TEXT' 					=> $val2->seg,
							'CREATED' 				=> date('Y-m-d H:i:s'),
							'CREATEDBY' 			=> $this->_userIdLogin,
							'UPDATEDBY' 			=> $this->_userIdLogin,
							'WEB' 					=> 'Frontend',
						);
						$this->_db->insert('M_TRANSLATE_DETAIL', $bindDetail);
						
						$i ++;
					}
					
					$this->_db->commit();
				}
			}
			
			unlink($filename);
			
			
							//$where = array('USER_ID = ?' => $this->view->userId	= $data2['USER_ID']);
							//$this->_db->delete('M_TRANSLATE_DETAIL');
		}else{
			echo 'DATA NOT FOUND';
		}
		
		echo '<br />';
		die('End');
	}	
}
