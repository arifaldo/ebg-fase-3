<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class predefinedbeneficiary_ImportbeneficiaryapprovedomController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout'); 
		$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
		//$data = $_SESSION['import_predefbenef'];	
		$data = $sessionNamespace->content;
		
		if(is_array($data) && count($data) > 0){
			foreach($data as $content)
			{
				$beneflist[] = $content['BENEFICIARY_ACCOUNT'];
			}
			
			$row=0;
			foreach($beneflist as $bene)
			{
				$temp=0;
				foreach($beneflist as $cek)
				{
					if($bene == $cek)
					{
						$temp++;
					}
				}
				if($temp>1 && !(isset($data[$row]['ERROR_MESSAGE'])))
				{
					$data[$row] += array( 'ERROR_MESSAGE' => 'Duplicate Beneficiary Account' );
				}
				$row++;
			}
			
			//$data = $_SESSION['import_predefbenef'];
		}			 
		
		$this->view->data = $data;

		if($this->_request->isPost() )
		{
			if($this->_getParam('back') == $this->language->_('Back'))
			{
				unset($_SESSION['import_predefbenef']); 
				$this->_redirect('/predefinedbeneficiary/domesticcct');
			}
			$Beneficiary = new Beneficiary();
			$this->_db->beginTransaction();

			try 
			{
				foreach($data as $content)
				{				
					$add = $Beneficiary->add($content);
				}
				//-----insert benef--------------
				$this->_db->commit();
				unset($_SESSION['import_predefbenef']); 
				$this->setbackURL('/'.$this->_request->getModuleName().'/importbeneficiarydom/index/');
				$this->_redirect('/notification/submited');
				Application_Helper_General::writeLog('BIMA','Submit Import Beneficiary');
			}
			catch(Exception $e) 
			{
				//rollback changes
				$this->_db->rollBack();
				
				$errorMsg = $this->getErrorRemark('82');
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
				//Application_Log_GeneralLog::technicalLog($e);
			}
			Application_Helper_General::writeLog('BIMA','Approve Import Beneficiary');
		}		
	}
}
