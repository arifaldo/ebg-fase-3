<?php
require_once 'Zend/Controller/Action.php';

class popup_PopulateremittanceController extends Application_Main
{
	public function initController()
	{
		$this->_helper->layout()->setLayout('newpopup');
	}

	public function indexAction()
	{
		Zend_Session::namespaceUnset('populateremit');

// 		$model = new customer_Model_Customer();
		$arrContry = $this->_db->select()
						->from(array('B' => 'M_COUNTRY'),array())

		->joinLeft(array('O'=>'M_OFAC'),'B.COUNTRY_CODE = O.country_code',array('B.*'))
		->where('O.country_code IS NULL')->query();

	    $this->view->countryArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($arrContry,'COUNTRY_CODE','COUNTRY_NAME'));

		$filterArr = array(	'next' => array('StringTrim','StripTags'),
							'bank_code' => array('StringTrim','StripTags','StringToUpper'),
							'bank_name' => array('StringTrim','StripTags','StringToUpper'),
							'country' => array('StringTrim','StripTags','StringToUpper')
		);

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$next = $zf_filter->getEscaped('next');
		$bank_code = $zf_filter->getEscaped('bank_code');
		$bank_name = $zf_filter->getEscaped('bank_name');
		$country = $zf_filter->getEscaped('country');
		$ccy = $zf_filter->getEscaped('ccy');
		$code = $zf_filter->getEscaped('code');
		$this->view->bank_code = $bank_code;
		$this->view->bank_name = $bank_name;
		$this->view->country = $country;

		$this->view->code = $code;
		$this->view->ccy = $ccy;

		if($next == TRUE)
		{
			if(empty($bank_code) && empty($bank_name) && empty($country)){
				$this->view->message = 'Error : All filters cannot be left blank. Please fill in one filter.';
			}
			else{
				$sessionNamespace 		= new Zend_Session_Namespace('populateremit');
				$sessionNamespace->bankCode = (!empty($bank_code))? $bank_code : null;
				$sessionNamespace->bankName = (!empty($bank_name))? $bank_name : null;
				$sessionNamespace->country = (!empty($country))? $country : null;
				$sessionNamespace->code = (!empty($code))? $code : null;
				$sessionNamespace->ccy = (!empty($ccy))? $ccy : null;

// 				$this->_redirect('/popup/populateremittance/confirm/');
			}
		}

		$sessionNamespace = new Zend_Session_Namespace('populateremit');
		$fbankname = $sessionNamespace->bankName;
		$fbankcode = $sessionNamespace->bankCode;
		$fcountry = $sessionNamespace->country;
		$code = $sessionNamespace->code;
		$ccy = $sessionNamespace->ccy;
		$country = $this->language->_('Country');
		$benefname = $this->language->_('Beneficiary Bank Name');
		$benefcode = $this->language->_('Beneficiary Bank Code');

		$fields = array(
				'bank_code'  => array('field' =>'bank_code',
						'label' => $benefcode,
						'sortable' => true),
				'bank_name'     => array('field' => 'bank_name',
						'label' => $country,
						'sortable' => true),
				'bank_name'      => array('field' => 'bank_name',
						'label' => $benefname,
						'sortable' => true),
		);

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$scode = $this->_getParam('swiftcode');
		if(!empty($scode)){
			$this->view->code = $this->_getParam('swiftcode');
			$this->view->ccy = $this->_getParam('currency');

		}
		$sortBy1  = $this->_getParam('sortby','bank_name');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
				array(array_keys($fields))
		))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
				array('haystack'=>array('asc','desc'))
		))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$select = $this->_db->select()
		->distinct()
		->from(array('A' => 'M_BANK_REMITTANCE'),array('bank_code'=>'SUBSTR(bank_code,1,8)','bank_name','country_name'=>'B.COUNTRY_NAME','B.COUNTRY_CODE'))
		->join(array('B' => 'M_COUNTRY'),'A.country_code = B.COUNTRY_CODE',array())
		->joinLeft(array('C' => 'M_OFAC'),'C.country_code = B.COUNTRY_CODE',array());
		$select->where('C.country_code IS NULL');

		if($fbankcode) $select->where('UPPER(bank_code) LIKE '.$this->_db->quote('%'.strtoupper($fbankcode).'%'));
		if($fbankname) $select->where('UPPER(bank_name) LIKE '.$this->_db->quote('%'.strtoupper($fbankname).'%'));
		if($fcountry)  $select->where('UPPER(B.country_code) LIKE '.$this->_db->quote('%'.strtoupper($fcountry).'%'));

		if(!empty($ccy) && !empty($code)){
			$select->where('UPPER(SUBSTR(BANK_CODE,1,8)) NOT IN (SELECT BANK_CODE FROM `M_MEMBER_BANK` WHERE NOSTRO_SWIFTCODE = '.$this->_db->quote(strtoupper($code)).' AND CCY = '.$this->_db->quote(strtoupper($ccy)).' )');
			// 			WHERE SUBSTR(BANK_CODE,1,8) NOT IN(SELECT BANK_CODE FROM `M_MEMBER_BANK` WHERE NOSTRO_SWIFTCODE = 'MANDIRI001' AND CCY = 'USD')
		}

		$select->order($sortBy.' '.$sortDir);
// 		echo $select;
		$select = $this->_db->fetchall($select);
// 		print_r($select);
		$this->paging($select);
		$this->view->fields = $fields;

		Application_Helper_General::writeLog('Populate Remittance','Populate Remittance');

	}


	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('populateremit');
		$fbankname = $sessionNamespace->bankName;
		$fbankcode = $sessionNamespace->bankCode;
		$fcountry = $sessionNamespace->country;

		$country = $this->language->_('Country');
		$benefname = $this->language->_('Beneficiary Bank Name');
		$benefcode = $this->language->_('Beneficiary Bank Code');

		$fields = array(
				'bank_code'  => array('field' =>'bank_code',
									      'label' => $benefcode,
									      'sortable' => true),
				'bank_name'     => array('field' => 'bank_name',
									      'label' => $country,
									      'sortable' => true),
				'bank_name'      => array('field' => 'bank_name',
									      'label' => $benef_name,
									      'sortable' => true),
		      );

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy1  = $this->_getParam('sortby','bank_name');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$select = $this->_db->select()
					->distinct()
			        ->from(array('A' => 'M_BANK_REMITTANCE'),array('bank_code'=>'SUBSTR(bank_code,1,8)','bank_name','country_name'=>'B.COUNTRY_NAME','B.COUNTRY_CODE'))
			        ->join(array('B' => 'M_COUNTRY'),'A.country_code = B.COUNTRY_CODE',array());

		if($fbankcode) $select->where('UPPER(bank_code) LIKE '.$this->_db->quote('%'.strtoupper($fbankcode).'%'));
        if($fbankname) $select->where('UPPER(bank_name) LIKE '.$this->_db->quote('%'.strtoupper($fbankname).'%'));
        if($fcountry)  $select->where('UPPER(A.country_code) LIKE '.$this->_db->quote('%'.strtoupper($fcountry).'%'));

        $select->order($sortBy.' '.$sortDir);

		$select = $this->_db->fetchall($select);
		$this->paging($select);
		$this->view->fields = $fields;

		Application_Helper_General::writeLog('Populate Remittance','Populate Remittance Confirm');


	}
}
