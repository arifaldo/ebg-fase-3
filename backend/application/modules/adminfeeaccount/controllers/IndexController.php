<?php


require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class adminfeeaccount_IndexController extends Application_Main
{
	public function indexAction() 
	{	
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $custid = $AESMYSQL->decrypt($this->_getParam('custid'), $password);
	    $custid = (Zend_Validate::is($custid,'Alnum') && Zend_Validate::is($custid,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $custid : null;
		
		$selectcust = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$selectcust -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $selectcust->query()->FetchAll();
		$this->view->result = $result;
		
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		/*$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));*/
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		//$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;

		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;
		
		foreach($resultlist as $defaultacct)
		{
			$acctno = $defaultacct['ACCT_NO'];
			$cekacct = 'cek'.$acctno;
			$this->view->$cekacct = $defaultacct['ACCT_NO'];
		}
		
		$monthlyfeestatus = $result[0]["CUST_MONTHLYFEE_STATUS"];
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = $this->language->_('Disabled');
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = $this->language->_('Enabled');
		}
			
		$custname = $result[0]['CUST_NAME'];
		
		foreach($resultunion as $row)
		{	
			$acctno = $row['ACCT_NO'];
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_ADMFEE_MONTHLY'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
			$select4 -> where("MONTHLYFEE_TYPE = '1'");
			$result4 = $select4->query()->FetchAll();

			$i = 0;
			if($select4)
			{
				foreach($result4 as $row2)
				{	
   					$idamt = $row2['ACCT_NO'].'amount';
					$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
					$cekacct = 'cek'.$acctno; 
					$this->view->$idamt = $amt;
					$this->view->$cekacct = $row2['CHARGES_ACCT_NO'];
					$i++;
				}
			}
		}	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
		if(!$submit)
		{
			Application_Helper_General::writeLog('CHUD','View Update Administration fee account charges page ('.$custid.')');
		}
		
		$cek = $this->_db->select()
							->from('TEMP_ADMFEE_MONTHLY');
		$cek -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek = $cek->query()->FetchAll();
		
		$cek2 = $this->_db->select()
							->from('TEMP_CHARGES_MONTHLY');
		$cek2 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek2 = $cek2->query()->FetchAll();
		//Zend_Debug::dump($submit); die;
		if(!$cek && !$cek2)
		{
			if($submit)
			{
				Application_Helper_General::writeLog('CHUD','Updating Administration fee account charges ('.$custid.')');
				$error = 0;
				foreach($resultunion as $row)
				{
					$acctno = $row['ACCT_NO'];
					$cekacct = 'cek'.$acctno;
					$idamt = $row['ACCT_NO'].'amount';
					
   					//Zend_Debug::dump($idamt); die;
					$cekamt = $this->_getParam($idamt);
					$amt = Application_Helper_General::convertDisplayMoney($cekamt);
					$temp_amt = str_replace('.','',$amt);
					$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
					$this->view->$idamt = $cekamt;
					
					$cekcekacct = $this->_getParam($acctno);
   					$cek_angka2 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   					$this->view->$cekacct = $cekcekacct;
   					
   					
   					$selectcekcur = $this->_db->select()
				        			->from (($selectnoprk2),array('*'));
					$selectcekcur -> where("ACCT_NO LIKE ".$this->_db->quote($cekcekacct));
					$cekcur = $this->_db->fetchRow($selectcekcur);			    
					
					if($cekcekacct)
					{
							if($cekcur['CCY_ID'] != $row['CCY_ID'])
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account currency must be same');
								$this->view->$errid = $err;
								$error++;
							}
					}
					
					if($cek_angka == false && $cek_angka != null)
					{
							$errid = 'err'.$idamt;
							$err = $this->language->_('Amount value has to be numeric');
							$this->view->$errid = $err;
							$error++;
							//die;
					}
					
					if($cekamt != '0.00')
					{
						if(!$cekcekacct)
						{
							$errid = 'err'.$cekacct;
							$err = $this->language->_('Account should be choosen while the amount is filled');
							$this->view->$errid = $err;
							$error++;
							//die;
						}
						if($cekcekacct)
						{
							if($cek_angka2 == false)
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account value has to be numeric');
								$this->view->$errid = $err;
								$error++;
							}
						}
					}	
				}
				//Zend_Debug::dump($error); die;
					
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
					$info = $this->language->_('Charges');
					$info2 = $this->language->_('Set Charges Administration fee per Account');
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_ADMFEE_MONTHLY','TEMP_ADMFEE_MONTHLY',$custid,$custname,$custid);
					
					foreach($resultunion as $row)
					{
						$idamt = $row['ACCT_NO'].'amount';
						
						$cekamt = $this->_getParam($idamt);
						if($cekamt == null)
						{
							$cekamt = '0.00';
						}
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
						$cekcekacct = $this->_getParam($row['ACCT_NO']);
						
						$data= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'AMOUNT' 			=> $amt,
										'LAST_EXECUTED' 	=> new Zend_Db_Expr('GETDATE()'),
										'MONTHLYFEE_TYPE' 	=> '1',
										'CHARGES_ACCT_NO'	=> $cekcekacct
									);
						$this->_db->insert('TEMP_ADMFEE_MONTHLY',$data);										
						//echo 'aaa';die;
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/setcompanycharges/detail/index/custid/'.$this->_getParam('custid'));
						$this->_redirect('/notification/submited/index');
					}
				}
				
				catch(Exception $e)
				{
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}		
		}
		}
		if($cek || $cek2)
		{			
			$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";	
			$this->view->error = $docErr;
			$this->view->changestatus = "disabled";
		}
	}
}