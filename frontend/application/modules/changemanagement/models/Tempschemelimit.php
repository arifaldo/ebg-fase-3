<?php
/**
 * Tempanchorlimit model
 * 
 * @author 
 * @version
 */

require_once 'Zend/Db/Table/Abstract.php';

class Changemanagement_Model_Tempanchorlimit extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'TEMP_ANCHORLIMIT';
	protected $_primary = 'TEMP_ID';
		
	protected $_referenceMap = array('GLOBALCHANGES' => array(
											            'columns'           => 'CHANGES_ID',
											            'refTableClass'     => 'Changemanagement_Model_Globalchanges',
											            'refColumns'        => 'CHANGES_ID'
        												)
        							 );
    protected $_masterTable = 'M_ANCHORLIMIT';
    protected $_masterId = 'ANCHOR_ID';
        							 
    protected $dbObj;

    //fields to exclude when executing insert/update query in master table
	protected $_excludeFromMaster = array('TEMP_ID','CHANGES_ID'); 
    protected $_id;
	protected $_rows;
	
    
	/**
     * Constructor
     * @param  String $id Optional Change Id
     */
    public function __construct($id) {
    	parent::__construct();
    	$this->dbObj = $this->getDefaultAdapter();
    	
    	if(isset($id)) 
			$this->setId($id);
	}    							 
	
	/**
     * set Id for this object
     * @param  String $id Change Id
     * @return boolean 
     */
	public function setId($id) {
		
		if(!isset($id))
			throw new Exception('Id has to be set');

		$rows = $this->dbObj
    				->select()
					->from($this->_name)
					->where('CHANGES_ID = ?', $id)
    				->query()
					->fetch(Zend_Db::FETCH_ASSOC);
		if($rows) {
    		$this->_id = $id;
			$this->_rows = $rows;
			return true;
    	}
    	else 
    		throw new Exception('Records with given id not found');	
	}
    
    /**
     * update changes to master table (in the case of an approved edit change)
     *
     * @param  String $masterId Primary Key value of master table to update
     * @return boolean indicating operation success/failure
     */
    public function updateEdit($masterId) {
    	
    	if(!isset($this->_rows))
    		throw new Exception('Cannot update. rows are not set');
    	
    	//NOTE : following series of queries does not use transaction, function caller should take care of that
    	$this->dbObj->delete($this->_masterTable, $this->_masterId.' = ?', $masterId);

    	$allInsertOk = true;
	    $insertArr = array();	
	    foreach($this->_rows as $record) {
	    	
	    	foreach($record as $key=>$val)
	    		if(!in_array($key,$this->_excludeFromMaster))
	    			$insertArr[$key] = $val;
	    			
	    	$row = $this->dbObj->insert($this->_masterTable,$insertArr);
	    	if(!$row)
	    		$allInsertOk = false;		
	    }
	    	    
    	return $allInsertOk;
    }
    
    /**
     * update changes to master table (in the case of an approved add change)
     *
     * @return boolean indicating operation success/failure
     */
    public function updateInsert() {
    	
    	if(!isset($this->_rows))
    		throw new Exception('Cannot update. rows are not set');
    	
    	//NOTE : following series of queries does not use transaction, function caller should take care of that
    	//$this->dbObj->delete($this->_masterTable, $this->_masterId.' = '.$masterId);
    	$allInsertOk = true;
	    $insertArr = array();	
	    foreach($this->_rows as $record) {
	    	
	    	foreach($record as $key=>$val)
	    		if(!in_array($key,$this->_excludeFromMaster))
	    			$insertArr[$key] = $val;
	    			
	    	$row = $this->dbObj->insert($this->_masterTable,$insertArr);
	    	if(!$row)
	    		$allInsertOk = false;		
	    }
	    	    
    	return $allInsertOk;
    }
    
	/**
     * update changes to master table (in the case of an approved activate change)
     *
     * @param  String $masterId Primary Key value of master table to update
     * @return boolean indicating operation success/failure
     */
    public function updateActivate($masterId) {
    	if(!isset($this->_row))
    		throw new Exception('Cannot update. row is not set');
    	//do nothing, anchor limit has no status to update in case of activate suggestion approval
    	return true;    							 
	}
    
    /**
     * update changes to master table (in the case of an approved deactivate change)
     *
     * @param  String $masterId Primary Key value of master table to update
     * @return boolean indicating operation success/failure
     */
    public function updateDeactivate($masterId) {
    	if(!isset($this->_row))
    		throw new Exception('Cannot update. row is not set');
    	//do nothing, anchor limit has no status to update in case of deactivate suggestion approval
    	return true;    							 
	}
	
	/**
     * delete change data from temp table (for use in the case of rejected/approved change)
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteChanges() {
		if(!isset($this->_id))
    		throw new Exception('Cannot update. id is not set');

    	$row = $this->dbObj->delete($this->_name,$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_id));
    	return (boolean)$row;		
	}
	
	/**
     * get change details (as Add changes)
     *
     * @return array details array
     */
	public function getAddDetail() {
		if(!isset($this->_rows))
    		throw new Exception('Cannot get detail. rows are not set');
		
    	//setup suggested values
		$suggestedArr =array();
		foreach($this->_rows as $key=>$value) {
			$suggestedArr[] = array($value['CCY_ID'],
									Zend_Locale_Format::toNumber($value['LIMIT'],
																 key(Zend_Locale::getDefault()) 
																)
									);							
		}
    	//create mapping of fields for display and its label
    	$detail = array('display_type' => 'multiple', //value to determine how to display fields
    					'label' => array('Currency', 'Limit'),
    					'data' => array('suggested' => $suggestedArr)
					   );    									
    		
    	return $detail;
	}
	
	/**
     * get change details (as Edit changes)
     * @param  String $masterId Primary Key value of master table
     * @return array details array
     */
	public function getEditDetail($masterId) {
		if(!isset($this->_rows))
    		throw new Exception('Cannot get detail. rows are not set');
		
    	//get values from Master table
    	$masterRows = $this->dbObj->select()
    							 ->from($this->_masterTable)
    							 ->where($this->dbObj->quoteInto($this->_masterId.' = ?',$masterId))
    							 ->query()
    							 ->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!$masterRows)
			throw new Exception('Cannot get detail. Master records not found');	
    		
    		
    	//setup suggested values
		$suggestedArr =array();
		foreach($this->_rows as $key=>$value) {
			$suggestedArr[] = array($value['CCY_ID'],
									Zend_Locale_Format::toNumber($value['LIMIT'],
																 key(Zend_Locale::getDefault()) 
																)
									);							
		}
		
		//setup current values
		$currentArr =array();
		foreach($masterRows as $key=>$value) {
			$currentArr[] = array($value['CCY_ID'],
									Zend_Locale_Format::toNumber($value['LIMIT'],
																 key(Zend_Locale::getDefault()) 
																)
									);							
		}

		//create mapping of fields for display and its label
    	$detail = array('display_type' => 'multiple', //value to determine how to display fields
    					'label' => array('Currency', 'Limit'),
    					'data' => array('suggested' => $suggestedArr,
    									'current' => $currentArr,
    								    )
					   );    									
    		
    	return $detail;
	}
	
	/**
     * get change details (as Activate changes)
     * @param  String $masterId Primary Key value of master table
     * @return array details array
     */
	public function getActivateDetail($masterId) {
		//do nothing
		return true;
	}
	
	/**
     * get change details (as Deactivate changes)
     * @param  String $masterId Primary Key value of master table
     * @return array details array
     */
	public function getDeactivateDetail($masterId) {
		//do nothing
		return true;					
	}
}