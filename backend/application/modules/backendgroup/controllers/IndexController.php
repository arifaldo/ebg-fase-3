<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class Backendgroup_IndexController extends Application_Main
{
  public function indexAction() 
  { 

  	$setting = new Settings();			  	
	$enc_pass = $setting->getSetting('enc_pass');
	$enc_salt = $setting->getSetting('enc_salt');
	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
	$pw_hash = md5($enc_salt.$enc_pass);
	$rand = $this->_userIdLogin.date('dHis').$pw_hash;
	$sessionNamespace->token 	= $rand;
	$this->view->token = $sessionNamespace->token;
	
	$this->_helper->layout()->setLayout('newlayout');
		$this->view->statusCode = $this->_masterglobalstatus['code'];
    	$this->view->statusDesc = $this->_masterglobalstatus['desc'];
    	$this->view->modulename = $this->_request->getModuleName();
    	$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));
  		//Zend_Debug::dump($this->_masterglobalstatus); die;
		
    	
    $fields = array('BGROUP_ID'   	=> array('field'    => 'BGROUP_ID',
                                        'label'    => $this->language->_('Group ID'),
                                        'sortable' => true),
    
                    'BGROUP_DESC' 	=> array('field'    => 'BGROUP_DESC',
                                        'label'    => $this->language->_('Group Name'),
                                        'sortable' => true),
    
    				'BGROUP_STATUS'	=> array('field'  => 'BGROUP_STATUS',
                                        'label'    => $this->language->_('Status'),
                                        'sortable' => true),
    
                   	'UPDATED'		=> array('field'    => 'UPDATED',
                                        'label'    => $this->language->_('Last Updated'),
                                        'sortable' => true),
                   	'CREATED'		=> array('field'    => 'CREATED',
                                        'label'    => $this->language->_('Last Approved'),
                                        'sortable' => true),
                   );
	
    $filterlist = array("GROUP ID"=>"GROUP_ID","GROUP NAME"=>"GROUP_NAME","STATUS"=>"Status","APPROVED DATE"=>"Approved Date","LAST APPROVED BY"=>"Last Approved By","UPDATED DATE"=>"Updated Date","LAST UPDATED BY"=>"Last Updated By");
		
	$this->view->filterlist = $filterlist;
		
    
    //validasi page, jika input page bukan angka               
    $page = $this->_getParam('page');
    $csv = $this->_getParam('csv');
	$pdf = $this->_getParam('pdf');

    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
	$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

    $filterArr = array(	'filter' => array('StripTags','StringTrim'),
                       	'GROUP_ID'    => array('StripTags','StringTrim','StringToUpper'),
                       	'GROUP_NAME'  => array('StripTags','StringToUpper'),
                       	'Status' => array('StripTags','StringTrim'),
                       	'Last Updated By' => array('StripTags','StringTrim'),
                       	'Last Approved By' => array('StripTags','StringTrim'),
    					'Approved Date' => array('StripTags','StringTrim'),
    					'Approved Date_END' => array('StripTags','StringTrim'),
    					'Updated Date' => array('StripTags','StringTrim'),
    					'Updated Date_END' => array('StripTags','StringTrim'),
                      );
                      
    $validator = array('filter'			 	=> array(),
        			   'GROUP_ID'			=> array(),
        			   'GROUP_NAME'			=> array(),
        			   'Status'			 	=> array(),
        			   'Last Updated By'			 	=> array(),
        			   'Last Approved By'			 	=> array(),
        			   'Approved Date'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
        			   'Approved Date_END'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
        			   'Updated Date'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
        			   'Updated Date_END'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                      );

      $dataParam = array("GROUP_ID","GROUP_NAME","Status","Approved Date","Last Approved By","Updated Date","Last Updated By");
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "Approved Date" || $value == "Updated Date"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		// print_r($dataParamValue);die;
			if(!empty($this->_request->getParam('approveddate'))){
				$createarr = $this->_request->getParam('approveddate');
					$dataParamValue['Approved Date'] = $createarr[0];
					$dataParamValue['Approved Date_END'] = $createarr[1];
			}

			if(!empty($this->_request->getParam('updateddate'))){
				$createarr = $this->_request->getParam('updateddate');
					$dataParamValue['Updated Date'] = $createarr[0];
					$dataParamValue['Updated Date_END'] = $createarr[1];
			}
			// print_r($dataParamValue);die;
                      
    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
    // $filter = $zf_filter->getEscaped('filter');
    $filter 		= $this->_getParam('filter');
    $groupid     = html_entity_decode($zf_filter->getEscaped('GROUP_ID'));
	$groupname   = html_entity_decode($zf_filter->getEscaped('GROUP_NAME'));
	$lastupdatedby   = html_entity_decode($zf_filter->getEscaped('Last Updated By'));
	$lastapprovedby   = html_entity_decode($zf_filter->getEscaped('Last Approved By'));
	$status  = $zf_filter->getEscaped('Status');
	$datefrom   = html_entity_decode($zf_filter->getEscaped('Approved Date'));
	$dateto   = html_entity_decode($zf_filter->getEscaped('Approved Date_END'));
	$datefrom_updated   = html_entity_decode($zf_filter->getEscaped('Updated Date'));
	$dateto_update   = html_entity_decode($zf_filter->getEscaped('Updated Date_END'));
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;
    
    
    // proses pengambilan data filter,display all,sorting
    
      $caseStatus = "(CASE BGROUP_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";
		
  			
        $select2 = $this->_db->select()
					        ->from(array('A' => 'M_BGROUP'),array(	'A.BGROUP_ID',
																	'A.BGROUP_DESC',
																	'BGROUP_STATUS'=>$caseStatus,
																	'A.UPDATED','A.UPDATEDBY','A.CREATED','A.CREATEDBY')); 
			
  		/*if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}*/

  		//if($filter == null || $filter == 'Set Filter')
  		if($filter == TRUE)
		{
		 $this->view->fDateTo    = $dateto;
		 $this->view->fDateFrom  = $datefrom;

		 $this->view->fDateTo_Updated    = $dateto_update;
		 $this->view->fDateFrom_Updated  = $datefrom_updated;
		//Zend_Debug::dump($custid); die;
		 

		if($datefrom){
				$FormatDate 	= new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  	= $FormatDate->toString($this->_dateDBFormat);	
	            $select2->where("DATE(A.UPDATED) >= ?", $datefrom); 
	           }

	    if($dateto){
				$FormatDate 	= new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto 		= $FormatDate->toString($this->_dateDBFormat);	
	            $select2->where("DATE(A.UPDATED) <= ?", $dateto);
	           }

	    if($datefrom_updated){
				$FormatDate 	= new Zend_Date($datefrom_updated, $this->_dateDisplayFormat);
				$datefrom_updated  	= $FormatDate->toString($this->_dateDBFormat);	
	            $select2->where("DATE(A.CREATED) >= ?", $datefrom_updated); 
	           }

	    if($dateto_update){
				$FormatDate 	= new Zend_Date($dateto_update, $this->_dateDisplayFormat);
				$dateto_update 		= $FormatDate->toString($this->_dateDBFormat);	
	            $select2->where("DATE(A.CREATED) <= ?", $dateto_update);
	           }
		/*
		if(!empty($datefrom) && empty($dateto))
	            $select2->where("CONVERT(DATE, A.UPDATED) >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("CONVERT(DATE, A.UPDATED) <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("CONVERT(DATE, A.UPDATED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}
		*/
		}
        if($filter==TRUE)
        {
			if($groupid)$select2->where('UPPER(A.BGROUP_ID) LIKE '.$this->_db->quote('%'.strtoupper($groupid).'%'));
			if($groupname)$select2->where('UPPER(A.BGROUP_DESC) LIKE '.$this->_db->quote('%'.strtoupper($groupname).'%'));
			if($lastupdatedby)$select2->where('UPPER(A.CREATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($lastupdatedby).'%'));
			if($lastapprovedby)$select2->where('UPPER(A.UPDATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($lastapprovedby).'%'));
			if($status)$select2->where('BGROUP_STATUS=?',$status);
			//echo $status; die;
			
			$this->view->groupid = $groupid;
			$this->view->groupname = $groupname;
			$this->view->lastupdatedby = $lastupdatedby;
			$this->view->lastapprovedby = $lastapprovedby;
			$this->view->status = $status;
        }
		$select2->order($sortBy.' '.$sortDir); 

		// echo "<pre>";
		// var_dump($select2);
		// die();
		
		//$this->paging($select2);
		
		//Zend_Debug::dump($arr); die;
		//echo $select2; die;
 
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$result = $select2->query()->FetchAll();
			
			foreach($result as $key=>$value)
			{
				$result[$key]["UPDATED"] = Application_Helper_General::convertDate($value["UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
			
			if($csv)
			{
				Application_Helper_General::writeLog('BGLS','Download CSV Backend group list'); 
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv(array($this->language->_('Group ID'),$this->language->_('Group Name'),$this->language->_('Status'), $this->language->_('Last Updated'), $this->language->_('Last Updated By'), $this->language->_('Last Approved'), $this->language->_('Last Approved By')),$result,null,$this->language->_('Backend Group List'));
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('BGLS','Download PDF Backend group list'); 
				$this->_helper->download->pdf(array($this->language->_('Group ID'),$this->language->_('Group Name'),$this->language->_('Status'), $this->language->_('Last Updated')),$result,null,$this->language->_('Backend Group List'));
			}
			
			if($this->_request->getParam('print') == 1){
				unset($fields['action']);
				$this->_forward('print', 'index', 'widget', array('data_content' => $result, 'data_caption' => 'Backend Group List', 'data_header' => $fields));
			}
		}
		else
		{
			Application_Helper_General::writeLog('BGLS','Viewing Backend group list'); 
		}
    
    $this->paging($select2,30);
    //die;
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    // unset($dataParamValue['SUGEST_DATE_END']);
    	unset($dataParamValue['Approved Date_END']);
    	unset($dataParamValue['Updated Date_END']);
    	if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
     //insert log
	
  }
  
  
}