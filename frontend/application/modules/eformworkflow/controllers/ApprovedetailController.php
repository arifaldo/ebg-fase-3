<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_approvedetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');

		$stamp_fee = $settings->getSetting('stamp_fee');
		$this->view->stamp_fee = $stamp_fee;
		$adm_fee = $settings->getSetting('adm_fee');
		$this->view->adm_fee = $adm_fee;

		$this->view->systemType = $system_type;
		$this->view->ProvFee = 2000000;

		$conf = Zend_Registry::get('config');
		$this->_bankName = $conf['app']['bankname'];
		$this->view->masterbankname = $this->_bankName;

		$toc_ind = $settings->getSetting('ftemplate_bg_ind');
		$this->view->toc_ind = $toc_ind;
		$toc_eng = $settings->getSetting('ftemplate_bg_eng');
		$this->view->toc_eng = $toc_eng;

		$get_cash_collateral = $this->_db->select()
			->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
			->where("CUST_ID = ?", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$this->view->cash_collateral = $get_cash_collateral[0];

		$selectcomp = $this->_db->select()
			->from(array('A' => 'M_CUSTOMER'), array('*'))
			//  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
			->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->query()->fetchAll();

		$this->view->compinfo = $selectcomp[0];
		$numb = $this->_getParam('bgnumb');

		// decrypt numb
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token     = $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

		$numb = $BG_NUMBER;

		$checkOthersAttachment = $this->_db->select()
			->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
			->where("BG_REG_NUMBER = '$numb'")
			->order('A.INDEX ASC')
			->query()->fetchAll();

		if (count($checkOthersAttachment) > 0) {
			$this->view->othersAttachment = $checkOthersAttachment;
		}

		if (!empty($numb)) {


			$bgdata = $this->_db->select()
				->from(["A" => "TEMP_BANK_GUARANTEE"], ["*"])
				->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->where('A.BG_STATUS IN (2, 22)')
				->query()->fetchAll();

			if ($bgdata[0]['BG_OLD']) {
				$bgOld = $this->_db->select()
					->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
					->where('BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
					->query()->fetch();

				$this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
				$this->view->prevProv = $bgOld['PROVISION_FEE'];
			}


			$kontra = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
			$timePeriodStart = $bgdata[0]['TIME_PERIOD_START'];
			$timePeriodEnd = $bgdata[0]['TIME_PERIOD_END'];
			$bgAmount = $bgdata[0]['BG_AMOUNT'];
			$custId = $bgdata[0]['CUST_ID'];
			$usagePurpose = $bgdata[0]['USAGE_PURPOSE'];
			$usagePurposeDesc = $bgdata[0]['USAGE_PURPOSE_DESC'];
			$insuranceCode = $bgdata[0]['BG_INSURANCE_CODE'];

			// penkondisian kontra biaya provisi
			if ($kontra == '3') {

				$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
				$paramProvisionFee['CUST_ID'] = $insuranceCode;
				$paramProvisionFee['GRUP'] = $custId;
				$paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
				$paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
				$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

				$provisionPercentage = $getProvisionFee['provisionFee'];

				$getLfIns = $this->_db->select()
					->from(["MCLF" => "M_CUST_LINEFACILITY"], ["FEE_DEBITED"])
					->where("CUST_ID = ?", $insuranceCode)
					->query()->fetch();

				$this->view->feeDebited = $getLfIns['FEE_DEBITED'];
			}

			if ($kontra == '1') {
				$paramProvisionFee['TIME_PERIOD_START'] = $timePeriodStart;
				$paramProvisionFee['TIME_PERIOD_END'] = $timePeriodEnd;
				$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
				$paramProvisionFee['CUST_ID'] = $custId;
				$paramProvisionFee['GRUP'] = $custId;
				$paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
				$paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
				$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

				$provisionPercentage = $getProvisionFee['provisionFee'];
			}

			if ($kontra == '2') {

				$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
				$paramProvisionFee['CUST_ID'] = $custId;
				$paramProvisionFee['GRUP'] = $custId;
				$paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
				$paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
				$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

				$provisionPercentage = $getProvisionFee['provisionFee'];
			}
			// end pengkondisian kontra

			$provisi = Application_Helper_General::countProvision($kontra, ['TIME_PERIOD_START' => $timePeriodStart, 'TIME_PERIOD_END' => $timePeriodEnd, 'BG_AMOUNT' => $bgAmount], $provisionPercentage);

			$this->view->allToProvision = [$provisionPercentage, $provisi, $getProvisionFee];



			if (!empty($bgdata[0]['BG_OLD'])) {
				$tbgdata = $this->_db->select()
					->from(["A" => "T_BANK_GUARANTEE"], ["*"])
					->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
					->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
					->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
					->query()->fetch();
			}

			$bgdatadetail = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if (!empty($tbgdata['BG_REG_NUMBER'])) {
				$tbgdatadetail = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
					->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
					->query()->fetchAll();
			}

			// Guaranted Transaction -----------------------

			$getGuarantedTransanctions = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_UNDERLYING')
				->where('BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$this->view->guarantedTransanctions = $getGuarantedTransanctions;

			if (!empty($tbgdata['BG_REG_NUMBER'])) {
				$getGuarantedTransanctionsT = $this->_db->select()
					->from('T_BANK_GUARANTEE_UNDERLYING')
					->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
					->query()->fetchAll();

				$this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;
			}

			$bgdocType 		= $conf["bgdoc"]["type"]["desc"];
			$bgdocCode 		= $conf["bgdoc"]["type"]["code"];

			$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
			$this->view->arrbgdoc = $arrbgdoc;

			// end Guaranted Transaction -------------------

			if (!empty($tbgdata['BG_REG_NUMBER'])) {
				$checkOthersAttachmentT = $this->_db->select()
					->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
					->where("BG_REG_NUMBER = ?", $tbgdata['BG_REG_NUMBER'])
					->order('A.INDEX ASC')
					->query()->fetchAll();

				$this->view->othersAttachmentT = $checkOthersAttachmentT ?? [];

				$bgdatadetailT = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
					->query()->fetchAll();

				$this->view->bgdatadetailT = array_combine(array_map('strtolower', array_column($bgdatadetailT, 'PS_FIELDNAME')), array_column($bgdatadetailT, 'PS_FIELDVALUE'));

				$bgdatadetail = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $numb)
					->query()->fetchAll();

				$this->view->bgdatadetail = array_combine(array_map('strtolower', array_column($bgdatadetail, 'PS_FIELDNAME')), array_column($bgdatadetail, 'PS_FIELDVALUE'));
			}


			// Marginal Deposit View ------------------------
			// Marginal Deposit Principal ---------------
			$bgdatadetailmd = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if (!empty($bgdatadetailmd)) {
				foreach ($bgdatadetailmd as $key => $value) {
					if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
						$this->view->marginalDepositPercentage =   $value['PS_FIELDVALUE'];
					}
				}
			}
			// Marginal Deposit Principal ---------------

			// Marginal Deposit Eksisting ---------------
			$bgdatamdeks = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
				// ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				// ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
				->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : $numb)
				->query()->fetchAll();

			$this->view->bgdatamdeks = $bgdatamdeks ?? [];
			// Marginal Deposit Eksisting ---------------

			// Top Up Marginal Deposit ---------------
			$bgdatamd = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
				// ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
				// ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$this->view->bgdatamd = $bgdatamd ?? [];
			// Top Up Marginal Deposit ---------------
			// Marginal Deposit View ------------------------

			//$datas = $this->_request->getParams();
			// echo '<pre>';
			// var_dump($bgdatamd);
			// die;


			if (!empty($bgdata)) {

				$this->view->bgdata = $bgdata[0];
				$this->view->tbgdata = $tbgdata;

				// if ($bgdata[0]['CHANGE_TYPE'] == '0' && !empty($tbgdata[0])) {
				// 	$diff = array_diff($bgdata[0], $tbgdata[0]);
				// 	if (!empty($diff)) {
				// 		echo '<pre>';
				// 		var_dump($diff);
				// 		die;
				// 	}
				// }

				// switch ($bgdata[0]["CHANGE_TYPE"]) {
				// 	case '0':
				// 		$this->view->suggestion_type = "New";
				// 		break;
				// 	case '1':
				// 		$this->view->suggestion_type = "Amendment Changes";
				// 		break;
				// 	case '2':
				// 		$this->view->suggestion_type = "Amendment Draft";
				// 		break;
				// }

				// new code untuk amendemen
				$getct        = Zend_Registry::get('config');
				$ctdesc     = $getct["bg"]["changetype"]["desc"];
				$ctcode     = $getct["bg"]["changetype"]["code"];
				$suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

				$this->view->suggestion_type = $suggestion_type;

				$this->view->boundaryExist = false;

				$boundaryCek = $this->_db->select()
					->from("M_APP_GROUP_USER")
					->where("CUST_ID = ?", $this->_custIdLogin)
					->where("USER_ID = ?", $this->_userIdLogin);
				$boundaryCek = $this->_db->fetchRow($boundaryCek);
				
				$groupuserid = $boundaryCek['GROUP_USER_ID'];
				$groupusername = $boundaryCek['USER_ID'];
				$groupuseridexplode = explode("_", $groupuserid);

				if ($groupuseridexplode[0] == "S") {
					$this->view->usergroupid = "SG";
				} 
						



				if (!empty($boundaryCek)) {
					$cekExistBoundary = $this->_db->select()
						->from("M_APP_BOUNDARY_GROUP")
						->where("GROUP_USER_ID = ?", $boundaryCek["GROUP_USER_ID"]);
					$cekExistBoundary = $this->_db->fetchRow($cekExistBoundary);

					if (!empty($cekExistBoundary)) {
						$this->view->boundaryExist = true;
					}
				}




				$boundary = $this->validatebtn('38', $bgdata['0']['BG_AMOUNT'], 'IDR', $numb);


				if ($boundary) {
					// die;
					$this->view->validbtn = false;
				} else {
					// die;
					$this->view->validbtn = true;
				}

				$policyBoundary = $this->findPolicyBoundary(38, $bgdata['0']['BG_AMOUNT']);

				//echo '<pre>';
				//var_dump($policyBoundary);die;

				$approverUserList = $this->findUserBoundary(38, $bgdata['0']['BG_AMOUNT']);

				$newPolicyBoundary = '';
				foreach (explode(' ', $policyBoundary) as $key => $value) {
					if (array_key_exists($value, $approverUserList['GROUP_NAME'])) {
						$newPolicyBoundary .= ' ' . $approverUserList['GROUP_NAME'][$value];
					} else {
						$newPolicyBoundary .= ' ' . $value;
					}
				}

				$this->view->policyBoundary = trim($newPolicyBoundary);

				$releaserList = $this->findUserPrivi('RLBG');


				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->BG_PUBLISH = $arrbgpublish[$bgdata[0]['BG_PUBLISH']];

				if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();

					$this->view->insurance_branch = $insuranceBranch[0]["INS_BRANCH_NAME"];
				}

				if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
					$bgdatasplit = $this->_db->select()
						->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
						->where('A.BG_REG_NUMBER = ?', $numb)
						->query()->fetchAll();

					foreach ($bgdatasplit as $key => $value) {

						$callService = new Service_Account($value['ACCT'], '');
						$cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

						if (strtolower($cekAcctType) == 'deposito') {
							$inqbalance = $callService->inquiryDeposito();
						}
						
						if ($inqbalance['varian_rate']) {
							if (floatval($inqbalance['varian_rate']) !== floatval('0')) {
								$isSpecialRate = true;
							} else {
								$isSpecialRate = false;
							}
						}

						$temp_save = $this->_db->select()
							->from("M_CUSTOMER_ACCT")
							->where("ACCT_NO = ?", $value["ACCT"])
							->query()->fetchAll();

						$bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
						$bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
						$bgdatasplit[$key]["IS_SPECIAL_RATE"] = $isSpecialRate;
					}

					$bgdatasplitT = $this->_db->select()
						->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
						->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'] ? $bgdata[0]['BG_OLD'] : '')
						->query()->fetchAll();

					foreach ($bgdatasplitT as $key => $value) {
						$temp_save = $this->_db->select()
							->from("M_CUSTOMER_ACCT")
							->where("ACCT_NO = ?", $value["ACCT"])
							->query()->fetchAll();

						$bgdatasplitT[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
						$bgdatasplitT[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
					}

					$this->view->fullmember = $bgdatasplit ?? [];
					$this->view->fullmemberA = $bgdatasplit[0];
					$this->view->fullmemberT = $bgdatasplitT ?? [];
				}

				//BG Counter Guarantee Type
				$bgcgType         = $conf["bgcg"]["type"]["desc"];
				$bgcgCode         = $conf["bgcg"]["type"]["code"];

				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

				$this->view->warranty_type_text_new = $arrbgcg[$bgdata[0]['COUNTER_WARRANTY_TYPE']];



				$selectHistory	= $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY')
					->where("BG_REG_NUMBER = ?", $numb);

				$history = $this->_db->fetchAll($selectHistory);

				$config = Zend_Registry::get('config');

				$historyStatusCode = $config["history"]["status"]["code"];
				$historyStatusDesc = $config["history"]["status"]["desc"];
				$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

				$this->view->historyStatusArr   = $historyStatusArr;
				$this->view->dataHistory        = $history;

				// get linefacillity
				$paramLimit = array();

				$paramLimit['CUST_ID'] =  $this->_custIdLogin;
				$paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
				$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);


				$this->view->current_limit = $getLineFacility['currentLimit'];
				$this->view->max_limit =  $getLineFacility['plafondLimit'];

				//$this->view->linefacility = $get_linefacility[0];

				// end get linefacility

				$cust_approver = 1;
				//var_dump($history);
				foreach ($history as $row) {
					//if maker done
					if ($row['HISTORY_STATUS'] == 1 || $row['HISTORY_STATUS'] == 33) {
						$makerStatus = 'active';
						$makerIcon = '<i class="fas fa-check"></i>';

						$makerOngoing = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
							$reviewerOngoing = '';
							$approverOngoing = '';
							//$releaserOngoing = 'ongoing';
							$releaserOngoing = '';
						} else {
							//$reviewerOngoing = 'ongoing';
							$reviewerOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						}

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						$custEmail 	  = $customer[0]['USER_EMAIL'];
						$custPhone	  = $customer[0]['USER_PHONE'];

						$makerApprovedBy = $custFullname;

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

						$align = 'align="center"';
						$marginRight = '';

						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginRight = 'style="margin-right: 15px;"';
						}

						$this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
					}
					//if reviewer done

					//if approver done

					if ($row['HISTORY_STATUS'] == 2) {
						$selectuserapp	= $this->_db->select()
							->from(array('C' => 'T_APPROVAL'), array(
								'*'
							))
							->where("C.PS_NUMBER = ?", $numb)
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin);
						//->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
						//;

						$userapprove = $this->_db->fetchAll($selectuserapp);
						//tampung data user yang sudah approve
						if (!empty($userapprove) && empty($userid)) {
							foreach ($userapprove as $vl) {
								$userid[] = $vl['USER_ID'];
							}
						}

						$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
						//$userid[] = $custlogin;

					}
					if (false) {
						// $makerStatus = 'active';
						// $approveStatus = '';
						// $reviewStatus = 'active';

						// $makerOngoing = '';
						// $reviewerOngoing = '';
						// $approverOngoing = 'ongoing';
						// $releaserOngoing = '';

						// $custlogin = $row['USER_LOGIN'];

						// $selectuserapp	= $this->_db->select()
						// 	->from(array('C' => 'T_APPROVAL'), array(
						// 		'*'
						// 	))
						// 	->where("C.PS_NUMBER = ?", $numb)
						// 	->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						// 	//->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
						// ;

						// $userapprove = $this->_db->fetchAll($selectuserapp);
						// //tampung data user yang sudah approve
						// if (!empty($userapprove) && empty($userid)) {
						// 	foreach ($userapprove as $vl) {
						// 		$userid[] = $vl['USER_ID'];
						// 	}
						// }
						// //$userid[] = $custlogin;

						// $approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));


						$makerStatus = 'active';
						$makerIcon = '<i class="fas fa-check"></i>';

						$makerOngoing = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
							$reviewerOngoing = '';
							$approverOngoing = '';
							//$releaserOngoing = 'ongoing';
							$releaserOngoing = '';
						} else {
							//$reviewerOngoing = 'ongoing';
							$reviewerOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						}

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						$custEmail 	  = $customer[0]['USER_EMAIL'];
						$custPhone	  = $customer[0]['USER_PHONE'];

						$makerApprovedBy = $custFullname;

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

						$align = 'align="center"';
						$marginRight = '';

						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginRight = 'style="margin-right: 15px;"';
						}

						$this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
					}
					//if releaser done
					// if ($row['HISTORY_STATUS'] == 5) {
					if (false) {
						$makerStatus = 'active';
						/*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
						$approveStatus = '';
						$reviewStatus = '';
						$releaseStatus = '';
						$releaseIcon = '';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						// $custEmail 	  = $customer[0]['USER_EMAIL'];
						// $custPhone	  = $customer[0]['USER_PHONE'];

						$releaserApprovedBy = $custFullname;

						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						//$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
					}
				}

				//approvernamecircle jika sudah ada yang approve

				if (!empty($userid)) {


					$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

					$flipAlphabet = array_flip($alphabet);

					$approvedNameList = array();
					$i = 0;
					//var_dump($userid);die;
					foreach ($userid as $key => $value) {

						//select utk nama dan email
						$selectusername = $this->_db->select()
							->from(array('M_USER'), array(
								'*'
							))
							->where("USER_ID = ?", (string) $value)
							->where("CUST_ID = ?", (string) $this->_custIdLogin);

						$username = $this->_db->fetchAll($selectusername);

						//select utk cek user berada di grup apa
						$selectusergroup	= $this->_db->select()
							->from(array('C' => 'M_APP_GROUP_USER'), array(
								'*'
							))
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							->where("C.USER_ID 	= ?", (string) $value);

						$usergroup = $this->_db->fetchAll($selectusergroup);

						$groupuserid = $usergroup[0]['GROUP_USER_ID'];
						$groupusername = $usergroup[0]['USER_ID'];
						$groupuseridexplode = explode("_", $groupuserid);

						if ($groupuseridexplode[0] == "S") {
							$usergroupid = "SG";
						} else {
							$usergroupid = $alphabet[$groupuseridexplode[2]];
						}

						// $tempuserid = "";
						// foreach ($approverNameCircle as $row => $data) {
						// 	foreach ($data as $keys => $val) {
						// 		if ($keys == $usergroupid) {
						// 			if (preg_match("/active/", $val)) {
						// 				continue;
						// 			}else{
						// 				if ($groupuserid == $tempuserid) {
						// 					continue;
						// 				}else{
						// 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
						// 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
						// 				}
						// 				$tempuserid = $groupuserid;
						// 			}
						// 		}
						// 	}
						// }

						array_push($approvedNameList, $username[0]['USER_FULLNAME']);

						$efdate = $approveEfDate[$i];

						$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
						$i++;
					}

					$this->view->approverApprovedBy = $approverApprovedBy;


					//kalau sudah approve semua
					if (!$checkBoundary) {
						$approveStatus = '';
						$approverOngoing = '';
						$approveIcon = '';
						$releaserOngoing = 'ongoing';
					}
				}





				$selectsuperuser = $this->_db->select()
					->from(array('C' => 'T_APPROVAL'))
					->where("C.PS_NUMBER 	= ?", $numb)
					->where("C.GROUP 	= 'SG'");

				$superuser = $this->_db->fetchAll($selectsuperuser);

				if (!empty($superuser)) {
					$userid = $superuser[0]['USER_ID'];

					//select utk nama dan email
					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("USER_ID = ?", (string) $userid)
						->where("CUST_ID = ?", (string) $this->_custIdLogin);

					$username = $this->_db->fetchAll($selectusername);

					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

					$approveStatus = '';
					$approverOngoing = '';
					$approveIcon = '';
					$releaserOngoing = 'ongoing';
				}
				// <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
				//define circle
				$makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '
					
				</button>';

				foreach ($reviewerList as $key => $value) {

					$textColor = '';
					if ($value == $reviewerApprovedBy) {
						$textColor = 'text-white-50';
					}

					$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				// 
				$reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '
		
					</button>';

				$groupNameList = $approverUserList['GROUP_NAME'];
				unset($approverUserList['GROUP_NAME']);

				if ($approverUserList != '') {
					//echo '<pre>';
					//var_dump($approverUserList);die;
					foreach ($approverUserList as $key => $value) {
						$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
						$i = 1;
						foreach ($value as $key2 => $value2) {

							$textColor = '';
							if (in_array($value2, $approvedNameList)) {
								$textColor = 'text-white-50';
							}

							if ($i == count($value)) {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
							} else {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
							}
							$i++;
						}
					}
				} else {
					$approverListdata = 'There is no Approver User';
				}
				// 
				$spandata = '';
				if (!empty($approverListdata) && !$error_msg2) {
					$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
				}

				// $approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . ' ' . $spandata . ' </button>';
				$approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '</button>';

				foreach ($releaserList as $key => $value) {

					$textColor = '';
					if ($value == $releaserApprovedBy) {
						$textColor = 'text-white-50';
					}

					$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value['USER_FULLNAME'] . '</p>';
				}
				// 
				// $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span> </button>';
				$releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '</button>';

				$principleNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '
					<span class="hovertextcontent" style="left: -100px; text-align: center;">' . $principleListView . '</span>
								</button>';


				$this->view->policyBoundary = trim($newPolicyBoundary);
				$this->view->makerNameCircle = $makerNameCircle;
				$this->view->reviewerNameCircle = $reviewerNameCircle;
				$this->view->approverNameCircle = $approverNameCircle;
				$this->view->releaserNameCircle = $releaserNameCircle;
				$this->view->principleNameCircle = $principleNameCircle;


				$this->view->makerStatus = $makerStatus;
				$this->view->approveStatus = $approveStatus;
				$this->view->reviewStatus = $reviewStatus;
				$this->view->releaseStatus = $releaseStatus;


				$bgType 		= $conf["bg"]["type"]["desc"];
				$bgCode 		= $conf["bg"]["type"]["code"];

				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));

				$this->view->arrbgType = $arrbgType;

				$data = $bgdata['0'];

				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					//var_dump($selectbranch[0]['BRANCH_NAME']);die;
					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


				//$config    		= Zend_Registry::get('config');

				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

				$this->view->arrStatus = $arrStatus;
				$cust_id = $this->_custIdLogin;
				if ($data["COUNTER_WARRANTY_TYPE"] == 3 || $data["COUNTER_WARRANTY_TYPE"] == '3') {
					$cust_id = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE_DETAIL")
						->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
						->where("PS_FIELDNAME LIKE '%Insurance Name%'")
						->query()->fetch();

					$cust_id = $cust_id["PS_FIELDVALUE"];
				}
				$CustomerUser = new CustomerUser($bgdata[0]['CUST_ID'], $this->_userIdLogin);
				$param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
				$AccArr = $CustomerUser->getAccountsBG($param);
				// var_dump($AccArr);
				// die;

				if (!empty($AccArr)) {
					$this->view->src_name = $AccArr['0']['ACCT_NAME'];
					$this->view->currency_type = $AccArr['0']['CCY_ID'];
				}

				$arrBankFormat = array(
					1 => 'Bank Standard',
					2 => 'Special Format (with bank approval)'
				);

				$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
				$this->view->bankFormatNumber = $data['BG_FORMAT'];

				$arrLang = array(
					1 => 'Indonesian',
					2 => 'English',
					3 => 'Billingual',
				);

				$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

				$arrWaranty = array(
					1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
					2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
					3 => 'Insurance'

				);

				$this->view->repairto = ["prinsipal" => "Prinsipal", "asuransi" => "Asuransi"];
				if ($data['COUNTER_WARRANTY_TYPE'] == 1 || $data['COUNTER_WARRANTY_TYPE'] == 2) {
					$this->view->repairto = ["prinsipal" => "Prinsipal"];
				}

				$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];
				/*		
                         if(!empty($data['USAGE_PURPOSE'])){
                            $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                            foreach($data['USAGE_PURPOSE'] as $key => $val){
                                $str = 'checkp'.$val;
                                //var_dump($str);
                                $this->view->$str =  'checked';
                            }
                        }
						*/
				$this->view->usage_purpose = $data['USAGE_PURPOSE'];

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}
				$this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];
				$this->view->data = $data;

				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->contact_person = $data['RECIPIENT_CP'];
				$this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
				$this->view->contact_email = $data['RECIPIENT_EMAIL'];

				$this->view->comment = $data['SERVICE'];

				$this->view->fileName = $data['FILE'];
				$this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
				$bgdocType 		= $conf["bgdoc"]["type"]["desc"];
				$bgdocCode 		= $conf["bgdoc"]["type"]["code"];

				$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

				$this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];
				//$this->view->GT_TYPE = $data['GT_DOC_TYPE'];
				$this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];


				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];
				$this->view->status = $data['BG_STATUS'];
				$Settings = new Settings();
				$claim_period = $Settings->getSettingNew('max_claim_period');
				$this->view->claim_period = $claim_period;

				if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$selectQuery  = "SELECT
                                a.USER_LOGIN,
                                b.`USER_FULLNAME` AS u_name,
                                c.`BUSER_NAME` AS b_name,
                                a.DATE_TIME,
                                a.BG_REASON,
                                a.HISTORY_STATUS,
                                a.BG_REG_NUMBER
                                
                                
                              FROM
                                TEMP_BANK_GUARANTEE_HISTORY AS a
                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
					$result =  $this->_db->fetchAll($selectQuery);
					if (!empty($result)) {
						$data['REASON'] = $result['0']['BG_REASON'];
					}
					$this->view->reqrepair = true;
					$this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
				}

				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}

				if (!empty($tbgdatadetail)) {
					foreach ($tbgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->tinsuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->tPrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->tinsurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->tpaDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->tpaDateEnd =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->towner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->tamountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->towner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->tamountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->towner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->tamountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}



				$conf = Zend_Registry::get('config');
				$this->view->bankname = $conf['app']['bankname'];

				$check_boundary	= $this->_db->select()
					->from(array('C' => 'M_APP_BOUNDARY'), array(
						'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
						'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
						'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
						'C.TRANSFER_TYPE',
						'C.POLICY'
					))
					->where("C.TRANSFER_TYPE 	= ?", (string) '38')
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.BOUNDARY_MIN 	<= ?", $bgdata['0']['BG_AMOUNT'])
					->where("C.BOUNDARY_MAX 	>= ?", $bgdata['0']['BG_AMOUNT']);
				//echo $check_boundary;
				$check_boundary = $this->_db->fetchAll($check_boundary);

				$this->view->inOfBoundary = true;
				if (empty($check_boundary)) {
					$this->view->inOfBoundary = false;
				}

				$errLimitIns = false;
				if ($data["COUNTER_WARRANTY_TYPE"] == "3" && $data['BG_STATUS'] == '22') {
					$principleData = [];
					// if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					foreach ($bgdatadetail as $key => $value) {
						$principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
					}
					$cust_id = $principleData["Insurance Name"];
					$get_linefacilityINS = $this->_db->select()
						->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT", "STATUS"])
						->where("CUST_ID = ?", $cust_id)
						->query()->fetchAll();


					$check_all_detail = $this->_db->select()
						->from("T_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $cust_id)
						->query()->fetchAll();

					$total_bgamount_on_risk = 0;

					if (count($check_all_detail) > 0) {
						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_risks = $this->_db->select()
							->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
							->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->query()->fetchAll();

						foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
							$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
						}
					}

					$check_all_detail = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $cust_id)
						->query()->fetchAll();

					$total_bgamount_on_temp = 0;

					if (count($check_all_detail) > 0) {

						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_temps = $this->_db->select()
							->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
							->where("COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->where("BG_STATUS IN (?)", ["5", "6", "7", '10', "14", "17", "20", '21', '22', '23', '24'])
							->query()->fetchAll();

						foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
							$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
						}
					}

					$current_limitINS = $get_linefacilityINS[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;

					$this->view->current_limit_ins = $current_limitINS;
					// if ($data['BG_AMOUNT'] >= $current_limitINS) {
					if ($current_limitINS < 0) {
						$errLimitIns = true;

						$errmsg = 'Limit asuransi tidak tersedia';
						$this->view->errorTickerSize = $errmsg;
					}

					// die;

				}

				//echo '<pre>';
				//var_dump($data);

				$download = $this->_getParam('download');
				//print_r($edit);die;
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				if ($this->_request->isPost()) {
					$approve = $this->_getParam('approve');
					$BG_NUMBER = $this->_getParam('bgnumb');
					$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

					$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
					$reject = $this->_getParam('reject');
					$repair = $this->_getParam('repair');

					/*$datas = $this->_request->getParams();
					echo '<pre>';
					var_dump($datas);die;
					*/


					//print_r($edit);die;
					if ($approve) {
						// start validasi jaminan
						$errValidasi = false;

						if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '2') {

							$cust_id = $this->_custIdLogin;
							$jenisjaminan = ($data['USAGE_PURPOSE'] == 'F4299') ? strtoupper($data['USAGE_PURPOSE_DESC']) : strtoupper($arrbgType[$data['USAGE_PURPOSE']]);

							$check_grup_bumn = $this->_db->select()
								->from("M_CUSTOMER")
								->where("CUST_STATUS = 1")
								->where("CUST_ID = ?", $cust_id)
								->query()->fetchAll();


							$check_charges_bg = $this->_db->select()
								->from("M_CHARGES_BG")
								->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'LFBUMN' : 'LFNONBUMN') . '%')
								->where("CHARGES_NAME LIKE ?", '' . 'LF ' . $jenisjaminan . '%')
								->query()->fetchAll();


							$check_lf_detail = $this->_db->select()
								->from("M_CUST_LINEFACILITY_DETAIL")
								->where("OFFER_TYPE = ?", $jenisjaminan)
								//->where("FLAG = 1")
								->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
								->where("CUST_ID = ?", $cust_id)
								->query()->fetchAll();
							$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];

							$check_lf_detail[0]['FLAG'] == '1' && $check_lf_detail[0]['ACTIVE'] == 'Y' ? $errValidasi = false : $errValidasi = true;
						}

						if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {

							$cust_id = $data['BG_INSURANCE_CODE'];
							// $jenisjaminan = strtoupper($arrbgType[$data['USAGE_PURPOSE']]);
							$jenisjaminan = ($data['USAGE_PURPOSE'] == 'F4299') ? strtoupper($data['USAGE_PURPOSE_DESC']) : strtoupper($arrbgType[$data['USAGE_PURPOSE']]);

							$check_grup_bumn = $this->_db->select()
								->from("M_CUSTOMER")
								->where("CUST_STATUS = 1")
								->where("CUST_ID = ?", $cust_id)
								->query()->fetchAll();


							$check_charges_bg = $this->_db->select()
								->from("M_CHARGES_BG")
								->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'INSBUMN' : 'INSNONBUMN') . '%')
								->where("CHARGES_NAME LIKE ?", '' . 'ASURANSI ' . $jenisjaminan . '%')
								->query()->fetchAll();



							$check_lf_detail = $this->_db->select()
								->from("M_CUST_LINEFACILITY_DETAIL")
								->where("OFFER_TYPE = ?", $jenisjaminan)
								//->where("FLAG = 1")
								->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
								->where("CUST_ID = ?", $cust_id)
								->query()->fetchAll();

							$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];

							$check_lf_detail[0]['FLAG'] == '1' && $check_lf_detail[0]['ACTIVE'] == 'Y' ? $errValidasi = false : $errValidasi = true;
						}


						//echo $errValidasi;
						//die;
						// end validasi jaminan
						if (empty($errValidasi)) {


							$selectusergroup	= $this->_db->select()
								->from(array('C' => 'M_APP_GROUP_USER'), array(
									'*'
								))
								->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
								->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

							$usergroup = $this->_db->fetchAll($selectusergroup);
							$selectuser	= $this->_db->select()
								->from(array('C' => 'M_APP_BOUNDARY'), array(
									'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
									'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
									'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
									'C.TRANSFER_TYPE',
									'C.POLICY'
								))
								->where("C.TRANSFER_TYPE 	= ?", (string) '38')
								->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
								->where("C.BOUNDARY_MIN 	<= ?", $bgdata['0']['BG_AMOUNT'])
								->where("C.BOUNDARY_MAX 	>= ?", $bgdata['0']['BG_AMOUNT']);
							// echo $selectuser;
							$datauser = $this->_db->fetchAll($selectuser);

							// print_r($datauser);die;
							if (!empty($usergroup)) {
								$cek = false;
								foreach ($usergroup as $key => $value) {
									$group = explode('_', $value['GROUP_USER_ID']);
									$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
									$groupalfa = $alphabet[(int) $group[2]];
									// print_r($groupalfa);echo '-';
									$usergroup[$key]['GROUP'] = $groupalfa;

									foreach ($datauser as $nub => $val) {
										$command = str_replace('(', '', $val['POLICY']);
										$command = str_replace(')', '', $command);
										$list = explode(' ', $command);

										foreach ($list as $row => $data) {
											if ($data == $groupalfa) {
												// print_r($data);die;
												$groupuser = $data;
												$cek = true;
												// die('ter');
												break;
											}
										}
									}
								}

								if ($group[0] == 'S') {
									$cek = true;
									$groupuser = 'SG';
								}
								// echo $cek;
								// print_r($cek);die;
								if (empty($datauser)) {
									$groupuser = '-';
								} else if (!$cek) {
									return false;
								}

								$verifierInsert = array(
									'PS_NUMBER'  			=> $BG_NUMBER,
									'CUST_ID' 				=> $this->_custIdLogin,
									'USER_ID' 				=> $this->_userIdLogin,
									'GROUP' 				=> $groupuser,
									'CREATED' 				=> new Zend_Db_Expr("now()")
								);
								//print_r($verifierInsert);die;
								$this->_db->insert('T_APPROVAL', $verifierInsert);

								$historyInsert = array(
									'DATE_TIME'         => new Zend_Db_Expr("now()"),
									'BG_REG_NUMBER'         => $BG_NUMBER,
									'CUST_ID'           => $this->_custIdLogin,
									'USER_LOGIN'        => $this->_userIdLogin,
									// 'HISTORY_STATUS'    => 2
								);

								$checkNotTopUp = $this->_db->select()
									->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
									->where('A.BG_REG_NUMBER = ?', $numb)
									->where('A.IS_TRANSFER != ?', '1')
									->query()->fetchAll();

								if ($this->view->warranty_type == '3' && !empty($checkNotTopUp)) {
									$historyInsert['HISTORY_STATUS'] = 21;
								} else {
									$historyInsert['HISTORY_STATUS'] = 2;
								}

								$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

								if (!empty($datauser)) {
									//var_dump($list);die;

									$approve = $this->validateapproval($datauser['0']['POLICY'], $list);
								} else {
									$approve = true;
								}
							}


							if ($group[0] == 'S') {

								$approve = true;
							}


							if ($approve == true) {
								// die('here');
								// new code 14-feb-2023 -------------------------------------
								if ($bgdata[0]['BG_STATUS'] == '22') {
									$data = array('BG_STATUS' => '23');
									$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
									$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);
									// new code 14-feb-2023 -------------------------------------
								} else {
									$data = array('BG_STATUS' => '3');
									$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
									$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);
								}
							}

							//echo $approve;die;

							/*$verifierInsert = array(
								'PS_NUMBER'  			=> $BG_NUMBER,
								'CUST_ID' 				=> $this->_custIdLogin,
								'USER_ID' 				=> $this->_userIdLogin,
								'GROUP' 				=> $groupuser,
								'CREATED' 				=> new Zend_Db_Expr("now()")
							);
							//print_r($verifierInsert);die;
							$this->_db->insert('T_APPROVAL', $verifierInsert);
							
							$data = array('BG_STATUS' => '3');
							$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
							$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

							$historyInsert = array(
								'DATE_TIME'         => new Zend_Db_Expr("now()"),
								'BG_REG_NUMBER'         => $BG_NUMBER,
								'CUST_ID'           => $this->_custIdLogin,
								'USER_LOGIN'        => $this->_userIdLogin,
								'HISTORY_STATUS'    => 2
							);

							$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
							*/

							// $this->setbackURL('/'.$this->_request->getModuleName().'/review/');
							if ($this->view->warranty_type == '3' && !empty($bgdatamd)) {
								Application_Helper_General::writeLog('MDBG', 'Menyetujui Permintaan Tambah Marginal Deposit BG No Reg : ' . $bgdata[0]['BG_REG_NUMBER']);
							} else {
								Application_Helper_General::writeLog('APBG', 'Menyetujui BG No Reg : ' . $bgdata[0]['BG_REG_NUMBER']);
							}

							$this->setbackURL('/' . $this->_request->getModuleName() . '/approve/');
							$this->_redirect('/notification/success/index');
						} else {
							$this->view->err = true;
							$this->view->errMsg = 'Jaminan tidak tersedia';
						}
					}

					if ($reject) {
						$data = array('BG_STATUS' => '25');
						$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
						$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

						$notes = $this->_getParam('PS_REASON_REJECT');
						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'         => $BG_NUMBER,
							'CUST_ID'           => $this->_custIdLogin,
							'USER_LOGIN'        => $this->_userIdLogin,
							'HISTORY_STATUS'    => 4,
							'BG_REASON'         => $notes,
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

						Application_Helper_General::writeLog('APBG', 'Tolak BG No Reg : ' . $bgdata[0]['BG_REG_NUMBER'] . ', Catatan : ' . $notes);

						$this->setbackURL('/' . $this->_request->getModuleName() . '/approve/');
						$this->_redirect('/notification/success/index');
					}


					if ($repair) {
						//die('here');
						$repairmd = $this->_getParam('repairmd');

						// hapus data sebelumnya
						if ($bgdata[0]['BG_STATUS'] == '22' && !empty($bgdatamd) && $repairmd == '4') {
							$this->_db->delete("TEMP_BANK_GUARANTEE_SPLIT", ["BG_REG_NUMBER = ?" => $numb]);
						}
						// end hapus data sebelumya

						$data = array(
							// 'BG_STATUS' => '4',
							'NTU_COUNTING' => '0',
							'BG_UPDATED' => new Zend_Db_Expr("now()"),
						);

						if ($repairmd == '24') {
							$data['BG_STATUS'] = '24';
						} else {
							$data['BG_STATUS'] = '4';
						}

						// echo '<pre>';
						// print_r($data);die;
						$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
						$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

						$notes = $this->_getParam('PS_REASON_REPAIR');
						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'         => $BG_NUMBER,
							'CUST_ID'           => $this->_custIdLogin,
							'USER_LOGIN'        => $this->_userIdLogin,
							// 'HISTORY_STATUS'    => 3,
							'BG_REASON'         => $notes,
						);

						if ($repairmd == '24') {
							$historyInsert['HISTORY_STATUS'] = 22;
						} else {
							$historyInsert['HISTORY_STATUS'] = 3;
						}

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

						if ($repairmd == '24') {
							Application_Helper_General::writeLog('APBG', 'Permintaan Perbaikan Marginal Deposit Prinsipal BG No Reg : ' . $bgdata[0]['BG_REG_NUMBER'] . ', Catatan : ' . $notes);
						} else {
							Application_Helper_General::writeLog('APBG', 'Permintaan Perbaikan BG No Reg : ' . $bgdata[0]['BG_REG_NUMBER'] . ', Catatan : ' . $notes);
						}

						$this->setbackURL('/' . $this->_request->getModuleName() . '/approve/');
						$this->_redirect('/notification/success/index');
					}

					$back = $this->_getParam('back');
					if ($back) {
						$this->_redirect('/eformworkflow/approve');
					}
				}
			}
			Application_Helper_General::writeLog('APBG', 'Lihat Detail BG No Reg : ' . $data['BG_REG_NUMBER']);
		}
	}

	public function findPolicyBoundary($transfertype, $amount)
	{


		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{



		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}

		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}


		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	public function findUserPrivi($privID)
	{



		$selectuser	= $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'))
			->joinLeft(array('B' => 'M_USER'), 'A.FUSER_ID = CONCAT(B.CUST_ID,B.USER_ID)', array('USER_FULLNAME'))
			->where("A.FPRIVI_ID 	= ?", (string) $privID)
			->where("B.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->order('B.USER_FULLNAME ASC');

		//echo $selectuser;die();
		return $datauser = $this->_db->fetchAll($selectuser);
	}

	public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		//die;

		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		// echo $selectuser;

		$datauser = $this->_db->fetchAll($selectuser);
		if (empty($datauser)) {

			return true;
		}

		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_USER'), array(
				'*'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);




		$this->view->boundarydata = $datauser;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {
				$group = explode('_', $value['GROUP_USER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				// print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				//var_dump($usergroup);
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);

					//var_dump($list);
					foreach ($list as $row => $data) {

						if ($data == $groupalfa) {
							$cek = true;
							// die('ter');
							break;
						}
					}
				}
			}
			//die;

			//var_dump($group);die;

			if ($group[0] == 'S') {
				return true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		}
		$tempusergroup = $usergroup;

		if ($cek) {




			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);

			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);
			//var_dump($phpCommand);die;
			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);
			// print_r($list);die;
			// var_dump($command)

			$thendata = explode('THEN', $command);
			// print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);
			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {
					foreach ($secondlist as $row => $thenval) {
						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			//var_dump($cthen);
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
					// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						$newsecondcommand = str_replace('(', '', trim($thenval));
						$newsecondcommand = str_replace(')', '', $newsecondcommand);
						$newsecondcommand = str_replace('AND', '', $newsecondcommand);
						$newsecondcommand = str_replace('OR', '', $newsecondcommand);
						$newsecondlist = explode(' ', $newsecondcommand);
						//var_dump($newsecondcommand);
						if (in_array(trim($value['GROUP']), $newsecondlist)) {
							//if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}
			//var_dump($grouplist);die;
			//var_dump($thengroup);die;
			// var_dump($thengroup);die;
			// // print_r($group);die;
			// // echo $thengroup;die;
			//echo '<pre>';
			//var_dump($thengroup);
			//var_dump($thendata);
			//print_r($thendata);echo '<br/>';die('here');
			if ($thengroup == true) {


				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;
					//echo $oriCommand;die;
					$indno = $i;
					//echo $oriCommand;echo '<br>';

					for ($a = $cthen - $indno; $a >= 1; --$a) {

						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}


					//print_r($thendata);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					//print_r($oriCommand);echo '<br>';
					//print_r($list);echo '<br>';die;


					$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
					// print_r($i);
					//var_dump($result);
					//echo 'result-';var_dump($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							//die;
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);
							//						var_dump($secondlist);
							//						var_dump($grouplist);
							//							die;

							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											//echo 'sini';
											return false;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						//echo $oriCommand;
						$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
						//var_dump($result);echo '<br/>';
						if (!$result) {
							// die;
							//print_r($groupalfa);
							//print_r($thendata[$i]);

							if ($groupalfa == trim($thendata[$i])) {

								return true;
							} else {

								//return true;
							}
							/*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
						} else {

							//return false;

							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						//die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);
						//var_dump($i);
						// var_dump($thendata);
						//print_r($grouplist);
						//die;
						//if($groupalfa == $gro)
						$approver = array();
						$countlist = array_count_values($list);
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								$selectapprover	= $this->_db->select()
									->from(array('C' => 'T_APPROVAL'), array(
										'USER_ID'
									))
									->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
									// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
									->where("C.PS_NUMBER = ?", (string) $psnumb)
									->where("C.GROUP = ?", (string) $value);
								//	 echo $selectapprover;
								$usergroup = $this->_db->fetchAll($selectapprover);
								// print_r($usergroup);
								$approver[$value] = $usergroup;
								if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
									//var_dump($countlist[$value]);
									//var_dump($tempusergroup['0']['GROUP']);  
									//die('gere');
									return false;
								}
							}
						}

						//$array = array(1, "hello", 1, "world", "hello");


						//echo '<pre>';
						//var_dump($list);
						//var_dump($approver);
						//die;
						//var_dump($secondlist[0]);die;
						//foreach($approver as $app => $valpp){
						if (!empty($thendata[$i])) {
							if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
								//die('gere');
								return false;
							}
						}

						//echo '<pre>';
						//var_dump($thendata);
						//var_dump($grouplist);
						//var_dump($approver);
						//var_dump($groupalfa);
						//die;
						foreach ($grouplist as $key => $vg) {
							$newgroupalpa = str_replace('AND', '', $vg);
							$newgroupalpa = str_replace('OR', '', $newgroupalpa);
							$groupsecondlist = explode(' ', $newgroupalpa);
							//var_dump($newgroupalpa);
							//&& empty($approver[$groupalfa])
							if (in_array($groupalfa, $groupsecondlist)) {

								return true;
								//die('ge');
							}

							if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
								// echo 'gere';die;

								return true;
							}
						}

						// var_dump($approver[$groupalfa]);
						// var_dump($grouplist);
						//var_dump($groupalfa);die;

						//echo 'ger';die;
						//print_r($secondlist);die;
						//	 return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {

										if (empty($thendata[1])) {
											//die;

											return true;
										}
										//else{
										//	return false;
										//	}
									}
								}
							}
						}

						//	echo 'here';die;
						$secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);
						// var_dump($secondresult);
						//print_r($thendata[$i-1]);
						//die;
						//if()
						//	 echo '<pre>';
						//var_dump($grouplist);die;
						foreach ($grouplist as $key => $valgroup) {
							//print_r($valgroup);
							//var_dump($thendata[$i]); 


							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								//die('here');
								if ($secondresult) {
									return false;
								} else {

									return true;
								}

								//break;
							}

							//else if (trim($valg) == trim($thendata[$i - 1])) {
							//		$cekgroup = false;
							// die('here');
							//	return false;
							//	}
						}
						//die;
						//if (!$cekgroup) {
						// die('here');
						//return false;
						//}
					}

					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {

				//var_dump($groupalfa)die;
				foreach ($thendata as $ky => $vlue) {
					$newsecondcommand = str_replace('(', '', trim($vlue));
					$newsecondcommand = str_replace(')', '', $newsecondcommand);
					$newsecondcommand = str_replace('AND', '', $newsecondcommand);
					$newsecondcommand = str_replace('OR', '', $newsecondcommand);
					$newsecondlist = explode(' ', $newsecondcommand);
					if ($newsecondlist['0'] == $groupalfa) {
						return true;
					}
				}
				return false;
			}




			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_APPROVAL'), array(
							'USER_ID'
						))
						->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.PS_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
					//	 echo $selectapprover;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			//die;




			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$numb = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $numb . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			$thendata = explode('THEN$', $phpCommand);
			//var_dump($thendata);die;
			if (!empty($thendata['1'])) {
				$phpCommand = '';
				foreach ($thendata as $tkey => $tval) {
					$phpCommand .= '(';
					$phpCommand .= $tval . ')';
					if (!empty($thendata[$tkey + 1])) {
						$phpCommand .= ' && ';
					}
				}
			} else {

				$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			}
			//var_dump($phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			//var_dump($result);die;
			if (!$result) {

				return true;
			}
			// die('here2');
			//var_dump ($result);die;
			//return $result;
		} else {

			// die('here');
			return true;
		}
	}

	public function generate($command, $list, $param, $psnumb, $group, $thengroup)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();

		$count_list = array_count_values($list);
		//print_r($count_list);
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.PS_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		//var_dump($param);die;
		//var_dump($group);
		foreach ($approver as $appval) {
			$totaldata = count($approver[$group]);
			$totalgroup = $count_list[$group];
			//var_dump($totaldata);
			//var_dump($totalgroup);
			if ($totalgroup == $totaldata && $totalgroup != 0) {

				return false;
			}
		} //die;
		//die;





		foreach ($param as $url) {

			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$numb = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $numb . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}

		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		//print_r($phpCommand);echo '<br/>';

		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			//var_dump($thengroup);
			// var_dump($result);

			if ($result) {
				//var_dump($thengroup);die;
				if ($thengroup) {
					return true;
				} else {
					return false;
				}
			} else {

				if ($thengroup) {
					return false;
				} else {

					return true;
				}
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;




	}

	public function validateapproval($policy, $list)
	{
		$command = ' ' . $policy . ' ';
		$command = strtoupper($command);

		$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
		$commandspli = explode('THEN', $cleanCommand);
		$commandnew = '';
		foreach ($commandspli as $ky => $valky) {
			if ($commandspli[$ky + 1] != '') {
				$commandnew .= '(' . $valky . ') THEN ';
			} else {
				$commandnew .= '(' . $valky . ')';
			}
		}

		//transform to php logical operator syntak
		$translate = array(
			'AND' => '&&',
			'OR' => '||',
			'THEN' => '&&',
			'A' => '$A',
			'B' => '$B',
			'C' => '$C',
			'D' => '$D',
			'E' => '$E',
			'F' => '$F',
			'G' => '$G',
			'H' => '$H',
			'I' => '$I',
			'J' => '$J',
			'K' => '$K',
			'L' => '$L',
			'M' => '$M',
			'N' => '$N',
			'O' => '$O',
			'P' => '$P',
			'Q' => '$Q',
			'R' => '$R',
			// 'S' => '$S',
			'T' => '$T',
			'U' => '$U',
			'V' => '$V',
			'W' => '$W',
			'X' => '$X',
			'Y' => '$Y',
			'Z' => '$Z',
			'SG' => '$SG',
		);

		$phpCommand =  strtr($commandnew, $translate);

		$param = array(
			'0' => '$A',
			'1' => '$B',
			'2' => '$C',
			'3' => '$D',
			'4' => '$E',
			'5' => '$F',
			'6' => '$G',
			'7' => '$H',
			'8' => '$I',
			'9' => '$J',
			'10' => '$K',
			'11' => '$L',
			'12' => '$M',
			'13' => '$N',
			'14' => '$O',
			'15' => '$P',
			'16' => '$Q',
			'17' => '$R',
			// '18' => '$S',
			'19' => '$T',
			'20' => '$U',
			'21' => '$V',
			'22' => '$W',
			'23' => '$X',
			'24' => '$Y',
			'25' => '$Z',
			'26' => '$SG',
		);
		// print_r($phpCommand);die;
		// function str_replace_first($from, $to, $content,$row)
		// {
		//     $from = '/'.preg_quote($from, '/').'/';
		//     return preg_replace($from, $to, $content, $row);
		// }

		$command = str_replace('(', '', $policy);
		$command = str_replace(')', '', $command);
		$list = explode(' ', $command);
		// print_r($list);die;
		$approver = array();
		$BG_NUMBER = $this->_getParam('bgnumb');
		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));
		$AESMYSQL = new Crypt_AESMYSQL();
		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					// ->where("C.USER_ID 	= ?" , (string)$this->_userId)
					->where("C.PS_NUMBER = ?", (string) $BG_NUMBER)
					->where("C.GROUP = ?", (string) $value);
				//echo $selectapprover;die;
				$usergroup = $this->_db->fetchAll($selectapprover);
				//print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		// print_r($approver);die;
		foreach ($param as $url) {
			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
							foreach ($approver[$value] as $row => $val) {
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
								}
							}
						}
					}
				}
				for ($i = 1; $i <= $ta; $i++) {
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
							$values = 'G' . $value;
							if (empty(${$values}[$i])) {
								${$values}[$i] = false;
							}
						}
					}

					$numb = $i;
					$label = str_replace('$', '$G', $url);

					$replace = $label . '[' . $numb . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
						//	 print_r($phpCommand);
					} else {
						$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
						//	 print_r($phpCommand);
					}
				}
			}
		}
		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		//print_r($phpCommand);
		// if ($phpCommand == "(true)")
		if (str_replace([')', '('], '', $phpCommand) != '') eval('$result = ' . "$phpCommand;");
		// eval('$result = ' . "$phpCommand;");
		// var_dump ($result);die;
		return $result;
	}

	public function str_replace_first($from, $to, $content, $row)
	{
		$from = '/' . preg_quote($from, '/') . '/';
		return preg_replace($from, $to, $content, $row);
	}

	public function mdfcAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

		if ($bgRegNumber == null) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'nothing']);
			return true;
		}

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
			->where('A.BG_REG_NUMBER = ?', $bgRegNumber)
			->query()->fetchAll();

		$bgdatasplit = array_filter($bgdatasplit, function ($acct) {
			if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro')
				return true;
		});

		$mdAmount = 0;

		$htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

		foreach ($bgdatasplit as $key => $value) {

			$callService = new Service_Account($value['ACCT'], '');
			$cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

			if (strtolower($value['ACCT_DESC']) == 'escrow') {
				$inqbalance = $callService->inquiryAccountBalance();
				if (intval($inqbalance['available_balance']) == 0) {
					// hapus data atau ??
					continue;
				} else $mdAmount += $value['AMOUNT'];
			} else {
				// jika deposito
				if (strtolower($cekAcctType) == 'deposito') {
					$inqbalance = $callService->inquiryDeposito();

					if (strtolower($inqbalance['hold_description']) == 'n') {
						// hapus data atau ??
						continue;
					} else $mdAmount += $value['AMOUNT'];
				}
				// end jika deposito

				// jika tabungan
				else {
					$inqbalance = $callService->inqLockSaving();

					$cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
						return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
					}));

					if (!$cekSaving) {
						// hapus data atau
						continue;
					} else $mdAmount += $value['AMOUNT'];
				}
				// end jika tabungan
			}

			$htmlResult .= "
            <tr>
                <td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
                <td>" . str_pad($value['ACCT'], 16, '0', STR_PAD_LEFT) . "</td>
                <td>" . $value['NAME'] . "</td>
                <td>" . ($value['ACCT_DESC'] ?: $value['ACCT_DESC_MCA']) . "</td>
                <td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($value['CCY_ID'] ?: $value['CCY_ID_MCA'])) . "</td>
                <td>" . number_format($value['AMOUNT'], 2) . "</td>
            </tr>";
		}

		$htmlResult .= "</tbody></table>";
		$htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksis'></input>";

		echo $htmlResult;
	}
}
