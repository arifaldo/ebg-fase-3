<?php


require_once 'Zend/Controller/Action.php';


class charges_ChargesreportlistController extends Application_Main
{
	public function indexAction() 
	{
		$fields = array	(
							'Company Code'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Company Code',
																	'sortable' => false
																),
							'Company Name'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Company Name',
																	'sortable' => false
																),
							'Charging Date'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Charging Date',
																	'sortable' => false
																),
							'Acct Source'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Acct Source',
																	'sortable' => false
																),
							'Source Currency'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Source Currency',
																	'sortable' => false
																),
							'Acct COA'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Acct COA',
																	'sortable' => false
																),
							'Amount'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Amount',
																	'sortable' => false
																),
							'Status'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Status',
																	'sortable' => false
																),
							'Description'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Description',
																	'sortable' => false
																),
						);
		$this->view->fields = $fields;
	}
}
