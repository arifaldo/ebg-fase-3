<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class airlines_SuggestiondetailController extends airlines_Model_Airlines{

  public function indexAction()
  { 

    $this->_helper->layout()->setLayout('popup');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;  


    $AESMYSQL = new Crypt_AESMYSQL();
    $change_id = $AESMYSQL->decrypt($this->_getParam('changes_id'), $password);
    $change_id = (Zend_Validate::is($change_id,'Digits'))? $change_id : 0;
    
    $this->view->suggestionType = $this->_suggestType;
    
    if($change_id)
    {
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID'))
                             ->where('CHANGES_ID='.$this->_db->quote($change_id))
                             ->where("CHANGES_FLAG='F'")
                             ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
//                             ->where('UPPER(MODULE)='.$this->_db->quote(strtoupper($this->_request->getModuleName())));
      $result = $this->_db->fetchOne($select);
      
      if(empty($result))  $this->_redirect('/notification/invalid/index');
      
      
      if($result)
      {
        //content send to view
        $select = $this->_db->select()
                               ->from(array('T'=>'TEMP_AIRLINES_SETTINGS'))
      	                       ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('G.CHANGES_ID as G_CHANGES_ID','G.CHANGES_TYPE as G_CHANGES_TYPE','G.CREATED as G_CREATED','G.CREATED_BY as G_CREATED_BY','G.CHANGES_STATUS as G_CHANGES_STATUS','G.READ_STATUS as G_READ_STATUS'))
      	                       ->where('T.CHANGES_ID = ?', $change_id);

      	$resultdata = $this->_db->fetchRow($select);
      	
      	// if($result['CHANGES_ID'])
      	// {
      	    //----------------------------------------------TEMP DATA--------------------------------------------------------------
	  		$this->view->changes_id     = $resultdata['G_CHANGES_ID'];
	  		$this->view->changes_type   = $resultdata['G_CHANGES_TYPE'];
	  		$this->view->changes_status = $resultdata['G_CHANGES_STATUS'];
	  		$this->view->read_status    = $resultdata['G_READ_STATUS'];
	  		$this->view->created        = $resultdata['G_CREATED'];
	  		$this->view->created_by     = $resultdata['G_CREATED_BY'];
      	
      	
  	    $cust_id = strtoupper($resultdata['CUST_ID']);
  	    $airlineCode = strtoupper($resultdata['AIRLINE_CODE']);
  	   
  	    if($cust_id)
   	    {
   	  	  $select = $this->_db->select()
                               ->from('M_CUSTOMER',array('CUST_NAME'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
          $this->view->cust_name = $this->_db->fetchOne($select);
  	    }  

        if($airlineCode)
        {
          $select = $this->_db->select()
                               ->from('M_AIRLINES',array('AIRLINE_NAME'))
                               ->where('AIRLINE_CODE='.$this->_db->quote((string)$airlineCode));

          $this->view->airline_name = $this->_db->fetchOne($select).' ('.$airlineCode.')';
        }
  	
  	   $this->view->balance_cycle      = $resultdata['BALANCE_CYCLE'];
  	   $this->view->min_balance        = $resultdata['MIN_BALANCE'];
   	   $this->view->warn1              = $resultdata['WARN_LEVEL1'];
       $this->view->warn2              = $resultdata['WARN_LEVEL2'];
       $this->view->amount_topup       = $resultdata['AMOUNT_TOPUP'];
       $transaction_info   = json_decode($resultdata['TRANSACTION_INFO'], 1);

    
    	 $this->view->airline_created     = $resultdata['CREATED_DATE'];
    	 $this->view->airline_createdby   = $resultdata['CREATED_BY'];
    	 $this->view->airline_suggested   = $resultdata['UPDATED_DATE'];
    	 $this->view->airline_suggestedby = $resultdata['UPDATED_BY'];
    	 $this->view->airline_updated     = $resultdata['APPROVED_DATE'];
    	 $this->view->airline_updatedby   = $resultdata['APPROVED_BY'];
        
        	
        	
        //----------------------------------------------MASTER DATA--------------------------------------------------------------
        $m_resultdata = $this->getAirlinesSettings($airlineCode);

        $m_resultdata = $m_resultdata[0];
      	   
      	$this->view->m_balance_cycle      = $m_resultdata['BALANCE_CYCLE'];
        $this->view->m_min_balance        = $m_resultdata['MIN_BALANCE'];
        $this->view->m_warn1              = $m_resultdata['WARN_LEVEL1'];
        $this->view->m_warn2              = $m_resultdata['WARN_LEVEL2'];
        $this->view->m_amount_topup       = $m_resultdata['AMOUNT_TOPUP'];
        $m_transaction_info   = json_decode($m_resultdata['TRANSACTION_INFO'], 1);
    
        $this->view->m_airline_created     = $m_resultdata['CREATED_DATE'];
        $this->view->m_airline_createdby   = $m_resultdata['CREATED_BY'];
        $this->view->m_airline_suggested   = $m_resultdata['UPDATED_DATE'];
        $this->view->m_airline_suggestedby = $m_resultdata['UPDATED_BY'];
        $this->view->m_airline_updated     = $m_resultdata['APPROVED_DATE'];
        $this->view->m_airline_updatedby   = $m_resultdata['APPROVED_BY'];
        //---------------------------------------------END MASTER DATA--------------------------------------------------------------

         if ($this->view->changes_type != 'L') {

          $divided_type = $transaction_info[0]['type'];

          if ($divided_type == 'p') {
            $divided_type_text = 'Percent';
          }
          else{
            $divided_type_text = 'Fix Amount(IDR)';
          }

          $transaction_info_text = '';
          foreach ($transaction_info as $key => $value) {

            if ($divided_type == 'f') {
              $dividedVal = 'IDR '.Application_Helper_General::displayMoney($value['value']);
            }
            else{
              $dividedVal = $value['value'];
            }

            $transaction_info_text .= '<li>'.$this->language->_('From source account: ').$this->getAccountAlias($value['bankcode'], $value['sourceaccount']).$this->language->_(' to travel agent id: ').$value['travelagentid'].$this->language->_(' of ').$dividedVal.'</li>';
          }

          //-------------------------------------------------------------------------------------------------------------------------------------------------------

          $m_divided_type = $m_transaction_info[0]['type'];

          if ($m_divided_type == 'p') {
            $m_divided_type_text = 'Percent';
          }
          else{
            $m_divided_type_text = 'Fix Amount(IDR)';
          }

          $m_transaction_info_text = '';
          foreach ($m_transaction_info as $key => $value) {

            if ($m_divided_type == 'f') {
              $m_dividedVal = 'IDR '.Application_Helper_General::displayMoney($value['value']);
            }
            else{
              $m_dividedVal = $value['value'];
            }

            $m_transaction_info_text .= '<li>'.$this->language->_('From source account: ').$this->getAccountAlias($value['bankcode'], $value['sourceaccount']).$this->language->_(' to travel agent id: ').$value['travelagentid'].$this->language->_(' of ').$m_dividedVal.'</li>';
          }
        }
        else{
          $transaction_info_text = '';
        }

        $this->view->transaction_info_text   = $transaction_info_text;
        $this->view->divided_type_text       = $divided_type_text;
        $this->view->m_transaction_info_text = $m_transaction_info_text;
        $this->view->m_divided_type_text     = $m_divided_type_text;
        	
        $this->view->status_type = $this->_masterglobalstatus;
      	  
       	// }
       	// else{ $change_id = 0; }
      }
      else{ $change_id = 0; } 
    }
    
    
    
    
    
    if(!$change_id)
    {
      $error_remark = $this->language->_('Changes Id is not found');
      $this->_redirect($this->_backURL);
      $this->_redirect('/popuperror/index/index'); 
    }
    
    //insert log
	try 
	{
	  $this->_db->beginTransaction();
	  $fulldesc = 'CHANGES_ID:'.$change_id;
	  if(!$this->_request->isPost()){
	  Application_Helper_General::writeLog('VBTU','View Airlines Balance Monitoring');
	  }
	  
	  $this->_db->commit();
	}
    catch(Exception $e){
 	  $this->_db->rollBack();
	}

    $this->view->changes_id = $change_id;
  }
}