<?php

require_once 'Zend/Controller/Action.php';
ini_set("display_errors","Off");
class binsetup_ViewController extends binsetup_Model_Binsetup
{

    public function indexAction() 
    {
       $cust_id = strtoupper($this->_getParam('cust_id'));
       $pdf       = $this->_getParam('pdf');
       $cust_bin = strtoupper($this->_getParam('cust_bin'));
       $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
       //pengaturan url untuk button back
	   $this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$cust_id); 
       
       if($cust_id && $cust_bin)
       {
          $resultdata = $this->getCustomer($cust_id,$cust_bin);
         
          if($resultdata['CUST_ID'])
          {
      	        $this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
       			$this->view->cust_name    = $resultdata['CUST_NAME'];
              if($resultdata['CUST_BIN_STATUS']=='1'){
                $resultdata['CUST_BIN_STATUS'] = 'Approved';
              }else if($resultdata['CUST_BIN_STATUS'] == '2'){
                $resultdata['CUST_BIN_STATUS'] = 'Suppend';
              }else{
                $resultdata['CUST_BIN_STATUS'] = 'Deleted';
              }
      		    $this->view->cust_bin_status   = strtoupper($resultdata['CUST_BIN_STATUS']);
      		    $this->view->cust_bin   = strtoupper($resultdata['CUST_BIN']);
      		    
      		    
      		    $this->view->cust_created     = $resultdata['BIN_CREATED'];
      		    $this->view->cust_createdby   = $resultdata['BIN_CREATEDBY'];
      		    $this->view->cust_suggested   = $resultdata['BIN_SUGGESTED'];
      		    $this->view->cust_suggestedby = $resultdata['BIN_SUGGESTEDBY'];
      		    $this->view->cust_updated     = $resultdata['BIN_UPDATED'];
      		    $this->view->cust_updatedby   = $resultdata['BIN_UPDATEDBY'];

              $this->view->bank   = $resultdata['BANK_NAME'];
              $this->view->branch   = $resultdata['BRANCH_NAME'];
              $this->view->acc_no   = $resultdata['ACCT_NO'];
              $this->view->acc_name   = $resultdata['ACCT_NAME'];
              $this->view->app_date   = $resultdata['APPROVED_DATE'];
              $this->view->ccy   = $resultdata['CCY'];
              $this->view->alias_name   = $resultdata['ACCT_ALIAS_NAME'];
              $this->view->bin_name   = $resultdata['BIN_NAME'];
			                       
//      		 if(!$result){   
//			 	$result = $this->getTempCustomerId($cust_id,$cust_bin,false);
//      		 }
//      		 else{
//      		 	$result = $this->getTempCustomerId($cust_id,$cust_bin,true);
//      		 }

      		 $result = $this->getTempCustomerId($cust_id,$cust_bin,TRUE);
      		 
      		 if($result){
      		 	$resultAll = $this->getTempCustomerId($cust_id,$cust_bin,TRUE);
      		 }
      		 else{
      		 	$resultAll = $this->getTempCustomerId($cust_id,$cust_bin,false);
      		 }
      		 
      	     if($resultAll)  $temp = 0; 
      	     else  $temp = 1; 
      	     
      	      
             $this->view->cust_temp = $temp; 
           }
           else $cust_id = null; 
       }// End if cust_id == true
      
        if(!$cust_id)
        {
           $error_remark = 'Invalid Cust ID';
		    //insert log
			try
			{
			   $this->_db->beginTransaction();
			   
			   Application_Helper_General::writeLog('BNUD','View Detail Customer BIN [Invalid Cust ID]');
			   
			   $this->_db->commit();
			}
			catch(Exception $e) 
			{
			   $this->_db->rollBack();
			}
           $this->_helper->getHelper('FlashMessenger')->addMessage('F');
           $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
           $this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));	
         }
    
        $this->view->cust_id = $cust_id;
        $this->view->status_type = $this->_masterglobalstatus;
        $this->view->modulename = $this->_request->getModuleName();
    
          
      if($this->_request->getParam('print') == 1){
      
        // $this->_helper->download->pdf($header,$data,null,'List View Payment');  
        // Application_Helper_General::writeLog('DARC','Export PDF View Payment');
        $this->_forward('printtrxbin', 'index', 'widget', array('param' => $resultdata, 'data_caption' => 'Bin Customer'));
      } 
        
        //insert log
        try
        {
	       $this->_db->beginTransaction();
	       
	       Application_Helper_General::writeLog('BNUD','View Customer BIN Detail ['.$cust_id.']');
	       
           $this->_db->commit();
	    }
	    catch(Exception $e) 
	    {
	       $this->_db->rollBack();
	    }
	   
   }	
  
  
}




