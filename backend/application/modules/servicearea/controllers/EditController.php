<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class servicearea_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
		$model = new servicearea_Model_Servicearea();

	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

	    $this->view->report_msg = array();

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

		$area_id = $this->_getParam('area_id');
		$this->view->area_id        = $area_id;
		$area_id = (Zend_Validate::is($area_id,'Digits'))? $area_id : null;

		if($area_id)
		{
			$temp = $model->getDetail($area_id);
			if(empty($temp))
			{
				$this->_redirect('/servicearea');
			}

			$resultdata = $temp;
			if($resultdata)
			{
				$this->view->area_name    = $resultdata['AREA_NAME'];
				$this->view->area_id      = $resultdata['AREA_ID'];
				$this->view->area_name_old = $resultdata['AREA_NAME'];


				if($this->_request->isPost())
				{
					$filters = array(
										'area_name'     => array('StringTrim','StripTags'),
									);

					$AREA_NAME = "AREA_NAME <> ".$this->_db->quote($resultdata['AREA_NAME']);
					$validators = array('area_name'  => array('NotEmpty',
															//new Zend_Validate_Alnum(true),
															new Zend_Validate_StringLength(array('min'=>1,'max'=>15)),
															array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'M_SERVICE_AREA',
																						'field'=>'AREA_NAME',
																						'exclude'=>$AREA_NAME
																					)
																		),
															'messages' => array(
																			   $this->language->_('Can not be empty'),
																			   //$this->language->_('Invalid Format'),
																				'Maksimum panjang karakter adalah 15 karakter',
																				'Area layanan sudah didaftarkan',
																				)
																		),
										   );

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{
						$content = array(
										'AREA_NAME'  => $zf_filter_input->area_name,
										'LAST_UPDATED' => new Zend_Db_Expr('GETDATA()'),
										'LAST_UPDATEDBY' => $this->_userIdLogin,
										);

						try
						{

							//-----insert--------------
							$this->_db->beginTransaction();

							$model->updateData($area_id,$content);

							$this->_db->commit();

							$this->fillParam($zf_filter_input);
							$this->_helper->getHelper('FlashMessenger')->addMessage('S');
							$this->_helper->getHelper('FlashMessenger')->addMessage('Update Success');
							$this->_redirect('/servicearea/edit/index/area_id/'.$area_id);
						}
						catch(Exception $e)
						{
							//rollback changes
							$this->_db->rollBack();

							$this->fillParam($zf_filter_input);

							$errorMsg = 'exception';
							$this->view->error_msg = $errorMsg;
						}
					}
					else
					{
						$this->view->error = true;
						$this->fillParam($zf_filter_input);
						$error = $zf_filter_input->getMessages();

						//format error utk ditampilkan di view html
						foreach($error as $keyRoot => $rowError)
						{
						   foreach($rowError as $errorString)
						   {
								$errID = 'error_'.$keyRoot;
								$this->view->$errID = $errorString;
						   }
						}
					}
				}
			}
			else{
				$this->view->error_msg = "Data area layanan tidak ditemukan";
			}
		}
		else
		{
		   $this->view->error_msg = "Data area layanan tidak ditemukan";
		}
		//Application_Helper_General::writeLog('OBUD','Edit online bank table,Bank ID : '.$bank_id);
	}


    private function fillParam($zf_filter_input)
	{
		$this->view->area_name_old = $this->_getParam('area_name_old');
		$this->view->area_name = ($zf_filter_input->isValid('area_name')) ? $zf_filter_input->area_name : $this->_getParam('area_name');
	}



}