<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';
require_once "Service/Account.php";
require_once 'General/Account.php';

class directdebit_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	protected $_payType;
	public function initController()
	{
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
		//$this->_payType = "'".$this->_beneftype["code"]["domestic"]."' , '".$this->_beneftype["code"]["online"]."'";
		//$this->_payType = "'".$this->_beneftype["code"]["remittance"]."'";
		
		
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		// $model = new predefinedbeneficiary_Model_Predefinedbeneficiary();

		$aliasname = $this->language->_('Alias Name');
    	$beneficiaryaccount = $this->language->_('Destination Name');
    	
    	$nrc = $this->language->_('NRC');
  		$phone = $this->language->_('Phone');
  		$ccy = $this->language->_('CCY');
  		$favorite = $this->language->_('Favorite');
  		$bankcode = $this->language->_('Bank Code');
  		$status = $this->language->_('Status');
  		$bankname = $this->language->_('Bank Name');
  		$delete = $this->language->_('Action');
  		$address = $this->language->_('Address');
  		
  		
  		
		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANKTABLE'));
		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$databank 					= $this->_db->fetchAll($selectbank);
		// print_r($databank);die;
		$bankArr = array();
		foreach ($databank as $key => $value) {
			// print_r($value);die;
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}
		// print_r($bankArr);die;
		$this->view->DEBIT_BANKarr  = $bankArr;
  		// print_r($this->_request->getParams());die;

		if($this->_getParam('error') == '1'){
			$sessionNamespace = new Zend_Session_Namespace('directdebit');
			$error = $sessionNamespace->error;
			// print_r($sessionNamespace);die;
			$this->view->error = $error;
		}

		if($this->_getParam('confirm') == '1'){
			$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
			$content = $sessionNamespace->content;
			$this->view->confirmPage = 1;
			$this->view->COMPANY_ID = $content['COMP_ID'];
			$this->view->COMPANY_NAME = $content['COMP_NAME'];
			$this->view->COMPANY_TYPE = $content['COMP_TYPE'];
			
			$bankdata = $this->_db->select()
					 ->from(array('C' => 'M_BANKTABLE'));
			$bankdata->where("BANK_CODE = ".$this->_db->quote($content['DEBIT_BANK']));
			$databank 					= $this->_db->fetchAll($bankdata);
			
			$this->view->DEBIT_BANK_NAME = $databank['0']['BANK_NAME'];
			$this->view->DEBIT_BANK = $content['DEBIT_BANK'];
			$this->view->DEBIT_ACCOUNT = $content['DEBIT_ACCT'];
			$this->view->ACCT_NAME = $content['ACCT_NAME'];
		}

		$fields1 = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $aliasname,
											   'sortable' => true),*/
						// 'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
						// 					   'label' => $beneficiaryaccount,
						// 					   'sortable' => true),
						// 'comp_id'  => array('field' => 'BENEFICIARY_NAME',
						// 					   'label' => $this->language->_('Company ID'),
						// 					   'sortable' => true),
						'comp_name'  => array('field' => 'company',
											   'label' => $this->language->_('Credited to Company'),
											   'sortable' => true),
						'client_reff'  => array('field' => 'CLIENT_REFF',
											   'label' => $this->language->_('Client Reff'),
											   'sortable' => true),
						'client_name'  => array('field' => 'CLIENT_NAME',
											   'label' => $this->language->_('Client Name'),
											   'sortable' => true),
						// 'doc_reff'  => array('field' => 'DOCUMENT_REFF',
						// 					   'label' => $this->language->_('Document Reff'),
						// 					   'sortable' => true),
						'debit_acct'   => array('field'    => 'DEBIT_ACCT',
											  'label'    => $this->language->_('Debited Account'),
											  'sortable' => false),						
						'last_suggest'   => array('field'    => 'DIR_SUGESTEDBY',
											  'label'    => $this->language->_('Last Suggested'),
											  'sortable' => false),
						'last_approved'   => array('field'    => 'DIR_APPROVEDBY',
											  'label'    => $this->language->_('Last Approved'),
											  'sortable' => false),
						'status'   => array('field'    => 'DIR_STATUS',
											  'label'    => $this->language->_('Status'),
											  'sortable' => false),
						// 'account_name'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
						// 					  'label'    => $this->language->_('Account Name'),
						// 					  'sortable' => false),
				);

		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $aliasname,
											   'sortable' => true),*/
						// 'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
						// 					   'label' => $beneficiaryaccount,
						// 					   'sortable' => true),
						// 'comp_id'  => array('field' => 'BENEFICIARY_NAME',
						// 					   'label' => $this->language->_('Company ID'),
						// 					   'sortable' => true),
						'comp_name'  => array('field' => 'company',
											   'label' => $this->language->_('Credited to Company'),
											   'sortable' => true),
						'client_reff'  => array('field' => 'CLIENT_REFF',
											   'label' => $this->language->_('Client Reff'),
											   'sortable' => true),
						'client_name'  => array('field' => 'CLIENT_NAME',
											   'label' => $this->language->_('Client Name'),
											   'sortable' => true),
						// 'doc_reff'  => array('field' => 'DOCUMENT_REFF',
						// 					   'label' => $this->language->_('Document Reff'),
						// 					   'sortable' => true),
						'debit_acct'   => array('field'    => 'DEBIT_ACCT',
											  'label'    => $this->language->_('Debited Account'),
											  'sortable' => false),						
						'last_suggest'   => array('field'    => 'DIR_SUGESTEDBY',
											  'label'    => $this->language->_('Last Suggested'),
											  'sortable' => false),
						'last_approved'   => array('field'    => 'DIR_APPROVEDBY',
											  'label'    => $this->language->_('Last Approved'),
											  'sortable' => false),
						// 'last_suggest'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
						// 					  'label'    => $this->language->_('Last Suggested'),
						// 					  'sortable' => false),
						// 'last_approved'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
						// 					  'label'    => $this->language->_('Last Approved'),
						// 					  'sortable' => false),
						// 'account_name'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
						// 					  'label'    => $this->language->_('Account Name'),
						// 					  'sortable' => false),
						'status'   => array('field'    => 'DIR_STATUS',
											  'label'    => $this->language->_('Status'),
											  'sortable' => false),
				);

		$filterlist = array('COMP_ID','DEBIT_BANK','COMP_NAME','DEBIT_ACCT');
		
		$this->view->filterlist = $filterlist;

		$selectcomp = $this->_db->select()
                            ->from('M_CUSTOMER', array('value' => 'CUST_ID','CUST_NAME','CUST_TYPE'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);

        $this->view->custarr = json_encode($tempColumn);
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'COMP_ID'    	=> array('StripTags'),
	                       	'COMP_NAME'  		=> array('StripTags','StringTrim','StringToUpper'),
							'DEBIT_BANK'    => array('StripTags'),
							'DEBIT_ACCT' 		=> array('StripTags','StringTrim','StringToUpper'),
		);

		$dataParam = array('COMP_ID','DEBIT_BANK','COMP_NAME','DEBIT_ACCT');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
			}
		}

		$validators = array(
						'COMP_ID' 	=> array(),
						'COMP_NAME' 	=> array(),
						'DEBIT_BANK' 	=> array(),	
						'DEBIT_ACCT' 	=> array(),		
						);
		
		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue);

		$filter = $this->_getParam('filter');

		$alpha = $this->_getParam('alpha');

		$this->view->currentPage = $page;
		$this->view->sortBy = $param['sortBy'] = $sortBy;
		$this->view->sortDir = $param['sortDir'] = $sortDir;
		$this->view->alpha = $alpha;

		if($filter == TRUE)
		{
			$fcompid = $zf_filter->getEscaped('COMP_ID');
			$fcompname = $zf_filter->getEscaped('COMP_NAME');
			$fdebitbank = $zf_filter->getEscaped('DEBIT_BANK');
			$fdebitacct = $zf_filter->getEscaped('DEBIT_ACCT');

			if($fcompid) $param['fcompid'] = $fcompid;
	        if($fcompname) $param['fcompname'] = $fcompname;
	        if($fdebitbank) $param['fdebitbank'] = $fdebitbank;
	        if($fdebitacct) $param['fdebitacct'] = $fdebitacct;

			$this->view->custid = $fcompid;
			$this->view->custname = $fcompname;
			$this->view->debit_bank = $fdebitbank;
			$this->view->debit_acct = $fdebitacct;


			$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('benef_acct');
			$fName = $zf_filter->getEscaped('benef_name');
			$payType = $zf_filter->getEscaped('benef_type');

			if($fAlias) $param['fAlias'] = $fAlias;
	        if($fAcct) $param['fAcct'] = $fAcct;
	        if($fName) $param['fName'] = $fName;
	        if($payType) $param['payType'] = $payType;

			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->benef_type = $payType;
		}

		$param['user_id'] = $this->_userIdLogin;
		$param['cust_id'] = $this->_custIdLogin;
		$param['payType'] = '4';
		$param['beneLinkage'] = $this->view->hasPrivilege('BLBU');
		/*if(empty($payType))
			$param['payType'] = $this->_payType;*/
		if($alpha) $param['alpha'] = $alpha;

		$select = $this->_db->select()
					 ->from(array('C' => 'T_DIRECTDEBIT'),array('*','company'	=> new Zend_Db_Expr("CONCAT(COMP_NAME , ' (' , COMP_ID , ')  ' )")))
					 ->joinleft(array('X'=>'M_DOMESTIC_BANK_TABLE'), 'X.SWIFT_CODE = C.DEBIT_BANK',array('BANK_NAME'))
					 ->where('DIR_TYPE = 1')
					//  ->where('DIR_STATUS IN (1,0)');
					 ->order('DIR_APPROVED DESC');

					 // echo $select;die;

		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		// $databank 					= $this->_db->fetchAll($selectbank);
		if($fcompid)              $select->where('UPPER(COMP_ID)='.$this->_db->quote(strtoupper($fcompid)));
		if($fcompname)            $select->where('UPPER(COMP_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fcompname).'%'));
		if($fdebitbank)            $select->where('UPPER(DEBIT_BANK)='.$this->_db->quote(strtoupper($fdebitbank)));
		if($fdebitacct)            $select->where('UPPER(DEBIT_ACCT) LIKE '.$this->_db->quote('%'.strtoupper($fdebitacct).'%'));
		// $select   = $model->getBeneficiaries($param);
		// print_r($select);die;
		// echo $select;die;
		
		
		$this->paging($select);
		$this->view->payType = array(''=> ' -- Please Select -- ',$this->_beneftype["code"]["domestic"]=>$this->_beneftype["desc"]["domestic"],$this->_beneftype["code"]["online"]=>$this->_beneftype["desc"]["online"]);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		$conf = Zend_Registry::get('config');
		$bankDebit = $conf['app']['bankname'];
		$this->view->DEBIT_BANK = $bankDebit;
	    Application_Helper_General::writeLog('BLBA','View Destination Account');

	    $arr = $this->_db->fetchAll($select);

	    if($this->_request->getParam('print') == 1){

	    	// echo "<pre>";
	    	// var_dump($arr);
	    	// die();

	    	foreach($arr as $key=>$row)
			{
				$arr[$key]['company'] 	 = $row['company'];
				$arr[$key]['COMP_TYPE']  = $row['COMP_TYPE'];
				$arr[$key]['BANK_NAME']  = $row['BANK_NAME'];
				$arr[$key]['DEBIT_ACCT'] = $row['DEBIT_ACCT'].' - '.$row['ACCT_NAME'];
				$arr[$key]['DIR_CCY'] 	 = $row['DIR_CCY'];

				if ($row['DIR_STATUS']==1) {
			      $arr[$key]['DIR_STATUS'] = "Approved";
			    }elseif( $row['DIR_STATUS']==2){
			      $arr[$key]['DIR_STATUS'] = "Rejected";
			    }elseif( $row['DIR_STATUS']==0){
			      $arr[$key]['DIR_STATUS'] = "Waiting Approval";
			    }

			   	$arr[$key]['DIR_SUGESTEDBY'] = $row['DIR_SUGESTEDBY'].' ('.$row['DIR_SUGESTED'].')';
			   	$arr[$key]['DIR_APPROVEDBY'] = $row['DIR_APPROVEDBY'].' ('.$row['DIR_APPROVED'].')';
				
			}

			$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Direct Debit', 'data_header' => $fields1));
		}

		$delete 	= $this->_getParam('delete');
		//$filter_clear 	= $zf_filter->getEscaped('clearfilter');
		
		
		if($delete)
		{
			$direct_id = $this->_getParam('req_id');

			if(count($direct_id) > 0)
			{
				foreach($direct_id as $key=>$row){						
					try 
					{
						$this->_db->beginTransaction();		

						
						//$this->db->delete('T_DIRECTDEBIT',$this->db->quoteInto('DIRECT_ID = ?',$row));
						$dataapprove =  array(					
												'DIR_STATUS' => '4'
											);							
						
						$where = array('DIRECT_ID = ?' => $row);
						$result = $this->_db->update('T_DIRECTDEBIT',$dataapprove,$where);

						Application_Helper_General::writeLog('APLO','Delete Direct Debit '.$row);	
						
						$this->_db->commit();
						var_dump($delete);die;							
						
					}
					catch(Exception $e) 
					{
						$this->_db->rollBack();
						
					}
			    }     
			    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
				$this->_redirect('/notification/success');	
			} else {
				$this->view->error = TRUE;
				$this->view->report_msg = $this->language->_('Error: Please select a direct debit');
			}

			
		}

	    if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
	        $this->view->wherecol     = $wherecol;
	        $this->view->whereval     = $whereval;
	     // print_r($whereval);die;
	    }


	}
		
	public function addAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->bankpop = 'disabled';


//		$payment					= new payment_Model_Payment();
		// $modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		
		$attahmentDestination 	= UPLOAD_PATH . '/document/help/';		
		$errorRemark 			= null;
		$adapter 				= new Zend_File_Transfer_Adapter_Http();

		$settings 			= new Application_Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBA');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();

		//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
		
		$selectcomp = $this->_db->select()
                            ->from('M_CUSTOMER', array('value' => 'CUST_ID','CUST_NAME','CUST_TYPE'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);

        $this->view->custarr = json_encode($tempColumn);
        // $data = 
        // echo $data;die;

		//validasi token -- begin
		// $select3 = $this->_db->select()
		// 			 ->from(array('C' => 'M_USER'));
		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		// $data2 					= $this->_db->fetchRow($select3);
		

		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANKTABLE'));
		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$databank 					= $this->_db->fetchAll($selectbank);
		// print_r($databank);die;
		$bankArr = array();
		foreach ($databank as $key => $value) {
			// print_r($value);die;
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		$this->view->DEBIT_BANKarr  = $bankArr;

		if($this->_request->isPost() )
		{	

			$params = $this->_request->getParams();

			if($this->_getParam('import') != '1')	//import by csv
			{

				$adapter = new Zend_File_Transfer_Adapter_Http();
				$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));
                
                if ($adapter->isValid ())
                {
       				
                    $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                    $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                    $adapter->addFilter ( 'Rename',$newFileName  );

                    if ($adapter->receive ())
                    {

                        //PARSING CSV HERE
                        $csvData = $this->parseCSV($newFileName);
                        $csvData2 = $this->parseCSV($newFileName);

												//check format
												// foreach ( $csvData as $columnlist )
												// {
												// 	if(count($columnlist) != 5)
												// 	{	
												// 		// var_dump('Wrong File Format');die;
												// 		$error = true;
												// 		$error = 'Wrong File Format';
												// 		$sessionNamespace = new Zend_Session_Namespace('directdebit');
												// 		$sessionNamespace->error = $error;
												// 		$this->_redirect('/directdebit/index/index/error/1');
														
												// 	}

												// }

												//check validate account
												// foreach($csvData as $key => $val){
												// 	if($key == 0){
												// 		continue;
												// 	}
												// 	$ccy_id_num = Application_Helper_General::getCurrNum('IDR');
												// 	$account    = new Service_Account($val['1'],$ccy_id_num);
												// 	$result     = $account->accountInquiry();
													
												// 	if($result['ResponseCode'] == '00'){
												// 		$csvData[$key]['cif'] = $result['Cif'];
												// 		$csvData[$key]['acctname'] = $result['AccountName'];
												// 		$csvData[$key]['type'] = $result['ProductType'];
												// 		$csvData[$key]['productPlan'] = $result['productPlan'];
												// 		$csvData[$key]['planCode'] = $result['planCode'];
														
												// 	}else{
												// 		$newerror[1] = 'AD01';
														// var_dump($val['1']);die;
														// $error = true;
														// $error = 'Invalid Account';
														// $sessionNamespace = new Zend_Session_Namespace('directdebit');
														// $sessionNamespace->error = $error;
														// $this->_redirect('/directdebit/index/index/error/1');
													// }
													
												// }

												// echo "<pre>";
												// var_dump($result);

												// validate duplicate clientreff
												$listClient = array();
												$duplicateList = array();
												foreach($csvData as $key => $value){
													foreach($listClient as $ll => $vv){
														if($vv['cust_id'] == $value['0'] && $vv['clientreff'] == $value['2']){
															$duplicate = array(
																'id' => $vv['cust_id'],
																'reff' => $vv['clientreff']
															);
															array_push($duplicateList, $duplicate);
														}
													}
													$listArray = array(
														'cust_id' => $value['0'],
														'clientreff' => $value['2']
													);
													
													array_push($listClient, $listArray);
												}

												// echo "<pre>";
												// var_dump($listClient);
												// var_dump($duplicateList);
												
                        
                        @unlink($newFileName);
                        //end

                        $totalRecords2 = count($csvData2);
                        $totaldata = 0;
                        if($totalRecords2)
                            {
                                for ($a= 1; $a<$totalRecords2; $a++ ){
                                $totaldata++;
                            }
                        }

												
                        // var_dump($csvData['1']); die;
                        if(!empty($csvData['1'])){
                        	$totalrow = 1+$totaldata;
													//  var_dump($totalrow);
                        	// try 
							// {				
								$dataConfirm = array();							
								for ($i=1; $i < $totalrow; $i++) {
									
									// validate column
									if(empty($csvData[$i]['0'])){
										$newerror['AD01'] = 'Company code must be filled';
									}elseif(empty($csvData[$i]['1'])){
										$newerror['AD02'] = 'Debited account must be filled';
									}elseif(empty($csvData[$i]['2'])){
										$newerror['AD03'] = 'Client reference must be filled';
									}elseif(empty($csvData[$i]['3'])){
										$newerror['AD04'] = 'Client name must be filled';
									}elseif(empty($csvData[$i]['4'])){
										$newerror['AD05'] = 'Document reference must be filled';
									}

									// validate duplicate client reff
									foreach($duplicateList as $key => $vv){
										if($vv['id'] == $csvData[$i]['0'] && $vv['reff'] == $csvData[$i]['2']){
											$newerror['AD09'] = 'Duplicate client reference';
											break;
										}
									}

									// if(!empty($csvData[$i]['0']) && !empty($csvData[$i]['1']) && !empty($csvData[$i]['2']) && !empty($csvData[$i]['3']) && !empty($csvData[$i]['4'])){
										// $this->_db->beginTransaction();
										// if($i=='1'){
										// 	$info = $this->language->_('directdebit');
										// 	$info2 = $this->language->_('Import new direct debit data');
											// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'T_DIRECTDEBIT','TEMP_DIRECTDEBIT',$csvData['3']['1'],'',$csvData['3']['1']);
										// }

										// validate account number
										$ccy_id_num = Application_Helper_General::getCurrNum('IDR');
										$account    = new Service_Account($csvData[$i]['1'],$ccy_id_num);
										$result     = $account->accountInquiry();
										
										if($result['ResponseCode'] == '00'){
											$csvData['acctname'] = $result['AccountName'];	
										}else{
											$newerror['AD06'] = 'Invalid Account';
										}

										$compdata = $this->_db->select()
										->from(array('C' => 'M_CUSTOMER'));
										$compdata->where("CUST_ID = ?",$csvData[$i]['0']);
										$compdata 					= $this->_db->fetchRow($compdata);

										$COMP_ID = $compdata['CUST_ID'];
										$COMP_NAME = $compdata['CUST_NAME'];
										$COMP_TYPE = $compdata['COMP_TYPE'];
										// print_r($compdata);die;
										if(empty($compdata)){
											$newerror['AD07'] = 'Company code not found';
											$COMP_ID = $csvData[$i]['0'];
											$COMP_NAME = '-';
											$COMP_TYPE = '-';
											// print_r($csvData[$i]['0']);die;
											// $error = 'invalid customer code';
											// $this->_db->rollBack();
											// $sessionNamespace = new Zend_Session_Namespace('directdebit');
											// $sessionNamespace->error = $error;
											// $this->_redirect('/directdebit/index/index/error/1');
											// break;

										}

										$docId = $this->_db->select()
										->from(array('C' => 'T_FILE_SUBMIT'));
										$docId->where("DOCUMENT_NUMBER = ?",$csvData[$i]['4']);
										$docId 					= $this->_db->fetchRow($docId);
										// print_r($docId);die;
										if(empty($docId)){
											$newerror['AD08'] = 'Document reference not found';
											// print_r($csvData[$i]['0']);die;
											// $error = 'Document Reff '. $csvData[$i]['4'] . ' not Found';
											// $this->_db->rollBack();
											// $sessionNamespace = new Zend_Session_Namespace('directdebit');
											// $sessionNamespace->error = $error;
											// $this->_redirect('/directdebit/index/index/error/1');
											// break;

										}else{
											$fileid = $docId['FILE_ID'];
										}

										// $bankdata = $this->_db->select()
										// ->from(array('C' => 'M_DOMESTIC_BANK_TABLE'));
										// $bankdata->where("SWIFT_CODE = ?",(int)$csvData[$i]['1']);
										// $bankdata 					= $this->_db->fetchRow($bankdata);
										// // print_r($bankdata);die;
										// if(empty($bankdata)){
										// 	print_r($csvData[$i]['1']);die;
										// 	$error = 'invalid bank code';
										// 	$this->_db->rollBack();
										// 	$sessionNamespace = new Zend_Session_Namespace('directdebit');
										// 	$sessionNamespace->error = $error;
										// 	$this->_redirect('/directdebit/index/index/error/1');
										// 	break;
										// }
									
										$inputarr = array(
											'COMP_ID' => $COMP_ID,
											'COMP_NAME' => $COMP_NAME,
											'COMP_TYPE' => $COMP_TYPE,
											// 'DEBIT_BANK'  => sprintf("%03s", $csvData[$i]['1']),
											'DEBIT_BANK'  => $params['debitbank'],
											'DOC_ID'  => $fileid,
											'DEBIT_ACCT'  => $csvData[$i]['1'],
											'CLIENT_REFF' => $csvData[$i]['2'],
											'CLIENT_NAME' => $csvData[$i]['3'],
											'DOCUMENT_REFF' => $csvData[$i]['4'],
											'ACCT_NAME' => $csvData['acctname'],
											'ERROR_CODE' => $newerror

										);
										$inputarr['DIR_SUGESTED'] = new Zend_Db_Expr('now()');
										$inputarr['DIR_SUGESTEDBY'] = $this->_userIdLogin;
										$inputarr['DIR_STATUS'] = "0"; //waiting approval
										// $inputarr['DIR_TYPE'] = "1";
										$inputarr['DIR_CCY'] = "IDR";
										// $inputarr['CHANGES_ID'] = $change_id;

										array_push($dataConfirm, $inputarr);
										$newerror = NULL; //return zero error for next line
										// var_dump($inputarr); die;
										// try{
											
											// $currentD = $this->_db->select()
											// 			->from(array('C' => 'TEMP_DIRECTDEBIT'));

											// $currentD->where("CLIENT_REFF = ?",$inputarr['CLIENT_REFF']);
											// $currentD = $this->_db->fetchRow($currentD);

											// if($currentD){
											// 	$where = array('CLIENT_REFF = ?' => $inputarr['CLIENT_REFF']);
											// 	$result = $this->_db->update('TEMP_DIRECTDEBIT',$inputarr,$where);
											// }else{

											// 	$this->_db->insert('TEMP_DIRECTDEBIT',$inputarr);
												
											// 	$rowUpdated = $this->_db->update(
											// 		'T_GLOBAL_CHANGES',
											// 		array(
											// 			'CHANGES_STATUS' => 'WA',
											// 			'DISPLAY_TABLENAME' => 'Auto Debit',
											// 			'CHANGES_INFORMATION' => 'New Auto Debit',
											// 			'COMPANY_CODE' => $inputarr['COMP_ID'],
											// 			'COMPANY_NAME' => $inputarr['COMP_NAME'],
											// 			'KEY_FIELD' => $inputarr['CLIENT_REFF'],
											// 			'KEY_VALUE' => $inputarr['CLIENT_NAME'],
											// 			'LASTUPDATED' => new Zend_Db_Expr("now()"),
											// 		),
											// 		$this->_db->quoteInto('CHANGES_ID = ?', $inputarr['CHANGES_ID'])
											// 	);
											// }

											// $rowHistory = $this->_db->insert(
											// 	'T_PSLIP_HISTORY',
											// 	array(
											// 		'PS_REASON' => 'Suggest New',
											// 		'USER_LOGIN' => $this->_userIdLogin,
											// 		'PS_NUMBER' => $inputarr['CLIENT_REFF'],
											// 		'DATE_TIME' => new Zend_Db_Expr("now()"),
											// 	)
											// );

											

										// }catch(Exception $e){
											// var_dump($e); die;
														// $this->_db->rollBack();
										// }
										
										
										// 	Application_Helper_General::writeLog('BADA','Add Direct Debit '.$compdata['CUST_ID']);
										// $this->_db->commit();
										


									// }
									// else{
									// 	$error = 'invalid data';
									// 	// die('1');
									// 	// $this->_db->rollBack();
									// 	$sessionNamespace = new Zend_Session_Namespace('directdebit');
									// 	$sessionNamespace->error = $error;
									// 	$this->_redirect('/directdebit/index/index/error/1');
									// 	break;

									// }
								}
								// $sessionNamespace = new Zend_Session_Namespace('directdebit');
                                // $sessionNamespace->content = $dataConfirm;
								// $this->_redirect('/directdebit/index/newconfirm');
                        	// }
							// catch(Exception $e) 
							// {
								// $this->_db->rollBack();
							// }

						}else{
							$error = 'empty data';
							// die('2');
							$sessionNamespace = new Zend_Session_Namespace('directdebit');
							$sessionNamespace->error = $error;
							$this->_redirect('/directdebit/index/index/error/1');

						}

						$sessionNamespace = new Zend_Session_Namespace('directdebit');
						$sessionNamespace->content = $dataConfirm;
						$this->_redirect('/directdebit/index/newconfirm');
						// $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
						// $this->_redirect('/notification/success');	
						// $this->_redirect('/notification/submited/index');
                    }                    
                }   
                else
				{						
					$error = $this->displayError($adapter->getMessages());	
					// var_dump($errors); die;
                	$sessionNamespace = new Zend_Session_Namespace('directdebit');
					$sessionNamespace->error = $error;
					$this->_redirect('/directdebit/index/index/error/1');

				}   


			}else{	//input by form
				
				// $fileExt 				= "pdf";
			
				// $sourceFileName = $adapter->getFileName();

				// if($sourceFileName == null)
				// {
				// 	$sourceFileName = null;
				// 	$fileType = null;
				// }
				// else
				// {
				// 	$sourceFileName = substr(basename($adapter->getFileName()), 0);
				// 	if($_FILES["document"]["type"])
				// 	{
				// 		$adapter->setDestination($attahmentDestination);
				// 		$maxFileSize 			= $this->getSetting('Fe_attachment_maxbyte');
				// 		$fileType = $adapter->getMimeType();
				// 		$size = $_FILES["document"]["size"];
				// 	}
				// 	else
				// 	{
				// 		$fileType = null;
				// 		$size = null;
				// 	}
				// }
				// $paramsName['sourceFileName'] 	= $sourceFileName;
				
				
				$filters = array(
					'COMPANY_ID' => array('StringTrim','StripTags'),
					'COMPANY_NAME' => array('StringTrim','StripTags'),
					// 'auto_monthlyfee' => array('StringTrim','StripTags'),
					'COMPANY_TYPE' => array('StringTrim','StripTags'),
					'DEBIT_BANK' => array('StringTrim','StripTags'),
					'DEBIT_ACCOUNT' => array('StringTrim','StripTags'),
					'ACCT_NAME' => array('StringTrim','StripTags'),
					'sourceFileName'		=> array('StringTrim')
					);
				
				$validators = array(
					
					'COMPANY_ID'      => array('NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
					'COMPANY_NAME'      => array('NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
					'COMPANY_TYPE'      => array('allowEmpty' => TRUE),
					'DEBIT_BANK'      => array('NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
					'DEBIT_ACCOUNT'      => array('NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
					'ACCT_NAME'      => array('NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty')
						)
					),
					'sourceFileName' => array('allowEmpty' => TRUE)
					
				);
			
				
				//$benefType = $this->_getParam('benefType');
				//setelah disamakan dgn FSD
				// $benefType = '4';//$this->_getParam('benefType'); //kalo dua ONLINE harus panggil fundtransferinquiry, kalo 1 skn rtgs langsung lewat
			
				$COMPANY_ID   = $this->_request->getParam('COMPANY_ID');
				$COMPANY_NAME   = $this->_request->getParam('COMPANY_NAME');
				$COMPANY_TYPE = $this->_request->getParam('COMPANY_TYPE');
				$DEBIT_BANK   = $this->_request->getParam('DEBIT_BANK');
				$DEBIT_ACCOUNT   = $this->_request->getParam('DEBIT_ACCOUNT');
				$ACCT_NAME = $this->_request->getParam('ACCT_NAME');
			
				// $this->view->CURR_CODE = $CURR_CODE;

					
			 //-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//	
				
				$fCOM_ID   = $filter->filter($COMPANY_ID  , "COMPANY_ID");
				$fCOM_NAME = $filter->filter($COMPANY_NAME , "COMPANY_NAME");
				$fCOM_TYPE = $filter->filter($COMPANY_TYPE, "COMPANY_TYPE");
				$fDEBIT_BANK   = $filter->filter($DEBIT_BANK  , "DEBIT_BANK");
				$fDEBIT_ACCT = $filter->filter($DEBIT_ACCOUNT , "DEBIT_ACCOUNT");
				$fACCT_NAME = $filter->filter($ACCT_NAME, "ACCT_NAME");
				
				
				$selectcheck = $this->_db->select()
				->from(array('C' => 'T_DIRECTDEBIT'));
				$selectcheck->where("COMP_ID = ?",$fCOM_ID);
				$selectcheck->where("DEBIT_BANK = ?",$fDEBIT_BANK);
				$selectcheck->where("DEBIT_ACCT = ?",$fDEBIT_ACCT);
				//  echo $selectcheck;die; 
				$checkbene 					= $this->_db->fetchRow($selectcheck);
				//validasi token -- end
				// $zf_filter_input =/ array();
				// if(empty($checkbene)){
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);	
					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);	
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);	
				// }
				$fileTypeMessage = explode('/',$fileType);
				$fileType =  $fileTypeMessage[1];
				$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
				$extensionValidator->setMessage("Extension file must be *.pdf");

				$maxFileSize = "1024000";
				$size = number_format($size);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
				$sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");
				
				$adapter->setValidators(array($extensionValidator, $sizeValidator));
					$validfile = true;
				if($adapter->isValid()){
					$validfile = true;
				}

				if($zf_filter_input->isValid() && $validfile)
				{
					// die;
					// $content = array(
					// 				'BRANCH_NAME' 	 => $zf_filter_input->branch_name,
					// 				'BANK_ADDRESS' 	 => $zf_filter_input->branch_address,
					// 			    'CITY_NAME' 	 => $zf_filter_input->city_name,
					// 			    'REGION_NAME' 	 => $zf_filter_input->region_name,
					// 			    'CONTACT' 	 => $zf_filter_input->contact
					// 		       );
					
					if(empty($checkbene)){

						// $privibenelinkage = $this->view->hasPrivilege('BLBA');

						$newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
						$adapter->addFilter('Rename', $newFileName);
						//$fileType = $adapter->getMimeType();

						$adapter->receive();
						
						$content = array(
								'COMP_ID' 			=> $zf_filter_input->COMPANY_ID,
								'COMP_NAME' 		=> $zf_filter_input->COMPANY_NAME,
								'COMP_TYPE' 		=> $zf_filter_input->COMPANY_TYPE,
								'DEBIT_BANK' 		=> $zf_filter_input->DEBIT_BANK,
								'DEBIT_ACCT' 		=> $zf_filter_input->DEBIT_ACCOUNT,
								'ACCT_NAME'			=> $zf_filter_input->ACCT_NAME,
								'DIR_FILENAME' 		=> $sourceFileName,
								'DIR_SYS_FILENAME'	=> $newFileName,
						);
							
						// $content['BENEFICIARY_TYPE'] = 4; // 2 = domestic ( SKN/RTGS )
						//$content['CLR_CODE'] = $CLR_CODE;
						$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
						$sessionNamespace->mode = 'Add';
						$sessionNamespace->content = $content;
						// $this->_redirect('/directdebit/index/confirm');
						$this->_redirect('/directdebit/index/confirm/1');

						
					}else{
						$this->view->succes = false;
						$this->view->error = true;
						$this->view->report_msg = $this->language->_('Direct Debit data already exist');	
					}
							
				}
				else
				{

					$this->view->error = true;
					foreach(array_keys($filters) as $field)
							$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

					$error = $zf_filter_input->getMessages();
					
					//format error utk ditampilkan di view html 
					$errorArray = null;
					foreach($error as $keyRoot => $rowError)
					{
					foreach($rowError as $errorString)
					{
						$errorArray[$keyRoot] = $errorString;
					}
					}
					// print_r($errorArray);die;
					$this->view->succes = false;
					$this->view->report_msg = $errorArray;			
				}
			}			
			
		}	
		
		$this->view->COMPANY_ID = $COMPANY_ID;
		$this->view->COMPANY_NAME = $COMPANY_NAME;
		$this->view->COMPANY_TYPE = $COMPANY_TYPE;
		$this->view->DEBIT_BANK = $DEBIT_BANK;
		$this->view->DEBIT_ACCOUNT = $DEBIT_ACCOUNT;
		$this->view->ACCT_NAME = $ACCT_NAME;
		
		Application_Helper_General::writeLog('BADA','Add Direct debit data');
					
	}

	public function confirmAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		$this->view->COMPANY_ID = $content['COMP_ID'];
		$this->view->COMPANY_NAME = $content['COMP_NAME'];
		$this->view->COMPANY_TYPE = $content['COMP_TYPE'];
		$this->view->DEBIT_BANK = $content['DEBIT_BANK'];
		$this->view->DEBIT_ACCOUNT = $content['DEBIT_ACCT'];
		$this->view->ACCT_NAME = $content['ACCT_NAME'];
		// print_r($content);die;
		// $mode = $sessionNamespace->mode;
		// $this->view->mode = $mode;
		

// 		die;
//  		$this->fillParams(null,$content['PHONE'],$content['ACBENEF'],$content['NRC']);
		if($this->_request->isPost() )
		{	
			// die;
			if($this->_getParam('submit1')==$this->language->_('Back'))
			{
				if($mode=='Add'){
					$this->_redirect('/directdebit/index/index/isback/1');
				}elseif ($mode=='Edit'){
					$this->_redirect('/directdebit/index/index/benef_id/'.$content['BENEFICIARY_ID'].'/isback/1');	
				}
			}
			else
			{
// die;
				try 
				{
					//-----insert benef--------------
					$this->_db->beginTransaction();
					// $Beneficiary = new Beneficiary();
					// $add = $Beneficiary->add($content);
					$info = $this->language->_('directdebit');
					$info2 = $this->language->_('Set new direct debit data');
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'T_DIRECTDEBIT','TEMP_DIRECTDEBIT',$content['COMP_ID'],$content['COMP_NAME'],$content['COMP_ID']);
							$content['CHANGES_ID'] = $change_id;
							$content['DIR_SUGESTED'] = new Zend_Db_Expr('now()');
							$content['DIR_SUGESTEDBY'] = $this->_userIdLogin;
							// print_r($content);die;
					$this->_db->insert('TEMP_DIRECTDEBIT',$content);
					Application_Helper_General::writeLog('BADA','Add Direct Debit '.$content['COMPANY_ID']);
					$this->_db->commit();
					unset($_SESSION['beneficiaryAccountAddEdit']);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e) 
				{
					// print_r($e);die;
					//rollback changes
					$this->_db->rollBack();
				}
			}
		}
		Application_Helper_General::writeLog('BADA','Confirm Direct Debit');
	}

	public function newconfirmAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('directdebit');
		$content = $sessionNamespace->content;
		$this->view->DEBIT_BANK = $content[0]['DEBIT_BANK'];

		// $fields = array('COMPANY CODE', 'DEBITED ACCOUNT', 'CLIENT REFF', 'CLIENT NAME', 'DOCUMENT REFF', 'ERROR MESSAGE');

		$fields = array(
						'comp' => array('field'    => 'COMP_ID',
										'label'    => $this->language->_('Company Code'),
								),
						'debitacct' => array('field'    => 'DEBIT_ACCT',
										'label'    => $this->language->_('Debited Account'),
								),
						'clienreff' => array('field'    => 'CLIENT_REFF',
										'label'    => $this->language->_('Client Reff'),
								),
						'clienname' => array('field'    => 'CLIENT_NAME',
										'label'    => $this->language->_('Client Name'),
								),
						'docref' => array('field'    => 'DOCUMENT_REFF',
										'label'    => $this->language->_('Document Reff'),
								),
						'errormsg' => array('field'    => 'ERROR_CODE',
										'label'    => $this->language->_('Error Description'),
								),
		);

		$setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');

		if(count($content) >= $this->_maxRow){
			$this->view->maxRows = $this->_maxRow;
		}

		foreach($content as $key => $list){
			foreach($list['ERROR_CODE'] as $er => $v){
				if($er == 'AD01'){
					unset($content[$key]['ERROR_CODE']['AD07']);
				}
				if($er == 'AD02'){
					unset($content[$key]['ERROR_CODE']['AD06']);
				}
				if($er == 'AD05'){
					unset($content[$key]['ERROR_CODE']['AD08']);
				}
			}
		}

		// echo "<pre>";
		// var_dump($content[0]['ERROR_CODE']);
		// var_dump($content);
		$csv = $this->_getParam('csv');
		$this->view->content = $content;
		if($csv){
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			 		
			$listdata = array();
			foreach($content as $key => $value){
				$errorlist = '';
				if($value['ERROR_CODE'] != ''){
					foreach($value['ERROR_CODE'] as $er => $vv){
						$errorlist .= $vv.", ";
					}
				}
				$errorDesc = rtrim($errorlist, ", ");

				$list = array (
					'COMPANY_CODE' => $value['COMP_ID'],
					'DEBITED_ACCOUNT' => $value['DEBIT_ACCT'],
					'CLIENT_REFF' => $value['CLIENT_REFF'],
					'CLIENT_NAME' => $value['CLIENT_NAME'],
					'DOCUMENT_REFF' => $value['DOCUMENT_REFF'],
					'ERROR_CODE' => $errorDesc
				);
				array_push($listdata, $list);
			}

			// var_dump($listdata);die;
			$date = date('Ymd');
			$this->_helper->download->csv($header, $listdata, null, 'Auto Debit Granted Account_'.$date);
		}

		if($this->_request->isPost())
		{	
			
			// die;
				foreach($content as $val){
					unset($val['ERROR_CODE']);
					try 
					{
						
						$this->_db->beginTransaction();
						$info = $this->language->_('Auto Debit');
						$info2 = $this->language->_('New Auto Debit');
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'T_DIRECTDEBIT','TEMP_DIRECTDEBIT',$val['CLIENT_REFF'],$val['CLIENT_NAME'],$val['COMP_ID']);

						$changesID = array('CHANGES_ID' => $change_id);
						$newval = array_merge($val, $changesID);

						$currentD = $this->_db->select()
									->from(array('C' => 'TEMP_DIRECTDEBIT'));

						$currentD->where("CLIENT_REFF = ?",$val['CLIENT_REFF']);
						$currentD = $this->_db->fetchRow($currentD);

						if($currentD){
							$where = array('CLIENT_REFF = ?' => $val['CLIENT_REFF']);
							$result = $this->_db->update('TEMP_DIRECTDEBIT',$newval,$where);
						}else{

							$this->_db->insert('TEMP_DIRECTDEBIT',$newval);
							
							$rowUpdated = $this->_db->update(
								'T_GLOBAL_CHANGES',
								array(
									'CHANGES_STATUS' => 'WA',
									'DISPLAY_TABLENAME' => 'Auto Debit',
									'CHANGES_INFORMATION' => 'New Auto Debit',
									'COMPANY_CODE' => $val['COMP_ID'],
									'COMPANY_NAME' => $val['COMP_NAME'],
									'KEY_FIELD' => $val['CLIENT_REFF'],
									'KEY_VALUE' => $val['CLIENT_NAME'],
									'LASTUPDATED' => new Zend_Db_Expr("now()"),
								),
								$this->_db->quoteInto('CHANGES_ID = ?', $change_id)
							);
						}
						
						Application_Helper_General::writeLog('BADA','Add Auto Debit '.$val['COMP_ID']);
						$this->_db->commit();
						
					}
					catch(Exception $e) 
					{
						print_r($e);die;
						//rollback changes
						$this->_db->rollBack();
					}
				}
				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
				$this->_redirect('/notification/success');
				
				Application_Helper_General::writeLog('BADA','Confirm Auto Debit');
			
		}
	}

	public function editemailAction()
	{
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;
		$benef_id = $this->_getParam('benef_id');
		$param['cust_id'] = $this->_custIdLogin;
		//echo $benef_id."asfd"; die;
		
		if(!$this->_request->isPost())
		{
			//$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];	
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];		 
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];		 
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];		
					
// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
// 					$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					// 					print_r($temp);;die;
// 					$this->view->CATEGORY_NAME_LLD		  		= $category_name['0']['sm_text1'];
						
						
					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];
						
					$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
					$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
					$this->view->BANK_CITY = $resultdata['BANK_CITY'];
					$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];
					$this->view->SWIFT_CODE = $resultdata['SWIFT_CODE'];
					
					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];
					// $modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					// $arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					$this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];
					
					// $CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
					// $arr 				= $modelCity->getCityCode($CITY_CODEGet);
					
					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();
					
					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;	
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];	
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];	
					// $this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				} 
			}
			else
			{
			    $error_remark = 'Beneficiary ID does not exist.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());	
			}
		}
		else
		{
			$filters = array( 	'BENEFICIARY_ID' => array('StringTrim','StripTags'),
								'ACBENEF_EMAIL' 	=> array('StringTrim','StripTags'));
								
			$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																$this->language->_('Error').': '.$this->language->_('Beneficiary ID cannot be left blank').'.',
																$this->language->_('Error').': '.$this->language->_('Wrong Format Beneficiary ID').'.')
														),
								'ACBENEF_EMAIL' => array(	//new SGO_Validate_EmailAddress(),
															'allowEmpty' => true,
															//'messages' => array(
															//	'Error: Wrong Format Email Address of {$email}. Please correct it.')
														));
		
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{													
				$error = false;
				
				if (trim($zf_filter_input->ACBENEF_EMAIL) != "") 
				{
					$arr_email = explode(";",$zf_filter_input->ACBENEF_EMAIL);										
					foreach ($arr_email as $value) 
					{
						if (Zend_Validate::is(trim($value), 'EmailAddress') == false)  
						{	
							$error = true;
							break;
						}
					}
				}
				
				if (!$error)
				{
					try 
					{
						//-----edit email--------------
						$this->_db->beginTransaction();
						
						$Beneficiary = new Beneficiary();
						//echo $benef_id; die;
						$Beneficiary->editEmail($zf_filter_input->BENEFICIARY_ID,$zf_filter_input->ACBENEF_EMAIL);
						
						Application_Helper_General::writeLog('BEDA','Edit Email Destination Account '.$this->_getParam('ACBENEF'));
						
						$this->_db->commit();
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');
					}
					catch(Exception $e) 
					{
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
					}
				}
				else
				{
					$this->view->error = true;
					$param['benef_id'] = $zf_filter_input->BENEFICIARY_ID;
					$temp = $model->getBeneficiaries($param);
					$resultdata = $temp[0];
					
					  if($resultdata)
					  {
							$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
							$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
							$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
							$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
							$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
							$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
							$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
							$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];	
							$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];		 
							$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];		 
							$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];	
							
							$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
							$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
							$this->view->BANK_CITY = $resultdata['BANK_CITY'];
							$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
							$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
							$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];

							$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
							$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];
							
							// $modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
							// $CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
							// $arr 				= $modelCity->getCityCode($CITY_CODEGet);
							
							// 9. Create LLD string
							$settings 			= new Application_Settings();
							$LLD_array 			= array();
							$LLD_DESC_arrayCat 	= array();
							$lldTypeArr  		= $settings->getLLDDOMType();
							
							if (!empty($LLD_CATEGORY))
							{
								$lldCategoryArr  	= $settings->getLLDDOMCategory();
								$LLD_array["CT"] 	= $LLD_CATEGORY;
								$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
							}
		
							$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;	
							$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];	
							$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];	
							// $this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
					   } 
					$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
					$docErr = $this->language->_('Error').': '.$this->language->_('Invalid Format - Email Address').'.';
					$this->view->report_msg = $docErr;
				}
			}
			else
			{
				$this->view->error = true;
				$param['benef_id'] = $zf_filter_input->BENEFICIARY_ID;
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				  if($resultdata)
				  {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];	
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];		 
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];		 
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];	

						$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
						$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];
						
						// $modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
						// $CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
						// $arr 				= $modelCity->getCityCode($CITY_CODEGet);
						
						// 9. Create LLD string
						$settings 			= new Application_Settings();
						$LLD_array 			= array();
						$LLD_DESC_arrayCat 	= array();
						$lldTypeArr  		= $settings->getLLDDOMType();
						
						if (!empty($LLD_CATEGORY))
						{
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_array["CT"] 	= $LLD_CATEGORY;
							$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
						}
	
						$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;	
						$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];	
						$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];	
						// $this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				   } 
				$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}
			
			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}	
			}
		}
		Application_Helper_General::writeLog('BEDA','Edit Email Destination Account ');
						
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}
	
	
	
	public function detailAction()
	{
		// $model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;
		$benef_id = $this->_getParam('benef_id');
		$param['cust_id'] = $this->_custIdLogin;
		//echo $benef_id."asfd"; die;
	
		if(!$this->_request->isPost())
		{
			//$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
				//print_r($param);die;
				// $temp = $model->getBeneficiaries($param);
				// $resultdata = $temp[0];
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->SWIFT_CODE  		= $resultdata['SWIFT_CODE'];

						
					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					// 					$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					// 					print_r($temp);;die;
					// 					$this->view->CATEGORY_NAME_LLD		  		= $category_name['0']['sm_text1'];
	
	
					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];
	
					$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
					$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
					$this->view->BANK_CITY = $resultdata['BANK_CITY'];
					$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];
						
					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];
						
					// $modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					// $arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					// $this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];
					// $CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
					// $arr 				= $modelCity->getCityCode($CITY_CODEGet);
						
					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();
						
					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}
	
					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					// $this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
			}
			else
			{
				$error_remark = 'Beneficiary ID does not exist.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			}
		}
		
		Application_Helper_General::writeLog('BEDA','Edit Email Destination Account ');
	
	}
	
	public function deleteAction()
	{
		// $model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;
		$param['cust_id'] = $this->_custIdLogin;
		if(!$this->_request->isPost())
		{
			$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			
			if($benef_id)
			{
		//print_r($param);die;
				$temp = $model->getBeneficiaries($param);
				$resultdata =  $temp[0];
// 				echo "<pre>";
// 				print_r($resultdata);die;
			  if($resultdata)
			  {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->ACBENEF_NAME  		= $resultdata['BENEFICIARY_NAME'];
					$this->view->ACBENEF_PHONE  		= $resultdata['BENEFICIARY_PHONE'];
						
					 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					 					$this->view->NATIONALITY		  		= $resultdata['BENEFICIARY_CITIZENSHIP'];

					 					$CATEGORY = $resultdata['BENEFICIARY_CATEGORY'];
										if($CATEGORY == '5'){
											$CATEGORY_CODE = 'Company';
										}
										elseif($CATEGORY == '1'){
											$CATEGORY_CODE = 'Individual';
										}
										elseif($CATEGORY == '2'){
											$CATEGORY_CODE = 'Government';
										}
										elseif($CATEGORY == '3'){
											$CATEGORY_CODE = 'Bank';
										}
										elseif($CATEGORY == '4'){
											$CATEGORY_CODE = 'Non Bank Financial Institution';
										}
										elseif($CATEGORY == '6'){
											$CATEGORY_CODE = 'Other';
										}
					//$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					 					//print_r($resultdata['BENEFICIARY_CATEGORY']);;die;
					 					$this->view->CATEGORY_NAME_LLD		  		= $CATEGORY_CODE;
	
					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];
	
					$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
					$this->view->BENEFICIARY_BANK_CODE = $resultdata['SWIFT_CODE'];
					
					$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];
						
					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];
						
					// $modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					// $CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
					// $arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					// $this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];	
					$this->view->SWIFT_CODE =   $resultdata['SWIFT_CODE'];	

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();
						
					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}
	
					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];
					// $this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
			   } 
			}
			else
			{
			   $error_remark = $this->language->_('Error').': '.$this->language->_('Beneficiary ID cannot be left blank').'.';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			}
		}
		else
		{
			$filters = array(  	'BENEFICIARY_ID' => array('StringTrim','StripTags'),
								'STATUS' => array('StringTrim','StripTags'));
								
			$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																$this->language->_('Error').': '.$this->language->_('Beneficiary ID cannot be left blank').'.',
																$this->language->_('Error').': '.$this->language->_('Wrong Format Beneficiary ID').'.')
														),
								'STATUS' 		=> array(	'NotEmpty',
															'Digits',
															'messages' => array(
																$this->language->_('Error').': '.$this->language->_('Status cannot be left blank').'.',
																$this->language->_('Error').': '.$this->language->_('Wrong Format Status').'.')
														));
		
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{								
				try 
				{
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					//$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
					//print_r($zf_filter_input->STATUS);die;
					if($zf_filter_input->STATUS)
						$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
					else
						$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
					$this->_db->commit();
					Application_Helper_General::writeLog('BEDA','Delete Destination Account '.$this->_getParam('ACBENEF'));
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
				}
			}
			else
			{
				$this->view->error = true;
				$param['benef_id'] = $zf_filter_input->BENEFICIARY_ID;
				$temp = $model->getBeneficiaries($param);
				$resultdata =  $temp[0];
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];		 
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];		 
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];	
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];		 		 
					$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];	 	 
				} 
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}
		}
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
    	Application_Helper_General::writeLog('BEDA','Delete Destination Account ');
	}
	
	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID'); 
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS'); 
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->ACBENEF_BANKNAME = ($zf_filter_input->isValid('ACBENEF_BANKNAME')) ? $zf_filter_input->ACBENEF_BANKNAME : $this->_getParam('ACBENEF_BANKNAME');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ADDRESS = ($zf_filter_input->isValid('ADDRESS')) ? $zf_filter_input->ADDRESS : $this->_getParam('ADDRESS');
		$this->view->CITIZENSHIP = ($zf_filter_input->isValid('CITIZENSHIP')) ? $zf_filter_input->CITIZENSHIP : $this->_getParam('CITIZENSHIP');
		$this->view->NATIONALITY = ($zf_filter_input->isValid('NATIONALITY')) ? $zf_filter_input->NATIONALITY : $this->_getParam('NATIONALITY');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
		$this->view->BANK_NAME = ($zf_filter_input->isValid('BANK_NAME')) ? $zf_filter_input->BANK_NAME : $this->_getParam('BANK_NAME');
		$this->view->CLR_CODE = ($zf_filter_input->isValid('CLR_CODE')) ? $zf_filter_input->CLR_CODE : $this->_getParam('CLR_CODE');
		$this->view->BANK_CITY = ($zf_filter_input->isValid('BANK_CITY')) ? $zf_filter_input->BANK_CITY : $this->_getParam('BANK_CITY');
	}
	
	private function fillParams($benefType,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_ID=null,$BENEFICIARY_TYPE,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE)
	{
		$modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
		$arr 					= $modelCity->getCityCode($CITY_CODEGet);
		
		// 9. Create LLD string
		$settings 			= new Application_Settings();
		$LLD_array 			= array();
		$LLD_DESC_arrayCat 	= array();
		$lldTypeArr  		= $settings->getLLDDOMType();
		
		if (!empty($LLD_CATEGORY))
		{
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$LLD_array["CT"] 	= $LLD_CATEGORY;
			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
		}
		
		if (!empty($LLD_BENEIDENTIF))
		{
			$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
			$LLD_array["CT"] 	= $LLD_BENEIDENTIF;
			$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$LLD_BENEIDENTIF];
		}
		
		if($BENEFICIARY_ID) 
		$this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		//$this->view->benefType = $benefType;
		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ADDRESS = $ACBENEF_ADDRESS;
		$this->view->CITIZENSHIP = $ACBENEF_RESIDENT;
		$this->view->NATIONALITY = $ACBENEF_CITIZENSHIP;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
		$this->view->BANK_NAME = $BANK_NAME;
		$this->view->CLR_CODE = $CLR_CODE;
		$this->view->BANK_CITY = $BANK_CITY;
		$this->view->BENEFICIARY_TYPE = $BENEFICIARY_TYPE;
		
		$this->view->LLD_CATEGORY = $LLD_CATEGORY;;
		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
		$this->view->CITY_CODE = $CITY_CODE;
		
		$this->view->CITY_NAME = $arr[0]['CITY_NAME'];
		$this->view->CATEGORY_NAME_LLD = $LLD_CATEGORY_POST;
		$this->view->BENEIDENTIF_NAME_LLD = $LLD_BENEIDENTIF_POST;
		
		
	}
	
	public function suggestdeleteAction()
	{
		$filters = array( 'BENEFICIARY_ID' => array('StringTrim','StripTags'));
		$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
														'Digits',
														'messages' => array(
																$this->language->_('Error').': '.$this->language->_('Beneficiary ID cannot be left blank').'.',
																$this->language->_('Error').': '.$this->language->_('Wrong Format Beneficiary ID').'.')
													));
													
		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
		if($zf_filter_input->isValid())
		{
			$Beneficiary = new Beneficiary();
			$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
		}
		$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
	}



	public function whereAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('M_CUSTOMER', array('CUST_ID','CUST_NAME','CUST_TYPE'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($select);
       

        echo json_encode($tempColumn);
    }
}
