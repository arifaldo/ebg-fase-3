<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';


class tools_ChangefpassController extends Application_Main {
	
	public function indexAction() 
	{
		if($this->_request->isPost())
		{
		$password = $this->_getParam('password');
		$userId = $this->_getParam('userId');
		$custId = $this->_getParam('custId');

		$key = md5((string)$userId.(string)$custId);
		
		// $encrypted_pwd = Crypt_AESMYSQL::encrypt($password, $key);
		// $encryptedNewPassword = md5($encrypted_pwd);

		$encrypt = new Crypt_AESMYSQL ();
		$encryptedNewPassword = md5 ( $encrypt->encrypt ( $password, $key ) );
		
		
		$data = array(
						'USER_PASSWORD' => $encryptedNewPassword,
						'USER_UPDATED' => new Zend_Db_Expr("now()"),
						'USER_LASTCHANGEPWD' => new Zend_Db_Expr("now()"),
					);
					$where =  array();
					$where['CUST_ID = ?'] = $custId;
					$where['USER_ID = ?'] = $userId;
					$update = $this->_db->update('M_USER',$data,$where);

			$this->view->message = $update.' password changes';

		}
	}	
}
