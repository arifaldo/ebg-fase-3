<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));

	$db = Zend_Db_Table::getDefaultAdapter();

	$where = array("TIMESTAMPDIFF(SECOND,(SELECT FROM_UNIXTIME( modified , '%Y-%m-%d %H:%i:%s')),now()) > lifetime = 1");
	$db->delete('bosession',$where);// Delete Session BO
	$db->delete('fosession',$where);// Delete Session FO
	$db->query('OPTIMIZE TABLE fosession');
	$db->query('OPTIMIZE TABLE bosession');

	Application_Helper_General::cronLog(basename(__FILE__),'Clering Session Bo Session & Fo Session',1);
	
?>