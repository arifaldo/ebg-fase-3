<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Service/Token.php'; //added new
require_once ('Crypt/AESMYSQL.php');

//NOTE:
//Watch the modulename, filename and classname carefully
class Changesemail_IndexController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		$cust_id = $this->_custIdLogin;
		$user_id = $this->_userIdLogin;
		$this->view->user_id 	= $user_id;
		
		$settings =  new Settings();
		
		
		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
		$data2 = $this->_db->fetchRow($select3);
		// print_r($data2);die;
		$this->view->old_email		= $data2['USER_EMAIL'];

		$mail=$data2['USER_EMAIL'];

		$mail_first=explode('@',$mail);
		$arr=str_split($mail_first[0]);
		$mask=array();

		for($i=0;$i<count($arr);$i++) {
		    if($i) {
		        $arr[$i]='*';
		    }

		    $mask[]=$arr[$i];
		}

		$mask=join($mask).'@'.$mail_first[1];

		$this->view->maskmail = $mask;

		$this->view->userId		= $data2['USER_ID'];
		$this->view->usermobilephone	= $data2['USER_MOBILE_PHONE'];
		$this->view->tokentype = '2';
		$this->view->tokenIdUser = $data2['TOKEN_ID'];
		$tokenIdUser = $data2['TOKEN_ID'];
		$tokenType = '2';
		
		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode 					= $HardToken->generateChallengeCode();
		$this->view->challengeCode 		= $challengeCode;
		$this->view->challengeCodeReq 	= $challengeCode;
		if(is_string($user_id))
		{
			if($this->_request->isPost())
			{
			
				$filters = array(
					'old_email'     => array('StripTags','StringTrim'),
			 		'new_email'     => array('StripTags','StringTrim'),
					'confirm_email'     => array('StripTags','StringTrim'),
				);
			
				$validators =  array(
							'old_email'       => array('NotEmpty',														
													'messages' => array(
																	   $this->language->_('Current Email Can not be left blank'),																		   
																	   )
													),						  
						  
						   'new_email'    => array('NotEmpty',
													new Application_Validate_EmailAddress(),
													//array('StringLength',array('min'=>1,'max'=>128)),
													'messages' => array(
																	   $this->language->_('New Email cannot be left blank'),
																	   //$this->language->_('Email lenght cannot be more than 128'),
																	   $this->language->_('Invalid email format'),
																	   )
													),
							'confirm_email'    => array('NotEmpty',
													new Application_Validate_EmailAddress(),
													//array('StringLength',array('min'=>1,'max'=>128)),
													'messages' => array(
													$this->language->_('New Email cannot be left blank'),
													//$this->language->_('Email lenght cannot be more than 128'),
													$this->language->_('Invalid email format'),
								)
						),
				);

				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getPost());
	
				$oldpass	= $zf_filter_input->old_email;
				$newpass	= $zf_filter_input->new_email;
				$confirm_email	= $zf_filter_input->confirm_email;
				
				if($newpass!=$confirm_email){
					$this->view->error = 1;
// 					$this->view->old_email	= $oldpass;
					$this->view->msg_failed = $this->language->_('Error: Email does not match.');
				}else
				{				
				$errDesc = array();			
			
				if($zf_filter_input->isValid())
	  			{
	  				
	  				$filters = array('*' => array('HtmlEntities', 'StripTags', 'StringTrim'));
					$validators = array(
						// 'responseCodeReq' => array(
						// 	'notEmpty',
						// 	'alnum',
						// 	'presence' => 'required',
						// )
					);

					$filter = new Zend_Filter_Input($filters, $validators, $this->_getAllParams(), array('breakChainOnFailure' => true));
					if ($filter->isValid()){
						if($tokenType == '1')
						{
							$responseCode = $filter->getEscaped('responseCodeReq');
							//echo'smstoken';
						}
						elseif($tokenType == '2')
						{
							$responseCode = $filter->getEscaped('responseCodeReq');
							//echo'hardtoken';
						}
						elseif($tokenType == '3')
						{
							//echo'mobiletoken';
						}
						else{}
		
						$userData = $this->_db->select()
						->from(array('M_USER'),array('USER_ID','USER_MOBILE_PHONE','USER_EMAIL'))
						->where('USER_ID = ?', $this->_userIdLogin)
						->where('CUST_ID = ?', $this->_custIdLogin)
						->limit(1)
						;
						$userData = $this->_db->fetchRow($userData);
						$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);
							
						$Settings = new Settings();
						$message = $Settings->getSetting('token_confirm_message');
		
						$trans = array("[Response_Code]" => $responseCode);
						$message = strtr($message, $trans);
		
// 						$token = new Service_Token(NULL,$this->_userIdLogin);
// 						$token->setMsisdn($userData['USER_MOBILE_PHONE']);
// 						$token->setMessage($message);
		
// 						if($tokenType == '1'){ //sms token
// 							$res = $token->confirm();
// 							$resultToken = $res['ResponseCode'] == '0000';
// 						}
// 						elseif($tokenType == '2'){ //hard token
							
// 							$chal = $this->_getParam('challengeCodeReq');
// // 							print_r($chal);die;
// 							$resHard = $HardToken->verifyHardToken($chal, $responseCode);
// 							$resultToken = $resHard['ResponseCode'] == '0000';
		
// 							//set user lock token gagal

// 							$CustUser = new CustomerUser($this->_custIdLogin,$this->_userIdLogin);
					
// 							if ($resHard['ResponseCode'] != '0000'){
// 								$tokenFailed = $CustUser->setFailedTokenMustLogout($resHard['ResponseTO']);
// 								if ($tokenFailed)
// 									$this->_forward('home');
// 							}
// 						}
						$combine = ((string)$this->_userIdLogin.(string)$this->_custIdLogin);
						$key = md5($combine);
						$pass = $this->_getParam('password');

						$encrypt = new Crypt_AESMYSQL();
						$password = md5($encrypt->encrypt($pass,$key));


// 						elseif($tokenType == '3'){ //mobile token
// 						}
// 						else{}
//		  				echo 'here';die;
						if( $userData['USER_PASSWORD'] == $password){
						// if ($resultToken)
						// {
							$CustomerUser =  new CustomerUser($cust_id,$user_id);

							$result = array();
							$failed = 0;
							if(count($errDesc)>0) $failed = 1;
							$result = $CustomerUser->changeEmail($oldpass,$newpass,$failed,$errDesc);
							
							if( (is_array($result)  && $result !== true) ||  $failed == 1 )
							{
								if(count($result)>0)
								{
									foreach($result as $key=>$value)	
									{						
										$errDesc[$key] = $value;							
									}
								}
									$this->view->error 	= true;
									$this->view->msg_failed = $this->language->_('Error in processing form values. Please correct values and re-submit');
		
									$this->view->errDesc = $errDesc;
									$this->view->new_email	= $newpass;
									$this->view->old_email	= $oldpass;
							}
							else{
									Application_Helper_General::writeLog('CHME','Change My Email Cust ID : '.$cust_id.',User ID : '.$user_id);
									$this->setbackURL('/home');
									$this->_redirect('/notification/success/index');
							}
						}else{ // jika token failed
							$this->view->error 		= true;
							$this->view->msg_failed = $this->language->_('Invalid Token');
							$this->view->new_email	= $newpass;
							$this->view->old_email	= $oldpass;
						}	
						
						/*/* FORCE LOGOUT 
						$locked = $this->_db->select()
						->from(array('M_USER'),array('USER_ISLOCKED'))
						->where('USER_ID = ?', $this->_userIdLogin)
						->limit(1);
						$locked = $this->_db->fetchRow($locked);
						$locked = $locked['USER_ISLOCKED'];

						if (isset($locked) && $locked == '1'){
							//$CustomerUser->forceLogout();
							$this->redirect('/default/index/logout');
						}*/
					}else{
						$this->view->error = 1;
						$this->view->msg_failed = $this->language->_('Response Token cannot be left blank');
						$this->view->new_email	= $newpass;
						$this->view->old_email	= $oldpass;
					}					  			
					
	  			}else{

	  				$this->view->error = 1;
					$this->view->new_email = ($zf_filter_input->isValid('new_email'))? $zf_filter_input->new_email : $this->_getParam('new_email');
					$error = $zf_filter_input->getMessages();
					
					
					//format error utk ditampilkan di view html 
					$errorArray = null;
					
					foreach($error as $keyRoot => $rowError)
					{
					   foreach($rowError as $errorString)
					   {
						  $errorArray[$keyRoot] = $errorString;
					   }
					}
											
					foreach($errorArray as $key => $val)
					{
						$nameError = "Error_".$key ;
						$this->view->$nameError  = $val;
					}					
					
					$this->view->old_email	= $oldpass;
					$this->view->msg_failed = $this->language->_('Error in processing form values. Please correct values and re-submit');
	  				
	  			}
	  			
			}
			}	
		}
		
		Application_Helper_General::writeLog('CHME','Change My Email');
	}	
}

