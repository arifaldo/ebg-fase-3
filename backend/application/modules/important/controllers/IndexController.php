<?php
require_once 'Zend/Controller/Action.php';
class important_IndexController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$arr = null;
		$viewFilter = null;
		$errors = null;
		
		$fields = array	(
							'HelpTopic'  			=> array	(
																	'field' => 'SUBJECT',
																	'label' => $this->language->_('Subject'),
																	'sortable' => true
																),
							'TargetTopic'  			=> array	(
																	'field' => 'TARGET',
																	'label' => $this->language->_('Target'),
																	'sortable' => true
																),
							'NewsTopic'  			=> array	(
																	'field' => 'NEWS',
																	'label' => $this->language->_('Send To'),
																	'sortable' => true
																),
							
							// 'FileDescription'  					=> array	(
							// 											'field' => 'IMAGE',
							// 											'label' => $this->language->_('Image'),
							// 											'sortable' => true
							// 										),
							// 'FileName'  			=> array	(
							// 										'field' => 'HELP_FILENAME',
							// 										'label' => $this->language->_('File Name'),
							// 										'sortable' => true
							// 									),
							'Uploaded By'  					=> array	(
																		'field' => 'CREATEDBY',
																		'label' => $this->language->_('Last Updated'),
																		'sortable' => true
																	),	
							/*'Uploaded Date'  					=> array	(
																		'field' => 'CREATED',
																		'label' => $this->language->_('Created Date'),
																		'sortable' => true
																	),	*/
							'efdate'  					=> array	(
																		'field' => 'EF_DATE',
																		'label' => $this->language->_('Effective Date'),
																		'sortable' => true
																	),	
							'expDate'  					=> array	(
																		'field' => 'EXP_DATE',
																		'label' => $this->language->_('Expired Date'),
																		'sortable' => true
																	),	
							'status'  					=> array	(
																		'field' => 'ACTIVE',
																		'label' => $this->language->_('Status'),
																		'sortable' => true
																	),
							/*'dateNews'  					=> array	(
																		'field' => 'date_news',
																		'label' => $this->language->_('Date'),
																		'sortable' => true
																	),	*/
						);
		$this->view->fields = $fields;


		$filterlist = array('TITLE','CREATED','CREATEDBY');
		
		$this->view->filterlist = $filterlist;
	  
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'TITLE'   => array('StringTrim','StripTags','StringToUpper'),
							'CREATED'   	=> array('StringTrim','StripTags','StringToUpper'),
							'CREATEDBY' 	=> array('StringTrim','StripTags')
							
		);

		 $dataParam = array('TITLE','CREATED','CREATEDBY');
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}

		if(!empty($this->_request->getParam('uploaddate'))){
				$createarr = $this->_request->getParam('uploaddate');
					$dataParamValue['CREATED'] = $createarr[0];
					$dataParamValue['CREATED_END'] = $createarr[1];
					// print_r($dataParamValue);
			}
		// print_r($this->_request->getParams());die;
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		// $zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		// $filter 	= $zf_filter->getEscaped('filter');
		$delete 	= $this->_getParam('delete');
		
		if($delete)
		{
			//echo("<br>ok<br>");
			$postreq_id	= $this->_request->getParam('req_id');
			if($postreq_id)
			{
				foreach ($postreq_id as $key => $value) 
				{
					if($postreq_id[$key]==0)
					{
						unset($postreq_id[$key]);
					}
					
				}
			}
			
			if($postreq_id == null)
			{
				$params['req_id'] = null;
			}
			else
			{
				$params['req_id'] = 1;
			}
			
			$validators = array	(
										'req_id' => 	array	(	
																	'NotEmpty',
																	'messages' => array	(
																							'Error File ID Submitted',
																						)
																),
									);
			$filtersVal = array	( 	
									'req_id' => array('StringTrim','StripTags')
								);
			$zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
			
			if($zf_filter_input->isValid())
			{			
				try
				{
					foreach ($postreq_id as  $key =>$value) 
					{		
						$HELP_ID_DELETE =  $postreq_id[$key];
				
						$this->_db->beginTransaction();
						$param = array('STATUS'=> '4',);	
						
						$where = array('ID = ?' => $HELP_ID_DELETE);
						$query = $this->_db->update ( "M_NOTIFICATION", $param, $where );
						$this->_db->commit();
					}
					$id = implode(",", $postreq_id);
					Application_Helper_General::writeLog('HLUD','Delete important notification. Important Notification ID: ['.$id.']');
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
			else
			{
				$error 			= true;
				$errors 		= $zf_filter_input->getMessages();
				$req_idErr 		= (isset($errors['req_id']))? $errors['req_id'] : null;
				
				$this->_redirect("/important/index?error=true&req_idErr=$req_idErr&filter=Filter");
			}
			
			//$filter = 'Filter';
			
		}
		
		if($this->_getParam('error'))
		{
			$this->view->error 			= $this->_getParam('error');
			
			$this->view->req_idErr 		= $this->_getParam('req_idErr');
			
		}
		
		//if($filter)
		//{
			$select = $this->_db->select()
								->from(	'M_NOTIFICATION',
										array('*'));
								// ->where("HELP_ISDELETED != 1");
								
		//}
		
		
		$HELP_ID   	= $this->_getParam('ID');
		
		// if($HELP_ID)
		// {
		// 	$select->where('ID =?',$HELP_ID);
		// 	$data = $this->_db->fetchRow($select);
		// 	$attahmentDestination = UPLOAD_PATH . '/document/help/';
		// 	$this->_helper->download->file($data['HELP_FILENAME'],$attahmentDestination.$data['HELP_SYS_FILENAME']);
		// 	Application_Helper_General::writeLog('HLDL','Download Help.  Help ID: ['.$HELP_ID.'] , Help Topic: ['.$data['HELP_TOPIC'].']');
		// }
		// else
		// {
		// 	Application_Helper_General::writeLog('HLLS','View help list');
		// }
		
		if($filter == TRUE)
		{
			// print_r($dataParamValue);die;
			$SEARCH_TEXT   	= $this->_getParam('SEARCH_TEXT');
			// $HELP_TOPIC   	= $this->_getParam('TITLE');
			// $UPLOADED_BY    = $this->_getParam('UPLOADED_BY');
			$HELP_TOPIC   	= $dataParamValue['TITLE'];
			$UPLOADED_BY    = $dataParamValue['CREATEDBY'];
			$DATE_START    	= $dataParamValue['CREATED'];					
			$DATE_END		= $dataParamValue['CREATED_END'];					
			

			// print_r($DATE_START);die;
			$DATESTART   			= (Zend_Date::isDate($DATE_START,$this->_dateDisplayFormat))?
									   new Zend_Date($DATE_START,$this->_dateDisplayFormat):
									   false;
			
			$DATEEND    			= (Zend_Date::isDate($DATE_END,$this->_dateDisplayFormat))?
									   new Zend_Date($DATE_END,$this->_dateDisplayFormat):
									   false;
									   
			$description	= $this->_getParam('description');
			
			if(!empty($DATE_START))
			{
				$select->where("DATE (EFDATE) >= DATE(".$this->_db->quote($DATESTART->toString($this->_dateDBFormat)).")");
			}
			
			if(!empty($DATE_END))
			{
				$select->where("DATE (EFDATE) <= DATE(".$this->_db->quote($DATEEND->toString($this->_dateDBFormat)).")");
			}
			
			// if($description)
			// {
			// 	$select->where("UPPER(CONTENT) LIKE ".$this->_db->quote('%'.$description.'%'));
			// }
			
			// if($SEARCH_TEXT)
			// {
			// 	$select->where("UPPER(HELP_FILENAME) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%'));
			// }
			
			if($HELP_TOPIC)
			{
				$select->where("UPPER(SUBJECT) LIKE ".$this->_db->quote('%'.$HELP_TOPIC.'%'));
			}
			
			if($UPLOADED_BY)
			{
				$select->where("UPPER(CREATEDBY) LIKE ".$this->_db->quote('%'.$UPLOADED_BY.'%'));
			}
			// echo $select;die;
			$this->view->DATE_END 		= $DATE_END;
			$this->view->DATE_START 	= $DATE_START;
			$this->view->description 	= $description;
			$this->view->SEARCH_TEXT 	= $SEARCH_TEXT;
			$this->view->HELP_TOPIC 	= $HELP_TOPIC;
			$this->view->UPLOADED_BY 	= $UPLOADED_BY;
		}
		// echo $select;die;
		$select->order($sortBy.' '.$sortDir);
		$arr = $this->_db->fetchAll($select);

		$this->view->arr = $arr;

		$csv = $this->_getParam('csv');

		if($this->_request->getParam('print') == 1 || $csv){
			
				$newarr = array();
				//echo '<pre>';
				// var_dump($arr);die;
				foreach($arr as $keys => $val){
					$newarr[$keys]['SUBJECT'] = $val['SUBJECT'];
					
					if($val['TARGET']=='1'){
                        $target = 'Front End';
                    }else if($val['TARGET']=='2'){
                        $target = 'Back End';
                    }else{
                        $target= 'FO and BO';
                    }
                            
                    if($val['SEND_TO'] == '1'){
                        // die;
                        $sendto = $this->language->_('All');
                    }else{
                        $sendto = $val['RECEIVER'];
                    }
                    
                    if($val['STATUS'] == '1'){
                        $status = $this->language->_('Started');
                    }else if($val['STATUS'] == '2'){
                        $status = $this->language->_('Ended');
                    }else if($val['STATUS'] == '3'){
                        $status = $this->language->_('Pending Future date');
                    }else if($val['STATUS'] == '4'){
                        $status = $this->language->_('Deleted');
                    }
					
					$newarr[$keys]['TARGET'] = $target;
					$newarr[$keys]['NEWS'] = $sendto;
					$newarr[$keys]['CREATEDBY'] = $val['CREATEDBY'].' - '.Application_Helper_General::convertDate($val['CREATED'],'dd MMM yyyy HH:mm:ss',$this->defaultDateFormat);
					$newarr[$keys]['EF_DATE'] = Application_Helper_General::convertDate($val['EFDATE'],'dd MMM yyyy',$this->defaultDateFormat) .' '.$val['EFTIME'];
					$newarr[$keys]['EXP_DATE'] = Application_Helper_General::convertDate($val['EXP_DATE'],'dd MMM yyyy',$this->defaultDateFormat).' ' . $val['EXP_TIME'];
					$newarr[$keys]['ACTIVE'] = $status;
					
					
					
				}
				
				if($csv){
					Application_Helper_General::writeLog('SYLG','Download CSV Notification');				
					$this->_helper->download->csv(array('Subject','Target','Send To','Last Update','Effective Date','Expired Date','Status'),$newarr,null,'Important Notification');
				}else{
				
						$this->_forward('print', 'index', 'widget', array('data_content' => $newarr, 'data_caption' => 'Notification', 'data_header' => $fields));
				}
				
			}

		$this->paging($arr);
		unset($dataParamValue['CREATED_END']);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}

	public function saveAction() 
	{

		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
		if($this->_request->isPost())
		{


			$params = $this->_request->getParams();
			// print_r($params);die;
			try {
				if(!empty($params['req_id'])){
					$i = 0;
					foreach ($params['req_id'] as $key => $value) {

							if($value){
								$param = array(
								'TARGET'			=> $params['target'][$i] 
								);		
							}
							// print_r($key);
							// print_r($params['target']);
							$where = array('ID = ?' => $value);
							// print_r($param);
							// print_r($where);die;
							$query = $this->_db->update ( "M_NOTIFICATION", $param, $where );
							$i++;
					}	
				}
				$this->setbackURL('/important');
				$this->_redirect('/notification/success');	
			} catch (Exception $e) {
				$this->setbackURL('/important');
				$this->_redirect('/notification/invalid');	
			}
			
			
		}
	}
}