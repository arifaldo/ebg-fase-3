<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class releasepayment_SetupautoreleaseController extends Application_Main
{
	
	public function indexAction() 
	{
		$setting= new Settings();
		
		$settingValue = $setting->getSetting('autorelease_isenabled');
		//Zend_Debug::dump($settingValue); die;
		$value = $this->_getParam('s');
		$save = $this->_getParam('m');
			
		if(isset($value))
		{
			if($value == "1")
			{
				$settingValue = "0";
			}
			else
			{
				$settingValue = "1";
			}
			$setting->setSetting('autorelease_isenabled',$settingValue);
		}
			$this->view->value = $settingValue;
			$this->view->save = $save;	
	}
}
