<?php

class superadmin_HomeController extends Zend_Controller_Action 
{
	
	public function init()
	{
		$this->_helper->layout()->setLayout('layout_superadmin');
		$this->_db 	= Zend_Db_Table::getDefaultAdapter();
		$objSessionNamespace = new Zend_Session_Namespace( 'sysadmin' );
		
		if(isset($objSessionNamespace->userIdLogin) && isset($objSessionNamespace->userNameLogin))
		{
			$suser_id	= $objSessionNamespace->userIdLogin;
			$this->view->userId = $suser_id;
		}
		else
		{
			$this->_redirect('/superadmin');
		}
	}
	
	public function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	public function indexAction() 
	{
		require_once 'Service/Account.php';
		
		$config 		= Zend_Registry::get('config');
		
		$time_start 	= $this->microtime_float();
		
		$CHECKDATABASE 	= "Connection Fail";
		if($this->_db->getConnection())
		{
			$CHECKDATABASE = "Connection OK";
		}
		
		$time_end 			= $this->microtime_float();
		$time 				= $time_end - $time_start;
		
		$time_start_login 	= $this->microtime_float();
		$select = $this->_db->select()->from(
												array('M_USER'),
												array(
														'TOTAL'=>'COUNT(*)',
													)
											)
									->where("USER_ISLOGIN = '1'");
		$totalLogin 	= $this->_db->fetchOne($select);
		$time_end_login = $this->microtime_float();
		$time_login 	= $time_end_login - $time_start_login;
		
		$this->view->totalLogin 	= $totalLogin['TOTAL'];
		
		$this->view->CHECKDATABASE 	= $CHECKDATABASE;
		$this->view->time 			= $time;
		$this->view->time_login 	= $time_login;
		
		$this->view->hostname 		= exec('hostname',$hostname);
		$this->view->iphostname 	= exec('ip addr show',$iphostname);
		$this->view->osversion 		= exec('cat /proc/version',$osversion);
		$this->view->uptime 		= exec('uptime',$uptime);
		$this->view->statusmemory 	= exec('free',$statusmemory);
		$this->view->filesystem 	= exec('df -h',$filesystem);
		$this->view->kernel 		= exec('uname -r',$kernel);
		$this->view->distro 		= exec('cat /etc/*-release',$distro);
		$this->view->model 			= exec('grep "model name" /proc/cpuinfo',$model);
		$this->view->processor 		= exec('grep "processor" /proc/cpuinfo',$processor);
		$this->view->cpu 			= exec('grep "cpu MHz" /proc/cpuinfo',$cpu);
		$this->view->cache 			= exec('grep "cache size" /proc/cpuinfo',$cache);
		$this->view->bogomips 		= exec('grep "bogomips" /proc/cpuinfo',$bogomips);
		
		//----------------------- data usb ------------------------
		
		$usb 		= shell_exec('lsusb');
		$arrayTemp 	= explode("Bus", $usb);
		$x			= 0;
		
		for($i=0;$i<=count($arrayTemp);$i++)
		{
			if($arrayTemp[$i])
			{
				$usbArr[$x] 		= substr($arrayTemp[$i], 30);
				$x++;
			}
		}
		$this->view->usb = $usbArr;
		
		//---------------------- data SCSI ---------------------------
		
		$scsi 		= shell_exec('cat /proc/scsi/scsi');
		$scsi 		= substr($scsi, 18);
		$arrayTemp 	= explode("Host", $scsi);
		$x			= 0;
		
		for($i=0;$i<=count($arrayTemp);$i++)
		{
			if($arrayTemp[$i])
			{
				$vendor 		= substr($arrayTemp[$i], 45);
				$arrayVendor 	= explode("Model: ", $vendor);
				$arrayModel 	= explode("Rev: ", $arrayVendor[1]);
				$arrayType 		= explode("Rev: ", $arrayModel[1]);
				$type 			= substr($arrayModel[1], 13);
				$arrayType 		= explode("ANSI", $type);
				
				$scsiArr[$x]['vendor'] 	= $arrayVendor[0];
				$scsiArr[$x]['model'] 	= $arrayModel[0];
				$scsiArr[$x]['type'] 	= $arrayType[0];
				$x++;
			}
		}
		$this->view->scsi = $scsiArr;
		
		//----------------- data Network Usage ----------------------
		
		$networkDevice 	= shell_exec("cat /proc/net/dev | awk '{print $1}'");
		$networkDevice 	= substr($networkDevice, 13);
		$networkDevice 	= explode(":", $networkDevice);
		$x 				= 0;
		
		for($i=0;$i<=count($networkDevice);$i++)
		{
			if($networkDevice[$i])
			{
				$networkDevice2 = explode("\n", $networkDevice[$i]);
				if($i == 0)
				{
					$networkInterface[$x]['interface'] = $networkDevice2[0];
					$x++;
				}
				else
				{
					$networkInterface[$x]['interface'] = $networkDevice2[1];
					
					$received = 0;
					if($networkDevice2[0])
					{
						$received = $networkDevice2[0]/1048576;
					}
					$received = round($received,2);
					$networkInterface[$x-1]['received'] = $received;
					$x++;
				}
			}
		}
		
		$networkDeviceSend 			= shell_exec("cat /proc/net/dev | awk '{print $9}'");
		$networkDeviceSend 			= substr($networkDeviceSend, 17);
		$networkDeviceSend 			= explode("\n", $networkDeviceSend);
		
		$networkDeviceReceipeErr 	= shell_exec("cat /proc/net/dev | awk '{print $4}'");
		$networkDeviceReceipeErr 	= substr($networkDeviceReceipeErr, 17);
		$networkDeviceReceipeErr 	= explode("\n", $networkDeviceReceipeErr);
		
		$networkDeviceTransmitErr 	= shell_exec("cat /proc/net/dev | awk '{print $11}'");
		$networkDeviceTransmitErr 	= substr($networkDeviceTransmitErr, 17);
		$networkDeviceTransmitErr 	= explode("\n", $networkDeviceTransmitErr);
		
		$networkDeviceReceipeDrop 	= shell_exec("cat /proc/net/dev | awk '{print $5}'");
		$networkDeviceReceipeDrop 	= substr($networkDeviceReceipeDrop, 17);
		$networkDeviceReceipeDrop 	= explode("\n", $networkDeviceReceipeDrop);
		
		$networkDeviceTransmitDrop 	= shell_exec("cat /proc/net/dev | awk '{print $12}'");
		$networkDeviceTransmitDrop 	= substr($networkDeviceTransmitDrop, 17);
		$networkDeviceTransmitDrop 	= explode("\n", $networkDeviceTransmitDrop);
		
		$x 							= 0;
		
		for($i=0;$i<=count($networkDeviceSend);$i++)
		{
			$networkInterface[$x]['send'] 	= round(($networkDeviceSend[$i]/1048576),2);
			$networkInterface[$x]['err'] 	= $networkDeviceReceipeErr[$i]+$networkDeviceTransmitErr[$i];
			$networkInterface[$x]['drop'] 	= $networkDeviceReceipeDrop[$i]+$networkDeviceTransmitDrop[$i];
			$x++;
		}
		//Zend_Debug::dump($networkInterface);
		$this->view->networkDevice = $networkInterface;
		
		//--------------------- cek Memory Usage ------------------------------
		
		$memoryUsage['ram'] = array('total' => 0, 'free' => 0, 'used' => 0, 'percent' => 0);
		$memoryUsage['swap'] = array('total' => 0, 'free' => 0, 'used' => 0, 'percent' => 0);
		$memoryUsage['devswap'] = array();

		$bufr 	= shell_exec("cat /proc/meminfo");
		if ( $bufr != "ERROR" ) 
		{
			$bufe = explode("\n", $bufr);
			
			foreach( $bufe as $buf ) 
			{
				if (preg_match('/^MemTotal:\s+(.*)\s*kB/i', $buf, $ar_buf)) 
				{
				  $memoryUsage['ram']['total'] = $ar_buf[1];
				} 
				else if (preg_match('/^MemFree:\s+(.*)\s*kB/i', $buf, $ar_buf)) 
				{
				  $memoryUsage['ram']['free'] = $ar_buf[1];
				} 
				else if (preg_match('/^Cached:\s+(.*)\s*kB/i', $buf, $ar_buf)) 
				{
				  $memoryUsage['ram']['cached'] = $ar_buf[1];
				} 
				else if (preg_match('/^Buffers:\s+(.*)\s*kB/i', $buf, $ar_buf)) 
				{
				  $memoryUsage['ram']['buffers'] = $ar_buf[1];
				} 
			} 

			$memoryUsage['ram']['used'] = $memoryUsage['ram']['total'] - $memoryUsage['ram']['free'];
			$memoryUsage['ram']['percent'] = round(($memoryUsage['ram']['used'] * 100) / $memoryUsage['ram']['total']);
		  
			// values for splitting memory usage
			if (isset($memoryUsage['ram']['cached']) && isset($memoryUsage['ram']['buffers'])) 
			{
				$memoryUsage['ram']['app'] = $memoryUsage['ram']['used'] - $memoryUsage['ram']['cached'] - $memoryUsage['ram']['buffers'];
				$memoryUsage['ram']['app_percent'] = round(($memoryUsage['ram']['app'] * 100) / $memoryUsage['ram']['total']);
				$memoryUsage['ram']['buffers_percent'] = round(($memoryUsage['ram']['buffers'] * 100) / $memoryUsage['ram']['total']);
				$memoryUsage['ram']['cached_percent'] = round(($memoryUsage['ram']['cached'] * 100) / $memoryUsage['ram']['total']);
			}

			$bufr 	= shell_exec("cat /proc/swaps");
			if ( $bufr != "ERROR" ) 
			{
				$swaps = explode("\n", $bufr);
				for ($i = 1; $i < (sizeof($swaps)); $i++) 
				{
					if( trim( $swaps[$i] ) != "" ) 
					{
						$ar_buf = preg_split('/\s+/', $swaps[$i], 6);
						$memoryUsage['devswap'][$i - 1] = array();
						$memoryUsage['devswap'][$i - 1]['dev'] = $ar_buf[0];
						$memoryUsage['devswap'][$i - 1]['total'] = $ar_buf[2];
						$memoryUsage['devswap'][$i - 1]['used'] = $ar_buf[3];
						$memoryUsage['devswap'][$i - 1]['free'] = ($memoryUsage['devswap'][$i - 1]['total'] - $memoryUsage['devswap'][$i - 1]['used']);
						$memoryUsage['devswap'][$i - 1]['percent'] = round(($ar_buf[3] * 100) / $ar_buf[2]);
						$memoryUsage['swap']['total'] += $ar_buf[2];
						$memoryUsage['swap']['used'] += $ar_buf[3];
						$memoryUsage['swap']['free'] = $memoryUsage['swap']['total'] - $memoryUsage['swap']['used'];
						$memoryUsage['swap']['percent'] = round(($memoryUsage['swap']['used'] * 100) / $memoryUsage['swap']['total']);
					}
				} 
			}
		}
		
		$this->view->memoryUsage = $memoryUsage;
		
		$serverProcessInformation 	= shell_exec("vmstat 1 5");
		$this->view->serverProcessInformation = $serverProcessInformation;
		
		//------------------------------buat cek conection coolpay 1 -------------------------
	/*	
		$ch = curl_init($config['coolpay1']['url']);//'http://116.90.162.173'); //
		curl_setopt($ch, CURLOPT_HEADER, 1); 
		$c = curl_exec($ch);	
		$this->view->conn_coolpay1                = curl_getinfo($ch, CURLINFO_TOTAL_TIME);//CURLINFO_CONNECT_TIME);
		curl_close($ch);
	*/
		//------------- cek conection ESB -----------------------------------
		
		$time_start 	= $this->microtime_float();
		
		$data =  new Service_Account('2003560507','016');	
		$result = $data->accountInquiry();	
		
		$CHECKESB = 'Connection Ok';
		if($result['ResponseCode'] == 'XT')
		{
			$CHECKESB = 'Connection Fail';
		}
		
		$time_end 				= $this->microtime_float();
		$timeESB 				= $time_end - $time_start;
		$this->view->timeESB 	= $timeESB;
		$this->view->CHECKESB 	= $CHECKESB;
		
		//-----------------------cek XCOM connection --------------------------------
		
		$sftpServer 	= $config['FTP_EOD_SERVER'];
		$sftpPort 		= $config['FTP_EOD_PORT'];
		$time_start 	= $this->microtime_float();
		
		$xCom 	= shell_exec("telnet $sftpServer $sftpPort");
		$CHECKXCOM = 'Connection Ok';
		if($xCom == null)
		{
			$CHECKXCOM = 'Connection Fail';
		}
		
		$time_end 				= $this->microtime_float();
		$timeXCOM 				= $time_end - $time_start;
		$this->view->timeXCOM 	= $timeXCOM;
		$this->view->CHECKXCOM 	= $CHECKXCOM;
		
		//-----------------------cek SMTP  connection --------------------------------
		
		$smtpServer 	= $config['email']['smtpserver'];
		$smtpPort 		= $config['email']['smtpport'];
		$time_start 	= $this->microtime_float();
		
		$xCom 	= shell_exec("telnet $sftpServer $sftpPort");
		$CHECKSMTP = 'Connection Ok';
		if($xCom == null)
		{
			$CHECKSMTP = 'Connection Fail';
		}
		
		$time_end 				= $this->microtime_float();
		$timeSMTP 				= $time_end - $time_start;
		$this->view->timeSMTP 	= $timeSMTP;
		$this->view->CHECKSMTP 	= $CHECKSMTP;
		
		//---------------------- Mounted File System ----------------
		
		$mountPartition 	= shell_exec("df -T -h | awk '{print $1}'");
		$mountPartition 	= substr($mountPartition, 11);
		$mountPartition 	= explode("\n", $mountPartition);
		
		$mountType     		= shell_exec("df -T -h | awk '{print $2}'");
		$mountType 			= substr($mountType, 5);
		$mountType 			= explode("\n", $mountType);
		
		$mountSize   		= shell_exec("df -T -h | awk '{print $3}'");
		$mountSize 			= substr($mountSize, 5);
		$mountSize 			= explode("\n", $mountSize);
		
		$mountUsed  		= shell_exec("df -T -h | awk '{print $4}'");
		$mountUsed 			= substr($mountUsed, 5);
		$mountUsed 			= explode("\n", $mountUsed);
		
		$mountAvail  		= shell_exec("df -T -h | awk '{print $5}'");
		$mountAvail 		= substr($mountAvail, 6);
		$mountAvail 		= explode("\n", $mountAvail);
		
		$mountUsedPercent 	= shell_exec("df -T -h | awk '{print $6}'");
		$mountUsedPercent 	= substr($mountUsedPercent, 5);
		$mountUsedPercent 	= explode("\n", $mountUsedPercent);
		
		$mountMount 		= shell_exec("df -T -h | awk '{print $7}'");
		$mountMount 		= substr($mountMount, 11);
		$mountMount 		= explode("\n", $mountMount);
		
		$x = 0;
		
		for($i=0;$i<=count($mountPartition);$i++)
		{
			if($mountPartition != null)
			{
				$mount[$x]['Mount'] 	= $mountMount[$i];
				$mount[$x]['Type'] 		= $mountType[$i];
				$mount[$x]['Partition'] = $mountPartition[$i];
				$mount[$x]['Percent'] 	= $mountUsedPercent[$i];
				$mount[$x]['Free'] 		= $mountAvail[$i];
				$mount[$x]['Used'] 		= $mountUsed[$i];
				$mount[$x]['Size'] 		= $mountSize[$i];
				$x++;
			}
		}
		
		$this->view->mount 	= $mount;
		
		//----------------- cek conection file server ---------------
		
		$time_start 	= $this->microtime_float();
		
		$fileServer 	= shell_exec("telnet 10.110.10.132 445");
		$CHECKfileServer = 'Connection Ok';
		if($fileServer == null)
		{
			$CHECKfileServer = 'Connection Fail';
		}
		
		$time_end 						= $this->microtime_float();
		$timefileServer 				= $time_end - $time_start;
		$this->view->timefileServer 	= $timefileServer;
		$this->view->CHECKfileServer 	= $CHECKfileServer;
	}
}
