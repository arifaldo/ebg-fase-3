<?php

require_once 'Zend/Controller/Action.php';

class bgapprovalmatrix_IndexController extends customer_Model_Customer
{
  protected $_moduleDB = 'CST';

  public function indexAction()
  {
    if (!$this->view->hasPrivilege("VBAM")) {
      return $this->_redirect("/home/dashboard");
    }

    Application_Helper_General::writeLog("VBAM", "View Bank Approval Matrix");

    $select2 = $this->_db->select()
      ->from('TEMP_APP_BGBOUNDARY');
    $cek = $select2->query()->FetchAll();
    if ($cek) {
      $docErr = "*" . $this->language->_('No changes allowed for this record while awaiting approval for previous change') . "";
      $this->view->error = $docErr;
    }

    $this->_helper->layout()->setLayout('newlayout');

    $selectold = $this->_db->select()
      ->from(array('A' => 'M_APP_GROUP_BUSER'))
      ->query()->FetchAll();

    $oldAppGroupArray = array();
    $oldgroup = array();

    foreach ($selectold as $row) {
      $oldAppGroupArray[$row['GROUP_NAME']][] = $row['BUSER_ID'];
      $oldgroup[$row['GROUP_NAME']] = $row['GROUP_BUSER_ID'];
    }
    $this->view->datalamagroup = $oldgroup;
    $this->view->datalama = $oldAppGroupArray;

    $arrTraTypenew = array();
    $arrTraTypenew['50'] = 'Cash Collateral';
    $arrTraTypenew['51'] = 'Non Cash Collateral';
    $this->view->paymenttype    = $arrTraTypenew;
    //    print_r($paytypeall);die;
    //matrix



    $current = $this->_db->select()
      ->from(array('A' => 'M_APP_BGBOUNDARY'), array("A.*", 'CCY_BOUNDARY', 'BOUNDARY_MIN', 'BOUNDARY_MAX', 'BOUNDARY_ID', 'POLICY', 'TRANSFER_TYPE'))
      ->joinLeft(["B" => "M_BUSER"], "A.LASTUPDATEDBY = B.BUSER_ID", ["LU_NAME" => "B.BUSER_NAME"]);

    $current->where("A.BOUNDARY_ISUSED LIKE '1'");
    $rcurrent = $current->query()->fetchAll();
    $oldSuggestArray = array();
    foreach ($rsuggested as $row) {
      $oldSuggestArray[$row['TRANSFER_TYPE']][] = $row['BOUNDARY_MAX'];
    }
    $this->view->oldSuggest = $oldSuggestArray;
    $this->view->curlist = $rcurrent;

    $currentccy = $this->_db->select()
      ->from(array('A' => 'M_APP_BGBOUNDARY'), array('CCY_BOUNDARY', 'TRANSFER_TYPE'));

    $currentccy->where("A.BOUNDARY_ISUSED LIKE '1'");
    $currentccy->group('A.CCY_BOUNDARY');
    $currentccy->group('A.TRANSFER_TYPE');
    $rcurrentccy = $currentccy->query()->FetchAll();
    $this->view->curlistccy = $rcurrentccy;




    // Zend_Debug::dump($oldAppGroupArray);die;
    $this->view->cek = 1;

    if (!$this->_request->isPost()) {
      Application_Helper_General::writeLog('AGCL', 'Viewing Update Approver Group');
    }
  }
}
