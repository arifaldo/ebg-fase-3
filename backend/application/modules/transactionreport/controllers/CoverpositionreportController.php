<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class transactionreport_CoverpositionreportController extends Application_Main 
{	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		
//		$data = $this->_db->select()->distinct()
//					->from(array('T_TRANSACTION'),array('SERVICE_NAME'))
////					->order('SERVICE_NAME ASC')
//					->where("SERVICE_NAME != ''")
//					->query()->fetchAll();
//
//    	$this->view->serviceTypeArr = $data;

		$getPayment = new transactionreport_Model_Transactionreport();
		$arrCcyId    = $getPayment->getCCYList();
// 		print_r($arrCcyId);die;
		$listCcyId = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$listCcy = array_merge($listCcyId,Application_Helper_Array::listArray($arrCcyId,'CCY_ID','CCY_ID'));
		$this->view->arrCcyId 		= $listCcy;
		
// 		$billerCharges = new setbillercharges_Model_Setbillercharges();
    	$serviceTypeArr 	 = $getPayment->getServiceType();
    	$this->view->serviceTypeArr = $serviceTypeArr;
    	
		
		$filterSend = array();
		
		//$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		//Zend_Debug::dump($arrPayType);
		unset($arrPayType['8']);
		
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$arrTraTypeRaw 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		
		$listUserArr = $this->getUserID();
		$listUser = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$listUser = array_merge($listUser,Application_Helper_Array::listArray($listUserArr,'USER_ID','USER_ID'));
// 		var_dump($arrPayStatus);die;
// 		Zend_Debug::dump($arrPayStatus);
		unset($arrPayStatus[10]);
		unset($arrPayStatus[15]);
		unset($arrPayStatus[16]);
		unset($arrPayStatus[1]);
		unset($arrPayStatus[2]);
		unset($arrPayStatus[3]);
		
		
		foreach($arrPayType as $key => $value){
			if($key != 3){
				$optpaytypeRaw[$key] = $this->language->_($value);
			}
		}
		
		$this->view->arrPayType 	= $optpaytypeRaw;
		$this->view->arrPayStatus 	= $arrPayStatus;
		$this->view->arrTraType 	= $arrTraTypeRaw;
		$this->view->listUser 		= $listUser;
		
		$filterSend = array();
		
		$custid = $this->language->_('Company ID');
		$custname = $this->language->_('Company Name');
		$paymentref = $this->language->_('Payment Ref');
		$beneficiaryaccount = $this->language->_('Beneficiary Account');
		$paymentref = $this->language->_('Payment Ref#');
		$trxid = $this->language->_('TRX ID');		
		$traceNo = $this->language->_('Trace No');
		$createddate = $this->language->_('Created Date');
		$beneficiarytype = $this->language->_('Beneficiary Type');
		$updateddate = $this->language->_('Updated Date');
		$paymentdate = $this->language->_('Payment Date');
		$sourceaccount = $this->language->_('Source Account');
		$paymenttype = $this->language->_('Payment Type');
		$paymentstatus = $this->language->_('Payment Status');
		$amount = $this->language->_('Currency / Amount');
		$fee = $this->language->_('Fee');
		$amountbuy = $this->language->_('Amount Buy');
		$ratebuy = $this->language->_('Rate Buy');
		$amountsell = $this->language->_('Amount Sell');
		$ratesell = $this->language->_('Rate Sell');
		$equivalen = $this->language->_('Equivalent Amount in IDR');
		
			$fields = array	(	
							'companylogin'  		=> array(
															'field' => 'CUST_ID',
															'label' => $custid,
															'sortable' => true
														),
							'companyname'  		=> array(
															'field' => 'CUST_NAME',
															'label' => $custname,
															'sortable' => true
														),
							'payref'  			=> array(
															'field' => 'PS_NUMBER',
															'label' => $paymentref,
															'sortable' => true
														),							
							'sourceacount'  		=> array(
															'field' => 'ACCT_SOURCE',
															'label' => 'Source Acct No/ Source Acct Name/CCY',
															'sortable' => true
							),
							
							'destinationacount'  	=> array(
															'field' => 'ACCT_BENE',
															'label' => 'Destination Acct No/ Destination Acct Name/CCY',
															'sortable' => true
														),
							'amountbuy'  			=> array(
															'field' => 'BUY_AMOUNT',
															'label' => $amountbuy,
															'sortable' => true
														),
							'ratebuy'  			=> array(
															'field' => 'RATE_BUY',
															'label' => $ratebuy,
															'sortable' => true
														),
							'amountsell'  			=> array(
														'field' => 'SELL_AMOUNT',
														'label' => $amountsell,
														'sortable' => true
													),
							'ratesell'  			=> array(
														'field' => 'RATE_SELL',
														'label' => $ratesell,
														'sortable' => true
												),

							'equivalen'  			=> array(
															'field' => 'AMT_EQ',
															'label' => $equivalen,
															'sortable' => true
														),
							'transfertype'  			=> array(
															'field' => 'TRANS_TYPE',
															'label' => $this->language->_('Transfer Type'),
															'sortable' => true
													),
							'date'  			=> array(
															'field' => 'TRANS_DATE',
															'label' => $this->language->_('Date'),
															'sortable' => true
													),
																					
						);

		$filterlist = array('PS_TRFDATE','COMP_ID','COMP_NAME','TRFTYPE','PAY_REF','CCY');
		
		$this->view->filterlist = $filterlist;
		
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$filter_clear 		= $this->_getParam('filter_clear');
		$currency 			= $this->_getParam('currency');	
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		if($sortBy=='U.USER_ID'){
			$sortBy = 'PS_CREATED';
		}
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$data = array();
		
		$filterArr = array('userid'    			=> array('StripTags'),
							'COMP_ID'    			=> array('StripTags'),
							'COMP_NAME'    			=> array('StripTags'),
						   'CCY'    			=> array('StripTags'),
	                       'username'  			=> array('StripTags','StringTrim','StringToUpper'),
						   
						   'PAY_REF' 		=> array('StripTags','StringTrim','StringToUpper'),
						   						   
						   'PS_TRFDATE' 		=> array('StripTags','StringTrim'),
						   'PS_TRFDATE_END' 		=> array('StripTags','StringTrim'),
						   'createdfrom' 		=> array('StripTags','StringTrim'),
						   'createdto' 			=> array('StripTags','StringTrim'),
						   'updatedfrom' 		=> array('StripTags','StringTrim'),
						   'updatedto' 			=> array('StripTags','StringTrim'),
						   
						   'acctsource' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'beneaccount' 		=> array('StripTags','StringTrim','StringToUpper'),
						  						   
						   'paymentstatus' 		=> array('StripTags'),
						   'paymenttype' 		=> array('StripTags'),
						   'TRFTYPE' 		=> array('StripTags'),
						   'providerName' 		=> array('StripTags','StringTrim','StringToUpper'),
				   		   'serviceCategory' 	=> array('StripTags'),
				   		   'traceNo' 	=> array('StripTags','StringTrim','StringToUpper'),
						   'currency' 	=> array('StripTags','StringTrim','StringToUpper'),
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','COMP_NAME','PAY_REF','TRFTYPE','CCY');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_TRFDATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}

		$currency = $dataParamValue['CCY'];

		if (empty($currency)) { 
			$currency = 'IDR';
		}
    	
    	$this->view->currency = $currency;

		
		//echo "<pre>";
		//print_r ($dataParamValue);

			if(!empty($this->_request->getParam('trfdate'))){
				$transferarr = $this->_request->getParam('trfdate');
					$dataParamValue['PS_TRFDATE'] = $transferarr[0];
					$dataParamValue['PS_TRFDATE_END'] = $transferarr[1];
			}
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
		
						'userid' 		=> array(array('InArray', array('haystack' => array_keys($listUserArr)))),
						'username' 		=> array(),	
						'COMP_ID' 		=> array(),
						'COMP_NAME' 	=> array(),		
						'CCY' 		=> array(),
						'PAY_REF' 	=> array(),	
												
						'PS_TRFDATE' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_TRFDATE_END'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'createdfrom' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'createdto'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'updatedfrom' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'updatedto'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'acctsource' 	=> array(),	
						'beneaccount' 	=> array(),	
						
						'paymentstatus' => array(array('InArray', array('haystack' => array_keys($arrPayStatus)))),							
						'paymenttype' 	=> array(array('InArray', array('haystack' => array_keys($optpaytypeRaw)))),	
						'TRFTYPE' 	=> array(array('InArray', array('haystack' => array_keys($arrTraTypeRaw)))),	
						'providerName' => array(),	
						'serviceCategory' 	=>array(),	
						'traceNo' 	=>array(),	
						'currency' 	=>array(),	
						);

		
		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$fcompid 			= html_entity_decode($zf_filter->getEscaped('COMP_ID'));
		$fcompname 			= html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
		$fuserid 			= html_entity_decode($zf_filter->getEscaped('userid'));
		$fcurrency 			= html_entity_decode($zf_filter->getEscaped('CCY'));
		$fusername			= html_entity_decode($zf_filter->getEscaped('username'));
		$fpaymentreff 		= html_entity_decode($zf_filter->getEscaped('PAY_REF'));
		
		$fcreatedfrom 		= html_entity_decode($zf_filter->getEscaped('createdfrom'));
		$fcreatedto 		= html_entity_decode($zf_filter->getEscaped('createdto'));
		
		$ftransferfrom 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE'));
		$ftransferto 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE_END'));
		
		$fupdatedfrom		= html_entity_decode($zf_filter->getEscaped('updatedfrom'));
		$fupdatedto			= html_entity_decode($zf_filter->getEscaped('updatedto'));
		
		$facctsource 		= html_entity_decode($zf_filter->getEscaped('acctsource'));
		$fbeneaccount 		= html_entity_decode($zf_filter->getEscaped('beneaccount'));
		$fpaymentstatus 	= html_entity_decode($zf_filter->getEscaped('paymentstatus'));
		$fpaymenttype 		= html_entity_decode($zf_filter->getEscaped('paymenttype'));
		$ftransfertype 		= html_entity_decode($zf_filter->getEscaped('TRFTYPE'));
		$PROVIDER_NAME		= html_entity_decode($zf_filter->getEscaped('providerName'));
		$serviceCategory		= html_entity_decode($zf_filter->getEscaped('serviceCategory'));
		$traceNo			= html_entity_decode($zf_filter->getEscaped('traceNo'));
		//$currency				= html_entity_decode($zf_filter->getEscaped('currency'));
		//print_r($currency);die;
		/*if($filter != 'Set Filter')
		{	
			$ftransferfrom = (date("d/m/Y"));
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = $ftransferfrom;
			$this->view->transferto  = $ftransferto;
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
		}*/
		
		if($filter == null)
		{	
			$ftransferfrom = (date("d/m/Y"));
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = $ftransferfrom;
			$this->view->transferto  = $ftransferto;
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
		} 
		
		//if($filter == 'Set Filter')
		if($filter == True)
		{
			
			if($fcompid!=null)
			{ 
				$filterSend['custid'] = $fcompid;
				$this->view->custid = $fcompid;
			}

			if($fcompname!=null)
			{ 
				$filterSend['custname'] = $fcompname;
				$this->view->custname = $fcompname;
			}
			
			if($fuserid!=null)
			{ 
				$filterSend['userid'] = $fuserid;
				$this->view->userid = $fuserid;
			}
			
			if($fusername!=null)
			{ 
				$filterSend['username'] = $fusername;
				$this->view->username = $fusername;
			}
			
			if($fpaymentreff!=null)
			{ 
				$filterSend['paymentreff'] = $fpaymentreff;
				$this->view->paymentreff = $fpaymentreff;
			}
			
			if($ftransferfrom!=null)
			{
				$this->view->transferfrom = $ftransferfrom;
				$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
				$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['transferfrom'] = $transferfrom;
			}
			
			if($ftransferto!=null)
			{
				$this->view->transferto = $ftransferto;
				$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
				$transferto  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['transferto'] = $transferto;
			}
			
			if($fcreatedfrom!=null)
			{
				$this->view->createdfrom = $fcreatedfrom;
				$FormatDate 	= new Zend_Date($fcreatedfrom, $this->_dateDisplayFormat);
				$fcreatedfrom  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['createdfrom'] = $fcreatedfrom;
				
			}
			
			if($fcreatedto!=null)
			{
				$this->view->createdto = $fcreatedto;
				$FormatDate 	= new Zend_Date($fcreatedto, $this->_dateDisplayFormat);
				$fcreatedto  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['createdto'] = $fcreatedto;
				
			}
			
			if($fupdatedfrom!=null)
			{
				$this->view->updatedfrom = $fupdatedfrom;
				$FormatDate 	= new Zend_Date($fupdatedfrom, $this->_dateDisplayFormat);
				$fupdatedfrom  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['updatedfrom'] = $fupdatedfrom;
				
			}
			
			if($fupdatedto!=null)
			{
				$this->view->updatedto = $fupdatedto;
				$FormatDate = new Zend_Date($fupdatedto, $this->_dateDisplayFormat);
				$fupdatedto  = $FormatDate->toString($this->_dateDBFormat);
				$filterSend['updatedto'] = $fupdatedto;
				
			}
			
			if($facctsource!=null)
			{ 
				$filterSend['acctsource'] = $facctsource; 
				$this->view->acctsource = $facctsource;
			}
			
			if($fbeneaccount!=null)	
			{ 
				$filterSend['beneaccount'] = $fbeneaccount;
				$this->view->beneaccount = $fbeneaccount;
			}
			
			if($fpaymentstatus!=null)
			{ 
				$filterSend['paymentstatus'] = $fpaymentstatus;
				$this->view->paymentstatus = $fpaymentstatus;		
			}
			
			if($fpaymenttype!=null)
			{ 
				$filterSend['paymenttype'] = $fpaymenttype;
				$this->view->paymenttype = $fpaymenttype;
			}
			
			if($ftransfertype != null)
			{
				$filterSend['transfertype'] = $ftransfertype;
				$this->view->transfertype = $ftransfertype;
			}
			
			if($PROVIDER_NAME!=null)
			{ 
				$filterSend['providerName'] = $PROVIDER_NAME;
				$this->view->providerName = $PROVIDER_NAME;
			}
			if($serviceCategory!=null)
			{ 
				$filterSend['serviceCategory'] = $serviceCategory;
				$this->view->serviceCategory = $serviceCategory;
			}
			if($traceNo!=null)
			{ 
				$filterSend['traceNo'] = $traceNo;
				$this->view->traceNo = $traceNo;
			}
			
			if($currency!=null)
			{ 
				$filterSend['CCYID'] = $currency;
				$this->view->CCYID = $currency;
			}
			//print_r($filterSend);die;
		}
		
		
		else if($filter_clear == TRUE){
			$ftransferfrom = '00/00/0000';
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = '';
			$this->view->transferto  = '';
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
			
	
			
		}
		
		$sorting = $sortBy.' '.$sortDir;
		$result = $getPayment -> getPaymentCoverPosition($filterSend,$sorting);
		
		$resultcount = $getPayment -> getPaymentCoverPositionCount($filterSend,$sorting);
//         print_r($resultcount);die;

		//summary total
		$totalnumbertx = 0;
		$totalvaluetx = 0;
		foreach($result as $key =>$dt)
		{

			$totalnumbertx = $totalnumbertx + $dt["TRA_AMOUNT"];
			$totalvaluetx  = $totalvaluetx + $dt["TRANSFER_FEE"];

		}

		@$avgnumbertx 	= round($totalnumbertx / count($result),2);
		@$avgvaluetx 	= round($totalvaluetx / count($result),2);
		
		
		//count totalhit and average:
		$totalvaluetx 	= Application_Helper_General::displayMoney($totalvaluetx);
		$totalnumbertx 	= Application_Helper_General::displayMoney($totalnumbertx);
		
		
		$this->view->totalresult 	= $resultcount;
		$this->view->totalnumbertx 	= $totalnumbertx;
		$this->view->totalvaluetx 	= $totalvaluetx;
		
		foreach($result as $key=>$value)
		{
			if($value['TRANSFER_FEE']==''){
				$result[$key]["TRANSFER_FEE"] = '0.00';
			}
			
			if(!empty($value['TRA_AMOUNT'])){
// 			    print_r($value);
			    if($value['SELL_AMOUNT']==''){
			      
			        $result[$key]["TOTAL_AMOUNT"] = $value['BUY_AMOUNT']*$value['RATE_BUY'];
			    }else{
			        $result[$key]["TOTAL_AMOUNT"] = $value['SELL_AMOUNT']*$value['RATE_SELL'];
			    }
// 			    die;
			}else{
			    $result[$key]["TOTAL_AMOUNT"] = '0.00';
			}
			$result[$key]["LOG_DATE"] = Application_Helper_General::convertDate($value["LOG_DATE"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		}
			
		if ($csv || $pdf) 
		{
				$arr_print = $result;

				$sort_cetak = $this->_getParam('sortby');
				$sortDir_cetak = $this->_getParam('sortdir');
				
				$sortDir_cetak = (Zend_Validate::is($sortDir_cetak,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir_cetak : 'desc';
				$sorting_cetak = $sort_cetak.' '.$sortDir_cetak;
				$arr_cetak = $getPayment ->getPaymentCoverPosition($filterSend,$sorting_cetak);
				$arr = $arr_cetak;
			$i = 0;
			$p = 0;
			$no = 0;
			foreach($result as $key => $val)
			{
				$no++;
				$biller = (!empty($val["PS_BILLER_ID"])?$val["PS_BILLER_ID"]:'');
				$resultTes = $getPayment->getProvider($biller);
				
				unset($arr[$key]['TRA_STATUS']);
				
				unset($arr[$key]['PS_CREATED']);
				
				unset($arr[$key]['PS_EFDATE']);
				unset($arr[$key]['PS_UPDATED']);
				unset($arr[$key]['BANK_NAME']);
				unset($arr[$key]['PS_STATUS']);
				unset($arr[$key]['TRACE_NO']);
				unset($arr[$key]['BANK_RESPONSE']);
				unset($arr[$key]['TRANSFER_FEE']);
				
				unset($arr[$key]['BILLER_ORDER_ID']);
				unset($arr[$key]['PS_BILLER_ID']);
				unset($arr[$key]['LLD_TRANSACTION_PURPOSE']);
				unset($arr[$key]['BENEFICIARY_CATEGORY']);
				unset($arr[$key]['NOSTRO_NAME']);
				unset($arr[$key]['TRA_AMOUNT']);
				$pstype = $arr[$key]['PS_TYPE'];
				unset($arr[$key]['PROVIDER_NAME']);
				unset($arr[$key]['PS_TYPE']);
				unset($arr[$key]['PS_CCY']);
				unset($arr[$key]['TRX_ID']);
				
				$arr[$key]['BUY_AMOUNT'] = ''; 
				if($val["RATE_BUY"]==''){
					$arr[$key]["RATE_BUY"] = '-';
				}else{
					$arr[$key]["RATE_BUY"] = 'IDR '.Application_Helper_General::displayMoney($val["RATE_BUY"]);
				}
				
				if($val["RATE_SELL"]==''){
					$arr[$key]["RATE_SELL"] = '-';
				}else{
					$arr[$key]["RATE_SELL"] = 'IDR '.Application_Helper_General::displayMoney($val["RATE_SELL"]);
				}
				
				if($val["BUY_AMOUNT"]==''){
					$arr[$key]["BUY_AMOUNT"] = '-';
				}else{
					$arr[$key]["BUY_AMOUNT"] = $val["BUY_CCY"].' '.Application_Helper_General::displayMoney($val["BUY_AMOUNT"]);
				}
					
				if($val["SELL_AMOUNT"]==''){
					$arr[$key]["SELL_AMOUNT"] = '-';
				}else{
					$arr[$key]["SELL_AMOUNT"] = $val["SELL_CCY"].' '.Application_Helper_General::displayMoney($val["SELL_AMOUNT"]);
				}
// 				$crdate_clean = str_replace('00:00:00','',$crdate);
// 				$arr[$key]["PS_EFDATE"] = $crdate_clean;
				if(!empty($value['TRA_AMOUNT'])){
				    if($value['BUY_AMOUNT']=='0.00'){
				        $result[$key]["TOTAL_AMOUNT"] = 'IDR '.$value['BUY_AMOUNT']*$value['RATE_BUY'];
				    }else{
				        $result[$key]["TOTAL_AMOUNT"] = 'IDR '.$value['SELL_AMOUNT']*$value['RATE_SELL'];
				    }
				}else{
				    $result[$key]["TOTAL_AMOUNT"] = '0.00';
				}
				unset($arr[$key]["EQUIVALENT_AMOUNT_IDR"]);
// 				 = 'IDR '.Application_Helper_General::displayMoney($val["EQUIVALENT_AMOUNT_IDR"]);
				//print_r($val);die;
				if($val['TRANSFER_TYPE']=='7'){
					$arr[$key]['TRANS_TYPE'] = 'Mayapada (Sell)';
				}else if($val['TRANSFER_TYPE']=='8'){
					$arr[$key]['TRANS_TYPE'] = 'Mayapada (Buy)';
				}else if($val['TRANSFER_TYPE']=='3'){
					$arr[$key]['TRANS_TYPE'] = 'OUR';
				}else if($val['TRANSFER_TYPE']=='4'){
					$arr[$key]['TRANS_TYPE'] = 'SHA';
				}	
// 				$arr[$key]["TRANSFER_FEE"] = Application_Helper_General::displayMoney($val["TRANSFER_FEE"]);
// 				$arr[$key]["PS_BILLER_ID"] = $resultTes[0]["SERVICE_NAME"];
// 				$arr[$i++]['PROVIDER_NAME'] = $resultTes[0]["PROVIDER_NAME"];
				$p++;
				$crdate = $arr[$key]['PS_CREATE'];
				$arr[$i++]['PDATE'] = $crdate;
				unset($arr[$key]['TRANSFER_TYPE']);
				unset($arr[$key]['PS_CREATE']);
				unset($arr[$key]['BUY_CCY']);
				unset($arr[$key]['SELL_CCY']);
			}
// 			echo "<pre>";
// 			print_r($arr);die;
			if(!empty($resultcount)){
				$ccy_sell = $resultcount[0]['SELL_CCY'];
				$ccy_buy = $resultcount[0]['BUY_CCY'];
				if($resultcount[0]['SUMEQ']!='0.00'){
					$sumeq = 'IDR '.Application_Helper_General::displayMoney($resultcount[0]['SUMEQ']);
				}else{
					$sumeq = '-';
				}
				if($resultcount[0]['SUMBUY']!='0.00'){
					$sumbuy = $ccy_buy.' '.Application_Helper_General::displayMoney($resultcount[0]['SUMBUY']);
				}else{
					$sumbuy = '-';
				}
				if($resultcount[0]['AVGBUY']!='0.00'){
					$avgbuy = 'IDR '.Application_Helper_General::displayMoney($resultcount[0]['AVGBUY']);
				}else{
					$avgbuy = '-';
				}
				if($resultcount[0]['SUMSELL']!='0.00'){
					$sumsell = $ccy_sell.' '.Application_Helper_General::displayMoney($resultcount[0]['SUMSELL']);
				}else{
					$sumsell = '-';
				}
				if($resultcount[0]['AVGSELL']!='0.00'){
					$avgsell = 'IDR '.Application_Helper_General::displayMoney($resultcount[0]['AVGSELL']);
				}else{
					$avgsell = '-';
				}
					
			}
			$arr[$no+1]['USER_ID']	= 'Total';
			$arr[$no+1]['USER']	= ' ';
			$arr[$no+1]['USER1_ID']	= ' ';
			$arr[$no+1]['USER2_ID']	= ' ';
			$arr[$no+1]['USER3_ID']	= ' ';
			$arr[$no+1]['BUY_AMOUNT']	= $sumbuy;
			$arr[$no+1]['RATE_BUY']	= $avgbuy;
			$arr[$no+1]['SELL_AMOUNT']	= $sumsell;
			$arr[$no+1]['RATE_SELL']	= $avgsell;
			$arr[$no+1]['AMT_EQ']	= $sumeq;
			$arr[$no+1]['USER4_ID']	= ' ';
			$arr[$no+1]['USER5_ID']	= ' ';
			
			if($sumsell == '-' && $sumbuy == '-'){
				$nettext = '-';
			}else if($sumsell == '-'){
				$nettext = '(+) '.$sumbuy;
			}else if($sumbuy == '-'){
				$nettext = '(-) '.$sumsell;
			}else{
				$selisih = $resultcount[0]['SUMBUY']-$resultcount[0]['SUMSELL'];
				// 					print_r($this->totalresult[0]['SUMBUY']);
				if($selisih<0){
					$nettext = '(-) '.Application_Helper_General::displayMoney($selisih);
				}else{
					$nettext = '(+) '.Application_Helper_General::displayMoney($selisih);
				}
				// 					print_r($this->totalresult[0]['SUMSELL']);
				// 					die;
				// 					$nettext =
			}
			
			$arr[$no+2]['USER_ID']	= 'Net (Amount Buy � Amount Sell)';
			$arr[$no+2]['USER']	= ' ';
			$arr[$no+2]['USER1_ID']	= ' ';
			$arr[$no+2]['USER2_ID']	= ' ';
			$arr[$no+2]['USER3_ID']	= ' ';
			$arr[$no+2]['NET']	= $nettext;
			$arr[$no+2]['USER4_ID']	= ' ';
			$arr[$no+2]['USER5_ID']	= ' ';
			$arr[$no+2]['USER6_ID']	= ' ';
			$arr[$no+2]['USER7_ID']	= ' ';
			$arr[$no+2]['USER8_ID']	= ' ';
			$arr[$no+2]['USER9_ID']	= ' ';
	
//				unset($fields['username']);
//				unset($fields['updateddate']);
				
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			
			if($csv)
			{
// 				$arr[$key+1]['number']	= false;
// 				$arr[$key+1][0]	= '';
// 				$arr[$key+1][1]	= '';
// 				$arr[$key+1][2]	= '';
// 				$arr[$key+1][4]	= '';
// 				$arr[$key+1][5]	= '';
// 				$arr[$key+1][6]	= '';
// 				$arr[$key+1][7]	= '';
// 				$arr[$key+1][8]	= '';
// 				$arr[$key+1][9]	= '';
// 				$arr[$key+1][10]	= '';
				
// 				$arr[$key+1][11]	= 'Total';
// 				$arr[$key+1][12]	= $totalnumbertx;
// 				$arr[$key+1][13]	= $totalvaluetx;
// 				$arr[$key+1][14]	= '';
// 				$arr[$key+1][15]	= '';
// 				$arr[$key+1][16]	= '';
// 				$arr[$key+1][17]	= '';
				$this->_helper->download->csv($header,$arr,null,$this->language->_('Cover Position Report'));  
				Application_Helper_General::writeLog('RPPY','Export CSV Payment Report');
			}
			if($pdf)
			{
// 				$arr[$key+1]['number']	= false;
// 				$arr[$key+1][0]	= '';
// 				$arr[$key+1][1]	= '';
// 				$arr[$key+1][2]	= '';
// 				$arr[$key+1][4]	= '';
// 				$arr[$key+1][5]	= '';
// 				$arr[$key+1][6]	= '';
// 				$arr[$key+1][7]	= '';
// 				$arr[$key+1][8]	= '';
// 				$arr[$key+1][9]	= '';
// 				$arr[$key+1][10]	= '';
// 				$arr[$key+1][11]	= '';
// 				$arr[$key+1][12]	= 'Total';
// 				$arr[$key+1][13]	= $totalnumbertx;
// 				$arr[$key+1][14]	= $totalvaluetx;
// 				$arr[$key+1][15]	= '';
// 				$arr[$key+1][16]	= '';
// 				$arr[$key+1][17]	= '';
				//$arr[$key+1][18]	= '';
				$this->_helper->download->pdf($header,$arr,null,$this->language->_('Cover Position Report'));  
				Application_Helper_General::writeLog('RPPY','Export PDF Cover Position Report');
			}
			
			//Zend_Debug::dump($dataSQL);die;
		}else if($this->_request->getParam('print')){
				
				$arr_print = $result;

				$i = 0;
				foreach($result as $key => $val)
				{
					$biller = (!empty($val["PS_BILLER_ID"])?$val["PS_BILLER_ID"]:'');
					$resultTes = $getPayment->getProvider($biller);
					
					unset($arr_print[$key]['TRA_STATUS']);
					$crdate = $arr_print[$key]["PS_EFDATE"];
					unset($arr_print[$key]["PS_EFDATE"]);
					$crdate_clean = str_replace('00:00:00','',$crdate);
					$arr_print[$key]["PS_EFDATE"] = $crdate_clean;
					$arr_print[$key]["AMT_EQ"] = 'IDR '.Application_Helper_General::displayMoney($val['EQUIVALENT_AMOUNT_IDR']);
					$arr_print[$key]["TRANS_DATE"] = Application_Helper_General::convertDate($val["PS_CREATE"],$this->displayDateTimeFormat,$this->defaultDateFormat);
					if($val['TRANSFER_TYPE']=='7'){
					$arr_print[$key]['TRANS_TYPE'] = 'Mayapada (Sell)';
				}else if($val['TRANSFER_TYPE']=='8'){
					$arr_print[$key]['TRANS_TYPE'] = 'Mayapada (Buy)';
				}else if($val['TRANSFER_TYPE']=='3'){
					$arr_print[$key]['TRANS_TYPE'] = 'OUR';
				}else if($val['TRANSFER_TYPE']=='4'){
					$arr_print[$key]['TRANS_TYPE'] = 'SHA';
				}
					// 					print_r($row["RATE_BUY"]);die;
					if($val["RATE_BUY"]=='' || $val["RATE_BUY"]=='0.00'){
						$arr_print[$key]["RATE_BUY"] = '-';
					}else{
						$arr_print[$key]["RATE_BUY"] = 'IDR '.Application_Helper_General::displayMoney($val["RATE_BUY"]);
					}
					
					if($val["RATE_SELL"]==''){
						$arr_print[$key]["RATE_SELL"] = '-';
					}else{
						$arr_print[$key]["RATE_SELL"] = 'IDR '.Application_Helper_General::displayMoney($val["RATE_SELL"]);
					}
					
					if($val["BUY_AMOUNT"]=='' || $val["BUY_AMOUNT"]=='0.00'){
						$arr_print[$key]["BUY_AMOUNT"] = '-';
					}else{
						$arr_print[$key]["BUY_AMOUNT"] = $val["BUY_CCY"].' '.Application_Helper_General::displayMoney($val["BUY_AMOUNT"]);
					}
						
					if($val["SELL_AMOUNT"]==''){
						$arr_print[$key]["SELL_AMOUNT"] = '-';
					}else{
					    if($row['SELL_AMOUNT'] == '-'){
					        $row["PS_CCY"] = '';
					    }
						$arr_print[$key]["SELL_AMOUNT"] = $val["PS_CCY"].' '.Application_Helper_General::displayMoney($val["SELL_AMOUNT"]);
					}
// 					$arr_print[$key]["PS_CREATED"] = Application_Helper_General::convertDate($val["PS_CREATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
// 					$arr_print[$key]["PS_UPDATED"] = Application_Helper_General::convertDate($val["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
	//				$arr[$key]["PS_EFDATE"] = Application_Helper_General::convertDate($val["PS_EFDATE"],$this->view->_dateViewFormat);
// 					$arr_print[$key]["PS_EFDATE"] = Application_Helper_General::convertDate($val["PS_EFDATE"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
					$arr_print[$key]["TRA_AMOUNT"] = Application_Helper_General::displayMoney($val["TRA_AMOUNT"]);
					$arr_print[$key]["TRANSFER_FEE"] = Application_Helper_General::displayMoney($val["TRANSFER_FEE"]);
					$arr_print[$key]["PS_BILLER_ID"] = $resultTes[0]["SERVICE_NAME"];
					$arr_print[$i++]['PROVIDER_NAME'] = $resultTes[0]["PROVIDER_NAME"];
					
	//				unset($arr[$key]['USER_FULLNAME']);
	//				unset($arr[$key]['PS_UPDATED']);
				}

				if($this->_request->getParam('print') == 1){
				if(!empty($resultcount)){
						$ccy_sell = $resultcount[0]['SELL_CCY'];
						$ccy_buy = $resultcount[0]['BUY_CCY'];
						if($ccy_sell==''){
						    $ccy_sell = $ccy_buy;
						}else{
						    $ccy_buy = $ccy_sell;
						}
						if($resultcount[0]['SUMEQ']!='0.00'){
							$sumeq = 'IDR '.Application_Helper_General::displayMoney($resultcount[0]['SUMEQ']);
						}else{
							$sumeq = '-';
						}
						if($resultcount[0]['SUMBUY']!='0.00'){
							$sumbuy = $ccy_buy.' '.Application_Helper_General::displayMoney($resultcount[0]['SUMBUY']);
						}else{
							$sumbuy = '-';
						}
						if($resultcount[0]['AVGBUY']!='0.00'){
							$avgbuy = 'IDR '.Application_Helper_General::displayMoney($resultcount[0]['AVGBUY']);
						}else{
							$avgbuy = '-';
						}
						if($resultcount[0]['SUMSELL']!='0.00'){
							$sumsell = $ccy_sell.' '.Application_Helper_General::displayMoney($resultcount[0]['SUMSELL']);
						}else{
							$sumsell = '-';
						}
						if($resultcount[0]['AVGSELL']!='0.00'){
							$avgsell = 'IDR '.Application_Helper_General::displayMoney($resultcount[0]['AVGSELL']);
						}else{
							$avgsell = '-';
						}
							
					}
//				unset($fields['username']);
//				unset($fields['updateddate']);
				$arr_print[$key+1][0]	= '';
				$arr_print[$key+1][1]	= '';
				$arr_print[$key+1][2]	= '';
				$arr_print[$key+1][4]	= '';
				$arr_print[$key+1][5]	= '';
				$arr_print[$key+1][6]	= '';
				$arr_print[$key+1][7]	= '';
				$arr_print[$key+1][8]	= '';
				$arr_print[$key+1][9]	= '';
				$arr_print[$key+1][10]	= '';
				$arr_print[$key+1][11]	= '';
				$arr_print[$key+1]['CUST_ID']	= 'Total';
				$arr_print[$key+1]['BUY_AMOUNT']	= $sumbuy;
				$arr_print[$key+1]['SELL_AMOUNT']	= $sumsell;
				$arr_print[$key+1]['RATE_SELL']	= $avgsell;
				$arr_print[$key+1]['RATE_BUY']	= $avgbuy;
				$arr_print[$key+1]['AMT_EQ']	= $sumeq;
				
				if($sumsell == '-' && $sumbuy == '-'){
					$nettext = '-';
				}else if($sumsell == '-'){
					$nettext = '(+) '.$sumbuy;
				}else if($sumbuy == '-'){
					$nettext = '(-) '.$sumsell;
				}else{
					$selisih = $resultcount[0]['SUMBUY']-$resultcount[0]['SUMSELL'];
					// 					print_r($this->totalresult[0]['SUMBUY']);
					if($selisih<0){
						$nettext = '(-) '.Application_Helper_General::displayMoney($selisih);
					}else{
						$nettext = '(+) '.Application_Helper_General::displayMoney($selisih);
					}
					// 					print_r($this->totalresult[0]['SUMSELL']);
					// 					die;
					// 					$nettext =
				}
				
				$arr_print[$key+2][0]	= '';
				$arr_print[$key+2][1]	= '';
				$arr_print[$key+2][2]	= '';
				$arr_print[$key+2][4]	= '';
				$arr_print[$key+2][5]	= '';
				$arr_print[$key+2][6]	= '';
				$arr_print[$key+2][7]	= '';
				$arr_print[$key+2][8]	= '';
				$arr_print[$key+2][9]	= '';
				$arr_print[$key+2][10]	= '';
				$arr_print[$key+2][11]	= '';
				$arr_print[$key+2]['CUST_ID']	= 'Net (Amount Buy � Amount Sell)';
				$arr_print[$key+2]['BUY_AMOUNT']	= $nettext;
				
				
	            $this->_forward('print', 'index', 'widget', array('data_content' => $arr_print, 'data_caption' => 'Cover Position Report', 'data_header' => $fields));
	       	}

		}
		else
		{
			Application_Helper_General::writeLog('RPPY','View Cover Position Report');
		}
		
		$this->paging($result);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if(!empty($dataParamValue)){
	    	$this->view->trfdateStart = $dataParamValue['PS_TRFDATE'];
	    	$this->view->trfdateEnd = $dataParamValue['PS_TRFDATE_END'];

			unset($dataParamValue['PS_TRFDATE_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     	/*print_r($whereval);die;*/
      }
	}
}
