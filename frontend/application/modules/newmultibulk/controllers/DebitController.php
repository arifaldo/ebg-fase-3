<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';


class newmultibulk_DebitController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	protected $_destinationUploadDir = '';
	protected $_maxRow = '';

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
	}

	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL();
		$this->view->ccyArr = $this->getCcy();

		$settingObj = new Settings();
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();
		$this->view->AccArr =  $AccArr;
		$listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');
		// print_r($this->_paymenttype);die;
		$BULK_TYPEARR = $this->_request->getParam('BULK_TYPE');
		if(!empty($BULK_TYPEARR)){
			$this->view->BULK_TYPE  = $BULK_TYPEARR;
		}
		$bulkarr = array(
    								'6' => $this->language->_('Multi Credit'),
    								'7' => $this->language->_('Multi Debet'),
    								'4' => $this->language->_('Payroll'),
    								'17' => $this->language->_('Many to many'),
    								'18' => $this->language->_('E-Money'),
    								);
		$this->view->BulkType = $bulkarr;

		if($this->_request->isPost() )
		{
		    $this->_request->getParams();

			$filter = new Application_Filtering();
			$confirm = false;
			$error_msg[0] = "";

			$BULK_TYPE 	= $filter->filter($this->_request->getParam('BULK_TYPE'), "BULK_TYPE");
			// print_r($BULK_TYPE);die;
			$BULK_TYPE 	= '1';
			// var_dump($BULK_TYPE); die;
	if($BULK_TYPE == '0'){
		// die('here');
			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");
			// var_dump($ACCTSRC); die;
			if(!$ACCTSRC)
			{
				$error_msg[0] = $this->language->_('Source Account cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if(!$PS_EFDATE)
			{
				$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Payment Date cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else
			{
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();
				$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					$this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{


										$srcData = $this->_db->select()
										->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE'))
										->where('ACCT_NO = ?', $ACCTSRC)
										->limit(1);

										$acsrcData = $this->_db->fetchRow($srcData);

										if(!empty($acsrcData)){
											$accsrc = $acsrcData['ACCT_NO'];
											$accsrctype = $acsrcData['ACCT_TYPE'];
										}

										$svcInquiry = new Service_Inquiry($this->_userIdLogin,$ACCTSRC,$acsrcData['ACCT_TYPE']);
										$resultKursEx = $svcInquiry->rateInquiry();
											
										//rate inquiry for display
										$kurssell = '';
										$kurs = '';
										$book = '';				
										if($resultKursEx['ResponseCode']=='00'){
											$kursList = $resultKursEx['DataList'];
											$kurssell = '';
											$kurs = '';
											
											foreach($kursList as $row){
												if($row["currency"] == 'USD'){
													$row["sell"] = str_replace(',','',$row["sell"]);
													$row["book"] = str_replace(',','',$row["book"]);							
													$kurssell = $row["sell"];
													$book = $row["book"];
													if($ACCTSRC_CURRECY=='IDR'){
														$kurs = $row["sell"];
													}else{
														$kurs = $row["buy"];
													}
												}
											}		
											
										}

					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end

						$totalRecords = count($csvData);
						// var_dump($csvData); die;
						if($totalRecords)
						{
							unset($csvData[0]);
							unset($csvData[1]);
							unset($csvData[2]);
							unset($csvData[3]);
							unset($csvData[4]);
							unset($csvData[5]);
							unset($csvData[6]);
							unset($csvData[7]);
							$totalRecords = count($csvData);
						}

						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{

								$rowNum = 0;

								$paramPayment = array( 	"CATEGORY"      	=> "BULK CREDITT",
														"FROM"       		=> "I",
														"PS_NUMBER"     	=> "",
														"PS_SUBJECT"   	 	=> $PS_SUBJECT,
														"PS_EFDATE"     	=> $PS_EFDATE,
														"_dateFormat"    	=> $this->_dateDisplayFormat,
														"_dateDBFormat"    	=> $this->_dateDBFormat,
														"_addBeneficiary"   => $this->view->hasPrivilege('BADA'),
														"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'),
														"_createPB"     	=> $this->view->hasPrivilege('CBPW'),
														"_createDOM"    	=> $this->view->hasPrivilege('CBPI'),
														"_createREM"    	=> false,
													  );

								$paramTrxArr = array();
								// echo '<pre>';
								// print_r($csvData);
								foreach ( $csvData as $row )
								{
									// print_r(count($row));die;
									if(count($row)==7)
									{
										$rowNum++;
										$benefAcct = trim($row[0]);
										$ccy = strtoupper(trim($row[1]));
										$amount = trim($row[2]);
										$purpose = trim($row[3]);
										$message = trim($row[4]);
										$addMessage = '';
										$type = trim($row[5]);
										$bankCode = trim($row[6]);

										$fullDesc = array(
											'BENEFICIARY_ACCOUNT' => $benefAcct,
											'BENEFICIARY_ACCOUNT_CCY' => $ccy,
											'TRA_AMOUNT' => $amount,
											'TRA_MESSAGE' => $message,
											'TRA_PURPOSE' => $purpose,
											'REFNO' => $addMessage,
											'TRANSFER_TYPE' => $type,
											'CLR_CODE' => $bankCode,
										);
										// print_r($fullDesc);die;

										$filter = new Application_Filtering();

										$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
										$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
										$ACBENEF_EMAIL 		= $filter->filter($email, "EMAIL");
										$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
										$ACBENEF_ADDRESS	= $filter->filter($bankCity, "ADDRESS");
										$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
										$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");
										$TRANS_PURPOSE 		= $filter->filter($purpose, "SELECTION");

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										if($TRANSFER_TYPE == 'RTGS'){
											$chargeType = '1';
											$select = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType);
											$resultSelecet = $this->_db->FetchAll($select);
											$chargeAmt = isset($resultSelecet['0']['CHARGES_AMT']);
										}
										else if($TRANSFER_TYPE == 'SKN'){
											$chargeType1 = '2';
											$select1 = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType1);
											$resultSelecet1 = $this->_db->FetchAll($select1);
											$chargeAmt = isset($resultSelecet1['0']['CHARGES_AMT']);
										}
										else{
											$chargeAmt = '0';
										}

										$filter->__destruct();
										unset($filter);

										$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
															"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
															"TRA_AMOUNT_EQ" 			=> $TRA_AMOUNT_num,
															"TRANSFER_FEE" 				=> $chargeAmt,
															"TRA_MESSAGE" 				=> $TRA_MESSAGE,
															"TRA_REFNO" 				=> $TRA_REFNO,
															"ACCTSRC" 					=> $ACCTSRC,
															"ACBENEF" 					=> $ACBENEF,
															"ACBENEF_CCY" 				=> $ACBENEF_CCY,
															"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
															"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
															"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,
															"BANK_CODE" 				=> $CLR_CODE,
															"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
															"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
															"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
															"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
															"BANK_NAME" 	=> $BANK_NAME,

														 );

										array_push($paramTrxArr,$paramTrx);
										// die('here');
									}
									else
									{
										$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
										break;
									}
								}
							}

							else
							{
								$error_msg[0] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}
							if(!$error_msg[0])
							{

								$resWs = array();

								$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								$resultVal	= $validate->checkCreateBulk($paramPayment, $paramTrxArr);

								$payment 		= $validate->getPaymentInfo();

								// Zend_Debug::dump($validate->getErrorMsg(),'err');
								// Zend_Debug::dump($validate->getErrorTrxMsg(),'errT');
								// die('asd');

								if($validate->isError() === false)	// payment data is valid
								{

									$confirm = true;

									$validate->__destruct();
									unset($validate);
								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

									$validate->__destruct();

									unset($validate);
									// print_r($error_msg);
									// Zend_Debug::dump($validate);die;
									if($errorMsg)
									{
										$error_msg[0] = 'Error: '.$errorMsg;
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else
						{
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}
					}
				}
				else
				{
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
				}
			}else if($BULK_TYPE == '1'){
				// die;
					$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
					$PS_EFDATE 			= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
					$ACBENEF 			= $filter->filter($this->_request->getParam('ACBENEF'), "ACCOUNT_NO");
					$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
					$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
					$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");

					$minLen = 10;
			$maxLen = 20;
			$error_msg[0] = "";

			if (Zend_Validate::is($ACBENEF, 'NotEmpty') == false) {
				$error_msg[0] = $this->language->_('Beneficiary Account cannot be left blank').".";
			}

			elseif (Zend_Validate::is($ACBENEF, 'Digits') == false) {
				$error_msg[0] = $this->language->_('Beneficiary Account must be numeric').".";
			}

			elseif (strlen($ACBENEF) < $minLen || strlen($ACBENEF) > $maxLen) {
				//$error_msg[0] = "Beneficiary Account length should be between $minLen and $maxLen.";
				$error_msg[0] = $this->language->_('Beneficiary Account length should be between 10 and 20.')."";
			}


			/*elseif ($ACBENEF_ALIAS == "")
				$error_msg[0] = $this->language->_('Beneficiary Alias Name cannot be left blank.')."";*/
			elseif (strlen($ACBENEF_ALIAS) > 35) {
				$error_msg[0] = $this->language->_('Maximum lengths of Alias Name is 35 characters. Please correct it').".";
			}

			else if ($ACBENEF_CCY == "") {
					$error_msg[0] = $this->language->_('Currency cannot be left blank').".";
			}

			else if(!$PS_EFDATE) {
					$error_msg[0] = $this->language->_('Payment Date can not be left blank').".";
					// die('here');
			}



			else
			{
				// die('here');
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );

				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					$this->language->_('Extension file must be').' *.csv'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							unset($csvData[1]);
							unset($csvData[2]);
							unset($csvData[3]);
							unset($csvData[4]);
							unset($csvData[5]);
							unset($csvData[6]);
							unset($csvData[7]);


							$totalRecords = count($csvData);
							// var_dump($csvData); die;
						}

						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{
								// die('here');
								$rowNum = 0;

								$paramPayment = array( "CATEGORY"      		=> "BULK DEBET",
													   "FROM"       		=> "I",
													   "PS_NUMBER"     		=> "",
													   "PS_SUBJECT"    		=> $PS_SUBJECT,
													   "PS_EFDATE"     		=> $PS_EFDATE,
													   "_dateFormat"    	=> $this->_dateDisplayFormat,
													   "_dateDBFormat"    	=> $this->_dateDBFormat,
													   "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
													   "_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
													   "_createPB"     		=> $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
													   "_createDOM"    		=> false,        // cannot create DOM trx
													   "_createREM"    		=> false,        // cannot create REM trx
													  );

								$paramTrxArr = array();
								// print_r($csvData);die;
								foreach ( $csvData as $row )
								{
									// var_dump(count($row));
									if(count($row)==5)
									{
										// die('here');
										$rowNum++;
										$sourceAcct = trim($row[0]);
										$ccy = trim($row[1]);
										$amount = trim($row[2]);
										$purpose = trim($row[3]);
										$message = trim($row[4]);
										$addMessage = '';

										$filter = new Application_Filtering();

										$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
										// $ACCTSRC 			= $filter->filter($sourceAcct, "ACCOUNT_NO");
										$ACCTSRC 			= $sourceAcct;
										$TRANSFER_TYPE 		= 'PB';

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										$filter->__destruct();
										unset($filter);

										$paramTrx = array(	"TRANSFER_TYPE" 	=> $TRANSFER_TYPE,
															"TRA_AMOUNT" 		=> $TRA_AMOUNT_num,
															"TRA_MESSAGE" 		=> $TRA_MESSAGE,
															"TRA_REFNO" 		=> $TRA_REFNO,
															"ACCTSRC" 			=> $ACCTSRC,
															"ACBENEF" 			=> $ACBENEF,
															"ACBENEF_CCY" 		=> $ACBENEF_CCY,
															"ACBENEF_EMAIL" 	=> '',

														// for Beneficiary data, except (bene CCY and email), must be passed by reference
															"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
															"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
														//	"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
														//	"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
														//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
														//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

														//	"ORG_DIR" 					=> $ORG_DIR,
														//	"BANK_CODE" 				=> $CLR_CODE,
														//	"BANK_NAME" 				=> $BANK_NAME,
														//	"BANK_BRANCH" 				=> $BANK_BRANCH,
														//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
														//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
														//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
														 );

										array_push($paramTrxArr,$paramTrx);
									}
									else
									{
										// die('here');
										$error_msg[0] = $this->language->_('Wrong File Format').'';
										break;
									}
								}
							}
							// kalo jumlah trx lebih dari setting
							else
							{
								// die('here1');
								$error_msg[0] = 'The number of rows to be imported should not more than '.$this->_maxRow.'.';
							}

							// kalo gak ada error
							if(!$error_msg[0])
							{
								$resWs = array();
								$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								$resultVal	= $validate->checkCreate($paramPayment , $paramTrxArr, $resWs);

								$payment 		= $validate->getPaymentInfo();

								if($validate->isError() === false)	// payment data is valid
								{
									$confirm = true;

									$validate->__destruct();
									unset($validate);

								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

									$validate->__destruct();

									unset($validate);
									// print_r($error_msg);
									// Zend_Debug::dump($validate);die;
									if($errorMsg)
									{
										$error_msg[0] = 'Error: '.$errorMsg;
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else //kalo total record = 0
						{
							//$error_msg[0] = 'Wrong File Format. There is no data on csv File.';
							$error_msg[0] = $this->language->_('Wrong File Format').'.';
						}
					}
				}
				else
				{
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
				}



			}
		}else if($BULK_TYPE=='2'){

			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");

			if(!$ACCTSRC)
			{
				$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Source Account cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if(!$PS_EFDATE)
			{
				$error_msg[0] = 'Error: Payment Date can not be left blank.';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else
			{
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					$this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
// die;
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						$csvData2 = $this->parseCSV($newFileName);
						//after parse delete document temporary
						// Zend_Debug::dump($csvData);die;
						@unlink($newFileName);
						//end

						$totalRecords2 = count($csvData2);

						if($totalRecords2)
							{
								for ($a= 1; $a<$totalRecords2; $a++ ){
									unset($csvData2[$a]);
								}
//								unset($csvData2[1]);
//								unset($csvData2[2]);
//								unset($csvData2[3]);
								$totalRecords2 = count($csvData2);
						}


						foreach ( $csvData2 as $row )
						{
							$type =  trim($row[0]);
						}
// 						print_r($type);d
						// var_dump($type); die;
						if (strtoupper(trim($type)) == 'PAYROLL'){
//							echo 'berhasil';
//	die;
							// var_dump($csvData); die;
							$totalRecords = count($csvData);
							if($totalRecords)
							{
								unset($csvData[0]);
								unset($csvData[1]);
								unset($csvData[2]);
								unset($csvData[3]);
								unset($csvData[4]);
								unset($csvData[5]);
								unset($csvData[6]);
								unset($csvData[7]);
								unset($csvData[8]);
								$totalRecords = count($csvData);
							}

							// echo $csvData; die;
							if($totalRecords)
							{
								if($totalRecords <= $this->_maxRow)
								{
									$rowNum = 0;

									$paramPayment = array( 	"CATEGORY"      	=> "BULK PAYROLL",
															"FROM"       		=> "I",
															"PS_NUMBER"     	=> "",
															"PS_SUBJECT"   	 	=> $PS_SUBJECT,
															"PS_EFDATE"     	=> $PS_EFDATE,
															"_dateFormat"    	=> $this->_dateDisplayFormat,
															"_dateDBFormat"    	=> $this->_dateDBFormat,
															"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
															"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
															"_createPB"     	=> $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
															"_createDOM"    	=> $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
															"_createREM"    	=> false,        // cannot create REM trx
														  );

									$paramTrxArr = array();
									// Zend_Debug::dump($csvData); die;

									foreach ( $csvData as $row )
									{
										if(count($row)==3)
										{
											$rowNum++;
											$benefAcct 		= trim($row[0]);
// 											$benefName 		= trim($row[1]);
											$ccy 			= "IDR";
											$amount 		= trim($row[1]);
											$message 		= trim($row[2]);
											$addMessage 	= '';
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
											$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
											//$bankName 		= trim($row[10]);
	//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
											//$resident = strtoupper(trim($row[11]));

											/*
											 * Change parameter into document
											 */
											$fullDesc = array(
												'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
												'BENEFICIARY_NAME' 			=> '',
												'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
												'TRA_AMOUNT' 				=> $amount,
												'TRA_MESSAGE'				=> $message,
												'REFNO' 					=> $addMessage,
												'BENEFICIARY_EMAIL' 		=> '',
												'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
												'TRANSFER_TYPE' 			=> 'PB',
												//'CLR_CODE' 					=> $bankCode,
												//'BENEFICIARY_BANK_NAME' 	=> $bankName,
	//											'BENEFICIARY_CITY' => $bankCity,
												'BENEFICIARY_ADDRESS'		=> '',
												'BENEFICIARY_CITIZENSHIP' 	=> ''
												//'BENEFICIARY_RESIDENT' => $resident
											);

											$filter = new Application_Filtering();

											$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
											$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
											$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
											$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
											$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
											$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
											$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
											$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
											$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
											$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
											$ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
											//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
											//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
											//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
											//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
											$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");

											$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

											if($TRANSFER_TYPE == 'RTGS'){
												$chargeType = '1';
												$select = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType);
												$resultSelecet = $this->_db->FetchAll($select);
												$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
											}
											else if($TRANSFER_TYPE == 'SKN'){
												$chargeType1 = '2';
												$select1 = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType1);
												$resultSelecet1 = $this->_db->FetchAll($select1);
												$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
											}
											else{
												$chargeAmt = '0';
											}


											$filter->__destruct();
											unset($filter);

											$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
																"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
																"TRANSFER_FEE" 				=> $chargeAmt,
																"TRA_MESSAGE" 				=> $TRA_MESSAGE,
																"TRA_REFNO" 				=> $TRA_REFNO,
																"ACCTSRC" 					=> $ACCTSRC,
																"ACBENEF" 					=> $ACBENEF,
																"ACBENEF_CCY" 				=> $ACBENEF_CCY,
																"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
																"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,

															// for Beneficiary data, except (bene CCY and email), must be passed by reference
																"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
																"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
																"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
															//	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
																"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
															//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
															//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

															//	"ORG_DIR" 					=> $ORG_DIR,
																//"BANK_CODE" 				=> $CLR_CODE,
															//	"BANK_NAME" 				=> $BANK_NAME,
															//	"BANK_BRANCH" 				=> $BANK_BRANCH,
															//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
															//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
															//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
															 );

											array_push($paramTrxArr,$paramTrx);

										}
										else
										{
											$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatvv').'.';
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
											break;
										}
									}
								}
								// kalo jumlah trx lebih dari setting
								else
								{
									$error_msg[0] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
								}

								// kalo gak ada error
								if(!$error_msg[0])
								{


									$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
									$resultVal	= $validate->checkCreatePayroll($paramPayment, $paramTrxArr);
									$payment 		= $validate->getPaymentInfo();

									// Zend_Debug::dump($validate->getErrorMsg(),'err');
									// Zend_Debug::dump($validate->getErrorTrxMsg(),'errT');
									// die('asd');

									if($validate->isError() === false)	// payment data is valid
									{
										$confirm = true;

										$validate->__destruct();
										unset($validate);
									}
									else
									{
										$errorMsg 		= $validate->getErrorMsg();
										$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

										$validate->__destruct();

										unset($validate);
										// print_r($error_msg);
										// Zend_Debug::dump($validate);die;
										if($errorMsg)
										{
											$error_msg[0] = 'Error: '.$errorMsg;
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
										}
										else
										{
											$confirm = true;
										}
									}
								}
							}
							else //kalo total record = 0
							{
// 								echo 'here';die;
								//$error_msg[0] = 'Error: Wrong File Format. There is no data on csv File.';
								$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}
						} else { //kalo csv bukan payroll
							//echo 'gagal';
// 							echo 'here1';die;
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatssss').'.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}
					}

				}
				else
				{
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
			}



		}else if($BULK_TYPE == '3'){

				// die('here');
				$filter 	= new Application_Filtering();
				$adapter 	= new Zend_File_Transfer_Adapter_Http ();

				$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
				$max		= $this->getSetting('max_import_single_payment');

				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
													'Error: Extension file must be *.csv'
												);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
											'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
										);

				$adapter->setValidators(array($extensionValidator,$sizeValidator));
				// die('here');
				if ($adapter->isValid ())
				{
					// die('here');
					$sourceFileName = $adapter->getFileName();
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						$Csv = new Application_Csv ($newFileName,",");
						$csvData = $Csv->readAll ();

						@unlink($newFileName);

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							unset($csvData[1]);
							unset($csvData[2]);
							unset($csvData[3]);
							unset($csvData[4]);
							unset($csvData[5]);
							unset($csvData[6]);
							unset($csvData[7]);
							$totalRecords = count($csvData);
						}
						// var_dump($csvData); die;
						if ($totalRecords){
							if($totalRecords <= $max)
							{
								$no =0;
								foreach ( $csvData as $columns )
								{
									// print_r($columns);die;
									if(count($columns)==15)
									{
										$params['PAYMENT_SUBJECT'] 			= trim($columns[0]);
										$params['SOURCE_ACCT_NO'] 			= trim($columns[1]);
										$params['CCY'] 						= trim($columns[2]);
										$params['BENEFICIARY_ACCT_NO'] 		= trim($columns[3]);
										$params['BENEFICIARY_ACCT_CCY'] 	= trim($columns[4]);
										$params['AMOUNT'] 					= trim($columns[5]);
										$params['MESSAGE'] 					= trim($columns[7]);
										$params['TRANS_PURPOSE'] 			= trim($columns[6]);
										$params['ADDITIONAL_MESSAGE'] 		= '';
										$origDate = trim($columns[8]);
										$date = str_replace('/', '-', $origDate );
										$date2 = date_create($date);
										$params['PAYMENT_DATE'] 			= date_format($date2,"d/m/y");
										$params['TRANSFER_TYPE'] 			= trim($columns[9]);
										$params['BANK_CODE'] 				= trim($columns[10]);
										if(!empty($columns[12]) || !empty($columns[13])){
											$params['TRA_NOTIF']			= '2';	
										}else{
											$params['TRA_NOTIF']			= '1';	
										}
										$params['TRA_SMS']					= trim($columns[12]);
										$params['TRA_EMAIL']				= trim($columns[13]);
										$params['TREASURY_NUM']				= trim($columns[11]);
										$params['LLD_TRANSACTION_PURPOSE']  = trim($columns[14]);
										

										// var_dump($params['PAYMENT DATE']); die;


										$PS_SUBJECT 		= $filter->filter($params['PAYMENT_SUBJECT'],"PS_SUBJECT");
										$PS_EFDATE 			= $filter->filter($params['PAYMENT_DATE'],"PS_DATE");
										$TRA_AMOUNT 		= $filter->filter($params['AMOUNT'],"AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($params['ADDITIONAL_MESSAGE'],"TRA_REFNO");
										$ACCTSRC 			= $filter->filter($params['SOURCE_ACCT_NO'],"ACCOUNT_NO");
										$ACBENEF 			= $filter->filter($params['BENEFICIARY_ACCT_NO'],"ACCOUNT_NO");
										$ACBENEF_BANKNAME 	= $filter->filter($params['BENEFICIARY_NAME'],"ACCOUNT_NAME");
										$ACBENEF_CCY 		= $filter->filter($params['CCY'],"SELECTION");
										$CLR_CODE			= $filter->filter($params['BANK_CODE'], "BANK_CODE");
										$TRANSFER_TYPE 		= $filter->filter($params['TRANSFER_TYPE'], "SELECTION");

										$BENEFICIARY_ACCT_CCY			= $filter->filter($params['BENEFICIARY_ACCT_CCY'], "BENEFICIARY_ACCT_CCY");
										$TRA_NOTIF			= $filter->filter($params['TRA_NOTIF'], "TRA_NOTIF");
										$TRA_SMS			= $filter->filter($params['TRA_SMS'], "TRA_SMS");
										$TRA_EMAIL			= $filter->filter($params['TRA_EMAIL'], "TRA_EMAIL");
										$TREASURY_NUM		= $filter->filter($params['TREASURY_NUM'], "TREASURY_NUM");
										$LLD_TRANSACTION_PURPOSE		= $filter->filter($params['LLD_TRANSACTION_PURPOSE'], "LLD_TRANSACTION_PURPOSE");
										
										// var_dump($PS_EFDATE); die;
										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										if($TRANSFER_TYPE == 'RTGS'){
											$chargeType = '1';
											$select = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType);
											$resultSelecet = $this->_db->FetchAll($select);
											$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

											//$param['TRANSFER_FEE'] = $chargeAmt;
										}
										else if($TRANSFER_TYPE == 'SKN'){
											$chargeType1 = '2';
											$select1 = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType1);
											$resultSelecet1 = $this->_db->FetchAll($select1);
											$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

											//$param['TRANSFER_FEE'] = $chargeAmt1;
										}
										else{
											$chargeAmt = '0';
											//$param['TRANSFER_FEE'] = $chargeAmt2;
										}


										if($BENEFICIARY_ACCT_CCY != $ACBENEF_CCY){
											$CROSS_CURR = '2';
										}else{
											$CROSS_CURR = '1';
										}

										$paramPayment = array(
												"CATEGORY" 					=> "SINGLE PAYMENT",
												"FROM" 						=> "I",				// F: Form, I: Import
												"PS_NUMBER"					=> "",
												"PS_SUBJECT"				=> $PS_SUBJECT,
												"PS_EFDATE"					=> $PS_EFDATE,
												"_dateFormat"				=> $this->_dateUploadFormat,
												"_dateDBFormat"				=> $this->_dateDBFormat,
												"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
												"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
												"_createPB"					=> $this->view->hasPrivilege('CRIP'),								// cannot create PB trx
												"_createDOM"				=> $this->view->hasPrivilege('CRDI'),	// privi CDFT (Create Domestic Fund Transfer)
												"_createREM"				=> $this->view->hasPrivilege('CRIR'),								// cannot create REM trx
												"TRA_CCY"					=> $BENEFICIARY_ACCT_CCY,
												"CROSS_CURR"				=> $CROSS_CURR
										);

										$paramTrxArr[0] = array(
												"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
												"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
												"TRANSFER_FEE" 				=> $chargeAmt,
												"TRA_MESSAGE" 				=> $TRA_MESSAGE,
												"TRA_REFNO" 				=> $TRA_REFNO,
												"ACCTSRC" 					=> $ACCTSRC,
												"ACBENEF" 					=> $ACBENEF,
												"ACBENEF_CCY" 				=> $ACBENEF_CCY,
												"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
												"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
//												"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
												"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
												// 																"ACBENEF_ADDRESS1" 			=> $BANK_CITY,
//												"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,
												"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
												"BANK_CODE" 				=> $CLR_CODE,
//												"BENEFICIARY_BANK_NAME"		=> $BANK_NAME,
//												"LLD_IDENTICAL" 			=> "",
//												"LLD_CATEGORY" 				=> "",
//												"LLD_RELATIONSHIP" 			=> "",
//												"LLD_PURPOSE" 				=> "",
//												"LLD_DESCRIPTION" 			=> "",
												"LLD_TRANSACTION_PURPOSE"	=> $LLD_TRANSACTION_PURPOSE,
												"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
												"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
												"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
												"BENEFICIARY_ACCT_CCY" 		=> $BENEFICIARY_ACCT_CCY,
												"TRA_NOTIF" 				=> $TRA_NOTIF,
												"TRA_SMS" 					=> $TRA_SMS,
												"TRA_EMAIL" 				=> $TRA_EMAIL,
												"TREASURY_NUM" 				=> $TREASURY_NUM,

												"BANK_NAME" 	=> $BANK_NAME,

										);

										$arr[$no]['paramPayment'] = $paramPayment;
										$arr[$no]['paramTrxArr'] = $paramTrxArr;
									}
									else
									{
										// die('ge');
										$this->view->error 		= true;
										break;
									}
									$no++;
								}

								if(!$this->view->error)
								{
									$resWs = array();
									$err 	= array();

									$resultVal	= $validate->checkCreate($arr, $resWs);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();

									// Zend_Debug::dump($errorTrxMsg);die;
									$i = 0;
									foreach($resWs as $key=>$dataAcctType){
										//Zend_Debug::dump($dataAcctType);
										$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
									}


									//die;

									$sourceAccountType 	= $resWs['accountType'];

									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
									$content['sourceAccountType'] 	= $sourceAccountType;

									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;

									$this->_redirect('/singlepayment/import/confirm');
								}

							}
							else
							{
								// die('here');
								$this->view->error2 = true;
								$this->view->max 	= $max;
							}
						}else{
							// die('here1');
							$this->view->error = true;
						}
					}
				}
				else
				{
					// die('here3');
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}




		}








			if($confirm)
			{
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				if($BULK_TYPE=='0'){
					$content['sourceAccountType'] = $sourceAccountType;
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
					$sessionNamespace->content = $content;
					$this->_redirect('/multicredit/bulk/confirm');

				}else if($BULK_TYPE=='1'){
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
					$sessionNamespace->content = $content;
					$this->_redirect('/multidebet/bulk/confirm');

				}else if($BULK_TYPE=='2'){
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
					$sessionNamespace->content = $content;
					$this->_redirect('/payroll/bulk/confirm');
				}else if($BULK_TYPE == '3'){

									$resWs = array();
									$err 	= array();

									$resultVal	= $validate->checkCreate($arr, $resWs);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();

									//Zend_Debug::dump($resWs);die;
									$i = 0;
									foreach($resWs as $key=>$dataAcctType){
										//Zend_Debug::dump($dataAcctType);
										$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
									}


									//die;

									$sourceAccountType 	= $resWs['accountType'];

									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
									$content['sourceAccountType'] 	= $sourceAccountType;

									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;

									$this->_redirect('/singlepayment/import/confirm');


				}




		}

			$this->view->BULK_TYPE = $BULK_TYPE;
			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACCTSRC = $ACCTSRC;
			$this->view->PSEFDATE = $PS_EFDATE;
		}
		Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
	}

	private function resData($benefAccount){
			$select	= $this->_db->select()
								->from(array('B'	 			=> 'M_BENEFICIARY'), array('BANK_NAME','BENEFICIARY_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_CITIZENSHIP','BENEFICIARY_RESIDENT','BENEFICIARY_ID_NUMBER','BENEFICIARY_ID_TYPE','BENEFICIARY_CITY_CODE','BENEFICIARY_CATEGORY')
									   );
			$select->where("B.BENEFICIARY_ACCOUNT = ?", $benefAccount);

			$bene = $this->_db->fetchAll($select);
			return $bene;
	}

	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$data = $sessionNamespace->content;
//		die;
		$sourceAcct = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']];

		$this->view->CCY = $sourceAcct['CCY_ID'];
		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'].' / '.$sourceAcct['ACCT_NAME'].' / '.$sourceAcct['CCY_ID'];
		//if($sourceAcct['ACCT_ALIAS']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS'];

		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];

		$chargesAmt = array();
		$totalChargesAmt = 0;

		foreach($data['paramTrxArr'] as $row)
		{
//			$benefAccount = $row["ACBENEF"];
//			if($row["TRANSFER_TYPE"] !="PB"){
//				$dataRes = $this->resData($benefAccount);
//
//				$row['BENEFICIARY_ID_NUMBER'] = $dataRes[0]['BENEFICIARY_ID_NUMBER'];
//				$row['BENEFICIARY_ID_TYPE'] = $dataRes[0]['BENEFICIARY_ID_TYPE'];
//				$row['BENEFICIARY_CITY_CODE'] = $dataRes[0]['BENEFICIARY_CITY_CODE'];
//				$row['BENEFICIARY_CATEGORY'] = $dataRes[0]['BENEFICIARY_CATEGORY'];
//				$row['BENEFICIARY_BANK_NAME'] = $dataRes[0]['BANK_NAME'];
//				$row['BENEFICIARY_CITIZENSHIP'] = $dataRes[0]['BENEFICIARY_CITIZENSHIP'];
//				$row['BENEFICIARY_RESIDENT'] = $dataRes[0]['BENEFICIARY_RESIDENT'];
//				$row['BENEFICIARY_ACCOUNT_NAME'] = $dataRes[0]['BENEFICIARY_NAME'];
//			}

			if(!isset($chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']])) $chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] = 0;
			//$chargesObj = Charges::factory($this->_custIdLogin, $row['TRANSFER_TYPE']);

//			if(empty($row['TRANSFER_TYPE'])){
//				$transferType = '0';
//			}
//			else{
//				$transferType = $row['TRANSFER_TYPE'];
//			}
			$chargesObj = Charges::factory($this->_custIdLogin, isset($row['TRANSFER_TYPE']));

			$paramCharges = array("accsrc" => $row['ACCTSRC'], "transferType" => $row['TRANSFER_TYPE']);
			if(!isset($data['errorTrxMsg'][$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']]))
			{
// 				$chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] += $chargesObj->getCharges($paramCharges);
// 				$totalChargesAmt += $chargesObj->getCharges($paramCharges);
			}
		}
		$this->view->chargesAmt = $chargesAmt;
		$this->view->totalChargesAmt = $totalChargesAmt;

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$settings = new Settings();
		$this->view->cutOffSKN = $settings->getSetting('cut_off_time_skn');
		$this->view->cutOffRTGS = $settings->getSetting('cut_off_time_rtgs');
		$this->view->cutOffBI = $settings->getSetting('cut_off_time_bi');

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/multicredit/bulk');
			}

			if ($data["payment"]["countTrxPB"] == 0)
				$priviCreate = 'CBPI';
			else
				$priviCreate = 'CBPW';

			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= $this->_paymenttype['code']['bulkcredit'];
			$param['PS_CCY']  = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];

			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
						'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
						'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
						'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
						'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
//						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'], //ADA
						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
						'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
						'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'], //ADA
						'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
						//'ACBENEF_ADDRESS1' 			=> $row['ACBENEF_ADDRESS1'],
						'BENEFICIARY_RESIDENT' 		=> $row['BENEFICIARY_RESIDENT'],
						'CLR_CODE' 					=> $row['BANK_CODE'],
						'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'], //SKN,RTGS,PB
						'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
						'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
						'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
						'TRA_REFNO' 				=> $row['TRA_REFNO'],
						'sourceAccountType' 		=> $data['sourceAccountType'],

						'BENEFICIARY_BANK_NAME' 	=> $row['BANK_NAME'], //ADA
						'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
						'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
						'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
						'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],

				);
			}

			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = $priviCreate;
			//$param['sourceAccountType'] = $data['sourceAccountType'];


			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
//			Zend_Debug::dump($param);die;
			$result = $BulkPayment->createPayment($param);
			if($result)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/notification/success');
			}
			else
			{
				$this->view->error = true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/multicredit/bulk');
			}
		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}
}
