<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';

class paymentworkflow_OngoingController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
		$this->_transferStatus 	= $conf["transfer"]["status"];
	}

	public function setPaymentType($paymentType, $transferType)
	{
		$filterType     = array();

		foreach ($paymentType["code"] as $key => $val) {
			//|| $key == "remittance"
			if ($key == "within" || $key == "domestic" || $key == "remittance" || $key == "purchase" || $key == "payment") {
				$filterType[$paymentType["code"][$key]] = $paymentType["desc"][$key];
			} elseif ($key == "multicredit") {
				$code = $paymentType["code"]["multicredit"] . "," . $paymentType["code"]["bulkcredit"];
				$desc = $paymentType["desc"][$key];
				$filterType["{$code}"] = $desc;
			} elseif ($key == "multidebet") {
				$code = $paymentType["code"]["multidebet"] . "," . $paymentType["code"]["bulkdebet"];
				$desc = $paymentType["desc"][$key];
				$filterType["{$code}"] = $desc;
			}
		}

		return $filterType;
	}

	public function indexAction()
	{

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$range_reporthistory = $setting->getSettingVal('range_reporthistory');
		$this->view->range_reporthistory = $range_reporthistory;
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		// print_r($this->_request->getParams());die;
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		
		
			
				
				
		
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}
		
		if ($this->_getParam('cancelBtn') == TRUE) {
			$filter       = new Application_Filtering();
			$password = $sessionNamespace->token;
			$AESMYSQL = new Crypt_AESMYSQL();
			$PS_NUMBER       = urldecode($filter->filter($this->_getParam('PERIODIC_ID'), "PS_NUMBER"));
			$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

			$select  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'), array(
					'P.PS_STATUS',
					'P.PS_NUMBER',
					'A.PS_PERIODIC_NUMBER',
					'A.PS_PERIODIC_STATUS',
					'A.PS_PERIODIC'
				))
				->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
				->where('P.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchRow($select);
			
			if ($data['PS_PERIODIC_NUMBER'] === null) {
				$update['PS_STATUS']   = '14';
				$where = array('PS_NUMBER = ?' => $data['PS_NUMBER']);
				$this->_db->update('T_PSLIP', $update, $where);
			} else {
				$update['PS_PERIODIC_STATUS']   = '0';
				$where = array('PS_PERIODIC = ?' => $data['PS_PERIODIC']);
				$this->_db->update('T_PERIODIC', $update, $where);
				
				$update['PS_STATUS']   = '14';
				$where = array('PS_NUMBER = ?' => $data['PS_NUMBER']);
				$this->_db->update('T_PSLIP', $update, $where);
			}
			$historyInsert = array(
				'DATE_TIME' 		=> new Zend_Db_Expr("now()"),
				'PS_NUMBER' 		=> $data['PS_NUMBER'],
				'CUST_ID' 			=> $this->_custIdLogin,
				'USER_LOGIN' 		=> $this->_userIdLogin,
				'HISTORY_STATUS' 	=> 14,
			);

			$this->_db->insert('T_PSLIP_HISTORY', $historyInsert);
			Application_Helper_General::writeLog('DARC', 'Cancel Ongoing Payment');
		}
		
		
		if ($this->_getParam('holdBtn') == TRUE) {
			$filter       = new Application_Filtering();
			$password = $sessionNamespace->token;
			$AESMYSQL = new Crypt_AESMYSQL();
			$PS_NUMBER       = urldecode($filter->filter($this->_getParam('HOLDPERIODIC_ID'), "PS_NUMBER"));
			$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

			$select  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'), array(
					'P.PS_STATUS',
					'P.PS_NUMBER',
					'A.PS_PERIODIC_NUMBER',
					'A.PS_PERIODIC_STATUS',
					'A.PS_PERIODIC'
				))
				->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
				->where('P.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchRow($select);
			
			if ($data['PS_PERIODIC_NUMBER'] === null) {
				$update['PS_STATUS']   = '25';
				$where = array('PS_NUMBER = ?' => $data['PS_NUMBER']);
				$this->_db->update('T_PSLIP', $update, $where);
			} else {
				$update['PS_PERIODIC_STATUS']   = '0';
				$where = array('PS_PERIODIC = ?' => $data['PS_PERIODIC']);
				$this->_db->update('T_PERIODIC', $update, $where);
				
				$update['PS_STATUS']   = '25';
				$where = array('PS_NUMBER = ?' => $data['PS_NUMBER']);
				$this->_db->update('T_PSLIP', $update, $where);
			}
			$historyInsert = array(
				'DATE_TIME' 		=> new Zend_Db_Expr("now()"),
				'PS_NUMBER' 		=> $data['PS_NUMBER'],
				'CUST_ID' 			=> $this->_custIdLogin,
				'USER_LOGIN' 		=> $this->_userIdLogin,
				'HISTORY_STATUS' 	=> 25,
			);

			$this->_db->insert('T_PSLIP_HISTORY', $historyInsert);
			Application_Helper_General::writeLog('DARC', 'Hold Ongoing Payment');
		}

		$CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$filter     = new Application_Filtering();

		$opt[""] = "-- " . $this->language->_('Please Select') . " --";

		$payType   = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		//$optpaytype 	= $this->setPaymentType($this->_paymenttype,$this->_transfertype);
		$arrPayStatus  = array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
		$trfType     = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		$arrAccount   = $CustomerUser->getAccounts();

		foreach ($payType as $key => $value) {
			// 			if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);

			$optpaytypeRaw[$key] = $this->language->_($value);
		}
		//foreach($trfType as $key => $value){ if($key != 3 && $key != 4) $filterTrfType[$key] = $this->language->_($value); }
		foreach ($trfType as $key => $value) {
			$filterTrfType[$key] = $this->language->_($value);
		}
		foreach ($arrPayStatus as $key => $value) {
			if ($key != 3) $optpayStatusRaw[$key] = $this->language->_($value);
		}

		if (is_array($arrAccount) && count($arrAccount) > 0) {
			foreach ($arrAccount as $key => $value) {

				$val     = $arrAccount[$key]["ACCT_NO"];
				$ccy     = $arrAccount[$key]["CCY_ID"];
				$acctname   = $arrAccount[$key]["ACCT_NAME"];
				//$acctalias 	= $arrAccount[$key]["ACCT_ALIAS_NAME"];
				$accttype   = ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING' : 'GIRO';  // 10 : saving, 20 : giro;

				$arrAccountRaw[$val] = $val . ' [' . $ccy . '] ' . $acctname . ' (' . $accttype . ')';
			}
		} else {
			$arrAccountRaw = array();
		}


		unset($arrPayStatus[10]);
		unset($arrPayStatus[15]);
		unset($arrPayStatus[16]);

		$optPayType   = $opt + $optpaytypeRaw;

		//$optPayStatus 	= $opt + $arrPayStatus;
		$optPayStatus   = $opt + $optpayStatusRaw;
		unset($optPayStatus[10]);
		unset($optPayStatus[15]);
		unset($optPayStatus[16]);

		//hilangkan payment type payroll, Sweep In, Sweep Out
		$optPayTypePayroll = "11,11";
		$optPayTypeSweepIn = "14,14";
		$optPayTypeSweepOut = "15,15";
		$optPayTypeCredit = "6,4";
		$optPayTypeDebit = "7,5";
		//unset($optPayType[$optPayTypePayroll]);
		//unset($optPayType[$optPayTypeSweepIn]);
		//unset($optPayType[$optPayTypeSweepOut]);

		//unset($optPayType[$optPayTypeDebit]);
		//unset($optPayType[$optPayTypeCredit]);

		$optarrAccount   = $opt + $arrAccountRaw;

		$this->view->optPayType   = $optPayType;
		$this->view->optPayStatus   = $optPayStatus;
		$this->view->optarrAccount   = $optarrAccount;

		$fields = array(
			'paytype'            => array(
				'field' => 'paytype',
				'label' => $this->language->_('Payment Type'),
				'sortable' => true
			),
			'PS_NUMBER'    => array(
				'field' => 'PS_NUMBER',
				'label' => $this->language->_('FreqRef# / Payment Ref#'),
				'sortable' => true
			),
			'PS_SUBJECT'            => array(
				'field' => 'PS_SUBJECT',
				'label' => $this->language->_('Subject'),
				'sortable' => true
			),
			'PS_EVERY_PERIODIC_UOM'      => array(
				'field'   => 'PS_EVERY_PERIODIC_UOM',
				'label'   => $this->language->_('Frequently'),
				'sortable' => true
			),
			'PS_TOTAL_AMOUNT'      => array(
				'field'   => 'PS_TOTAL_AMOUNT',
				'label'   => $this->language->_('Amount'),
				'sortable' => true
			),
			'nextExecute'      => array(
				'field'   => 'PS_EFDATE',
				'label'   => $this->language->_('Next Execute'),
				// 'sortable' => true
			),
			'STATUS'            => array(
				'field' => 'STATUS',
				'label' => $this->language->_('Status'),
			),

			// 'Info'        => array(
			// 	'field'   => 'INFO',
			// 	'label'   => $this->language->_('Info'),
			// 	'sortable' => true
			// )
		);
		// ,'SOURCE_ACCOUNT_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_ACCOUNT_NAME','TRANSACTION_ID',,'PS_CCY'
		$filterlist = array('PS_NUMBER', 'PS_SUBJECT', 'ONGOING_STATUS', 'PS_TYPE', 'TRA_AMOUNT', "FREQUENTLY", 'NEXT_EXECUTE');

		$this->view->filterlist = $filterlist;

		// print_r($this->_request->getParams());die;
		//get page, sortby, sortdir
		$page        = $this->_getParam('page');
		$sortBy      = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('P.PS_EFDATE');
		$sortDir     = $this->_getParam('sortdir');
		$filter     = $this->_getParam('filter');
		$clearfilter  = $this->_getParam('clearfilter');

		$csv       = $this->_getParam('csv');
		$pdf       = $this->_getParam('pdf');

		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		$this->view->filter      = $filter;
		$this->view->clearfilter  = $clearfilter;

		//validate parameters before passing to view and query
		$page     = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy   = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir   = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

		$this->view->currentPage = $page;

		$filterArr = array(
			// 'PS_UPDATED'   => array('StringTrim', 'StripTags'),
			// 'PS_EFDATE'   => array('StringTrim', 'StripTags'),
			// 'PS_CREATED'   => array('StringTrim', 'StripTags'),
			'PS_SUBJECT'   => array('StringTrim', 'StripTags'),
			'FREQUENTLY'   => array('StringTrim', 'StripTags'),
			'TRA_AMOUNT'   => array('StringTrim', 'StripTags'),
			'NEXT_EXECUTE'   => array('StringTrim', 'StripTags'),
			'NEXT_EXECUTE_END'   => array('StringTrim', 'StripTags'),
			// 'SOURCE_ACCOUNT'   => array('StringTrim', 'StripTags'),

			// 'PS_UPDATED_END'   => array('StringTrim', 'StripTags'),
			// 'PS_EFDATE_END'   => array('StringTrim', 'StripTags'),
			// 'PS_CREATED_END'     => array('StringTrim', 'StripTags'),

			'PS_NUMBER'     => array('StringTrim', 'StripTags', 'StringToUpper'),
			// 'TRANSACTION_ID'	=> array('StringTrim','StripTags'),
			// 'PS_CCY'	=> array('StringTrim','StripTags'),
			'PS_STATUS'  => array('StringTrim', 'StripTags'),
			'PS_TYPE'   => array('StringTrim', 'StripTags'),
			// 'TRANSFER_TYPE'   => array('StringTrim', 'StripTags'),


		);

		// print_r($filterArr);die;
		// if POST value not null, get post, else get param ,"SOURCE_ACCOUNT_NAME","BENEFICIARY_ACCOUNT","BENEFICIARY_ACCOUNT_NAME","TRANSACTION_ID","PS_CCY"
		$dataParam = array("PS_SUBJECT", "PS_NUMBER", "ONGOING_STATUS", "PS_TYPE", 'TRA_AMOUNT' ,"FREQUENTLY", 'NEXT_EXECUTE');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		// if (!empty($this->_request->getParam('createdate'))) {
		// 	$createarr = $this->_request->getParam('createdate');
		// 	$dataParamValue['PS_CREATED'] = $createarr[0];
		// 	$dataParamValue['PS_CREATED_END'] = $createarr[1];
		// }
		// if (!empty($this->_request->getParam('updatedate'))) {
		// 	$updatearr = $this->_request->getParam('updatedate');
		// 	$dataParamValue['PS_UPDATED'] = $updatearr[0];
		// 	$dataParamValue['PS_UPDATED_END'] = $updatearr[1];
		// }

		// if (!empty($this->_request->getParam('efdate'))) {
		// 	$efdatearr = $this->_request->getParam('efdate');
		// 	$dataParamValue['PS_EFDATE'] = $efdatearr[0];
		// 	$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
		// }

		if (!empty($this->_request->getParam('nextexecute'))) {
			$efdatearr = $this->_request->getParam('nextexecute');
			$dataParamValue['NEXT_EXECUTE'] = $efdatearr[0];
			$dataParamValue['NEXT_EXECUTE_END'] = $efdatearr[1];
		}

		// print_r($dataParamValue);die;
		// The default is set so all fields allow an empty string
		$options = array('allowEmpty' => true);
		$validators = array(
			// 'PS_UPDATED'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			// 'PS_EFDATE'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			// 'PS_CREATED'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),

			// 'PS_UPDATED_END'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			// 'PS_EFDATE_END'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			// 'PS_CREATED_END'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),

			'FREQUENTLY'  => array(),
			'PS_SUBJECT'  => array(),
			'TRA_AMOUNT'  => array(),
			'NEXT_EXECUTE'  => array(new Zend_Validate_Date("d-m-Y")),
			'NEXT_EXECUTE_END'  => array(new Zend_Validate_Date("d-m-Y")),
			// 'SOURCE_ACCOUNT'     => array(),
			// 'SOURCE_ACCOUNT_NAME'		=> array(),		// $filter!
			// 'BENEFICIARY_ACCOUNT'		=> array(),
			// 'BENEFICIARY_ACCOUNT_NAME'		=> array(),
			'PS_NUMBER'    => array(),
			// 'TRANSACTION_ID'		=> array(),
			// 'PS_CCY'		=> array(),
			// 'PS_STATUS'		=> array(),
			'ONGOING_STATUS' => array(),  // $filter!
			'PS_TYPE'   => array(),  // $filter!
			// 'TRANSFER_TYPE' => array(array('InArray', array('haystack' => array_keys($filterTrfType)))),  // $filter!

		);

		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);


		// $fUpdatedStart   = $zf_filter->getEscaped('PS_UPDATED');
		// $fUpdatedEnd   = $zf_filter->getEscaped('PS_UPDATED_END');

		// $fPaymentStart   = $zf_filter->getEscaped('PS_EFDATE');
		// $fPaymentEnd   = $zf_filter->getEscaped('PS_EFDATE_END');

		// $fCreatedStart   = $zf_filter->getEscaped('PS_CREATED');
		// $fCreatedEnd   = $zf_filter->getEscaped('PS_CREATED_END');
		// print_r($fCreatedEnd);die;


		$frequently     = $zf_filter->getEscaped('FREQUENTLY');
		$subject     = $zf_filter->getEscaped('PS_SUBJECT');

		$traamount     = $zf_filter->getEscaped('TRA_AMOUNT');

		$traamount_disp = Application_Helper_General::displayMoney($traamount);

		$next_execute_start     = $zf_filter->getEscaped('NEXT_EXECUTE');
		$next_execute_end     = $zf_filter->getEscaped('NEXT_EXECUTE_END');

		$fPaymentReff   = $zf_filter->getEscaped('PS_NUMBER');
		$ongoingStatus = $zf_filter->getEscaped('ONGOING_STATUS');
		$fPaymentType   = $zf_filter->getEscaped('PS_TYPE');
		// $fTransferType   = $zf_filter->getEscaped('TRANSFER_TYPE');


		if ($filter == NULL && $clearfilter != 1) {
			$fUpdatedStart   = "";
			$fUpdatedEnd   = "";

			// echo "f0 c1";
		} else {

			if ($filter != NULL) {
				$fUpdatedStart   = $zf_filter->getEscaped('PS_UPDATED');
				$fUpdatedEnd   = $zf_filter->getEscaped('PS_UPDATED_END');
				// echo "f1 c0";
			} else if ($clearfilter == 1) {
				$fUpdatedStart   = "";
				$fUpdatedEnd   = "";
				// echo "f0 c0";
			}
		}


		//jika tombol csv dan pdf ditekan
		if ($pdf || $csv) {
			$fUpdatedStart   = $zf_filter->getEscaped('PS_UPDATED');
			$fUpdatedEnd   = $zf_filter->getEscaped('PS_UPDATED_END');
		}

		$paramPayment = array(
			"WA"         => false,
			"ACCOUNT_LIST"   => $this->_accountList,
			"_beneLinkage"   => $this->view->hasPrivilege('BLBU'),
		);
		$config        = Zend_Registry::get('config');
		$paymentType   = $config["payment"]["type"];
		$transferType   = $config["transfer"]["type"];


		// case when string		
		$caseArr      = Application_Helper_General::casePaymentType($paymentType, $transferType);
		// Zend_Debug::dump($caseArr);die;
		$casePaymentType = $caseArr["PS_TYPE"];
		// get query
		$selectFuture  = $this->_db->select()
			->from(array('P' => 'T_PSLIP'), array(
				'P.PS_NUMBER',
				'P.PS_SUBJECT',
				'P.PS_TYPE',
				'P.PS_STATUS',
				'P.PS_CCY',
				'P.PS_EFTIME',
				'P.PS_EFDATE',
				'P.PS_PERIODIC',
				'P.PS_TOTAL_AMOUNT',
				'P.PS_CREATED',
				'P.PS_UPDATED',
				'T.TRANSFER_TYPE',
				'PS_PERIODIC_NUMBER' => 'A.PS_PERIODIC_NUMBER',
				'PS_EVERY_PERIODIC_UOM' => 'A.PS_EVERY_PERIODIC_UOM',
				'PS_EVERY_PERIODIC' => 'A.PS_EVERY_PERIODIC',
				'PS_PERIODIC_STATUS' => 'A.PS_PERIODIC_STATUS',
				'PS_PERIODIC_ENDDATE' => 'A.PS_PERIODIC_ENDDATE',
				'paytype'    => new Zend_Db_Expr("CASE $casePaymentType ELSE 'N/A' END")
			))
			->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
			->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
			->where("P.CUST_ID = '" . $this->_custIdLogin . "'")
			->where("P.PS_STATUS IN(7,14)")
			->where("A.`PS_PERIODIC` IS NULL");

		//filter
		if($subject)
		{	
			$fSubjectArr = explode(',', $subject);
			$selectFuture->where("UPPER(P.PS_SUBJECT)  in (?)",$fSubjectArr );
	//$selectFuture->where("P.PS_SUBJECT LIKE ".$this->_db->quote('%'.$subject.'%'));	
		}

		if ($fPaymentReff) {
			$fPaymentReffArr = explode(',', $fPaymentReff);
			$selectFuture->where("UPPER(P.PS_NUMBER)  in (?)",$fPaymentReffArr );
			//$selectFuture->where("UPPER(P.PS_NUMBER) LIKE " . $this->_db->quote('%' . $fPaymentReff . '%'));
		}

		if ($fPaymentType) {
			$fPayType      = explode(",", $fPaymentType);
			$selectFuture->where("P.PS_TYPE in (?) ", $fPayType);
		}

		if($traamount){
			$selectFuture->where("P.PS_TOTAL_AMOUNT = ?", Application_Helper_General::convertDisplayMoney($traamount_disp));
		}

		$selectPeriodic  = $this->_db->select()
			->from(array('A' => 'T_PERIODIC'), array())
			->joinLeft(array('P' => 'T_PSLIP'), 'P.PS_PERIODIC = A.PS_PERIODIC', array(
				'P.PS_NUMBER',
				'P.PS_SUBJECT',
				'P.PS_TYPE',
				'P.PS_STATUS',
				'P.PS_CCY',
				'P.PS_EFTIME',
				'P.PS_EFDATE',
				'P.PS_PERIODIC',
				'P.PS_TOTAL_AMOUNT',
				'P.PS_CREATED',
				'P.PS_UPDATED',
				'T.TRANSFER_TYPE',
				'PS_PERIODIC_NUMBER' => 'A.PS_PERIODIC_NUMBER',
				'PS_EVERY_PERIODIC_UOM' => 'A.PS_EVERY_PERIODIC_UOM',
				'PS_EVERY_PERIODIC' => 'A.PS_EVERY_PERIODIC',
				'PS_PERIODIC_STATUS' => 'A.PS_PERIODIC_STATUS',
				'PS_PERIODIC_ENDDATE' => 'A.PS_PERIODIC_ENDDATE',
				'paytype'    => new Zend_Db_Expr("CASE $casePaymentType ELSE 'N/A' END")
			))
			->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
			->where("P.PS_STATUS IN(7,14,5,25)")
			->where("P.CUST_ID = '" . $this->_custIdLogin . "'");

		//filter
		if($subject)
		{	
		$fSubjectArr = explode(',', $subject);
			$selectPeriodic->where("UPPER(P.PS_SUBJECT)  in (?)",$fSubjectArr );
	//$selectPeriodic->where("P.PS_SUBJECT LIKE ".$this->_db->quote('%'.$subject.'%'));	
	}


		if ($fPaymentReff) {
			$fPaymentReffArr = explode(',', $fPaymentReff);
			$selectPeriodic->where("UPPER(P.PS_NUMBER)  in (?)",$fPaymentReffArr );
			//$selectPeriodic->where("UPPER(P.PS_NUMBER) LIKE " . $this->_db->quote('%' . $fPaymentReff . '%'));
		}

		if ($fPaymentType) {
			$fPayType      = explode(",", $fPaymentType);
			$selectPeriodic->where("P.PS_TYPE in (?) ", $fPayType);
		}

		if($traamount){
			$selectPeriodic->where("P.PS_TOTAL_AMOUNT = ?", Application_Helper_General::convertDisplayMoney($traamount_disp));
		}


		$selectFuture->where('DATE(P.PS_EFDATE) >= DATE(NOW())');
		$selectPeriodic->where('DATE(A.PS_PERIODIC_ENDDATE) >= DATE(NOW())');
		$selectPeriodic->group('A.PS_PERIODIC_NUMBER');
		$selectPeriodic->order('PS_EFDATE DESC');
		
		$selectFuture1 	= $selectFuture->__toString();
		$selectPeriodic1 		= $selectPeriodic->__toString();


		$unionquery = $this->_db->select()->union(array($selectFuture1, $selectPeriodic1));
		$select = $this->_db->select()
			->from(($unionquery), array('*'));
		// $select->group('PS_PERIODIC_NUMBER');
		$select->order('PS_UPDATED DESC');
		$select->order(array(
			new Zend_Db_Expr("CASE WHEN PS_STATUS = '7' THEN '14' ELSE 'N/A' END"),
			new Zend_Db_Expr("CASE WHEN PS_PERIODIC_STATUS = '2' THEN '0' ELSE 'N/A' END"),
			'TIME(PS_EFTIME) ASC',
			'DATE(PS_EFDATE) ASC',
		));
		// $select->order(new Zend_Db_Expr("FIELD(PS_STATUS, '7','14','5')"));
		// $select->order(new Zend_Db_Expr("FIELD(PS_PERIODIC_STATUS, '2','0','1')"));

		// Filter Data
		if ($fUpdatedStart) {
			$FormatDate   = new Zend_Date($fUpdatedStart, $this->_dateDisplayFormat);
			$updatedFrom     = $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_UPDATED) >= ?', $updatedFrom);
		}

		if ($fUpdatedEnd) {
			$FormatDate   = new Zend_Date($fUpdatedEnd, $this->_dateDisplayFormat);
			$updatedTo     = $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_UPDATED) <= ?', $updatedTo);
		}

		if ($fCreatedStart) {
			$FormatDate   = new Zend_Date($fCreatedStart, $this->_dateDisplayFormat);
			$createdFrom     = $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_CREATED) >= ?', $createdFrom);
		}

		if ($fCreatedEnd) {
			$FormatDate   = new Zend_Date($fCreatedEnd, $this->_dateDisplayFormat);
			$createdTo     = $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_CREATED) <= ?', $createdTo);
		}

		if ($fPaymentStart) {
			$FormatDate   = new Zend_Date($fPaymentStart, $this->_dateDisplayFormat);
			$payDateFrom     = $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) >= ?', $payDateFrom);
		}

		if ($fPaymentEnd) {
			$FormatDate   = new Zend_Date($fPaymentEnd, $this->_dateDisplayFormat);
			$payDateTo    = $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) <= ?', $payDateTo);
		}

		// $select->order($sortBy . ' ' . $sortDir);

		 // echo $select;die();

		$dataSQL = $this->_db->fetchAll($select);

		if ($csv || $pdf || $this->_request->getParam('print')) {
			$header  = Application_Helper_Array::simpleArray($fields, "label");
		} else {
			// $this->paging($dataSQL);
			// $this->view->paginator = $dataSQL;
		}

		$data = array();
		// echo '<pre>';
		// print_r($dataSQL);
		// die;
		foreach ($dataSQL as $d => $dt) {
			$data[$d]["PS_PERIODIC"] 		= $dt["PS_PERIODIC"];
			$data[$d]["PS_TYPE"] 		= $dt["PS_TYPE"];
			$data[$d]["PS_PERIODIC_NUMBER"] 		= $dt["PS_PERIODIC_NUMBER"] === null ? $dt["PS_NUMBER"] : $dt["PS_PERIODIC_NUMBER"];

			$PS_NUMBER = $dt["PS_NUMBER"];
			$countExecute = 0;
			$countSuccess = 0;
			$countUntransfered = 0;
			$countFailed = 0;
			if (isset($PS_NUMBER)) {
				$select = $this->_db->select()
					->from(array('C' => 'T_TRANSACTION', array('countExecute' => 'count(C.TRANSACTION_ID)', 'C.TRA_STATUS')))
					->where("C.PS_NUMBER  = ?", $PS_NUMBER);
				$count = $this->_db->fetchAll($select);
				// var_dump($count);die;
				if (is_array($count)) {
					foreach ($count as $key) {
						if ($key["TRA_STATUS"] == '3') {
							$countSuccess++;
						} elseif ($key["TRA_STATUS"] == '4') {
							$countFailed++;
						} elseif ($key["TRA_STATUS"] == '1') {
							$countUntransfered++;
						}
					}
					$countExecute = count($count);
				}
			}
			//Hitung total execute
			$data[$d]["TotalExecute"] 	= $countExecute;
			$data[$d]["countSuccess"] 	= $countSuccess;
			$data[$d]["countFailed"] 	= $countFailed;
			$data[$d]["countUntransfered"] 	= $countUntransfered;


			// get status ongoing
			$status = '';
			if ($dt["PS_PERIODIC_NUMBER"] === null) {
				if ($dt["PS_STATUS"] == '14') {
					$status = 2; //canceled
				} elseif ($dt["PS_STATUS"] == '7') {
					$status = 1; //ongoing
				} elseif ($dt["PS_STATUS"] == '5') {
					$status = 3; //ended
				}
			} else {
				if ($dt["PS_PERIODIC_STATUS"] == '0') {
					$status = 2; //canceled
				} elseif ($dt["PS_PERIODIC_STATUS"] == '2') {
					$status = 1; //ongoing
				} elseif ($dt["PS_PERIODIC_STATUS"] == '1') {
					$status = 3; //ended
				} elseif ($dt["PS_PERIODIC_STATUS"] == '25') {
					$status = 4; //hold
				}
			}
			$data[$d]["Info"] = $status;

			$payType1   = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
			
			$dt['TRANSFER_TYPE1'] = $trfType[$dt["TRANSFER_TYPE"]];

            $tra_type	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");

            $tra_type1 = $tra_type[$dt["TRANSFER_TYPE"]];
            
            if ($dt["PS_TYPE"] == 19) {
                 $payType = 'CP Same Bank Remains';
            }else if ($dt["PS_TYPE"] == 20) {
                 $payType = 'CP Same Bank Maintains';
            }else if ($dt["PS_TYPE"] == 23) {
                 $payType = 'CP Others Remains - '.$tra_type1;
            }else if ($dt["PS_TYPE"] == 21) {
                 $payType = 'MM - '.$tra_type1;
            }else if($dt['PS_TYPE'] == '29' || $dt['PS_TYPE'] == '30' || $dt['PS_TYPE'] == '25' || $dt['PS_TYPE'] == '4' || $dt['PS_TYPE'] == '26' || $dt['PS_TYPE'] == '28' || $dt['PS_TYPE'] == '3' || $dt['PS_TYPE'] == '27'){
				$payType = $dt['paytype'];
			}else if($dt['PS_TYPE'] == '14' || $dt['PS_TYPE'] == '15'){
				$payType = 'Sweep Same Bank';
			}
			else {
                $payType = $payType1[$dt["PS_TYPE"]].' - '.$dt['TRANSFER_TYPE1'];
            }

			
			
			

            $data[$d]["paytype1"] = $payType;
				
			foreach ($fields as $key => $field) {
				$value      = $dt[$key];
				if ($key == 'PS_TOTAL_AMOUNT' && $value != '-') {
					if($dt['PS_TYPE'] == '23' || $dt['PS_TYPE'] == '19' || $dt['PS_TYPE'] == '20'){
						$value = '-';
					}else{
						$value = $dt['PS_CCY'].' '.Application_Helper_General::displayMoney($value);
					}					
				}

				if ($key == 'PS_EVERY_PERIODIC_UOM') {
					if ($value === null) {
						$value = '1x';
					} elseif ($value == '1') {
						$value = 'Daily';
					} elseif ($value == '2') {
						$value = 'Weekly';
					} elseif ($value == '3') {
						$value = 'Monthly';
					} elseif ($value == '4') {
						$value = 'Yearly';
					} elseif ($value == '5') {
						$periodicEveryArr = array(
							'1' => $this->language->_('Monday'),
							'2' => $this->language->_('Tuesday'),
							'3' => $this->language->_('Wednesday'),
							'4' => $this->language->_('Thursday'),
							'5' => $this->language->_('Friday'),
							'6' => $this->language->_('Saturday'),
							'7' => $this->language->_('Sunday'),
						);
						$value = $this->language->_('Every Day of ') . $periodicEveryArr[$dt['PS_EVERY_PERIODIC']];
					} elseif ($value == '6') {
						$value = $this->language->_('Every Date of ') . $dt['PS_EVERY_PERIODIC'];
					} else {
						$value = '-';
					}
				} elseif ($key == "nextExecute") {
					// get next exute date
					if ($dt["PS_EVERY_PERIODIC_UOM"] == 1) {
						$select = $this->_db->select()
							->from(array('C' => 'T_PERIODIC_DAY', array('C.DAY_ID')))
							->where("C.PERIODIC_ID  = ?", $dt["PS_PERIODIC"]);

						$PERIODIC_DAY = $this->_db->fetchAll($select);
						$days = array();
						foreach ($PERIODIC_DAY as $key) {
							$days[] = (int) $key['DAY_ID'];
						}
						$date = $dt["PS_EFDATE"] . ' ' . $dt["PS_EFTIME"];
						$dateNow = date("Y-m-d H:i");
						$result = $date;
						foreach ($days as $day) {
							if ($dateNow > $result) {
								$date = $dateNow;
								$dayofweek = date('N', strtotime($date));
								$result = date('Y-m-d', strtotime(($day - $dayofweek) . ' day', strtotime($date)));
								$result = $result . ' ' . $dt["PS_EFTIME"];
							}
						}

						if ($result < $dateNow) {
							$date = date("Y-m-d", strtotime("+1 week", strtotime($date)));
							$dayofweek = date('N', strtotime($date));
							$result = date('Y-m-d H:i', strtotime(($days[0] - $dayofweek) . ' day', strtotime($date)));
						}
						$date = $result;
						$date = strtotime($date);
						if (date('Y-m-d', $date) == date('Y-m-d')) {
							$data[$d]["nextExecute"] = $this->language->_('Today') . ' @' . date('H:i', $date);
						} else {
							$data[$d]["nextExecute"] = date('d M Y @H:i', $date);
						}
					} elseif ($dt["PS_EVERY_PERIODIC_UOM"] == 2 || $dt["PS_EVERY_PERIODIC_UOM"] == 3) {
						$date = $dt["PS_PERIODIC_NEXTDATE"];
					} elseif ($dt["PS_EVERY_PERIODIC_UOM"] == 5) {
						$date = $dt["PS_EFDATE"] . ' ' . $dt["PS_EFTIME"];
						$dateNow = date("Y-m-d H:i");

						if ($dateNow > $date) {
							$date = $dateNow;
						}
						$dayofweek = date('N', strtotime($date));
						$result = date('Y-m-d', strtotime(($dt['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
						$result = $result . ' ' . $dt["PS_EFTIME"];
						if ($result < $dateNow) {
							$date = date("Y-m-d", strtotime("+1 week"));
							$result = date('Y-m-d', strtotime(($dt['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
						}
						$dt["PS_EFDATE"] = date("Y-m-d", strtotime($result));
					} elseif ($dt["PS_EVERY_PERIODIC_UOM"] == 6) {
						$date = $dt["PS_EFDATE"] . ' ' . $dt["PS_EFTIME"];
						$dateNow = date("Y-m-d H:i");

						if ($date < $dateNow) {
							$dt["PS_EFDATE"] = date("Y-m-d", strtotime("+1 month", strtotime($date)));
						}
					}

					if ($dt["PS_EVERY_PERIODIC_UOM"] != 1) {
						$date = $dt["PS_EFDATE"] . ' ' . $dt["PS_EFTIME"];
						// var_dump($date);
						$date = strtotime($date);
						if (date('Y-m-d', $date) == date('Y-m-d')) {
							$value = $this->language->_('Today') . ' @' . date('H:i', $date);
						} else {
							$value = date('d M Y @H:i', $date);
						}
					}
				}

				$value = ($value == "" && !$csv) ? "&nbsp;" : $value;
				$data[$d][$key] = $this->language->_($value);
			}
		}
		//echo '<pre>';
		//var_dump($data);die('ge');
		//filter
		if ($frequently) {
			foreach ($data as $key => $value) {
				if ($value['PS_EVERY_PERIODIC_UOM'] == $frequently) {
					$newData[$key] = $value;
				}
			}

			$data = $newData;
		}

		if ($ongoingStatus) {
			foreach ($data as $key => $value) {
				if ($value['Info'] == $ongoingStatus) {
					$newData[$key] = $value;
				}
			}
			$data = $newData;
		}

		if($next_execute_start || $next_execute_end){
			foreach ($data as $key => $value) {
				$explodeDate = explode('@', $value['nextExecute']);

				$dateTime = date_create($explodeDate[0]); 
				$newDateTime = date_format($dateTime, "d-m-Y");

				if (empty($next_execute_start)) {
					$next_execute_start = date('d-m-Y');
				}

				if(empty($next_execute_end)){
					$next_execute_end = date('d-m-Y');
				}

				// echo strtotime($newDateTime).' -------- '.strtotime($next_execute_start) .' ------ '.strtotime($next_execute_end);die();

				if (strtotime($newDateTime) >= strtotime($next_execute_start) && strtotime($newDateTime) <= strtotime($next_execute_end) && $value['Info'] == 1) {
					$newData[$key] = $value;
				}
			}

			$data = $newData;
		}

		if (empty($data)) {
			$data = array();
		}

		// print_r($header);
		//sorting Info
		// echo "<pre>";
		 //var_dump($data);
		 //die();

		$this->view->compName = $this->_custNameLogin;

		$this->paging($data);

		$filterlistdata = array("Filter");
		foreach($dataParamValue as $fil => $val){
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
		}
		if ($csv) {

			$listable = array_merge_recursive(array($header), $data);
			$this->_helper->download->csv($filterlistdata, $listable, null, 'List View Payment');
			Application_Helper_General::writeLog('DARC', 'Export CSV View Payment');
		} elseif ($pdf) {

			$this->view->filterSet = $dataParamValue;
			$this->view->payType = $payType;
			$this->view->clearfilter     = $clearfilter; //display all

			$outputHTML = "<tr><td>" . $this->view->render('/ongoing/indexpdf1.phtml') . "</td></tr>";
			$this->_helper->download->newPdf(null, null, null, $this->language->_('Ongoing Transaction'), $outputHTML);
		} else if($this->_request->getParam('print')){

			foreach($data as $key=>$row)
			{

				if ($row['Info'] == 3) {
					$STATUS = "Ended";
				}else if ($row['Info'] == 2) {
					$STATUS = "Canceled";
				}else if ($row['Info'] == 1) {
					$STATUS = "Ongoing";
				}

				$data[$key]['Info'] = $STATUS;
				
			}
			$filterlistdatax = $this->_request->getParam('data_filter');
			$this->_forward('printtable', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Ongoing Transaction', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
		}
		else {
		$stringParam = array(
			'updatedStart'   => $fUpdatedStart,
			'updatedEnd'  => $fUpdatedEnd,
			'createdStart'  => $fCreatedStart,
			'createdEnd'  => $fCreatedEnd,
			'paymentStart'  => $fPaymentStart,
			'paymentEnd'  => $fPaymentEnd,
			'accsrc'    => $fAcctsrc,
			'payReff'    => $fPaymentReff,
			'paymentStatus'  => $fPaymentStatus,
			'paymentType'  => $fPaymentType,
			'transferType'  => $fTransferType,
			'clearfilter'  => $clearfilter,

		);
			/*
			$stringParam = array('payReff'		=> $fPaymentReff,
								 'payType'		=> $fPaymentType,
								 'trfType'		=> $fTrfType,
								 'createdFrom'	=> $fCreatedFrom,
								 'createdTo'	=> $fCreatedTo,
								 'payDateFrom'	=> $fPayDateFrom,
								 'payDateTo'	=> $fPayDateTo,
							    );
			*/

			//echo "fPaymentStatus: $fPaymentStatus";
			//echo "fAcctsrc: $fAcctsrc";
			//echo "fPaymentType: $fPaymentType";

			// $this->view->filterPayType 		= $filterPayType;
			// $this->view->optfilterPayStatus 	= $optfilterPayStatus;
			// $this->view->CustomerArr 		= $CustomerArr;
			// print_r($fCreatedEnd);die;
			// print_r($optarrAccount);die;
			$this->view->optPayType   = $optPayType;
			$this->view->optPayStatus   = $optPayStatus;
			$this->view->optarrAccount   = $optarrAccount;


			// $this->view->updatedStart   = $fUpdatedStart;
			// $this->view->updatedEnd   = $fUpdatedEnd;
			// $this->view->createdStart   = $fCreatedStart;
			// $this->view->createdEnd   = $fCreatedEnd;
			// $this->view->paymentStart   = $fPaymentStart;
			// $this->view->paymentEnd   = $fPaymentEnd;

			$this->view->nextexecuteStart   = $next_execute_start;
			$this->view->nextexecuteEnd   = $next_execute_end;
			$this->view->accsrc     = $fAcctsrc;
			$this->view->payReff     = $fPaymentReff;
			$this->view->paymentStatus   = $fPaymentStatus;
			$this->view->paymentType   = $fPaymentType;
			$this->view->transferType   = $fTransferType;

			//$this->view->query_string_params = $stringParam;
			// print_r($fields);die;
			if (!empty($dataParamValue)) {

				$this->view->createdStart = $dataParamValue['PS_CREATED'];
				$this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
				$this->view->efdateStart = $dataParamValue['PS_UPDATED'];
				$this->view->efdateEnd = $dataParamValue['PS_UPDATED_END'];
				$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
				$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];


				unset($dataParamValue['PS_CREATED_END']);
				unset($dataParamValue['PS_EFDATE_END']);
				unset($dataParamValue['PS_UPDATED_END']);
				unset($dataParamValue['NEXT_EXECUTE_END']);

				foreach ($dataParamValue as $key => $value) {
					$duparr = explode(',',$value);
								if(!empty($duparr)){
									
									foreach($duparr as $ss => $vs){
										$wherecol[]	= $key;
										$whereval[] = $vs;
									}
								}else{
										$wherecol[]	= $key;
										$whereval[] = $value;
								}
				}
				$this->view->wherecol     = $wherecol;
				$this->view->whereval     = $whereval;
			}

			$this->view->data         = $data;
			$this->view->fields       = $fields;
			$this->view->filter       = $filter;
			$this->view->sortBy       = $sortBy;
			$this->view->sortDir       = $sortDir;
			$this->view->clearfilter     = $clearfilter;
			$this->view->filterlistdata = $filterlistdata;

			// $this->view->currentPage 		= $page;
			// $this->view->paginator 			= $data;

			Application_Helper_General::writeLog('DARC', 'View Payment');

			//$this->view->sortBy = $sortBy;
			//$this->view->sortDir = $sortDir;

		}
	}

	public function viewdetailAction()
	{

		$this->_helper->_layout->setLayout('newlayout');
		$filter       = new Application_Filtering();
		$AESMYSQL = new Crypt_AESMYSQL();

		$trfType1 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);	
        $payType1   = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;
		
		$settingObj = new Settings();
		$tokenType = $settingObj->getSetting('cut_off_time_skn', '1');

				$conf = Zend_Registry::get('config');

				$tokenTypeGlobalVar = $conf['token']['type']['code'];
				//var_dump($tokenTypeGlobalVar);die;
				//if google auth
				//if ($tokenType == $tokenTypeGlobalVar['googleauth']) {

					$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
	                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
					$usergoogleAuth =  $this->_db->fetchAll($selectQuery);
					
					$check = $this->_db->select()
    							->from('M_DAILYLIMIT',array('USER_LOGIN'))
    							->where('USER_LOGIN = ?', (string)$this->_userIdLogin)
    							->where('CUST_ID = ?', (string)$this->_custIdLogin)
    							->where('CCY_ID = ?', 'IDR')
								->where('DAILYLIMIT >= ?', 1)
    							->where('DAILYLIMIT_STATUS = ?', '1');
    		 		$checkdata = $this->_db->fetchOne($check);
					
					if (!empty($usergoogleAuth) && $checkdata) {
						if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
							$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
							$maxtoken = $settingObj->getSetting("max_failed_token");
							$tokenfail = (int)$maxtoken - (int)$usergoogleAuth['0']['USER_FAILEDTOKEN'];
							$this->view->tokenfail = $tokenfail;
						} 
					}
					else{
						$this->view->nogoauth = true;
					}

					$googleAuth = true;
					$this->view->googleauth = true;
				//}
				
		
		
		$this->view->masterBankName = $this->_bankName;
		
		if($this->_custSameUser){
			$this->view->custSameUser = true;
		}
		
		
		$inputtoken1 		= $this->_getParam('inputtoken1');
		$inputtoken2 		= $this->_getParam('inputtoken2');
		$inputtoken3 		= $this->_getParam('inputtoken3');
		$inputtoken4 		= $this->_getParam('inputtoken4');
		$inputtoken5 		= $this->_getParam('inputtoken5');
		$inputtoken6 		= $this->_getParam('inputtoken6');
		$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

		//if google auth
		if ($reExecute && $googleAuth) {

			$pga = new PHPGangsta_GoogleAuthenticator();
			$setting 		= new Settings();
			$google_duration 	= $setting->getSetting('google_duration');
			if ($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, $google_duration)) {
			 	$resultToken = true;
			 	$datatoken = array(
															'USER_FAILEDTOKEN' => 0
														);

														$wheretoken =  array();
														$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
														$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
														$data = $this->_db->update('M_USER',$datatoken,$wheretoken);
			} else {
			 	$resultToken = false;
			 	$tokenFailed = $CustUser->setLogToken(); //log token activity
			 	if ($tokenFailed === true) {
			 		$this->_redirect('/default/index/logout');
			 	}
			}
		}
		
		$reExecute = $this->_getParam('reExecute');
		$repsnumb = $this->_getParam('ps_number');
		
		if ($reExecute && $resultToken) {
			$Payment = new Payment($repsnumb, $this->_custIdLogin, $this->_userIdLogin);
			$resultRelease = $Payment->reReleasePayment();

			if ($resultRelease['status'] == '00') {
				$this->setbackURL('/display/executedtransaction');
				$this->_redirect('/notification/success/index');
			} else {
				$this->_helper->getHelper('FlashMessenger')->addMessage($detail[0]['PS_NUMBER']);
				$this->setbackURL('/display/executedtransaction');
				$this->_redirect('/notification/index/release');
			}
		}
		else if($reExecute && !$resultToken){
			//show token error
			$errorMsg = $this->language->_('Invalid Response Code');
			$this->view->popauth = true;
			$this->view->error_msg = $errorMsg;
		}
		
		

		if ($this->_getParam('cancelBtn') == TRUE) {
			$payReff       = urldecode($filter->filter($this->_getParam('payReff'), "PS_NUMBER"));
			$payReff = $AESMYSQL->decrypt($payReff, $password);

			$PS_NUMBER       = urldecode($filter->filter($this->_getParam('PERIODIC_ID'), "PS_NUMBER"));
			$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

			$select  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'), array(
					'P.PS_STATUS',
					'P.PS_NUMBER',
					'A.PS_PERIODIC_NUMBER',
					'A.PS_PERIODIC_STATUS',
					'A.PS_PERIODIC'
				))
				->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
				->where('P.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchRow($select);

			if ($data['PS_PERIODIC_NUMBER'] === null) {
				$update['PS_STATUS']   = '14';
				$where = array('PS_NUMBER = ?' => $data['PS_NUMBER']);
				$this->_db->update('T_PSLIP', $update, $where);
			} else {
				$update['PS_PERIODIC_STATUS']   = '0';
				$where = array('PS_PERIODIC = ?' => $data['PS_PERIODIC']);
				$this->_db->update('T_PERIODIC', $update, $where);
				
				$updates['PS_STATUS']   = '14';
				$wheres = array('PS_NUMBER = ?' => $data['PS_NUMBER']);
				$this->_db->update('T_PSLIP', $updates, $wheres);
			}

			$historyInsert = array(
				'DATE_TIME' 		=> new Zend_Db_Expr("now()"),
				'PS_NUMBER' 		=> $data['PS_NUMBER'],
				'CUST_ID' 			=> $this->_custIdLogin,
				'USER_LOGIN' 		=> $this->_userIdLogin,
				'HISTORY_STATUS' 	=> 14,
			);

			$this->_db->insert('T_PSLIP_HISTORY', $historyInsert);
			Application_Helper_General::writeLog('DARC', 'Cancel Ongoing Payment');
		}
		
		
		
		if ($this->_getParam('holdBtn') == TRUE) {
			

			$PS_NUMBER       = urldecode($filter->filter($this->_getParam('HOLDPERIODIC_ID'), "PS_NUMBER"));
			$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

			$select  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'), array(
					'P.PS_STATUS',
					'P.PS_NUMBER',
					'A.PS_PERIODIC_NUMBER',
					'A.PS_PERIODIC_STATUS',
					'A.PS_PERIODIC'
				))
				->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
				->where('P.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchRow($select);

			if ($data['PS_PERIODIC_NUMBER'] === null) {
				$update['PS_STATUS']   = '25';
				$where = array('PS_NUMBER = ?' => $data['PS_NUMBER']);
				$this->_db->update('T_PSLIP', $update, $where);
			} else {
				$update['PS_PERIODIC_STATUS']   = '25';
				$where = array('PS_PERIODIC = ?' => $data['PS_PERIODIC']);
				$this->_db->update('T_PERIODIC', $update, $where);
				
				$updates['PS_STATUS']   = '25';
				$wheres = array('PS_NUMBER = ?' => $data['PS_NUMBER']);
				$this->_db->update('T_PSLIP', $updates, $wheres);
			}

			$historyInsert = array(
				'DATE_TIME' 		=> new Zend_Db_Expr("now()"),
				'PS_NUMBER' 		=> $data['PS_NUMBER'],
				'CUST_ID' 			=> $this->_custIdLogin,
				'USER_LOGIN' 		=> $this->_userIdLogin,
				'HISTORY_STATUS' 	=> 25,
			);

			$this->_db->insert('T_PSLIP_HISTORY', $historyInsert);
			Application_Helper_General::writeLog('DARC', 'Hold Ongoing Payment');
		}

		// $PS_NUMBER is PS_PERIODIC

		$PS_NUMBER       = urldecode($filter->filter($this->_getParam('payReff'), "PS_NUMBER"));
		$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);
		// print_r($PS_NUMBER);die();

		$pdf         = $this->_getParam('pdf');
		$cancelfuturedatebtn = $filter->filter($this->_getParam('cancelfuturedate'), "BUTTON");
		$backBtn      = $filter->filter($this->_getParam('back'), "BUTTON");

		$select  = $this->_db->select()
			->from(array('P' => 'T_PSLIP'), array('P.*', 'A.*'))
			->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
			->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
			->where('P.PS_NUMBER = ?', $PS_NUMBER);


		$cekPsNumber = $this->_db->fetchRow($select);
		// echo '<pre>';
		// print_r($cekPsNumber);die;
		$pslip = $cekPsNumber;

		$PS_NUMBER = $pslip['PS_NUMBER'];
		
		

		$PS_STATUS = $pslip['PS_STATUS'];
		$PS_PERIODIC_NUMBER = $pslip['PS_PERIODIC_NUMBER'];
		$PS_EVERY_PERIODIC_UOM = $pslip['PS_EVERY_PERIODIC_UOM'];
		$PS_EVERY_PERIODIC = $pslip['PS_EVERY_PERIODIC'];
		$PS_PERIODIC_STATUS = $pslip['PS_PERIODIC_STATUS'];

		$this->view->PS_TYPE = $pslip['PS_TYPE'];
		$this->view->PS_NUMBER = $pslip['PS_NUMBER'];
		$this->view->PS_STATUS = $PS_STATUS;
		$this->view->PS_PERIODIC = $pslip['PS_PERIODIC'];
		$this->_paymentRef   = $PS_NUMBER;

		//cari policy
		$policyBoundary = $this->findPolicyBoundary($pslip['PS_TYPE'], $pslip['PS_TOTAL_AMOUNT']);

		$checkBoundary = $this->validatebtn($pslip['PS_TYPE'], $pslip['PS_TOTAL_AMOUNT'], $pslip['PS_CCY'], $pslip['PS_NUMBER']);

		//cek privilage
		$custidlike = '%' . $this->_custIdLogin . '%';

		//select user with reviewer privilage
		$selectReviewer = $this->_db->select()
			->from(array('P' => 'M_FPRIVI_USER'))
			->from(array('U' => 'M_USER'))
			->where("P.FPRIVI_ID = 'RVPV' AND P.FUSER_ID LIKE ?", (string) $custidlike)
			->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
			->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

		$userReviewer = $this->_db->fetchAll($selectReviewer);

		$reviewerList = array();
		foreach ($userReviewer as $row) {
			$userIdReviewer = explode($this->_custIdLogin, $row['FUSER_ID']);

			//get user name
			$selectReviewerName = $this->_db->select()
				->from('M_USER')
				->where("USER_ID = ?", (string) $userIdReviewer[1]);

			$userReviewerName = $this->_db->fetchAll($selectReviewerName);

			array_push($reviewerList, $userReviewerName[0]['USER_FULLNAME']);
		}

		//function utk munculin button dengan policy grup jika belum ada yg approve
		if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5' || $pslip['PS_TYPE'] == '11') {
			$PS_TYPE = '18';
		} else {
			$PS_TYPE = $pslip['PS_TYPE'];
		}

		$approverUserList = $this->findUserBoundary($PS_TYPE, $pslip['PS_TOTAL_AMOUNT']);


		//select user with releaser privilage
		$selectReleaser = $this->_db->select()
			->from(array('P' => 'M_FPRIVI_USER'))
			->from(array('U' => 'M_USER'))
			->where("P.FPRIVI_ID = 'PRLP' AND P.FUSER_ID LIKE ?", (string) $custidlike)
			->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
			->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

		$userReleaser = $this->_db->fetchAll($selectReleaser);

		$releaserList = array();
		foreach ($userReleaser as $row) {
			$userIdReleaser = explode($this->_custIdLogin, $row['FUSER_ID']);

			//get user name
			$selectReleaserName = $this->_db->select()
				->from('M_USER')
				->where("USER_ID = ?", (string) $userIdReleaser[1]);

			$userReleaserName = $this->_db->fetchAll($selectReleaserName);

			array_push($releaserList, $userReleaserName[0]['USER_FULLNAME']);
		}

		//cek ada privilage reviewer atau approver
		$selectpriv = $this->_db->select()
			->from(array('M_CUSTOMER'))
			->where("CUST_ID = ?", $this->_custIdLogin);

		$userpriv = $this->_db->fetchAll($selectpriv);

		if ($userpriv[0]['CUST_REVIEW'] != 1) {
			$cust_reviewer = 0;
		} else {
			$cust_reviewer = 1;
		}

		if ($userpriv[0]['CUST_APPROVER'] != 1) {
			$cust_approver = 0;
		} else {
			$cust_approver = 1;
		}

		$lastpsnumber = array();
		if(!empty($pslip['PS_PERIODIC'])){
		$selectlast  = $this->_db->select()
			->from('T_PSLIP')
			->where("PS_PERIODIC = ?", $pslip['PS_PERIODIC'])
			->order('PS_CREATED ASC');
			//echo $selectlast;
		$lastpsnumber = $this->_db->fetchRow($selectlast);
		
		}else{
			$lastpsnumber['PS_NUMBER'] = $pslip['PS_NUMBER'];
		}
		
		
		//var_dump($lastpsnumber);die;
		//var_dump($lastpsnumber);die;

		$selectHistory  = $this->_db->select()
			->from('T_PSLIP_HISTORY')
			->where("PS_NUMBER = ?", $lastpsnumber['PS_NUMBER']);

		$history = $this->_db->fetchAll($selectHistory);

		foreach ($history as $row) {
			//if maker done
			if ($row['HISTORY_STATUS'] == 1) {
				$makerStatus = 'active';
				$makerIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
					$reviewerOngoing = '';
					$approverOngoing = '';
					$releaserOngoing = 'ongoing';
				} else {
					$reviewerOngoing = 'ongoing';
					$approverOngoing = '';
					$releaserOngoing = '';
				}

				$custlogin = $row['USER_LOGIN'];

				$selectCust  = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin)
					->where("CUST_ID = ?", $row['CUST_ID']);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				$custEmail     = $customer[0]['USER_EMAIL'];
				$custPhone    = $customer[0]['USER_PHONE'];

				$makerApprovedBy = $custFullname;

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

				$align = 'align="center"';
				$marginRight = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = 'center';
					// $marginRight = 'style="margin-right: 15px;"';
				}

				$this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
			}
			//if reviewer done
			if ($row['HISTORY_STATUS'] == 15) {
				$makerStatus = 'active';
				$reviewStatus = 'active';
				$reviewIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'ongoing';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				$selectCust  = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin)
					->where("CUST_ID = ?", $row['CUST_ID']);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$reviewerApprovedBy = $custFullname;

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->reviewerApprovedBy = '<div align="center" class="textTheme">' . $efdate . '<br>' . $custFullname . '</div>';
			}
			//if approver done
			if ($row['HISTORY_STATUS'] == 2) {
				$makerStatus = 'active';
				$approveStatus = '';
				$reviewStatus = 'active';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'ongoing';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				//tampung data user yang sudah approve
				$userid[] = $custlogin;

				$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
			}
			//if releaser done
			if ($row['HISTORY_STATUS'] == 5) {
				$makerStatus = 'active';
				$approveStatus = 'active';
				$reviewStatus = 'active';
				$releaseStatus = 'active';
				$releaseIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = '';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				$selectCust  = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin)
					->where("CUST_ID = ?", $row['CUST_ID']);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$releaserApprovedBy = $custFullname;

				$align = 'align="center"';
				$marginLeft = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginLeft = 'style="margin-left: 15px;"';
				}

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';

				break;
			}
		}

		//approvernamecircle jika sudah ada yang approve
		if (!empty($userid)) {

			$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

			$flipAlphabet = array_flip($alphabet);

			$approvedNameList = array();
			$i = 0;
			foreach ($userid as $key => $value) {

				//select utk nama dan email
				$selectusername = $this->_db->select()
					->from(array('M_USER'), array(
						'*'
					))
					->where("CUST_ID = ?" ,$this->_custIdLogin)
					->where("USER_ID = ?", (string) $value);

				$username = $this->_db->fetchAll($selectusername);

				//select utk cek user berada di grup apa
				$selectusergroup  = $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array(
						'*'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.USER_ID 	= ?", (string) $value);

				$usergroup = $this->_db->fetchAll($selectusergroup);

				$groupuserid = $usergroup[0]['GROUP_USER_ID'];
				$groupusername = $usergroup[0]['USER_ID'];
				$groupuseridexplode = explode("_", $groupuserid);

				if ($groupuseridexplode[0] == "S") {
					$usergroupid = "SG";
					$approveIcon = '<i class="fas fa-check"></i>';
				} else {
					$usergroupid = $alphabet[$groupuseridexplode[2]];
				}

				// $tempuserid = "";
				// foreach ($approverNameCircle as $row => $data) {
				// 	foreach ($data as $keys => $val) {
				// 		if ($keys == $usergroupid) {
				// 			if (preg_match("/active/", $val)) {
				// 				continue;
				// 			}else{
				// 				if ($groupuserid == $tempuserid) {
				// 					continue;
				// 				}else{
				// 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
				// 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
				// 				}
				// 				$tempuserid = $groupuserid;
				// 			}
				// 		}
				// 	}
				// }

				array_push($approvedNameList, $username[0]['USER_FULLNAME']);

				$efdate = $approveEfDate[$i];

				$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

				$i++;
			}

			$this->view->approverApprovedBy = $approverApprovedBy;

			//kalau sudah approve semua
			if (!$checkBoundary) {
				$approveStatus = 'active';
				$approverOngoing = '';
				$approveIcon = '<i class="fas fa-check"></i>';
				$releaserOngoing = 'ongoing';
			}
		}

		$selectsuperuser = $this->_db->select()
			->from(array('C' => 'T_APPROVAL'))
			->where("C.PS_NUMBER 	= ?", $PS_NUMBER)
			->where("C.GROUP 	= 'SG'");

		$superuser = $this->_db->fetchAll($selectsuperuser);

		if (!empty($superuser)) {
			$userid = $superuser[0]['USER_ID'];
			//select utk nama dan email
			$selectusername = $this->_db->select()
				->from(array('M_USER'), array(
					'*'
				))
				->where("CUST_ID = ?" ,$this->_custIdLogin)
				->where("USER_ID = ?", (string) $userid);

			$username = $this->_db->fetchAll($selectusername);


			$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

			$approveStatus = 'active';
			$approverOngoing = '';
			$approveIcon = '<i class="fas fa-check"></i>';
			$releaserOngoing = 'ongoing';
		}

		// $this->view->approverApprovedBy = $approverApprovedBy;
		// echo "<pre>";
		// var_dump($pslip);die;




		//define circle
		$makerNameCircle = '<img src="/assets/themes/assets/newlayout/img/maker.PNG"> <br/><button id="makerCircle" class="btnCircleGroup ' . $makerStatus . ' hovertext" disabled>' . $makerIcon . '</button>';
		// echo $makerNameCircle;die;
		// var_dump($makerNameCircle);die;

		foreach ($reviewerList as $key => $value) {

			$textColor = '';
			if ($value == $reviewerApprovedBy) {
				$textColor = 'text-white-50';
			}

			$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
		}

		$reviewerNameCircle = '<img src="/assets/themes/assets/newlayout/img/reviewer.png"> <br/><button class="btnCircleGroup ' . $reviewStatus . ' hovertext" disabled>' . $reviewIcon . '</button>';

		$groupNameList = $approverUserList['GROUP_NAME'];
		unset($approverUserList['GROUP_NAME']);

		if ($approverUserList != '') {
			foreach ($approverUserList as $key => $value) {
				$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
				$i = 1;
				foreach ($value as $key2 => $value2) {

					$textColor = '';
					if (in_array($value2, $approvedNameList)) {
						$textColor = 'text-white-50';
					}

					if ($i == count($value)) {
						$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
					} else {
						$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
					}
					$i++;
				}
			}
		} else {
			$approverListdata = 'There is no Approver User';
		}

		$approverNameCircle = '<img src="/assets/themes/assets/newlayout/img/approver.png"> <br/><button class="btnCircleGroup ' . $approveStatus . ' hovertext" disabled>' . $approveIcon . '</button>';

		foreach ($releaserList as $key => $value) {

			$textColor = '';
			if ($value == $releaserApprovedBy) {
				$textColor = 'text-white-50';
			}

			$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
		}

		$releaserNameCircle = '<img src="/assets/themes/assets/newlayout/img/releaser.png"> <br/><button id="releaserCircle" class="btnCircleGroup  ' . $releaseStatus . ' hovertext" disabled>' . $releaseIcon . '</button>';

		$this->view->cust_reviewer = $cust_reviewer;
		$this->view->cust_approver = $cust_approver;


		$this->view->policyBoundary = $policyBoundary;
		$this->view->makerNameCircle = $makerNameCircle;
		$this->view->reviewerNameCircle = $reviewerNameCircle;
		$this->view->approverNameCircle = $approverNameCircle;
		$this->view->releaserNameCircle = $releaserNameCircle;
	
		$makerStatus = 'active';

		$this->view->makerStatus = $makerStatus;
		$this->view->approveStatus = $approveStatus;
		$this->view->reviewStatus = $reviewStatus;
		$this->view->releaseStatus = $releaseStatus;

		if (!$cekPsNumber) {

			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Invalid FreqRef#");
			$this->_redirect('/display/viewpayment/index');
		} else {
			// var_dump($cancelfuturedatebtn);
			if ($cancelfuturedatebtn) {
				$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
				$Payment->cancelFutureDate();

				$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/payReff/' . $this->_paymentRef);
				$this->_redirect('/notification/success');
			}

			$paramPayment = array(
				"WA"         => false,
				"ACCOUNT_LIST"   => $this->_accountList,
				"_beneLinkage"   => $this->view->hasPrivilege('BLBU'),
			);

			// get payment query
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$select   = $CustUser->getPaymentOutgoing($paramPayment);
			$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);
//echo $select;
			$pslip = $this->_db->fetchRow($select);
//echo '<pre>';
//					var_dump($pslip);die;
			$PSSTATUS  = $pslip["PS_STATUS"];
			$PSNUMBER  = $pslip["payReff"];
			$payStatus = $pslip["payStatus"];

			$this->view->AccArr = $CustUser->getAccounts();
			$this->view->AccArrDomestic = $CustUser->getAccounts(array("CCY_IN" => array("IDR")));

			// Set variables needed in view
			$settings = new Application_Settings();
			$ccyList  = $settings->setCurrencyRegistered();

			$lldIdenticalArr    = $settings->getLLDDOMIdentical();
			$lldRelationshipArr = $settings->getLLDDOMRelationship();
			$lldCategoryArr = $settings->getLLDDOMCategory();

			$model = new purchasing_Model_Purchasing();

			$purposeArr = $model->getTranspurpose();

			$purposeList = array('' => '-- Select Transaction Purpose --');
			foreach ($purposeArr as $key => $value) {
				$purposeList[$value['CODE']] = $value['DESCRIPTION'];
			}

			$this->view->TransPurposeArr = $purposeList;

			$selectuser = $this->_db->select()
				->from(array('A' => 'M_USER'));
			$selectuser->where("A.CUST_ID = " . $this->_db->quote($this->_custIdLogin));
			$this->view->dataact = $selectuser->query()->fetchAll();

			$selectccy = $this->_db->select()
				->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
			$this->view->dataccy = $selectccy->query()->fetchAll();

			// print_r($PSNUMBER);die;

			$selecttransaction = $this->_db->select()
				->from(array('A' => 'T_TRANSACTION'), array('*'))
				->where("A.PS_NUMBER = ?", $PS_NUMBER);

			$this->view->datatrx = $selecttransaction->query()->fetchAll();

			// print_r($this->view->datatrx);die();


			$settings->setSettings(null, $paramSettingID);              // Zend_Registry => 'APPSETTINGS'
			$ccyList        = $settings->setCurrencyRegistered();        // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'

			//for skn rtgs
			$lldTypeArr      = $settings->getLLDDOMType();
			$lldCategoryArr    = $settings->getLLDDOMCategory();
			$lldIdenticalArr    = $settings->getLLDDOMIdentical();
			$lldRelationshipArr = $settings->getLLDDOMRelationship();
			$lldPurposeArr     = $settings->getLLDDOMPurpose();
			$lldBeneIdentifArr   = $settings->getLLDDOMBeneIdentification();
			$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
			$residentArr     = array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
			$citizenshipArr    = array("W" => "WNI", "N" => "WNA");

			$this->view->residentArr = $residentArr;
			$this->view->citizenshipArr = $citizenshipArr;
			$this->view->lldCategoryArr = $lldCategoryArr;
			$this->view->lldBeneIdentifArr = $lldBeneIdentifArr;
			$this->view->TransactorArr = $lldRelationshipArr;
			$this->view->IdentyArr = $lldIdenticalArr;


			$anyValue = '-- ' . $this->language->_('Select City') . ' --';
			$select = $this->_db->select()
				->from(array('A' => 'M_CITY'), array('*'));
			$select->order('CITY_NAME ASC');
			$arr = $this->_db->fetchall($select);
			$cityCodeArr       = array('' => $anyValue);
			$cityCodeArr       += Application_Helper_Array::listArray($arr, 'CITY_CODE', 'CITY_NAME');
			$this->view->cityCodeArr   = $cityCodeArr;

			if ($this->view->hasPrivilege('MTSP')) {
				$usetemp = 1;
			} else {
				$usetemp = 0;
			}

			$this->view->usetemplate = $usetemp;

			if ($pslip["PS_TYPE"] == '19' || $pslip["PS_TYPE"] == '20') {

				if ($pslip["PS_TYPE"] == '19') {
					$tableTransSum[0]["label"] = $this->language->_('Remains on source');	
				}else{
					$tableTransSum[0]["label"] = $this->language->_('Maintains on beneficiary');
				}
				
				$tableTransSum[1]["label"] = $this->language->_('Minimum Transfer Amount');
				
				$tableTransSum[0]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['TRA_REMAIN']);
				$tableTransSum[1]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['PS_MIN_AMOUNT']);
				
				$this->view->tableTransSum 		= $tableTransSum;
				
			}else{

				//trans summary data
				$tableTransSum[0]["label"] = $this->language->_('Amount');
				$tableTransSum[1]["label"] = $this->language->_('Total Transfer Fee');
				$tableTransSum[2]["label"] = $this->language->_('Total');
				
				if(!empty($pslip['LLD_TRANSACTION_PURPOSE'])){
						$tableTransSum[3]["label"] = $this->language->_('Beneficiary Category');
						$tableTransSum[4]["label"] = $this->language->_('Identity');
						$tableTransSum[5]["label"] = $this->language->_('Transactor Relationship');
						$tableTransSum[6]["label"] = $this->language->_('Transactor Purpose');
				}
		
				$tableTransSum[0]["value"] = $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']);
				$tableTransSum[1]["value"] = Application_Helper_General::displayMoney($pslip['FULL_AMOUNT_FEE'] == '0' ? '-' : $pslip['ccy'] . ' ' . $pslip['FULL_AMOUNT_FEE']);
				$tableTransSum[2]["value"] = $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney(($pslip['PS_TOTAL_AMOUNT'] + $pslip['FULL_AMOUNT_FEE']));
				if ($pslip["PS_BILLER_ID"] == '1158' && $pslip["PS_TYPE"] == '17') {
					$tableTransSum = $this->etax($pslip);
				}
				
				if(!empty($pslip['LLD_TRANSACTION_PURPOSE'])){
					
					if (!empty($pslip['LLD_IDENTITY'])) {
							$lldidentity = $pslip['LLD_IDENTITY'];
							$lldident = $lldIdenticalArr[$lldidentity];
						}
						$pslip['LLD_IDENTITY'] = $lldident;
						
						if (!empty($pslip['LLD_TRANSACTOR_RELATIONSHIP'])) {
							$lldrelation = $pslip['LLD_TRANSACTOR_RELATIONSHIP'];
							$lldrelations = $lldRelationshipArr[$lldrelation];
						}
						$pslip['LLD_TRANSACTOR_RELATIONSHIP'] = $lldrelations;
						
						if (!empty($pslip['LLD_TRANSACTION_PURPOSE'])) {
							$lldp = $pslip['LLD_TRANSACTION_PURPOSE'];
							$lldpurpose = $lldPurposeArr[$lldp];
						}
						$pslip['LLD_TRANSACTION_PURPOSE'] = $lldpurpose;
						
						
						if (!empty($pslip['BENEFICIARY_CATEGORY'])) {
							$b_cat = $pslip['BENEFICIARY_CATEGORY'];
							$bcategory = $lldCategoryArr[$b_cat];
							$pslip['BENEFICIARY_CATEGORY'] = $bcategory;
						}
						
						$tableTransSum[3]["value"] = $pslip['BENEFICIARY_CATEGORY'];
						$tableTransSum[4]["value"] = $pslip['LLD_TRANSACTION_PURPOSE'];
						$tableTransSum[5]["value"] = $pslip['LLD_IDENTITY'];
						$tableTransSum[6]["value"] = $pslip['LLD_TRANSACTOR_RELATIONSHIP'];
						
				}
				 // echo '<pre>';
				 // var_dump($pslip);die;
				$this->view->tableTransSum 		= $tableTransSum;

			}

			
			if ($PSSTATUS == 5) {

				// COMPLETED WITH (_) TRANSACTION (S) FAILED
				// TRA_STATUS FAILED (4)

				$select = $this->_db->select()
					->from('T_TRANSACTION', array('countfailed' => 'count(TRANSACTION_ID)'))
					->where("TRA_STATUS = '4' AND PS_NUMBER = ?", $PSNUMBER);
				$countFailed = $this->_db->fetchOne($select);

				if ($countFailed == 0)   $value = $payStatus;
				else           $value = 'Completed with ' . $countFailed . ' Failed Transaction(s)';
			} else $value = $payStatus;

			//ambil dari slip_detail - begin
			$getPaymentDetail   = new display_Model_Paymentreport();
			$pslipdetail = $getPaymentDetail->getPslipDetail($PS_NUMBER);

			$htmldataDetailDetail = '';

			foreach ($pslipdetail as $pslipdetaillist) {
				if ($pslipdetaillist['PS_FIELDTYPE'] == 1) {
					$value = Application_Helper_General::displayMoney($pslipdetaillist['PS_FIELDVALUE']);
				} elseif ($pslipdetaillist['PS_FIELDTYPE'] == 2) {
					$value = Application_Helper_General::convertDate($pslipdetaillist['PS_FIELDVALUE'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				} elseif ($pslipdetaillist['PS_FIELDTYPE'] == 3) {
					$value = $pslipdetaillist['PS_FIELDVALUE'];
				} else {
					$value = '';
				}

				$htmldataDetailDetail .= '
				<tr>
					<td class="">' . $this->language->_($pslipdetaillist['PS_FIELDNAME']) . '</td>
					<td class="">' . $this->language->_($value) . '</td>
				</tr>
				';
			}
			// print_r($htmldataDetailDetail);die;
			if (isset($htmldataDetailDetail)) $this->view->templateDetail = $htmldataDetailDetail;
			//ambil dari slip_detail - end

			$this->_tableMstleft[0]["label"] = $this->language->_('Payment Type');

			if ($pslip['PS_PERIODIC'] == NULL) {
				$this->_tableMstleft[1]["label"] = $this->language->_('PayRef') . "#";
			}else{
				$this->_tableMstleft[1]["label"] = $this->language->_('FreqRef') . "#";
			}

			$this->_tableMstleft[2]["label"] = $this->language->_('Payment Subject');
			$this->_tableMstleft[3]["label"] = $this->language->_('Trans#');
			$this->_tableMstleft[4]["label"] = $this->language->_('Message');
			// $this->_tableMstleft[5]["label"] = $this->language->_('Additional Message');

			$dt1 = $trfType[$pslip["TRANSFER_TYPE"]];

            $tra_type	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");

            $tra_type1 = $tra_type[$pslip["TRANSFER_TYPE"]];
            
            if ($pslip["PS_TYPE"] == 19) {
                 $payType = 'CP Same Bank Remains';
            }else if ($pslip["PS_TYPE"] == 20) {
                 $payType = 'CP Same Bank Maintains';
            }else if ($pslip["PS_TYPE"] == 23) {
                 $payType = 'CP Others Remains - '.$tra_type1;
            }else if ($pslip["PS_TYPE"] == 21) {
                 $payType = 'MM - '.$tra_type1;
            }else if($pslip['PS_TYPE'] == '30' || $pslip['PS_TYPE'] == '25' || $pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '26' || $pslip['PS_TYPE'] == '28' || $pslip['PS_TYPE'] == '3' || $pslip['PS_TYPE'] == '27'){
				$payType = $pslip['payType'];
			}else {
                $payType = $payType1[$pslip["PS_TYPE"]].' - '.$dt1;
            }
			//echo '<pre>';
			//var_dump($pslip);die;

			$this->_tableMstleft[0]["value"] = $payType;

			if ($pslip['PS_PERIODIC'] == NULL) {
				$this->_tableMstleft[1]["value"] = $PS_NUMBER;
			}else{
				$this->_tableMstleft[1]["value"] = $PS_PERIODIC_NUMBER;
			}
			
			$this->_tableMstleft[2]["value"] = $pslip['paySubj'];
			$buttondownload = '';
			if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" || ($pslip["PS_CATEGORY"] == "SWEEP PAYMENT" ) && $pslip['numtrx'] > 1) {
				// download trx bulk file
				$downloadURL = $this->view->url(array('module' => 'paymentworkflow', 'controller' => 'ongoing', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $pslip['PS_NUMBER']), null, true);

				$buttondownload = $this->view->formButton('download', $this->language->_('download'), array('class' => 'btngrad hov', 'onclick' => "window.location = '" . $downloadURL . "';"));
			}
			// $this->_tableMstleft[2]["label"] = $this->language->_('Trans').'#';
			$this->_tableMstleft[3]["value"] = $pslip['numtrx'] . " " . $buttondownload;
			// $this->_tableMstleft[3]["value"] = $pslip[''];
			// $this->_tableMstleft[4]["value"] = $pslip['paySubj'];

			// if (($pslip['PS_CATEGORY'] == 'SINGLE PAYMENT' || $pslip['PS_CATEGORY'] == 'OPEN TRANSFER')) {
			// 	if ($pslip['TRANSFER_TYPE'] == 0) {
			// 		if ($pslip['SOURCE_ACCT_BANK_CODE'] == $pslip['BENEF_ACCT_BANK_CODE']) {
			// 			$payType = $pslip['payType'] . ' - ' . 'PB';
			// 		} else {
			// 			$payType = $pslip['payType'] . ' - ' . 'ONLINE';
			// 		}
			// 	} else if ($pslip['TRANSFER_TYPE'] == 1) {
			// 		$payType = $pslip['payType'];
			// 	} else if ($pslip['TRANSFER_TYPE'] == 2) {
			// 		$payType = $pslip['payType'];
			// 	} else {
			// 		$payType = $pslip['payType'];
			// 	}
			// } else if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

			// 	if ($pslip['PS_TYPE'] == '19') {
			// 		$poolingType = 'Remains';
			// 	} else {
			// 		$poolingType = 'Maintains';
			// 	}

			// 	$payType = $pslip['payType'] . ' - ' . $poolingType;
			// } else {
			// 	$payType = $pslip['payType'];
			// }
		
			$this->_tableMstright[0]["label"] = $this->language->_('Frequently');
			//Frequently
			$value = '';
			$recurring = '';
			if ($PS_EVERY_PERIODIC_UOM === null) {
				$value = '1x';
				$recurring = $value;

				if ($pslip["PS_STATUS"] == '14') {
					$status = 2; //canceled
				} elseif ($pslip["PS_STATUS"] == '7') {
					$status = 1; //ongoing
				} elseif ($pslip["PS_STATUS"] == '5') {
					$status = 3; //ended
				}

				if ($status == 2 || $status == 3) {
					$date = ' - ';
				}
				else{
					$date = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
				}

				// $this->_tableMstleft[5]["label"] = $this->language->_('Payment Date');
				// $this->_tableMstleft[5]["value"] = $date;
			} else {

				// ------------------------------------------------------------get next exute date------------------------------------------------------

				// get status ongoing
				$select	= $this->_db->select()
					->from(array('P'	 		=> 'T_PERIODIC'), array('*'))
					->where("P.PS_PERIODIC  = ?", $pslip["PS_PERIODIC"]);
					
				$periodicData = $this->_db->fetchRow($select);

				$status = '';
				if ($periodicData["PS_PERIODIC_STATUS"] == '0') {
					$status = 2; //canceled
				} elseif ($periodicData["PS_PERIODIC_STATUS"] == '2') {
					$status = 1; //ongoing
				} elseif ($periodicData["PS_PERIODIC_STATUS"] == '1') {
					$status = 3; //ended
				}
				

				

				if ($status != 2 && $status != 3) {
					if ($PS_EVERY_PERIODIC_UOM == 1) {
						$select = $this->_db->select()
							->from(array('C' => 'T_PERIODIC_DAY', array('C.DAY_ID')))
							->where("C.PERIODIC_ID  = ?", $pslip["PS_PERIODIC"]);

						$PERIODIC_DAY = $this->_db->fetchAll($select);
						$days = array();
						foreach ($PERIODIC_DAY as $key) {
							$days[] = (int) $key['DAY_ID'];
						}
						$date = $pslip["PS_EFDATE"] . ' ' . $pslip["PS_EFTIME"];
						$dateNow = date("Y-m-d H:i");
						$result = $date;
						foreach ($days as $day) {
							if ($dateNow > $result) {
								$date = $dateNow;
								$dayofweek = date('N', strtotime($date));
								$result = date('Y-m-d', strtotime(($day - $dayofweek) . ' day', strtotime($date)));
								$result = $result . ' ' . $pslip["PS_EFTIME"];
							}
						}

						if ($result < $dateNow) {
							$date = date("Y-m-d", strtotime("+1 week", strtotime($date)));
							$dayofweek = date('N', strtotime($date));
							$result = date('Y-m-d H:i', strtotime(($days[0] - $dayofweek) . ' day', strtotime($date)));
						}
						$date = $result;
						$date = strtotime($date);
						if (date('Y-m-d', $date) == date('Y-m-d')) {
							$nextExecute = $this->language->_('Today') . ' @' . date('H:i', $date);
						} else {
							$nextExecute = date('d M Y @H:i', $date);
						}
					} elseif ($PS_EVERY_PERIODIC_UOM == 2 || $PS_EVERY_PERIODIC_UOM == 3) {
						$date = $periodicData["PS_PERIODIC_NEXTDATE"];
						$nextExecute = $date;
					} elseif ($PS_EVERY_PERIODIC_UOM == 5) {
						$date = $pslip["PS_EFDATE"] . ' ' . $pslip["PS_EFTIME"];
						$dateNow = date("Y-m-d H:i");

						if ($dateNow > $date) {
							$date = $dateNow;
						}
						$dayofweek = date('N', strtotime($date));
						$result = date('Y-m-d', strtotime(($periodicData['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
						$result = $result . ' ' . $pslip["PS_EFTIME"];
						if ($result < $dateNow) {
							$date = date("Y-m-d", strtotime("+1 week"));
							$result = date('Y-m-d', strtotime(($periodicData['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
						}

						$nextExecute = date("Y-m-d", strtotime($result));
					} elseif ($PS_EVERY_PERIODIC_UOM == 6) {
						$date = $pslip["PS_EFDATE"] . ' ' . $pslip["PS_EFTIME"];
						$dateNow = date("Y-m-d H:i");

						if ($date < $dateNow) {
							$nextExecute = date("Y-m-d", strtotime("+1 month", strtotime($date)));
						}
					}	
				//	var_dump($date);
					// $this->_tableMstleft[5]["value"] = substr(Application_Helper_General::convertDate($nextExecute, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
				//	echo 'here';
				}
				else{
				//	echo 'her';
					$this->_tableMstleft[3]["value"] = ' - ';
				}

				//var_dump($this->_tableMstleft);

				// ------------------------------------------------------------end get next exute date------------------------------------------------------

				if ($PS_EVERY_PERIODIC_UOM == '1') {
					$value = 'Daily';
				} elseif ($PS_EVERY_PERIODIC_UOM == '2') {
					$value = 'Weekly';
				} elseif ($PS_EVERY_PERIODIC_UOM == '3') {
					$value = 'Monthly';
				} elseif ($PS_EVERY_PERIODIC_UOM == '4') {
					$value = 'Yearly';
				} elseif ($PS_EVERY_PERIODIC_UOM == '5') {
					$periodicEveryArr = array(
						'1' => $this->language->_('Monday'),
						'2' => $this->language->_('Tuesday'),
						'3' => $this->language->_('Wednesday'),
						'4' => $this->language->_('Thursday'),
						'5' => $this->language->_('Friday'),
						'6' => $this->language->_('Saturday'),
						'7' => $this->language->_('Sunday'),
					);
					$value = $this->language->_('Every Day of ') . $periodicEveryArr[$PS_EVERY_PERIODIC];
				} elseif ($PS_EVERY_PERIODIC_UOM == '6') {
					$value = $this->language->_('Every Date of ') . $PS_EVERY_PERIODIC;
				}
				$recurring = $value;
			}
			$this->_tableMstright[0]["value"] = $recurring;

			$status = '';
			if ($PS_PERIODIC_NUMBER === null) {
				if ($PS_STATUS == '14') {
					$PERIOD_STATUS = '<span class="indicator warning"></span> ' . $this->language->_('Canceled');
					$status = '1';
				} elseif ($PS_STATUS == '5') {
					$PERIOD_STATUS = '<span class="indicator offline"></span> ' . $this->language->_('Ended');
					$status = '3';
				} elseif ($PS_STATUS == '7') {
					$PERIOD_STATUS = '<span class="indicator online"></span> ' . $this->language->_('Ongoing');
					$status = '2';
				}
			} else {
				if ($PS_PERIODIC_STATUS == '0') {
					$PERIOD_STATUS = '<span class="indicator warning"></span> ' . $this->language->_('Canceled');
					$status = '1';
				} elseif ($PS_PERIODIC_STATUS == '2') {
					$PERIOD_STATUS = '<span class="indicator online"></span> ' . $this->language->_('Ongoing');
					$status = '2';
				} elseif ($PS_PERIODIC_STATUS == '1') {
					$PERIOD_STATUS = '<span class="indicator offline"></span> ' . $this->language->_('Ended');
					$status = '3';
				}
			}

			$arrdayongoing = array(
					'0' => 'sun',
					'1' => 'mon',
					'2' => 'tue',
					'3' => 'wed',
					'4' => 'thu',
					'5' => 'fry',
					'6' => 'sat'

				);

			$PeriodIdSweepIn = $pslip['PS_PERIODIC'];
			if(!empty($PeriodIdSweepIn)){
			$selectday	= $this->_db->select()
				->from(array('TTS' => 'T_PERIODIC_DAY'))
				->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
			
			$report_day = $this->_db->fetchAll($selectday);
			
			//var_dump($report_day);die;
			if (!empty($report_day)) {
							foreach ($report_day as $key => $value) {
								${'pooling_' . $arrdayongoing[$value['DAY_ID']]} = $value['LIMIT_AMOUNT'];
							}
			}

			$this->view->pooling_sun = $pooling_sun;
			$this->view->pooling_mon = $pooling_mon;
			$this->view->pooling_tue = $pooling_tue;
			$this->view->pooling_wed = $pooling_wed;
			$this->view->pooling_thu = $pooling_thu;
			$this->view->pooling_fry = $pooling_fry;
			$this->view->pooling_sat = $pooling_sat;

			$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
			$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
			$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
			$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
			$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
			$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
			$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);
			}
			//if sweep 
			if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

				if  (empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[2]["label"] = $this->language->_('Payment Date');
					$this->_tableMstright[2]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					$repeat = '1x';
				}else{
					if ($PS_EVERY_PERIODIC_UOM == '3') {
						$this->_tableMstright[1]["label"] = $this->language->_('Repeat On Date');
					}else{
						$this->_tableMstright[1]["label"] = $this->language->_('Repeat On');	
					}
					
					$this->_tableMstright[2]["label"] = $this->language->_('Start From');
				}

				$this->_tableMstright[3]["label"] = $this->language->_('Time');

				if (!empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[4]["label"] = $this->language->_('Next Payment Date');
					$this->_tableMstright[4]["value"] = substr(Application_Helper_General::convertDate($nextExecute, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
				}


				$arrday2 = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);
				if  (empty($pslip['PS_PERIODIC'])) {
					$repeat = '1x';
					// $repeatOn = '-';

					// $efDate = date_create($pslip['PS_EFDATE']);
					// $newEfDate = date_format($efDate, "d M Y");

					// $startFrom = $newEfDate;
				}
				else{
					$select	= $this->_db->select()
							->from(array('P'	 		=> 'T_PSLIP'))
							->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
							->joinLeft(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
							->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
							->GROUP('D.DAY_ID');
					//echo $select;

					$periodicData = $this->_db->fetchAll($select);
					//echo '<pre>';
					//var_dump($periodicData);die;
					
					$arrday = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);
					if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 1) {
						
						$repeat = 'Daily';
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						foreach ($periodicData as $key => $value) {
						$labelcheck = 'check'.$value['DAY_ID'];
						${$labelcheck} = 'checked';
						
						// edit day T to Thue
						//$repeatOn .= substr($arrday[$value['DAY_ID']], 0, 3).' ';
						//$repeatOn .= '<div class="roundChk" style="margin-left: -4%;">
						//				<input type="checkbox" disabled class="checkboxday" id="checkbox' . $value['DAY_ID'] . '" value="' . $value['DAY_ID'] . '" checked />
						//				<label for="checkbox' . $value['DAY_ID'] . '">' . substr($arrday[$value['DAY_ID']], 0, 1) . '</label>
						//			</div>';
						
						}
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 2) {
						$repeat = 'Weekly';
						// $repeatOn = $arrday2[$periodicData[0]['PS_EVERY_PERIODIC']];
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						
						$labelcheck = 'check'.$periodicData[0]['PS_EVERY_PERIODIC'];
						${$labelcheck} = 'checked';

						// foreach ($periodicData as $key => $value) {
						// $labelcheck = 'check'.$value['DAY_ID'];
						// ${$labelcheck} = 'checked';
						
						// }
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else {
						$repeat = 'Monthly';
						$repeatOn = $periodicData[0]['PS_EVERY_PERIODIC'];
					}

					$startDate = date_create($periodicData[0]['PS_PERIODIC_STARTDATE']);
					$newStartDate = date_format($startDate, "d M Y");
				
					
					$endDate = date_create($periodicData[0]['PS_PERIODIC_ENDDATE']);
					$newEndDate = date_format($endDate, "d M Y");
					
					$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;
				
					$arrday = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);

					if ($pslip['PS_TYPE'] == '19') {
						//remain
						// $amountlabel = $this->language->_('Remain on source');
						// $amount 	 = '<span class="text-danger"><b>' . $pslip['ccy'] . ' n/a </b></span>';
					} else if ($pslip['PS_TYPE'] == '20') {
						// $amountlabel = $this->language->_('Maintain Beneficiary');

						$maintainsData = '';
						foreach ($periodicData as $key => $value) {
							$maintainsData .= substr($arrday[$value['DAY_ID']], 0, 3) . ' IDR ' . Application_Helper_General::displayMoney($value['LIMIT_AMOUNT']) . '<br>';
						}

						$content = "<div class='text-center'>".$maintainsData."</div>";

						// $amount = '<button type="button" class="btngrad hov" tabindex="0" data-toggle="popover" data-html="true" data-trigger="focus" title="Maintains Config" data-content="'.$content.'">View</button>';

						$this->view->maintainsData = $maintainsData; //for pdf
					}

					
					$this->_tableMstright[1]["value"] = $repeatOn;
					$this->_tableMstright[2]["value"] = $startFrom;
				}

				$AESMYSQL = new Crypt_AESMYSQL();
				$encrypted_pwd = $AESMYSQL->encrypt($PS_NUMBER, $rand);
				$encreff = urlencode($encrypted_pwd);

				$eftime = date('h:i A', strtotime($pslip['PS_EFTIME']));
				$this->_tableMstright[3]["value"] = $eftime;

				if ($pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '23' || $pslip['PS_TYPE'] == '30') {
					$amountlabel = $this->language->_('Remain on source');
				} else if ($pslip['PS_TYPE'] == '20') {
					$amountlabel = $this->language->_('Maintain Beneficiary');					
				}else if($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15'){
					//var_dump($pslip);
					if($pslip['PS_SWEEP_TYPE'] == '1'){
							$amountlabel = $this->language->_('Remain on source');
					}else{
							$amountlabel = $this->language->_('Maintain Beneficiary');					
					}
				}
				$this->_tableMstright[5]["label"] = $amountlabel;
				//var_dump($this->_tableMstright);die;
				if ($pslip['PS_TXCOUNT'] > 1) {
					$amount = '-';
				}else{
					
					if ($pslip["PS_TYPE"] == '20' && $PS_EVERY_PERIODIC_UOM == '1') {
						$amount = ' - <button type="button" class="btngrad hov" class="toogleModal text-primary" data-toggle="modal" data-target="#poolingConfirmModal">View Config</button>';
					}else{
						$amount = '<span>' . $pslip['ccy'] . ' '.Application_Helper_General::displayMoney($pslip['TRA_REMAIN']).'</span>';
					}
				}

				$this->_tableMstright[5]["value"] = $amount;

				$this->_tableMstright[6]["label"] = 'Minimum Transfer';
				$amount1 	 = '<span>' . $pslip['ccy'] .' '.Application_Helper_General::displayMoney($pslip['PS_MIN_AMOUNT']).'</span>';
				$this->_tableMstright[6]["value"] = $amount1;
			
				if ($pslip['TRANSFER_TYPE'] == '1' && $pslip['PS_TYPE'] == '23') {
					$this->_tableMstright[7]["label"] = $this->language->_('If sweep exceed RTGS');
					$this->_tableMstright[7]["value"] = 'Split if amount exceed RTGS limit';
				}else if($pslip['TRANSFER_TYPE'] == '2' && $pslip['PS_TYPE'] == '23'){
					$this->_tableMstright[7]["label"] = $this->language->_('If sweep exceeds SKN');
					$this->_tableMstright[7]["value"] = 'Split if amount exceed SKN limit';
				}

				$this->_tableMstright[8]["label"] = $this->language->_('Status');
				$this->_tableMstright[8]["value"] = $PERIOD_STATUS;
			}
			else{
				
				
				if  (empty($pslip['PS_PERIODIC'])) {
					$repeat = '1x';
					$this->_tableMstright[1]["label"] = $this->language->_('Payment Date');
					$this->_tableMstright[1]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					$this->_tableMstright[2]["label"] = $this->language->_(' ');
					$this->_tableMstright[2]["value"] = '';
					// $repeatOn = '-';

					// $efDate = date_create($pslip['PS_EFDATE']);
					// $newEfDate = date_format($efDate, "d M Y");

					// $startFrom = $newEfDate;
				}
				else{
					$select	= $this->_db->select()
							->from(array('P'	 		=> 'T_PSLIP'))
							->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
							->joinLeft(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
							->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
							->GROUP('D.DAY_ID');
					//echo $select;

					$periodicData = $this->_db->fetchAll($select);
					//echo '<pre>';
					//var_dump($periodicData);die;
					
					$arrday2 = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);
					if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 1) {
						
						$repeat = 'Daily';
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						foreach ($periodicData as $key => $value) {
						$labelcheck = 'check'.$value['DAY_ID'];
						${$labelcheck} = 'checked';
						
						// edit day T to Thue
						//$repeatOn .= substr($arrday[$value['DAY_ID']], 0, 3).' ';
						//$repeatOn .= '<div class="roundChk" style="margin-left: -4%;">
						//				<input type="checkbox" disabled class="checkboxday" id="checkbox' . $value['DAY_ID'] . '" value="' . $value['DAY_ID'] . '" checked />
						//				<label for="checkbox' . $value['DAY_ID'] . '">' . substr($arrday[$value['DAY_ID']], 0, 1) . '</label>
						//			</div>';
						
						}
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 2) {
						$repeat = 'Weekly';
						// $repeatOn = $arrday2[$periodicData[0]['PS_EVERY_PERIODIC']];
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						
						$labelcheck = 'check'.$periodicData[0]['PS_EVERY_PERIODIC'];
						${$labelcheck} = 'checked';

						// foreach ($periodicData as $key => $value) {
						// $labelcheck = 'check'.$value['DAY_ID'];
						// ${$labelcheck} = 'checked';
						
						// }
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else {
						$repeat = 'Monthly';
						$repeatOn = $periodicData[0]['PS_EVERY_PERIODIC'];
					}

					$startDate = date_create($periodicData[0]['PS_PERIODIC_STARTDATE']);
					$newStartDate = date_format($startDate, "d M Y");
				
					
					$endDate = date_create($periodicData[0]['PS_PERIODIC_ENDDATE']);
					$newEndDate = date_format($endDate, "d M Y");
					
					$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;
				
					$arrday = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);

					if ($pslip['PS_TYPE'] == '19') {
						//remain
						$amountlabel = $this->language->_('Remain on source');
						$amount 	 = '<span class="text-danger"><b>' . $pslip['ccy'] . ' n/a </b></span>';
					} else if ($pslip['PS_TYPE'] == '20') {
						$amountlabel = $this->language->_('Maintain Beneficiary');

						$maintainsData = '';
						foreach ($periodicData as $key => $value) {
							$maintainsData .= substr($arrday[$value['DAY_ID']], 0, 3) . ' IDR ' . Application_Helper_General::displayMoney($value['LIMIT_AMOUNT']) . '<br>';
						}

						$content = "<div class='text-center'>".$maintainsData."</div>";

						$amount = '<button type="button" class="btngrad hov" tabindex="0" data-toggle="popover" data-html="true" data-trigger="focus" title="Maintains Config" data-content="'.$content.'">View</button>';

						$this->view->maintainsData = $maintainsData; //for pdf
					}

					if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '3') {
						$this->_tableMstright[3]["label"] = $this->language->_('Repeat on date');
					}else{
						$this->_tableMstright[3]["label"] = $this->language->_('Repeat on');
					}
					$this->_tableMstright[3]["value"] = $repeatOn;
					$this->_tableMstright[4]["label"] = $this->language->_('Start From');
					$this->_tableMstright[4]["value"] = $startFrom;

				}
				
				$amountlabel1 = $this->language->_('Amount');
				// $amount    = $pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['amount']);

				if (!empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[5]["label"] = $this->language->_('Next Payment Date');
					$this->_tableMstright[5]["value"] = substr(Application_Helper_General::convertDate($nextExecute, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
				}
				$this->_tableMstright[6]["label"] = $amountlabel1;
				$this->_tableMstright[6]["value"] = '<span style="color: red;"><b>' .$pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']).'</b></span>';
				$this->_tableMstright[7]["label"] = $this->language->_('Status');
				$this->_tableMstright[7]["value"] = $PERIOD_STATUS;
			}


			$this->view->status = $status;
			
			// $this->_tableMstleft[1]["label"] = $this->language->_('Payment Subject');
			// $this->_tableMstleft[2]["label"] = $this->language->_('Payment Date');
			// $this->_tableMstleft[3]["label"] = $this->language->_('Message');
			// $this->_tableMstleft[4]["label"] = $this->language->_('Additional Message');


			// View Data
			$this->_tableMst[0]["label"] = $this->language->_('FreqRef') . "#";
			$this->_tableMst[1]["label"] = $this->language->_('Created Date');
			$this->_tableMst[2]["label"] = $this->language->_('Updated Date');
			$this->_tableMst[3]["label"] = $this->language->_('Payment Date');
			$this->_tableMst[4]["label"] = $this->language->_('Payment Subject');
			$this->_tableMst[5]["label"] = $this->language->_('Master Account');
			$this->_tableMst[6]["label"] = $this->language->_('Payment Type');
			$this->_tableMst[7]["label"] = $this->language->_('Payment Status');
			$this->_tableMst[8]["label"] = $this->language->_('Source Bank Name');

			$this->_tableMst[0]["value"] = $PS_NUMBER;
			$this->_tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			$this->_tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			$this->_tableMst[3]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
			$this->_tableMst[4]["value"] = $pslip['paySubj'];
			$this->_tableMst[5]["value"] = "";
			$this->_tableMst[6]["value"] = $pslip['payType'];
			$this->_tableMst[7]["value"] = $payStatus;

			/*	pending future date = 7 ;
				display button Cancel Future Date, if payment status 'Pending Future Date'
			*/
			//echo "pstype: ";
			//echo $pslip["PS_STATUS"];
			$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
		        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
		        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
		        $cacheID = 'BANKTABLE';
		        
		        $bankNameArr = $cache->load($cacheID);
				//var_dump($select_int);
		        if(empty($bankNameArr)){
			        	$selectbank = $this->_db->select()
						->from(array('C' => 'M_BANK_TABLE'), array('*'));

					$databank = $this->_db->fetchAll($selectbank);

					foreach ($databank as $key => $value) {
						$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
					}
					
					$cache->save($bankNameArr,$cacheID);
		        }
			if (empty($pslip['SOURCE_ACCT_BANK_CODE'])) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$pslip['SOURCE_ACCT_BANK_CODE']];
			}
			$this->_tableMst[8]["value"] = $bankname;

			if ($pslip['PS_STATUS'] == 7 && $this->view->hasPrivilege('CFDT')) {
				$this->view->paypendingfuture = true;
			} // privillege cancel future date.

			//View File dihidden untuk PAYROLL
			if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && !$pdf && $pslip["PS_TYPE"] != "11") {
				// download trx bulk file
				$downloadURL = $this->view->url(array('module' => 'paymentworkflow', 'controller' => 'ongoing', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $pslip['PS_NUMBER']), null, true);
				$this->_tableMst[9]["label"] = $this->language->_('View File');
				$this->_tableMst[9]["value"] = $this->view->formButton('download', $this->language->_('download'), array('class' => 'btngreen hov', 'onclick' => "window.location = '" . $downloadURL . "';"));
			}

			// separate credit and debet view
			if (
				// $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] ||
				$pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]
			) {
				// die('hee');
				$this->debet($pslip);
			} elseif ($pslip["PS_BILLER_ID"] == '1158') {
				// echo 'ss';die;
				$this->etax($pslip);
			} else {
				// die('sene');

				$this->credit($pslip);
			}

			//Zend_Debug::dump($pslip["PS_TYPE"]); die;
			// && $pslip['PS_STATUS'] != 5  && != 6 Hannichr2010
			// View Transfer to di hidden untuk PAYROLL
			if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && $pslip['PS_STATUS'] != 5 && $pslip['PS_STATUS'] != 6 || $pslip['PS_TYPE'] == 11) {
				// $this->view->fields			 = array();
				// $this->view->tableDtl 		 = array();
				// $this->view->TITLE_DTL		 = "";
			}

			/*
				Status payment :
					1. PayStatus (5) completed + TransStatus (3) success
					2. PayStatus (4) rejected
					2. PayStatus (6) transferfailed + TransStatus (4) Failed


			*/
			$txtMessage = '';
			if ($pslip['PS_STATUS'] == 4) {
				$txtMessage         = $this->language->_('Payment Rejected');
				$this->view->flagstatus   = true;
			}
			if (($pslip['PS_STATUS'] == 5) || ($pslip['PS_STATUS'] == 7)) {
				$custId   = $this->_custIdLogin;

				$UUID = $this->_db->select()
					->from(
						array('P'    => 'T_PSLIP'),
						array('uuid'  => 'UUID')
					)
					->where('PS_NUMBER = ' . $this->_db->quote($PS_NUMBER) . ' AND CUST_ID = ?', $custId);

				$UUID = $this->_db->fetchOne($UUID);
				//->query()
				//->fetchOne();

				$daterelease = $this->_db->select()
					->from(
						array('P'    => 'T_PSLIP_HISTORY'),
						array('date'  => 'DATE_TIME')
					)
					->where('PS_NUMBER = ' . $this->_db->quote($PS_NUMBER) . ' AND CUST_ID = ?', $custId);

				$daterelease = $this->_db->fetchOne($daterelease);
				//date("d/m/Y H:i:s")
				if ($daterelease) {
					$daterelease = Application_Helper_General::convertDate($daterelease, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					//$daterelease = date("d/m/Y H:i:s",$daterelease);
				} else $daterelease = '';

				//->query()
				//->fetchOne();
				$txtMessage = "
					<p><h4><u>" . $this->language->_('Release ' . $pslip["payStatus"]) . "</u></h4></p>
					<p><h5>" . $this->language->_('Release Date') . ": <b>" . $daterelease . " </b></h5></p>";

				$this->view->flagstatus   = true;

				//<p><h5>Trace ID: <b>".$UUID."</b></h5></p>
			}
			if ($pslip['PS_STATUS'] == 6) {
				$txtMessage         = $this->language->_('Transfer Failed');
				$this->view->flagstatus   = true;
			}
			if ($pslip['PS_STATUS'] == 9) {
				$txtMessage         = $this->language->_('Payment Exception');
				$this->view->flagstatus   = true;
			}

			$this->view->txtMessage = $txtMessage;

			$this->view->PS_NUMBER       = $PS_NUMBER;
			$this->view->pslip 				= $pslip;
			$this->view->tableMst       = $this->_tableMst;

			$this->view->tableMstleft       = $this->_tableMstleft;
			$this->view->tableMstright       = $this->_tableMstright;



			$this->view->totalTrx       = $pslip["numtrx"];
			$persenLabel = $pslip["BALANCE_TYPE"] == '2' ? ' %' : '';
			// 			print_r($pslip);die;
			//	if(!empty($persenLabel)){
			//		$this->view->totalAmt 			= $pslip["TRA_AMOUNT"];
			//}else
			if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
				//die;
				$this->view->totalAmt      = $pslip['PS_REMAIN'];
			} else {
				//print_r($pslip);die;
				if ($pslip['PS_TYPE'] == '3' && $pslip['acbenef_ccy'] == 'USD' && $pslip['accsrc_ccy'] == 'USD') {
					$this->view->totalAmt       = $pslip["ccy"] . ' ' . $pslip["EQUIVALEN_IDR"];
					$this->view->ps_ccy       = $pslip["ccy"];
				} else if ($pslip['PS_TYPE'] == '3') {
					//$this->view->totalAmt 			= 'IDR '.$pslip["amount"].' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
					$this->view->totalAmt       = 'IDR ' . Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']);
					$this->view->ps_ccy       = 'IDR';
				} else {
					$this->view->totalAmt       = $pslip["ccy"] . ' ' . $pslip["amount"];
					$this->view->ps_ccy       = $pslip["ccy"];
				}
				/*	if($pslip['ccy']!='IDR'){
					$this->view->totalAmt 			= $pslip['ccy'].' '.$pslip["amount"].' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
				}else{
					$this->view->totalAmt 			= $pslip['ccy'].' '.$pslip["amount"];
				}
			*/
			}
			//print_r($pslip);die;
			if ($pslip['PS_TYPE'] == '3' || ($pslip['PS_TYPE'] == '1')) {
				$this->view->totaltext       = true;
			} else {
				$this->view->totaltext       = false;
			}
			$this->view->pdf         = ($pdf) ? true : false;

			$this->view->compName = $this->_custNameLogin;
			if ($pdf) {

				$Payment = new 	Payment($pslip['PS_NUMBER']);
				$listHistory =  $Payment->getHistory();
				$this->view->paymentHistory =  $listHistory;

				$selectUser = $this->_db->select()
				->from(array('A' => 'M_USER'), array('A.USER_ID','A.USER_FULLNAME'))
				->where('A.USER_STATUS != 3')
				->where('A.CUST_ID = ?', $this->_custIdLogin);

				$selectUserList = $selectUser->query()->fetchAll();

				foreach ($selectUserList as $key => $value) {
					$newUserList[$value['USER_ID']] = $value['USER_FULLNAME'];
				}

				$this->view->dataUser = $newUserList;

				$outputHTML = "<tr><td>" . $this->view->render('/ongoing/indexpdf.phtml') . "</td></tr>";

				$this->_helper->download->newPdf(null, null, null, $this->language->_('Ongoing Transaction Detail'), $outputHTML);
			}

			Application_Helper_General::writeLog('DARC', 'Detail View Payment');
		}
	}

	function moneyAliasFormatter($n)
	{
		// first strip any formatting;
		return str_replace('.00', '', Application_Helper_General::displayMoney($n));
		// $n = (0+str_replace(",", "", $n));

		// // is this a number?
		// if (!is_numeric($n)) return false;

		// // now filter it;
		// if ($n > 1000000000000) return round(($n/1000000000000), 2).' T';
		// elseif ($n > 1000000000) return round(($n/1000000000), 2).' B';
		// elseif ($n > 1000000) return round(($n/1000000), 2).' M';
		// elseif ($n > 1000) return $n;

		// return number_format($n);
	}

	public function historyAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		$filter 			= new Application_Filtering();

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER"));
		$payreff = $AESMYSQL->decrypt($PS_NUMBER, $password);

		$PS_PERIODIC 			= $this->_getParam('PS_PERIODIC');

		$PS_NUMBER = $payreff;
		
		$this->view->paymentRef = $paymentRef;
		
		$sqlTransaction  	= "SELECT * FROM T_PSLIP WHERE PS_NUMBER = ? ";
		$pslipdata 				= $this->_db->fetchRow($sqlTransaction, (string) $PS_NUMBER);
		
		$arrPayStatus	= array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
		
		$this->view->pslip = $pslipdata;
		$this->view->arrPayStatus = $arrPayStatus;
		
		if($this->_custSameUser){
			$this->view->custSameUser = true;
		}

		$this->view->PS_NUMBER = $PS_NUMBER;
		$this->view->PS_PERIODIC = $PS_PERIODIC;
	}

	public function findPolicyBoundary($transfertype, $amount)
	{

		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);
		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		// echo $selectuser;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{


		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);	
		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		 //echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}
		
		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}
	
		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
	//	echo '<pre>';
//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	private function debet($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"], $this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
		foreach ($arrTransferStatus as $key => $val) {
			$caseTraStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseTraStatus .= " END)";

		$this->_tableMst[5]["label"] = $this->language->_('Beneficiary Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_bankname"], $pslip["acbenef_alias"]);

		// Table Detail Header
		$fields = array(
			"ACCTSRC_NAME"		=> $this->language->_('Source Account Name'),
			"ACCTSRC"			=> $this->language->_('Source Account'),
			"ACCSRC_ALIAS" => $this->language->_('Source Account Alias'),
			"ACBENEF_ALIAS"			=> $this->language->_('Beneficiary Alias'),
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			"ACCTSRC_CCY"  	   	=> $this->language->_('CCY'),
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			"TRA_STATUS"  	   	=> $this->language->_('Transaction Status'),
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);

		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
					'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
					'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),
					'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
					'ACCTSRC_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																				 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME)
																			END"),
					//'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
					'TRA_AMOUNT'				=> 'TT.TRA_AMOUNT',
					'TRA_ADDMESSAGE'				=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
					'TRA_REFNO'					=> 'TT.TRA_REFNO',
					'TRA_STATUS'				=> $caseTraStatus
				)
			)
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }

		$tableDtl = array();
		if (count($pslipTrx) <= 50) {
			foreach ($pslipTrx as $p => $pTrx) {

				// create table detail data
				$psCategory = $pslip['PS_CATEGORY'];
				foreach ($fields as $key => $field) {
					$value = $pTrx[$key];

					if ($key == "TRA_AMOUNT") {
						$value = Application_Helper_General::displayMoney($value);
					}

					$value = ($value == "") ? "&nbsp;" : $value;

					$tableDtl[$p][$key] = $value;
				}
				if (!empty($pslip['BANK_CODE'])) {
					$bankcode = $pslip['BANK_CODE'];
				} else {
					$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
				}

				if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
					$bankname = '-';
				} else if (empty($bankcode)) {
					$bankname = $this->_bankName;
				} else {
					$bankname = $bankNameArr[$bankcode];
				}

				$tableDtl[$p]['BENEFICIARY_ID'] = $pTrx['BENEFICIARY_ID'];
				$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
			}

			if ($pslip['PS_CATEGORY'] == 'SINGLE PAYMENT' || $pslip['PS_TYPE'] == '23' || $pslip['PS_TYPE'] == '1') {
				 $this->getBenefData($tableDtl[0]['BENEFICIARY_ID']);
			}

			$this->view->fields			 = $fields;
			$this->view->tableDtl 		 = $tableDtl;
		}
		$this->view->TITLE_MST		 = $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 = $this->language->_('Transfer From');
	}

	private function credit($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"], $this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
		foreach ($arrTransferStatus as $key => $val) {
			$caseTraStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseTraStatus .= " END)";

		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], "");

		// Table Detail Header
		$fields = array(
			"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
			"ACBENEF"			=> $this->language->_('Beneficiary Account'),

			//		"BENEFICIARY_CATEGORY"			=> $this->language->_('Beneficiary Category'),
			//		"BENEFICIARY_ID_TYPE"			=> $this->language->_('Beneficiary Identification Type'),
			//		"BENEFICIARY_ID_NUMBER"			=> $this->language->_('Beneficiary Identification Number'),
			//		"BENEFICIARY_CITY_CODE"			=> $this->language->_('Beneficiary City'),
			"ACBENEF_ALIAS"			=> $this->language->_('Beneficiary Alias'),
			"ACCSRC_ALIAS" => $this->language->_('Source Account Alias'),
			"BENEFICIARY_ID_NUMBER"			=> $this->language->_('Beneficiary NRC'),
			"BENEFICIARY_MOBILE_PHONE_NUMBER"			=> $this->language->_('Beneficiary Phone'),

			/* "TRA_MESSAGE" 		=> $this->language->_('Message'),
						"TRA_REFNO"  	   	=> $this->language->_('Additional Message'),
						"TRANSFER_TYPE"	   	=> $this->language->_('Transfer Type'),
						"PS_CCY"  	   	=> $this->language->_('CCY'),
						"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
						//"TRANSFER_FEE"  	=> 'Transfer Charge',
						"BANK_NAME"  	   	=> $this->language->_('Bank'),
						"TRA_STATUS" 		=> $this->language->_('Transaction Status'),
						*/
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			"TRANSFER_TYPE"	   	=> $this->language->_('Transfer Type'),
			"PS_CCY"  	   	=> $this->language->_('CCY'),
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),

			"RATE"  	   	=> $this->language->_('Rate'),
			"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			"TOTAL"  	=> $this->language->_('Total'),
			"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'),
			"BENEFICIARY_BANK_ADDRESS1"  	   	=> $this->language->_('Beneficiary Bank Address'),
			"BENEFICIARY_BANK_CITY"  	   	=> $this->language->_('Beneficiary Bank City'),
			"BENEFICIARY_BANK_COUNTRY"  	   	=> $this->language->_('Country'),
			"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),
			"TRA_STATUS" 		=> $this->language->_('Transaction Status'),
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);
		//print_r($pslip);die;
		if ($pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME)"),
						'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
						'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'C.PS_CCY', 'C.EQUIVALENT_AMOUNT_IDR', 'C.PS_TYPE',
						'BENEFICIARY_CATEGORY'				=> 'TT.BENEFICIARY_CATEGORY',
						'BENEFICIARY_ID_TYPE'				=> 'TT.BENEFICIARY_ID_TYPE',
						'BENEFICIARY_ID_NUMBER'				=> 'TT.BENEFICIARY_ID_NUMBER',
						'BENEFICIARY_CITY_CODE'				=> 'TT.BENEFICIARY_CITY_CODE',
						'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',

						'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
						'TRA_REFNO'				=> 'TT.TRA_REFNO',

						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House(Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House(Buy)'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT', 'TT.SOURCE_ACCOUNT_CCY',
						'C.CUST_ID',
						#'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $this->_bankName . "'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'TRA_STATUS' 			=> $caseTraStatus
					)
				)
				->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
				->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
				//->joinLeft	(   array('D' => 'T_PERIODIC_DETAIL'), 'C.PS_PERIODIC = D.PS_PERIODIC', array() )
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		} else {

			$app = Zend_Registry::get('config');
			$appBankname = $app['app']['bankname'];

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME)"),
						'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
						'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),

						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'C.PS_CCY', 'C.PS_TYPE',
						'C.CUST_ID',
						'TT.EQUIVALENT_AMOUNT_IDR',
						'BENEFICIARY_CATEGORY'				=> 'TT.BENEFICIARY_CATEGORY',
						'BENEFICIARY_ID_TYPE'				=> 'TT.BENEFICIARY_ID_TYPE',
						'BENEFICIARY_ID_NUMBER'				=> 'TT.BENEFICIARY_ID_NUMBER',
						'BENEFICIARY_CITY_CODE'				=> 'TT.BENEFICIARY_CITY_CODE',
						'TT.BENEFICIARY_MOBILE_PHONE_NUMBER',
						'TT.RATE', 'TT.SOURCE_ACCOUNT_CCY',
						'TT.PROVISION_FEE',
						'TT.NOSTRO_NAME',
						'TT.BENEFICIARY_BANK_ADDRESS1',
						'TT.BENEFICIARY_BANK_CITY',
						'TT.FULL_AMOUNT_FEE',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

						'TRA_REFNO'				=> 'TT.TRA_REFNO',
						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House(Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House(Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
						'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
						#'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'TRA_STATUS' 			=> $caseTraStatus
					)
				)
				->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
				->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		}
		//echo $select;die;
		$pslipTrx = $this->_db->fetchAll($select);
		// print_r($pslipTrx);die;
		$tableDtl = array();
		$psCategory = $pslip['PS_CATEGORY'];
		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }
		// if(count($pslipTrx) <= 50){
		foreach ($pslipTrx as $p => $pTrx) {

			if ($pslip['PS_TYPE'] == "BULK PAYMENT") {
				$pTrx['TRA_MESSAGE'] = '-';
				$pTrx['TRA_ADDMESSAGE'] = '-';
			}
			$this->_tableMstleft[4]["value"] = $pTrx['TRA_MESSAGE'];
			// $this->_tableMstleft[5]["value"] = $pTrx['TRA_ADDMESSAGE'];

			// $this->_tableMstright[5]["value"] = $pTrx['TRA_STATUS'];
			// $this->_tableMstleft[4]["value"] = $pTrx['TRA_ADDMESSAGE'];

			//print_r($fields);die;
			// create table detail data
			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];
				// print_r($key);die;
				if ($key == 'ACBENEF' && (trim($pTrx['TRANSFER_TYPE']) == 'FA' || trim($pTrx['TRANSFER_TYPE']) == 'No FA')) {
					$value = '-';
					// die;
				}
				if ($key == "BENEFICIARY_CATEGORY") {
					$LLD_CATEGORY = $value;
					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}
					$value = $LLD_CATEGORY_POST;
				}
				if ($key == "BENEFICIARY_CITY_CODE") {
					$CITY_CODE = $value;
					$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
					$select = $this->_db->select()
						->from(array('A' => 'M_CITY'), array('*'));
					$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
					$arr = $this->_db->fetchall($select);
					$value = $arr[0]['CITY_NAME'];
				}

				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['PS_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						//print_r($pTrx);die;
						if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD' && $pslip['PS_TYPE'] == '1') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							//$value = $pTrx['PS_CCY'].' '.Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']).' (IDR '.Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']).')';
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']);
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}

				if ($key == 'RATE') {
					$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
				}
				//print_r($pTrx);die;
				if ($key == "FULL_AMOUNT_FEE") {
					if ($pTrx['TRANSFER_TYPE'] == 'FA') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
					} elseif ($pTrx['TRANSFER_TYPE'] == 'No FA') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '4')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
					}



					// echo $selecttrfFA;
					// print_r($pTrx);die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);

					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);

					//print_r($value);die;
				}


				if ($key == 'PROVISION_FEE') {
					//print_r($pTrx);die;
					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrfpro;die;
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != 'N/A') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}

				if ($key == "TRANSFER_FEE") {
					//print_r($value);die;
					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;

					$trffee = $this->_db->fetchRow($selecttrffee);
					if ($pTrx['TRANSFER_FEE'] != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRANSFER_FEE']);
					} else {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' 0.00';
					}
				}
				if ($key == "BENEF_ACCT_BANK_CODE") {
					if (!empty($pslip['BANK_CODE'])) {
						$bankcode = $pslip['BANK_CODE'];
					} else {
						$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
					}

					if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
						$bankname = '-';
					} else if (empty($bankcode)) {
						$bankname = $this->_bankName;
					} else {
						$bankname = $bankNameArr[$bankcode];
					}

					$value = $bankname;
				}

				//print_r($value);die;
				$value = ($value == "") ? "&nbsp;" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
				$tableDtl[$p]['BENEFICIARY_ID'] = $pTrx['ACBENEF_ID'];
			}
		}
		//echo '<pre>';
		//var_dump($tableDtl);die;
		
		if ($pslip['PS_CATEGORY'] != 'BULK PAYMENT' || $pslip['PS_TYPE'] == '23' || $pslip['PS_TYPE'] == '1') {
			 $this->getBenefData($tableDtl[0]['BENEFICIARY_ID'],$tableDtl[0]['ACBENEF']);
		}

		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
		if (count($tableDtl) >= 50) {
			$this->view->countTable = true;
		} else {
			$this->view->countTable = false;
		}
		$this->view->TITLE_MST		 	= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer To');
	}

	private function getBenefData($benefId,$acbenef = null){
	
	
		if($benefId != NULL){
		$select = $this->_db->select()
			->from('M_BENEFICIARY', array('*'))
			->where("BENEFICIARY_ID = ?", $benefId); 
		}else{
			$select = $this->_db->select()
			->from('M_BENEFICIARY', array('*'))
			->where("BENEFICIARY_ACCOUNT = ?", $acbenef)
			->where("CUST_ID = ?", $this->_custIdLogin); 
		}

		$benefData = $this->_db->fetchRow($select);

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'));

		$databank = $this->_db->fetchAll($selectbank);

		foreach ($databank as $key => $value) {
			$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		$conf = Zend_Registry::get('config');
		$paymentType = $conf['account']['beneftype'];
		$paymentTypeFlip = array_flip($paymentType['code']);

		$citizenshipArr	= array("W" => "WNI", "N" => "WNA");
		$residentArr = array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);

		$settings 			= new Application_Settings();
		$LLD_array 			= array();
		$LLD_DESC_arrayCat 	= array();
		$lldTypeArr  		= $settings->getLLDDOMType();

		$LLD_CATEGORY_POST = '';
		if (!empty($benefData['BENEFICIARY_CATEGORY'])) {
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$LLD_array["CT"] 	= $benefData['BENEFICIARY_CATEGORY'];
			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$benefData['BENEFICIARY_CATEGORY']];
		}

		$tableMst[0]["label"] = $this->language->_('Beneficiary Type');
		$tableMst[1]["label"] = $this->language->_('Bank Name');
		$tableMst[2]["label"] = $this->language->_('Beneficiary Account');
		$tableMst[3]["label"] = $this->language->_('Beneficiary Name');
		$tableMst[4]["label"] = $this->language->_('Beneficiary Email');
		$tableMst[5]["label"] = $this->language->_('Beneficiary Phone');
		$tableMst[6]["label"] = $this->language->_('Beneficiary Address 1');
		$tableMst[7]["label"] = $this->language->_('Beneficiary Address 2');
		$tableMst[8]["label"] = $this->language->_('City');
		$tableMst[9]["label"] = $this->language->_('Beneficiary Category');
		$tableMst[10]["label"] = $this->language->_('Citizenship');
		$tableMst[11]["label"] = $this->language->_('Nationality');
		$tableMst[12]["label"] = $this->language->_('Beneficiary ID Type');
		$tableMst[13]["label"] = $this->language->_('Beneficiary ID Number');

		$tableMst[0]["value"] = empty($benefData['BENEFICIARY_TYPE']) ? '-' : $paymentType['desc'][$paymentTypeFlip[$benefData['BENEFICIARY_TYPE']]];
		$tableMst[1]["value"] = empty($benefData['BANK_NAME']) ? '-' : $bankNameArr[$benefData['BANK_CODE']];
		$tableMst[2]["value"] = empty($benefData['BENEFICIARY_ACCOUNT']) ? '-' : $benefData['BENEFICIARY_ACCOUNT'];
		$tableMst[3]["value"] = empty($benefData['BENEFICIARY_NAME']) ? '-' : $benefData['BENEFICIARY_NAME'];
		$tableMst[4]["value"] = empty($benefData['BENEFICIARY_EMAIL']) ? '-' : $benefData['BENEFICIARY_EMAIL'];
		$tableMst[5]["value"] = empty($benefData['BENEFICIARY_PHONE']) ? '-' : $benefData['BENEFICIARY_PHONE'];
		$tableMst[6]["value"] = empty($benefData['BENEFICIARY_ADDRESS']) ? '-' : $benefData['BENEFICIARY_ADDRESS'];
		$tableMst[7]["value"] = empty($benefData['BENEFICIARY_ADDRESS2']) ? '-' : $benefData['BENEFICIARY_ADDRESS2'];
		$tableMst[8]["value"] = empty($benefData['BENEFICIARY_CITY']) ? '-' : $benefData['BENEFICIARY_CITY'];
		$tableMst[9]["value"] = empty($LLD_CATEGORY_POST) ? '-' : $LLD_CATEGORY_POST;
		$tableMst[10]["value"] = empty($benefData['BENEFICIARY_RESIDENT']) ? '-' : $residentArr[$benefData['BENEFICIARY_RESIDENT']];
		$tableMst[11]["value"] = empty($benefData['BENEFICIARY_CITIZENSHIP']) ? '-' : $citizenshipArr[$benefData['BENEFICIARY_CITIZENSHIP']];
		$tableMst[12]["value"] = empty($benefData['BENEFICIARY_ID_TYPE']) ? '-' : $benefData['BENEFICIARY_ID_TYPE'];
		$tableMst[13]["value"] = empty($benefData['BENEFICIARY_ID_NUMBER']) ? '-' : $benefData['BENEFICIARY_ID_NUMBER'];

		$this->view->tableBene 		= $tableMst;
	}

	private function etax($pslip)
	{
		$PS_NUMBER = $this->_paymentRef;
		$getPaymentDetail 	= new display_Model_Paymentreport();
		$detail = $getPaymentDetail->getPaymentDetail($pslip["PS_NUMBER"]);
		$dataEtax = json_decode($detail[0]['LOG']);
		// echo '<pre>';
		// var_dump($dataEtax);die;


		// $this->_tableMst[4]["value"] = $dataEtax->paymentSubject;
		// $this->_tableMst[5]["label"] = $this->language->_('Source Account');
		// $this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$ACCTSRC_arr = array();

		//table detail
		$compulsory = array('0' => 'No', '1' => 'Yes');
		$taxType = array('0' => 'NPWP', '1' => 'Non NPWP');
		$identity = array(
			'1' => 'KTP',
			'2' => 'NPWP',
			'3' => 'SIM',
			'4' => 'PASPOR',
			'5' => 'KITAS'
		);
		// roki
		$month = array(
			$this->language->_('January'),
			$this->language->_('February'),
			$this->language->_('March'),
			$this->language->_('April'),
			$this->language->_('May'),
			$this->language->_('June'),
			$this->language->_('July'),
			$this->language->_('August'),
			$this->language->_('September'),
			$this->language->_('October'),
			$this->language->_('November'),
			$this->language->_('December')
		);
		$tableMst[0]["label"] = $this->language->_('FreqRef#');
		$tableMst[0]["value"] = $pslip['PS_NUMBER'];

		$tableMst[1]["label"] = $this->language->_('Created Date');
		$tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

		$tableMst[2]["label"] = $this->language->_('Updated Date');
		$tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

		$tableMst[3]["label"] = $this->language->_('Payment Subject');
		$tableMst[3]["value"] = $dataEtax->paymentSubject;

		$tableMst[4]["label"] = $this->language->_('Source Account');
		$tableMst[4]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], "");

		$tableMst[5]["label"] = $this->language->_('Payment Status');
		$tableMst[5]["value"] = $pslip["payStatus"];

		$tableMst[6]["label"] = $this->language->_('Billing Code');
		$tableMst[6]["value"] = '2020032300014203';

		$tableMst[7]["label"] = $this->language->_('Amount');
		$tableMst[7]["value"] = 'IDR ' . $dataEtax->amount;

		$tableMst[8]["label"] = $this->language->_('Payer NPWP');
		$tableMst[8]["value"] = $dataEtax->payerNpwp;

		$tableMst[9]["label"] = $this->language->_('Payer Name');
		$tableMst[9]["value"] = $dataEtax->payername;

		$tableMst[10]["label"] = $this->language->_('Compulsory');
		$tableMst[10]["value"] = $compulsory[$dataEtax->chargetype] . ' ' . ($dataEtax->chargetype == '1' ? ' - (' . $dataEtax->chargeid . ')' : '');

		$tableMst[11]["label"] = $this->language->_('Asessable NPWP');
		$tableMst[11]["value"] = $dataEtax->asessableNpwp;

		$tableMst[12]["label"] = $this->language->_('Asessable Name');
		$tableMst[12]["value"] = $dataEtax->asessablename;

		$tableMst[13]["label"] = $this->language->_('Asessable Address');
		$tableMst[13]["value"] = $dataEtax->asessableaddress;

		$tableMst[14]["label"] = $this->language->_('Asessable City');
		$tableMst[14]["value"] = $dataEtax->asessablecity;

		$tableMst[15]["label"] = $this->language->_('Asessable Identity');
		$tableMst[15]["value"] = $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;

		$tableMst[16]["label"] = $this->language->_('Map / Akun Code');
		$tableMst[16]["value"] = $dataEtax->akuncode;

		$tableMst[17]["label"] = $this->language->_('Deposit Type');
		$tableMst[17]["value"] = $dataEtax->deposittype;

		$tableMst[18]["label"] = $this->language->_('Tax Object Number (NOP)');
		$tableMst[18]["value"] = $dataEtax->taxobjectnumber;

		$tableMst[19]["label"] = $this->language->_('SK Number');
		$tableMst[19]["value"] = $dataEtax->skNumber;

		$tableMst[20]["label"] = $this->language->_('Remark');
		$tableMst[20]["value"] = $dataEtax->remark;

		$tableMst[21]["label"] = $this->language->_('Tax Period  Payment');
		$tableMst[21]["value"] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2] . ' ' . $dataEtax->periodic;

		$tableMst[22]["label"] = $this->language->_('Transaction Status');
		$tableMst[22]["value"] = $detail[0]["TRANSFER_STATUS"];
		return $tableMst;
		/////
	}

	public function downloadtrxAction()
	{
		$PS_NUMBER 			= trim(strip_tags($this->_getParam('payReff')));

		$paramPayment = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);
		// print_r($pslip);die;
		if (!empty($pslip)) {
			// separate credit and debet view
			if (
				// $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || removed globar var ?
				$pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]
			)
				$this->downloadTrxDebet($PS_NUMBER);
			else
				$this->downloadTrxCredit($PS_NUMBER, $pslip['payType'], $pslip['PS_TYPE']);
		} else {
			$data = array();
			$data[0][0] = "Invalid Payment Number";
			$this->_helper->download->csv(array(), $data, null, $PS_NUMBER);
		}
	}

	protected function downloadTrxCredit($PS_NUMBER, $PS_TYPE, $paymenttype)
	{
		//		"BENEFICIARY ACC","BENEFICIARY NAME","CCY","AMOUNT","MESSAGE","ADDITIONAL MESSAGE","EMAIL ADDRESS","TRANSFER TYPE","BANK CODE"," BANK NAME ","BANK CITY","CITIZENSHIP"
		//		"2003019114","Account Name","USD","3,000.50","MSG USD WITHIN","ADDITIONAL MSG","admin@yahoo.com","PB","","",""
		//		"2003019126","Baharudin","IDR","3,500.50","MSG IDR SKN","PAYMENT DESC 2004","admin2@yahoo.com","SKN","00023234","BNI, Ambon","Thamrin","R"
		//		"2003019120","Antony","IDR","1000","MSG IDR RTGS","PAYMENT DESC 2005","admin2@yahoo.com","RTGS","00023235","BCA, SBY","Address 12","NR"

		$caseTransferType = Application_Helper_General::caseArray($this->_transfertype);

		// print_r($PS_TYPE);die;

		if ($PS_TYPE == 'Payroll') {
			$header = array($this->language->_('BENEFICIARY ACC'), $this->language->_('CCY'), $this->language->_('AMOUNT'), $this->language->_('MESSAGE'));

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE'
					)
				)
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);
			$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
		} else if ($PS_TYPE == 'Multi Credit - Import') {
			$header = array($this->language->_('DESTINATION ACC'), $this->language->_('CCY'), $this->language->_('AMOUNT'), $this->language->_('MESSAGE'), $this->language->_('TRANSFER TYPE'), $this->language->_('CLEARING CODE'));

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_TYPE'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '5' THEN 'ONLINE'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'SELL'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'BUY'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 ELSE ''
																			END"),
						'CLR_CODE'				=> 'TT.CLR_CODE',

					)
				)
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);
			$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
		} else {
			
			$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
			$caseTransStatus = "(CASE TT.TRA_STATUS ";
			foreach($transstatusarr as $key=>$val)
			{
				$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
			}
			$caseTransStatus .= " ELSE 'N/A' END)";

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_NAME'			=> 'TT.BENEFICIARY_ACCOUNT_NAME',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						//'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

						'TRA_REFNO'				=> 'TT.TRA_REFNO',
						'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
						'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
						'BANK_CODE'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 WHEN TT.TRANSFER_TYPE = '1' THEN TT.CLR_CODE
																				 WHEN TT.TRANSFER_TYPE = '2' THEN TT.CLR_CODE
																				 ELSE TT.SWIFT_CODE
																			END"),
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'BANK_CITY'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 ELSE TT.BENEFICIARY_BANK_CITY
																			END"),
						'CITIZENSHIP'			=> 'TT.BENEFICIARY_CITIZENSHIP',
						'RESIDENT_STATUS'			=> 'TT.BENEFICIARY_RESIDENT',
						'PS_CREATED'			=> 'C.PS_CREATED',
						'REFF_ID'				=> 'C.REFF_ID',
						'PS_TXCOUNT'			=> 'C.PS_TXCOUNT',
						'PS_CCY'				=> 'C.PS_CCY',
						'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
						'TRA_STATUS'			=> $caseTransStatus,
					)
				)
				->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);

			if ($paymenttype == '19') {

				$PS_CREATED = explode('-',$data[0]['PS_CREATED']);
				$PSCREATED  = explode(' ',$PS_CREATED[2]);
				
				$datakey = array(
		                  "0" => '0',
		                  "1" => 'CASHPOOLA1',
		                  "2" => $PSCREATED[0],
		                  "3" => $PS_CREATED[1],
		                  "4" => $PS_CREATED[0],
		                  "5" => $data[0]["PS_TXCOUNT"], 
		                  "6" => $data[0]["PS_CCY"],
		                );
		      	$headerData[] = $datakey;
		      	$num = 0;
		      	$paramTrx1 = array(
		                          "0" => $num,
		                          "1" => 'SWEEP FROM ACCT (15)',
		                          "2" => 'REMAIN BALANCE',
		                          "3" => 'MESSAGE (30)',
		                          "4" => 'CUST REF (16)',
		                          "5" => 'TRANSACTION STATUS',
		                          "6" => '',
		                  );
		      	$newData[] = $paramTrx1;
		      	$no = 1;
		      	foreach ($data as $p => $pTrx)
				{
					$paramTrx = array(  
					          "0" => $no++,
					          "1" => $pTrx['SOURCE_ACCOUNT'],
					          "2" => $pTrx['TRA_REMAIN'],
					          "3" => $pTrx['TRA_MESSAGE'],
					          "4" => $pTrx['REFF_ID'],
					          "5" => $pTrx['TRA_STATUS'],
					          "6" => '',
					        );

					$newData[] = $paramTrx;
				}
				
			}else{

				$headerData = array($this->language->_('BENEFICIARY ACC'), $this->language->_('BENEFICIARY NAME'), $this->language->_('CCY'), $this->language->_('AMOUNT'), $this->language->_('MESSAGE'), $this->language->_('EMAIL ADDRESS'), $this->language->_('TRANSFER TYPE'), $this->language->_('BANK CODE'), $this->language->_('BANK NAME'), $this->language->_('BANK CITY'), $this->language->_('CITIZENSHIP'), $this->language->_('RESIDENT STATUS'));
				$newData = $data;

			}
			

			$this->_helper->download->csv($headerData, $newData, null, $PS_NUMBER);
		}
		//$header = array("BENEFICIARY ACC","BENEFICIARY NAME","CCY","AMOUNT","MESSAGE","ADDITIONAL MESSAGE","EMAIL ADDRESS","TRANSFER TYPE","BANK CODE"," BANK NAME" ,"BANK CITY","CITIZENSHIP","RESIDENT STATUS");

	}

	protected function downloadTrxDebet($PS_NUMBER)
	{
		//		ACCT SOURCE,AMOUNT,MESSAGE,ADDITIONAL MESSAGE
		//		"0000000000000000","350,000.90","MESSAGE","ADDITIONAL MESSAGE"

		$header = array("ACCT SOURCE", "AMOUNT", "MESSAGE");
		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					//	'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

					//	'TRA_REFNO'				=> 'TT.TRA_REFNO',
				)
			)
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
	}

	public function templateaddAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$cross = $this->_getParam('cross');
		$transpose = $this->_getParam('transpose');
		$exrate = $this->_getParam('exrate');
		$refnum = $this->_getParam('refnum');
		$custnum = $this->_getParam('custnum');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "1",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_CROSS' 			=> $cross,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_EXCHANGE' 		=> $exrate,
			'TEMP_REFNUM' 			=> $refnum,
			'TEMP_CUSTREF' 			=> $custnum,
			'TEMP_LOCKED'			=> $locked,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function templateadddomesticAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$tratype = $this->_getParam('tratype');
		$bene = $this->_getParam('bene');
		$beneaddress = $this->_getParam('beneaddress');
		$citizenship = $this->_getParam('citizenship');
		$nationality = $this->_getParam('nationality');
		$benecategory = $this->_getParam('benecategory');
		$beneidtype = $this->_getParam('beneidtype');
		$beneidnum = $this->_getParam('beneidnum');
		$bank = $this->_getParam('bank');
		$city = $this->_getParam('city');
		$email_domestic = $this->_getParam('email_domestic');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');


		try {
			$this->_db->beginTransaction();
			$insertArr = array(
				'T_NAME' 			=> $template,
				'T_CREATEDBY' 		=> $this->_userIdLogin,
				'T_CREATED' 		=> new Zend_Db_Expr("now()"),
				'T_GROUP'			=> $this->_custIdLogin,
				'T_TARGET'			=> $target,
				'T_TYPE'			=> '2',
			);

			$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);



			$lastId = $this->_db->lastInsertId();

			if (!empty($bene)) {
				$select = $this->_db->select()
					->from(array('A' => 'M_BENEFICIARY'), array('*'));
				$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
				$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
				$benedata = $this->_db->fetchall($select);
				$benename = $benedata['0']['BENEFICIARY_NAME'];
			} else {
				$benedata = array();
				$benename = '';
			}

			// print_r($insertArr);die;

			$insertData = array(
				'TEMP_ID' 				=> $lastId,
				'TEMP_SUBJECT' 			=> $subject,
				'TEMP_SOURCE' 			=> $source,
				'TEMP_TRATYPE' 			=> $tratype,
				'TEMP_BENE' 			=> $bene,
				'TEMP_BENE_NAME' 		=> $benename,
				'TEMP_BENEADDRESS' 		=> $beneaddress,
				'TEMP_CITIZENSHIP' 		=> $citizenship,
				'TEMP_NATIONALITY' 		=> $nationality,
				'TEMP_BENCATEGORY' 		=> $benecategory,
				'TEMP_BENEIDTYPE' 		=> $beneidtype,
				'TEMP_BENEIDNUM' 		=> $beneidnum,
				'TEMP_BANK' 			=> $bank,
				'TEMP_CITY' 			=> $city,
				'TEMP_EMAIL_DOMESTIC' 	=> $email_domestic,

				'TEMP_AMOUNT' 			=> $amount,
				'TEMP_NOTIF' 			=> $notif,
				'TEMP_EMAIL' 			=> $email,
				'TEMP_SMS' 				=> $sms,
				'TEMP_LOCKED'			=> $locked,
			);

			$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollback();
			Zend_Debug::Dump($e->getMessages());
		}

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function templateaddremittanceAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();


		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$benename2 = $this->_getParam('benename');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$chargetype = $this->_getParam('chargetype');
		$transpose = $this->_getParam('transpose');
		$emaildomestic = $this->_getParam('emaildomestic');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');
		$messagetext = $this->_getParam('messagetext');
		$identity = $this->_getParam('identity');
		$transactor = $this->_getParam('transactor');
		$ccy = $this->_getParam('ccy');
		$bankname = $this->_getParam('bankname');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "3",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_CHARGE_TYPE' 		=> $chargetype,
			'TEMP_MESSAGE' 		=> $messagetext,
			'TEMP_EMAIL_DOMESTIC' 	=> $emaildomestic,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_IDENTY' 			=> $identity,
			'TEMP_RELATIONSHIP'		=> $transactor,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_LOCKED'			=> $locked,
			'TEMP_BANK'				=> $bankname,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}


	public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		// die;

		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);

		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		// echo $selectuser;
		$datauser = $this->_db->fetchAll($selectuser);
		// print_r($datauser);die;
		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_USER'), array(
				'*'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);

		// print_r($usergroup);die();


		$this->view->boundarydata = $datauser;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {
				$group = explode('_', $value['GROUP_USER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				// print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);


					foreach ($list as $row => $data) {
						if ($data == $groupalfa) {
							$cek = true;
							// die('ter');
							break;
						}
					}
				}
			}
			if ($group[0] == 'S') {
				$cek = true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		}


		if ($cek) {




			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);

			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);

			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);
			// print_r($list);die;
			// var_dump($command)
			$thendata = explode('THEN', $command);
			// print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);
			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {
					foreach ($secondlist as $row => $thenval) {
						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			// var_dump($thendata);
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
					// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}
			// var_dump($grouplist);
			// // print_r($group);die;
			// // echo $thengroup;die;
			// echo '<pre>';
			// var_dump($thengroup);
			// var_dump($thendata);die;
			// print_r($thendata);echo '<br/>';die('here');
			if ($thengroup == true) {

				// echo $oriCommand;die;
				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;

					$indno = $i;
					// echo $oriCommand;echo '<br>';
					for ($a = $cthen - $indno; $a >= 1; --$a) {

						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}


					// print_r($oriCommand);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					// print_r($oriCommand);echo '<br>';
					// print_r($list);echo '<br>';

					$result = $this->generate($oriCommand, $list, $param, $psnumb);
					// print_r($i);

					// echo 'result-';print_r($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);


							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											return true;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						// echo $oriCommand;die;
						$result = $this->generate($oriCommand, $list, $param, $psnumb);
						// echo $result;echo '<br/>';die;
						if (!$result) {
							// die;
							foreach ($grouplist as $key => $valg) {
								// print_r($valg);die;
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							}
						} else {
							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						// die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);
						// print_r($secondlist);die;
						// print_r($grouplist);die;
						// print_r($thendata[$i]);die;
						// return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {
										return false;
									}
								}
							}
						}
						// $secondresult = $this->generate($thendata[$i-1],$list,$param,$psnumb);
						// print_r($thendata[$i-1]);
						// die;
						// print_r($thendata);die;
						foreach ($grouplist as $key => $valg) {
							// print_r($valg);
							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								// die('here');
								return true;
							} else if (trim($valg) == trim($thendata[$i - 1])) {
								$cekgroup = false;
								// die('here');
								return false;
							}
						}
						if (!$cekgroup) {
							// die('here');
							return true;
						}
					}
					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {
				// var_dump($groupalfa);die;
				foreach ($thendata as $ky => $vlue) {
					if ($vlue == $groupalfa) {
						return true;
					}
				}
				return false;
			}
			// die;


			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_APPROVAL'), array(
							'USER_ID'
						))
						->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.PS_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
					// echo $selectapprover;die;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			// print_r($approver);die;



			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$numb = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $numb . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			// print_r($phpCommand);die;
			$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			// die('here2');
			// var_dump ($result);die;
			return $result;
		} else {
			// die('here');
			return true;
		}
	}


	public function generate($command, $list, $param, $psnumb)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();
		// print_r($list);die;  	
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.PS_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;die;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		// print_r($approver);die;



		// print_r($approver);
		foreach ($param as $url) {
			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$numb = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $numb . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}

		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		// print_r($phpCommand);echo '<br/>';die;
		// var_dump($phpCommand);die;
		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			// var_dump($result);die;
			if ($result) {
				// var_dump($result);die;
				return false;
			} else {
				return true;
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;
	}

	public function summarydetailAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		$filter 			= new Application_Filtering();
		$PS_NUMBER 			= $filter->filter($this->_getParam('payReff'), "PS_NUMBER");

		$paramSQL = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);


		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramSQL);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);

		$tableMst[0]["label"] = $this->language->_('Payment Type');
		$tableMst[1]["label"] = $this->language->_('#Trans');
		$tableMst[2]["label"] = $this->language->_('Recurring');
		// var_dump($pslip);die;
		if (($pslip['PS_CATEGORY'] == 'SINGLE PAYMENT' || $pslip['PS_CATEGORY'] == 'OPEN TRANSFER')) {
			if ($pslip['TRANSFER_TYPE'] == 0) {
				if ($pslip['SOURCE_ACCT_BANK_CODE'] == $pslip['BENEF_ACCT_BANK_CODE']) {
					$payType = $pslip['payType'] . ' - ' . 'PB';
				} else {
					$payType = $pslip['payType'] . ' - ' . 'ONLINE';
				}
			} else if ($pslip['TRANSFER_TYPE'] == 1) {
				$payType = $pslip['payType'] . ' - ' . 'SKN';
			} else if ($pslip['TRANSFER_TYPE'] == 2) {
				$payType = $pslip['payType'] . ' - ' . 'RTGS';
			} else {
				$payType = $pslip['payType'];
			}
		} else if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

			if ($pslip['PS_TYPE'] == '19') {
				$poolingType = 'Remains';
			} else {
				$poolingType = 'Maintains';
			}

			$payType = $pslip['payType'] . ' - ' . $poolingType;
		} else {
			$payType = $pslip['payType'];
		}

		if (!empty($pslip['PERIODIC'])) {
			$recurring = $this->language->_('Yes');
		} else {
			$recurring = $this->language->_('No');
		}

		$tableMst[0]["value"] = $payType;
		$tableMst[1]["value"] = $pslip['PS_TXCOUNT'];
		$tableMst[2]["value"] = $recurring;

		if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {



			$arrday = array(
				'0' => 'Sunday',
				'1' => 'Monday',
				'2' => 'Tuesday',
				'3' => 'Wednesday',
				'4' => 'Thursday',
				'5' => 'Friday',
				'6' => 'Saturday'
			);



			$select	= $this->_db->select()
				->from(array('P'	 		=> 'T_PSLIP'))
				->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
				->join(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
				->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
				->GROUP('D.DAY_ID');

			$periodicData = $this->_db->fetchAll($select);

			$tableMst[3]["label"] = $this->language->_('Repeat');
			$tableMst[4]["label"] = $this->language->_('Repeat On');
			$tableMst[5]["label"] = $this->language->_('Start From');
			$tableMst[6]["label"] = $this->language->_('Time');

			if ($pslip['PS_TYPE'] == 19) {
				$tableMst[7]["label"] = $this->language->_('Remains on Source');

				$remainsOrMaintains = '<h6 class="text-danger"><b>' . $pslip['ccy'] . ' n/a </b></h6>';
			} else {
				$tableMst[7]["label"] = $this->language->_('Maintains Destination');

				$remainsOrMaintains = '';
				foreach ($periodicData as $key => $value) {
					$remainsOrMaintains .= substr($arrday[$value['DAY_ID']], 0, 3) . ' IDR ' . Application_Helper_General::displayMoney($value['LIMIT_AMOUNT']) . '<br>';
				}
			}

			if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 1) {


				if (empty($pslip['PS_PERIODIC'])) {
					$repeat = 'No';
					$repeatOn = '-';
				} else {
					$repeat = 'Daily';
					$repeatOn = '<div class="input-group">';
					foreach ($periodicData as $key => $value) {


						$repeatOn .= '<div class="roundChk" style="margin-left: -4%;">
										<input type="checkbox" disabled class="checkboxday" id="checkbox' . $value['DAY_ID'] . '" value="' . $value['DAY_ID'] . '" checked />
										<label for="checkbox' . $value['DAY_ID'] . '">' . substr($arrday[$value['DAY_ID']], 0, 1) . '</label>
									</div>';
					}
					$repeatOn .= '</div>';
				}
			} else if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 2) {
				if (empty($pslip['PS_PERIODIC'])) {
					$repeat = 'No';
					$repeatOn = '-';
				} else {
					$repeat = 'Weekly';
					$repeatOn = $arrday2[$pslip['PS_EVERY_PERIODIC']];
				}
			} else {
				if (empty($pslip['PS_PERIODIC'])) {
					$repeat = 'No';
					$repeatOn = '-';
				} else {
					$repeat = 'Monthly';
					$repeatOn = $pslip['PS_EVERY_PERIODIC'] . ' of every month';
				}
			}

			if (empty($pslip['PS_PERIODIC'])) {
				$efDate = date_create($pslip['PS_EFDATE']);
				$newEfDate = date_format($efDate, "d M Y");

				$startFrom = $newEfDate;
			} else {

				$startDate = date_create($periodicData[0]['PS_PERIODIC_STARTDATE']);
				$newStartDate = date_format($startDate, "d M Y");

				$endDate = date_create($periodicData[0]['PS_PERIODIC_ENDDATE']);
				$newEndDate = date_format($endDate, "d M Y");

				$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;
			}

			$tableMst[3]["value"] = $repeat;
			$tableMst[4]["value"] = $repeatOn;
			$tableMst[5]["value"] = $startFrom;
			$tableMst[6]["value"] = $pslip['PS_EFTIME'];
			$tableMst[7]["value"] = $remainsOrMaintains;
		} else {
			$tableMst[3]["label"] = $this->language->_('Amount');
			$tableMst[4]["label"] = $this->language->_('Total Transfer Fee');
			$tableMst[5]["label"] = $this->language->_('Total');


			$tableMst[3]["value"] = $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']);
			$tableMst[4]["value"] = Application_Helper_General::displayMoney($pslip['FULL_AMOUNT_FEE'] == '0' ? '-' : $pslip['ccy'] . ' ' . $pslip['FULL_AMOUNT_FEE']);
			$tableMst[5]["value"] = '<h6 class="text-danger"><b>' . $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney(($pslip['PS_TOTAL_AMOUNT'] + $pslip['FULL_AMOUNT_FEE'])) . '</b></h6>';
		}
		if ($pslip["PS_BILLER_ID"] == '1158' && $pslip["PS_TYPE"] == '17') {
			$tableMst = $this->etax($pslip);
		}
		// echo '<pre>';
		// var_dump($tableMst);die;
		$this->view->tableMst 		= $tableMst;
	}
}
