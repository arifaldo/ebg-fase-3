<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Onlinebank_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index'); 
	
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
    	
    	$this->view->report_msg = array();

		if(!$this->_request->isPost())
		{
			$bank_id = $this->_getParam('bank_id');
			$bank_id = (Zend_Validate::is($bank_id,'Digits'))? $bank_id : null;
			
			if($bank_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_DOMESTIC_ONLINE_BANK_TABLE')) 
									 ->where("BANK_ID=?", $bank_id)
							                   );	
			  if($resultdata)
			  {
			        $this->view->bank_id        = $resultdata['BANK_ID'];
					$this->view->bank_name      = $resultdata['BANK_OL_NAME'];					
					$this->view->bank_code  	= $resultdata['BANK_OL_CODE'];					
			  } 
			}
			else
			{
			   $error_remark = 'Bank ID not found';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());	
			}
		}
		else
		{
			$filters = array(
							 'bank_id'       => array('StringTrim','StripTags'),
			                 'bank_name'     => array('StringTrim','StripTags'),			                 
							 'bank_code'	 => array('StringTrim','StripTags'),							 
							);
							
			$bank_id = $this->_getParam('bank_id');
			$resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_DOMESTIC_ONLINE_BANK_TABLE')) 
									 ->where("BANK_ID=?", $bank_id)
							                   );	
			$BANK_CODE = "BANK_OL_CODE <> '".$resultdata['BANK_OL_CODE']."'";
			$BANK_NAME = "BANK_OL_NAME <> '".$resultdata['BANK_OL_NAME']."'";

			$validators = array('bank_code'  => array('NotEmpty',
																'Digits',
																 new Zend_Validate_StringLength(array('min'=>3,'max'=>3)),
																 array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'M_DOMESTIC_ONLINE_BANK_TABLE',
																							'field'=>'BANK_OL_CODE',
																							'exclude'=>$BANK_CODE
																						)
																			),
																'messages' => array(
																				   $this->language->_('Can not be empty'),
																				   $this->language->_('Invalid Format'),
																					$this->language->_('Data too long (min. 3 chars and max. 3 chars)'),
																					$this->language->_('Bank Code already exists'),
																					)
																			),

											'bank_name' => array('NotEmpty',
																	new Zend_Validate_Alnum(true),
																	new Zend_Validate_StringLength(array('max'=>35)),
																	array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'M_DOMESTIC_ONLINE_BANK_TABLE',
																							'field'=>'BANK_OL_NAME',
																							'exclude'=>$BANK_NAME
																						)
																			),
																	'messages' => array(
																					 $this->language->_('Can not be empty'),
																					 $this->language->_('Invalid Bank Name Format ( no special characters )'),
																					 $this->language->_('Data too long (max 35 chars)'),
																					 $this->language->_('Bank Name already exists'),
																					 )
																),
											'bank_id'  => array('NotEmpty',
													'Digits',
													'messages' => array(
															           $this->language->_('Can not be empty'),
															           $this->language->_('invalid format'))
													                    ),
										   );
		
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//Zend_Debug::Dump($this->_request->getParams());die;
			
			if($zf_filter_input->isValid())
			{  
				//echo "masuk";die;
				$content = array(
								'BANK_OL_NAME' 	 	=> $zf_filter_input->bank_name,								
								'BANK_OL_CODE' 	 	=> $zf_filter_input->bank_code,								
						       );
				//$content['BANK_ISDISPLAYED'] = (!empty($zf_filter_input->swift_code)) ? 1 : 0;
				
				try 
				{
				    //-----insert--------------
					$this->_db->beginTransaction();
					
					$whereArr  = array('BANK_ID = ?'=>$zf_filter_input->bank_id);
					$this->_db->update('M_DOMESTIC_ONLINE_BANK_TABLE',$content,$whereArr);

					$this->_db->commit();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					Application_Helper_General::writeLog('DBUD','Edit Online Bank. Bank ID : ['.$zf_filter_input->bank_id.']');
					$this->view->success = true;
					$this->view->report_msg = array();
					
					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();

				    foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);
					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{  
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('DBUD','Viewing Edit Online Bank');
		}
	}

}