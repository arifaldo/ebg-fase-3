<?php

require_once 'General/Settings.php';

class user_Model_User extends Application_Main
{

  public function insertTempUser($change_id, $user_data)
  {
    $content = array(
      'CHANGES_ID'     => $change_id,
      'CUST_ID'        => $user_data['CUST_ID'],
      'USER_ID'        => $user_data['USER_ID'],
      'USER_FULLNAME'  => $user_data['USER_FULLNAME'],
      'USER_PASSWORD'  => $user_data['USER_PASSWORD'],
      'USER_EMAIL'     => $user_data['USER_EMAIL'],
      'USER_PHONE'     => $user_data['USER_PHONE'],
      'TOKEN_ID'       => $user_data['TOKEN_ID'],
      'USER_EXT'       => $user_data['USER_EXT'],


      // 'USER_ISWEBSERVICES' => $user_data['USER_ISWEBSERVICES'],
      'USER_ISEMAIL'       => $user_data['USER_ISEMAIL'],

      'USER_SUGGESTED'     => $user_data['USER_SUGGESTED'],
      'USER_SUGGESTEDBY'   => $user_data['USER_SUGGESTEDBY'],

      'USER_STATUS'        => $user_data['USER_STATUS'],

      'USER_CREATED'       => $user_data['USER_CREATED'],
      'USER_CREATEDBY'     => $user_data['USER_CREATEDBY'],
      'USER_UPDATED'       => $user_data['USER_UPDATED'],
      'USER_UPDATEDBY'     => $user_data['USER_UPDATEDBY'],

      'GOOGLE_CODE'     => $user_data['GOOGLE_CODE'],
      'USER_HOME_CONTENT'     => $user_data['USER_HOME_CONTENT'],
      'USER_THEME'     => $user_data['USER_THEME'],
      'USER_INDEX'     => $user_data['USER_INDEX'],
      'USER_CODE'     => $user_data['USER_CODE'],
      'USER_IMAGE'     => $user_data['USER_IMAGE'],
      'USER_DATEPASS'     => $user_data['USER_DATEPASS'],
      'USER_ISLOCKED'      => $user_data['USER_ISLOCKED'],

      // 'USER_ISEMAIL_DOC'   => $user_data['USER_ISEMAIL_DOC'],
      // 'USER_ISEMAIL_EARLY' => $user_data['USER_ISEMAIL_EARLY'],
      'USER_ISEMAIL_TRAN'  => 1,
      'USER_ISLOGIN'       => 0,
      //                        'USER_ISLOCKED'      => 0,
      'USER_FAILEDATTEMPT' => 0,
    );

    //sif(isset($user_data['TOKEN_SERIALNO']))$content = array_merge($content,array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
    $this->_db->insert('TEMP_USER', $content);
  }

  public function getUserData($cust_id, $user_id)
  {
    $select = $this->_db->select()
      ->from(array('T' => 'M_USER'))
      ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
      ->where('UPPER(USER_ID)=' . $this->_db->quote((string)$user_id))
      ->query()->fetch();

    return $select;
  }

  public function updateTempUser($changes_id, $user_data)
  {
    /*Zend_Debug::dump($user_data);
      die;*/

    $content = array(
      //'CUST_ID'        => $user_data['CUST_ID'],
      //'USER_ID'        => $user_data['USER_ID'],
      'USER_FULLNAME'  => $user_data['USER_FULLNAME'],
      'USER_PASSWORD'  => $user_data['USER_PASSWORD'],
      'USER_EMAIL'     => $user_data['USER_EMAIL'],
      'USER_PHONE'     => $user_data['USER_PHONE'],
      'TOKEN_ID'       => $user_data['TOKEN_ID'],
      'USER_EXT'       => $user_data['USER_EXT'],
      //'USER_STATUS'    => $user_data['USER_STATUS'],
      // 'USER_ISWEBSERVICES' => $user_data['USER_ISWEBSERVICES'],
      'USER_ISEMAIL'       => $user_data['USER_ISEMAIL'],
      //'USER_SUGGESTED'     => $user_data['USER_SUGGESTED'],
      //'USER_SUGGESTEDBY'   => $user_data['USER_SUGGESTEDBY'],
      // 'USER_ISEMAIL_DOC'   => $user_data['USER_ISEMAIL_DOC'],
      // 'USER_ISEMAIL_EARLY' => $user_data['USER_ISEMAIL_EARLY'],
      //'USER_ISLOGIN'       => 0,
      //'USER_ISLOCKED'      => 0,
      // 'USER_FAILEDATTEMPT' => 0,
    );

    $whereArr = array('CHANGES_ID = ?' => $changes_id);
    $update = $this->_db->update('TEMP_USER', $content, $whereArr);
  }


  public function insertTempPrivilege($change_id, $privilege_data, $user_data)
  {
    foreach ($privilege_data as $privi) {
      $fuser_id = $user_data['CUST_ID'] . $user_data['USER_ID'];

      $content = array(
        'CHANGES_ID' => $change_id,
        'FPRIVI_ID'  => $privi,
        'FUSER_ID'   => $fuser_id
      );

      $this->_db->insert('TEMP_FPRIVI_USER', $content);
    }
  }

  public function getPrivilege()
  {
    // $select = $this->_db->select()
    //                     ->from('M_FPRIVILEGE',array('FPRIVI_ID','FPRIVI_DESC','FPRIVI_MODULEID'))
    //                     ->order('FPRIVI_DESC ASC')
    //                     ->query()->fetchAll();

    $select = $this->_db->select()
      ->from('M_FPRIVILEGE', array('FPRIVI_ID', 'FPRIVI_DESC', 'FPRIVI_MODULEID'));

    $setting = new Settings();
    $type = $setting->getSetting('system_type');
    // var_dump($type);
    if ($type == '1') {
      $select->where('FPRIVI_MODE IN (0,1)');
    } else if ($type == '2') {
      $select->where('FPRIVI_MODE IN (0,2)');
    }
    $select->order('FPRIVI_DESC ASC');

    //echo $select;
    $select = $select->query()->fetchAll();

    //$select = $select->query()->fetchAll();
    // ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' OR UPPER(CUST_ID)='.$this->_db->quote('BANK'))
    // ->where('UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
    return $select;
  }

  //mengurutkan privi grouping
  public function modifPrivi($privi)
  {
    $privi_final = array();
    $privi_final['ACGR'] = (isset($privi['ACGR'])) ? $privi['ACGR'] : '';
    $privi_final['ACIF'] = (isset($privi['ACIF'])) ? $privi['ACIF'] : '';
    $privi_final['BEAC'] = (isset($privi['BEAC'])) ? $privi['BEAC'] : '';
    $privi_final['CHRL'] = (isset($privi['CHRL'])) ? $privi['CHRL'] : '';
    $privi_final['CMPW'] = (isset($privi['CMPW'])) ? $privi['CMPW'] : '';
    $privi_final['CMPY'] = (isset($privi['CMPY'])) ? $privi['CMPY'] : '';
    $privi_final['CMRP'] = (isset($privi['CMRP'])) ? $privi['CMRP'] : '';
    $privi_final['OPBK'] = (isset($privi['OPBK'])) ? $privi['OPBK'] : '';
    $privi_final['OPAD'] = (isset($privi['OPAD'])) ? $privi['OPAD'] : '';
    $privi_final['OPIF'] = (isset($privi['OPIF'])) ? $privi['OPIF'] : '';
    $privi_final['ATDB'] = (isset($privi['ATDB'])) ? $privi['ATDB'] : '';
    $privi_final['SMBG'] = (isset($privi['SMBG'])) ? $privi['SMBG'] : '';
    $privi_final['SWBG'] = (isset($privi['SWBG'])) ? $privi['SWBG'] : '';
    // $privi_final['SCPD'] = (isset($privi['SCPD'])) ? $privi['SCPD'] : '';
    // $privi_final['SCRP'] = (isset($privi['SCRP'])) ? $privi['SCRP'] : '';
    $privi_final['LABE'] = (isset($privi['LABE'])) ? $privi['LABE'] : '';
    $privi_final['OTRP'] = (isset($privi['OTRP'])) ? $privi['OTRP'] : '';
    $privi_final['USRL'] = (isset($privi['USRL'])) ? $privi['USRL'] : '';
    $privi_final['UTLT'] = (isset($privi['UTLT'])) ? $privi['UTLT'] : '';
    $privi_final['IPDT'] = (isset($privi['IPDT'])) ? $privi['IPDT'] : '';

    return $privi_final;
  }

  public function getTemplate($type = null)
  {
    if ($type == '1') {
      $select = $this->_db->select()
        ->from('M_FTEMPLATE')
        ->where('FTEMPLATE_GROUP = ?', 'B')
        ->query()->fetchAll();
    } else {
      $select = $this->_db->select()
        ->from('M_FTEMPLATE')
        ->where('FTEMPLATE_GROUP = ?', 'F')
        ->query()->fetchAll();
    }

    return $select;
  }


  public function getPriviTemplate()
  {
    $select = $this->_db->select()
      ->from('M_FPRIVILEGE_TEMPLATE')
      ->query()->fetchAll();

    //                       ->query()->fetchAll();
    return $select;
  }

  public function getModuleDescArr()
  {
    $select = $this->_db->select()
      ->from('M_MODULE');
    $setting = new Settings();
    $type = $setting->getSetting('system_type');
    // var_dump($type);
    if ($type == '1') {
      $select->where('MODULE_OPEN IN (0,1)');
    } else if ($type == '2') {
      $select->where('MODULE_OPEN IN (0,2)');
    }

    //$result->order('FPRIVI_DESC ASC');

    //echo $select;
    $select = $select->query()->fetchAll();

    $module_desc = array();
    foreach ($select as $row) {
      $module_desc[$row['MODULE_ID']] = $row['MODULE_DESC'];
    }

    return $module_desc;
  }

  public function getTempUser($changes_id)
  {
    $select = $this->_db->select()
      ->from(array('T' => 'TEMP_USER'))
      ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS'))
      ->where('T.CHANGES_ID = ?', $changes_id)
      ->query()->fetch();
    return $select;
  }

  public function getTempFpriviUser($changes_id)
  {
    $select = $this->_db->select()
      ->from(array('T' => 'TEMP_FPRIVI_USER'))
      ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS'))
      ->where('T.CHANGES_ID = ?', $changes_id)
      ->query()->fetchAll();
    return $select;
  }

  public function deleteTempFpriviUser($changes_id)
  {
    $delete = $this->_db->delete('TEMP_FPRIVI_USER', $this->_db->quoteInto('CHANGES_ID = ?', $changes_id));
  }


  public function getTempMaker($changes_id)
  {
    $select = $this->_db->select()
      ->from('TEMP_MAKERLIMIT')
      ->where('CHANGES_ID = ?', $changes_id)
      ->query()->fetchAll();
    return $select;
  }

  public function getTempDaily($changes_id)
  {
    $select = $this->_db->select()
      ->from('TEMP_DAILYLIMIT')
      ->where('CHANGES_ID = ?', $changes_id)
      ->query()->fetchAll();
    return $select;
  }

  // public function deleteTempFpriviUser($changes_id)
  // {
  //    $delete = $this->_db->delete('TEMP_FPRIVI_USER',$this->_db->quoteInto('CHANGES_ID = ?',$changes_id));
  // }


  public function deleteTempDaily($changes_id)
  {
    $delete = $this->_db->delete('TEMP_DAILYLIMIT', $this->_db->quoteInto('CHANGES_ID = ?', $changes_id));
  }

  public function deleteTempMaker($changes_id)
  {
    $delete = $this->_db->delete('TEMP_MAKERLIMIT', $this->_db->quoteInto('CHANGES_ID = ?', $changes_id));
  }


  public function deleteTempCustacct($changes_id)
  {

    $delete = $this->_db->delete('TEMP_CUSTOMER_ACCT', $this->_db->quoteInto('CHANGES_ID = ?', $changes_id));
  }
}
