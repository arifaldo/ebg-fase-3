<?php

require_once 'Zend/Controller/Action.php';

class onlinebank_ImportController extends Application_Main
{
	public function indexAction()
	{
		$attahmentDestination 	= UPLOAD_PATH.'/document/temp';		
		$adapter 				= new Zend_File_Transfer_Adapter_Http();
		/* $field = array(
										'CLR_CODE' 			=> 7,
										'SWIFT_CODE' 		=> 15,
										'BANK_NAME'	 		=> 35,
										'BANK_OFFICE_NAME'  => 20,
										'BANK_ADDRESS' 		=> 50,
										'CITY'		 	=> 4,
										'WORKFIELD_CODE'	=> 1,
										'PROVINCE_CODE' 	=> 4,
										'KBI_CODE'			=> 3,
										'BRANCH_CODE' 		=> 4,
										'BI_ACCOUNT'		=> 12,
										'POP_STATUS_CODE' 	=> 1,
										'RES_STATUS_CODE' 	=> 1,
										'CLR_STATUS' 		=> 1,
										'CORD_STATUS_CODE' 	=> 1,
										'BANK_INST_CODE' 	=> 1,
										'ACTIVE_DATE' 		=> 8,
										);  */  
		$field = array(										
										'BANK_OL_CODE' 		=> 3,
										'BANK_OL_NAME'		=> 35,
										/* 'WORKFIELD_CODE'	=> 1,
										'CITY_CODE'		 	=> 4,
										'PROVINCE_CODE' 	=> 4,
										'BRANCH_CODE' 		=> 4,
										'BRANCH_NAME' 		=> 30, */
										);
		$this->view->field = $field;
		if($this->_request->isPost())
		{
			//Zend_Debug::dump($attahmentDestination);die;
			$params = $this->_request->getParams();
			$adapter->setDestination ($attahmentDestination);
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
			$extensionValidator->setMessage(
												$this->language->_('Error: Extension file must be *.txt')
											);
											
			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
										$this->language->_('Error: File size must not more than ').$this->getSetting('Fe_attachment_maxbyte')
										);

			$adapter->setValidators(array($extensionValidator,$sizeValidator));	
			
			if($adapter->isValid())
			{
/*
	Clearing Code* (max 7 chars) *	
	Swift Code (max 15 chars)	
	Bank Name* (max 35 chars) *	
	Bank Office Name (max 20 chars)	
	Bank Address (max 50 chars)	
	City Code (max 4 chars)	
	Workfield Code* (max 1 chars) *	
	Province Code (max 4 chars)	
	KBI Code* (max 3 chars) *	
	Branch Code* (max 4 chars) *	
	BI Account (max 12 chars)	
	Population Status (max 1 chars)	
	Resident Status (max 1 chars)	
	Clearing Status (max 1 chars)	
	Cord Status (max 1 chars)	
	Bank Inst Code (max 1 chars)	
	Active Date (Datepicker)
*/
				$success = 1;
				$sourceFileName = $adapter->getFileName();
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				$adapter->addFilter ( 'Rename',$newFileName);
				$adapter->receive();
				//Zend_Debug::dump($newFileName);die;
				$myFile = file_get_contents($newFileName);
				$arry_myFile = explode("\n", $myFile);
				@unlink($newFileName);
				//Zend_Debug::dump($arry_myFile);die;
				if($adapter->receive())
				{
					$this->_db->beginTransaction();
					$master = $this->_db->FetchRow
										($this->_db->select()
													->from('M_DOMESTIC_ONLINE_BANK_TABLE')
													//->where('BANK_ISMASTER = 1')
										);

					$this->_db->query('TRUNCATE TABLE M_DOMESTIC_ONLINE_BANK_TABLE');

					if(!empty($master))
					{
						unset($master['BANK_ID']);
						$this->_db->insert("M_DOMESTIC_ONLINE_BANK_TABLE",$master);
					}

					foreach ($arry_myFile as $row) 
					{

						// $clr_code = trim(str_replace("'","\'",substr($row,0,7)));
						// $bank_name_city = str_replace("'","\'",substr($row,7,35));							
						
						// $swift_code 	  = trim(str_replace("'","\'",substr($row,42,15)));
						// $workfield_code	  = trim(str_replace("'","\'",substr($row,58,1)));
						
						$start = 0;
						foreach($field as $key=>$length)
						{
							$var[$key] = trim(str_replace("'","\'",substr($row,$start,$length)));
							$start += $length;
						}
						
						
						//$var['BANK_ISDISPLAYED'] = 1;
						
						/* if($var['SWIFT_CODE'] != "") 
						{								
							$var['BANK_ISDISPLAYED'] = 0;
						} */
						
/* 						if($var['WORKFIELD_CODE'] == " ") 
						{
							$var['WORKFIELD_CODE'] = 0;
						} */

						try
						{
							if( !empty($var['BANK_OL_CODE']) )
							{
								// $paramPayment = array(	
														// "CLR_CODE"			=> $clr_code,
														// "SWIFT_CODE"		=> $swift_code,
														// "BANK_NAME"			=> $bank_name,
														// "CITY"				=> $bank_city,
														// "WORKFIELD_CODE"	=> $workfield_code,
														// "BANK_ISDISPLAYED"	=> $bank_isdisplayed,
													 // );
								$this->_db->insert("M_DOMESTIC_ONLINE_BANK_TABLE",$var);
							}
						}
						catch(Exception $e)
						{
							$success = 0;
							$this->_db->rollBack();
						}
					}
				}
				if($success == 1)
				{
					$this->_db->commit();
					Application_Helper_General::writeLog('DBIM','Import Online Bank');
					$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
					$this->_redirect('/notification/success/index');
				}		
			}
			else
			{
				$this->view->errorMsg = $adapter->getMessages();
				//Zend_Debug::dump($adapter->getMessages());die;
			}
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('DBIM','Viewing Import Online Bank');
		}
	}
}