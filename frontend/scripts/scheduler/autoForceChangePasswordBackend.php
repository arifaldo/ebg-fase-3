<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

/*
SELECT t.CUST_ID, t.USER_ID, MAX(PASSWORD_DATETIME)
FROM "T_FPASSWORD_HISTORY" AS "T" 
group by t.CUST_ID, t.USER_ID
HAVING (DATEADD(day, 3,  MAX(PASSWORD_DATETIME)) < now())

*/

function forcechange($result,$timeout_pwd){
	
	
	$db = Zend_Db_Table::getDefaultAdapter();	
	
	try{
		
		$db->beginTransaction();
		$res = NULL;
		
		//print_r($result);
		if(is_array($result) && count($result) > 0){
		
			foreach($result as $row){
				
				unset($where);
				
				$userid = $row['userid'];
				
				//echo "userid: $userid, custid: $custid <br />";
				$lockreason = "User is required to change password. Password will expire after ".$timeout_pwd." day(s). Last change password was at ".$row['datetime'];
				$update = array('BUSER_ISREQUIRE_CHANGEPWD'=>'1',
								'BUSER_ISLOGIN'		=>'0',
								'BUSER_LOCKREASON'	=> $lockreason
								);
				$where[] = "BUSER_ID = '".$userid."'";
				$where[] = "BUSER_ID != 'SUPERADMIN01'";
								
				$res += $db->update('M_BUSER', $update, $where);
				
			}
			
			echo "ROW EFFECTED = ".$res;
		}
		$db->commit();	
		Application_Helper_General::cronLog('autoForceChangePasswordUserBackend', $res, '1');
	
	}
	catch(Exception $e){
		
		$db->rollBack();
		$res = $e->getMessage();
		Application_Helper_General::cronLog('autoForceChangePasswordUserBackend', $res, '0');
		
	}

}


function forcepwdbackend($timeout_pwd){
	
	$db = Zend_Db_Table::getDefaultAdapter();
	
	$result = $db->select()
				 ->from(array('A'=>'M_BUSER'),array( 'userid'	=>'BUSER_ID',
													 'datetime'	=>'BUSER_LASTCHANGEPASSWORD'
													))
				 ->where("	A.BUSER_STATUS = '1' 
							AND A.BUSER_ISREQUIRE_CHANGEPWD != '1'
							AND A.CREATED is not NULL
							AND TIMESTAMPDIFF(day, date(BUSER_LASTCHANGEPASSWORD) , date(now()) ) > ?", $timeout_pwd
						)
				->query()
				->fetchall();
				
	 //echo $result ->__toString();
	
	if(is_array($result) && count($result) > 0){
	
		//print_r($result);
		forcechange($result,$timeout_pwd);
		
	}
	
	else echo "No Data";
	
	
}

function autoforcechangepwdbackend(){
	
	$set = new Settings();
	$timeout_pwd = $set->getSetting('pwd_expired_day');
	forcepwdbackend($timeout_pwd);

}

autoforcechangepwdbackend();

?>