<?php

require_once 'Zend/Controller/Action.php';


class tools_DatasubmissionController extends Application_Main {

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

		$maxFileSize = "1024"; //"1024000"; //1024
		//~ $fileExt = "csv,dbf";jpeg / jpg / pdf
		$fileExt = "jpeg,jpg,pdf";

		if($this->_request->isPost())
		{
			$attahmentDestination 	= UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			// print_r($attahmentDestination);die;
			$params = $this->_request->getParams();

			$sourceFileName = trim($adapter->getFileName());
			// print_r($sourceFileName);die;
			if($sourceFileName == null){

				$sourceFileName = null;
				$fileType = null;
			}
			else
			{

				$sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
				if($_FILES["document"]["type"])
				{
					$adapter->setDestination($attahmentDestination);
					$fileType 				= $adapter->getMimeType();
					$size 					= $_FILES["document"]["size"];
				}
				else{
					$fileType = null;
					$size = null;
				}
			}
			$paramsName['sourceFileName'] 	= $sourceFileName;

			$helpTopic 	= $this->_getParam('helpTopic');
			if($helpTopic == null)	$helpTopic = null;

			$paramsName['helpTopic']  	= $helpTopic;
			$description 				= $this->_getParam('description');
			$paramsName['description']  = $description;

			$filtersName = array(
									'sourceFileName'	=> array('StringTrim'),
									'description'		=> array('StringTrim'),
								);

			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error : File cannot be left blank')
																				)
														),
									'description' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error: Description cannot be left blank')
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);

			if($sourceFileName == NULL){

				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName']))? $errors['sourceFileName'] : null;

			}
			elseif($description == NULL){


				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->descriptionErr 	= (isset($errors['description']))? $errors['description'] : null;

			}
			else{

				if($zf_filter_input_name->isValid())
				{
					$fileTypeMessage = explode('/',$fileType);
					if (isset($fileTypeMessage[1])){
						$fileType =  $fileTypeMessage[1];
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
						$extensionValidator->setMessage("Extension file must be *.csv /.dbf / .pdf");

						$size 			= number_format($size);
						$sizeTampil 	= $size/1000;
						//$sizeValidator 	= new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
						// $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");

						$adapter->setValidators(array($extensionValidator));

						if($adapter->isValid())
						{
							$date = date("dmy");
							$time = date("his");

							$newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
							$adapter->addFilter('Rename', $newFileName);

							if($adapter->receive())
							{


								$filedata = $attahmentDestination.$newFileName;
								// var_dump($filedata);
								if (file_exists($filedata)) {
								exec("clamdscan --fdpass ".$filedata, $out ,$int);
									
								// if()
									// var_dump($int);
									// var_dump($out);
									// die;
								if($int == 1){

									unlink($filedata);
									//die('here');
									$this->view->error = true;
									$errors = array(array('FileType'=>'Error: An infected file. Please upload other file'));
									$this->view->errorMsg = $errors;


								}else{

									try
									{
										$this->_db->beginTransaction();
										$insertArr = array(
												'CUST_ID'				=> $this->_custIdLogin,
												'FILE_TYPE'				=> '0',
												'FILE_UPLOADEDBY'			=> $this->_userIdLogin,
												'FILE_NAME' 			=> $sourceFileName,
												'FILE_DESCRIPTION'		=> $description,
												'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
												'FILE_DOWNLOADED'		=> '0',
												'FILE_DELETED'			=> '0',
												'FILE_DOWNLOADEDBY'		=> '',
												'FILE_DELETEDBY'		=> '',
												'FILE_ISEMAILED'		=> '0',
												'FILE_SYSNAME'			=> $newFileName,
										);

										$this->_db->insert('T_FILE_SUBMIT', $insertArr);
										$this->_db->commit();

										Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);

										$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
										$this->_redirect('/notification/success');
									}
									catch(Exception $e)
									{
										$this->_db->rollBack();
										Application_Log_GeneralLog::technicalLog($e);
									}


								}
							}

							}
						}
						else
						{
							$this->view->error = true;
							$errors = array($adapter->getMessages());
							$this->view->errorMsg = $errors;
						}
					}else{
						$this->view->error = true;
						$errors = array(array('FileType'=>'Error: Unknown File Type'));
						$this->view->errorMsg = $errors;
					}
				}
			}
		}

		$FILE_DESCRIPTION_len = (isset($description))? strlen($description): 0;
		$FILE_DESCRIPTION_len = 150 - $FILE_DESCRIPTION_len;

		//$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;

		$this->view->FILE_DESCRIPTION_len		= $FILE_DESCRIPTION_len;


		$this->view->max_file_size = $maxFileSize; //'1024';
		$this->view->file_extension = $fileExt;
		Application_Helper_General::writeLog('DELI',"Viewing File Sharing");
	}
}
