<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class predefinedbeneficiary_InhouseacctController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	protected $_destinationUploadDir = '';
	protected $_listCCYValidate = '';

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(), 'CCY_ID');
		$selectCurrency = '-- ' . $this->language->_('Select Currency') . '--';
		$listCcy = array('' => $selectCurrency);
		if (count($this->getCcy()) == 1) { //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy, Application_Helper_Array::listArray($this->getCcy(), 'CCY_ID', 'CCY_ID'));
		$this->view->ccy = $listCcy;
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$sessionNamespace = new Zend_Session_Namespace('errimport');

		$this->view->error = $sessionNamespace->error;
		$sessionNamespace->unsetAll();
		//var_dump($this->view->error);


		$fields = array(
			/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
			'benef_acct'  => array(
				'field' => 'BENEFICIARY_ACCOUNT',
				'label' => $this->language->_('Beneficiary Account'),
				'sortable' => true
			),
			'benef_name'  => array(
				'field' => 'BENEFICIARY_NAME',
				'label' => $this->language->_('Beneficiary Account Name'),
				'sortable' => true
			),
			'email'  => array(
				'field' => 'BENEFICIARY_EMAIL',
				'label' => $this->language->_('Email Address'),
				'sortable' => true
			),
			'ccy'   => array(
				'field'    => 'CURR_CODE',
				'label'    => $this->language->_('CCY'),
				'sortable' => true
			),
			'favorite'   => array(
				'field'    => 'ISFAVORITE',
				'label'    => $this->language->_('Favorite'),
				'sortable' => true
			),
			'checked'   => array(
				'field'    => 'BENEFICIARY_BANKSTATUS_disp',
				'label'    => $this->language->_('Checked by Bank'),
				'sortable' => true
			),

			'date'   => array(
				'field'    => 'BENEFICIARY_CREATED',
				'label'    => $this->language->_('Created Date'),
				'sortable' => true
			),

			'status'   => array(
				'field'    => 'BENEFICIARY_ISAPPROVE_disp',
				'label'    => $this->language->_('Status'),
				'sortable' => true
			)

			// 'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
			// 					  'label'    => $this->language->_('Delete'),
			// 					  'sortable' => false)
		);


		$filterlist = array('BENEFICIARY_ACCOUNT', 'BENEFICIARY_NAME', 'BENEFICIARY_ALIAS', 'CURR_CODE');

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$csv 	 = $this->_getParam('csv');
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'favorite');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		$dataParam = array('BENEFICIARY_ACCOUNT', 'BENEFICIARY_NAME', 'BENEFICIARY_ALIAS', 'CURR_CODE');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		// print_r($dataParamValue);die;
		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(
			'filter' 	  	=> array('StringTrim', 'StripTags'),
			'BENEFICIARY_ALIAS' 	  	=> array('StringTrim', 'StripTags'),
			'BENEFICIARY_ACCOUNT'    => array('StringTrim', 'StripTags', 'StringToUpper'),
			'BENEFICIARY_NAME'    => array('StringTrim', 'StripTags'),
			'CURR_CODE'    	=> array('StringTrim', 'StripTags', 'StringToUpper')
			//'ISFAVORITE'     	=> array('StringTrim','StripTags')
		);

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $dataParamValue);
		$filter = $this->_request->getParam('filter');

		$alpha = $this->_getParam('alpha');
		$fav = $this->_getParam('fav');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->alpha = $alpha;
		$this->view->fav = $fav;
		// print_r($this->_request->getParams());die;
		/*$select = $this->_db->select()
					   ->from('M_BENEFICIARY',array('BENEFICIARY_ID','BENEFICIARY_ALIAS','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BENEFICIARY_EMAIL','CURR_CODE','ISFAVORITE','BENEFICIARY_BANKSTATUS' => '(CASE BENEFICIARY_BANKSTATUS WHEN 0 THEN \'No\' WHEN 1 THEN \'Yes\' WHEN 2 THEN \'Failed\' END)','BENEFICIARY_ISREQUEST_DELETE','BENEFICIARY_ISAPPROVE' => '(CASE BENEFICIARY_ISAPPROVE WHEN 0 THEN \'Not Approved\' WHEN 1 THEN \'Approved\' END)','BENEFICIARY_CREATED'));
		$select->where("BENEFICIARY_TYPE = 1");
		$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));*/

		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

		$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));

		$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		if ($alpha) $select->where("BENEFICIARY_NAME LIKE " . $this->_db->quote($alpha . '%'));
		if ($fav) $select->where('ISFAVORITE=1');

		// Zend_Debug::dump($select->query()->fetchAll());die;
		$result = $select->query()->fetchAll();

		if ($filter == TRUE) {
			// print_r($zf_filter);die;
			$fAlias = $zf_filter->getEscaped('BENEFICIARY_ALIAS');
			$fAcct = $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');
			$fName = $zf_filter->getEscaped('BENEFICIARY_NAME');
			$fCcy = $zf_filter->getEscaped('CURR_CODE');
			$fFav = $zf_filter->getEscaped('ISFAVORITE');

			if ($fAlias) $select->where('UPPER(BENEFICIARY_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fAlias) . '%'));
			if ($fAcct) $select->where('BENEFICIARY_ACCOUNT LIKE ' . $this->_db->quote('%' . strtoupper($fAcct) . '%'));
			if ($fName) $select->where('UPPER(BENEFICIARY_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fName) . '%'));
			if ($fCcy) $select->where('CURR_CODE = ' . $this->_db->quote(strtoupper($fCcy)));
			if ($fFav == 1) $select->where('ISFAVORITE=1');

			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->benef_ccy = $fCcy;
			if ($fFav == 1) $this->view->favorit = true;
			else $this->view->favorit = false;
		} else $this->view->favorit = false;

		$select->order($sortBy . ' ' . $sortDir);
		$select = $select->query()->fetchAll();

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token   = $rand;
		$this->view->token = $sessionNamespace->token;

		foreach ($select as $key => $value) {
			$get_beneficiary_id = $value["BENEFICIARY_ID"];

			$AESMYSQL = new Crypt_AESMYSQL();
			$rand = $this->token;

			$encrypted_payreff = $AESMYSQL->encrypt($get_beneficiary_id, $rand);
			$encpayreff = urlencode($encrypted_payreff);

			$select[$key]["BENEFICIARY_ID_ENCRYPTED"] = $encpayreff;
		}

		$this->paging($select);

		$data_val = array();
		foreach ($result as $valu) {
			$paramTrx = array(
				"BENEFICIARY_ACCOUNT" => $valu['BENEFICIARY_ACCOUNT'],
				"BENEFICIARY_NAME"	=> $valu['BENEFICIARY_NAME'],
				"BENEFICIARY_EMAIL"	=> $valu['BENEFICIARY_EMAIL'],
				"CURR_CODE"	=> $valu['CURR_CODE'],
				"ISFAVORITE_disp"	=> $valu['ISFAVORITE_disp'],
				"BENEFICIARY_BANKSTATUS_disp"	=> $valu['BENEFICIARY_BANKSTATUS_disp'],
				"BENEFICIARY_CREATED"	=> $valu['BENEFICIARY_CREATED'],
				"BENEFICIARY_ISAPPROVE_disp"	=> $valu['BENEFICIARY_ISAPPROVE_disp'],
			);
			array_push($data_val, $paramTrx);
		}

		if ($csv) {

			$header  = Application_Helper_Array::simpleArray($fields, "label");
			$this->_helper->download->csv($header, $data_val, null, 'Beneficiary List');
		}
		Application_Helper_General::writeLog('BLBA', 'Viewing In House Beneficiary');
		$this->view->fields = $fields;
		if (!empty($this->_request->getParam('wherecol'))) {
			$this->view->wherecol			= $this->_request->getParam('wherecol');
		}

		if (!empty($this->_request->getParam('whereopt'))) {
			$this->view->whereopt			= $this->_request->getParam('whereopt');
		}

		if (!empty($this->_request->getParam('whereval'))) {
			$this->view->whereval			= $this->_request->getParam('whereval');
		}
		$this->view->filter = $filter;

		// $this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');

	}

	public function addbeneficiaryAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$Settings = new Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();
		//Zend_Debug::dump($ccyList);die;

		$this->view->userIdLogin  = $this->_userIdLogin;

		//validasi token -- begin
		/*$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$data2 					= $this->_db->fetchRow($select3);
		$tokenIdUser 			= $data2['TOKEN_ID'];
		$tokenType 				= $data2['TOKEN_TYPE'];
		$usermobilephone 		= $data2['USER_MOBILE_PHONE'];
		$this->view->tokentype 	= $data2['TOKEN_TYPE'];*/

		//added new hard token
		/*$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();

		$challengeCodeSub = substr($challengeCode, 0,2);
		$this->view->challengeCodeReq = $challengeCodeSub;*/
		//validasi token -- end

		if (!$this->_request->isPost() && $this->_getParam('isback')) {
			$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
			$content = $sessionNamespace->content;
			$this->fillParams($content['BENEFICIARY_ACCOUNT'], $content['CURR_CODE'], $content['BENEFICIARY_ALIAS'], $content['BENEFICIARY_EMAIL']);
			$this->view->ACBENEF_NAME = $content['BENEFICIARY_NAME'];
			$this->view->ACBENEF_NEWNAME = $content['BENEFICIARY_NEWNAME'];
			unset($_SESSION['beneficiaryAccountAddEdit']);
		}

		if ($this->_request->isPost()) {
			$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
			$CURR_CODE = $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');
			$BENEFICIARY_NEWNAME = strtoupper($this->_getParam('ACBENEF_NEWNAME'));

			//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
			$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT, "ACCOUNT_NO");
			$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS, "ACCOUNT_ALIAS");
			$ACBENEF_NEWNAME   = $filter->filter($BENEFICIARY_NEWNAME, "ACCOUNT_NEWNAME");
			$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL, "EMAIL");
			$ACBENEF_CCY   = $filter->filter($CURR_CODE, "SELECTION");

			//validasi token -- begin
			/*$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2')	, "challengeCodeReq2");
			 $challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq')	, "challengeCodeReq");


			 $responseCodeReq 	= $filter->filter($this->_request->getParam('responseCodeReq')	, "responseCodeReq");
			 //$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);
			 $this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);*/
			//validasi token -- end

			$paramBene = array(
				"TRANSFER_TYPE"     => 'PB',
				"FROM"      		=> 'B',
				"ACBENEF"     		=> $ACBENEF,
				"ACBENEF_CCY"    	=> $ACBENEF_CCY,
				"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
				"ACBENEF_NEWNAME"   => $ACBENEF_NEWNAME,
				"ACBENEF_EMAIL"    	=> $ACBENEF_EMAIL,
				"_addBeneficiary"   => $this->view->hasPrivilege('BADA'),
				"_beneLinkage"    	=> $privibenelinkage,
			);

			$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			$validateACBENEF->setPrefixMessageAccount('', '');

			$validateACBENEF->check($paramBene);

			// Zend_Debug::dump($paramBene);die;
			if (!$validateACBENEF->isError()) {
				$content = array(
					'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
					'USER_ID_LOGIN' 			=> $this->_userIdLogin,
					'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
					'BENEFICIARY_NEWNAME' 		=> $ACBENEF_NEWNAME,
					'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
					'BENEFICIARY_NAME' 			=> $paramBene['ACBENEF_BANKNAME'],
					'BENEFICIARY_RESIDENT' 		=> $paramBene['ACBENEF_CITIZEN'],
					'BENEFICIARY_CITIZENSHIP' 	=> $paramBene['ACBENEF_NATIONAL'],
					'BENEFICIARY_ID_NUMBER' 	=> $paramBene['ACBENEF_IDNUMBER'],
					'BENEFICIARY_ID_TYPE' 		=> $paramBene['ACBENEF_IDTYPE'],
					'BENEFICIARY_CATEGORY' 		=> $paramBene['ACBENEF_CATEGORY'],
					'CURR_CODE' 				=> $ACBENEF_CCY,
					'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
					//'BENEFICIARY_CITIZENSHIP' => null,
					'BENEFICIARY_ADDRESS'		=> null,
					'BENEFICIARY_TYPE' 			=> 1,
					'ACCOUNT_TYPE' 				=> $paramBene['ACBENEF_TYPE'], //1,
					'SWIFT_CODE' 				=> null,
					'CLR_CODE' 					=> null,
					'_beneLinkage'     			=> $privibenelinkage,
				);

				$content['paramAccount'] = $responseData;
				$content['payment'] = $payment;

				$content['BENEFICIARY_TITLE'] = $paramBene['ACBENEF_TITLE'];
				$content['BENEFICIARY_CUSTID'] = $paramBene['ACBENEF_CUSTID'];
				$content['BENEFICIARY_BANCHCODE'] = $paramBene['ACBENEF_BANCHCODE'];
				$content['BENEFICIARY_BANCHNAME'] = $paramBene['ACBENEF_BANCHNAME'];
				$content['BENEFICIARY_ADDRESS'] = $paramBene['ACBENEF_ADDRESS'];
				$content['BENEFICIARY_PHONEBUSSINESS'] = $paramBene['ACBENEF_PHONEBUSSINESS'];
				$content['BENEFICIARY_PHONEFAX'] = $paramBene['ACBENEF_PHONEFAX'];
				$content['BENEFICIARY_PHONEHOME'] = $paramBene['ACBENEF_PHONEHOME'];
				$content['BENEFICIARY_PRK'] = $paramBene['ACBENEF_PRK'];


				//validasi hard token page 1 -- begin
				/*$message = $Settings->getSetting('token_confirm_message');
					$trans = array("[Response_Code]" => $responseCode);
					$message = strtr($message, $trans);

					if($tokenType == '1'){ //sms token
						if(empty($responseCodeReq)){
							$resultToken = FALSE;
							$this->view->error = true;
							$errMessage = 'Error : Response Token cannot be left blank.';
							$this->view->report_msg = $errMessage;
						}
						else
						{
							$token = new Service_Token(NULL,$this->_userIdLogin);
							$token->setMsisdn($usermobilephone);
							$token->setMessage($message);

							$res = $token->confirm();
							$resultToken = $res['ResponseCode'] == '0000';
						}
					}
					elseif($tokenType == '2'){ //hard token

						if(empty($responseCodeReq)){
							$resultToken = FALSE;
							$this->view->error = true;
							$errMessage = 'Error : Response Token cannot be left blank.';
							$this->view->report_msg = $errMessage;
						}
						else
						{

							$resHard = $HardToken->verifyHardToken($challengeCodeReq2.$challengeCodeReq, $responseCodeReq);
							$resultToken = $resHard['ResponseCode'] == '0000';

							//set user lock token gagal
							$CustUser = new CustomerUser($this->_custId, $this->_userIdLogin);
							if ($resHard['ResponseCode'] != '0000'){
								$tokenFailed = $CustUser->setFailedTokenMustLogout();
								if ($tokenFailed)
									$this->_forward('home');
							}
						}
					}
					elseif($tokenType == '3'){ //mobile token
						$resultToken = TRUE;
					}
					else{$resultToken = TRUE;}*/
				//validasi hard token page 1 -- end
				// print_r($content);die;
				//if($resultToken == TRUE){
				//print_r($content);die;
				$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
				$sessionNamespace->mode = 'Add';
				$sessionNamespace->content = $content;
				$this->_redirect('/predefinedbeneficiary/inhouseacct/confirm');
				//}
				/*else{
						//echo $resultToken."aaa"; die;
						$this->view->ACBENEF = $ACBENEF;
						$this->view->CURR_CODE = $CURR_CODE;
						$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;

						if($resultToken == FALSE){
							if($res['ResponseCode'] == 'XT'){
								$errMessage = 'Error : Service Rejected';
								$this->view->resulttoken = $errMessage;
							}
							else{
								$errMessage = 'Error : Invalid Token';
								$this->view->resulttoken = $errMessage;
							}
						}
					}*/

				/*$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
					$sessionNamespace->mode = 'Add';
					$sessionNamespace->content = $content;
					$this->_redirect('/predefinedbeneficiary/inhouseacct/confirm');*/
			} else {
				$this->view->error = true;
				$this->fillParamsnew($ACBENEF, $ACBENEF_CCY, $ACBENEF_NEWNAME, $ACBENEF_EMAIL);
				$docErr = $validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;
			}
		}
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('BADA', 'Viewing Add Beneficiary Account');
		}
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		// Zend_Debug::dump($content, "content");die;
		$mode = $sessionNamespace->mode;

		$this->view->mode = $mode;
		$this->fillParams($content['BENEFICIARY_ACCOUNT'], $content['CURR_CODE'], $content['BENEFICIARY_ALIAS'], $content['BENEFICIARY_EMAIL']);
		$this->view->ACBENEF_NEWNAME = $content['BENEFICIARY_NEWNAME'];
		$this->view->ACBENEF_NAME = $content['BENEFICIARY_NAME'];
		$this->view->BENEFICIARY_RESIDENT = $content['BENEFICIARY_RESIDENT'];
		if ($content['BENEFICIARY_RESIDENT'] == 'R') {
			$content['BENEFICIARY_RESIDENTTEXT'] = 'Resident';
		} else if ($content['BENEFICIARY_RESIDENT'] == 'NR') {
			$content['BENEFICIARY_RESIDENTTEXT'] = 'Non Resident';
		}
		$this->view->BENEFICIARY_RESIDENTTEXT = $content['BENEFICIARY_RESIDENTTEXT'];
		$this->view->BENEFICIARY_CITIZENSHIP = $content['BENEFICIARY_CITIZENSHIP'];
		if ($content['BENEFICIARY_CITIZENSHIP'] == 'W') {
			$content['BENEFICIARY_CITIZENSHIPTEXT'] = 'WNI';
		} else if ($content['BENEFICIARY_CITIZENSHIP'] == 'N') {
			$content['BENEFICIARY_CITIZENSHIPTEXT'] = 'WNA';
		}
		$this->view->BENEFICIARY_CITIZENSHIPTEXT = $content['BENEFICIARY_CITIZENSHIPTEXT'];
		$this->view->BENEFICIARY_ID_NUMBER = $content['BENEFICIARY_ID_NUMBER'];
		if ($content['BENEFICIARY_ID_TYPE'] == 'KIT') {
			$idtype = 'KITAS';
		} elseif ($content['BENEFICIARY_ID_TYPE'] == 'PAS') {
			$idtype = 'PASPOR';
		} else {
			$idtype = $content['BENEFICIARY_ID_TYPE'];
		}
		$this->view->BENEFICIARY_ID_TYPE = $idtype;
		$this->view->BENEFICIARY_CATEGORY = $content['BENEFICIARY_CATEGORY'];
		if ($content['BENEFICIARY_CATEGORY'] == '1') {
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Individual';
		} else if ($content['BENEFICIARY_CATEGORY'] == '2') {
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Government';
		} else if ($content['BENEFICIARY_CATEGORY'] == '3') {
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Bank';
		} else if ($content['BENEFICIARY_CATEGORY'] == '4') {
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Financial';
		} else if ($content['BENEFICIARY_CATEGORY'] == '5') {
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Company';
		} else if ($content['BENEFICIARY_CATEGORY'] == '6') {
			$content['BENEFICIARY_CATEGORYTEXT'] = 'Other';
		}
		$this->view->BENEFICIARY_CATEGORYTEXT = $content['BENEFICIARY_CATEGORYTEXT'];

		$selectAcctType = $this->_db->select()
			->from('M_PRODUCT_TYPE', array('PRODUCT_NAME'))
			->where('PRODUCT_CODE=' . $this->_db->quote((string)$content['ACCOUNT_TYPE']));
		$this->view->ACBENEF_TYPE = $this->_db->fetchOne($selectAcctType);

		//		$this->view->ACBENEF_TYPE = $content['BENEFICIARY_TYPE'];
		$this->view->ACBENEF_TITLE = $content['BENEFICIARY_TITLE'];
		$this->view->ACBENEF_CUSTID = $content['BENEFICIARY_CUSTID'];
		$this->view->ACBENEF_BRRANCHCODE = $content['BENEFICIARY_BANCHCODE'];
		$this->view->ACBENEF_BRANCHNAME = $content['BENEFICIARY_BANCHNAME'];
		$this->view->ACBENEF_ADDRESS = $content['BENEFICIARY_ADDRESS'];
		$this->view->ACBENEF_PHONEBUSSINESS = $content['BENEFICIARY_PHONEBUSSINESS'];
		$this->view->ACBENEF_PHONEFAX = $content['BENEFICIARY_PHONEFAX'];
		$this->view->ACBENEF_PHONEHOME = $content['BENEFICIARY_PHONEHOME'];
		$this->view->ACBENEF_PRK = $content['BENEFICIARY_PRK'];

		if ($this->_request->isPost()) {
			if ($this->_getParam('back') == 'Back') {
				if ($mode == 'Add') {
					$this->_redirect('/predefinedbeneficiary/inhouseacct/addbeneficiary/isback/1');
				} elseif ($mode == 'Edit') {
					$this->_redirect('/predefinedbeneficiary/inhouseacct/editbeneficiary/benef_id/' . $content['BENEFICIARY_ID'] . '/isback/1');
				}
			} else {
				try {
					// if($this->_getParam('changename')=='1'){
					// 	$content['CHANGE_NAME'] = '1';
					// }else{
					// 	$content['CHANGE_NAME'] = '0';
					// }

					$Beneficiary = new Beneficiary();
					$this->_db->beginTransaction();
					if ($mode == 'Add') {
						$add = $Beneficiary->add($content);
						Application_Helper_General::writeLog('BADA', 'Add Beneficiary Account ' . $content['BENEFICIARY_ACCOUNT']);
					} elseif ($mode == 'Edit') {
						$update = $Beneficiary->update($content);
						Application_Helper_General::writeLog('BEDA', 'Edit Beneficiary Account ' . $content['BENEFICIARY_ACCOUNT']);
					}
					$this->_db->commit();
					$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
					$this->_redirect('/notification/submited');
				} catch (Exception $e) {
					$this->_db->rollBack();
				}
			}
		}
	}



	public function importbeneficiaryAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		if ($this->_request->isPost()) {
			$this->_destinationUploadDir = UPLOAD_PATH . '/Frontend/document/temp/';
			//die($this->_destinationUploadDir);
			$adapter = new Zend_File_Transfer_Adapter_Http();
			$adapter->setDestination($this->_destinationUploadDir);
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
			$extensionValidator->setMessage(
				'Error: Extension file must be *.csv'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				$this->getErrorRemark('13')
			);

			$adapter->setValidators(array(
				$extensionValidator,
				$sizeValidator,
			));

			if ($adapter->isValid()) {
				$sourceFileName = substr_replace(basename($adapter->getFileName()), '', 100);
				$newFileName = $adapter->getFileName() . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

				$adapter->addFilter('Rename', $newFileName);

				if ($adapter->receive()) {
					//PARSING CSV HERE
					$csvData = $this->parseCSV($newFileName);
					//after parse delete document temporary
					@unlink($newFileName);
					//end

					$totalRecords = count($csvData);
					if ($totalRecords) {
						unset($csvData[0]);
						$totalRecords = count($csvData);
					}

					if ($totalRecords) {
						$rowNum = 0;
						$totalSucces = 0;
						$totalFailed = 0;

						foreach ($csvData as $row) {
							$rowNum++;
							$benefAcct = trim($row[0]);
							$ccy = strtoupper(trim($row[1]));
							$alias = trim($row[2]);
							$email = trim($row[3]);

							$fullDesc = array(
								'BENEFICIARY_ACCOUNT' => $benefAcct,
								'CURR_CODE' => $ccy,
								'BENEFICIARY_ALIAS' => $alias,
								'BENEFICIARY_EMAIL' => $email
							);

							//validate field
							$dataValidate =  array(
								'benef_acct' => $benefAcct,
								'ccy' => $ccy,
								'alias' => $alias,
								'email' => $email
							);

							//validate format from csv document
							$resultValidate = $this->validateField($dataValidate);

							if ($resultValidate === true) {
								$totalSucces++;

								$benefData = array(
									'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
									'USER_ID_LOGIN' 			=> $this->_userIdLogin,
									'BENEFICIARY_ALIAS' 		=> $alias,
									'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
									'BENEFICIARY_NAME' 			=> '',
									'CURR_CODE' 				=> $ccy,
									'BENEFICIARY_EMAIL' 		=> $email,
									'BENEFICIARY_CITIZENSHIP' 	=> null,
									'BENEFICIARY_ADDRESS'		=> null,
									'BENEFICIARY_TYPE' 			=> 1,
									'SWIFT_CODE' 				=> null,
									'CLR_CODE' 					=> null
								);

								try {
									$this->_db->beginTransaction();
									//Zend_Debug::Dump($benefData);die;
									$Beneficiary = new Beneficiary();
									$add = $Beneficiary->add($content);
									$this->_db->commit();
								} catch (Exception $e) {
									$this->_db->rollBack();
									$error_remark = 'Error: Database Process Failed';
									Application_Helper_GeneralLog::technicalLog($e);
								}
							} else // jika validasi salah
							{
								$errorArr[] = $resultValidate->getMessages();
							}
						}
					} else {
					}
				}
			} else {
				$this->view->error = true;
				$errors = $this->displayError($adapter->getMessages());
				$this->view->report_msg = $errors;
			}
		}
	}

	public function editbeneficiaryAction()
	{
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();

		$this->view->isEdited = true;

		if (!$this->_request->isPost() && $this->_getParam('isback')) {
			$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
			$content = $sessionNamespace->content;
			$this->fillParams($content['BENEFICIARY_ACCOUNT'], $content['CURR_CODE'], $content['BENEFICIARY_ALIAS'], $content['BENEFICIARY_EMAIL'], $content['BENEFICIARY_ID']);
			$this->view->ACBENEF_NAME = $content['BENEFICIARY_NAME'];
			unset($_SESSION['beneficiaryAccountAddEdit']);
		} else if (!$this->_request->isPost()) {
			$benef_id = $this->_getParam('benef_id');
			$benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;
			if ($benef_id) {
				$resultdata = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID=?", $benef_id)
				);
				if ($resultdata) {
					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];
				}

				$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
				$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
				$select->where("B.BENEFICIARY_ID = ?", (string) $benef_id);

				$isEdited = $select->query()->fetchAll();

				if ($isEdited)
					$this->view->isEdited = true;
				else {
					$this->view->isEdited = false;
					$this->view->error = true;
					$this->view->report_msg = 'Error: You have no right to edit beneficiary.';
				}
			} else {
				$error_remark = 'Beneficiary ID does not exist.';

				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
			}
		} else {
			$ACBENEF_ID =  $this->_getParam('BENEFICIARY_ID');
			$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
			$CURR_CODE = $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');

			//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
			$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT, "ACCOUNT_NO");
			$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS, "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL, "EMAIL");
			$ACBENEF_CCY   = $filter->filter($CURR_CODE, "SELECTION");

			$paramBene = array(
				"TRANSFER_TYPE"     => 'PB',
				"FROM"      		=> 'B',
				"ACBENEF_ID"     	=> $ACBENEF_ID,
				"ACBENEF"     		=> $ACBENEF,
				"ACBENEF_CCY"    	=> $ACBENEF_CCY,
				"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
				"ACBENEF_EMAIL"   	=> $ACBENEF_EMAIL,
				"_editBeneficiary"  => $this->view->hasPrivilege('BEDA'),
				"_beneLinkage"    	=> $privibenelinkage,
			);

			$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			$validateACBENEF->check($paramBene);
			$currCode 	= Application_Helper_General::getCurrNum($ACBENEF_CCY);
			$dataAcct =  new Service_Account($ACBENEF, $currCode);
			$dataInquiry = $dataAcct->accountInquiry('AB', FALSE);

			$resAcct = $dataInquiry['ResponseCode'];
			$descAcct = $dataInquiry['ResponseDesc'];

			if (!$validateACBENEF->isError() && $resAcct == '00') {
				$content = array(
					'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
					'USER_ID_LOGIN' 			=> $this->_userIdLogin,
					'BENEFICIARY_ID' 			=> $ACBENEF_ID,
					'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
					'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
					'BENEFICIARY_NAME' 			=> $paramBene['ACBENEF_BANKNAME'],
					'CURR_CODE' 				=> $ACBENEF_CCY,
					'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
					'BENEFICIARY_CITIZENSHIP' 	=> null,
					'BENEFICIARY_ADDRESS'		=> null,
					'BENEFICIARY_TYPE' 			=> 1,
					'SWIFT_CODE' 				=> null,
					'CLR_CODE' 					=> null,
					'_beneLinkage'     			=> $privibenelinkage,
				);
				//-----update benef--------------
				$content['paramAccount'] = $responseData;
				$content['payment'] = $payment;

				$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
				$sessionNamespace->mode = 'Edit';
				$sessionNamespace->content = $content;
				$this->_redirect('/predefinedbeneficiary/inhouseacct/confirm');
			} else {
				$this->view->error = true;
				$this->fillParams($ACBENEF, $ACBENEF_CCY, $ACBENEF_ALIAS, $ACBENEF_EMAIL, $ACBENEF_ID);
				if ($resAcct == '00') {
					$docErr = 'Error: ' . $validateACBENEF->getErrorMsg();
				} else {
					$docErr = 'Error: ' . $descAcct;
				}

				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;
			}
		}
	}

	public function editemailAction()
	{
		$benef_id_decrypted = $this->_getParam("benef_id");

		// decrypt benef id -------------------------------------------------------
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER 	= urldecode($benef_id_decrypted);

		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
		$benef_id_decrypted = $BG_NUMBER;

		// ------------------------------------------------------------------------------

		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		if (!$this->_request->isPost()) {
			$benef_id = $benef_id_decrypted;
			$benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;
			if ($benef_id) {
				$resultdata = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID=?", $benef_id)
				);
				// 			  print_r($resultdata);die;
				if ($resultdata) {
					$benef_nationality = '';
					if ($resultdata['BENEFICIARY_CITIZENSHIP'] == 'W') $benef_nationality = 'WNI';
					else if ($resultdata['BENEFICIARY_CITIZENSHIP'] == 'N') $benef_nationality = 'WNA';

					$citizenship = '';
					if ($resultdata['BENEFICIARY_RESIDENT'] == 'NR') $citizenship = 'Non Resident';
					else if ($resultdata['BENEFICIARY_RESIDENT'] == 'R') $citizenship = 'Resident';

					$settings 			= new Application_Settings();


					if (!empty($resultdata['BENEFICIARY_CATEGORY'])) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_CATEGORY_POST = $lldCategoryArr[$resultdata['BENEFICIARY_CATEGORY']];
					}

					if (!empty($resultdata['BENEFICIARY_ID_TYPE'])) {
						$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
						$LLD_BENEIDENTIF_POST = $lldBeneIdentifArr[$resultdata['BENEFICIARY_ID_TYPE']];
					}

					$this->view->BENEFICIARY_ID = $this->_getParam("benef_id");
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BENEFICIARY_CITIZENSHIP = $citizenship;
					$this->view->BENEFICIARY_NAME	= $resultdata['BENEFICIARY_NAME'];
					$this->view->BENEFICIARY_NATIONALITY	= $benef_nationality;
					$this->view->BENEFICIARY_CATEGORY = $LLD_CATEGORY_POST;
					$this->view->BENEFICIARY_ID_TYPE = $LLD_BENEIDENTIF_POST;
					$this->view->BENEFICIARY_ID_NUMBER = $resultdata['BENEFICIARY_ID_NUMBER'];
				}
			} else {
				$error_remark = 'Beneficiary ID does not exist.';

				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
			}
		}
		if ($this->_request->isPost()) {
			$get_request = $this->_request->getParams();
			$get_id = $get_request["BENEFICIARY_ID"];

			$get_id = urldecode($get_id);

			$get_id = $AESMYSQL->decrypt($get_id, $password);
			$get_request["BENEFICIARY_ID"] = $get_id;


			$filters = array(
				'BENEFICIARY_ID' => array('StringTrim', 'StripTags'),
				'ACBENEF_EMAIL' => array('StringTrim', 'StripTags')
			);

			$validators = array(
				'BENEFICIARY_ID' => array(
					'NotEmpty',
					'Digits',
					'messages' => array(
						'Error: Beneficiary ID cannot be left blank.',
						'Error: Wrong Format Beneficiary ID.'
					)
				),
				'ACBENEF_EMAIL' => array(	//new SGO_Validate_EmailAddress(),
					'allowEmpty' => true,
					//'messages' => array(
					//	'Error: Wrong Format Email Address. Please correct it.')
				)
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $get_request, $this->_optionsValidator);
			if ($zf_filter_input->isValid()) {
				$error = false;

				if (trim($zf_filter_input->ACBENEF_EMAIL) != "") {
					$arr_email = explode(";", $zf_filter_input->ACBENEF_EMAIL);
					foreach ($arr_email as $value) {
						if (Zend_Validate::is(trim($value), 'EmailAddress') == false) {
							$error = true;
							break;
						}
					}
				}

				if (!$error) {
					try {
						//-----edit email--------------
						$this->_db->beginTransaction();

						$Beneficiary = new Beneficiary();
						$Beneficiary->editEmail($zf_filter_input->BENEFICIARY_ID, $zf_filter_input->ACBENEF_EMAIL);

						$beneAcct = $this->_db->fetchOne('SELECT BENEFICIARY_ACCOUNT FROM M_BENEFICIARY WHERE BENEFICIARY_ID=' . $zf_filter_input->BENEFICIARY_ID);

						Application_Helper_General::writeLog('BEDA', 'Edit Email Beneficiary Account ' . $beneAcct);

						$this->_db->commit();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						//$this->view->success = true;
						//$msg = $this->getErrorRemark('00','Edit Email');
						//$this->view->report_msg = $msg;
						$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
						$this->_redirect('/notification/success');
					} catch (Exception $e) {
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				} else {
					$this->view->error = true;

					$resultdata = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('M_BENEFICIARY'))
							->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
					);

					if ($resultdata) {
						$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					}

					$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
					$docErr = $this->language->_('Error: Invalid Format - Email Address.') . '';
					$this->view->report_msg = $docErr;
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
				}
			} else {
				$this->view->error = true;
				$resultdata = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
				);
				if ($resultdata) {
					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
				}
				$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}
		}
	}

	public function deleteAction()
	{
		// echo "<pre>";
		// print_r($this->_request->getParams());die;
		$this->_helper->layout()->setLayout('newlayout');
		if (!$this->_request->isPost()) {
			$benef_id = $this->_getParam('benef_id');
			// print_r($benef_id);die;
			$checkmulti = explode(',', $benef_id);
			// var_dump($checkmulti);die;
			if (empty($checkmulti[1])) {
				$this->view->multiview = false;
				$benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;

				if ($benef_id) {
					$resultdata = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('M_BENEFICIARY'))
							->where("BENEFICIARY_ID=?", $benef_id)
					);
					if ($resultdata) {
						$benef_nationality = '';
						if ($resultdata['BENEFICIARY_CITIZENSHIP'] == 'W') $benef_nationality = 'WNI';
						else if ($resultdata['BENEFICIARY_CITIZENSHIP'] == 'N') $benef_nationality = 'WNA';

						$citizenship = '';
						if ($resultdata['BENEFICIARY_RESIDENT'] == 'NR') $citizenship = 'Non Resident';
						else if ($resultdata['BENEFICIARY_RESIDENT'] == 'R') $citizenship = 'Resident';

						$settings 			= new Application_Settings();


						if (!empty($resultdata['BENEFICIARY_CATEGORY'])) {
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_CATEGORY_POST = $lldCategoryArr[$resultdata['BENEFICIARY_CATEGORY']];
						}

						if (!empty($resultdata['BENEFICIARY_ID_TYPE'])) {
							$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
							$LLD_BENEIDENTIF_POST = $lldBeneIdentifArr[$resultdata['BENEFICIARY_ID_TYPE']];
						}

						$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_NAME	= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
						$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];
						$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];
						$this->view->BENEFICIARY_CITIZENSHIP = $citizenship;
						$this->view->BENEFICIARY_NATIONALITY	= $benef_nationality;
						$this->view->BENEFICIARY_CATEGORY = $LLD_CATEGORY_POST;
						$this->view->BENEFICIARY_ID_TYPE = $LLD_BENEIDENTIF_POST;
						$this->view->BENEFICIARY_ID_NUMBER = $resultdata['BENEFICIARY_ID_NUMBER'];
					}
				} else {
					$error_remark = 'Error: Beneficiary ID cannot be left blank.';

					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				}
			} else {
				$this->view->multiview = true;

				$fields = array(
					/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
					'benef_acct'  => array(
						'field' => 'BENEFICIARY_ACCOUNT',
						'label' => $this->language->_('Beneficiary Account'),
						'sortable' => true
					),
					'benef_name'  => array(
						'field' => 'BENEFICIARY_NAME',
						'label' => $this->language->_('Beneficiary Account Name'),
						'sortable' => true
					),
					'email'  => array(
						'field' => 'BENEFICIARY_EMAIL',
						'label' => $this->language->_('Email Address'),
						'sortable' => true
					),
					'ccy'   => array(
						'field'    => 'CURR_CODE',
						'label'    => $this->language->_('CCY'),
						'sortable' => true
					),
					// 'favorite'   => array('field'    => 'ISFAVORITE',
					// 					  'label'    => $this->language->_('Favorite'),
					// 					  'sortable' => true),
					// 'checked'   => array('field'    => 'BENEFICIARY_BANKSTATUS_disp',
					// 					  'label'    => $this->language->_('Checked by Bank'),
					// 					  'sortable' => true),

					'date'   => array(
						'field'    => 'BENEFICIARY_CREATED',
						'label'    => $this->language->_('Created Date'),
						'sortable' => true
					),

					'status'   => array(
						'field'    => 'BENEFICIARY_ISAPPROVE',
						'label'    => $this->language->_('Status'),
						'sortable' => true
					)

					// 'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
					// 					  'label'    => $this->language->_('Delete'),
					// 					  'sortable' => false)
				);
				$this->view->fields = $fields;

				$select = $this->_db->select()
					->from(array('M_BENEFICIARY'))
					->where("BENEFICIARY_ID in (?)", $checkmulti);
				$this->paging($select);


				$resultdata = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID in (?)", $checkmulti)
				);
				// echo $resultdata;die;

				// print_r($resultdata);die;
				if ($resultdata) {
					$benef_nationality = '';
					if ($resultdata['BENEFICIARY_CITIZENSHIP'] == 'W') $benef_nationality = 'WNI';
					else if ($resultdata['BENEFICIARY_CITIZENSHIP'] == 'N') $benef_nationality = 'WNA';

					$citizenship = '';
					if ($resultdata['BENEFICIARY_RESIDENT'] == 'NR') $citizenship = 'Non Resident';
					else if ($resultdata['BENEFICIARY_RESIDENT'] == 'R') $citizenship = 'Resident';

					$settings 			= new Application_Settings();


					if (!empty($resultdata['BENEFICIARY_CATEGORY'])) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_CATEGORY_POST = $lldCategoryArr[$resultdata['BENEFICIARY_CATEGORY']];
					}

					if (!empty($resultdata['BENEFICIARY_ID_TYPE'])) {
						$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
						$LLD_BENEIDENTIF_POST = $lldBeneIdentifArr[$resultdata['BENEFICIARY_ID_TYPE']];
					}

					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];
					$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];
					$this->view->BENEFICIARY_CITIZENSHIP = $citizenship;
					$this->view->BENEFICIARY_NATIONALITY	= $benef_nationality;
					$this->view->BENEFICIARY_CATEGORY = $LLD_CATEGORY_POST;
					$this->view->BENEFICIARY_ID_TYPE = $LLD_BENEIDENTIF_POST;
					$this->view->BENEFICIARY_ID_NUMBER = $resultdata['BENEFICIARY_ID_NUMBER'];
				}
			}
		} else {

			//	echo "<pre>";
			// print_r($this->_getParam('BENEFICIARY_ID')[1]);
			// var_dump($this->_getParam('BENEFICIARY_ID'));
			// var_dump($this->_getParam('STATUS'));
			// die;

			if ($this->_getParam('BENEFICIARY_ID')[1] == 1) {


				$filters = array(
					'BENEFICIARY_ID' => array('StringTrim', 'StripTags'),
					'STATUS' => array('StringTrim', 'StripTags')
				);

				$validators = array(
					'BENEFICIARY_ID' => array(
						'NotEmpty',
						'Digits',
						'messages' => array(
							'Error: Beneficiary ID cannot be left blank.',
							'Error: Wrong Format Beneficiary ID.'
						)
					),
					'STATUS' 		=> array(
						'NotEmpty',
						'Digits',
						'messages' => array(
							'Error: Status cannot be left blank.',
							'Error: Wrong Format Status.'
						)
					)
				);

				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
				if ($zf_filter_input->isValid()) {
					try {

						//-----delete--------------
						$this->_db->beginTransaction();
						$Beneficiary = new Beneficiary();
						if ($zf_filter_input->STATUS)
							$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
						else
							$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
						$this->_db->commit();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
						$this->_redirect('/notification/submited');
					} catch (Exception $e) {
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				} else {

					$this->view->error = true;
					$resultdata = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('M_BENEFICIARY'))
							->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
					);
					if ($resultdata) {
						$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_NAME	= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
						$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];
						$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];
					}
					$docErr = $this->displayError($zf_filter_input->getMessages());
					$this->view->report_msg = $docErr;
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
				}
			} else {
				// die('here');
				try {
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();

					$id = $this->_getParam('BENEFICIARY_ID');

					if (count($id) == 1) {
						$Beneficiary->suggestDelete($id);
					} else {
						foreach ($this->_getParam('BENEFICIARY_ID') as $key => $value) {
							if ($this->_getParam('STATUS')[$key])
								$Beneficiary->suggestDelete($value);
							else
								$Beneficiary->delete($value);
						}
					}

					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
					$this->_redirect('/notification/submited');
				} catch (Exception $e) {
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}
			}
		}

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}
		Application_Helper_General::writeLog('BADA', 'Delete Beneficiary Account ' . $benef_id);
	}

	private function fillParam($zf_filter_input)
	{
		if (isset($zf_filter_input->BENEFICIARY_ID)) $this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID');
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS');
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
	}

	private function fillParams($ACBENEF, $ACBENEF_CCY, $ACBENEF_ALIAS, $ACBENEF_EMAIL, $BENEFICIARY_ID = null)
	{
		if ($BENEFICIARY_ID) $this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
	}

	private function fillParamsnew($ACBENEF, $ACBENEF_CCY, $ACBENEF_NEWNAME, $ACBENEF_EMAIL, $BENEFICIARY_ID = null)
	{
		if ($BENEFICIARY_ID) $this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		$this->view->ACBENEF_NEWNAME = $ACBENEF_NEWNAME;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
	}

	private function validateField($data)
	{

		$filters = array(
			'benef_acct' 	=> array('StripTags', 'StringTrim'),
			'ccy' 			=> array('StripTags', 'StringTrim'),
			'alias'			=> array('StripTags', 'StringTrim'),
			'email' 		=> array('StripTags', 'StringTrim')
		);

		$validators = array(
			'benef_acct' => array(
				'NotEmpty',
				'Digits',
				new Zend_Validate_StringLength(array('max' => 10, 'min' => 10)),
				'messages' => array(
					'Beneficiary Account cannot be left blank. Please correct it.',
					'Beneficiary Account must be numbers. Please correct it.',
					'Beneficiary Account length must be 10 digits. Please correct it.',
				)
			),

			'alias' => array(
				'NotEmpty',
				new Zend_Validate_StringLength(array('max' => 35)),
				'messages' => array(
					'Alias Name cannot be left blank. Please correct it.',
					'Alias Name length should be less than 35 characters. Please correct it.'
				)
			),
			'ccy' => 	array(
				'NotEmpty',
				//new Zend_Validate_Alnum(),
				//new Zend_Validate_StringLength(array('min'=>3,'max'=>3)),
				array('InArray', array('haystack' => $this->_listCCYValidate)),
				'messages' => array(
					'Currency cannot be left blank. Please correct it.',
					//'04',
					//'04',
					'Currency is not on the database.'
				)
			),

			'email' => array(
				new SGO_Validate_EmailAddress(),
				'allowEmpty' => true,
				'messages' => array(
					'Wrong Format Email Address. Please correct it.'
				),
			),
		);

		$zf_filter_input = new Zend_Filter_Input($filters, $validators, $data);
		if ($zf_filter_input->isValid()) {
			return true;
		} else {
			return $zf_filter_input;
		}
	}

	public function setfavoriteAction()
	{

		$fav = $this->_request->getParam('benef_id');
		$flag = false;
		foreach ($fav as $val) {
			if ($val == 1) {
				$flag = true;
				break;
			}
		}

		// if($flag)
		// {
		// echo "<pre>";
		// print_r($this->_request->getParams());
		// print_r($fav);die;
		foreach ($fav as $key => $value) {
			$Beneficiary = new Beneficiary();
			$Beneficiary->setFavorite($value, '1');
		}
		$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
		$this->_redirect('/notification/success');
		// }
		// else
		// {
		// $error_remark = 'Error: Please check selection.';
		// $this->_helper->getHelper('FlashMessenger')->addMessage('F');
		// $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
		// $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
		// }

	}

	public function suggestdeleteAction()
	{
		$benef_id = $this->_getParam('benef_id');
		$benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;

		if ($benef_id) {
			$Beneficiary = new Beneficiary();
			$Beneficiary->suggestDelete($benef_id);
		}
		$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
		$this->_redirect('/notification/success');
	}
}
