ddsmoothmenu.init({
	mainmenuid: "menu_left", //Menu DIV id
	orientation: 'v', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu-v', //class added to menu's outer DIV
	customtheme: ["#0c67a0","#FFBE00"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
});
