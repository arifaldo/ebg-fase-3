<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class userdailylimit_DeleteController extends userdailylimit_Model_Userdailylimit 
{

  public function indexAction() 
  {
   
  	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));
    
     $ccy_id = strtoupper($AESMYSQL->decrypt($this->_getParam('ccy_id'), $password));
     $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
     
     $error_remark = null;
    
    
  
     
    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_STATUS','CUST_ID'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $cust_data = $this->_db->fetchRow($select);
      if(!$cust_data['CUST_ID'])$cust_id = null;
    }
    
    
    if(!$cust_id)
    { 
      $error_remark = $this->getErrorRemark('22','Customer ID');
      //insert log
      try 
      {
	    $this->_db->beginTransaction();
	    $this->backendLog(strtoupper($this->_changeType['code']['delete']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
        $this->_db->commit();
	  }
	  catch(Exception $e) 
	  {
	    $this->_db->rollBack();
  	    SGO_Helper_GeneralLog::technicalLog($e);
	  }
	  
	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);

      // require_once 'Crypt/AESMYSQL.php';
      // $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_custid = $AESMYSQL->encrypt($cust_id, $rand);
      $enccustid = urlencode($encrypted_custid);

      $this->setbackURL('/customer/view/index/cust_id/'.$enccustid);

      // $enccustid = urlencode($encrypted_custid);

      // $buttonDetail = $this->url(array('module'=>$this->modulename,'controller'=>'view','action'=>'index','cust_id'=>$enccustid),null,true);

      // $encrypted_userid = $AESMYSQL->encrypt($this->user_id, $rand);
      // $encuserid = urlencode($encrypted_userid);

      // $encrypted_ccy = $AESMYSQL->encrypt($this->ccy_id, $rand);
      // $encccy= urlencode($encrypted_ccy);

      $this->_redirect($this->view->backURL);
    }
    
   
    
    
    
    if($user_id && $ccy_id)
    {
      $select = $this->_db->select()
                             ->from(array('d'=>'M_DAILYLIMIT'))
                             ->join(array('u'=>'M_USER'),'d.USER_LOGIN=u.USER_ID and d.CUST_ID=u.CUST_ID',array('USER_FULLNAME'))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id))
                             ->where('d.DAILYLIMIT_STATUS!=3');
      $dailyLimitData = $this->_db->fetchRow($select);
      
     
      if($dailyLimitData['DAILYLIMIT'])
      {
      	
      	  $select = $this->_db->select()
                               ->from('TEMP_DAILYLIMIT',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                               ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id));
          $result = $this->_db->fetchOne($select);
          
          if(!$result)
          {
             $info = 'Cust ID = '.$cust_id.', User Id = '.$user_id.', Ccy Id = '.$ccy_id;
             
             $dailyLimitData['DAILYLIMIT_STATUS'] = 3;
             $dailyLimitData['SUGGESTED']    = new Zend_Db_Expr('now()');
		     $dailyLimitData['SUGGESTEDBY']  = $this->_userIdLogin;
             
             try 
             {
		        $this->_db->beginTransaction();
		  	    $change_id = $this->suggestionWaitingApproval('User Daily Limit',$info,strtoupper($this->_changeType['code']['delete']),null,'M_DAILYLIMIT','TEMP_DAILYLIMIT',$user_id,$dailyLimitData['USER_FULLNAME'],$cust_id);
			    $this->insertTempDailyLimit($change_id,$dailyLimitData);
			    
			    //log CRUD
		        Application_Helper_General::writeLog('DLUD','User Daily Limit has been Updated (delete), User Id : '.$user_id. ' CCY : '.$dailyLimitData['CCY_ID'].' Change id : '.$change_id);
			    
			    $this->_db->commit();
			    
			    $this->_redirect('/notification/submited/index');
			    //$this->_redirect('/notification/success/index');
		      }
		      catch(Exception $e) 
		      {
			    $this->_db->rollBack();
			    $error_remark = $this->getErrorRemark('82');
			    SGO_Helper_GeneralLog::technicalLog($e);
		      }
          }
          else
          { 
              $error_remark = $this->getErrorRemark('03','Customer Account'); 
          }
      	}
      	else
      	{
      	    $error_remark = $this->getErrorRemark('73'); 
      	}
     
    }
    
   
    
    if(!$user_id)
    {
      $error_remark = $this->getErrorRemark('22','Customer Account');
      $fulldesc = 'CUST_ID:'.$cust_id;
      $key_value = null;
    }
    else
    {
      $fulldesc = null;
      $key_value = $cust_id.','.$acct_no;
    }
    
     
    
    
    if($error_remark){ $class = 'F'; $msg = $error_remark; }
    else
    {
      $msg = $this->getErrorRemark('00','Customer Account',$acct_no);
      $class = 'S';
    }
    
    //insert log
    try 
    {
	  $this->_db->beginTransaction();
	  
	  Application_Helper_General::writeLog('DLUD','');
	  
      $this->_db->commit();
	}
	catch(Exception $e) 
	{
	  $this->_db->rollBack();
  	  SGO_Helper_GeneralLog::technicalLog($e);
	}
	
	
	
	$this->_helper->getHelper('FlashMessenger')->addMessage($class);
    $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
   // $this->_redirect($this->_backURL);
    
     
    
  }
}
