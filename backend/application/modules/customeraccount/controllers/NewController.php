<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';
//require_once ("Service/Account.php");

class customeraccount_NewController extends customeraccount_Model_Customeraccount
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password));
		$cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;
		$error_remark = null;

		$getCcy = $this->getCcy();
		$this->view->CCYData = $getCcy;
		$this->view->custAcct_msg  = array();

		$acct_no = $this->_getParam('acct_no');

		if ($cust_id) {
			$this->view->cust_id = $cust_id;
			$custData = $this->_db->select()
				->from('M_CUSTOMER', array('CUST_ID', 'CUST_STATUS', 'CUST_CIF'))
				->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
				//->where('CUST_STATUS=1');
				->query()->fetch();

			if (!$custData['CUST_ID']) $cust_id = null;
			$cust_status 	= $custData['CUST_STATUS'];
			$cust_cif 		= $custData['CUST_CIF'];
		}

		if (!$cust_id) {
			$error_remark = 'Invalid Cust ID';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			// $this->_redirect($this->_backURL);
		}

		if ($this->_request->isPost()) {

			$exclude_fgroup_id = '(UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id);
			$exclude_fgroup_id .= ' OR UPPER(CUST_ID)=' . $this->_db->quote('BANK');
			$exclude_fgroup_id .= ') AND UPPER(FGROUP_STATUS)=' . $this->_db->quote(strtoupper($this->_masterStatus['code']['active']));

			$filters = array(
				'acct_no'        => array('StripTags', 'StringTrim'),
				// 'acct_alias'        => array('StripTags','StringTrim'),
				// 'ccy_id'         => array('StripTags','StringTrim','StringToUpper'),
				'acct_email'     => array('StripTags', 'StringTrim'),
				// 'cust_id'        => array('StripTags','StringTrim','StringToUpper'),
			);

			$TokenError = array();

			$validators =  array(
				// 'cust_id'       => array(),
				// 'acct_alias'       => array('NotEmpty',
				// 	'messages' => array(
				// 		$this->language->_('Can not be empty'),
				// 	)),
				'acct_no'       => array(
					'NotEmpty',
					'Digits',
					array('StringLength', array('min' => 16)),
					array('StringLength', array('max' => 16)),
					// array('Db_NoRecordExists', array('table' => 'M_CUSTOMER_ACCT', 'field' => 'ACCT_NO')),
					// array('Db_NoRecordExists', array('table' => 'M_CUSTOMER_ACCT', 'field' => 'ACCT_NO', 'exclude' => 'ACCT_STATUS!=3')),
					// array('Db_NoRecordExists', array('table' => 'TEMP_CUSTOMER_ACCT', 'field' => 'ACCT_NO')),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Account Number must be numeric'),
						$this->language->_('Minimum length of Account Number is 16 digits'),
						$this->language->_('Maximum length of Account Number is 16 digits'),
						// $this->language->_('Account already assigned. Please use another'),
						// $this->language->_('Account Number') . ' ' . $acct_no . ' ' . $this->language->_('already suggested! Please Approve or Reject previous suggestion first!'),
					)
				),

				// 'ccy_id'         => array('NotEmpty',
				// 	'messages' => array(
				// 		$this->language->_('Can not be empty'),
				// 	)
				// ),


				'acct_email'     => array( //new Application_Validate_EmailAddress(),
					array('StringLength', array('max' => 128)),
					'NotEmpty' => true,
					'messages' => array(
						//'Invalid email format',
						$this->language->_('Email lenght cannot be more than 128'),
						$this->language->_('Can not be empty'),
					)
				),
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getPost(), $this->_optionsValidator);

			// var_dump($zf_filter_input);
			// die();

			//--------------validasi tambahan-------------------
			// validasi account dan cust id apakah didelete atau tidak
			$cust_id_acc_flag = true;

			/* if($zf_filter_input->acct_no)
	      if(count($accData) > 0)
	      { 
	         $accStatus = $accData['ACCT_STATUS'];
	         $custDataBasedAcc = $this->getCustData($accData['CUST_ID']);
	         
	         if($custDataBasedAcc['CUST_STATUS'] == 3) $cust_id_acc_flag = false;
	         else $cust_id_acc_flag = true;
	      }
	      else $cust_id_acc_flag = true;*/
			$cek_account = true;
			$cek_balance = true;

			$aro = true;
			$specialRate = false;
			$status = true;
			$hold = false;

			$acct_no = $this->_request->getPost('acct_no');

			if ($acct_no) {
				//integrate ke core
				//  var_dump($zf_filter_input->acctno);die;
				$account = new Service_Account($acct_no, null, null, null);
				//var_dump($val['TRANSFER_TYPE']);
				//var_dump($val['TRANSACTION_ID']);

				$result = $account->inquiryAccontInfo();

				//echo $result['balance_active'];
				// if ($result['balance_active'] == "0" || $result['balance'] == "0") {
				if ($account->inquiryAccountBalance()["response_code"] == "0000") {
					$saveResult = $account->inquiryAccountBalance();
					if ($account->inquiryAccountBalance()["status"] != 1 && $account->inquiryAccountBalance()["status"] != 4) {
						$status = false;
						$cust_status = 0;
						$cek_balance = false;

						//$this->view->message = 'Error : Account number tidak aktif';
					}

					$accountType = $saveResult['account_type'];
					$productType = $saveResult['product_type'];
				}
				// else if ($result['response_code'] != "0000") {
				// 	$cust_status = 0;
				// 	$cek_account = false;
				// }
				else {
					if ($result['response_desc'] == 'Success') //00 = success
					{
						$result = $account->inquiryDeposito();

						if ($result['response_desc'] != 'Success') {
							$cust_id_acc_flag = false;
							$cust_id_acc_type = 1;
							$cek_account = false;
						} else {
							if ($result["status"] != 1 && $result["status"] != 4) {
								$status = false;
								$cust_status = 0;
								$cek_balance = false;
							}

							$accountType = $result['account_type'];
							$productType = $result['product_type'];

							$aro = ($result['aro_status'] == 'Y') ? true : false;
							$specialRate = ($result['varian_rate'] != '0') ? true : false;
							$hold = (strtolower($result['hold_description']) == 'y') ? true : false;

							$select = $this->_db->select()
								->from('M_PRODUCT_TYPE', array('PRODUCT_NAME'))
								->where('PRODUCT_CODE= ?', $result['account_type']);
							$checkproduct = $this->_db->fetchOne($select);
							//var_dump($checkproduct);die;
							if (!empty($checkproduct)) {
								$accData  = $this->getAccStatus($acct_no);

								if (count($accData) > 0) {
									$accStatus = $accData['ACCT_STATUS'];
									$custDataBasedAcc = $this->getCustData($accData['CUST_ID']);
								}
								$accountType = $result['account_type'];
							} else {
								$cust_id_acc_flag = false;
								$cust_id_acc_type = 1;
							}
						}
					} else {
						$cust_status = 1;
						$cek_account = false;
						if ($account->inquiryAccountBalance()["response_code"] == "0000") {
							$result["account_type"] = $account->inquiryAccountBalance()["account_type"];
							if ($account->inquiryAccountBalance()['balance_active'] == "0" || $account->inquiryAccountBalance()['balance'] == "0") {
								$cek_balance = false;
							}
						} else {
							$result["account_type"] = $account->inquiryDeposito()["account_type"];
							if ($account->inquiryDeposito()['balance_active'] == "0" || $account->inquiryDeposito()['balance'] == "0") {
								$cek_balance = false;
							}
						}

						if (!$result['account_type']) {
							$result['account_type'] = "XXXX";
						}

						$select = $this->_db->select()
							->from('M_PRODUCT_TYPE', array('PRODUCT_NAME'))
							->where('PRODUCT_CODE= ?', $result['account_type']);
						$checkproduct = $this->_db->fetchOne($select);
						if (!empty($checkproduct)) {
							$accData  = $this->getAccStatus($acct_no);

							if (count($accData) > 0) {
								$accStatus = $accData['ACCT_STATUS'];
								$custDataBasedAcc = $this->getCustData($accData['CUST_ID']);
							}
							$accountType = $result['account_type'];
						} else {
							$cust_id_acc_flag = false;
							$cust_id_acc_type = 1;
						}
					}
				}
			}

			$accountTypeCheck = false;
			$productTypeCheck = false;

			$_SESSION['special_rate'] = $specialRate;

			$getAllProduct = $this->_db->select()
				->from("M_PRODUCT")
				->query()->fetchAll();

			foreach ($getAllProduct as $key => $value) {
				# code...
				if ($value['PRODUCT_CODE'] == $accountType) {
					$accountTypeCheck = true;
				};

				if ($value['PRODUCT_PLAN'] == $productType) {
					$productTypeCheck = true;
				};
			}

			if ($cust_status == 1) $cust_id_flag = true;
			else $cust_id_flag = false;

			//validasi multiple email

			if ($zf_filter_input->acct_email) {
				$validate = new validate;
				$cek_multiple_email = $validate->isValidEmailMultiple($zf_filter_input->acct_email);
			} else {
				$cek_multiple_email = true;
			}

			$valid_cif = true;
			$svcCheckCif = new Service_Account($this->_request->getPost('acct_no'), "", null, null, null);
			$getCif = $svcCheckCif->inquiryAccontInfo()["cif"];

			$checkCif = $this->_db->select()
				->from("M_CUSTOMER", ["CUST_CIF"])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetch();

			// if (!($checkCif["CUST_CIF"] == $getCif)) {
			// 	$valid_cif = false;
			// }

			//----------END validasi tambahan-------------------

			if ($zf_filter_input->isValid() &&  $cust_id_flag == true && $cust_id_acc_flag == true && $cek_multiple_email == true && $valid_cif == true && $aro && $status && $accountTypeCheck && $productTypeCheck && !$hold) {

				$acct_email   = $zf_filter_input->acct_email;
				$ccy_id       = $zf_filter_input->ccy_id;
				$acct_no      = $zf_filter_input->acct_no;
				$acct_alias      = $zf_filter_input->acct_alias;
				//get balance and owner from core
				//$acct_balance = $account->getAvailableBalance();
				// $acct_owner   = $account->getCoreAccountName();


				/*
				$info = 'Account No = '.$zf_filter_input->acct_no;
				$acct_data = $this->_acctData;
									
				foreach($validators as $key=>$value)
				{
				if($zf_filter_input->$key) $acct_data[strtoupper($key)] = $zf_filter_input->$key;
				}
				
				if($zf_filter_input->token_serialno)
				{ 
				$user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['yes']); 
				}
				else
				{ 
				$user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['no']); 
				}
				
				$acct_data['ACCT_STATUS'] = 1;

				
				try 
				{  
				$this->_db->beginTransaction();
				
				$acct_data['ACCT_STATUS'] = 1;
				$acct_data['ACCT_SUGGESTED']   = new Zend_Db_Expr('now()');
				$acct_data['ACCT_SUGGESTEDBY'] = $this->_userIdLogin;
				
				
				$change_id = $this->suggestionWaitingApproval('customeraccount',$info,$this->_changeType['code']['new'],null,'M_CUSTOMER_ACCT','TEMP_CUSTOMER_ACCT','CUST_ID,ACCT_NO',$zf_filter_input->cust_id.','.$zf_filter_input->acct_no);
				
				$this->insertTempCustomerAcct($change_id,$acct_data);
				
				$this->_db->commit();
				
				$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
				$this->_db->rollBack();
				$error_remark = $this->getErrorRemark('82');
				SGO_Helper_GeneralLog::technicalLog($e);
				}*/

				Application_Helper_General::writeLog('ACAD', '');

				//jika berhasil ke layar confirm :
				$this->_redirect('/customeraccount/confirm/index/cust_id/' . $cust_id . '/acct_email/' . $acct_email . '/acct_no/' . $acct_no . '/acct_alias/' . $acct_alias . '/ccy_id/' . $ccy_id . '/type/' . $checkproduct);
			} else {
				$this->view->error = 1;
				$this->view->acct_no      = ($zf_filter_input->isValid('acct_no')) ? $zf_filter_input->acct_no : $this->_request->getPost('acct_no');
				$this->view->acct_alias      = ($zf_filter_input->isValid('acct_alias')) ? $zf_filter_input->acct_alias : $this->_getParam('acct_alias');
				$this->view->ccy_id       = ($zf_filter_input->isValid('ccy_id')) ? $zf_filter_input->ccy_id : $this->_getParam('ccy_id');
				$this->view->acct_email   = ($zf_filter_input->isValid('acct_email')) ? $zf_filter_input->acct_email : $this->_getParam('acct_email');
				//$this->view->user_group = ($zf_filter_input->isValid('fgroup_id'))? $zf_filter_input->fgroup_id : $this->_getParam('fgroup_id');

				$error = $zf_filter_input->getMessages();
				/*if(count($error))$error_remark = $this->displayErrorRemark($error);
	  			$this->view->user_msg = $this->displayError($error);*/

				//format error utk ditampilkan di view html 
				$errorArray = null;
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$errorArray[$keyRoot] = $errorString;
					}
				}

				//konfigurasi error cust id jika didelete
				if ($cust_id_acc_flag == false) {
					if ($cust_id_acc_type == 1) {
						//print_r($result['errorMessage']);die;
						if ($result['errorMessage']) $errorArray['acct_no'] = $result['errorMessage'];
						else $errorArray['acct_no'] = $this->language->_($result['response_desc']);

						if ($result['account_type'] == "XXXX") {
							$errorArray['acct_no'] = "Account Type Null From Core";
						}
					}
					//else if($cust_id_acc_type == 2) $errorArray['acct_no'] = 'NOTICE : This account is assigned (already deleted) to Company : ['. $custDataBasedAcc['CUST_NAME'] .' �('. $custDataBasedAcc['CUST_ID'] .')]';
				}

				if ($cust_id_flag == false) {
					if ($cust_status == 3)       $errorArray['acct_no'] = $this->language->_('Cannot add record, company already deleted');
					else if ($cust_status == 2)  $errorArray['acct_no'] = $this->language->_('Cannot add record, company already Suspended');
				}

				//$errorArray['acct_no'] = 'test';
				//Zend_Debug::dump($errorArray); //die;

				if (isset($cek_multiple_email) && $cek_multiple_email == false) $errorArray['acct_email'] = $this->language->_('Invalid format');

				if ($accountTypeCheck == '') $errorArray['acct_no'] = $this->language->_('Nomor Rekening tidak boleh kosong');
				elseif (!$accountTypeCheck) $errorArray['acct_no'] = $this->language->_('Tipe Akun tidak terdaftar pada Manajemen Produk');
				elseif (!$productTypeCheck) $errorArray['acct_no'] = $this->language->_('Tipe Produk ' . $productType . ' tidak terdaftar pada Manajemen Produk');
				elseif (!$aro) $errorArray['acct_no'] = $this->language->_('Bukan ARO');
				elseif ($hold) $errorArray['acct_no'] = $this->language->_('Status Rekening Hold');
				elseif ($cek_balance == false) $errorArray['acct_no'] = $this->language->_('Account tidak aktif');
				// if ($valid_cif == false && !empty($zf_filter_input->acct_no)) $errorArray['acct_no'] = $this->language->_('CIF tidak sama');

				if ($cek_account == false) $errorArray['acct_no'] = $this->language->_('Account tidak ditemukan');

				$this->view->custAcct_msg  = $errorArray;
			}
		} //if($this->_request->isPost())     
		else {

			$this->view->acct_alias     = strip_tags(trim($this->_getParam('acct_alias')));
			$this->view->acct_no     = strip_tags(trim($this->_getParam('acct_no')));
			$this->view->acct_email  = strip_tags(trim($this->_getParam('acct_email')));

			$ccy_id = strip_tags(trim($this->_getParam('ccy_id')));
			if (empty($ccy_id)) $ccy_id = $getCcy[0]['CCY_ID'];

			$this->view->ccy_id = $ccy_id;
		}

		$select = $this->_db->select()
			->from('M_FGROUP', array('FGROUP_ID', 'FGROUP_NAME'))
			->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id) . ' OR UPPER(CUST_ID)=' . $this->_db->quote('BANK'))
			->where('UPPER(FGROUP_STATUS)=' . $this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
		$this->view->fgroup = $this->_db->fetchAll($select);

		$select = $this->_db->select()
			->from('M_CUSTOMER', array('CUST_NAME'))
			->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id));
		$this->view->cust_name = $this->_db->fetchOne($select);

		$this->view->cust_id = $cust_id;
		$this->view->modulename = $this->_request->getModuleName();

		//insert log
		try {
			$this->_db->beginTransaction();
			Application_Helper_General::writeLog('ACAD', '');
			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollBack();
			// SGO_Helper_GeneralLog::technicalLog($e);
		}
	}

	public function checkaccountexistAction(){
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$custId   = $this->_getParam('cust_id');
		$acctNo = $this->_getParam('acct_no');

		$checkAcct = $this->_db->select()
			->from('M_CUSTOMER_ACCT')
			->where('ACCT_NO = ?', $acctNo)
			->where('CUST_ID = ?', $custId)
			->query()->fetch();

		$checkAcctDel = $this->_db->select()
			->from('M_CUSTOMER_ACCT')
			->where('ACCT_NO = ?', $acctNo)
			->where('CUST_ID = ?', $custId)
			->where('ACCT_STATUS = 3')
			->query()->fetch();

		$checkTempAcct = $this->_db->select()
			->from('TEMP_CUSTOMER_ACCT')
			->where('ACCT_NO = ?', $acctNo)
			->where('CUST_ID = ?', $custId)
			->query()->fetch();

		$result = false;
		
		if ($checkAcct || $checkTempAcct) {
			$result = true;

			if($checkAcct){
				if($checkAcctDel){
					
					$data = 'Rekening sudah terdaftar dengan status delete';
				}else{
					$data = 'Rekening sudah terdaftar';
				}
			}

			if($checkTempAcct){
				$data = 'Rekening sedang dalam menunggu persetujuan';
			}
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode(['response' => $result, 'data' => $data]);
	}
}
