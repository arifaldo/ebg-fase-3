<?php

class mail_template
{
	static public function autoDisbursement($tmpl, $contentHead, $contentData, $emailSubject)
	{
		$tableData = self::arr_to_tableHtml($contentData);
		
		$trTranslate = array("[CUST_NAME]" 		  => $contentHead['CUST_NAME'],
							 "[LOG_DATE]" 		  => $contentHead['LOG_DATE'],
							 "[CUST_CIF]"	 	  => $contentHead['CUST_CIF'],
							 "[SCHEME_NAME]" 	  => $contentHead['SCHEME_NAME'],
							 "[CCY_ID]"	 	  	  => $contentHead['CCY_ID'],
							 "[AMOUNT]" 		  => $contentHead['AMOUNT'],
							 "[DISBURSE_ACCT_NO]" => $contentHead['DISBURSE_ACCT_NO'],
							 "[EMAIL_SUBJECT]"    => $emailSubject,
							 "[CONTENT_DETAILS]"  => $tableData,
							 );
		$tmpl =  strtr($tmpl, $trTranslate);
		
		$mailFormat = <<<MAIL
$tmpl
MAIL;

		//return self::toHtml($mailFormat);
		return $mailFormat;
	}
	
	static public function autoRepayment($tmpl, $contentHead, $contentData, $emailSubject)
	{
		$tableData = self::arr_to_tableHtml($contentData);
		
		$trTranslate = array("[CUST_NAME]" 		  => $contentHead['CUST_NAME'],
							 "[LOG_DATE]" 		  => $contentHead['LOG_DATE'],
							 "[CUST_CIF]"	 	  => $contentHead['CUST_CIF'],
							 "[SCHEME_NAME]" 	  => $contentHead['SCHEME_NAME'],
							 "[CCY_ID]"	 	  	  => $contentHead['CCY_ID'],
							 "[AMOUNT]" 		  => $contentHead['AMOUNT'],
							 "[REPAY_ACCT_NO]" 	  => $contentHead['REPAY_ACCT_NO'],
							 "[EMAIL_SUBJECT]"    => $emailSubject,
							 "[CONTENT_DETAILS]"  => $tableData,
							 );
		$tmpl =  strtr($tmpl, $trTranslate);
		
		$mailFormat = <<<MAIL
$tmpl
MAIL;

		return $mailFormat;
	}
	
	static public function autoDisburseBA($tmpl, $contentHead, $contentData, $emailSubject)
	{
		$tableData = self::arr_to_tableHtml($contentData);
		
		$trTranslate = array("[LOG_DATE]" 		  => $contentHead['LOG_DATE'],
							 "[BANK_ADMIN]"		  => $contentHead['BANK_ADMIN'],
							 "[EMAIL_SUBJECT]"    => $emailSubject,
							 "[CONTENT_DETAILS]"  => $tableData,
							 );
		$tmpl =  strtr($tmpl, $trTranslate);
		
		$mailFormat = <<<MAIL
$tmpl
MAIL;

		return $mailFormat;
	}
	
	static public function autoDisburseSuspect($tmpl, $contentHead, $contentData1, $contentData2, $emailSubject)
	{
		$tableData1 = self::arr_to_tableHtml($contentData1);
		$tableData2 = self::arr_to_tableHtml($contentData2);
		
		$trTranslate = array("[LOG_DATE]" 		    => $contentHead['LOG_DATE'],
							 "[BANK_ADMIN]"		    => $contentHead['BANK_ADMIN'],
							 "[EMAIL_SUBJECT]"      => $emailSubject,
							 "[CONTENT_DETAILS_1]"  => $tableData1,
							 "[CONTENT_DETAILS_2]"  => $tableData2,
							 );
		$tmpl =  strtr($tmpl, $trTranslate);
		
		$mailFormat = <<<MAIL
$tmpl
MAIL;

		return $mailFormat;
	}
	
	static public function arr_to_tableHtml($arr) 
	{
		$lines = array();
		if (!empty($arr))
		{
			foreach ($arr as $v) 
			{
				$lines[] = self::arr_to_tr($v);
			}
			
			return '<table border="1" cellspacing="0" cellpadding="1"><tbody>'.implode("", $lines).'</tbody></table>';
		}
		
		return "";
	}
	
	static public function arr_to_tr($arr) 
	{
		$line = array();
		if (!empty($arr))
		{
			foreach ($arr as $v)
			{
				$line[] = is_array($v) ? self::arr_to_tr($v) : '<td>' .  $v . '</td>';
			}
			
			return "<tr>".implode("", $line)."</tr>";
		}
		
		return "<tr><td></td></tr>";
	}
	
	static function toHtml($string)
	{	
		$trTranslate = array("\t" => str_repeat('&nbsp;',5),
							);
		return strtr($string,$trTranslate);
	}
	
}
