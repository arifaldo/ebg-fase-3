<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'CMD/Validate/ValidatePaymentOpen.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'CMD/Beneficiary.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'CMD/Beneficiary.php';
class opentransfer_DomesticController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}
    	// $this->view->hidetoken = true;
		
		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($this->_getParam('PS_NUMBER'));
		$this->view->encps_number = $this->_getParam('PS_NUMBER');
		$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);
		if (!empty($PS_NUMBER) && !$this->_request->isPost()) {

			$paramList = array(
				"WA" 			=> false,
				"ACCOUNT_LIST" 	=> $this->_accountList,
				"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
			);
			$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$select   = $CustomerUser->getPayment($paramList, false);
			$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

			/*if ($crossCurr == 1) {
				$select->columns(
					array(
						"tra_message"		=> "T.TRA_MESSAGE",
						"TRA_ADDMESSAGE"		=> "T.TRA_ADDITIONAL_MESSAGE",
						"acbenef_email"			=> "T.BENEFICIARY_EMAIL",
						"acbenef_address"		=> "T.BENEFICIARY_ADDRESS",
						"acbenef_citizenship"	=> "T.BENEFICIARY_CITIZENSHIP", //WNI WNA W / R
						"acbenef_resident"		=> "T.BENEFICIARY_RESIDENT",
						"bank_name"				=> "T.BENEFICIARY_BANK_NAME",
						"bank_city"				=> "T.BENEFICIARY_BANK_CITY",
						"clr_code"				=> "T.CLR_CODE",
						"lld_desc"				=> "T.LLD_DESC",
						"lld_code"				=> "T.LLD_CODE",
						"transfer_type"			=> "T.TRANSFER_TYPE",
						"acbenef_category"		=> "T.BENEFICIARY_CATEGORY",
						"acbenef_identity_type" => "T.BENEFICIARY_ID_TYPE",
						"acbenef_identity_num"	=> "T.BENEFICIARY_ID_NUMBER",
						"acbenef_address1"		=> "T.BENEFICIARY_BANK_ADDRESS1",
						"acbenef_address2"		=> "T.BENEFICIARY_BANK_ADDRESS2",
						"acbenef_pob"			=> "T.POB_NUMBER",
						"acbenef_country"		=> "T.BENEFICIARY_BANK_COUNTRY",
						"lld_identity"			=> "T.LLD_IDENTITY",
						"lld_trans_purpose"		=> "T.LLD_TRANSACTION_PURPOSE",
						"lld_trans_rel"			=> "T.LLD_TRANSACTOR_RELATIONSHIP"
						//"acbenef_bankname"		=> "T.BENEFICIARY_ACCOUNT_NAME",
						//"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
					)
				); */
			//} else {
				$select->columns(
					array(
						"tra_message"		=> "T.TRA_MESSAGE",
						"tra_refno"			=> "T.TRA_REFNO",
						"acbenef_email"		=> "T.BENEFICIARY_EMAIL",
						"acbenef_bankname"	=> "T.BENEFICIARY_ACCOUNT_NAME",
						"acbenef_alias"		=> "T.BENEFICIARY_ALIAS_NAME",
					)
				);
			//}

			$pslipData 	= $this->_db->fetchRow($select);
			//echo '<pre>';
//var_dump($pslipData);die;
			if (!empty($pslipData)) {
				//tambahn pentest
				$PS_EFDATE_ORI  	= date("d/m/Y", strtotime($pslipData['efdate']));
				$PS_SUBJECT 		= $pslipData['paySubj'];

				if ($PS_EFDATE_ORI > date('d/m/Y')) {
					$TransferDate 	=  "2"; //future
					$PS_EFDATE = $PS_EFDATE_ORI;
				} else {
					$TransferDate	=  "1"; //imediate
					$PS_EFDATE = date('d/m/Y');
				}
				
				//echo '<pre>';
				//var_dump($pslipData);die;

				$TRA_AMOUNT  		= Application_Helper_General::displayMoney($pslipData['amount']);
				$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
				$TRA_MESSAGE 		= $pslipData['tra_message'];
				$PS_NOTIF			= $pslipData['ps_notif'];
				$PS_EMAIL			= $pslipData['ps_email'];
				$PS_SMS				= $pslipData['ps_sms'];
				$ACCTSRC 			= $pslipData['accsrc'];
				$ACBENEF  			= $pslipData['acbenef'];
				$ACBENEF_CCY  		= $pslipData['acbenef_ccy'];
				$ACBENEF_BANKNAME	= $pslipData['acbenef_bankname'];
				$ACBENEF_ALIAS  	= $pslipData['acbenef_alias'];
				$ACBENEF_EMAIL  	= $pslipData['acbenef_email'];
				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE'];
				$ACCTSRC_BANKCODE   = $pslipData['SOURCE_ACCT_BANK_CODE'];
				//if ($crossCurr == 1) {
					$TRA_ADDMESSAGE 	= $pslipData['TRA_ADDMESSAGE'];
					$ACCTSRC_CCY		= $pslipData['accsrc_ccy'];
					$BENEF_CURRENCY  	= $pslipData['acbenef_ccy'];
					$CURR_CODE			= $pslipData['ccy'];

					$ACBENEF_ADDRESS  	= $pslipData['BENEFICIARY_ADDRESS'];
					$ACBENEF_ADDRESS2  	= $pslipData['BENEFICIARY_ADDRESS2'];
					$BENEFICIARY_RESIDENT = $pslipData['acbenef_resident'];
					$BENEFICIARY_CITIZENSHIP = $pslipData['acbenef_citizenship'];
					$BANK_CODE 			= $pslipData['BENEF_ACCT_BANK_CODE'];
					$BANK_NAME			= $pslipData['BENEFICIARY_ACCOUNT_BANK'];
					$BANKNAME			= $pslipData['BENEFICIARY_ACCOUNT_BANK'];
					$BANK_CITY			= $pslipData['bank_city'];
					$CLR_CODE			= $pslipData['CLR_CODE'];
					$LLD_DESC			= $pslipData['lld_desc'];
					$LLD_CODE			= $pslipData['lld_code'];
					$SWIFT_CODE			= $pslipData['SWIFT_CODE'];
					$CITY_CODE          = $pslipData['BENEFICIARY_BANK_CITY'];
					$LLD_BENEIDENTIF    = $pslipData['BENEFICIARY_ID_TYPE'];
					$LLD_BENEIDENTIF    = $pslipData['BENEFICIARY_ID_TYPE'];
					$ACBENEF_PHONE		= $pslipData['BENEFICIARY_MOBILE_PHONE_NUMBER'];
					
					if($pslipData['TRANSFER_TYPE'] == 0){
						$pslipData['transfer_type'] = 'ONLINE';
					}else if($pslipData['TRANSFER_TYPE'] == 2){
						$pslipData['transfer_type'] = 'SKN';
					}else if($pslipData['TRANSFER_TYPE'] == 1){
						$pslipData['transfer_type'] = 'RTGS';
					}
					
					$TRANSFER_TYPE  	= $pslipData['transfer_type'];	// 1, 2
					
					$BENEFICIARY_CATEGORY = $lldCategoryArr[$pslipData['acbenef_category']];
					$BENEFICIARY_ID_TYPE = $pslipData['BENEFICIARY_ID_TYPE'];
					$ACBENEF_IDENTITY_TYPE_DISP = $lldBeneIdentifArr[$BENEFICIARY_ID_TYPE];
					$BENEFICIARY_ID_NUMBER  = $pslipData['BENEFICIARY_ID_NUMBER'];
					$LLD_BENENUMBER	= $pslipData['BENEFICIARY_ID_NUMBER'];
					$ACBENEF_BANK_ADD1 = $pslipData['acbenef_address1'];
					$ACBENEF_BANK_ADD2 = $pslipData['acbenef_address2'];
					$ACBENEF_POBNUMB   = $pslipData['acbenef_pob'];
					$country_code   = $pslipData['acbenef_country'];
					$ACBENEF_RESIDENT = $pslipData['BENEFICIARY_CITIZENSHIP'];
					$LLD_IDENTICAL  = $pslipData['LLD_IDENTITY'];

					$LLD_RELATIONSHIP = $pslipData['LLD_TRANSACTOR_RELATIONSHIP'];
					$LLD_PURPOSE = $pslipData['LLD_TRANSACTION_PURPOSE'];

					$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
					$sessionNameConfrim->benefAcct = $ACBENEF;
					$sessionNameConfrim->sourceAcct = $ACCTSRC;
					$sessionNameConfrim->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
					$sessionNameConfrim->traAmount = $TRA_AMOUNT;
			//	} else {
			//		$TRA_REFNO 			= $pslipData['tra_refno'];
			//	}

				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"]) {
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				} 
				//elseif ($PS_TYPE != $this->_paymenttype["code"]["within"])	// 0: PB
			//	{
				//	$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				//	$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
				//	$this->_redirect('/paymentworkflow/requestrepair/index');
			//	}
			} else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
		}

		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP')) {
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}
	    

    	$USE_CONFIRM_PAGE = true;

    	$periodicEveryArr = array(
    								'1' => $this->language->_('Monday'), 
    								'2' => $this->language->_('Tuesday'), 
    								'3' => $this->language->_('Wednesday'), 
    								'4' => $this->language->_('Thursday'), 
    								'5' => $this->language->_('Friday'), 
    								'6' => $this->language->_('Saturday'), 
    								'7' => $this->language->_('Sunday'),);
		$periodicEveryDateArr = range(1,28);
		if (empty($PS_NUMBER)){
		$TRANSFER_TYPE = $this->_request->getParam('TRANSFER_TYPE');
		if(empty($TRANSFER_TYPE))
			$TRANSFER_TYPE='ONLINE';
		}
		// Set variables needed in view
    	$paramSettingID = array('range_futuredate', //'auto_release', 'cut_off_time_inhouse'
								'cut_off_time_skn'   , 'cut_off_time_rtgs',
								'threshold_rtgs'     , 'threshold_lld');

    	//appliacation settings
    	$settings 			= new Application_Settings();
		$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
		$ccyList  			= $settings->setCurrencyRegistered();	

		//general settings, utk ambil parameter skn rtgs
		$Settings = new Settings();
		$this->view->maxdate = $Settings->getSetting('range_futuredate');
		$this->view->min_amount_skn = $Settings->getSetting('min_amount_skn');			
		$this->view->max_amount_skn = $Settings->getSetting('max_amount_skn');		

		$this->view->min_amount_rtgs = $Settings->getSetting('min_amount_rtgs');			
		$this->view->max_amount_rtgs = $Settings->getSetting('max_amount_rtgs');	

		$this->view->contactemail = $Settings->getSetting('master_bank_email');	
		$this->view->contactphone = $Settings->getSetting('master_bank_telp');	
		
		//general settings, utk ambil bank support list
		$this->view->bank_support_api = $Settings->getSetting('bank_support_api');		
		$this->view->bank_support_inhouse_transfer = $Settings->getSetting('bank_support_inhouse_transfer');			
		$this->view->bank_support_online_transfer = $Settings->getSetting('bank_support_online_transfer');			
		$this->view->bank_support_domestic = $Settings->getSetting('bank_support_domestic');			
		$this->view->bank_support_inhouse_inquiry = $Settings->getSetting('bank_support_inhouse_inquiry');			
		$this->view->bank_support_interbank_inquiry = $Settings->getSetting('bank_support_interbank_inquiry');	
		
		
		//select m_onlinebank_limit, utk ambil parameter online
		$onlineLimit = $this->_db->fetchAll(
					$this->_db->select()
					->from(array('A' => 'M_ONLINEBANK_LIMIT'), array('*'))
		);

		$this->view->onlineLimit = json_encode($onlineLimit);

		// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'

		//for skn rtgs
		$lldTypeArr  		= $settings->getLLDDOMType();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
		$residentArr 		= array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
 		$citizenshipArr		= array("W" => "WNI", "N" => "WNA");

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


		$frontendOptions = array ('lifetime' => 600,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'MAGR'.$this->_custIdLogin;

        // $AccArr = $cache->load($cacheID);
        // if(empty($AccArr)){
		$complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                       
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );	
        // echo $complist;;die;
         // var_dump($complist);die;
    	$comp = "'";
    	// print_r($complist);die;
    	foreach ($complist as $key => $value) {
    		$comp .= "','".$value['CUST_ID']."','";
    	}
    	$comp .= "'";

		$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 // ->where('A.ACCT_STATUS = ?','5')
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->order('A.APIKEY_ID ASC')
						 // echo $acctlist;
				);
						 // echo $acctlist;die;
		// echo '<pre>';
		// echo $acctlist;
		// print_r($acctlist);die;
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}
		// echo "<pre>";
		// var_dump($account);die;
		// $acct = array();
			$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];

			$acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
			$acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
			$acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
			$acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			$i++;
		}
		
		// var_dump($acct);
		$AccArr = $acct;
		// }else{
		//  $AccArr = $cache->load($cacheID);	
		// }

		// echo "<pre>";
		// print_r($acct);die();



		// $AccArr 	  = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only
		// var_dump($AccArr);die;




 
		
		$futureDate = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
		if($this->_custSameUser){
			 //echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			
			
			$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
			// echo $selectQuery;
			$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

			// var_dump($usergoogleAuth);die; 
			if (!empty($usergoogleAuth)) {
				$this->view->googleauth = true;
				//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
				$settingObj = new Settings();
				$maxtoken = $settingObj->getSetting("max_failed_token");
				$this->view->tokenfail = (int)$maxtoken-1;
				if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
					//die;   
					
					$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
					
					//var_dump($usergoogleAuth);
					//echo $maxtoken;
					//echo $usergoogleAuth['0']['USER_FAILEDTOKEN'];
					$tokenfail = (int)$maxtoken - ((int)$usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
					$this->view->tokenfail = $tokenfail;
				}
				
			}
			else{
				$this->view->nogoauth = '1';
				
			}

			

			$this->view->googleauth = true;
			
			
		}
		//var_dump($tokenfail);
		$anyValue = '-- '.$this->language->_('Select City'). ' --';
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
		$select ->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);
		
		$cityCodeArr 			= array(''=> $anyValue);
		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		$this->view->cityCodeArr 	= $cityCodeArr;
		
		
		$Settings = new Settings();
		$address_mandatory = $Settings->getSettingNew('address_mandatory');
		$this->view->address_mandatory = $address_mandatory; 
		
		// Add by Hamdan
		//$tranferdatetype = $this->_request->getParam('tranferdatetype');
		//$this->view->tranferdatetype = $tranferdatetype;
		
		$date_val	= date('d/m/Y');
		$PS_EFDATE_VAL = $this->_request->getParam('PS_EFDATE');
		$this->view->paymentDate = $PS_EFDATE_VAL;
		if($PS_EFDATE_VAL > $date_val){
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}
		elseif ($PS_EFDATE_VAL == $date_val){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}
		else{
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}
		
		
		$PS_PERIODIC_EVERY = $this->_request->getParam('PERIODIC_EVERY');
		$PS_PERIODIC_EVERY_DATE = $this->_request->getParam('PERIODIC_EVERYDATE');
		$PS_ENDDATE_VAL = ($this->_request->getParam('PS_ENDDATEPERIODIC'))?$this->_request->getParam('PS_ENDDATEPERIODIC'):date('d/m/Y');
		$this->view->endDatePeriodic = $PS_ENDDATE_VAL;		
				
		$jenisTransfer =  $this->_request->getParam('tranferdatetype');
		$jenisPeriodic =  $this->_request->getParam('tranferdateperiodictype');
		
		if ($jenisTransfer == '1'){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}elseif ($jenisTransfer == '2'){
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}elseif ($jenisTransfer == '3'){
			$periodictranfer = $this->language->_('Periodic Transfer');
			$this->view->paymentType = $periodictranfer;
			
			if ($jenisPeriodic == '5'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY;
			}elseif ($jenisPeriodic == '6'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY_DATE;
			}
		}
		// End

		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		if(empty($tranferdatetype)){$tranferdatetype=1;}
		$this->view->tranferdatetype = $tranferdatetype;

		$filter 		= new Application_Filtering();
		//$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
		$PS_EFDATE 		= Application_Helper_General::convertDate($this->getCurrentDate());
		$isConfirmPage 	= $this->_getParam('confirmPage');


		if($this->_getParam('process') == 'back'){
			Zend_Session::namespaceUnset('TW');
		}
		
		if($this->_getParam('clear') == '1'){ 
		//var_dump('gere');die;
			Zend_Session::namespaceUnset('TW');
			Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
			Zend_Session::namespaceUnset('confirmTransactRand');
			Zend_Session::namespaceUnset('confirmTransact');
			
			
			
		}


		$sessionNamespace = new Zend_Session_Namespace('TW');


		$process 	= $this->_getParam('process');
		$submitBtn 	= ($this->_request->isPost())? true: false;

		// print_r($this->_request->getParams());
		// get ps_number data for repair payment
		
		//tambahn pentest
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;
		
		if (!empty($pslipData))	
		{
			if($TransferDate == '1'){
				$this->view->PS_EFDATEFUTURE = $futureDate;
			}
			elseif ($tranferdatetype =='3'){
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII;//						
			}
			else{
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			}
		}
		else{
			$this->view->PS_EFDATEFUTURE = $futureDate;
		}
		
		//$TRANSFER_TYPE ='SKN';
		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
		if($this->_request->isPost() )
		{
			// print_r($this->_request->getParams());die;
			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
			
			
				
			if($this->_getParam('randomTransact') == $sessionNameRand->randomTransact){

				try {
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					$blocksize = 256;  // can be 128, 192 or 256
					$ACBENEF =  SGO_Helper_AES::decrypt( $this->_request->getParam('ACBENEF'), $passwordRand, $blocksize );
					$ACCTSRC =  SGO_Helper_AES::decrypt( $this->_request->getParam('ACCTSRC'), $passwordRand, $blocksize );
					$TRA_AMOUNT =  SGO_Helper_AES::decrypt( $this->_request->getParam('TRA_AMOUNT'), $passwordRand, $blocksize );
				} 
				catch (Exception $e) {
					$ACBENEF = '';
					$ACCTSRC = '';
					$TRA_AMOUNT = '';
				}

				$SWIFT_CODE 		= $this->_getParam('SWIFT_CODE');
				$SAVEBENE 			= $this->_getParam('save_bene');
				if($SAVEBENE){
					$this->view->savebenecheck = 'checked';
				}
				$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PS_SUBJECT')		, "PS_SUBJECT");
				$PS_EFDATE 			= $filter->filter($this->_request->getParam('PS_EFDATE')		, "PS_DATE");
			
				if ($isConfirmPage == 1) {
					$TRA_AMOUNT 		= $filter->filter($TRA_AMOUNT		, "AMOUNT");
				}else{
					$TRA_AMOUNT 		= $filter->filter($this->_request->getParam('TRA_AMOUNT')		, "AMOUNT");
				}
				

				$TRA_MESSAGE 		= $filter->filter($this->_request->getParam('TRA_MESSAGE')		, "TRA_MESSAGE");

				if ($isConfirmPage == 1) {
					$ACCTSRC 			= $filter->filter($ACCTSRC	, "ACCOUNT_NO");
				}else{
					$ACCTSRC 			= $this->_request->getParam('ACCTSRC');
				}

				$ACCTSRCTEXT		= $filter->filter($this->_request->getParam('ACCTSRCTEXT'), "ACCTSRCTEXT");
				$ACCTSRC_BANKCODE	= $filter->filter($this->_request->getParam('sourceBankCode')		, "BANK_CODE");
				$ACBENEF 			= $filter->filter($ACBENEF			, "ACCOUNT_NO");
				$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME')	, "ACCOUNT_NAME");
				$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS')	, "ACCOUNT_ALIAS");
				$ACBENEF_EMAIL 		= $filter->filter($this->_request->getParam('ACBENEF_EMAIL')	, "EMAIL");
				$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE')		, "SELECTION");

				$ACBENEF_CITIZEN 		= $filter->filter($this->_request->getParam('ACBENEF_CITIZENSHIP_SELECT')		, "SELECTION");
				$ACBENEF_RESIDENT 		= $filter->filter($this->_request->getParam('ACBENEF_RESIDENT_SELECT')		, "SELECTION");
				$ACBENEF_CATEGORY 		= $filter->filter($this->_request->getParam('LLD_CATEGORY_SELECT')		, "SELECTION");
				$ACBENEF_IDTYPE 		= $filter->filter($this->_request->getParam('LLD_BENEIDENTIF_SELECT')		, "SELECTION");

				$ACBENEF_CITY 		= $this->_getParam('ACBENEF_CITY');
				$ACBENEF_PHONE 		= $this->_getParam('ACBENEF_PHONE');


				$BANK_NAME			= $filter->filter($this->_request->getParam('BANK_NAME')		, "BANK_NAME");

				$TRANSFER_TYPE 		= $filter->filter($this->_request->getParam('TRANSFER_TYPE')	, "SELECTION");

				// if($TRANSFER_TYPE == 'ONLINE'){
					$BANK_CODE			= $filter->filter($this->_request->getParam('BANK_CODE')		, "BANK_CODE");
				// }
				// else{
					$TRANSFER_FEE 		= $filter->filter($this->_request->getParam('TRANSFER_FEE')		, "TRANSFER_FEE");
					$TRA_REFNO 			= $filter->filter($this->_request->getParam('TRA_REFNO')		, "TRA_REFNO");

					$ACBENEF_ADDRESS	= $filter->filter($this->_request->getParam('ACBENEF_ADDRESS')	, "ADDRESS");
					$ACBENEF_ADDRESS2	= $filter->filter($this->_request->getParam('ACBENEF_ADDRESS2')	, "ADDRESS2");
				
					$ACBENEF_RESIDENT= $filter->filter($this->_request->getParam('ACBENEF_RESIDENT'), "SELECTION");
					$this->view->ACBENEF_RESIDENT= (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT	: 'R'; 
					
					$ACBENEF_CITIZENSHIP= $filter->filter($this->_request->getParam('ACBENEF_CITIZENSHIP'), "SELECTION");
					$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: 'W';
					
					$CITY_CODE= $filter->filter($this->_request->getParam('CITY_CODE'), "SELECTION");
					$this->view->CITY_CODE= (isset($CITY_CODE))	? $CITY_CODE	: ''; 
					
					
					$CLR_CODE			= $filter->filter($this->_request->getParam('CLR_CODE')			, "BANK_CODE");
					$LLD_CATEGORY 		= $filter->filter($this->_request->getParam('LLD_CATEGORY')		, "LLD_CODE");
					$LLD_IDENTICAL 		= $this->_request->getParam('LLD_IDENTICAL');
					$LLD_RELATIONSHIP 	= $this->_request->getParam('LLD_RELATIONSHIP');
					$LLD_PURPOSE 		= $this->_request->getParam('LLD_PURPOSE');
					$LLD_DESCRIPTION 	= $this->_request->getParam('LLD_DESCRIPTION');
					$LLD_BENEIDENTIF 	= $filter->filter($this->_request->getParam('LLD_BENEIDENTIF')	, "LLD_CODE");
					$LLD_BENENUMBER 	= $filter->filter($this->_request->getParam('LLD_BENENUMBER')	, "LLD_CODE");
					$LLD_SENDERIDENTIF 	= $filter->filter($this->_request->getParam('LLD_SENDERIDENTIF'), "LLD_CODE");
					$LLD_SENDERNUMBER 	= $filter->filter($this->_request->getParam('LLD_SENDERNUMBER')	, "LLD_CODE");
					 //echo "<pre>";
					 //print_r($this->_request->getParams());die;
					$TRANSFER_FEE_num 	= Application_Helper_General::convertDisplayMoney($TRANSFER_FEE);
				// }
					
	
				$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
				
				$PERIODIC_EVERY 	= $filter->filter($this->_request->getParam('PERIODIC_EVERY')	, "SELECTION");
				$PERIODIC_EVERYDATE = $filter->filter($this->_request->getParam('PERIODIC_EVERYDATE')	, "SELECTION");
				$TrfDateType 		= $filter->filter($this->_request->getParam('tranferdatetype')	, "SELECTION");				
				$TrfPeriodicType 	= $filter->filter($this->_request->getParam('tranferdateperiodictype')	, "SELECTION");
				$PS_ENDDATE			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC')		, "PS_DATEPERIODIC");
				
				$PS_EVERY_PERIODIC_UOM 	= $TrfPeriodicType;
				$START_DATE 			= $filter->filter(date('d/m/Y')	, "PS_DATE");
				$EXPIRY_DATE 			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC')	, "PS_DATE");
				$readonly 			= $filter->filter($this->_request->getParam('readonlymode')	, "readonly");
				$checkboxmode 			= $filter->filter($this->_request->getParam('checkboxmode')	, "checkboxmode");

				$TRA_NOTIF		= $this->_request->getParam('notif');
					$TRA_EMAIL		= $this->_request->getParam('email_notif');
					$TRA_SMS		= $this->_request->getParam('sms_notif');
	
				$filter->__destruct();
				unset($filter);

				$TRANSFER_TYPE = $this->_request->getParam('TRANSFER_TYPE');
				
				if (empty($TRANSFER_TYPE)) {
					$TRANSFER_TYPE = 'ONLINE';
				}

				if ($isConfirmPage == 1) {
					$TRANSFER_TYPE = $this->_request->getParam('TRANSFER_TYPE');
				}

				if ($ACCTSRC_BANKCODE == $BANK_CODE) {
					$this->view->TRANSFER_TYPE_view = 'MM - Online';
				}
				else{
					$this->view->TRANSFER_TYPE_view = 'MM - '.$TRANSFER_TYPE;
				}
	
				// post submit payment
				if ($submitBtn)
				{

					// echo $TrfDateType;
					// die('here');

					//cek periodik				
					if ($TrfDateType=='3'){
											
						$repetition = $this->_getParam('repetition');
						$efDate = $this->_getParam('PS_EFDATE1');
						$PS_EFDATE 			= $this->_request->getParam('PS_EFDATE1');
						//daily
						if ($repetition == 1) {
							$repeatOn = $this->_getParam('report_day');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
						}
						//weekly
						else if ($repetition == 2) {
							$repeatOn = $this->_getParam('report_day');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

							if (empty($repeatOn)) {
								$this->view->error = true;
								$confirmPage = false;
								$this->view->error_msg = 'Repeat On Be Selected';
							}
						}
						//monthly
						else if ($repetition == 3) {
							$repeatEvery = $this->_getParam('selectrepeat');
							// $repeatOn = $this->_getParam('PS_REPEATON');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

							if (empty($repeatEvery)) {
								$this->view->error = true;
								$confirmPage = false;
								$this->view->error_msg = 'Repeat Every Must Be Selected';
							}
							// if (empty($repeatOn)) {
							// 	$this->view->error = true;
							// 	$confirmPage = false;
							// 	$error_msg[] = 'Repeat On Be Selected';
							// }
						}

						if (empty($endDate)) {
							$this->view->error = true;
							$confirmPage = false;
							$this->view->error_msg = 'End Date Must Be Selected';
						}

						//periodic
						$this->view->efDate = $efDate;
						$this->view->repetition = $repetition;
						$this->view->endDate = $endDate;
						$this->view->repeatEvery = $repeatEvery;
						$this->view->repeatOn = $repeatOn;
						$this->view->endDate = $endDate;
						$this->view->repeat_day = $repeatOn;


					}else if($TrfDateType=='2'){
						$PS_EFDATE 			= $this->_request->getParam('PS_EFDATE');
						//var_dump($PS_EFDATE);
						$PS_EFDATE = $PS_EFDATE;
					}else{
						$PS_EFDATE = $PS_EFDATE;
					}
					
			//		var_dump($PS_EFDATE); 
					if($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS'){

						if($ACBENEF_ADDRESS == '' || $ACBENEF_ADDRESS2 == ''){
							// die('here');
							$error_msg		 		= 'Beneficiary Address can not be left blank.';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
						}

						if(empty($ACBENEF_PHONE) || $ACBENEF_PHONE == ''){
							// die('here');
							$error_msg		 		= 'Beneficiary Phone can not be left blank.';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
						}

						$settingObj = new Settings();
						$limitlld	= $settingObj->getSetting("threshold_lld"	, 0);

						if ($limitlld <= $TRA_AMOUNT_num && $ACBENEF_RESIDENT == 'NR') {
							if(empty($LLD_IDENTICAL) || $LLD_IDENTICAL == ''){
								// die('here');
								$error_msg		 		= 'Identity can not be left blank.';
								$this->view->error 		= true;
								$this->view->error_msg	= $error_msg;
							}

							if(empty($LLD_RELATIONSHIP) || $LLD_RELATIONSHIP == ''){
								// die('here');
								$error_msg		 		= 'Transactor Relationship can not be left blank.';
								$this->view->error 		= true;
								$this->view->error_msg	= $error_msg;
							}

							if(empty($LLD_PURPOSE) || $LLD_PURPOSE == ''){
								// die('here');
								$error_msg		 		= 'Transaction Purpose can not be left blank.';
								$this->view->error 		= true;
								$this->view->error_msg	= $error_msg;
							}
						}

					}
					if(!$PS_EFDATE){
	
						$error_msg		 		= 'Payment Date can not be left blank.';
						$this->view->error 		= true;
						$this->view->error_msg	= $error_msg;
						
					}elseif($TrfDateType=='3' && $TrfPeriodicType==5 && empty($PERIODIC_EVERY)){
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->error_msg	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					
					}elseif($TrfDateType=='3' && $TrfPeriodicType==6 && empty($PERIODIC_EVERYDATE)){
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->error_msg	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					
					}elseif($TrfDateType=='3' && strtotime($NEXT_DATE) > strtotime($END_DATE)){
						$errorMsg		 		= $this->language->_('End Date must be greater than'). " ". $NEXT_DATE;
						$this->view->error 		= true;
						$this->view->error_msg	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
	
					}
					elseif(empty($ACBENEF_EMAIL)){
						//var_dump($ACCTSRC);
						//var_dump($ACBENEF);die;
						if($ACCTSRC == "" || $ACBENEF == "" || $TRA_AMOUNT_num == ""){ 
							//die('get');
							$error_msg		 		= 'Mandatory field cannot be empty.';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
						}else{
							$error_msg		 		= 'Email can not be left blank.';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
						}

						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
						$sessionNameConfrim->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;

						if(empty($CITY_CODE)){
							$CITY_CODE = $ACBENEF_CITY;
						}
						if(empty($LLD_BENEIDENTIF)){
							$LLD_BENEIDENTIF = $ACBENEF_IDTYPE;
						}

					}elseif(!filter_var($ACBENEF_EMAIL, FILTER_VALIDATE_EMAIL)){
						$error_msg		 		= 'Invalid email format';
						$this->view->error 		= true;
						$this->view->error_msg	= $error_msg;

						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
						$sessionNameConfrim->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;

						if(empty($CITY_CODE)){
							$CITY_CODE = $ACBENEF_CITY;
						}		
						if(empty($LLD_BENEIDENTIF)){
							$LLD_BENEIDENTIF = $ACBENEF_IDTYPE;
						}
					}
					else{
						// die($PS_EFDATE);
						$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
						if(!$validateDateFormat->isValid($PS_EFDATE))
						{
							$error_msg = 'Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
							$this->view->error 		= true;
							$this->view->error_msg	= $error_msg;
	
						}
						else{
							$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
							$sessionNameConfrim->benefAcct = $ACBENEF;
							$sessionNameConfrim->sourceAcct = $ACCTSRC;
							$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;	

							//periodic
							$report_day = $this->_getParam('report_day');

							$sessionNameConfrim->efDate = $efDate;						
							$sessionNameConfrim->endDate = $endDate;						
							$sessionNameConfrim->repeatDay = $repeatOn;
							$sessionNameConfrim->repeatEvery = $repeatEvery;
							$sessionNameConfrim->repetition = $repetition;
							$sessionNameConfrim->report_day = $report_day;
							$sessionNameConfrim->trfDateType = $TrfDateType;

							$paramPayment = array(	"CATEGORY" 					=> "SINGLE DOM",
													"FROM" 						=> "F",				// F: Form, I: Import
													"PS_NUMBER"					=> $PS_NUMBER,
													"PS_SUBJECT"				=> $PS_SUBJECT,
													"PS_EFDATE"					=> $PS_EFDATE,
													"_dateFormat"				=> $this->_dateDisplayFormat,
													"_dateDBFormat"				=> $this->_dateDBFormat,
													"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
													"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
													"_createPB"					=> false,								// cannot create PB trx
													"_createDOM"				=> $this->view->hasPrivilege('CDFT'),	// privi CDFT (Create Domestic Fund Transfer)
													"_createREM"				=> false,								// cannot create REM trx
												 );
												

												if(empty($CITY_CODE)){
													$CITY_CODE = $ACBENEF_CITY;
												}

												

												if(empty($LLD_BENEIDENTIF)){
													$LLD_BENEIDENTIF = $ACBENEF_IDTYPE;
												}


												if(empty($LLD_CATEGORY)){
													$LLD_CATEGORY = $ACBENEF_CATEGORY;
												}

												if(empty($ACBENEF_CITIZENSHIP)){
													$ACBENEF_CITIZENSHIP = $ACBENEF_CITIZEN;
												}
												
							$paramTrxArr[0] = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
													"TRA_AMOUNT" 				=> $sessionNameConfrim->traAmount,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_REFNO" 				=> $TRA_REFNO,
													"ACCTSRC" 					=> $sessionNameConfrim->sourceAcct,
													"ACBENEF" 					=> $sessionNameConfrim->benefAcct,
													"ACBENEF_CCY" 				=> $ACBENEF_CCY,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
	
												// for Beneficiary data, except (bene CCY and email), must be passed by reference
													"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
													"ReffId" 					=> &$ReffId,
													"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
	 												"ACBENEF_CITIZENSHIP" 		=> &$ACBENEF_CITIZENSHIP,		// W:WNI, N: WNA
													"ACBENEF_RESIDENT" 			=> &$ACBENEF_RESIDENT,			// 1/R: RESIDENT, 2/NR: NON-RESIDENT
													"ACBENEF_ADDRESS1" 			=> &$ACBENEF_ADDRESS,
													"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,
	
												//	"ORG_DIR" 					=> $ORG_DIR,
													"BANK_CODE" 				=> $BANK_CODE,
												//	"BANK_NAME" 				=> $BANK_NAME,
												//	"BANK_BRANCH" 				=> $BANK_BRANCH,
												//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
												//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
												//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
	
													"LLD_IDENTICAL" 			=> $LLD_IDENTICAL,
												//	"LLD_CITIZENSHIP"			=> $LLD_CITIZENSHIP,
													"LLD_CATEGORY" 				=> $LLD_CATEGORY,
													"LLD_RELATIONSHIP" 			=> $LLD_RELATIONSHIP,
													"LLD_PURPOSE" 				=> $LLD_PURPOSE,
													"LLD_DESCRIPTION" 			=> $LLD_DESCRIPTION,
													"LLD_BENEIDENTIF" 			=> $LLD_BENEIDENTIF,
													"LLD_BENENUMBER" 			=> $LLD_BENENUMBER,
													"LLD_SENDERIDENTIF" 		=> $LLD_SENDERIDENTIF,
													"LLD_SENDERNUMBER" 			=> $LLD_SENDERNUMBER,
													"CITY_CODE" 				=> $CITY_CODE, //yang akan dikirimkan ke vlink field 3
													"PS_EMAIL"					=> $TRA_EMAIL,
													"PS_NOTIF"					=> $TRA_NOTIF,
													"PS_SMS"					=> $TRA_SMS,
												 );

							if($TRANSFER_TYPE == 'ONLINE'){
								$paramPayment['_createDOM'] = true;

								unset($paramTrxArr[0]['ReffId']);
								unset($paramTrxArr[0]['LLD_BENEIDENTIF']);
								unset($paramTrxArr[0]['LLD_BENENUMBER']);
								unset($paramTrxArr[0]['LLD_SENDERIDENTIF']);
								unset($paramTrxArr[0]['LLD_SENDERNUMBER']);
								unset($paramTrxArr[0]['CITY_CODE']);

								$paramTrxArr[0]['TRA_REFNO'] = "";
								$paramTrxArr[0]['ACBENEF_CITIZENSHIP'] = "";
								$paramTrxArr[0]['ACBENEF_RESIDENT'] = "";
								$paramTrxArr[0]['ACBENEF_ADDRESS1'] = "";
								$paramTrxArr[0]['ACBENEF_ADDRESS2'] = "";
								$paramTrxArr[0]['BANK_CODE'] = $BANK_CODE;

								$paramTrxArr[0]['LLD_IDENTICAL'] = "";
								$paramTrxArr[0]['LLD_CATEGORY'] = "";
								$paramTrxArr[0]['LLD_RELATIONSHIP'] = "";
								$paramTrxArr[0]['LLD_PURPOSE'] = "";
								$paramTrxArr[0]['LLD_DESCRIPTION'] = "";
							}
	
	
							if($isConfirmPage != 1){
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME = $paramTrxArr[0]['ACBENEF_BANKNAME'];
							}
							else{
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME;		
							}

							if(empty($sessionNameConfrim->SAVEBENE)){
								$sessionNameConfrim->SAVEBENE = $SAVEBENE;
							}
							
							// echo "<pre>";
							// var_dump($paramPayment);
							// var_dump($paramTrxArr);die;
							$resWs = array();
							$validate   = new ValidatePaymentOpen($this->_custIdLogin, $this->_userIdLogin);

							if(empty($TRANSFER_TYPE) && $this->view->error == false)
								$validate->setFlagConfirmPage(($isConfirmPage == 1)?TRUE:FALSE); //tujuan untuk set supaya jangan manggil inquiry lg di page ke 2

							//c check account inquiry
							// echo "<pre>";
							// var_dump($account);die;

							//define clientuser object
							$clientUser  =  new SGO_Soap_ClientUser();
							// echo "<pre>";
							// ;
							// if(empty($this->_request->getParam('TRANSFER_TYPE')))
							//supaya di confirm page tidak terpanggil lagi
							if ($isConfirmPage != 1 && !empty($this->_request->getParam('TRANSFER_TYPE'))) {
								if ($TRANSFER_TYPE == 'ONLINE') {
									$request['sender_id'] = $AccArr['0']['SENDER_ID'];
									$request['AUTH_USER'] = $AccArr['0']['AUTH_USER'];
									$request['AUTH_PASS'] = $AccArr['0']['AUTH_PASS'];
									$request['signature'] = $AccArr['0']['SIGNATURE_KEY'];
									$request['corporate_id_channel'] = $this->_custIdLogin;
									$request['beneficiary_account_number'] = $sessionNameConfrim->benefAcct;
									$request['beneficiary_bank_code'] = $this->_getParam('BANK_CODE');;
									 //echo "<pre>";
									 //print_r($this->_request->getParams());
									 //print_r($request);die;
									    
									 $soucebank = $this->_getParam('sourceBankCode');
									 $benebank = $this->_getParam('BANK_CODE');
									 if($soucebank == $benebank){
										 $serviceType = 7;
									 }else{
										 $serviceType = 8;
									 }
									$success = $clientUser->callapi('accountname',$request,'b2b/inquiry/name');

									$result  = $clientUser->getResult();

									// echo "<pre>";
									// print_r($result);die();

									//insert T_DIGI_LOG
									$paramlog = array(
							            'DIGI_USER' => $this->_userIdLogin,
							            'DIGI_CUST' => $this->_custIdLogin,
							            'DIGI_BANK' => $request['beneficiary_bank_code'],
							            'DIGI_ACCOUNT' => $request['beneficiary_account_number'],
							            'DIGI_ERROR_CODE' => $result->error_code,
							            'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
							            'DIGI_SERVICE'  => $serviceType
							        );

							        $this->_helper->ServiceLog->serviceLog($paramlog);

									// var_dump($result);die;
									// echo '<pre>';
									// print_r($result);die;
									if ($result->error_code == '0000') {
											$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
											$sessionNameConfrim->traAmount	   = $TRA_AMOUNT_num;
											$sessionNameConfrim->acct_numb = $ACCTSRC;
											$sessionNameConfrim->bene_numb = $ACBENEF;
											$sessionNameConfrim->bene_bank	   = $BANK_NAME;
											$sessionNameConfrim->bank_code	   = $BANK_CODE;
											$sessionNameConfrim->desc	   = $TRA_MESSAGE;
											$sessionNameConfrim->bene_name = $result->beneficiary_account_name;
											$sessionNameConfrim->bank_name	   = $result->beneficiary_bank_name;
											
											//$isConfirmPage = true;
									}else{
										$isConfirmPage = false; 
										$this->view->error = true;
										$this->view->inquiry_error = 'Inquiry Failed, Please try again';
									}

								}
								else if ($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS') {
									$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
									$sessionNameConfrim->swift_code = $SWIFT_CODE;
									$sessionNameConfrim->traAmount	   = $TRA_AMOUNT_num;
									$sessionNameConfrim->acct_numb = $ACCTSRC;
									$sessionNameConfrim->bene_numb = $ACBENEF;
									$sessionNameConfrim->bene_bank = $ACBENEF_BANKNAME;
									$sessionNameConfrim->bene_address = $ACBENEF_ADDRESS;
									$sessionNameConfrim->bene_address2 = $ACBENEF_ADDRESS2;
									$sessionNameConfrim->citizenship = $ACBENEF_RESIDENT;
									$sessionNameConfrim->nationality = $ACBENEF_CITIZENSHIP;
									$sessionNameConfrim->bene_category = $LLD_CATEGORY;
									$sessionNameConfrim->bene_idtype = $LLD_BENEIDENTIF;
									$sessionNameConfrim->bene_idnum = $LLD_BENENUMBER;
									$sessionNameConfrim->bene_city = $ACBENEF_CITY;
									$sessionNameConfrim->bene_phone = $ACBENEF_PHONE;
									$sessionNameConfrim->bank_code = $BANK_CODE;
									$sessionNameConfrim->desc	   = $TRA_MESSAGE;
									$sessionNameConfrim->bene_name = $ACBENEF_BANKNAME;
									$sessionNameConfrim->bank_name = $BANK_NAME;
									
									$sessionNameConfrim->lld_identical = $LLD_IDENTICAL;
									$sessionNameConfrim->lld_relation	   = $LLD_RELATIONSHIP;
									$sessionNameConfrim->lld_purpose = $LLD_PURPOSE;
									$sessionNameConfrim->lld_desc = $LLD_DESCRIPTION;

									$sessionNameConfrim->bene_email = $ACBENEF_EMAIL;
									
									
									//$isConfirmPage = true;
								}

								//validate
								$resWs = array();
								$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);

								if (!$resultVal) {
									$isConfirmPage = false;
									$this->view->error = true;
									$errorMsg 		= $validate->getErrorMsg();								
									$this->view->error_msg = $errorMsg;
								}
								else{
									//$isConfirmPage = true;
								}


								if($this->view->error){
									$isConfirmPage = false;
								}
							}

							//simpan session utk dipakai saat back
							$data['PS_SUBJECT'] = $PS_SUBJECT;
							$data['ACCTSRC'] = $ACCTSRC;
							$data['ACCTSRCTEXT'] = $ACCTSRCTEXT;
							$data['ACCTSRC_BANKCODE'] = $ACCTSRC_BANKCODE;
							$data['TRA_AMOUNT'] = $TRA_AMOUNT_num;
							$data['BANK_CODE'] = $BANK_CODE;
							$data['BANK_NAME'] = $BANK_NAME; 
							$data['CLR_CODE'] = $CLR_CODE;
							$data['SWIFT_CODE'] = $SWIFT_CODE;
							$data['CITY_CODE'] = $CITY_CODE;
							$data['ACBENEF'] = $ACBENEF;
							$data['ACBENEF_BANKNAME'] = $ACBENEF_BANKNAME;
							$data['ACBENEF_ADDRESS'] = $ACBENEF_ADDRESS;
							$data['ACBENEF_ADDRESS2'] = $ACBENEF_ADDRESS2;
							$data['ACBENEF_RESIDENT'] = $ACBENEF_RESIDENT;
							$data['ACBENEF_CITIZEN'] = $ACBENEF_CITIZEN;
							$data['ACBENEF_CATEGORY'] = $ACBENEF_CATEGORY;
							$data['ACBENEF_IDTYPE'] = $ACBENEF_IDTYPE;
							$data['LLD_BENENUMBER'] = $LLD_BENENUMBER;
							$data['ACBENEF_PHONE'] = $ACBENEF_PHONE;
							$data['ACBENEF_CITY'] = $ACBENEF_CITY;
							$data['ACBENEF_EMAIL'] = $ACBENEF_EMAIL;
							$data['TRANSFER_TYPE'] = $TRANSFER_TYPE;
							$data['TRA_MESSAGE'] = $TRA_MESSAGE;
							$data['PS_EFDATEFUTURE'] = $PS_EFDATE ;
							$data['TrfDateType'] = $tranferdatetype;

							$data['PS_EFDATE1'] = $efDate;
							$data['PS_ENDDATEPERIODIC'] = $endDate;
							$data['selectrepeat'] = $repeatEvery;
							$data['REPORT_DAY'] = $report_day;
							$data['repetition'] = $repetition;
							
							$data['LLD_IDENTICAL'] = $LLD_IDENTICAL;
							$data['LLD_RELATIONSHIP'] = $LLD_RELATIONSHIP ;
							$data['LLD_PURPOSE'] = $LLD_PURPOSE;
							$data['LLD_DESCRIPTION'] = $LLD_DESCRIPTION;
							$data['PS_SAVEBENE'] = $SAVEBENE;

							$data['readonly'] = $readonly;
							$data['checkboxmode'] = $checkboxmode;

							// echo "<pre>";
							// print_r($data);die();
							
							
							//$sessionNameConfrim->tranferdatetype = $tranferdatetype;
								//	$sessionNameConfrim->ps_efdate = $PS_EFDATE ;
							$sessionNamespace = new Zend_Session_Namespace('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
							$sessionNamespace->data = $data;

							/////////

							// $resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);

							// if($TRANSFER_TYPE != 'ONLINE' && $this->view->error == false){
							// 	$sourceAccountType = $resWs['accountType'];
							// 	$infoWarning = $resWs['infoHoliday']['infoWarning']; //permintaan mayapada hari libur dikasih warning saja
							// 	$reffId = $paramTrxArr[0]['ReffId'];
							// 	if($isConfirmPage != 1){
							// 		$this->view->reffIdSend = $reffId;
							// 		$this->view->sourceAcctTypeGet = $sourceAccountType;
							// 	}
							// 	else{
							// 		$reffIdGet 		= $this->_getParam('reffId');
							// 		$this->view->reffIdSend = $reffIdGet;
									
							// 		$sourceAcctTypeGet 		= $this->_getParam('sourceAcctType');
							// 		$this->view->sourceAcctTypeGet = $sourceAcctTypeGet;
							// 	}
							// }
							// var_dump($this->_request->getParam('TRANSFER_TYPE'));die;

						//	if($this->_request->getParam('ERROR_AUTH')){
						//		$this->view->error == true;
					//			$isConfirmPage = false;
					//			$this->view->inquiry_error = 'Invalid Data';							
					//		} 
					
					
							if($SAVEBENE == '1'){
								
							$privibenelinkage = $this->view->hasPrivilege('BLBU');
					
					
							$acctStatus = 1;
							$dataAcct = $this->_db->fetchRow(
								$this->_db->select()
								->from(array('C' => 'M_CUSTOMER_ACCT'))
								->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
								->where("ACCT_STATUS = ?",$acctStatus)
								->limit(1)
							);
							$ACCT_NO = (!empty($dataAcct['ACCT_NO'])?$dataAcct['ACCT_NO']:'');
							$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE'])?$dataAcct['ACCT_TYPE']:'');
							
							$paramBene = array( 
															"FROM"      			=> 'B',
															 "ACBENEF"     			=> $ACBENEF,
															 "ACBENEF_CCY"    		=> 'IDR',
															 "ACBENEF_ALIAS"    	=> $ACBENEF_BANKNAME,
															 "ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
															 "BANK_CODE"    	 	=> $BANK_CODE,
															 "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
															 "_beneLinkage"    		=> $privibenelinkage,
															 "ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
															 "VALIDATE"     		=> '1',
															 "sourceAccountType"    => $ACCT_TYPE,
														);
							if($benefType == 1){
								$paramAdd = array("TRANSFER_TYPE"      	=>  'SKN',
																	 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
																	 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
																	 "ACBENEF_ADDRESS2"   	=> $ACBENEF_ADDRESS2,
																	 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZEN,
																	 "ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,

																	 "LLD_CATEGORY"  	=> $LLD_CATEGORY,
																	 "LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
																	 "LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
																	 "CITY_CODE"  	=> $CITY_CODE,
																	);
							}else{
								$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
																	 "ACBENEF_BANKNAME" 	=> '',
																	 "ACBENEF_ADDRESS1"   	=> '',
																	 "ACBENEF_CITIZENSHIP"  => '',
																	 "ACBENEF_RESIDENT"  	=> '',

																	"LLD_CATEGORY"  	=> '',
																	 "LLD_BENEIDENTIF"  	=> '',
																	 "LLD_BENENUMBER"  	=> '',
																	 "CITY_CODE"  	=> '',
																	);
							}


							$paramBene += array_merge($paramBene,$paramAdd);
							
							 $validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
							 $checkedParams = $validateACBENEF->check($paramBene);


							 if (!$validateACBENEF->isError())
							 {
								 $this->view->error == true;
								 $errorbene = 1;
								 
							 }
							
						}
							

							$confirmBtn = $this->_getParam('submitBtn');
							//echo '<pre>';
							//var_dump($this->_request->getParams());
							//var_dump($this->view->error);
							//var_dump($confirmBtn);die;

							if($this->view->error == false && ($confirmBtn == 'Submit' || $confirmBtn == 'Confirm'))	// payment data is valid
							{
								$confirmPage = $this->_getParam('confirmPage');
								//var_dump($confirmPage);
								//die('here');
								
								$payment 		= $validate->getPaymentInfo();
								
								//echo '<pre>';
							//var_dump($this->_request->getParams());
							//var_dump($this->view->error);
								//var_dump($isConfirmPage);
								//var_dump($USE_CONFIRM_PAGE);
								//var_dump($confirmPage);die;
								
								//if($confirmPage)
								if ($USE_CONFIRM_PAGE == true && $isConfirmPage == false && $confirmPage == false)
								{
									 //echo "<pre>";
									 //var_dump($param);die;
									// foreach ($AccArr as $key => $value) {
										// 
									// }
									$isConfirmPage 	= true;
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
									$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
									
									if($TRANSFER_TYPE == 'ONLINE')
										$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$ACCTSRC]["ACCT_ALIAS"];
									else
										$ACCTSRC_ALIAS = "";

									$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
									$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);
	
									$validate->__destruct();
									unset($validate);
	
									require_once 'General/Charges.php';
									$trfType		= $TRANSFER_TYPE;
									$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
									$paramCharges 	= array("accsrc" => $sessionNameConfrim->sourceAcct, "transferType" => $trfType);

									if($TRANSFER_TYPE == "ONLINE")
										$chargesAMT = 0;
									else
										$chargesAMT 	= $chargesObj->getCharges($paramCharges);

									$chargesCCY 	= $ACCTSRC_CCY;
								}
								else
								{
									$validate->__destruct();
									unset($validate);
	
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
	
									$param = array();
									$param['PS_SUBJECT'] 					= $PS_SUBJECT;
									$param['PS_CCY'] 						= 'IDR';
									$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
									$param['TRA_AMOUNT'] 					= $sessionNameConfrim->traAmount;
									$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;


									$acct_name = '';
									$acct_ccy = '';
									$acct_bankcode = '';
									foreach ($account as $key => $value) {
										if($value['account_number'] == $sessionNameConfrim->sourceAcct){
											$acct_name = $value['account_name'];
											$acct_ccy = (!empty($value['account_currency']) ? $value['account_currency'] : 'IDR');
											$acct_bankcode = $value['BANK_CODE'];
										}
									}
									
									$param['SOURCE_ACCOUNT'] 				= $sessionNameConfrim->sourceAcct;
									$param['SOURCE_ACCOUNT_NAME']			= $acct_name;
									$param['SOURCE_ALIAS_NAME']				= '-';
									$param['SOURCE_ACCOUNT_CCY']			= $acct_ccy;
									$param['SOURCE_ACCT_BANK_CODE']			= $acct_bankcode;
									$param['BENEFICIARY_ACCOUNT'] 			= $sessionNameConfrim->benefAcct;
									$param['BENEF_ACCT_BANK_CODE'] 			= $sessionNameConfrim->bank_code;
									$param['BENEFICIARY_ACCOUNT_CCY'] 		= $ACBENEF_CCY;
									$param['BENEFICIARY_ACCOUNT_NAME'] 		= $ACBENEF_BANKNAME;
									$param['BENEFICIARY_ALIAS_NAME'] 		= '-';
									$param['BENEFICIARY_BANK_NAME'] 		= $BANK_NAME;
									$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;
									$param['PHONE_NUMBER'] 					= $ACBENEF_PHONE;
									$param['ACBENEF_CITY']					= $ACBENEF_CITY;
									//tiara
									//$param['BENEFICIARY_BANK_CITY'] 		= $sessionNameConfrim->bene_city;
									$param['BENEFICIARY_BANK_CITY'] 		= '';
									$param['BENEFICIARY_ADDRESS'] 			= $ACBENEF_ADDRESS;
									$param['BENEFICIARY_ADDRESS2'] 			= $ACBENEF_ADDRESS2;


									$param['ACBENEF_CITIZEN'] 				= $sessionNameConfrim->nationality; //nationality
									$param['ACBENEF_RESIDENT'] 				= $sessionNameConfrim->citizenship; //citizenship
									$param['ACBENEF_CATEGORY'] 				= $sessionNameConfrim->bene_category;
									$param['ACBENEF_IDTYPE'] 				= $sessionNameConfrim->bene_idtype;
									$param['ACBENEF_IDNUM'] 				= $sessionNameConfrim->bene_idnum;
									
									$param['LLD_DESCRIPTION'] 				= $sessionNameConfrim->lld_desc;
									$param['LLD_IDENTICAL'] 				= $sessionNameConfrim->lld_identical;
									$param['LLD_RELATIONSHIP'] 				= $sessionNameConfrim->lld_relation;
									$param['LLD_PURPOSE'] 					= $sessionNameConfrim->lld_purpose;

									$param['PS_CATEGORY']					= 'OPEN TRANSFER';
									$param['PS_TYPE']						= '21';

						

									$param['TRANSFER_TYPE']		 			= $TRANSFER_TYPE;
									$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
									$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
									
									$param['TRANS_TYPE']					= $TRANSFER_TYPE;


									$param['SWIFT_CODE']					= $SWIFT_CODE;
									$param['CLR_CODE']						= $CLR_CODE;
									$param['PS_SAVEBENE']					= $SAVEBENE;
									
									//echo '<pre>';
									//var_dump($param);die;
									// foreach ($account as $key => $value) {
									// 			if($value['account_number'] == $sessionNameConfrim->acct_numb){
									// 				$acct_name = $value['account_name'];
									// 			}
									// 		}
									// 		if(empty($acct_name)){
									// 			$acct_name = '';
									// 		}

									// var_dump($this->_custSameUser);die;
									if($this->_custSameUser){


										if(!$this->view->hasPrivilege('PRLP')){
											// die('here');
											
											$errMessage = $this->language->_("Error: You don't have privilege to release payment");
											$this->view->error = true;
											$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
										}else{
											
											///google auth
											$challengeCode		= $this->_getParam('challengeCode');

											$inputtoken1 		= $this->_getParam('inputtoken1');
											$inputtoken2 		= $this->_getParam('inputtoken2');
											$inputtoken3 		= $this->_getParam('inputtoken3');
											$inputtoken4 		= $this->_getParam('inputtoken4');
											$inputtoken5 		= $this->_getParam('inputtoken5');
											$inputtoken6 		= $this->_getParam('inputtoken6');

											$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;


											$select3 = $this->_db->select()
												 ->from(array('C' => 'M_USER'));
											$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
											// $select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
											$data2 = $this->_db->fetchRow($select3);

											// $code = $param['googleauth'];
		

											$pga = new PHPGangsta_GoogleAuthenticator();
									    	 //var_dump($data2['GOOGLE_CODE']);
									    	 //var_dump($code);
									    	 //print_r($responseCode);die();
											$setting 		= new Settings();
											$google_duration 	= $setting->getSetting('google_duration');
									        if($pga->verifyCode($data2['GOOGLE_CODE'], $responseCode, $google_duration))
									        {
									        			$datatoken = array(
															'USER_FAILEDTOKEN' => 0
														);

														$wheretoken =  array();
														$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
														$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
														$data = $this->_db->update('M_USER',$datatoken,$wheretoken);

									        	$resultToken = $resHard['ResponseCode'] == '0000';
									        	$tokenAuth = true;
									        }else{
												$tokenFailed = $CustUser->setLogToken(); //log token activity

												$error = true;
												$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
												$this->view->popauth = true;
									        	$tokenAuth = false;	
									        	if ($tokenFailed === true) {
											 		$this->_redirect('/default/index/logout');
											 	}
									        }
										}

									//if token valid
									if ($tokenAuth) {

										//---------------------------------insert periodic-------------------------------------------
										if ($sessionNameConfrim->trfDateType == 3) {
											$repetition = $sessionNameConfrim->repetition;

											//daily
											if ($repetition == 1) {
												
												$repeatOn = $sessionNameConfrim->report_day;
												$endDate = $sessionNameConfrim->endDate;

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->efDate);
												$nextDate = $nextDate->format('Y-m-d');

												$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
											//var_dump($datenumb);die;												
																		foreach ($dataday as $key => $value) {
																			if($value== '0' || $value == '6'){
																					unset($dataday[$key]);
																			}
																		}
																			//var_dump($efDate);
																			//var_dump($dataday);
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value];
																				//var_dump($string);die;
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																				
																				$nextDatenow = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDatenow = $nextDatenow->format('Y-m-d');
																				$datenumb = date("w", strtotime($nextDatenow));
																				//var_dump($nextDatenow);
																				//var_dump($datenumb);
																				//var_dump($value);
																				//echo '-';
																				if ($datenumb == $value) {
																					$nextDateArr[] = $nextDatenow;
																				//	var_dump($nextDate);die('1');
																					break;

																				}
																				$nextDateArr[] = $nextDate;
																				//var_dump($nextDate);die('2');
																			//	break;
																		//	}
																		//	}
																			
																			
																			
																		}
																		//var_dump($nextDateArr);die;
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																			//if(!empty($nextDateArr['1'])){
																			//	$nextDate = $nextDateArr['1'];
																		//	}else{
																				$nextDate = $nextDateArr['0'];
																		//	}
																			
																			//$param['PS_EFDATE'] = $nextDateArr['0'];
																	}
																	
												
											}
											//weekly
											else if ($repetition == 2) {

												$repeatOn = $sessionNameConfrim->report_day;
												$endDate = $sessionNameConfrim->endDate;

												$repeatEvery = $repeatOn['0'];

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->efDate);
												$nextDate = $nextDate->format('Y-m-d');

												$dataday = $repeatOn;
												$arrday = array(
													'0' => 'sunday',
													'1' => 'monday',
													'2' => 'tuesday',
													'3' => 'wednesday',
													'4' => 'thursday',
													'5' => 'friday',
													'6' => 'saturday'
												);

												// get number of day in a week of startdate
												$datenumb = date("w", strtotime($nextDate));

												if (!empty($dataday)) {
													foreach ($dataday as $key => $value) {

														if ($datenumb == $value) {
															$nextDate = $nextDate;
															break;
														}
														else {
															$string = 'next ' . $arrday[$dataday[0]];
															// var_dump($string);die();
															$nextDate = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->efDate);
															$nextDate->modify($string);
															$nextDate = $nextDate->format('Y-m-d');

															break;
														}
													}
												}
											}
											//monthly
											else if ($repetition == 3) {
												$repeatEvery = $sessionNameConfrim->repeatEvery;
												$endDate = $sessionNameConfrim->endDate;

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->efDate);
												$nextDate = $nextDate->format('Y-m-d');

												// get date of startdate
												$datenumb = date("d", strtotime($nextDate));
											
												if($repeatEvery == 'last'){
												
												$nextDate = date("Y-m-t", strtotime($nextDate));
												
												//die;
												}else{
												
													if ($datenumb == $repeatEvery) {
														$nextDate = $nextDate;
													}
													else if($datenumb < $repeatEvery){
														$nextDateExplode = explode('-', $nextDate);

														$nextDateExplode[2] = $repeatEvery;

														$nextDate = implode('-', $nextDateExplode);
													}
													else if($datenumb > $repeatEvery) {

														$nextDateExplode = explode('-', $nextDate);

														$nextDateExplode[2] = $repeatEvery;

														$nextDate = implode('-', $nextDateExplode);

														$nextDate = date('Y-m-d', strtotime("+1 month", strtotime($nextDate)));
													}
												}
											}
										}else{
											
											$nextDate = $PS_EFDATE;
										}

										//if perioduc
										if ($sessionNameConfrim->trfDateType == 3) {
											// echo 'here';
											$start = $sessionNameConfrim->efDate;
											$STR_DATE = DateTime::createFromFormat('d/m/Y', $start);
											$START_DATE = $STR_DATE->format('Y-m-d');

											$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->endDate);
											$expDate = $EXPIRY_DATE->format('Y-m-d');

											if (!empty($sessionNameConfrim->trfDateType == 3 && !empty($sessionNameConfrim->endDate))) {
												$EF_DATE = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->endDate);
												$endDate = $EF_DATE->format('Y-m-d');
												// var_dump($endDate);die;
											}

											if (empty($sessionNameConfrim->endDate)) {
												$endDate = join('-', array_reverse(explode('/', date('d/m/Y'))));
												$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
												$endDate = $EF_DATE->format('Y-m-d');
											}
										}

										// var_dump($START_DATE);
										// var_dump($endDate);die;
										if ($sessionNameConfrim->trfDateType == 3) {
											$insertPeriodic = array(
												'PS_EVERY_PERIODIC' 	=> $repeatEvery,
												'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
												'PS_EVERY_PERIODIC_UOM' => $repetition, 	// 5: every day of, 6: every date of
												'PS_PERIODIC_STARTDATE' => $START_DATE,
												'PS_PERIODIC_ENDDATE'	=> $endDate,
												// 'PS_PERIODIC_NEXTDATE'	=> $nextDate,$param['PS_EFDATE']
												'PS_PERIODIC_NEXTDATE'	=> Application_Helper_General::convertDate($nextDate, $this->_dateDBFormat, $this->_dateDisplayFormat),

												'PS_PERIODIC_STATUS' 	=> 2,					// 2: INPROGRESS KALO BELUM BERAKHIR, 1: COMPLETE KALO SUDAH HABIS END DATE, 0: CANCEL
												'USER_ID' 				=> $this->_userIdLogin,
												'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
												'SESSION_TYPE'			=> 0,
											);
											// echo '<pre>';
											// print_r($insertPeriodic);die;

											$this->_db->insert('T_PERIODIC', $insertPeriodic);
											$psPeriodicID =  $this->_db->lastInsertId();
										}

										//select data beneficiary dari m_apikey
										$beneList = $this->_db->fetchAll(
											$this->_db->select()
												->from(array('A' => 'M_APIKEY'), array('*'))
												->where("A.VALUE = ? ", $sessionNameConfrim->benefAcct)
												->where("A.CUST_ID = ? ", $this->_custIdLogin)
												->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
												->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
										);

										foreach ($beneList as $key => $value) {
											$newBeneList[$value['FIELD']] = $value['VALUE'];
											$newBeneList['bank_code'] = $value['BANK_CODE'];
											$newBeneList['bank_name'] = $value['BANK_NAME'];
											$newBeneList['id'] = $value['APIKEY_ID'];
										}

										//select data source account dari m_apikey
										$sourceList = $this->_db->fetchAll(
											$this->_db->select()
												->from(array('A' => 'M_APIKEY'), array('*'))
												->where("A.VALUE = ? ", $sessionNameConfrim->sourceAcct)
												->where("A.CUST_ID = ? ", $this->_custIdLogin)
												->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
												->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
										);

										foreach ($sourceList as $key => $value) {
											$newSourceList[$value['FIELD']] = $value['VALUE'];
											$newSourceList['bank_code'] = $value['BANK_CODE'];
											$newSourceList['bank_name'] = $value['BANK_NAME'];
										}
										
										if ($sessionNameConfrim->trfDateType == 3) {

											if ($TRANSFER_TYPE == 'ONLINE') {
												$tfType = 0;
											}
											else if($TRANSFER_TYPE == 'RTGS'){
												$tfType = 1;
											}
											else if($TRANSFER_TYPE == 'SKN'){
												$tfType = 2;
											}
										
											//T_PERIODIC_DETAIL
											$insertPeriodicDetail = array(
												'PS_PERIODIC' 				=> $psPeriodicID,
												'SOURCE_ACCOUNT' 			=> $sessionNameConfrim->sourceAcct,
												'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
												'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
												'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
												'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
												'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
												'BENEFICIARY_ACCOUNT' 		=> $sessionNameConfrim->benefAcct,
												'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
												'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
												// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
												'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
												'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
												// 'TRA_AMOUNT' 				=> $remainsBalance,
												'TRA_MESSAGE' 				=> $TRA_MESSAGE,
												'TRA_MESSAGE_ADD'			=> '',
												'PAYMENT_TYPE'				=> 21,
												'TRANSFER_TYPE' 			=> $tfType,	 // 0 : Inhouse, 1: RTGS, 2: SKN
											);
											$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

											$report_day = $sessionNameConfrim->report_day;
											//var_dump($data);die;
											if (!empty($report_day)) {

												$filter_day = array_unique($report_day);
												foreach ($filter_day as $keyday => $valday) {
													$insertPeriodicday = array(
														'PERIODIC_ID' => $psPeriodicID,
														'DAY_ID'		=> $valday
													);

													$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
												}
											}
										}
										//---------------------------------end insert periodic-------------------------------------------
						
						
										
										$param['HISTORY_STATUS'] = array(1,5);
										$param['PS_STATUS'] = 5;
										$param['PS_PERIODIC'] = $psPeriodicID == null ? null : $psPeriodicID;
										$param['PS_EFDATE'] = Application_Helper_General::convertDate($nextDate, $this->_dateDBFormat, $this->_dateDisplayFormat);
										
										try
										{
											
											//ada by Hamdan for Periodic
											
											// End
											
											foreach ($account as $key => $value) {
													if($value['account_number'] == $sessionNameConfrim->acct_numb){
														$acct_name = $value['account_name'];
													}
												}

												$currentDate = date("Ymd");
												$seqNumber	 = strtoupper(Application_Helper_General::str_rand(6));
												$checkDigit  = '';
												$checkDigit  = strtoupper(substr(preg_replace("/[^a-zA-Z0-9]/", "", uniqid()),-2));
												$paymentID   = "M".$currentDate.$seqNumber.$checkDigit;

												// echo "<pre>";
												// var_dump($param);
												// var_dump($sessionNameConfrim);
												$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
												$request['sender_id'] = $AccArr['0']['SENDER_ID'];
												$request['AUTH_USER'] = $AccArr['0']['AUTH_USER'];
												$request['AUTH_PASS'] = $AccArr['0']['AUTH_PASS'];
												$request['signature'] = $AccArr['0']['SIGNATURE_KEY'];
												// $request['corporate_id_channel'] = $this->_custIdLogin;
												$request['beneficiary_account_number'] = $sessionNameConfrim->benefAcct;
												$request['beneficiary_account_name'] = $sessionNameConfrim->bene_name;
												$request['beneficiary_bank_code'] = $BANK_CODE;
												 $banklist = $this->_db->fetchAll(
													$this->_db->select()
														 ->from(array('A' => 'M_BANK_TABLE'))
														 ->where("A.BANK_CODE = ?", (string)$BANK_CODE)
												);

											    


												if(!empty($param['SOURCE_ACCOUNT'])){
														foreach ($AccArr as $key => $value) {
															if($value['ACCT_NO'] == $param['SOURCE_ACCOUNT']){
																// var_dump($value);
														//		$BANK_NAME = $value['BANK_NAME'];
																$ACCT_NAME = $value['ACCT_NAME'];
																$BANK_CODE = $value['BANK_CODE'];
															}
														}
													}

												$BANK_NAME = $banklist[0]['BANK_NAME'];

												$request['swift_code'] = $sessionNameConfrim->swift_code;
												

												if ($TRANSFER_TYPE == 'ONLINE') {
													$request['transfer_type'] = '0';
												}
												else if($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS'){

													if ($TRANSFER_TYPE == 'SKN') {
														$request['transfer_type'] = '2';
													}
													else{
														$request['transfer_type'] = '1';
													}

													if($sessionNameConfrim->citizenship == 'R'){
														$sessionNameConfrim->citizenship = '1';
													}else{
														$sessionNameConfrim->citizenship = '0';
													}

													if($sessionNameConfrim->nationality == 'W'){
														$sessionNameConfrim->nationality = '1';
													}else{
														$sessionNameConfrim->nationality = '0';
													}

													$request['beneficiary_phone_number'] = $sessionNameConfrim->bene_phone;
													$request['beneficiary_address1'] = $sessionNameConfrim->bene_address;
													$request['beneficiary_address2'] = $sessionNameConfrim->bene_address2;
													$request['beneficiary_district'] = $sessionNameConfrim->bene_city;
													$request['beneficiary_category'] = $sessionNameConfrim->bene_category;
													$request['beneficiary_citizenship'] = $sessionNameConfrim->citizenship;
													$request['beneficiary_nationality'] = $sessionNameConfrim->nationality;
													$request['beneficiary_email'] = $sessionNameConfrim->bene_email;
													// $request['beneficiary_idtype'] = $sessionNameConfrim->bene_idtype;
													// $request['beneficiary_idnum'] = $sessionNameConfrim->bene_idnum;
												}

												

												$request['transaction_id'] = $paymentID;
												$request['beneficiary_bank_name'] 	  = $banklist[0]['BANK_NAME'];
												// $request['source_bank_code'] = '0';
												$request['amount'] = $param['TRA_AMOUNT'];
												$request['source_account_number'] = $param['SOURCE_ACCOUNT'];
												$request['source_account_name'] = $ACCT_NAME;
												$request['source_bank_code'] 	  = $acct_bankcode;
												$request['companyId'] 		= $this->_custIdLogin;
												$request['userId'] 		= $this->_userIdLogin;
												$request['desc'] 		= $sessionNameConfrim->desc;

												// echo "<pre>";
												// print_r($request);
												$success = $clientUser->callapiservice('transaction',$request,'b2b/transfer/fund');

												$result  = $clientUser->getResult();

												// print_r($result);

												if ($TRANSFER_TYPE == 'ONLINE') {

													if ($request['beneficiary_bank_code'] == $request['source_bank_code']) {
														$serviceType = 6;
													}
													else{
														$serviceType = 6;
													}
												}
												else if($TRANSFER_TYPE == 'SKN'){
													$serviceType = 4;
												}
												else if($TRANSFER_TYPE == 'RTGS'){
													$serviceType = 5;
												}

												//insert T_DIGI_LOG
												$paramlog = array(
										            'DIGI_USER' => $this->_userIdLogin,
										            'DIGI_CUST' => $this->_custIdLogin,
										            'DIGI_BANK' => $request['beneficiary_bank_code'],
										            'DIGI_ACCOUNT' => $request['beneficiary_account_number'],
										            'DIGI_ERROR_CODE' => $result->error_code,
										            'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
										            'DIGI_SERVICE'  => $serviceType
										        );

										        $this->_helper->ServiceLog->serviceLog($paramlog);


												if($SAVEBENE){

													$frontendOptions = array ('lifetime' => 259200, 
                                  								'automatic_serialization' => true );
											        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
											        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
											        $cacheID = 'BANKTABLE';
											        
											        $bankArr = $cache->load($cacheID);
													//var_dump($select_int);
											        if(empty($bankArr)){
												        	$selectbank = $this->_db->select()
															->from(array('C' => 'M_BANK_TABLE'), array('*'));

														$databank = $this->_db->fetchAll($selectbank);

														foreach ($databank as $key => $value) {
															$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
														}
														
														$cache->save($bankArr,$cacheID);
											        }

													$content = array(
														'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
														'USER_ID_LOGIN' 			=> $this->_userIdLogin,
														'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
														'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
														'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
														'CURR_CODE' 				=> $ACBENEF_CCY,
														'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
														'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_CITIZEN,
														'BENEFICIARY_RESIDENT' 		=> $ACBENEF_RESIDENT,
														'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
														'BENEFICIARY_ADDRESS2' 		=> $ACBENEF_ADDRESS2,
														'BANK_NAME' 				=> $bankArr[$BENEFICIARY_BANK_CODE],
														'CLR_CODE' 					=> $CLR_CODE,
														'BANK_CODE' 				=> $BENEFICIARY_BANK_CODE,
														'SWIFT_CODE'    			=> $SWIFT_CODE,
														'BANK_CITY'			 		=> $BANK_CITY,
														'BENEFICIARY_TYPE' 			=> 2,
														'_beneLinkage'    			=> $privibenelinkage,
														'BENEFICIARY_PHONE'    		=> $ACBENEF_PHONE,
														'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
														'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
														'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
														'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE,
														'BENEFICIARY_ISAPPROVE'		=> '1'
													);
													
													if($benefType == '1'){
														$content['TYPE'] = 'SKN'; // 2 = domestic ( SKN/RTGS )

														$content['BENEFICIARY_TYPE'] = 2; // 2 = domestic ( SKN/RTGS )
														$content['CLR_CODE'] = $CLR_CODE;
														//echo '<pre>';
														//var_dump($content);die;
														//$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
														//$sessionNamespace->mode = 'Add';
														//$sessionNamespace->content = $content;
														//$this->_redirect('/predefinedbeneficiary/domesticcct/confirm');
													}
													else if($benefType == '2'){
														$content['TYPE'] = 'ONLINE'; // 2 = domestic ( SKN/RTGS )

														$content['BENEFICIARY_NAME'] = $checkedParams['ACBENEF_BANKNAME'];
														$content['BENEFICIARY_TYPE'] = 8; // 8 = online
														//$content['BANK_CODE'] = $CLR_CODE;
														$content['CLR_CODE'] = $CLR_CODE;
														//$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
														//$sessionNamespace->mode = 'Add';
														//$sessionNamespace->content = $content;
														//$this->_redirect('/predefinedbeneficiary/domesticcct/confirm');
													}
														$Beneficiary = new Beneficiary();
							// print_r($content);die;
							
														$add = $Beneficiary->add($content);
														Application_Helper_General::writeLog('BADA','Add Destination Account '.$content['BENEFICIARY_ACCOUNT']);
													
												}
												// echo "<pre>";
												// print_r($result);die;
												if ($result->error_code == '0000') {
													$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
														$sessionNameConfrim->bene_numb = $result->beneficiary_account_number;
														$sessionNameConfrim->bene_name = $request['beneficiary_account_name'];
														
														$sessionNameConfrim->acct_name	   = $acct_name;
														$sessionNameConfrim->amount	   = $request['amount'];
														$sessionNameConfrim->acct_numb = $request['source_account_number'];
														$sessionNameConfrim->bene_bank	   = $request['beneficiary_bank_code'];
														$sessionNameConfrim->bank_code	   = $request['source_bank_code'];
														$sessionNameConfrim->bank_name	   = $result->beneficiary_bank_name;
														$sessionNameConfrim->desc	   = $request['desc'];
														$sessionNameConfrim->result	   = $result->error_code;
														$sessionNameConfrim->createdate = $result->rs_datetime;
														$sessionNameConfrim->refid = $result->reference_id;
														$param['PS_STATUS'] = 5;
														$param['TRA_STATUS'] = 3;
														if ($TRANSFER_TYPE == 'ONLINE') {
															try {
																$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
																if (!empty($PS_NUMBER)) {
																	$sendProvider->isRepair = true;
																	$sendProvider	 = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
																	$param['isRepair'] = true;
																	$param['PS_NUMBER'] = $PS_NUMBER;
																} 
																$resProvider	 = $sendProvider->createPaymentOnline($param, $msg);
																$success = true;
															} catch (Exception $e) {
																$success = false;
																//var_dump($e);die;
															}
														}
														else if($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS'){
															try {
																$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
																if (!empty($PS_NUMBER)) {
																	$sendProvider	 = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
																	$sendProvider->isRepair = true;
																	$param['isRepair'] = true;
																	$param['PS_NUMBER'] = $PS_NUMBER;
																}
																$resProvider	 = $sendProvider->createPaymentSknRtgs($param, $msg);
																$success = true;
															} catch (Exception $e) {
																$success = false;
																//var_dump($e);die;
															}
														}
														
														

												}else{
													
														$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
														$sessionNameConfrim->bene_numb = $result->beneficiary_account_number;
														$sessionNameConfrim->bene_name = $request['beneficiary_account_name'];
														
														$sessionNameConfrim->acct_name	   = $acct_name;
														$sessionNameConfrim->amount	   = $request['amount'];
														$sessionNameConfrim->acct_numb = $request['source_account_number'];
														$sessionNameConfrim->bene_bank	   = $request['beneficiary_bank_code'];
														$sessionNameConfrim->bank_code	   = $request['source_bank_code'];
														$sessionNameConfrim->bank_name	   = $result->beneficiary_bank_name;
														$sessionNameConfrim->desc	   = $request['desc'];
														$sessionNameConfrim->result	   = $result->error_code;
														$sessionNameConfrim->createdate = $result->rs_datetime;
														$sessionNameConfrim->refid = $result->reference_id;
														if($result->error_code == '9999' || $result->error_code == '0062' || $result->error_code == '0061'){
																$param['TRA_STATUS'] = 9;
																$param['PS_STATUS'] = 9;
														}else{
																$param['TRA_STATUS'] = 4;
																$param['PS_STATUS'] = 6;
														}
														
														if ($TRANSFER_TYPE == 'ONLINE') {
															try {
																$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
																$resProvider	 = $sendProvider->createPaymentOnline($param, $msg);
																$success = true;
																Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
															} catch (Exception $e) {
																$success = false;
																var_dump($e);die;
															}
														}
														else if($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS'){
															try {
																$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
																$resProvider	 = $sendProvider->createPaymentSknRtgs($param, $msg);
																$success = true;
																Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
															} catch (Exception $e) {
																$success = false;
																var_dump($e);die;
															}
														}
													// var_dump($result);die;
													$this->view->error = true;
													$success = false;
													$this->view->report_service = true;
													$errorMsg = 'Payment Failed, Please Contact Administrator (99)';
												}

										}
										catch(Exception $e)
										{
											// var_dump($e);die;
											$this->view->error = true;
											$success = false;
											Application_Helper_General::exceptionLog($e);
											$errorMsg = 'Payment Failed, Please Contact Administrator (99)';
										}	
									}
									else{
										$this->view->error = true;
										$this->view->tokenError = true;
										$success = false;
										$resultPage = 'false';
										$errorMsg = 'Token Invalid Response Code';
									}
									
									$isConfirmPage = true;	
									$resultPage = (!empty($resultPage) ? false : true);			
									$this->view->success = $success;
									$this->view->success_msg = 'Payment Success';
									$this->view->error_msg = $errorMsg;

								}else{
									// die('gere');
									// echo "<pre>";
									// print_r($sessionNameConfrim->trfDateType);die();

									//---------------------------------insert periodic-------------------------------------------
									if ($sessionNameConfrim->trfDateType == 3) {
										$repetition = $sessionNameConfrim->repetition;

										//daily
										if ($repetition == 1) {
											
											$repeatOn = $sessionNameConfrim->report_day;
											$endDate = $sessionNameConfrim->endDate;

											//get from start date
											$nextDate = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->efDate);
											$nextDate = $nextDate->format('Y-m-d');




											
														$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
											//var_dump($datenumb);die;												
																		foreach ($dataday as $key => $value) {
																			if($value== '0' || $value == '6'){
																					unset($dataday[$key]);
																			}
																		}
																			//var_dump($efDate);
																			//var_dump($dataday);
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value];
																				//var_dump($string);die;
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																				
																				$nextDatenow = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDatenow = $nextDatenow->format('Y-m-d');
																				$datenumb = date("w", strtotime($nextDatenow));
																				//var_dump($nextDatenow);
																				//var_dump($datenumb);
																				//var_dump($value);
																				//echo '-';
																				if ($datenumb == $value) {
																					$nextDateArr[] = $nextDatenow;
																				//	var_dump($nextDate);die('1');
																					break;

																				}
																				$nextDateArr[] = $nextDate;
																				//var_dump($nextDate);die('2');
																			//	break;
																		//	}
																		//	}
																			
																			
																			
																		}
																		
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																			//var_dump($nextDateArr);
																			//if(!empty($nextDateArr['1'])){
																			//	$nextDate = $nextDateArr['1'];
																			//}else{
																				$nextDate = $nextDateArr['0'];
																		//	}
																			
																			//$param['PS_EFDATE'] = $nextDateArr['0'];
																	}
											
//											var_dump($nextDate);die;
										}
										//weekly
										else if ($repetition == 2) {

											$repeatOn = $sessionNameConfrim->report_day;
											$endDate = $sessionNameConfrim->endDate;
											$repeatEvery = $repeatOn['0'];
											//get from start date
											$nextDate = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->efDate);
											$nextDate = $nextDate->format('Y-m-d');

											$dataday = $repeatOn;
											$arrday = array(
												'0' => 'sunday',
												'1' => 'monday',
												'2' => 'tuesday',
												'3' => 'wednesday',
												'4' => 'thursday',
												'5' => 'friday',
												'6' => 'saturday'
											);

											// get number of day in a week of startdate
											$datenumb = date("w", strtotime($nextDate));

											if (!empty($dataday)) {
												foreach ($dataday as $key => $value) {

													if ($datenumb == $value) {
														$nextDate = $nextDate;
														break;
													}
													else {
														$string = 'next ' . $arrday[$dataday[0]];
														// var_dump($string);die();
														$nextDate = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->efDate);
														$nextDate->modify($string);
														$nextDate = $nextDate->format('Y-m-d');

														break;
													}
												}
											}
										}
										//monthly
										else if ($repetition == 3) {
											$repeatEvery = $sessionNameConfrim->repeatEvery;
											$endDate = $sessionNameConfrim->endDate;

											//get from start date
											$nextDate = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->efDate);
											$nextDate = $nextDate->format('Y-m-d');
											
											if($repeatEvery == 'last'){
												
												$nextDate = date("Y-m-t", strtotime($nextDate));
												
												//die;
												}else{
											
											// get date of startdate
											$datenumb = date("d", strtotime($nextDate));

											if ($datenumb == $repeatEvery) {
												$nextDate = $nextDate;
											}
											else if($datenumb < $repeatEvery){
												$nextDateExplode = explode('-', $nextDate);

												$nextDateExplode[2] = $repeatEvery;

												$nextDate = implode('-', $nextDateExplode);
											}
											else if($datenumb > $repeatEvery) {

												$nextDateExplode = explode('-', $nextDate);

												$nextDateExplode[2] = $repeatEvery;

												$nextDate = implode('-', $nextDateExplode);

												$nextDate = date('Y-m-d', strtotime("+1 month", strtotime($nextDate)));
											}
											}
										}
									}else{
										$nextDate = $PS_EFDATE;
									}

									//if perioduc
									if ($sessionNameConfrim->trfDateType == 3) {
										// echo 'here';
										$start = $sessionNameConfrim->efDate;
										$STR_DATE = DateTime::createFromFormat('d/m/Y', $start);
										$START_DATE = $STR_DATE->format('Y-m-d');

										$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->endDate);
										$expDate = $EXPIRY_DATE->format('Y-m-d');

										if (!empty($sessionNameConfrim->trfDateType == 3 && !empty($sessionNameConfrim->endDate))) {
											$EF_DATE = DateTime::createFromFormat('d/m/Y', $sessionNameConfrim->endDate);
											$endDate = $EF_DATE->format('Y-m-d');
											// var_dump($endDate);die;
										}

										if (empty($sessionNameConfrim->endDate)) {
											$endDate = join('-', array_reverse(explode('/', date('d/m/Y'))));
											$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
											$endDate = $EF_DATE->format('Y-m-d');
										}
									}

									// var_dump($START_DATE);
									// var_dump($endDate);die;
									if ($sessionNameConfrim->trfDateType == 3) {
										$insertPeriodic = array(
											'PS_EVERY_PERIODIC' 	=> $repeatEvery,
											'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
											'PS_EVERY_PERIODIC_UOM' => $repetition, 	// 5: every day of, 6: every date of
											'PS_PERIODIC_STARTDATE' => $START_DATE,
											'PS_PERIODIC_ENDDATE'	=> $endDate,
											'PS_PERIODIC_NEXTDATE'	=> Application_Helper_General::convertDate($nextDate, $this->_dateDBFormat, $this->_dateDisplayFormat),
											//'PS_PERIODIC_NEXTDATE'	=> $param['PS_EFDATE'],
											'PS_PERIODIC_STATUS' 	=> 2,					// 2: INPROGRESS KALO BELUM BERAKHIR, 1: COMPLETE KALO SUDAH HABIS END DATE, 0: CANCEL
											'USER_ID' 				=> $this->_userIdLogin,
											'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
											'SESSION_TYPE'			=> 0,
										);
										
										 //echo '<pre>';
										 //print_r($insertPeriodic);die;

										$this->_db->insert('T_PERIODIC', $insertPeriodic);
										$psPeriodicID =  $this->_db->lastInsertId();
									}

									//select data beneficiary dari m_apikey
									$beneList = $this->_db->fetchAll(
										$this->_db->select()
											->from(array('A' => 'M_APIKEY'), array('*'))
											->where("A.VALUE = ? ", $sessionNameConfrim->benefAcct)
											->where("A.CUST_ID = ? ", $this->_custIdLogin)
											->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
											->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
									);

									foreach ($beneList as $key => $value) {
										$newBeneList[$value['FIELD']] = $value['VALUE'];
										$newBeneList['bank_code'] = $value['BANK_CODE'];
										$newBeneList['bank_name'] = $value['BANK_NAME'];
										$newBeneList['id'] = $value['APIKEY_ID'];
									}

									//select data source account dari m_apikey
									$sourceList = $this->_db->fetchAll(
										$this->_db->select()
											->from(array('A' => 'M_APIKEY'), array('*'))
											->where("A.VALUE = ? ", $sessionNameConfrim->sourceAcct)
											->where("A.CUST_ID = ? ", $this->_custIdLogin)
											->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
											->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
									);

									foreach ($sourceList as $key => $value) {
										$newSourceList[$value['FIELD']] = $value['VALUE'];
										$newSourceList['bank_code'] = $value['BANK_CODE'];
										$newSourceList['bank_name'] = $value['BANK_NAME'];
									}
									
									if ($sessionNameConfrim->trfDateType == 3) {

										if ($TRANSFER_TYPE == 'ONLINE') {
											$tfType = 0;
										}
										else if($TRANSFER_TYPE == 'RTGS'){
											$tfType = 1;
										}
										else if($TRANSFER_TYPE == 'SKN'){
											$tfType = 2;
										}
									
										//T_PERIODIC_DETAIL
										$insertPeriodicDetail = array(
											'PS_PERIODIC' 				=> $psPeriodicID,
											'SOURCE_ACCOUNT' 			=> $sessionNameConfrim->sourceAcct,
											'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
											'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
											'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
											'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
											'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
											'BENEFICIARY_ACCOUNT' 		=> $sessionNameConfrim->benefAcct,
											'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
											'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
											// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
											'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
											'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
											// 'TRA_AMOUNT' 				=> $remainsBalance,
											'TRA_MESSAGE' 				=> $TRA_MESSAGE,
											'TRA_MESSAGE_ADD'			=> '',
											'PAYMENT_TYPE'				=> 21,
											'TRANSFER_TYPE' 			=> $tfType,	 // 0 : Inhouse, 1: RTGS, 2: SKN
										);
										$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

										$report_day = $sessionNameConfrim->report_day;
										//var_dump($data);die;
										if (!empty($report_day)) {

											$filter_day = array_unique($report_day);
											foreach ($filter_day as $keyday => $valday) {
												$insertPeriodicday = array(
													'PERIODIC_ID' => $psPeriodicID,
													'DAY_ID'		=> $valday
												);

												$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
											}
										}
									}
									//---------------------------------end insert periodic-------------------------------------------


									$param['HISTORY_STATUS'] = 1;
									$param['PS_STATUS'] = 17;
									$param['PS_PERIODIC'] = $psPeriodicID == null ? null : $psPeriodicID;
									$param['PS_EFDATE'] = Application_Helper_General::convertDate($nextDate, $this->_dateDBFormat, $this->_dateDisplayFormat);
									$param['PS_SAVEBENE'] = $SAVEBENE;
									//echo '<pre>';
									//var_dump($param);
									//var_dump($nextDate);die;

									if ($TRANSFER_TYPE == 'ONLINE') {

										 //echo '<pre>';
										 //print_r($PS_NUMBER);die();
										try {
											$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
											if (!empty($PS_NUMBER)) {
																	$sendProvider->isRepair = true;
																	$sendProvider	 = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
																	$param['isRepair'] = true;
																	$param['PS_NUMBER'] = $PS_NUMBER;
																} 
																
											$resProvider	 = $sendProvider->createPaymentOnline($param, $msg);
											$success = true;
										} catch (Exception $e) {
											$success = false;
											var_dump($e);die; 
										}
									}
									else if($TRANSFER_TYPE == 'SKN' || $TRANSFER_TYPE == 'RTGS'){
										 //echo '<pre>';
										 //print_r($param);die();
										try {
											$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
											if (!empty($PS_NUMBER)) {
																	$sendProvider->isRepair = true;
																	$sendProvider	 = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
																	$param['isRepair'] = true;
																	$param['PS_NUMBER'] = $PS_NUMBER;
																} 
																
											$resProvider	 = $sendProvider->createPaymentSknRtgs($param, $msg);
											$success = true;
										} catch (Exception $e) {
											$success = false;
											var_dump($e);die;
										}
									}

									Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
									$this->setbackURL('/opentransfer/domestic');
									$this->_redirect('/notification/success/index');
								}

								}
							}
							else
							{
								$errorMsg 		= $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								//var_dump($errorMsg);
								//var_dump($errorTrxMsg);die;
								$validate->__destruct();
								unset($validate);
								if($errorbene){
								$docErr = ''.$validateACBENEF->getErrorMsg();
								$validateACBENEF->__destruct();
								unset($validateACBENEF);
									$errMessage = $docErr;
								}
								if(empty($errMessage)){
									$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));	
								}
	
								$this->view->error 		= true;
								$this->view->ERROR_MSG	= $errMessage;
							}
						}
					}
				}
				else{
					//tambahn pentest
					$TRA_AMOUNT  		= $this->_getParam('TRA_AMOUNT');
					
					$PS_EFDATE_ORI_now  		= $this->_getParam('PS_EFDATE');
					if($PS_EFDATE_ORI_now > date('d/m/Y')){
						$TransferDate 	=  "2";
					}else{
						$TransferDate	=  "1";
						$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
					}
					
					$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
					$isConfirmPage 	= false;
				}
			}
			else{
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/opentransfer/domestic');
			}
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;	
			
			//tambahn pentest
			if($tranferdatetype =='1'){
				$this->view->PS_EFDATE 			= $PS_EFDATE;
			}
			elseif ($tranferdatetype =='3'){
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII;//						
			}
			else{
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;						
			}

			// if ($this->_getParam('back') == true)
			// {
			// 	$sessionNamespace = new Zend_Session_Namespace('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
			// 	$data = $sessionNamespace->data;
			// 	$this->view->back = '1';
			// 	$this->_redirect('opentransfer/domestic');
			// }

			// Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);

		}
		else
		{
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;
		}
		
		$this->view->infoWarning = $infoWarning;

		$TRA_MESSAGE_len = (isset($TRA_MESSAGE))? strlen($TRA_MESSAGE): 0;
		$TRA_REFNO_len 	 = (isset($TRA_REFNO))  ? strlen($TRA_REFNO)  : 0;

		$TRA_MESSAGE_len = 120 - $TRA_MESSAGE_len;
		$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;

		$settingObj = new Settings();
		$this->view->COT_SKN			= $settingObj->getSetting("cut_off_time_skn"	, "00:00:00");
		$this->view->COT_RTGS			= $settingObj->getSetting("cut_off_time_rtgs", "00:00:00");
		$this->view->COT_BI				= $settingObj->getSetting("cut_off_time_bi", "00:00:00");
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$this->view->AccArr 			= $AccArr;
		$this->view->transferTypeArr 	= array($this->_transfertype["desc"]["SKN"]  => $this->_transfertype["desc"]["SKN"],
												$this->_transfertype["desc"]["RTGS"] => $this->_transfertype["desc"]["RTGS"],
												'ONLINE' => 'ONLINE');
 		$this->view->citizenshipArr 	= $citizenshipArr;
		$this->view->residentArr 		= $residentArr;
		$this->view->lldCategoryArr 	= $lldCategoryArr;
		$this->view->lldIdenticalArr 	= $lldIdenticalArr;
		$this->view->lldRelationshipArr = $lldRelationshipArr;
		$this->view->lldPurposeArr 		= $lldPurposeArr;
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		$this->view->lldSenderIdentifArr  = $lldSenderIdentifArr;

		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		$this->view->PS_EFDATE 			= $PS_EFDATE;

		$this->view->TransferDate		= (isset($TransferDate))		? $TransferDate			: '1';

		if($PS_NUMBER){
			$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT_num))			? Application_Helper_General::displayMoneyplain($TRA_AMOUNT_num)			: '';
		}
		else{
			$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT_num))			? Application_Helper_General::displayMoneyplain($TRA_AMOUNT_num): '';
		}


		$this->view->TRA_MESSAGE 		= (isset($TRA_MESSAGE))			? $TRA_MESSAGE			: '';
		$this->view->TRA_REFNO 			= (isset($TRA_REFNO))			? $TRA_REFNO			: '';
		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;
		$this->view->TRA_REFNO_len		= $TRA_REFNO_len;

		$BANKNAME = '';
		$ACCT_NAME = '';
		if(!empty($ACCTSRC)){
			foreach ($AccArr as $key => $value) {
				if($value['ACCT_NO'] == $ACCTSRC){
					$BANKNAME = $value['BANK_NAME'];
					$ACCT_NAME = $value['ACCT_NAME'];
				}
			}
		}

		$this->view->ACCTSRC_view		= (isset($ACCTSRC))		? $ACCTSRC.' (IDR) / '.$BANKNAME.' / '.$ACCT_NAME			: '';
		$this->view->ACCTSRCNEW			= (isset($ACCTSRC))		? $ACCTSRC.' (IDR) / '.$BANKNAME			: '';
		$this->view->ACCTSRCTEXT			= (isset($ACCTSRCTEXT))		? $ACCTSRCTEXT			: '';
				
		if (!empty($pslipData))	
		{
			$this->view->ACCTSRC 			= (isset($ACCTSRC))				? $ACCTSRC				: '';
			$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
		}
		else{
			$this->view->ACCTSRC 			= (isset($sessionNameConfrim->sourceAcct))				? $sessionNameConfrim->sourceAcct				: '';
			$this->view->ACBENEF 			= (isset($sessionNameConfrim->benefAcct))				? $sessionNameConfrim->benefAcct				: '';
		}
		
		
		if (!empty($pslipData))	
		{
			$this->view->ACBENEF_BANKNAME	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
		}
		else{
			
			if($tranferdatetype == 3){
				$this->view->ACBENEF_BANKNAME	= $this->_getParam('ACBENEF_BANKNAME');
			}
			else{
				$this->view->ACBENEF_BANKNAME	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
			}
		}

		$this->view->ACCTSRC_BANKCODE 			= (isset($ACCTSRC_BANKCODE))			? $ACCTSRC_BANKCODE			: "";

		$this->view->notif 		= (isset($TRA_NOTIF))			? $TRA_NOTIF			: '';
		$this->view->sms_notif 		= (isset($TRA_SMS))			? $TRA_SMS			: '';
		$this->view->email_notif 		= (isset($TRA_EMAIL))			? $TRA_EMAIL			: '';
		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->ACBENEF_EMAIL 		= (isset($ACBENEF_EMAIL))		? $ACBENEF_EMAIL		: '';
		$this->view->CURR_CODE 			= (isset($ACBENEF_CCY))			? $ACBENEF_CCY			: '';
		$this->view->ACBENEF_ADDRESS 	= (isset($ACBENEF_ADDRESS))		? $ACBENEF_ADDRESS		: '';
		// $ACBENEF_ADDRESS2 = $this->_getParam('ACBENEF_ADDRESS2');
		//echo '<pre>';
		//var_dump($);
		$this->view->ACBENEF_ADD2 		= (isset($ACBENEF_ADDRESS2))		? $ACBENEF_ADDRESS2		: '';
		$this->view->ACBENEF_ADDRESS2 	= (isset($ACBENEF_ADDRESS2))		? $ACBENEF_ADDRESS2		: '';
 		$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: '';
 		$this->view->ACBENEF_CITIZEN    = (isset($ACBENEF_CITIZEN))		? $ACBENEF_CITIZEN		: '';
 		$this->view->CITY_CODE			= (isset($CITY_CODE))			? $CITY_CODE	: ''; 
 		$this->view->ACBENEF_PHONE		= (isset($ACBENEF_PHONE))			? $ACBENEF_PHONE	: ''; 
		$this->view->ACBENEF_CITY		= (isset($ACBENEF_CITY))		? $ACBENEF_CITY 		: '';	
		$this->view->ACBENEF_RESIDENT	= (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT		: '';
		$this->view->BANK_NAME			= (isset($BANK_NAME))			? $BANK_NAME			: '';
		$this->view->BANK_CODE 			= (isset($sessionNameConfrim->bank_code))			? $sessionNameConfrim->bank_code			: $BANK_CODE;
		$this->view->SWIFT_CODE 		= (isset($SWIFT_CODE))			? $SWIFT_CODE			: "";
//var_dump($TRANSFER_TYPE);die('s');  
		$this->view->TRANSFER_TYPE 		= (isset($TRANSFER_TYPE))		? $TRANSFER_TYPE		: "ONLINE";
		$this->view->CLR_CODE 			= (isset($CLR_CODE))			? $CLR_CODE				: "";   // "0140601";  // 0000000
		$this->view->LLD_CATEGORY 		= (isset($LLD_CATEGORY))		? $LLD_CATEGORY			: "1";
		$this->view->LLD_IDENTICAL 		= (isset($LLD_IDENTICAL))		? $LLD_IDENTICAL		: "";
		$this->view->LLD_RELATIONSHIP 	= (isset($LLD_RELATIONSHIP))	? $LLD_RELATIONSHIP		: "";
		$this->view->LLD_PURPOSE 		= (isset($LLD_PURPOSE))			? $LLD_PURPOSE			: "";
		$this->view->LLD_DESCRIPTION 	= (isset($LLD_DESCRIPTION))		? $LLD_DESCRIPTION		: "";
		$this->view->LLD_BENEIDENTIF 	= (isset($LLD_BENEIDENTIF))		? $LLD_BENEIDENTIF		: "";
		$this->view->LLD_BENENUMBER 	= (isset($LLD_BENENUMBER))		? $LLD_BENENUMBER		: "";
		$this->view->LLD_SENDERIDENTIF 	= (isset($LLD_SENDERIDENTIF))	? $LLD_SENDERIDENTIF	: "";
		$this->view->LLD_SENDERNUMBER 	= (isset($LLD_SENDERNUMBER))	? $LLD_SENDERNUMBER		: "";
		$this->view->CHARGES_AMT 		= (isset($chargesAMT))			? Application_Helper_General::displayMoney($chargesAMT)	: '';
		$this->view->CHARGES_CCY 		= (isset($chargesCCY))			? $chargesCCY			: '';
			if($isConfirmPage != ''){
					$this->view->confirmPage		= $isConfirmPage;
			}else{
				$this->view->confirmPage		= '0';
			}
		
		$this->view->resultPage			= $resultPage;
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;
		
		$this->view->PERIODIC_EVERY 	= (isset($PERIODIC_EVERY))		? $PERIODIC_EVERY		: '0';
		$this->view->PERIODIC_EVERYDATE = (isset($PERIODIC_EVERYDATE))	? $PERIODIC_EVERYDATE	: '0';
		$this->view->TrfDateType 		= (isset($TrfDateType))			? $TrfDateType			: '1';
		$this->view->TrfPeriodicType 	= (isset($TrfPeriodicType))		? $TrfPeriodicType		: ''; //$TrfPeriodicType;
		
		//periodic
		$this->view->PS_EFDATE1 = $efDate;
		$this->view->PS_ENDDATEPERIODIC = $endDate;
		$this->view->repetition = $repetition;
		$this->view->selectrepeat = $repeatEvery;
		$this->view->REPORT_DAY = $repeatOn;
		//readonly and checkbox when back page
		$this->view->readonly = (isset($readonly))		? $readonly		: '';
		$this->view->checkboxmode = (isset($checkboxmode))		? $checkboxmode		: '';
		// $back = $this->_getParam('process');
		// $this->view->back = (isset($back))		? $back		: '';


		$threshold_lld = $settingObj->getSetting("threshold_lld"	, 0);
		//var_dump($LLD_IDENTICAL);
		//var_dump($TRA_AMOUNT_num);
		// var_dump($this->view->ACCTSRC);die();
		if($TRANSFER_TYPE != 'ONLINE' && $ACBENEF_RESIDENT != 'R' && ((int)$TRA_AMOUNT_num >= (int)$threshold_lld)){
			$this->view->LLD_HI = '-';
		}
		else{ 
			$this->view->LLD_HI = 'none';
		}
		
		if(!$this->_request->isPost()) {
			//data saat confirm
			$sessionNamespace = new Zend_Session_Namespace('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
			$data = $sessionNamespace->data;
			
			// echo '<pre>';
			// var_dump($data);
			if (!empty($data)) {
				foreach ($data as $key => $value) {
					if($key == 'TRA_AMOUNT'){
							$this->view->$key = Application_Helper_General::displayMoneyplain($value);
					}else{
							$this->view->$key = $value;
					}
					
				}
				Zend_Session::namespaceUnset('moneymovement'.$this->_custIdLogin.$this->_userIdLogin);
			}
		} 
		

		if ($isConfirmPage == '1')
		{
		}
		else{
			Application_Helper_General::writeLog('CDFT','Viewing Create Single Payment Domestic');
		}

		/*------------------------------------approval matrix data--------------------------------------------------------*/

			$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;







$select = $this->_db->select()
                          ->from(array('MAB'=>'M_APP_BOUNDARY'))
                          ->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
                          ->where('MAB.CUST_ID = ?', (string)$cust_id)
                          ->group('BOUNDARY_ID');
                    // echo $select;
                  $result = $this->_db->fetchAll($select);

                  $dataArr = array();
                  // $alf = 'A';
                  // $alf++;
                  // ++$alf;
                  // print_r((int)$alf);
                  // $nogroup = sprintf("%02d", 1);
                  // print_r($result);die;
          
          
                  foreach($result as $row){
                    list($grouptype,$groupname,$groupnum) = explode("_",$row['GROUP_USER_ID']);
                    if($grouptype == 'N')
                      $name = 'Group '.trim($groupnum,'0');
                    else
                      $name = 'Special Group';

                    $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
                                // echo $selectUser;die;
                                ->query()->fetchall();

                    $userlist = '';

          $policythen = explode(' THEN ', $row['POLICY']);

          foreach ($policythen as $keythen => $valuethen) {
          

                    $policy = explode(' AND ', $valuethen);

                    foreach ($policy as $key => $value) {
                      $replaceVal = str_replace('(', '', $value);
                      $replaceVal = str_replace(')', '', $replaceVal);
                      $policy[$key] = $replaceVal;
                    }

                    foreach ($policy as $key => $value) {
                      if($value == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist))
                            $userlist .= $val['USER_ID'];
                          else
                            $userlist .= ', '.$val['USER_ID'];
                        }                         
                      }else{



                        $alphabet = array("A"=>1,"B"=>2,"C"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>26,);

                        $policyor = explode(' OR ', $value);

                        // print_r($policyor);die;
                        foreach ($policyor as $numb => $valpol) {
                          if($valpol == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist)){
                            $userlist .= $val['USER_ID'];
                          }
                          else{
                            $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }else{
                          $nogroup = sprintf("%02d", $alphabet[$valpol]);
                          // print_r($valpol);
                          $group = 'N_'.$cust_id.'_'.$nogroup;
                          $selectUser = $this->_db->select()
                                  ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                  ->where('GROUP_USER_ID = ?', $group)
                                  // echo $selectUser;die;
                                  ->query()->fetchall();  
                                // print_r($selectUser);  
                          foreach($selectUser as $val){
                            if(empty($userlist))
                              $userlist .= $val['USER_ID'];
                            else
                              $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }
                        

                      }
                    }
          
          }
                    // print_r($userlist);die;
                    // $nogroup = sprintf("%02d", $key+1);

                    // foreach($selectUser as $val){
                    //  if(empty($userlist))
                    //    $userlist .= $val['USER_ID'];
                    //  else
                    //    $userlist .= ', '.$val['USER_ID'];
                    // }

                    $arrTraType     = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
                    $arrTraType['19'] = 'Cash Pooling Same Bank';
                    $arrTraType['20'] = 'Cash Pooling Same Bank';
                    $arrTraType['23'] = 'Cash Pooling Other Bank';


          $userl = explode(', ',$userlist);
          $userlist = array_unique($userl);
          $userlistnew = '';
          
          foreach($userlist as $ky  => $val){
            //var_dump($ky);
            if($ky == 0){
            $userlistnew .= $val;
            //var_dump($userlistnew);
            }else{
            $userlistnew .= ', '.$val;
            }
          }

              $changepolicy = explode(" ", $row['POLICY']);
              $data = array();
              foreach ($changepolicy as $keypolicy => $valpolicy) {

                if ($valpolicy == 'AND') {
                  $valpolicy = '<span style="color: blue;">and</span>';
                }elseif ($valpolicy == 'OR') {
                  $valpolicy = '<span style="color: blue;">or</span>';
                }elseif ($valpolicy == 'THEN') {
                  $valpolicy = '<span style="color: blue;">then</span>';
                }

                $replacepolicy1 = str_replace('(', '<span style="color: blue;">(</span>', $valpolicy);
                $replacepolicy2 = str_replace(')', '<span style="color: blue;">)</span>', $replacepolicy1);

                $data[$keypolicy] = $this->language->_($replacepolicy2);
              }

              $valpolicy2 = implode(" ", $data);

                    $dataArr[] = array( 
                              'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
                              'CURRENCY' => $row['CCY_BOUNDARY'],
                              'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN'])." - ".Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
                              'GROUP_NAME' => $valpolicy2,
                              'USERS' => $userlistnew);
                  }

          //var_dump($dataArr);
                  // print_r($dataArr);die;
                 /* print_r($row);die();*/

                  $this->view->dataBoundary = $dataArr;

            /*--------------------------------END approval matrix data------------------------------------*/


        	//cek privilage
		$selectpriv = $this->_db->select()
				->from(array('A' => 'M_FPRIVI_USER'),array('FPRIVI_ID'))
				->where('A.FUSER_ID = ?' , (string) $this->_custIdLogin.$this->_userIdLogin)
				->query()->fetchAll();

		foreach ($selectpriv as $key => $value) {
			foreach ($value as $row => $data) {
				if ($data == 'MTSP') {
					$usermtsp = true;
				}
				if ($data == 'UTSP') {
					$userutsp = true;
				}
			}
		}

		if (!empty($usermtsp)) {

			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'),array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
				$select->where('A.T_GROUP = ?' , (string) $this->_custIdLogin);
				$select->where('A.T_TYPE = "2"');
			$select ->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
			
		}else if (!empty($userutsp)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'),array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
				$select->where('A.T_GROUP = ?' , (string) $this->_custIdLogin)
				->where('A.T_TARGET LIKE ?' , (string) '%'.$this->_userIdLogin.'%')
				->where('A.T_TYPE = "2"')
				->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		}

		if(!empty($usermtsp) && !empty($userutsp)){
			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'),array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
				$select->where('A.T_GROUP = ?' , (string) $this->_custIdLogin);
				$select->where('A.T_TYPE = "2"');
			$select ->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		}

		if($this->view->hasPrivilege('UTSP')){
                  	$templatePriv = 1;
                  }
                  else{
                  	$templatePriv = 0;
                  }

                  if($this->view->hasPrivilege('CDFT')){
					$priv = 1;                  	
                  }else{
                  	$priv = 0;
                  }

                  if($this->view->hasPrivilege('MTSP')){
					$usetemp = 1;                  	
                  }else{
                  	$usetemp = 0;
                  }
                  
                  $this->view->usetemplate = $usetemp;
                  $this->view->btntemplatePriv = $priv;
                  $this->view->templatePriv = $templatePriv;


	}

	public function acctsrcsuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

     //    $complist = $this->_db->fetchAll(
     //                $this->_db->select()
     //                     ->from(array('A' => 'M_USER'),array('CUST_ID'))
					// 	->where("A.CUST_ID = ? ", $this->_custIdLogin)
     //                     ->where("A.USER_ID = ? ", $this->_userIdLogin)
     //           );	
    	// $comp = "'";
    	// foreach ($complist as $key => $value) {
    	// 	$comp .= "','".$value['CUST_ID']."','";
    	// }
    	// $comp .= "'";

        $acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANK_TABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 // ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->order('A.APIKEY_ID ASC')
				);

        $masterData = $this->_db->fetchAll(
            $this->_db->select()
              ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
              ->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
              ->where('A.CUST_ID = ?', $this->_custIdLogin)
              ->where('A.USER_LOGIN = ?', $this->_userIdLogin)
              ->where('A.MAXLIMIT > 0')
              ->where('A.MAXLIMIT_OPEN = 1')
          );

		$account = array();
		foreach ($acctlist as $key => $value) {
			
			if($value['FIELD'] == 'account_number'){
				foreach ($masterData as $newkey => $valuemaster) {
				  if($value['VALUE'] == $valuemaster['ACCT_NO']){
				    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
				    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
					$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
					$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
					$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
					$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
					$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
				  }
				}
			}
		}

		$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$i]['ACCT_CCY'] = $value['account_currency'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];

			$acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
			$acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
			$acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
			$acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			$i++;
		}

		$newacct = array();
		foreach ($acct as $key => $value) {
			$newacct[$key]['value'] = $value['ACCT_NO'];
			$newacct[$key]['bankcode'] = $value['BANK_CODE'];
			if(empty($value['ACCT_ALIAS'])){
				$newacct[$key]['label'] = $value['ACCT_NO']." (IDR) / ".$value['ACCT_BANK'];
			}else{
				$newacct[$key]['label'] = $value['ACCT_NO']." (IDR) / ".$value['ACCT_BANK']." / ".$value['ACCT_ALIAS'];
			}
			
		}

        $searchTerm = $_GET['term'];

        $accData = array();
        foreach ($newacct as $key => $value) {

        	if (strpos(strtolower($value['label']), strtolower($searchTerm)) !== false) {
		        $data['value'] = $value['label'];
        		$data['bankcode']    = $value['bankcode'];
        		$data['labeltext']    = $value['value'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <span>'.$value['label'].'</span>
		        </a>';
		        array_push($accData, $data);
        	}
        }
        echo json_encode($accData);

	}


	public function acctsrcsuggestiondomAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

     //    $complist = $this->_db->fetchAll(
     //                $this->_db->select()
     //                     ->from(array('A' => 'M_USER'),array('CUST_ID'))
					// 	->where("A.CUST_ID = ? ", $this->_custIdLogin)
     //                     ->where("A.USER_ID = ? ", $this->_userIdLogin)
     //           );	
    	// $comp = "'";
    	// foreach ($complist as $key => $value) {
    	// 	$comp .= "','".$value['CUST_ID']."','";
    	// }
    	// $comp .= "'";

    	$Settings = new Settings();
    	$dombank = $Settings->getSetting('bank_support_domestic');

        $acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 // ->where("A.CUST_ID IN (".$comp.")")
						  ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE IN (".$dombank.")")
						 ->order('A.APIKEY_ID ASC')
				);
		
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}
		
		$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$i]['ACCT_CCY'] = $value['account_currency'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];

			$acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
			$acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
			$acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
			$acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			$i++;
		}

		$newacct = array();
		foreach ($acct as $key => $value) {
			$newacct[$key]['value'] = $value['ACCT_NO'];
			$newacct[$key]['bankcode'] = $value['BANK_CODE'];
			$newacct[$key]['bankname'] = $value['BANK_NAME'];
			$newacct[$key]['ACCT_ALIAS'] = $value['ACCT_ALIAS'];
			if(empty($value['ACCT_ALIAS'])){
				$newacct[$key]['label'] = $value['ACCT_NO']." (IDR) / ".$value['ACCT_BANK'];
			}else{
				$newacct[$key]['label'] = $value['ACCT_NO']." (IDR) / ".$value['ACCT_BANK']." / ".$value['ACCT_ALIAS'];
			}
			
		}
        

        $searchTerm = $_GET['term'];

        $accData = array();
        foreach ($newacct as $key => $value) {

        	if (strpos(strtolower($value['label']), strtolower($searchTerm)) !== false) {
		        $data['value'] = $value['label'];
        		$data['bankcode']    = $value['bankcode'];
				$data['bankname']    = $value['bankname'];
        		$data['labeltext']    = $value['value'];
        		$data['aliasname']    = $value['ACCT_ALIAS'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <span>'.$value['label'].'</span>
		        </a>';
		        array_push($accData, $data);
        	}
        } 
        echo json_encode($accData);

	}

	public function bankdataAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

	    $bankcode = $this->_getParam('bankcode');


	    $banklist = $this->_db->fetchAll(
			$this->_db->select()
				 ->from(array('A' => 'M_BANK_TABLE'))
				 ->where("A.BANK_CODE = ?", (string)$bankcode)
		);

	    $result = array();
	    if (!empty($banklist)) {
			$result['bankname'] = $banklist[0]['BANK_NAME'];
			$result['swiftcode'] = $banklist[0]['SWIFT_CODE'];
			$result['clrcode'] = $banklist[0]['CLR_CODE'];
	    }

	    echo json_encode($result);
	}


	public function banksuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $banklist = $this->_db->fetchAll(
			$this->_db->select()
				 ->from(array('A' => 'M_BANK_TABLE'), array('*'))
		);

	    $bankdata = array();
	    if (!empty($banklist)) {
	    	foreach ($banklist as $key => $value) {
				$bankdata[$key]['swiftcode'] = $value['SWIFT_CODE'];
				$bankdata[$key]['clrcode'] = $value['CLR_CODE'];
				$bankdata[$key]['value'] = $value['BANK_NAME'];
				$bankdata[$key]['bankcode'] = $value['BANK_CODE'];
	    	}
	    }

        $searchTerm = $_GET['term'];

        $bankDataList = array();
        foreach ($bankdata as $key => $value) {

        	if (strpos(strtolower($value['value']), strtolower($searchTerm)) !== false) {
		        $data['swiftcode'] = $value['swiftcode'];
        		$data['clrcode']    = $value['clrcode'];
        		$data['value']    = $value['value'];
        		$data['bankcode']    = $value['bankcode'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <span>'.$value['value'].'</span>
		        </a>';
		        array_push($bankDataList, $data);
        	}
        }

        echo json_encode($bankDataList);

	}

	public function acctbenefsuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

     //    $complist = $this->_db->fetchAll(
     //                $this->_db->select()
     //                     ->from(array('A' => 'M_USER'),array('CUST_ID'))
					// 	 ->where("A.CUST_ID = ? ", $this->_custIdLogin)
     //                     ->where("A.USER_ID = ? ", $this->_userIdLogin)
     //           );	
    	// $comp = "'";
    	// foreach ($complist as $key => $value) {
    	// 	$comp .= "','".$value['CUST_ID']."','";
    	// }
    	// $comp .= "'";

    	$bankcode = $_GET['bankcode'];
    	$sourceacct = $_GET['sourceacct'];
		$sourceacct = explode(' ',$sourceacct);
		$sourceacct = $sourceacct[0];
		
		if($this->view->hasPrivilege('BLBU')){
			
			
			if (empty($bankcode)) {
    
			$acctlist = $this->_db->fetchAll(
				$this->_db->select()
					 ->from(array('A' => 'M_BENEFICIARY'))
					 ->join(array('C' => 'M_BENEFICIARY_USER'),'A.BENEFICIARY_ID = C.BENEFICIARY_ID',array())					  
					 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
					 ->where("C.USER_ID = ?",$this->_userIdLogin)
					 // ->where("C.CUST_ID IN (".$comp.")")
					 ->where("C.CUST_ID = ?",$this->_custIdLogin)
					 
			);
    	}
    	else{
    		if (!empty($sourceacct)) {
    		
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BENEFICIARY'))
						 ->join(array('C' => 'M_BENEFICIARY_USER'),'A.BENEFICIARY_ID = C.BENEFICIARY_ID',array())
						 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
						 ->where("C.USER_ID = ?",$this->_userIdLogin)
						 // ->where("C.CUST_ID IN (".$comp.")")
						 ->where("C.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->where("A.BENEFICIARY_ACCOUNT != ?", (string)$sourceacct)
						 
				);
    		}
    		else{
    			
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BENEFICIARY'))
						 ->join(array('C' => 'M_BENEFICIARY_USER'),'A.BENEFICIARY_ID = C.BENEFICIARY_ID',array())
						 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
						 ->where("C.USER_ID = ?",$this->_userIdLogin)
						 // ->where("C.CUST_ID IN (".$comp.")")
						 ->where("C.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 //->where("A.BENEFICIARY_ACCOUNT != ?", (string)$sourceacct)
						 
				);
    		}
    	}
			
			
			
			
			
		}else{
		
    	if (empty($bankcode)) {
    	/*	$acctlist = $this->_db->fetchAll(
				$this->_db->select()
					 ->from(array('A' => 'M_APIKEY'))
					 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
					  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
					 ->where("A.CUST_ID IN (".$comp.")")
					 ->order('A.APIKEY_ID ASC')
			); */
			
			$acctlist = $this->_db->fetchAll(
				$this->_db->select()
					 ->from(array('A' => 'M_BENEFICIARY'))
					 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
					 // ->where("A.CUST_ID IN (".$comp.")")
					 ->where("A.CUST_ID = ?",$this->_custIdLogin)
					 
			);
    	}
    	else{
    		if (!empty($sourceacct)) {
    		 /*	$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->where("A.VALUE != ?", (string)$sourceacct)
						 ->order('A.APIKEY_ID ASC')
				); */
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BENEFICIARY'))
						 // ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
						 ->where("A.BENEFICIARY_ACCOUNT != ?", (string)$sourceacct)
						 
				);
    		}
    		else{
    			/*$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->order('A.APIKEY_ID ASC')
				); */
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BENEFICIARY'))
						 // ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 //->where("A.BENEFICIARY_ACCOUNT != ?", (string)$sourceacct)
						 
				);
    		}
    	}
		
		}
		

		$newacct = array();
		foreach ($acctlist as $key => $value) {
			$newacct[$key]['value'] = $value['BENEFICIARY_ACCOUNT'];
			$newacct[$key]['bankcode'] = $value['BANK_CODE'];
			$newacct[$key]['benefname'] = $value['BENEFICIARY_NAME'];
			//$newacct[$key]['label'] = $value['ACCT_NO']." [".$value['ACCT_BANK']."] - ".$value['ACCT_NAME'];
			if(empty($value['BENEFICIARY_ALIAS'])){
				$newacct[$key]['label'] = $value['BENEFICIARY_ACCOUNT']." (".$value['CURR_CODE'].") / ".$value['BENEFICIARY_NAME'];
			}else{
				$newacct[$key]['label'] = $value['BENEFICIARY_ACCOUNT']." (".$value['CURR_CODE'].") / ".$value['BENEFICIARY_NAME']." / ".$value['BENEFICIARY_ALIAS'];
			}
			
		}
        

        $searchTerm = $_GET['term'];

        $accData = array();
        foreach ($newacct as $key => $value) {

        	if (strpos(strtolower($value['label']), strtolower($searchTerm)) !== false) {
		        $data['value'] = $value['value'];
        		$data['bankcode'] = $value['bankcode'];
        		$data['benefname'] = $value['benefname'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <span>'.$value['label'].'</span>
		        </a>';
		        array_push($accData, $data);
        	}
        }

        echo json_encode($accData);

	}


	public function acctbenefsuggestionmmAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

     //    $complist = $this->_db->fetchAll(
     //                $this->_db->select()
     //                     ->from(array('A' => 'M_USER'),array('CUST_ID'))
					// 	 ->where("A.CUST_ID = ? ", $this->_custIdLogin)
     //                     ->where("A.USER_ID = ? ", $this->_userIdLogin)
     //           );	
    	// $comp = "'";
    	// foreach ($complist as $key => $value) {
    	// 	$comp .= "','".$value['CUST_ID']."','";
    	// }
    	// $comp .= "'";

    	$bankcode = $_GET['bankcode'];
    	$sourceacct = $_GET['sourceacct'];
    	$tratype = $_GET['tratype'];
		$sourceacct = explode(' ',$sourceacct);
		$sourceacct = $sourceacct[0];
		
		if($tratype == 'ONLINE'){
			$benetype = '8';
		}else if($tratype == 'SKN'){
			$benetype = '2';
		}else if($tratype == 'RTGS'){
			$benetype = '2';
		}else{
			$benetype = '8';
		}


		if($this->view->hasPrivilege('BLBU')){
			
			
			if (empty($bankcode)) {
    
			$acctlist = $this->_db->fetchAll(
				$this->_db->select()
					 ->from(array('A' => 'M_BENEFICIARY'))
					 ->join(array('C' => 'M_BENEFICIARY_USER'),'A.BENEFICIARY_ID = C.BENEFICIARY_ID',array())					  
					 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
					 ->where("A.BENEFICIARY_ISREQUEST_DELETE = ?",'0')
					 ->where("A.BENEFICIARY_TYPE = ?",$benetype)
					 
					 ->where("C.USER_ID = ?",$this->_userIdLogin)
					 ->where("C.CUST_ID = ?",$this->_custIdLogin)
					 
			);
    	}
    	else{
    		if (!empty($sourceacct)) {
    		
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BENEFICIARY'))
						 ->join(array('C' => 'M_BENEFICIARY_USER'),'A.BENEFICIARY_ID = C.BENEFICIARY_ID',array())
						 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
						 ->where("A.BENEFICIARY_TYPE = ?",$benetype)
						 ->where("C.USER_ID = ?",$this->_userIdLogin)
						 ->where("C.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->where("A.BENEFICIARY_ACCOUNT != ?", (string)$sourceacct)
						 
				);
    		}
    		else{
    			
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BENEFICIARY'))
						 ->join(array('C' => 'M_BENEFICIARY_USER'),'A.BENEFICIARY_ID = C.BENEFICIARY_ID',array())
						 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
						 ->where("A.BENEFICIARY_TYPE = ?",$benetype)
						 ->where("C.USER_ID = ?",$this->_userIdLogin)
						 // ->where("C.CUST_ID IN (".$comp.")")
						 ->where("C.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 //->where("A.BENEFICIARY_ACCOUNT != ?", (string)$sourceacct)
						 
				);
    		}
    	}
			
			
			
			
			
		}else{
		
    	if (empty($bankcode)) {
    	/*	$acctlist = $this->_db->fetchAll(
				$this->_db->select()
					 ->from(array('A' => 'M_APIKEY'))
					 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
					  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
					 ->where("A.CUST_ID IN (".$comp.")")
					 ->order('A.APIKEY_ID ASC')
			); */
			
			$acctlist = $this->_db->fetchAll(
				$this->_db->select()
					 ->from(array('A' => 'M_BENEFICIARY'))
					 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
					 ->where("A.BENEFICIARY_TYPE = ?",$benetype)
					 // ->where("A.CUST_ID IN (".$comp.")")
					 ->where("A.CUST_ID = ?",$this->_custIdLogin)
					 
			);
    	}
    	else{
    		if (!empty($sourceacct)) {
    		 /*	$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->where("A.VALUE != ?", (string)$sourceacct)
						 ->order('A.APIKEY_ID ASC')
				); */
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BENEFICIARY'))
						 // ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->where("A.BENEFICIARY_TYPE = ?",$benetype)
						 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
						 ->where("A.BENEFICIARY_ACCOUNT != ?", (string)$sourceacct)
						 
				);
    		}
    		else{
    			/*$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->order('A.APIKEY_ID ASC')
				); */
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BENEFICIARY'))
						 // ->where("A.CUST_ID IN (".$comp.")")
						 ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BENEFICIARY_TYPE = ?",$benetype)
						 ->where("A.BENEFICIARY_ISAPPROVE = ?",'1')
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 //->where("A.BENEFICIARY_ACCOUNT != ?", (string)$sourceacct)
						 
				);
    		}
    	}
		
		}
		
		$newacct = array();
		foreach ($acctlist as $key => $value) {
			$newacct[$key]['value'] = $value['BENEFICIARY_ACCOUNT'];
			$newacct[$key]['bankcode'] = $value['BANK_CODE'];
			$newacct[$key]['benefname'] = $value['BENEFICIARY_NAME'];
			$newacct[$key]['benefemail'] = $value['BENEFICIARY_EMAIL'];
			$newacct[$key]['benefaddress'] = $value['BENEFICIARY_ADDRESS'];
			$newacct[$key]['benefaddress2'] = $value['BENEFICIARY_ADDRESS2'];
			$newacct[$key]['benefphone'] = $value['BENEFICIARY_PHONE'];
			$newacct[$key]['benefcategory'] = $value['BENEFICIARY_CATEGORY'];
			$newacct[$key]['benefcitizenship'] = $value['BENEFICIARY_RESIDENT'];
			$newacct[$key]['benefnationality'] = $value['BENEFICIARY_CITIZENSHIP'];
			$newacct[$key]['benefcity'] = $value['BENEFICIARY_CITY'];
			$newacct[$key]['benefidnumber'] = $value['BENEFICIARY_ID_NUMBER'];
			$newacct[$key]['benefidtype'] = $value['BENEFICIARY_ID_TYPE'];
			//$newacct[$key]['label'] = $value['ACCT_NO']." [".$value['ACCT_BANK']."] - ".$value['ACCT_NAME'];
			if(empty($value['BENEFICIARY_ALIAS'])){
				$newacct[$key]['label'] = $value['BENEFICIARY_ACCOUNT']." (".$value['CURR_CODE'].") / ".$value['BENEFICIARY_NAME'];
			}else{
				$newacct[$key]['label'] = $value['BENEFICIARY_ACCOUNT']." (".$value['CURR_CODE'].") / ".$value['BENEFICIARY_NAME']." / ".$value['BENEFICIARY_ALIAS'];
			}
			
		}
        

        $searchTerm = $_GET['term'];

        $accData = array();
        foreach ($newacct as $key => $value) {

        	if (strpos(strtolower($value['label']), strtolower($searchTerm)) !== false) {
		        $data['value'] = $value['value'];
        		$data['bankcode'] = $value['bankcode'];
				$data['benefname'] = $value['benefname'];
				$data['benefemail'] = $value['benefemail'];
				$data['benefaddress'] = $value['benefaddress'];
				$data['benefaddress2'] = $value['benefaddress2'];
				$data['benefphone'] = $value['benefphone'];
				$data['benefcategory'] = $value['benefcategory'];
				$data['benefcitizenship'] = $value['benefcitizenship'];
				$data['benefnationality'] = $value['benefnationality'];
				$data['benefcity'] = $value['benefcity'];
				$data['benefidnumber'] = $value['benefidnumber'];
				$data['benefidtype'] = $value['benefidtype'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <span>'.$value['label'].'</span>
		        </a>';
		        array_push($accData, $data);
        	}
        }

		echo json_encode($accData);

	}

	public function generateTransactionID(){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(5));
		$trxId = 'FR'.$currentDate.$this->_custIdLogin.$seqNumber;

		return $trxId;
	}

}
  