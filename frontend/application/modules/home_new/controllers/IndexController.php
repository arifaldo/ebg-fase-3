<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'General/SystemBalance.php';
require_once 'General/CustomerUser.php';

class home_IndexController extends Application_Main {



	public function indexAction()
    {
    	$this->_helper->_layout->setLayout('newlayout');
    	$frontendOptions = array ('lifetime' => 900,
								  'automatic_serialization' => true );
		$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
		$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
		$cacheID = 'BAL'.$this->_custIdLogin.$this->_userIdLogin;
		$cache->remove($cacheID);

		// $cekdata = $db->select()
  //               	->FROM(array('T' => 'T_TRANSACTION_REPORT'), array('*'))
  //               	->WHERE("T.ID = ?", $row['ID'])
		// 			->WHERE("T.STATUS IN ('FTS','CTF')")
  //               	->QUERY()
  //               	->FETCHALL();
                	// echo "string";

		if(!$result = $cache->load($cacheID))
		{
			$data = $this->_db->fetchAll(
						$this->_db->select()->distinct()
							 ->from(array('A' => 'M_CUSTOMER_ACCT'))
							 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
							 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
							 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
							 ->where("A.ACCT_STATUS = 1")
							 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
							 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
							 ->where("C.MAXLIMIT > 0")
							 ->order('B.GROUP_NAME DESC')
							 ->order('A.ORDER_NO ASC')
					);

			$tempList = array();

			foreach($data as $row){
				$systemBalance = new SystemBalance($this->_custIdLogin,$row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']));
				$systemBalance->setFlag(false);
				$systemBalance->checkBalance();

				$arr = array();
				$arr['acct_no'] = $row['ACCT_NO'];
				$arr['acct_name'] = $row['ACCT_ALIAS_NAME'];
				$arr['acct_ccy'] = $row['CCY_ID'];
				$arr['balance'] = $row['CCY_ID']." ".Application_Helper_General::displayMoney($systemBalance->getEffectiveBalance());
				array_push($tempList, $arr);
			}
			$cache->save($tempList,$cacheID);
			$result = $tempList;
		}

		$this->view->accountList = $result;

		// print_r($result);die;
		$suggest = array();
		foreach ($result as $key => $value) {
					$suggest[] = $value['acct_no'];
		}

		$statementavg = $this->_db->select()
                	->FROM(array('T' => 'T_STATEMENT'), array('T.STAT_TYPE','T.STAT_DATE', 'TOTAL' => 'SUM(T.STAT_AMOUNT)','MONTH' => "CONCAT(MONTHNAME(T.STAT_DATE),' ',YEAR(T.STAT_DATE))"))
                	->WHERE("T.ACCT_NO IN(?)", $suggest)
                	// ->GROUP("T.STAT_TYPE,T.STAT_DATE")
                	->GROUP("T.`STAT_TYPE`,YEAR(T.STAT_DATE), MONTH(T.STAT_DATE)")
                	->order("T.STAT_DATE ASC");

        $dataavg = $this->_db->select()->FROM(array('a' => $statementavg),array('AVG' =>'AVG(TOTAL)','STAT_DATE'))
        					->GROUP('a.STAT_DATE')
        					->order("a.STAT_DATE ASC")
        					->QUERY()
                	->FETCHALL();


		$statement = $this->_db->select()
                	->FROM(array('T' => 'T_STATEMENT'), array('T.STAT_TYPE','T.STAT_DATE', 'TOTAL' => 'SUM(T.STAT_AMOUNT)','MONTH' => "CONCAT(MONTHNAME(T.STAT_DATE),' ',YEAR(T.STAT_DATE))"))
                	->WHERE("T.ACCT_NO IN(?)", $suggest)
                	// ->GROUP("T.STAT_TYPE,T.STAT_DATE")
                	->GROUP("T.`STAT_TYPE`,YEAR(T.STAT_DATE), MONTH(T.STAT_DATE)")
                	->order("T.STAT_DATE ASC")
                	->QUERY()
                	->FETCHALL();
                	// echo $statement;die;

         $dchartdata = $this->_db->select()
                	->FROM(array('T' => 'T_BALANCE'), array('T.PLAFOND','T.ACCT_NO'))
                	->WHERE("T.ACCT_NO IN(?)", $suggest)
                	->where("DATE(T.PLAFOND_END) = DATE(NOW())")
                	->QUERY()
                	->FETCHALL();

          $totaldchart = $this->_db->select()
                	->FROM(array('T' => 'T_BALANCE'), array('TOTAL'=>'SUM(T.PLAFOND)'))
                	->WHERE("T.ACCT_NO IN(?)", $suggest)
                	->where("DATE(T.PLAFOND_END) = DATE(NOW())")
                	->QUERY()
                	->FETCHALL();
                	// print_r($totaldchart);die;
                	// echo $dchartdata;die;
        	$cashin = array();
            $cashout = array();
            $everage = array();
            $month = array();
         if(!empty($statement)){
         foreach ($statement as $key => $value) {
         	if($value['STAT_TYPE'] == 'C'){
         		$cashin[] = (int)$value['TOTAL'];
         		$in = $value['TOTAL'];
         		$month[] = $value['MONTH'];
         	}else if($value['STAT_TYPE'] == 'D'){
         		$cashout[] = (int)0-(int)$value['TOTAL'];
         		$out = $value['TOTAL'];
         	}
         }

     	 }else{
     	 	$cashin = array();
            $cashout = array();

            $month = array();
     	 }

     	 if(!empty($dataavg)){
         foreach ($dataavg as $key => $value) {
         		$everage[] = (int)$value['AVG'];
         }
     	}else{
     		$everage = array();
     	}

         $dchart = array();
         $total = 0;
         if(!empty($totaldchart)){
         	$total = $totaldchart['0']['TOTAL'];
         }
         if(!empty($dchartdata)){
         // print_r($suggest);
         foreach ($suggest as $keys => $val) {
         		// print_r($val);die;

         		// print_r($dchartdata);die;
         	foreach ($dchartdata as $key => $value) {
         		// print_r($value);die;
         		if($value['ACCT_NO'] == $val){
         			// print_r($value);die;
         				$dchart[$keys][] = $value['ACCT_NO'];
         				$dchart[$keys][] = ((int)$value['PLAFOND']/(int)$total)*100;
         		}
         	}

         	if(empty($dchart[$keys])){
         		$dchart[$keys][] = $val;
         		$dchart[$keys][] = (int)0;
         	}

         }

     	}else{
     		$dchart = array();
     	}
         // print_r($dchart);die;




         // var_dump($everage);die;
         // echo $cashin;die;
         $this->view->dchartdata = $dchart;
         $this->view->month = $month;
         $this->view->cashin = $cashin;
         $this->view->cashout = $cashout;
         $this->view->everage = $everage;


		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

		$param = array('USER_ID'=>$userId, 'CUST_ID'=>$custId);
		$model = new accountstatement_Model_Accountstatement();
		$userAcctInfo = $model->getCustomerAccountKurs($param);
		// print_r($userAcctInfo);die;
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];


		$selectkurs = $this->_db->select()
                            ->from('M_KURS', array('*'))
                            ->where('DATE(KURS_DATE) = DATE(NOW())');
        $kurslist  =  $this->_db->fetchAll($selectkurs);

        if(empty($kurslist)){
        	$svcInquiry = new Service_Inquiry($userId, $accountNo, $accountType, $accountCcy);
			$result		= $svcInquiry->rateInquiry();
			if($result['ResponseCode']=='00'){
        		foreach ($result['DataList'] as $key => $value) {
        			try{
        			$param = array(
        						'KURS_CCY' => $value['currency'],
        						'KURS_VALUE' => Application_Helper_General::displayNumber($value['sell']),
        						'KURS_DATE' => new Zend_Db_Expr("now()")
        					);

        			$this->_db->insert('M_KURS',$param);
        			}catch(Exception $e)
					{

					}
        		}

        	}
        }

        $selectkurs = $this->_db->select()
                            ->from('M_KURS', array('*'));
        $kursdata  =  $this->_db->fetchAll($selectkurs);
        $this->view->kurslist =$kursdata;

		$report =  $this->_db->fetchAll(
						$this->_db->select()->distinct()
							 ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID'))
							 ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
							 ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin))
					 );
		// echo $report;die;
		$output = '';
		$secret_key = 'cmdemo';
	    $secret_iv = 'democm';
	        // hash
	    $key = hash('sha256', $secret_key);

	    $iv = substr(hash('sha256', $secret_iv), 0, 16);
	    $encrypt_method = "AES-256-CBC";
	    if(!empty($report)){
		foreach ($report as $keys => $value) {

	        $string = $value['ID'];
	        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	        $output = base64_encode($output);
	        $report[$keys]['CODE'] = $output;
		}

		$this->view->reportlist = $report;
    	$reportId = Application_Helper_Array::simpleArray($report, "ID");


    	$selectcolomn = $this->_db->select()
                            ->from('T_REPORT_COLOMN', array('COLM_NAME','COLM_FIELD','COLM_TYPE','COLM_REPORD_ID'))
                            ->where('COLM_REPORD_ID IN (?)', $reportId)
                            ->order('COLM_ID ASC');
        $colomnArr  =  $this->_db->fetchAll($selectcolomn);
         // echo $selectcolomn;die;
        $this->view->colomnlist = $colomnArr;
    	}
        $privi = $this->_priviId;
        $userLogin 	= $this->_custIdLogin;


        $selectwidget = $this->_db->select()
                            ->from('M_WIDGET', array('*'))
                            ->where('CUST_ID = ?', $this->_custIdLogin)
                            ->where('USER_ID = ?', $this->_userIdLogin);

        $hidewidget  =  $this->_db->fetchAll($selectwidget);

        if(!empty($hidewidget)){
        	foreach ($hidewidget as $key => $value) {
        		$str = 'widget'.$value['WIDGET_ID'];
        		// print_r($str);
        		$this->view->$str = 'hide';
        	}
        }


        if (!empty($privi))
		{
			$changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
			$listAutModuleArr = $changeModulePrivilegeObj->getAuthorizeModule();

			$whPriviID = "'".implode("','", $listAutModuleArr)."'";

			$selectChanges = $this->_db->select()
							->from(array('G'=>'T_GLOBAL_CHANGES'),array('DISPLAY_TABLENAME'))
							->where('(CHANGES_STATUS = '.$this->_db->quote('WA').' OR CHANGES_STATUS = '.$this->_db->quote('RR').')')
							->where('G.CHANGES_FLAG = ?', 'F')
							->where('G.CUST_ID = ?', $userLogin)
							->where('G.MODULE IN ('.$whPriviID.')')
							->order('DISPLAY_TABLENAME ASC')
							->query()->fetchAll();

			$waitingAutorization = array();
			foreach($selectChanges as $row)
			{
				if(isset($waitingAutorization[$row['DISPLAY_TABLENAME']]))
				{
					$waitingAutorization[$row['DISPLAY_TABLENAME']] += 1;
				}
				else
				{
					$waitingAutorization[$row['DISPLAY_TABLENAME']] = 1;
				}
			}
			$this->view->waitingAutorization = $waitingAutorization;
		}

		$paramPayment = array("WA" 				=> true,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );

		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


		$selectuser	= $this->_db->select()
								->from(array('A'	 		=> 'M_APP_GROUP_USER'),
								   array('GROUP_USER_ID' 	=> 'A.GROUP_USER_ID',
								   		 'USER_ID' => 'A.USER_ID',
								   		 'CUST_ID' => 'A.CUST_ID'
								   		)
								   )
							->join(array('B' => 'M_APP_BOUNDARY_GROUP'), 'A.GROUP_USER_ID = B.GROUP_USER_ID', array())
							->join(array('C' => 'M_APP_BOUNDARY'), 'C.BOUNDARY_ID = B.BOUNDARY_ID', array('BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
								   		 'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
								   		 'CCY_BOUNDARY' => 'C.CCY_BOUNDARY'
								   		))
		->where("A.USER_ID 	= ?" , (string)$this->_userIdLogin)
		->where('A.CUST_ID = ?', (string)$this->_custIdLogin);

		$datauser = $this->_db->fetchAll($selectuser);


		$rangesql = '';
		foreach ($datauser as $key => $value){
			$rangesql .= " (P.PS_TOTAL_AMOUNT BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
			if(!empty($datauser[$key+1])){
				$rangesql .= " OR ";
			}
		}
		$rangesql .= " OR ";
		foreach ($datauser as $key => $value){
			$rangesql .= " (P.PS_TYPE = '14' AND P.PS_REMAIN BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
			if(!empty($datauser[$key+1])){
				$rangesql .= " OR ";
			}
		}
		$rangesql .= " OR ";
		foreach ($datauser as $key => $value){
			$rangesql .= " (P.PS_TYPE = '15' AND P.PS_REMAIN BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
			if(!empty($datauser[$key+1])){
				$rangesql .= " OR ";
			}
		}

		$select   = $CustUser->getPayment($paramPayment);

		$select->where('P.PS_STATUS = ?', (string)$this->_paymentstatus["code"]["waitingforapproval"]);

		if(!empty($rangesql))
		{
		    if(!empty($datauser)){
				$select->where($rangesql);
		    }

			$select->where("P.PS_NUMBER NOT IN ('P20160921NIG0UTYHC','P20160920PMQ6Q1CEC') ");

		}

		$dataSQL = $this->_db->fetchAll($select);
		$this->view->waitingApproval = $dataSQL;


    }



     public function columnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'cmdemo';
        $secret_iv = 'democm';
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $string = $this->_getParam('report');


         $report = openssl_decrypt(base64_decode($tblName), $encrypt_method, $key, 0, $iv);

        $deleteWhere['ID = ?'] = $report;
        $deleteWherecolm['COLM_REPORD_ID = ?'] = $report;

		$this->_db->delete('T_REPORT_GENERATOR',$deleteWhere);
		$this->_db->delete('T_REPORT_COLOMN',$deleteWherecolm);


        echo $optHtml;
    }


    public function hidedashAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $param = array(
        		'USER_ID' => $this->_userIdLogin,
        		'CUST_ID' => $this->_custIdLogin,
        		'WIDGET_ID' => $tblName
        	);
             			$this->_db->insert('m_widget',$param);

        echo $optHtml;
    }


    public function inquiryAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $id = $this->_getParam('id');
        $ccy = $this->_getParam('data');

        $systemBalance = new SystemBalance($this->_custIdLogin,$id,Application_Helper_General::getCurrNum($ccy));
				$systemBalance->setFlag(false);
				$systemBalance->checkBalance();
		$optHtml = $systemBalance->getEffectiveBalance();
		 // $optHtml = '200000';

        echo $optHtml;
    }

}
