<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';


class backendgroup_AddController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);

		$privil = $this->_db->select()
			->FROM('M_BPRIVILEGE');


		$setting = new Settings();

		$type = $setting->getSetting('system_type');
		if ($type == '1') {
			$privil->where('BPRIVI_MODE IN (0,1)');
		} else if ($type == '2') {
			$privil->where('BPRIVI_MODE IN (0,2)');
		}
		$privil->ORDER('BPRIVI_DESC ASC');

		$privil = $privil->query()->fetchall();
		// ->query()->fetchall();

		$filterArr = array(
			'groupid'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'groupname'  => array('StripTags', 'StringTrim'),
		);
		$validators = array(
			'groupid' => array(
				'NotEmpty',
				'Alnum',
				array('StringLength', array('min' => 3, 'max' => 6)),
				array('Db_NoRecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_ID')),
				array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_ID')),
				'messages' => array(
					$this->language->_('Can not be empty'),
					$this->language->_('Must be alphabet or numeric values'),
					$this->language->_('Group ID length minimal 3, maximal 6 character'),
					$this->language->_('Group ID already in use. Please use another'),
					$this->language->_('Group ID already suggested. Please use another')
				)
			),
			'groupname' => array(
				'NotEmpty',
				array('StringLength', array('min' => 1, 'max' => 32)),
				array('Db_NoRecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_DESC')),
				array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_DESC')),
				'messages' => array(
					$this->language->_('Can not be empty'),
					$this->language->_('Template Code length must not be more than 32'),
					$this->language->_('Group Name already in use. Please use another'),
					$this->language->_('Group Name already suggested. Please use another')
				)
			)
		);


		$zf_filter = new Zend_Filter_Input($filterArr, $validators, $this->_request->getParams());
		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		$re = '%^[a-zA-Z0-9+\- ]*$%';
		$cekspecialchar = preg_match($re, $zf_filter->groupname) ? 1 : 0;
		//Zend_Debug::dump($submit);die;
		if ($submit && $this->view->hasPrivilege('BGCR')) {
			$groupid = $this->_getParam('groupid');
			// $callcenter = $this->_getParam('callcenter');
			$groupname = $this->_getParam('groupname');
			$cekcheckbox = 0;
			//echo '<pre>';
			//var_dump($this->_request->getParams());die;
			$arrpriv = array();
			foreach ($privil as $row) {

				if ($this->_getParam($row['BPRIVI_ID']) == 1) {
					//echo 'here';die;
					$cekcheckbox++;
					//var_dump($row['BPRIVI_ID']);
					$arrpriv[] = $row['BPRIVI_ID'];
					$this->view->$row['BPRIVI_ID'] = '1';
				}
			}
			$this->view->datatemp = $arrpriv;


			//Zend_Debug::dump($cekcheckbox);die;
			if ($cekcheckbox == 0) {
				$this->view->errcheckbox = $this->language->_('Please Select at least 1 Privilege');
				//die;
			}
			// print_r($cekcheckbox);echo '<br>';
			// print_r($groupname);echo '<br>';
			// print_r($groupid);echo '<br>';
			// print_r($zf_filter->getMessages());
			// die;
			if ($zf_filter->isValid() && $cekcheckbox != 0 && $groupname != $groupid && $cekspecialchar) {
				// die;
				$cekcheckbox = 0;
				$info = "Backend Group";
				$change_id = $this->suggestionWaitingApproval('Backend Group', $info, $this->_changeType['code']['new'], null, 'M_BGROUP', 'TEMP_BGROUP', $groupid, $groupname);
				$this->_db->beginTransaction();
				try {
					// if($callcenter=='1'){
					// 	$callcenter = '1';
					// }else{
					// 	$callcenter = '0';
					// }

					$data = array(
						'CHANGES_ID' => $change_id,
						'BGROUP_ID' => $groupid,
						'BGROUP_CALL' => '0',
						'BGROUP_DESC' => $groupname,
						'BGROUP_STATUS' => '1',
						'CREATED' => new Zend_Db_Expr('now()'),
						'CREATEDBY' => $this->_userIdLogin,
					);

					$this->_db->insert('TEMP_BGROUP', $data);

					foreach ($privil as $row) {
						if ($this->_getParam($row['BPRIVI_ID']) == 1) {
							$cekcheckbox++;
							$data2 = array(
								'CHANGES_ID' => $change_id,
								'BPRIVI_ID' => $row['BPRIVI_ID'],
								'BGROUP_ID' => $groupid
							);
							$this->_db->insert('TEMP_BPRIVI_GROUP', $data2);
						}
					}


					$this->_db->commit();
					Application_Helper_General::writeLog('BGCR', 'Creating Backend group');
					$this->setbackURL('/' . $this->_request->getModuleName() . '/index/');
					$this->_redirect('/notification/submited/index');
				} catch (Exception $e) {
					// print_r($e);die;
					$this->_db->rollBack();
				}
			} else {

				$groupid = $this->_getParam('groupid');
				$groupname = $this->_getParam('groupname');

				$this->view->groupid 	= $groupid;
				$this->view->groupname 	= $groupname;

				if ($groupname && $groupid && $groupname == $groupid) {
					$this->view->xgroupname = $this->language->_("Group ID and Group name must not to be same");
				} else if (!$cekspecialchar) {
					$this->view->xgroupname = $this->language->_('Must not contain special characters');
					$zf_filter->groupname = '';
				} else {
					foreach ($zf_filter->getMessages() as $key => $err) {
						$xxx = 'x' . $key;
						$this->view->$xxx = $this->displayError($err);
					}
				}
			}
			Application_Helper_General::writeLog('BGCR', 'Create Backend group (' . $groupid . ')');
		} else {
			Application_Helper_General::writeLog('BGCR', 'Viewing Create Backend group page');
		}

		$this->view->groupid = (isset($groupid)) ? $groupid : '';
		$this->view->groupname = (isset($groupname)) ? $groupname : '';
		// $this->view->callcenter = (isset($callcenter)) ? $callcenter : '';
		$this->view->privil = $privil;
		$this->view->modulename = $this->_request->getModuleName();
	}
}
