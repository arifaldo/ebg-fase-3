<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
class Customer_RequestresetpassController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');


		$listId = $this->_db->select()
			->from(
				array('M_CUSTOMER'),
				array('CUST_ID')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();
		$this->view->listCustId = Application_Helper_Array::listArray($listId, 'CUST_ID', 'CUST_ID');

		$fields = array(
			'Company Code'  			=> array(
				'field' => 'a.CUST_ID',
				'label' => $this->language->_('Company Code'),
				'sortable' => true
			),
			'Company Name'  			=> array(
				'field' => 'b.CUST_NAME',
				'label' => $this->language->_('Company Name'),
				'sortable' => true
			),
			'User Id'  			=> array(
				'field' => 'a.USER_ID',
				'label' => $this->language->_('User ID'),
				'sortable' => true
			),
			'User Name'  			=> array(
				'field' => 'a.USER_FULLNAME',
				'label' => $this->language->_('User Name'),
				'sortable' => true
			),
			'Lock Status'  			=> array(
				'field' => 'USER_ISLOCKED',
				'label' => $this->language->_('User is Locked'),
				'sortable' => true
			),
			'Lock Reason'  			=> array(
				'field' => 'a.USER_LOCKREASON',
				'label' => $this->language->_('Lock Reason'),
				'sortable' => true
			),
			'Request Made By'  			=> array(
				'field' => 'USER_RPWD_ISBYBO',
				'label' => $this->language->_('Request Made By'),
				'sortable' => true
			),
			'Recommended Method'  			=> array(
				'field' => 'USER_ISEMAILPWD',
				'label' => $this->language->_('Recommended Method'),
				'sortable' => true
			),
		);

		$filterlist = array('COMP_ID', 'COMP_NAME', 'USER_ID', 'USER_ISLOCKED', 'USER_FULLNAME');

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby');
		$sortDir = $this->_getParam('sortdir');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;


		$filters = array(
			'filter' 	   	=>  array('StringTrim', 'StripTags'),
			'COMP_ID'      =>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'COMP_NAME'  	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'USER_ID'     	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'USER_ISLOCKED'     	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'REQUESTED_BY'     	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
		);
		$validators = array(
			'filter'	  => array('allowEmpty' => true),
			'COMP_ID'	  => array('allowEmpty' => true),
			'COMP_NAME'	  => array('allowEmpty' => true),
			'USER_ID'		  => array('allowEmpty' => true),
			'USER_ISLOCKED'		  => array('allowEmpty' => true),
			'REQUESTED_BY'		  => array('allowEmpty' => true),
		);

		// $dataParam = array('COMP_ID','COMP_NAME','USER_ID','USER_ISLOCKED','REQUESTED_BY', "USER_FULLNAME");
		$dataParam = array('COMP_ID', 'COMP_NAME', 'USER_ID', 'USER_ISLOCKED', 'USER_FULLNAME');
		$dataParamValue = array();
		if ($this->_request->getParam('wherecol')) {
			$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
			$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);

			foreach ($dataParam as $no => $dtParam) {

				if (!empty($this->_request->getParam('wherecol'))) {
					$dataval = $this->_request->getParam('whereval');
					// print_r($dataval);
					$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if ($value == "SUGGESTED_DATE") {
							$order--;
						}
						if ($dtParam == $value) {
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				}
			}
		}

		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

		$filter 		= $this->_getParam('filter');

		$cust_id   			= html_entity_decode(($zf_filter->cust_id)    ? $zf_filter->cust_id     : $zf_filter->getEscaped('COMP_ID'));
		$companyName    = html_entity_decode(($zf_filter->companyName)  ? $zf_filter->companyName   : $zf_filter->getEscaped('COMP_NAME'));
		$user_id 				= html_entity_decode(($zf_filter->user_id))	? $zf_filter->user_id : $zf_filter->getEscaped('USER_ID');
		$lockstatus		= html_entity_decode(($zf_filter->lockstatus))	? $zf_filter->lockstatus : $zf_filter->getEscaped('USER_ISLOCKED');
		$user_name		= html_entity_decode(($zf_filter->user_name))	? $zf_filter->user_name : $zf_filter->getEscaped('REQUESTED_BY');
		$this->view->currentPage = $page;

		$arrUSER_ISEMAILPWD = array(1 => "Email", 0 => "Postal Mail");
		$arrUSER_RPWD_ISBYBO = array(0 => "Frontend", 1 => "Backend");
		$arrUSER_ISLOCKED = array(0 => "no", 1 => "yes");
		// var_dump($arrUSER_ISEMAILPWD);die;
		$this->view->ISEMAIL = $arrUSER_ISEMAILPWD;
		$this->view->ISBYBO = $arrUSER_ISEMAILPWD;
		$this->view->ISLOCKED = $arrUSER_ISEMAILPWD;


		// $caseUSER_ISEMAILPWD = "(CASE a.USER_ISEMAILPWD ";
		// foreach($arrUSER_ISEMAILPWD as $key=>$val)
		// {
		// 	$caseUSER_ISEMAILPWD .= " WHEN ".$key." THEN '".$val."'";
		// }
		// $caseUSER_ISEMAILPWD .= " END)";

		// $caseUSER_RPWD_ISBYBO = "(CASE a.USER_RPWD_ISBYBO ";
		// foreach($arrUSER_RPWD_ISBYBO as $key=>$val)
		// {
		// 	$caseUSER_RPWD_ISBYBO .= " WHEN ".$key." THEN '".$val."'";
		// }
		// $caseUSER_RPWD_ISBYBO .= " END)";

		// $caseUSER_ISLOCKED = "(CASE a.USER_ISLOCKED ";
		// foreach($arrUSER_RPWD_ISBYBO as $key=>$val)
		// {
		// 	$caseUSER_ISLOCKED .= " WHEN ".$key." THEN '".$val."'";
		// }
		// $caseUSER_ISLOCKED .= " END)";

		// $caseUSER_FULLNAME = "(CASE a.USER_FULLNAME ";
		// foreach($arrUSER_FULLNAME as $key=>$val)
		// {
		// 	$caseUSER_FULLNAME .= " WHEN ".$key." THEN '".$val."'";
		// }
		// $caseUSER_FULLNAME .= " END)";

		$select = $this->_db->SELECT()
			->FROM(array('a' => 'M_USER'), array('a.CUST_ID', 'a.USER_ID', 'a.USER_FULLNAME', 'USER_ISLOCKED' => 'a.USER_ISLOCKED', 'a.USER_LOCKREASON', 'USER_RPWD_ISBYBO' => 'a.USER_RPWD_ISBYBO', 'USER_ISEMAILPWD' => 'a.USER_ISEMAILPWD', 'USER_RPWD_ISBYUSER'))
			->JOIN(array('b' => 'M_CUSTOMER'), 'a.CUST_ID = b.CUST_ID', array('b.CUST_NAME'))
			->WHERE('b.CUST_STATUS != 3 ')
			->WHERE('a.USER_STATUS != 3 ')
			->WHERE('a.USER_RPWD_ISBYBO = 1 ');

		if ($filter == $this->language->_('Set Filter') || $filter == $this->language->_('Reject') || $filter == $this->language->_('Approve')) {

			if ($cust_id) {
				$this->view->cust_id    = $cust_id;
				$select->where("UPPER(a.CUST_ID) LIKE " . $this->_db->quote('%' . $cust_id . '%'));
			}

			if ($companyName) {
				$this->view->companyName	= $companyName;
				$select->where("UPPER(b.CUST_NAME) LIKE " . $this->_db->quote('%' . $companyName . '%'));
			}

			if ($user_id) {
				$this->view->user_id  = $user_id;
				$select->where("UPPER(a.USER_ID) LIKE " . $this->_db->quote('%' . $user_id . '%'));
			}

			if ($user_name) {
				$this->view->user_name  = $user_name;
				$select->where("UPPER(a.USER_FULLNAME) LIKE " . $this->_db->quote('%' . $user_name . '%'));
			}

			if (isset($lockstatus) && $lockstatus != -1) {
				$this->view->lockstatus      = $lockstatus;

				$tempLockStatus = array_keys($arrUSER_ISLOCKED, $lockstatus);

				$select->where("USER_ISLOCKED = ?", $tempLockStatus[0]);
			}
		}
		$select->order($sortBy . ' ' . $sortDir);

		$select = $this->_db->fetchAll($select);

		if ($filter == $this->language->_('Reject') || $filter == $this->language->_('Approve')) { //Zend_Debug::dump($this->_getAllParams());die;
			$temp = 0;
			foreach ($select as $row) {
				if ($this->_getParam($row['USER_ID']) == $row['CUST_ID']) {
					if ($filter == $this->language->_('Approve')) {
						$CustomerUser = new CustomerUser($row['CUST_ID'], $row['USER_ID']);
						$CustomerUser->approveRequestResetPassword();
						$temp = 1;
						//Application_Helper_General::writeLog('CSRP','Success '.$filter.' : Cust Id ( '.$row['CUST_ID'].' ) ,User Id ( '.$row['USER_ID'].' )');

					}
					if ($filter == $this->language->_('Reject')) {
						$CustomerUser = new CustomerUser($row['CUST_ID'], $row['USER_ID']);
						$CustomerUser->rejectRequestResetPassword();
						$temp = 1;
						//Application_Helper_General::writeLog('CSRP','Success '.$filter.' : Cust Id ( '.$row['CUST_ID'].' ) ,User Id ( '.$row['USER_ID'].' )');

					}
					Application_Helper_General::writeLog('CSRP', 'Success ' . $filter . ' : Cust Id ( ' . $row['CUST_ID'] . ' ) ,User Id ( ' . $row['USER_ID'] . ' )');
				}
				//Application_Helper_General::writeLog('CSRP','Success '.$filter.' : Cust Id ( '.$row['CUST_ID'].' ) ,User Id ( '.$row['USER_ID'].' )');
			}
			if ($temp == 1) {
				$this->setbackURL('/' . $this->_request->getModuleName() . '/requestresetpass');
				$this->_redirect('/notification/success/index');
			} else {
				$this->view->error = true;
				$msg = $this->language->_('Error: Please checked selection.');
				$this->view->report_msg = $msg;
			}
		}

		$this->paging($select);
		$this->view->fields = $fields;

		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('CSCP', 'View Customer Setup > Reset Password Request List');

			if (!empty($dataParamValue)) {
				foreach ($dataParamValue as $key => $value) {
					$wherecol[]	= $key;
					$whereval[] = $value;
				}
			} else {
				$wherecol = array();
				$whereval = array();
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
	}
}
