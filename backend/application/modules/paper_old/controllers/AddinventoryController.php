<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class paper_AddinventoryController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        if (!$this->view->hasPrivilege("APAM")) {
            return $this->_redirect("/home/dashboard");
        }

        Application_Helper_General::writeLog("APAM", "Add Paper Inventory");

        $flashMessanger = $this->_helper->getHelper('FlashMessenger');

        if (count($flashMessanger->getMessages()) > 0) {
            $this->view->error = $flashMessanger->getMessages()[0]["error"];
        }

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $branchdata = $this->_db->select()
            ->from(array('A' => 'M_BRANCH'), array('*'))
            ->query()->fetchAll();

        $saveBranchData = [];

        foreach ($branchdata as $value) {
            $saveBranchData[$value['BRANCH_CODE']] = $value["BRANCH_NAME"];
        }

        $this->view->branchDataArr = $saveBranchData;

        if ($this->_request->isPost()) {
            $filters = array(
                'paper_key'        => array('StripTags', 'StringTrim'),
                'paper_from'        => array('StripTags', 'StringTrim'),
                'paper_to'        => array('StripTags', 'StringTrim'),
                'paper_quantity'        => array('StripTags', 'StringTrim'),
                'paper_branch'        => array('StripTags', 'StringTrim'),
            );

            $validators =  array(
                'paper_key'    => array(
                    'NotEmpty',
                    'messages' => array($this->language->_('Can not be empty'))
                ),
                'paper_from'    => array(
                    'NotEmpty',
                    'messages' => array($this->language->_('Can not be empty'))
                ),
                'paper_to'    => array(
                    'NotEmpty',
                    'messages' => array($this->language->_('Can not be empty'))
                ),
                'paper_quantity'    => array(
                    'NotEmpty',
                    'messages' => array($this->language->_('Can not be empty'))
                ),
                'paper_branch'    => array(
                    'NotEmpty',
                    'messages' => array($this->language->_('Can not be empty'))
                ),
            );

            $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

            if ($zf_filter_input->isValid()) {

                $paper_data['PAPER_KEY'] = $zf_filter_input->paper_key;
                $paper_data['PAPER_FROM'] = $zf_filter_input->paper_from;
                $paper_data['PAPER_TO']     = $zf_filter_input->paper_to;
                $paper_data['PAPER_QTY']   = $zf_filter_input->paper_quantity;
                $paper_data['PAPER_BRANCH']     = $zf_filter_input->paper_branch;

                $cek_kode_kertas = $this->_db->select()
                    ->from("M_PAPER_GROUP")
                    ->where("PAPER_KEY = ?", $paper_data["PAPER_KEY"])
                    // ->where("PAPER_BRANCH = ?", $paper_data["PAPER_BRANCH"])
                    ->query()->fetchAll();

                $exist = false;

                if (count($cek_kode_kertas) > 0) {
                    $get_total_paper = intval($paper_data['PAPER_TO']) - $paper_data['PAPER_FROM'] + 1;
                    $from_paper = intval($paper_data['PAPER_FROM']);
                    foreach ($cek_kode_kertas as $row) {
                        for ($i = $from_paper; $i <= $get_total_paper + $from_paper; $i++) {
                            if (($i >= $row["PAPER_FROM"] && $i <= $row["PAPER_TO"])) {
                                var_dump($i);
                                $exist = true;
                                break;
                            }
                        }
                    }
                }

                if ($exist) {
                    $flashMessanger->addMessage(["error" => "Nomor kertas sudah terdaftar"]);
                    return $this->_redirect("/paper/addinventory");
                }

                try {

                    $paper_data["PAPER_FROM"] = str_pad(intval($paper_data["PAPER_FROM"]), 6, "0", STR_PAD_LEFT);
                    $paper_data["PAPER_TO"] = str_pad(intval($paper_data["PAPER_TO"]), 6, "0", STR_PAD_LEFT);

                    $this->_db->beginTransaction();

                    $change_id = $this->suggestionWaitingApproval('Paperinventory', null, $this->_changeType['code']['new'], null, 'M_PAPER_GROUP', null, null, null, null);

                    // $agent_data['CHANGES_ID'] = $change_id;
                    $m_paper_group = $paper_data;
                    $m_paper_group['PAPER_UPDATED'] = new Zend_Db_Expr('now()');
                    $m_paper_group['PAPER_UPDATEDBY'] = $this->_userIdLogin;
                    $this->_db->insert('M_PAPER_GROUP', $m_paper_group);
                    $this->_db->commit();

                    $check_paper_group = $this->_db->select()
                        ->from(array('B' => 'M_PAPER_GROUP'), array("*"))
                        ->where("PAPER_FROM = " . $paper_data['PAPER_FROM'] . " AND PAPER_TO = " . $paper_data['PAPER_TO'] . " AND PAPER_BRANCH = " . $paper_data['PAPER_BRANCH'])
                        ->query()->FetchAll();

                    $this->_db->beginTransaction();
                    $paper_data['CHANGES_ID'] = $change_id;
                    $paper_data['PAPER_GROUP'] = $check_paper_group[0]["PAPER_GROUP"];
                    $this->_db->insert('TEMP_PAPER_GROUP', $paper_data);
                    $this->_db->commit();

                    // generate paper inventory report -----------------------------------------

                    // $this->_db->beginTransaction();
                    // $start_paper = $check_paper_group[0]["PAPER_FROM"] + 0;
                    // // $save_paper_report = [];
                    // for ($i = 0; $i <= $check_paper_group[0]["PAPER_QTY"]; $i++) {
                    //     $temp_id = str_pad($start_paper, 7, "0", STR_PAD_LEFT);
                    //     $temp_paper_report["PAPER_ID"] = $temp_id;
                    //     $temp_paper_report["STATUS"] = "0";
                    //     $temp_paper_report["PAPER_GROUP"] = $check_paper_group[0]["PAPER_GROUP"];
                    //     $temp_paper_report["PAPER_UPDATED"] = new Zend_Db_Expr('now()');
                    //     $temp_paper_report["PAPER_UPDATEDBY"] = $this->_userIdLogin;

                    //     $this->_db->insert('M_PAPER', $temp_paper_report);

                    //     $start_paper += 1;
                    // }
                    // $this->_db->commit();

                    // --------------------------------------------------------------------------

                    $this->setbackURL('/paper/inventory');
                    $this->_redirect('/notification/submited/index');
                } catch (Exception $e) {
                    $this->_db->rollBack();
                    SGO_Helper_GeneralLog::technicalLog($e);

                    $this->view->error_remark = 'Gagal menambahkan inventory ke database. Silahkan coba lagi.';
                }
            }
        }
    }
}
