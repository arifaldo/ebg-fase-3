<?php
require_once 'Zend/Controller/Action.php';

class escrow_SuggestiondetailController extends Application_Main
{

  public function indexAction()
  { 
    $change_id = $this->_getParam('changes_id');
    $change_id = (Zend_Validate::is($change_id,'Digits'))? $change_id : 0;
    $this->_helper->layout()->setLayout('popup');
    
    $this->view->suggestionType = $this->_suggestType;
    
    if($change_id)
    {
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID','CHANGES_TYPE'))
                             ->where('CHANGES_ID='.$this->_db->quote($change_id))
                             ->where("CHANGES_FLAG='B'")
                             ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
      $result = $this->_db->fetchRow($select);
      if(empty($result))
	  {
		$this->_redirect('/notification/invalid/index');
	  }
	  else
	  {
		$this->view->CHANGES_TYPE = $result['CHANGES_TYPE'];
	  }

      if($result)
      {
		//suggest data
        $select = $this->_db->select()
                               ->from(array('T' =>'TEMP_MASTER'))
      	                       ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
	  				           ->where('T.CHANGES_ID = ?', $change_id);
      	$resultdata = $this->_db->fetchRow($select);
      	if($resultdata['TEMP_ID'])
      	{
			// $getCcy = $this->getCcy();
			// $getCcy = Application_Helper_Array::listArray($getCcy,'CCY_NUM','CCY_ID');
			// $this->view->CCYData = $getCcy;
			//content send to view
			$this->view->resultdata     = $resultdata;
       	}
       	else{ $change_id = 0; }
      }
      else{ $change_id = 0; } 
    }

    if(!$change_id)
    {
		$this->_redirect('/popuperror/index/index'); 
    }
    $this->view->changes_id = $change_id;
    
    //insert log
	Application_Helper_General::writeLog('ECCL','View Escrow Change List');
  }
}