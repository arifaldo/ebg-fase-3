<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class Cmdreport_CustomeruserlistDetailController extends Application_Main
{
	public function indexAction()
	{

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$userid = $AESMYSQL->decrypt($this->_getParam('userid'), $password);
		$custid = $AESMYSQL->decrypt($this->_getParam('custid'), $password);

		$pdf = $this->_getParam('pdf');
		$select = $this->_db->select()
			->from(array('A' => 'M_USER'), array('*'))
			->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('*'));
		$select->where('UPPER(USER_ID) LIKE ' . $this->_db->quote(strtoupper($userid)));
		$select->where('UPPER(B.CUST_ID) LIKE ' . $this->_db->quote(strtoupper($custid)));
		$list = $this->_db->fetchRow($select);
		//Zend_Debug::dump($list);die;	
		$mix = $custid . $userid;

		$selectcekpivi = $this->_db->select()
			->from(array('E' => 'M_FPRIVI_USER'), array('E.FPRIVI_ID'))
			->where("E.FUSER_ID = " . $this->_db->quote($mix));

		$select2 = $this->_db->select()
			->from(array('D' => 'M_FPRIVILEGE'), array())
			->joinleft(
				array('E' => $selectcekpivi),
				'D.FPRIVI_ID = E.FPRIVI_ID',
				array(
					'D.FPRIVI_ID',
					'D.FPRIVI_DESC',
					'ASSIGNED' => new Zend_Db_Expr("(CASE E.FPRIVI_ID 
																				        	  								   	WHEN NULL THEN 'No' 
																				        	  									ELSE 'Yes' 
																				        	  									END)")
				)
			)
			->Order('D.FPRIVI_ID ASC');
		$result2 = $select2->query()->FetchAll();
		$this->view->privilist = $result2;

		$setting = new Settings();
		$system_type1 = $setting->getSetting('system_type');

		$list2 = $this->_db->fetchRow($select2);
		//echo $select2; die;
		//Zend_Debug::dump($result2);die;
		$select4 = $this->_db->select()->from('M_FPRIVILEGE');
		$list4 = $this->_db->fetchAll($select4);
		$this->view->cekprivi = $list4;

		$select5 = $this->_db->select()
			->from('M_FPRIVILEGE_TEMPLATE', array('*'));
		$list5 = $this->_db->fetchAll($select5);

		$selectprivi = $this->_db->select()
			->from(array('D' => 'M_FPRIVI_USER'), array('*'));
		$selectprivi->where("D.FUSER_ID = " . $this->_db->quote($mix));
		$priviList = $selectprivi->query()->FetchAll();
		$priviListArr = Application_Helper_Array::simpleArray($priviList, 'FPRIVI_ID');
		//Zend_Debug::dump($priviListArr);die;
		//cek apakah list template sesuai list privilege yang dipilih
		$priviTemplate = array();
		foreach ($list5 as $row) {
			$priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
		}


		$flag_template = 0;
		$user_role     = '';
		$template_id   = '';
		if (count($priviTemplate) > 0) {
			foreach ($priviTemplate as $key => $row) {
				$count_row = count($row);
				$count_priviListArr = count($priviListArr);
				if ($count_row == $count_priviListArr) {
					$arr_diff = array_diff_key(array_flip($priviListArr), $row);
					if (count($arr_diff) == 0) {
						$template_id = $key;
						$flag_template = 1;
						break;
					}
				}
			}
		}

		if ($flag_template == 0) $user_role = 'Custom';
		else if ($flag_template == 1) {
			$cekrole = $this->_db->select()
				->from('M_FTEMPLATE')
				->where('UPPER(FTEMPLATE_ID)=' . $this->_db->quote($template_id))
				->query()->fetch();
			$user_role = $cekrole['FTEMPLATE_DESC'];
		}

		/*foreach($list5 as $cekrole)
	{
		$rolecek = $cekrole['FTEMPLATE_ID'];
		$select51 = $this->_db->select()->from ('M_FPRIVILEGE_TEMPLATE',array('FPRIVI_ID'))
		->where("FTEMPLATE_ID = ".$this->_db->quote($rolecek));
		$list51 = $this->_db->fetchAll($select51);
		$count = count($list51);
		foreach	($list51 as $cekrole2)
		{
			$i = 0;
			$rolecek2 = $cekrole2['FPRIVI_ID'];
			$select52 = $this->_db->select()
					        ->from(array('D' => 'M_FPRIVI_USER'), array('*'));
			$select52 -> where("D.FUSER_ID = ".$this->_db->quote($mix));
			$select52 -> where("D.FPRIVI_ID = ".$this->_db->quote($rolecek2));
			$result52 = $select2->query()->FetchAll();
			
			$select53 = $this->_db->select()
					        ->from(array('D' => 'M_FPRIVI_USER'), array('*'));
			$select53 -> where("D.FUSER_ID = ".$this->_db->quote($mix));
			$result53 = $select2->query()->FetchAll();
			$count2 = count($result53);
			
			if($result52 == null)
			{
				$i++;
			}
		}
		if($i == 0 && $count2 == $count)
		{
			$resultrole = $rolecek;
			break;
		}
		else
		{
			$resultrole = 'Custom';
		}
	}*/
		$this->view->role = $user_role;

		$select3 = $this->_db->select()
			->from(array('C' => 'M_DAILYLIMIT'));
		$select3->where("C.CUST_ID = " . $this->_db->quote($custid));
		$select3->where("C.USER_LOGIN = " . $this->_db->quote($userid));
		$result3 = $select3->query()->FetchAll();
		$this->view->daily = $result3;
		$list3 = $this->_db->fetchRow($select3);

		$lastactive = $this->_db->select()
			->from('T_FACTIVITY', array('LOG_DATE'));
		$lastactive->where("CUST_ID = " . $this->_db->quote($custid));
		$lastactive->where("USER_ID = " . $this->_db->quote($userid));
		$lastactive->order('LOG_DATE DESC');
		$lastactive = $this->_db->fetchRow($lastactive);

		$statusCode = array_flip($this->_masterglobalstatus['code']);
		$statusDesc = $this->_masterglobalstatus['desc'];
		$this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
		$this->view->statusDesc = $this->_masterglobalstatus['desc'];
		$this->view->modulename = $this->_request->getModuleName();

		$status = $statusDesc[$statusCode[$list['USER_STATUS']]];

		// TEMPLATE 1
		// <tr>
		// 	<td class="tbl-evencontent" style="border-right: none;">&nbsp;'.$this->language->_('Token ID').'</td>
		// 	<td class="tbl-evencontent" style="border-left: none;">:&nbsp;'.$list['TOKEN_ID'].'</td>
		// </tr>
		{
			$htmldata1 =
				'<table border="1" cellspacing="0" cellpadding="0" class="table table-sm table-striped" >
			<tr>
				<th class="filterarea tablehead" colspan="3">
					' . $this->language->_('Profile') . '				
				</th>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Company Code') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['CUST_ID'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Company Name') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['CUST_NAME'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('User ID') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_ID'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('User Name') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_FULLNAME'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Status') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $status . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Email') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_EMAIL'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Phone Number') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_PHONE'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Extension') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_EXT'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Created Date') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . Application_Helper_General::convertDate($list['USER_CREATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Created By') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_CREATEDBY'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Last Suggestion') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . Application_Helper_General::convertDate($list['USER_SUGGESTED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Last Suggester') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_SUGGESTEDBY'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Last Approval') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . Application_Helper_General::convertDate($list['USER_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" style="border-right: none;">&nbsp;' . $this->language->_('Last Approver') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_UPDATEDBY'] . '</td>
			</tr>
		</table>';
		}
		// TEMPLATE 2

		if ($list['USER_ISLOGIN'] == 0) {
			$login = "No";
		} else {
			$login = "Yes";
		}

		if ($list['USER_ISLOCKED'] == 0) {
			$lock = "No";
		} else {
			$lock = "Yes";
		} {
			$htmldata2 =
				'<table border="1" cellspacing="0" cellpadding="0" class="table table-sm table-striped" >
			<tr>
				<th class="filterarea tablehead" colspan="3">
					' . $this->language->_('Statistic') . '				
				</th>
			</tr>
			<tr>
				<td class="tbl-evencontent" width="35%" style="border-right: none;">&nbsp;' . $this->language->_('Last Login') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . Application_Helper_General::convertDate($list['USER_LASTLOGIN'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" width="35%" style="border-right: none;">&nbsp;' . $this->language->_('Last Activity') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . Application_Helper_General::convertDate($lastactive['LOG_DATE'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" width="35%" style="border-right: none;">&nbsp;' . $this->language->_('Login Failed Attempt') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_FAILEDATTEMPT'] . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" width="35%" style="border-right: none;">&nbsp;' . $this->language->_('Is Now Logged On') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $login . '</td>
			</tr>
			<tr>
				<td class="tbl-evencontent" width="35%" style="border-right: none;">&nbsp;' . $this->language->_('User Is Locked') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $lock . '</td>
			</tr>
			<!-- tr>
				<td class="tbl-evencontent" width="35%" style="border-right: none;">&nbsp;' . $this->language->_('Locked Reason') . '</td>
				<td class="tbl-evencontent" style="border-left: none;">:&nbsp;' . $list['USER_LOCKREASON'] . '</td>
			</tr -->
		</table>';
		}
		// TEMPLATE 3
		{
			$i = 1;
			$htmldata3detail = '';
			foreach ($result3 as $daily) {
				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
				if ($system_type1 == 1) {

					$htmldata3detail .= '
					<tr>
						<td class="' . $td_css . '">' . $daily['CCY_ID'] . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::displayMoney($daily['DAILYLIMIT']) . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::convertDate($daily['SUGGESTED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . ' (' . $daily['SUGGESTEDBY'] . ')' . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::convertDate($daily['UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . ' (' . $daily['UPDATEDBY'] . ')' . '</td>
					</tr>
					';
				} elseif ($system_type1 == 2) {

					$htmldata3detail .= '
					<tr>
						<td class="' . $td_css . '">' . $daily['CCY_ID'] . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::displayMoney($daily['DAILYLIMIT']) . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::convertDate($daily['SUGGESTED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . ' (' . $daily['SUGGESTEDBY'] . ')' . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::convertDate($daily['UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . ' (' . $daily['UPDATEDBY'] . ')' . '</td>
					</tr>
					';
				} else {

					$htmldata3detail .= '
					<tr>
						<td class="' . $td_css . '">' . $daily['CCY_ID'] . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::displayMoney($daily['DAILYLIMIT']) . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::displayMoney($daily['DAILYLIMIT']) . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::convertDate($daily['SUGGESTED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . ' (' . $daily['SUGGESTEDBY'] . ')' . '</td>
						<td class="' . $td_css . '">' . Application_Helper_General::convertDate($daily['UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat) . ' (' . $daily['UPDATEDBY'] . ')' . '</td>
					</tr>
					';
				}

				$i++;
			}
			if (empty($result3)) {
				$htmldata3detail = '
				<tr>
					<td class="tbl-evencontent" colspan="5" align="center">--- No Data ---</td>
				</tr>';
			}

			if ($system_type1 == 1) {

				$htmldata3 =
					'<table border="1" cellspacing="0" cellpadding="0" class="table table-sm table-striped"
				width="400px">
				<tr>
					<th class="filterarea tablehead" colspan="6">
						' . $this->language->_('Daily Limit') . '				
					</th>
				</tr>
				<tr>
					<th valign="top">' . $this->language->_('Currency') . '</th>
					<th valign="top">' . $this->language->_('CBS Limit') . '</th>
					<th valign="top">' . $this->language->_('Last Suggested') . '</th>
					<th valign="top">' . $this->language->_('Last Approved') . '</th>
				</tr>'
					. $htmldata3detail . '
			</table>';
			} elseif ($system_type1 == 2) {

				$htmldata3 =
					'<table border="1" cellspacing="0" cellpadding="0" class="table table-sm table-striped"
				width="400px">
				<tr>
					<th class="filterarea tablehead" colspan="6">
						' . $this->language->_('Daily Limit') . '				
					</th>
				</tr>
				<tr>
					<th valign="top">' . $this->language->_('Currency') . '</th>
					<th valign="top">' . $this->language->_('Daily Limit') . '</th>
					<th valign="top">' . $this->language->_('Last Suggested') . '</th>
					<th valign="top">' . $this->language->_('Last Approved') . '</th>
				</tr>'
					. $htmldata3detail . '
			</table>';
			} else {

				$htmldata3 =
					'<table border="1" cellspacing="0" cellpadding="0" class="table table-sm table-striped"
				width="400px">
				<tr>
					<th class="filterarea tablehead" colspan="6">
						' . $this->language->_('Daily Limit') . '				
					</th>
				</tr>
				<tr>
					<th valign="top">' . $this->language->_('Currency') . '</th>
					<th valign="top">' . $this->language->_('CBS Limit') . '</th>
					<th valign="top">' . $this->language->_('Open Banking Limit') . '</th>
					<th valign="top">' . $this->language->_('Last Suggested') . '</th>
					<th valign="top">' . $this->language->_('Last Approved') . '</th>
				</tr>'
					. $htmldata3detail . '
			</table>';
			}
		}
		// TEMPLATE 4
		{
			$htmldata4 =
				'<table border="1" cellspacing="0" cellpadding="0" class="table table-sm table-striped" >
			<tr>
				<th class="filterarea tablehead" colspan="3">
					' . $this->language->_('User Role') . '				
				</th>
			</tr>
			<tr>
				<td class="tbl-evencontent">&nbsp;' . $this->language->_($user_role) . '</td>
			</tr>
		</table>';
			$htmldata5detail = '';
			foreach ($result2 as $cekprivi) {
				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
				$htmldata5detail .=
					'<tr>
					<td class="' . $td_css . '">' . $this->language->_($cekprivi['FPRIVI_DESC']) . '</td>
					<td class="' . $td_css . '">' . $this->language->_($cekprivi['ASSIGNED']) . '</td>
				</tr>';
				$i++;
			}
		}
		// TEMPLATE 5
		{
			$htmldata5 =
				'<table width="400px" cellpadding="2" cellspacing="0" border="1" class="table table-sm table-striped">
			<tr>
				<th class="filterarea tablehead" colspan="3">
					' . $this->language->_('Privilage List') . '				
				</th>
			</tr>
			<tr>
				<th valign="top">' . $this->language->_('Function') . '</th>
				<th valign="top">' . $this->language->_('Assigned') . '</th>
			</tr>'
				. $htmldata5detail .
				'</table>';
		}
		$this->view->template1 = $htmldata1;
		$this->view->template2 = $htmldata2;
		// $this->view->template3 = $htmldata3;
		$this->view->template4 = $htmldata4;
		$this->view->template5 = $htmldata5;

		if ($pdf) {
			// $complete1 = "<h2>Profile</h2> " . $htmldata1 . " <br /> <br /> <h2>Statistic</h2> " . $htmldata2 . " <br /> <br /> <h2>Daily Limit</h2> " . $htmldata3 . " <br /> <br /> <h2>User Role</h2> " . $htmldata4 . " <br /> <br />";
			$complete1 =
				"<h2>Profile</h2>
						" . $htmldata1 . "
						<br />
						<br />
						<h2>Statistic</h2>
						" . $htmldata2 . "
						<h2>User Role</h2>
						" . $htmldata4 . "
						<br />
						<br />";
			$complete2 =
				"<h2>Privilege List</h2>
			" . $htmldata5;

			$datapdf = "<tr><td>" . $complete1 . "</td></tr><tr><td>" . $complete2 . "</td></tr>";
			$this->_helper->download->pdfModif(null, null, null, 'Customer User List', $datapdf);
			Application_Helper_General::writeLog('RPCU', 'Download PDF Customer User Detail (' . $userid . ' - ' . $custid . ')');
		} else {
			Application_Helper_General::writeLog('RPCU', 'View Customer User Detail (' . $userid . ' - ' . $custid . ')');
		}
	}
}
