<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';
require_once 'General/CustomerUser.php';

require_once 'General/Settings.php';

class virtualaccount_NewecollectionController extends Application_Main
{

	protected $_destinationUploadDir = '';
      protected $_maxRow = '';

      public function initController(){
      	$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
      }

      public function indexAction()
      {
      	$setting = new Settings();			  	
      	$enc_pass = $setting->getSetting('enc_pass');
      	$enc_salt = $setting->getSetting('enc_salt');
      	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
      	$pw_hash = md5($enc_salt.$enc_pass);
      	$rand = $this->_userIdLogin.date('dHis').$pw_hash;
      	$sessionNamespace->token 	= $rand;
      	$this->view->token = $sessionNamespace->token;
      	$password = $sessionNamespace->token; 

      	$this->_helper->layout()->setLayout('newlayout');   

              //bin list
            $select = $this->_db->select()
                  ->from(array('B' => 'M_CUSTOMER_BIN'),array('*'))
                  ->where('B.CUST_ID = ?', (string)$this->_custIdLogin)
                  ->where('B.BIN_TYPE = 1') //ecollection
                  ->where('B.HOSTTOHOST != 1') //ecollection
                  ->query()->fetchAll();

            $optBin = '<option value="" >-- ' . $this->language->_('Any Value') . ' --</option>';
            foreach ($select as $key => $value) {
                  $optBin .= '<option value="'.$value['CUST_BIN'].'">'.$value['CUST_BIN'].'</option>';
            }

            $this->view->optBin = $optBin;

            $this->view->TrfDateType = '1';

            $PS_EFDATE = date('Y-m-d', strtotime('+1 days', strtotime(date('Y-m-d'))));
            $this->view->PS_EFDATEFUTURE = $PS_EFDATE;

            $periodicEveryArr = array(
                  '1' => $this->language->_('Monday'),
                  '2' => $this->language->_('Tuesday'),
                  '3' => $this->language->_('Wednesday'),
                  '4' => $this->language->_('Thursday'),
                  '5' => $this->language->_('Friday'),
                  '6' => $this->language->_('Saturday'),
                  '7' => $this->language->_('Sunday'),
            );
            $periodicEveryDateArr = range(1, 28);
            $periodicEveryDateArr['31'] = $this->language->_('End of month');

            $this->view->periodicEveryArr       = $periodicEveryArr;
            $this->view->periodicEveryDateArr  = $periodicEveryDateArr;

            $PS_ENDDATE_VAL = ($this->_request->getParam('PS_ENDDATEPERIODIC')) ? $this->_request->getParam('PS_ENDDATEPERIODIC') : date('d/m/Y');
            $this->view->endDatePeriodic = $PS_ENDDATE_VAL;

            $sessionNamespace = new Zend_Session_Namespace('ecollection');
            $paramSession = $sessionNamespace->paramSession;
            $back = $sessionNamespace->back;

            $getbin = $this->_getParam('BIN');
            $getvatype = $this->_getParam('vatype');

            $this->view->getbin = $getbin;
            $this->view->getvatype = $getvatype;

            //bin from detail
            $binFromDetail = $this->_getParam('cust_bin');
            $vanumberFromDetail = $this->_getParam('va_number');
            $vanameFromDetail = $this->_getParam('va_name');
            if (!empty($binFromDetail) && !empty($vanumberFromDetail)) {
                $this->view->fromDetail = true;
                $this->view->single = true;
                $this->view->bin = $binFromDetail;
                $this->view->vanumber = $vanumberFromDetail;
                $this->view->vaname = $vanameFromDetail;
            }

      	if($this->_request->isPost() )
            {
        	   $param = $this->_request->getParams();

               //--------------------------------------------single--------------------------------------------
               if ($param['next_single']) {

                  $bin = $param['bin'];

                  $binData = $this->_db->fetchRow(
                        $this->_db->select()
                          ->from(array('B' => 'M_CUSTOMER_BIN'),array('*'))
                          ->where('B.CUST_ID = ?', (string)$this->_custIdLogin)
                          ->where('B.CUST_BIN = ?', (string)$bin)
                  );

                  $trfDateType = $param['tranferdatetype'];

                  $this->view->vatype = $binData['VA_TYPE'];


                  $filters = array(
                    'bin' => array('StringTrim','StripTags'),
                    'vaname' => array('StringTrim','StripTags'),
                    'invoice_amount' => array('StringTrim','StripTags'),
                    'message' => array('StringTrim','StripTags')
                  );

                  //if static
                  if ($binData['VA_TYPE'] != '0') {
                        $filters['vanumber'] = array('StringTrim','StripTags');
                  }

                  //if futuredate
                  if ($trfDateType == '2') {
                        $filters['PS_EFDATE'] = array('StringTrim','StripTags');
                  }
                  // periodic
                  else if($trfDateType == '3'){
                        $tranferdateperiodictype = $param['tranferdateperiodictype'];

                        //every day of
                        if ($tranferdateperiodictype == '5') {
                              $filters['PERIODIC_EVERY'] = array('StringTrim','StripTags');
                        }
                        //every date of
                        else{
                              $filters['PERIODIC_EVERYDATE'] = array('StringTrim','StripTags');
                        }
                  }

                  //input later
                  if ($binData['EXP_TIME'] == 0) {
                        $filters['exp_date'] = array('StringTrim','StripTags');
                        $filters['exp_time'] = array('StringTrim','StripTags');
                  }

                  $validators = array(
                        'bin' => array(
                                          'NotEmpty',
                                                'messages' => array(
                                                                      $this->language->_('Can not be empty'),
                                                )
                                          ),
                        'vaname' => array(
                                          'NotEmpty',
                                                'messages' => array(
                                                                      $this->language->_('Can not be empty'),
                                                )
                                          ),
                        'invoice_amount' => array(
                                          'NotEmpty',
                                                'messages' => array(
                                                                      $this->language->_('Can not be empty'),
                                                )
                                          ),
                        'message'  => array(
                                                'NotEmpty',
                                                'messages' => array(
                                                                        $this->language->_('Can not be empty')
                                                                    )
                                          )
                  );

                  //if static
                  if ($binData['VA_TYPE'] != '0') {
                        $validators['vanumber'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
                  }

                   //if futuredate
                  if ($trfDateType == '2') {
                        $validators['PS_FUTUREDATE'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
                        
                  }
                  // periodic
                  else if($trfDateType == '3'){

                        //every day of
                        if ($tranferdateperiodictype == '5') {
                              $validators['PERIODIC_EVERY'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
                        }
                        //every date of
                        else{
                              $validators['PERIODIC_EVERYDATE'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
                        }
                  }

                  //input later
                  if ($binData['EXP_TIME'] == 0) {
                        $validators['exp_date'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
                        $validators['exp_time'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
                  }

                  $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

                  if($zf_filter_input->isValid())
                  {     

                        $insertArr = array(
                              'VA_TYPE' => $binData['VA_TYPE'], // static or dynamic
                              'CUST_BIN' => $bin,
                              'VA_NAME' => $zf_filter_input->vaname,
                              'CCY' => 'IDR',
                              'INVOICE_AMOUNT' => Application_Helper_General::convertDisplayMoney($zf_filter_input->invoice_amount),
                              'IN_BILLING' => Application_Helper_General::convertDisplayMoney($zf_filter_input->invoice_amount),
                              'MESSAGE' => $zf_filter_input->message,
                              'VA_STATUS' => 1, //1 open, 2 closed, 3 deleted
                              'TOTAL_COUNTER' => 1, 
                        );

                         //if static
                        if ($binData['VA_TYPE'] != '0') {
                              $insertArr['VA_NUMBER'] = $zf_filter_input->vanumber;
                        }
                        //if dynamic then auto generated


                         //if futuredate
                        if ($trfDateType == '2') {
                              $insertArr['DOC_STATUS'] = 1; //0 inbilling, 1 pending future date, 2 settled, 3 unsettled, 4 cancelled
                              $insertArr['EF_DATE'] = $zf_filter_input->PS_FUTUREDATE;
                              $efstatus = 'Future Date';
                              $efdate = $zf_filter_input->PS_FUTUREDATE;
                        }
                        // periodic
                        else if($trfDateType == '3'){
//TO DO: PERIODIC 
                              //every day of
                              // if ($tranferdateperiodictype == '5') {
                              //       //hitung expired date sm efdatenya
                              //       $dateNow = mktime(0, 0, 0, date("n"), date("j"), date("Y"));
                              //       $every = (int) $zf_filter_input->PERIODIC_EVERY;
                              //       $d = date("w", $dateNow);

                              //       $addDay = $every;
                              //       if ($every > $d) {
                              //             $addDay = $addDay;
                              //       } else {
                              //             $addDay = $addDay + 7;
                              //       }
                              //       $nextDate  =  (int) $addDay - (int) $d;
                              //       $NEXT_DATE = date("Y-m-d", strtotime("+$nextDate day"));
                              //       $PERIODIC_EVERY_VAL = $zf_filter_input->PERIODIC_EVERY;
                              // }
                              // //every date of
                              // else{
                              //       //hitung expired date sm efdatenya
                              //       $dateNextMonth = mktime(0, 0, 0, date("n"), date("j") + 1, date("Y"));
                              //       $dateNow = date("j");
                              //       $maxDays = date('t', $dateNextMonth);
                              //       //echo $maxDays; die;
                              //       $every = (int) $zf_filter_input->PERIODIC_EVERYDATE;

                              //       if ($every > $dateNow) {
                              //             $addMonth = 0;
                              //       } else {
                              //             $addMonth = 1;
                              //       }

                              //       if ($maxDays >=  $every) {
                              //             $every = $every;
                              //       } else {
                              //             $every = $maxDays;
                              //       }

                              //       $nextDate = mktime(0, 0, 0, date("n") + $addMonth, $every, date("Y"));
                              //       $NEXT_DATE = date("Y-m-d", $nextDate);
                              //       $PERIODIC_EVERY_VAL = $zf_filter_input->PERIODIC_EVERYDATE;
                              // }

                              // $END_DATE = join('-', array_reverse(explode('/', $EXPIRY_DATE)));
                              // $PS_EFDATE = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
                              // $PS_EFDATE_ORII = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');

                              // $efstatus = 'Future Date';
                              // $efdate = $NEXT_DATE;
                        }
                        else{ 

                              $insertArr['DOC_STATUS'] = 0; //0 inbilling, 1 pending future date, 2 settled, 3 unsettled, 4 cancelled
                              $insertArr['EF_DATE'] = date('Y-m-d H:i:s');
                              $efstatus = 'Immediate';
                              $efdate = date('Y-m-d H:i:s');
                        }

                        //input later
                        if ($binData['EXP_TIME'] == 0) {

                              $insertArr['EXP_DATE'] = $zf_filter_input->exp_date.' '.$zf_filter_input->exp_time;

                              $expdate = $zf_filter_input->exp_date.' '.$zf_filter_input->exp_time;
                        }
                        else{
                              $exTimeType = $binData['EX_TIME_TYPE'];
                              $exTime = $binData['EX_TIME'];

                              //minutes
                              if ($exTimeType == '0') {
                                    $exp_date = date('Y-m-d H:i:s',strtotime($efdate." +".$exTime." minute"));
                                    $type = 'Minute(s)';
                              }
                              //hours
                              else if ($exTimeType == '1') {
                                    $exp_date = date('Y-m-d H:i:s',strtotime($efdate." +".$exTime." hour"));
                                    $type = 'Hour(s)';
                              }
                              else {
                                    $exp_date = date('Y-m-d H:i:s',strtotime($efdate." +".$exTime." day"));
                                    $type = 'Day(s)';
                              }

                              $insertArr['EXP_DATE'] = $exp_date;

                              $expdate = 'Predefined set to '.$exTime.' '.$type.' after effective';
                        }
                        
                        $emailAddress = $param['emailAddress'];
                        $notif = $param['notif'];
                        $email = $param['email_notif'];
                        $sms = $param['sms_notif'];
                        $notif = $param['notif'];
                        if ($notif == 2) { //yes
                              $insertArr['NOTIF'] = 1;
                              $insertArr['EMAIL'] = $email;
                              $insertArr['SMS'] = $sms;
                              $notifstatus = 'Yes';
                        }
                        else{
                              $insertArr['NOTIF'] = 0;
                              $notifstatus = 'No';
                        }

                        //dipakai untuk insert data ke db
                        $sessionNamespace->insertArr     = $insertArr;

                        //if static
                        if ($binData['VA_TYPE'] != '0') {
                              $vanumber = $bin.'-'.$zf_filter_input->vanumber;
                        }
                        //if dynamic
                        else{
                              $vanumber = 'Auto Generated';
                        }

                        //dipakai untuk tampilan confirmation
                        $paramSession = array(
                              'addtype' => 'single',
                              'bin' => $bin,
                              'vatype' => $binData['VA_TYPE'], //static or dynamic
                              'vano' => $zf_filter_input->vanumber, //yg blm digabung sm bin, untuk select2
                              'vanumber' => $vanumber,
                              'vaname' => $zf_filter_input->vaname,
                              'invoice_amount' => $zf_filter_input->invoice_amount,
                              'message' => $zf_filter_input->message,
                              'efstatus' => $efstatus,
                              'efdate' => $efdate,
                              'expdate' => $expdate,
                              'emailAddress' => $emailAddress,
                              'notif' => $notifstatus,
                              'email_notif' => $email,
                              'sms_notif' => $sms,

                              //dipakai utk back data
                              'b_efdate' => $this->_getParam('efdate'),
                              'exp_date' => $this->_getParam('exp_date'),
                              'exp_time' => $this->_getParam('exp_time'),
                              'trfDateType' => $this->_getParam('tranferdatetype'),
                              'tranferdateperiodictype' => $this->_getParam('tranferdateperiodictype'),
                              'PS_FUTUREDATE' => $this->_getParam('PS_FUTUREDATE'),
                              'PERIODIC_EVERY' => $this->_getParam('PERIODIC_EVERY'),
                              'PERIODIC_EVERYDATE' => $this->_getParam('PERIODIC_EVERYDATE'),
                              'b_notif' => $this->_getParam('notif')
                        );
                        $sessionNamespace->paramSession     = $paramSession;
                        $sessionNamespace->binData     = $binData;


                        $this->_redirect('/virtualaccount/newecollection/next');

                  }
                  //not valid
                  else{
                        $this->view->error = 1;
                        $this->view->single = true;
                        $this->view->bin = $this->_getParam('bin');
                        $this->view->vanumber = ($zf_filter_input->isValid('vanumber')) ? $zf_filter_input->vanumber : $this->_getParam('vanumber');
                        $this->view->vano = ($zf_filter_input->isValid('vanumber')) ? $zf_filter_input->vanumber : $this->_getParam('vanumber');
                        $this->view->vaname = ($zf_filter_input->isValid('vaname')) ? $zf_filter_input->vaname : $this->_getParam('vaname');
                        $this->view->invoice_amount = ($zf_filter_input->isValid('invoice_amount')) ? $zf_filter_input->invoice_amount : $this->_getParam('invoice_amount');
                        $this->view->message = ($zf_filter_input->isValid('message')) ? $zf_filter_input->message : $this->_getParam('message');
                        $this->view->emailAddress = $this->_getParam('emailAddress');

                        $this->view->efdate = ($zf_filter_input->isValid('efdate')) ? $zf_filter_input->efdate : $this->_getParam('efdate');
                        $this->view->PS_FUTUREDATE = ($zf_filter_input->isValid('PS_FUTUREDATE')) ? $zf_filter_input->efdate : $this->_getParam('PS_FUTUREDATE');
                        $this->view->exp_date = ($zf_filter_input->isValid('exp_date')) ? $zf_filter_input->exp_date : $this->_getParam('exp_date');
                        $this->view->exp_time = ($zf_filter_input->isValid('exp_time')) ? $zf_filter_input->exp_time : $this->_getParam('exp_time');
                       
                        $this->view->trfDateType = ($zf_filter_input->isValid('tranferdatetype')) ? $zf_filter_input->trfDateType : $this->_getParam('tranferdatetype');
                        $this->view->tranferdateperiodictype = ($zf_filter_input->isValid('tranferdateperiodictype')) ? $zf_filter_input->tranferdateperiodictype : $this->_getParam('tranferdateperiodictype');
                        $this->view->PERIODIC_EVERY = ($zf_filter_input->isValid('PERIODIC_EVERY')) ? $zf_filter_input->PERIODIC_EVERY : $this->_getParam('PERIODIC_EVERY');
                        $this->view->PERIODIC_EVERYDATE = ($zf_filter_input->isValid('PERIODIC_EVERYDATE')) ? $zf_filter_input->PERIODIC_EVERYDATE : $this->_getParam('PERIODIC_EVERYDATE');

                        $this->view->notif = ($zf_filter_input->isValid('notif')) ? $zf_filter_input->notif : $this->_getParam('notif');
                        $this->view->email_notif = ($zf_filter_input->isValid('email_notif')) ? $zf_filter_input->email_notif : $this->_getParam('email_notif');
                        $this->view->sms_notif = ($zf_filter_input->isValid('sms_notif')) ? $zf_filter_input->sms_notif : $this->_getParam('sms_notif');

                        $error = $zf_filter_input->getMessages();
                        // print_r($error);die;
                        //format error utk ditampilkan di view html 
                        $errorArray = null;
                        foreach ($error as $keyRoot => $rowError) {
                              foreach ($rowError as $errorString) {
                                    $errorArray[$keyRoot] = $errorString;
                              }
                        }
                        foreach ($error as $keyRoot => $rowError) {
                              foreach ($rowError as $errorString) {
                                    $keyname = "error_" . $keyRoot;
                                    // print_r($keyname);die;
                                    $this->view->$keyname = $this->language->_($errorString);
                              }
                        }

                        $this->view->error_msg  = $errorArray;
                  }

                  //--------------------------------------------end single--------------------------------------------
               }


               //------------------------------------------------------bulk---------------------------------------------------------
               else if ($param['next_bulk']) {

                  //---------input data----------------------------
                  $bin = $param['bin_bulk'];
                  $fileId = $param['idfile_bulk'];
                  $action = $param['uploadaction_bulk'];

                  $adapter = new Zend_File_Transfer_Adapter_Http();

                  $adapter->setDestination ( $this->_destinationUploadDir );
                  $extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
                  $extensionValidator->setMessage(
                      $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.txt'
                  );

                  $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                  $sizeValidator->setMessage(
                      'Error: File exceeds maximum size'
                  );

                  $adapter->setValidators ( array (
                      $extensionValidator,
                      $sizeValidator,
                  ));

                  $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                  $adapter->addFilter ( 'Rename',$newFileName  );

                  if ($adapter->isValid()){

                    if ($adapter->receive()) {

                        //parse csv jg bisa utk txt
                        $data = $this->_helper->parser->parseCSV($newFileName, '|');    

                        $totalRecords = count($data) - 2;
                        $max = 5000;
                        //if dibawah / sama dengan 5000
                        if($totalRecords <= $max)
                        {

                              $binData = $this->_db->fetchRow($this->_db->select()
                                ->from(array('B' => 'M_CUSTOMER_BIN'),array('*'))
                                ->where('B.CUST_ID = ?', (string)$this->_custIdLogin)
                                ->where('B.CUST_BIN = ?', (string)$bin)
                              );

                              //---------file header data--------------------
                              $headerData = $data[0];

                              //add new
                              if($action == '0'){
                                    $file_bin = $headerData[1];
                                    $file_action = $headerData[2];
                                    $file_fileId = $headerData[3];
                                    $file_uploadDate = $headerData[4];
                                    $file_docCount = $headerData[5];
                                    $file_ccy = $headerData[6];
                              }
                              else{
                                    $file_bin = $headerData[1];
                                    $file_vaType = $headerData[2];
                                    $file_action = $headerData[3];
                                    $file_fileId = $headerData[4];
                                    $file_uploadDate = $headerData[5];
                                    $file_docCount = $headerData[6];
                                    $file_ccy = $headerData[7];

                                    $vaTypeArr = array(
                                          '0' => 'Dynamic',
                                          '1' => 'Static',
                                          '2' => 'Static'
                                    );

                                    if ($vaTypeArr[$binData['VA_TYPE']] != $file_vaType) {
                                           $error_msg['uploaddoc_bulk'] = $this->language->_('Bin is not '.$file_vaType);
                                    }
                              }                              

                              $actionArr = array(
                                    '0' => 'Add New VA',
                                    '1' => 'Overwrite VA',
                                    '2' => 'Cancel VA'
                              );

                              //---------------------------------validasi header------------------------
                              if ($bin != $file_bin) {
                                    $error_msg['bin_bulk'] = $this->language->_('Bin is not match with file');
                              }
                              if ($fileId != 'BYPASSIDFU') {
                                  if ($fileId != $file_fileId) {
                                    $error_msg['idfile_bulk'] = $this->language->_('File id is not match with file');
                                  } 
                              }
                              if ($actionArr[$action] != $file_action) {
                                    $error_msg['uploadaction_bulk'] = $this->language->_('Upload action is not match with file');
                              }

                              //---------------------------------end validasi header------------------------
                        }

                        @unlink($newFileName); 
                    }
                  }
                  else
                  {   
                    $this->view->error = true;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                        if($key=='fileUploadErrorNoFile')
                            $error_msg['uploaddoc_bulk'] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                        else
                            $error_msg['uploaddoc_bulk'] = $val;
                        break;
                    }
                  }   

                  if(empty($error_msg)){

                        $rawData = $data;

                        //unset header data
                        unset($data[0]);
                        unset($data[1]);

                        $no =0;
                        foreach ($data as $columns)
                        {     
                              //add new
                              if ($action == '0') {
                                    $colCount = 8;
                              }
                              else{
                                    $colCount = 7;
                              }

                              if(count($columns) == $colCount)
                              {

                                    $paramPayment = array(
                                          "CATEGORY"                          => 'VIRTUAL ACCOUNT',
                                          "FROM"                              => "I",                       // F: Form, I: Import
                                          "PS_EFDATE"                         => date('d/m/y'),
                                          "_dateFormat"                       => $this->_dateUploadFormat,
                                          "_dateDBFormat"                     => $this->_dateDBFormat,
                                          "VA_TYPE"                           => $binData['VA_TYPE'], //0 = dynamic else static
                                          "BIN_EXP_TYPE"                      => $binData['EXP_TIME'], // 0=input later(defined in file), 1=predefined 
                                          "ACTION"                            => $action // 0=add new, 1=overwrite, 2=cancel 
                                    );

                                    if ($action == '0') {
                                          $paramTrxArr[0] = array(
                                                "BIN"                               => $bin,
                                                "VA_NUMBER"                         => $columns[1],
                                                "VA_NAME"                           => $columns[2],
                                                "CCY"                               => 'IDR',
                                                "TRA_AMOUNT"                        => $columns[3],
                                                "MESSAGE"                           => $columns[4],
                                                "EXP_DATE"                          => $columns[5],
                                                "EXP_TIME"                          => $columns[6],


                                                "FILE_ID"                           => $fileId,
                                                "UPLOAD_DATE"                       => $file_uploadDate
                                          );
                                    }
                                    else{

                                          $paramTrxArr[0] = array(
                                                "BIN"                               => $bin,
                                                "CCY"                               => 'IDR',
                                                "FILE_ID"                           => $fileId,
                                                "UPLOAD_DATE"                       => $file_uploadDate
                                          );

                                           //if static
                                          if ($file_vaType == 'Static') {
                                                $paramTrxArr[0]['VA_ID'] = $columns[1];
                                          }
                                          else{
                                                $paramTrxArr[0]['VA_NUMBER'] = $columns[1];
                                          }

                                          if ($action == '1') {
                                                $paramTrxArr[0]['TRA_AMOUNT'] = $columns[2];
                                                $paramTrxArr[0]['EXP_DATE'] = $columns[4];
                                                $paramTrxArr[0]['EXP_TIME'] = $columns[5];
                                          }
                                    }

                                    $arr[$no]['paramPayment'] = $paramPayment;
                                    $arr[$no]['paramTrxArr'] = $paramTrxArr;
                              }
                              else{
                                    $errorData            = true;
                                    break;
                              }

                              $no++;
                        }

                        if (!$errorData) {
                              $validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);

                              $resultVal  = $validate->checkCreateEcollection($arr);
                              $payment    = $validate->getPaymentInfo();
                              $errorTrxMsg      = $validate->getErrorTrxMsg();

                              $trfDateType = $param['tranferdatetype_bulk'];

                              //insert array utk insert
                              $i = 0;
                              foreach ($data as $columns)
                              {     
                                    //add new va
                                    if ($action == '0') {
                                          $insertArr[$i] = array(
                                                'VA_TYPE' => $binData['VA_TYPE'], // static or dynamic
                                                'CUST_BIN' => $bin,
                                                'VA_NAME' => $columns[2],
                                                'CCY' => 'IDR',
                                                'INVOICE_AMOUNT' => $columns[3],
                                                'IN_BILLING' => $columns[3],
                                                'MESSAGE' => $columns[4],
                                                'VA_STATUS' => 1, //1 open, 2 closed, 3 deleted
                                                "FILE_ID"       => $fileId
                                          );

                                          //if static
                                          if ($binData['VA_TYPE'] != '0') {

                                                $binLength = strlen($bin);

                                                $vanumber = substr($columns[1], $binLength); 
                                                $insertArr[$i]['VA_NUMBER'] = $vanumber;
                                          }
                                          //if dynamic then auto generated

                                          //if futuredate
                                          if ($trfDateType == '2') {
                                                $insertArr[$i]['DOC_STATUS'] = 1; //0 inbilling, 1 pending future date, 2 settled, 3 unsettled, 4 cancelled
                                                $insertArr[$i]['EF_DATE'] = $this->_getParam('PS_FUTUREDATE_bulk');
                                                $efstatus = 'Future Date';
                                                $efdate = $this->_getParam('PS_FUTUREDATE_bulk');
                                          }
                                          // periodic
                                          else if($trfDateType == '3'){
      //TO DO: PERIODIC 
                                          }
                                          else{ 
                                                $insertArr[$i]['DOC_STATUS'] = 0; //0 inbilling, 1 pending future date, 2 settled, 3 unsettled, 4 cancelled
                                                $insertArr[$i]['EF_DATE'] = date('Y-m-d H:i:s');
                                                $efstatus = 'Immediate';
                                                $efdate = date('Y-m-d H:i:s');
                                          }

                                          //input later
                                          if ($binData['EXP_TIME'] == 0) {

                                                $date = Application_Helper_General::convertDate($columns[5],'Y-MM-d','dMMyyyy');
                                                $time = Application_Helper_General::convertDate($columns[6],'HH:mm:ss','hhmm');

                                                $insertArr[$i]['EXP_DATE'] = $date.' '.$time;

                                                $expdate = '-';
                                          }
                                          else{
                                                $exTimeType = $binData['EX_TIME_TYPE'];
                                                $exTime = $binData['EX_TIME'];

                                               //minutes
                                                if ($exTimeType == '0') {
                                                      $exp_date = date('Y-m-d H:i:s',strtotime($efdate." +".$exTime." minute"));
                                                      $type = 'Minute(s)';
                                                }
                                                //hours
                                                else if ($exTimeType == '1') {
                                                      $exp_date = date('Y-m-d H:i:s',strtotime($efdate." +".$exTime." hour"));
                                                      $type = 'Hour(s)';
                                                }
                                                else {
                                                      $exp_date = date('Y-m-d H:i:s',strtotime($efdate." +".$exTime." day"));
                                                      $type = 'Day(s)';
                                                }

                                                $insertArr[$i]['EXP_DATE'] = $exp_date;

                                                $expdate = 'Predefined set to '.$exTime.' '.$type.' after effective';
                                          }  
                                    }
                                    //overwrite
                                    else if($action == '1'){
                                          $insertArr[$i] = array(
                                                'ACTION' => 'overwrite',
                                                'VA_TYPE' => $binData['VA_TYPE'], // static or dynamic
                                                'INVOICE_AMOUNT' => $columns[2],
                                                'IN_BILLING' => $columns[2],
                                                'MESSAGE' => $columns[3],
                                                "FILE_ID" => $fileId
                                          );

                                          //if static
                                          if ($binData['VA_TYPE'] != '0') {

                                                $insertArr[$i]['VA_ID'] = $columns[1];
                                          }
                                          else{
                                                $insertArr[$i]['VA_NUMBER'] = $columns[1];
                                          }

                                          $date = Application_Helper_General::convertDate($columns[4],'Y-MM-d','dMMyyyy');
                                          $time = Application_Helper_General::convertDate($columns[5],'HH:mm:ss','hhmm');

                                          $insertArr[$i]['EXP_DATE'] = $date.' '.$time;

                                          //if futuredate
                                          if ($trfDateType == '2') {
                                                $insertArr[$i]['DOC_STATUS'] = 1; //0 inbilling, 1 pending future date, 2 settled, 3 unsettled, 4 cancelled
                                                $insertArr[$i]['EF_DATE'] = $this->_getParam('PS_FUTUREDATE_bulk');
                                                $efstatus = 'Future Date';
                                                $efdate = $this->_getParam('PS_FUTUREDATE_bulk');
                                          }
                                          // periodic
                                          else if($trfDateType == '3'){
      //TO DO: PERIODIC 
                                          }
                                          else{ 
                                                $insertArr[$i]['DOC_STATUS'] = 0; //0 inbilling, 1 pending future date, 2 settled, 3 unsettled, 4 cancelled
                                                $insertArr[$i]['EF_DATE'] = date('Y-m-d H:i:s');
                                                $efstatus = 'Immediate';
                                                $efdate = date('Y-m-d H:i:s');
                                          }

                                          $expdate = 'In file';
                                    }
                                    //cancel
                                    else{
                                          $insertArr[$i] = array(
                                                'ACTION' => 'cancel',
                                                'VA_TYPE' => $binData['VA_TYPE'], // static or dynamic
                                                'INVOICE_AMOUNT' => 0, 
                                                'IN_BILLING' => 0,  
                                                'SETTLED_AMOUNT' => 0, 
                                                'UNSETTLED_AMOUNT' => 0, 
                                                'AVAILABLE_CREDIT' => 0, 
                                                'REMARKS' => $columns[3],
                                                'DOC_STATUS' => 4, //0 inbilling, 1 pending future date, 2 settled, 3 unsettled, 4 cancelled
                                                "FILE_ID" => $fileId
                                          );

                                          //if static
                                          if ($binData['VA_TYPE'] != '0') {

                                                $insertArr[$i]['VA_ID'] = $columns[1];
                                          }
                                          else{
                                                $insertArr[$i]['VA_NUMBER'] = $columns[1];
                                          }
                                    }

                                    $i++;
                              }

                               //dipakai untuk insert data ke db
                              $sessionNamespace->insertArr     = $insertArr;
                                    

                              $content['payment'] = $payment;
                              $content['arr']   = $arr;
                              $content['errorTrxMsg'] = $errorTrxMsg;

                              $paramSession = array(
                                    'addtype' => 'bulk',
                                    'bin' => $bin,
                                    'efstatus' => $efstatus,
                                    'efdate' => $efdate,
                                    'expdate' => $expdate,

                                    //dipakai utk back data
                                    'idfile_bulk' => $fileId,
                                    'uploadaction_bulk' => $action,
                                    'trfDateType' => $this->_getParam('tranferdatetype_bulk'),
                                    'tranferdateperiodictype' => $this->_getParam('tranferdateperiodictype_bulk'),
                                    'PS_FUTUREDATE' => $this->_getParam('PS_FUTUREDATE_bulk'),
                                    'PERIODIC_EVERY' => $this->_getParam('PERIODIC_EVERY_bulk'),
                                    'PERIODIC_EVERYDATE' => $this->_getParam('PERIODIC_EVERYDATE_bulk'),
                              );

                              $sessionNamespace = new Zend_Session_Namespace('ecollection');
                              $sessionNamespace->content = $content;
                              $sessionNamespace->rawData = $rawData; //utk btn download
                              $sessionNamespace->binData     = $binData;
                              $sessionNamespace->paramSession     = $paramSession;
                              $sessionNamespace->action     = $action; //add new / overwrite / cancel
                              $this->_redirect('/virtualaccount/newecollection/nextbulk');     
                        }
                  }
                  else{
                        $this->view->bulk = true;
                        $this->view->error_msg = $error_msg;

                        $this->view->bin = $bin;
                        $this->view->idfile_bulk = $fileId;
                        $this->view->uploadaction_bulk = $action;
                  }

            }
            //------------------------------------------------------end bulk---------------------------------------------------------
        }
        
        //if back from next
        if($back){

            $this->view->back = true;

            if ($paramSession['addtype'] == 'bulk') {
                  $this->view->bulk = true;
            }
            else{
                $this->view->single = true;  
            }
           
            foreach ($paramSession as $key => $value) {
                  $this->view->$key = $value;
            }

            $this->view->efdate = $paramSession['b_efdate'];
            $this->view->notif = $paramSession['b_notif'];
            $this->view->PS_EFDATEFUTURE = $paramSession['PS_FUTUREDATE'];
           
            $sessionNamespace->back = false;
        }
        else
        {
            unset($_SESSION['ecollection']);
        }
      }

      //---------------------------------------------------next single--------------------------------------------------
      public function nextAction()  
      {
            $this->_helper->layout()->setLayout('newlayout');
            $sessionNamespace       = new Zend_Session_Namespace('ecollection');
            $paramSession           = $sessionNamespace->paramSession;
            $binData           = $sessionNamespace->binData; 
            $insertArr           = $sessionNamespace->insertArr; 

            foreach ($paramSession as $key => $value) {
                  $this->view->$key = $value;
            }

            $this->view->email = $this->view->email_notif;
            $this->view->sms = $this->view->sms_notif;

            //bin info html
            $this->view->bininfohtml = $this->binInfoHtml($binData);

            if($this->_request->isPost() )
            {
               $param = $this->_request->getParams();

               $back = $param['back'];
               $submit = $param['submit'];

               if ($back) {
                  $sessionNamespace->back    = true;
                  $this->_redirect('/virtualaccount/newecollection/index');
               }

               if ($submit) {
                  try 
                  {
                        // echo "<pre>";
                        // var_dump($insertArr);die;
                        $SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);                           
                        $result = $SinglePayment->createVirtualAccountPayment($insertArr);

                        $this->setbackURL('/'.$this->_request->getModuleName().'/binecollection/index/filter/1/BIN/'.$insertArr['CUST_BIN'].'/vatype/'.$insertArr['VA_TYPE'].'/');
                        $this->_redirect('/notification/success/index');
                        
                  }
                  catch(Exception $e)
                  {
                        Application_Helper_General::exceptionLog($e);
                        $errr = 1;
                  }
               }
            }
      }
      
      //---------------------------------------------------next bulk--------------------------------------------------
      public function nextbulkAction()  
      {
            $this->_helper->layout()->setLayout('newlayout');
            $sessionNamespace       = new Zend_Session_Namespace('ecollection');
            $paramSession           = $sessionNamespace->paramSession;
            $insertArr              = $sessionNamespace->insertArr;
            $action                 = $sessionNamespace->action;
            $rawData                 = $sessionNamespace->rawData;

            $actionArr = array(
                  '0' => 'Add New VA',
                  '1' => 'Overwrite VA',
                  '2' => 'Cancel VA'
            );

            $this->view->uploadAction = $actionArr[$action];

            $this->view->paramSession = $paramSession;

            $data             = $sessionNamespace->content;
            $binData          = $sessionNamespace->binData;

            //bin info html
            $this->view->bininfohtml = $this->binInfoHtml($binData);
            $this->view->binData = $binData;

            if(!$data["payment"]["countTrxCCY"])
            {
                  $this->_redirect("/authorizationacl/index/nodata");
            }
            
            $this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
            
            $this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];
            
            $totalSuccess = 0;
            foreach($data["payment"]["countTrxCCY"] as $row)
            {
                  foreach($row as $ccy)
                  {
                        $totalSuccess += $ccy['success'];
                  }
            }
            
            $amountSuccess = 0;
            foreach($data["payment"]["sumTrxCCY"] as $row)
            {
                  foreach($row as $ccy)
                  {
                        $amountSuccess += $ccy['success'];
                  }
            }
            
            $totalFailed = 0;
            foreach($data["payment"]["countTrxCCY"] as $row)
            {
                  foreach($row as $ccy)
                  {
                        $totalFailed += $ccy['failed'];
                  }
            }
            
            $amountFailed = 0;
            foreach($data["payment"]["sumTrxCCY"] as $row)
            {
                  foreach($row as $ccy)
                  {
                        $amountFailed += $ccy['failed'];
                  }
            }

            $this->view->totalSuccess = $totalSuccess;
            $this->view->amountSuccess = $amountSuccess;
            $this->view->totalFailed = $totalFailed;
            $this->view->amountFailed = $amountFailed;
            $this->view->totalTrx = $data['payment']['countTrxTOTAL'];
            $this->view->errorTrxMsg = $data['errorTrxMsg']['VA']['IDR'];

            if($this->_request->isPost() )
            {

               $param = $this->_request->getParams();

               $back = $param['back'];
               $submit = $param['submit'];
               $download = $param['download'];

               if ($back) {
                  $sessionNamespace->back    = true;
                  $this->_redirect('/virtualaccount/newecollection/index');
               }

               if ($submit) {

                  foreach ($data['errorTrxMsg']['VA']['IDR'] as $key => $value) {
                        unset($insertArr[$key]);
                  }

                  $success = 0;
                  $SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);   
                  foreach ($insertArr as $value) {

                        if ($action == '0') {
                              try 
                              {                        
                                    $result = $SinglePayment->createVirtualAccountPayment($value);
                                    $success++;
                              }
                              catch(Exception $e)
                              {
                                    Application_Helper_General::exceptionLog($e);
                                    $errr = 1;
                              }  
                        }
                        else{
                              try 
                              {                        
                                    $result = $SinglePayment->updateVirtualAccountPayment($value);
                                    $success++;
                              }
                              catch(Exception $e)
                              {
                                    Application_Helper_General::exceptionLog($e);
                                    $errr = 1;
                              }  
                        }  
                  }

                  if ($success == $totalSuccess) {
                        $this->setbackURL('/'.$this->_request->getModuleName().'/binecollection/index/');
                        $this->_redirect('/notification/success/index');
                  }
               }

               if ($download) {
                     $header = $rawData[0];
                     unset($rawData[0]);
                     $dataValue = $rawData;

                     $result = $this->_helper->download->txtBatch($header,$dataValue,null,'VA_UPLOAD_LIST');  
               }
            }
      }

      public function bincheckAction(){
          $this->_helper->viewRenderer->setNoRender();
          $this->_helper->layout()->disableLayout();

          $bin = $this->_getParam('bin');
          $bulk = $this->_getParam('bulk');

            //select data bin
            $select = $this->_db->select()
              ->from(array('B' => 'M_CUSTOMER_BIN'),array('*'))
              ->where('B.CUST_ID = ?', (string)$this->_custIdLogin)
              ->where('B.CUST_BIN = ?', (string)$bin)
              ->query()->fetchAll();

            $resultdata = $select[0];

            //html utk btn view info
            $html = $this->binInfoHtml($resultdata);

            $result = array(
                  'bininfohtml' => $html,
                  'bindata' => $resultdata
            );

            //droplist va number kalau static
            if ($resultdata['VA_TYPE'] != 0 && $bulk != 1) {
                  $vaNumberData = $this->_db->select()->distinct()
                    ->from(array('B' => 'T_VA_TRANSACTION'),array('VA_NUMBER', 'VA_NAME'))
                    ->where('B.CUST_ID = ?', (string)$this->_custIdLogin)
                    ->where('B.CUST_BIN = ?', (string)$bin)
                    ->query()->fetchAll();

                  $result['vanumberdata'] = $vaNumberData;
            }

          echo json_encode($result);
      }


      public function binInfoHtml($resultdata){

            $binTypeArr = array(
                '1' => 'eCollection',
                '2' => 'VA Debit'
              );

              $optStatus = array(
            //'' => '-- Please Select --',
                '1' => $this->language->_('No'),
                '0' => $this->language->_('Yes')
              );

              $optH2H = array(
            //'' => '-- Please Select --',
                '0' => $this->language->_('No'),
                '1' => $this->language->_('Yes')
              );

              $optSkip = array(
                '0' => $this->language->_('Yes'),
                '1' => $this->language->_('No')
              );

              //------------------------ecollection opt--------------------------------
              $optVA = array(
                '0' => $this->language->_('Dynamic Close'),
                '1' => $this->language->_('Static Open'),
                '2' => $this->language->_('Static Close')
              );

              $optExpTime = array(
                '0' => $this->language->_('Input Later'),
                '1' => $this->language->_('Predefined')
              );

              $optExTimeType = array(
                '0' => $this->language->_('Minute(s)'),
                '1' => $this->language->_('Hour(s)'),
                '2' => $this->language->_('Day(s)')
              );

              $optPayment = array(
                '0' => $this->language->_('Yes'),
                '1' => $this->language->_('No'),
                '2' => $this->language->_('Over'),
              );

              $optBill = array(
                '0' => $this->language->_('Total'),
                '3' => $this->language->_('Selected'),
                '1' => $this->language->_('FIFO'),
                '2' => $this->language->_('LIFO')
              );

              //----------------------Va Debit opt-------------------------------

              $optRemainingCredit = array(
                '0' => $this->language->_('Accumulate'),
                '1' => $this->language->_('Auto Reset'),
              );


              // print_r($resultdata);die;
              $h2h   = !empty($resultdata['HOSTTOHOST']) ? $optH2H[$resultdata['HOSTTOHOST']] : 'No';
              $url_inquiry   = $resultdata['URL_INQUIRY'];             
              $url_payment   = $resultdata['URL_PAYMENT'];  

              $allowskip        = $optSkip[$resultdata['SKIP_ERROR']];

              //ecollection
              $va_type   = $optVA[$resultdata['VA_TYPE']];
              $exp_time   = $optExpTime[$resultdata['EXP_TIME']];

              if ($resultdata['EXP_TIME'] == '1') {
                $exp_time .= ' ('.$resultdata['EX_TIME'].' '.$optExTimeType[$resultdata['EX_TIME_TYPE']].')';
              }

              $partial_type   = $optPayment[$resultdata['PARTIAL_TYPE']];
              $bill_method   = $optBill[$resultdata['BILLING_TYPE']];
              $vp_max   = 'IDR '.Application_Helper_General::displayMoney($resultdata['VP_MAX']);
              $vp_min   = 'IDR '.Application_Helper_General::displayMoney($resultdata['VP_MIN']);
              $seller_fee   = 'IDR '.Application_Helper_General::displayMoney($resultdata['SELLER_FEE']);
              $buyer_fee   = 'IDR '.Application_Helper_General::displayMoney($resultdata['BUYER_FEE']);

              //Va Debit
              $va_credit_max   = 'IDR '.Application_Helper_General::displayMoney($resultdata['VA_CREDIT_MAX']);
              $va_daily_limit   = 'IDR '.Application_Helper_General::displayMoney($resultdata['VA_DAILY_LIMIT']);
              $remainingCredit   = $optRemainingCredit[$resultdata['REMAINING_AVAILABLE_CREDIT']];

              $dayArr = array(
                '0' => 'Monday',
                '1' => 'Tuesday',
                '2' => 'Wednesday',
                '3' => 'Thursday',
                '4' => 'Friday',
                '5' => 'Saturday',
                '6' => 'Sunday',
              );

              $monthArr = array(
                '1' => 'January',
                '2' => 'February',
                '3' => 'March',
                '4' => 'April',
                '5' => 'May',
                '6' => 'June',
                '7' => 'July',
                '8' => 'August',
                '9' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December'
              );

              if ($resultdata['BIN_TYPE'] == '2' && ($resultdata['HOSTTOHOST'] == '0' || $resultdata['HOSTTOHOST'] == '')) {
                if ($resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
                  if ($resultdata['EX_TIME_TYPE'] == '2') {
                    $exTime = 'Weekly every '. $dayArr[$resultdata['EX_TIME']];
                  }
                  else if ($resultdata['EX_TIME_TYPE'] == '3') {
                    $exTime = 'Monthly on '. $resultdata['EX_TIME'];
                  }
                  else if ($resultdata['EX_TIME_TYPE'] == '4') {
                    $explodeTime = explode('_', $resultdata['EX_TIME']);

                    $exTime = 'Yearly every '. $explodeTime[0].' '.$monthArr[$explodeTime[1]];
                  }

                  $remainingCredit .= ' ('.$exTime.')';
                }
            }


              $html = '<div class="container-fluid">
                          <div class="row content-box" style="background-color: #f2f2f2">
                                <table width="100%">
                                    <tr>
                                    <td class="text-black-50" width="50%">'.$this->language->_('BIN').'</td>
                                    <td>'.$resultdata['CUST_BIN'].'</td>
                                  </tr>
                                  <tr>
                                    <td class="text-black-50" width="50%">'.$this->language->_('BIN Type').'</td>
                                    <td>'.$binTypeArr[$resultdata['BIN_TYPE']].'</td>
                                  </tr>';
                                
                        if ($h2h == 'Yes') { 
                           $html .= '<tr>
                                      <td class="text-black-50" width="50%">'.$this->language->_('URL Inquiry').'</td>
                                      <td>'.$url_inquiry.'</td>
                                    </tr>
                                    <tr>
                                      <td class="text-black-50" width="50%">'.$this->language->_('URL Payment').'</td>
                                      <td>'.$url_payment.'</td>
                                    </tr>';
                              }

                  $html .= '</table>
                          </div>
                          <br>';

                          

                  if ($h2h == 'No') {
                  $html .= '
                           <h6>'.$binTypeArr[$resultdata['BIN_TYPE']].' '.$this->language->_('Parameter Setting').'</h6>
                           <div class="row content-box" style="background-color: #f2f2f2">
                           <table width="100%">';

                        if ($resultdata['BIN_TYPE'] == '1') {
                     $html .= '<tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('VA Type').'</td>
                                <td>'.$va_type.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('Expired Time').'</td>
                                <td>'.$exp_time.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('Allow skip error when uploading').'</td>
                                <td>'.$allowskip.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('Partial Payment').'</td>
                                <td>'.$partial_type.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('Billing Method').'</td>
                                <td>'.$bill_method.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('Buyer Fee').'</td>
                                <td>'.$buyer_fee.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('Seller Fee').'</td>
                                <td>'.$seller_fee.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('VA Min Amount').'</td>
                                <td>'.$vp_min.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('VA Max Amount').'</td>
                                <td>'.$vp_max.'</td>
                              </tr>';

                            
                             } else {
                     $html .= '<tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('Allow skip error when uploading').'</td>
                                <td>'.$allowskip.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('VA Available Credit Max Limit').'</td>
                                <td>'.$va_credit_max.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('VA Daily Limit Transaction').'</td>
                                <td>'.$va_daily_limit.'</td>
                              </tr>
                              <tr>
                                <td class="text-black-50" width="50%">'.$this->language->_('Remaining Available Credit').'</td>
                                <td>'.$remainingCredit.'</td>
                              </tr>';
                            }

                  $html .= '</table>
                          </div>';

                          } 
                  $html .= '</div>';

            return $html;
      }
}
 