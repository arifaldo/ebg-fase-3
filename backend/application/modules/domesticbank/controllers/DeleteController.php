<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Domesticbank_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');  
	
	    $filters = array('bank_id' => array('StringTrim', 'StripTags'));
							 
		$validators =  array(
					    'bank_id' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_DOMESTIC_BANK_TABLE', 'field' => 'BANK_ID')),
							       	  'messages' => array(
							   						  'Cannt be empty',
							   					      'Bank ID is not founded',
							   						     ) 
							             )
					        );
			
		if(array_key_exists('bank_id',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
				    $this->_db->beginTransaction();
				    
					$bankId  = $zf_filter_input->getEscaped('bank_id'); 
					
					
//					$this->_db->delete('M_DOMESTIC_BANK_TABLE','BANK_ID = ?', $bankId);
				   	$this->_db->delete('M_DOMESTIC_BANK_TABLE','BANK_ID = '.$this->_db->quote($bankId));
					
					$this->_db->commit();		
						 
					Application_Helper_General::writeLog('DBUD','Delete Domestic Bank. Bank ID : ['.$bankId.']');
					$this->_redirect('/notification/success/index');
					
					/*$msg_success = 'Record Deleted';
					
        	        $this->_helper->getHelper('FlashMessenger')->addMessage('S');
			        $this->_helper->getHelper('FlashMessenger')
						   ->addMessage(($msg_success !="")? $msg_success : null);
			   
			 
			 
			        $this->backendLog('A', $this->_moduleDB, $bankId  , null, null);
			        */
				    
						
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						Application_Helper_General::writeLog('DBUD','Update domestic bank');
						
						$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
						
					}					
				}	
			} 
			
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('DBUD','Update domestic bank');
			
			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
