<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
class accountstatement_BalancedetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$listId = $this->_db->select()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID'))
						->order('CUST_ID ASC')
						->query()->fetchAll();     	
		$this->view->listCustId = Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');

		$filterlist = array('COMP_ID','COMP_NAME','ACC_NUMBER');
		
		$this->view->filterlist = $filterlist;

		$cust_id = $this->_getParam('COMP_ID');
		$cust_name = $this->_getParam('COMP_NAME');
		$accnumber = $this->_getParam('ACC_NUMBER');
		$filter = $this->_getParam('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		if( ($filter == TRUE || $csv || $pdf) && ( !empty($cust_id) || !empty($cust_name) ) )
		{
			$this->view->cust_id = $cust_id;
			$this->view->cust_name = $cust_name;
			$this->view->accnumber = $accnumber;

			$query  = $this->_db->SELECT()
												->FROM(array('C' => 'M_CUSTOMER'));
			
			if(!empty($cust_id))
				$query->WHERE("C.CUST_ID = ?",$cust_id);
			if(!empty($cust_name))
				$query->WHERE("C.CUST_NAME like ".$this->_db->quote('%'.$cust_name.'%'));

			$query->ORDER('C.CUST_NAME');
			$arrCustInfo = $this->_db->fetchAll( $query);

			foreach($arrCustInfo as $key=>$custInfo)
			{
				$arrCustname[$key]		= $custInfo['CUST_NAME'];
				$arrCustID[$key]		= $custInfo['CUST_ID'];
				$arrCustadd[$key]		= $custInfo['CUST_ADDRESS'];
				$arrCustcity[$key]		= $custInfo['CUST_CITY'];
				$arrCustprov[$key]		= $custInfo['CUST_PROVINCE'];
				$arrCustzip[$key]			= $custInfo['CUST_ZIP'];
				$arrCustphone[$key]	= $custInfo['CUST_PHONE'];
				$arrCustfax[$key]			= $custInfo['CUST_FAX'];

				$query = $this->_db->select()
													 ->from(array('A' => 'M_CUSTOMER_ACCT'))
													 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
													 ->where("A.ACCT_STATUS = 1")
													 ->where("A.CUST_ID = ?",$custInfo['CUST_ID'])
													 ->order('B.GROUP_NAME DESC')
													 ->order('A.ORDER_NO ASC');
				if($accnumber)
					$query->where("A.ACCT_NO like ".$this->_db->quote('%'.$accnumber.'%'));
				
				$data = $this->_db->fetchAll( $query );

				$isGroup = false;
				if(count($data) > 0)
				{
					foreach($data as $row)
					{
						if($row['GROUP_ID'])
						{
							$isGroup = true;
						}
					}
					$viewIsGroup[$key] = $isGroup;
					$arrCustId[$key] = $custInfo['CUST_ID'];
					$arrResultdata[$key] = $data;
				}
			}
	// SENT TO VIEW =====================================================================		
			$this->view->custname		= (isset($arrCustname) ? $arrCustname : NULL);
			$this->view->arrCustID		= (isset($arrCustID) ? $arrCustID : NULL);
			$this->view->custadd		= (isset($arrCustadd) ? $arrCustadd : NULL);
			$this->view->custcity		= (isset($arrCustcity) ? $arrCustcity : NULL);
			$this->view->custprov		= (isset($arrCustprov) ? $arrCustprov : NULL);
			$this->view->custzip		= (isset($arrCustzip) ? $arrCustzip : NULL);
			$this->view->custphone		= (isset($arrCustphone) ? $arrCustphone : NULL);
			$this->view->custfax		= (isset($arrCustfax) ? $arrCustfax : NULL);
			$this->view->isGroup 		= (isset($viewIsGroup) ? $viewIsGroup : NULL);
			$this->view->arrcustId 		= (isset($arrCustId) ? $arrCustId : NULL);
			$this->view->resultdata 	= (isset($arrResultdata) ? $arrResultdata : NULL);
	// END SENT TO VIEW =====================================================================		

			$logDesc = 'View Balance Inquiry';

	// PDF & CSV ============================================================================
			if($csv)
			{
				$arr = array();
				if(is_array($arrCustId))
				{
					foreach($arrCustId as $key=>$custId)
					{
						$isGroup 					= $viewIsGroup[$key];
						$data 							= $arrResultdata[$key];

						$arr[count($arr)+1] = array('Company Code ',$custId);

						$header = array(
							'Account Number',
							'Account Alias',
							'Account Name',
							'Currency',
							'Type',
							'Status',
							'Effective Balance',
						);
						if(!$isGroup)
						{
							$arr[count($arr)+1] = $header;
							$i = count($arr)+2;
							$arr_value_tot_group =  array();
							$arr_display_tot_group =  array();
							foreach($data as $row)
							{
							
								$systemBalance = new SystemBalance($row['CUST_ID'],$row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']));
								$systemBalance->checkBalance();
								
								
								if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
								  $arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
								}
								else {
								  $arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
								}
								$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row['CCY_ID']]);
								
								$arr[$i][] = $row['ACCT_NO'];
								$arr[$i][] = $row['ACCT_ALIAS_NAME'];
								$arr[$i][] = $systemBalance->getCoreAccountName();
								$arr[$i][] = $row['CCY_ID'];
								$arr[$i][] = $row['ACCT_DESC'];
								$arr[$i][] = $systemBalance->getCoreAccountStatusDesc();
								$arr[$i][] = $systemBalance->getEffectiveBalance();
								
								$i++;
							}
							$display_tot_group = implode("; ",$arr_display_tot_group);
							$arr[$i][] = 'TOTAL EFFECTIVE BALANCE : '.$display_tot_group;	
						}
						else
						{
							$i = count($arr)+1;
							$group = null;
							
							$resultdataGroup =  array();
							foreach ($data as $rowGroup){
								$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup; 
							}
							$i = count($arr)+1;
							$group = null;
							$arr_value_tot_group = array();
							foreach ( $resultdataGroup as $groupname=>$rowPerGroup ) 
							{
								$i++;
								$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
								$groupname = ($groupname) ? $groupname : 'OTHERS';
								$arr[$i][] = $groupname;
								$i++;
								$arr[$i][] = $header;
								
									if(count($rowPerGroup) > 0){
										$arr_value_tot_group =  array();
										$arr_display_tot_group =  array();
										foreach ($rowPerGroup as $row){
										$i++;	
										$systemBalance = new SystemBalance($row['CUST_ID'],$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
										$systemBalance->checkBalance();
											if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
												$arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
											}else{
												$arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
											}
											$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);

											$arr[$i][] = $row['ACCT_NO'];
											$arr[$i][] = $row['ACCT_ALIAS_NAME'];
											$arr[$i][] = $systemBalance->getCoreAccountName();
											$arr[$i][] = $row['CCY_ID'];
											$arr[$i][] = $row['ACCT_DESC'];
											$arr[$i][] = $systemBalance->getCoreAccountStatusDesc();
											$arr[$i][] = $systemBalance->getEffectiveBalance();
										}
									}
									$i++;	
									$display_tot_group = implode("; ",$arr_display_tot_group);	
									$arr[$i][] = 'TOTAL EFFECTIVE BALANCE : '.$display_tot_group;
							}
						}
					}
				}
					$logDesc = 'Download CSV Balance Inquiry';
					$this->_helper->download->csv(array(),$arr,null,'Balance Detail');  
			
			}
			
				if($pdf){
					$logDesc = 'Download PDF Balance Inquiry';
					$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf.phtml')."</td></tr>";
					$this->_helper->download->pdf(null,null,null,'Balance Detail',$outputHTML);
				}
	// PDF & CSV ======================================================================


	// WRITE LOG ======================================================================
		Application_Helper_General::writeLog('BAIQ',$logDesc);
		}
		else if($filter == 'Clear Filter')
		{
			$this->_redirect('/accountstatement/balancedetail');
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('BAIQ','View Balance Inquiry');
		}
	}
	
}
