<?php

require_once 'Zend/Controller/Action.php';

class useropenlimit_RepairController extends Application_Main 
{
  public function indexAction() 
  {
  		$this->_helper->layout()->setLayout('popup');
  		$params = $this->_request->getParams();
		
  		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

		$conf = Zend_Registry::get('config');

		$this->view->masterbankname = $conf['app']['bankname'];

  		if(is_array($params) && count($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;

  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
					'messages' => array(
						$this->language->_('Suggestion ID does not exist.'),
						$this->language->_('Suggestion ID must be number.'),
					),
  				),
  			);

  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
	  			$cust_id = $user_id = null;
	  			$changeId = $zf_filter_input->changes_id;
				
				$tempData = $this->_db->fetchRow(
	  				$this->_db->select()
	  				->from('TEMP_MAKERLIMIT',array('USER_LOGIN','CUST_ID'))
					->where('CHANGES_ID = ?', $changeId)
	  			);
				
				if(isset($tempData['CUST_ID']) && isset($tempData['USER_LOGIN']))
				{
					$cust_id = $tempData['CUST_ID'];
					$user_id = $tempData['USER_LOGIN'];
				}
  	
				if($cust_id && $user_id)
				{
				  $select = $this->_db->select()
										 ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'))
										 ->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($cust_id)));
				  $result = $this->_db->fetchRow($select);
				  if($result['CUST_ID'])
				  {
					$this->view->cust_name = $result['CUST_NAME'];
				  }
				  else{ $cust_id = null; }
				}
				
				if(!$cust_id)
				{
				  $error_remark = $this->language->_('Customer ID does not exist.');
				  //insert log
				  /*try 
				  {
					$this->_db->beginTransaction();
					$this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
					$this->_db->commit();
				  }
				  catch(Exception $e) 
				  {
					$this->_db->rollBack();
					SGO_Helper_GeneralLog::technicalLog($e);
				  }*/
					
				  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
				  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				  $this->_redirect($this->_helper->url->url(array('module'=>'userlimit','controller'=>'suggestiondetail','action'=>'index')));
				}
										 
				$fields = array('bank_name'       => array('field'    => 'A.ACCT_NO',
													   'label'    => $this->language->_('Bank Name'),
													   'sortable' => false),
								'acct_no'       => array('field'    => 'A.ACCT_NO',
													   'label'    => $this->language->_('Account No'),
													   'sortable' => false),
				
								'acct_name'    => array('field'   => 'A.ACCT_NAME',
													  'label'    => $this->language->_('Account Name'),
													  'sortable' => false),
				
								'ccy'   	 => array('field'   => 'A.CCY_ID',
													  'label'    => $this->language->_('Currency'),
													  'sortable' => false),
								);
				$sortBy = $this->_getParam('sortby','acct_no');
				$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
				$sortDir = $this->_getParam('sortdir','asc');
				$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
				
				$this->view->sortBy = $sortBy;
				$this->view->sortDir = $sortDir;
			
				$select = $this->_db->select()
									 ->from(array('A' => 'M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_NAME','CCY_ID','MAXLIMIT' => '(SELECT MAXLIMIT FROM TEMP_MAKERLIMIT WHERE USER_LOGIN=\''.$user_id.'\' AND CUST_ID=\''.$cust_id.'\' AND ACCT_NO=A.ACCT_NO)'))
									 ->where('UPPER(A.CUST_ID)='.$this->_db->quote(strtoupper($cust_id)))
									 ->order($sortBy.' '.$sortDir)
									 ->query()->fetchAll();	
				
				$this->view->data = $select;
				
				
				
				$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						  ->where("A.FIELD IN ('account_number','account_name','account_currency')")
						 ->where("A.CUST_ID IN ('".$cust_id."')")
						 ->order('B.BANK_NAME ASC')
						 //->order('A.APIKEY_ID ASC')
						  //echo $acctlist;
				);
						 // echo $acctlist;die; 
		// echo '<pre>';
		// echo $acctlist;
		 
		$masterData = $this->_db->fetchAll(
						$this->_db->select()
							->from(array('A' => 'TEMP_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
							->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
							//->joinLeft(array('B' => 'M_CUSTOMER_ACCT'),'A.ACCT_NO = B.ACCT_NO',array('CCY_ID','ACCT_NAME'))
							->where('A.CUST_ID = ?', $cust_id)
							->where('A.USER_LOGIN = ?', $user_id)
							//->where('A.MAXLIMIT > 0')
							->where('A.MAXLIMIT_OPEN = 1')
							->where('A.MAKERLIMIT_STATUS <> 3')
							->order('B.BANK_NAME ASC')
						//	->where('B.ACCT_STATUS <> 3')
							//->order($sortBy.' '.$sortDir)
					);
		 
		 //echo '<pre>';
		 //var_dump($masterData);die;
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['APIKEY_ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['APIKEY_ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['APIKEY_ID']]['BANK_NAME'] = $value['BANK_NAME'];
			if($value['FIELD'] == 'account_number'){
				foreach ($masterData as $newkey => $valuemaster) {
					if($value['VALUE'] == $valuemaster['ACCT_NO']){
						$account[$value['APIKEY_ID']]['MAXLIMIT'] = $valuemaster['MAXLIMIT'];
					}
				}
			}
			
		}
		//echo '<pre>'; 
		//var_dump($account);die;
		
		$this->view->dataapi = $account;
		
				$this->view->fields = $fields;
				
				$this->view->status_type = $this->_masterglobalstatus;
	  			$this->view->changes_id = $changeId;
				$this->view->cust_id = $cust_id;
				$this->view->user_id = $user_id;
				$this->view->modulename = $this->_request->getModuleName();
			}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			// $errorRemark = Application_Helper_General::getErrorRemark($errors);
	  			// $this->backendLog($actionId,$this->_moduleDB,null,$fullDesc,$errorRemark);
	  			
	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
      			// $this->_redirect($this->_backURL);
	  		}
		}
  		else
  		{
  			$errorRemark = 'Suggestion ID does not exist';
  			// $this->backendLog($actionId,$this->_moduleDB,null,$fullDesc,$errorRemark);
  			
  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
      		// $this->_redirect($this->_backURL);
  		}
  	
		if($this->_request->isPost() && $this->view->hasPrivilege('RSOB'))
		{
			$maxlimit = $this->_request->getParam('maxlimit');
			$maxlimitapi = $this->_request->getParam('maxlimitapi');
			$change_id = $this->_request->getParam('changes_id');
			$cust_id = $this->_request->getParam('cust_id');
			$user_id = $this->_request->getParam('user_id');
			// Zend_Debug::dump($change_id);die;
			foreach($maxlimit as $key=>$value)
			{
				$maxlimit[$key] = $maxlimit[$key]?Application_Helper_General::convertDisplayMoney($value):null;
				if(!$maxlimit[$key]) unset($maxlimit[$key]);
			}
			
			foreach($maxlimitapi as $key=>$value)
			{
				$maxlimitapi[$key] = $maxlimitapi[$key]?Application_Helper_General::convertDisplayMoney($value):null;
				if(!$maxlimitapi[$key]) unset($maxlimitapi[$key]);
			}
			
			try
			{
				$this->_db->beginTransaction();
				
				$this->updateGlobalChanges($change_id);

				//DELETE DATA TEMP
				$where = array('CHANGES_ID = ?' => $change_id);
				$this->_db->delete('TEMP_MAKERLIMIT', $where);
				
				//LOOPING HERE JIKA ACCOUNT YANG DI LIMIT BANYAK
				if(!empty($maxlimit)){
					foreach($maxlimit as $key=>$value)
					{
						$data = array(	'CHANGES_ID' => $change_id,
										'USER_LOGIN' => $user_id,
										'ACCT_NO' => (string)$key,
										'CUST_ID' => $cust_id,
										'MAXLIMIT' => $maxlimit[$key],
										'SUGGESTED' => new Zend_Db_Expr('now()'),	
										'SUGGESTEDBY' => $this->_userIdLogin,	
										'MAKERLIMIT_STATUS' => 1
									);
									//Zend_Debug::Dump($data);die;
						$this->_db->insert('TEMP_MAKERLIMIT',$data);
					}
				}
				if(!empty($maxlimitapi)){
					foreach($maxlimitapi as $key=>$value)
					{
						$data = array(	'CHANGES_ID' => $change_id,
										'USER_LOGIN' => $user_id,
										'ACCT_NO' => (string)$key,
										'CUST_ID' => $cust_id,
										'MAXLIMIT' => $maxlimit[$key],
										'SUGGESTED' => new Zend_Db_Expr('now()'),	
										'SUGGESTEDBY' => $this->_userIdLogin,	
										'MAKERLIMIT_STATUS' => 1,
										'MAXLIMIT_OPEN' => 1
									);
									//Zend_Debug::Dump($data);die;
						$this->_db->insert('TEMP_MAKERLIMIT',$data);
					}
				}
				//END LOOP
				
				//UPDATE STATUS JADI WA
				// $updateArr = array('CHANGES_STATUS' => 'WA');
				// $where['CHANGES_ID = ?'] = $changeId;
				// $this->_db->update('T_GLOBAL_CHANGES',$updateArr,$where);
				Application_Helper_General::writeLog('MLCR','Submitting Repair User Limit Cust ID : ( '.$cust_id.' ) User ID ( '.$user_id.' )');
				$this->_db->commit();
				
				// $this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
				//$this->view->success = true;
				//$msg = $this->getErrorRemark('00','Set User Limit');
				//$this->view->report_msg = $msg;
				
				$this->_redirect('/popup/successpopup');
			}
			catch(Exception $e) 
			{
				//rollback changes
				$this->_db->rollBack();
				$errorMsg = 'Database failed.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				//Zend_Debug::Dump($e->getMessages());die;
				// $this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
				// Application_Log_GeneralLog::technicalLog($e);
				//$this->_redirect($this->_backURL);			
			}
			
			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}	
			}
		}
		if(!$this->_request->isPost())
			Application_Helper_General::writeLog('MLCR','Viewing Repair User Limit Cust ID : ( '.$cust_id.' ) User ID ( '.$user_id.' )');
  }
}