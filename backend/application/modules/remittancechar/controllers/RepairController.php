<?php


require_once 'Zend/Controller/Action.php';


class remittancechar_RepairController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$changeid = $this->_getParam('changes_id');

		$remit = new Zend_Session_Namespace('remittance');

		if(!empty($changeid)){
			$remit->changeid = $changeid;
		}

		$chgid = "";

		if(isset($remit->changeid)){
			$chgid = $remit->changeid;
			$this->view->changeid = $chgid;
		}
		
		$select = $this->_db->select()->distinct()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
		$select -> where("A.CHANGES_ID LIKE ".$this->_db->quote($chgid));
		$result = $this->_db->fetchRow($select);

		$cust_id = $result['COMPANY_CODE'];
		
		$model = new remittancechar_Model_Remittancechar();
		$this->view->ccyArr = ( array(''=>'-')+Application_Helper_Array::listArray($model->getCCYList(),'CCY_ID','CCY_ID'));
		
		
		$select2 = $this->_db->select()
							->from('TEMP_CHARGES_REMITTANCE');
		$select2 -> where("CHANGES_ID LIKE ".$this->_db->quote($chgid));
		$result2 = $this->_db->fetchall($select2);
		
		$this->view->paginator = $result2;
	
		$charge_type = $result2[0]['CHARGE_TYPE'];

		if($charge_type=='3'){
			$this->view->title = 'Transfer Fee';
		}
		if($charge_type == '4'){
			$this->view->title = 'Full Amount Fee';
		}
		if($charge_type == '5'){
			$this->view->title = 'Provision Fee';
		}

		$this->view->id = $charge_type;
		$this->view->custid = $cust_id;
/////////////////////////////////////////////////////////////////////////////////////////////
		
		$submit = $this->_getParam('submit');
		if($submit)
		{
				$params = $this->getRequest()->getParams();
				Application_Helper_General::writeLog('CHCR','Repairing Remittance charges ('.$chgid.')');
				$this->_db->beginTransaction();
				try
				{
					$info = 'Repair Remittance Charges';
					$this->updateGlobalChanges($chgid,$info);
					
					$where = array('CHANGES_ID = ?' => $chgid);
					$this->_db->delete('TEMP_CHARGES_REMITTANCE',$where);
					
					if($charge_type=='3' || $charge_type=='4'){
						foreach ($params['ccy'] as $value){
							$amount_data = 'amount'.$value;  
							$ccy_amount = 'ccy'.$value;
							$charge_amt =	Application_Helper_General::convertDisplayMoney($params[$amount_data]);

							$params_insert = array(
									'CHANGES_ID' => $chgid,
									'CUST_ID' => $cust_id,
									'CHARGE_TYPE' => $charge_type,
									'CHARGE_CCY' => $value,
									'CHARGE_AMOUNT_CCY' => $params[$ccy_amount],
									'CHARGE_AMT' => $charge_amt
									
							);
							if($params[$ccy_amount]=='-'){
							
							}else{
								$result = $model->insertTemp($params_insert);
							}
								
						}
					}
					if($charge_type=='5'){						
						foreach ($params['ccy'] as $value){
							$amount_min = 'min'.$value;
							$amount_max = 'max'.$value;
							$amount_ccy = 'ccy'.$value;
							$pcy_amount = 'pcy'.$value;
								
							$params_insert = array(
									'CHANGES_ID' => $chgid,
									'CUST_ID' => $cust_id,
									'CHARGE_TYPE' => $charge_type,
									'CHARGE_CCY' => $value,
									'CHARGE_AMOUNT_CCY' => $params[$amount_ccy],
									'CHARGE_PROV_MIN_AMT' => Application_Helper_General::convertDisplayMoney($params[$amount_min]),
									'CHARGE_PROV_MAX_AMT' => Application_Helper_General::convertDisplayMoney($params[$amount_max]),
									'CHARGE_PCT' => $params[$pcy_amount],
							);
							
							$result = $model->insertTemp($params_insert);
								
						}
					}

					
					$this->_db->commit();						    
					$this->_redirect('/popup/successpopup/index');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}

				Zend_Session::namespaceUnset('remittance');
		}
		else
		{
			Application_Helper_General::writeLog('CHCR','View Repair Remittance charges page ('.$chgid.')');
		}
	}
}
