<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class user_DetailController extends user_Model_User{


  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $this->_custIdLogin;

    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));
//    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
  // $this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$cust_id.'/user_id/'.$user_id);

    $error_remark   = null;

    if($user_id && $cust_id)
    {
      $select = $this->_db->SELECT()
      ->FROM(array('u'=>'M_USER'))
      ->JOINLEFT(array('c'=>'M_CUSTOMER'),'c.CUST_ID = u.CUST_ID',array('CUST_NAME','CUST_CONTACT'))
                             //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
      ->WHERE('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
      ->WHERE('UPPER(u.USER_ID)='.$this->_db->quote((string)$user_id));
      $resultdata = $this->_db->fetchRow($select);

      if(is_array($resultdata))
      {

        // if($resultdata['USER_RRESET'] == 1 || $resultdata['USER_RPWD_ISBYBO'] == 1){
        //   $this->view->showwarning = true;
        // }
        $this->view->cust_name  = $resultdata['CUST_NAME'];
        $this->view->user_name  = $resultdata['USER_FULLNAME'];
        $this->view->user_email = $resultdata['USER_EMAIL'];
        $this->view->user_phone = $resultdata['USER_PHONE'];
        $this->view->token_id   = $resultdata['TOKEN_ID'];
        $this->view->user_ext   = $resultdata['USER_EXT'];
        $this->view->user_isnew   = $resultdata['USER_ISNEW'];

        //$this->view->user_group = $resultdata['FGROUP_NAME'];
        $this->view->user_status = $resultdata['USER_STATUS'];
        $this->view->user_islogin = $resultdata['USER_ISLOGIN'];
        $this->view->user_failed_attempt =  $resultdata['USER_FAILEDATTEMPT'];
        $this->view->user_is_login  = $resultdata['USER_ISLOGIN'];
        $this->view->user_is_locked = $resultdata['USER_ISLOCKED'];
        $this->view->lock_reason = $resultdata['USER_LOCKREASON'];
        $this->view->user_is_webservices = $resultdata['USER_ISWEBSERVICES'];

        $this->view->user_lastlogin   = $resultdata['USER_LASTLOGIN'];
        $this->view->user_created     = $resultdata['USER_CREATED'];
        $this->view->user_createdby   = $resultdata['USER_CREATEDBY'];
        $this->view->user_suggested   = $resultdata['USER_SUGGESTED'];


        $debit  = $this->_db->select()
        ->from('M_USER_DEBIT')
        ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
        ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
        ->query()->fetchAll();  		
        $debitarr = '';
        if(!empty($debit)){

         foreach($debit as $kval => $dval){
          if(empty($debit[$kval+1]['USER_DEBITNUMBER'])){
           $debitarr .= $dval['USER_DEBITNUMBER'];
         }else{
           $debitarr .= $dval['USER_DEBITNUMBER'].';';
         }
       }
     }		
     $this->view->debitarr = $debitarr;


     if(empty($resultdata['USER_SUGGESTEDBY'])){
      $this->view->user_suggestedby = $resultdata['CUST_CONTACT'];
    }else{
      $this->view->user_suggestedby = $resultdata['USER_SUGGESTEDBY'];
    }
    if(!empty($resultdata['FGROUP_ID'])){
      $template_id = $resultdata['FGROUP_ID'];
    }else{
      $template_id = 'A01';
    }

    $select = $this->_db->select()
    ->from('M_FTEMPLATE')
    ->where('UPPER(FTEMPLATE_ID)='.$this->_db->quote($template_id))
    ->query()->fetch();
    $user_role = $select['FTEMPLATE_DESC'];
    $this->view->role = $user_role;
    $this->view->user_updated     = $resultdata['USER_UPDATED'];
    $this->view->user_updatedby   = $resultdata['USER_UPDATEDBY'];


    if($resultdata['USER_ISEMAIL']) $userIsEmail = $this->language->_('Yes');
    else $userIsEmail = $this->language->_('No');
    $this->view->userIsEmail = $userIsEmail;

    $this->view->statusreset=1;
    //Permintaan SASYA VERISSA BOENTORO 26-04-2013 FSD Forgot Password - CP2 Changing.pdf fixed by SUHANDI
    if(!$resultdata['USER_ISNEW'])
    {
      if($resultdata['USER_RRESET'] ==1 || $resultdata['USER_RPWD_ISBYBO']==1)
      {
        $this->view->statusreset=0;
      }
    }

    if($resultdata['USER_RRESET'] ==1 || $resultdata['USER_RPWD_ISBYBO']==1)
    {
      $this->view->statusreset=0;
    }
        // else
        // {
            // if($resultdata['USER_RRESET'] ==1)
      // {
        // $this->view->statusreset=0;
      // }
        // }


       //-------------------------------------------user privilege-------------------------------------------------------------
    $fuser_id  = $cust_id.$user_id;
    $privilege = $this->_db->select()
    ->from('M_FPRIVI_USER')
    ->where('FUSER_ID ='.$this->_db->quote($fuser_id))
    ->query()->fetchAll();

    if(count($privilege) > 0)
    {
      $priviListArr = Application_Helper_Array::simpleArray($privilege,'FPRIVI_ID');
      $this->view->priviview = $priviListArr;
    }

       //modifikasi privilege agar bisa ditampilkan sesuai golongan
    $this->view->getModuleDescArr = $this->getModuleDescArr();

    $privilege_final = array();
    $privilege_final_modif = array();

    $getprivilege = $this->getprivilege();
    foreach($getprivilege as $row)
    {
     $fprivi_moduleid = trim($row['FPRIVI_MODULEID']);
     $privilege_final[$fprivi_moduleid][] = $row;
     $privilege_final_modif = $this->modifPrivi($privilege_final);
   }

   $setting = new Settings();
   $system_type = $setting->getSetting('system_type');

   $this->view->fprivilege = $privilege_final_modif;
   $this->view->template   = Application_Helper_Array::listArray($this->getTemplate($system_type),'FTEMPLATE_ID','FTEMPLATE_DESC');

   $priviTemplate = array();



   foreach($this->getPriviTemplate() as $row)
   {
    $priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
  }

  $this->view->priviTemplate = $priviTemplate;

  $flag_template = 0;
  $user_role     = '';
  $template_id   = '';
  if(count($priviTemplate) > 0)
  {
   foreach($priviTemplate as $key => $row)
   {
    $count_row = count($row);
    $count_priviListArr = count($priviListArr);
    if($count_row == $count_priviListArr)
    {
      $arr_diff = array_diff_key(array_flip($priviListArr),$row);
      if(count($arr_diff) == 0)
      {
       $template_id = $key;
       $flag_template = 1;
       break;
     }
   }
 }
}


if($flag_template == 1){
  $select = $this->_db->select()
  ->from('M_FTEMPLATE')
  ->where('UPPER(FTEMPLATE_ID)='.$this->_db->quote($template_id))
  ->query()->fetch();
  $user_role = $select['FTEMPLATE_DESC'];
}else{
  $user_role = 'Custom';
}

$this->view->user_role = $user_role;

        //-------------------------------------------end user privilege-------------------------------------------------------------

$select = $this->_db->select()
->from('TEMP_USER',array('TEMP_ID'))
->where('UPPER(CUST_ID)='.$this->_db->quote($cust_id))
->where('UPPER(USER_ID)='.$this->_db->quote($user_id));

$result = $this->_db->fetchOne($select);
if($result){ $temp = 0; }else{ $temp = 1; }
$this->view->user_temp = $temp;

        //ambil user limit
$this->view->userLimitData = $this->_db->select()
->from('M_USER_LIMIT')
->where('CUST_ID = ?', $cust_id)
->where('USER_ID = ?', $user_id)
->query()
->fetchAll();

}else{ $user_id = null; }

}

if(!$user_id)
{
      // $error_remark = 'User ID is empty';
      //insert log
  try
  {
    $this->_db->beginTransaction();
    $fulldesc = 'CUST_ID:'.$cust_id;
    $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
    $this->_db->commit();
  }
  catch(Exception $e){
    $this->_db->rollBack();
    SGO_Helper_GeneralLog::technicalLog($e);
  }

  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
  $this->_redirect('/user/custlist');
}

$this->view->cust_id = $cust_id;
$this->view->change_type = $this->_changeType;
$this->view->status_type = $this->_masterglobalstatus;
$this->view->login_type = $this->_masterhasStatus;
$this->view->user_id = $user_id;
$this->view->current_login_user_id = $this->_userIdLogin;
$this->view->modulename = $this->_request->getModuleName();

    //insert log
try
{
  $this->_db->beginTransaction();
//    if(!$this->_request->isPost()){
//    Application_Helper_General::writeLog('CSLS','View Frontend User Detail [' .$user_id . ']');
//    }
  $this->_db->commit();
}
catch(Exception $e){
  $this->_db->rollBack();
}


if(!$this->_getParam('resetMsgSucces')){
  Application_Helper_General::writeLog('CSLS','View Frontend User Detail [' .$user_id . ']');
}
}
}
