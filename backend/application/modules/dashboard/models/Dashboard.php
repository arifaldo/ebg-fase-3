<?php

Class dashboard_Model_Dashboard extends Application_Main
{
    protected $_custData; 
    protected $_userData;  
    protected $_accData;
    protected $_limitData;

	// constructor
    public function initModel()
    { 
      	$this->_custData = array('CUST_ID'              => null,
					             'CUST_CIF'             => null,
					             'CUST_NAME'            => null,
					             'CUST_TYPE'            => null,
					             'CUST_ADDRESS'         => null,
					             'CUST_CITY'            => null,
      							 'CUST_ZIP'             => null,
					             'CUST_PROVINCE'        => null,
                                 'CUST_CONTACT'         => null,
					             'CUST_PHONE'           => null,
      							 'COUNTRY_CODE'         => null,
					             'CUST_EXT'             => null,
					             'CUST_FAX'             => null,
					             'CUST_EMAIL'           => null,
					             'CUST_WEBSITE'         => null,
      	                         'CUST_CHARGES_STATUS'  => null,
      	                         'CUST_MONTHLYFEE_STATUS'   => null,
      	                         'CUST_TOKEN_AUTH'   	=> null,
      	
					             // 'CUST_EMOBILE'         => null,
					             // 'CUST_ISEMOBILE'       => null,
                            );
                            
           $this->_userData = array('CUST_ID'        => null,
  	                         		'USER_ID'        => null,
		                     		'USER_NAME'      => null,
                             		'USER_EMAIL'     => null,
                             		'USER_PHONE'     => null,
                             		'USER_MOBILENO'  => null,
                             		'USER_DIVISION'  => null,
                             		'USER_STATUS'    => null,
  	                         		'FGROUP_ID'      => null,
  	                         		'USER_HASTOKEN'  => null
                            		);
                            		
         	$this->_accData = array('CUST_ID'        => null,
  	                         		'ACCT_NO'        => null,
		                     		'CCY_ID'         => null,
                             		'ACCT_NAME'      => null,
                             		'ACCT_GROUP'     => null,
                             		'ACCT_TYPE'      => null,
                             		'ACCT_LIMIT'     => null,
                             		'BANK_ID'        => null,
  	                         		'COUNTRY_CODE'   => null,
  	                         		'BANK_NAME'  	 => null,
  	                         		'BRANCH_CODE'  	 => null,
  	                         		'ACCT_DESC'  	 => null
                            		);
                            		
          $this->_limitData = array('CUST_ID'        	=> null,
  	                         		'DR_DAILY_LIMIT'   	=> null,
		                     		'CR_DAILY_LIMIT'    => null,
                             		'AMT_BLOCK_LIMIT'   => null,
                             		'CCY_ID'     		=> null
                            		);                  		
    }
	
    public function getAllCustomer()
    { 
       $select = $this->_db->select()
                           ->from(array('a' => 'M_CUSTOMER'),array('CUST_ID'))
                           ->query()->fetchAll();
	   return $select;
    }
    
    
    public function getCustomer($cust_id)
    { 
       $select = $this->_db->select()
                           ->from(array('a'=> 'M_CUSTOMER'))
                           ->joinleft(array('c' => 'M_COUNTRY'),'a.COUNTRY_CODE=c.COUNTRY_CODE',array('COUNTRY_NAME'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->query()->fetch();
	   return $select;
    }
    
    public function getCountry()
    { 
       $select = $this->_db->select()
                           ->from('M_COUNTRY')
                           ->query()->fetchAll();
	   return $select;
    }
    
	
	public function getCustomerAcct($cust_id)
    { 
        $select = $this->_db->select()
	 				         ->from(array('CA'=>'M_CUSTOMER_ACCT'),array('ACCT_NO','CCY_ID','ACCT_NAME','ACCT_DESC','ACCT_GROUP','ACCT_EMAIL','ORDER_NO','ACCT_STATUS'))
		  				     ->joinleft(array('g'=>'M_GROUPING'),'g.GROUP_ID=CA.GROUP_ID',array('GROUP_NAME'))
		  				     ->where('UPPER(CA.CUST_ID)='.$this->_db->quote((string)$cust_id))
		  				     //->where('ACCT_STATUS!=3')
		  				     ->order(array('ORDER_NO ASC'))
		  				     ->query()->fetchAll();
// 		  				     print_r($select->query())
		return $select;  				     
    }
    
    public function getDailyTransactionTrx()
    { 
       $select = $this->_db->select()
	 				         ->from(array('a'=>'T_TRANSACTION'),array())
	 				         ->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
	 				         													'b.PS_CREATED',
																				'TOTAL'=>'COUNT(a.`TRANSACTION_ID`)'))
																				
		  				     //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
		  				     ->where('MONTH(b.PS_CREATED) = MONTH(NOW())')
		  				     ->group('CAST(b.`PS_CREATED` AS DATE)')
		  				     //->where('USER_STATUS!=3');
		  				     ->query()->fetchAll();
		  				    
       return $select;  				     
    }
    
  
     
  
}




