<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
class requestspecialrate_IndexController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
		
		$transstatus = $this->_transferstatus;
		$transstatuscode = array_flip($transstatus['code']);
		$statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
		
		$paymenttype = $this->_paymenttype;

		$arrPayType  = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$this->view->arrPayType = $arrPayType;
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
		
		$tratypearr = array_combine(array_values($this->_transfertype['code']),array_values($this->_transfertype['desc']));

        $select2 = $this->_db->select()
					        ->from('T_REQ_SPCRATE');

        $select2->where('CUST_ID = ? ', $this->_custIdLogin);


        $fields = array(
            'PS_NUMBER'      	=> array('field' => 'PS_NUMBER',
                                        'label' => $this->language->_('Request Id '),
                                        'sortable' => true),
            'TREASURY_REFF_CODE'      	=> array('field' => 'TREASURY_REFF_CODE',
                                        'label' => $this->language->_('Treasury Reff Code'),
                                        'sortable' => true),
            'PS_REQUESTED'      	=> array('field' => 'PS_REQUESTED',
                                      'label' => $this->language->_('Date-Time Request'),
                                      'sortable' => true),
            'VALID_DATE'  	=> array('field' => 'VALID_DATE',
                                        'label' => $this->language->_('Valid Date '),
                                        'sortable' => true),
            'FROM_CURRENCY'     		=> array('field' => 'FROM_CURRENCY',
                                      'label' => $this->language->_('From Currency '),
                                      'sortable' => true),
            'TO_CURRENCY'  	=> array('field' => 'TO_CURRENCY',
                                        'label' => $this->language->_('To Currency '),
                                        'sortable' => true),
            'DEAL_RATE'  	=> array('field' => 'DEAL_RATE',
                                        'label' => $this->language->_('Deal Rate '),
                                        'sortable' => true),
            
            'REQUEST_BY'  	=> array('field' => 'REQUEST_BY',
                                      'label' => $this->language->_('Request By '),
                                      'sortable' => true),
            
            'PS_STATUS'  	=> array('field' => 'SUGGEST_STATUS',
                                      'label' => $this->language->_('Status'),
                                      'sortable' => true)
            
          );

          $this->view->fields = $fields;
          $this->paging($select2);
    }

}