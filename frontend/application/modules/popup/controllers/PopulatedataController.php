<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once ("Service/Account.php");
require_once 'General/Settings.php';

class popup_PopulatedataController extends Application_Main
{
	public function initController()
	{
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction()
	{
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$filterArr = array(	'next' => array('StringTrim','StripTags'),
							'acct_no' => array('StringTrim','StripTags')
		);

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$next = $zf_filter->getEscaped('next');
		$acct_no = $zf_filter->getEscaped('acct_no');
		$this->view->acct_no = $acct_no;

		if($next == TRUE)
		{
			if(empty($acct_no)){
				$this->view->message = 'Error : Account number cannot be left blank';
			}
			else{
				// die;
				// $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
//				$result		= $svcAccount->accountInquiry();
				// $result		= $svcAccount->accountInquiry('AB',TRUE);
				// $responseCode = $result['ResponseCode'];
				// $responseDesc = $result['ResponseDesc'];

				// $AccountName 	= $result['AccountName'];
				// $Cif 			= $result['Cif'];
				// $Birthday 		= $result['Birthday'];
				// $Phone 			= $result['Phone'];
				// $Email 			= $result['Email'];
				// $Address 		= $result['Address'];
				// $City 			= $result['City'];
				// $State 			= $result['State'];
				$responseCode = '00';
				$sessionNamespace 		= new Zend_Session_Namespace('populatedata');
				$paramSession 			= $sessionNamespace->paramSession;

				// $paramSession['acct_no']		= $acct_no;
				// $paramSession['AccountName']	= $AccountName;
				// $paramSession['Cif']			= $Cif;
				// $paramSession['Birthday']		= $Birthday;
				// $paramSession['Phone']			= $Phone;
				// $paramSession['Email']			= $Email;
				// $paramSession['Address']		= $Address;
				
				// $paramSession['City']			= $City;
				// $paramSession['State']			= $State;


				$paramSession['acct_no']		= '435374364';
				$paramSession['AccountName']	= 'RAMLAN GUSTIAN';
				// $paramSession['Cif']			= $Cif;
				// $paramSession['Birthday']		= $Birthday;
				$paramSession['Phone']			= '0812812891111';
				$paramSession['Email']			= 'benny@sgo.co.id';
				$paramSession['Address']		= 'Jl ABC no 13, RT 01/RW 04, Jatinegara';
				
				$paramSession['City']			= 'DKI Jakarta';
				$paramSession['State']			= 'Indonesia';


				$paramSession['SRE']		= '2423423432';
				$paramSession['SID']		= '273649575957';
				$paramSession['Zipcode']		= '139121';
				$paramSession['Country']		= 'Indonesia';

				$sessionNamespace->paramSession = $paramSession;

				if($responseCode == '00'){
					$this->_redirect('/popup/populatedata/confirm/index/');
				}
				else{
					$this->view->message = 'Error : '.$responseDesc;
				}
			}
		}

			  Application_Helper_General::writeLog('Populate Data','Populate Data');
		
	}


	public function confirmAction()
	{
		//$this->_getParam('back');
//		$customerUser = new customer_Model_Customer();
		$set = new Settings();
		$maxLengthUserid = 12;//$set->getSetting('max_length_userid');
		$minLengthUserid = 5;//$set->getSetting('min_length_userid');


		$sessionNamespace 	= new Zend_Session_Namespace('populatedata');
		$paramSession 		= $sessionNamespace->paramSession;
		$this->view->paramSession = $paramSession;
//		Zend_Debug::dump($paramSession);

		$acct_no		= $paramSession['acct_no'];
		$AccountNameOri 	= $paramSession['AccountName'];
//		$AccountName 	= str_replace(",", '', $AccountNameOri);
		$AccountName 	= preg_replace("/[^0-9a-zA-Z]/", "", $AccountNameOri);
		$this->accountName = $AccountName;
 		$Cif 			= $paramSession['Cif'];
		$BirthdayOri	= $paramSession['Birthday'];
		$Phone 			= $paramSession['Phone'];
		$Email 			= $paramSession['Email'];
		$Address 		= $paramSession['Address'];
		$City 			= $paramSession['City'];
		$State 			= $paramSession['State'];

		$Sid 			= $paramSession['Sid'];
		$Sre 			= $paramSession['Sre'];
		$Zipcode 			= $paramSession['Zipcode'];
		$Country 			= $paramSession['Country'];


		//die;
			Application_Helper_General::writeLog('Populate Data','Populate Data Confirm');
		

	}
}
