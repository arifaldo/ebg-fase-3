<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'Crypt/AESMYSQL.php';

class cmdreport_customerdetailController extends customer_Model_Customer
{

	public function initController()
	{
		$this->_helper->layout()->setLayout('popup');
		$this->view->button = true;
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newpopup');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
		$this->view->cust_id = $cust_id;

		$conf 				= Zend_Registry::get('config');
		$custmodelCode 		= $conf["cust"]["model"]["code"];
		$custmodelDesc 		= $conf["cust"]["model"]["desc"];
		$arrcustmodel 		= array_combine(array_values($custmodelCode), array_values($custmodelDesc));

		// echo '<pre>';
		// print_r($arrcustmodel);
		$customerstatus = $this->_customerstatus;
		$options = array_combine(array_values($customerstatus['code']), array_values($customerstatus['desc']));

		$setting = new Settings();
		$system_type1 = $setting->getSetting('system_type');

		// echo '<pre>';
		// print_r($system_type1);
		// echo "<BR><code>system_type1 = $system_type1</code><BR>";
		$this->view->system_type1 = $system_type1;

		$tokenType = $setting->getSetting('tokentype');

		$tokenTypeCode = array_flip($this->_tokenType['code']);
		$tokenTypeDesc = $this->_tokenType['desc'];

		$this->view->tokenTypeText = $tokenTypeDesc[$tokenTypeCode[$tokenType]];

		// ----------------------------------------------------COMPANY DETAIL----------------------------------------------------------------------------------
		{
			$listYesNo = array(0 => 'No', 1 => 'Yes');

			$caseCUST_MONTHLYFEE_STATUS = "(CASE CUST_MONTHLYFEE_STATUS ";
			foreach ($listYesNo as $key => $val) {
				$caseCUST_MONTHLYFEE_STATUS .= " WHEN " . $key . " THEN '" . $val . "'";
			}
			$caseCUST_MONTHLYFEE_STATUS .= " END)";

			$caseCUST_CHARGES_STATUS = "(CASE CUST_CHARGES_STATUS ";
			foreach ($listYesNo as $key => $val) {
				$caseCUST_CHARGES_STATUS .= " WHEN " . $key . " THEN '" . $val . "'";
			}
			$caseCUST_CHARGES_STATUS .= " END)";


			$caseCUST_SECURITIES = "(CASE CUST_SECURITIES ";
			foreach ($listYesNo as $key => $val) {
				$caseCUST_SECURITIES .= " WHEN " . $key . " THEN '" . $val . "'";
			}
			$caseCUST_SECURITIES .= " END)";


			$caseModel = "(CASE CUST_MODEL ";
			foreach ($arrcustmodel as $key => $val) {
				$caseModel .= " WHEN " . $key . " THEN '" . $val . "'";
			}
			$caseModel .= " END)";

			$caseStatus = "(CASE CUST_STATUS ";
			foreach ($options as $key => $val) {
				$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
			}
			$caseStatus .= " END)";

			$select	= $this->_db->select()
				->FROM(
					array('CUST' => 'M_CUSTOMER'),
					array(
						'CUST.CUST_ID',
						'CUST_MODEL',
						'CUST_STATUS' => $caseStatus,
						'CUST_SECURITIES' => $caseCUST_SECURITIES,
						'CUST_SECURITIES1' => 'CUST_SECURITIES',
						'CUST_NAME',
						'CUST_CIF',
						'CUST_TYPE',
						'CUST_WORKFIELD',
						'CUST_CITY',
						'CUST_ADDRESS',
						'CUST_ZIP',
						'CUST_PROVINCE',
						'CUST_PHONE',
						'CUST_EXT',
						'CUST_FAX',
						'CUST_EMAIL',
						'CUST_WEBSITE',
						'CUST_CONTACT',
						'CUST_MONTHLYFEE_STATUS' => $caseCUST_MONTHLYFEE_STATUS,
						'CUST_CHARGES_STATUS' => $caseCUST_CHARGES_STATUS,
						'CUST_LIMIT_IDR',
						'CUST_LIMIT_USD',
						'CUST_FINANCE',
						'CUST_CREATED',
						'CUST_CREATEDBY' => 'CUST_UPDATEDBY',
						'CUST_UPDATED',
						'CUST_UPDATEDBY',
						'CUST_SUGGESTED',
						'CUST_SUGGESTEDBY',
						'CUST_CODE',
						'CUST_REVIEW',
						'CUST_APPROVER',
						'CUST_SAME_USER',
						'CUST_APP_TOKEN',
						'CUST_MODEL_VAL' => 'CUST_MODEL',
						'CUST_NPWP',
						'CUST_VILLAGE',
						'CUST_DISTRICT',
						'BUSINESS_TYPE',
						'GO_PUBLIC',
						'DEBITUR_CODE',
						'COLLECTIBILITY_CODE',
						'CUST_CONTACT_PHONE',
						// 'CUST.*', 
						'GRUP_BUMN'
					)
				)
				->joinleft(array('COUNTRY' => 'M_COUNTRY'), 'CUST.COUNTRY_CODE = COUNTRY.COUNTRY_CODE', array('COUNTRY_NAME'))
				->joinleft(array('P' => 'M_PRELIMINARY'), 'P.CUST_ID = CUST.CUST_ID', array("*"))
				->joinleft(array('PM' => 'M_PRELIMINARY_MEMBER'), 'PM.CUST_ID = CUST.CUST_ID', array("*"))
				->joinleft(array('C' => 'M_CITYLIST'), 'C.CITY_CODE = CUST.CUST_CITY', array("*"))
				->WHERE('CUST.CUST_ID = ?', $cust_id);

			// echo "<BR><code>select = $select</code><BR>";   
			$resultcompanydetail = $this->_db->fetchRow($select);

			$custModelArr = [
				'1' => $this->language->_('Applicant'),
				'2' => $this->language->_('Insurance'),
				'3' => $this->language->_('Special Obligee')
			];


			$companyType = Application_Helper_Array::listArray($this->getCompanyType(), 'COMPANY_TYPE_CODE', 'COMPANY_TYPE_DESC');

			$resultcompanydetail['CUST_ID'] = $cust_id;

			$resultcompanydetail['CUST_TYPE'] = strtoupper($companyType[$resultcompanydetail['CUST_TYPE']]);

			$resultcompanydetail['CUST_MODEL'] = strtoupper($custModelArr[$resultcompanydetail['CUST_MODEL']]);

			$this->view->companydetail = $resultcompanydetail;

			$this->view->cust_rls_token  = 1;

			if ($resultcompanydetail['CUST_MODEL_VAL'] == '1') {
				$model = new linefacility_Model_Linefacility();
				$data = $model->getDataByCustId($cust_id);
				$this->view->data = $data;

				$segmentationArr = [
					1 => 'CORPORATE',
					2 => 'COMMERCIAL',
					3 => 'SME',
					4 => 'FINANCIAL INSTITUTION'
				];
				$this->view->segmentationArr = $segmentationArr;
			}
			if ($resultcompanydetail['CUST_MODEL_VAL'] == '3') {
				$model                              	= new specialobligee_Model_Specialobligee();
				$filterParam = array();
				$filterParam['fCompanyCode'] 			= $cust_id;
				$dataSp                               	= $model->getData($filterParam, true);

				$this->view->dataSp                   	= $dataSp['0'];

				$select = $this->_db->select()
					->from(
						array('A' => 'M_PRELIMINARY'),
						array('*')
					)
					->where('A.CUST_ID = ?', $cust_id);
				$datapre = $this->_db->fetchRow($select);

				$datapre['CUST_NAME'] = $resultcompanydetail['CUST_NAME'];
				$this->view->datapre = $datapre;


				$select = $this->_db->select()
					->from(
						array('A' => 'M_PRELIMINARY_MEMBER'),
						array('*')
					)
					->where('A.CUST_ID = ?', $cust_id);
				$dataMember = $this->_db->fetchAll($select);
				$this->view->dataMember = $dataMember;

				$getCustAcctById                    = $model->getCustAcctById($cust_id);
				$debitedAcctArr                     = [];
				foreach ($getCustAcctById as $row) {
					$debitedAcctArr[$row['ACCT_NO']]   = $row['ACCT_NO'] . ' (' . $row['CCY_ID'] . ')' . ' / ' . $row['ACCT_NAME'] . ' / ' . $row['ACCT_DESC'];
				}
				$this->view->debitedAcctArr         = $debitedAcctArr;
			}


			$this->view->businessEntityArr = Application_Helper_Array::listArray($this->getBusinessEntity(), 'BUSINESS_ENTITY_CODE', 'BUSINESS_ENTITY_DESC');
			$this->view->debiturCodeArr = Application_Helper_Array::listArray($this->getDebitur(), 'DEBITUR_CODE', 'DEBITUR_DESC');
			$this->view->collectCodeArr = Application_Helper_Array::listArray($this->getCreditQuality(), 'CODE', 'DESCRIPTION');

			$entityCode = $conf["bg"]["entity"]["code"];
			$entityDesc = $conf["bg"]["entity"]["desc"];
			$entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));
			$this->view->entityArr = $entityArr;

			$statusCode = $conf["bg"]["statusowner"]["code"];
			$statusDesc = $conf["bg"]["statusowner"]["desc"];
			$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));
			$this->view->statusArr = $statusArr;

			$select = $this->_db->select()
				->from(
					array('A' => 'M_POSITION'),
					array('*')
				);
			$position    = $this->_db->fetchAll($select);
			$positionArr = Application_Helper_Array::listArray($position, 'CODE', 'POSITION');
			$this->view->positionArr = $positionArr;

			$M_PRELIMINARY_MEMBER = $this->_db->select()
				->from(
					array('A' => 'M_PRELIMINARY_MEMBER'),
					array('*')
				)
				->where('A.CUST_ID = ?', $cust_id);
			$M_PRELIMINARY_MEMBER = $this->_db->fetchAll($M_PRELIMINARY_MEMBER);
			$this->view->M_PRELIMINARY_MEMBER = $M_PRELIMINARY_MEMBER;

			// echo '<pre>';
			// print_r($M_PRELIMINARY_MEMBER);
			//get adapter profile data
			$select = $this->_db->select()
				->from(array('H' => 'M_CUST_ADAPTER_PROFILE'), array('*'))
				->where('H.CUST_ID = ' . $this->_db->quote($cust_id));

			$profileData = $this->_db->fetchAll($select);

			foreach ($profileData as $key => $value) {
				if ($value['ADAPTER_PROFILE_ID'] !== 'defaults') {
					//get adapter profile data
					$selectAdapterName = $this->_db->select()
						->from(array('M_ADAPTER_PROFILE'), array('PROFILE_NAME'))
						->where('PROFILE_ID = ' . $this->_db->quote($value['ADAPTER_PROFILE_ID']));

					$profileName = $this->_db->fetchAll($selectAdapterName);

					$profileData[$key]['PROFILE_NAME'] = $profileName[0]['PROFILE_NAME'];
				} else {
					$profileData[$key]['PROFILE_NAME'] = 'default';
				}
			}

			foreach ($profileData as $key => $value) {
				if ($value['TRF_TYPE'] == 'payroll') {
					$this->view->payrollAdapter = $value['PROFILE_NAME'];
				}

				if ($value['TRF_TYPE'] == 'manytomany') {
					$this->view->manytomanyAdapter = $value['PROFILE_NAME'];
				}

				if ($value['TRF_TYPE'] == 'emoney') {
					$this->view->emoneyAdapter = $value['PROFILE_NAME'];
				}

				if ($value['TRF_TYPE'] == 'onetomany') {
					$this->view->onetomanyAdapter = $value['PROFILE_NAME'];
				}

				if ($value['TRF_TYPE'] == 'manytoone') {
					$this->view->manytooneAdapter = $value['PROFILE_NAME'];
				}
			}

			$selectMaker = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where("(FPRIVI_ID = 'CRSP' OR FPRIVI_ID = 'CRVA' OR FPRIVI_ID = 'CDFT') AND FUSER_ID LIKE ?", "%" . (string)$resultcompanydetail['CUST_ID'] . "%")
				->group('FUSER_ID');

			$userMaker = $this->_db->fetchAll($selectMaker);

			foreach ($userMaker as $row) {
				$userIdMaker = explode($resultcompanydetail['CUST_ID'], $row['FUSER_ID']);

				//get user name
				$selectMakerName = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", (string)$userIdMaker[1]);

				$userMakerName = $this->_db->fetchAll($selectMakerName);

				$MakerList .= $userMakerName[0]['USER_FULLNAME'] . '<br>';
			}

			$imgClass = '';
			if ($resultcompanydetail['CUST_FINANCE'] == '3') {
				$imgClass = "imgSepia";
			}

			// var_dump($MakerList);exit();
			$MakerNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/maker.PNG">
			<span class="hovertextcontent" style="left: -70px;">' . $MakerList . '</span></a>';

			$this->view->makerNameCircle = $MakerNameCircle;

			$selectReviewer = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where("FPRIVI_ID = 'RVPV' AND FUSER_ID LIKE ?", "%" . (string)$resultcompanydetail['CUST_ID'] . "%");

			$userReviewer = $this->_db->fetchAll($selectReviewer);

			foreach ($userReviewer as $row) {
				$userIdReviewer = explode($resultcompanydetail['CUST_ID'], $row['FUSER_ID']);

				//get user name
				$selectReviewerName = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", (string)$userIdReviewer[1]);

				$userReviewerName = $this->_db->fetchAll($selectReviewerName);

				$ReviewerList .= $userReviewerName[0]['USER_FULLNAME'] . '<br>';
			}

			// var_dump($ReviewerList);exit();
			$ReviewerNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/reviewer.png">
			<span class="hovertextcontent" style="left: -100px;">' . $ReviewerList . '</span></a>';

			$this->view->reviewerNameCircle = $ReviewerNameCircle;

			$selectApprover = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where("FPRIVI_ID = 'PAPV' AND FUSER_ID LIKE ?", "%" . (string)$resultcompanydetail['CUST_ID'] . "%");

			$userApprover = $this->_db->fetchAll($selectApprover);

			foreach ($userApprover as $row) {
				$userIdApprover = explode($resultcompanydetail['CUST_ID'], $row['FUSER_ID']);

				//get user name
				$selectApproverName = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", (string)$userIdApprover[1]);

				$userApproverName = $this->_db->fetchAll($selectApproverName);

				$ApproverList .= $userApproverName[0]['USER_FULLNAME'] . '<br>';
			}

			// var_dump($ApproverList);exit();
			$ApproverNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/approver.png">
			<span class="hovertextcontent" style="left: -100px;">' . $ApproverList . '</span></a>';

			$this->view->approverNameCircle = $ApproverNameCircle;

			$selectReleaser = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where("FPRIVI_ID = 'PRLP' AND FUSER_ID LIKE ?", "%" . (string)$resultcompanydetail['CUST_ID'] . "%");

			$userReleaser = $this->_db->fetchAll($selectReleaser);

			foreach ($userReleaser as $row) {
				$userIdReleaser = explode($resultcompanydetail['CUST_ID'], $row['FUSER_ID']);

				//get user name
				$selectReleaserName = $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", (string)$userIdReleaser[1]);

				$userReleaserName = $this->_db->fetchAll($selectReleaserName);

				$releaserList .= $userReleaserName[0]['USER_FULLNAME'] . '<br>';
			}

			// var_dump($releaserList);exit();
			$releaserNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/releaser.png">
			<span class="hovertextcontent" style="left: -100px;">' . $releaserList . '</span></a>';

			$this->view->releaserNameCircle = $releaserNameCircle;
		}
		// ----------------------------------------------------END COMPANY DETAIL----------------------------------------------------------------------------------

		// --------------------------------------------------------BANK ACCOUNT----------------------------------------------------------------------------------------
		{
			$bankacc = $this->getCustomerAcct($cust_id);
			$this->view->bankacc = $bankacc;
		}
		// --------------------------------------------------END BANK ACCOUNT-----------------------------------------------------------------------------------

		// ------------------------------------------------------USER ACCOUNT---------------------------------------------------------------------------------------
		{
			$useracc = $this->getUserAcct($cust_id);
			//Zend_Debug::dump($selectuseracc);Zend_Debug::dump($useracc);die;
			$this->view->useracc = $useracc;
		}
		// --------------------------------------------------END USER ACCOUNT-----------------------------------------------------------------------------------

		// ------------------------------------------------------------USER LIMIT---------------------------------------------------------------------------------------
		{
			$userlimit = $this->getUserLimit($cust_id);
			$this->view->userlimit = $userlimit;
		}
		// ---------------------------------------------------------END USER LIMIT-----------------------------------------------------------------------------------

		// ------------------------------------------------------USER DAILY LIMIT-----------------------------------------------------------------------------------
		{
			$userdailylimit = $this->getUserDailyLimit($cust_id);
			$this->view->userdailylimit = $userdailylimit;
		}
		// --------------------------------------------------END USER DAILY LIMIT--------------------------------------------------------------------------------	

		// ------------------------------------------------------USER BENEF BANK ACC-----------------------------------------------------------------------------------
		$this->view->benefBankAcc    = $this->getBenefBankAcc($cust_id);
		// --------------------------------------------------END USER BENEF BANK ACC--------------------------------------------------------------------------------	

		// ------------------------------------------------------USER BENEF USER-----------------------------------------------------------------------------------
		$this->view->benefUser  		  = $this->getBenefUser($cust_id);

		$this->view->benefAccount   	= $this->getBenefAccount($cust_id);

		$colomn = array('account_number', 'account_name', 'account_currency');
		$acctlist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'))
				->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
				->join(array('B' => 'M_BANK_TABLE'), 'A.BANK_CODE = B.BANK_CODE', array('bankname' => 'B.BANK_NAME'))
				//    ->group('A.APIKEY_ID')
				->where('UPPER(A.CUST_ID)=' . $this->_db->quote((string)$cust_id))
				->where('A.`FIELD` IN (?)', $colomn)
				->order('A.APIKEY_ID ASC')

		);

		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['bankname'] = $value['bankname'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
		}

		$datapers = array();

		$totalonline = 0;

		foreach ($account as $key => $request) {

			$datapers[$key - 1]['bank_name']  = $request['bankname'];
			$datapers[$key - 1]['bank_code']  = $request['BANK_CODE'];
			$datapers[$key - 1]['account_number']  = $request['account_number'];
			$datapers[$key - 1]['account_name']  = $request['account_name'];
			if (isset($request['account_alias'])) {
				$datapers[$key - 1]['account_alias']  = $request['account_alias'];
			} else {
				$datapers[$key - 1]['account_alias']  = '-';
			}
			$datapers[$key - 1]['account_currency']  = 'IDR';
			$datapers[$key - 1]['account_status'] = '1';
		}

		$total = count($datapers);

		$acctmanual = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'T_BALANCE'))
				->join(array('C' => 'M_BANK_TABLE'), 'A.BANK_CODE = C.BANK_CODE', array('bankname' => 'C.BANK_NAME'))
				->where('A.ACCT_STATUS = ?', '5')
				->where('UPPER(A.CUST_ID)=' . $this->_db->quote((string)$cust_id))
				->group("A.ACCT_NO")
				->group("A.BANK_CODE")
				->limit(100)
		);
		// echo $acctmanual;die;
		$totalmanual = 0;
		foreach ($acctmanual as $key => $value) {
			$datapersmanual[$total]['bank_code']  = $value['BANK_CODE'];
			$datapersmanual[$total]['bank_name']  = $value['bankname'];
			$datapersmanual[$total]['account_number']  = $value['ACCT_NO'];
			$datapersmanual[$total]['account_currency']  = $value['CCY_ID'];
			$datapersmanual[$total]['account_alias']  = $value['ACCT_ALIAS'];
			$datapersmanual[$total]['account_name']  = $value['ACCT_ALIAS'];
			$datapersmanual[$total]['account_status'] = $value['ACCT_STATUS'];
			$total++;
		}

		if (count($datapersmanual) > 0) {
			$datapersonal = array_merge($datapers, $datapersmanual);
		} else {
			$datapersonal = $datapers;
		}

		$this->view->datapersonal = $datapersonal;

		// --------------------------------------------------END BENEF USER--------------------------------------------------------------------------------	


		// --------------------------------------------------APPROVAL MATRIX--------------------------------------------------------------------------------	
		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists)) {
						$userlists .= $val['USER_ID'];
					} else {
						$userlists .= ', ' . $val['USER_ID'];
					}
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int)$groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;


		$select = $this->_db->select()
			->from(array('MAB' => 'M_APP_BOUNDARY'))
			->join(array('MABG' => 'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
			->where('MAB.CUST_ID = ?', (string)$cust_id)
			->group('BOUNDARY_ID');
		// echo $select;
		$result = $this->_db->fetchAll($select);

		$dataArr = array();
		// $alf = 'A';
		// $alf++;
		// ++$alf;
		// print_r((int)$alf);
		// $nogroup = sprintf("%02d", 1);
		// print_r($result);die;

		foreach ($result as $row) {
			list($grouptype, $groupname, $groupnum) = explode("_", $row['GROUP_USER_ID']);
			if ($grouptype == 'N') {
				$name = 'Group ' . trim($groupnum, '0');
			} else {
				$name = 'Special Group';
			}

			$selectUser = $this->_db->select()
				->from(array('M_APP_GROUP_USER'), array('USER_ID'))
				->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
				// echo $selectUser;die;
				->query()->fetchall();

			$userlist = '';

			$policythen = explode(' THEN ', $row['POLICY']);

			foreach ($policythen as $keythen => $valuethen) {
				$policy = explode(' AND ', $valuethen);

				foreach ($policy as $key => $value) {
					$replaceVal = str_replace('(', '', $value);
					$replaceVal = str_replace(')', '', $replaceVal);
					$policy[$key] = $replaceVal;
				}

				foreach ($policy as $key => $value) {
					if ($value == 'SG') {
						$group = 'S_' . $cust_id;
						$selectUser = $this->_db->select()
							->from(array('M_APP_GROUP_USER'), array('USER_ID'))
							->where('GROUP_USER_ID = ?', $group)
							// echo $selectUser;die;
							->query()->fetchall();
						foreach ($selectUser as $val) {
							if (empty($userlist)) {
								$userlist .= $val['USER_ID'];
							} else {
								$userlist .= ', ' . $val['USER_ID'];
							}
						}
					} else {
						$alphabet = array("A" => 1, "B" => 2, "C" => 3, "D" => 4, "E" => 5, "F" => 6, "G" => 7, "H" => 8, "I" => 9, "J" => 10, "K" => 11, "L" => 12, "M" => 13, "N" => 14, "O" => 15, "P" => 16, "Q" => 17, "R" => 18, "S" => 19, "T" => 20, "U" => 21, "V" => 22, "W" => 23, "X" => 24, "Y" => 25, "Z" => 26,);

						$policyor = explode(' OR ', $value);

						// print_r($policyor);die;
						foreach ($policyor as $numb => $valpol) {
							if ($valpol == 'SG') {
								$group = 'S_' . $cust_id;
								$selectUser = $this->_db->select()
									->from(array('M_APP_GROUP_USER'), array('USER_ID'))
									->where('GROUP_USER_ID = ?', $group)
									// echo $selectUser;die;
									->query()->fetchall();
								foreach ($selectUser as $val) {
									if (empty($userlist)) {
										$userlist .= $val['USER_ID'];
									} else {
										$userlist .= ', ' . $val['USER_ID'];
									}
								}
							} else {
								$nogroup = sprintf("%02d", $alphabet[$valpol]);
								// print_r($valpol);
								$group = 'N_' . $cust_id . '_' . $nogroup;
								$selectUser = $this->_db->select()
									->from(array('M_APP_GROUP_USER'), array('USER_ID'))
									->where('GROUP_USER_ID = ?', $group)
									// echo $selectUser;die;
									->query()->fetchall();
								// print_r($selectUser);  
								foreach ($selectUser as $val) {
									if (empty($userlist)) {
										$userlist .= $val['USER_ID'];
									} else {
										$userlist .= ', ' . $val['USER_ID'];
									}
								}
							}
						}
					}
				}
			}

			$arrTraType     = array_combine($this->_paymenttype["code"], $this->_paymenttype["desc"]);
			$arrTraType['19'] = 'Cash Pooling Same Bank';
			$arrTraType['20'] = 'Cash Pooling Same Bank';
			$arrTraType['23'] = 'Cash Pooling Other Bank';

			$userl = explode(', ', $userlist);
			$userlist = array_unique($userl);
			$userlistnew = '';

			foreach ($userlist as $ky  => $val) {
				//var_dump($ky);
				if ($ky == 0) {
					$userlistnew .= $val;
					//var_dump($userlistnew);
				} else {
					$userlistnew .= ', ' . $val;
				}
			}

			$changepolicy = explode(" ", $row['POLICY']);
			$data = array();
			foreach ($changepolicy as $keypolicy => $valpolicy) {
				if ($valpolicy == 'AND') {
					$valpolicy = '<span style="color: blue;">and</span>';
				} elseif ($valpolicy == 'OR') {
					$valpolicy = '<span style="color: blue;">or</span>';
				} elseif ($valpolicy == 'THEN') {
					$valpolicy = '<span style="color: blue;">then</span>';
				}

				$replacepolicy1 = str_replace('(', '<span style="color: blue;">(</span>', $valpolicy);
				$replacepolicy2 = str_replace(')', '<span style="color: blue;">)</span>', $replacepolicy1);

				$data[$keypolicy] = $this->language->_($replacepolicy2);
			}

			$valpolicy2 = implode(" ", $data);

			$dataArr[] = array(
				'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
				'CURRENCY' => $row['CCY_BOUNDARY'],
				'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN']) . " - " . Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
				'GROUP_NAME' => $valpolicy2,
				'USERS' => $userlistnew
			);
		}

		$this->view->dataBoundary = $dataArr;


		// -------------------------------------------------- END APPROVAL MATRIX--------------------------------------------------------------------------------

		// --------------------------------------------------------VERIFIER GROUP-----------------------------------------------------------------------------------
		{
			$verifiergroup = $this->getAppGroup($cust_id);
			$verGroup = '';
			foreach ($verifiergroup as $param) {
				if (isset($verGroup[$param['GROUP_USER_ID']])) {
					$verGroup[$param['GROUP_USER_ID']] = $verGroup[$param['GROUP_USER_ID']] . ' , ' . $param['USER_ID'];
				} else {
					$verGroup[$param['GROUP_USER_ID']] = $param['USER_ID'];
				}
			}
			$this->view->verifiergroup = $verGroup;
		}
		// ----------------------------------------------------END VERIFIER GROUP--------------------------------------------------------------------------------

		// --------------------------------------------VERIFIER GROUP BOUNDARY-----------------------------------------------------------------------------
		{
			$getAppBoundaryGroup =	$this->_db->SELECT()
				->FROM(array('MAB' => 'M_APP_BOUNDARY'), array('*'))
				->JOINLEFT(array('MABG' => 'M_APP_BOUNDARY_GROUP'), 'MABG.BOUNDARY_ID=MAB.BOUNDARY_ID', array('*'))
				->WHERE('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
				->WHERE('BOUNDARY_ISUSED = 1')
				->ORDER('BOUNDARY_MIN ASC')
				// ->GROUP('CCY_BOUNDARY')
				// ->GROUP('BOUNDARY_MIN')
				// ->GROUP('BOUNDARY_MAX')
				->QUERY()->FETCHALL();
			// echo '<pre>';
			// print_r($getAppBoundaryGroup);die;
			$verifiergroupbound = array();
			foreach ($getAppBoundaryGroup as $row) {
				$group_desc = null;
				//example : N_CUSTCHRIS_01 & S_CUSTCHRIS
				$explodeGroup = explode('_', $row['GROUP_USER_ID']);

				if ($explodeGroup[0] == 'N')      $group_desc = 'Group ' . (int)$explodeGroup[2];
				else if ($explodeGroup[0] == 'S') $group_desc = 'Special Group';

				$key = $row['BOUNDARY_ID'];
				$verifiergroupbound[$key]['GROUP_USER_ID'][] 	= $group_desc;
				$verifiergroupbound[$key]['BOUNDARY_MIN']    	= $row['BOUNDARY_MIN'];
				$verifiergroupbound[$key]['BOUNDARY_MAX']    = $row['BOUNDARY_MAX'];
				$verifiergroupbound[$key]['CCY_BOUNDARY']    	= $row['CCY_BOUNDARY'];
			}
			$this->view->verifiergroupbound = $verifiergroupbound;
		}
		$this->view->status_type = $this->_customerstatus;
		$pdf = $this->_getParam('pdf');

		// if(!empty($pdf))
		if ($pdf || $this->_request->getParam('print')) {
			$this->view->button = false;
			$MakerNameCirclePDF = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/maker.PNG">
			<span class="hovertextcontent" style="left: -70px;"></span></a>';

			$this->view->MakerNameCirclePDF = $MakerNameCirclePDF;
			$ReviewerNamePDF = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/reviewer.png">
			<span class="hovertextcontent" style="left: -100px;"></span></a>';
			$this->view->ReviewerNamePDF = $ReviewerNamePDF;
			$ApproverNameCirclePDF = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/approver.png">
			<span class="hovertextcontent" style="left: -100px;"></span></a>';
			$this->view->ApproverNameCirclePDF = $ApproverNameCirclePDF;
			$releaserNameCirclePDF = '<a class="hovertext" style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="/assets/themes/assets/newlayout/img/releaser.png">
			<span class="hovertextcontent" style="left: -100px;"></span></a>';
			$this->view->releaserNameCirclePDF = $releaserNameCirclePDF;
			if ($pdf) {
				$viewHtml = $this->view->render('customerdetail/index.phtml');

				$viewHtml 	= str_ireplace($MakerNameCircle, $MakerNameCirclePDF, $viewHtml);
				$viewHtml 	= str_ireplace($ReviewerName, $ReviewerNamePDF, $viewHtml);
				$viewHtml 	= str_ireplace($ApproverNameCircle, $ApproverNameCirclePDF, $viewHtml);
				$viewHtml 	= str_ireplace($releaserNameCircle, $releaserNameCirclePDF, $viewHtml);
				// echo "<BR><code>viewHtml = $viewHtml</code><BR>"; die;	
				$viewHtml 	= str_ireplace('table-fixed', '', $viewHtml);
				$outputHTML = "<tr><td>" . $viewHtml . "</td></tr>";
				// if(strlen($outputHTML) < 22000)
				{

					$this->_db->insert('T_BACTIVITY', array(
						'LOG_DATE'         => new Zend_Db_Expr('now()'),
						'USER_ID'           => $this->_userIdLogin,
						'USER_NAME'           => $this->_userNameLogin,
						'ACTION_DESC'       => 'RPCS',
						'ACTION_FULLDESC'   => 'Download PDF Customer Detail. Cust id : ( ' . $cust_id . ' )',
					));

					// Application_Helper_General::writeLog('RPCS','Download PDF Customer Detail. Cust id : ( '.$cust_id.' )');

					$this->_helper->download->pdfModif(null, null, null, 'Customer Detail ( ' . $cust_id . ' )', $outputHTML);
				}
				// else{
				// $this->view->button = true;
				// $this->view->errmsg ="PDF too large";
				// }

			} else if ($this->_request->getParam('print') == 1) {
				$this->_forward('printcustomerdetailall', 'index', 'widget');
			}
		} else {
			Application_Helper_General::writeLog('RPCS', 'View Customer Detail. Cust id : ( ' . $cust_id . ' )');
		}
		// -----------------------------------------END VERIFIER GROUP BOUNDARY-------------------------------------------------------------------------

	}
}
