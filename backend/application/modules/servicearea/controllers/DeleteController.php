<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class servicearea_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
		$model = new servicearea_Model_Servicearea();

	    $filters = array('area_id' => array('StringTrim', 'StripTags'));
							 
		$validators =  array(
					    'area_id' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_SERVICE_AREA', 'field' => 'AREA_ID')),
							       	  'messages' => array(
							   						  'Tidak boleh kosong',
							   					      'Area Layanan tidak ditemukan',
												) 
							             )
					        );
			
		if(array_key_exists('area_id',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
					//Application_Helper_General::writeLog('OBUD','Delete online bank table,Bank ID : '.$this->_getParam('bank_id'));
				    $this->_db->beginTransaction();
					$areaId  = $zf_filter_input->getEscaped('area_id');
				   	$model->deleteData($areaId);
				   	
				
					
					$this->_db->commit();
					$this->_helper->getHelper('FlashMessenger')->addMessage('S');
					$this->_helper->getHelper('FlashMessenger')->addMessage('Area Layanan berhasil dihapus');
					$this->_redirect('/'.$this->_request->getModuleName());
				}
				catch(Exception $e) 
				{
					//rollback changes
					$errorMsg = "Exception";
					$this->_db->rollBack();
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					$this->_redirect('/'.$this->_request->getModuleName());
				}
			}
			else
			{
				//$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						//Application_Helper_General::writeLog('OBDE','Update domestic bank');
					}					
				}

				$this->_redirect('/'.$this->_request->getModuleName());	
			}
		}
		else
		{
			$errorMsg = 'Area layanan tidak ditemukan';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			//Application_Helper_General::writeLog('OBDE','Delete domestic bank');
			
			$this->_redirect('/'.$this->_request->getModuleName());	
		}
	}
}
