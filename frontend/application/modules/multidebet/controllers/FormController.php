<?php

/*
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
*/

require_once 'Zend/Controller/Action.php';
require_once 'CMD/MultiPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';

class multidebet_FormController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{	
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$USE_CONFIRM_PAGE = true;
	
		$err = array();
		$arr = array();
		
		$filter 	= new Application_Filtering();
		$settings 	= new Application_Settings();
		$newsetting = new Settings();
		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		$isConfirmPage  = $this->_getParam('confirmPage'); 
		$submitBtn 		= $this->_getParam('submit');
		$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
		
		$TRA_AMOUNT_tot = $this->_getParam('TRA_AMOUNT_tot');
		$disabled 		= 'disabled = "disabled"';
		
		$AccArr 	  	= $CustomerUser->getAccounts();
		$ccyList  		= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
		$rowmax			= $newsetting->getSetting('max_row_multi_transaction');
		$PS_EFDATE 		= Application_Helper_General::convertDate($this->getCurrentDate());
		
		
		$this->view->ERROR_MSGTRX 		= $err;
		$this->view->arr 				= $arr;
		$this->view->ccyArr 			= $ccyList;
		$this->view->rowmax 			= $rowmax;
		$this->view->AccArr 			= $AccArr;
		$this->view->disabled 			= $disabled;
		$this->view->TRA_AMOUNT_tot 	= $TRA_AMOUNT_tot;
		
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		
		$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
		$this->view->ACBENEF_BANKNAME 	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->CURR_CODE 			= (isset($CURR_CODE))			? $CURR_CODE			: '';
		
		// $this->view->PS_EFDATE 			= (isset($PS_EFDATE))			? $PS_EFDATE			: $this->getCurrentDate();
		$this->view->PS_EFDATE 			= $PS_EFDATE;
		
		
		if (!empty($PS_NUMBER) && !$this->_request->isPost())
    	{
    		$paramList = array("WA" 			=> false,
							   "ACCOUNT_LIST" 	=> $this->_accountList,
							   "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							   "paymentType" 	=> $this->_paymenttype,
							   "transferType" 	=> $this->_transfertype,
							 );
    		$select   = $CustomerUser->getPayment($paramList, false);
			$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);
			
			$select->columns(array(	"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
									"acbenef_acct_name"		=> "T.BENEFICIARY_ACCOUNT_NAME"));
			
			$pslipData 	= $this->_db->fetchRow($select);
							 
			if (!empty($pslipData))	
			{
				$PS_SUBJECT 		= $pslipData['paySubj'];
				$PS_EFDATE  		= date("d/m/Y",strtotime($pslipData['efdate']));
				
				// $PS_EFDATE  		= Application_Helper_General::convertDate($pslipData['efdate'],$this->_dateViewFormat);  
				$ACBENEF  			= $pslipData['acbenef']; 
				$ACBENEF_CCY  		= $pslipData['acbenef_ccy'];
				$ACBENEF_BANKNAME	= $pslipData['acbenef_acct_name']; 
				$ACBENEF_ALIAS  	= $pslipData['acbenef_alias']; 
				$ACBENEF_EMAIL  	= $pslipData['acbenef_email']; 
				$CURR_CODE  		= $pslipData['acbenef_ccy']; 
				$TRA_AMOUNT_tot		= Application_Helper_General::displayMoney($pslipData['amount']);  
				
				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE']; 
				
				$db 	= Zend_Db_Table::getDefaultAdapter();
				$result = $db->fetchAll(
								$db	->select()
									->from(	array(	'a'=>'T_TRANSACTION' ),
											array(	'accsrc'	=>'SOURCE_ACCOUNT',
													'amount'	=>'TRA_AMOUNT',
													'tramessage'=>'TRA_MESSAGE',
													'trarefno'	=>'TRA_REFNO',
												)
										)
									->where('a.PS_NUMBER = ?',(string) $PS_NUMBER)
				);
				
				if(!empty($result)){
					foreach($result as $key => $val){
						
						$accsrcval 		= $result[$key]['accsrc'];
						$traamountval 	= Application_Helper_General::displayMoney($result[$key]['amount']);
						$tramessageval 	= $result[$key]['tramessage'];
						$trarefnoval 	= $result[$key]['trarefno'];
						
						$dataTx[$key]= array(	"ACCTSRC" 			=> $accsrcval,
												"TRA_AMOUNT" 		=> $traamountval,
												"TRA_MESSAGE" 		=> $tramessageval,
												"TRA_REFNO" 		=> $trarefnoval																	
											);	
						
					}
				}
				else{
					$dataTx = array();
				}
				
				$this->view->dataTx = $dataTx;				
				
				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
				elseif ($PS_TYPE != $this->_paymenttype["code"]["multidebet"])	// 0: PB multidebit & multicredit
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
			}
			else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
								
		
		}
		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP'))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}
		
		if($this->_request->isPost()){	
			
			$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), 			"PS_NUMBER");
			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PS_SUBJECT'), 	"PS_SUBJECT");
			$PS_EFDATE		= $filter->filter($this->_request->getParam('PS_EFDATE'), 	"PS_DATE");
			
			$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));			
			
			$ACBENEF 			= $filter->filter($this->_request->getParam('ACBENEF'), "ACCOUNT_NO");
			$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
			$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
			$CURR_CODE 			= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");
			
			if(!$ACBENEF){
			
				$error_msg[0] = 'Error: Beneficiary Account can not be left blank.';
				$this->view->error 		= true;
				$this->view->ERROR_MSG	= $this->displayError($error_msg);
			}
			else if(!$ACBENEF_ALIAS){
			
				$error_msg[0] = 'Error: Alias Name can not be left blank.';
				$this->view->error 		= true;
				$this->view->ERROR_MSG	= $this->displayError($error_msg);
			}
			elseif(!$CURR_CODE){
			
				$error_msg[0] = 'Error: Currency can not be left blank.';
				$this->view->error 		= true;
				$this->view->ERROR_MSG	= $this->displayError($error_msg);	
			}
			
			else if(!$PS_EFDATE){
			
				$error_msg[0] = 'Error: Payment Date can not be left blank.';
				$this->view->error 		= true;
				$this->view->ERROR_MSG	= $this->displayError($error_msg);
				
			}
			else{
				
				if(!$validateDateFormat->isValid($this->_getParam('PS_EFDATE'))){
								
					$error_msg[0] = 'Error: Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
					$this->view->error 		= true;
					$this->view->ERROR_MSG	= $this->displayError($error_msg);
				}
				
				else {
					
					$bb = 0;
					$tot_amount = 0;
					$r = 0;
					foreach($_POST as $value){
							
						$bb++;
						if($bb <= $rowmax){ 
																	
							$accsrc = 'ACCTSRC'.$bb;
							$traamount = 'TRA_AMOUNT'.$bb;
							$tramessage= 'TRA_MESSAGE'.$bb;
							$trarefno= 'TRA_REFNO'.$bb;
										
							$acc_src = $filter->filter($this->_request->getParam($accsrc), "ACCOUNT_NO");
							$tra_amount = $filter->filter($this->_request->getParam($traamount), "AMOUNT");
										
							$tra_message = $filter->filter($this->_request->getParam($tramessage), "TRA_MESSAGE");
							$tra_refno = $filter->filter($this->_request->getParam($trarefno), "TRA_REFNO");
																							
										
							$TRA_AMOUNT_num = Application_Helper_General::convertDisplayMoney($tra_amount);
																
							if( ($acc_src != "" && $tra_amount != "") || ($acc_src == "" && $tra_amount != "") || ($acc_src != "" && $tra_amount == "")){
								
								$arr[$r]	= array(	"TRANSFER_TYPE" 			=> "PB",
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"TRA_MESSAGE" 				=> $tra_message,
														"TRA_REFNO" 				=> $tra_refno,
														"ACBENEF" 					=> $ACBENEF,
														"ACBENEF_CCY" 				=> $CURR_CODE,	
														"ACCTSRC" 					=> $acc_src,	
							
														// for Beneficiary data, except (bene CCY and email), must be passed by reference
														"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,	
														"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
														"ACBENEF_EMAIL" 			=> ""
																		
													);	
								$r++;
											
							}
										
							$tot_amount = $tot_amount + $TRA_AMOUNT_num;
										
							unset($accsrc);
							unset($traamount);
							unset($tramessage);
							unset($trarefno);
										
						}		
						
						
					}
					unset ($bb);
							
					$countarr = count($arr);
							
					//$TRA_AMOUNT_tot = $tot_amount;
					
					$TRA_AMOUNT_tot = Application_Helper_General::displayMoney($tot_amount);
					$this->view->TRA_AMOUNT_tot 	= $TRA_AMOUNT_tot;
					
					//Zend_Debug::Dump($arr);die;
					//Zend_Debug::Dump($paramPayment);die;
					
					
					//$ACCTSRC1 = $filter->filter($this->_request->getParam('ACCTSRC1'), "ACCOUNT_NO");
					if(empty($arr)){

						$error_msg[0] = 'Error: Please insert at least 1 (One) Account Source.';
						$this->view->errorbene		= true;
						$this->view->ERROR_MSGbene	= $this->displayError($error_msg);
						
						
					}
					else{
						
						if ($submitBtn)
						{
							// MULTI DEBET BY FORM
							$paramPayment = array(					
									"CATEGORY"      	=> "MULTI DEBET",
									"FROM"       		=> "F",
									"PS_NUMBER"     	=> $PS_NUMBER,
									"PS_SUBJECT"    	=> $PS_SUBJECT,
									"PS_EFDATE"     	=> $PS_EFDATE,
									"_dateFormat"    	=> $this->_dateDisplayFormat,
									"_dateDBFormat"    	=> $this->_dateDBFormat,
									"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
									"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU '), // privi BLBU (Linkage Beneficiary User)
									"_createPB"     	=> $this->view->hasPrivilege('CRMO'),  // privi CRMO (Create Multi Debet Payment)
									"_createDOM"    	=> false,  // cannot create DOM trx
									"_createREM"    	=> false,  // cannot create REM trx
							);
							
							$paramSettingID = array('range_futuredate', 'auto_release_payment');								 
						
							$settings->setSettings(null, $paramSettingID);	// Zend_Registry => 'APPSETTINGS'
							$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
							
							//echo "<pre>";
							//print_r($paramPayment);
							//print_r($arr);
							//die;
							
							$resultVal	= $validate->checkCreate($paramPayment, $arr);
							
							$payment 		= $validate->getPaymentInfo();
							//print_r($payment);
							//die;
													
							if($validate->isError() === false)	// payment data is valid
							{
								if ($USE_CONFIRM_PAGE == true && $isConfirmPage == false)
								{
									$isConfirmPage 	= true;
									//$payment 		= $validate->getPaymentInfo();
									$validate->__destruct();
									unset($validate);
									
									// if (!empty($PS_NUMBER))
										// Application_Helper_General::writeLog('CRMO',"Confirmation Repair Payment Multi Debit Form. Payment Ref# = $PS_NUMBER");
									// else
										// Application_Helper_General::writeLog('CRMO','Confirmation Create Payment Multi Debit Form');
								}
								else{
									

									$validate->__destruct();
									unset($validate);
																	
									$param['PS_EFDATE'] 		= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
									$param['PS_SUBJECT'] 		= $PS_SUBJECT;
									$param['PS_CCY'] 			= $CURR_CODE;
									$param['PS_TYPE'] 			= $this->_paymenttype['code']['multidebet'];
									$param['_addBeneficiary'] 	= $this->view->hasPrivilege('BADA');
									$param['_beneLinkage']  	= $this->view->hasPrivilege('BLBU');
									$param['_priviCreate'] 		= 'CRMO';
										
									// Create Payment and Transactions after check
									for($x=0;$x<=$rowmax;$x++)
									{
											
										$y = $x+1;
										$data_temp = $this->_request->getParam('ACCTSRC'.$y);
										if($data_temp)
										{	
											$param['TRANSACTION_DATA'][$x]['TRANSFER_TYPE'] = "PB";
											$param['TRANSACTION_DATA'][$x]['SOURCE_ACCOUNT'] = $this->_request->getParam('ACCTSRC'.$y);
											
											$param['TRANSACTION_DATA'][$x]['BENEFICIARY_ACCOUNT'] = $ACBENEF;
											$param['TRANSACTION_DATA'][$x]['BENEFICIARY_ACCOUNT_CCY'] = $CURR_CODE;
											$param['TRANSACTION_DATA'][$x]['BENEFICIARY_ACCOUNT_NAME'] = $ACBENEF_BANKNAME;
											$param['TRANSACTION_DATA'][$x]['BENEFICIARY_ALIAS_NAME'] = $ACBENEF_ALIAS;
											
											$param['TRANSACTION_DATA'][$x]['TRA_AMOUNT'] = Application_Helper_General::convertDisplayMoney($this->_request->getParam('TRA_AMOUNT'.$y));
											$param['TRANSACTION_DATA'][$x]['TRA_MESSAGE'] = $this->_request->getParam('TRA_MESSAGE'.$y);
											$param['TRANSACTION_DATA'][$x]['TRA_REFNO'] = $this->_request->getParam('TRA_REFNO'.$y);
										}	
									}
									
									
										
									$MultiPayment = new MultiPayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
									if (!empty($PS_NUMBER))
									{	
										$MultiPayment->isRepair = true;
									}
										
									$result = $MultiPayment->createPayment($param);
										
									if ($result === true)
									{	
										$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
										$this->_redirect('/notification/success');
									
									}
									else
									{	
										$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
										$this->_redirect('/notification/success');
									}
									
									
									
									
								}
							}
							else{
								
								$errorMsg 		= $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								
								//var_dump($errorMsg);
								//var_dump($errorTrxMsg);
								
								$validate->__destruct();
								unset($validate);
										
								//$errMessage 	= (!empty($errorMsg))? $errorMsg: $errorTrxMsg[0];
								//Zend_Debug::Dump($errorMsg);die;
								
								if($errorMsg)
								{
									
									$confirmPage = false;
									$error_msg[0] = 'Error: '.$errorMsg;
									$this->view->error 		= true;
									$this->view->ERROR_MSG	= $this->displayError($error_msg);
									
								}
								if($errorTrxMsg){
								
									//Zend_Debug::dump($errorTrxMsg);die;
									$confirmPage = false;
									$ccystring = $paramTrxArr['ACBENEF_CCY'];
									//$errorTrxMsg 	= $validate->getErrorColMsg();;	// array
									if(!empty($errorTrxMsg["PB"])){
										
										$err = array();
										foreach($errorTrxMsg["PB"] as $ccy =>$row){
											
											// Zend_Debug::dump($val);die;
											foreach ($row as $key => $val){
											
												$rr[0] = $val;
												$err[$key] = $this->displayError($rr);
												
											}
											
										}
										
										//Zend_Debug::dump($errorTrxMsg);die;
										//$error_msgtrx[$t] = 'Error: '.$errorTrxMsg["PB"][$ccystring][$t];
										//$err_display[$t] 	= $this->displayError($err);
														
									}							
									
								}
								
								
								
							}
							
						}
						else { $isConfirmPage = false; }
					
					}
				
				}
				
			}
			
			$bb = 0;
			$r = 0;
			$allparam = $this->_request->getParams();
			
			foreach($allparam as $value){
						
				$bb++;
				if($bb <= $rowmax){ 
																
					$accsrc 	= 'ACCTSRC'.$bb;
					$traamount 	= 'TRA_AMOUNT'.$bb;
					$tramessage	= 'TRA_MESSAGE'.$bb;
					$trarefno	= 'TRA_REFNO'.$bb;
									
					$accsrcval 		= $filter->filter($this->_request->getParam($accsrc), "ACCOUNT_NO");
					$traamountval 	= $filter->filter($this->_request->getParam($traamount), "AMOUNT");
					$tramessageval 	= $filter->filter($this->_request->getParam($tramessage), "TRA_MESSAGE");
					$trarefnoval 	= $filter->filter($this->_request->getParam($trarefno), "TRA_REFNO");
									
					$TRA_AMOUNT_num = Application_Helper_General::convertDisplayMoney($tra_amount);
															
					if( ($accsrcval != "" && $traamountval != "") || ($accsrcval == "" && $traamountval != "") || ($accsrcval != "" && $traamountval == "")){
						
						$ACCTSRC_CCY   	= $payment["acctsrcArr"][$accsrcval]["CCY_ID"];
						$ACCTSRC_NAME   = $payment["acctsrcArr"][$accsrcval]["ACCT_NAME"];
						$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$accsrcval]["ACCT_ALIAS"];
						$ACCTSRC_TYPE   = $payment["acctsrcArr"][$accsrcval]["ACCT_TYPE"];
						$ACCTSRC_view 	= Application_Helper_General::viewAccount($accsrcval, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);
					
						
						$dataTx[$r]= array(		"ACCTSRCview" 		=> $ACCTSRC_view,
												"ACCTSRC" 			=> $accsrcval,
												"TRA_AMOUNT" 		=> $traamountval,
												"TRA_MESSAGE" 		=> $tramessageval,
												"TRA_REFNO" 		=> $trarefnoval																	
											);	
						$r++;				
					}
									
				}	
				
					
			}
			unset ($bb);
			
			//var_dump($payment);
			//----------------------------------------------------------------- 
			
			// $key = 0;
			// $allparam = $this->_request->getParams();
			// foreach($allparam as $value){
				// $key++;
				// if($key <= $rowmax){
					
					// $accscr = 'ACCTSRC'.$key;
					// $traamount  = 'TRA_AMOUNT'.$key;
					// $tramessage = 'TRA_MESSAGE'.$key;
					// $trarefno 	= 'TRA_REFNO'.$key;
										
					// $acct_srcvat = $filter->filter($this->_request->getParam($accscr), "ACCOUNT_NO");
					
					// $tra_amountval = $filter->filter($this->_request->getParam($traamount), "AMOUNT");
					// $tra_messageval = $filter->filter($this->_request->getParam($tramessage), "TRA_MESSAGE");
					// $tra_refnoval = $filter->filter($this->_request->getParam($trarefno), "TRA_REFNO");
							
					//if($acct_srcvat !=""){
					
					// $this->view->$accscr = $acct_srcvat;
					// $this->view->$traamount = $tra_amountval;
					// $this->view->$tramessage = $tra_messageval;
					// $this->view->$trarefno = $tra_refnoval;
					
					//}
					
					// unset($accscr);
					// unset($traamount);
					// unset($tramessage);
					// unset($trarefno);
							
						
				// }
				
			// }
			// $filter->__destruct();
			// unset($filter);
			// unset($key);
			
		}
		
		//echo "countarr = $countarr";
		
		//echo "<pre>";
		//print_r($dataTx);
		
		$this->view->dataTx 			= $dataTx;
		$this->view->lastkey 			= $countarr;
		$this->view->ERROR_MSGTRX 		= $err;
		$this->view->arr 				= $arr;
		$this->view->ccyArr 			= $ccyList;
		$this->view->rowmax 			= $rowmax;
		$this->view->AccArr 			= $AccArr;
		$this->view->disabled 			= $disabled;
		$this->view->TRA_AMOUNT_tot 	= $TRA_AMOUNT_tot;
		
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		
		$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
		$this->view->ACBENEF_BANKNAME 	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->CURR_CODE 			= (isset($CURR_CODE))			? $CURR_CODE			: '';
		
		$this->view->PS_EFDATE 			= $PS_EFDATE;
		// $this->view->PS_EFDATE 			= (isset($PS_EFDATE))			? $PS_EFDATE			: $this->getCurrentDate();
		$this->view->confirmPage		= $isConfirmPage;
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;
		
	}
}
