<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgclosingworkflow_ReleaseController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');
		$this->view->systemType = $system_type;

		$conf = Zend_Registry::get('config');
		$this->_bankName = $conf['app']['bankname'];
		$this->view->masterbankname = $this->_bankName;

		$this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

		$fields = array(
			'bgno'     => array(
				'field'    => 'BG_NUMBER',
				'label'    => $this->language->_('Nomor BG / Subjek'),
			),
			'obligee'     => array(
				'field'    => 'BG_SUBJECT',
				'label'    => $this->language->_('Obligee'),
			),
			'bank_branch'   => array(
				'field'    => 'BRANCH_NAME',
				'label'    => $this->language->_('Bank Cabang'),
			),
			'counter_type'   => array(
				'field'    => 'COUNTER_TYPE',
				'label'    => $this->language->_('Tipe Kontra'),
			),
			'bgamount'  => array(
				'field'    => 'BG_AMOUNT',
				'label'    => $this->language->_('Nominal BG')
			),
			'change_type' => array(
				'field' => 'CHANGE_TYPE',
				'label' => $this->language->_('Tipe Usulan'),
			),
		);

		$status = array('3');
		$selectbg = $this->_db->select()
			->from(
				array('A' => 'T_BANK_GUARANTEE'),
				array(
					'REG_NUMBER'                    => 'BG_REG_NUMBER',
					'BG_NUMBER'                     => 'BG_NUMBER',
					'SUBJECT'                       => 'BG_SUBJECT',
					'RECIPIENT_NAME'                => 'RECIPIENT_NAME',
					'TIME_PERIOD_START'             => 'TIME_PERIOD_START',
					'TIME_PERIOD_END'               => 'TIME_PERIOD_END',
					'COUNTER_WARRANTY_TYPE'         => 'COUNTER_WARRANTY_TYPE',
					'COUNTER_WARRANTY_ACCT_NO'      => 'COUNTER_WARRANTY_ACCT_NO',
					'COUNTER_WARRANTY_ACCT_NAME'    => 'COUNTER_WARRANTY_ACCT_NAME',
					'IS_AMENDMENT'                  => 'D.CHANGE_TYPE',
					'AMOUNT'                        => 'BG_AMOUNT',
					// 'FULLNAME'                      => 'T.USER_FULLNAME',
					'BRANCH_NAME'                   => 'C.BRANCH_NAME',
				)
			)
			// ->joinleft(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID')
			->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE')
			->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			// ->where('T.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('D.SUGGESTION_STATUS IN (?)', $status)
			->order('A.BG_CREATED DESC');

		// filter ----------------------------------------------------------
		$filterlist = array('Nomor BG' => 'BG_NUMBER', 'Subject' => 'BG_SUBJECT', 'Obligee' => 'OBLIGEE_NAME', 'Bank Cabang' => 'BRANCH_NAME', 'Tipe Kontra' => 'COUNTER_TYPE');
		$this->view->filterlist = $filterlist;

		$filterArr = array(
			'BG_NUMBER'  => array('StringTrim', 'StripTags'),
			'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
			'OBLIGEE_NAME'  => array('StringTrim', 'StripTags'),
			'BRANCH_NAME'   => array('StringTrim', 'StripTags'),
			'COUNTER_TYPE'   => array('StringTrim', 'StripTags')
		);

		$dataParam = array('BG_NUMBER', 'BG_SUBJECT', 'OBLIGEE_NAME', 'BRANCH_NAME', 'COUNTER_TYPE');
		$dataParamValue = array();

		foreach ($dataParam as $dtParam) {
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						if (empty($dataParamValue[$dtParam])) {
							$dataParamValue[$dtParam] = [];
						}
						array_push($dataParamValue[$dtParam], $dataval[$key]);
					}
				}
			}
		}

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',', $value);
				if (!empty($duparr)) {
					foreach ($duparr as $ss => $vs) {
						$wherecol[] = $key;
						$whereval[] = $vs[0];
					}
				} else {
					$wherecol[] = $key;
					$whereval[] = $value[0];
				}
			}
			$this->view->wherecol = $wherecol;
			$this->view->whereval = $whereval;
		}

		$options = array('allowEmpty' => true);
		$validators = array(
			'BG_NUMBER' => array(),
			'BG_SUBJECT' => array(),
			'OBLIGEE_NAME' => array(),
			'BRANCH_NAME' => array(),
			'COUNTER_TYPE' => array(),
		);

		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

		$fBgNumber = $zf_filter->getEscaped('BG_NUMBER');
		$fSubject = $zf_filter->getEscaped('BG_SUBJECT');
		$fObligateName = $zf_filter->getEscaped('OBLIGEE_NAME');
		$fBranchName = $zf_filter->getEscaped('BRANCH_NAME');
		$fCounterType = $zf_filter->getEscaped('COUNTER_TYPE');

		if ($fBgNumber) {
			$selectbg->where("A.BG_NUMBER LIKE " . $this->_db->quote('%' . $fBgNumber[0] . '%'));
		}
		if ($fSubject) {
			$selectbg->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $fSubject[0] . '%'));
		}
		if ($fObligateName) {
			$selectbg->where("A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $fObligateName[0] . '%'));
		}
		if ($fBranchName) {
			$selectbg->where("BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fBranchName[0]) . '%'));
		}
		if ($fCounterType) {
			$selectbg->where("A.COUNTER_WARRANTY_TYPE = ?", $fCounterType[0]);
		}
		// filter -----------------------------------------------------------------

		$selectbg = $this->_db->fetchAll($selectbg);

		$result = $selectbg;

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$sessionNamespace->token = $rand;
		$this->view->token = $sessionNamespace->token;

		foreach ($result as $key => $value) {
			$get_reg_number = $value["BG_NUMBER"];
			$AESMYSQL = new Crypt_AESMYSQL();
			$rand = $this->token;
			$encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
			$encpayreff = urlencode($encrypted_payreff);
			$result[$key]["BG_NUMBER_ENCRYPTED"] = $encpayreff;
		}

		$this->paging($result);

		$conf = Zend_Registry::get('config');
		$this->view->bankname = $conf['app']['bankname'];

		$config        = Zend_Registry::get('config');
		$BgType     = $config["bg"]["status"]["desc"];
		$BgCode     = $config["bg"]["status"]["code"];
		$arrStatus = array_combine(array_values($BgCode), array_values($BgType));
		$this->view->arrStatus = $arrStatus;

		$arrType = array(
			1 => 'Standart',
			2 => 'Custom'
		);

		$arrLang = array(
			1 => 'Indonesian',
			2 => 'English',
			3 => 'Bilingual'
		);

		$this->view->langArr = $arrLang;
		$this->view->formatArr = $arrType;
		$this->view->fields = $fields;

		$getct = Zend_Registry::get('config');
		$ctdesc = $getct["bgclosing"]["changetype"]["desc"];
		$ctcode = $getct["bgclosing"]["changetype"]["code"];
		$arr_type = array_combine(array_values($ctcode), array_values($ctdesc));
		$this->view->arr_type = $arr_type;

		Application_Helper_General::writeLog('RLBG', $this->language->_('Lihat Daftar Permintaan Rilis Pengajuan Penutupan Bank Garansi'));
	}

	public function getcountertypeAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$tblName = $this->_getParam('id');
		$optHtml = "<option value='' disabled>-- " . $this->language->_('Please Select') . " --</option>";
		$arrWarrantyType = array(
			'1' => 'FC',
			'2' => 'LF',
			'3' => 'INS'
		);
		foreach ($arrWarrantyType as $key => $row) {
			$tblName == $key ? $select = 'selected' : $select = '';
			$optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
		}
		echo $optHtml;
	}
}
