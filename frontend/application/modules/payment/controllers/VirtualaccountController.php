<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';

class payment_VirtualaccountController extends Application_Main
{
	public function initModel()
    {
		$this->model 					= new payment_Model_Payment();
		$this->param['SERVICE_TYPE'] 	= 16;
		$this->param['PROVIDER_TYPE'] 	= 1;
		$this->param['fetch'] 			= 'fetchAll';
		$this->param['USER_ID'] 		= $this->_userIdLogin;
		$this->CustomerUser 			= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    }

	public function indexAction()
	{
		//$this->_helper->layout()->setLayout('popup');
		$sessionNamespace 		= new Zend_Session_Namespace('phone');
		$paramSession 			= $sessionNamespace->paramSession;

		$this->view->radioCheck	= 1;

		$CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$paramac = array(
				'CCY_IN' => 'IDR'
		);
		$AccArr 	  			= $this->CustomerUser->getAccounts($paramac);
		$this->view->AccArr 	= $AccArr;
		$anyValue = '-- '.$this->language->_('Any Value'). ' --';
		$arr 						= $this->model->getProviderId(16,1);
		$providerArr 				= array(''=> $anyValue);
		$providerArr 				+= Application_Helper_Array::listArray($arr,'PROVIDER_ID','PROVIDER_NAME');

		$this->view->providerArr 	= $providerArr;

		$list 						= $this->model->cekListVA($this->param);
		$this->view->paymentList 	= $list;


		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();
		$this->view->ccyArr 			= $ccyList;

		//added token
		$userData = $this->_db->select()
		->from(array('M_USER'),array('USER_ID','TOKEN_TYPE','TOKEN_ID','USER_MOBILE_PHONE'))
		->where('USER_ID = ?', $this->_userIdLogin)
		->limit(1)
		;
		$userData = $this->_db->fetchRow($userData);

		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);

		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();
		$this->view->challengeCode = $challengeCode;
		$this->view->challengeCodeReq = $challengeCode;

		if($this->_getParam('submit1') == TRUE)
		{
			$msg = array();
			$param = array();
			$param['SERVICE_TYPE'] = $this->_getParam('SERVICE_TYPE');
			$param['PROVIDER_TYPE'] = 1; //1
			$param['operator']		= $arr[0]['PROVIDER_ID']; //$this->_getParam('operator');
			//$param['orderId']		= $this->_getParam('orderId');
			$param['ACCTSRC']		= $this->_getParam('ACCTSRC');
			$param['cek']			= $this->_getParam('cek');
			$param['radioCheck']	= $this->_getParam('radioCheck');
			$param['payment']		= $this->_getParam('payment');
			$param['namaperusahaan']	= $this->_getParam('namaperusahaan');
			$param['amount']	 	= Application_Helper_General::convertDisplayMoney($this->_getParam('amount'));
			$param['novirtual'] 	= $this->_getParam('novirtual');
			$this->view->novir 		= $param['novirtual'];

			$sendProvider = new SinglePayment(null,$this->_userIdLogin);
			$resProvider = $sendProvider->createPaymentPurchase($this->_request->getParams(), $msg);
			
			$sgo_product_code_data	= $this->model->getSgoProductCode($param['operator']);
			$sgo_product_code = $sgo_product_code_data['0']['SGO_PRODUCT_CODE'];

			$listProviderName = $this->model->getProviderName($param['operator'], 1);
			$providerName = $listProviderName['PROVIDER_NAME'];
			
			if($param['radioCheck']==1)
			{
				$param['orderId'] 			= $this->_getParam('orderId');
				$ACBENEF					= $param['orderId'];
				$this->view->orderId		= $param['orderId'];
			}
			else if($param['radioCheck']==2)
			{
				$param['orderId'] 			= $this->_getParam('novirtual');
				$ACBENEF					= $param['novirtual'];
				//$this->view->orderId		= $param['novirtual'];
			}


			$param['productCode'] = $sgo_product_code; //STCKAI
			$biller = new Service_Biller();

			//if($phone == TRUE)
			//die('masuk');
			if(count($msg) < 1)
			{
				
				//begin
				//validasi hard token page 1 -- begin
				$filter_acct 	= new Application_Filtering();
				$challengeCodeReq = $filter_acct->filter($this->_getParam('challengeCodeReq'), "challengeCodeReq");
				$responseCodeReq  = $filter_acct->filter($this->_getParam('responseCodeReq'), "responseCodeReq");

				// add hamdan, validasi VA
				$paramPayment = array(				"CATEGORY" 					=> "SINGLE PB",
													"FROM" 						=> "F",				// F: Form, I: Import
													"PS_NUMBER"					=> '',
													"PS_SUBJECT"				=> '',
													"PS_EFDATE"					=> date('d/m/Y'),
													"_dateFormat"				=> $this->_dateDisplayFormat,
													"_dateDBFormat"				=> $this->_dateDBFormat,
													"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),		// privi BADA (Add Beneficiary)
													"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),		// privi BLBU (Linkage Beneficiary User)
													"_createPB"					=> $this->view->hasPrivilege('CRSP'),		// privi CRSP (Create Single Payment)
													"_createDOM"				=> false,									// cannot create DOM trx
													"_createREM"				=> false,									// cannot create REM trx
												 );
				$paramTrxArr[0] 				= array("TRANSFER_TYPE" 		=> "PB",
													"TRA_AMOUNT" 				=> $param['amount'],
													"TRA_MESSAGE" 				=> '',
													"TRA_REFNO" 				=> '',
													"ACCTSRC" 					=> $param['ACCTSRC'],
													"ACBENEF" 					=> $ACBENEF,
													"ACBENEF_CCY" 				=> 'IDR',
													"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
													"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
													"ACBENEF_EMAIL" 			=> &$ACBENEF_EMAIL,
												 );
				//end VA
				//validasi hard token page 1 -- begin
				if($tokenType == '1'){ //sms token
					$resultToken = TRUE;
				}
				elseif($tokenType == '2'){ //hard token
					if(empty($responseCodeReq)){
						$errM = 'Error : Response Token cannot be left blank.';
						$this->view->ERROR_MSG_N = $errM;
					}
					else{
						$resHard = $HardToken->verifyHardToken($challengeCodeReq, $responseCodeReq);
						$resultToken = $resHard['ResponseCode'] == '0000';
						//set user lock token gagal
						if ($resHard['ResponseCode'] != '0000')
						{
							$CustUser = new CustomerUser($this->_userIdLogin);
							$tokenFailed = $CustUser->setFailedTokenMustLogout();
						}
					}
				}
				elseif($tokenType == '3'){ //mobile token
					$resultToken = TRUE;
				}
				else{$resultToken = TRUE;}
				//validasi hard token page 1 -- end

				//if($validate->isError() === false && $resultToken === true){

				if($resultToken == TRUE){
					
					$paramSettingID = array('range_futuredate', 'auto_release_payment');
					$settings->setSettings(null, $paramSettingID);	// Zend_Registry => 'APPSETTINGS'
					
					$validate   = new ValidatePaymentSingle($this->_userIdLogin);
					$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr);
					//echo $ACBENEF_BANKNAME."ggg"; die;
					if($validate->isError() == false){

							$amount = $param['amount'];
							
							$detailCustomer	= $this->model->getCustomerData($this->param);
							$validateACCTSRC = new ValidateAccountSource($param['ACCTSRC'], $this->_userIdLogin);

							$this->param['ACCT_NO'] = $param['ACCTSRC'];
							$charges = $this->getCharges($param['operator']);
							if($charges)
							{
								$fee = $charges[$detailCustomer['CCY_ID']]['CHARGES_AMT'];
							}
							else
							{
								$fee = 0;
							}
							if($amount  == ''){
								$amountGet = '0.00';
							}
							else{
								$amountGet = $amount;
							}

							$params['amount']	=  $param['amount'];
							/*
								TO DO GET AMOUNT FROM BILLER
							*/
							$total 				= $params['amount'] + $fee;
							$check 				= $validateACCTSRC->check($total);

							if ($check == true)

							{
								
								$this->param['ACCT_NO'] = $param['ACCTSRC'];
								$detailCustomer	= $this->model->getCustomerData($this->param);
								
								$paramsearch['PROVIDER_ID'] 		= $param['operator'];
								$paramsearch['fetch'] 				= 'fetchRow';
								$arr 								= $this->model->getProvider($paramsearch);
								if(isset($param['cek']))
								{
									$paramsearch['USER_ID'] 		= $this->_userIdLogin;
									$paramsearch['REF_NO'] 			= $param['orderId'];
									$result 						= $this->model->cekList($paramsearch);

									if(!$result)
									{
										$paramSession['cek']		= $param['cek'];
									}
								}

								$paramSession['operator']			= $param['operator'];
								$paramSession['operatorNama']		= $providerName;

								if($param['radioCheck']==1)
								{
									$paramSession['orderId']			= $param['orderId'];
								}
								else if($param['radioCheck']==2)
								{
									$paramSession['orderId']			= $param['novirtual'];
								}

								$charges = $this->getCharges($param['operator']);
								if($charges)
								{
									$fee = $charges[$detailCustomer['CCY_ID']]['CHARGES_AMT'];
								}
								else
								{
									$fee = 0;
								}
								
								$paramSession['operator']			= $param['operator'];
								$paramSession['ACCTSRC']			= $param['ACCTSRC'];
								$paramSession['ccy']				= $detailCustomer['CCY_ID'];
								$paramSession['ACCT_NAME']			= $customer_name_ori;
								$paramSession['ACCT_NAME_DATA']		= $detailCustomer['ACCT_NAME'];
								$paramSession['ACCT_ALIAS_NAME']	= $detailCustomer['ACCT_ALIAS_NAME'];
								$paramSession['ACCT_TYPE']			= $detailCustomer['ACCT_TYPE'];
								$paramSession['FREEZE_STATUS']		= $detailCustomer['FREEZE_STATUS'];
								$paramSession['MAXLIMIT']			= $detailCustomer['MAXLIMIT'];
								$paramSession['amount']				= $params['amount'];
								$paramSession['fee']				= $fee;
								$paramSession['total']				= $total;
								$paramSession['sgoProductCode']		= $param['productCode'];
								$paramSession['amount']				= $param['amount'];
								$paramSession['ACBENEF_BANKNAME']	= $ACBENEF_BANKNAME;

								$paramSession['BENEFICIARY_ACCOUNT']		= $paramSession['orderId'];
								$paramSession['BENEFICIARY_ACCOUNT_NAME']	= $ACBENEF_BANKNAME;
								$paramSession['BENEFICIARY_ALIAS_NAME']		= $ACBENEF_BANKNAME;
								$paramSession['BENEFICIARY_EMAIL']			= '';
								$paramSession['cek'] 				= $param['cek'];
								$sessionNamespace->paramSession = $paramSession;
								if ($res['ResponseCode'] != '00'){
									$this->_redirect('/payment/virtualaccount/next');
								}
								else{

									$this->view->operator				= $param['operator'];
									$this->view->ACCTSRC				= $param['ACCTSRC'];
									$this->view->cek					= $param['cek'];
									$this->view->radioCheck				= $param['radioCheck'];
									$this->view->amount					= $param['amount'];

									$this->view->ERROR_MSG_N = $res['ResponseDesc'] ;
								}
							}
							else
							{
								$errMsg 			= $validateACCTSRC->getErrorMsg();
								$error				= true;
							}
					}
					else{
							$errMessage 	= '';
							$errorMsg 		= (!empty($errorMsg)) ? $errorMsg : $validate->getErrorMsg();
							$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
							$validate->__destruct();
							unset($validate);
							if(!empty($errorMsg) || !empty($errorTrxMsg))//{
								$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));

							$this->view->error 		= true;
							$this->view->ERROR_MSG	= $errMessage;
							$this->view->amount		= $param['amount'];
					}
				}
				else{

						$this->view->xACCTSRC 				= (isset($errMsg))? $errMsg : null;

						$this->view->operator				= $param['operator'];
						$this->view->ACCTSRC				= $param['ACCTSRC'];
						$this->view->cek					= $param['cek'];
						$this->view->radioCheck				= $param['radioCheck'];
						$this->view->amount					= $param['amount'];
						//$this->view->ERROR_MSG_N = $errM;

						if(empty($responseCodeReq)){}else{
							$this->view->ERROR_MSG_N = 'Error : Invalid Token APPLI 2';
						}

				}
				//end
			}
			else
			{

				$errors 	= $msg;
				$error		= true;
			}

			if($error)
			{

				$this->view->error 					= $error;
				$this->view->operatorErr 			= (isset($errors['operator']))? $errors['operator'] : null;
				$this->view->orderIdErr 			= (isset($errors['orderId']))? $errors['orderId'] : null;
				$this->view->paymentErr 			= (isset($errors['novirtual']))? $errors['novirtual'] : null;
				$this->view->amountErr 				= (isset($errors['amount']))? $errors['amount'] : null;
				$this->view->ACCTSRCErr 			= (isset($errors['ACCTSRC']))? $errors['ACCTSRC'] : null;
				$this->view->xACCTSRC 				= (isset($errMsg))? $errMsg : null;

				$this->view->operator				= $param['operator'];
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				$this->view->cek					= $param['cek'];
				$this->view->radioCheck				= $param['radioCheck'];
				$this->view->amount					= $param['amount'];
				$this->view->novirtual				= $this->_getParam['novirtual'];
				$this->view->paymentList			= $list;

				if($param['radioCheck']==1)
				{
					//$this->view->orderId					= $param['orderId'];
				}
				else if($param['radioCheck']==2)
				{

					//$this->view->orderId					= $param['novirtual'];
				}

			}
		}
		else if($this->_getParam('submit') == true)
		{
			$this->cancel();
		}
		else
		{
			unset($_SESSION['virtualaccount']);
		}
	Application_Helper_General::writeLog('MPHO','Virtual Account');
	}

	public function nextAction()
	{
		$this->view->userId	= $this->_userIdLogin;
		$SinglePayment 		= new SinglePayment( null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('phone');
		$paramSession 		= $sessionNamespace->paramSession;

		$paramSession['berita'] = $this->_getParam('berita');
		$paramSession['berita2'] = $this->_getParam('berita2');

		// add hamdan
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO'=>$paramSession['ACCTSRC']));
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		// end hamdan

		//added token
		$userData = $this->_db->select()
		->from(array('M_USER'),array('USER_ID','TOKEN_TYPE','TOKEN_ID','USER_MOBILE_PHONE'))
		->where('USER_ID = ?', $this->_userIdLogin)
		->limit(1)
		;
		$userData = $this->_db->fetchRow($userData);
		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);

		//add hardtoken
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);


		if(!$paramSession)
		{
			$this->cancel();
		}

		if($this->_getParam('submit1') == TRUE)
		{

			$validator = new Payment(null,null,$this->_userIdLogin);
			$createLimit = $validator->createLimit($paramSession['amount'], $paramSession['ccy'], $paramSession['ACCTSRC'], $msg1);

			$PS_NUMBER 		= "P".$SinglePayment->generatePaymentID(1);
			$PS_EFDATE		= $this->model->date();

			$SinglePayment 	= new SinglePayment( $PS_NUMBER, $this->_userIdLogin);

			$setting 	= new Settings();
			$bank_code 	= $setting->getSetting('bank_code');

//			if(isset($paramSession['cek']))
//			{
//				$paramcek['cek'] = $paramSession['cek'];
//				$param +=$paramcek;
//			}
				$sessionNamespace->paramSession = $paramSession;
				$this->_redirect('/payment/virtualaccount/nextlast');
		}
		else if ($this->_getParam('submit') == true)
		{
			$this->_redirect('/payment/virtualaccount/index');
		}

		$this->view->paramSession		= $paramSession;
	}

	public function nextlastAction()
	{
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->view->userId	= $this->_userIdLogin;
		$SinglePayment 		= new SinglePayment( null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('phone');
		$paramSession 		= $sessionNamespace->paramSession;

		$PS_EFDATE 				= date('d/m/Y');
		$this->view->PS_EFDATE 	= (isset($PS_EFDATE))?$PS_EFDATE:'';

		//added token
		$userData = $this->_db->select()
		->from(array('M_USER'),array('USER_ID','TOKEN_TYPE','TOKEN_ID','USER_MOBILE_PHONE'))
		->where('USER_ID = ?', $this->_userIdLogin)
		->limit(1)
		;
		$userData = $this->_db->fetchRow($userData);
		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);

		// add hamdan
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO'=>$paramSession['ACCTSRC']));
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		// end hamdan

		$paramPayment = array(		"CATEGORY" 					=> "SINGLE PB",
									"FROM" 						=> "F",				// F: Form, I: Import
									"PS_NUMBER"					=> '',
									"PS_SUBJECT"				=> '',
									"PS_EFDATE"					=> date('d/m/Y'),
									"_dateFormat"				=> $this->_dateDisplayFormat,
									"_dateDBFormat"				=> $this->_dateDBFormat,
									"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),		// privi BADA (Add Beneficiary)
									"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),		// privi BLBU (Linkage Beneficiary User)
									"_createPB"					=> $this->view->hasPrivilege('CRSP'),		// privi CRSP (Create Single Payment)
									"_createDOM"				=> false,									// cannot create DOM trx
									"_createREM"				=> false,									// cannot create REM trx
								 );

		//add hardtoken
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);

		if(!$paramSession)
		{
			$this->cancel();
		}

		if($this->_getParam('submit1') == TRUE)
		{
			$validator = new Payment(null,null,$this->_userIdLogin);
			$createLimit = $validator->createLimit($paramSession['amount'], $paramSession['ccy'], $paramSession['ACCTSRC'], $msg1);
			$PS_NUMBER 		= $SinglePayment->generatePaymentID(1);
			//$filter 		= new Application_Filtering();
			//$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
			$PS_EFDATE		= $this->model->date();

			$SinglePay 	= new SinglePayment( $PS_NUMBER, $this->_userIdLogin);

			$setting 	= new Settings();
			$bank_code 	= $setting->getSetting('bank_code');

			$param = array();
			$param['PS_SUBJECT'] 					= '';
			$param['PS_CCY'] 						= $paramSession['ccy'];
			$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['EXPIRY_DATE'] 					= Application_Helper_General::convertDate($EXPIRY_DATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['TRA_AMOUNT'] 					= $paramSession['amount'];
			$param['TRA_MESSAGE'] 					= $paramSession['berita'];
			$param['TRA_REFNO'] 					= $paramSession['berita2'];
			$param['SOURCE_ACCOUNT'] 				= $paramSession['ACCTSRC'];
			$param['BENEFICIARY_ACCOUNT'] 			= $paramSession['BENEFICIARY_ACCOUNT'];
			$param['BENEFICIARY_ACCOUNT_CCY'] 		= $paramSession['ccy'];
			$param['BENEFICIARY_ACCOUNT_NAME'] 		= $paramSession['BENEFICIARY_ACCOUNT_NAME'];
			$param['BENEFICIARY_ALIAS_NAME'] 		= $paramSession['BENEFICIARY_ACCOUNT_NAME'];
			$param['BENEFICIARY_EMAIL'] 			= '';
			$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
			$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
			$param['_priviCreate'] 					= 'CRSP';
			$param['cek'] 							= $paramSession['cek'];
			$param['PS_TYPE'] 						= '0';
			$param['PROVIDER_ID'] 					= $paramSession['operator'];
			$param['VA'] 							= '6';

			//token validasi here added agung
			$filters = array('*' => array('HtmlEntities', 'StripTags', 'StringTrim'));
				$validators = array(
					'responseCode' => array(
						'notEmpty',
						'alnum',
						'presence' => 'required',
					)
				);

				$filter = new Zend_Filter_Input($filters, $validators, $this->_getAllParams(), array('breakChainOnFailure' => true));

			if($createLimit == TRUE){
					if ($filter->isValid()){
						$responseCode = $filter->getEscaped('responseCode');
						$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);
						if (isset($userMobilePhone) || !(empty($userMobilePhone))){

							$Settings = new Settings();
							$message = $Settings->getSetting('token_confirm_message');

							$trans = array("[Response_Code]" => $responseCode);
							$message = strtr($message, $trans);

							$token = new Service_Token(NULL,$this->_userIdLogin);
							$token->setMsisdn($userData['USER_MOBILE_PHONE']);
							$token->setMessage($message);

							if($tokenType == '1'){ //sms token
								$res = $token->confirm();
								$resultToken = $res['ResponseCode'] == '0000';
							}
							elseif($tokenType == '2'){ //hard token
								$resHard = $HardToken->validateOtp($responseCode);
								$resultToken = $resHard['ResponseCode'] == '0000';

								//set user lock token gagal
								if ($resHard['ResponseCode'] != '0000')
								{
									$CustUser = new CustomerUser($this->_userIdLogin);
									$tokenFailed = $CustUser->setFailedTokenMustLogout();
								}
							}
							elseif($tokenType == '3'){ //mobile token
							}

							if ($resultToken){
								//$return = $SinglePay->createVirtualAccount($param);
								$return = $SinglePay->createPaymentWithin($param);
								$sessionNamespace->psNumber 	= $return;
								$payReff = $PS_NUMBER = $return;

								if($paramSession['cek'] == 1)
								{
									$select	= $this->_db->select()
													->from	(array(	'TPL' => 'T_PAY_LIST'));
									$select->where("TPL.USER_BANK_CODE = ? ", $app);
									$select->where("TPL.USER_ID = ? ", $this->_userIdLogin);
									$select->where("TPL.REF_NO = ? ", $param['BENEFICIARY_ACCOUNT']);
									$select->where("TPL.PROVIDER_ID = ? ", $param['PROVIDER_ID']);
									$tpaylist = $this->_db->fetchAll($select);

									if(!$tpaylist)
									{
										$arrTlist = array(
															'USER_ID' 			=>$this->_userIdLogin,
															'USER_BANK_CODE' 	=>$app,
															'PROVIDER_ID' 		=>$param['PROVIDER_ID'],
															'REF_NO' 			=>$param['BENEFICIARY_ACCOUNT'],
															'ACCT_NAME' 		=>$param['BENEFICIARY_ACCOUNT_NAME'],
														);
										$this->_db->insert('T_PAY_LIST',$arrTlist);
									}
								}

								if ($return){
									$paramSession['PS_NUMBER'] 		= $payReff;
									$paramSession['date']			= $PS_EFDATE;
									$paramSession['paymentMessage']	= 'Payment Success';
									$sessionNamespace->paramSession = $paramSession;

									$this->_redirect('/payment/virtualaccount/confirm');
								}

							}
							else{
								$error_msg	 			= 'Invalid Token';
								$this->view->error 		= true;
								$this->view->ERROR_MSG	= $error_msg;
							}
						}
					}
					else{
						$error_msg	 			= 'Token cannot be left blank';
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $error_msg;
					}
			}
			else{
				$error_msg	 			= $msg1;
				$this->view->error 		= true;
				$this->view->ERROR_MSG	= $error_msg;
			}


		}
		else if ($this->_getParam('submit') == true)
		{
			$this->_redirect('/payment/virtualaccount/next');
		}

		$this->view->paramSession			= $paramSession;
	}

	public function confirmAction()
	{
		$pdf = $this->_getParam('pdf');
		$sessionNamespace 					= new Zend_Session_Namespace('phone');
		$this->view->ERROR_MSG_N			= $paramSession['paymentMessage'];
		$paramSession 						= $sessionNamespace->paramSession;

		if(!$paramSession)
		{
			$this->cancel();
		}

		if ($this->_getParam('submit') == TRUE)
		{
			$this->_redirect('/payment/virtualaccount/index');
		}

		$sessionNamespace->paramSession 	= $paramSession;
		$this->view->paramSession			= $paramSession;

		// add hamdan
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO'=>$paramSession['ACCTSRC']));
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		$this->view->PS_NUMBER = $paramSession['PS_NUMBER'];

		// end hamdan
			if($pdf)
			{
				$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($paramSession['PSNumber'], $paramSession);
				$this->_helper->download->pdf(null,null,null,'Payment Report Detail',$datapdf);
			}

	}

	public function cancel()
	{
		unset($_SESSION['virtualaccount']);
		$this->_redirect("/home/index");
	}
}
