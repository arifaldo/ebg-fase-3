<?php

require_once 'Zend/Controller/Action.php';

class dropbox_AddController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$maxFileSize = "1024";
		$fileExt = "csv,dbf,pdf";

		if ($this->_request->isPost()) {

			$attahmentDestination 	= UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			$params = $this->_request->getParams();


			$sourceFileName = $adapter->getFileName();

			if ($sourceFileName == null) {
				$sourceFileName = null;
				$fileType = null;
			} else {
				$sourceFileName = substr(basename($adapter->getFileName()), 0);
				if ($_FILES["document"]["type"]) {
					$adapter->setDestination($attahmentDestination);
					$maxFileSize 			= $this->getSetting('Fe_attachment_maxbyte');
					$fileType = $adapter->getMimeType();
					$size = $_FILES["document"]["size"];
				} else {
					$fileType = null;
					$size = null;
				}
			}
			$paramsName['sourceFileName'] 	= $sourceFileName;

			$helpTopic 	= $this->_getParam('helpTopic');
			if ($helpTopic == null)	$helpTopic = null;

			$paramsName['helpTopic']  	= $helpTopic;
			$description 				= $this->_getParam('description');
			$paramsName['description']  = $description;

			$filtersName = array(
				'sourceFileName'	=> array('StringTrim'),
				'description'		=> array('StringTrim'),
			);

			$validatorsName = array(
				'sourceFileName' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Error : File cannot be left blank')
					)
				),
				'description' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Error: Description cannot be left blank')
					)
				),
			);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);

			if ($sourceFileName == NULL) {

				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName'])) ? $errors['sourceFileName'] : null;
			} elseif ($description == NULL) {


				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->descriptionErr 	= (isset($errors['description'])) ? $errors['description'] : null;
			} else {

				if ($zf_filter_input_name->isValid()) {
					$fileTypeMessage = explode('/', $fileType);
					if (isset($fileTypeMessage[1])) {
						$fileType =  $fileTypeMessage[1];
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
						$extensionValidator->setMessage("Extension file must be *.csv /.dbf / .pdf");

						$size 			= number_format($size);
						$sizeTampil 	= $size / 1000;

						$adapter->setValidators(array($extensionValidator));

						if ($adapter->isValid()) {

							$date = date("dmy");
							$time = date("his");

							$newFileName = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . $sourceFileName;
							$adapter->addFilter('Rename', $newFileName);

							if ($adapter->receive()) {
								try {
									$this->_db->beginTransaction();
									$insertArr = array(
										'CUST_ID'				=> $params['company'],
										'FILE_UPLOADEDBY'			=> $this->_userIdLogin,
										'FILE_TYPE'				=> '1',
										'FILE_NAME' 			=> $sourceFileName,
										'FILE_DESCRIPTION'		=> $description,
										'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
										'FILE_DOWNLOADED'		=> '0',
										'FILE_DELETED'			=> '0',
										'FILE_DOWNLOADEDBY'		=> '',
										'FILE_DELETEDBY'		=> '',
										'FILE_ISEMAILED'		=> '0',
										'FILE_SYSNAME'			=> $newFileName,
									);

									$this->_db->insert('T_FILE_SUBMIT', $insertArr);
									$this->_db->commit();

									Application_Helper_General::writeLog('DELI', "Successfully upload " . $newFileName);

									$this->setbackURL('/dropbox');
									$this->_redirect('/notification/success');
								} catch (Exception $e) {
									$this->_db->rollBack();
									Application_Log_GeneralLog::technicalLog($e);
								}
							}
						} else {
							$this->view->error = true;
							$errors = array($adapter->getMessages());
							$this->view->errorMsg = $errors;
						}
					} else {
						$this->view->error 				= true;
						$errors 						= $zf_filter_input_name->getMessages();

						$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName'])) ? $errors['sourceFileName'] : null;
						$this->view->helpTopicErr 		= (isset($errors['helpTopic'])) ? $errors['helpTopic'] : null;
						$this->view->helpTopic 			= $params['helpTopic'];
						$this->view->description 			= $params['description'];
					}
				}
			}
		}

		$selectcust =  $this->_db->select()
			->from('M_CUSTOMER', array('CUST_ID', 'CUST_ID'));

		$cust = $this->_db->fetchAll($selectcust);
		$arrcust = Application_Helper_Array::listArray($cust, 'CUST_ID', 'CUST_ID');

		$this->view->arrcust = $arrcust;


		//$this->view->max_file_size = $maxFileSize;
		//$this->view->file_extension = $fileExt;

		//$disable_note_len 	 = (isset($params['description']))  ? strlen($params['description'])  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 100; //- $disable_note_len;
		$note_len 	 = 100 - $note_len;

		$this->view->disable_note_len	= $disable_note_len;
		$this->view->note_len			= $note_len;
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('HLAD', 'Viewing Add Dropbox');
		}
	}
}
