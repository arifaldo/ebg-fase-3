<?php

class Holiday_RepairController extends Application_Main {
	
	protected $_moduleDB  = 'HTB';
	
	public function indexAction()
	{
		$params = $this->_request->getParams();
		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;

  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
  					array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
					array('Db_RecordExists', array('table' => 'TEMP_HOLIDAY', 'field' => 'CHANGES_ID')),
					'messages' => array(
						$this->language->_('Cannot be empty'),
						$this->language->_('Invalid format'),
						$this->language->_('Changes ID not found'),
						$this->language->_('Changes ID not found')
					),
  				),
  			);
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
	  			$year = null;
	  			$changes_id = $zf_filter_input->changes_id;
				
				$year = $this->_db->fetchOne(
	  				$this->_db->select()
	  				->from('TEMP_HOLIDAY',array('HOLIDAY_YEAR'))
					->where('CHANGES_ID = ?', $changes_id)
	  			);

				$changes_reason = $this->_db->fetchOne(
																					$this->_db->select()
																					->from('T_GLOBAL_CHANGES',array('CHANGES_REASON'))
																					->where('CHANGES_ID = ?', $changes_id)
																				);
				
				$this->view->year = $year;
				$this->view->changes_reason = $changes_reason;
				
				if($this->_request->isPost() && $this->view->hasPrivilege('HDCR'))
				{
					$filters = array('year' => array('StringTrim','StripTags'));
					$validators = array('year' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Cannot be empty',
																'Invalid format',
														)));
					
					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
					if($zf_filter_input->isValid())
					{
						$this->view->isedited=0;
						if($this->_getParam("m")=='holidaysave')
						{	
							$this->_db->beginTransaction();
							$holiday_date_arr=explode('~|~',$this->_getParam("dates"));
							$holiday_desc_arr=explode('~|~',$this->_getParam("datesdesc"));
							$year = $this->_getParam("year");	

							if(is_array($holiday_date_arr))
							{
								if($holiday_date_arr[0] !=null)
								{

								  // insert ke T_GLOBAL_CHANGES
									try{
										$i=0;
										//DELETE DATA TEMP
										$where = array('CHANGES_ID = ?' => $changes_id);
										$this->_db->delete('TEMP_HOLIDAY',$where);
										//UPDATE STATUS GLOBAL CHANGES
										$this->updateGlobalChanges($changes_id,NULL);
										foreach($holiday_date_arr as $holidate) 
										{
											$data = array(	'CHANGES_ID' => $changes_id,
																'HOLIDAY_YEAR' => $year,
																'HOLIDAY_DATE' => $holidate,
																'HOLIDAY_DESCRIPTION' => $holiday_desc_arr[$i],
															);
											$this->_db->insert('TEMP_HOLIDAY',$data);
											$i++;
										}
										Application_Helper_General::writeLog('HDCR','Submitting Repair Holiday Setting');
										$this->_db->commit();
										$this->view->success = true;
										$this->view->isedited=1;
										$msg = $this->language->_('Suggestion Repaired');
										$this->view->report_msg = $msg;
									}
									catch(Exception $e) 
									{
										$this->_db->rollBack();
									}
								}
							}
						}
					}
					else
					{
						$this->view->isedited=1;
						$this->view->error = true;
						$docErr = $this->displayError($zf_filter_input->getMessages());
						$this->view->report_msg = $docErr;
					}
				}
				
				$sqlQueryHoliday=
				"SELECT *,
				DAY(HOLIDAY_DATE) AS day,
				MONTH(HOLIDAY_DATE) AS month,
				YEAR(HOLIDAY_DATE) AS year
				FROM TEMP_HOLIDAY WHERE HOLIDAY_YEAR= ?";
				$listHoliday = $this->_db->fetchAll($sqlQueryHoliday,$year);
				$this->view->listHoliday = $listHoliday;
			}
		}
		if(!$this->_request->isPost())
		Application_Helper_General::writeLog('HDCR','Viewing Repair Holiday Setting');
	}

}

?>
