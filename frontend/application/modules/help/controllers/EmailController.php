<?php

require_once 'Zend/Controller/Action.php';


class help_EmailController extends Application_Main {
     
        public function indexAction() 
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $email = $this->_getParam('email');
            if(!empty($email)){
                $emaillist = explode(',', $email);
            }else{
                $emaillist = array('yose@sgo.co.id');    
            }

            

            $config     = Zend_Registry::get('config');
                        $bankName    = $config['app']['bankname'];
                             $appName     = $config['app']['name'];
                             $appBankCode   = $config['app']['bankcode'];

                            $Settings = new Settings();
                            $templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
                            $templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
                            $templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
                            $templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
                            $templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
                            $templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
                            $templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
                            $templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
                            $templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
                            $templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
                            
                            $template = file_get_contents(LIBRARY_PATH.'/email template/emailreport.html');
                            $FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
                            $FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
                            // print_r($template);die;
                            $transFT   = array( '[[APP_NAME]]'          => $templateEmailMasterBankAppName,
                                                '[[BANK_NAME]]'         => $templateEmailMasterBankName,
                                                '[[APP_URL]]'           => $templateEmailMasterBankAppUrl                                                   
                                              );

                            $FOOTER_ID = strtr($FOOTER_ID, $transFT);
                            $FOOTER_EN = strtr($FOOTER_EN, $transFT);
                        
                        //if($string != ''){  
                          $translate = array( 
                            '[[DATA_CONTENT]]' => '',
                            '[[FOOTER_ID]]'     => $FOOTER_ID,
                            '[[FOOTER_EN]]'     => $FOOTER_EN,
                            '[[APP_NAME]]'      => $templateEmailMasterBankAppName,
                            '[[BANK_NAME]]'     => $templateEmailMasterBankName,
                            '[[APP_URL]]'       => $templateEmailMasterBankAppUrl,
                            '[[master_bank_name]]'  => $templateEmailMasterBankName,
                            '[[master_bank_fax]]'   => $templateEmailMasterBankFax,
                            '[[master_bank_address]]'   => $templateEmailMasterBankAddress,
                            '[[master_bank_telp]]'  => $templateEmailMasterBankTelp,    
                          );
                          
                          $mailContent = strtr($template,$translate);
                        $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                        $filename = 'Account Statement_xxxxxxx1142.pdf';
                        $updateAllArr = array('REPORT_FILESTATUS' => '1');
                            $updateallWhere['REPORT_FILESTATUS = ?'] = 0;
                            $this->_db->update('T_REPORT_GENERATOR',$updateAllArr,$updateallWhere);
                        //var_dump($emaillist);die;
                             if(!empty($emaillist)){
                                foreach ($emaillist as $key => $value) {
                               $status = Application_Helper_Email::sendEmailAttachementpath($value,'Report Statement',$mailContent,$filename,'Account Statement.pdf',$attahmentDestination);
                                    var_dump($status);
                                }
                            }
        }

         public function balanceAction() 
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

             $email = $this->_getParam('email');
            if(!empty($email)){
                $emaillist = explode(',', $email);
            }else{
                $emaillist = array('yose@sgo.co.id');    
            }

            $config     = Zend_Registry::get('config');
                        $bankName    = $config['app']['bankname'];
                             $appName     = $config['app']['name'];
                             $appBankCode   = $config['app']['bankcode'];

                            $Settings = new Settings();
                            $templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
                            $templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
                            $templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
                            $templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
                            $templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
                            $templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
                            $templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
                            $templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
                            $templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
                            $templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
                            
                            $template = file_get_contents(LIBRARY_PATH.'/email template/emailreport.html');
                            $FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
                            $FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
                            // print_r($template);die;
                            $transFT   = array( '[[APP_NAME]]'          => $templateEmailMasterBankAppName,
                                                '[[BANK_NAME]]'         => $templateEmailMasterBankName,
                                                '[[APP_URL]]'           => $templateEmailMasterBankAppUrl                                                   
                                              );

                            $FOOTER_ID = strtr($FOOTER_ID, $transFT);
                            $FOOTER_EN = strtr($FOOTER_EN, $transFT);
                        
                        //if($string != ''){  
                          $translate = array( 
                            '[[DATA_CONTENT]]' => '',
                            '[[FOOTER_ID]]'     => $FOOTER_ID,
                            '[[FOOTER_EN]]'     => $FOOTER_EN,
                            '[[APP_NAME]]'      => $templateEmailMasterBankAppName,
                            '[[BANK_NAME]]'     => $templateEmailMasterBankName,
                            '[[APP_URL]]'       => $templateEmailMasterBankAppUrl,
                            '[[master_bank_name]]'  => $templateEmailMasterBankName,
                            '[[master_bank_fax]]'   => $templateEmailMasterBankFax,
                            '[[master_bank_address]]'   => $templateEmailMasterBankAddress,
                            '[[master_bank_telp]]'  => $templateEmailMasterBankTelp,    
                          );
                          
                          $mailContent = strtr($template,$translate);
                        $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                        $filename = 'Account Balance_xxxxxxx1142.pdf';

                            $updateAllArr = array('REPORT_FILESTATUS' => '1');
                            $updateallWhere['REPORT_FILESTATUS = ?'] = 0;
                            $this->_db->update('T_REPORT_GENERATOR',$updateAllArr,$updateallWhere);

                        //var_dump($attahmentDestination);die;
                        if(!empty($emaillist)){
                            foreach ($emaillist as $key => $value) {
                                $status = Application_Helper_Email::sendEmailAttachementpath($value,'Report Balance Inquiry',$mailContent,$filename,'Balance Inquiry.pdf',$attahmentDestination);                 
                            var_dump($status);
                            }
                            
                        }
           
                
        }
        
}
