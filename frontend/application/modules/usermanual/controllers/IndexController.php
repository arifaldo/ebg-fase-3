<?php

require_once 'Zend/Controller/Action.php';


class usermanual_IndexController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$arr = null;
		$viewFilter = null;
		
		$fields = array	(
							'HelpTopic'  			=> array	(
																	'field' => 'HELP_TOPIC',
																	'label' => $this->language->_('Topic'),
																	'sortable' => true
																),
							'FileDescription'  					=> array	(
																		'field' => 'HELP_DESCRIPTION',
																		'label' => $this->language->_('File Description'),
																		'sortable' => true
																	),
							'Action'  							=> array	(
																	'field' => 'HELP_ID',
																	'label' => $this->language->_('Action'),
																	'sortable' => false
																	)
						);

		$filterlist = array('HELP_TOPIC','FILE_DESCRIPTION');
		
		$this->view->filterlist = $filterlist;

		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		
		$this->view->fields = $fields;
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'FILE_DESCRIPTION'   	=> array('StringTrim','StripTags','StringToUpper'),
							'HELP_TOPIC'   	=> array('StringTrim','StripTags','StringToUpper'),
		);

		$validator = array(	'filter'  => array(),
                       'FILE_DESCRIPTION'     => array(),
                       'HELP_TOPIC'   => array(),
                      );

		$dataParam = array('HELP_TOPIC','FILE_DESCRIPTION');
		$dataParamValue = array();
			
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

			// print_r($output);die;
			// print_r($this->_request->getParam('wherecol'));
			foreach ($dataParam as $no => $dtParam)
			{
			
				if(!empty($this->_request->getParam('wherecol'))){
					$dataval = $this->_request->getParam('whereval');
					// print_r($dataval);
					$order = 0;
						foreach ($this->_request->getParam('wherecol') as $key => $value) {
							if($dtParam==$value){
								$dataParamValue[$dtParam] = $dataval[$order];
							}
							$order++;
						}
					
				}
			}


		
		$zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
   
 		$filter 		= $this->_getParam('filter');
		
		$select = $this->_db->select()
							->from(	'T_HELP',array(	'HELP_ID', 
													'HELP_TOPIC',
													'UPLOADED_BY', 
													'UPLOAD_DATETIME', 
													'HELP_DESCRIPTION', 
													'HELP_FILENAME',
													'HELP_SYS_FILENAME'))
							->where("HELP_ISDELETED != 1");
		
		$HELP_ID   	= $this->_getParam('HELP_ID');
		
		if($HELP_ID)
		{
			$select2 = $this->_db->select()
							->from(	'T_HELP',array(	'HELP_ID', 
													'HELP_TOPIC',
													'UPLOADED_BY', 
													'UPLOAD_DATETIME', 
													'HELP_DESCRIPTION', 
													'HELP_FILENAME',
													'HELP_SYS_FILENAME'))
							->where("HELP_ISDELETED != 1");
			$select2->where('HELP_ID =?',$HELP_ID);
			$data = $this->_db->fetchRow($select2);
			$attahmentDestination = HELPER_PATH . '/document/help/';
			//echo($attahmentDestination);die;
			if (file_exists($attahmentDestination.$data['HELP_SYS_FILENAME']))
			{	$this->_helper->download->file($data['HELP_FILENAME'],$attahmentDestination.$data['HELP_SYS_FILENAME']);	
				Application_Helper_General::writeLog('HLDL','Download Help '.$data['HELP_FILENAME']);
			}
			else
			{	echo "<script type='text/javascript'>alert('File Not Found');</script>";	
				
			}
		}
		else{
			Application_Helper_General::writeLog('HLLS','Viewing Help List');
		}
		
		if($filter == TRUE)
		{
			$description   	= $zf_filter->getEscaped('FILE_DESCRIPTION');
			$HELP_TOPIC    = $zf_filter->getEscaped('HELP_TOPIC');
			
			if($description)
			{
				$select->where("UPPER(HELP_DESCRIPTION) LIKE ".$this->_db->quote('%'.$description.'%'));
				$this->view->description = $description;
			}
			
			if($HELP_TOPIC)
			{
				$select->where("UPPER(HELP_TOPIC) LIKE ".$this->_db->quote('%'.$HELP_TOPIC.'%'));
				$this->view->HELP_TOPIC = $HELP_TOPIC;
			}
		}
		
		$select->order($sortBy.' '.$sortDir);
		//$arr = $this->_db->fetchAll($select);
		$this->paging($select);
			
		if($arr != null)
		{
			$viewFilter = 1;
		}
		
		$this->view->viewFilter = $viewFilter;

		 if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
		//Application_Helper_General::writeLog('HLLS','Export CSV Waiting Release');
//		if(!$this->_request->isPost()){
//		Application_Helper_General::writeLog('HLLS','Viewing Help List');
//		}
	}	
}
