<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class localtransreport_IndexController extends Application_Main 
{
	protected $_moduleDB = 'RTF';
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$arrTraType 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		$arrTraStatus	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		
		unset ($arrTraStatus['2']);
		unset ($arrTraStatus['0']);
		
		$selectUser = $this->_db->select()
		->from(array('A' => 'M_BUSER'),array('*'))
		->joinleft(array('C' => 'M_BRANCH'), 'A.BUSER_BRANCH = C.ID',array('*'))
		->joinleft(array('B' => 'M_BGROUP'), 'A.BGROUP_ID = B.BGROUP_ID',array('*'));
		$selectUser -> where("A.BUSER_ID = ?",$this->_userIdLogin);
		$resultUser = $this->_db->fetchRow($selectUser);
		$this->view->branch_name = $resultUser['BRANCH_NAME'];
// 		print_r($resultUser);die;
		$listId = $this->_db->select()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID','CUST_NAME'))
						->order('CUST_ID ASC')
						->query()->fetchAll();
		
       	$list = array(""=>'-- '.$this->language->_('Please Select'). " --");
		$list += Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');
		
			
		$this->view->listCustId 		= $list;
		
		$fields = array	(
							'companycode'  		=> array(
															'field' => 'companycode',
															'label' => $this->language->_('Release Date'),
															'sortable' => true
														),
							'companyname'  		=> array(
															'field' => 'companyname',
															'label' => $this->language->_('Payment Ref').'#',
															'sortable' => true
														),
							'payref'  			=> array(
															'field' => 'payref',
															'label' => $this->language->_('Company Code'),
															'sortable' => true
														),
														
							'traceNo'  			=> array(
															'field' => 'traceNo',
															'label' => $this->language->_('Company Name'),
															'sortable' => true
														),
														
							'bankResponse'  			=> array(
															'field' => 'bankResponse',
															'label' => $this->language->_('Payment Type'),
															'sortable' => true
														),

						);
		
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');
		
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;
			
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array('branch_name'    	=> array('StripTags'),
	                       'bene_phone'  		=> array('StripTags','StringTrim'),
						   
						   'verify_code' 		=> array('StripTags','StringTrim')
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array(	"branch_name","bene_phone","verify_code");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		// 'name' => array(
		// 													'NotEmpty',
		// 													'messages' => array(
		// 																			$this->language->_('Can not be empty'),
		// 																		)
		// 												),	
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						
						
						'branch_name' 	=> array('NotEmpty',
															'messages' => array(
																					$this->language->_('Branch Can not be empty'),
																				)),	
						
						'bene_phone' 	=> array('NotEmpty',
															'messages' => array(
																					$this->language->_('Beneficiary Phone Can not be empty'),
																				)),	
						'verify_code' => array('NotEmpty',
															'messages' => array(
																					$this->language->_('Verification Code Can not be empty'),
																				)),	
                    	);
		
		$zf_filter  		= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$fBRANCHNAME 		= html_entity_decode($zf_filter->getEscaped('branch_name'));
		$fBENEPHONE 		= html_entity_decode($zf_filter->getEscaped('bene_phone'));
		$fVERIFYCODE 		= html_entity_decode($zf_filter->getEscaped('verify_code'));
		
		if($filter == NULL && $clearfilter != 1){
		    
			$this->view->result = false;
		}
		else{
		    $this->view->result = true;
			if($filter != NULL){
				$fUPDATEDFROM 		= html_entity_decode($zf_filter->getEscaped('UPDATEDFROM'));
				$fUPDATEDTO 		= html_entity_decode($zf_filter->getEscaped('UPDATEDTO'));
			}
			else if($clearfilter == 1){
				$fUPDATEDFROM 	= "";
				$fUPDATEDTO 	= "";
			}
		}
		
		/*
			CONCAT(sb_pslip.acctsrc,' (',sb_pslip.accsrc_ccy,')',' / ', sb_pslip.accsrc_bankname,' / ',sb_pslip.accsrc_alias) AS fullaccfrom,
			CONCAT(sb_transaction.tra_accttgt,' (', sb_transaction.tra_accttgtcurr,')',' / ', sb_transaction.tra_accttgtbankname) AS fullaccto,
				
			IF (tra_alreadycharged=1,'Y','N') AS tra_statuscharged,
		
		*/
		
		
		$caseTraStatus = "(CASE T.TRA_STATUS ";
  		foreach($arrTraStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";
		
		$select = $this->_db->select()
							->from(		array(	'T'=>'T_TRANSACTION'),array())
							->joinLeft(	array(	'P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',
										array(	
												'companycode'	=>'C.CUST_ID',
												'companyname'	=>'C.CUST_NAME',	
												'payref'		=>'P.PS_NUMBER',	
												'traceNo'		=>'T.TRACE_NO',	
												'bankResponse'	=>'T.BANK_RESPONSE',	
												'pssubject'		=>'P.PS_SUBJECT',	
												'createddate'	=>'P.PS_CREATED',	
												'updateddate'	=>'P.PS_UPDATED',	
												'transferdate'	=>'P.PS_EFDATE',
												'acctsrc'		=> new Zend_Db_Expr("
																	CONCAT( T.SOURCE_ACCOUNT , ' (' , T.SOURCE_ACCOUNT_CCY , ')  ' , T.SOURCE_ACCOUNT_NAME ) "),
												'beneacct'		=> new Zend_Db_Expr("
																	CONCAT( T.BENEFICIARY_ACCOUNT , ' (' , T.BENEFICIARY_ACCOUNT_CCY , ')  ' , T.BENEFICIARY_ACCOUNT_NAME ) "),
												'benebankname'	=>'T.BENEFICIARY_ALIAS_NAME',
												
												'ccy'			=>'P.PS_CCY',	
												'traamount'		=>'T.TRA_AMOUNT',
												'sourceccy'		=>'T.SOURCE_ACCOUNT_CCY',
												'transactionid'	=>'T.TRANSACTION_ID',	
												'addmessage'	=>'T.TRA_ADDITIONAL_MESSAGE',
												'tramessage'	=>'T.TRA_MESSAGE',	
												'trarefno'		=>'T.TRA_REFNO',	
												'bookrate'		=>'T.BOOK_RATE',
												'bookbuy'		=>'T.BOOK_RATE_BUY',
												'transferfee'	=>'T.TRANSFER_FEE',
												'fullamountfee'	=>'T.FULL_AMOUNT_FEE',
												'provisionfee'	=>'T.PROVISION_FEE',
												'T.EQUIVALENT_AMOUNT_IDR',													
												'T.EQUIVALENT_AMOUNT_USD',
												'T.BENEFICIARY_BANK_NAME',
												'transferstatus'=>$caseTraStatus,
												 'charged'		=>'T.TRANSFER_FEE_STATUS',
												'T.TRA_REMAIN',
												
												))
							->joinLeft(array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array());
							//->where("T.TRA_STATUS IN (?)",array('1','3','4','9','13'));
		$select->where('PS_STATUS IN ("8","5")');
		$select->where("UPPER(T.BENEFICIARY_BANK_NAME) = ".$this->_db->quote($resultUser['BRANCH_NAME']));
		
		if(!empty($fBENEPHONE) && !empty($fVERIFYCODE)){
		if($fBENEPHONE)	{ $select->where("UPPER(T.BENEFICIARY_MOBILE_PHONE_NUMBER) = ".$this->_db->quote($fBENEPHONE)); }
		if($fVERIFYCODE)	{ $select->where("UPPER(SUBSTR(`P`.`PS_NUMBER`, -6)) = ".$this->_db->quote($fVERIFYCODE)); }
		// if($fVERIFYCODE)	{ $select->where("UPPER(SUBSTR(`P`.`PS_NUMBER`, -6)) = ".$this->_db->quote($fVERIFYCODE)); }
		$this->view->bene_phone = $fBENEPHONE;
		$this->view->verify_code = $fVERIFYCODE;
		// $this->view->verify_code = $fVERIFYCODE;
		}else{
			$this->view->result = false;
		}
		// if($fPAYMENTREF) 	{ $select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPAYMENTREF.'%')); }
		// if($fTRANSACTIONID) { $select->where("UPPER(T.TRANSACTION_ID) LIKE ".$this->_db->quote('%'.$fTRANSACTIONID.'%')); }
		
		if($fUPDATEDFROM){
			$FormatDate 	= new Zend_Date($fUPDATEDFROM, $this->_dateDisplayFormat);
			$updatedfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) >= ?', $updatedfrom);
		}
		if($fUPDATEDTO){
			$FormatDate 	= new Zend_Date($fUPDATEDTO, $this->_dateDisplayFormat);
			$updatedto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) <= ?', $updatedto);
		}
		
		
		$select->order($sortBy.' '.$sortDir);
		// echo $select;die;
		$dataSQL 	= $this->_db->fetchAll($select);
		$dataCount 	= count($dataSQL); // utk validasi max row csv

		if($csv || $pdf || $this->_request->getParam('print')){	
		
			$header  = Application_Helper_Array::simpleArray($fields, "label"); 
			
			
			if(is_array($dataSQL) && count($dataSQL) > 0){
				foreach($dataSQL as $key => $val){
					
					if($val["ccy"] || $val["traamount"]){
						$dataSQL[$key]["ccy"] = $val["ccy"].' '.Application_Helper_General::displayMoney($val["traamount"]);
						// $dataSQL[$key]["ccy"] = "test";
					}
					if($val["beneacct"] || $val["benebankname"]){
						$dataSQL[$key]["beneacct"] = $val["beneacct"].' / '.$val["benebankname"];
					}

					if($val['bookrate']){
						$dataSQL[$key]['bookrate'] = 'IDR '.Application_Helper_General::displayMoney($val['bookrate']);
					}
					// if($val["charged"] == 1){
						
						// $dataSQL[$key]["charged"] = 'Y';
						
						// if($val["charged"] == '1') 		$dataSQL[$key]["charged"] = 'Y';
						// else if($val["charged"] == '0') 	$dataSQL[$key]["charged"] = 'N';
						// else if($val["charged"] == NULL)	$dataSQL[$key]["charged"] = '-';
					
					// }
					// else if($val["charged"] == '0') 	$dataSQL[$key]["charged"] = 'N';
					// else if($val["charged"] == NULL)	$dataSQL[$key]["charged"] = '-';
					
					unset($dataSQL[$key]["traamount"]);	
					unset($dataSQL[$key]["benebankname"]);	
					
				}
			
			}
			else  $dataSQL = array();

			
			if($csv)
			{			
				// echo "<pre>";
				// print_r($dataSQL);
				// die;
				
				$this->_helper->download->csv($header,$dataSQL,null,'Transaction Report');  
				Application_Helper_General::writeLog('RPTX','Download CSV transaction report');
			}
			elseif($pdf)
			{
				$this->_helper->download->pdf($header,$dataSQL,null,'Transaction Report');  
				Application_Helper_General::writeLog('RPTX','Download PDF transaction report');
			}
			else if($this->_request->getParam('print') == 1){
				$data = $this->_db->fetchAll($select);
				if(is_array($data) && count($data) > 0){
					foreach($data as $key => $val){
						
						if($val["ccy"] || $val["traamount"]){
							$data[$key]["traamount"] = $val["ccy"].' '.Application_Helper_General::displayMoney($val["traamount"]);
						}
						if($val['bookrate']){
							$data[$key]['bookrate'] = 'IDR '.Application_Helper_General::displayMoney($val['bookrate']);
						}
					}
				}

				unset($fields['paymenttype']);
				unset($fields['transfertype']);
				unset($fields['transferstatus']);
				unset($fields['charged']);
				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Transaction Report', 'data_header' => $fields));
			}
		}	
		else
		{		
			//print_r($dataSQL);die;
			$this->paging($dataSQL);
			
			$stringParam = array(
									'COMPANYCODE'	=>$fCOMPANYCODE,
									'COMPANYNAME'	=>$fCOMPANYNAME,
									'PAYMENTREF'	=>$fPAYMENTREF,
									'TRANSACTIONID'	=>$fTRANSACTIONID,
									
									'TRANSFERFROM'	=>$fTRANSFERFROM,
									'TRANSFERTO'	=>$fTRANSFERTO,
									
									'CREATEDFROM'	=>$fCREATEDFROM,
									'CREATEDTO'		=>$fCREATEDTO,
									
									'UPDATEDFROM'	=>$fUPDATEDFROM,
									'UPDATEDTO'		=>$fUPDATEDTO,
									
									'ACCTSRC'		=>$fACCTSRC,
									'BENEACCT'		=>$fBENEACCT,
									
									'PAYTYPE'		=>$fPAYTYPE,
									'TRANSFERTYPE'	=>$fTRANSFERTYPE,
									'TRANSSTATUS'	=>$fTRANSSTATUS,
									'CHARGEDSTATUS'	=>$fCHARGEDSTATUS,
									'clearfilter'	=> $clearfilter,
									'filter'		=> $filter,
								
								);
			
			$this->view->COMPANYCODE 	= $fCOMPANYCODE;
			$this->view->COMPANYNAME 	= $fCOMPANYNAME;
			$this->view->PAYMENTREF 	= $fPAYMENTREF;
			$this->view->TRANSACTIONID 	= $fTRANSACTIONID;
			
			$this->view->TRANSFERFROM 	= $fTRANSFERFROM;
			$this->view->TRANSFERTO 	= $fTRANSFERTO;
			
			$this->view->CREATEDFROM 	= $fCREATEDFROM;
			$this->view->CREATEDTO 		= $fCREATEDTO;
			
			$this->view->UPDATEDFROM 	= $fUPDATEDFROM;
			$this->view->UPDATEDTO 		= $fUPDATEDTO;
			
			$this->view->ACCTSRC 		= $fACCTSRC;
			$this->view->BENEACCT 		= $fBENEACCT;
			
			$this->view->PAYTYPE 		= $fPAYTYPE;
			$this->view->TRANSFERTYPE 	= $fTRANSFERTYPE;
			$this->view->TRANSSTATUS 	= $fTRANSSTATUS;
			$this->view->CHARGEDSTATUS 	= $fCHARGEDSTATUS;
			
			$this->view->fields = $fields;
			$this->view->filter = $filter;
			
			$this->view->arrPayType 		= $optpaytypeRaw;
			$this->view->arrTraType 		= $arrTraTypeRaw;
			$this->view->arrTraStatus 		= $arrTraStatus;
			$this->view->arrChargedStatus 	= $arrChargedStatus;		
			$this->view->listCustId 		= $list;
			$this->view->dataCount 			= $dataCount;
			
			Application_Helper_General::writeLog('RPTX','View Transaction Report');
			
		}		
		
	}
}
