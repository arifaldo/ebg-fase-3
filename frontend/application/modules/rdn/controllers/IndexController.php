<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';

class rdn_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
    	
    	
    	$this->view->acctpage = '1';
		$process 	= $this->_getParam('process');
		$submitBtn 	= ($this->_request->isPost() && $process == "submit")? true: false;
		
		//tambahn pentest
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;
		
		
		
			
		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');


		if($this->_request->isPost() )
		{
			if($this->_getParam('page3')=='1'){
				$sessionNamespace 	= new Zend_Session_Namespace('populatedata');
				$paramSession 		= $sessionNamespace->paramSession;

				print_r($this->_request->getParams());die;
				// print_r($paramSession);
				$paramrdn = array(
					'CUST_ID'	=> $this->_custIdLogin,
					'ACCT_NO' => $paramSession['acct_no'],
					'INVESTOR_NAME' => $paramSession['AccountName'],
					'SID' => $paramSession['SID'],
					'SRE' => $paramSession['SRE'],
					'USER_ADDRESS' => $paramSession['Address'],
					'USER_PROVINCE' => $paramSession['City'],
					'USER_CITY' => $paramSession['City'],
					'USER_ZIP' => $paramSession['Zipcode'],
					'USER_COUNTRY' => $paramSession['State'],
					'USER_MOBILE_PHONE' => $paramSession['Phone'],
					'USER_EMAIL' => $paramSession['Email'],

				);
				// print_r($paramrdn);die;
				try 
				{
					
					//-------- insert --------------
					$this->_db->beginTransaction();
					
					$this->_db->insert('M_RDN_ACCT',$paramrdn);
					
					$stock['0']['STOCK_CODE'] = 'WIKA';
					$stock['0']['STOCK_NAME'] = 'WIJAYA KARYA';
					$stock['0']['LOTS'] = '1000';
					$stock['0']['SHARES'] = '200';
					$stock['1']['STOCK_CODE'] = 'WIKA1';
					$stock['1']['STOCK_NAME'] = 'WIJAYA KARYA1';
					$stock['1']['LOTS'] = '2000';
					$stock['1']['SHARES'] = '200';
					foreach ($stock as $key => $value) {

						// if
						# code...
					}

					// $this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('RBAD','Add Stock. Stock Code : ['.$zf_filter_input->stock_code.'], Stock Name : ['.$zf_filter_input->stock_name.']');
					$this->view->success = true;
					$this->view->report_msg = array();
					
					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e) 
				{
					print_r($e);die;
					//rollback changes
					$this->_db->rollBack();
					
					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}


				foreach ($this->_getParam('stock') as $key => $value) {
					$paramSession['stockdata'][$key] = $value;
				}
				
				$this->view->acctpage = '1';
				$this->view->stockpage = '1';
				$this->view->confirmPage = '1';
				$this->view->paramSession = $paramSession;
				// print_r($paramSession);die;
			}else if($this->_getParam('page2')=='1'){
				$sessionNamespace 	= new Zend_Session_Namespace('populatedata');
				$paramSession 		= $sessionNamespace->paramSession;
				foreach ($this->_getParam('stock') as $key => $value) {
					$paramSession['stockdata'][$key] = $value;
				}
				// print_r($this->_request->getParams());die;
				$this->view->acctpage = '1';
				$this->view->stockpage = '1';
				$this->view->confirmPage = '1';
				$this->view->paramSession = $paramSession;
				// print_r($paramSession);die;
			}else if($this->_getParam('page1')=='1'){
			$sessionNamespace 		= new Zend_Session_Namespace('populatedata');
			$paramSession 			= $sessionNamespace->paramSession;
			$paramSession['ACTSRC'] 		= $this->_getParam('ACTSRC');
			$paramSession['ACCT_NAME'] 		= $this->_getParam('ACCT_NAME');
			$paramSession['SID'] 			= $this->_getParam('SID');
			$paramSession['SRE'] 			= $this->_getParam('SRE');
			$paramSession['ADDRESS'] 		= $this->_getParam('ADDRESS');
			$paramSession['STATE'] 			= $this->_getParam('STATE');
			$paramSession['CITY'] 			= $this->_getParam('CITY');
			$paramSession['ZIPCODE'] 		= $this->_getParam('ZIPCODE');
			$paramSession['COUNTRY'] 		= $this->_getParam('COUNTRY');
			$paramSession['PHONE'] 			= $this->_getParam('PHONE');
			$paramSession['EMAIL'] 			= $this->_getParam('EMAIL');
			$this->view->acctpage = '0';
			$this->view->stockpage = '1';

			$stock['0']['STOCK_CODE'] = 'WIKA';
			$stock['0']['STOCK_NAME'] = 'WIJAYA KARYA';
			$stock['0']['LOTS'] = '1000';
			$stock['0']['SHARES'] = '200';
			$stock['1']['STOCK_CODE'] = 'WIKA1';
			$stock['1']['STOCK_NAME'] = 'WIJAYA KARYA1';
			$stock['1']['LOTS'] = '2000';
			$stock['1']['SHARES'] = '200';

			$this->view->stockdata = $stock;
			}


			
		}
		$stock['0']['STOCK_CODE'] = 'WIKA';
			$stock['0']['STOCK_NAME'] = 'WIJAYA KARYA';
			$stock['0']['LOTS'] = '1000';
			$stock['0']['SHARES'] = '200';
			$stock['1']['STOCK_CODE'] = 'WIKA1';
			$stock['1']['STOCK_NAME'] = 'WIJAYA KARYA1';
			$stock['1']['LOTS'] = '2000';
			$stock['1']['SHARES'] = '200';

			$this->view->stockdata = $stock;
		// print_r($paramSession);die;
		$this->view->paramSession = $paramSession;

		

		
		
	}

	
}
