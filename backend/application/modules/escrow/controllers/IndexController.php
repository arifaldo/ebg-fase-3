<?php

class escrow_IndexController extends Application_Main
{
	public function indexAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->msg = $msg;
			}	
		}
		
		$getCcy = $this->getCcy();
		$getCcy = Application_Helper_Array::listArray($getCcy,'CCY_NUM','CCY_ID');
		$this->view->CCYData = $getCcy;
		// MASTER DATA
		$data = $this->_db->SELECT()
								->FROM(array('A' => 'M_MASTER'),array('*'))
								->WHERE('M_KEY = ? ','ESCROW')
								->WHERE('M_STATUS <> 3 ') // SASYA VERISSA BOENTORO klo add new account p acc itu sdh trdftr jadi tggl update
								->ORDER('M_KEY1 ASC')
				 				->QUERY()->FETCHALL();
		// TEMP DATA
		$tempData = $this->_db->SELECT()
								->FROM(array('A' => 'TEMP_MASTER'),array('*'))
								->WHERE('M_KEY = ? ','ESCROW')
								->ORDER('M_KEY1 ASC')
				 				->QUERY()->FETCHALL();
		$tempData = Application_Helper_Array::listArray($tempData,'M_KEY1','M_KEY2');

		$fields = array(
						'Currency'     => array('field' => 'M_KEY1',
											      	'label' => 'Currency',
											      	'sortable' => true),
						'Account_Number'      	=> array('field' => 'M_KEY2',
											      'label' => 'Account Number',
											      'sortable' => true),
						'Account Name'        => array('field' => 'M_KEY3',
											      'label' => 'Account Name',
											      'sortable' => true),
						'Action'   => array('field' => '',
											      'label' => 'Action',
											      'sortable' => false)
				      );
				      
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','M_KEY1');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		Application_Helper_General::writeLog('ECLS','View Escrow Account');

		$this->view->fields = $fields;
		$this->view->data = $data;
		$this->view->tempData = $tempData;
	}
		
	public function deleteAction()
	{
		$acct = $this->_getParam('acct','');
		$getCcy = $this->getCcy();
		$getCcy = Application_Helper_Array::listArray($getCcy,'CCY_ID','CCY_ID');

		$filters = array(
										'acct'        => array('StripTags','StringTrim'),
										'ccy'         => array('StripTags','StringTrim','StringToUpper'),
									);

		$validators =  array(
										'acct'       => array('NotEmpty',
																 'Digits',
																 array('Db_RecordExists',array('table'=>'M_MASTER','field'=>'M_KEY2','exclude'=>'M_STATUS!=3')),
																 array('Db_NoRecordExists',array('table'=>'TEMP_MASTER','field'=>'M_KEY2')),
																 'messages' => array('Can not be empty',
																					 'Account Number must be numeric',
																					 'Account Number ['.$acct.'] has not been registered',
																					 'Account Number ['.$acct.'] already suggested! Please Approve or Reject previous suggestion first!',
																					)
																),
									   'ccy'         => array('NotEmpty',
																 array('Db_RecordExists',array('table'=>'M_MASTER','field'=>'M_KEY1','exclude'=>'M_STATUS!=3')),
																 array('Db_NoRecordExists',array('table'=>'TEMP_MASTER','field'=>'M_KEY1')),
																  'messages' => array(
																					 'Can not be empty',
																					 'Escrow Account for this currency has not been registered.',
																					 'Escrow Account for this currency already suggested! Please Approve or Reject previous suggestion first!',
																					)
																),
											);

		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		if($zf_filter_input->isValid())
		{
			$params = $this->_db->FETCHROW(
																	$this->_db->SELECT()
																						->FROM(array('A' => 'M_MASTER'),array('*'))
																						->WHERE('M_KEY = ? ','ESCROW')
																						->WHERE('M_KEY1 = ? ',$zf_filter_input->ccy)
																						->WHERE('M_KEY2 = ? ',$zf_filter_input->acct)
																);
			$params['M_STATUS']	=	3;
			$params['M_SUGGESTEDBY']	=	$this->_userIdLogin;
			$params['M_SUGGESTED']		=	new Zend_Db_Expr('now()');
			$curr = (isset($getCcy[$zf_filter_input->ccy])) ? $getCcy[$zf_filter_input->ccy] : $zf_filter_input->ccy;
			$info = 'Delete Escrow Account : '.$zf_filter_input->acct.' , Currency '.$curr;

			unset($params['M_ISEDITED']);
			$this->_db->beginTransaction();
			try{
				$change_id = $this->suggestionWaitingApproval('Escrow Account',$info,$this->_changeType['code']['delete'],null,'M_MASTER','TEMP_MASTER',$zf_filter_input->acct,$params['M_KEY3']);
				$params['CHANGES_ID']	=	$change_id;
				$model = new escrow_Model_Escrow();
				$insert = $model->insertTemp($params);
				Application_Helper_General::writeLog('ECDL',$info);
				$this->_db->commit();
				$this->setbackURL('/'.$this->view->modulename);
				$this->_redirect('/notification/submited/index');
			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
				$this->_redirect('/notification/submited/index/err/1');
			}
		}
		else
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage('Invalid Action');
			$this->_redirect($this->_helper->url->url(array('module'=>$this->view->modulename,'controller'=>'index','action'=>'index')));
		}
		// ["type"] => string(6) "escrow"
		// ["ccy"] => string(3) "016"
		// ["acct"] => string(10) "1234567890"
	}
}
?>
