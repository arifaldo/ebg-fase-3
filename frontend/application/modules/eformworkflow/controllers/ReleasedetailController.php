<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';

require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Helper/Encryption.php';

class eformworkflow_releasedetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');
		$this->view->systemType = $system_type;

		$stamp_fee = $settings->getSetting('stamp_fee');
		$this->view->stamp_fee = $stamp_fee;
		$adm_fee = $settings->getSetting('adm_fee');
		$this->view->adm_fee = $adm_fee;

		$conf = Zend_Registry::get('config');

		$counterWarrantyType = $conf["bgcg"]["type"]["desc"];
		$counterWarrantyCode = $conf["bgcg"]["type"]["code"];

		$counterWarranty = array_combine(array_values($counterWarrantyCode), array_values($counterWarrantyType));

		$bgStatusType = $conf["bg"]["status"]['desc'];
		$bgStatusCode = $conf["bg"]["status"]["code"];

		$bgStatus = array_combine(array_values($bgStatusCode), array_values($bgStatusType));

		$Settings = new Settings();
		$claim_period = $Settings->getSettingNew('max_claim_period');
		$this->view->BG_CLAIM_PERIOD = $claim_period;

		$getOtpTrx = $this->_db->select()
			->from("M_SETTING")
			->where("SETTING_ID = ?", "otp_exp_trx");

		$getOtpTrx = $this->_db->fetchRow($getOtpTrx);
		$this->view->timeExpOtp = $getOtpTrx["SETTING_VALUE"];


		$this->view->systemType = $system_type;
		$this->view->ProvFee = 2000000;

		$conf = Zend_Registry::get('config');
		$this->_bankName = $conf['app']['bankname'];
		$this->view->masterbankname = $this->_bankName;

		$toc_ind = $settings->getSetting('ftemplate_bg_ind');
		$this->view->toc_ind = $toc_ind;
		$toc_eng = $settings->getSetting('ftemplate_bg_eng');
		$this->view->toc_eng = $toc_eng;

		$selectQuery    = "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
		// echo $selectQuery;
		$usergoogleAuth =  $this->_db->fetchAll($selectQuery);
		$this->view->userid = $this->_userIdLogin;
		$this->view->custid = $this->_custIdLogin;

		// var_dump($usergoogleAuth);die;
		if (!empty($usergoogleAuth)) {
			$this->view->googleauth = true;
			//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
			$settingObj = new Settings();
			$maxtoken = $settingObj->getSetting("max_failed_token");
			$this->view->tokenfail = (int)$maxtoken;
			if ($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0') {
				//die;
				$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];



				$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN'] + 1);
				$this->view->tokenfail = $tokenfail;
			}
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
		} else {
			$this->view->nogoauth = '1';
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
		}



		$this->view->googleauth = true;

		$selectcomp = $this->_db->select()
			->from(array('A' => 'M_CUSTOMER'), array('*'))
			//  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
			->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->query()->fetchAll();

		$this->view->compinfo = $selectcomp[0];

		$get_cash_collateral = $this->_db->select()
			->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
			->where("CUST_ID = ?", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$this->view->cash_collateral = $get_cash_collateral[0];

		$numb = $this->_getParam('bgnumb');

		// decrypt numb
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token     = $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

		$numb = $BG_NUMBER;
		$this->view->numb = $numb;

		$checkOthersAttachment = $this->_db->select()
			->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
			->where("BG_REG_NUMBER = '$numb'")
			->order('A.INDEX ASC')
			->query()->fetchAll();

		if (count($checkOthersAttachment) > 0) {
			$this->view->othersAttachment = $checkOthersAttachment;
		}
		if (!empty($numb)) {

			$bgstats = array('3', '23');
			$bgdata = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
				->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->where('A.BG_STATUS IN (?)', $bgstats)
				->query()->fetchAll();

			if ($bgdata[0]['BG_OLD']) {
				$bgOld = $this->_db->select()
					->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
					->where('BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
					->query()->fetch();

				$this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
				$this->view->prevProv = $bgOld['PROVISION_FEE'];
			}

			$kontra = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
			$timePeriodStart = $bgdata[0]['TIME_PERIOD_START'];
			$timePeriodEnd = $bgdata[0]['TIME_PERIOD_END'];
			$bgAmount = $bgdata[0]['BG_AMOUNT'];
			$custId = $bgdata[0]['CUST_ID'];
			$usagePurpose = $bgdata[0]['USAGE_PURPOSE'];
			$usagePurposeDesc = $bgdata[0]['USAGE_PURPOSE_DESC'];
			$insuranceCode = $bgdata[0]['BG_INSURANCE_CODE'];

			// penkondisian kontra biaya provisi
			if ($kontra == '3') {

				$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
				$paramProvisionFee['CUST_ID'] = $insuranceCode;
				$paramProvisionFee['GRUP'] = $custId;
				$paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
				$paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
				$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

				$provisionPercentage = $getProvisionFee['provisionFee'];

				$getLfIns = $this->_db->select()
					->from(["MCLF" => "M_CUST_LINEFACILITY"], ["FEE_DEBITED"])
					->where("CUST_ID = ?", $insuranceCode)
					->query()->fetch();

				$this->view->feeDebited = $getLfIns['FEE_DEBITED'];
			}

			if ($kontra == '1') {
				$paramProvisionFee['TIME_PERIOD_START'] = $timePeriodStart;
				$paramProvisionFee['TIME_PERIOD_END'] = $timePeriodEnd;
				$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
				$paramProvisionFee['CUST_ID'] = $custId;
				$paramProvisionFee['GRUP'] = $custId;
				$paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
				$paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
				$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

				$provisionPercentage = $getProvisionFee['provisionFee'];
			}

			if ($kontra == '2') {

				$paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
				$paramProvisionFee['CUST_ID'] = $custId;
				$paramProvisionFee['GRUP'] = $custId;
				$paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
				$paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
				$getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

				$provisionPercentage = $getProvisionFee['provisionFee'];
			}
			// end pengkondisian kontra

			$provisi = Application_Helper_General::countProvision($kontra, ['TIME_PERIOD_START' => $timePeriodStart, 'TIME_PERIOD_END' => $timePeriodEnd, 'BG_AMOUNT' => $bgAmount], $provisionPercentage);

			$this->view->allToProvision = [$provisionPercentage, $provisi, $getProvisionFee];

			$tbgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'] ? $bgdata[0]['BG_OLD'] : '')
				->query()->fetch();

			if (!empty($tbgdata['BG_REG_NUMBER'])) {
				$bgdatadetail = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $numb)
					->query()->fetchAll();

				$this->view->bgdatadetail = array_combine(array_map('strtolower', array_column($bgdatadetail, 'PS_FIELDNAME')), array_column($bgdatadetail, 'PS_FIELDVALUE'));

				$bgdatadetailT = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
					->query()->fetchAll();

				$this->view->bgdatadetailT = array_combine(array_map('strtolower', array_column($bgdatadetailT, 'PS_FIELDNAME')), array_column($bgdatadetailT, 'PS_FIELDVALUE'));
			}


			// Guaranted Transaction -----------------------

			$getGuarantedTransanctions = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_UNDERLYING')
				->where('BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$getGuarantedTransanctionsT = $this->_db->select()
				->from('T_BANK_GUARANTEE_UNDERLYING')
				->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
				->query()->fetchAll();

			$this->view->guarantedTransanctions = $getGuarantedTransanctions;
			$this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;

			$bgdocType 		= $conf["bgdoc"]["type"]["desc"];
			$bgdocCode 		= $conf["bgdoc"]["type"]["code"];

			$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
			$this->view->arrbgdoc = $arrbgdoc;

			// end Guaranted Transaction -------------------

			$checkOthersAttachmentT = $this->_db->select()
				->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
				->where("BG_REG_NUMBER = ?", $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
				->order('A.INDEX ASC')
				->query()->fetchAll();

			// if (count($checkOthersAttachmentT) > 0) {
				$this->view->othersAttachmentT = $checkOthersAttachmentT ?? [];
			// }

			$bgdatadetail = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$tbgdatadetail = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
				->query()->fetchAll();
			//$datas = $this->_request->getParams();
			//var_dump($bgdatadetail);

			if (!empty($bgdata)) {

				$data = $bgdata['0'];
				$this->view->tbgdata = $tbgdata;

				$this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];


				// switch ($bgdata[0]["CHANGE_TYPE"]) {
				// 	case '0':
				// 		$this->view->suggestion_type = "New";
				// 		break;
				// 	case '1':
				// 		$this->view->suggestion_type = "Amendment Changes";
				// 		break;
				// 	case '2':
				// 		$this->view->suggestion_type = "Amendment Draft";
				// 		break;
				// }

				// new code untuk amendemen
				$getct        = Zend_Registry::get('config');
				$ctdesc     = $getct["bg"]["changetype"]["desc"];
				$ctcode     = $getct["bg"]["changetype"]["code"];
				$suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

				$this->view->suggestion_type = $suggestion_type;

				if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
					$this->view->isinsurance = true;
					$this->view->stamp_fee = $data['STAMP_FEE'];
					$this->view->adm_fee = $data['ADM_FEE'];
					$this->view->ProvFee = $data['PROVISION_FEE'];
				}


				$policyBoundary = $this->findPolicyBoundary(38, $bgdata['0']['BG_AMOUNT']);
				//echo '<pre>';
				//var_dump($policyBoundary);die;
				$this->view->policyBoundary = $policyBoundary;

				$approverUserList = $this->findUserBoundary(38, $bgdata['0']['BG_AMOUNT']);

				$newPolicyBoundary = '';
				foreach (explode(' ', $policyBoundary) as $key => $value) {
					if (array_key_exists($value, $approverUserList['GROUP_NAME'])) {
						$newPolicyBoundary .= ' ' . $approverUserList['GROUP_NAME'][$value];
					} else {
						$newPolicyBoundary .= ' ' . $value;
					}
				}

				$releaserList = $this->findUserPrivi('RLBG');

				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->BG_PUBLISH = $arrbgpublish[$data['BG_PUBLISH']];

				if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == "3") {

					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceName = array_search("Insurance Name", array_column($bgdatadetail, "PS_FIELDNAME"));

					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];
					$getInsuranceName = $bgdatadetail[$getInsuranceName];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();

					$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];

					// get linefacillity

					$paramLimit = array();

					$paramLimit['CUST_ID'] =  $getInsuranceName["PS_FIELDVALUE"];
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $bgdata['0']['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
					/*echo "<pre>";
					var_dump($getLineFacility);*/
					//$this->view->linefacility = $get_linefacility[0];

					// end get linefacility
				}

				if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '2') {
					// get linefacillity

					$paramLimit = array();

					$paramLimit['CUST_ID'] =  $this->_custIdLogin;
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $bgdata['0']['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];

					//$this->view->linefacility = $get_linefacility[0];

					// end get linefacility
				}

				if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {


					// get linefacillity

					$paramLimit = array();

					$paramLimit['CUST_ID'] =  $this->_custIdLogin;
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $bgdata['0']['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];

					//$this->view->linefacility = $get_linefacility[0];

					// end get linefacility

					$bgdatasplit = $this->_db->select()
						->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
						->where('A.BG_REG_NUMBER = ?', $numb)
						->query()->fetchAll();

					foreach ($bgdatasplit as $key => $value) {

						$callService = new Service_Account($value['ACCT'], '');
						$cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

						if (strtolower($cekAcctType) == 'deposito') {
							$inqbalance = $callService->inquiryDeposito();
						}
						
						if ($inqbalance['varian_rate']) {
							if (floatval($inqbalance['varian_rate']) !== floatval('0')) {
								$isSpecialRate = true;
							} else {
								$isSpecialRate = false;
							}
						}

						$temp_save = $this->_db->select()
							->from("M_CUSTOMER_ACCT")
							->where("ACCT_NO = ?", $value["ACCT"])
							->query()->fetchAll();

						$bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
						$bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
						$bgdatasplit[$key]["IS_SPECIAL_RATE"] = $isSpecialRate;

					}

					$bgdatasplitT = $this->_db->select()
						->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
						->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'] ? $bgdata[0]['BG_OLD'] : '')
						->query()->fetchAll();

					foreach ($bgdatasplitT as $key => $value) {
						$temp_save = $this->_db->select()
							->from("M_CUSTOMER_ACCT")
							->where("ACCT_NO = ?", $value["ACCT"])
							->query()->fetchAll();

						$bgdatasplitT[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
						$bgdatasplitT[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
					}

					$this->view->fullmember = $bgdatasplit ?? [];
					$this->view->fullmemberA = $bgdatasplit[0];
					$this->view->fullmemberT = $bgdatasplitT ?? [];
				}

				$selectHistory	= $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY')
					->where("BG_REG_NUMBER = ?", $numb);

				$history = $this->_db->fetchAll($selectHistory);

				$config = Zend_Registry::get('config');

				$historyStatusCode = $config["history"]["status"]["code"];
				$historyStatusDesc = $config["history"]["status"]["desc"];
				$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

				$this->view->historyStatusArr   = $historyStatusArr;
				$this->view->dataHistory        = $history;

				$getBoundary = $this->_db->select()
					->from('M_APP_BOUNDARY')
					->where('CUST_ID = ?', $data['CUST_ID'])
					->where('BOUNDARY_MIN <= \'' . $data['BG_AMOUNT'] . '\'')
					->where('BOUNDARY_MAX >= \'' . $data['BG_AMOUNT'] . '\'')
					->query()->fetch();

				$checkBoundary = strpos($getBoundary['POLICY'], 'THEN');
				if ($checkBoundary === false) $checkBoundary = strpos($getBoundary['POLICY'], 'AND');
				$checkCount = 1;

				//var_dump($history);die;
				$cust_approver = 1;
				foreach ($history as $row) {
					//if maker done
					if ($row['HISTORY_STATUS'] == 1 || $row['HISTORY_STATUS'] == 33) {
						$makerStatus = 'active';
						$makerIcon = '<i class="fas fa-check"></i>';

						$makerOngoing = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
							$reviewerOngoing = '';
							$approverOngoing = '';
							//$releaserOngoing = 'ongoing';
							$releaserOngoing = '';
						} else {
							//$reviewerOngoing = 'ongoing';
							$reviewerOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						}

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						$custEmail 	  = $customer[0]['USER_EMAIL'];
						$custPhone	  = $customer[0]['USER_PHONE'];

						$makerApprovedBy = $custFullname;

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

						$align = 'align="center"';
						$marginRight = '';

						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginRight = 'style="margin-right: 15px;"';
						}

						$this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
					}
					//if reviewer done

					//if approver done
					if (false) {
						$makerStatus = 'active';
						$approveStatus = 'active';
						$reviewStatus = 'active';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = 'ongoing';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectuserapp	= $this->_db->select()
							->from(array('C' => 'T_APPROVAL'), array(
								'*'
							))
							->where("C.PS_NUMBER = ?", $numb)
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							//->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
						;

						$userapprove = $this->_db->fetchAll($selectuserapp);
						//tampung data user yang sudah approve
						if (!empty($userapprove) && empty($userid)) {
							foreach ($userapprove as $vl) {
								$userid[] = $vl['USER_ID'];
							}
						}
						//$userid[] = $custlogin;

						$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
					}

					//if approver done
					if ($checkBoundary === false) {
						if ($row['HISTORY_STATUS'] == 2) {
							$makerStatus = 'active';
							$makerIcon = '<i class="fas fa-check"></i>';

							$makerOngoing = '';
							if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
								$reviewerOngoing = '';
								$approverOngoing = '';
								//$releaserOngoing = 'ongoing';
								$releaserOngoing = '';
							} else {
								//$reviewerOngoing = 'ongoing';
								$reviewerOngoing = '';
								$approverOngoing = '';
								$releaserOngoing = '';
							}

							$custlogin = $row['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $row['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							$custEmail 	  = $customer[0]['USER_EMAIL'];
							$custPhone	  = $customer[0]['USER_PHONE'];

							$approversApprovedBy = $custFullname;

							$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

							$align = 'align="center"';
							$marginRight = '';

							if ($cust_reviewer == 0 && $cust_approver == 0) {
								$align = '';
								$marginRight = 'style="margin-right: 15px;"';
							}

							$this->view->approversApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
						}
					} else {
						if ($row['HISTORY_STATUS'] == 2) {
							$makerStatus = 'active';
							$makerIcon = '<i class="fas fa-check"></i>';

							$makerOngoing = '';
							if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
								$reviewerOngoing = '';
								$approverOngoing = '';
								//$releaserOngoing = 'ongoing';
								$releaserOngoing = '';
							} else {
								//$reviewerOngoing = 'ongoing';
								$reviewerOngoing = '';
								$approverOngoing = '';
								$releaserOngoing = '';
							}

							$custlogin = $row['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $row['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							$custEmail 	  = $customer[0]['USER_EMAIL'];
							$custPhone	  = $customer[0]['USER_PHONE'];

							$approversApprovedBy = $custFullname;

							$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

							$align = 'align="center"';
							$marginRight = '';

							// $custFullname = $customer[0]['USER_FULLNAME'];
							if ($checkCount === 1) {
								// $custFullname = $customer[0]['USER_FULLNAME'];
								$getGroupUser = $this->_db->select()
									->from('M_APP_GROUP_USER')
									->where('USER_ID = ?', $customer[0]['USER_ID'])
									->query()->fetch();

								$arrayGroup = range('A', 'Z');
								$groupUser = $arrayGroup[intval(explode('_', $getGroupUser['GROUP_USER_ID'])[2]) - 1];
								$custFullname = $efdate . '<br><span>' . $customer[0]['USER_FULLNAME'] . ' (' . $groupUser . ')</span><br>';
								$checkCount++;
							} else {
								$getGroupUser = $this->_db->select()
									->from('M_APP_GROUP_USER')
									->where('USER_ID = ?', $customer[0]['USER_ID'])
									->query()->fetch();

								$arrayGroup = range('A', 'Z');
								$groupUser = $arrayGroup[intval(explode('_', $getGroupUser['GROUP_USER_ID'])[2]) - 1];
								$custFullname = $custFullname . $efdate . '<br><span>' . $customer[0]['USER_FULLNAME'] . ' (' . $groupUser . ')</span>';
								$checkCount = 1;
							}

							if ($cust_reviewer == 0 && $cust_approver == 0) {
								$align = '';
								$marginRight = 'style="margin-right: 15px;"';
							}

							$this->view->approversApprovedBy = '<div ' . $align . ' class="textTheme">' . $custFullname . '</span></div>';
						}
					}
					//if releaser done
					// if ($row['HISTORY_STATUS'] == 5) {
					if (false) {
						$makerStatus = 'active';
						$approveIcon = '<i class="fas fa-check"></i>';
						/*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
						$approveStatus = '';
						$reviewStatus = '';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						// $custEmail 	  = $customer[0]['USER_EMAIL'];
						// $custPhone	  = $customer[0]['USER_PHONE'];

						$releaserApprovedBy = $custFullname;

						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
					}


					if ($row['HISTORY_STATUS'] == 13) {
						$makerStatus = 'active';
						$insuranceIcon = '<i class="fas fa-check"></i>';
						/*$approveStatus = 'active';
				$reviewStatus = 'active';
				$releaseStatus = 'active';
				$releaseIcon = '<i class="fas fa-check"></i>';*/
						$insuranceStatus = 'active';
						$reviewStatus = '';
						//$releaseStatus = '';
						//$releaseIcon = '';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						//$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin);
						//->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						// $custEmail 	  = $customer[0]['USER_EMAIL'];
						// $custPhone	  = $customer[0]['USER_PHONE'];

						$insuranceApprovedBy = $custFullname;

						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						$this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
					}
				}
				//var_dump($approveStatus);die;

				//approvernamecircle jika sudah ada yang approve
				if (!empty($userid)) {

					$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

					$flipAlphabet = array_flip($alphabet);

					$approvedNameList = array();
					$i = 0;
					//var_dump($userid);die;
					foreach ($userid as $key => $value) {

						//select utk nama dan email
						$selectusername = $this->_db->select()
							->from(array('M_USER'), array(
								'*'
							))
							->where("USER_ID = ?", (string) $value)
							->where("CUST_ID = ?", (string) $this->_custIdLogin);

						$username = $this->_db->fetchAll($selectusername);

						//select utk cek user berada di grup apa
						$selectusergroup	= $this->_db->select()
							->from(array('C' => 'M_APP_GROUP_USER'), array(
								'*'
							))
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							->where("C.USER_ID 	= ?", (string) $value);

						$usergroup = $this->_db->fetchAll($selectusergroup);

						$groupuserid = $usergroup[0]['GROUP_USER_ID'];
						$groupusername = $usergroup[0]['USER_ID'];
						$groupuseridexplode = explode("_", $groupuserid);

						if ($groupuseridexplode[0] == "S") {
							$usergroupid = "SG";
						} else {
							$usergroupid = $alphabet[$groupuseridexplode[2]];
						}

						// $tempuserid = "";
						// foreach ($approverNameCircle as $row => $data) {
						// 	foreach ($data as $keys => $val) {
						// 		if ($keys == $usergroupid) {
						// 			if (preg_match("/active/", $val)) {
						// 				continue;
						// 			}else{
						// 				if ($groupuserid == $tempuserid) {
						// 					continue;
						// 				}else{
						// 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
						// 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
						// 				}
						// 				$tempuserid = $groupuserid;
						// 			}
						// 		}
						// 	}
						// }

						array_push($approvedNameList, $username[0]['USER_FULLNAME']);

						$efdate = $approveEfDate[$i];

						$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
						$i++;
					}
					//var_dump($approverApprovedBy);die;
					$this->view->approverApprovedBy = $approverApprovedBy;


					//kalau sudah approve semua
					if (!$checkBoundary) {
						//$approveStatus = '';
						//$approverOngoing = '';
						//$approveIcon = '';
						$releaserOngoing = 'ongoing';
					}
				}



				$selectsuperuser = $this->_db->select()
					->from(array('C' => 'T_APPROVAL'))
					->where("C.PS_NUMBER 	= ?", $numb)
					->where("C.GROUP 	= 'SG'");

				$superuser = $this->_db->fetchAll($selectsuperuser);

				if (!empty($superuser)) {
					$userid = $superuser[0]['USER_ID'];

					//select utk nama dan email
					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("USER_ID = ?", (string) $userid)
						->where("CUST_ID = ?", (string) $this->_custIdLogin);

					$username = $this->_db->fetchAll($selectusername);

					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

					$approveStatus = 'active';
					$approverOngoing = '';
					$approveIcon = '<i class="fas fa-check"></i>';
					$releaserOngoing = 'ongoing';
				}
				// <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
				//define circle
				$makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '
					
				</button>';

				foreach ($reviewerList as $key => $value) {

					$textColor = '';
					if ($value == $reviewerApprovedBy) {
						$textColor = 'text-white-50';
					}

					$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				// 
				$reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '
		
					</button>';


				foreach ($insuranceList as $key => $value) {

					$textColor = '';
					if ($value == $insuranceApprovedBy) {
						$textColor = 'text-white-50';
					}

					$insuranceListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				// 
				$insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . '
		
					</button>';




				$groupNameList = $approverUserList['GROUP_NAME'];
				unset($approverUserList['GROUP_NAME']);

				if ($approverUserList != '') {
					//echo '<pre>';
					//var_dump($approverUserList);die;
					foreach ($approverUserList as $key => $value) {
						$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
						$i = 1;
						foreach ($value as $key2 => $value2) {

							$textColor = '';
							if (in_array($value2, $approvedNameList)) {
								$textColor = 'text-white-50';
							}

							if ($i == count($value)) {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
							} else {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
							}
							$i++;
						}
					}
				} else {
					$approverListdata = 'There is no Approver User';
				}
				// 
				$spandata = '';
				// if (!empty($approverListdata) && !$error_msg2) {
				// 	$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
				// }

				$approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '
						' . $spandata . '
					</button>';

				$releasenewNameCircle =  '<button class="btnCircleGroup  hovertext" disabled>
						' . $spandata . '
					</button>';

				foreach ($releaserList as $key => $value) {

					$textColor = '';
					if ($value == $releaserApprovedBy) {
						$textColor = 'text-white-50';
					}

					$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value['USER_FULLNAME'] . '</p>';
				}
				// 
				// $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span> </button>';
				$releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '</button>';



				$this->view->policyBoundary = trim($newPolicyBoundary);
				$this->view->releasenewNameCircle = $releasenewNameCircle;
				$this->view->makerNameCircle = $makerNameCircle;
				$this->view->reviewerNameCircle = $reviewerNameCircle;
				$this->view->insuranceNameCircle = $insuranceNameCircle;
				$this->view->approverNameCircle = $approverNameCircle;
				$this->view->releaserNameCircle = $releaserNameCircle;

				$this->view->makerStatus = $makerStatus;
				$this->view->approveStatus = $approveStatus;
				$this->view->reviewStatus = $reviewStatus;
				$this->view->releaseStatus = $releaseStatus;


				$bgType 		= $conf["bg"]["type"]["desc"];
				$bgCode 		= $conf["bg"]["type"]["code"];

				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));
				//var_dump($arrbgType);
				$this->view->arrbgType = $arrbgType;


				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					//var_dump($selectbranch[0]['BRANCH_NAME']);die;
					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


				//$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));


				$this->view->arrStatus = $arrStatus;

				$CustomerUser = new CustomerUser($bgdata[0]['CUST_ID'], $this->_userIdLogin);


				$param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
				$AccArr = $CustomerUser->getAccountsBG($param);
				//var_dump($AccArr);die;

				if (!empty($AccArr)) {
					$this->view->src_name = $AccArr['0']['ACCT_NAME'];
					$this->view->currency_type = $AccArr['0']['CCY_ID'];
				}

				$arrBankFormat = array(
					1 => 'Bank Standard',
					2 => 'Special Format (with bank approval)'
				);

				$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
				$this->view->bankFormatNumber = $data["BG_FORMAT"];

				$arrLang = array(
					1 => 'Indonesian',
					2 => 'English',
					3 => 'Billingual',
				);

				$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

				// $arrWaranty = array(
				// 	1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
				// 	2 => 'Bank Guarantee Line Facility',
				// 	3 => 'Insurance'

				// );

				// $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

				//BG Counter Guarantee Type
				$bgcgType         = $conf["bgcg"]["type"]["desc"];
				$bgcgCode         = $conf["bgcg"]["type"]["code"];

				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

				$this->view->warranty_type_text = $arrbgcg[$bgdata[0]['COUNTER_WARRANTY_TYPE']];

				// if (!empty($data['USAGE_PURPOSE'])) {
				// 	$data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
				// 	foreach ($data['USAGE_PURPOSE'] as $key => $val) {
				// 		$str = 'checkp' . $val;
				// 		//var_dump($str);
				// 		$this->view->$str =  'checked';
				// 	}
				// }

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}
				$this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];

				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->contact_person = $data['RECIPIENT_CP'];
				$this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
				$this->view->contact_email = $data['RECIPIENT_EMAIL'];
				$this->view->data = $data;
				//$this->view->comment = $data['GUARANTEE_TRANSACTION'];

				$this->view->fileName = $data['FILE'];
				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];
				$this->view->status = $data['BG_STATUS'];
				$Settings = new Settings();
				$claim_period = $Settings->getSettingNew('max_claim_period');
				$this->view->claim_period = $claim_period;


				$this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
				$bgdocType 		= $conf["bgdoc"]["type"]["desc"];
				$bgdocCode 		= $conf["bgdoc"]["type"]["code"];

				$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

				$this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];
				//$this->view->GT_TYPE = $data['GT_DOC_TYPE'];
				$this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];
				//var_dump($data['USAGE_PURPOSE']);die;
				$this->view->usage_purpose = $data['USAGE_PURPOSE'];
				$this->view->comment = $data['SERVICE'];


				if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$selectQuery  = "SELECT
                                a.USER_LOGIN,
                                b.`USER_FULLNAME` AS u_name,
                                c.`BUSER_NAME` AS b_name,
                                a.DATE_TIME,
                                a.BG_REASON,
                                a.HISTORY_STATUS,
                                a.BG_REG_NUMBER
                                
                                
                              FROM
                                T_BANK_GUARANTEE_HISTORY AS a
                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
					$result =  $this->_db->fetchAll($selectQuery);
					if (!empty($result)) {
						$data['REASON'] = $result['0']['BG_REASON'];
					}
					$this->view->reqrepair = true;
					$this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
				}



				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
								$insBranchCode =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}

				if (!empty($tbgdatadetail)) {
					foreach ($tbgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->tinsuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->tPrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->tinsurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->tpaDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->tpaDateEnd =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->towner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->tamountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->towner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->tamountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->towner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->tamountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}



				$conf = Zend_Registry::get('config');
				$this->view->bankname = $conf['app']['bankname'];

				//echo '<pre>';
				//var_dump($data);

				$download = $this->_getParam('download');
				//print_r($edit);die;
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				// Marginal Deposit View ------------------------
				// Marginal Deposit Principal ---------------
				$bgdatadetailmd = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $numb)
					->query()->fetchAll();

				if (!empty($bgdatadetailmd)) {
					foreach ($bgdatadetailmd as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
							$this->view->marginalDepositPercentage =   $value['PS_FIELDVALUE'];
						}
					}
				}
				// Marginal Deposit Principal ---------------

				// Marginal Deposit Eksisting ---------------
				$bgdatamdeks = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					// ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
					// ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
					->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : $numb)
					->query()->fetchAll();

				$this->view->bgdatamdeks = $bgdatamdeks ?? [];
				// Marginal Deposit Eksisting ---------------

				// Top Up Marginal Deposit ---------------
				$bgdatamd = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
					->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
					->where('B.BG_REG_NUMBER = ?', $numb)
					->query()->fetchAll();

				$this->view->bgdatamd = $bgdatamd ?? [];
				// Top Up Marginal Deposit ---------------
				// Marginal Deposit View ------------------------

				$errLimitIns = false;
				if ($data["COUNTER_WARRANTY_TYPE"] == "3" && $data['BG_STATUS'] == '23') {
					$principleData = [];
					// if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					foreach ($bgdatadetail as $key => $value) {
						$principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
					}
					$cust_id = $principleData["Insurance Name"];
					$get_linefacilityINS = $this->_db->select()
						->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT", "STATUS"])
						->where("CUST_ID = ?", $cust_id)
						->query()->fetchAll();


					$check_all_detail = $this->_db->select()
						->from("T_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $cust_id)
						->query()->fetchAll();

					$total_bgamount_on_risk = 0;

					if (count($check_all_detail) > 0) {
						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_risks = $this->_db->select()
							->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
							->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->query()->fetchAll();

						foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
							$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
						}
					}

					$check_all_detail = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $cust_id)
						->query()->fetchAll();

					$total_bgamount_on_temp = 0;

					if (count($check_all_detail) > 0) {

						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_temps = $this->_db->select()
							->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
							->where("COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->where("BG_STATUS IN (?)", ["5", "6", "7", '10', "14", "17", "20", '21', '22', '23', '24'])
							->query()->fetchAll();

						foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
							$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
						}
					}

					$current_limitINS = $get_linefacilityINS[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;

					$this->view->current_limit_ins = $current_limitINS;
					// if ($data['BG_AMOUNT'] >= $current_limitINS) {
					if ($current_limitINS < 0) {
						$errLimitIns = true;

						$errmsg = 'Limit asuransi tidak tersedia';
						$this->view->errorTickerSize = $errmsg;
					}

					//die;

				}

				if ($this->_request->isPost()) {
					$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
					$approve = $this->_getParam('release');
					$BG_NUMBER = $this->_getParam('bgnumb');
					$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

					$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
					$reject = $this->_getParam('reject');
					$repair = $this->_getParam('repair');
					$filter             = new Application_Filtering();
					$inputtoken1        = $this->_getParam('inputtoken1');
					$inputtoken2        = $this->_getParam('inputtoken2');
					$inputtoken3        = $this->_getParam('inputtoken3');
					$inputtoken4        = $this->_getParam('inputtoken4');
					$inputtoken5        = $this->_getParam('inputtoken5');
					$inputtoken6        = $this->_getParam('inputtoken6');

					$responseCode       = $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

					$responseCode       = $filter->filter($responseCode, "SELECTION");
					//print_r($edit);die;
					//var_dump($approve);die;


					if ($approve) {

						$getNamaPemohon = $this->_db->select()
							->from('M_CUSTOMER', ['CUST_NAME'])
							->where('CUST_ID = ?', $bgdata[0]['CUST_ID'])
							->query()->fetch();

						$getNamaPemohon = $getNamaPemohon['CUST_NAME'];

						// start validasi jaminan
						$errValidasi = false;

						if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '2') {

							$cust_id = $this->_custIdLogin;
							$jenisjaminan = ($data['USAGE_PURPOSE'] !== 'F4299') ? strtoupper($arrbgType[$data['USAGE_PURPOSE']]) : strtoupper($data['USAGE_PURPOSE_DESC']);

							$check_grup_bumn = $this->_db->select()
								->from("M_CUSTOMER")
								->where("CUST_STATUS = 1")
								->where("CUST_ID = ?", $cust_id)
								->query()->fetchAll();


							$check_charges_bg = $this->_db->select()
								->from("M_CHARGES_BG")
								->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'LFBUMN' : 'LFNONBUMN') . '%')
								->where("CHARGES_NAME LIKE ?", '' . 'LF ' . $jenisjaminan . '%')
								->query()->fetchAll();


							$check_lf_detail = $this->_db->select()
								->from("M_CUST_LINEFACILITY_DETAIL")
								->where("OFFER_TYPE = ?", $jenisjaminan)
								//->where("FLAG = 1")
								->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
								->where("CUST_ID = ?", $cust_id)
								->query()->fetchAll();
							$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];

							$check_lf_detail[0]['FLAG'] == '1' && $check_lf_detail[0]['ACTIVE'] == 'Y' ? $errValidasi = false : $errValidasi = true;
						}

						if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {

							$cust_id = $data['BG_INSURANCE_CODE'];
							$jenisjaminan = ($data['USAGE_PURPOSE'] !== 'F4299') ? strtoupper($arrbgType[$data['USAGE_PURPOSE']]) : strtoupper($data['USAGE_PURPOSE_DESC']);

							$check_grup_bumn = $this->_db->select()
								->from("M_CUSTOMER")
								->where("CUST_STATUS = 1")
								->where("CUST_ID = ?", $cust_id)
								->query()->fetchAll();


							$check_charges_bg = $this->_db->select()
								->from("M_CHARGES_BG")
								->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'INSBUMN' : 'INSNONBUMN') . '%')
								->where("CHARGES_NAME LIKE ?", '' . 'ASURANSI ' . $jenisjaminan . '%')
								->query()->fetchAll();


							$check_lf_detail = $this->_db->select()
								->from("M_CUST_LINEFACILITY_DETAIL")
								->where("OFFER_TYPE = ?", $jenisjaminan)
								//->where("FLAG = 1")
								->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
								->where("CUST_ID = ?", $cust_id)
								->query()->fetchAll();
							$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];

							$check_lf_detail[0]['FLAG'] == '1' && $check_lf_detail[0]['ACTIVE'] == 'Y' ? $errValidasi = false : $errValidasi = true;
						}



						//die;
						// end validasi jaminan

						$bgdata = $this->_db->select()
							->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
							->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
							->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
							->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
							// ->where('A.BG_STATUS IN (?)', $bgstats)
							->query()->fetchAll();


						$data = $bgdata['0'];
						//var_dump($usergoogleAuth);die;
						if (!empty($usergoogleAuth)) {

							$pga = new PHPGangsta_GoogleAuthenticator();
							// var_dump($usergoogleAuth['0']['GOOGLE_CODE']);
							//var_dump($responseCode);die;
							$settingObj = new Settings();
							$gduration = $settingObj->getSetting("google_duration");
							//$token = SGO_Helper_GeneralFunction::generateToken($userId);
							//if ($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, $gduration)) {

							//var_dump($responseCode);
							//var_dump($this->_userIdLogin);
							//die();

							if (SGO_Helper_GeneralFunction::validateToken($BG_NUMBER, $this->_custIdLogin, $this->_userIdLogin, $responseCode)) {

								if (empty($errValidasi) && !$errLimitIns) {

									//die('here');
									$this->_db->beginTransaction();
									$resultToken = $resHard['ResponseCode'] == '0000';
									$datatoken = array(
										'USER_FAILEDTOKEN' => 0
									);

									$wheretoken =  array();
									$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
									$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
									$this->_db->update('M_USER', $datatoken, $wheretoken);


									//$insertbg = $bgdata['0'];
									//$insertbg['BG_STATUS'] = '7';
									//$this->_db->insert('T_BANK_GUARANTEE',$insertbg);

									if ($data['BG_STATUS'] == '3' && $data['COUNTER_WARRANTY_TYPE'] == '3') {
										$data = array('BG_STATUS' => '8');
										$historyInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'BG_REG_NUMBER'         => $BG_NUMBER,
											'CUST_ID'           => $this->_custIdLogin,
											'USER_LOGIN'        => $this->_userIdLogin,
											'HISTORY_STATUS'    => 5
											//  'BG_REASON'         => $notes,
										);

										$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
									} else {
										$data = array('BG_STATUS' => '5');
										$historyInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'BG_REG_NUMBER'         => $BG_NUMBER,
											'CUST_ID'           => $this->_custIdLogin,
											'USER_LOGIN'        => $this->_userIdLogin,
											// 'HISTORY_STATUS'    => 5,
											//  'BG_REASON'         => $notes,
										);

										if ($this->view->warranty_type == '3' && !empty($bgdatamd)) {
											$historyInsert['HISTORY_STATUS'] = 23;
										} else {
											$historyInsert['HISTORY_STATUS'] = 5;
										}

										$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
									}

									$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
									$data['NTU_COUNTING'] = 0;

									if ($this->view->warranty_type == '3' && $this->view->status != '23') {
										$data["PRINCIPLE_APPROVE"] = 0;
									} elseif ($this->view->warranty_type == '3' && $this->view->status == '23') {
										$data["PRINCIPLE_APPROVE"] = 1;
									}
									$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

									// insert to TEMP_BANK_GUARANTEE_PRELIMINARY ------------------------------------------------

									$getPreliminary = $this->_db->select()
										->from('M_PRELIMINARY')
										->where('CUST_ID = ?', $this->_custIdLogin)
										->query()->fetch();

									$getPreliminary['BG_REG_NUMBER'] = $bgdata[0]['BG_REG_NUMBER'];
									unset($getPreliminary['LAST_SUGGESTED']);
									unset($getPreliminary['LAST_SUGGESTEDBY']);
									unset($getPreliminary['LAST_APPROVED']);
									unset($getPreliminary['LAST_APPROVEDBY']);

									$this->_db->insert('TEMP_BANK_GUARANTEE_PRELIMINARY', $getPreliminary);

									$getPreliminaryMember = $this->_db->select()
										->from('M_PRELIMINARY_MEMBER')
										->where('CUST_ID = ?', $this->_custIdLogin)
										->query()->fetchAll();

									foreach ($getPreliminaryMember as $member) {
										unset($member['ID']);
										$member['BG_REG_NUMBER'] = $bgdata[0]['BG_REG_NUMBER'];

										$this->_db->insert('TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER', $member);
									}

									// end insert to TEMP_BANK_GUARANTEE_PRELIMINARY ---------------------------------------------

									//	$insertbgdetail = $bgdatadetail['0'];

									//	if(!empty($insertbgdetail)){
									//	
									//	$this->_db->insert('T_BANK_GUARANTEE_DETAIL',$insertbgdetail);
									//    }
									//var_dump($bgdatasplit);die;
									//if(!empty($bgdatasplit)){
									//	foreach($bgdatasplit as $vl){
									//	$insSplit = $vl;
									//	$this->_db->insert('T_BANK_GUARANTEE_SPLIT',$insSplit);	
									//}
									//}
									//$this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL',$this->dbObj->quoteInto('BG_REG_NUMBER = ?',$insertbg['BG_REG_NUMBER']));
									//$this->_db->delete('TEMP_BANK_GUARANTEE',$this->dbObj->quoteInto('BG_REG_NUMBER = ?',$insertbg['BG_REG_NUMBER']));
									//$this->_db->delete('TEMP_BANK_GUARANTEE_SPLIT',$this->dbObj->quoteInto('BG_REG_NUMBER = ?',$insertbg['BG_REG_NUMBER']));

									// $notes = $this->_getParam('PS_REASON_REJECT');

									if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '1') {
										$getAllBgroup = $this->_db->select()
											->from('M_BPRIVI_GROUP')
											->where('BPRIVI_ID  = ?', 'VCCS')
											->query()->fetchAll();
									} elseif ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '2') {
										$getAllBgroup = $this->_db->select()
											->from('M_BPRIVI_GROUP')
											->where('BPRIVI_ID  = ?', 'VNCS')
											->query()->fetchAll();
									} elseif ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '3' && $bgdata[0]['BG_STATUS'] == '23') {
										$getAllBgroup = $this->_db->select()
											->from('M_BPRIVI_GROUP')
											->where('BPRIVI_ID  = ?', 'VNCS')
											->query()->fetchAll();
									}

									if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '1' || $bgdata[0]['COUNTER_WARRANTY_TYPE'] === '2') {
										$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');

										$getAllBuser = $this->_db->select()
											->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
											->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
											->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
											->where('MB.BGROUP_ID IN (?)', $saveBgroup)
											->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
											->query()->fetchAll();

										$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';

										$allSetting = $setting->getAllSetting();
										foreach ($getAllBuser as $buser) {
											$getEmailTemplate = $allSetting['bemailtemplate_verification_notif'];

											$data = [
												'[[user_name]]' => $buser['BUSER_NAME'],
												'[[group_name]]' => $buser['BGROUP_DESC'],
												'[[master_bank_name]]' => $allSetting["master_bank_name"],
												'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
												'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
												'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
												// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
												'[[recipient_name]]' => $getNamaPemohon,
												'[[is_amandment]]' => $tipePengajuan,
												'[[bg_status]]' => $bgStatus[5],
												'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
											];

											$getEmailTemplate = strtr($getEmailTemplate, $data);
											Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], 'BG Verification Notif', $getEmailTemplate);
										}
									}

									if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '3' && $bgdata[0]['BG_STATUS'] == '23') {
										$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');

										$getAllBuser = $this->_db->select()
											->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
											->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
											->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
											->where('MB.BGROUP_ID IN (?)', $saveBgroup)
											->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
											->query()->fetchAll();

										$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';

										$allSetting = $setting->getAllSetting();
										foreach ($getAllBuser as $buser) {
											$getEmailTemplate = $allSetting['bemailtemplate_verification_notif'];

											$data = [
												'[[user_name]]' => $buser['BUSER_NAME'],
												'[[group_name]]' => $buser['BGROUP_DESC'],
												'[[master_bank_name]]' => $allSetting["master_bank_name"],
												'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
												'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
												'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
												// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
												'[[recipient_name]]' => $getNamaPemohon,
												'[[is_amandment]]' => $tipePengajuan,
												'[[bg_status]]' => $bgStatus[5],
												'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
											];

											$getEmailTemplate = strtr($getEmailTemplate, $data);
											Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], 'BG Verification Notif', $getEmailTemplate);
										}
									}

									if (intval($bgdata[0]['COUNTER_WARRANTY_TYPE']) === 3 && $bgdata[0]['BG_STATUS'] != '23') {
										$getInsranch = $this->_db->select()
											->from('M_INS_BRANCH', ['INS_BRANCH_NAME', 'INS_BRANCH_EMAIL'])
											->where('INS_BRANCH_CODE = ?', $insBranchCode)
											->query()->fetch();

										$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';

										$allSetting = $setting->getAllSetting();
										$getEmailTemplate = $allSetting['femailtemplate_ijinprinsip_notif'];

										$data = [
											'[[nama_cabang_asuransi]]' => $getInsranch['INS_BRANCH_NAME'],
											'[[user_name]]' => $buser['BUSER_NAME'],
											'[[group_name]]' => $buser['BGROUP_DESC'],
											'[[master_bank_name]]' => $allSetting["master_bank_name"],
											'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
											'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
											'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
											// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
											'[[recipient_name]]' => $getNamaPemohon,
											'[[is_amandment]]' => $tipePengajuan,
											'[[bg_status]]' => $bgStatus[8],
											'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
										];

										$getEmailTemplate = strtr($getEmailTemplate, $data);
										Application_Helper_Email::sendEmail($getInsranch['INS_BRANCH_EMAIL'], 'Permintaan Permohonan Ijin Prinsip', $getEmailTemplate);
									}


									$this->_db->commit();
									$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
									$this->_redirect('/notification/success/index');
								} else {
									$this->view->err = true;
									$this->view->errMsg = 'Jaminan tidak tersedia';
								}
							} else {
								$tokenFailed = $CustUser->setLogToken(); //log token activity

								$error = true;
								$errorMsg[] = $this->language->_('Invalid Response Code');  //$verToken['ResponseDesc'];
								$this->view->popauth = true;
								if ($tokenFailed === true) {
									$this->_redirect('/default/index/logout');
								}
							}

							//$resultToken = $resHard['ResponseCode'] == '0000';
						} else {

							if (SGO_Helper_GeneralFunction::validateToken($BG_NUMBER, $this->_custIdLogin, $this->_userIdLogin, $responseCode)) {

								if (empty($errValidasi)) {

									//die('here');
									$this->_db->beginTransaction();
									$resultToken = $resHard['ResponseCode'] == '0000';
									$datatoken = array(
										'USER_FAILEDTOKEN' => 0
									);

									$wheretoken =  array();
									$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
									$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
									$this->_db->update('M_USER', $datatoken, $wheretoken);


									//$insertbg = $bgdata['0'];
									//$insertbg['BG_STATUS'] = '7';
									//$this->_db->insert('T_BANK_GUARANTEE',$insertbg);
									//echo "<pre>";
									//var_dump($data);
									//die();
									if ($data['BG_STATUS'] == '3' && $data['COUNTER_WARRANTY_TYPE'] == '3') {
										$data = array('BG_STATUS' => '8');
										$historyInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'BG_REG_NUMBER'         => $BG_NUMBER,
											'CUST_ID'           => $this->_custIdLogin,
											'USER_LOGIN'        => $this->_userIdLogin,
											'HISTORY_STATUS'    => 5
											//  'BG_REASON'         => $notes,
										);

										$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
									} else {
										$data = array('BG_STATUS' => '5');
										$historyInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'BG_REG_NUMBER'         => $BG_NUMBER,
											'CUST_ID'           => $this->_custIdLogin,
											'USER_LOGIN'        => $this->_userIdLogin,
											// 'HISTORY_STATUS'    => 5,
											//  'BG_REASON'         => $notes,
										);

										if ($this->view->warranty_type == '3' && !empty($bgdatamd)) {
											$historyInsert['HISTORY_STATUS'] = 23;
										} else {
											$historyInsert['HISTORY_STATUS'] = 5;
										}

										$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
									}

									if ($this->view->warranty_type == '3' && $this->view->status != '23') {
										$data["PRINCIPLE_APPROVE"] = 0;
									} elseif ($this->view->warranty_type == '3' && $this->view->status == '23') {
										$data["PRINCIPLE_APPROVE"] = 1;
									}
									$data['NTU_COUNTING'] = 0;
									$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
									$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

									// insert to TEMP_BANK_GUARANTEE_PRELIMINARY ------------------------------------------------

									$getPreliminary = $this->_db->select()
										->from('M_PRELIMINARY')
										->where('CUST_ID = ?', $this->_custIdLogin)
										->query()->fetch();

									$getPreliminary['BG_REG_NUMBER'] = $bgdata[0]['BG_REG_NUMBER'];
									unset($getPreliminary['LAST_SUGGESTED']);
									unset($getPreliminary['LAST_SUGGESTEDBY']);
									unset($getPreliminary['LAST_APPROVED']);
									unset($getPreliminary['LAST_APPROVEDBY']);

									$this->_db->insert('TEMP_BANK_GUARANTEE_PRELIMINARY', $getPreliminary);

									$getPreliminaryMember = $this->_db->select()
										->from('M_PRELIMINARY_MEMBER')
										->where('CUST_ID = ?', $this->_custIdLogin)
										->query()->fetchAll();

									foreach ($getPreliminaryMember as $member) {
										unset($member['ID']);
										$member['BG_REG_NUMBER'] = $bgdata[0]['BG_REG_NUMBER'];

										$this->_db->insert('TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER', $member);
									}

									// end insert to TEMP_BANK_GUARANTEE_PRELIMINARY ---------------------------------------------

									//	$insertbgdetail = $bgdatadetail['0'];

									//	if(!empty($insertbgdetail)){
									//	
									//	$this->_db->insert('T_BANK_GUARANTEE_DETAIL',$insertbgdetail);
									//    }
									//var_dump($bgdatasplit);die;
									//if(!empty($bgdatasplit)){
									//	foreach($bgdatasplit as $vl){
									//	$insSplit = $vl;
									//	$this->_db->insert('T_BANK_GUARANTEE_SPLIT',$insSplit);	
									//}
									//}
									//$this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL',$this->dbObj->quoteInto('BG_REG_NUMBER = ?',$insertbg['BG_REG_NUMBER']));
									//$this->_db->delete('TEMP_BANK_GUARANTEE',$this->dbObj->quoteInto('BG_REG_NUMBER = ?',$insertbg['BG_REG_NUMBER']));
									//$this->_db->delete('TEMP_BANK_GUARANTEE_SPLIT',$this->dbObj->quoteInto('BG_REG_NUMBER = ?',$insertbg['BG_REG_NUMBER']));

									// $notes = $this->_getParam('PS_REASON_REJECT');


									if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '1') {
										$getAllBgroup = $this->_db->select()
											->from('M_BPRIVI_GROUP')
											->where('BPRIVI_ID  = ?', 'VCCS')
											->query()->fetchAll();
									} elseif ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '2') {
										$getAllBgroup = $this->_db->select()
											->from('M_BPRIVI_GROUP')
											->where('BPRIVI_ID  = ?', 'VNCS')
											->query()->fetchAll();
									} elseif ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '3' && $bgdata[0]['BG_STATUS'] == '23') {
										$getAllBgroup = $this->_db->select()
											->from('M_BPRIVI_GROUP')
											->where('BPRIVI_ID  = ?', 'VNCS')
											->query()->fetchAll();
									}

									if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '1' || $bgdata[0]['COUNTER_WARRANTY_TYPE'] === '2') {
										$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');

										$getAllBuser = $this->_db->select()
											->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
											->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
											->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
											->where('MB.BGROUP_ID IN (?)', $saveBgroup)
											->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
											->query()->fetchAll();

										// $getAllBuserEmail = array_column($getAllBuser, 'BUSER_EMAIL');


										$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';

										$allSetting = $setting->getAllSetting();
										foreach ($getAllBuser as $buser) {
											$getEmailTemplate = $allSetting['bemailtemplate_verification_notif'];

											$data = [
												'[[user_name]]' => $buser['BUSER_NAME'],
												'[[group_name]]' => $buser['BGROUP_DESC'],
												'[[master_bank_name]]' => $allSetting["master_bank_name"],
												'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
												'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
												'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
												'[[recipient_name]]' => $getNamaPemohon,
												// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
												'[[is_amandment]]' => $tipePengajuan,
												'[[bg_status]]' => $bgStatus[5],
												'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
											];

											$getEmailTemplate = strtr($getEmailTemplate, $data);
											Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], 'BG Verification Notif', $getEmailTemplate);
										}
									}

									if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '3' && $bgdata[0]['BG_STATUS'] == '23') {
										$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');

										$getAllBuser = $this->_db->select()
											->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
											->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
											->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
											->where('MB.BGROUP_ID IN (?)', $saveBgroup)
											->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
											->query()->fetchAll();

										$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';

										$allSetting = $setting->getAllSetting();
										foreach ($getAllBuser as $buser) {
											$getEmailTemplate = $allSetting['bemailtemplate_verification_notif'];

											$data = [
												'[[user_name]]' => $buser['BUSER_NAME'],
												'[[group_name]]' => $buser['BGROUP_DESC'],
												'[[master_bank_name]]' => $allSetting["master_bank_name"],
												'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
												'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
												'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
												// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
												'[[recipient_name]]' => $getNamaPemohon,
												'[[is_amandment]]' => $tipePengajuan,
												'[[bg_status]]' => $bgStatus[5],
												'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
											];

											$getEmailTemplate = strtr($getEmailTemplate, $data);
											Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], 'BG Verification Notif', $getEmailTemplate);
										}
									}

									if (intval($bgdata[0]['COUNTER_WARRANTY_TYPE']) === 3 && $bgdata[0]['BG_STATUS'] != '23') {
										$getInsranch = $this->_db->select()
											->from('M_INS_BRANCH', ['INS_BRANCH_NAME', 'INS_BRANCH_EMAIL'])
											->where('INS_BRANCH_CODE = ?', $insBranchCode)
											->query()->fetch();

										$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';

										$allSetting = $setting->getAllSetting();
										$getEmailTemplate = $allSetting['femailtemplate_ijinprinsip_notif'];

										$data = [
											'[[nama_cabang_asuransi]]' => $getInsranch['INS_BRANCH_NAME'],
											'[[user_name]]' => $buser['BUSER_NAME'],
											'[[group_name]]' => $buser['BGROUP_DESC'],
											'[[master_bank_name]]' => $allSetting["master_bank_name"],
											'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
											'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
											'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
											// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
											'[[recipient_name]]' => $getNamaPemohon,
											'[[is_amandment]]' => $tipePengajuan,
											'[[bg_status]]' => $bgStatus[8],
											'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
										];

										$getEmailTemplate = strtr($getEmailTemplate, $data);
										Application_Helper_Email::sendEmail($getInsranch['INS_BRANCH_EMAIL'], 'Permintaan Permohonan Ijin Prinsip', $getEmailTemplate);
									}

									$this->_db->commit();
									if ($this->view->warranty_type == '3' && !empty($bgdatamd)) {
										Application_Helper_General::writeLog('MDBG', $this->language->_('Rilis Tambah Marginal Deposit BG No Reg') . ' : ' . $bgdata[0]['BG_REG_NUMBER']);
									} else {
										Application_Helper_General::writeLog('RLBG', $this->language->_('Rilis BG No Reg') . ' : ' . $bgdata[0]['BG_REG_NUMBER']);
									}

									$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
									$this->_redirect('/notification/success/index');
								} else {
									$this->view->err = true;
									$this->view->errMsg = 'Jaminan tidak tersedia';
								}
							} else {
								$tokenFailed = $CustUser->setLogToken(); //log token activity

								$error = true;
								$errorMsg[] = $this->language->_('Invalid Response Code');  //$verToken['ResponseDesc'];
								$this->view->popauth = true;
								if ($tokenFailed === true) {
									$this->_redirect('/default/index/logout');
								}
							}
						}

						if ($error) {
							$this->view->popauth                = true;
							//var_dump($errorMsg);
							$this->view->errorMsg           = $errorMsg[0];
						}
					}

					if ($reject) {
						$data = array('BG_STATUS' => '25');
						$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
						$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);



						$notes = $this->_getParam('PS_REASON_REJECT');
						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'         => $BG_NUMBER,
							'CUST_ID'           => $this->_custIdLogin,
							'USER_LOGIN'        => $this->_userIdLogin,
							'HISTORY_STATUS'    => 4,
							'BG_REASON'         => $notes,
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

						Application_Helper_General::writeLog('RLBG', $this->language->_('Tolak BG No Reg') . ' : ' . $bgdata[0]['BG_REG_NUMBER'] . ', ' . $this->language->_('Catatan') . ' : ' . $notes);

						$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
						$this->_redirect('/notification/success/index');
					}


					if ($repair) {
						//die('here');
						// $data = array ('BG_STATUS' => '10');
						$repairmd = $this->_getParam('repairmd');

						// hapus data sebelumnya
						if ($bgdata[0]['BG_STATUS'] == '23' && !empty($bgdatamd) && $repairmd == '4') {
							$this->_db->delete("TEMP_BANK_GUARANTEE_SPLIT", ["BG_REG_NUMBER = ?" => $numb]);
						}
						// end hapus data sebelumya

						$data = array(
							// 'BG_STATUS' => '4',
							'BG_UPDATED' => new Zend_Db_Expr("now()"),
						);

						if ($repairmd == '24') {
							$data['BG_STATUS'] = $repairmd;
						} else {
							$data['BG_STATUS'] = '4';
						}

						$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
						$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

						$notes = $this->_getParam('PS_REASON_REPAIR');
						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'         => $BG_NUMBER,
							'CUST_ID'           => $this->_custIdLogin,
							'USER_LOGIN'        => $this->_userIdLogin,
							// 'HISTORY_STATUS'    => 3,
							'BG_REASON'         => $notes,
						);

						if ($repairmd == '24') {
							$historyInsert['HISTORY_STATUS'] = 22;
						} else {
							$historyInsert['HISTORY_STATUS'] = 3;
						}

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

						if ($repairmd == '24') {
							Application_Helper_General::writeLog('RLBG', $this->language->_('Permintaan Perbaikan Marginal Deposit Prinsipal BG No Reg') . ' : ' . $bgdata[0]['BG_REG_NUMBER'] . ', ' . $this->language->_('Catatan') . ' : ' . $notes);
						} else {
							Application_Helper_General::writeLog('RLBG', $this->language->_('Permintaan Perbaikan BG No Reg') . ' : ' . $bgdata[0]['BG_REG_NUMBER'] . ', ' . $this->language->_('Catatan') . ' : ' . $notes);
						}

						$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
						$this->_redirect('/notification/success/index');
					}


					$back = $this->_getParam('back');
					if ($back) {
						$this->_redirect('/eformworkflow/release');
					}
				}
			}

			Application_Helper_General::writeLog('RLBG', $this->language->_('Lihat Detail BG No Reg') . ' : ' . $data['BG_REG_NUMBER']);
		}
	}


	public function findPolicyBoundary($transfertype, $amount)
	{


		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{



		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
		//	echo '<pre>';
		//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	public function findUserPrivi($privID)
	{



		$selectuser	= $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'))
			->joinLeft(array('B' => 'M_USER'), 'A.FUSER_ID = CONCAT(B.CUST_ID,B.USER_ID)', array('USER_FULLNAME'))
			->where("A.FPRIVI_ID 	= ?", (string) $privID)
			->where("B.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->order('B.USER_FULLNAME ASC');

		//echo $selectuser;die();
		return $datauser = $this->_db->fetchAll($selectuser);
	}




	public function sendtokenAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$numb = $this->_getParam('id');
		$userId = $this->_userIdLogin;

		$token = SGO_Helper_GeneralFunction::generateToken($userId);

		//var_dump($token);die;
		$selectQuery    = "SELECT USER_EMAIL FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " ";
		// echo $selectQuery;
		$userData =  $this->_db->fetchAll($selectQuery);
		$userData = $userData['0'];

		$bgdata = $this->_db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('A.BG_REG_NUMBER = ?', $numb)
			->where('A.BG_STATUS IN (3, 23)')
			->query()->fetchAll();
		//echo $bgdata;
		$data = $bgdata['0'];
		//var_dump($data);die;

		$bgdatadetail = $this->_db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('A.BG_REG_NUMBER = ?', $numb)
			->query()->fetchAll();

		if (!empty($bgdatadetail)) {
			foreach ($bgdatadetail as $key => $value) {
				if ($value['PS_FIELDNAME'] == 'Currency') {
					$currency['currency'] = $value['PS_FIELDVALUE'];
				}
			}
		}

		$Settings = new Settings();
		$allSetting = $Settings->getAllSetting();
		$EmailLoginNotif 				= $allSetting['femailtemplate_token'];
		$templateEmailMasterBankName	= $allSetting['master_bank_name'];
		$templateEmailMasterBankAppName = $allSetting['master_bank_app_name'];
		$templateEmailMasterBankEmail	= $allSetting['master_bank_email'];
		$templateEmailMasterBankTelp	= $allSetting['master_bank_telp'];
		$templateEmailMasterBankWapp 	= $allSetting['master_bank_wapp'];

		$getOtpTrx = $this->_db->select()
			->from("M_SETTING")
			->where("SETTING_ID = ?", "otp_exp_trx");

		$getOtpTrx = $this->_db->fetchRow($getOtpTrx);

		$timeExpiredToken = new DateTime("now");
		// $timeExpiredToken->add(new DateInterval("PT1M"));
		$timeExpiredToken->add(new DateInterval("PT" . strval($getOtpTrx["SETTING_VALUE"]) . "M"));
		$stamp = $timeExpiredToken->format('Y-m-d H:i:s');
		$timeExpiredToken = $timeExpiredToken->format("H:i:s");



		//$timeExpiredToken = $timeExpiredToken->format("H:i");

		//echo $stamp;


		$listtoken    = "SELECT * FROM M_LIST_TOKEN WHERE BG_REG_NUMBER = '" . $data['BG_REG_NUMBER'] . "' AND CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " ";

		// echo $listtoken;

		$listtokenData =  $this->_db->fetchAll($listtoken);

		if ($listtokenData) {
			$this->_db->delete("M_LIST_TOKEN", "M_LIST_TOKEN.BG_REG_NUMBER = '" . $data['BG_REG_NUMBER'] . "' AND CUST_ID = '" . $this->_custIdLogin . "' AND USER_ID = '" . $this->_userIdLogin . "'");
		}

		$encryption = new SGO_Helper_Encryption();
		$encryptToken = $encryption->encrypt($token);

		$listToken = array(
			'EXPIRY_TIMESTAMP'         => $stamp,
			'BG_REG_NUMBER'         => $data['BG_REG_NUMBER'],
			'CUST_ID'           => $this->_custIdLogin,
			'USER_ID'        => $this->_userIdLogin,
			'TOKEN'    =>  $encryptToken,
		);

		$this->_db->insert('M_LIST_TOKEN', $listToken);

		$datatemp = array(
			'[[comp_code]]' => $this->_custIdLogin,
			'[[user_login]]' => $this->_userIdLogin,
			'[[register_number]]' => $data['BG_REG_NUMBER'],
			'[[token]]' => $token,
			'[[timeExpiredToken]]' => $timeExpiredToken,
			'[[dateExpired]]' => date("d-M-Y"),
			'[[cust_name]]' => $this->_custNameLogin,
			'[[subject]]' => $data['BG_SUBJECT'],
			'[[currency]]' => $currency['currency'],
			'[[amount]]' => number_format($data['BG_AMOUNT']),
			'[[master_bank_name]]' => $templateEmailMasterBankName,
			'[[master_bank_app_name]]' => $templateEmailMasterBankAppName,
			'[[master_bank_email]]' => $templateEmailMasterBankEmail,
			'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,
			'[[master_bank_wapp]]' 	=> $templateEmailMasterBankWapp
		);

		//var_dump($datatemp);
		$EmailLoginNotif = strtr($EmailLoginNotif, $datatemp);
		//echo $EmailLoginNotif;die;
		//echo $userData['USER_EMAIL'];die;
		if (!empty($userData['USER_EMAIL'])) {
			Application_Helper_Email::sendEmail($userData['USER_EMAIL'], 'Token Notification', $EmailLoginNotif);
		}
		return true;
		//echo $optHtml;
	}

	public function validatetokenAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$numb = $this->_getParam('id');
		$userId = $this->_getParam('userid');
		$custId = $this->_getParam('custid');
		$bgregnum = $this->_getParam('id');
		$inputtoken1        = $this->_getParam('inputtoken1');
		$inputtoken2        = $this->_getParam('inputtoken2');
		$inputtoken3        = $this->_getParam('inputtoken3');
		$inputtoken4        = $this->_getParam('inputtoken4');
		$inputtoken5        = $this->_getParam('inputtoken5');
		$inputtoken6        = $this->_getParam('inputtoken6');

		$responseCode       = $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

		$filter             = new Application_Filtering();
		$responseCode       = $filter->filter($responseCode, "SELECTION");
		$result = SGO_Helper_GeneralFunction::validateToken($bgregnum, $custId, $userId, $responseCode);
		//echo $result;die;
		$return = array();
		if ($result == "tokenSuccess") {
			$return['return'] = "tokenSuccess";
		} else {
			$settingObj = new Settings();
			$maxtoken = $settingObj->getSetting("max_failed_token");

			$selectQuery    = "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
					 WHERE CUST_ID = " . $this->_db->quote($custId) . " AND USER_ID = " . $this->_db->quote($userId) . "";
			// echo $selectQuery;
			$usergoogleAuth =  $this->_db->fetchAll($selectQuery);
			$tokensubmit = ($usergoogleAuth['0']['USER_FAILEDTOKEN']) + 1;
			$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN'] + 1);


			$this->_db->beginTransaction();
			$datatoken = array(
				'USER_FAILEDTOKEN' => $tokensubmit
			);

			$wheretoken =  array();
			$wheretoken['USER_ID = ?'] = $userId;
			$wheretoken['CUST_ID = ?'] = $custId;
			//var_dump($wheretoken);die;
			$this->_db->update('M_USER', $datatoken, $wheretoken);
			$this->_db->commit();

			$return['return'] = "tokenFailed";
			$return['tokenfail'] = $tokenfail;

			if ($tokensubmit == $maxtoken) {
				$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$tokenFailed = $CustUser->setLogToken(); //log token activity
				$return['redirect'] = true;
				//$this->_redirect('/default/index/logout');
			}
		}


		//var_dump($result);die;
		//header('Content-Type: application/json; charset=utf-8');
		echo json_encode($return);

		//echo json_encode($result);


	}

	public function checktypebglfAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$get_offer_type = $this->_request->getParam("offertype");
		$warranty_type = $this->_request->getParam("warranty_type");

		if ($warranty_type == 2) {
			$cust_id = $this->_request->getParam("cust_id");

			$check_grup_bumn = $this->_db->select()
				->from("M_CUSTOMER")
				->where("CUST_STATUS = 1")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();


			$check_charges_bg = $this->_db->select()
				->from("M_CHARGES_BG")
				->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'LFBUMN' : 'LFNONBUMN') . '%')
				->where("CHARGES_NAME LIKE ?", '' . 'LF ' . $get_offer_type . '%')
				->query()->fetchAll();


			$check_lf_detail = $this->_db->select()
				->from("M_CUST_LINEFACILITY_DETAIL")
				->where("OFFER_TYPE = ?", $get_offer_type)
				//->where("FLAG = 1")
				->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();
			$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($check_lf_detail[0]);
	}

	public function checktypebginsAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$get_offer_type = $this->_request->getParam("offertype");
		$warranty_type = $this->_request->getParam("warranty_type");

		if ($warranty_type == 3) {
			$cust_id = $this->_request->getParam("cust_id");

			$check_grup_bumn = $this->_db->select()
				->from("M_CUSTOMER")
				->where("CUST_STATUS = 1")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();


			$check_charges_bg = $this->_db->select()
				->from("M_CHARGES_BG")
				->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'INSBUMN' : 'INSNONBUMN') . '%')
				->where("CHARGES_NAME LIKE ?", '' . 'ASURANSI ' . $get_offer_type . '%')
				->query()->fetchAll();


			$check_lf_detail = $this->_db->select()
				->from("M_CUST_LINEFACILITY_DETAIL")
				->where("OFFER_TYPE = ?", $get_offer_type)
				//->where("FLAG = 1")
				->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();
			$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($check_lf_detail[0]);
	}
}
