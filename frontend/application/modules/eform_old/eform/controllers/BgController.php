	<?php
    require_once 'Zend/Controller/Action.php';
    require_once 'General/CustomerUser.php';
    require_once 'SGO/Helper/AES.php';
    require_once 'General/Settings.php';
    require_once 'Crypt/AESMYSQL.php';
    require_once("Service/Account.php");

    class eform_BgController extends Application_Main
    {

        protected $_moduleDB = 'RTF'; // masih harus diganti

        public function indexAction()
        {
            $this->_helper->_layout->setLayout('newlayout');

            // Zend_Session::namespaceUnset('eform');

            $settings = new Settings();
            $system_type = $settings->getSetting('system_type');
            $this->view->systemType = $system_type;

            $sessionNamespace = new Zend_Session_Namespace('eform');
            $old_data = $sessionNamespace->content;
            $this->view->old_data = $old_data;

            // echo "<pre>";
            // print_r($old_data["othersAttachment"]);
            // echo "</pre>";
            // die();

            $Settings = new Settings();
            $claim_period = $Settings->getSetting('max_claim_period');

            $toc_ind = $Settings->getSetting('ftemplate_bg_ind');
            $this->view->toc_ind = $toc_ind;
            $toc_eng = $Settings->getSetting('ftemplate_bg_eng');
            $this->view->toc_eng = $toc_eng;

            $conf = Zend_Registry::get('config');
            $this->_bankName = $conf['app']['bankname'];
            $this->view->masterbankname = $this->_bankName;


            //$this->view->insurance_amount = '125000000';
            //$this->view->paDateStart = '2021-11-15';
            //$this->view->paDateEnd = '2021-11-30';

            //BG Document Type
            $bgdocType         = $conf["bgdoc"]["type"]["desc"];
            $bgdocCode         = $conf["bgdoc"]["type"]["code"];

            $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

            $this->view->arrbgdoc = $arrbgdoc;

            // $claim_period = $Settings->getSetting('claim_period');
            $this->view->BG_CLAIM_PERIOD = $claim_period;

            //BG Counter Guarantee Type
            $bgcgType         = $conf["bgcg"]["type"]["desc"];
            $bgcgCode         = $conf["bgcg"]["type"]["code"];

            $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

            $this->view->arrbgcg = $arrbgcg;


            // get cash collateral

            $get_cash_collateral = $this->_db->select()
                ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
                ->where("CUST_ID = ?", "GLOBAL")
                ->where("CHARGES_TYPE = ?", "10")
                ->query()->fetchAll();

            $this->view->cash_collateral = $get_cash_collateral[0];

            // end get cash collateral


            $get_ccy_exist = $this->_db->select()
                ->from("M_MINAMT_CCY", ["CCY_ID"])
                ->query()->fetchAll();

            $this->view->ccy_exist = $get_ccy_exist;

             // get linefacillity
				$paramLimit = array();
				
				$paramLimit['CUST_ID'] =  $this->_custIdLogin;
				$paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
				$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
				

				$this->view->current_limit = $getLineFacility['currentLimit'];
				$this->view->max_limit =  $getLineFacility['plafondLimit'];

				//$this->view->linefacility = $get_linefacility[0];

				// end get linefacility

            // BG Publishing Form
            $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
            $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

            $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
            unset($arrbgpublish[3]);

            $this->view->arrbgpublish = $arrbgpublish;

            // BG TYPE
            $bgType         = $conf["bg"]["type"]["desc"];
            $bgCode         = $conf["bg"]["type"]["code"];

            $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

            $arrbgType["F4299"] = ["Jaminan Pembayaran", "Jaminan Pemeliharaan", "Lainnya (SP2D)"];

            $this->view->arrbgType = $arrbgType;

            $selectbranch = $this->_db->select()
                ->from(array('A' => 'M_BRANCH'), array('*'))
                ->query()->fetchAll();
            $arrbranch = array('' => '--Select Branch--');
            foreach ($selectbranch as $vl) {
                $arrbranch[$vl['BRANCH_CODE']] = $vl['BRANCH_NAME'];
            }
            $this->view->branchArr = $arrbranch;

            $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

            $getInsuranceCustomer = $this->_db->select()
                ->from(["A" => "M_CUSTOMER"], ["CUST_ID", "CUST_NAME"])
                ->joinLeft(["MCLF" => "M_CUST_LINEFACILITY"], "MCLF.CUST_ID = A.CUST_ID")
                ->where("A.CUST_MODEL = '2'")
                ->where("A.CUST_STATUS = '1'")
                ->where("MCLF.CUST_SEGMENT = ?", "4")
                ->where("MCLF.STATUS = ?", "1")
                ->query()->fetchAll();

            $simpanInsurance = [];

            $simpanInsurance[0] = ["key" => "-", "value" => "-- Choose one --"];

            foreach ($getInsuranceCustomer as $key => $customer) {
                $getId = $customer["CUST_ID"];
                $getName = $customer["CUST_NAME"];
                if ($customer["TICKET_SIZE"] == "1") {
                    $ticket_size = floatval((10 / 100) * $customer["PLAFOND_LIMIT"]);
                } else {
                    $ticket_size = floatval($customer["PLAFOND_LIMIT"]);
                }
                array_push($simpanInsurance, ["key" => $getId, "value" => $getName, "ticket_size" => $ticket_size]);
            }

            $this->view->simpanInsurance = $simpanInsurance;

            $selectcomp = $this->_db->select()
                ->from(array('A' => 'M_CUSTOMER'), array('*'))
                //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                ->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->query()->fetchAll();

            $this->view->compinfo = $selectcomp[0];

            $this->view->owner1 = $selectcomp[0]["CUST_NAME"];

            $select = $this->_db->select()
                ->from(array('M_PRELIMINARY'), array('LAST_APPROVED', 'LAST_APPROVEDBY'))
                ->where('CUST_ID = ?', $this->_custIdLogin);
            $preliminary = $this->_db->fetchRow($select);

            if (empty($preliminary)) {
                $this->view->disableChecklist = true;
            }

            $select_cust_linefacility = $this->_db->select()
                ->from(
                    array('A' => 'M_CUST_LINEFACILITY'),
                    array('*')
                )
                ->joinLeft(
                    array('B' => 'M_CUSTOMER'),
                    'B.CUST_ID = A.CUST_ID',
                    array('CUST_NAME', 'COLLECTIBILITY_CODE')
                )
                ->where("A.CUST_ID = ?", $this->_custIdLogin);
            //->where();
            // $select_cust_linefacility->where("DATE(NOW()) between DATE(A.PKS_START_DATE) AND DATE(A.PKS_EXP_DATE)");
            // ->where("A.STATUS in (2,3,4)");
            //echo $select_cust_linefacility;
            $select_cust_linefacility = $this->_db->fetchRow($select_cust_linefacility);

            if (!empty($select_cust_linefacility)) {
                $this->view->statusLF = $select_cust_linefacility["STATUS"];
            }

            $this->view->lastApproved = date('d M Y', strtotime($preliminary['LAST_APPROVED']));
            $this->view->lastApprovedBy = $preliminary['LAST_APPROVEDBY'];
            /* 
        $complist = $this->_db->fetchAll(
            $this->_db->select()
                 ->from(array('A' => 'M_USER'),array('CUST_ID'))
               
                 ->where("A.USER_ID = ? ", $this->_userIdLogin)
        );  
        // echo $complist;;die;
        // var_dump($complist);die;
        $comp = "'";
        // print_r($complist);die;
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";
        */
            $comp = "'" . $this->_custIdLogin . "'";

            $selectcomp = $this->_db->select()
                ->from('M_CUSTOMER', array('value' => new Zend_Db_Expr("CONCAT(CUST_NAME, ' - ', CUST_ID)"), 'CUST_NAME', 'CUST_ID', 'CUST_TYPE'))
                // ->where('CUST_NAME = ? ',$tblName)
                ->where('CUST_STATUS = 1 ');

            $tempColumn = $this->_db->fetchAll($selectcomp);

            $this->view->custarr = json_encode($tempColumn);


            //if($system_type == '2'){

            $acctlist = $this->_db->fetchAll(
                $this->_db->select()
                    ->from(array('A' => 'M_APIKEY'))
                    ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                    ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                    // ->where('A.ACCT_STATUS = ?','5')
                    ->where("A.CUST_ID IN (" . $comp . ")")
                    ->order('A.APIKEY_ID ASC')
                // echo $acctlist;
            );

            // echo "<pre>";
            // var_dump($acctlist);

            $account = array();
            foreach ($acctlist as $key => $value) {
                $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
            }
            // echo "<pre>";
            // var_dump($account);die;
            // $acct = array();
            $i = 0;
            foreach ($account as $key => $value) {

                $acct[$i]['ACCT_NO'] = $value['account_number'];
                $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                $acct[$i]['ACCT_NAME'] = $value['account_name'];
                $i++;
            }

            //$this->view->sourceAcct = $acct;


            //}else{

            $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

            //if ($type == 1) {
            //  $AccArr = $CustomerUser->getAccounts();
            //} else {

            // $type = array('10', '20');
            $type = array('D', 'T', '20', '30', 'S', '10');
            $param = array('ACCT_TYPE' => $type);
            //var_dump($param);die;
            //$param = array('ACCT_TYPE' => "20");
            $AccArr = $CustomerUser->getAccountsBG($param);
            $AccArr2 = [];

            $onlyIdr = array_keys(array_column($AccArr, "CCY_ID"), "IDR");
            //}
            if (!empty($AccArr)) {
                foreach ($AccArr as $i => $value) {
                    if (array_search($i, $onlyIdr)) {
                        $AccArr2[$i] = $value;
                        $AccArr2[$i]["ACCT_BANK"] = $this->_bankName;
                    }

                    $AccArr[$i]['ACCT_BANK'] = $this->_bankName;
                }
            }

            $this->view->AccArr = $AccArr;
            $type = array('D', 'S', '20', '10');
            $param = array('ACCT_TYPE' => $type);
            //var_dump($param);die;
            //$param = array('ACCT_TYPE' => "20");
            $AccArr2 = $CustomerUser->getAccountsBG($param);
            $this->view->AccArr2 = $AccArr2;

            //usd
            $paramUSD = array('CCY_IN' => 'USD');
            $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
            //}
            if (!empty($AccArrUSD)) {
                foreach ($AccArrUSD as $iUSD => $valueUSD) {

                    $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                }
            }
            //echo '<pre>';
            //var_dump($AccArr);die;
            $this->view->AccArrUSD = $AccArrUSD;
            //}

            $maxFileSize = "1024"; //"1024000"; //1024
            //~ $fileExt = "csv,dbf";
            $fileExt = "jpeg,jpg,pdf,text,plain";

            // validasi limit
            $select2 = $this->_db->select()
                ->from(array('BG' => 'T_BANK_GUARANTEE'), array('*'))
                ->join(array('C' => 'M_CUSTOMER'), 'BG.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'))
                ->order('BG_UPDATED DESC');
            $select2->where('BG.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));

            $bglist = $this->_db->fetchAll($select2);

            $usedbg = 0;
            foreach ($bglist as $vl) {
                if ($vl['BG_STATUS'] == '5' && $vl['COUNTER_WARRANTY_TYPE'] == '2') {
                    $usedbg = $usedbg + $vl['BG_AMOUNT'];
                }
            }
            $this->view->bgused = Application_Helper_General::displayMoney($usedbg);
            $this->view->bglist = $bglist;

            $select3 = $this->_db->select()
                ->from(array('C' => 'M_CUSTOMER'), array('*'));
            $select3->where('C.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
            $bglimit = $this->_db->fetchRow($select3);

            //foreach($bglimit as $val){
            if ($bglimit['CUST_BG_LIMIT'] != NULL) {
                $this->view->bglimit = Application_Helper_General::displayMoney($bglimit['CUST_BG_LIMIT']);
            } else {
                $this->view->bglimit = 0;
            }
            //}

            // end validasi limit

            $getMaxLimitCalendar = $this->_db->select()
                ->from("M_SETTING")
                ->where("SETTING_ID = ?", "max_claim_period")
                ->query()->fetchAll();

            $this->view->maxCalendarLimit = $getMaxLimitCalendar[0]["SETTING_VALUE"];

            // preliminary Member
            $selectMP = $this->_db->select()
                ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
            $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));

            $preliminaryMemberArr = $this->_db->fetchAll($selectMP);

            if (empty($preliminaryMemberArr)) {
                $this->view->disableChecklist = true;
            }

            $this->view->preliminaryMemberArr = $preliminaryMemberArr;

            if ($this->_request->isPost()) {
                $datadump = $this->_request->getParams();
                $newamount = str_replace(',', '', $datadump['bank_amount']);

                // if ($datadump['currency_type'] == '1') {
                //     $selectbankamount = $this->_db->select()
                //         ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                //         ->where('CCY_ID = ?', 'IDR');
                // } else {
                //     $selectbankamount = $this->_db->select()
                //         ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                //         ->where('CCY_ID = ?', 'USD');
                // }

                $selectbankamount = $this->_db->select()
                    ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                    ->where('CCY_ID = ?', $datadump["currency_type"]);
                $databankamount = $this->_db->fetchRow($selectbankamount);

                $filters = array(
                    'subject'        => array('StripTags', 'StringTrim'),
                    'CUST_CP'        => array('StripTags', 'StringTrim'),
                    'CUST_CONTACT_NUMBER'        => array('StripTags', 'StringTrim'),
                    'CUST_EMAIL'        => array('StripTags', 'StringTrim'),
                    'bank_amount'        => array('StripTags', 'StringTrim'),
                    'updatedate'        => array('StripTags', 'StringTrim'),
                    'BG_CLAIM_PERIOD'        => array('StripTags', 'StringTrim'),
                    'recipent_name'        => array('StripTags', 'StringTrim'),
                    'address'        => array('StripTags', 'StringTrim'),
                    'city'        => array('StripTags', 'StringTrim'),
                    'RECIPIENT_OFFICE_NUMBER'        => array('StripTags', 'StringTrim'),
                    'RECIPIENT_CP'        => array('StripTags', 'StringTrim'),
                    'RECIPIENT_CONTACT'        => array('StripTags', 'StringTrim'),
                    'RECIPIENT_EMAIL'        => array('StripTags', 'StringTrim'),
                    'SERVICE'        => array('StripTags', 'StringTrim'),
                    'GT_DOC_TYPE'        => array('StripTags', 'StringTrim'),
                    'GT_DOC_NUMBER'        => array('StripTags', 'StringTrim'),
                    'GT_DOC_DATE'        => array('StripTags', 'StringTrim'),
                    'BG_PUBLISH'        => array('StripTags', 'StringTrim'),
                    'BG_PURPOSE'        => array('StripTags', 'StringTrim'),
                    'warranty_type' => array('StripTags', 'StringTrim'),
                );

                if ($newamount < $databankamount['MIN_TX_AMT']) {
                    $error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Amount cannot be lower than ') . $databankamount['MIN_TX_AMT'] . '.';
                    $this->view->error      = true;
                    $this->view->error_msg = $error_msg;

                    $validators =  array(
                        'subject'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 120)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'branch'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 120)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'warranty_type' => array(
                            'NotEmpty',
                            'messages' => array(
                                'Can not be empty',
                            )
                        ),
                        'CUST_CP'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'CUST_CONTACT_NUMBER'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'CUST_EMAIL'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'bank_amount'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'updatedate'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_CLAIM_PERIOD'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'recipent_name'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'address'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 120)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'city'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'RECIPIENT_OFFICE_NUMBER'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'RECIPIENT_CP'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'RECIPIENT_CONTACT'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'RECIPIENT_EMAIL'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'SERVICE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'GT_DOC_TYPE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'GT_DOC_NUMBER'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'GT_DOC_DATE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_PUBLISH'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_PURPOSE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),

                    );

                    $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

                    foreach ($validators as $key => $value) {
                        $this->view->$key = ($zf_filter_input->isValid($key)) ? $zf_filter_input->getEscaped($key) : $this->_getParam($key);
                    }
                } else {

                    $validators =  array(
                        'subject'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 120)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'branch'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 120)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'warranty_type' => array(
                            'NotEmpty',
                            'messages' => array(
                                'Can not be empty',
                            )
                        ),
                        'CUST_CP'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'CUST_CONTACT_NUMBER'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'CUST_EMAIL'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'bank_amount'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'updatedate'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_CLAIM_PERIOD'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'recipent_name'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'address'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 120)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'city'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'RECIPIENT_OFFICE_NUMBER'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'RECIPIENT_CP'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'RECIPIENT_CONTACT'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'RECIPIENT_EMAIL'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'SERVICE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'GT_DOC_TYPE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'GT_DOC_NUMBER'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'GT_DOC_DATE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_PUBLISH'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_PURPOSE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),

                    );

                    $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

                    if ($zf_filter_input->isValid()) {
                        $attahmentDestination   = UPLOAD_PATH . '/document/submit/';
                        $errorRemark            = null;
                        $adapter                = new Zend_File_Transfer_Adapter_Http();
                        $files = $adapter->getFileInfo();

                        $datas = $this->_request->getParams();
                        $sourceFileName = trim($adapter->getFileName("uploadSource"));

                        if ($sourceFileName == null) {
                            //die('here');
                            $sourceFileName = null;
                            $fileType = null;
                        } else {

                            $sourceFileName = trim(substr(basename($adapter->getFileName("uploadSource")), 0));
                            //echo '<pre>';

                            if ($_FILES["uploadSource"]["type"]) {

                                $adapter->setDestination($attahmentDestination, "uploadSource");
                                //echo "<pre>";
                                //var_dump($attahmentDestination);
                                //echo $adapter->getFileName();
                                $files = $adapter->getFileInfo();
                                //var_dump($files);
                                //foreach ($files as $file) {
                                // Print the MIME Type for $file
                                $fileType = $_FILES["uploadSource"]['type'];

                                //}      
                                //die('b');
                                //$fileType               = $adapter->getMimeType();

                                $size                   = $_FILES["document"]["size"];
                            } else {
                                // die('a');
                                $fileType = null;
                                $size = null;
                            }
                            /*
                        if($_FILES["uploadSource2"]["type"])
                        {
                            $adapter->setDestination($attahmentDestination);
                        //$fileType2               = $adapter->getMimeType();
                        $files = $adapter->getFileInfo();
                        foreach ($files as $file) {
                            // Print the MIME Type for $file
                            $fileType2 == $file['type'];

                            }      

                        $size2                   = $_FILES["document"]["size"];
                        }
                        else{
                            $fileType2 = null;
                            $size2 = null;
                        }
                        */
                        }

                        $paramsName['sourceFileName']   = $sourceFileName;
                        // print_r($fileType);
                        $fileTypeMessage = explode('/', $fileType);
                        //$fileTypeMessage2 = explode('/',$fileType2);



                        $othersAttachment = [];

                        foreach (explode(",", $datadump["save_others_attachment"]) as $key => $value) {
                            if (array_search($value, $old_data["othersAttachment"]) !== false && ($old_data["othersAttachment"] != NULL)) {
                                array_push($othersAttachment, $value);
                            }
                        }

                        foreach ($files as $file => $fileInfo) {
                            if ($file == "uploadSource") continue;

                            if ($file == "specialFormatFile") {
                                if ($_FILES["specialFormatFile"]["size"] == NULL) {
                                    $datas["specialFormatFile"] = $old_data["specialFormatFile"];
                                }
                                $sizeFile = $fileInfo['size'] + 0;
                                if ($sizeFile == 0) continue;

                                $fileName = $fileInfo["name"];
                                $adapter->setDestination($attahmentDestination);

                                $date = date("dmy");
                                $time = date("his");

                                $newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $fileName;

                                $adapter->addFilter('Rename', $newFileName2, $file);
                                $adapter->receive($file);
                                $datas["specialFormatFile"] = $newFileName2;
                                continue;
                            }

                            $sizeFile = $fileInfo['size'] + 0;
                            if ($sizeFile == 0) continue;

                            $fileName = $fileInfo["name"];

                            $adapter->setDestination($attahmentDestination);

                            $date = date("dmy");
                            $time = date("his");

                            $newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $fileName;

                            $adapter->addFilter('Rename', $newFileName2, $file);
                            $adapter->receive($file);
                            array_push($othersAttachment, $newFileName2);
                        }

                        $datas["othersAttachment"] = $othersAttachment;

                        if ($_FILES["uploadSource"]["size"] == NULL) {
                            $datas['fileName'] = $old_data["fileName"];
                        }

                        //var_dump($fileTypeMessage[1]);die;
                        if (!empty($fileTypeMessage[1])) {
                            $fileType =  $fileTypeMessage[1];
                            //$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                            //$extensionValidator->setMessage("Extension file must be *.jpg /.jpeg / .pdf");

                            $size           = number_format($size);
                            $sizeTampil     = $size / 1000;
                            //$sizeValidator    = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                            // $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");

                            //$adapter->setValidators(array($extensionValidator));


                            if ($adapter->isValid("uploadSource")) {
                                $date = date("dmy");
                                $time = date("his");

                                $newFileName = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . $sourceFileName;
                                $adapter->addFilter('Rename', $newFileName, "uploadSource");

                                if ($adapter->receive("uploadSource")) {

                                    $filedata = $attahmentDestination . $newFileName;
                                    // var_dump($filedata);
                                    if (file_exists($filedata)) {
                                        //exec("clamdscan --fdpass " . $filedata, $out, $int);

                                        // if()
                                        // var_dump($int);
                                        //var_dump($datas);
                                        //die();

                                        /*if ($int == 1) {

                                            unlink($filedata);
                                            //die('here');
                                            $this->view->error = true;
                                            $errors = array(array('FileType' => 'Error: An infected file. Please upload other file'));
                                            $this->view->errorMsg = $errors[0]["FileType"];
                                        } else {
										*/
                                            $datas['fileName'] = $newFileName;
                                            $sessionNamespace = new Zend_Session_Namespace('eform');
                                            $sessionNamespace->content = $datas;
                                            $this->_redirect('/eform/bg/confirm');
                                        /*}*/
                                    }
                                }
                            } else {
                                $errors = array($adapter->getMessages());
                                //var_dump($errors);die;
                                $this->view->error = true;
                                $errors = $this->language->_('Extension file must be *.jpg /.jpeg / .pdf');
                                $this->view->errorMsg = $errors;
                            }
                        } else {

                            $sessionNamespace = new Zend_Session_Namespace('eform');
                            $sessionNamespace->content = $datas;
                            $this->_redirect('/eform/bg/confirm');
                        }
                    } else {
                        $this->view->error = true;

                        $error = $zf_filter_input->getMessages();
                        $save_valid = [];

                        $all_value = ["subject", "flname", "warranty_type", "branch", "CUST_CP", "CUST_CONTACT_NUMBER", "CUST_EMAIL", "bank_amount", "updatedate", "recipent_name", "address", "city", "RECIPIENT_OFFICE_NUMBER", "RECIPIENT_CP", "RECIPIENT_CONTACT", "RECIPIENT_EMAIL", "SERVICE", "GT_DOC_TYPE", "GT_DOC_NUMBER", "GT_DOC_DATE", "BG_PUBLISH", "BG_PURPOSE"];

                        foreach ($all_value as $value) {
                            if ($value == "branch") {
                                $save_valid[$value] = $this->_request->getParam('branch');
                            } else if ($value == "warranty_type") {

                                if ($zf_filter_input->$value == 1) {

                                    $save_valid[$value] = [
                                        "warranty_type" => $zf_filter_input->$value,
                                        "flselect_own" => $this->_request->getParam('flselect_own'),
                                        "flname" => $this->_request->getParam('flname'),
                                        "type_desc" => $this->_request->getParam('type_desc'),
                                        "currency" => $this->_request->getParam('currency'),
                                        "flamount" => $this->_request->getParam('flamount'),
                                    ];
                                }
                            } else {
                                if (!isset($error[$value])) {
                                    $save_valid[$value] = $zf_filter_input->$value;
                                }
                            }
                        }

                        $this->view->valid = $save_valid;

                        //format error utk ditampilkan di view html 
                        $errorArray = null;
                        foreach ($error as $keyRoot => $rowError) {
                            foreach ($rowError as $errorString) {
                                $errorArray[$keyRoot] = $errorString;
                            }
                        }

                        $this->view->report_msg = $errorArray;
                    }
                }
            }
            // $paramac = array(
            //  'CCY_IN' => 'IDR'
            // );
            // $AccArr                  = $this->CustomerUser->getAccounts($paramac);
            // $this->view->AccArr      = $AccArr;

        }



        public function confirmAction()
        {
            $this->_helper->_layout->setLayout('newlayout');

            $settings = new Settings();
            $system_type = $settings->getSetting('system_type');
            $this->view->systemType = $system_type;

            $conf = Zend_Registry::get('config');
            $this->_bankName = $conf['app']['bankname'];
            $this->view->masterbankname = $this->_bankName;


            $selectcomp = $this->_db->select()
                ->from(array('A' => 'M_CUSTOMER'), array('*'))
                //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                ->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->query()->fetchAll();

            $this->view->compinfo = $selectcomp[0];

            $sessionNamespace = new Zend_Session_Namespace('eform');
            $data = $sessionNamespace->content;

            $insuranceBranch = $this->_db->select()
                ->from("M_INS_BRANCH")
                ->where("INS_BRANCH_CODE = ?", $data["insuranceBranch"])
                ->query()->fetchAll();

            $this->view->insurance_branch = $insuranceBranch[0]["INS_BRANCH_NAME"];

            $this->view->data = $data;

            $this->view->othersAttachment = $data["othersAttachment"];
            $this->view->warrantyType = $data['warranty_type'];

            //echo '<pre>'; 
            //var_dump($data);
            $this->view->subject = $data['subject'];
            if ($data['warranty_type'] == '1') {
                if (!empty($data['flselect_account_manual'])) {
                    $splitArr = array();
                    foreach ($data['flselect_account_manual'] as $ky => $vl) {
                        if ($vl == '') {
                            $nameAcct = explode('|', $data['flselect_account_number'][$ky]);

                            $splitArr[$ky]['acct'] = $nameAcct[0];
                        } else {
                            $splitArr[$ky]['acct'] = $vl;
                        }

                        if ($data['flname_manual'][$ky] == '') {
                            $splitArr[$ky]['acct_name'] = $data['flname'][$ky];
                        } else {
                            $splitArr[$ky]['acct_name'] = $data['flname_manual'][$ky];
                        }

                        $splitArr[$ky]['amount'] = $data['flamount'][$ky];
                        $splitArr[$ky]['flag'] = $data['flselect_own'][$ky];
                        $splitArr[$ky]['bank_code'] = '999';
                    }
                    //die(count($splitArr));
                    //echo count($splitArr);
                    foreach ($splitArr as $key => $value) {
                        $temp_save = $this->_db->select()
                            ->from("M_CUSTOMER_ACCT")
                            ->where("ACCT_NO = ?", $value["acct"])
                            ->query()->fetchAll();



                        $splitArr[$key]["currency"] = $data['currency'][$ky]; //'IDR';//$temp_save[0]["CCY_ID"];
                        $splitArr[$key]["type"] = $data['type_desc'][$ky]; //$temp_save[0]["ACCT_DESC"];

                        //var_dump($core);
                        //echo $core['currency'];
                        //die;



                    }
                    $this->view->acctSplit = $splitArr;
                }
            }

            $selectbranch = $this->_db->select()
                ->from(array('A' => 'M_BRANCH'), array('*'))
                ->where('A.BRANCH_CODE = ?', $data['branch'])
                ->query()->fetchAll();
            //var_dump($selectbranch[0]['BRANCH_NAME']);die;
            $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];


            // $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


            // // $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['acct']);
            // $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['acct'], 'ACCT_TYPE' => array('T', 'D'));
            // $AccArr = $CustomerUser->getAccounts($param);

            $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
            $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
            $AccArr = $CustomerUser->getAccountsBG($param);

            // print_r($AccArr['0']['ACCT_NAME']);
            // print_r($AccArr);
            // die();

            if (!empty($AccArr)) {
                $this->view->src_name = $AccArr['0']['ACCT_NAME'];
            }

            if (!empty($data['chkpurpose'])) {
                foreach ($data['chkpurpose'] as $key => $val) {
                    $str = 'checkp' . $val;
                    //var_dump($str);
                    $this->view->$str =  'checked';
                }
            }

            foreach ($data as $key => $val) {

                if ($key != 'language') {
                    $this->view->$key = $val;
                }
            }

            $this->view->updateStart = Application_Helper_General::convertDate($data['updatedate'][0], $this->view->viewDateFormat, $this->view->defaultDateFormat);
            $this->view->updateEnd = Application_Helper_General::convertDate($data['updatedate'][1], $this->view->viewDateFormat, $this->view->defaultDateFormat);

            $this->view->paDateStart = Application_Helper_General::convertDate($data['paDate'][0], $this->view->viewDateFormat, $this->view->defaultDateFormat);
            $this->view->paDateEnd = Application_Helper_General::convertDate($data['paDate'][1], $this->view->viewDateFormat, $this->view->defaultDateFormat);

            $Settings = new Settings();

            $this->view->bgclaimdate = date('Y-m-d', strtotime('+' . $Settings->getSetting('max_claim_period') . ' days', strtotime($data['updatedate'][1])));

            $arrBankFormat = array(
                1 => 'Bank Standard',
                2 => 'Special Format (with bank approval)'
            );

            $this->view->bankFormat = $arrBankFormat[$data['bankFormat']];

            $arrLang = array(
                1 => 'Indonesian',
                2 => 'English',
                3 => 'Billingual',
            );

            $this->view->languagetext = $arrLang[$data['language']];

            $arrWaranty = array(
                1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                //2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                2 => 'Bank Guarantee Line Facility',
                3 => 'Insurance'

            );

            //$this->view->warranty_type_text = $arrWaranty[$data['warranty_type']];
            //echo '<pre>';
            //var_dump($data);

            //BG Document Type
            $bgdocType         = $conf["bgdoc"]["type"]["desc"];
            $bgdocCode         = $conf["bgdoc"]["type"]["code"];

            $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

            $this->view->GT_DOC_DESC = $arrbgdoc[$data['GT_DOC_TYPE']];

            //BG Counter Guarantee Type
            $bgcgType         = $conf["bgcg"]["type"]["desc"];
            $bgcgCode         = $conf["bgcg"]["type"]["code"];

            $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

            $this->view->warranty_type_text = $arrbgcg[$data['warranty_type']];

            // BG Publishing Form
            $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
            $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

            $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

            $this->view->BG_PUBLISH_DESC = $arrbgpublish[$data['BG_PUBLISH']];

            // BG TYPE
            $bgType         = $conf["bg"]["type"]["desc"];
            $bgCode         = $conf["bg"]["type"]["code"];

            $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

            $this->view->BG_PURPOSE_DESC = $arrbgType[$data['BG_PURPOSE']];
            $this->view->BG_PURPOSE_OTHERS = $data['others'];

            if ($data['currency_type'] == "1") {

                if ($data['select_own'] == "1") {
                    $select_account_number = $data['flselect_account_number'];
                    $this->view->jml_account_number = count($data['flselect_account_number']);
                    for ($i = 0; $i < count($data['flselect_account_number']); $i++) {
                        $arr_select_account_number = explode("|", $select_account_number[$i]);

                        $account_number = 'account_number' . $i;
                        $account_name = 'account_name' . $i;

                        $this->view->$account_number = $arr_select_account_number[0];
                        $this->view->$account_name = $arr_select_account_number[1];
                        $amount = 'amount' . $i;
                        $this->view->$amount = $data['flamount'][$i];
                        $this->view->currency_type = "IDR";
                    }
                } else {
                    $select_account_number = $data['flselect_account_manual'];
                    $flname_manual = $data['flname_manual'];
                    $this->view->jml_account_number = count($data['flselect_account_manual']);
                    for ($i = 0; $i < count($data['flselect_account_manual']); $i++) {

                        $account_number = 'account_number' . $i;
                        $account_name = 'account_name' . $i;
                        $this->view->$account_number =  $select_account_number[$i];
                        $this->view->$account_name = $flname_manual[$i];
                        $amount = 'amount' . $i;
                        $this->view->$amount = $data['flamount'][$i];
                        $this->view->currency_type = "IDR";
                    }
                }

                $this->view->acct = $data['acct'];
            } elseif ($data['currency_type'] == "2") {
                $select_account_numberUSD = $data['select_account_numberUSD'];
                $arr_account_numberUSD = explode("|", $select_account_numberUSD);
                $this->view->account_number = $arr_account_numberUSD[0];
                $this->view->account_name = $arr_account_numberUSD[1];
                $this->view->acct = $data['acctUSD'];
                $this->view->currency_type = "USD";
            }

             // get linefacillity
				$paramLimit = array();
				
				$paramLimit['CUST_ID'] =  $this->_custIdLogin;
				$paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
				$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
				

				$this->view->current_limit = $getLineFacility['currentLimit'];
				$this->view->max_limit =  $getLineFacility['plafondLimit'];

				//$this->view->linefacility = $get_linefacility[0];

				// end get linefacility

            $download = $this->_getParam('download');
            $BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
            if ($BG_APPROVE_DOC) {
                $attahmentDestination = UPLOAD_PATH . '/document/submit/';

                $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['BG_APPROVE_DOC']);
            }
            //print_r($edit);die;
            if ($download == 1) {
                $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                $this->_helper->download->file($data['fileName'], $attahmentDestination . $data['fileName']);
            }

            if ($download == "specialformatfile") {
                $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                $this->_helper->download->file($data['specialFormatFile'], $attahmentDestination . $data['specialFormatFile']);
            }

            if ($download > 1) {
                $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                $this->_helper->download->file($data['othersAttachment'][$download - 2], $attahmentDestination . $data['othersAttachment'][$download - 2]);
            }

            if ($data['warranty_type'] == '3') {
                $getInsuranceCustomer = $this->_db->select()
                    ->from(["A" => "M_CUSTOMER"], ["CUST_ID", "CUST_NAME"])
                    ->where("CUST_ID = '" . $data["insuranceName"] . "'")
                    ->query()->fetchAll();

                $this->view->insuranceName = $getInsuranceCustomer[0]["CUST_NAME"];
            } else {
                $this->view->insuranceName = "";
            }

            if ($this->_request->isPost()) {

                $settings = new Settings();
                $max_ntu = $settings->getSetting('max_ntu_date');


                $usage = '';
                foreach ($data['chkpurpose'] as $val) {
                    $usage .= $val . ',';
                }
                $usage = substr($usage, 0, -1);

                try {
                    if ($data['bank_amount'] == '') {
                        $data['bank_amount'] = 0;
                    }

                    if ($data['amount'] == '') {
                        $data['amount'] = 0;
                    }

                    if ($data['currency_type'] == "IDR") {

                        if ($data['select_own'] == "1") {
                            $select_account_number = $data['flselect_account_number'];
                            $account_number = '';
                            $account_name = '';
                            $amount = '';
                            for ($i = 0; $i <= count($data['flselect_account_number']); $i++) {
                                $arr_select_account_number = explode("|", $select_account_number[$i]);
                                $account_number .= $arr_select_account_number[0] . ',';
                                $account_name .= $arr_select_account_number[1] . ',';
                                $amount .= $data['flamount'][$i] . ',';
                            }
                            $account_number = substr($account_number, 0, -1);
                            $account_name = substr($account_name, 0, -1);
                            $amount = substr($amount, 0, -1);
                        } else {
                            $select_account_number = $data['flselect_account_manual'];
                            $flname_manual = $data['flname_manual'];

                            $account_number = '';
                            $account_name = '';
                            $amount = '';
                            for ($i = 0; $i <= count($data['flselect_account_manual']); $i++) {
                                $account_number .= $select_account_number[$i] . ',';
                                $account_name .= $flname_manual[$i] . ',';
                                $amount .= $data['flamount'][$i] . ',';
                            }
                            $account_number = substr($account_number, 0, -1);
                            $account_name = substr($account_name, 0, -1);
                            $amount = substr($amount, 0, -1);
                        }

                        $acct = $data['acct'];
                    } elseif ($data['currency_type'] == "2") {
                        $select_account_numberUSD = $data['select_account_numberUSD'];
                        for ($i = 0; $i <= count($data['select_account_numberUSD']); $i++) {
                            $arr_account_numberUSD = explode("|", $select_account_numberUSD[$i]);
                            $account_number .= $arr_account_numberUSD[0] . ',';;
                            $account_name .= $arr_account_numberUSD[1] . ',';;
                        }

                        $acct = $data['acctUSD'];
                    }

                    $generateBgRegNumber = $this->generateTransactionID($data['branch'], $data['BG_PURPOSE']);



                    if ($data['warranty_type'] == '3') {
                        $insurance_code = $data['insuranceName'];
                    } else {
                        $insurance_code = "";
                    }
                    $this->_db->beginTransaction();
                    $specialFormat = "";
                    if ($data["bankFormat"] == 2) {
                        $specialFormat = $data['specialFormatFile'];
                    }
                    $BG_CLAIM_DATE = date('Y-m-d', strtotime('+' . $data['BG_CLAIM_PERIOD'] . ' days', strtotime($data['updatedate'][1])));
                    $insertArr = array(
                        'BG_REG_NUMBER' => $generateBgRegNumber, //$this->generateTransactionID($data['branch'],$data['BG_PURPOSE']),
                        'CUST_ID'               => $this->_custIdLogin,
                        'CUST_CP'               => $data['CUST_CP'],
                        'CUST_EMAIL'            => $data['CUST_EMAIL'],
                        'CUST_CONTACT_NUMBER'   => $data['CUST_CONTACT_NUMBER'],
                        'BG_STATUS'             => 2,
                        'BG_SUBJECT'            => $data['subject'],
                        'RECIPIENT_NAME'            => $data['recipent_name'],
                        'RECIPIENT_ADDRES'          => $data['address'],
                        'RECIPIENT_CITY'            => $data['city'],
                        'RECIPIENT_CP'              => $data['RECIPIENT_CP'],
                        'RECIPIENT_OFFICE_NUMBER'   => $data['RECIPIENT_OFFICE_NUMBER'],
                        'RECIPIENT_EMAIL'           => $data['RECIPIENT_EMAIL'],
                        'RECIPIENT_CONTACT'         => $data['RECIPIENT_CONTACT'],
                        'SERVICE'                   => $data['SERVICE'],
                        'GT_DOC_TYPE'               => $data['GT_DOC_TYPE'],
                        'GT_DOC_NUMBER'             => $data['GT_DOC_NUMBER'],
                        'GT_DOC_DATE'               => $data['GT_DOC_DATE'],
                        //'GUARANTEE_TRANSACTION'         => $data['comment'], //useless
                        'FILE'          => $data['fileName'],
                        'BG_AMOUNT'           => Application_Helper_General::convertDisplayMoney($data['bank_amount']),
                        'PROVISION_FEE' => Application_Helper_General::convertDisplayMoney($data["provision_fee_input"]),
                        'ADM_FEE' => Application_Helper_General::convertDisplayMoney($data["administration_fee_input"]),
                        'STAMP_FEE' => Application_Helper_General::convertDisplayMoney($data["stamp_fee_input"]),
                        'TIME_PERIOD_START'         => $data['updatedate'][0],
                        'TIME_PERIOD_END'           => $data['updatedate'][1],
                        // 'BG_CLAIM_PERIOD'       => $data['BG_CLAIM_PERIOD'],
                        'COUNTER_WARRANTY_TYPE'         => $data['warranty_type'],
                        'COUNTER_WARRANTY_ACCT_NO'          => rtrim($account_number, ","),
                        'COUNTER_WARRANTY_ACCT_NAME'            => rtrim($account_name, ","),
                        'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney(rtrim($amount, ",")),
                        'FEE_CHARGE_TO'         => $acct,
                        'BG_FORMAT'         => $data['bankFormat'],
                        'BG_LANGUAGE'           => $data['language'],
                        'USAGE_PURPOSE'         => $data['BG_PURPOSE'], //$usage,
                        'USAGE_PURPOSE_DESC'         => $data['purpose_desc'], //$usage,
                        'BG_CREATED'    => new Zend_Db_Expr('now()'),
                        'BG_CREATEDBY'      => $this->_userIdLogin,
                        'BG_INSURANCE_CODE'    => $insurance_code,
                        'BG_UNDERLYING_DOC' => $data['fileName'],
                        'BG_BRANCH'    => $data['branch'],
                        'BG_PUBLISH'    => $data['BG_PUBLISH'],
                        'SPECIAL_FORMAT_DOC' => $specialFormat,
                        'NTU_COUNTING' => $max_ntu,
                        'BG_CLAIM_DATE' => $BG_CLAIM_DATE
                    );

                    $this->_db->insert('TEMP_BANK_GUARANTEE', $insertArr);

                    // insert others document attachment to temp_bank_guarantee_file
                    if ($data["othersAttachment"]) {
                        foreach ($data["othersAttachment"] as $key => $value) {
                            $insertArr2 = [
                                "BG_REG_NUMBER" => $generateBgRegNumber,
                                "BG_FILE" => $value,
                                "INDEX" => $key
                            ];
                            $this->_db->insert('TEMP_BANK_GUARANTEE_FILE', $insertArr2);
                        }
                    }


                    if ($data['warranty_type'] == '1') {
                        foreach ($splitArr as $ky => $vl) {
                            $tmparrDetail = array(
                                'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                'ACCT' => $vl['acct'],
                                'BANK_CODE' =>  $vl['bank_code'],
                                'NAME' => $vl['acct_name'],
                                'AMOUNT' => Application_Helper_General::convertDisplayMoney(rtrim($vl['amount'], ",")),
                                'FLAG' => $vl['flag']
                            );
                            $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                        }
                    }


                    if ($data['warranty_type'] == '2') {
                        if (!empty($data['owner1'])) {
                            $arrDetail = array(
                                'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                'CUST_ID' => $insertArr['CUST_ID'],
                                'USER_ID' =>  $this->_userIdLogin,
                                'PS_FIELDNAME' => 'Plafond Owner',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['owner1']
                            );
                            $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                            $arrDetail = array(
                                'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                'CUST_ID' => $insertArr['CUST_ID'],
                                'USER_ID' =>  $this->_userIdLogin,
                                'PS_FIELDNAME' => 'Amount Owner',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['amountowner1']
                            );
                            $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);
                        }

                        if (!empty($data['owner2'])) {
                            $arrDetail = array(
                                'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                'CUST_ID' => $insertArr['CUST_ID'],
                                'USER_ID' =>  $this->_userIdLogin,
                                'PS_FIELDNAME' => 'Plafond Owner 2',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['owner2']
                            );
                            $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                            $arrDetail = array(
                                'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                'CUST_ID' => $insertArr['CUST_ID'],
                                'USER_ID' =>  $this->_userIdLogin,
                                'PS_FIELDNAME' => 'Amount Owner 2',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['amountowner2']
                            );
                            $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);
                        }

                        if (!empty($data['owner3'])) {
                            $arrDetail = array(
                                'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                'CUST_ID' => $insertArr['CUST_ID'],
                                'USER_ID' =>  $this->_userIdLogin,
                                'PS_FIELDNAME' => 'Plafond Owner 3',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['owner3']
                            );
                            $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                            $arrDetail = array(
                                'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                                'CUST_ID' => $insertArr['CUST_ID'],
                                'USER_ID' =>  $this->_userIdLogin,
                                'PS_FIELDNAME' => 'Amount Owner 3',
                                'PS_FIELDTYPE' => 1,
                                'PS_FIELDVALUE' => $data['amountowner3']
                            );
                            $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);
                        }
                    }

                    if ($data['warranty_type'] == '3') {
                        $arrDetail = array(
                            'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                            'CUST_ID' => $insertArr['CUST_ID'],
                            'USER_ID' =>  $this->_userIdLogin,
                            'PS_FIELDNAME' => 'Insurance Name',
                            'PS_FIELDTYPE' => 1,
                            'PS_FIELDVALUE' => $data['insuranceName']
                        );
                        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                        $arrDetail = array(
                            'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                            'CUST_ID' => $insertArr['CUST_ID'],
                            'USER_ID' =>  $this->_userIdLogin,
                            'PS_FIELDNAME' => 'Insurance Branch',
                            'PS_FIELDTYPE' => 1,
                            'PS_FIELDVALUE' => $data['insuranceBranch']
                        );
                        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                        // $arrDetail = array(
                        //     'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                        //     'CUST_ID' => $insertArr['CUST_ID'],
                        //     'USER_ID' =>  $this->_userIdLogin,
                        //     'PS_FIELDNAME' => 'Principal Agreement',
                        //     'PS_FIELDTYPE' => 1,
                        //     'PS_FIELDVALUE' => $data['PrincipalAgreement']
                        // );
                        // $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                        // $arrDetail = array(
                        //     'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                        //     'CUST_ID' => $insertArr['CUST_ID'],
                        //     'USER_ID' =>  $this->_userIdLogin,
                        //     'PS_FIELDNAME' => 'Principal Agreement Start Date',
                        //     'PS_FIELDTYPE' => 1,
                        //     'PS_FIELDVALUE' => $data['paDate'][0]
                        // );
                        // $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                        // $arrDetail = array(
                        //     'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                        //     'CUST_ID' => $insertArr['CUST_ID'],
                        //     'USER_ID' =>  $this->_userIdLogin,
                        //     'PS_FIELDNAME' => 'Principal Agreement End Date',
                        //     'PS_FIELDTYPE' => 1,
                        //     'PS_FIELDVALUE' => $data['paDate'][1]
                        // );
                        // $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);


                        // $arrDetail = array(
                        //     'BG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                        //     'CUST_ID' => $insertArr['CUST_ID'],
                        //     'USER_ID' =>  $this->_userIdLogin,
                        //     'PS_FIELDNAME' => 'Amount',
                        //     'PS_FIELDTYPE' => 1,
                        //     'PS_FIELDVALUE' => $data['insurance_amount']
                        // );
                        // $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                    }

                    // if ($data['currency_type'] == "1") {
                    //     $Currency = "IDR";
                    // } elseif ($data['currency_type'] == "2") {
                    //     $Currency = "USD";
                    // }

                    $arrDetail = array(
                        'BG_REG_NUMBER' => $insertArr['BG_REG_NUMBER'],
                        'CUST_ID' => $insertArr['CUST_ID'],
                        'USER_ID' =>  $this->_userIdLogin,
                        'PS_FIELDNAME' => 'Currency',
                        'PS_FIELDTYPE' => 1,
                        'PS_FIELDVALUE' => $data['currency_type']
                    );

                    $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                    $historyInsert = array(
                        'DATE_TIME'         => new Zend_Db_Expr("now()"),
                        'BG_REG_NUMBER'         => $insertArr['BG_REG_NUMBER'],
                        'CUST_ID'           => $this->_custIdLogin,
                        'USER_LOGIN'        => $this->_userIdLogin,
                        'HISTORY_STATUS'    => 1
                    );

                    $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                    $this->_db->commit();

                    //Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);
                    unset($sessionNamespace->content);
                    $this->setbackURL('/' . $this->_request->getModuleName() . '/bg');
                    $this->_redirect('/notification/success');
                } catch (Exception $e) {
                    // var_dump($e);
                    // die;
                    $this->_db->rollBack();
                    //Application_Log_GeneralLog::technicalLog($e);
                }
            }
        }

        /*public function generateTransactionID($branch,$bgtype){

        $currentDate = date("Ymd");
        $seqNumber   = strtoupper(Application_Helper_General::str_rand(4));
        $trxId = $seqNumber.$branch.'01'.$bgtype

        return $trxId;
    }*/

        public function generateTransactionID($branch, $bgtype)
        {

            $currentDate = date("Ymd");
            $seqNumber   = mt_rand(1111, 9999);
            $trxId = $seqNumber . $branch . '01' . $bgtype;

            return $trxId;
        }

        public function insurancebranchAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $cust_id = $this->_request->getParam("custid");
            $bg_reg_number = $this->_request->getParam("bg_reg_number");
			

            $select_ins_branch = $this->_db->select()
                ->from("M_INS_BRANCH")
                ->where("CUST_ID = ?", strval($cust_id))
                ->query()->fetchAll();

            $save_ins_branch = [];

            $save_ins_branch["-"] = "-- Choose one --";

            foreach ($select_ins_branch as $key => $value) {
                $save_ins_branch[$value["INS_BRANCH_CODE"]] = $value["INS_BRANCH_NAME"];
            }

			if(!empty($bg_reg_number)){
				
				 $selected_ins_branch = $this->_db->select()
                ->from("TEMP_BANK_GUARANTEE_DETAIL")
                ->where("BG_REG_NUMBER = ?", strval($bg_reg_number))
                ->where("PS_FIELDNAME = 'Insurance Branch'")
                ->query()->fetchAll();
				
			}
            foreach ($save_ins_branch as $key => $value) {
				if($selected_ins_branch[0]['PS_FIELDVALUE'] == $key){
					echo "<option value=\"$key\" selected='selected'>$value</option>";
				}else{
					echo "<option value=\"$key\">$value</option>";
				}
                
            }
        }

        public function insurancebranchcountAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $cust_id = $this->_request->getParam("custid");

            $dataDetail = $this->_db->select()
                ->from(
                    array('M_MARGINALDEPOSIT_DETAIL'),
                    array('*')
                )
                ->where('CUST_ID = ?', $cust_id)
                ->query()->fetchAll();

            $total_marginal = 0;
            foreach ($dataDetail as $row) {
                $acctNo  = $row['MD_ACCT'];
                $acctCcy = Application_Helper_General::getCurrNum($row['MD_ACCT_CCY']);

                $svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
                $result = $svcAccount->inquiryAccountBalance('AB', FALSE);
                //var_dump($result);die;
                if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
                    $currentGuarantee = (float)$result['balance_active'];
                    $dataUpdate  = ['GUARANTEE_AMOUNT' => $result['balance_active']];
                    $whereUpdate = ['MD_ACCT = ?' => $acctNo];

                    $total_marginal += $result['balance_active'];

                    //$this->_db->beginTransaction();
                    try {
                        //var_dump($dataUpdate);
                        //var_dump($whereUpdate);
                        $this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
                    } catch (Exception $error) {
                        //var_dump($error);die;
                        $this->_db->rollBack();
                        //echo '<pre>';
                        //print_r($error->getMessage());
                        //echo '</pre><br>';
                        //die;
                    }
                } else {

                    $svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
                    $result = $svcAccount->inquiryDeposito('AB', TRUE);
                    if ($result['response_code'] == '0000') {
                        $dataUpdate  = ['GUARANTEE_AMOUNT' => $result['balance']];
                        $whereUpdate = ['MD_ACCT = ?' => $acctNo];

                        $total_marginal += $result['balance_active'];

                        $this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
                    }
                }
            }

            // $select_ins_branch = $this->_db->select()
            //     ->from("M_INS_BRANCH")
            //     ->where("CUST_ID = ?", strval($cust_id))
            //     ->query()->fetchAll();



            $get_m_customer = $this->_db->select()
                ->from("M_CUSTOMER", ["CUST_STATUS"])
                ->where("CUST_ID = ?", $cust_id)
                ->query()->fetch();

            $get_linefacility = $this->_db->select()
                ->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT", "STATUS", "FEE_DEBITED", "DEBITED_ACCOUNT"])
                ->where("CUST_ID = ?", $cust_id)
                ->query()->fetchAll();

            if ($get_linefacility[0]["CUST_SEGMENT"] == "4") {
                $check_all_detail = $this->_db->select()
                    ->from("T_BANK_GUARANTEE_DETAIL")
                    ->where("PS_FIELDNAME = ?", "Insurance Name")
                    ->where("PS_FIELDVALUE = ?", $get_linefacility[0]["CUST_ID"])
                    ->query()->fetchAll();

                $total_bgamount_on_risk = 0;

                if (count($check_all_detail) > 0) {
                    $save_bg_reg_number = [];
                    foreach ($check_all_detail as $value) {
                        array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
                    }

                    $get_bgamount_on_risks = $this->_db->select()
                        ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
                        ->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
                        ->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
                        ->query()->fetchAll();

                    foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
                        $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
                    }
                }

                $check_all_detail = $this->_db->select()
                    ->from("TEMP_BANK_GUARANTEE_DETAIL")
                    ->where("PS_FIELDNAME = ?", "Insurance Name")
                    ->where("PS_FIELDVALUE = ?", $get_linefacility[0]["CUST_ID"])
                    ->query()->fetchAll();

                $total_bgamount_on_temp = 0;

                if (count($check_all_detail) > 0) {

                    $save_bg_reg_number = [];
                    foreach ($check_all_detail as $value) {
                        array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
                    }

                    $get_bgamount_on_temps = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
                        ->where("COUNTER_WARRANTY_TYPE = '3'")
                        ->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
                        ->where("BG_STATUS IN (?)", ["5", "6", "7", "14", "17", "20"])
                        ->query()->fetchAll();

                    foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
                        $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
                    }
                }

                $current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
            } else {
                $get_bgamount_on_risks = $this->_db->select()
                    ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
                    ->where("(BG_STATUS = 15 OR BG_STATUS = 16) AND COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($get_linefacility[0]["CUST_ID"]))
                    ->query()->fetchAll();


                $total_bgamount_on_risk = 0;

                foreach ($get_bgamount_on_risks as $key => $get_bgamount_on_risk) {
                    $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
                }

                $total_bgamount_on_temp = 0;

                $get_bgamount_on_temps = $this->_db->select()
                    ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
                    ->where("COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($get_linefacility[0]["CUST_ID"]))
                    ->query()->fetchAll();

                foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
                    $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
                }

                $current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
            }

            $result = json_encode([
                "current_limit" => $current_limit,
                "total_marginal" => $total_marginal,
                "cust_lf_status" => $get_linefacility[0]["STATUS"],
                "cust_status" => $get_m_customer["CUST_STATUS"],
                "fee_debited" => $get_linefacility[0]["FEE_DEBITED"],
                "debited_acct" => $get_linefacility[0]["DEBITED_ACCOUNT"],
            ]);
            header('Content-Type: application/json');
            echo $result;
        }

        public function marginaldepsoitcheckAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $cust_id = $this->_request->getParam("custid");

            $get_marginal_depsit_detail = $this->_db->select()
                ->from("M_MARGINALDEPOSIT_DETAIL")
                ->where("CUST_ID = ?", $cust_id)
                ->query()->fetchAll();

            $save_guarantee_amount = 0;
            foreach ($get_marginal_depsit_detail as $value) {
                $save_guarantee_amount += $value["GUARANTEE_AMOUNT"];
            }

            $result = json_encode(["marginal_deposit" => $save_guarantee_amount]);
            echo $result;
        }

        public function inquirydepositoAction()
        {

            $app = Zend_Registry::get('config');
            $app = $app['app']['bankcode'];

            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $acct_no = $this->_getParam('acct_no');
            $bg_reg_number = $this->_getParam('bg_reg_number');
            //echo $acct_no;

            $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
            $result = $svcAccount->inquiryDeposito('AB', TRUE);

            $info = $svcAccount->inquiryAccontInfo("AB");

            $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
            $result2 = $svcAccountCIF->inquiryCIFAccount();

            if ($result["response_code"] != "0000") {
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($result);
                return 0;
            }

            if ($result["status"] != 1 && $result["status"] != 4) {
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($result);
                return 0;
            }

            $sqlRekeningJaminanExist = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                ->where('A.ACCT = ?', $result['account_number'])
                ->query()->fetchAll();

            if ($bg_reg_number && $sqlRekeningJaminanExist) {
                foreach ($sqlRekeningJaminanExist as $key => $value) {
                    if ($value["BG_REG_NUMBER"] == $bg_reg_number) {
                        $result["check_exist_split"] = 0;
                    } else {
                        $result["check_exist_split"] = 1;
                    }
                }
            } else {
                $result["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;
            }

            $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
            $result2 = $svcAccountCIF->inquiryCIFAccount();

            $filterBy = $result['account_number']; // or Finance etc.
            // $tempor = array_filter($result2["accounts"], function ($test) use ($filterBy) {
            //     return ($test['account_number'] == $filterBy);
            // });
            // $get_product_type = current($tempor)["type"];
            $check_prod_type = $this->_db->select()
                ->from("M_PRODUCT_TYPE")
                ->where("PRODUCT_CODE = ?", $result["account_type"])
                ->query()->fetch();

            $result["check_product_type"] = 0;
            if (!empty($check_prod_type)) {
                $result["check_product_type"] = 1;
            }

            $data['data'] = false;
            // is_array($return) ? $return :  $return = $data;
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
        }

        public function inquiryaccountbalanceAction()
        {

            $app = Zend_Registry::get('config');
            $app = $app['app']['bankcode'];

            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $acct_no = $this->_getParam('acct_no');
            //echo $acct_no;

            $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
            $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
            $info = $svcAccount->inquiryAccontInfo("AB");

            if ($result["status"] != 1 && $result["status"] != 4) {
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($result);
                return 0;
            }

            $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
            // $result2 = $svcAccountCIF->inquiryCIFAccount();

            $filterBy = $result['account_number']; // or Finance etc.
            // $tempor = array_filter($result2["accounts"], function ($test) use ($filterBy) {
            //     return ($test['account_number'] == $filterBy);
            // });
            // $get_product_type = current($tempor)["type"];
            $check_prod_type = $this->_db->select()
                ->from("M_PRODUCT_TYPE")
                ->where("PRODUCT_CODE = ?", $result["account_type"])
                ->query()->fetch();

            $result["check_product_type"] = 0;
            if (!empty($check_prod_type)) {
                $result["check_product_type"] = 1;
            }

            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            die();

            // $result = $svcAccount->inquiryAccontInfo('AB', TRUE);

            $data['data'] = false;
            // is_array($return) ? $return :  $return = $data;
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
        }

        public function inquiryaccountinfoAction()
        {

            //integrate ke core
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();
            $acct_no = $this->_getParam('acct_no');
            $svcAccount = new Service_Account($acct_no, null);
            $result = $svcAccount->inquiryAccontInfo('AI', TRUE);

            if ($result['response_desc'] == 'Success') //00 = success
            {
                $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
                $result2 = $svcAccountCIF->inquiryCIFAccount();

                $filterBy = $result['account_number']; // or Finance etc.
                $new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
                    return ($var['account_number'] == $filterBy);
                });

                $singleArr = array();
                foreach ($new as $key => $val) {
                    $singleArr = $val;
                }
            } else {
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($result);
                return 0;
            }

            if ($singleArr['type_desc'] == 'Deposito') {
                $svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
                $result3 = $svcAccountDeposito->inquiryDeposito();

                if ($result3["response_code"] != "0000") {
                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode(array_merge($result3, $singleArr, $result));
                    return 0;
                }

                if ($result3["status"] != 1 && $result3["status"] != 4) {
                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode(array_merge($result3, $singleArr, $result));
                    return 0;
                }

                $sqlRekeningJaminanExist = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                    ->where('A.ACCT = ?', $result['account_number'])
                    ->query()->fetchAll();

                $result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;

                $get_product_type = current($new)["type"];

                $check_prod_type = $this->_db->select()
                    ->from("M_PRODUCT_TYPE")
                    ->where("PRODUCT_CODE = ?", $result3["account_type"])
                    ->query()->fetch();

                $result3["check_product_type"] = 0;
                if (!empty($check_prod_type)) {
                    $result3["check_product_type"] = 1;
                }
            } else {
                $svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
                $result3 = $svcAccountBalance->inquiryAccountBalance();

                if ($result3["status"] != 1 && $result3["status"] != 4) {
                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode($result3);
                    return 0;
                }

                $get_product_type = current($new)["type"];
                $check_prod_type = $this->_db->select()
                    ->from("M_PRODUCT_TYPE")
                    ->where("PRODUCT_CODE = ?", $result3["account_type"])
                    ->query()->fetch();

                $result3["check_product_type"] = 0;
                if (!empty($check_prod_type)) {
                    $result3["check_product_type"] = 1;
                }
            }

            $return = array_merge($result, $singleArr, $result3);

            $data['data'] = false;
            is_array($return) ? $return :  $return = $data;

            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($return);
        }

        public function checkstatuslfAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $get_status_lf = $this->_db->select()
                ->from("M_CUST_LINEFACILITY")
                ->where("CUST_ID = ?", $this->_custIdLogin)
                ->query()->fetch();

            header('Content-Type: application/json; charset=utf-8');
            echo json_encode(["STATUS" => $get_status_lf["STATUS"]]);
        }

        public function checkcustlfdetailAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $get_offer_type = $this->_request->getParam("offertype");

            $check_db = $this->_db->select()
                ->from("M_CUST_LINEFACILITY_DETAIL")
                ->where("OFFER_TYPE = ?", $get_offer_type)
                ->where("FLAG = 1")
                ->query()->fetchAll();

            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($check_db);
        }

        public function checktypebglfAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $get_offer_type = $this->_request->getParam("offertype");
            $warranty_type = $this->_request->getParam("warranty_type");

            if ($warranty_type == 2) {
                $cust_id = $this->_request->getParam("cust_id");

                $check_grup_bumn = $this->_db->select()
                    ->from("M_CUSTOMER")
                    ->where("CUST_STATUS = 1")
                    ->where("CUST_ID = ?", $cust_id)
                    ->query()->fetchAll();


                $check_charges_bg = $this->_db->select()
                    ->from("M_CHARGES_BG")
                    ->where("CHARGES_ID LIKE ? ", '' . $check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'LFBUMN' : 'LFNONBUMN' . '%')
                    ->where("CHARGES_NAME LIKE ?", '' . 'LF ' . $get_offer_type . '%')
                    ->query()->fetchAll();


                $check_lf_detail = $this->_db->select()
                    ->from("M_CUST_LINEFACILITY_DETAIL")
                    ->where("OFFER_TYPE = ?", $get_offer_type)
                    ->where("FLAG = 1")
                    ->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
                    ->where("CUST_ID = ?", $cust_id)
                    ->query()->fetchAll();
                $check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];
            }

            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($check_lf_detail[0]);
        }

        public function checktypebginsAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout()->disableLayout();

            $get_offer_type = $this->_request->getParam("offertype");
            $warranty_type = $this->_request->getParam("warranty_type");

            if ($warranty_type == 3) {
                $cust_id = $this->_request->getParam("cust_id");

                $check_grup_bumn = $this->_db->select()
                    ->from("M_CUSTOMER")
                    ->where("CUST_STATUS = 1")
                    ->where("CUST_ID = ?", $cust_id)
                    ->query()->fetchAll();


                $check_charges_bg = $this->_db->select()
                    ->from("M_CHARGES_BG")
                    ->where("CHARGES_ID LIKE ? ", '' . $check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'INSBUMN' : 'INSNONBUMN' . '%')
                    ->where("CHARGES_NAME LIKE ?", '' . 'ASURANSI ' . $get_offer_type . '%')
                    ->query()->fetchAll();


                $check_lf_detail = $this->_db->select()
                    ->from("M_CUST_LINEFACILITY_DETAIL")
                    ->where("OFFER_TYPE = ?", $get_offer_type)
                    //->where("FLAG = 1")
                    ->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
                    ->where("CUST_ID = ?", $cust_id)
                    ->query()->fetchAll();
                $check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];
            }

            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($check_lf_detail[0]);
        }
    }
