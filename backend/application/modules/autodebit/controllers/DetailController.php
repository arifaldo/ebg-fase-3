<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class autodebit_DetailController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $this->view->dd_no = $this->_getParam('dd_no');
        $this->view->dd_name = $this->_getParam('dd_name');
        $this->view->dd_ref = $this->_getParam('dd_ref');

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();
        $cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password));
        $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
        $error_remark = null;
        
        $getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;
        $this->view->custAcct_msg  = array();
        
        $acct_no = $this->_getParam('acct_no');
        
        if($cust_id)
        {
        $custData = $this->_db->select()
                                ->from('M_CUSTOMER',array('CUST_ID','CUST_STATUS','CUST_CIF'))
                                ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                                //->where('CUST_STATUS=1');
                                ->query()->fetch();
                                
        if(!$custData['CUST_ID']) $cust_id = null;
            $cust_status 	= $custData['CUST_STATUS'];
            $cust_cif 		= $custData['CUST_CIF'];
        }
        
        if(!$cust_id)
        {
        $error_remark = 'Invalid Cust ID';
        $this->_helper->getHelper('FlashMessenger')->addMessage('F');
        $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
        // $this->_redirect($this->_backURL);
        }
        
        $select = $this->_db->select()
  	                       ->from('M_FGROUP',array('FGROUP_ID','FGROUP_NAME'))
  	                       ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' OR UPPER(CUST_ID)='.$this->_db->quote('BANK'))
  	                       ->where('UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
        $this->view->fgroup = $this->_db->fetchAll($select);
        
        $select = $this->_db->select()
                            ->from('M_CUSTOMER',array('CUST_NAME'))
                            ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
        $this->view->cust_name = $this->_db->fetchOne($select);
        
        $this->view->cust_id = $cust_id;
        $this->view->modulename = $this->_request->getModuleName();
    }
}