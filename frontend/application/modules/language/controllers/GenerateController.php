<?php
class language_GenerateController extends Application_Main {
	public function indexAction(){
		$model = new language_Model_Index();
		$rawDictionary = $model->getDictionary(array('ui'=>1));
		
$tmx = '<?xml version="1.0" ?>
<!DOCTYPE tmx SYSTEM "tmx14.dtd">
<tmx version="1.4">
<header creationtool="CTmx" creationtoolversion="1.0" datatype="winres" segtype="sentence" adminlang="en-us" srclang="en-us" o-tmf="txt">
</header>
<body>
	APPENDAREA
</body>
</tmx>';
		
		$words = array();
		if (count($rawDictionary)){
			foreach ($rawDictionary as $key => $val){
				if (!(in_array($val['WORD'], $words))){
					$words = array_merge($words, array($val['WORD']));
				}
			}
		}
		
		
		
		$pairWords = array();
		if(count($words)){
			foreach ($words as $word){
				foreach ($rawDictionary as $key => $val){
					if (strcmp($word, $val['WORD']) == 0){
						$lang = explode('_', $val['CODE']);
						$pairWords = array_merge_recursive($pairWords, 
							array(
								$word => array(
									array(
										'lang' => $lang[0],
										'seg' => $val['TEXT'],
									)
								)
							)
						);
					}
				}
			}
		}
		
		$appendText = NULL;
		foreach ($pairWords as $key => $arr){
			$appendText .= '<tu tuid="'.$key.'">'."\r";
			foreach ($arr as $id => $val){
				$appendText .= '<tuv xml:lang="'.$val['lang'].'"><seg>'.$val['seg'].'</seg></tuv>'."\r";
			}
			$appendText .= '</tu>'."\r";
		}
		
		$tmx = strtr($tmx, array('APPENDAREA' => $appendText));
		//$tmxPath = LIBRARY_PATH.'data/languages/backend/language.tmx';
		$tmxPath = LIBRARY_PATH.'/data/languages/frontend/language.tmx';
//		if(file_exists($tmxPath)){
//			echo'ada';
//		}
//		else{
//			echo'tidak ada';
//		}
//		Zend_Debug::dump($tmxPath);die;
		if (file_exists($tmxPath)){
			unlink($tmxPath);
			//file_put_contents(LIBRARY_PATH.'data/languages/backend/language.tmx', $tmx);
			file_put_contents(LIBRARY_PATH.'/data/languages/frontend/language.tmx', $tmx);
			
		}
		
		Zend_Translate::clearCache();
		$this->_redirect('language/change');
	}	
}
