<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class ongoingsubmission_DetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        // echo "<code>masuk = $data</code>"; die;	
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;
        $toc_ind = $settings->getSetting('ftemplate_bg_ind');
        $this->view->toc_ind = $toc_ind;
        $toc_eng = $settings->getSetting('ftemplate_bg_eng');
        $this->view->toc_eng = $toc_eng;
        // echo "<code>systemType = $system_type</code><br>";	
        // echo '<pre>';
        // print_r($system_type); 
        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;


        $Settings = new Settings();
        $claim_period = $Settings->getSetting('max_claim_period');
        $this->view->BG_CLAIM_PERIOD = $claim_period;

        $this->view->currencyArr = array(
            // ''=>'-- '.$this->language->_('Any Value').' --',
            '1' => $this->language->_('IDR'),
            '2' => $this->language->_('USD'),
        );

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            ->joinLeft(['CL' => 'M_CITYLIST'], 'A.CUST_CITY = CL.CITY_CODE', ["CITY_NAME" => "CL.CITY_NAME"])
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];
        $numb = $this->_getParam('bgnumb');
        $this->view->bgnumb = $numb;



        if (!empty($numb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            if (empty($bgdata)) {
                $bgdata = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $bgdatadetail = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    // ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $bgdatasplit = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                    //  ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();
            } else {

                $bgdatadetail = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    // ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $bgdatasplit = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    //  ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();
            }

            switch ($bgdata[0]["CHANGE_TYPE"]) {
                case '0':
                    $this->view->suggestion_type = "New";
                    break;
                case '1':
                    $this->view->suggestion_type = "Amendment Changes";
                    break;
                case '2':
                    $this->view->suggestion_type = "Amendment Draft";
                    break;
            }

            $this->view->bgdatasplit = $bgdatasplit;
            $this->view->data = $bgdata[0];
            // var_dump($datas);
            // echo '<pre>';
            // print_r($bgdatasplit);



            if (!empty($bgdata)) {

                $data = $bgdata['0'];
                if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
                    $this->view->isinsurance = true;
                }
                //cek privilage
                $custidlike = '%' . $this->_custIdLogin . '%';

                $principleData = [];
                // if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
                foreach ($bgdatadetail as $key => $value) {
                    $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
                }

                $this->view->principleData = $principleData;

                //select user with releaser privilage
                $selectReleaser = $this->_db->select()
                    ->from(array('P' => 'M_FPRIVI_USER'))
                    ->from(array('U' => 'M_USER'))
                    ->where("P.FPRIVI_ID = 'PRLP' AND P.FUSER_ID LIKE ?", (string) $custidlike)
                    ->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
                    ->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

                // echo "<code>selectReleaser = $selectReleaser</code><BR>";  
                $userReleaser = $this->_db->fetchAll($selectReleaser);

                $releaserList = array();
                foreach ($userReleaser as $row) {
                    $userIdReleaser = explode($this->_custIdLogin, $row['FUSER_ID']);

                    //get user name
                    $selectReleaserName = $this->_db->select()
                        ->from('M_USER')
                        ->where("USER_ID = ?", (string) $userIdReleaser[1]);

                    $userReleaserName = $this->_db->fetchAll($selectReleaserName);

                    array_push($releaserList, $userReleaserName[0]['USER_FULLNAME']);
                }


                //cek ada privilage reviewer atau approver
                $selectpriv = $this->_db->select()
                    ->from(array('M_CUSTOMER'))
                    ->where("CUST_ID = ?", $this->_custIdLogin);
                $userpriv = $this->_db->fetchrow($selectpriv);

                if ($userpriv['CUST_REVIEW'] != 1) {
                    $cust_reviewer = 0;
                } else {
                    $cust_reviewer = 1;
                }

                if ($userpriv['CUST_APPROVER'] != 1) {
                    $cust_approver = 0;
                } else {
                    $cust_approver = 1;
                }


                $selectHistory    = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $numb)
                    ->order("DATE_TIME DESC");

                $history = $this->_db->fetchAll($selectHistory);

                $get_notes = $history[0]["BG_REASON"];
                $this->view->notes = $get_notes;

                $cust_approver = 1;

                $bg_submission_hisotrys = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $numb)
                    ->where("CUST_ID = ? ", $selectcomp[0]["CUST_ID"]);
                $bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

                foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {

                    // maker
                    if ($bg_submission_hisotry['HISTORY_STATUS'] == 1) {
                        $makerStatus = 'active';
                        $makerIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        // $custFullname = $customer[0]['USER_FULLNAME'];
                        $custFullname = $customer[0]['USER_FULLNAME'];

                        $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                        $align = 'align="center"';

                        $this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }

                    // approver
                    if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
                        $approverStatus = 'active';
                        $approverIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        // $custFullname = $customer[0]['USER_FULLNAME'];
                        $custFullname = $customer[0]['USER_FULLNAME'];

                        $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                        $align = 'align="center"';

                        $this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

                        $this->view->approverStatus = $approverStatus;
                    }

                    if ($data["COUNTER_WARRANTY_TYPE"] == '3') {

                        $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
                        $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

                        $insuranceBranch = $this->_db->select()
                            ->from("M_INS_BRANCH")
                            ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
                            ->query()->fetchAll();

                        $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
                        $this->view->insuranceAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];

                        //if releaser
                        // if ($bg_submission_hisotry['HISTORY_STATUS'] == 8 || $bg_submission_hisotry['HISTORY_STATUS'] == 10) {
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5) {
                            $releaserStatus = 'active';
                            $releaserIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            // $custFullname = $customer[0]['USER_FULLNAME'];
                            $custFullname = $customer[0]['USER_FULLNAME'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                            $this->view->releaserStatus = $releaserStatus;
                        }
                    } else {
                        //if releaser
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5) {
                            $releaserStatus = 'active';
                            $releaserIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            // $custFullname = $customer[0]['USER_FULLNAME'];
                            $custFullname = $customer[0]['USER_FULLNAME'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                            $this->view->releaserStatus = $releaserStatus;
                        }
                    }


                    // if ($bg_submission_hisotry['HISTORY_STATUS'] == 4 || $bg_submission_hisotry['HISTORY_STATUS'] == 14) {
                    // 	$releaserStatus = 'active';
                    // 	$releaserIcon = '<i class="fas fa-check"></i>';

                    // 	$custlogin = $bg_submission_hisotry['USER_LOGIN'];

                    // 	$selectCust	= $this->_db->select()
                    // 		->from('M_USER')
                    // 		->where("USER_ID = ?", $custlogin)
                    // 		->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                    // 	$customer = $this->_db->fetchAll($selectCust);

                    // 	$custFullname = $customer[0]['USER_FULLNAME'];

                    // 	$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                    // 	$align = 'align="center"';

                    // 	$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span></span></div>';
                    // 	$this->view->releaserStatus = $releaserStatus;
                    // }
                }

                foreach ($history as $row) {

                    if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
                        if ($row['HISTORY_STATUS'] == 8) {
                            $makerStatus = 'active';
                            $insuranceIcon = '<i class="fas fa-check"></i>';
                            /*$approveStatus = 'active';
							$reviewStatus = 'active';
							$releaseStatus = 'active';
							$releaseIcon = '<i class="fas fa-check"></i>';*/
                            $insuranceStatus = 'active';
                            $reviewStatus = '';
                            //$releaseStatus = '';
                            //$releaseIcon = '';

                            $makerOngoing = '';
                            $reviewerOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = '';

                            $custlogin = $row['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin);
                            //->where("CUST_ID = ?", $row['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custEmail 	  = $customer[0]['USER_EMAIL'];
                            // $custPhone	  = $customer[0]['USER_PHONE'];

                            $insuranceApprovedBy = $custFullname;

                            $align = 'align="center"';
                            $marginLeft = '';
                            if ($cust_reviewer == 0 && $cust_approver == 0) {
                                $align = '';
                                $marginLeft = 'style="margin-left: 15px;"';
                            }

                            $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                            $this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>ASURANSI</span></div>';
                        }
                    }

                    if ($row['HISTORY_STATUS'] == 6) {
                        $verifyStatus = 'active';
                        $verifyIcon = '<i class="fas fa-check"></i>';

                        $verifyOngoing = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = 'ongoing';
                            $releaserOngoing = '';
                        } else {
                            //$reviewerOngoing = 'ongoing';
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            $releaserOngoing = '';
                        }

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

                        $align = 'align="center"';
                        $marginRight = '';

                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginRight = 'style="margin-right: 15px;"';
                        }

                        $this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>BANK</span></div>';
                    }

                    //if reviewer done
                    if ($row['HISTORY_STATUS'] == 7) {
                        $bankReviewStatus = "active";
                        $bankReviewOngoing = "ongoing";
                        $bankReviewIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        $this->view->bankReviewedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>BANK</span></div>';
                    }

                    //if approver done
                    if ($row['HISTORY_STATUS'] == 14) {
                        $bankApproveStatus = "active";
                        $bankReviewOngoing = "ongoing";
                        $bankApproveIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        $this->view->bankApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>BANK</span></div>';
                    }

                    //if releaser done
                    if ($row['HISTORY_STATUS'] == 1) {
                        $makerStatus = 'active';
                        /*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
                        $approveStatus = '';
                        $reviewStatus = '';
                        $releaseStatus = '';
                        $releaseIcon = '';

                        $makerOngoing = '';
                        $reviewerOngoing = '';
                        $approverOngoing = '';
                        $releaserOngoing = '';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $row['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['USER_FULLNAME'];
                        // $custEmail 	  = $customer[0]['USER_EMAIL'];
                        // $custPhone	  = $customer[0]['USER_PHONE'];

                        $releaserApprovedBy = $custFullname;

                        $align = 'align="center"';
                        $marginLeft = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginLeft = 'style="margin-left: 15px;"';
                        }

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        // $this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }
                }


                //approvernamecircle jika sudah ada yang approve
                if (!empty($userid)) {

                    // echo "<code>masuk = $data</code><BR>";  
                    $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

                    $flipAlphabet = array_flip($alphabet);

                    $approvedNameList = array();
                    $i = 0;
                    foreach ($userid as $key => $value) {

                        //select utk nama dan email
                        $selectusername = $this->_db->select()
                            ->from(array('M_USER'), array(
                                '*'
                            ))
                            ->where("CUST_ID = ?", $this->_custIdLogin)
                            ->where("USER_ID = ?", (string) $value);

                        $username = $this->_db->fetchAll($selectusername);

                        //select utk cek user berada di grup apa
                        $selectusergroup    = $this->_db->select()
                            ->from(array('C' => 'M_APP_GROUP_USER'), array(
                                '*'
                            ))
                            ->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
                            ->where("C.USER_ID 	= ?", (string) $value);

                        $usergroup = $this->_db->fetchAll($selectusergroup);

                        $groupuserid = $usergroup[0]['GROUP_USER_ID'];
                        $groupusername = $usergroup[0]['USER_ID'];
                        $groupuseridexplode = explode("_", $groupuserid);

                        if ($groupuseridexplode[0] == "S") {
                            $usergroupid = "SG";
                        } else {
                            $usergroupid = $alphabet[$groupuseridexplode[2]];
                        }



                        array_push($approvedNameList, $username[0]['USER_FULLNAME']);

                        $efdate = $approveEfDate[$i];

                        $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

                        $i++;
                    }

                    $this->view->approverApprovedBy = $approverApprovedBy;

                    //kalau sudah approve semua
                    if (!$checkBoundary) {
                        $approveStatus = 'active';
                        $approverOngoing = '';
                        $approveIcon = '<i class="fas fa-check"></i>';
                        $releaserOngoing = 'ongoing';
                    }
                }

                //define circle
                $makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '</button>';


                // $approverNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled><i class="fas fa-check"></i>
                // 	</button>';
                $approverNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>
                ' . $approveIcon . '
                </button>';

                foreach ($releaserList as $key => $value) {

                    $textColor = '';
                    if ($value == $releaserApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }

                $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup  ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '
					<span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span></button>';

                $verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $verifyStatus . ' ' . $verifyOngoing . ' hovertext" disabled>' . $verifyIcon . ' </button>';

                foreach ($insuranceList as $key => $value) {

                    $textColor = '';
                    // if ($value == $insuranceApprovedBy) {
                    $textColor = 'text-white-50';
                    // }

                    $insuranceListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }
                // 
                $insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . ' </button>';


                $releasenewNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $releasenewStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $releasenewIcon . '</button>';

                $bankReviewNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $bankReviewStatus . ' ' . $reviewOngoing . ' hovertext" disabled>' . $bankReviewIcon . '</button>';

                $bankApproveNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $bankApproveStatus . ' ' . $reviewOngoing . ' hovertext" disabled>' . $bankApproveIcon . '</button>';

                $bankApproverNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $bankApproverStatus . ' ' . $bgapproverOngoing . ' hovertext" disabled> ' . $bankApproverIcon . ' </button>';

                foreach ($releaserList as $key => $value) {

                    $textColor = '';
                    if ($value == $verificationApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $verificationListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }

                $verificationNameCircle = '<button id="releaserCircle" class="btnCircleGroup  ' . $verificationStatus . ' ' . $verificationOngoing . ' hovertext" disabled>' . $verificationIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $verificationListView . '</span></button>';

                // echo "<code>cust_reviewer = $cust_reviewer | cust_approver = $cust_approver </code><BR>";  
                $this->view->cust_reviewer = $cust_reviewer;
                $this->view->cust_approver = $cust_approver;
                $this->view->releasenewNameCircle = $releasenewNameCircle;
                $this->view->insuranceNameCircle = $insuranceNameCircle;
                $this->view->makerNameCircle = $makerNameCircle;
                $this->view->bankReviewNameCircle = $bankReviewNameCircle;
                $this->view->bankApproveNameCircle = $bankApproveNameCircle;
                $this->view->approverNameCircle = $approverNameCircle;
                $this->view->releaserNameCircle = $releaserNameCircle;
                $this->view->verifyNameCircle = $verificationNameCircle;
                $this->view->verifyNameCircle = $verifyNameCircle;


                $this->view->makerStatus = $makerStatus;
                $this->view->approveStatus = $approveStatus;
                $this->view->releaseStatus = $releaseStatus;
                $this->view->verificationStatus = $verificationStatus;
                // echo "<code>releaseStatus = $releaseStatus</code><BR>"; 



                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                // $updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'],$this->view->viewDateFormat,$this->view->defaultDateFormat);


                // echo "<code>updateEnd = $updateEnd</code><BR>"; die;
                $config            = Zend_Registry::get('config');
                $BgType         = $config["bg"]["status"]["desc"];
                $BgCode         = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

                $this->view->arrStatus = $arrStatus;

                $comp = "'" . $this->_custIdLogin . "'";
                $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                        ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                        // ->where('A.ACCT_STATUS = ?','5')
                        ->where("A.CUST_ID IN (" . $comp . ")")
                        ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
                );

                // echo "<pre>";
                // var_dump($acctlist);

                $account = array();
                foreach ($acctlist as $key => $value) {
                    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                    $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                    $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                    $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                    $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                    $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
                }
                // echo "<pre>";
                // var_dump($account);die;
                // $acct = array();
                $i = 0;
                foreach ($account as $key => $value) {

                    $acct[$i]['ACCT_NO'] = $value['account_number'];
                    $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                    $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                    $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                    $acct[$i]['ACCT_NAME'] = $value['account_name'];
                    $i++;
                }

                $this->view->sourceAcct = $acct;


                // $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


                // $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                // $AccArr = $CustomerUser->getAccounts($param);

                //usd
                // $paramUSD = array('CCY_IN' => 'USD');
                // $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
                // if (!empty($AccArrUSD)) {
                //     foreach ($AccArrUSD as $iUSD => $valueUSD) {
                //         $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                //     }
                // }

                // $this->view->AccArrUSD = $AccArrUSD;

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                $AccArr = $CustomerUser->getAccountsBG($param);
                //var_dump($AccArr);die;

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->BG_FORMAT = $data['BG_FORMAT'];
                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                if ($data['BG_STATUS'] == '7' || $data['BG_STATUS'] == '11' ||  $data['BG_STATUS'] == '5' || $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                        a.USER_LOGIN,
                        b.`USER_FULLNAME` AS u_name,
                        c.`BUSER_NAME` AS b_name,
                        a.DATE_TIME,
                        a.BG_REASON,
                        a.HISTORY_STATUS,
                        a.BG_REG_NUMBER
                        
                        
                    FROM
                        T_BANK_GUARANTEE_HISTORY AS a
                        LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                        LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                    WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS IN (10,5) GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    if ($data['BG_STATUS'] == '10' || $data['BG_STATUS'] == '5') {
                        $result =  $this->_db->fetchAll($selectQuery);
                    } else {
                        $result = array();
                    }
                    if (!empty($result)) {
                        $this->view->reqrepair = true;

                        $data['REASON'] = $resut['0']['BG_REASON'];
                        $this->view->username = ' (By ' . $result['0']['u_name'] . '' . $result['0']['b_name'] . ')';
                        foreach ($result as $key => $value) {
                            $reason[$value['HISTORY_STATUS']][] = $value['BG_REASON'];
                        }
                    }
                    $rowindex = count($reason[10]) - 1;

                    $this->view->reason = $reason[10][$rowindex] . '' . $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->BG_LANGUAGE = $data['BG_LANGUAGE'];
                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                foreach ($arrbgType as $key => $val) {
                    $arrbgType_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_purpose                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_bg_purpose                += Application_Helper_Array::listArray($arrbgType_new, 'ID', 'VALUE');
                $this->view->bgpurposeArr       = $list_bg_purpose;

                // $this->view->arrbgType = $arrbgType;
                $this->view->BG_PURPOSE = $data['USAGE_PURPOSE'];
                $this->view->BG_PURPOSE_LBL = $arrbgType[$data['USAGE_PURPOSE']];
                $this->view->BG_PURPOSE_DESC = $arrbgType[$data['USAGE_PURPOSE']];

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                foreach ($arrbgdoc as $key => $val) {
                    $arrbgdoc_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_document_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_document_type                += Application_Helper_Array::listArray($arrbgdoc_new, 'ID', 'VALUE');
                $this->view->documenttypeArr     = $list_document_type;

                $this->view->GT_DOC_TYPE =  $data['GT_DOC_TYPE'];
                $this->view->GT_DOC_DESC = $arrbgdoc[$data['GT_DOC_TYPE']];

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                foreach ($arrbgcg as $key => $val) {
                    $arrbgcg_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_warranty_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_warranty_type                += Application_Helper_Array::listArray($arrbgcg_new, 'ID', 'VALUE');
                $this->view->warrantytypeArr     = $list_warranty_type;

                $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];


                $linefacilitydata = $this->_db->fetchRow(
                    $this->_db->select()
                        ->from(array('M_CUSTOMER'))
                        ->where("CUST_ID=?",  $data['CUST_ID'])->limit(1)
                );
                $get_linefacility = $this->_db->select()
                    ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
                    ->where("CUST_ID = ?", $data["CUST_ID"])
                    ->query()->fetchAll();

                $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
                $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];
                $this->view->linefacilitydata = $linefacilitydata;


                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
                foreach ($arrbgpublish as $key => $val) {
                    $arrbgpublish_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_publist                = array('' => ' --- ' . $this->language->_('Any Value') . ' ---');
                $list_bg_publist                += Application_Helper_Array::listArray($arrbgpublish_new, 'ID', 'VALUE');
                $this->view->BG_PUBLISH_ARR     = $list_bg_publist;
                // echo '<pre>';
                // print_r($list_bg_publist); 
                $this->view->BG_PUBLISH = $data['BG_PUBLISH'];
                $this->view->BG_PUBLISH_DESC = $arrbgpublish[$data['BG_PUBLISH']];

                //$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];

                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];

                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];

                $this->view->CUST_NAME = $data['CUST_NAME'];
                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];

                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->SERVICE = $data['SERVICE'];

                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                // $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                $checkOthersAttachment = $this->_db->select()
                    ->from(['A' => 'TEMP_BANK_GUARANTEE_FILE'], ['*'])
                    ->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
                    ->order('A.INDEX ASC')
                    ->query()->fetchAll();

                if (count($checkOthersAttachment) > 0) {
                    $this->view->othersAttachment = $checkOthersAttachment;
                }

                if ($data['COUNTER_WARRANTY_TYPE'] == 1) {

                    $bgdatasplit = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
                        ->query()->fetchAll();

                    $this->view->fullmember = $bgdatasplit;
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {
                        $insurancedata = $this->_db->fetchRow(
                            $this->_db->select()
                                ->from(array('M_CUSTOMER'))
                                ->where("CUST_ID=?",  $value['PS_FIELDVALUE'])->joinLeft(
                                    array('MCL' => 'M_CITYLIST'),
                                    'MCL.CITY_CODE = M_CUSTOMER.CUST_CITY',
                                    array('CITY_NAME')
                                )->limit(1)
                        );
                        $insurancebranch = $this->_db->fetchRow(
                            $this->_db->select()
                                ->from(array('M_INS_BRANCH'))
                                ->where("INS_BRANCH_CODE=?",  $value['PS_FIELDVALUE'])->limit(1)
                        );
                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $insurancedata['CUST_NAME'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
                                $this->view->insuranceBranch =   $insurancebranch['INS_BRANCH_NAME'];
                            }


                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDataEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }


                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];
                // preliminary Member
                $selectMP = $this->_db->select()
                    ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
                $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
                // echo "<code>selectMP = $selectMP</code><BR>";  
                $preliminaryMemberArr = $this->_db->fetchAll($selectMP);
                // echo '<pre>';
                // print_r($preliminaryMemberArr);
                $this->view->preliminaryMemberArr = $preliminaryMemberArr;

                $params = $this->_request->getParams();
                $toa = $params['toa'];

                $BG_UNDERLYING_DOC = $this->_getParam('BG_UNDERLYING_DOC');
                //print_r($edit);die;
                if ($BG_UNDERLYING_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['BG_UNDERLYING_DOC'], $attahmentDestination . $data['BG_UNDERLYING_DOC']);
                }


                if ($this->_request->isPost()) {



                    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
                    if (count($temp) > 1) {
                        if ($temp[0] == 'F' || $temp[0] == 'S') {
                            if ($temp[0] == 'F')
                                $this->view->error = 1;
                            else
                                $this->view->success = 1;
                            $msg = '';
                            unset($temp[0]);
                            foreach ($temp as $value) {
                                if (!is_array($value))
                                    $value = array($value);
                                $msg .= $this->view->formErrors($value);
                            }
                            $this->view->report_msg = $msg;
                        }
                    }
                    $attahmentDestination     = UPLOAD_PATH . '/document/submit/';
                    $errorRemark             = null;
                    $adapter                 = new Zend_File_Transfer_Adapter_Http();
                    $fileExt                 = "jpeg,jpg,pdf";

                    $sourceFileName = $adapter->getFileName();

                    if ($sourceFileName == null) {
                        $sourceFileName = null;
                        $fileType = null;
                    } else {
                        $sourceFileName = substr(basename($adapter->getFileName()), 0);
                        // echo "<code>sourceFileName = $sourceFileName</code>"; die;	
                        if ($_FILES["uploadSource"]["type"]) {
                            $adapter->setDestination($attahmentDestination);
                            $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                            $fileType = $adapter->getMimeType();
                            $fileInfo = $adapter->getFileInfo();
                            $size = $_FILES["uploadSource"]["size"];
                        } else {
                            $fileType = null;
                            $size = null;
                        }
                    }

                    // $filters = array(
                    //     'updatedate'                        => array('StripTags','StringTrim'),   
                    // );

                    // if ($toa=='1')
                    // {
                    $filters = array(
                        'updatedate'                    => array('StripTags', 'StringTrim'),
                        'CUST_CP_TXT'                   => array('StripTags', 'StringTrim'),
                        'CUST_CONTACT_NUMBER_TXT'       => array('StripTags', 'StringTrim'),
                        'CUST_EMAIL_TXT'                => array('StripTags', 'StringTrim'),
                        'BG_PURPOSE_TXT'                => array('StripTags', 'StringTrim'),
                        'bank_amount_txt'               => array('StripTags', 'StringTrim'),
                        'currency'                      => array('StripTags', 'StringTrim'),
                        'RECIPIENT_CP_TXT'              => array('StripTags', 'StringTrim'),
                        'RECIPIENT_CONTACT_TXT'         => array('StripTags', 'StringTrim'),
                        'RECIPIENT_EMAIL_TXT'           => array('StripTags', 'StringTrim'),
                        'SERVICE_TXT'                   => array('StripTags', 'StringTrim'),
                        'GT_DOC_TYPE_TXT'               => array('StripTags', 'StringTrim'),
                        'GT_DOC_NUMBER_TXT'             => array('StripTags', 'StringTrim'),
                        'underlyingDocument'            => array('StripTags', 'StringTrim'),
                        'GT_DOC_DATE_TXT'               => array('StripTags', 'StringTrim'),
                        'acct_txt'                      => array('StripTags', 'StringTrim'),
                        'COUNTER_WARRANTY_TYPE_TXT'     => array('StripTags', 'StringTrim'),
                        'BG_PUBLISH_TXT'                => array('StripTags', 'StringTrim'),
                        'BG_FORMAT'                     => array('StripTags', 'StringTrim'),
                        'BG_LANGUAGE'                   => array('StripTags', 'StringTrim'),

                    );
                    // }

                    $validators =  array(
                        'updatedate'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),

                    );

                    // echo "<code>toa = $toa</code><BR>"; die;	
                    if ($toa == '1') {

                        $validators +=  array(
                            'CUST_CP_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_CONTACT_NUMBER_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_EMAIL_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PURPOSE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'bank_amount_txt'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'currency'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CP_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CONTACT_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_EMAIL_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'SERVICE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_TYPE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_NUMBER_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'underlyingDocument'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_DATE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'acct_txt'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'COUNTER_WARRANTY_TYPE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PUBLISH_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_FORMAT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_LANGUAGE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),

                        );
                    }

                    // echo "<code>masuk1 = $data</code>"; die;	
                    $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

                    if ($zf_filter_input->isValid()) {

                        // echo "<code>masuk1 = $data</code><BR>"; die;	

                        if ($zf_filter_input->COUNTER_WARRANTY_TYPE_TXT == '1') {

                            if (!empty($params['flselect_account_manual'])) {
                                $splitArr = array();
                                foreach ($params['flselect_account_manual'] as $ky => $vl) {
                                    if ($vl == '') {
                                        $nameAcct = explode('|', $params['flselect_account_number'][$ky]);
                                        $splitArr[$ky]['acct'] = $nameAcct[0];
                                    } else {
                                        $splitArr[$ky]['acct'] = $vl;
                                    }

                                    if ($params['flname_manual'][$ky] == '') {
                                        $splitArr[$ky]['acct_name'] = $params['flname'][$ky];
                                    } else {
                                        $splitArr[$ky]['acct_name'] = $params['flname_manual'][$ky];
                                    }

                                    $splitArr[$ky]['amount'] = $params['flamount'][$ky];
                                    $splitArr[$ky]['flag'] = $params['flselect_own'][$ky];
                                    $splitArr[$ky]['bank_code'] = '999';


                                    // echo "<code>masuk = $params</code>"; 
                                }
                                $this->view->acctSplit = $splitArr;
                            }
                        }
                        // echo '<pre>';
                        // print_r($splitArr);die;

                        if ($params['currency_type'] == "1") {

                            // echo "<code>masuk1 = $data</code><BR>"; die;
                            // echo '<pre>';
                            // print_r($params['flselect_own']);die;
                            // foreach($params['flselect_own'] as $i => $val)
                            // {
                            //     if($val == "1")
                            //     {
                            //         $arr_select_account_number = explode("|",$select_account_number[$i]);
                            //         $account_number .= $arr_select_account_number[0].',';
                            //         $account_name .= $arr_select_account_number[1].',';
                            //         $amount .= $params['flamount'][$i].',';
                            //     }
                            //     else if($val == "2")
                            //     {
                            //         $account_number .= $select_account_number[$i].',';
                            //         $account_name .= $flname_manual[$i].',';
                            //         $amount .= $params['flamount'][$i].',';
                            //     }
                            // }

                            // echo "<code>account_number = $account_number</code><BR>"; die;
                            // if($params['select_own'] == "1")
                            // {
                            // echo "<code>masuk1 = $data</code><BR>"; die;
                            $select_account_number = $params['flselect_account_number'];
                            $account_number = '';
                            $account_name = '';
                            $amount = '';
                            for ($i = 0; $i <= count($params['flselect_account_number']); $i++) {
                                $arr_select_account_number = explode("|", $select_account_number[$i]);
                                $account_number .= $arr_select_account_number[0] . ',';
                                $account_name .= $arr_select_account_number[1] . ',';
                                $amount .= $params['flamount'][$i] . ',';
                            }
                            $account_number = substr($account_number, 0, -1);
                            $account_name = substr($account_name, 0, -1);
                            $amount = substr($amount, 0, -1);

                            // }
                            // else
                            // {
                            //     // echo "<code>masuk2 = $data</code><BR>"; die;
                            //     $select_account_number = $params['flselect_account_manual'];
                            //     $flname_manual = $params['flname_manual'];

                            //     $account_number = '';
                            //     $account_name = '';
                            //     $amount = '';
                            //     for($i=0;$i<=count($params['flselect_account_manual']);$i++){
                            //         $account_number .= $select_account_number[$i].',';
                            //         $account_name .= $flname_manual[$i].',';
                            //         $amount .= $params['flamount'][$i].',';
                            //     }
                            //     $account_number = substr($account_number, 0, -1);
                            //     $account_name = substr($account_name, 0, -1);
                            //     $amount = substr($amount, 0, -1);
                            // }


                            $acct = $params['acct'];
                        } elseif ($params['currency_type'] == "2") {
                            $select_account_numberUSD = $params['select_account_numberUSD'];
                            for ($i = 0; $i <= count($params['select_account_numberUSD']); $i++) {
                                $arr_account_numberUSD = explode("|", $select_account_numberUSD[$i]);
                                $account_number .= $arr_account_numberUSD[0] . ',';;
                                $account_name .= $arr_account_numberUSD[1] . ',';;
                            }

                            $acct = $params['acctUSD'];
                        }

                        $fileTypeMessage = explode('/', $fileType);
                        $fileType =  $fileTypeMessage[1];
                        $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                        $extensionValidator->setMessage("Extension file must be *.pdf");

                        $maxFileSize = "1024000";
                        $size = number_format($size);

                        $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                        $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                        $adapter->setValidators(array($extensionValidator, $sizeValidator));

                        if (!empty($fileInfo['uploadSource']["name"])) {

                            if ($adapter->isValid()) {

                                if ($adapter->receive()) {
                                    // echo "<code>masuk = $data</code>"; die;	
                                    $file_name = $fileInfo['uploadSource']["name"];
                                    // unlink($attahmentDestination.$data['BG_UNDERLYING_DOC']); 

                                } else {
                                    $errmsgs = $adapter->getMessages();
                                    foreach ($errmsgs as $key => $val) {
                                        $errfield = "error_" . $fieldname;
                                        $errorArray[$errfield] = $val;
                                    }
                                    $this->view->error = true;
                                    $this->view->err_msg = $errmsgs;
                                    // echo '<pre>';
                                    // echo "<code>errors1 = $errors</code>"; die;	
                                    // print_r($errmsgs);die;
                                }
                            } else {
                                $this->view->error = true;
                                $errors = $this->language->_('Extension file must be *.jpg /.jpeg / .pdf');
                                $this->view->err_msg = $errors;
                                // echo '<pre>';
                                // echo "<code>errors2 new= $errors</code>"; die;	
                                // print_r($errors);die;
                            }
                        } else {
                            $file_name = $data['BG_UNDERLYING_DOC'];
                        }

                        // echo '<pre>';
                        // print_r($fileInfo);die;

                        // echo "<code>file_name = $file_name</code>"; die;
                        if ($this->_request->isPost()) {

                            $resultdata = $this->_db->fetchRow(
                                $this->_db->select()
                                    ->from(array('TEMP_BANK_GUARANTEE'))
                                    ->where("BG_REG_NUMBER=?", $this->_getParam('bgnumb'))->order("BG_REG_NUMBER DESC")->limit(1)
                            );

                            $historyInsert = array(
                                'DATE_TIME'             => date('Y-m-d H:i:s'),
                                'BG_REG_NUMBER'                 => $this->_getParam('bgnumb'),
                                'USER_LOGIN'         => $this->_userIdLogin,
                                'CUST_ID'         => $this->_custIdLogin,
                                'HISTORY_STATUS' => $resultdata['BG_STATUS'],
                            );
                            $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                            $change_id = $this->suggestionWaitingApproval('Submission Cancelled', 'Waiting for Cancellation Approval', strtoupper($this->_changeType['code']['new']), null, 'T_BANK_GUARANTEE_HISTORY', 'TEMP_BANK_GUARANTEE', $zf_filter_input->BG_PURPOSE_TXT, 'Submission Cancelled', $this->_custIdLogin);



                            $updatedata['BG_STATUS'] = 13;
                            $wheredata['BG_REG_NUMBER = ?'] = $this->_getParam('bgnumb');
                            // var_dump($wheredata);die;

                            $query = $this->_db->update("TEMP_BANK_GUARANTEE", $updatedata, $wheredata);

                            $this->_redirect("ongoingsubmission");
                        }
                        try {

                            $this->_db->beginTransaction();

                            if ($toa == '1') {
                                // $insertArr = array(   
                                //     'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'],$zf_filter_input->BG_PURPOSE_TXT), 
                                //     'CUST_CP'                           => $zf_filter_input->CUST_CP_TXT,
                                //     'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER_TXT,
                                //     'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL_TXT,
                                //     'BG_AMOUNT'                         => $zf_filter_input->bank_amount_txt, 
                                //     'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE_TXT,
                                //     'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP_TXT,
                                //     'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT_TXT,
                                //     'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL_TXT,
                                //     'SERVICE'                           => $zf_filter_input->SERVICE_TXT,
                                //     'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE_TXT,
                                //     'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER_TXT,
                                //     'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE_TXT,
                                //     'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                //     'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE_TXT,
                                //     'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT, 
                                //     'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                //     'BG_LANGUAGE'                       => $zf_filter_input->BG_LANGUAGE,
                                //     'BG_UNDERLYING_DOC'                 => $file_name,   
                                //     'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                //     'BG_UPDATEDBY'                      => $this->_userIdLogin, 
                                // );


                                $insertArr = array(
                                    'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'], $zf_filter_input->BG_PURPOSE_TXT),
                                    'ACCT_NO'                           => $data['ACCT_NO'],
                                    'BG_NUMBER'                         => $data['BG_NUMBER'],
                                    'BG_SUBJECT'                        => $data['BG_SUBJECT'],
                                    'CUST_ID'                           => $this->_custIdLogin,
                                    'CUST_CP'                           => $zf_filter_input->CUST_CP_TXT,
                                    'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER_TXT,
                                    'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL_TXT,
                                    'BG_STATUS'                         => $data['BG_STATUS'],
                                    'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE_TXT,
                                    'USAGE_PURPOSE_DESC'                => $data['USAGE_PURPOSE_DESC'],
                                    'RECIPIENT_NAME'                    => $data['RECIPIENT_NAME'],
                                    'RECIPIENT_ADDRES'                  => $data['RECIPIENT_ADDRES'],
                                    'RECIPIENT_CITY'                    => $data['RECIPIENT_CITY'],
                                    'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP_TXT,
                                    'RECIPIENT_OFFICE_NUMBER'           => $data['RECIPIENT_OFFICE_NUMBER'],
                                    'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT_TXT,
                                    'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL_TXT,

                                    'SERVICE'                           => $zf_filter_input->SERVICE_TXT,
                                    'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE_TXT,
                                    'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER_TXT,
                                    'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE_TXT,
                                    'GT_DOC_OTHER'                      => $data['GT_DOC_OTHER'],
                                    'GUARANTEE_TRANSACTION'             => $data['GUARANTEE_TRANSACTION'],
                                    'SP_OBLIGEE_CODE'                   => $data['SP_OBLIGEE_CODE'],
                                    'FILE'                              => $data['FILE'],
                                    'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($zf_filter_input->bank_amount_txt),
                                    'TIME_PERIOD_START'                 => $data['TIME_PERIOD_START'],
                                    'TIME_PERIOD_END'                   => $zf_filter_input->updatedate,

                                    'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE_TXT,
                                    // 'COUNTER_WARRANTY_TYPE'             => $data['COUNTER_WARRANTY_TYPE'], 
                                    'COUNTER_WARRANTY_ACCT_NO'          => rtrim($account_number, ","),
                                    'COUNTER_WARRANTY_ACCT_NAME'        => rtrim($account_name, ","),
                                    'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney(rtrim($amount, ",")),

                                    'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                    'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                    'BG_LANGUAGE'                       => $zf_filter_input->BG_LANGUAGE,

                                    'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                    'BG_CREATEDBY'                      => $this->_userIdLogin,
                                    'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                    'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                    'BG_REJECT_NOTES'                   => $data['BG_REJECT_NOTES'],
                                    'BG_APPROVE_DOC'                    => $data['BG_APPROVE_DOC'],
                                    'REASON'                            => $data['REASON'],
                                    'BG_CLAIM_PERIOD'                   => $data['BG_CLAIM_PERIOD'],
                                    'BG_CLAIMEDBY'                      => $data['BG_CLAIMEDBY'],
                                    'BG_CLAIM_NOTES'                    => $data['BG_CLAIM_NOTES'],
                                    'BG_CANCELEDBY'                     => $data['BG_CANCELEDBY'],
                                    'BG_CANCEL_NOTES'                   => $data['BG_CANCEL_NOTES'],
                                    'BG_UNDERLYING_DOC'                 => $data['BG_UNDERLYING_DOC'],
                                    'BG_INSURANCE_CODE'                 => $data['BG_INSURANCE_CODE'],
                                    'BG_BRANCH'                         => $data['BG_BRANCH'],
                                    'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT,
                                    'PROVISION_FEE'                     => $data['PROVISION_FEE'],
                                    'ADM_FEE'                           => $data['ADM_FEE'],
                                    'STAMP_FEE'                         => $data['STAMP_FEE'],
                                    'IS_AMENDMENT'                      => 1,

                                );
                                // echo '<pre>';
                                // print_r($insertArr);die;
                                $this->_db->insert('T_BANK_GUARANTEE', $insertArr);

                                if ($zf_filter_input->COUNTER_WARRANTY_TYPE_TXT == '1') {
                                    // echo "<code>COUNTER_WARRANTY_TYPE_TXT = 1</code>"; die;	   
                                    // $this->_db->delete('T_BANK_GUARANTEE_SPLIT','BG_NUMBER = '.$this->_db->quote($numb));

                                    foreach ($splitArr as $ky => $vl) {
                                        $tmparrDetail = array(
                                            'BG_NUMBER'         => $insertArr['BG_REG_NUMBER'],
                                            'ACCT'              => $vl['acct'],
                                            'BANK_CODE'         => $vl['bank_code'],
                                            'NAME'              => $vl['acct_name'],
                                            'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['amount'], ",")),
                                            'FLAG'              => $vl['flag']
                                        );

                                        // echo '<pre>';
                                        // print_r($tmparrDetail);die;
                                        $this->_db->insert('T_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                    }
                                }
                                // else
                                // {
                                //     echo "<code>COUNTER_WARRANTY_TYPE_TXT = $COUNTER_WARRANTY_TYPE_TXT</code>"; die;
                                // }

                            } else if ($toa == '2') {

                                // echo '<pre>';
                                // print_r($zf_filter_input->updatedate);die;
                                $insertArr = array(
                                    'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'], $zf_filter_input->BG_PURPOSE_TXT),
                                    'ACCT_NO'                           => $data['ACCT_NO'],
                                    'BG_NUMBER'                         => $data['BG_NUMBER'],
                                    'BG_SUBJECT'                        => $data['BG_SUBJECT'],
                                    'CUST_ID'                           => $data['CUST_ID'],
                                    'CUST_CP'                           => $data['CUST_CP'],
                                    'CUST_EMAIL'                        => $data['CUST_EMAIL'],
                                    'CUST_CONTACT_NUMBER'               => $data['CUST_CONTACT_NUMBER'],
                                    'BG_STATUS'                         => $data['BG_STATUS'],
                                    'USAGE_PURPOSE'                     => $data['USAGE_PURPOSE'],
                                    'USAGE_PURPOSE_DESC'                => $data['USAGE_PURPOSE_DESC'],
                                    'RECIPIENT_NAME'                    => $data['RECIPIENT_NAME'],
                                    'RECIPIENT_ADDRES'                  => $data['RECIPIENT_ADDRES'],
                                    'RECIPIENT_CITY'                    => $data['RECIPIENT_CITY'],
                                    'RECIPIENT_CP'                      => $data['RECIPIENT_CP'],
                                    'RECIPIENT_OFFICE_NUMBER'           => $data['RECIPIENT_OFFICE_NUMBER'],
                                    'RECIPIENT_EMAIL'                   => $data['RECIPIENT_EMAIL'],
                                    'RECIPIENT_CONTACT'                 => $data['RECIPIENT_CONTACT'],
                                    'SERVICE'                           => $data['SERVICE'],
                                    'GT_DOC_TYPE'                       => $data['GT_DOC_TYPE'],
                                    'GT_DOC_OTHER'                      => $data['GT_DOC_OTHER'],
                                    'GT_DOC_NUMBER'                     => $data['GT_DOC_NUMBER'],
                                    'GT_DOC_DATE'                       => $data['GT_DOC_DATE'],
                                    'GUARANTEE_TRANSACTION'             => $data['GUARANTEE_TRANSACTION'],
                                    'SP_OBLIGEE_CODE'                   => $data['SP_OBLIGEE_CODE'],
                                    'FILE'                              => $data['FILE'],
                                    'TIME_PERIOD_START'                 => $data['TIME_PERIOD_START'],
                                    'TIME_PERIOD_END'                   => $zf_filter_input->updatedate,

                                    'COUNTER_WARRANTY_TYPE'             => $data['COUNTER_WARRANTY_TYPE'],
                                    'COUNTER_WARRANTY_ACCT_NO'          => $data['COUNTER_WARRANTY_ACCT_NO'],
                                    'COUNTER_WARRANTY_ACCT_NAME'        => $data['COUNTER_WARRANTY_ACCT_NAME'],
                                    'COUNTER_WARRANTY_AMOUNT'           => $data['COUNTER_WARRANTY_AMOUNT'],

                                    'FEE_CHARGE_TO'                     => $data['FEE_CHARGE_TO'],
                                    'BG_FORMAT'                         => $data['BG_FORMAT'],
                                    'BG_LANGUAGE'                       => $data['BG_LANGUAGE'],

                                    'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                    'BG_CREATEDBY'                      => $this->_userIdLogin,
                                    'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                    'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                    'BG_REJECT_NOTES'                   => $data['BG_REJECT_NOTES'],
                                    'BG_APPROVE_DOC'                    => $data['BG_APPROVE_DOC'],
                                    'REASON'                            => $data['REASON'],
                                    'BG_CLAIM_PERIOD'                   => $data['BG_CLAIM_PERIOD'],
                                    'BG_CLAIMEDBY'                      => $data['BG_CLAIMEDBY'],
                                    'BG_CLAIM_NOTES'                    => $data['BG_CLAIM_NOTES'],
                                    'BG_CANCELEDBY'                     => $data['BG_CANCELEDBY'],
                                    'BG_CANCEL_NOTES'                   => $data['BG_CANCEL_NOTES'],
                                    'BG_UNDERLYING_DOC'                 => $data['BG_UNDERLYING_DOC'],
                                    'BG_INSURANCE_CODE'                 => $data['BG_INSURANCE_CODE'],
                                    'BG_BRANCH'                         => $data['BG_BRANCH'],
                                    'BG_PUBLISH'                        => $data['BG_PUBLISH'],
                                    'PROVISION_FEE'                     => $data['PROVISION_FEE'],
                                    'ADM_FEE'                           => $data['ADM_FEE'],
                                    'STAMP_FEE'                         => $data['STAMP_FEE'],
                                    'IS_AMENDMENT'                      => 1,

                                );

                                $this->_db->insert('T_BANK_GUARANTEE', $insertArr);

                                if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

                                    foreach ($bgdatasplit as $ky => $vl) {
                                        $tmparrDetail = array(
                                            'BG_NUMBER'         => $insertArr['BG_REG_NUMBER'],
                                            'ACCT'              => $vl['ACCT'],
                                            'BANK_CODE'         => $vl['BANK_CODE'],
                                            'NAME'              => $vl['NAME'],
                                            'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['AMOUNT'], ",")),
                                            'FLAG'              => $vl['FLAG']
                                        );

                                        // echo '<pre>';
                                        // print_r($tmparrDetail);die;
                                        $this->_db->insert('T_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                    }
                                }
                            }

                            // echo '<pre>';
                            // print_r($insertArr);die;
                            // $where = array('BG_REG_NUMBER = ?' => $numb);
                            // $query = $this->_db->update ( "T_BANK_GUARANTEE", $updArr, $where ); 

                            $this->_db->commit();
                            $this->setbackURL('/' . $this->_request->getModuleName() . '/');
                            $this->_redirect('/notification/success');
                        } catch (Exception $e) {
                            $this->_db->rollBack();
                            $this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
                        }
                    } else {
                        // echo "<code>masuk2 = $data</code><BR>"; die;	
                        $error = $zf_filter_input->getMessages();
                        // $error = true;


                        //format error utk ditampilkan di view html 
                        $errorArray = null;
                        foreach ($error as $keyRoot => $rowError) {
                            foreach ($rowError as $errorString) {
                                $errorArray[$keyRoot] = $errorString;
                            }
                        }
                        $this->view->report_msg = $errorArray;
                    }
                }


                $download = $this->_getParam('download');
                $BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
                //print_r($edit);die;
                if ($BG_APPROVE_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/bg/';
                    //var_dump($attahmentDestination.$data['BG_APPROVE_DOC']);die;
                    $this->_helper->download->file($data['BG_APPROVE_DOC'], $attahmentDestination . $data['BG_APPROVE_DOC']);
                }
            }
        }
    }

    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function Terbilangen($nilai)
    {
        $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 20 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 100) {
            return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilangen($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilangen($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function generateTransactionID($branch, $bgtype)
    {


        // echo "<code>branch = $branch | bgtype = $bgtype  </code><BR>"; die;	
        $currentDate = date("Ymd");
        $seqNumber   = Application_Helper_General::get_rand_id(4);
        $trxId = $seqNumber . $branch . '01' . $bgtype;

        return $trxId;
    }


    public function cancelAction()
    {
        $params = $this->_request->getParams();


        $updatedata['BG_STATUS'] = 99;
        $wheredata['BG_REG_NUMBER = ?'] = $param['BG_REG_NUMBER'];
        $query = $this->_db->update("TEMP_BANK_GUARANTEE", $updatedata, $wheredata);


        $this->_redirect("ongoingsubmission");
        //Zend_Debug::dump($_SESSION['import_predefbenef']);die;
    }
}
