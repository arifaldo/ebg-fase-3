<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';

class airlines_TestschedulerController extends airlines_Model_Airlines
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');  

		$filename = basename(__FILE__);

		$curTime = date('Y-m-d H:i');

		$data = $this->_db->select()
				->FROM	(array('A' => 'M_AIRLINES_SETTINGS'),array('*'))
				->WHERE('A.EF_DATE >= ?', $curTime.':00')
				->WHERE('A.EF_DATE <= ?', $curTime.':59')
				->WHERE('A.STATUS = 1')
				->query()->fetchAll();

		if (!empty($data)) {

			$clientUser  =  new SGO_Soap_ClientUser(); 

			// $currentBalance = '35000000';
			// $successCheckBalance = true;
			echo "<pre>";
			$count = 1;
			foreach ($data as $key => $value) {

				$transaction_info = json_decode($value['TRANSACTION_INFO'], 1);

				//----------------------------------------kalau travelagentnya bs lebih dari satu dalam suatu settingan-------------------------------------
				$currentBalance = 0;
				$successCheckBalance = 0;
				$travelagentArr = array();
				foreach ($transaction_info as $row => $rowVal) {
					if (!in_array($rowVal['travelagentid'], $travelagentArr)) {
						$paramObj = array(
							'airline_code' => $value['AIRLINE_CODE'],
							'AUTH_USER' => 'travel',
							'AUTH_PASS' => 'test',
							'TRAVELAGENT_ID' => $rowVal['travelagentid']
						);

						//check balance diulang 4x sampai dapat sukses
						
						for($i = 0; $i < 4; $i++){
							$success = $clientUser->callapi('airlinesDepositInquiry',$paramObj,'airlines/agent/info');
					
							if($clientUser->isTimedOut()){
								$returnStruct = array(
										'ResponseCode'	=>'XT',
										'ResponseDesc'	=>'Service Timeout',					
										'Cif'			=>'',
										'AccountList'	=> '',
								);
							}else{
								
								$result  = $clientUser->getResult();

								if($result->error_code == '0000'){
									$currentBalance += $result->account_info->available_credit;
									$successCheckBalance++;
									break;
								}
							}
						}
						array_push($travelagentArr, $rowVal['travelagentid']);
					}
				}

				//----------------------------------------end kalau travelagentnya bs lebih dari satu dalam suatu settingan-------------------------------------

				//----------------------------------------kalau travelagentnya hanya satu dalam suatu settingan-------------------------------------
				// $paramObj = array(
				// 	'airline_code' => $value['AIRLINE_CODE'],
				// 	'AUTH_USER' => 'travel',
				// 	'AUTH_PASS' => 'test',
				// 	'TRAVELAGENT_ID' => $transaction_info[0]['travelagentid']
				// );

				// //check balance diulang 4x sampai dapat sukses
				// $successCheckBalance = false;
				// $currentBalance = '';
				// for($i = 0; $i < 4; $i++){
				// 	$success = $clientUser->callapi('airlinesDepositInquiry',$paramObj,'airlines/agent/info');
			
				// 	if($clientUser->isTimedOut()){
				// 		$returnStruct = array(
				// 				'ResponseCode'	=>'XT',
				// 				'ResponseDesc'	=>'Service Timeout',					
				// 				'Cif'			=>'',
				// 				'AccountList'	=> '',
				// 		);
				// 	}else{
						
				// 		$result  = $clientUser->getResult();

				// 		if($result->error_code == '0000'){
				// 			$successCheckBalance = true;
				// 			$currentBalance = $result->account_info->available_credit;
				// 			break;
				// 		}
				// 	}
				// }
				//----------------------------------------end kalau travelagentnya hanya satu dalam suatu settingan-------------------------------------

				if ($successCheckBalance == count($travelagentArr)) {

					if ($currentBalance < $value['MIN_BALANCE']) {

						$dividedby = $transaction_info[0]['type'];

						$balanceAfter = $currentBalance;

						foreach ($transaction_info as $key2 => $value2) {

							$amount = '';
							if ($dividedby == 'p') {
								$amount = (($value['AMOUNT_TOPUP'] * $value2['value']) / 100);
							}
							else{
								$amount = $value2['value'];
							}

							$description = 'Auto Topup from Source Account: '.$value2['sourceaccount'].' to Travel agent id: '.$value2['travelagentid'].' of IDR '.Application_Helper_General::displayMoney($amount);

							$balanceAfter += $amount;

							$transactionId = $this->generateTransactionID($value);

							$data = array(
								'TRANSACTION_ID'		=> $transactionId,
								'CUST_ID'				=> $value['CUST_ID'],
								'AIRLINE_CODE'			=> $value['AIRLINE_CODE'],
								'BANK_CODE'				=> $value2['bankcode'],
								'SOURCE_ACCOUNT'		=> $value2['sourceaccount'],
								'TRAVELAGENT_ID' 		=> $value2['travelagentid'],
								'BALANCE_PREV' 			=> $currentBalance,
								'CURRENT_BALANCE' 		=> $balanceAfter,
								'TOPUP_AMOUNT' 			=> $amount,
								'DESCRIPTION' 			=> $description,
								'DATE_TIME' 			=> date('Y-m-d H:i:s'),
								'STATUS'	 			=> 2, // 1=checking balance, 2=autotopup, 3=failed check balance, 4=failed autotopup
								'BALANCE_CYCLE'			=> $value['BALANCE_CYCLE']
							);	



							$paramObj = array(
								'airline_code' => $value['AIRLINE_CODE'],
								// 'AUTH_USER' => $transaction_info[0]['travelagentid'],
								'AUTH_USER' => 'travel',
								'AUTH_PASS' => 'test',
								'TRAVELAGENT_ID' => $value2['travelagentid'],
								'TRANSACTION_ID' => $transactionId,
								'AMOUNT' => $amount,
								'ccy' => 'IDR',
							);

							$successTopup = false;
							$success = $clientUser->callapi('airlinesDepositTopup',$paramObj,'airlines/topup/commit');
					
							if($clientUser->isTimedOut()){
								$returnStruct = array(
										'ResponseCode'	=>'XT',
										'ResponseDesc'	=>'Service Timeout',					
										'Cif'			=>'',
										'AccountList'	=> '',
								);
							}else{
								
								$result  = $clientUser->getResult();

								print_r($result).'<br>';

								if($result->error_code == '0000'){
									$successTopup = true;
								}
							}

							if ($successTopup) {
								$this->insertAutotopupLog($data);
								$currentBalance = $balanceAfter;
							}
							else{
								$data['CURRENT_BALANCE'] = $currentBalance;
								$data['TOPUP_AMOUNT'] = null;

								$description = 'Failed Auto Topup from Source Account: '.$value2['sourceaccount'].' to Travel agent id: '.$value2['travelagentid'].' of IDR '.Application_Helper_General::displayMoney($amount).' | '.$result->response_desc;
								$data['DESCRIPTION'] = $description;
								$data['STATUS'] = 4; // 1=checking balance, 2=autotopup, 3=failed check balance, 4=failed autotopup
								$this->insertAutotopupLog($data);
							}

							echo $count.'.) '.$description.'<br><br>';
						}

						$this->updateTimeAutotopupLog($data);
					}
					else{

						//kalau travel agent id bisa banyak dlm suatu setting
						$travelagentid = implode(',', $travelagentArr); 

						//kalau travel agent hanya satu dlm suatu setting
						// $travelagentid = $transaction_info[0]['travelagentid'];

						$description = 'Checking balance. Balance: IDR '.Application_Helper_General::displayMoney($currentBalance);

						$data = array(
							'TRANSACTION_ID'		=> null,
							'CUST_ID'				=> $value['CUST_ID'],
							'AIRLINE_CODE'			=> $value['AIRLINE_CODE'],
							'BANK_CODE'				=> null,
							'SOURCE_ACCOUNT'		=> null,
							'TRAVELAGENT_ID' 		=> $travelagentid,
							'BALANCE_PREV' 			=> null,
							'CURRENT_BALANCE' 		=> $currentBalance,
							'TOPUP_AMOUNT' 			=> null,
							'DESCRIPTION' 			=> $description,
							'DATE_TIME' 			=> date('Y-m-d H:i:s'),
							'STATUS'	 			=> 1, // 1=checking balance, 2=autotopup, 3=failed check balance, 4=failed autotopup
							'BALANCE_CYCLE'			=> $value['BALANCE_CYCLE']
						);

						$this->insertAutotopupLog($data);

						$this->updateTimeAutotopupLog($data);	

						echo $count.'.) '.$description.'<br><br>';
					}	
				}
				else{

					$description = 'Failed check balance airlines:  '.$value['AIRLINE_CODE'].' of customer id: '.$value['CUST_ID'];

					//kalau travel agent id bisa banyak dlm suatu setting
					$travelagentid = implode(',', $travelagentArr); 

					//kalau travel agent hanya satu dlm suatu setting
					// $travelagentid = $transaction_info[0]['travelagentid'];

					$data = array(
						'CUST_ID'				=> $value['CUST_ID'],
						'AIRLINE_CODE'			=> $value['AIRLINE_CODE'],
						'TRAVELAGENT_ID' 		=> $travelagentid,
						'DESCRIPTION' 			=> $description,
						'DATE_TIME' 			=> date('Y-m-d H:i:s'),
						'STATUS'	 			=> 3, // 1=checking balance, 2=autotopup, 3=failed check balance, 4=failed autotopup
						'BALANCE_CYCLE'			=> $value['BALANCE_CYCLE']
					);

					$this->insertAutotopupLog($data);

					$this->updateTimeAutotopupLog($data);	

					echo $count.'.) '.$description.'<br><br>';
					// Application_Helper_General::cronLog($filename, 'Failed check balance airlines:  '.$value['AIRLINE_CODE'].' of customer id: '.$value['CUST_ID'], 0);
				}

				$count++;
			}
		}
		die('end');
	}

	public function generateTransactionID($data){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(9));
		$trxId = 'AT'.$currentDate.$seqNumber;

		return $trxId;
	}

	public function insertAutotopupLog($data){

		unset($data['BALANCE_CYCLE']);

		try {
			//insert T_AIRLINES_LOG
			$this->_db->insert('T_AIRLINES_LOG',$data);

			// Application_Helper_General::cronLog($filename, 'Success insert to T_AIRLINES_LOG', 1);
		} catch (Exception $e) {
			die($e);
			// Application_Helper_General::cronLog($filename, 'Failed insert to T_AIRLINES_LOG', 0);
		}
	}

	public function updateTimeAutotopupLog($data){

		if ($data['BALANCE_CYCLE'] == '30m') {
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+30 minutes"));
		}
		else if($data['BALANCE_CYCLE'] == '1h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+1 hour"));
		}
		else if($data['BALANCE_CYCLE'] == '2h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+2 hour"));
		}
		else if($data['BALANCE_CYCLE'] == '3h'){
			$EF_DATE = date('Y-m-d H:i:s', strtotime("+3 hour"));
		}

		$updateData = array('EF_DATE' => $EF_DATE);
		$where['CUST_ID = ?'] = $data['CUST_ID'];
		$where['AIRLINE_CODE = ?'] = $data['AIRLINE_CODE'];

		try {
			//update efdate M_AIRLINES_SETTINGS
			$this->_db->update('M_AIRLINES_SETTINGS', $updateData, $where);
			// Application_Helper_General::cronLog($filename, 'Success insert to T_AIRLINES_LOG', 1);
		} catch (Exception $e) {
			die($e);
			// Application_Helper_General::cronLog($filename, 'Failed insert to T_AIRLINES_LOG', 0);
		}
	}
}
