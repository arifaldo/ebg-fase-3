<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'General/CustomerUser.php';
require_once('Crypt/AES.php');

class pass_IndexController extends Application_Main
{
	public function initController()
	{
		$this->_helper->_layout->setLayout('newlayout');
	}

	public function indexAction()
	{

		if (Zend_Auth::getInstance()->hasIdentity()) {
			$this->_redirect('/home/dashboard');
		}
		// $cust = urldecode($this->getRequest()->getParam('cust_id'));
		$user = $this->getRequest()->getParam('user_id');
		//print_r($user);die;
		$code = urldecode($this->getRequest()->getParam('code'));
		// print_r($this->getRequest()->getParams());die;

		$key = md5('permataNet92');
		// $pass = ($zf_filter_input->password);

		// $encrypt = new Crypt_AESMYSQL ();
		$encrypt = new Crypt_AES();
		// echo "<pre>";
		// print_r($encrypt);die;
		// $decrypcust_id =  ( $encrypt->decrypt ( $cust ) );

		$decrypuser_id  =  ($this->sslDec($user));

		// print_r($decrypuser_id);die;
		// echo '<br/>';
		// print_r($cust_id);
		// die;
		// $decrypuser_id = str_replace('\0', '', $this->decrypt($user, "permataNet92"));
		// $decrypcust_id = str_replace('\0', '', $this->decrypt($cust, "permataNet92"));
		// $decrypuser_id = $this->decrypt('ÒÊ¹å´¶', "permataNet92");

		// $decrypcust_id = $this->decrypt($cust, "permataNet92");
		$settings =  new Settings();
		// print_r($user);die;
		$maxLengthPassword = $settings->getSetting('maxbpassword');
		$minLengthPassword = $settings->getSetting('minbpassword');
		$keepMaxPasswordHistory = $settings->getSetting('password_history');
		$this->view->maxlengthpass = $maxLengthPassword;
		$this->view->minlengthpass = $minLengthPassword;

		// $model = new poss_Model_Pass();
		// print_r($decrypcust_id);die;
		$select = $this->_db->select()
			->from(array('A' => 'M_BUSER'));
		// ->joinLeft(array('G' => 'M_GROUPING'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'));
		// $select->where("A.CUST_ID = ".str_replace('\0', '',$this->_db->quote($decrypcust_id)));
		$select->where("A.BUSER_ID = " . str_replace('\0', '', $this->_db->quote($decrypuser_id)));
		$select->where("A.BUSER_CODE = ?", $code);
		$select->where("DATE(A.BUSER_DATEPASS) >= DATE(NOW())");
		// echo $select;die;
		$data = $this->_db->fetchOne($select);
		// print_r($data);die;
		if (!empty($data)) {
			$this->view->accDisplay = false;
			if ($this->_request->isPost()) {
				//customer data
				$filters = array(
					'pass'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'confirm_pass'    => array('StripTags', 'StringTrim', 'HtmlEntities')
				);

				$validators =  array(
					'pass'                => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty'),
						)
					),
					'confirm_pass'                => array(
						'NotEmpty',
						'messages' => array(
							$this->language->_('Can not be empty'),
						)
					),
				);



				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
				//validasi multiple email
				$errDesc = array();
				if (!$zf_filter_input->pass)
					$errDesc['conpass'] = "Error: Confirm New Password cannot be left blank. Please correct it.";
				elseif ($zf_filter_input->pass <> $zf_filter_input->confirm_pass)
					$errDesc['conpass'] = "Error: Sorry, but the two passwords you entered are not same.";
				elseif (!$zf_filter_input->confirm_pass)
					$errDesc['newpass'] = "Error: New Password cannot be left blank. Please correct it.";
				elseif ((strlen($zf_filter_input->pass) < $minLengthPassword) || (strlen($zf_filter_input->pass) > $maxLengthPassword)) {
					//$errDesc['newpass'] = "Error: Minimum char ".$minLengthPassword." and maximum char ".$maxLengthPassword." for New Password length. Please correct it.";
					//$errDesc['newpass'] = "Error: Minimum char ".$minLengthPassword." and maximum char ".$maxLengthPassword." for New Password length. Please correct it.";
					// print_r($minLengthPassword);echo 'here';
					// print_r($maxLengthPassword);echo 'zz';
					$errDesc['newpass'] = $this->language->_('Error: Minimum char') . " " . $minLengthPassword . " " . $this->language->_('and maximum char') . " " . $maxLengthPassword . " " . $this->language->_('for New Password length. Please correct it') . ".";
				} elseif (Application_Helper_General::checkPasswordStrength($zf_filter_input->pass) < 3)
					$errDesc['newpass'] = "Error: New Password must containt at least one uppercase character, one lowercase character and one number.Please correct it.";
				// elseif (! ($validator->isValid($zf_filter_input->pass)) ) {
				// $errDesc['newpass'] = "Error: New Password may not contain whitespaces or special characters";
				// }

				if ($zf_filter_input->isValid() && empty($errDesc)) {

					//   		$custData = array();
					// foreach($validators as $key=>$value)
					// {
					//    	$custData[strtoupper($key)] = $zf_filter_input->getEscaped($key);	
					// }

					$CustomerUser =  new CustomerUser($decrypcust_id, $decrypuser_id);
					$result = array();
					$failed = 0;

					if (count($errDesc) > 0) $failed = 1;
					$result = $CustomerUser->changePasswordNewBackend(null, $zf_filter_input->pass, $failed);
					// print_r($result);die;
					if ((is_array($result)  && $result !== true) ||  $failed == 1) {
						if (count($result) > 0) {
							foreach ($result as $key => $value) {
								$errDesc[$key] = $value;
							}
						}
						$this->view->error 	= true;
						$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';

						$this->view->errDesc = $errDesc;
					} else {
						// die;
						Application_Helper_General::writeLog('CHMP', 'Change My Password Cust ID : ' . $decrypcust_id . ',User ID : ' . $decrypuser_id);
						// $this->setbackURL('/home');
						$this->_redirect('/pass/index/success');
					}



					// $cust_id = $tempID;
					$toUpload = true;
				} else {
					$toUpload = false;
					$this->view->error = 1;

					$this->view->errMsg = 'Error in processing form values. Please correct values and re-submit';

					foreach ($validators as $key => $value) {
						$this->view->$key = ($zf_filter_input->isValid($key)) ? $zf_filter_input->getEscaped($key) : $this->_getParam($key);
					}


					//   	$error = $zf_filter_input->getMessages();

					// foreach($error as $keyRoot => $rowError)
					// {
					// 	foreach($rowError as $errorString)
					// 	{
					// 	   	  $keyname = "error_" . $keyRoot;
					// 		  $this->view->$keyname = $this->language->_($errorString);
					// 	}
					// }
					foreach ($errDesc as $key => $value) {
						$keyname = "error_" . $key;
						// print_r($keyname);die;
						$this->view->$keyname = $this->language->_($value);
					}
				}

				//customer data -- ends




			} else {


				$this->view->accDisplay = false;
				// $this->view->userDisplay = 'none';
				// $this->view->accDisplay = 'none';
			}
		} else {
			// die;
			$this->view->accDisplay = true;
		}
	}

	public function sslPrm()
	{
		return array("6a1f325be4c0492063e83a8cb2cb9ae7", "IV (optional)", "aes-128-cbc");
	}

	public function sslDec($msg)
	{
		list($pass, $iv, $method) = $this->sslPrm();
		return trim(urldecode(openssl_decrypt(urldecode($msg), $method, $pass, false, $iv)));
	}

	// function decrypt($encrypted_string, $encryption_key) {
	//     $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	//     $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	//     $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
	//     return $decrypted_string;
	// }
	function decrypt($encrypted_string)
	{
		$dirty = array("+", "/", "=");
		$clean = array("_PLUS_", "_SLASH_", "_EQUALS_");

		$string = base64_decode(str_replace($clean, $dirty, $encrypted_string));

		$decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, 'permataNet92', $string, MCRYPT_MODE_ECB, 'permataNet92');

		return $decrypted_string;
	}

	public function successAction()
	{
	}

	public function activationAction()
	{
		$user = $this->getRequest()->getParam('user_id');
		//print_r($user);die;
		$code = urldecode($this->getRequest()->getParam('code'));
		// print_r($this->getRequest()->getParams());die;

		$key = md5('permataNet92');
		// $pass = ($zf_filter_input->password);

		// $encrypt = new Crypt_AESMYSQL ();
		$encrypt = new Crypt_AES();
		// echo "<pre>";
		// print_r($encrypt);die;
		// $decrypcust_id =  ( $encrypt->decrypt ( $cust ) );

		$decrypuser_id  =  ($this->sslDec($user));

		// print_r($decrypcust_id);die;
		$select = $this->_db->select()
			->from(array('A' => 'M_BUSER'));
		// ->joinLeft(array('G' => 'M_GROUPING'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'));
		// $select->where("A.CUST_ID = ".str_replace('\0', '',$this->_db->quote($decrypcust_id)));
		$select->where("A.BUSER_ID = " . str_replace('\0', '', $this->_db->quote($decrypuser_id)));
		$select->where("A.BUSER_CODE = ?", $code);
		$select->where("DATE(A.BUSER_DATEPASS) >= DATE(NOW())");
		// echo $select;die;
		$data = $this->_db->fetchOne($select);

		$whereArr  = array('BUSER_ID = ?' => $data);
		$content = array(
			'BUSER_STATUS' 	 => 1,
			"BUSER_ISNEW" => 0
		);
		$update = $this->_db->update('M_BUSER', $content, $whereArr);

		if ($update) {
			Application_Helper_General::writeLog('CHMP', 'Aktivasi User ID : ' . $decrypuser_id);
			// $this->setbackURL('/home');
			$this->_redirect('/pass/index/success');
		}
	}
}
