<?php

/**
 * IndexController
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class Popuperror_SingleimportpopupController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->_helper->layout()->setLayout('popup');
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$type = $this->_request->getParam('type');
		$ccy = $this->_request->getParam('ccy');
		
		$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
		$data = $sessionNamespace->content;
		
		$errorMsg = $data["errorTrxMsg"][$type][$ccy];
		
		foreach($errorMsg as $key=>$val)
		{
			$errorMsg[$key] = $this->language->_('Error Line').' '.($key+1).' : '.$this->language->_($val);
		}
		
		$this->view->error 		= true;
		$this->view->report_msg	= $this->displayError($errorMsg);
	}

}

