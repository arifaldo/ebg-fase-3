<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class charges_CoaaccountrequestlistController extends Application_Main
{
	public function indexAction() 
	{
		$set = new Settings();
		$coa = $set->getSetting('charges_acct_IDR');                	
		$cek = $this->_db->FetchOne("SELECT SETTING_VALUE FROM TEMP_SETTING WHERE MODULE_ID ='COA'");
//		select()
//							->FROM (array('A' => 'TEMP_SETTING'),array('SETTING_VALUE'))
//							->WHERE ('MODULE_ID = ?','COA')
//							);
		if($cek == null){
			$this->view->cek = 0;
			
		}
		else{
			$this->view->cek = 1;
			$this->view->oldCOAAccount = $coa;
			$this->view->newCOAAccount = $cek;
		}
	}
}
