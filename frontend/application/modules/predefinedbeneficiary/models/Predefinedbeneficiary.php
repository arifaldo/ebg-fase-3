<?PHP
class predefinedbeneficiary_Model_Predefinedbeneficiary
{
	protected $_db;
	
    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
	
	function getCity()
	{
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
//		$select->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('A.PROVIDER_ID','A.PROVIDER_NAME'));		
		$select ->order('CITY_NAME ASC');
		return $this->_db->fetchall($select);
	}
	
	function getCCYId()
	{
		$select = $this->_db->select()
      	                   ->from(	array(	'M'			=>'M_MINAMT_CCY'),
									array(	'ccyid'		=>'CCY_ID') )
      	                   ->query()->fetchAll();
		
		foreach($select as $key => $val)
		{
			foreach($val as $key1 => $val1)
			{
				$result[$val1] = $val1;
			}
		}
		return $result;
	}
	
	function getTransPurposeId()
	{
		$select = $this->_db->select()
		->from(	array(	'M'			=>'M_TRANSACTION_PURPOSE'),
				array(	'ccyid'		=>'CODE') )
				->query()->fetchAll();
	
		foreach($select as $key => $val)
		{
			foreach($val as $key1 => $val1)
			{
				$result[$val1] = $val1;
			}
		}
		return $result;
	}
	
	function getCityCode($cityCode)
	{
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
//		$select->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('A.PROVIDER_ID','A.PROVIDER_NAME'));		
		$select->where('A.CITY_CODE = ?',$cityCode);
//		$select ->order('CITY_NAME ASC');
		//echo $select;die;
		return $this->_db->fetchall($select);
	}
	
	protected function getCategory()
	{

// 		$select = $this->_db->select()
// 		->from(array('A' => 'M_MASTER'),array('*'));
// 		//		$select->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('A.PROVIDER_ID','A.PROVIDER_NAME'));
// 		$select ->where('sm_key = ?','LLD');
// 		$select->where('sm_key1 = ?','CATEGORY');
// 		$select ->order('sm_key2 ASC');
// // 		print_r($select->query());die;
// 		return $this->_db->fetchall($select);
		$select = $this->_db->select()
		->from(array('A' => 'M_CITY'),array('*'));
		//		$select->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('A.PROVIDER_ID','A.PROVIDER_NAME'));
		$select ->order('CITY_NAME ASC');
		return $this->_db->fetchall($select);
	}
	
	function getCategoryCode($categoryCode)
	{
		$select = $this->_db->select()
		->from(array('A' => 'M_MASTER'),array('*'));
		$select->where('A.M_KEY2 = ?',$categoryCode);
		//print_r($select->query());die;
		return $this->_db->fetchall($select);
	}
	
	function getPurposeCode($categoryCode)
	{
		$select = $this->_db->select()
		->from(array('A' => 'M_TRANSACTION_PURPOSE'),array('*'));
		$select->where('A.CODE = ?',$categoryCode);
		//print_r($select->query());die;
		return $this->_db->fetchall($select);
	}
	
	public function getBeneficiaries($param)
	{
		$select	= $this->_db->select()
							->from(array('B' => 'M_BENEFICIARY'));
							//->join(array('C' => 'M_BENEFICIARY_USER'), 'B.BENEFICIARY_ID = C.BENEFICIARY_ID');
		$select->columns(array(
								'BANK_CODE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '2' THEN BANK_CODE
																						WHEN '8' THEN CLR_CODE
																						END"),
								'BENEFICIARY_RESIDENT_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_RESIDENT	
																						WHEN 'R' THEN 'Resident' 
																						WHEN 'NR' THEN 'Non Resident'
																						ELSE ''	END"),
								'BENEFICIARY_CITIZENSHIP_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_CITIZENSHIP	
																						WHEN 'W' THEN 'WNI' 
																						WHEN 'N' THEN 'WNA'
																						ELSE ''	END"),
							   'BENEFICIARY_TYPE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '1' THEN 'Within' 
																						WHEN '2' THEN 'Domestic'
																						WHEN '3' THEN 'Remittance' 
																						WHEN '8' THEN 'Online' 
																						END"),
							   'ISFAVORITE_disp'			=> new Zend_Db_Expr("CASE B.ISFAVORITE 	
																						WHEN '0' THEN 'No' 
																						WHEN '1' THEN 'Yes'	END"),
							   'BENEFICIARY_BANKSTATUS_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_BANKSTATUS 
							   															WHEN '0' THEN 'No' 
																						WHEN '1' THEN 'Yes'	
																						WHEN '2' THEN 'Failed'	END"),
							   'BENEFICIARY_ISAPPROVE_disp'	=> new Zend_Db_Expr("CASE B.BENEFICIARY_ISAPPROVE 
							   															WHEN '0' THEN 'Not Approved' 
																						WHEN '1' THEN 'Approved'	END"),
							   'ACTION_disp'	=> new Zend_Db_Expr("CASE WHEN BENEFICIARY_ISREQUEST_DELETE = 1 THEN 'Delete'
							   											  WHEN BENEFICIARY_ISAPPROVE 		= 0 THEN 'Add'  END"),
							  )
						);
// 		if(isset($param['payType']) && !($param['payType'] == 0))$select->where("B.BENEFICIARY_TYPE in (".$param['payType'].")");
		//
		$select->where('B.CUST_ID = ?', (string)$param['cust_id']);

		if(isset($param['payType'])){
			if($param['payType'] == '0'){
				$select->where("B.BENEFICIARY_TYPE in (1,8)");
			}
			elseif($param['payType'] == '1'){
				$select->where("B.BENEFICIARY_TYPE in (1)");
			}
			elseif($param['payType'] == '3'){
				$select->where("B.BENEFICIARY_TYPE in (3)");
			}
			elseif($param['payType'] == '4'){
				$select->where("B.BENEFICIARY_TYPE in (4)");
			}
			else{
				//$select->where("B.BENEFICIARY_TYPE in (".$param['payType'].")");
				$select->where("B.BENEFICIARY_TYPE in (2,8)");
			}
		}

		if($param['beneLinkage'] == true){
			$select->join(array('C' => 'M_BENEFICIARY_USER'), 'B.BENEFICIARY_ID = C.BENEFICIARY_ID', array());
			$select->where("C.USER_ID = ?", (string)$param['user_id']);
			$select->where("C.CUST_ID = ?", (string)$param['cust_id']);
		}
		
		if(isset($param['currency'])){
			if($param['payType'] == '3'){ //for remit, if SA = IDR, BA = all, if SA = USD, BA = USD
				if($param['currency'] == 'USD')
					$select->where('B.CURR_CODE = ?', 'USD');
			}
			else{
				if($param['currency']=='IDR'){
					$select->where("B.CURR_CODE = ?", 'USD');
				}
				if($param['currency']=='USD'){
					$select->where("B.CURR_CODE = ?", 'IDR');
				}
			}
		}

		//if(isset($param['payType']))$select->where("B.BENEFICIARY_TYPE in ('8','2')");
		//if(isset($param['user_id']))$select->where("C.USER_ID = ?", (string)$param['user_id']);
		//if(isset($param['cust_id']))$select->where("C.CUST_ID = ?", (string)$param['cust_id']);
		if(isset($param['fAlias']))$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($param['fAlias']).'%'));
		if(isset($param['fAcct']))$select->where('BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($param['fAcct']).'%'));
		if(isset($param['fName']))$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($param['fName']).'%'));
		if(isset($param['fCcy']))$select->where('CURR_CODE = '.$this->_db->quote(strtoupper($param['fCcy'])));
		if(isset($param['fFav==1']))$select->where('ISFAVORITE=1');
		if(isset($param['alpha'])) $select->where("BENEFICIARY_NAME LIKE ".$this->_db->quote($param['alpha'].'%'));
	    if(isset($param['fav'])) $select->where('ISFAVORITE=1');
	    if(isset($param['benef_id'])) $select->where('B.BENEFICIARY_ID= ?',$param['benef_id']);
		if(isset($param['sortBy']) && isset($param['sortDir'])) $select->order($param['sortBy'].' '.$param['sortDir']);
		// echo $select;die;
		return $this->_db->fetchall($select);
	}

	public function getBeneficiariesRemit($param)
	{
		$select	= $this->_db->select()
							->from(array('B' => 'M_BENEFICIARY'));
							//->join(array('C' => 'M_BENEFICIARY_USER'), 'B.BENEFICIARY_ID = C.BENEFICIARY_ID');
		$select->columns(array(
								'BANK_CODE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '2' THEN B.BANK_CODE
																						WHEN '8' THEN CLR_CODE
																						END"),
								'BENEFICIARY_RESIDENT_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_RESIDENT	
																						WHEN 'R' THEN 'Resident' 
																						WHEN 'NR' THEN 'Non Resident'
																						ELSE ''	END"),
								'BENEFICIARY_CITIZENSHIP_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_CITIZENSHIP	
																						WHEN 'W' THEN 'WNI' 
																						WHEN 'N' THEN 'WNA'
																						ELSE ''	END"),
							   'BENEFICIARY_TYPE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '1' THEN 'Within' 
																						WHEN '2' THEN 'Domestic'
																						WHEN '3' THEN 'Remittance' 
																						WHEN '8' THEN 'Online' 
																						END"),
							   'ISFAVORITE_disp'			=> new Zend_Db_Expr("CASE B.ISFAVORITE 	
																						WHEN '0' THEN 'No' 
																						WHEN '1' THEN 'Yes'	END"),
							   'BENEFICIARY_BANKSTATUS_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_BANKSTATUS 
							   															WHEN '0' THEN 'No' 
																						WHEN '1' THEN 'Yes'	
																						WHEN '2' THEN 'Failed'	END"),
							   'BENEFICIARY_ISAPPROVE_disp'	=> new Zend_Db_Expr("CASE B.BENEFICIARY_ISAPPROVE 
							   															WHEN '0' THEN 'Not Approved' 
																						WHEN '1' THEN 'Approved'	END"),
							   'ACTION_disp'	=> new Zend_Db_Expr("CASE WHEN BENEFICIARY_ISREQUEST_DELETE = 1 THEN 'Delete'
							   											  WHEN BENEFICIARY_ISAPPROVE 		= 0 THEN 'Add'  END"),
							  )
						);
// 		if(isset($param['payType']) && !($param['payType'] == 0))$select->where("B.BENEFICIARY_TYPE in (".$param['payType'].")");
		//
		$select->where('B.CUST_ID = ?', (string)$param['cust_id']);

		if(isset($param['payType'])){
			if($param['payType'] == '0'){
				$select->where("B.BENEFICIARY_TYPE in (1,8)");
			}
			elseif($param['payType'] == '1'){
				$select->where("B.BENEFICIARY_TYPE in (1)");
			}
			elseif($param['payType'] == '3'){
				$select->where("B.BENEFICIARY_TYPE in (3)");
			}
			else{
				//$select->where("B.BENEFICIARY_TYPE in (".$param['payType'].")");
				$select->where("B.BENEFICIARY_TYPE in (2,8)");
			}
		}

		if($param['beneLinkage'] == true){
			$select->join(array('C' => 'M_BENEFICIARY_USER'), 'B.BENEFICIARY_ID = C.BENEFICIARY_ID', array());
			$select->where("C.USER_ID = ?", (string)$param['user_id']);
			$select->where("C.CUST_ID = ?", (string)$param['cust_id']);
		}
		
		if(isset($param['currency'])){
			if($param['payType'] == '3'){ //for remit, if SA = IDR, BA = all, if SA = USD, BA = USD
				if($param['currency'] == 'USD')
					$select->where('B.CURR_CODE = ?', 'USD');
				else
					$select->where('B.CURR_CODE != ?', 'IDR');
			}
			else{
				if($param['currency']=='IDR'){
					$select->where("B.CURR_CODE = ?", 'USD');
				}
				if($param['currency']=='USD'){
					$select->where("B.CURR_CODE = ?", 'IDR');
				}
			}
		}

		if($param['payType'] == '3'){
			$select->join(array('MB' => 'M_MEMBER_BANK'), 'SUBSTRING(B.SWIFT_CODE,1,LENGTH(B.SWIFT_CODE)-3) = MB.BANK_CODE AND B.CURR_CODE = MB.CCY', array());
		}

		//if(isset($param['payType']))$select->where("B.BENEFICIARY_TYPE in ('8','2')");
		//if(isset($param['user_id']))$select->where("C.USER_ID = ?", (string)$param['user_id']);
		//if(isset($param['cust_id']))$select->where("C.CUST_ID = ?", (string)$param['cust_id']);
		if(isset($param['fAlias']))$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($param['fAlias']).'%'));
		if(isset($param['fAcct']))$select->where('BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($param['fAcct']).'%'));
		if(isset($param['fName']))$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($param['fName']).'%'));
		if(isset($param['fCcy']))$select->where('CURR_CODE = '.$this->_db->quote(strtoupper($param['fCcy'])));
		if(isset($param['fFav==1']))$select->where('ISFAVORITE=1');
		if(isset($param['alpha'])) $select->where("BENEFICIARY_NAME LIKE ".$this->_db->quote($param['alpha'].'%'));
	    if(isset($param['fav'])) $select->where('ISFAVORITE=1');
	    if(isset($param['benef_id'])) $select->where('B.BENEFICIARY_ID= ?',$param['benef_id']);
		if(isset($param['sortBy']) && isset($param['sortDir'])) $select->order($param['sortBy'].' '.$param['sortDir']);
		
		return $this->_db->fetchall($select);
	}
	
	public function getBeneficiariesRemitPopup($param)
	{
		$select	= $this->_db->select()
		->from(array('B' => 'M_BENEFICIARY'));
		//->join(array('C' => 'M_BENEFICIARY_USER'), 'B.BENEFICIARY_ID = C.BENEFICIARY_ID');
		$select->columns(array(
				'BANK_CODE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '2' THEN B.BANK_CODE
																						WHEN '8' THEN CLR_CODE
																						END"),
				'BENEFICIARY_RESIDENT_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_RESIDENT
																						WHEN 'R' THEN 'Resident'
																						WHEN 'NR' THEN 'Non Resident'
																						ELSE ''	END"),
				'BENEFICIARY_CITIZENSHIP_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_CITIZENSHIP
																						WHEN 'W' THEN 'WNI'
																						WHEN 'N' THEN 'WNA'
																						ELSE ''	END"),
				'BENEFICIARY_TYPE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '1' THEN 'Within'
																						WHEN '2' THEN 'Domestic'
																						WHEN '3' THEN 'Remittance'
																						WHEN '8' THEN 'Online'
																						END"),
																							'ISFAVORITE_disp'			=> new Zend_Db_Expr("CASE B.ISFAVORITE
																						WHEN '0' THEN 'No'
																						WHEN '1' THEN 'Yes'	END"),
																							'BENEFICIARY_BANKSTATUS_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_BANKSTATUS
							   															WHEN '0' THEN 'No'
																						WHEN '1' THEN 'Yes'
																						WHEN '2' THEN 'Failed'	END"),
																							'BENEFICIARY_ISAPPROVE_disp'	=> new Zend_Db_Expr("CASE B.BENEFICIARY_ISAPPROVE
							   															WHEN '0' THEN 'Not Approved'
																						WHEN '1' THEN 'Approved'	END"),
																							'ACTION_disp'	=> new Zend_Db_Expr("CASE WHEN BENEFICIARY_ISREQUEST_DELETE = 1 THEN 'Delete'
							   											  WHEN BENEFICIARY_ISAPPROVE 		= 0 THEN 'Add'  END"),
		)
		);
		// 		if(isset($param['payType']) && !($param['payType'] == 0))$select->where("B.BENEFICIARY_TYPE in (".$param['payType'].")");
		//
		$select->where('B.CUST_ID = ?', (string)$param['cust_id']);
	
		if(isset($param['payType'])){
			if($param['payType'] == '0'){
				$select->where("B.BENEFICIARY_TYPE in (1,8)");
			}
			elseif($param['payType'] == '1'){
				$select->where("B.BENEFICIARY_TYPE in (1)");
			}
			elseif($param['payType'] == '3'){
				$select->where("B.BENEFICIARY_TYPE in (3)");
			}
			else{
				//$select->where("B.BENEFICIARY_TYPE in (".$param['payType'].")");
				$select->where("B.BENEFICIARY_TYPE in (2,8)");
			}
		}
	
		if($param['beneLinkage'] == true){
			$select->join(array('C' => 'M_BENEFICIARY_USER'), 'B.BENEFICIARY_ID = C.BENEFICIARY_ID', array());
			$select->where("C.USER_ID = ?", (string)$param['user_id']);
			$select->where("C.CUST_ID = ?", (string)$param['cust_id']);
		}
		$select->where("B.BENEFICIARY_ISAPPROVE = ?", (string)'1');
		if(isset($param['currency'])){
			if($param['payType'] == '3'){ //for remit, if SA = IDR, BA = all, if SA = USD, BA = USD
				if($param['currency'] == 'USD')
					$select->where('B.CURR_CODE = ?', 'USD');
				else
					$select->where('B.CURR_CODE != ?', 'IDR');
			}
			else{
				if($param['currency']=='IDR'){
					$select->where("B.CURR_CODE = ?", 'USD');
				}
				if($param['currency']=='USD'){
					$select->where("B.CURR_CODE = ?", 'IDR');
				}
			}
		}
	
		if($param['payType'] == '3'){
			$select->join(array('MB' => 'M_MEMBER_BANK'), 'SUBSTRING(B.SWIFT_CODE,1,LENGTH(B.SWIFT_CODE)-3) = MB.BANK_CODE AND B.CURR_CODE = MB.CCY', array());
		}
	
		//if(isset($param['payType']))$select->where("B.BENEFICIARY_TYPE in ('8','2')");
		//if(isset($param['user_id']))$select->where("C.USER_ID = ?", (string)$param['user_id']);
		//if(isset($param['cust_id']))$select->where("C.CUST_ID = ?", (string)$param['cust_id']);
		if(isset($param['fAlias']))$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($param['fAlias']).'%'));
		if(isset($param['fAcct']))$select->where('BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($param['fAcct']).'%'));
		if(isset($param['fName']))$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($param['fName']).'%'));
		if(isset($param['fCcy']))$select->where('CURR_CODE = '.$this->_db->quote(strtoupper($param['fCcy'])));
		if(isset($param['fFav==1']))$select->where('ISFAVORITE=1');
		if(isset($param['alpha'])) $select->where("BENEFICIARY_NAME LIKE ".$this->_db->quote($param['alpha'].'%'));
		if(isset($param['fav'])) $select->where('ISFAVORITE=1');
		if(isset($param['benef_id'])) $select->where('B.BENEFICIARY_ID= ?',$param['benef_id']);
		if(isset($param['sortBy']) && isset($param['sortDir'])) $select->order($param['sortBy'].' '.$param['sortDir']);

		return $this->_db->fetchall($select);
	}

	public function getBeneficiariesLocalRemitPopup($param)
	{
		$select	= $this->_db->select()
		->from(array('B' => 'M_BENEFICIARY'));
		//->join(array('C' => 'M_BENEFICIARY_USER'), 'B.BENEFICIARY_ID = C.BENEFICIARY_ID');
		$select->columns(array(
				'BANK_CODE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '2' THEN B.BANK_CODE
																						WHEN '8' THEN CLR_CODE
																						END"),
				'BENEFICIARY_RESIDENT_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_RESIDENT
																						WHEN 'R' THEN 'Resident'
																						WHEN 'NR' THEN 'Non Resident'
																						ELSE ''	END"),
				'BENEFICIARY_CITIZENSHIP_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_CITIZENSHIP
																						WHEN 'W' THEN 'WNI'
																						WHEN 'N' THEN 'WNA'
																						ELSE ''	END"),
				'BENEFICIARY_TYPE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '1' THEN 'Within'
																						WHEN '2' THEN 'Domestic'
																						WHEN '3' THEN 'Remittance'
																						WHEN '8' THEN 'Online'
																						END"),
																							'ISFAVORITE_disp'			=> new Zend_Db_Expr("CASE B.ISFAVORITE
																						WHEN '0' THEN 'No'
																						WHEN '1' THEN 'Yes'	END"),
																							'BENEFICIARY_BANKSTATUS_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_BANKSTATUS
							   															WHEN '0' THEN 'No'
																						WHEN '1' THEN 'Yes'
																						WHEN '2' THEN 'Failed'	END"),
																							'BENEFICIARY_ISAPPROVE_disp'	=> new Zend_Db_Expr("CASE B.BENEFICIARY_ISAPPROVE
							   															WHEN '0' THEN 'Not Approved'
																						WHEN '1' THEN 'Approved'	END"),
																							'ACTION_disp'	=> new Zend_Db_Expr("CASE WHEN BENEFICIARY_ISREQUEST_DELETE = 1 THEN 'Delete'
							   											  WHEN BENEFICIARY_ISAPPROVE 		= 0 THEN 'Add'  END"),
		)
		);
		// 		if(isset($param['payType']) && !($param['payType'] == 0))$select->where("B.BENEFICIARY_TYPE in (".$param['payType'].")");
		//
		$select->where('B.CUST_ID = ?', (string)$param['cust_id']);
		$select->where('BENEFICIARY_ISAPPROVE = ?', '1');
	
		// if(isset($param['payType'])){
		// 	if($param['payType'] == '0'){
		// 		$select->where("B.BENEFICIARY_TYPE in (1,8)");
		// 	}
		// 	elseif($param['payType'] == '1'){
		// 		$select->where("B.BENEFICIARY_TYPE in (1)");
		// 	}
		// 	elseif($param['payType'] == '3'){
		// 		$select->where("B.BENEFICIARY_TYPE in (3)");
		// 	}
		// 	else{
				//$select->where("B.BENEFICIARY_TYPE in (".$param['payType'].")");
				$select->where("B.BENEFICIARY_TYPE in (4)");
		// 	}
		// }
	
		if($param['beneLinkage'] == true){
			$select->join(array('C' => 'M_BENEFICIARY_USER'), 'B.BENEFICIARY_ID = C.BENEFICIARY_ID', array());
			$select->where("C.USER_ID = ?", (string)$param['user_id']);
			$select->where("C.CUST_ID = ?", (string)$param['cust_id']);
		}
	
		//if(isset($param['payType']))$select->where("B.BENEFICIARY_TYPE in ('8','2')");
		//if(isset($param['user_id']))$select->where("C.USER_ID = ?", (string)$param['user_id']);
		//if(isset($param['cust_id']))$select->where("C.CUST_ID = ?", (string)$param['cust_id']);
		
		
		if(isset($param['fName']))$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($param['fName']).'%'));
		if(isset($param['fNrc']))$select->where('BENEFICIARY_ID_NUMBER = '.$this->_db->quote(strtoupper($param['fNrc'])));
		// if(isset($param['fFav==1']))$select->where('ISFAVORITE=1');
		// if(isset($param['alpha'])) $select->where("BENEFICIARY_NAME LIKE ".$this->_db->quote($param['alpha'].'%'));
		// if(isset($param['fav'])) $select->where('ISFAVORITE=1');
		// if(isset($param['benef_id'])) $select->where('B.BENEFICIARY_ID= ?',$param['benef_id']);
		if(isset($param['sortBy']) && isset($param['sortDir'])) $select->order($param['sortBy'].' '.$param['sortDir']);
		// echo $select;die;
		return $this->_db->fetchall($select);
	}

	

	function getCountry($countryCode)
	{
		$select = $this->_db->select()
		->from(array('A' => 'M_COUNTRY'),array('COUNTRY_CODE', 'COUNTRY_NAME'));
		$select->where('A.COUNTRY_CODE = ?',$countryCode);
		//print_r($select->query());die;
		return $this->_db->fetchall($select);
	}
}