<?php

/**
 * Tempcustomer model
 *
 * @author
 * @version
 */


class Changemanagement_Model_Tempbintemplate extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = '';
	/**
	 * Approve Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveNew($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
			->from('TEMP_BIN_TEMPLATE')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customer)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['TEMP_CREATED']     = new Zend_Db_Expr('now()');
		$insertArr['TEMP_CREATEDBY']   = $actor;
		$insertArr['TEMP_UPDATED']     = new Zend_Db_Expr('now()');
		// $insertArr['APPROVED_DATE']     = new Zend_Db_Expr('now()');
		$insertArr['TEMP_UPDATEDBY']   = $actor;

		$insertArr['TEMP_SUGGESTED']   = $customer['TEMP_SUGGESTED'];
		$insertArr['TEMP_SUGGESTEDBY'] = $customer['TEMP_SUGGESTEDBY'];

		try {
			$customerins = $this->dbObj->insert('BIN_TEMPLATE', $insertArr);
		} catch (exception $e) {
			Zend_Debug::dump($e->getMessage());
		}
		if (!(bool) $customerins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		$deleteChanges  = $this->deleteNew();
		if (!$deleteChanges)
			return false;

		return true;
	}

	/**
	 * Approve Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveEdit($actor = null)
	{
		$transFailed = false;
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
			->from('TEMP_BIN_TEMPLATE')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customer)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		} else {
			$custId = $customer['TEMP_CODE'];
		}
		//		Zend_Debug::dump($customer);
		//		die;
		//update record customer
		$updateArr = array_diff_key($customer, array('TEMP_ID' => '', 'CHANGES_ID' => '', 'TEMP_CREATED' => '', 'TEMP_CREATEDBY' => ''));
		$updateArr['TEMP_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['TEMP_UPDATEDBY']   = $actor;

		$insertArr['TEMP_CREATED']     = $customer['TEMP_CREATED'];
		$insertArr['TEMP_CREATEDBY']   = $customer['TEMP_CREATEDBY'];
		$insertArr['TEMP_SUGGESTED']   = $customer['TEMP_SUGGESTED'];
		$insertArr['TEMP_SUGGESTEDBY'] = $customer['TEMP_SUGGESTEDBY'];

		$whereArr = array(
			'TEMP_CODE = ?' => (string) $customer['TEMP_CODE']
		);

		$customerupdate = $this->dbObj->update('BIN_TEMPLATE', $updateArr, $whereArr);

		if (!(bool) $customerupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Edited, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$updateArr['CUST_NAME'].' Change id : '.$this->_changeId);

		$deleteChanges  = $this->deleteEdit();
		if (!$deleteChanges)
			return false;

		return true;
	}

	/*
	 if delete customer :
	 - delete user
	 - delete bank account
	 - delete user limit
	 - delete daily limit
	*/
	public function approveDelete($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
			->from('TEMP_BIN_TEMPLATE')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);

		if (!count($customer)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(BIN TEMPLATE)';
			return false;
		}
		
		
		// var_dump($whereArr);die;
		$customerdelete = $this->dbObj->delete('BIN_TEMPLATE', $this->dbObj->quoteInto('TEMP_CODE = ?', (string) $customer['TEMP_CODE']));
		if (!$customerdelete) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(BIN TEMPLATE)';
			return false;
		}
		//END update status customer



		//delete temp customer
		$deleteChanges  = $this->deleteDelete();
		if (!$deleteChanges)
			return false;

		return true;
	}
	public function deleteNew()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteEdit()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteUnsuspend()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Deactivate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteSuspend()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	public function deleteDelete()
	{


		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}
	public function approveDeactivate()
	{
	}

	public function approveActivate()
	{
	}

	public function deleteDeactivate()
	{
	}

	public function deleteActivate()
	{
	}
}
