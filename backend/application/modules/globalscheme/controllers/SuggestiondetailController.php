<?php
require_once 'Zend/Controller/Action.php';
class Globalscheme_SuggestiondetailController extends Application_Main
{


	
	public function initController(){
		$this->_helper->layout()->setLayout('popup');
	}
	
	public function indexAction() 
	{		
		
	$changeid = $this->_getparam('changes_id');	
	$changesid = $changeid;//dipake dibawah	
	$select = $this->_db->select()
             ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
	 $select -> where("A.CHANGES_ID = ?",$changeid);
	 $select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
	 $cek = $this->_db->fetchRow($select);
	 
	 if(!$cek)
	 {
	  $this->_redirect('/notification/invalid/index');
	 }
		
		if ($cek){
				$this->view->cek 		   = true;
				//$listInvoicetypeCode = array('1','2','3');
				//$listInvoicetypeVal	  = array( 'Item', 'Unit','Bulk');

				$arrStatus = $this->_changeStatus;				
				$this->view->status = str_replace($arrStatus['code'],$arrStatus['desc'],$cek['CHANGES_STATUS']);
				$this->view->created = $cek['CREATED'];
				$this->view->createdby = $cek['CREATED_BY'];	
				
				
				
								// GET SCHEMECODE FROM TEMP_SCHEME		
				//------------------------------------------------------------------------------- SUGGESTED SCHEME  ----------------------------------------------------------------------------------------------//
				//==================================== SCHEME INFORMATION ===========================================//							

					
						$suggestedschemeInfo = $this->getTempScheme($changesid);											
						$scheme_code = $suggestedschemeInfo['BUSS_SCHEME_CODE'];
						$this->_db->select()
									->from(array('B' => 'M_BUSINESS_SCHEME'),'BUSS_SCHEME_NAME')
									->where("B.BUSS_SCHEME_CODE =?", $scheme_code);
						$buss_scheme = 
									$this->_db->fetchOne(
									$this->_db->select()
									->from(array('B' => 'M_BUSINESS_SCHEME'),'BUSS_SCHEME_NAME')
									->where("B.BUSS_SCHEME_CODE =?", $scheme_code)
									);
						
						$schemeInfo = $this->ConstructScheme($scheme_code);			
						$buss_scheme = 
							$this->_db->fetchRow($this->_db->select()
							->from(array('B' => 'M_BUSINESS_SCHEME'))
							->where("B.BUSS_SCHEME_CODE =?", $schemeInfo['BUSS_SCHEME_CODE']));
							
					
				//================================ END SCHEME INFORMATION ===========================================//
				//------------------------------------------------------------------------------- END SUGGESTED SCHEME  ----------------------------------------------------------------------------------------------//
				//---------------------------------------------------------------------------------- CURRENT SCHEME  ----------------------------------------------------------------------------------------------//
						
				//==================================== SENT TO VIEW =========================================//
								$this->view->schemeInfo			 = $schemeInfo;	
								$this->view->disbursement		 = $this->getFacilityInterest($scheme_code,$buss_scheme['DISB_CHARGES_TYPE'],1);
								$this->view->addTenor 		 	 = $this->getFacilityInterest($scheme_code,$buss_scheme['ADD_TENOR_CHARGES_TYPE'],2);
								$this->view->gracePeriod		     = $this->getFacilityInterest($scheme_code,$buss_scheme['GP_CHARGES_TYPE'],3);
								$this->view->penaltyPeriod 	     = $this->getFacilityInterest($scheme_code,$buss_scheme['PEN_CHARGES_TYPE'],4);  
								
				//==================================== END SENT TO VIEW =========================================//			
				//------------------------------------------------------------------------------- END CURRENT SCHEME  ------------------------------------------------------------------------------------------//
					
			//================================ END TRANSACTION WORKFLOW ===========================================//		


			//==================================== SENT TO VIEW =========================================//				
							$this->view->suggestedschemeInfo			 = $suggestedschemeInfo;
							$this->view->suggesteddisbursement			 = $this->getTempFacilityInterest($suggestedschemeInfo['DISB_CHARGES_TYPE'],1,$changesid);
							$this->view->suggestedaddTenor 		 		 = $this->getTempFacilityInterest($suggestedschemeInfo['ADD_TENOR_CHARGES_TYPE'],2,$changesid);
							$this->view->suggestedgracePeriod		     = $this->getTempFacilityInterest($suggestedschemeInfo['GP_CHARGES_TYPE'],3,$changesid);
							$this->view->suggestedpenaltyPeriod 	     = $this->getTempFacilityInterest($suggestedschemeInfo['PEN_CHARGES_TYPE'],4,$changesid);														
							$this->view->modulename 			 			 = $this->_request->getModuleName();							

			//==================================== END SENT TO VIEW =========================================//
			//------------------------------------------------------------------------------- END SUGGESTED SCHEME  ----------------------------------------------------------------------------------------------//			
		}
		$this->view->changes_id 			                 = $changesid;
		$this->view->mode					 				 = $cek['CHANGES_TYPE'];

	}

	public function getTempScheme($changes_id)
    { 
		$select = 
						$this->_db->fetchRow($this->_db->select()
						->from(array('S' => 'TEMP_BUSINESS_SCHEME'))						
						->where("S.CHANGES_ID		= ? ", $changes_id));				
		return $select;
    }
	//$this->view->disbursement		 = $this->getFacilityInterest($scheme_code,$buss_scheme['DISB_CHARGES_TYPE'],1);
	public function getFacilityInterest($code,$chargesType,$option)
	{ 		
		$chargesInfos = $chargesINT = $chargesFEE = $chargesInfos[0] = $chargesInfos[1] = array();
		$arrPER = array();
		$period = '';
		$fulldisb = null;
		//$code = $this->_globalCode;
		
		if($option == 1)
		{$arrPER = array('1' => $this->language->_('On Disbursement (Upfront)'),
							  '2' => $this->language->_('On Maturity Date (After)')
							 );}
		elseif($option == 2)
		{$arrPER = array('1' => $this->language->_('On Invoice Maturity Date (Upfront)'),
							   '2' => $this->language->_('On Bank Maturity Date (After)')
							  );}
		elseif($option == 3)
		{$arrPER = array('1' => $this->language->_('On Grace Period Date (Upfront)'),
							   '2' => $this->language->_('On End Of Grace Period (After)')
							  );}
		elseif($option == 4)
		{$arrPER = array('1' => $this->language->_('On Penalty Start Date (Upfront)'),
							     '2' => $this->language->_('On End Of Penalty(After)')
							    );}

		if($chargesType == 0 || $chargesType == 2)
		{
			$caseINT_PER = "(CASE INT_PERIOD ";
			foreach($arrPER as $key=>$val)
			{
				$caseINT_PER .= " WHEN '".$key."' THEN '".$val."' ";
			}
			$caseINT_PER .= " END)";	
			
			$chargesINT = 
							$this->_db->fetchAll($this->_db->select()
							->from(array('CD' => 'M_BUSINESS_SCHEME_CHARGES'),array('*','INT_PERIOD'=>$caseINT_PER))
							->where("CD.BUSS_SCHEME_CODE = ? ", $code)
							->where("CD.INT_PERIOD is not null")
							->where("CD.INT_OPTION = ? ",$option));
		}
		
		if($chargesType == 1 || $chargesType == 2)
		{
			
			$chargesFEE = 
							$this->_db->fetchAll(
							$this->_db->select()
							->from(array('CD' => 'M_BUSINESS_SCHEME_CHARGES'))
							->where("CD.BUSS_SCHEME_CODE = ? ", $code)
							->where("CD.FEE_PERIOD is not null")
							->where("CD.INT_OPTION = ? ",$option)
							);
			
		}
		
		if($chargesINT)
			$chargesInfos[0] = $chargesINT;
		if($chargesFEE)
			$chargesInfos[1] = $chargesFEE;
		$Titlename = array($this->language->_('Disbursement'),$this->language->_('Additional Tenor'),$this->language->_('Grace Period'),$this->language->_('Penalty Period'));
		$Titlecode  = array('1','2','3','4');
		$Title  = str_replace($Titlecode, $Titlename, $option);
		
		$Typename = array($this->language->_('Interest'),$this->language->_('Transaction Fee'),$this->language->_('Interest & Transaction Fee'));
		$Typecode  = array('0','1','2');
		$chargessType  = str_replace($Typecode, $Typename, $chargesType);
		$table = '';

		if(isset($chargesInfos[0]) || isset($chargesInfos[1]))
		{
			foreach($chargesInfos as $key=>$chargesInfo)
			{					
				$period = ($chargesInfo['0']['FEE_PERIOD']) ? $chargesInfo['0']['FEE_PERIOD'] : $chargesInfo['0']['INT_PERIOD'];
				$feeType = $chargesInfo['0']['FEE_TYPE'];
				$feeTypeName = (isset($chargesInfo['0']['FEE_TYPE_NAME'])) ? $chargesInfo['0']['FEE_TYPE_NAME'] : '';
				$fullDisb = ($fulldisb == 1) ? 'Yes' : 'No';
				
				if($period == 1 || $period == 2)
				$period = $arrPER[$period];
			
				
				if(!$table)
				{
				$table ='<table width="100%" cellpadding="5" cellspacing="0" border="0" class="bigClass tableform">
									<tr>
										<th colspan="4" class="tdform-sub"><strong>'.$this->language->_($Title).'</strong></th>
									</tr>
									<tr>
										<td class="tbl-evencontent">Charges Type</td>
										<td class="tbl-evencontent">
												'.$chargessType;
				$table .='</td>
									</tr>';
				}
				if($chargesType == 2)
				{
					$type2 = ($key == 1) ? $this->language->_('Transaction Fee') : $this->language->_('Interest');
					$table .='
									<tr>
										<td class="tbl-evencontent" colspan="2"><strong>'.$type2;
					$table .= '</strong></td>
									</tr>';
				}
											if($key == 0){
												$table	.= '
															<tr id="detaild_type1">
																<td class="tbl-evencontent">'.$this->language->_('Charges Rate').'</td>
																<td class="tbl-evencontent">
																  <table width="100%" border="1" class="bigClass tableform">';
												$table .=
												'<tr>
													<th class="bigClass tableform" width="3%">'.$this->language->_('No').'.</th>
													<th class="bigClass tableform" width="45%">'.$this->language->_('CCY').'</th>
													<th class="bigClass tableform" width="42%">'.$this->language->_('Rate').'</th>
												</tr>';
											}
											elseif($key == 1 && $feeType==1){
												$table	.= '
															<tr id="detaild_type1">
																<td class="tbl-evencontent">'.$this->language->_('Charges Rate').'</td>
																<td class="tbl-evencontent">
																  <table width="100%" border="1" class="bigClass tableform">';
												$table .=
												'<tr>									   
													<th class="bigClass tableform" width="3%">'.$this->language->_('No').'.</th>
													<th class="bigClass tableform" width="25%">'.$this->language->_('CCY').'</th>
													<th class="bigClass tableform" width="25%">'.$this->language->_('Rate').'</th>
													<th class="bigClass tableform" width="23%">'.$this->language->_('Min').'</th>
													<th class="bigClass tableform" width="23%">'.$this->language->_('Max').'</th>											
												</tr>';
											}
											elseif($key == 1 && $feeType==2){
												$table	.= '
															<tr id="detaild_type1">
																<td class="tbl-evencontent">'.$this->language->_('Charges Amount').'</td>
																<td class="tbl-evencontent">
																  <table width="100%" border="1" class="bigClass tableform">';
												$table .=
												'<tr>
													<th class="bigClass tableform" width="3%">'.$this->language->_('No').'.</th>
													<th class="bigClass tableform" width="45%">'.$this->language->_('CCY').'</th>
													<th class="bigClass tableform" width="42%">'.$this->language->_('Amount').'</th>
												</tr>';
											}
				//<!----------------------------------------------------LOOP CURRENCY-------------------------------------------------------------------------------------------->
												if($key == 0){
												$counter=0;	
												foreach($chargesInfo as $row){

												$table .=
												'<tr>
													<td class="bigClass tableform" width="3%">'.++$counter.'</td>
													<td class="bigClass tableform" width="45%">'.$row['INT_CCY'].'</td>
													<td class="bigClass tableform" width="42%">'.$row['INT_RATE'].' % </td>										
												</tr>';
											}}
											elseif($key == 1 && $feeType==1){
											$counter=0;
											foreach($chargesInfo as $row){
												$table .=
												'<tr>
													<td class="bigClass tableform" width="3%">'.++$counter.'</td>
													<td class="bigClass tableform" width="25%">'.$row['FEE_CCY'].'</td>
													<td class="bigClass tableform" width="25%">'.$row['FEE_RATE'].' % </td>
													<td class="bigClass tableform" width="23%">'.Application_Helper_General::displayMoney($row['FEE_MIN']).'</td>
													<td class="bigClass tableform" width="23%">'.Application_Helper_General::displayMoney($row['FEE_MAX']).'</td>											
												</tr>';
											}}
											elseif($key == 1 && $feeType==2){
											$counter=0;
											foreach($chargesInfo as $row){
												$table .=
												'<tr>										
													<td class="bigClass tableform" width="3%">'.++$counter.'</td>
													<td class="bigClass tableform" width="45%">'.$row['FEE_CCY'].'</td>
													<td class="bigClass tableform" width="42%">'.Application_Helper_General::displayMoney($row['FEE_AMT']).'</td>										
												</tr>';
											}}
										$table .='
										  </table>
										  </td>
											</tr>
											<tr>
												<td class="tbl-evencontent" width="80px">'.$this->language->_('Type').'</td>
												<td class="tbl-evencontent">
													'.$period.'
												</td>
											</tr>';
			}
								if($option == 1 && $fulldisb != null)
								{
									$table .=
										'<tr>
											<td class="tbl-evencontent" width="80px">'.$this->language->_('Full Disbursement').'</td>
											<td class="tbl-evencontent">
												'.$fullDisb.'
											</td>
										</tr> ';
								}		
								$table .= '</table>';
		}
		else{
		$table = '<table width="100%" cellpadding="5" cellspacing="0" border="0" class="bigClass tableform">
										<tr>
											<th colspan="4" class="tdform-sub"><strong>'.$this->language->_($Title).'</strong></th>
										</tr>
										<tr>
											<td class="tbl-evencontent" align="center"> -- '.$this->language->_('No').' '.$this->language->_($Title).' --</td>
										</tr>
										</table>
										';}
			
		return $table;
	
	}
	
	
	public function getTempFacilityInterest($chargesType,$option,$changes_id)
	{
		$fulldisb = null;
		$chargesInfos = array();				
		$arrPER = array();
		
		if($option == 1)
		{
			$arrPER = array('1' => $this->language->_('On Disbursement (Upfront)'),
							  '2' => $this->language->_('On Maturity Date (After)')
							 );
		}
		elseif($option == 2)
		{$arrPER = array('1' => $this->language->_('On Invoice Maturity Date (Upfront)'),
							   '2' => $this->language->_('On Bank Maturity Date (After)')
							  );}
		elseif($option == 3)
		{$arrPER = array('1' => $this->language->_('On Grace Period Date (Upfront)'),
							   '2' => $this->language->_('On End Of Grace Period (After)')
							  );}
		elseif($option == 4)
		{$arrPER = array('1' => $this->language->_('On Penalty Start Date (Upfront)'),
							     '2' => $this->language->_('On End Of Penalty(After)')
							    );}											
		if($chargesType == 0 || $chargesType == 2)
		{
			$caseINT_PER = "(CASE INT_PERIOD ";
			foreach($arrPER as $key=>$val)
			{
				$caseINT_PER .= " WHEN '".$key."' THEN '".$val."' ";
			}
			$caseINT_PER .= " END)";	
			
			$chargesINT = 
							$this->_db->fetchAll(
							$this->_db->select()
							->from(array('CD' => 'TEMP_BUSINESS_SCHEME_CHARGES'),array('*','INT_PERIOD'=>$caseINT_PER))
							->where("CD.CHANGES_ID = ? ", $changes_id)
							->where("CD.INT_PERIOD is not null")
							->where("CD.INT_OPTION = ? ",$option)
							);	
			
		}
		
		if($chargesType == 1 || $chargesType == 2)
		{
			
			
			$chargesFEE = 
							$this->_db->fetchAll(
							$this->_db->select()
							->from(array('CD' => 'TEMP_BUSINESS_SCHEME_CHARGES'))
							->where("CD.INT_OPTION = ? ",$option)
							->where("CD.FEE_PERIOD is not null")						    
							->where("CD.CHANGES_ID = ? ",$changes_id)							
							);						
		}
		
		if(isset($chargesINT))
			$chargesInfos[0] = $chargesINT;
		if(isset($chargesFEE))
			$chargesInfos[1] = $chargesFEE;

		$Titlename = array($this->language->_('Disbursement'),$this->language->_('Additional Tenor'),$this->language->_('Grace Period'),$this->language->_('Penalty Period'));
		$Titlecode  = array('1','2','3','4');
		$Title  = str_replace($Titlecode, $Titlename, $option);

		$Typename = array($this->language->_('Interest'),$this->language->_('Transaction Fee'),$this->language->_('Interest & Transaction Fee'));
		$Typecode  = array('0','1','2');
		$chargessType  = str_replace($Typecode, $Typename, $chargesType);
		$table	= '';

		if(array_key_exists(0,$chargesInfos) || array_key_exists(1,$chargesInfos))
		{
			foreach($chargesInfos as $key=>$chargesInfo)
			{					
				if(array_key_exists('0',$chargesInfo)){
					$period = ($chargesInfo['0']['FEE_PERIOD']) ? $chargesInfo['0']['FEE_PERIOD'] : $chargesInfo['0']['INT_PERIOD'];
					$feeType = $chargesInfo['0']['FEE_TYPE'];
				}else{
					$period = '';
					$feeType = '';
				}
				$feeTypeName = (isset($chargesInfo['0']['FEE_TYPE_NAME'])) ? $chargesInfo['0']['FEE_TYPE_NAME'] : '';
				$fullDisb = ($fulldisb == 1) ? 'Yes' : 'No';
				if($period == 1 || $period == 2)
				$period = $arrPER[$period];
				if(!$table)
				{
				$table ='<table width="100%" cellpadding="5" cellspacing="0" border="0" class="bigClass tableform">
									<tr>
										<th colspan="4" class="tdform-sub"><strong>'.$this->language->_($Title).'</strong></th>
									</tr>
									<tr>
										<td class="tbl-evencontent">'.$this->language->_('Charges Type').'</td>
										<td class="tbl-evencontent">
												'.$chargessType;
				$table .='</td>
									</tr>';
				}
				if($chargesType == 2)
				{
					$type2 = ($key == 1) ? $this->language->_('Transaction Fee') : $this->language->_('Interest');
					$table .='
									<tr>
										<td class="tbl-evencontent" colspan="2"><strong>'.$type2;
					$table .= '</strong></td>
									</tr>';
				}

											if($key == 0){
												$table	.= '
															<tr id="detaild_type1">
																<td class="tbl-evencontent">'.$this->language->_('Charges Rate').'</td>
																<td class="tbl-evencontent">
																  <table width="100%" border="1" class="bigClass tableform">';
												$table .=
												'<tr>
													<th class="bigClass tableform" width="3%">'.$this->language->_('No').'.</th>
													<th class="bigClass tableform" width="45%">'.$this->language->_('CCY').'</th>
													<th class="bigClass tableform" width="42%">'.$this->language->_('Rate').'</th>
												</tr>';
											}
											elseif($key == 1 && $feeType==1){
												$table	.= '
															<tr id="detaild_type1">
																<td class="tbl-evencontent">'.$this->language->_('Charges Rate').'</td>
																<td class="tbl-evencontent">
																  <table width="100%" border="1" class="bigClass tableform">';
												$table .=
												'<tr>									   
													<th class="bigClass tableform" width="3%">'.$this->language->_('No').'.</th>
													<th class="bigClass tableform" width="25%">'.$this->language->_('CCY').'</th>
													<th class="bigClass tableform" width="25%">'.$this->language->_('Rate').'</th>
													<th class="bigClass tableform" width="23%">'.$this->language->_('Min').'</th>
													<th class="bigClass tableform" width="23%">'.$this->language->_('Max').'</th>											
												</tr>';
											}
											elseif($key == 1 && $feeType==2){
												$table	.= '
															<tr id="detaild_type1">
																<td class="tbl-evencontent">'.$this->language->_('Charges Amount').'</td>
																<td class="tbl-evencontent">
																  <table width="100%" border="1" class="bigClass tableform">';
												$table .=
												'<tr>
													<th class="bigClass tableform" width="3%">'.$this->language->_('No').'.</th>
													<th class="bigClass tableform" width="45%">'.$this->language->_('CCY').'</th>
													<th class="bigClass tableform" width="42%">'.$this->language->_('Amount').'</th>
												</tr>';
											}
				//<!----------------------------------------------------LOOP CURRENCY-------------------------------------------------------------------------------------------->
												if($key == 0){
												$counter=0;	
												foreach($chargesInfo as $row){

												$table .=
												'<tr>
													<td class="bigClass tableform" width="3%">'.++$counter.'</td>
													<td class="bigClass tableform" width="45%">'.$row['INT_CCY'].'</td>
													<td class="bigClass tableform" width="42%">'.$row['INT_RATE'].' % </td>										
												</tr>';
											}}
											elseif($key == 1 && $feeType==1){
											$counter=0;
											foreach($chargesInfo as $row){
												$table .=
												'<tr>
													<td class="bigClass tableform" width="3%">'.++$counter.'</td>
													<td class="bigClass tableform" width="25%">'.$row['FEE_CCY'].'</td>
													<td class="bigClass tableform" width="25%">'.$row['FEE_RATE'].' % </td>
													<td class="bigClass tableform" width="23%">'.Application_Helper_General::displayMoney($row['FEE_MIN']).'</td>
													<td class="bigClass tableform" width="23%">'.Application_Helper_General::displayMoney($row['FEE_MAX']).'</td>											
												</tr>';
											}}
											elseif($key == 1 && $feeType==2){
											$counter=0;
											foreach($chargesInfo as $row){
												$table .=
												'<tr>										
													<td class="bigClass tableform" width="3%">'.++$counter.'</td>
													<td class="bigClass tableform" width="45%">'.$row['FEE_CCY'].'</td>
													<td class="bigClass tableform" width="42%">'.Application_Helper_General::displayMoney($row['FEE_AMT']).'</td>										
												</tr>';
											}}
										$table .='
										  </table>
										  </td>
											</tr>
											<tr>
												<td class="tbl-evencontent" width="80px">'.$this->language->_('Type').'</td>
												<td class="tbl-evencontent">
													'.$period.'
												</td>
											</tr>';
			}
								if($option == 1 && $fulldisb != null)
								{
									$table .=
										'<tr>
											<td class="tbl-evencontent" width="80px">'.$this->language->_('Full Disbursement').'</td>
											<td class="tbl-evencontent">
												'.$fullDisb.'
											</td>
										</tr> ';
								}		
								$table .= '</table>';
		}
		else{
		$table = '<table width="100%" cellpadding="5" cellspacing="0" border="0" class="bigClass tableform">
										<tr>
											<th colspan="4" class="tdform-sub"><strong>'.$this->language->_($Title).'</strong></th>
										</tr>
										<tr>
											<td class="tbl-evencontent" align="center"> -- '.$this->language->_('No').' '.$this->language->_($Title).' --</td>
										</tr>
										</table>
										';}
		return $table;
		
	}
	

	
	public function ConstructScheme($scheme_code)
    { 
		
		$select = 
						$this->_db->fetchRow($this->_db->select()
						->from(array('S' => 'M_BUSINESS_SCHEME'))
						->where("S.BUSS_SCHEME_CODE=?", $scheme_code));
		$this->_globalCode = $scheme_code;		
		//$this->_type = $type;
		
		return $select;
    }	
}