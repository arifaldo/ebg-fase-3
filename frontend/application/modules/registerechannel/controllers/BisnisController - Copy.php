<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class registerechannel_BisnisController extends Application_Main
{	
	protected $_country;

	public function initController(){
		$model = new registerechannel_Model_Registerechannel();
		$this->_country = Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');
	}

	public function indexAction()
	{
		$model = new registerechannel_Model_Registerechannel();

		$this->view->countryArr = array_merge(array(''=>'--- '.$this->language->_('Any Value').' ---'),$this->_country);		

	    $limitidr = $this->_getParam('cust_limit_idr');
	    $limitidr = Application_Helper_General::convertDisplayMoney($limitidr);
	    $this->_setParam('cust_limit_idr',$limitidr); 

	    $limitusd = $this->_getParam('cust_limit_usd');
	    $limitusd = Application_Helper_General::convertDisplayMoney($limitusd);
	    $this->_setParam('cust_limit_usd',$limitusd);
      
  	
		if($this->_request->isPost())
		{ 
			$filters = array(//'cust_id'           => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
			                 //'cust_cif'          => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_name'         => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_type'         => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
			                 'cust_workfield'    => array('StripTags','StringTrim','HtmlEntities'),
						     'cust_address'      => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_city'         => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_zip'          => array('StripTags','StringTrim','HtmlEntities'),
			                 'country_code'      => array('StripTags','StringTrim','HtmlEntities'),
							 'cust_province'     => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_contact'      => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_phone'        => array('StripTags','StringTrim','HtmlEntities'),
						     'cust_ext'          => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_fax'          => array('StripTags','StringTrim','HtmlEntities'),
						     'cust_email'        => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_website'      => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_limit_idr'      => array('StripTags','StringTrim','HtmlEntities'),
			                 'cust_limit_usd'      => array('StripTags','StringTrim','HtmlEntities'),
			                );

			$validators =  array(/*'cust_id'        => array('NotEmpty',
      	                                                   'Alnum',
      	                                                   array('StringLength',array('min'=>6,'max'=>16)),
  	                                                       array('Db_NoRecordExists',array('table'=>'TEMP_MYONLINE_BIS','field'=>'CUST_ID')),
  	                                                       'messages' => array($this->language->_('Can not be empty'),
  	                                                                           $this->language->_('Invalid Company Code Format'),
																				$this->language->_('Company Code length min').' 6 '.$this->language->_('max') . '16',
  	                                                                           $this->language->_('Company code already in use. Please use another'),)
  	                                                    ),
          
      	                     'cust_cif'                 => 	array('NotEmpty',
										               			array('Db_NoRecordExists',array('table'=>'TEMP_MYONLINE_BIS','field'=>'CUST_CIF')),
												               'messages' => array(
												               						$this->language->_('Can not be empty'),
                                                                                   	$this->language->_('CIF is already suggested. Please use another')
                                                                                   )
												        	),*/                            

							 'cust_name'                => array('NotEmpty',
											               		'messages' => array($this->language->_('Can not be empty'),
								 					  	      	         		   )
												            ),					              
												              
                             'cust_type'                => array('allowEmpty' => true,
												              	 array('StringLength',array('max'=>16)),
												                 //array('InArray',array('haystack'=>$haystack_cust_type)),
                                                                 'messages' => array(
												              	 				   $this->language->_('The maximum characters allowed is').' 16',
												                                   )
											                ),

                            					              
                             'cust_workfield'            => array('allowEmpty' => true,
												                 array('StringLength',array('max'=>80)),
                                                                 'messages' => array(
												                                   	$this->language->_('The maximum characters allowed is').' 80',
												                                   )
											                ),
											                  
											                  
						     'cust_address'            => array('allowEmpty'=>true,
												                array('StringLength',array('max'=>200)),
												               'messages' => array(
												                                    //$this->language->_('Can not be empty'),
												                					$this->language->_('The maximum characters allowed is').' 200'
												                                   )
												            ),
												  
						  
												  
						     'cust_city'               => array(array('StringLength',array('min'=>1,'max'=>20)),
												               'allowEmpty' => true,
												               'messages' => array(
												                                   $this->language->_('The maximum characters allowed is').' 20'
												                                  )
												              ),
												  
						     'cust_zip'           => array(
												               'allowEmpty' => true,
						     									'Digits',
												               'messages' => array(
												                                	$this->language->_('Invalid Zip Code format'),
												                                  )
												            ),
												              
						     'country_code'           => array(
												               'NotEmpty',
												               'messages' => array(
												                              		$this->language->_('Can not be empty'),
												                                  )
												              ),				              
												  
                           'cust_province'      => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
												               'messages' => array($this->language->_('The maximum characters allowed is').' 128')
												              ),

                           'cust_contact'       => array('allowEmpty' => true,
												         array('StringLength',array('min'=>1,'max'=>128)),
												         'messages' => array(
												                             $this->language->_('The maximum characters allowed is').' 128')
												         ),

						    'cust_phone'        => array('Digits',
												         array('StringLength',array('min'=>1,'max'=>20)),
												         'allowEmpty'=>true,
												         'messages' => array($this->language->_('Invalid phone number format'),
												                             $this->language->_('The maximum characters allowed is').' 20',
												                             //$this->language->_('Can not be empty')
												                            )
												              ),					         
												         
                            'cust_ext'          => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
                            									'Digits',
												               'messages' => array(
												                                  $this->language->_('The maximum characters allowed is').' 128'
												                                   )
												              ),
												  
                            'cust_fax'          => array('Digits',
												         'allowEmpty' => true,
												         'messages' => array(
												                            $this->language->_('Invalid fax number format')
												                            )
												              ),
												  
                            'cust_email'        => array( 
												          array('StringLength',array('min'=>1,'max'=>128)),
												          'allowEmpty' => true,
												          'messages' => array(
												                             $this->language->_('Email length cannot be more than 128'),
												                             //$this->language->_('Can not be empty'),
												                             )
                                                        ),
												  
                            'cust_website'      => array(new Application_Validate_Hostname(),
                                                               array('StringLength',array('min'=>1,'max'=>100)),
												               'allowEmpty' => true,
                                                               'messages' => array($this->language->_('Invalid URL format'),
                                                               					   $this->language->_('The maximum characters allowed is').' 100'
                                                                                   )
                                                        ),
                            'cust_limit_idr' => array('allowEmpty'=>true,
															array('Between', array('min'=>0,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															(//'Can not be empty',
															$this->language->_('Value between').' 1 - 9999999999.99',
															$this->language->_('Must be numeric values'),
															)),
							'cust_limit_usd' => array('allowEmpty'=>true,
															array('Between', array('min'=>0,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															(//'Can not be empty',
															$this->language->_('Value between').' 1 - 9999999999.99',
															$this->language->_('Must be numeric values'),
															)),                             
							);

							
     
	       $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
	       
			//validasi user_id : paling sedikit harus punya 1 huruf dan 1 angka
		    // if($zf_filter_input->cust_id)
		    // {
		    //     $user_id =  $zf_filter_input->cust_id;
		    //     $check_alpha  = 0;
		    //     $check_digits = 0;
		    //     $strlen_user_id = strlen($user_id);

		    //     for($i=0; $i<$strlen_user_id; $i++)
		    //     {
		    //         if(Zend_Validate::is($user_id[$i],'Alpha'))  $check_alpha  = 1;
		    //         if(Zend_Validate::is($user_id[$i],'Digits')) $check_digits = 1;
		    //     }

		    //     if($check_alpha == 1 && $check_digits == 1) $flag_user_id = 'T';
		    //     else $flag_user_id = 'F';
		    // }
		    // else
		    // {
		    //      $flag_user_id = 'T';
		    // }

		    $flag_user_id = 'T';
	       
			//validasi multiple email
		    if($this->_getParam('cust_email'))
		    {
				$validate = new validate;
		      	$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('cust_email'));
		    }
		    else
		    {
		      	$cek_multiple_email = true;
		    }

	       
	      	if($zf_filter_input->isValid() && $flag_user_id == 'T' && $cek_multiple_email == true)
	       	{  

	       		$param = array();
			  	foreach($validators as $key=>$value)
			  	{
			  	   	$param[$key] = $zf_filter_input->getEscaped($key);	
			  	}

			  	$sessionNamespace = new Zend_Session_Namespace('bisFirst');
			  	$sessionNamespace->param = $param;

			  	$this->_redirect('/registerechannel/bisnis/user');
		  	}
			else
			{ 
			    $this->view->error = 1;

			    foreach($validators as $key => $value){
			    	$this->view->$key = ($zf_filter_input->isValid($key))? $zf_filter_input->$key : $this->_getParam($key);
				}
		        
		        
		    	$error = $zf_filter_input->getMessages();
													
				foreach($error as $keyRoot => $rowError)
				{
					foreach($rowError as $errorString)
					{
					   	  $keyname = "error_" . $keyRoot;
						  $this->view->$keyname = $this->language->_($errorString);
					}
				}
		        
		    	//if($flag_user_id == 'F')  $this->view->error_cust_id = $this->language->_('Invalid Company Code format');
		        
		   		if(isSet($cek_multiple_email) && $cek_multiple_email == false) $this->view->error_cust_email = $this->language->_('Invalid email format');		
			}
	    }
	    else{
	    	$sessionNamespace = new Zend_Session_Namespace('bisFirst');
	    	if(isset($sessionNamespace->param)){
	    		$param = $sessionNamespace->param;

	    		foreach($param as $key => $value){
	    			$this->view->$key = $value;
	    		}
	    	}
	    }
	}

	public function userAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('bisFirst');
		if(isset($sessionNamespace->param)){
			$param = $sessionNamespace->param;
			//$this->view->cust_id = $param['cust_id'];
			$this->view->cust_name = $param['cust_name'];
		}
		else{
			$this->view->errnotfound = 1;
			$this->view->errMsg = "Data not found";

			Zend_Session::namespaceUnset('bisFirst');
			Zend_Session::namespaceUnset('bisSecond');
		}

		$sessionUser = new Zend_Session_Namespace('bisSecond');
		if(isset($sessionUser->userlist))
			$userlist = $sessionUser->userlist;
		else
			$userlist = array();

		if($this->_request->isPost()){
			$saveParam = $this->_getParam('save');
			$nextParam = $this->_getParam('next');

			if($saveParam){
				$filters = array(//'user_id'       => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
			                 'user_fullname'     => array('StripTags','StringTrim','HtmlEntities'),
			                 'user_phone'        => array('StripTags','StringTrim','HtmlEntities'),
						     'user_ext'          => array('StripTags','StringTrim','HtmlEntities'),
						     'user_email'        => array('StripTags','StringTrim','HtmlEntities'),
			                 'user_privi'      	 => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
			                );

				$validators =  array(/*'user_id'      => array('NotEmpty',
      	                                                   //'Alnum',
      	                                                   array('StringLength',array('min'=>6,'max'=>17)),
  	                                                       'messages' => array($this->language->_('Can not be empty'),
  	                                                                           //$this->language->_('Invalid User ID Format'),
																				$this->language->_('User ID length min').' 6 '.$this->language->_('max') . '17',
  	                                                                           )
  	                                                    ),*/                          

				 			'user_fullname'         => array('NotEmpty',
										               		'messages' => array($this->language->_('Can not be empty'),
							 					  	      	         		   )
											            ),					              
												              
						    'user_phone'        => array('Digits',
												         array('StringLength',array('min'=>1,'max'=>20)),
												         'allowEmpty'=>true,
												         'messages' => array($this->language->_('Invalid phone number format'),
												                             $this->language->_('The maximum characters allowed is').' 20',
												                             //$this->language->_('Can not be empty')
												                            )
												              ),					         
												         
                            'user_ext'          => array(array('StringLength',array('min'=>1,'max'=>128)),
												               'allowEmpty' => true,
                            									'Digits',
												               'messages' => array(
												                                  $this->language->_('The maximum characters allowed is').' 128'
												                                   )
												              ),
												  
                            'user_email'        => array( 
												          array('StringLength',array('min'=>1,'max'=>128)),
												          'NotEmpty',
												          'messages' => array(
												                             $this->language->_('Email length cannot be more than 128'),
												                             $this->language->_('Can not be empty'),
												                             )
                                                        ),

												  
                            'user_privi'         => array('NotEmpty',
										               		'messages' => array($this->language->_('Can not be empty'),
							 					  	    	)
											            ),                        
							);

				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				if($zf_filter_input->isValid()){
					if(empty($userlist)){
						foreach($validators as $key => $value){
							$userlist[0][strtoupper($key)] = $zf_filter_input->getEscaped($key);
						}
					}
					else{
						foreach($userlist as $key => $val){
							if($val['USER_FULLNAME'] == $zf_filter_input->getEscaped('user_fullname'))
								$exist = 1;
						}

						if(!$exist){
							$ctr = count($userlist);
							foreach($validators as $key => $value){
								$userlist[$ctr][strtoupper($key)] = $zf_filter_input->getEscaped($key);
							}
						}
						else{
							$this->view->error = 1;
							$this->view->errMsg = "User already added";
						}
					}

					if(!$this->view->error){
						$sessionUser = new Zend_Session_Namespace('bisSecond');
						$sessionUser->userlist = $userlist;

						$this->view->display = "";
						$this->view->displayForm = "display:none;";
					}
					else{
						$this->view->display = "display:none;";
						$this->view->displayForm = "";
					}
				}
				else{
					foreach($validators as $key=>$value)
					{
						$this->view->$key = $this->_getParam($key);
					}
					
					$error = $zf_filter_input->getMessages();
											
					foreach($error as $keyRoot => $rowError)
					{
					   foreach($rowError as $errorString)
					   {
					   	  $keyname = "error_" . $keyRoot;
						  $this->view->$keyname = $this->language->_($errorString);
					   }
					}

					$this->view->display = "display:none;";
					$this->view->displayForm = "";
				}
			}
			elseif($nextParam){
				$filterArr = array('list_id' => 'StringTrim','StripTags','StringToUpper','HtmlEntities');

	        	$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getPost());

	        	if(is_array($zf_filter->list_id)){
	        		$list_id = $zf_filter->list_id;

	        		foreach($userlist as $key => $row){
	        			if(!in_array($row['USER_FULLNAME'], $list_id))
	        				unset($userlist[$key]);
	        		}

	        		$sessionUser = new Zend_Session_Namespace('bisSecond');
	        		$sessionUser->userlist = $userlist;

	        		$this->_redirect('/registerechannel/bisnis/account');
	        	}
	        	else{
	        		if(empty($zf_filter->list_id)){
	        			$this->view->error = 1;
	        			$this->view->errMsg = "Please choose at least one user";

	        			$this->view->display = "";
						$this->view->displayForm = "display:none;";
	        		}
	        	}
			}
		}
		else{
			if(empty($userlist)){
				$this->view->display = "display:none;";
				$this->view->displayForm = "";
			}
			else{
				$this->view->display = "";
				$this->view->displayForm = "display:none;";
			}
		}

		$this->view->userlist = $userlist;
	}

	public function accountAction()
	{
		$model = new registerechannel_Model_Registerechannel();
		$ccyArr = Application_Helper_Array::listArray($model->getCCY(),'CCY_ID','CCY_ID');
		$this->view->ccyArr = array_merge(array(''=>'--- '.$this->language->_('Any Value').' ---'),$ccyArr);

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->errMsg = $msg;
     		}
    	}

    	$sessionNamespace = new Zend_Session_Namespace('bisThird');
		if(isset($sessionNamespace->acclist)){
			$acclist = $sessionNamespace->acclist;
		}
		else{
			$acclist = array();
		}

		$addParam = $this->_getParam('add');
		$submitParam = $this->_getParam('submit');

		if($this->_request->isPost()){
			if($addParam){

				$filters = 	array( 
									'acct_no'			=> 	array('StringTrim','StripTags','HtmlEntities'),
									'acct_name'			=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
									'acct_ccy'			=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
									'acct_ownership'	=> 	array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
								);

				$validators = array	( 
										'acct_no' 		=> 	array	(	
																		'NotEmpty',
																		array('StringLength',array('min'=>1,'max'=>50)),
																		'messages' => array	(
																								$this->language->_("Can not be empty"),
																								$this->language->_("Account number maximum length is")." 50",
																							)
																	),
										'acct_name' 	=> 	array	(	
																		'NotEmpty',
																		array('StringLength',array('min'=>1,'max'=>50)),
																		'messages' => array	(
																								$this->language->_("Can not be empty"),
																								$this->language->_("Name maximum length is")." 50",
																							)
																	),
										'acct_ccy'		=> array('NotEmpty',
										               				'messages' => array($this->language->_('Can not be empty'),
							 					  	    		)
											            	),
										'acct_ownership'=> array('NotEmpty',
										               				'messages' => array($this->language->_('Can not be empty'),
							 					  	    		)
											            	),  
									);

				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

				if($zf_filter_input->isValid()){
					if(empty($acclist)){
						$acclist[0]['ACCT_NO'] = $zf_filter_input->getEscaped('acct_no');
						$acclist[0]['ACCT_NAME'] = $zf_filter_input->getEscaped('acct_name');
						$acclist[0]['ACCT_CCY'] = $zf_filter_input->getEscaped('acct_ccy');
						$acclist[0]['ACCT_OWNERSHIP'] = $zf_filter_input->getEscaped('acct_ownership');
					}
					else{
						$acct_no = $zf_filter_input->getEscaped('acct_no');
						foreach($acclist as $key => $val){
							if($val['ACCT_NO'] == $acct_no)
								$exist = 1;
						}

						if(!$exist){
							$ctr = count($acclist);
							$acclist[$ctr]['ACCT_NO'] = $acct_no;
							$acclist[$ctr]['ACCT_NAME'] = $zf_filter_input->getEscaped('acct_name');
							$acclist[$ctr]['ACCT_CCY'] = $zf_filter_input->getEscaped('acct_ccy');
							$acclist[$ctr]['ACCT_OWNERSHIP'] = $zf_filter_input->getEscaped('acct_ownership');
						}
						else{
							$this->view->error = 1;
							$this->view->errMsg = "Account number already added";
						}
					}

					if(!$this->view->error){
						$sessionNamespace = new Zend_Session_Namespace('bisThird');
						$sessionNamespace->acclist = $acclist;
					}
				}
				else{
					foreach($validators as $key=>$value)
					{
						$this->view->$key = $this->_getParam($key);
					}
					
					$error = $zf_filter_input->getMessages();
											
					foreach($error as $keyRoot => $rowError)
					{
					   foreach($rowError as $errorString)
					   {
					   	  $keyname = "error_" . $keyRoot;
						  $this->view->$keyname = $this->language->_($errorString);
					   }
					}
				}
			}
			elseif($submitParam){
				$this->_redirect('/registerechannel/bisnis/confirm');
			}
		}
		else{
			$delParam = $this->_getParam('delete');

			if($delParam){
				if(empty($acclist)){
					$errMsg = 'There is no account in the list';

					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errMsg);
				}
				else{
					$filter = new Zend_Filter_StringTrim();
					$filter2 = new Zend_Filter_StripTags();
					$filter3 = new Zend_Filter_HtmlEntities();

					$delParam = $filter->filter($delParam);
					$delParam = $filter2->filter($delParam);
					$delParam = $filter3->filter($delParam);

					$acct_no = $delParam;
					$exist = 0;

					foreach($acclist as $key => $val){
						if($val['ACCT_NO'] == $acct_no){
							$exist = 1;
							$delkey = $key;
						}
					}

					if($exist){
						unset($acclist[$key]);
						$errMsg = "Account deleted from the list";

						$sessionNamespace = new Zend_Session_Namespace('bisThird');
						$sessionNamespace->acclist = $acclist;

						$this->_helper->getHelper('FlashMessenger')->addMessage('S');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errMsg);
					}
					else{
						$errMsg = "Account doesn't exist in the list";
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errMsg);
					}
				}

				$this->_redirect('/registerechannel/bisnis/account');
			}
		}

		$sessionNamespace = new Zend_Session_Namespace('bisThird');
		if(isset($sessionNamespace->acclist)){
			$acclist = $sessionNamespace->acclist;
			$this->view->acclist = $acclist;
		}
	}

	public function confirmAction()
	{
		$model = new registerechannel_Model_Registerechannel();

		$this->view->countryArr = $this->_country;

		$sessionNamespace = new Zend_Session_Namespace('bisFirst');
		if(isset($sessionNamespace->param)){
			$custdata = $sessionNamespace->param;
			foreach($custdata as $key => $value){
				$this->view->$key = $value;
			}

			$sessionNamespace2 = new Zend_Session_Namespace('bisSecond');
			if(isset($sessionNamespace2->userlist)){
				$userlist = $sessionNamespace2->userlist;
				$this->view->userlist = $userlist;
			}

			$sessionNamespace3 = new Zend_Session_Namespace('bisThird');
			if(isset($sessionNamespace3->acclist)){
				$acclist = $sessionNamespace3->acclist;
				$this->view->acclist = $acclist;
			}
		}
		else{
			Zend_Session::namespaceUnset('bisFirst');
			Zend_Session::namespaceUnset('bisSecond');
			Zend_Session::namespaceUnset('bisThird');

			$this->view->errnotfound = 1;
			$this->view->errMsg = "Data not found";
		}


		if($this->_request->isPost()){
			$agree = $this->_getParam('agreement');

			if($agree){
				try{
					$insertArr = array();
					foreach($custdata as $key => $val){
						$insertArr[strtoupper($key)] = $val;
					}

					$typeTitle = "My Online (Bisnis)";
					$changeType = "N";
					$change_id = $this->suggestionWaitingApproval($typeTitle,null,$changeType,null,null,'TEMP_MYONLINE_BIS','BIS',$insertArr['CUST_NAME']);

					$insertArr['CREATED'] = date('Y-m-d H:i:s');
					$insertArr['CREATEDBY'] = 'SYSTEM';
					$insertArr['CHANGES_ID'] = $change_id;

					$id = $model->insertMyOnlineReg('bis',$insertArr);

					if(!empty($userlist)){
						foreach($userlist as $row){
							$insertUser = array();

							$insertUser['CUST_ID'] = $id;
							//$insertUser['USER_ID'] = $row['USER_ID'];
							$insertUser['USER_FULLNAME'] = $row['USER_FULLNAME'];
							$insertUser['USER_PHONE'] = $row['USER_PHONE'];
							$insertUser['USER_EXT'] = $row['USER_EXT'];
							$insertUser['USER_EMAIL'] = $row['USER_EMAIL'];
							$insertUser['USER_PRIVI'] = $row['USER_PRIVI'];

							$model->insertMyOnlineBisUser($insertUser);
						}
					}

					if(!empty($acclist)){
						foreach($acclist as $row){
							$insertAcc = array();

							$insertAcc['CUST_ID'] = $id;
							$insertAcc['ACCT_NO'] = $row['ACCT_NO'];
							$insertAcc['ACCT_NAME'] = $row['ACCT_NAME'];
							$insertAcc['ACCT_CCY'] = $row['ACCT_CCY'];
							$insertAcc['ACCT_OWNERSHIP'] = $row['ACCT_OWNERSHIP'];

							$model->insertMyOnlineBisAcc($insertAcc);
						}
					}

					$this->_redirect('/registerechannel/bisnis/success');
				}
				catch(Exception $e){
					echo $e;
					$this->view->error = 1;
					$this->view->errMsg = "Failed to add data";
				}
			}
			else{
				$this->view->error = 1;
				$this->view->errMsg = "Please check agreement";
			}
		}
	}

	public function successAction()
	{
		Zend_Session::namespaceUnset('bisFirst');
		Zend_Session::namespaceUnset('bisSecond');
		Zend_Session::namespaceUnset('bisThird');
	}
}
