<?php
require_once 'General/BankUser.php';
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class Printstage_ListController extends Application_Main
{
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        Application_Helper_General::writeLog('PSBG', "View Printing Stage List");

        $fields = array(
            'regno'     => array(
                'field'    => 'BG_REG_NUMBER',
                'label'    => $this->language->_('BG Number') . ' / ' . $this->language->_('Subject'),
            ),
            // 'bgnum'     => array(
            //     'field'    => 'BG_NUMBER',
            //     'label'    => $this->language->_('BG Number'),
            // ),
            'app' => array(
                'field' => 'CUST_ID',
                'label' => $this->language->_('Prinsipal'),
            ),

            'obligee' => array(
                'field' => 'RECIPIENT_NAME',
                'label' => $this->language->_('Obligee'),
            ),

            'branch'     => array(
                'field'    => 'BG_BRANCH',
                'label'    => $this->language->_('Bank Branch'),
            ),

            'countertype'     => array(
                'field'    => 'COUNTER_WARRANTY_TYPE',
                'label'    => $this->language->_('Counter Type'),
            ),
            'type'  => array(
                'field'    => 'BRANCH_RELEASE',
                'label'    => $this->language->_('Cabang Penerbit')
            ),
            'status' => array(
                'field' => 'BRANCH_PRINT',
                'label' => $this->language->_('Cabang Pencetak'),
            ),
        );

        //add reza
        /*$filterlist = array(
            "BG_REG_NUMBER",
            'BG_SUBJECT' => "SUBJECT",
            //'BG_PERIOD' => "PERIOD TIME",
            "APPLICANT" => "APPLICANT",
            "OBLIGEE_NAME" => "OBLIGEE NAME",
            //"BG_AMOUNT" => "AMOUNT",
            "BRANCH" => "BRANCH",
            "COUNTER_TYPE" => "COUNTER TYPE"
        );*/
        $filterlist = array("BG_NUMBER", "BG_SUBJECT", "APPLICANT", "OBLIGEE_NAME", "BRANCH", "COUNTER_TYPE", "CABANG_PENERBIT", "CABANG_PENCETAK");
        $this->view->filterlist = $filterlist;

        $page    = $this->_getParam('page');

        $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
        $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

        $this->view->currentPage = $page;
        $this->view->sortBy      = $sortBy;
        $this->view->sortDir     = $sortDir;

        //end reza

        $warantyIn = ['1', '2', '3'];
        // Add Bahri
        // if ($this->view->hasPrivilege("ACCS")) { // Cash Collateral
        //     $warantyIn = ['1'];
        // }
        // if ($this->view->hasPrivilege("ANCS")) { // Non-Cash Collateral
        //     $warantyIn = ['2', '3'];
        // }
        // if ($this->view->hasPrivilege("ACCS") && $this->view->hasPrivilege("ANCS")) { // All
        //     $warantyIn = ['1', '2', '3'];
        // }
        $select = $this->_db->select()
            ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH', "BUSER_ID"))
            ->joinLeft(["BR" => "M_BRANCH"], "A.BUSER_BRANCH = BR.ID")
            ->where('A.BUSER_ID = ?', $this->_userIdLogin);
        $buser = $this->_db->fetchRow($select);
        $branchUser = $buser['BUSER_BRANCH'];

        // END Bahri

        $selectbg = $this->_db->select()
            ->from(array('A' => 'T_BANK_GUARANTEE'), array(
                'REG_NUMBER' => 'BG_REG_NUMBER',
                'BG_NUMBER' => 'BG_NUMBER',
                'SUBJECT' => 'BG_SUBJECT',
                'CREATED' => 'BG_CREATED',
                //'TYPE' => (string)'TYPE',
                'TIME_PERIOD_START',
                'TIME_PERIOD_END',
                'CREATEDBY' => 'BG_CREATEDBY',
                'AMOUNT' => 'BG_AMOUNT',
                'COUNTER_WARRANTY_TYPE',
                'BG_INSURANCE_CODE',
                'FULLNAME' => 'T.USER_FULLNAME',
                'SP_OBLIGEE_CODE',
                'RECIPIENT_NAME',
                'B.BRANCH_NAME',
                'C.CUST_NAME',
                'IS_AMENDMENT' => 'A.CHANGE_TYPE',
                "BG_BRANCH_PUBLISHER"

            ))
            ->joinLeft(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID AND A.CUST_ID = T.CUST_ID')
            ->joinLeft(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID')
            ->joinLeft(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE')
            // ->joinLeft(array('TU' => 'M_BUSER'), 'A.BG_UPDATEDBY = TU.BUSER_ID')
            ->joinLeft(array('TU' => 'M_BRANCH'), 'A.BG_BRANCH = TU.BRANCH_CODE')
            ->joinLeft(array('BR' => 'M_BRANCH'), 'BR.ID = TU.ID', [
                "BRANCH_RELEASE" => "B.BRANCH_NAME"
            ])
            ->joinLeft(array('BP' => 'M_BRANCH'), 'A.BG_BRANCH_PUBLISHER = BP.BRANCH_CODE', [
                "BRANCH_PUBLISHER" => "BP.BRANCH_NAME"
            ])

            //->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
            ->where('A.BG_STATUS = 15')
            // ->where("A.BG_BRANCH_PUBLISHER = '" . $buser["BRANCH_CODE"] . "' OR TU.BUSER_BRANCH = '" . $buser["BUSER_BRANCH"] . "'")
            ->where("A.REPRINT IS NULL OR A.REPRINT = 0")
            ->where("A.COMPLETION_STATUS = 2")
            ->where("B.BRANCH_CODE = ? OR A.BG_BRANCH_PUBLISHER = ?", $buser["BRANCH_CODE"])
            // ->where("A.BG_UPDATEDBY = '" . $buser["BUSER_ID"] . "' OR A.BG_BRANCH_PUBLISHER = '" . $buser["BRANCH_CODE"] . "'")
            //->where('B.ID = ?', $branchUser) // Add Bahri
            ->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn); // Add Bahri


        $auth = Zend_Auth::getInstance()->getIdentity();
        if ($auth->userHeadQuarter == "NO") {
            $selectbg->where("A.BG_BRANCH_PUBLISHER = '" . $buser["BRANCH_CODE"] . "' OR TU.ID = '" . $buser["BUSER_BRANCH"] . "'"); // Add Bahri
        }

        $selectbg->order('A.BG_CREATED DESC');

        //->query()->fetchAll();
        // $auth = Zend_Auth::getInstance()->getIdentity();
        // if ($auth->userHeadQuarter == "NO") {
        //     $selectbg->where('B.ID = ?', $branchUser); // Add Bahri
        // }

        // select branch ------------------------------------------------------------
        $select_branch = $this->_db->select()
            ->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
            ->query()->fetchAll();

        $save_branch = [];

        foreach ($select_branch as $key => $value) {
            $save_branch[$value["BRANCH_CODE"]] = $value["BRANCH_NAME"];
        }

        $this->view->sel_branch = $save_branch;
        //  --------------------------------------------------------------------------

        // select counter type ------------------------------------------------------------
        $save_counter_type = [
            1 => 'FC',
            2 => 'LF',
            3 => 'Insurance'
        ];

        $this->view->counter_type = $save_counter_type;
        //  -------------------------------------------------------------------------- 

        $selectlc = $this->_db->select()
            ->from(array('A' => 'T_LC'), array(
                'REG_NUMBER' => 'LC_REG_NUMBER',
                'SUBJECT' => 'LC_CREDIT_TYPE',
                'CREATED' => 'LC_CREATED',
                'CCYID' => 'LC_CCY',
                //'TYPE' => (string)'TYPE',
                'TIME_PERIOD_END' => 'LC_EXPDATE',
                'CREATEDBY' => 'LC_CREATEDBY',
                'AMOUNT' => 'LC_AMOUNT',

                'FULLNAME' => 'T.USER_FULLNAME'
            ))
            ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
            //->where('A.LC_CUST ='.$this->_db->quote((string)$this->_custIdLogin))
            ->where('A.LC_STATUS = 1')
            ->order('A.LC_CREATED DESC');
        // ->query()->fetchAll();


        //$result = array_merge($selectbg, $selectlc);

        //$this->paging($result);

        $conf = Zend_Registry::get('config');


        $this->view->bankname = $conf['app']['bankname'];


        $config     = Zend_Registry::get('config');
        $BgType     = $config["bg"]["status"]["desc"];
        $BgCode     = $config["bg"]["status"]["code"];

        $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

        $this->view->arrStatus = $arrStatus;

        $arrWaranty = array(
            1 => 'FC',
            2 => 'LF',
            3 => 'INS'
        );
        $this->view->arrWaranty = $arrWaranty;

        $arrType = array(
            1 => 'Standart',
            2 => 'Custom'
        );

        $arrLang = array(
            1 => 'Indonesian',
            2 => 'English',
            3 => 'Bilingual'
        );
        $this->view->langArr = $arrLang;
        $this->view->formatArr = $arrType;
        $this->view->fields = $fields;


        $filterArr = array(
            'filter'    =>  array('StripTags'),
            'BG_NUMBER'       =>  array('StringTrim', 'StripTags', 'StringToUpper'),
            'BG_SUBJECT'       =>  array('StringTrim', 'StripTags', 'StringToUpper'),
            'APPLICANT'       =>  array('StringTrim', 'StripTags', 'StringToUpper'),
            'OBLIGEE_NAME'       =>  array('StringTrim', 'StripTags', 'StringToUpper'),
            'BRANCH'       =>  array('StringTrim', 'StripTags', 'StringToUpper'),
            'COUNTER_TYPE'       =>  array('StringTrim', 'StripTags', 'StringToUpper'),
        );

        $validator = array(
            'BG_NUMBER'       =>  array(),
            'BG_SUBJECT'       =>  array(),
            'APPLICANT'       =>  array(),
            'OBLIGEE_NAME'       =>  array(),
            'BRANCH'       =>  array(),
            'COUNTER_TYPE'       =>  array(),
        );

        $dataParam = array("BG_NUMBER", "BG_SUBJECT", "APPLICANT", "OBLIGEE_NAME", "BRANCH", "COUNTER_TYPE", "CABANG_PENERBIT", "CABANG_PENCETAK");
        $dataParamValue = array();

        $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
        $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);

        foreach ($dataParam as $no => $dtParam) {

            if (!empty($this->_request->getParam('wherecol'))) {
                $dataval = $this->_request->getParam('whereval');
                // print_r($dataval);
                $order = 0;
                foreach ($this->_request->getParam('wherecol') as $key => $value) {

                    if ($dtParam == $value) {
                        $dataParamValue[$dtParam] = $dataval[$order];
                    }
                    $order++;
                }
            }
        }

        $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

        $filter         = $this->_getParam('filter');
        $BG_REG_NUMBER        = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));
        $BG_SUBJECT        = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
        $APPLICANT        = html_entity_decode($zf_filter->getEscaped('APPLICANT'));
        $OBLIGEE_NAME        = html_entity_decode($zf_filter->getEscaped('OBLIGEE_NAME'));
        $BRANCH        = html_entity_decode($zf_filter->getEscaped('BRANCH'));
        $COUNTER_TYPE        = html_entity_decode($zf_filter->getEscaped('COUNTER_TYPE'));

        if ($filter == TRUE) {
            $append_query = "";

            // if ($bgNubmer != null) {
            if ($BG_REG_NUMBER != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "A.BG_NUMBER LIKE " . $this->_db->quote('%' . $BG_REG_NUMBER . '%');
            }

            if ($BG_SUBJECT != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $BG_SUBJECT . '%');
            }

            if ($APPLICANT != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "C.CUST_NAME LIKE " . $this->_db->quote('%' . $APPLICANT . '%');
            }

            if ($OBLIGEE_NAME != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $OBLIGEE_NAME . '%');
            }

            if ($BRANCH != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "B.BRANCH_CODE LIKE " . $this->_db->quote('%' . $BRANCH . '%');
            }

            if ($COUNTER_TYPE != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "A.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $COUNTER_TYPE . '%');
            }

            if ($COUNTER_TYPE != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "A.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $COUNTER_TYPE . '%');
            }

            if ($dataParamValue["CABANG_PENERBIT"] != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "BR.BRANCH_CODE LIKE " . $this->_db->quote('%' . $dataParamValue["CABANG_PENERBIT"] . '%');
            }

            if ($dataParamValue["CABANG_PENCETAK"] != null) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "BP.BRANCH_CODE LIKE " . $this->_db->quote('%' . $dataParamValue["CABANG_PENCETAK"] . '%');
            }

            if ($append_query != "") {
                $selectbg->where($append_query);
            }
        }

        // $selectbg->order($sortBy.' '.$sortDir);


        $this->view->fields = $fields;
        $this->view->filter = $filter;

        $selectbg = $this->_db->fetchAll($selectbg);
        $selectlc = $this->_db->fetchAll($selectlc);
        $result = array_merge($selectbg, $selectlc);

        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token   = $rand;
        $this->view->token = $sessionNamespace->token;

        foreach ($result as $key => $value) {
            $get_reg_number = $value["REG_NUMBER"];

            $AESMYSQL = new Crypt_AESMYSQL();
            $rand = $this->token;

            $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
            $encpayreff = urlencode($encrypted_payreff);

            $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
        }

        //Zend_Debug::dump($result);

        $this->paging($result);

        $csv = $this->_getParam('csv');
        if ($this->_request->isPost()) {
            if ($csv) {

                $array_header = [
                    $this->language->_('No'),
                    $this->language->_('BG Number / Subject'),
                    $this->language->_('Prinsipal'),
                    $this->language->_('Obligee'),
                    $this->language->_('Branch'),
                    $this->language->_('Counter Type'),
                    $this->language->_('Cabang Penerbit'),
                    $this->language->_('Cacbang Pencetak'),
                ];

                $csv_data = [];
                foreach ($result as $key => $value) {

                    if ($value['IS_AMENDMENT'] == 1) {
                        $type = 'Amendment Changes';
                    } else if ($value['IS_AMENDMENT'] == 0) {
                        $type = "New";
                    } else {
                        $type = "Amendment Draft";
                    }

                    $status = $this->view->arrStatus[$value["BG_STATUS"]];

                    if ($value["BG_STATUS"] == 17) {
                        $signing_status = (empty($value["SIGNING_STATUS"])) ? 0 : $value["SIGNING_STATUS"];
                        $status = $this->view->arrStatus[$value["BG_STATUS"]] . " (" . strval($signing_status) . "/2)";
                    }

                    $temp = [
                        $key + 1,
                        $value["BG_NUMBER"] . " / " . $value["SUBJECT"],
                        $value['CUST_NAME'] . "(" . $value['CUST_ID'] . ")",
                        $value['RECIPIENT_NAME'],
                        $value['BRANCH_NAME'],
                        $this->view->arrWaranty[$value['COUNTER_WARRANTY_TYPE']],
                        $value["BRANCH_RELEASE"],
                        $value["BRANCH_PUBLISHER"],
                    ];

                    array_push($csv_data, $temp);
                }

                Application_Helper_General::writeLog('PSBG', 'Download CSV Printing Stage List');
                $this->_helper->download->csv($array_header, $csv_data, null, $this->language->_('Printing Stage List') . " - " . date("dMYHis"));
            }
        }

        if (!empty($dataParamValue)) {


            foreach ($dataParamValue as $key => $value) {
                $duparr = explode(',', $value);

                if (!empty($duparr)) {

                    foreach ($duparr as $ss => $vs) {
                        $wherecol[]  = $key;
                        $whereval[] = $vs;
                    }
                } else {
                    $wherecol[]  = $key;
                    $whereval[] = $value;
                }
            }

            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }
    }
}
