
function checkDateRange(name){
	var tdate1 = document.getElementsByName(name)[0].value;
	var day1 = tdate1.split("/")[0];
	var month1 = tdate1.split("/")[1];
	var year1 = tdate1.split("/")[2];
	var date1 = new Date(year1,month1,day1);

	if(name == "fDateFrom"){
		var tdate2 = document.getElementsByName("fDateTo")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("fDateTo")[0].value = tdate1;
		}
	}
	else if(name == "fDateTo"){
		var tdate2 = document.getElementsByName("fDateFrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("fDateFrom")[0].value = tdate1;
		}
	}
	else if(name == "fDateFrom2"){
		var tdate2 = document.getElementsByName("fDateTo2")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("fDateTo2")[0].value = tdate1;
		}
	}
	else if(name == "fDateTo2"){
		var tdate2 = document.getElementsByName("fDateFrom2")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("fDateFrom2")[0].value = tdate1;
		}
	}
	else if(name == "latestSuggestionFrom"){
		var tdate2 = document.getElementsByName("latestSuggestionTo")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("latestSuggestionTo")[0].value = tdate1;
		}
	}
	else if(name == "latestSuggestionTo"){
		var tdate2 = document.getElementsByName("latestSuggestionFrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("latestSuggestionFrom")[0].value = tdate1;
		}
	}
	else if(name == "latestApprovalFrom"){
		var tdate2 = document.getElementsByName("latestApprovalTo")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("latestApprovalTo")[0].value = tdate1;
		}
	}
	else if(name == "latestApprovalTo"){
		var tdate2 = document.getElementsByName("latestApprovalFrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("latestApprovalFrom")[0].value = tdate1;
		}
	}
	else if(name == "suggestionFrom"){
		var tdate2 = document.getElementsByName("suggestionTo")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("suggestionTo")[0].value = tdate1;
		}
	}
	else if(name == "suggestionTo"){
		var tdate2 = document.getElementsByName("suggestionFrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("suggestionFrom")[0].value = tdate1;
		}
	}
	else if(name == "approvalFrom"){
		var tdate2 = document.getElementsByName("approvalTo")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("approvalTo")[0].value = tdate1;
		}
	}
	else if(name == "approvalTo"){
		var tdate2 = document.getElementsByName("approvalFrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("approvalFrom")[0].value = tdate1;
		}
	}
	else if(name == "ufDateFrom"){
		var tdate2 = document.getElementsByName("ufDateTo")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("ufDateTo")[0].value = tdate1;
		}
	}
	else if(name == "ufDateTo"){
		var tdate2 = document.getElementsByName("ufDateFrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("ufDateFrom")[0].value = tdate1;
		}
	}
	else if(name == "createdfrom"){
		var tdate2 = document.getElementsByName("createdto")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("createdto")[0].value = tdate1;
		}
	}
	else if(name == "createdto"){
		var tdate2 = document.getElementsByName("createdfrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("createdfrom")[0].value = tdate1;
		}
	}
	else if(name == "updatedfrom"){
		var tdate2 = document.getElementsByName("updatedto")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("updatedto")[0].value = tdate1;
		}
	}
	else if(name == "updatedto"){
		var tdate2 = document.getElementsByName("updatedfrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("updatedfrom")[0].value = tdate1;
		}
	}
	else if(name == "transferfrom"){
		var tdate2 = document.getElementsByName("transferto")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("transferto")[0].value = tdate1;
		}
	}
	else if(name == "transferto"){
		var tdate2 = document.getElementsByName("transferfrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("transferfrom")[0].value = tdate1;
		}
	}
	else if(name == "changesCreatedFrom"){
		var tdate2 = document.getElementsByName("changesCreatedTo")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 > date2){
			document.getElementsByName("changesCreatedTo")[0].value = tdate1;
		}
	}
	else if(name == "changesCreatedTo"){
		var tdate2 = document.getElementsByName("changesCreatedFrom")[0].value;
		var day2 = tdate2.split("/")[0];
		var month2 = tdate2.split("/")[1];
		var year2 = tdate2.split("/")[2];
		var date2 = new Date(year2,month2,day2);

		if(date1 < date2){
			document.getElementsByName("changesCreatedFrom")[0].value = tdate1;
		}
	}
}

function openNav(){
    document.getElementById('myNav').style.width = "235px";
}

function closeNav(){
    document.getElementById('myNav').style.width = "0";
}
