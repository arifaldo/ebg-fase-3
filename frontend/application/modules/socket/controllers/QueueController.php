<?php

class socket_QueueController extends Application_Main
{
	public function initController(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function selectAction()
    {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$config = Zend_Registry::get('config');
		//firewall login
		if(!in_array($_SERVER['REMOTE_ADDR'], $config['app']['ip'])){
			echo 'FORBIDDEN Acces From '.$_SERVER['REMOTE_ADDR'];
			die;
		}
		
		$type 	   = $this->_getParam('type');
		$isDisable = $config['disableFrontendApplication'];
    	$iso	   = "";

    	if ($isDisable == 0)
    	{
	    	$dataQueue = Application_Helper_Service::getISOQueue($type);
	    	
	    	if (!empty($dataQueue))
	    	{	$iso = $dataQueue['TRACE_NO']."|||".$dataQueue['MSG_SEND'];		}
    	}
    	
    	echo $iso;
    }

	public function updateflagAction()
    {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$traceNo 	= $this->_getParam('traceNo');
		$isoStatus	= $this->_getParam('isoStatus');
		
		Application_Helper_Service::flagISOQueue($traceNo,$isoStatus);
		return true;
    }
	
	public function receiveisoAction()
    {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$traceNo 	= $this->_getParam('traceNo');
		$receiveData	= $this->_getParam('receiveData');
		
		Application_Helper_Service::updateISOQueue($traceNo,$receiveData);

		return true;
    }

	public function gettracenoAction()
    {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$traceNo = Service_ISO::getTraceNo();
    	echo $traceNo;
    }
   
}
