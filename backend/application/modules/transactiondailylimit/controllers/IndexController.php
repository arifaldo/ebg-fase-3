<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class Transactiondailylimit_IndexController extends Application_Main
{

    public function indexAction()
	{
		$model = new transactiondailylimit_Model_Transactiondailylimitsetting();
		$select = $model->getSetting();
		$setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		//Zend_Debug::dump($value,__FILE__);
		foreach($setting as $key=>$value){
			$this->view->$key = $value;
		}

		$purchase_limit_per_trx = $this->_getParam('purchase_limit_per_trx');
		$purchase_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($purchase_limit_per_trx);
		$this->_setParam('purchase_limit_per_trx',$purchase_limit_per_trx_val);

		$purchase_limit_per_day = $this->_getParam('purchase_limit_per_day');
		$purchase_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($purchase_limit_per_day);
		$this->_setParam('purchase_limit_per_day',$purchase_limit_per_day_val);

		$pb_limit_per_trx = $this->_getParam('pb_limit_per_trx');
		$pb_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($pb_limit_per_trx);
		$this->_setParam('pb_limit_per_trx',$pb_limit_per_trx_val);

		$pb_limit_per_day = $this->_getParam('pb_limit_per_day');
		$pb_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($pb_limit_per_day);
		$this->_setParam('pb_limit_per_day',$pb_limit_per_day_val);
//		
//		
		$pbusd_limit_per_trx = $this->_getParam('pbusd_limit_per_trx');
		$pbusd_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($pbusd_limit_per_trx);
		$this->_setParam('pbusd_limit_per_trx',$pbusd_limit_per_trx_val);

		$pbusd_limit_per_day = $this->_getParam('pbusd_limit_per_day');
		$pbusd_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($pbusd_limit_per_day);
		$this->_setParam('pbusd_limit_per_day',$pbusd_limit_per_day_val);

		$rtgs_limit_per_trx = $this->_getParam('rtgs_limit_per_trx');
		$rtgs_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($rtgs_limit_per_trx);
		$this->_setParam('rtgs_limit_per_trx',$rtgs_limit_per_trx_val);

		$rtgs_limit_per_day = $this->_getParam('rtgs_limit_per_day');
		$rtgs_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($rtgs_limit_per_day);
		$this->_setParam('rtgs_limit_per_day',$rtgs_limit_per_day_val);

		$skn_limit_per_trx = $this->_getParam('skn_limit_per_trx');
		$skn_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($skn_limit_per_trx);
		$this->_setParam('skn_limit_per_trx',$skn_limit_per_trx_val);

		$skn_limit_per_day = $this->_getParam('skn_limit_per_day');
		$skn_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($skn_limit_per_day);
		$this->_setParam('skn_limit_per_day',$skn_limit_per_day_val);

		
		
		$payment_limit_per_trx = $this->_getParam('payment_limit_per_trx');
		$payment_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($payment_limit_per_trx);
		$this->_setParam('payment_limit_per_trx',$payment_limit_per_trx_val);

		$payment_limit_per_day = $this->_getParam('payment_limit_per_day');
		$payment_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($payment_limit_per_day);
		$this->_setParam('payment_limit_per_day',$payment_limit_per_day_val);

		$domonline_limit_per_trx = $this->_getParam('domonline_limit_per_trx');
		$domonline_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($domonline_limit_per_trx);
		$this->_setParam('domonline_limit_per_trx',$domonline_limit_per_trx_val);

		$domonline_limit_per_day = $this->_getParam('domonline_limit_per_day');
		$domonline_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($domonline_limit_per_day);
		$this->_setParam('domonline_limit_per_day',$domonline_limit_per_day_val);

		//FILTER
		{
			$filters = array(
				'purchase_limit_per_trx' => array('StringTrim','StripTags'),
				'purchase_limit_per_day' => array('StringTrim','StripTags'),
// 				'remittance_limit_per_month' => array('StringTrim','StripTags'),
				'pb_limit_per_trx' => array('StringTrim','StripTags'),
				'pb_limit_per_day' => array('StringTrim','StripTags'),
				'pbusd_limit_per_trx' => array('StringTrim','StripTags'),
				'pbusd_limit_per_day' => array('StringTrim','StripTags'),
				'rtgs_limit_per_trx' => array('StringTrim','StripTags'),
				'rtgs_limit_per_day' => array('StringTrim','StripTags'),
				'skn_limit_per_trx' => array('StringTrim','StripTags'),
				'skn_limit_per_day' => array('StringTrim','StripTags'),
				'payment_limit_per_trx' => array('StringTrim','StripTags'),
				'payment_limit_per_day' => array('StringTrim','StripTags'),
				'domonline_limit_per_trx' => array('StringTrim','StripTags'),
				'domonline_limit_per_day' => array('StringTrim','StripTags'),
			);
		}

		//VALIDATE
		{
			$validators = array(
				'purchase_limit_per_trx' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),
				'purchase_limit_per_day' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),
				
					
				'pb_limit_per_trx' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'pb_limit_per_day' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'pbusd_limit_per_trx' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'pbusd_limit_per_day' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),
					
				'rtgs_limit_per_trx' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'rtgs_limit_per_day' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'skn_limit_per_trx' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'skn_limit_per_day' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'payment_limit_per_trx' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'payment_limit_per_day' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'domonline_limit_per_trx' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),

				'domonline_limit_per_day' => array('NotEmpty',
					array('Between', array('min'=>1,'max'=>9999999999.99)),
					new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
					'messages' => array
					($this->language->_('Can not be empty'),
//					$this->language->_('Amount must be greater than 0.00'),
					$this->language->_('Value between 1 - 9999999999.99'),
					$this->language->_('Value between 1 - 9999999999.99'),)),
			);
		}

		$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		//validasi multiple email
		/*$arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');*/

		$arrfck = array('isenabled','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');

		$cek = $model->cekTemp();
		if($cek == null){
			if($this->_request->isPost() && $this->view->hasPrivilege('STUD')){
				if($zf_filter->isValid()){
					$this->_db->beginTransaction();
					try{

						$info = "TRANSACTION DAILY LIMIT";
						$change_id = $this->suggestionWaitingApproval('Transaction Daily Limit',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','','');
						//$change_id = 1;

						foreach($setting as $key=>$value){
							if (in_array($key, $arrfck)) {
								$valuee = $this->_getParam($key);
								$data = array(
									'CHANGES_ID' => $change_id,
									'SETTING_ID' => $key,
									'SETTING_VALUE' => ($valuee),
									'MODULE_ID' => 'TRX',
								);
							}else{
								$data = array(
									'CHANGES_ID' => $change_id,
									'SETTING_ID' => $key,
									'SETTING_VALUE' => ($zf_filter->$key),
									'MODULE_ID' => 'TRX',
								);
							}

							$model->insertTemp($data);
						}

						Application_Helper_General::writeLog('STUD','Updating General Setting');
						$this->_db->commit();
						$this->view->success = true;
						$msg = $this->language->_('Settings Change Request Saved');
						$this->view->report_msg = $msg;
					}catch(Exception $e){
						$this->_db->rollBack();
					}
				}else{
					//deteksi error
					/*Zend_Debug::dump($this->_request->getParams());

					Zend_Debug::dump($zf_filter->getMessages());
					Zend_Debug::dump($zf_filter->hasInvalid());
					Zend_Debug::dump($zf_filter->hasMissing());
					Zend_Debug::dump($zf_filter->hasUnknown());*/
					//deteksi error
					$docErr = $this->language->_('Error in processing form values. Please correct values and re-submit.');
					$this->view->report_msg = $docErr;

					foreach($zf_filter->getMessages() as $key=>$err){
					$xxx = 'x'.$key;
					$this->view->$xxx = $this->displayError($err);
					}

					//if(isSet($cek_multiple_email) && $cek_multiple_email == false) displayError['user_email'] = $this->language->_('Invalid email format');
				}
			} //GAK ADA ELSE ?
		}else{
			$this->view->error = true;
			//$this->fillParam($zf_filter_input);
			$this->view->report_msg = $this->language->_('No changes allowed for this record while awaiting approval for previous change.');
		}


		if($this->_request->isPost()){
			foreach($setting as $key=>$value){
				if (in_array($key, $arrfck))
					$this->view->$key =   $this->_getParam($key);
				else
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);

			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		}else{
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			//Application_Helper_General::writeLog('STLS','View General Setting');
		}

		$disable_note_len	= (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 		= (isset($note))  ? strlen($note)  : 0;

		$disable_note_len	= 200 - $disable_note_len;
		$note_len 	 		= 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len				= $note_len;
	}
}