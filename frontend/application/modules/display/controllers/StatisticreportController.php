<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
class display_StatisticreportController extends Application_Main
{


	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$filter_clear 		= $this->_getParam('clearfilter');
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash; 
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;

		$transstatus = $this->_transferstatus;
		$transstatuscode = array_flip($transstatus['code']);
		$statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));

		$paymenttype = $this->_paymenttype;

		$settings 			= new Application_Settings();

		$ccyList  = $settings->setCurrencyRegistered();
		$this->view->ccyArr 			= $ccyList;

		$arrPayType  = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$this->view->arrPayType = $arrPayType;
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));

		$tratypearr = array_combine(array_values($this->_transfertype['code']),array_values($this->_transfertype['desc']));

		// $select = $this->_db->select()->distinct()
		// 	->from(array('A' => 'M_CUSTOMER'),
		// 		array('CUST_ID'))
		// 		->order('CUST_ID ASC')
		// 		 -> query() ->fetchAll();
		//Zend_Debug::dump($arrPayType); die;

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'CUSTOMER';
        
        $select = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($select)){
        	$select = $this->_db->select()->distinct()
			->from(
				array('A' => 'M_CUSTOMER'),
				array('CUST_ID')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();

			$cache->save($select,$cacheID);
        }

		$this->view->var=$select;

		$fields = array(
						'D.ACCT_NO'      	=> array('field' => 'D.ACCT_NO',
													'label' => $this->language->_('Date'),
													'sortable' => true),
						'PS_CREATED'      	=> array('field' => 'PS_CREATED',
											      'label' => $this->language->_('Account Number'),
											      'sortable' => true),
						'PS_SUBJECT'  	=> array('field' => 'PS_SUBJECT',
													'label' => $this->language->_('Number of TX'),
													'sortable' => true),
						'SOURCE_ACCOUNT'  	=> array('field' => 'SOURCE_ACCOUNT',
													'label' => $this->language->_('Value of TX'),
													'sortable' => true)
						// 'SOURCE_ACCOUNT_NAME'  	=> array('field' => 'SOURCE_ACCOUNT_NAME',
						// 							'label' => $this->language->_('Running Balance'),
						// 							'sortable' => true)
		);
		$filterlist = array('ACCT_NO','CURRENCY','PS_CREATED');
    
	    $this->view->filterlist = $filterlist;
					  

		$page    = $this->_getParam('page');

		// $sortBy  = $this->_getParam('sortby','B.PS_UPDATED');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('D.ACCT_NO');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		// $sortDir = $this->_getParam('sortdir','desc');
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

// 		$caseType = "(CASE B.PS_TYPE ";
//   		foreach($typearr as $key=>$val)
//   		{
//    			$caseType .= " WHEN ".$key." THEN '".$val."'";
//   		}
//   			$caseType .= " END)";

//   		$caseStatus = "(CASE C.TRA_STATUS ";
//   		foreach($statusarr as $key=>$val)
//   		{
//    			$caseStatus.= " WHEN ".$key." THEN '".$val."'";
//   		}
//   			$caseStatus .= " END)";

//   		$caseTraType = "(CASE C.TRANSFER_TYPE ";
//   		foreach($tratypearr as $key=>$val)
//   		{
//    			$caseTraType.= " WHEN ".$key." THEN '".$val."'";
//   		}
//   			$caseTraType .= " END)";
		//Zend_Debug::dump($caseStatus); die;
		$select2 = $this->_db->select()
					        ->from(array('b' => 'T_PSLIP'),array())
					        ->joinleft(array('a' => 'T_TRANSACTION'), 'b.PS_NUMBER = a.PS_NUMBER',array('a.SOURCE_ACCOUNT', 'b.PS_CREATED', 'a.SOURCE_ACCOUNT_CCY'));
// 		                    ->joinleft(array('D' => 'T_BALANCE'), 'B.CUST_ID = D.CUST_ID AND D.ACCT_NO = C.ACCT_NO AND D.CCY_ID = C.CCY_ID',array('D.PLAFOND','D.CCY_ID','C.CUST_ID'));
		$select2 -> where("a.TRA_STATUS in ('7','8')");
// 		$select2 -> where("b.CUST_ID = ? ",$this->_custIdLogin);
		$searchQuery = ' 1=1 ';

// 		$listTemp = $this->_db->fetchAll($sqlCheckTemp,'');

// 		print_r($listTemp);die;

		$filterArr = array(
						   'filter'			=> array('StripTags','StringTrim'),
							  'SOURCE_ACCOUNT' => array(),
	                       'ACCTSRC'		=> array('StripTags','StringTrim','StringToUpper'),
	                       'month'		=> array('StripTags','StringTrim','StringToUpper'),
	    				   'CURR_CODE'		=> array('StripTags'),
	                       'beneficiary'	=> array('StripTags','StringToUpper', 'StringToUpper'),
						   'status'			=> array('StripTags'),
						   'transtype'		=> array('StripTags'),
						   'createdStart'		=> array('StripTags','StringTrim'),
						   'createdEnd'		=> array('StripTags','StringTrim'),
	                      );

		$validator = array(
						   'filter'			=> array(),
							 'SOURCE_ACCOUNT' => array(),
	                       'ACCTSRC'		=> array(),
	                       'month'		=> array(),
	    				   'CURR_CODE'		=> array(),
	                       'beneficiary'	=> array(),
						   'status'			=> array(),
						   'transtype'		=> array(),
						   'createdStart'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'createdEnd'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);
						  
		$dataParam = array("USER_ID","SEARCH_ACTIVITY");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
		  
		  // print_r($dtParam);die;
		  if(!empty($this->_request->getParam('wherecol'))){
			$dataval = $this->_request->getParam('whereval');
			  foreach ($this->_request->getParam('wherecol') as $key => $value) {
				if($dtParam==$value){
				  $dataParamValue[$dtParam] = $dataval[$key];
				}
			  }
			
		  }
			
		  // $dataPost = $this->_request->getPost($dtParam);
		  // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		} 		

	     $zf_filter 	= new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	     $filter 		= $zf_filter->getEscaped('filter');


	     $AccArr 		= html_entity_decode($zf_filter->getEscaped('ACCTSRC'));
			 $sourceAccount 		= html_entity_decode($zf_filter->getEscaped('SOURCE_ACCOUNT'));
		 $month 		= html_entity_decode($zf_filter->getEscaped('month'));
		 $CURR_CODE 		= html_entity_decode($zf_filter->getEscaped('CURR_CODE'));
		 $beneficiary 	= html_entity_decode($zf_filter->getEscaped('beneficiary'));
		 $status 		= html_entity_decode($zf_filter->getEscaped('status'));
		 $transtype 	= html_entity_decode($zf_filter->getEscaped('transtype'));
		 $datefrom 		= html_entity_decode($zf_filter->getEscaped('createdStart'));
		 $dateto 		= html_entity_decode($zf_filter->getEscaped('createdEnd'));



		 if($filter == null)
		 {
		     $datefrom = (date("1/m/Y"));
		     $dateto = (date("d/m/Y"));
		     $this->view->createdStart  = (date("d/m/Y"));
		     $this->view->createdEnd  = (date("d/m/Y"));
		     $searchDate = 'MONTH(NOW()) = MONTH(b.`PS_CREATED`) AND YEAR(NOW()) = YEAR(b.`PS_CREATED`)';
		 }

		 if($filter_clear == '1'){
		     //die;
		     $this->view->createdStart  = '';
		     $searchDate = ' 1=1 ';
		     $this->view->createdEnd  = '';
		     $datefrom = '';
		     $dateto = '';

		 }
//
		if($filter != null || $filter == 'Set Filter')
		{
			$this->view->createdStart = $datefrom;
			$this->view->createdEnd = $dateto;


// 		if(!empty($datefrom) && empty($dateto))
// 	            $select2->where("DATE(B.PS_UPDATED) >= ".$this->_db->quote($datefrom));

// 	   	if(empty($datefrom) && !empty($dateto))
// 	            $select2->where("DATE(B.PS_UPDATED) <= ".$this->_db->quote($dateto));

// 	    if(!empty($datefrom) && !empty($dateto))
// 	            $select2->where("DATE(B.PS_UPDATED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));


		$this->view->GRAPH = false;
		if($filter == TRUE)
	    {

	        if(!empty($datefrom) && !empty($dateto))
	        {
	            $FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
	            $datefrom  = $FormatDate->toString($this->_dateDBFormat);

	            $FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
	            $dateto    = $FormatDate->toString($this->_dateDBFormat);
	            $searchDate = 'MONTH(DATE("'.$datefrom.'")) = MONTH(DATE(b.`PS_CREATED`)) AND YEAR(DATE("'.$datefrom.'")) = YEAR(DATE(b.`PS_CREATED`))';

	        }
	        if(empty($searchDate)){
	            $searchDate = '1=1 ';
	        }



			if($AccArr != null)
			{
			   $this->view->ACCTSRC = $AccArr;
			   $this->view->GRAPH = true;
		       $searchQuery .= " AND SOURCE_ACCOUNT = '$AccArr'";
			}

			if($sourceAccount != null)
			{
			   $this->view->SOURCE_ACCOUNT = $sourceAccount;
			   $this->view->GRAPH = true;
		       $searchQuery .= " AND SOURCE_ACCOUNT = '$sourceAccount'";
			}


		    if($CURR_CODE != null)
		    {
		       	$this->view->CURR_CODE = $CURR_CODE;

		        $searchQuery .= " AND SOURCE_ACCOUNT_CCY IN ('$CURR_CODE')";
		    }




		   if($month != null)
		    {
		    	$this->view->month = $month;
		    	//$status =
		    	$searchDate = 'MONTH(DATE("'.$month.'")) = MONTH(DATE(b.`PS_CREATED`)) AND YEAR(DATE("'.$month.'")) = YEAR(DATE(b.`PS_CREATED`))';
// 		    	$select2->where("TRA_STATUS LIKE ".$this->_db->quote($status));

		    }
			}
		}
		$searchQuery .= " AND CUST_ID = '$this->_custIdLogin' ";

		$sqlCheckTemp=
		"SELECT ANY_VALUE(a.`SOURCE_ACCOUNT`) AS SOURCE_ACCOUNT, ANY_VALUE(COUNT(a.`PS_NUMBER`)) AS totaltrx, ANY_VALUE(SUM(b.PS_TOTAL_AMOUNT)) AS totalamont, ANY_VALUE(DATE(b.`PS_CREATED`)) as ps_created, ANY_VALUE(a.`SOURCE_ACCOUNT_CCY`) FROM `T_TRANSACTION` a
INNER JOIN  `T_PSLIP` b ON b.`PS_NUMBER` = a.`PS_NUMBER`

WHERE b.`CUST_ID` = '$this->_custIdLogin' AND a.TRA_STATUS NOT IN (4)  AND ".$searchDate."
GROUP BY a.`SOURCE_ACCOUNT`,DATE(b.`PS_CREATED`) ORDER BY ps_created ASC";
// 		$select2->order($sortBy.' '.$sortDir);
		// echo $sqlCheckTemp;die;

		$arr = $this->_db->fetchAll($sqlCheckTemp);
		//Zend_Debug::dump($arr);die;
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$arr = $this->_db->fetchAll($select2);
			foreach ($arr as $key => $value)
			{
				//echo $key;
				$arr[$key]["PS_CREATED"] = Application_Helper_General::convertDate($value["PS_CREATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

				$arr[$key]["SOURCE_ACCOUNT_NAME"]= $value["SOURCE_ACCOUNT_NAME"];
				$arr[$key]["BENEFICIARY_ACCOUNT_NAME"]= $value["BENEFICIARY_ACCOUNT_NAME"];
				$arr[$key]["BENEFICIARY_ACCOUNT_CCY"]= $value["BENEFICIARY_ACCOUNT_CCY"].' / '.$value["TRA_AMOUNT"];
				$arr[$key]["PS_TYPE"]= $this->language->_($value["PS_TYPE"]).' ('.$value["TRANSFER_TYPE"].')';
				unset($arr[$key]["SOURCE_ACCOUNT_ALIAS_NAME"]);
				unset($arr[$key]["BENEFICIARY_ALIAS_NAME"]);
				$amount = Application_Helper_General::displayMoney( $value['TRA_AMOUNT'] );
				unset($arr[$key]["TRA_AMOUNT"]);
				unset($arr[$key]["TRA_MESSAGE"]);
				unset($arr[$key]["TRANSFER_TYPE"]);
				$ps_amount =  $value['PS_TOTAL_AMOUNT'];
				$balance =  $value['BALANCE_TYPE'];
				if($balance == '2'){
					$amounteq = $ps_amount.'% ('.$value['PS_CCY'].' '.$amount.')';
				}else if ($value['EQUIVALENT_AMOUNT_IDR']>0){
					//$amounteq = $value['PS_CCY'].' '.$amount.'( IDR :'. Application_Helper_General::displayMoney($value['EQUIVALENT_AMOUNT_IDR']).' )';
					if($value["BENEFICIARY_ACCOUNT_CCY"]=='USD' && $value['SOURCE_ACCOUNT_CCY']=='USD'){
				            $amounteq = 'USD '.Application_Helper_General::displayMoney($value['EQUIVALENT_AMOUNT_IDR']);
				    }else{

				            $amounteq = 'IDR '.Application_Helper_General::displayMoney($value['EQUIVALENT_AMOUNT_IDR']);
				    }
				}else{
					$amounteq = $value['PS_CCY'].' '.$amount;
				}
// 				$amounteq = $value['PS_CCY'].' '.$amount.'( IDR :'. Application_Helper_General::displayMoney($value['EQUIVALENT_AMOUNT_IDR']).' )';
				unset($arr[$key]["PS_TOTAL_AMOUNT"]);
				unset($arr[$key]["BALANCE_TYPE"]);
				unset($arr[$key]["EQUIVALENT_AMOUNT_IDR"]);
				unset($arr[$key]["TRANSACTION_ID"]);
				unset($arr[$key]["BENEFICIARY_ACCOUNT_CCY"]);
				$updated = Application_Helper_General::convertDate($value["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$efdate = Application_Helper_General::convertDate($value["PS_EFDATE"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
				$arr[$key]["AMOUNT_EQ"]= $amounteq;
				unset($arr[$key]["PS_UPDATED"]);
				unset($arr[$key]["PS_EFDATE"]);
				$status = $this->language->_($value["TRA_STATUS"]);
				unset($arr[$key]["TRA_STATUS"]);
				$pstype = $this->language->_($value["PS_TYPE"]);
				unset($arr[$key]["PS_TYPE"]);
				$arr[$key]["PS_UPDATED"] = $updated;
				$arr[$key]["PS_EFDATE"] = $efdate;
				$arr[$key]["TRA_STATUS"] = $status;
				$arr[$key]["PS_TYPE"] = $pstype;

			}
// 			echo "<pre>";
// 			print_r($arr);die;
			$header = Application_Helper_Array::simpleArray($fields, 'label');
			//Zend_Debug::dump($arr);die;
			if($csv)
			{
				Application_Helper_General::writeLog('DTRX','Download CSV Transaction Report');
				//Zend_Debug::dump($arr);die;
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->csv($header,$arr,null,'Executed Transaction');
			}

			if($pdf)
			{
				Application_Helper_General::writeLog('DTRX','Download PDF Transaction Report');
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->pdf($header,$arr,null,'Executed Transaction');
			}
			if($this->_request->getParam('print') == 1){

	                $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields));
	        }
		}
		else
		{
			Application_Helper_General::writeLog('DTRX','View Transaction Report');
		}
		if(!empty($this->_request->getParam('wherecol'))){
			$this->view->wherecol     = $this->_request->getParam('wherecol');
		}

		if(!empty($this->_request->getParam('whereopt'))){
			$this->view->whereopt     = $this->_request->getParam('whereopt');
		}

		if(!empty($this->_request->getParam('whereval'))){
			$this->view->whereval     = $this->_request->getParam('whereval');
		}
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->data = $arr;
		$this->paging($arr);

	}
}
?>
