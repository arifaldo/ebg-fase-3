<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
class tools_SubmissionreportController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->params = $this->_request->getParams();


		$FILE_DESCRIPTION_len = (isset($description)) ? strlen($description) : 0;
		$FILE_DESCRIPTION_len = 150 - $FILE_DESCRIPTION_len;

		//$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;

		$this->view->FILE_DESCRIPTION_len		= $FILE_DESCRIPTION_len;

		// $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();

		// if(count($temp)>1)
		// {
		// 	if($temp[0]=='F' || $temp[0]=='S')
		// 	{
		// 		if($temp[0]=='F')
		// 			$this->view->error = 1;
		// 		else
		// 			$this->view->success = 1;
		// 		$msg = ''; unset($temp[0]);
		// 		foreach($temp as $value)
		// 		{
		// 			if(!is_array($value))
		// 				$value = array($value);
		// 			$msg .= $this->view->formErrors($value);
		// 		}

		// 		$this->view->report_msg = $msg;
		// 	}	
		// }


		$arr = null;
		$viewFilter = null;

		$select = $this->_db->select()->distinct()
			->from(
				array('A' => 'M_USER'),
				array('USER_ID', 'USER_FULLNAME', 'CUST_ID')
			)
			->where("CUST_ID LIKE " . $this->_db->quote($this->_custIdLogin))
			->order('USER_FULLNAME ASC')
			->query()->fetchAll();

		$userlist = array("" => '-- ' . $this->language->_('Any Value') . '--');
		$userlist += Application_Helper_Array::listArray($select, 'USER_ID', 'USER_FULLNAME');
		$this->view->listCustId = $userlist;

		$fields = array(
			'FileName'  			=> array(
				'field' => 'FILE_NAME',
				'label' => $this->language->_('File Name'),
				'sortable' => true
			),
			'FileDescription'  		=> array(
				'field' => 'FILE_DESCRIPTION',
				'label' => $this->language->_('File Description'),
				'sortable' => true
			),
			'Upload Date and Time'  => array(
				'field' => 'FILE_UPLOADED_TIME',
				'label' => $this->language->_('Uploaded Date'),
				'sortable' => true
			),
			'Uploaded By'  			=> array(
				'field' => 'USER_FULLNAME',
				'label' => $this->language->_('Uploaded By'),
				'sortable' => true
			)
		);
		$this->view->fields = $fields;

		$filterlist = array('NAMA_FILE', 'DESKRIPSI_FILE', 'TANGGAL_UNGGAH', 'DIUNGGAH_OLEH');

		$this->view->filterlist = $filterlist;

		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', '');
		$sortDir = $this->_getParam('sortdir', 'asc');

		$page 		= (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy 	= (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$filterArr = array(
			'NAMA_FILE'   	=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'DESKRIPSI_FILE'   	=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'DIUNGGAH_OLEH' 	  	=> array('StringTrim', 'StripTags'),
		);

		$dataParam = array('NAMA_FILE', 'DESKRIPSI_FILE', 'TANGGAL_UNGGAH_AWAL', 'TANGGAL_UNGGAH_AKHIR', 'DIUNGGAH_OLEH');

		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						if (!empty($dataParamValue[$dtParam])) {
							$dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
						}

						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		if ($this->_request->getParam('createdate')) {
			$dataParamValue['TANGGAL_UNGGAH_AWAL'] = $this->_request->getParam('createdate')[0];
			$dataParamValue['TANGGAL_UNGGAH_AKHIR'] = $this->_request->getParam('createdate')[1];
		}

		$options = array('allowEmpty' => true);
		$validators = array(
			'NAMA_FILE'   	=> array(),
			'DESKRIPSI_FILE'   	=> array(),
			'DIUNGGAH_OLEH'   	=> array(),
			'TANGGAL_UNGGAH_AKHIR' 	  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'TANGGAL_UNGGAH_AWAL' 	  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),

		);
		$zf_filter 	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

		// $filter 	= $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$select = $this->_db->select()
			->distinct()
			->from(
				array('TFS' => 'T_FILE_SUBMIT'),
				array(
					'FILE_ID'			=> 'TFS.FILE_ID',
					'FILE_NAME'			=> 'TFS.FILE_NAME',
					'FILE_DESCRIPTION'	=> 'TFS.FILE_DESCRIPTION',
					'FILE_UPLOADED_TIME' => 'TFS.FILE_UPLOADED_TIME',
					'FILE_SYSNAME'		=> 'TFS.FILE_SYSNAME',
					'FILE_UPLOADEDBY'		=> 'TFS.FILE_UPLOADEDBY',
					'USER_FULLNAME'		=> 'MU.USER_FULLNAME',
					'FILE_DOWNLOADED' 	=> 'TFS.FILE_DOWNLOADED',
				)
			)
			->joinLeft(array('MU' => 'M_USER'), 'MU.USER_ID=TFS.FILE_UPLOADEDBY AND MU.CUST_ID=TFS.CUST_ID', array())
			->where('TFS.FILE_DELETED =?', '0')
			->where('TFS.CUST_ID =?', $this->_custIdLogin)
			->order('TFS.FILE_UPLOADED_TIME DESC');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER      = urldecode($this->_getParam('FILE_ID'));
		$FILE_ID = $AESMYSQL->decrypt($PS_NUMBER, $password);

		if ($FILE_ID) {
			$select->where('FILE_ID =?', $FILE_ID);
			$data = $this->_db->fetchRow($select);

			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['FILE_NAME'], $attahmentDestination . $data['FILE_SYSNAME']);

			$updateArr = array();
			$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED'] + 1;
			$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;

			$whereArr = array('FILE_ID = ?' => $FILE_ID);

			$fileupdate = $this->_db->update('T_FILE_SUBMIT', $updateArr, $whereArr);
			//echo "file downloaded: ".$data['FILE_DOWNLOADED'];
			Application_Helper_General::writeLog('VDEL', 'Unduh File Berbagi : ' . $data['FILE_NAME']);
		} else {
			Application_Helper_General::writeLog('VDEL', 'Lihat Daftar File Berbagi');
		}

		if ($filter == null) {
			$DATE_START = NULL; //(date("d/m/Y"));
			$DATE_END = NULL; //(date("d/m/Y"));
			$this->view->DATE_START  = $DATE_START;
			$this->view->DATE_END  = $DATE_END;
			//Zend_Debug::dump($this->view->fDateFrom); die;
		}
		// print_r($zf_filter->getEscaped('SEARCH_TEXT'));die;
		// if($filter == TRUE)
		// {


		$dataParam = array('NAMA_FILE', 'DESKRIPSI_FILE', 'TANGGAL_UNGGAH_AWAL', 'TANGGAL_UNGGAH_AKHIR', 'DIUNGGAH_OLEH');


		$nameFile   	= $zf_filter->getEscaped('NAMA_FILE');
		$uploadedBy    = $zf_filter->getEscaped('DIUNGGAH_OLEH');
		$fileDescription    = $zf_filter->getEscaped('DESKRIPSI_FILE');

		$this->view->createdStart 	= $DATE_START;
		$this->view->createdEnd 	= $DATE_END;

		if ($nameFile) {
			$select->where("FILE_NAME LIKE " . $this->_db->quote('%' . $nameFile . '%'));
		}
		if ($uploadedBy) {
			// $select->where("FILE_UPLOADEDBY LIKE " . $this->_db->quote('%' . $uploadedBy . '%'));
			$select->where("MU.USER_FULLNAME LIKE " . $this->_db->quote('%' . $uploadedBy . '%'));
		}
		if ($fileDescription) {
			$select->where("FILE_DESCRIPTION LIKE " . $this->_db->quote('%' . $fileDescription . '%'));
		}
		if (!empty($dataParamValue['TANGGAL_UNGGAH_AWAL']) && !empty($dataParamValue['TANGGAL_UNGGAH_AKHIR'])) {
			$select->where("FILE_UPLOADED_TIME >= ?", date('Y-m-d', strtotime($dataParamValue['TANGGAL_UNGGAH_AWAL'])));
			$select->where("FILE_UPLOADED_TIME <= ?", date('Y-m-d', strtotime($dataParamValue['TANGGAL_UNGGAH_AKHIR'])));
		}

		$select->where("FILE_TYPE = ?", '0');
		$select->order($sortBy . ' ' . $sortDir);
		$this->paging($select);
		if (!empty($dataParamValue)) {

			$this->view->createdStart = $dataParamValue['TANGGAL_UNGGAH_AWAL'];
			$this->view->createdEnd = $dataParamValue['TANGGAL_UNGGAH_AKHIR'];

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',', $value);
				if (!empty($duparr)) {

					foreach ($duparr as $ss => $vs) {
						$wherecol[]	= $key;
						$whereval[] = $vs;
					}
				} else {
					$wherecol[]	= $key;
					$whereval[] = $value;
				}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$params = $this->_request->getParams();

		$upload = $this->_getParam('upload');
		$maxFileSize = "10240000"; //"1024000"; //1024
		//~ $fileExt = "csv,dbf";
		$fileExt = "csv,pdf,doc,docx";

		$delete = $this->_getParam('multiReject');

		// print_r($_FILES["document"]);
		// die();

		if ($this->_request->isPost() && $upload) {

			$attahmentDestination 	= UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			// print_r($attahmentDestination);die;
			$params = $this->_request->getParams();

			$sourceFileName = trim($adapter->getFileName());
			// print_r($sourceFileName);die;
			if ($sourceFileName == null) {

				$sourceFileName = null;
				$fileType = null;
			} else {
				$sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
				if ($_FILES["document"]["type"]) {
					$adapter->setDestination($attahmentDestination);
					$fileType 				= $adapter->getMimeType();
					$size 					= $_FILES["document"]["size"];
				} else {
					$fileType = null;
					$size = null;
				}
			}
			$paramsName['sourceFileName'] 	= $sourceFileName;

			$helpTopic 	= $this->_getParam('helpTopic');
			if ($helpTopic == null)	$helpTopic = null;

			$paramsName['helpTopic']  	= $helpTopic;
			$description = $this->_getParam('description');
			$paramsName['description']  = $description;

			$filtersName = array(
				'sourceFileName'	=> array('StringTrim'),
				'description'		=> array('StringTrim'),
			);

			$validatorsName = array(
				'sourceFileName' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Error : File cannot be left blank')
					)
				),
				'description' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Error: Description cannot be left blank')
					)
				),
			);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);

			if ($sourceFileName == NULL) {
				$this->view->error 				= true;
				$errors = $zf_filter_input_name->getMessages();
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName'])) ? $errors['sourceFileName'] : null;
			} elseif ($description == NULL) {

				$this->view->error = true;
				$errors	= $zf_filter_input_name->getMessages();
				$this->view->descriptionErr 	= (isset($errors['description'])) ? $errors['description'] : null;
			} else {

				if ($zf_filter_input_name->isValid()) {
					$fileTypeMessage = explode('/', $fileType);
					if (isset($fileTypeMessage[1])) {
						$fileType =  $fileTypeMessage[1];
						//var_dump($fileExt);
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));

						$extensionValidator->setMessage("Extension file must be *.csv /.pdf / .doc / .docx");

						$size 			= number_format($size);
						$sizeTampil 	= $size / 1000;
						$sizeValidator 	= new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
						$sizeValidator->setMessage("Your file size more than 10 MB, it should be lower than 10 MB");

						$errors = array(array('FileType' => 'Error: An infected file. Please upload other file'));
						$this->view->errorMsg = $errors;

						$adapter->setValidators(array($extensionValidator, $sizeValidator));

						if ($adapter->isValid()) {
							//die('her');
							$date = date("dmy");
							$time = date("his");

							$newFileName = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . $sourceFileName;
							$adapter->addFilter('Rename', $newFileName);

							if ($adapter->receive()) {
								$filedata = $attahmentDestination . $newFileName;
								if (file_exists($filedata)) {
									exec("clamdscan --fdpass " . $filedata, $out, $int);

									if ($int == 1) {

										unlink($filedata);
										$this->view->error = true;
										$errors = 'Error: An infected file. Please upload other file';
										$this->view->errorMsgT = $errors;
									} else {

										try {
											$this->_db->beginTransaction();
											$insertArr = array(
												'CUST_ID'				=> $this->_custIdLogin,
												'FILE_TYPE'				=> '0',
												'FILE_UPLOADEDBY'			=> $this->_userIdLogin,
												'FILE_NAME' 			=> $sourceFileName,
												'FILE_DESCRIPTION'		=> $description,
												'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
												'FILE_DOWNLOADED'		=> '0',
												'FILE_DELETED'			=> '0',
												'FILE_DOWNLOADEDBY'		=> '',
												'FILE_DELETEDBY'		=> '',
												'FILE_ISEMAILED'		=> '0',
												'FILE_SYSNAME'			=> $newFileName,
											);

											$this->_db->insert('T_FILE_SUBMIT', $insertArr);
											$this->_db->commit();

											Application_Helper_General::writeLog('DELI', "Berhasil Unggah File : " . $newFileName);

											$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
											$this->_redirect('/notification/success');
										} catch (Exception $e) {
											$this->_db->rollBack();
											Application_Log_GeneralLog::technicalLog($e);
										}
									}
								}
							}
						} else {

							$this->view->error = true;
							$errors = array($adapter->getMessages());
							$this->view->errorMsg = $errors;

							$this->view->FILE_DESCRIPTION_len = 150;
						}
					} else {
						$this->view->error = true;
						$errors = array(array('FileType' => 'Error: Unknown File Type'));
						$this->view->errorMsg = $errors;

						$this->view->FILE_DESCRIPTION_len = 150;
					}
				} else {
					$this->view->error = true;
					$errors = $zf_filter_input_name->getMessages();
					$this->view->errorMsg = $errors;

					$this->view->FILE_DESCRIPTION_len = 150;
				}
			}
		} else if ($this->_request->isPost() && $delete) {
			try {
				$postreq_id	= $this->_request->getParam('mPayReff');

				foreach ($postreq_id as  $key => $value) {

					$AESMYSQL = new Crypt_AESMYSQL();
					$FILEENC      = urldecode($value);
					$FILE_ID_DELETE = $AESMYSQL->decrypt($FILEENC, $password);

					$getDetailFile = $this->_db->select()
						->from('T_FILE_SUBMIT', ['FILE_NAME'])
						->where('FILE_ID = ?', $FILE_ID_DELETE)
						->query()->fetch();

					$this->_db->beginTransaction();

					$param = array();
					$param['FILE_DELETED'] = '1';
					$param['FILE_DELETEDBY'] = $this->_userIdLogin;

					$where = array('FILE_ID = ?' => $FILE_ID_DELETE);
					$query = $this->_db->update("T_FILE_SUBMIT", $param, $where);
					// $stringlog = 'Hapus File Berbagi : ' . $FILE_ID_DELETE;
					$stringlog = 'Hapus File Berbagi : ' . $getDetailFile['FILE_NAME'];
					Application_Helper_General::writeLog('VDEL', $stringlog);
					$this->_db->commit();
				}
			} catch (Exception $e) {
				// var_dump($e);die;
				$this->_db->rollBack();
			}
			$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
			$this->_redirect('/notification/success/index');
		}
		if (!empty($errors)) {

			foreach ($errors as $keyRoot => $rowError) {
				foreach ($rowError as $errorString) {
					$errorArray[$keyRoot] = $errorString;
				}
			}
			if (!empty($errorArray['sourceFileName'])) {
				$error = $errorArray['sourceFileName'];
			} else if (!empty($errorArray['FileType'])) {
				$error = $errorArray['FileType'];
			} else if (!empty($errorArray['description'])) {
				$error = $errorArray['description'];
			}

			$this->view->error = true;
			$this->view->errormsg = $error;

			$this->view->FILE_DESCRIPTION_len = 150;
		}



		$this->view->max_file_size = $maxFileSize; //'1024';
		$this->view->file_extension = $fileExt;
		// Application_Helper_General::writeLog('DELI', "Viewing File Sharing");
	}
}
