<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class backenduser_RepairController extends Application_Main
{
	public function indexAction()
	{
		$changeid = $this->_getParam('changes_id');
		$this->view->changes_id = $changeid;
		$update = $this->_getParam('update');

		$select = $this->_db->select()->from('TEMP_BUSER', array('*'));
		$select->where("CHANGES_ID = ?", $changeid);
		$result = $this->_db->FetchRow($select);
		$this->view->result = $result;

		$select2 = $this->_db->select()->from('T_GLOBAL_CHANGES', array('CHANGES_REASON'));
		$select2->where("CHANGES_ID = ?", $changeid);
		$result2 = $this->_db->FetchRow($select2);
		$this->view->result2 = $result2;

		$selectGroup =	$this->_db->select()
			->from('M_BGROUP', array('BGROUP_ID', 'BGROUP_DESC'))
			->where("BGROUP_STATUS = '1'");
		//->where('PASSWORD = \''.$v_password_old.'\'');

		// echo $v_password;
		$group = $this->_db->fetchAll($selectGroup);
		$arrgroup = Application_Helper_Array::listArray($group, 'BGROUP_ID', 'BGROUP_DESC');
		$arrgroup = array('' => '---Any Value---') + $arrgroup;
		$this->view->arrGroup = $arrgroup;

		$flip = Application_Helper_Array::listArray($group, 'BGROUP_DESC', 'BGROUP_ID');


		$selectBranch =	$this->_db->select()
			->from('M_BRANCH', array('ID', 'BRANCH_NAME'));
		//->where('PASSWORD = \''.$v_password_old.'\'');

		// echo $v_password;
		$branch = $this->_db->fetchAll($selectBranch);
		$arrbranch = Application_Helper_Array::listArray($branch, 'ID', 'BRANCH_NAME');
		$flbranch = Application_Helper_Array::listArray($branch, 'BRANCH_NAME', 'ID');

		$this->view->arrBranch = $arrbranch;

		if ($update == null || $update == $this->language->_('Reset')) {
			$this->view->name = $result['BUSER_NAME'];
			$this->view->userId = $result['BUSER_ID'];
			$this->view->email = $result['BUSER_EMAIL'];
			$this->view->groupId = $result['BGROUP_ID'];
			$this->view->desc = $result['BUSER_DESC'];
			$this->view->nik = $result['BUSER_NIK'];
			$this->view->branch = $result['BUSER_BRANCH'];
		}

		if ($update == $this->language->_('Submit') && $this->view->hasPrivilege('ADCR')) {

			$filters = array(
				'name'			=> array('StripTags', 'StringTrim'),
				// 'email' 		=> array('StripTags', 'StringTrim'),
				'description' 	=> array('StripTags'),
				'groupId'  		=> array('StripTags', 'StringTrim'),
				'branch'  	=> array('StripTags', 'StringTrim'),
				'nik' 	=> array('StripTags', 'StringTrim'),
			);

			$validators = array(
				'name' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty'),
					)
				),
				// 'email' => array(
				// 	'NotEmpty',
				// 	array('StringLength',array('min'=>1,'max'=>128)),
				// 	'messages' => array(
				// 		$this->language->_('Can not be empty'),
				// 		$this->language->_('Email lenght cannot be more than 128'),
				// 	)
				// ),	
				'groupId' => array(
					array('InArray', $flip),
					'messages' => array(
						$this->language->_('Please Select Group'),
					)
				),
				'nik' => array(
					'NotEmpty',
					array('StringLength', array('min' => 16, 'max' => 16)),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Minimal 16 digit'),
					)
				),
				'branch' => array(
					array('InArray', $flbranch),
					'messages' => array(
						$this->language->_('Please Select Branch'),
					)
				),
			);

			$params = $this->_request->getParams();
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

			//validasi multiple email
			if ($this->_getParam('email')) {
				$validate = new validate;
				$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('email'));
			} else {
				$cek_multiple_email = true;
			}

			$groupid = $this->_request->getParam('groupId');
			$valid = new Zend_Validate_Identical(array($groupid => '0'));
			//			Zend_Debug::dump($arrgroup);die;

			//			echo $strtemp.' '.$digittemp;die;
			//			CEK GROUPID
			//			$group = (($this->_getParam('groupId')==0) ? false : true);
			//
			if ($zf_filter_input->isValid()  && $cek_multiple_email == true) {
				//die('here');		
				//$info = 'User ID = '.$params['userId'].', User Name = '.$params['name'];
				//$change_id = $this->suggestionWaitingApproval('User Backend',$info,'N',null,'M_BUSER','TEMP_BUSER','USER_ID',$params['userId']);

				$this->_db->beginTransaction();

				try {
					$info = 'User Backend';
					$where = array('CHANGES_ID = ?' => $changeid);
					$this->_db->delete('TEMP_BUSER', $where);
					$this->updateGlobalChanges($changeid, $info);
					//Zend_Debug::dump($params);die;
					$insertArr = array(
						'CHANGES_ID' 		=> $changeid,
						'BUSER_ID'			=> $result['BUSER_ID'],
						// 'BUSER_NAME' 		=> $zf_filter_input->name,
						'BUSER_NAME' 		=> $result['BUSER_NAME'],
						'BGROUP_ID' 		=> $zf_filter_input->groupId,
						'BUSER_BRANCH' 		=> $zf_filter_input->branch,
						'BUSER_NIK' 		=> $zf_filter_input->nik,
						'BUSER_EMAIL'		=> $result['BUSER_EMAIL'],
						'BUSER_DESC'		=> $params['desc'],
						'BUSER_STATUS'		=> $result['BUSER_STATUS'],
					);
					//var_dump($insertArr);die;
					$this->_db->insert('TEMP_BUSER', $insertArr);

					$this->_db->commit();
					$this->_redirect('/popup/successpopup/index');
				} catch (Exception $e) {
					//var_dump($e);die;
					$this->_db->rollBack();
				}
			} else {
				$this->view->error = true;
				$docErr = $this->language->_("Error in processing form values. Please correct values and re-submit.");
				$this->view->errorMsg = $docErr;

				if (isset($cek_multiple_email) && $cek_multiple_email == false) {
					$errr[] = array($this->language->_('Invalid email format'));
					$this->view->xemail = $this->displayError($errr);
				}

				foreach ($zf_filter_input->getMessages() as $key => $err) {
					$xxx = 'x' . $key;
					$this->view->$xxx = $this->displayError($err);
				}

				$this->view->userId 	= $result['BUSER_ID'];
				$this->view->name	 	= ($zf_filter_input->isValid('name')) ? $zf_filter_input->name : $params['name'];
				$this->view->desc 		= ($zf_filter_input->isValid('desc')) ? $zf_filter_input->desc : $params['desc'];
				$this->view->nik 		= ($zf_filter_input->isValid('nik')) ? $zf_filter_input->nik : $params['nik'];
				$this->view->email	 	= ($zf_filter_input->isValid('email')) ? $zf_filter_input->email : $params['email'];
				$this->view->groupId	= ($zf_filter_input->isValid('groupId')) ? $zf_filter_input->groupId : $params['groupId'];
				$this->view->branch	 		= ($zf_filter_input->isValid('branch')) ? $zf_filter_input->branch : $params['branch'];
				//				echo $docErr,die;
			}
			Application_Helper_General::writeLog('ADCR', 'Repair Backend User (' . $changeid . ')');
		} else {
			Application_Helper_General::writeLog('ADCR', 'Viewing Repair Backend User page (' . $changeid . ')');
		}
		// var_dump($result);die;
		if (!empty($result['BUSER_DESC'])) {
			$desc 		 = $result['BUSER_DESC'];
		} else {
			$desc 		 = $this->_request->getParam('desc');
		}

		$desc_len 	 = (isset($desc))  ? strlen($desc)  : 0;
		$desc_len 	 = 200 - $desc_len;

		$this->view->desc_len		= $desc_len;
	}
}
