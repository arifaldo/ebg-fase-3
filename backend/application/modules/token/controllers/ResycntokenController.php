<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class token_ResycntokenController extends Application_Main
{
	public function indexAction() 
	{ 
		$this->view->statusCode = $this->_masterglobalstatus['code'];
    	$this->view->statusDesc = $this->_masterglobalstatus['desc'];
    	$this->view->modulename = $this->_request->getModuleName();
    	
    	$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

	    $fields = array('C.CUST_ID'   		=> array('field'    => 'C.CUST_ID',
	                                        		 'label'    => $this->language->_('Company Code'),
	                                        		 'sortable' => true),
	    
	                    'CUST_NAME' 		=> array('field'    => 'CUST_NAME',
	                                        		 'label'    => $this->language->_('Company Name'),
	                                        		 'sortable' => true),
	    
	                    'USER_ID'     		=> array('field'    => 'USER_ID',
	                                        		 'label'    => $this->language->_('User ID'),
	                                       		  	 'sortable' => true),
	                   );
	    
	    //validasi page, jika input page bukan angka               
	    $page = $this->_getParam('page');
	    $csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
    
    
	    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
	    
	    //validasi sort, jika input sort bukan ASC atau DESC
	    $sortBy  = $this->_getParam('sortby');
	    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	    $sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
	    $filterArr = array(	'filter' => array('StripTags','StringTrim'),
	                       	'custid'    => array('StripTags','StringTrim','StringToUpper'),
	                       	'custname'  => array('StripTags','StringTrim','StringToUpper'),
	    					'username'  => array('StripTags','StringTrim','StringToUpper'),
	    					'userid'  => array('StripTags','StringTrim','StringToUpper'),
	                       	'status' => array('StripTags','StringTrim')
	                      );
	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
	    //$filter = $zf_filter->getEscaped('filter');
	    
	    $tokenSerial = $zf_filter->getEscaped('tokenSerial'); //textboxt
	   	$firstOtp = $zf_filter->getEscaped('firstOtp'); //textboxt
	    $secondOtp = $zf_filter->getEscaped('secondOtp'); //textboxt
	    
	    $this->view->tokenSerial = $tokenSerial;
	    $this->view->firstOtp = $firstOtp;
	    $this->view->secondOtp = $secondOtp;
	    
	    $validateotp = $zf_filter->getEscaped('validateotp'); //button
	    $rescyn = $zf_filter->getEscaped('rescyn'); //button
	    
	    $token = new Service_Token($custId, $userId, $tokenSerial);
	    
	    if($validateotp == true){
	    	if(empty($tokenSerial)){
	    		$errorMessage = $this->language->_('Token ID cannot be left blank');
	    		$this->view->errorMessage = $errorMessage;
	    	}
	    	elseif(empty($firstOtp)){
	    		$errorMessage = $this->language->_('OTP cannot be left blank');
	    		$this->view->errorMessage = $errorMessage;
	    	}
	    	else{
		    	$resTokenStatus = $token->validateOtp($firstOtp);
		    	$resultToken = $resTokenStatus['ResponseCode'] == '0000';
		    	$errorMessage = 'Service Call Failure. '.$resTokenStatus['ResponseDesc'];
		    	
	    		if ($resTokenStatus['ResponseCode'] != '0000')
				{
					$this->view->errorMessage = $errorMessage;
				}
		    	
		    	if($resultToken){
		    		$this->_redirect('/notification/success/index');
		    	}
	    	}
	    }
	    if($rescyn == true){
	    	if(empty($tokenSerial)){
	    		$errorMessage = $this->language->_('Token ID cannot be left blank');
	    		$this->view->errorMessage = $errorMessage;
	    	}
	    	elseif(empty($firstOtp)){
	    		$errorMessage = $this->language->_('OTP cannot be left blank');
	    		$this->view->errorMessage = $errorMessage;
	    	}
	    	elseif(empty($secondOtp)){
	    		$errorMessage = $this->language->_('OTP2 cannot be left blank');
	    		$this->view->errorMessage = $errorMessage;
	    	}
	    	else{
		    	$resTokenStatus = $token->resynchOtp($firstOtp, $secondOtp);
		    	$resultToken = $resTokenStatus['ResponseCode'] == '0000';
		    	
	    		$errorMessage = 'Service Call Failure. '.$resTokenStatus['ResponseDesc'];
	    		if ($resTokenStatus['ResponseCode'] != '0000')
				{
					$this->view->errorMessage = $errorMessage;
				}
		    	
		    	//Zend_Debug::dump($resTokenStatus);
	    		if($resultToken){
		    		$this->_redirect('/notification/success/index');
		    	}
	    	}
	    }
 

     //insert log
	} 
}