<?php
require_once 'Zend/Controller/Action.php';

class mobiletoken_IndexController extends Application_Main {

	public function initController()
	{
		$this->_helper->layout->setLayout('newlayout');
	}

	public function indexAction(){

		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

		$selectdata = $this->_db->select()
				->from(
					array('G' => 'M_USER'),
					array(
						'G.*'
					)
				)
				->joinleft(array('B' => 'M_CUSTOMER'), 'G.CUST_ID = B.CUST_ID', array('B.CUST_NAME'));
				
		//$selectdata->where('G.CHANGES_STATUS = ?','WA');
		$selectdata->where('G.TOKEN_TYPE = ?','6');
		//echo $selectdata;die;
		$userData = $this->_db->fetchAll($selectdata);
		$this->view->data = $userData;
		//$this->view->paginator = $resData;

	}
}