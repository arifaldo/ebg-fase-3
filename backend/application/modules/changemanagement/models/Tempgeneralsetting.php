<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempgeneralsetting extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'GNS';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//update global charge skn - rtgs
		$set = new Settings();
		//ambil dari m_setting SKN RTGS
		//$global_charges_skn_ori = $set->getSettingNew('global_charges_skn');
		//$global_charges_rtgs_ori = $set->getSettingNew('global_charges_rtgs');
		//ambil dari temp_setting 
		//$global_charges_skn = $set->getTmpSetting('global_charges_skn');
		//$global_charges_rtgs = $set->getTmpSetting('global_charges_rtgs');

		//ambil dari m_setting FEE CHARGE
		//$admin_fee_account_ori = $set->getSettingNew('admin_fee_account');
		//$admin_fee_company_ori = $set->getSettingNew('admin_fee_company');
		//ambil dari temp_setting
		//$admin_fee_account = $set->getTmpSetting('admin_fee_account');
		//$admin_fee_company = $set->getTmpSetting('admin_fee_company');
		
		// if($global_charges_skn == $global_charges_skn_ori){
		// 	//echo'nilai ori skn tidak diganti, tidak perlu update';
		// }else{
		// 	$this->deleteChargeOtherSkn();
		// 	$set->updateGlobalChargeSKN($global_charges_skn);
		// 	//echo'nilai skn telah berubah, perlu diupdate';
		// }
		
		// if($global_charges_rtgs == $global_charges_rtgs_ori){
		// 	//echo'nilai ori rtgs tidak diganti, tidak perlu update';
		// }else{
		// 	//echo'nilai rtgs telah berubah, perlu diupdate';
		// 	$this->deleteChargeOtherRtgs();
		// 	$set->updateGlobalChargeRTGS($global_charges_rtgs);
		// }
		
		
		// if($admin_fee_account < 1){
		// 	//echo'jalani fee company';
		// 	if($admin_fee_company != $admin_fee_company_ori){
		// 		$this->deleteAdminFeeCompany();
		// 		$set->updateGlobalFeeCompany($admin_fee_company);
		// 	}
		// }
		// elseif ($admin_fee_company < 1){
		// 	//echo'jalanin fee account';
		// 	if($admin_fee_account_ori != $admin_fee_account){
		// 		$this->deleteAdminFeeAccount();
		// 		$set->updateGlobalFeeAccount($admin_fee_account);
		// 	}
		// }
			
		//die;
		//query from TEMP_USER
		$listSetting = $this->dbObj->select()
						  	->from('TEMP_SETTING')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!count($listSetting)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Setting)';
			return false;
        }

		if(is_array($listSetting)){
			foreach ($listSetting as $row) {
				$updateArr = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>'','SETTING_ID'=>''));
				$whereArr = array('SETTING_ID = ?'=> (string) $row['SETTING_ID']);
				$userupdate = $this->dbObj->update('M_SETTING',$updateArr,$whereArr);
			}
		}
		//Application_Helper_General::writeLog('STCA','Approve General Setting');

		//update record
		// if(!(boolean)$userupdate) {
			// $this->_errorCode = '82';
			// $this->_errorMsg = 'Query failed(General Setting)';
			// return false;
		// }
		
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) {
		//will never be called
	}
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	
	public function deleteAdminFeeAccount() {
		$userdelete = $this->dbObj->delete('M_ADMFEE_MONTHLY',$this->dbObj->quoteInto('MONTHLYFEE_TYPE = ?','1'));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteAdminFeeCompany() {
		$userdelete = $this->dbObj->delete('M_ADMFEE_MONTHLY',$this->dbObj->quoteInto('MONTHLYFEE_TYPE = ?','2'));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteChargeOtherSkn() {
		$userdelete = $this->dbObj->delete('M_CHARGES_OTHER',$this->dbObj->quoteInto('CHARGES_TYPE = ?','2'));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}

	public function deleteChargeOtherRtgs() {
		$userdelete = $this->dbObj->delete('M_CHARGES_OTHER',$this->dbObj->quoteInto('CHARGES_TYPE = ?','1'));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteEdit() {
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_SETTING',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() {
	}
}