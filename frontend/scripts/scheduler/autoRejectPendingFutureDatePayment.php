<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	$actionid = 'scheduler';
	$moduleid = 'reject payment';
	
	$interval = 2;

	$truecounter = 0;
	$falsecounter = 0;

	$dataCM = $db->select()
						->FROM('T_PSLIP',array('PS_NUMBER','CUST_ID'))
						->WHERE('PS_STATUS = 7')
						->WHERE('DATE(T_PSLIP.PS_EFDATE) <  DATE(now())')
						->QUERY()->FETCHALL();
	
// 	Zend_Debug::dump($dataCM);die;
	
	if($dataCM)
	{
		foreach($dataCM as $row)
		{
			$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], 'System');
			$PS_REASON = "Authorization Overdue";			
			if($Payment->rejectPayment($PS_REASON))
			{
				$truecounter++;
			}
			else
			{
				$falsecounter++;
			}
		}
		
		if($truecounter>0)
		{
			Application_Helper_General::cronLog(basename(__FILE__), 'Cron Reject Succes', '1');
		}
		if($falsecounter>0)
		{
			Application_Helper_General::cronLog(basename(__FILE__), 'Cron Reject Failed', '0');
		}		
	}

	if($truecounter == 0 && $falsecounter == 0)
	{
		Application_Helper_General::cronLog('autoRejectPayment', 'No Data Payment for Auto Rejected', '1');
	}
?>