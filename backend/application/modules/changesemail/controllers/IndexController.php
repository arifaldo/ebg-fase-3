<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Service/Token.php'; //added new
require_once ('Crypt/AESMYSQL.php');

//NOTE:
//Watch the modulename, filename and classname carefully
class Changesemail_IndexController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		$this->_helper->layout()->setLayout('newlayout');
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		// $cust_id = $this->_custIdLogin;
		$user_id = $this->_userIdLogin;
		$this->view->user_id 	= $user_id;
		
		$settings =  new Settings();
		
		
		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_BUSER'));
		$select3->where("BUSER_ID = ".$this->_db->quote($this->_userIdLogin));
		// $select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
		$data2 = $this->_db->fetchRow($select3);
		// print_r($data2['BUSER_EMAIL']);die;
		$this->view->old_email		= $data2['BUSER_EMAIL'];

		$mail=$data2['BUSER_EMAIL'];

		$mail_first=explode('@',$mail);
		$arr=str_split($mail_first[0]);
		$mask=array();

		for($i=0;$i<count($arr);$i++) {
		    if($i) {
		        $arr[$i]='*';
		    }

		    $mask[]=$arr[$i];
		}

		$mask=join($mask).'@'.$mail_first[1];
		// print_r($mask);die;
		$this->view->maskmail = $mask;

		$this->view->userId		= $data2['BUSER_ID'];
		// $this->view->usermobilephone	= $data2['USER_MOBILE_PHONE'];
		// $this->view->tokentype = '2';
		// $this->view->tokenIdUser = $data2['TOKEN_ID'];
		// $tokenIdUser = $data2['TOKEN_ID'];
		// $tokenType = '2';
		
		//added new hard token
		// $HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		// $challengeCode 					= $HardToken->generateChallengeCode();
		// $this->view->challengeCode 		= $challengeCode;
		// $this->view->challengeCodeReq 	= $challengeCode;
		if(is_string($user_id))
		{
			if($this->_request->isPost())
			{
			
				$filters = array(
					'old_email'     => array('StripTags','StringTrim'),
			 		'new_email'     => array('StripTags','StringTrim'),
					'confirm_email'     => array('StripTags','StringTrim'),
					'password'     => array('StripTags','StringTrim'),
					
				);
			
				$validators =  array(
							'old_email'       => array('NotEmpty',														
													'messages' => array(
																	   $this->language->_('Current Email Can not be left blank'),																		   
																	   )
													),						  
						  
						   'new_email'    => array('NotEmpty',
													new Application_Validate_EmailAddress(),
													//array('StringLength',array('min'=>1,'max'=>128)),
													'messages' => array(
																	   $this->language->_('New Email cannot be left blank'),
																	   //$this->language->_('Email lenght cannot be more than 128'),
																	   $this->language->_('Invalid email format'),
																	   )
													),
							'confirm_email'    => array('NotEmpty',
													new Application_Validate_EmailAddress(),
													//array('StringLength',array('min'=>1,'max'=>128)),
													'messages' => array(
													$this->language->_('New Email cannot be left blank'),
													//$this->language->_('Email lenght cannot be more than 128'),
													$this->language->_('Invalid email format'),
								)
						),
							'password'       => array('NotEmpty',														
													'messages' => array(
																	   $this->language->_('Password Can not be left blank'),																		   
																	   )
													),
				);

				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getPost());
	
				$oldpass	= $zf_filter_input->old_email;
				$newpass	= $zf_filter_input->new_email;
				$confirm_email	= $zf_filter_input->confirm_email;
				
				if($newpass!=$confirm_email){
					$this->view->error = 1;
// 					$this->view->old_email	= $oldpass;
					$this->view->msg_failed = $this->language->_('Error: Email does not match.');
				}else
				{				
				$errDesc = array();

					$username = ($this->_userIdLogin);
					$userId = $username;
					$key = md5 ( $username );
					$pass = ($zf_filter_input->password);

					$encrypt = new Crypt_AESMYSQL ();
					$password = md5 ( $encrypt->encrypt ( $pass, $key ) );
					// print_r($password);die;
					$login = $this->_db->fetchrow ( $this->_db->SELECT ()->FROM ( 'M_BUSER' )->WHERE ( 'BUSER_ID = ?', $username ) );
					$cekpass = false;
					if ($login ['BUSER_PASSWORD'] == $password) {
							$cekpass = true;
						}
						// print_r($cekpass);die;
				if($zf_filter_input->isValid() && $cekpass)
	  			{





							if ($newpass == $oldpass){
								$this->view->error = 1;
								$errDesc['newem'] = $this->language->_('Email must be different from old email').".";
								$this->view->msg_failed = $this->language->_('Email must be different from old email').".";
								return $errDesc;
							}else{
									
								// check current email
								$select	= $this->_db->select()
								->from(array('M_BUSER'),
										array('BUSER_EMAIL')
								)
								->where("BUSER_ID 		 = ?", $this->_userIdLogin);
								// ->where("CUST_ID 		 = ?", $this->_custId);
								
					// 			echo $select;die;
								$currentEmail = $this->_db->fetchOne($select);
						
									
								if ($confirm_email != $newpass){
									$this->view->error = 1;
									$errDesc['oldem'] = $this->language->_('Sorry, but the Current Email you entered is incorrect').".";
									$this->view->msg_failed = $this->language->_('Sorry, but the Current Email you entered is incorrect').".";
									return $errDesc;
								}
						
						
								if(count($errDesc)>0) $failed = 1;
						
								if(count($errDesc)==0 && $failed == 0)
								{
									$dateFormat = "Y-m-d H:m:s";
									$date = date($dateFormat );
									$data = array(
											'BUSER_EMAIL' => $newpass,
											'UPDATED' => $date
									);
									$where =  array();
						
									$where['BUSER_ID = ?'] = $this->_userIdLogin;
									$updateM_USER = $this->_db->update('M_BUSER',$data,$where);
						
									Application_Helper_General::writeLog('CHME','Change My Email Buser ID : '.$user_id);
									$this->setbackURL('/home/dashboard');
									$this->_redirect('/notification/success/index');
									return true;
								}
							}






							if(!$result)
							{
								if(count($result)>0)
								{
									foreach($result as $key=>$value)	
									{						
										$errDesc[$key] = $value;							
									}
								}
									$this->view->error 	= true;
									$this->view->msg_failed = $this->language->_('Error in processing form values. Please correct values and re-submit');
		
									$this->view->errDesc = $errDesc;
									$this->view->new_email	= $newpass;
									$this->view->old_email	= $oldpass;
							}
							else{
									Application_Helper_General::writeLog('CHME','Change My Email Buser ID : '.$user_id);
									$this->setbackURL('/home');
									$this->_redirect('/notification/success/index');
							}
	  			}else{

	  				$this->view->error = 1;
					$this->view->new_email = ($zf_filter_input->isValid('new_email'))? $zf_filter_input->new_email : $this->_getParam('new_email');
					$error = $zf_filter_input->getMessages();
					
					
					//format error utk ditampilkan di view html 
					$errorArray = null;
					
					foreach($error as $keyRoot => $rowError)
					{
					   foreach($rowError as $errorString)
					   {
						  $errorArray[$keyRoot] = $errorString;
					   }
					}
											
					foreach($errorArray as $key => $val)
					{
						$nameError = "Error_".$key ;
						$this->view->$nameError  = $val;
					}					
					
					$this->view->old_email	= $oldpass;
					if(!$cekpass){
						$this->view->msg_failed = $this->language->_('Error invalid password');
					}else{
						$this->view->msg_failed = $this->language->_('Error in processing form values. Please correct values and re-submit');	
					}
					
	  				
	  			}
	  			
			}
			}	
		}
		
		Application_Helper_General::writeLog('CHME','Change My Email');
	}	
}

