<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Productaccount_IndexController extends Application_Main
{
	
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
		$model = new productaccount_Model_Productaccount();
// 		$conf 		= Zend_Registry::get('config');
		$account_typeArr 	= array_combine($this->_productaccount["code"],$this->_productaccount["desc"]);
      	
    	$this->view->account_typeArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+$account_typeArr);
	
	    $fields = array(
						'product_code'      => array('field' => 'PRODUCT_CODE',
											      'label' => $this->language->_('Account Type'),
											      'sortable' => true),
	    				// 'plan_code'      => array('field' => 'PRODUCT_PLAN',
									// 		      'label' => $this->language->_('Plan Code'),
									// 		      'sortable' => true),
						'product_name'           => array('field' => 'PRODUCT_NAME',
											      'label' => $this->language->_('Account Type Name'),
											      'sortable' => true),
	    				
						'action'           => array('field' => '',
											      'label' => $this->language->_('Action'),
											      'sortable' => false),
				      );
		
   		$filterlist = array("PRODUCT_CODE","PLAN_CODE","PRODUCT_NAME","ACCT_TYPE");
		
		$this->view->filterlist = $filterlist;
	
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','product_code');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'PRODUCT_CODE'  => array('StringTrim','StripTags'),
							// 'PLAN_CODE'  => array('StringTrim','StripTags'),
							'PRODUCT_NAME'      => array('StringTrim','StripTags','StringToUpper'),
							'ACCT_TYPE'      => array('StringTrim','StripTags','StringToUpper'),
		);
		

		 $dataParam = array("PRODUCT_CODE","PLAN_CODE","PRODUCT_NAME","ACCT_TYPE");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		// print_r($dataParamValue);die;
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	

		$fParam = array();
		if($filter == $this->language->_('Set Filter'))
		{
			// die;
			$fParam['fproduct_code'] 		= $zf_filter->getEscaped('PRODUCT_CODE');
			$fParam['fplan_code'] 		= $zf_filter->getEscaped('PLAN_CODE');
			$fParam['fproduct_name']     	= $zf_filter->getEscaped('PRODUCT_NAME');
			$fParam['faccount_type']     	= $zf_filter->getEscaped('ACCT_TYPE');
		}
		else if($filter == $this->language->_('Clear Filter') || $filter == '')
		{
			$fParam['fproduct_code'] = '';
			$fParam['fplan_code'] = '';
			$fParam['fproduct_name'] = '';
			$fParam['facct_type'] = '';
		}
		
		$this->view->product_code = $fParam['fproduct_code'];
		$this->view->plan_code = $fParam['fplan_code'];
		$this->view->product_name     = $fParam['fproduct_name'];
		$this->view->account_type     = $fParam['faccount_type'];
// 		die('here');
		$select = $model->getData($fParam,$sortBy,$sortDir,$filter);		
		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['PRODUCT_ID']);
		}
		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
				$this->_helper->download->csv(array($this->language->_('Product Code'),$this->language->_('Plan Code'),$this->language->_('Product Name')),$selectPdfCsv,null,'product_account');  
				Application_Helper_General::writeLog('PALS','Export to CSV');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf(array($this->language->_('Product Code'),$this->language->_('Plan Code'),$this->language->_('Product Name')),$selectPdfCsv,null,'product_account');   
				Application_Helper_General::writeLog('PALS','Export to PDF');
		}
		elseif($this->_request->getParam('print') == 1){
				unset($fields['action']);
	            $this->_forward('print', 'index', 'widget', array('data_content' => $selectPdfCsv, 'data_caption' => 'product_account', 'data_header' => $fields));
	       		Application_Helper_General::writeLog('PALS','Export to PDF');
		}
		else
		{		
				Application_Helper_General::writeLog('PALS','View Product Account List');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}