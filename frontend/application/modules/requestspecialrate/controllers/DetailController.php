<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';

class requestspecialrate_DetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	protected $_destinationUploadDir = '';
	protected $_listCCYValidate = '';

	public function initController()
	{       
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 	
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
        		
	}
	
	public function indexAction()
	{		

		$this->_helper->layout()->setLayout('newlayout');
		
		$Settings = new Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();		
		//Zend_Debug::dump($ccyList);die;
		
		$this->view->userIdLogin  = $this->_userIdLogin;
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
        
        $getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;
        // var_dump($CCYData);

        $numreq = $this->_getParam('numreq');

        if(!empty($numreq)){
            
            $srdata = $this->_db->select()
                             ->from(array('A' => 'T_REQ_SPCRATE'),array('*'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->where('A.ID_REQUEST = ?',$numreq)
                             ->query()->fetchAll();

            if($srdata){
                $data = $srdata['0'];

                $this->view->CUST_ID 	= $data['CUST_ID'];
                $this->view->USER_ID_LOGIN 	= $data['RATE_REQUESTBY'];
                $this->view->REG_NUM 		= $data['ID_REQUEST'];
                $this->view->FROM_CCY 	= $data['FROM_CCY'];
                $this->view->TO_CCY = $data['TO_CCY'];
                $this->view->AMOUNT 		= $data['AMOUNT_TRX'];
                $this->view->CCY_AMOUNT 		= $data['CCY_AMOUNT'];
                $this->view->STATUS 		= $data['STATUS'];

                if(!empty($data['DEAL_RATE']))
                    $this->view->DEAL_RATE 		= $data['DEAL_RATE'];
                else
                    $this->view->DEAL_RATE        = "0.00";

                if($this->_request->isPost() )
                {
                    $deal = $this->_getParam('deal');
                    $reject = $this->_getParam('reject');

                    
                    if($deal){
                        try 
                        {
                            $data = array (
                                'RATE_UPDATED' => new Zend_Db_Expr("now()"),
                                'RATE_UPDATEDBY' => $this->_userIdLogin,
                                'ACTION' => 'D',
                            );
                            $where['ID_REQUEST = ?'] = $numreq;
                            $this->_db->update('T_REQ_SPCRATE',$data,$where);
                                                    
                            $historyInsert = array(
                                'ID_REQUEST'         => $numreq,
                                'CHANGES_FLAG'         => 'F',
                                'CUST_ID'           => $this->_custIdLogin,
                                'CREATEDBY'        => $this->_userIdLogin,
                                'RATE_DATETIME'    => new Zend_Db_Expr("now()"),
                                'ACTION' => 'D',
                            );
        
                            $this->_db->insert('T_HISTORY_REQRATE', $historyInsert);

                            $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                            $this->_redirect('/notification/success/index');
                        }
                        catch(Exception $e) 
                        {   
                            var_dump($e);die;
                            $this->_db->rollBack();
                        }
                    }

                    if($reject){
                        try 
                        { 
                            $data = array (
                                'STATUS' => '4',
                                'RATE_UPDATED' => new Zend_Db_Expr("now()"),
                                'RATE_UPDATEDBY' => $this->_userIdLogin,
                                'ACTION' => 'R',
                            );
                            $where['ID_REQUEST = ?'] = $numreq;
                            $this->_db->update('T_REQ_SPCRATE',$data,$where);
                            
                            $historyInsert = array(
                                'ID_REQUEST'         => $numreq,
                                'CHANGES_FLAG'         => 'F',
                                'CUST_ID'           => $this->_custIdLogin,
                                'CREATEDBY'        => $this->_userIdLogin,
                                'RATE_DATETIME'    => new Zend_Db_Expr("now()"),
                                'ACTION' => 'R',
                            );
        
                            $this->_db->insert('T_HISTORY_REQRATE', $historyInsert);
    
                            $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                            $this->_redirect('/notification/success/index');
                        }
                        catch(Exception $e) 
                        {   
                            var_dump($e);die;
                            $this->_db->rollBack();
                        }
                    }
                }
            }
        }
    }
}