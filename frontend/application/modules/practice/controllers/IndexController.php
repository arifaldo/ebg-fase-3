<?php
require_once LIBRARY_PATH . 'vendor/autoload.php';

use Zend\InputFilter\InputFilter;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

use function GuzzleHttp\json_decode;

class practice_IndexController extends Application_Main
{
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');
        $requestZend = $this->getRequest();

        if ($requestZend->getParam('download')) {
            $this->downloadFile();
            die();
        }

        // $file = '/app/bgdev2/library/data/uploads/document/submit/9978564f-317f-4379-b1fe-eb6d0627c2f7.pdf';
        $file = '/app/bgdev2/library/data/uploads/document/submit/test.pdf';
        file_put_contents(UPLOAD_PATH . '/document/submit/test.pdf', fopen('https://mag.wcoomd.org/uploads/2018/05/blank.pdf', 'r'));
        // file_put_contents(UPLOAD_PATH . '/document/submit/test.pdf', fopen('https://doc.sandbox-111094.com/download/signed/eyJpdiI6IjRNT0xwRlBLcDErZDQ5NHJXcGN3MXc9PSIsInZhbHVlIjoiRmdONVVMVVhVVC9OcEIxYnVmSkNud1BBQjBPRG5MUmI5L2xpT3doSm1OYk5KK05QbUpmRmZyckZYTGRWVGNQTSIsIm1hYyI6ImYxMzIwMzQ4NTU5OTI0N2QyNGQyZjRjNTFkNzAwYjY2MTM0OTAxYWQxNGE4YzBjYWRiZDcxZTRmNDhhNjJjZTkiLCJ0YWciOiIifQ==?key=xcxoq6wclhde1ecq0vaylhi1zqgcxgzy', 'r'));

        for ($i = 0; $i < 2; $i++) {
            # code...
            $getMimeType = mime_content_type($file);
            $getFileSize = filesize($file);
            $fileExist = file_exists($file);

            if ($getMimeType != 'application/pdf' || $getFileSize == 0 || !$fileExist) {
                break;
            } else {
                file_put_contents(UPLOAD_PATH . '/document/submit/test' . $i . '.pdf', fopen('https://mag.wcoomd.org/uploads/2018/05/blank.pdf', 'r'));
                sleep(2);
            }
        }
        // Zend_Debug::dump(mime_content_type($file));
        die('finish');
        $getPdf = file_get_contents($file);
        print_r($getPdf);
        die();
        sleep(5);

        Zend_Debug::dump(filesize($file));
        die();

        // try {
        //     //code...
        //     Zend_Debug::dump($this->_request);
        //     Zend_Debug::dump($this->_helper->url(''));
        //     die();
        // } catch (\Throwable $th) {
        //     //throw $th;
        //     Zend_Debug::dump($th->getMessage());
        //     die();
        // }

        // $this->_helper->json(['Info' => [
        //     'Nama' => 'Mahabatan',
        //     'Umur' => 37
        // ]]);

        if ($requestZend->getParam('filter')) {
            try {
                //code...
                $fullUrl = parse_url($requestZend->getRequestUri());
                $uri = urldecode($fullUrl['query']);
                parse_str($uri, $requestUri);

                $this->view->filter = $requestUri;
            } catch (\Throwable $th) {
                //throw $th;
                Zend_Debug::dump($th->getMessage());
                die();
            }
        }

        try {
            //code...
            $client = new Client();
            $request = new Request('GET', 'https://jsonplaceholder.typicode.com/posts');
            $response = $client->send($request);

            $result = json_decode($response->getBody(), true);

            // filter 
            if ($requestZend->getParam('filter')) {
                $counter = 0;
                foreach ($requestUri['wherecol'] as $key => $value) {
                    // title
                    if ($value['title']) {
                        $result = array_filter($result, function ($item) use ($requestUri, $counter) {
                            return false !== strpos($item['title'], $requestUri['whereval'][$counter]);
                        });
                    }

                    $counter++;
                }
            }

            $this->paging($result, 10);
        } catch (\Throwable $th) {
            //throw $th;
            Zend_Debug::dump($th->getMessage());
            die();
        }

        if ($requestZend->isPost()) {

            $errorArray = [];
            $usernameArray = [];
            $validUsername = true;

            foreach ($request->getParam('username') as $key => $user) {
                $key = $key + 1;

                $filterStripTags = new Zend_Filter_StripTags();
                $user = $filterStripTags->filter($user);

                $filterStringTrim = new Zend_Filter_StringTrim();
                $user = $filterStringTrim->filter($user);

                $stringKeyUser = 'username' . $key;
                $this->view->$stringKeyUser = $user;

                $emptyValidator = new Zend_Validate_NotEmpty();
                $emptyValidator->setMessages([
                    Zend_Validate_NotEmpty::IS_EMPTY => 'Tidak boleh kosong',
                ]);

                if (!$emptyValidator->isValid($user)) {
                    $errorArray['username' . $key] = array_shift($emptyValidator->getMessages());
                    $validUsername = false;
                    continue;
                }

                $alphaValidator = new Zend_Validate_Alpha();
                $alphaValidator->setMessages([
                    Zend_Validate_Alpha::NOT_ALPHA => 'Hanya boleh huruf',
                ]);

                if (!$alphaValidator->isValid($user)) {
                    $errorArray['username' . $key] = array_shift($alphaValidator->getMessages());
                    $validUsername = false;
                    continue;
                }

                $usernameValidator = new Zend_Validate_StringLength(['min' => 5, 'max' => 10]);
                $usernameValidator->setMessages([
                    Zend_Validate_StringLength::TOO_SHORT => 'Tidak boleh kurang dari %min% karakter',
                    Zend_Validate_StringLength::TOO_LONG => 'Tidak boleh lebih dari %max% karakter'
                ]);
                if (!$usernameValidator->isValid($user)) {
                    $errorArray['username' . $key] = array_shift($usernameValidator->getMessages());
                    $validUsername = false;
                }
            }

            if (!$validUsername) {
                $this->view->errorUsernames = $errorArray;
            }

            $filters = array(
                'email' => array('StringToLower'),
            );

            $strLenValidator = new Zend_Validate_StringLength(['min' => 5, 'max' => 10]);
            $strLenValidator->setMessage(
                'Tidak boleh kurang dari %min% karakter',
                Zend_Validate_StringLength::TOO_SHORT,
            );
            $strLenValidator->setMessage(
                'Tidak boleh lebih dari %max% karakter',
                Zend_Validate_StringLength::TOO_LONG,
            );

            // $expProdCodeAndName = $this->_db->quoteIdentifier('PRODUCT_CODE') . " = " . $this->_db->quote($inputs->account_type);
            // $validators = array(

            // 	'plan_code'  => array(
            // 		'NotEmpty',
            // 		'Alnum',
            // 		new Zend_Validate_StringLength(array('min' => 1, 'max' => 4)),
            // 		array(
            // 			'Db_NoRecordExists',
            // 			array(
            // 				'table' => 'M_PRODUCT',
            // 				'field' => 'PRODUCT_PLAN',
            // 				'exclude' => $expProdCodeAndName
            // 			)
            // 		),
            // 		'messages' => array(
            // 			$this->language->_('Can not be empty'),
            // 			$this->language->_('Invalid Format'),
            // 			$this->language->_('Data too long (min. 1 chars and max. 4 chars)'),
            // 			$this->language->_('Plan Code already exists'),
            // 		)
            // 	),

            // 	'product_name' => array(
            // 		'NotEmpty',
            // 		new Zend_Validate_StringLength(array('max' => 35)),
            // 		array(
            // 			'Db_NoRecordExists',
            // 			array(
            // 				'table' => 'M_PRODUCT',
            // 				'field' => 'PRODUCT_NAME',
            // 				'exclude' => $expPlanProdCode
            // 			)
            // 		),
            // 		'messages' => array(
            // 			$this->language->_('Can not be empty'),
            // 			$this->language->_('Data too long (max 35 chars)'),
            // 			$this->language->_('Product Name already exists'),
            // 		)
            // 	),
            // 	'account_type' => array(
            // 		'NotEmpty',
            // 		'messages' => array(
            // 			$this->language->_('Please Choose One'),
            // 		)
            // 	),
            // );

            $validators =  [
                'password'      => [
                    'NotEmpty',
                    'messages' => [
                        'Tidak boleh kosong',
                    ]
                ],
                'email' => [
                    'NotEmpty',
                    'messages' => [
                        'Tidak boleh kosong'
                    ]
                ],
                'username' => [
                    'NotEmpty',
                    'Alpha',
                    $strLenValidator,
                    'messages' => [
                        'Tidak boleh kosong',
                        'Hanya boleh huruf',
                    ]
                ],
            ];

            $zf_filter_input = new Zend_Filter_Input($filters, $validators, $request->getParams(), $this->_optionsValidator);
            $zf_filter_input->setDefaultEscapeFilter(['StripTags', 'StringTrim']);

            if (!$zf_filter_input->isValid() || !$validUsername) {
                $errorMessages = $zf_filter_input->getMessages();

                $errorArray = [];
                foreach ($errorMessages as $errorKey => $messages) {
                    foreach ($messages as $message) {
                        $errorArray[$errorKey] = $message;
                    }
                }

                $this->view->messages = $errorArray;
                foreach ($validators as $key => $value) {
                    $this->view->$key = ($zf_filter_input->isValid($key)) ? $zf_filter_input->getEscaped($key) : $request->getParam($key);
                }
            } else {
                $this->setbackURL('/practice');
                $this->_redirect('/notification/success/index');
            }
        }
    }

    private function downloadFile()
    {
        $selectAggrementpdf = $this->_db->select()
            ->from('M_SETTING')
            ->where('SETTING_ID = ?', 'ftemplate_agreementonline');
        $terms = $this->_db->fetchRow($selectAggrementpdf);

        $this->view->terms = $terms['SETTING_VALUE'];

        //generate pdf utk di attach ke email
        $outputHTML = "<tr><td>" . $this->view->render('/index/indexpdfnew.phtml') . "</td></tr>";

        // echo $outputHTML;die();
        // echo $terms['SETTING_VALUE'];die();

        $namefile = $this->language->_('E-Registration') . date('Y-m-d_H:i:s');

        $this->_helper->download->generatePdf(null, null, null, $namefile, $outputHTML, false);
        // $this->_helper->download->generatePdf(null, null, null, $namefile, $terms['SETTING_VALUE'], false);
    }

    public function genpdfAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $destination = LIBRARY_PATH . '/data/temp/';
        $fileName = uniqid();

        $path =  $destination . '../../../frontend/public/images/logo//logo-btn.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);

        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $this->view->tnc = $this->getSetting('ftemplate_agreementonline');

        $fileHeader = '<!DOCTYPE html><html><head><title>Header PDF</title></head><body><div style="font-size: 12px;">
				
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td align="left"><img style="height:50px;" class="img-fluid" src="' . $base64 . '"></td>
						<td align="center" valign="middle"><strong>' . $fileName . '</strong></td>
						<td align="right" valign="middle"><strong>' . date("d F Y H:i:s") . '</strong></td>
					</tr>
				</table>
				
            </div></body></html>';

        $htmldata = $this->view->render('/index/genpdf.phtml');

        // echo $this->view->tnc;
        // die();

        // echo $htmldata;die();

        file_put_contents($destination . $fileName . '-header.html', $fileHeader);
        file_put_contents($destination . $fileName . '.html', $htmldata);

        $webkitOpt = '--footer-font-size 10 --footer-right "Page [page]/[topage]   " --margin-top 30mm --margin-bottom 30mm --print-media-type --header-spacing 10 --footer-spacing 10 --header-html file://' . $destination . $fileName . '-header.html';
        $shell = '/usr/local/bin/wkhtmltopdf ' . $webkitOpt . ' file://' . $destination . $fileName . '.html ' . $destination . $fileName . '.pdf';

        shell_exec($shell);
        unlink($destination . $fileName . '-header.html');
        // unlink($destination . $fileName . '.html');
    }
}
