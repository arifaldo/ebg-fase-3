<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	
	$emailSent = 0;
	$emailFailed = 0;
	$db = Zend_Db_Table::getDefaultAdapter();
	$update = 0;

	$emailQueue = $db->SELECT()
										->FROM(array('Q'=>'T_EMAIL_QUEUE'),array('EMAIL_ID','SUBJECT','EMAIL_TO','MESSAGE'))
										->WHERE('SENT_FLAG = ?', 0)
										->QUERY()->FETCHALL();
	if(is_array($emailQueue))
	{
		/*
		*Application_Helper_Email::sendEmail($EmailTo,$subject,$mailContent);
		*/
		foreach($emailQueue as $param)
		{
			$status = Application_Helper_Email::sendEmail($param['EMAIL_TO'],$param['SUBJECT'],$param['MESSAGE']);	
			if($status)
			{
				$emailSent++;
			}
			else
			{
				$emailFailed++;
			}
			$where = array();
			$updateParam = array(    'SENT_FLAG'      => '1' );
			$where[] = "EMAIL_ID = '".$param['EMAIL_ID']."'";
			$update += $db->update('T_EMAIL_QUEUE', $updateParam, $where);
		}
	}
	$msg =  $emailSent.' email Sent. '.$emailFailed.' email Failed.';
	echo $msg;
	
	Application_Helper_General::cronLog(basename(__FILE__),$msg,1);
?>
