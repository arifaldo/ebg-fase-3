<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'Zend/Loader/PluginLoader.php';
require_once 'Zend/Controller/Action/Helper/Abstract.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/Writer/Excel2007.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class bgreport_IndexController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $xls = $this->_getParam('xls');
    $xlsx = $this->_getParam('xlsx');
    $csv = $this->_getParam('csv');
    $pdf = $this->_getParam('pdf');

    Application_Helper_General::writeLog('RBGL', 'View Customer BG List');

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                     'label' => $this->language->_('Alias Name'),
                                     'sortable' => true),*/
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg No# /  BG No#'),
      ),
      'subject'     => array(
        'field'    => 'BG_SUBJECT',
        'label'    => $this->language->_('Subject'),
      ),
      'principal'     => array(
        'field'    => 'CUST_NAME',
        'label'    => $this->language->_('Principal Name'),
      ),
      'obligee'     => array(
        'field'    => 'RECIPIENT_NAME',
        'label'    => $this->language->_('Obligee Name'),
      ),
      'branch'     => array(
        'field'    => 'BRANCH_NAME',
        'label'    => $this->language->_('Bank Branch'),
      ),
      'countertype'     => array(
        'field'    => 'COUNTERTYPE',
        'label'    => $this->language->_('Counter Type'),
      ),
      'changetype'     => array(
        'field'    => 'CHANGETYPE',
        'label'    => $this->language->_('Change Type'),
      ),
      'status'     => array(
        'field'    => 'STATUS',
        'label'    => $this->language->_('Status'),
      )



    );

    $filterlist = array('BG_SUBJECT', 'BG_STATUS', 'BG_NUMBER', 'BG_REG_NUMBER');

    $this->view->filterlist = $filterlist;

    $filterArr = array(
      'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
      'BG_STATUS'  => array('StringTrim', 'StripTags'),
      'BG_NUMBER'   => array('StringTrim', 'StripTags'),
      'BG_REG_NUMBER'   => array('StringTrim', 'StripTags')
    );

    // print_r($filterArr);die;
    // if POST value not null, get post, else get param ,"SOURCE_ACCOUNT_NAME","BENEFICIARY_ACCOUNT","BENEFICIARY_ACCOUNT_NAME","TRANSACTION_ID","PS_CCY"
    $dataParam = array('BG_SUBJECT', 'BG_STATUS', 'BG_NUMBER', 'BG_REG_NUMBER');
    $dataParamValue = array();
    foreach ($dataParam as $dtParam) {

      // print_r($dtParam);die;
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            if (!empty($dataParamValue[$dtParam])) {
              $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
            }
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }

      // $dataPost = $this->_request->getPost($dtParam);
      // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
    }


    $options = array('allowEmpty' => true);
    $validators = array(
      'BG_SUBJECT'        => array(),
      'BG_STATUS'        => array(),
      // 'SOURCE_ACCOUNT'     => array(array('InArray', array('haystack' => array_keys($optarrAccount)))),
      'BG_NUMBER'     => array(),
      'BG_REG_NUMBER'     => array()

    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

    // print_r($fCreatedEnd);die;


    $subject    = $zf_filter->getEscaped('BG_SUBJECT');
    $status    = $zf_filter->getEscaped('BG_STATUS');
    $fbgnumb    = $zf_filter->getEscaped('BG_NUMBER');
    $fbgregnumb     = $zf_filter->getEscaped('BG_REG_NUMBER');




    if ($filter == NULL && $clearfilter != 1) {
      //      $fUpdatedStart  = date("d/m/Y");
      //      $fUpdatedEnd  = date("d/m/Y");

      // echo "f0 c1";
    } else {
    }

    //$whereIn = [7, 20, 21, 22, 23];
    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $BgcomplationType     = $config["bg"]["complation"]["desc"];
    $BgcomplationCode     = $config["bg"]["complation"]["code"];

    $arrStatusSettlement = array_combine(array_values($BgcomplationCode), array_values($BgcomplationType));

    $this->view->arrStatusSettlement = $arrStatusSettlement;

    $arrChangeType = array(
      '0' => 'New',
      //'2' => 'Indirect',
      '1' => 'Amendment Changes',
      '2' => 'Amendment Draft'
    );

    $this->view->arrChangeType = $arrChangeType;

    $arrWarrantyType = array(
      '1' => 'Full Cover',
      //'2' => 'Indirect',
      '2' => 'Line Facility',
      '3' => 'Insurance'
    );

    $this->view->arrWarrantyType = $arrWarrantyType;


    //print 
    $caseStatus = "(CASE BG_STATUS ";
    foreach ($arrStatus as $key => $val) {
      $caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseStatus .= " END)";

    $caseCounter = "(CASE COUNTER_WARRANTY_TYPE ";
    foreach ($arrWarrantyType as $key => $val) {
      $caseCounter .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseCounter .= " END)";

    $caseType = "(CASE CHANGE_TYPE ";
    foreach ($arrChangeType as $key => $val) {
      $caseType .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseType .= " END)";


    $select = $this->_db->select()
      ->from(
        array('A' => 'T_BANK_GUARANTEE'),
        array(
          'A.BG_REG_NUMBER',
          'A.BG_NUMBER',
          'A.BG_SUBJECT',
          'C.CUST_NAME',
          'A.RECIPIENT_NAME',
          'B.BRANCH_NAME',
          'COUNTERTYPE' => $caseCounter,
          'CHANGETYPE' => $caseType,
          'STATUS' => $caseStatus,
          'A.BG_INSURANCE_CODE',
          'A.COUNTER_WARRANTY_TYPE',
          'A.CHANGE_TYPE',
          'A.TIME_PERIOD_END',
          'A.BG_STATUS',
          'COMPLETION_STATUS' => 'A.COMPLETION_STATUS',
          // 'A.CLOSING_TYPE'
        )
      )
      ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
      ->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = A.CUST_ID', array('CUST_NAME'));
    //  ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
    //->where('A.BG_STATUS NOT IN (?)', '15','16');
    //->order('BG_CREATED DESC');
    $auth = Zend_Auth::getInstance()->getIdentity();
    // if ($auth->userHeadQuarter == "NO") {
    //   $select->where('B.ID = ?', $auth->userBranchId); // Add Bahri
    // }


    if ($status) {
      $select->where("BG_STATUS = ? ", $status);
    }

    if ($subject) {
      $subjectArr = explode(',', $subject);
      $select->where("UPPER(BG_SUBJECT)  like ?", '%' . $subjectArr . '%');

      $select->where("BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
    }

    if ($fbgnumb)
    // {  $select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and SOURCE_ACCOUNT = ?) > 0", (string)$fAcctsrc);   }
    { //$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fAcctsrc.'%'));  
      $fAcctsrcArr = explode(',', $fbgnumb);
      $select->where("BG_NUMBER  LIKE ?", '%' . $fAcctsrcArr . '%');
    } // ACCSRC ?????

    if ($fbgregnumb) { //$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBenefsrc.'%'));  
      $fBenefsrcArr = explode(',', $fbgregnumb);
      $select->where("BG_REG_NUMBER  LIKE ?", '%' . $fBenefsrcArr . '%');
    }


    // $txt = $this->_getParam('csv');
    if ($txt || $csv || $pdf || $this->_request->getParam('print') || $xls || $xlsx) {

      $arr = $this->_db->fetchAll($select);

      foreach ($arr as $key => $val) {

        unset($arr[$key]['TIME_PERIOD_END']);
        unset($arr[$key]['BG_REG_NUMBER']);
        unset($arr[$key]['BG_INSURANCE_CODE']);
        unset($arr[$key]['COUNTER_WARRANTY_TYPE']);
        unset($arr[$key]['CHANGE_TYPE']);
        unset($arr[$key]['BG_STATUS']);
        unset($arr[$key]['COMPLETION_STATUS']);
      }

      $header = Application_Helper_Array::simpleArray($fields, 'label');
      if ($csv) {
        $this->_helper->download->csv($header, $arr, null, $this->language->_('Customer List BG'));
        Application_Helper_General::writeLog('RBGR', 'Download CSV Customer List BG');
      } else if ($pdf) {
        foreach ($arr as $key => $val) {
          unset($arr[$key]['TIME_PERIOD_END']);
          unset($arr[$key]['BG_REG_NUMBER']);
          unset($arr[$key]['BG_INSURANCE_CODE']);
          unset($arr[$key]['COUNTER_WARRANTY_TYPE']);
          unset($arr[$key]['CHANGE_TYPE']);
          unset($arr[$key]['BG_STATUS']);
        }
        $this->_helper->download->pdf($header, $arr, null, $this->language->_('Customer List BG'));
        Application_Helper_General::writeLog('RBGR', 'Download PDF Customer List BG');
      } else if ($this->_request->getParam('print') == 1) {

        $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Customer List BG', 'data_header' => $fields));
      } else if ($xls) {
        var_dump("test");
        die();
        $this->_helper->download->xls($header, $arr, null, $this->language->_('Customer List BG Xls'));
      } else if ($xlsx) {
        $this->_helper->download->xlsx($header, $arr, null, $this->language->_('Customer List BG xlsx'));
      }

      // if ($txt) {

      //   Application_Helper_General::writeLog('DTRX', 'Download TXT Transaction Report');
      //   $arrTxt = $this->_db->fetchAll($select);


      //   $txtData = array();
      //   foreach ($arrTxt as $ky => $vl) {
      //     /*$selectcity = $this->_db->select()
      //                     ->from(array('M_CITY'),array('*'))
      // 			  ->where('CITY_NAME = ?',$vl['RECIPIENT_CITY']);
      // 	$city = $this->_db->fetchRow($selectcity);
      // 	if(empty($city)){
      // 		$citycode = $city['CITY_CODE'];
      // 	}else{
      // 		$citycode = '0300';
      // 	}*/

      //     $txtData[$ky]['FLAG'] = 'D';
      //     $txtData[$ky]['BG_NUMBERA'] = $vl['BG_NUMBER'] . 'A';
      //     $txtData[$ky]['BG_NUMBER'] = $vl['BG_NUMBER'];
      //     $txtData[$ky]['CUST_NAME'] = $vl['CUST_NAME'];
      //     $txtData[$ky]['SEGMENT'] = 'F05';
      //     $txtData[$ky]['AGUNAN'] = '1';
      //     $txtData[$ky]['KODEAGUNAN'] = '252';
      //     $txtData[$ky]['PERINGKATAGUNAN'] = '';
      //     $txtData[$ky]['KODEPERINGKAT'] = '';
      //     $txtData[$ky]['JENISPENGIKAT'] = '99';
      //     $txtData[$ky]['TGLPENGIKAT'] = str_replace('-', '', $vl['TIME_PERIOD_START']);
      //     $txtData[$ky]['RECIPIENT_NAME'] = $vl['RECIPIENT_NAME'];
      //     $txtData[$ky]['GUARANTEE_TRANSACTION'] = $vl['GUARANTEE_TRANSACTION'];
      //     $txtData[$ky]['RECIPIENT_ADDRES'] = $vl['RECIPIENT_ADDRES'];
      //     //$txtData[$ky]['CITY_CODE'] = $$citycode;
      //     $txtData[$ky]['BG_AMOUNT'] = $vl['BG_AMOUNT'];
      //     $txtData[$ky]['BG_AMOUNTLK'] = $vl['BG_AMOUNT'];
      //     $txtData[$ky]['DATELK'] = str_replace('-', '', $vl['TIME_PERIOD_START']);
      //     $txtData[$ky]['NILAIPENILAI'] = '';
      //     $txtData[$ky]['NAMAPENILAI'] = '';
      //     $txtData[$ky]['DATEPENILAI'] = '';
      //     $txtData[$ky]['PARIPASUSTATS'] = 'T';
      //     $txtData[$ky]['PARIPASUPECENT'] = '';
      //     $txtData[$ky]['STATUSKREDIT'] = 'T';
      //     $txtData[$ky]['STATUSINSURANCE'] = 'Y';
      //     $txtData[$ky]['DESC'] = '';
      //     $txtData[$ky]['KACAB'] = '70';
      //     $txtData[$ky]['OPERASIDATA'] = 'C';
      //   }

      //   $this->_helper->download->txtblank(null, $txtData, null, 'Bank Guarantee List');
      // }
    }

    $select = $select->query()->fetchAll();
    $this->paging($select);

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    // $arrStatus = array('1' => 'Waiting for review',
    //                     '2' => 'Waiting for approve',
    //                     '3' => 'Waiting to release',
    //                     '4' => 'Waiting for bank approval',
    //                     '5' => 'Issued',
    //                     '6' => 'Expired',
    //                     '7' => 'Canceled',
    //                     '8' => 'Claimed by applicant',
    //                     '9' => 'Claimed by recipient',
    //                     '10' => 'Request Repair',
    //                     '11' => 'Reject',
    //                   );





    $arrType = array(
      1 => 'Standard',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;



    if (!empty($dataParamValue)) {

      $this->view->createdStart = $dataParamValue['BG_CREATED'];
      $this->view->createdEnd = $dataParamValue['BG_CREATED_END'];
      $this->view->efdateStart = $dataParamValue['BG_DATEFROM'];
      $this->view->efdateEnd = $dataParamValue['BG_DATEFROM_END'];



      unset($dataParamValue['BG_DATEFROM_END']);
      unset($dataParamValue['BG_CREATED_END']);

      //var_dump($dataParamValue);
      foreach ($dataParamValue as $key => $value) {
        //foreach($value as $ss => $vs){

        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[] = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[] = $key;
          $whereval[] = $value;
        }

        //}


      }
      //var_dump($wherecol);
      //var_dump($whereval);die;

      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }
  }

  public function testAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();


    $this->_helper->download->csv([], [], null, $this->language->_('Signing Stage List') . " - " . date("dMYHis"));
  }
}
