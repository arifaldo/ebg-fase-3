<?php

require_once 'Zend/Controller/Action.php';

class Branchbank_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		//pengaturan url untuk button back
		$this->setbackURL('/' . $this->_request->getModuleName() . '/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}
		
		$id = $this->_getParam('id');
		$yhead = $this->_getParam('yhead');
		$nhead = $this->_getParam('nhead');
		
		$this->view->id = $id;
		$this->view->nhead = $nhead;
		$this->view->yhead = $yhead;

		$this->view->report_msg = array();

		if (!$this->_request->isPost()) {
			$id = $this->_getParam('id');

			//$bank_id = (Zend_Validate::is($bank_id,'Digits'))? $bank_id : null;

			if ($id) {
				$resultdata = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('M_BRANCH'))
						->where("ID=?", $id)
				);
				if ($resultdata) {
					$this->view->branch_code        = $resultdata['BRANCH_CODE'];
					$this->view->branch_name        = $resultdata['BRANCH_NAME'];
					$this->view->pnsetempat        = $resultdata['LOCAL_PN'];
					$this->view->branch_address      = $resultdata['BANK_ADDRESS'];
					$this->view->region_name      = $resultdata['REGION_NAME'];
					$this->view->city_name      = $resultdata['CITY_NAME'];
					$this->view->contact      = $resultdata['CONTACT'];
					$this->view->headquarter      = $resultdata['HEADQUARTER'];
					//die($resultdata['HEADQUARTER']);
					if ($resultdata['HEADQUARTER'] == 'YES') {
						$this->view->yhead      = 'checked';
					} else {
						$this->view->nhead      = 'checked';
					}
				}
				$resultdataCoverage = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_BRANCH_COVERAGE'))
						->where("BRANCH_CODE=?", $resultdata['BRANCH_CODE'])
				);

				$BRANCH_COVERAGE_CODE = "";
				$BRANCH_COVERAGE_NAME = "";

				foreach ($resultdataCoverage as $key => $value) {
					$BRANCH_COVERAGE_CODE .= $value['BRANCH_COVERAGE_CODE'] . ", ";
					$BRANCH_COVERAGE_NAME .= $value['BRANCH_COVERAGE_NAME'] . ", ";
				}

				$this->view->branch_code_coverage  =  rtrim($BRANCH_COVERAGE_CODE, ", ");
				$this->view->branch_name_coverage  =  rtrim($BRANCH_COVERAGE_NAME, ", ");
			} else {
				$error_remark = 'Branch Bank not found';

				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
				$this->_redirect('/' . $this->_request->getModuleName() . '/index');
			}
		} else {
			$filters = array(
				'branch_code' => array('StringTrim', 'StripTags'),
				'branch_name' => array('StringTrim', 'StripTags'),
				'branch_address' => array('StringTrim', 'StripTags'),
				'city_name' => array('StringTrim', 'StripTags'),
				'region_name' => array('StringTrim', 'StripTags'),
				'contact' => array('StringTrim', 'StripTags'),
				'headquarter' => array('StringTrim', 'StripTags'),
				'branch_code_coverage' => array('StringTrim', 'StripTags'),
				'branch_name_coverage' => array('StringTrim', 'StripTags'),
				'pnsetempat' => array('StringTrim', 'StripTags'),
			);

			$validators = array(
				'branch_code' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty'),
					)
				),
				'branch_name' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty'),
					)
				),
				'branch_address' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'city_name'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'region_name'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'contact'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'headquarter'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'branch_code_coverage' => array(
					'allowEmpty' => true,
					'messages' => array()
				),
				'branch_name_coverage' => array(
					'allowEmpty' => true,
					'messages' => array()
				),
				'pnsetempat' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

			if ($zf_filter_input->isValid()) {
				$branch_name_coverageArr = explode(",", $this->_getParam('branch_name_coverage'));

				$content = array(
					'BRANCH_NAME' 	 => $zf_filter_input->branch_name,
					'BANK_ADDRESS' 	 => $zf_filter_input->branch_address,
					'CITY_NAME' 	 => $zf_filter_input->city_name,
					'REGION_NAME' 	 => $zf_filter_input->region_name,
					'CONTACT' 	 => $zf_filter_input->contact,
					'HEADQUARTER' 	 => $zf_filter_input->headquarter,
					'LOCAL_PN' 	 => $zf_filter_input->pnsetempat
				);

				try {
					//-----insert--------------
					$this->_db->beginTransaction();

					if ($zf_filter_input->headquarter == 'YES') {
						$content2 = array(
							'HEADQUARTER' 	 => 'NO'
						);
						$whereArr2  = array('HEADQUARTER = ?' => $zf_filter_input->headquarter);
						$this->_db->update('M_BRANCH', $content2, $whereArr2);
					}

					$whereArr  = array('BRANCH_CODE = ?' => $zf_filter_input->branch_code);
					$this->_db->update('M_BRANCH', $content, $whereArr);
					$this->_db->delete('M_BRANCH_COVERAGE', 'BRANCH_CODE = ' . $this->_db->quote($zf_filter_input->branch_code));
					if ($branch_name_coverageArr) {
						//var_dump($branch_name_coverageArr);
						//die('1');
						foreach ($branch_name_coverageArr as $row) {
							if (!empty($row)) {
								$contentCoverage = array(
									'BRANCH_CODE' 	 => $zf_filter_input->branch_code,
									'BRANCH_COVERAGE_CODE' 	 => $this->getBranch($row)["BRANCH_CODE"],
									'BRANCH_COVERAGE_NAME' 	 => $this->getBranch($row)["BRANCH_NAME"],
									'BANK_COVERAGE_ADDRESS' 	 => $this->getBranch($row)["BANK_ADDRESS"],
									'CONTACT' 	 => $this->getBranch($row)["CONTACT"],
									'CREATED' 		 => date("Y-m-d H:i:s"),
									'CREATEDBY' 	 => $this->_userIdLogin
								);



								$this->_db->insert('M_BRANCH_COVERAGE', $contentCoverage);
							}
						}
					}


					$this->_db->commit();

					foreach (array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					Application_Helper_General::writeLog('MNBB', 'Edit Branch Bank. Branch Name : [' . $zf_filter_input->branch_name . ']');
					$this->view->success = true;
					$this->view->report_msg = array();

					$this->_redirect('/notification/success/index');
				} catch (Exception $e) {
					//var_dump($e);
					//die();
					//rollback changes
					$this->_db->rollBack();

					foreach (array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);
					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			} else {
				$this->view->error = true;

				foreach (array_keys($filters) as $field)
					$this->view->$field = $zf_filter_input->getEscaped($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html 
				$errorArray = null;
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$errorArray[$keyRoot] = $errorString;
					}
				}

				$this->view->succes = false;
				$this->view->report_msg = $errorArray;
			}
		}
	}

	public function getBranch($branch_name)
	{
		$select = $this->_db->select()
			->from(array('M_BRANCH'), array('*'))
			->where('BRANCH_NAME = ?', $branch_name);
		//die();
		$data = $this->_db->fetchRow($select);

		return $data;
	}

	public function saveAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();


		$this->_db->beginTransaction();

		$branch_name_coverageArr = explode(",", $this->_getParam('branch_name_coverage'));

		$this->_db->delete('TEMP_BRANCH_COVERAGE', 'BRANCH_CODE = ' . $this->_db->quote($this->_getParam('branch_code')));
		if ($branch_name_coverageArr) {
			foreach ($branch_name_coverageArr as $row) {

				if (!empty($row)) {
					$contentCoverage = array(
						'BRANCH_CODE' 	 => $this->_getParam('branch_code'),
						'BRANCH_COVERAGE_CODE' 	 => $this->getBranch(ltrim($row))["BRANCH_CODE"],
						'BRANCH_COVERAGE_NAME' 	 => $this->getBranch(ltrim($row))["BRANCH_NAME"],
						'BANK_COVERAGE_ADDRESS' 	 => $this->getBranch(ltrim($row))["BANK_ADDRESS"],
						'CONTACT' 	 => $this->getBranch($row)["CONTACT"],
						'CREATED' 		 => date("Y-m-d H:i:s"),
						'CREATEDBY' 	 => $this->_userIdLogin
					);

					$this->_db->insert('TEMP_BRANCH_COVERAGE', $contentCoverage);
				}
			}
		}

		$this->_db->commit();

		return true;
	}
}
