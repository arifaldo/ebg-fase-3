<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/SystemBalance.php';
class nationalpoolinggroup_DetailController extends Application_Main 
{
    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;
		
		$id = $this->_getParam('np');
		$clean = $this->_getParam('clean');
		$cleanna = $this->_getParam('cleanna');
		$params = $this->_request->getParams();
		//var_dump($params);die;
		$this->view->id = $id;
		$select = $this->_db->select()
                            ->from(array('A' => 'T_NOTIONAL_POOLING'),array('N_NUMBER','N_CUST_ID','N_CREATED','N_CIF','N_CREATEDBY','N_UPDATED','N_UPDATEDBY','N_STATUS','N_UD_ID','N_BALANCEUPDATE'))
                            ->join(array('B' => 'M_CUSTOMER'), 'B.CUST_ID = A.N_CUST_ID',array('B.CUST_NAME'))
							->where('A.N_NUMBER = ?',$id);
        $datapool                   = $this->_db->fetchAll($select);                    

		if(!empty($datapool)){
			$this->view->natid = $datapool['0']['N_NUMBER'];
			if($datapool['0']['N_STATUS'] == '0'){
				$status = 'Waiting Approval';
			}else if($datapool['0']['N_STATUS'] == '1'){
				$status = 'Approve';
			}else{
				$status = 'Deleted';
			} 
			$this->view->status = $status;
			$this->view->custname = $datapool['0']['CUST_NAME'];
			$this->view->custid = $datapool['0']['N_CUST_ID'];
			$this->view->custcif = $datapool['0']['N_CIF'];
			 
			$this->view->suggestby = $datapool['0']['N_UPDATEDBY'];
			
			$this->view->suggested = Application_Helper_General::convertDate($datapool['0']['N_UPDATED'],'dd MMM yyyy HH:mm:ss','"yyyy-MM-dd HH:mm:ss');
			$this->view->approve = Application_Helper_General::convertDate($datapool['0']['N_CREATED'],'dd MMM yyyy HH:mm:ss','"yyyy-MM-dd HH:mm:ss');
			$this->view->approvedby = $datapool['0']['N_CREATEDBY'];
			if($datapool['0']['N_BALANCEUPDATE'] != NULL){
			$this->view->lastupdate = Application_Helper_General::convertDate($datapool['0']['N_BALANCEUPDATE'],'dd MMM yyyy HH:mm:ss','"yyyy-MM-dd HH:mm:ss');
			}else{
			$this->view->lastupdate = Application_Helper_General::convertDate($datapool['0']['N_UPDATED'],'dd MMM yyyy HH:mm:ss','"yyyy-MM-dd HH:mm:ss');
			}
			//var_dump($this->view->lastupdate);
			if(!empty($datapool['0']['N_UD_ID'])){
			$selectud = $this->_db->select()
                    ->distinct()
                    ->from(	
                            array('TFS'=>'T_FILE_SUBMIT'),
                            array(
                                    'FILE_ID'			=>'TFS.FILE_ID',
                                    'FILE_NAME'			=>'TFS.FILE_NAME',
                                    'FILE_DESCRIPTION'	=>'TFS.FILE_DESCRIPTION',
                                    'DOCUMENT_NUMBER'	=>'TFS.DOCUMENT_NUMBER',											
                                    'FILE_UPLOADED_TIME'=>'TFS.FILE_UPLOADED_TIME',
                            ))
					->where('TFS.FILE_ID = ?',$datapool['0']['N_UD_ID']);
					//echo $selectud;
					
				$dataud                   = $this->_db->fetchAll($selectud);
				$this->view->dataud = $dataud;
			}
		}
		
		 
		$select = $this->_db->select()
                            ->from(array('A' => 'T_NOTIONAL_DETAIL'),array('A.N_IS_SAVING','A.N_IS_CREDIT','A.ACCT_NAME','A.ACCT_DESC','A.N_SOURCE_ACCOUNT','A.ACCT_NAME','A.BALANCE'))
                            ->join(array('D' => 'T_NOTIONAL_POOLING'), 'D.N_NUMBER = A.N_NUMBER',array('D.N_CUST_ID','D.N_STATUS','N_NUMBER'))
                            ->join(array('B' => 'M_PRODUCT_TYPE'), 'B.PRODUCT_CODE = A.PRODUCT_TYPE AND A.PRODUCT_PLAN = B.PRODUCT_PLAN',array('B.PRODUCT_RATE','B.PRODUCT_TYPE'))
                            ->join(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = D.N_CUST_ID',array('C.CUST_NAME','C.CUST_ID','C.CUST_CIF'))
							->where('A.N_NUMBER = ?',$id);
                            
	//echo $select;die;
        $dataacct                   = $this->_db->fetchAll($select);
		$this->view->dataacct = $dataacct;
		$dataasset = array();
		$dataliabi = array();
		$totalasset = 0;
		$totallia = 0;
	//	var_dump( $clean);die;
		if(!empty($dataacct) && $cleanna){
			foreach($dataacct as $key => $val){
					if($val['BALANCE'] == 'N/A' || $val['BALANCE'] == ''){
						$systemBalance = new SystemBalance($datapool['0']['N_CUST_ID'],$val['N_SOURCE_ACCOUNT'],Application_Helper_General::getCurrNum('IDR'));
						$systemBalance->checkBalance();
						$balance = $systemBalance->getAvailableBalance();
							$updateArr['BALANCE']	=   $balance;
							$whereArr = array( 
									'N_SOURCE_ACCOUNT = ?' => (string)$val['N_SOURCE_ACCOUNT'],
									'N_NUMBER = ? ' => 	(string)$val['N_NUMBER']	
								);

							try{
								$balanceupdate = $this->_db->update('T_NOTIONAL_DETAIL',$updateArr,$whereArr);
							}catch (Exception $e) {
							   // print_r($e);die;
							}
					}
				
			}
			$select = $this->_db->select()
                            ->from(array('A' => 'T_NOTIONAL_DETAIL'),array('A.N_IS_SAVING','A.N_IS_CREDIT','A.ACCT_NAME','A.ACCT_DESC','A.N_SOURCE_ACCOUNT','A.ACCT_NAME','A.BALANCE'))
                            ->join(array('D' => 'T_NOTIONAL_POOLING'), 'D.N_NUMBER = A.N_NUMBER',array('D.N_CUST_ID','D.N_STATUS','N_NUMBER'))
                            ->join(array('B' => 'M_PRODUCT_TYPE'), 'B.PRODUCT_CODE = A.PRODUCT_TYPE AND A.PRODUCT_PLAN = B.PRODUCT_PLAN',array('B.PRODUCT_RATE','B.PRODUCT_TYPE'))
                            ->join(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = D.N_CUST_ID',array('C.CUST_NAME','C.CUST_ID','C.CUST_CIF'))
							->where('A.N_NUMBER = ?',$id);
			 $dataacct                   = $this->_db->fetchAll($select);
			 
                foreach($dataacct as $key => $val){
					if($val['BALANCE'] != 'N/A'){
						if($val['PRODUCT_TYPE'] == '1'){
							$dataacct[$key]['BALANCE'] = $val['BALANCE'];
							$dataasset[] = $dataacct[$key];
							$totalasset = $totalasset + $val['BALANCE'];
							
						}else{
							$dataacct[$key]['BALANCE'] = $val['BALANCE'];
						$dataliabi[] = $dataacct[$key];
							$totallia = $totallia + $val['BALANCE'];
						}
					}
				}				
	//echo $select;die;
       
			
			
		}else if(!empty($dataacct) && $clean){
			foreach($dataacct as $key => $val){
				
				$systemBalance = new SystemBalance($datapool['0']['N_CUST_ID'],(string)$val['N_SOURCE_ACCOUNT'],Application_Helper_General::getCurrNum('IDR'));
				$systemBalance->checkBalance();
				$balance = $systemBalance->getAvailableBalance();
					$updateArr['BALANCE']	=   $balance;
					$whereArr = array( 
	                        'N_SOURCE_ACCOUNT = ?' => (string)$val['N_SOURCE_ACCOUNT'],
							'N_NUMBER = ? ' => 	(string)$val['N_NUMBER']	
	                    );

	                try{
	                	$balanceupdate = $this->_db->update('T_NOTIONAL_DETAIL',$updateArr,$whereArr);
	                }catch (Exception $e) {
	                   // print_r($e);die;
					}
				//var_dump($balance);
				if($val['PRODUCT_TYPE'] == '1'){
					$dataacct[$key]['BALANCE'] = $val['BALANCE'];
						$dataasset[] = $dataacct[$key];
					$totalasset = $totalasset + $balance;
					
				}else{
					$dataacct[$key]['BALANCE'] = $val['BALANCE'];
						$dataliabi[] = $dataacct[$key];
					$totallia = $totallia + $balance;
				}
				
			}
		}else{
		//echo '<pre>';
			if(!empty($dataacct)){
			
			foreach($dataacct as $key => $val){
				
			//	$systemBalance = new SystemBalance($datapool['0']['N_CUST_ID'],$val['N_SOURCE_ACCOUNT'],Application_Helper_General::getCurrNum('IDR'));
			//	$systemBalance->checkBalance();
			//	$balance = $systemBalance->getAvailableBalance();
				//var_dump($balance);
				if($val['BALANCE'] != 'N/A'){
				//var_dump($val['PRODUCT_TYPE']);
					if($val['PRODUCT_TYPE'] == '1'){
						
						$dataacct[$key]['BALANCE'] = $val['BALANCE'];
						$dataasset[] = $dataacct[$key];
						
						$totalasset = $totalasset + $val['BALANCE'];
					//	var_dump($dataasset);
					
						
					}else{
						
						$dataacct[$key]['BALANCE'] = $val['BALANCE'];
						$dataliabi[] = $dataacct[$key];
						$totallia = $totallia + $val['BALANCE'];
						
					}
				}
			}
			
			}
			//die;
			//var_dump($dataasset);die;
			
		}
		
		$this->view->dataasset = $dataasset;
		$this->view->dataliabi = $dataliabi;
		//var_dump($totalasset);
		$this->view->totalasset = $totalasset;
		$this->view->totallia = $totallia;
		
		//echo '<pre>';
		//var_dump($dataasset);die;
		
		
    }
	
	
	
	 function balanceupdateAction(){
    	$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

		$id = (string)$this->_getParam('id');
		$cust = (string)$this->_getParam('cust');
		$ref = (string)$this->_getParam('ref');
		
		$systemBalance = new SystemBalance($cust,$id,Application_Helper_General::getCurrNum('IDR'));
		$systemBalance->checkBalance();
		$balance = $systemBalance->getAvailableBalance();
	//	var_dump($balance);
					$updateArr['BALANCE']	=   $balance;
					$whereArr = array( 
	                        'N_SOURCE_ACCOUNT = ?' => (string)$id,
							'N_NUMBER = ? ' => 	(string)$ref	
	                    );

	                try{
	                	$balanceupdate = $this->_db->update('T_NOTIONAL_DETAIL',$updateArr,$whereArr);
	                }catch (Exception $e) {
	                    print_r($e);die;
					}
					
					$updateArrnew['N_BALANCEUPDATE']	=   new Zend_Db_Expr("now()");
					$whereArrnew = array( 
	                        'N_CUST_ID = ?' => (string)$cust,
							'N_NUMBER = ? ' => 	(string)$ref
	                    );

	                try{
	                	$balanceupdate = $this->_db->update('T_NOTIONAL_POOLING',$updateArrnew,$whereArrnew);
	                }catch (Exception $e) {
	                    print_r($e);die;
					}
		
        
    	echo $balance;
				//	break;
	

    }
	
	
	function refreshchartAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
		$id = (string)$this->_getParam('id');
		$select = $this->_db->select()
                            ->from(array('A' => 'T_NOTIONAL_DETAIL'),array('A.N_IS_SAVING','A.N_IS_CREDIT','A.ACCT_NAME','A.ACCT_DESC','A.N_SOURCE_ACCOUNT','A.ACCT_NAME','A.BALANCE'))
                            ->join(array('D' => 'T_NOTIONAL_POOLING'), 'D.N_NUMBER = A.N_NUMBER',array('D.N_CUST_ID','D.N_STATUS','N_NUMBER'))
                            ->join(array('B' => 'M_PRODUCT_TYPE'), 'B.PRODUCT_CODE = A.PRODUCT_TYPE AND A.PRODUCT_PLAN = B.PRODUCT_PLAN',array('B.PRODUCT_RATE','B.PRODUCT_TYPE'))
                            ->join(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = D.N_CUST_ID',array('C.CUST_NAME','C.CUST_ID','C.CUST_CIF'))
							->where('A.N_NUMBER = ?',$id);
                            
	//echo $select;die;
        $dataacct                   = $this->_db->fetchAll($select);
        
		$totalasset = 0;
		$totallia = 0;
		foreach($dataacct as $key => $val){
					if($val['BALANCE'] != 'N/A'){
						if($val['PRODUCT_TYPE'] == '1'){
							$dataasset[] = $dataacct[$key];
							$dataasset[$key]['BALANCE'] = $val['BALANCE'];
							$totalasset = $totalasset + $val['BALANCE'];
							
						}else{
							$dataliabi[] = $dataacct[$key];
							$dataliabi[$key]['BALANCE'] = $val['BALANCE'];
							$totallia = $totallia + $val['BALANCE'];
						}
					}
				}
				
        echo $totalasset.','.$totallia;


	}
	
	public function lastupdatedAction(){

	
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

			$balance = array();
			$balance['lastupdate']	= date('Y-m-d H:i:s');
			
		
				if(!empty($balance)){
				// print_r($balance);die;
				$date=date_create($balance['lastupdate']);
				$dateupdate = date_format($date,"d-M-Y H:i:s");
				
					echo $dateupdate;
			
        
				}		
	}

}