<?php
/**
 * Tempwelcome model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempremittancechar extends Changemanagement_Model_Tempchanges {
	
	protected $_moduleId = 'MAC';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		//query from TEMP_SETTING
		$minamtData = $this->dbObj->fetchAll($this->dbObj->select()
						  	  ->from('TEMP_CHARGES_REMITTANCE')
						  	  ->where('CHANGES_ID = ?',$this->_changeId)
						  	  );
		
        if(!empty($minamtData)){
        	
        	
//         	$this->deleteMasterDaily();
//         	print_r($minamtData);die;
        	foreach ($minamtData as $minamt) {
			
	        	// $cekdata = $this->dbObj->fetchAll($this->dbObj->select()
	        	// 			->from('M_CHARGES_REMITTANCE')
	        	// 			->where('CHARGE_CCY = ?',$minamt['CHARGE_CCY'])
	        	// 			->where('CHARGE_AMOUNT_CCY = ?',$minamt['CHARGE_AMOUNT_CCY'])
	        	// 			->where('CHARGE_TYPE = ?',$minamt['CHARGE_TYPE'])
	        	// 	);
	        	$cekdata = $this->dbObj->fetchAll($this->dbObj->select()
	        				->from('M_CHARGES_REMITTANCE')
	        				->where('CHARGE_CCY = ?',$minamt['CHARGE_CCY'])
	        				//->where('CHARGE_AMOUNT_CCY = ?',$minamt['CHARGE_AMOUNT_CCY'])
	        				->where('CHARGE_TYPE = ?',$minamt['CHARGE_TYPE'])
	        				->where('CUST_ID = ?', $minamt['CUST_ID'])
	        		);
	// 				print_r($minamt);die;
	        	if(!empty($cekdata)){	
	        		//$whereArr = $this->dbObj->quoteInto('CHARGE_CCY = ? ',$minamt['CHARGE_CCY']);
	//         		print_r($whereArr);die;
	//         		('CHARGE_CCY = ?',$minamt['CHARGE_CCY'],
	//         				'CHARGE_AMOUNT_CCY'=>$minamt['CHARGE_AMOUNT_CCY'],
	//         				'CHARGE_TYPE'=>$minamt['CHARGE_TYPE']
	//         		);
	        		
	//         		$this->_db->delete('M_CHARGES_REMITTANCE',$whereArr);
	//         		array('CHARGE_CCY = ?',$minamt['CHARGE_CCY'],
	//         				'CHARGE_AMOUNT_CCY'=>$minamt['CHARGE_AMOUNT_CCY'],
	//         				'CHARGE_TYPE'=>$minamt['CHARGE_TYPE']
	//         		);
	        		
	        		$where = array(
	        				'CHARGE_CCY = ?' => $minamt['CHARGE_CCY'],
	        				//'CHARGE_AMOUNT_CCY = ?' => $minamt['CHARGE_AMOUNT_CCY'],
	        				'CHARGE_TYPE = ?' => $minamt['CHARGE_TYPE'],
	        				'CUST_ID = ?' => $minamt['CUST_ID']
	        		);
	//         		print_r($where);die;
	        		
	         		$this->dbObj->delete('M_CHARGES_REMITTANCE', $this->dbObj->quoteInto($where));
	//         		print_r($where);die;
	        		$insertArr = array_diff_key($minamt,array('TEMP_ID'=>'','CHANGES_ID'=>''));
	//         		print_r($minamt);die;
	        		$minamtinsert = $this->dbObj->insert('M_CHARGES_REMITTANCE',$insertArr);
	        	}else{
	        		$insertArr = array_diff_key($minamt,array('TEMP_ID'=>'','CHANGES_ID'=>''));
	//         		print_r($insertArr);die;
	        		$minamtinsert = $this->dbObj->insert('M_CHARGES_REMITTANCE',$insertArr);
	        		
	        	}
        	
        	
				if(!(boolean)$minamtinsert) {
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Min Amt)';
					return false;
				}
				
				
        	}
        	
        }
//         print_r($update);die;
		Application_Helper_General::writeLog('CRCA','Approve Minimum Amount Currency');
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
// 		$this->_redirect('/notification/success/index');
		return true;
		
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_CHARGES_REMITTANCE',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Min Amt)';
//			return false;
//		}
		
		return true;
	}

// 	protected function deleteMaster($data) {
// 		//delete from TEMP_MINAMT_CCY
// // 		$settingdelete = $this->dbObj->delete('M_CHARGES_REMITTANCE');
// 		$settingdelete = $this->dbObj->delete('M_CHARGES_REMITTANCE',
// 							$this->dbObj->quoteInto('CHARGE_CCY = ?',$data['CHARGE_CCY'])
				
// 				);
// 		return true;
// 	}

// 	protected function deleteMasterDaily() {
// 		//delete from TEMP_MINAMT_CCY
// 		$settingdelete = $this->dbObj->delete('M_DAILYLIMIT');
		
// 		return true;
// 	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Min Amt)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Min Amt)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Min Amt)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteDelete() {
		//will never be called
	}
	/* (non-PHPdoc)
	 * @see Changemanagement_Model_Tempchanges::approveActivate()
	 */
	public function approveActivate() {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see Changemanagement_Model_Tempchanges::approveDeactivate()
	 */
	public function approveDeactivate() {
		// TODO Auto-generated method stub
		
	}
	/* (non-PHPdoc)
	 * @see Changemanagement_Model_Tempchanges::approveEdit()
	 */
	public function approveEdit() {
		// TODO Auto-generated method stub
		
	}


}