<?php

require_once 'Zend/Controller/Action.php';

class stock_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');  
	
	    $filters = array('stock_code' => array('StringTrim', 'StripTags','StringToUpper'));
							 
		$validators =  array(
					    'stock_code' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_STOCK', 'field' => 'stock_code')),
							       	  'messages' => array(
							   						  $this->language->_('Cannot be empty'),
							   					      $this->language->_('Stock Code is not found'),
							   						     ) 
							             )
					        );
			// print_r($this->_request->getParams());die;
		if(array_key_exists('stock_code',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
				    $this->_db->beginTransaction();
				    
					$stockCode  = $zf_filter_input->getEscaped('stock_code'); 
					
					
					$this->_db->delete('M_STOCK','STOCK_CODE = '.$this->_db->quote($stockCode));
				   	
					
					$this->_db->commit();		
						 
					Application_Helper_General::writeLog('RBUD','Delete Stock. Stock Code : ['.$stockCode.']');
					$this->_redirect('/notification/success/index');
				    
						
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
					$this->_redirect('/'.$this->_request->getModuleName().'/index');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						Application_Helper_General::writeLog('RBUD','Update Stock');
						
						//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
						$this->_redirect('/'.$this->_request->getModuleName().'/index');
					}					
				}	
			} 
			
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('RBUD','Update Stock');
			
			//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
			$this->_redirect('/'.$this->_request->getModuleName().'/index');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
