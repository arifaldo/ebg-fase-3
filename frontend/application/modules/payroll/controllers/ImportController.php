<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/MultiPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';

class payroll_ImportController extends Application_Main
{
	protected $_moduleDB = 'RTF';	
	
	protected $_destinationUploadDir = '';
	protected $_maxRowMulti = '';
	
	public function initController(){
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 

		$setting = new Settings();
		$this->_maxRowMulti = $setting->getSetting('max_row_multi_transaction');
		$this->view->maxRowMulti = $this->_maxRowMulti;
	}

	public function indexAction()
	{
		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();
		$this->view->AccArr =  $AccArr;
		$listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');
		
		if($this->_request->isPost() )
		{
			$filter = new Application_Filtering();
			$confirm = false;
			$error_msg[0] = "";
			
			$METHOD 		= $this->_request->getParam('INPUT_METHOD') ? $this->_request->getParam('INPUT_METHOD') : "";
			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");
			
			if(!$METHOD)
			{
				$error_msg[0] = 'Error: Import Methods cannot be left blank. Please correct it.';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if($METHOD=="multi" && !$PS_SUBJECT)
			{
				$error_msg[0] = 'Error: Payment Subject cannot be left blank. Please correct it.';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if($METHOD=="multi" && !$PS_EFDATE)
			{
				$error_msg[0] = 'Error: Payment Date can not be left blank.';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if(!$ACCTSRC)
			{
				$error_msg[0] = 'Error: Source Account can not be left blank.';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else
			{
				$paramSettingID = array('range_futuredate', 'auto_release_payment');
				
				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only
				
				$adapter = new Zend_File_Transfer_Adapter_Http();
				
				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					'Error: Extension file must be *.csv'
				);
				
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);
				
				$adapter->setValidators ( array (			
					$extensionValidator, 
					$sizeValidator,
				));
				
				if ($adapter->isValid ()) 
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
					
					$adapter->addFilter ( 'Rename',$newFileName  );
						
					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end 
						
						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}
									
						if($totalRecords)
						{
							// jika Multi Transaction 
							if($METHOD=='multi')
							{			
								if($totalRecords <= $this->_maxRowMulti)
								{
									$rowNum = 0;
									$paramTrxArr = array();
									
									// MULTI CREDIT BY IMPORT-MULTI PAYMENT				
									$paramPayment = array( 	"CATEGORY"      	=> "PAYROLL",
															"FROM"       		=> "I",
															"PS_NUMBER"     	=> "",
															"PS_SUBJECT"   	 	=> $PS_SUBJECT,
															"PS_EFDATE"     	=> $PS_EFDATE,
															"_dateFormat"    	=> $this->_dateDisplayFormat,
															"_dateDBFormat"    	=> $this->_dateDBFormat,
															"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
															"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
															"_createPB"     	=> $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
															"_createDOM"    	=> false, // cannot create DOM trx
															"_createREM"    	=> false,        // cannot create REM trx 
														  ); 											  
									
									foreach ( $csvData as $row ) 
									{
										if(count($row)==5)
										{
											$rowNum++;
											$benefAcct 		= trim($row[0]);
											$benefName		= trim($row[1]);
											$benefAlias 	= trim($row[2]);
											$ccy 			= strtoupper(trim($row[3]));
											$amount 		= trim($row[4]);
											$message 		= trim($row[5]);
											$addMessage 	= trim($row[6]);
											$email 			= trim($row[7]);
											$type 			= trim($row[8]);
											$bankCode 		= trim($row[9]);
											$bankName 		= trim($row[10]);
											$benefAdd		= trim($row[11]);
											$citizenship	= strtoupper(trim($row[12]));

											$filter = new Application_Filtering();
											
											$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
											$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
											$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
											$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
											$ACBENEF_ALIAS 		= $filter->filter($benefAlias, "ACCOUNT_ALIAS");
											$ACBENEF_BANKNAME 	= $filter->filter($benefName, "ACCOUNT_NAME");
											$ACBENEF_ALIAS 		= $filter->filter($benefName, "ACCOUNT_ALIAS");
											$ACBENEF_EMAIL 		= $filter->filter($email, "EMAIL");
											$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
											$ACBENEF_ADDRESS	= $filter->filter($benefAdd, "ADDRESS");
											$ACBENEF_CITIZENSHIP= $filter->filter($citizenship, "SELECTION");
											//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
											$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
											//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
											$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
											$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");
												
											$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
											
											$ACBENEF_CCY = $this->_db->fetchOne(
														$this->_db->select()
																  ->from('M_CUSTOMER_ACCT',array('CCY_ID'))
																  ->where('ACCT_NO =?', $ACCTSRC)
																  ->where('CUST_ID =?', $this->_custIdLogin)
													);
												
											$filter->__destruct();
											unset($filter);
																 
											$paramTrx = array(	"TRANSFER_TYPE" 			=> 'PB',
																"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,	
																"TRA_MESSAGE" 				=> $TRA_MESSAGE,	
																"TRA_REFNO" 				=> $TRA_REFNO,
																"ACCTSRC" 					=> $ACCTSRC,	
																"ACBENEF" 					=> $ACBENEF,	
																"ACBENEF_CCY" 				=> $ACBENEF_CCY,	
																"ACBENEF_EMAIL" 			=> '',		
										
															// for Beneficiary data, except (bene CCY and email), must be passed by reference
																"ACBENEF_BANKNAME" 			=> '',	
																"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
																"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
																"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,	
															//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,	
															//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,	
																	
															//	"ORG_DIR" 					=> $ORG_DIR,
																"BANK_CODE" 				=> $CLR_CODE,
															//	"BANK_NAME" 				=> $BANK_NAME,
															//	"BANK_BRANCH" 				=> $BANK_BRANCH,
															//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
															//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
															//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
															 );	
											
											array_push($paramTrxArr,$paramTrx);
										}
										else
										{				
											$error_msg[0] = 'Error: Wrong File Format.';
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
											break;
										}		
									}
								
									// kalo gak ada error
									if(!$error_msg[0])
									{					
										$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
										$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr);
										
										$payment 	= $validate->getPaymentInfo();
									}
								}
								// kalo jumlah trx lebih dari setting
								else
								{				
									$error_msg[0] = 'Error: The number of rows to be imported should not more than '.$this->_maxRowMulti.'.';
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
								}	
							}
							// jika single transaction
							else if($METHOD=='single')
							{			
								if($totalRecords<=20)
								{
									// Zend_Debug::dump(count($csvData));die;
									$rowNum = 0;
									$paramMultiple = array();
									
									foreach ( $csvData as $row ) 
									{
										if(count($row)==7)
										{
											$rowNum++;
											$subject = trim($row[0]);
											$benefAcct = trim($row[1]);
											$benefAlias = trim($row[2]);
											$amount = trim($row[3]);
											$message = trim($row[4]);
											$addMessage = trim($row[5]);
											$paymentDate = trim($row[6]);

											$filter = new Application_Filtering();
											
											$PS_SUBJECT 		= $filter->filter($subject, "PS_SUBJECT");
											$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
											$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
											$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
											$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
											$ACBENEF_ALIAS 		= $filter->filter($benefAlias, "ACCOUNT_ALIAS");
											$PS_EFDATE 			= $filter->filter($paymentDate, "PS_DATE");
												
											$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
											
											$ACBENEF_CCY = $this->_db->fetchOne(
														$this->_db->select()
																  ->from('M_CUSTOMER_ACCT',array('CCY_ID'))
																  ->where('ACCT_NO =?', $ACCTSRC)
																  ->where('CUST_ID =?', $this->_custIdLogin)
													);
												
											$filter->__destruct();
											unset($filter);
											
											// MULTI CREDIT BY IMPORT-SINGLE PAYMENT
											$paramMultiple[$rowNum]["paramPayment"] = array("CATEGORY"      	=> "SINGLE PAYMENT",
																  "FROM"       		=> "I",
																  "PS_NUMBER"     	=> "",
																  "PS_SUBJECT"    	=> $PS_SUBJECT,
																  "PS_EFDATE"     	=> $PS_EFDATE,
																  "_dateFormat"    	=> $this->_dateUploadFormat,
																  "_dateDBFormat"   => $this->_dateDBFormat,
																  "_addBeneficiary" => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
																  "_beneLinkage"    => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
																  "_createPB"     	=> $this->view->hasPrivilege('CRIP'), // privi CRIP (Create Single Payment PB by Import File (CSV))
																  "_createDOM"    	=> false,  // cannot create DOM trx
																  "_createREM"    	=> false,  // cannot create REM trx
																 );	
																 
											$paramMultiple[$rowNum]["paramTrxArr"][0] = array(	"TRANSFER_TYPE" 			=> 'PB',
																"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,	
																"TRA_MESSAGE" 				=> $TRA_MESSAGE,	
																"TRA_REFNO" 				=> $TRA_REFNO,
																"ACCTSRC" 					=> $ACCTSRC,	
																"ACBENEF" 					=> $ACBENEF,	
																"ACBENEF_CCY" 				=> $ACBENEF_CCY,	
																"ACBENEF_EMAIL" 			=> '',		
										
															// for Beneficiary data, except (bene CCY and email), must be passed by reference
																"ACBENEF_BANKNAME" 			=> '',	
																"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
																"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
																"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,	
															//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,	
															//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,	
																	
															//	"ORG_DIR" 					=> $ORG_DIR,
																"BANK_CODE" 				=> $CLR_CODE,
															//	"BANK_NAME" 				=> $BANK_NAME,
															//	"BANK_BRANCH" 				=> $BANK_BRANCH,
															//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
															//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
															//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
															 );	
										}	
										else
										{				
											$error_msg[0] = 'Error: Wrong File Format.';
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
											break;
										}	
									}
									 // Zend_Debug::dump($paramMultiple);die;
								
									// kalo gak ada error
									if(!$error_msg[0])
									{								
										$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
										
										$validate->breakOnError = false;
										$resultVal	= $validate->checkCreate($paramMultiple);
										
										
										$payment 	= $validate->getPaymentInfo();
									}
								}
								// kalo jumlah trx lebih dari 20
								else
								{				
									$error_msg[0] = 'Error: The number of rows to be imported should not more than 20.';
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
								}	
							}
		
							
							// kalo gak ada error
							if(!$error_msg[0])
							{		
								if($validate->isError() === false)	// payment data is valid
								{
									$confirm = true;
								
									$validate->__destruct();
									unset($validate);
								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									
									if($METHOD=='multi')
									{
										$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
									}
									else if($METHOD=='single')
									{
										$errorTrxMsg 	= $validate->getErrorColMsg();;	// array
									}
									
									$validate->__destruct();
									unset($validate);
									
									if($errorMsg)
									{
										$error_msg[0] = 'Error: '.$errorMsg;
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else //kalo total record = 0
						{
							$error_msg[0] = 'Error: Wrong File Format. There is no data on csv File.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}	
					}
				} 
				else 
				{		
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = 'Error: File cannot be left blank. Please correct it.';
						else
							$error_msg[0] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
			}
			
			if($confirm)
			{
				if($METHOD=='multi')
				{
					$content['paramPayment'] = $paramPayment;
					$content['paramTrxArr'] = $paramTrxArr;
				}
				else if($METHOD=='single')
				{
					$content['paramMultipleTrx'] = $paramMultiple;
				}
				
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				$content['method'] = $METHOD;
				
				$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
				$sessionNamespace->content = $content;
				$this->_redirect('/payroll/import/confirm');
			}
			
			if($METHOD=='multi')
				$this->view->MULTI = 'checked="checked"'; 
			else if($METHOD=='single')
				$this->view->SINGLE = 'checked="checked"'; 
			$this->view->PSSUBJECT = $PS_SUBJECT; 
			$this->view->ACCTSRC = $ACCTSRC;
			$this->view->PSEFDATE = $PS_EFDATE;
		}
	}
	
	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
		$data = $sessionNamespace->content;
		// Zend_Debug::dump($data);die;
		
		if($data['method']=='multi')
		{
			$sourceAcct = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']];
			
			foreach($data['paramTrxArr'] as $key=>$row)
			{
				$data['paramTrxArr'][$key]['REMARK'] = isset($data['errorTrxMsg']['PB'][$data['paramTrxArr'][0]['ACBENEF_CCY']][$key]) ? $data['errorTrxMsg']['PB'][$data['paramTrxArr'][0]['ACBENEF_CCY']][$key] : '';
			}
			
			$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
			$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
			$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'].' / '.$sourceAcct['ACCT_NAME'].' / '.$sourceAcct['CCY_ID'];
			if($sourceAcct['ACCT_ALIAS']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS'];
			$this->view->trxArr = $data['paramTrxArr'];	
		}
		else if($data['method']=='single')
		{
			$sourceAcct = $this->_db->fetchRow(
							$this->_db->select()
									  ->from('M_CUSTOMER_ACCT',array('ACCT_NO','ACCT_NAME','ACCT_ALIAS_NAME','CCY_ID'))
									  ->where('ACCT_NO =?', $data['paramMultipleTrx'][1]['paramTrxArr'][0]['ACCTSRC'])
									  ->where('CUST_ID =?', $this->_custIdLogin)
						);
			
			$this->view->ACCTSRC = $sourceAcct['ACCT_NO'].' / '.$sourceAcct['ACCT_NAME'].' / '.$sourceAcct['CCY_ID'];
			if($sourceAcct['ACCT_ALIAS_NAME']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS_NAME'];
			$this->view->trxArr = $data['paramMultipleTrx'];	
			$this->view->errorArr = $data['errorTrxMsg'];	
		}
			
		$this->view->METHOD = $data['method'];
		$this->view->CCY = $sourceAcct['CCY_ID'];
		
		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}
		
		$this->view->totalFailed = $totalFailed;
		$this->view->totalRecord = $data['payment']['countTrxTOTAL'];	
		$this->view->totalAmount = $data['payment']['sumTOTAL'];	
		
		if($this->_request->isPost() )
		{			
			if($this->_getParam('submit') == 'Cancel')
			{
				unset($_SESSION['confirmImportCredit']); 
				$this->_redirect('/payroll/import');
			}
			
			
			if($data['method']=='multi')
			{
				$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
				$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
				$param['PS_TYPE'] 	= $this->_paymenttype['code']['payroll'];
				$param['PS_CCY']  = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];
				
				$param['TRANSACTION_DATA'] = array();
				foreach($data['paramTrxArr'] as $row)
				{
					$param['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
							'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
							'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
							'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'],
							'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'],
							'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
							'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
							'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
							'CLR_CODE' 					=> $row['BANK_CODE'],
							'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'],
							'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
							'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
							'TRA_REFNO' 				=> $row['TRA_REFNO'],
					);
				}
			
				$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
				$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
				$param['_priviCreate'] = 'CBPW';
			
				// Zend_Debug::dump($param);die;
				
				$MultiPayment = new MultiPayment("", $this->_custIdLogin, $this->_userIdLogin);
				
				$result = $MultiPayment->createPayment($param);
			}
			else if($data['method']=='single')
			{
				foreach($data['paramMultipleTrx'] as $row)
				{
					$PS_EFDATE = substr_replace(Application_Helper_General::convertDate($row['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat), '2', 0, 1);
										
					$param = array(
							'PS_SUBJECT'				=> $row['paramPayment']['PS_SUBJECT'],
							'PS_EFDATE'		  			=> $PS_EFDATE,
							'PS_TYPE' 					=> $this->_paymenttype['code']['payroll'],
							'PS_CCY'  					=> $row['paramTrxArr'][0]['ACBENEF_CCY'],
							'SOURCE_ACCOUNT' 			=> $row['paramTrxArr'][0]['ACCTSRC'],
							'BENEFICIARY_ACCOUNT' 		=> $row['paramTrxArr'][0]['ACBENEF'],
							'BENEFICIARY_ACCOUNT_CCY' 	=> $row['paramTrxArr'][0]['ACBENEF_CCY'],
							'BENEFICIARY_ACCOUNT_NAME' 	=> $row['paramTrxArr'][0]['ACBENEF_BANKNAME'],
							'BENEFICIARY_ALIAS_NAME' 	=> $row['paramTrxArr'][0]['ACBENEF_ALIAS'],
							'BENEFICIARY_EMAIL' 		=> $row['paramTrxArr'][0]['ACBENEF_EMAIL'],
							'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
							'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
							'CLR_CODE' 					=> $row['BANK_CODE'],
							'TRANSFER_TYPE' 			=> $row['paramTrxArr'][0]['TRANSFER_TYPE'],
							'TRA_AMOUNT' 				=> $row['paramTrxArr'][0]['TRA_AMOUNT'],
							'TRA_MESSAGE' 				=> $row['paramTrxArr'][0]['TRA_MESSAGE'],
							'TRA_REFNO' 				=> $row['paramTrxArr'][0]['TRA_REFNO'],
					);
				
					$param['_addBeneficiary'] = $row['paramPayment']['_addBeneficiary'];
					$param['_beneLinkage'] = $row['paramPayment']['_beneLinkage'];
					$param['_priviCreate'] = 'CRIP';
				
					// Zend_Debug::dump($param);die;
				
					$SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);
					
					$result = $SinglePayment->createPayment($param);
					if(!$result) break;
				}
			}
			
			if($result)
			{
				unset($_SESSION['confirmImportCredit']); 
				$this->_redirect('/notification/success');
			}
			else
			{	
				$this->view->error 		= true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/payroll/import');
			}
		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;		
	}
}
