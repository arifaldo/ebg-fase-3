<?php
require_once(realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Account.php';
require_once 'General/Settings.php';

$setting = new Settings();
$starttime = $setting->getSetting('start_running_balance');
$endtime = $setting->getSetting('cut_off_running_balance');
$begin = new DateTime($starttime);
$end = new DateTime($endtime);
$now = new DateTime();

$db = Zend_Db_Table::getDefaultAdapter();
$actionid = 'scheduler';
$moduleid = 'reject payment';
$interval = 2;
$truecounter = 0;
$falsecounter = 0;


$select	= $db->select()
	->distinct()
	->from(
		['a' => 'T_BANK_GUARANTEE'],
		[
			'a.BG_CG_DEADLINE',
			'a.BG_REG_NUMBER',
			'a.BG_STATUS',
			'a.CUST_ID'
		]
	)
	->where("DATE(NOW()) > DATE(a.BG_CG_DEADLINE)")
	->where('a.BG_STATUS = ?', 15)
	->where("a.CGINS_STATUS = ?", '0');

$dataCM = $db->fetchAll($select);

// CEK UNTUK FREEZE
if ($dataCM) {

	$saveKontra = [];

	foreach ($dataCM as $row) {

		$select	= $db->select()
			->from(
				['a' => 'T_BANK_GUARANTEE_DETAIL'],
				[
					'a.PS_FIELDVALUE'
				]
			)
			->where("a.PS_FIELDNAME = ?", "Insurance Name")
			->where("a.BG_REG_NUMBER = ?", $row['BG_REG_NUMBER'])
			->where("a.CUST_ID = ?", $row['CUST_ID']);

		$dataInsurance = $db->fetchAll($select);

		if (!array_key_exists($dataInsurance[0]['PS_FIELDVALUE'], $saveKontra)) $saveKontra[$dataInsurance[0]['PS_FIELDVALUE']] = 0;

		$saveKontra[$dataInsurance[0]['PS_FIELDVALUE']] += 1;
	}

	foreach ($saveKontra as $custId => $countBg) {
		$selectLineFacility = $db->select()
			->from('M_CUST_LINEFACILITY')
			->where('CUST_ID = ?', $custId)
			->query()->fetch();

		if (!in_array($selectLineFacility['STATUS'], [1, 4])) continue;

		$where = [
			'CUST_ID = ?' => $custId
		];
		$updateData = [
			'STATUS' => 4,
			'MONITOR_KONTRA' => 1,
			'MINUS_KONTRA_COUNT' => $countBg
		];
		$db->update('M_CUST_LINEFACILITY', $updateData, $where);
	}
}

// CEK UNTUK UNFREEZE
$selectAllAsuransiLf = $db->select()
	->from('M_CUST_LINEFACILITY')
	->where('CUST_SEGMENT = ?', 4)
	->where('STATUS = ?', 4)
	// ->where('FREEZE_MANUAL IS NULL OR FREEZE_MANUAL != ?', 1)
	->query()->fetchAll();

if ($selectAllAsuransiLf) {
	foreach ($selectAllAsuransiLf as $row) {

		$selectAllBgWithThisIns = $db->select()
			->from('T_BANK_GUARANTEE')
			->where("DATE(NOW()) > DATE(BG_CG_DEADLINE)")
			->where('BG_STATUS = ?', 15)
			->where("CGINS_STATUS = ?", '0')
			->where('BG_INSURANCE_CODE = ?', $row['CUST_ID'])
			->query()->fetch();

		if ($selectAllBgWithThisIns) continue;

		$status = ($row['FREEZE_MANUAL'] != 1 && $row['MONITOR_MD'] != 1) ? 1 : $row['STATUS'];

		$updateData = [
			'MONITOR_KONTRA' => null,
			'MINUS_KONTRA_COUNT' => null,
			'STATUS' => $status
		];

		$where = [
			'CUST_ID = ?' => $row['CUST_ID']
		];

		$db->update('M_CUST_LINEFACILITY', $updateData, $where);
	}
}
