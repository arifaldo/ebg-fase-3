<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';
class reconadapter_IndexController extends Application_Main {


	
	public function indexAction()
    { 
    	$this->_helper->_layout->setLayout('newlayout');

        //h = header || d = detail
    	// $select = $this->_db->select()
    	// 					->from(array('R' => 'M_RECON_ADAPTER'),array('*'))
     //                        ->join(array('B' => 'M_BANK_TABLE'), 'R.BANK_CODE = B.BANK_CODE', array('B.BANK_NAME'));

        $select = $this->_db->select()
                            ->from(array('R' => 'M_RECON_ADAPTER'),array('*'));

    	$profileData = $this->_db->fetchAll($select);

        $this->view->profileData = $profileData;

        $delete     = $this->_getParam('delete');

        if($delete)
        {
            $profileid = $this->_request->getParam('profileid');

            $validators = array (
                                    'profileid' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'profileid' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            foreach ($profileid as $key => $value) {
                if(!empty($value)){
                    $success = true;
                }
            }


            if($zf_filter_input->isValid() && $success)
            {

                try
                {
                    foreach ($profileid as $key => $value)
                    {

                        $id = $value;

                        //unlink files
                        $select = $this->_db->select()
                                    ->from(array('H' => 'M_RECON_ADAPTER'),array('*'));

                        $select->where('H.ADAPTER_ID = '. $this->_db->quote($id));

                        $profileData = $this->_db->fetchAll($select);

                        $statement_file_path = $profileData[0]['STATEMENT_FILE_PATH'];
                        $report_file_path = $profileData[0]['REPORT_FILE_PATH'];
                        @unlink($statement_file_path);
                        @unlink($report_file_path);

                        $this->_db->beginTransaction();

                        $where['ADAPTER_ID = ?'] = $id;
                        $result =  $this->_db->delete('M_RECON_ADAPTER',$where);
                        $wheredelete['ADAPTER_ID = ?'] = $id;
                        $result = $this->_db->delete('M_RECON_ADAPTER_DETAIL',$wheredelete);

                        Application_Helper_General::writeLog('RPAD','Delete Reconciliation Adapter List. Profile Name: '.$profileData[0]['ADAPTER_NAME']);

                        $this->_db->commit();
                    }
                }
                catch(Exception $e)
                {
                    $this->_db->rollBack();
                }
                $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                $error          = true;
                $errors         = $zf_filter_input->getMessages();
                $profileiderr   = (isset($errors['profileiderr']))? $errors['profileiderr'] : null;
            }

        }
        else{
            Application_Helper_General::writeLog('RPLS','View Reconciliation Adapter List');
        }

       
    }
    
}
