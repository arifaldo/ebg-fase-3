<?php

require_once 'Zend/Controller/Action.php';

class Companycharges_MonthlyfeenewController extends Application_Main
{

    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');



		

		$cust_id = 'GLOBAL';
		$this->view->custid = $cust_id;

		
 		$cust_name = $this->_custNameLogin;

 			$select4 = $this->_db->select()->distinct()
				        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($cust_id));
			$select4 -> where("MONTHLYFEE_TYPE = '4'");
			$result4 = $select4->query()->FetchAll();

			$str = 'charges_monthly_';
			foreach ($result4 as $key => $value) {

				$this->view->{$str.strtolower($value['CCY'])} = Application_Helper_General::displayMoneyplain($value['AMOUNT']);
			}


			$charges_monthly_aud = $this->_getParam('charges_monthly_aud');
			$charges_monthly_aud_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_aud);
			$this->_setParam('charges_monthly_aud',$charges_monthly_aud_val);

			$charges_monthly_eur = $this->_getParam('charges_monthly_eur');
			$charges_monthly_eur_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_eur);
			$this->_setParam('charges_monthly_eur',$charges_monthly_eur_val);

			$charges_monthly_idr = $this->_getParam('charges_monthly_idr');
			$charges_monthly_idr_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_idr);
			$this->_setParam('charges_monthly_idr',$charges_monthly_idr_val);

			$charges_monthly_sgd = $this->_getParam('charges_monthly_sgd');
			$charges_monthly_sgd_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_sgd);
			$this->_setParam('charges_monthly_sgd',$charges_monthly_sgd_val);

			$charges_monthly_usd = $this->_getParam('charges_monthly_usd');
			$charges_monthly_usd_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_usd);
			$this->_setParam('charges_monthly_usd',$charges_monthly_usd_val);

			


		// var_dump($cust_name);exit();
		// $select = $this->_db->select()
		// 			->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
		// 			->where('MODULE_ID = ?' , 'GNS')
		// 			->query()->FetchAll();
		// $setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		// foreach($setting as $key=>$value)
		// {
		// 	if(!empty($key))
		// 		$this->view->$key = $value;
		// }

			//FILTER
			{
			$filters = array(
							// 'isenabled' => array('StringTrim','StripTags'),
							// 'disable_note' => array('StringTrim','StripTags'),
							// 'note' => array('StringTrim','StripTags'),

							
            			    
							);
			}

			//VALIDATE
			{
			$validators = array(
									// 'isenabled' => array(	'NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),

									// 'disable_note' => array(	'NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),
									// 'note' => array('NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),
									
									
															);
			}

				$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				$arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');

				

		if($this->_request->isPost()){

			$params = $this->getRequest()->getParams();
			$ccy = $params['ccy'];

			if($zf_filter->isValid() )
			{
				
				
				$this->_db->beginTransaction();
					//die;
					try
					{
				    //Zend_Debug::dump($resultunion); die;
					$info = 'Global Charges';
					$info2 = $this->language->_('Set Charges Monthly per Account');
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_MONTHLY','TEMP_CHARGES_MONTHLY',$cust_id,'Monthly Charges',$cust_id,null,'monthlyaccount');
					// print_r($resultunion);die;
					$resultunion= array('idr','usd','eur','aud','sgd');
					foreach($resultunion as $row)
					{
						
							$idamt = 'charges_monthly_'.$row;
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
							
							if(!empty($amt)){
								$data= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $cust_id,
										'ACCT_NO' 			=> '-',
										'TRA_FROM' 			=> NULL,
										'TRA_TO' 			=> NULL,
										'AMOUNT' 			=> $amt,
										'REDEBATE' 			=> NULL,
										// redebate
										'MONTHLYFEE_TYPE' 	=> '3',
										'CHARGES_ACCT_NO'	=> '-',
										'CCY'				=> strtoupper($row),
										'SUGGEST_DATE'		=> new Zend_Db_Expr('now()'),
										'SUGGEST_BY'		=> $this->_userIdLogin
									);
								$this->_db->insert('TEMP_CHARGES_MONTHLY',$data);										
							}
								//echo $row['ACCT_NO'].' aaa<br />';
								//Zend_Debug::dump($data);
							
									
					}
					
					
					
					//if($error == 0)
				//	{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/charges/chargesglobal');
						$this->_redirect('/notification/submited/index');
				//	}
				}
				
				catch(Exception $e)
				{
					//die;
					var_dump($e);die;
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
				// $this->setbackURL('/companycharges/monthlyfee/index/custid/GLOBAL');
				//$this->_redirect('/notification/submited/index');
				// die('here1');
			}
			else{
				
				$docErr = 'Error in processing form values. Please correct values and re-submit.';
				$this->view->report_msg = $docErr;
				// die('here');
				foreach($zf_filter->getMessages() as $key=>$err){
				$xxx = 'x'.$key;
				// print_r($zf_filter->getMessages());die;
				$this->view->$xxx = $this->displayError($err);
				} 
			}
			// var_dump($ccy); exit();

			foreach($setting as $key=>$value)
			{
				if (in_array($key, $arrfck)){
					$this->view->$key =   $this->_getParam($key);
				}
				else{
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
				}
			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		}
		else{
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			Application_Helper_General::writeLog('STLS','View Frontend Global Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 200 - $disable_note_len;
		$note_len 	 = 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;


	}
}
