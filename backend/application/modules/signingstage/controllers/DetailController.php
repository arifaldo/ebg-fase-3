<?php

use function PHPSTORM_META\type;

require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class Signingstage_detailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');

        if (!$this->view->hasPrivilege("SBGP")) {
            return $this->_redirect("/home/dashboard");
        }

        $flashMessanger = $this->_helper->getHelper('FlashMessenger');

        if (count($flashMessanger->getMessages()) > 0) {
            if ($flashMessanger->getMessages()[0]["error"]) {
                $this->view->error_message = $flashMessanger->getMessages()[0]["error"];
            } else {
                $this->view->error_message = $flashMessanger->getMessages();
            }
        }

        $stamp_fee = $settings->getSetting('stamp_fee');
        $this->view->stamp_fee = $stamp_fee;
        $adm_fee = $settings->getSetting('adm_fee');
        $this->view->adm_fee = $adm_fee;

        $this->view->systemType = $system_type;
        $this->view->ProvFee = 2000000;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;


        // $numb = $this->_getParam('bgnumb');

        // decrypt numb
        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token     = $rand;
        $this->view->token = $sessionNamespace->token;

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        // others attachment

        $checkOthersAttachment = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
            ->where("BG_REG_NUMBER = '$BG_NUMBER'")
            ->order('A.INDEX ASC')
            ->query()->fetchAll();

        if (count($checkOthersAttachment) > 0) {
            $this->view->othersAttachment = $checkOthersAttachment;
        }

        // end others attachment

        if (!empty($BG_NUMBER)) {

            $verifyData = $this->_db->select()
                ->from(array('A' => 'TEMP_BGVERIFY_DETAIL'), array('*'))
                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();
            //var_dump($verifyData);die;
            if (!empty($verifyData)) {
                foreach ($verifyData as $vl) {
                    //if()
                    $label = 'check' . $vl['INDEX'];
                    //var_dump($label);
                    $this->view->$label = true;
                    $labeldate = 'verify' . $vl['INDEX'];
                    $verifydate = Application_Helper_General::convertDate($vl['VERIFIED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                    $this->view->$labeldate = $verifydate . ' by ' . $vl['VERIFIEDBY'];
                }
            }

            $bgdata = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                ->joinLeft(["CUSTOMER" => "M_CUSTOMER"], "A.CUST_ID = CUSTOMER.CUST_ID", [
                    "CUSTOMER_CIF" => "CUSTOMER.CUST_CIF",
                    "CUSTOMER_PHONE" => "CUSTOMER.CUST_PHONE",
                    "APPLICANT_NAME" => "CUSTOMER.CUST_NAME",
                    'GRUP_BUMN'
                ])
                ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.SP_OBLIGEE_CODE = B.CUST_ID', array('B.CUST_NAME'))
                ->joinLeft(["MBRANCH" => "M_BRANCH"], "A.BG_BRANCH = MBRANCH.BRANCH_CODE", ["BANK_ADDRESS", "LOCAL_PN"])

                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->where('A.BG_STATUS IN (?)', [14, 17])
                ->query()->fetchAll();

            if ($bgdata[0]['BG_OLD']) {
                $bgOld = $this->_db->select()
                    ->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
                    ->where('BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
                    ->query()->fetch();

                $this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
                $this->view->prevProv = $bgOld['PROVISION_FEE'];
            }

            Application_Helper_General::writeLog('SBGP', "View Signing Stage Detail. Register Number: " . $bgdata[0]["BG_REG_NUMBER"] . " Applicant : " . $bgdata[0]["APPLICANT_NAME"] . " (" . $bgdata[0]["CUST_ID"] . "))");

            if (!empty($bgdata[0]['BG_OLD'])) {
                $tbgdata = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    ->joinLeft(["CUSTOMER" => "M_CUSTOMER"], "A.CUST_ID = CUSTOMER.CUST_ID", [
                        "CUSTOMER_CIF" => "CUSTOMER.CUST_CIF",
                        "CUSTOMER_PHONE" => "CUSTOMER.CUST_PHONE",
                        "APPLICANT_NAME" => "CUSTOMER.CUST_NAME",
                        'GRUP_BUMN'
                    ])
                    ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.SP_OBLIGEE_CODE = B.CUST_ID', array('B.CUST_NAME'))
                    ->joinLeft(["MBRANCH" => "M_BRANCH"], "A.BG_BRANCH = MBRANCH.BRANCH_CODE", ["BANK_ADDRESS", "LOCAL_PN"])
                    ->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
                    ->query()->fetch();
            }

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))

                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();

            if (!empty($tbgdata['BG_REG_NUMBER'])) {
                $tbgdatadetail = $this->_db->select()
                    ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
                    ->query()->fetchAll();
            }
            //$datas = $this->_request->getParams();
            //echo '<pre>';
            //var_dump($bgdata);
            //die;

            $getGuarantedTransanctions = $this->_db->select()
                ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
                ->where('BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();

            $this->view->guarantedTransanctions = $getGuarantedTransanctions;

            if (!empty($tbgdata['BG_REG_NUMBER'])) {
                $getGuarantedTransanctionsT = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_UNDERLYING')
                    ->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
                    ->query()->fetchAll();

                $this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;
            }

            if (!empty($tbgdata['BG_REG_NUMBER'])) {
                $checkOthersAttachmentT = $this->_db->select()
                    ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
                    ->where("BG_REG_NUMBER = ?", $tbgdata['BG_REG_NUMBER'])
                    ->order('A.INDEX ASC')
                    ->query()->fetchAll();

                $this->view->othersAttachmentT = $checkOthersAttachmentT;
            }


            if (!empty($bgdata)) {

                $check_error = false;

                $get_cash_collateral = $this->_db->select()
                    ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
                    ->where("CUST_ID = ?", "GLOBAL")
                    ->where("CHARGES_TYPE = ?", "10")
                    ->query()->fetchAll();

                $this->view->cash_collateral = $get_cash_collateral[0];

                $this->view->data = $bgdata[0];
                $data = $bgdata[0];

                if (!empty($data["TTD1_USERID"])) {
                    $this->view->sign_wait = true;
                    $get_ttd_detail1 = $this->_db->select()
                        ->from("M_BUSER")
                        // ->where("BUSER_ID = '" . $data["TTD1_USERID"] . "' OR BUSER_ID = '" . $data["TTD2_USERID"] . "'")
                        ->where("BUSER_ID IN (?)", [$data["TTD1_USERID"]])
                        ->query()->fetch();

                    $get_ttd_detail2 = $this->_db->select()
                        ->from("M_BUSER")
                        // ->where("BUSER_ID = '" . $data["TTD1_USERID"] . "' OR BUSER_ID = '" . $data["TTD2_USERID"] . "'")
                        ->where("BUSER_ID IN (?)", [$data["TTD2_USERID"]])
                        ->query()->fetch();

                    $this->view->ttd_detail = [$get_ttd_detail1, $get_ttd_detail2];
                }

                if ($data["BG_STATUS"] == 17) {
                    $get_signer = $this->_db->select()
                        ->from("T_BANK_GUARANTEE_SIGNER")
                        ->where("DOCUMENT_ID = ?", $data["DOCUMENT_ID"])
                        ->query()->fetchAll();

                    $ttd1_email = $get_ttd_detail[array_search($data["TTD1_USERID"], array_column($get_ttd_detail, "BUSER_ID"))]["BUSER_EMAIL"];
                    $ttd2_email = $get_ttd_detail[array_search($data["TTD2_USERID"], array_column($get_ttd_detail, "BUSER_ID"))]["BUSER_EMAIL"];

                    $ttd1_detail = $get_signer[array_search($ttd1_email, array_column($get_signer, "SIGNER_EMAIL"))];
                    $ttd2_detail = (array_search($ttd2_email, array_column($get_signer, "SIGNER_EMAIL")) !== false) ? $get_signer[array_search($ttd2_email, array_column($get_signer, "SIGNER_EMAIL"))] : "";

                    $this->view->ttd1_detail = $ttd1_detail;
                    $this->view->ttd2_detail = $ttd2_detail;
                }

                $principleData = [];
                // if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
                foreach ($bgdatadetail as $key => $value) {
                    $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
                }

                $this->view->principleData = $principleData;
                // }

                if (!empty($principleData["SLIK OJK Document"])) {
                    $temp_slikojk = explode("_", $principleData["SLIK OJK Document"]);
                    $new_temp_name = $temp_slikojk[0] . "_" . $temp_slikojk[1] . "_" . $temp_slikojk[2] . "_" . $temp_slikojk[3] . "_";

                    $this->view->slikojk = str_replace($new_temp_name, "", $principleData["SLIK OJK Document"]);
                }

                if (!empty($data["KUASA_DIREKSI_FILE"])) {
                    $temp_suratpermohonan = explode("_", $data["KUASA_DIREKSI_FILE"]);
                    $new_temp_name = $temp_suratpermohonan[0] . "_" . $temp_suratpermohonan[1] . "_" . $temp_suratpermohonan[2] . "_" . $temp_suratpermohonan[3] . "_";

                    $this->view->suratpermohonan = str_replace($new_temp_name, "", $data["KUASA_DIREKSI_FILE"]);
                }

                if (!empty($data["AGREE_FORMAT"])) {
                    $temp_agreeformat = explode("_", $data["AGREE_FORMAT"]);
                    $new_temp_name = $temp_agreeformat[0] . "_" . $temp_agreeformat[1] . "_" . $temp_agreeformat[2] . "_" . $temp_agreeformat[3] . "_";

                    $this->view->agreeformat = str_replace($new_temp_name, "", $data["AGREE_FORMAT"]);
                }

                if (!empty($data["MEMO_LEGAL"])) {
                    $temp_memolegal = explode("_", $data["MEMO_LEGAL"]);
                    $new_temp_name = $temp_memolegal[0] . "_" . $temp_memolegal[1] . "_" . $temp_memolegal[2] . "_" . $temp_memolegal[3] . "_";

                    $this->view->memolegal = str_replace($new_temp_name, "", $data["MEMO_LEGAL"]);
                }

                if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {
                    $this->view->isinsurance = true;
                }

                switch ($bgdata[0]["CHANGE_TYPE"]) {
                    case '0':
                        $this->view->suggestionType = "New";
                        break;
                    case '1':
                        $this->view->suggestionType = "Amendment Changes";
                        break;
                    case '2':
                        $this->view->suggestionType = "Amendment Draft";
                        break;
                }

                $selectcomp = $this->_db->select()
                    ->from(array('A' => 'M_CUSTOMER'), array('*'))
                    //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                    ->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
                    ->where('A.CUST_ID =' . $this->_db->quote((string)$bgdata['0']['CUST_ID']))
                    ->query()->fetchAll();

                $this->view->compinfo = $selectcomp[0];

                $getMinCharge = $this->_db->select()
                    ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                    ->where("CHARGES_ID = ?", "MINPROV01")
                    ->query()->fetch();


                // cek kontra untuk rebate
                $kontraRebate = ['IS_TRANSFER_COA'];

                // kontra garansi full cover ------------------------------------------------

                // start validasi 
                if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {

                    $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
                    $paramProvisionFee['TIME_PERIOD_START'] = $data['TIME_PERIOD_START'];
                    $paramProvisionFee['TIME_PERIOD_END'] = $data['TIME_PERIOD_END'];
                    $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
                    $percentage = $getProvisionFee['provisionFee'];
                    $data["ADM_FEE"] = $getProvisionFee['adminFee'];
                    $data["STAMP_FEE"] = $getProvisionFee['stampFee'];

                    $date_start = date_create($data["TIME_PERIOD_START"]);
                    $date_end = date_create($data["TIME_PERIOD_END"]);
                    $tenor = intval(date_diff($date_start, $date_end)->format('%a')) + 1;

                    if ($tenor <= 90) {
                        // $percentage = 0.25;
                        $prorata = $tenor / 90;
                    } else if ($tenor <= 180) {
                        // $percentage = 0.50;
                        $prorata = $tenor / 180;
                    } else if ($tenor <= 270) {
                        // $percentage = 0.75;
                        $prorata = $tenor / 270;
                    } else {
                        // $percentage = 0.75;
                        $prorata = $tenor / 360;
                    }

                    // rebate ------------------------------------------------------------

                    if ($data["REDEBATE_STATUS"] == "0") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();

                        if (count($cekrebate) > 0) {



                            if ($cekrebate[0]["FLAG"] == "1") {
                                $this->view->statusrebate = 1;
                                $this->view->REBATE_REVIEWEDBY = $cekrebate[0]["REBATE_REVIEWEDBY"];

                                $this->view->REBATE_REVIEWED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REVIEWED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                            } else {
                                $this->view->statusrebate = 4;
                                $this->view->REBATE_SUGGESTEDBY = $cekrebate[0]["REBATE_SUGGESTEDBY"];
                                $this->view->REBATE_SUGGESTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_SUGGESTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                            }
                        } else {

                            $this->view->statusrebate = 0;
                        }
                    } elseif ($data["REDEBATE_STATUS"] == "2") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();
                        $this->view->statusrebate = 2;

                        $this->view->REBATE_REJECTEDBY = $cekrebate[0]["REBATE_REJECTEDBY"];

                        $this->view->REBATE_REJECTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REJECTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                    } else {
                        $this->view->statusrebate = 3;
                    }

                    if ($data["REDEBATE_STATUS"] == "1") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();
                        $this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

                        $this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

                        $percentage = floatval($data['REDEBATE_AMOUNT']);
                    }

                    // end rebate --------------------------------------------------------


                    if (in_array($data['CHANGE_TYPE'], [1, 2])) {
                        $provisi = Application_Helper_General::countProvision($data['COUNTER_WARRANTY_TYPE'], ['TIME_PERIOD_START' => $data['TIME_PERIOD_START'], 'TIME_PERIOD_END' => $data['TIME_PERIOD_END'], 'BG_AMOUNT' => $data['BG_AMOUNT']], $percentage);

                        $previousProvision = $bgOld['PROVISION_FEE'];

                        $getMinGlobalProvisi = $this->_db->select()
                            ->from('M_CHARGES_BG')
                            ->where('CHARGES_ID = ?', 'MINPROV01')
                            ->query()->fetch();

                        $minGlobalProvisi = $getMinGlobalProvisi['CHARGES_AMT'];

                        $selisihProvisi = $provisi - $previousProvision;

                        if ($provisi <= $previousProvision) {
                            $biaya_provisi = 0;
                        } elseif ($selisihProvisi < $minGlobalProvisi) {
                            $biaya_provisi = $minGlobalProvisi * 100;
                        } else {
                            $biaya_provisi = $selisihProvisi * 100;
                        }
                    } else {
                        $biaya_provisi = round($prorata * ($percentage / 100) * $data["BG_AMOUNT"], 2) * 100;

                        if (($biaya_provisi / 100) < intval($getMinCharge["CHARGES_AMT"])) {
                            $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]) * 100;
                        }
                    }

                    $biaya_administrasi = round($data["ADM_FEE"], 2) * 100;
                    $biaya_materai = round($data["STAMP_FEE"], 2) * 100;

                    $this->view->percentage = $percentage;
                    $this->view->biaya_provisi = $biaya_provisi;
                    $this->view->biaya_administrasi = $biaya_administrasi;
                    $this->view->biaya_materai = $biaya_materai;

                    $total_biaya = $biaya_provisi + $biaya_administrasi + $biaya_materai;

                    $bgdatasplit = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                        // ->where('A.IS_TRANSFER != ?', '1')
                        // ->where("ACCT_DESC IS NULL")
                        ->query()->fetchAll();

                    $jmlbgdatasplit = count($bgdatasplit);
                    $totalPenjaminan = 0;

                    for ($i = 0; $i < $jmlbgdatasplit; $i++) {

                        //echo $bgdatasplit[$i]["AMOUNT"];
                        $totalPenjaminan += $bgdatasplit[$i]["AMOUNT"];

                        if ($bgdatasplit[$i]['IS_TRANSFER'] == '1') continue;

                        $temp_save = $this->_db->select()
                            ->from("M_CUSTOMER_ACCT")
                            ->where("ACCT_NO = ?", $bgdatasplit[$i]["ACCT"])
                            ->where("CUST_ID = ?", $data["CUST_ID"])
                            ->query()->fetchAll();

                        $bgdatasplit[$i]["CURRENCY"] = $temp_save[0]["CCY_ID"];
                        $bgdatasplit[$i]["TYPE"] = $temp_save[0]["ACCT_DESC"];

                        //echo $bgdatasplit[$i]["ACCT"];
                        //echo $temp_save[0]["ACCT_DESC"];
                        $svcAccount = new Service_Account($bgdatasplit[$i]["ACCT"], 'IDR');
                        if ($temp_save[0]["ACCT_DESC"] == 'Deposito' || strtolower($bgdatasplit[$i]["ACCT_DESC"]) == 'deposito' || strtolower($bgdatasplit[$i]["ACCT_TYPE"]) == 'deposito' || strtolower($bgdatasplit[$i]["ACCT_TYPE"]) == 't') {

                            $result = $svcAccount->inquiryDeposito('AB', FALSE);

                            if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
                                if ($result['status'] != '1' && $result['status'] != '4') {
                                    //if ($result['status'] == '1') {
                                    $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Status rekening tidak aktif';
                                    $this->view->errorStatusAccountDeposito = $error_fullcover;
                                    //$valid = false;
                                }

                                $getProductType = $this->_db->select()
                                    ->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
                                    ->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
                                    ->query()->fetchAll();

                                if (empty($getProductType)) {
                                    $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Product type tidak terdaftar di product management';
                                    $this->view->errorProductTypeDeposito = $error_fullcover;
                                    //$valid = false;
                                }

                                if ($result['aro_status'] != 'Y') {
                                    $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Type bukan ARO';
                                    $this->view->errorAccountTypeDeposito = $error_fullcover;
                                    //$valid = false;
                                }

                                if ($result['varian_rate'] != '0') {
                                    // $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Special Rate';
                                    $this->view->errorAccountRateDeposito = $error_fullcover;
                                    //$valid = false;
                                }


                                //var_dump($bgdatasplit[$i]["ACCT"]);
                                $sqlRekeningJaminanExist = $this->_db->select()
                                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                                    ->where('A.BG_REG_NUMBER != ?', $data['BG_REG_NUMBER'])
                                    ->where('A.ACCT = ?', $bgdatasplit[$i]["ACCT"]);
                                //die;
                                $getRekeningJaminanExist = $sqlRekeningJaminanExist
                                    ->query()->fetchAll();
                                //die;
                                // if (!empty($getRekeningJaminanExist)) {
                                //     $check_error = true;
                                //     $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Account Number sudah digunakan sebagai jaminan';
                                //     $this->view->errorRekeningJaminanDeposito = $error_fullcover;
                                //     //$valid = false;
                                // }

                                if ($result['balance'] != $bgdatasplit[$i]["AMOUNT"]) {
                                    $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Saldo tidak sama dengan awal pengajuan';
                                    $this->view->errorAmountDeposito = $error_fullcover;
                                }

                                if ($result["hold_description"] == "Y" && intval($bgdatasplit[$i]['IS_TRANSFER']) === 0) {
                                    $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Rekening sedang dijaminkan';
                                    $this->view->errorHoldDeposito = $error_fullcover;
                                }
                            } else {
                                $check_error = true;
                                $error_fullcover = $result['response_desc'];
                                $this->view->errorwaranty = $error_fullcover;
                                $valid = false;
                            }
                        } else {
                            $result = $svcAccount->inquiryAccountBalance('AB', FALSE);

                            if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
                                if ($result['status'] != '1' && $result['status'] != '4') {
                                    //if ($result['status'] == '1') {
                                    $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Status rekening tidak aktif';
                                    $this->view->errorStatusAccountGiro = $error_fullcover;
                                    //die('here');
                                    //$valid = false;
                                }

                                $getProductType = $this->_db->select()
                                    ->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
                                    ->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
                                    ->query()->fetchAll();
                                //var_dump($getProductType);

                                if (empty($getProductType)) {
                                    $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Product type tidak terdaftar di product management';
                                    $this->view->errorProductTypeGiro = $error_fullcover;
                                    //$valid = false;
                                }

                                if ($bgdatasplit[$i]["AMOUNT"] > $result["balance_active"]) {
                                    $check_error = true;
                                    $error_fullcover[$bgdatasplit[$i]["ACCT"]] = 'Saldo tidak mencukupi';
                                    $this->view->errorAmountGiro = $error_fullcover;
                                }
                            } else {
                                $check_error = true;
                                $error_fullcover = $result['response_desc'];
                                $this->view->errorwaranty = $error_fullcover;
                                $valid = false;
                            }
                        }

                        //echo '2';
                        //var_dump($result);//die;
                        //var_dump($value["ACCT"]);
                    }

                    $bgdatasplitmember = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                        ->query()->fetchAll();

                    $this->view->fullmember = $bgdatasplitmember;

                    // cek special rate
					if ($bgdatasplitmember) {
						$cekSpecialRate = [];
						foreach ($bgdatasplitmember as $key => $value) {
							$callService = new Service_Account($value['ACCT'], '');
							$cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));
	
							if (strtolower($cekAcctType) == 'deposito') {
								$inqbalance = $callService->inquiryDeposito();
								if ($inqbalance['varian_rate']) {
									if (floatval($inqbalance['varian_rate']) !== floatval('0')) {
										$isSpecialRate = true;
									} else {
										$isSpecialRate = false;
									}
								}
								$cekSpecialRate[$key]["ACCT"] = $value["ACCT"];
								$cekSpecialRate[$key]["IS_SPECIAL_RATE"] = $isSpecialRate;
							}
						}
	
						$this->view->cekSpecialRate = $cekSpecialRate;
					}

                    // var_dump($totalPenjaminan);
                    // die();

                    if ($totalPenjaminan < $data['BG_AMOUNT']) {

                        $error_fullcover = 'Total nilai penjaminan harus sama dengan / lebih besar dari nilai BG';
                        $this->view->minpenjaminan_err = $error_fullcover;
                        $check_error = true;
                    }

                    // $svcAccount = new Service_Account($data["FEE_CHARGE_TO"], 'IDR');
                    // $result = $svcAccount->inquiryAccountBalance('AB', FALSE);

                    // if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
                    //     if ($result['status'] != '1' && $result['status'] != '4') {
                    //         //$err = true;
                    //         $error_fullcover = 'Status rekening tidak aktif';
                    //         $this->view->errorChargeStatusAccount = $error_fullcover;
                    //         $check_error = false;
                    //     }

                    //     $getProductType = $this->_db->select()
                    //         ->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
                    //         ->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
                    //         ->query()->fetchAll();
                    //     if (empty($getProductType)) {
                    //         //$err = true;
                    //         $error_fullcover = 'Product type tidak terdaftar di product management';
                    //         $this->view->errorChargeProductType = $error_fullcover;
                    //         $check_error = false;
                    //     }

                    //     $getCustomerAccountCharge = $this->_db->select()
                    //         ->from(array('A' => 'M_CUSTOMER_ACCT'), array('A.CCY_ID'))
                    //         ->where('A.ACCT_NO = ?', $data['FEE_CHARGE_TO'])
                    //         ->where('A.CUST_ID = ?', $data['CUST_ID'])
                    //         ->query()->fetchAll();

                    //     $this->view->chargeto_ccy = $getCustomerAccountCharge[0]['CCY_ID'];
                    //     if ($getCustomerAccountCharge[0]['CCY_ID'] != "IDR") {
                    //         //$err = true;
                    //         $error_fullcover = 'mata uang bukan IDR';
                    //         $this->view->errorChargeCurrency = $error_fullcover;
                    //         $check_error = false;
                    //     }
                    // } else {
                    //     $err = true;
                    //     $error_fullcover = $result['response_desc'];
                    //     $this->view->errorwaranty = $error_fullcover;
                    //     $check_error = false;
                    // }

                    $bgdatasplitA = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                        ->where('A.IS_TRANSFER != ?', '1')
                        ->query()->fetchAll();

                    foreach ($bgdatasplitA as $key => $value) {
                        $temp_save = $this->_db->select()
                            ->from("M_CUSTOMER_ACCT")
                            ->where("ACCT_NO = ?", $value["ACCT"])
                            ->query()->fetchAll();

                        $bgdatasplitA[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
                        $bgdatasplitA[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
                    }

                    $bgdatasplitT = $this->_db->select()
                        ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'] ? $bgdata[0]['BG_OLD'] : '')
                        ->query()->fetchAll();

                    foreach ($bgdatasplitT as $key => $value) {
                        $temp_save = $this->_db->select()
                            ->from("M_CUSTOMER_ACCT")
                            ->where("ACCT_NO = ?", $value["ACCT"])
                            ->query()->fetchAll();

                        $bgdatasplitT[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
                        $bgdatasplitT[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
                    }

                    $this->view->fullmemberA = $bgdatasplitA[0];
                    $this->view->fullmemberT = $bgdatasplitT[0];
                }

                // ---------------------------------------------------------------------------

                // kontra garansi dan provision fee jika kontra garansi insurance -----------

                if ($data["COUNTER_WARRANTY_TYPE"] == "3") {

                    $kontraRebate[] = 'IS_TRANSFER_INS';

                    $provision_insurance = $this->_db->select()
                        ->from(["A" => "M_CUST_LINEFACILITY"])
                        ->where("CUST_ID = ?", $principleData["Insurance Name"])
                        ->query()->fetch();

                    if ($provision_insurance["STATUS"] != 1) {
                        $check_error = true;
                        switch ($provision_insurance["STATUS"]) {
                            case '2':
                                $wording = "Perjanjian Berhenti";
                                break;
                            case '3':
                                $wording = "Kadaluarsa";
                                break;
                            case '4':
                                $wording = "Freeze Pengajuan";
                                break;
                        }
                        // $errmsg["counter_warranty_type"] = "Status insurance : " . $wording;
                        $errmsg["counter_warranty_type"] = "Pengajuan belum dapat diproses. Cek status perjanjian Mitra Asuransi";
                        $this->view->errmsg = $errmsg;
                    }

                    $get_customer_ins = $this->_db->select()
                        ->from("M_CUSTOMER")
                        ->where("CUST_ID = ?", $principleData["Insurance Name"])
                        ->query()->fetch();

                    if ($get_customer_ins["CUST_STATUS"] != 1) {
                        $check_error = true;
                        switch ($get_customer_ins["CUST_STATUS"]) {
                            case '2':
                                $wording = "Suspend";
                                break;
                            case '3':
                                $wording = "Deleted";
                                break;
                        }
                        // $errmsg["counter_warranty_type"] = "Status customer : " . $wording;
                        $errmsg["counter_warranty_type"] = "Pengajuan belum dapat diproses. Cek status Debitur";
                        $this->view->errmsg = $errmsg;
                    }

                    $check_all_detail = $this->_db->select()
                        ->from("T_BANK_GUARANTEE_DETAIL")
                        ->where("PS_FIELDNAME = ?", "Insurance Name")
                        ->where("PS_FIELDVALUE = ?", $principleData["Insurance Name"])
                        ->query()->fetchAll();

                    $total_bgamount_on_risk = 0;

                    if (count($check_all_detail) > 0) {
                        $save_bg_reg_number = [];
                        foreach ($check_all_detail as $value) {
                            array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
                        }

                        $get_bgamount_on_risks = $this->_db->select()
                            ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
                            ->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
                            ->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
                            ->query()->fetchAll();

                        foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
                            $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
                        }
                    }

                    $check_all_detail = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE_DETAIL")
                        ->where("PS_FIELDNAME = ?", "Insurance Name")
                        ->where("PS_FIELDVALUE = ?", $principleData["Insurance Name"])
                        ->query()->fetchAll();

                    $total_bgamount_on_temp = 0;

                    if (count($check_all_detail) > 0) {

                        $save_bg_reg_number = [];
                        foreach ($check_all_detail as $value) {
                            array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
                        }

                        $get_bgamount_on_temps = $this->_db->select()
                            ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
                            ->where("COUNTER_WARRANTY_TYPE = '3'")
                            ->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
                            ->where("BG_STATUS IN (?)", ["5", "6", "7", '10', "14", "17", "20", '21', '22', '23', '24'])
                            ->query()->fetchAll();

                        foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
                            $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
                        }
                    }

                    $current_limit = $provision_insurance["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;

                    if ($current_limit < 0) {
                        $check_error = true;
                        // $errmsg["counter_warranty_type"] = "Pengajuan belum dapat diproses. Limit " . $get_customer_ins["CUST_NAME"] . " saat ini belum sesuai";
                        $errmsg["counter_warranty_type"] = "Pengajuan belum dapat diproses. Limit " . $get_customer_ins["CUST_NAME"] . " saat ini tidak tersedia";
                        $this->view->errmsg = $errmsg;
                    }

                    $this->view->provision_insurance = $provision_insurance[0];

                    $get_percentage = $this->_db->select()
                        ->from("M_CUST_LINEFACILITY_DETAIL")
                        ->where("CUST_ID = ?", $principleData["Insurance Name"])
                        ->where("OFFER_TYPE = ?", $data["USAGE_PURPOSE_DESC"])
                        ->where("GRUP_BUMN = ?", $get_customer_ins["GRUP_BUMN"])
                        ->query()->fetch();

                    $percentage = round(floatval($get_percentage["FEE_PROVISION"]), 2);

                    $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
                    $paramProvisionFee['CUST_ID'] = $data["BG_INSURANCE_CODE"];
                    $paramProvisionFee['GRUP'] = $data["CUST_ID"];
                    $paramProvisionFee['USAGE_PURPOSE'] = $data['USAGE_PURPOSE'];
                    $paramProvisionFee['USAGE_PURPOSE_DESC'] = $data['USAGE_PURPOSE_DESC'];
                    $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
                    $percentage = round(floatval($getProvisionFee['provisionFee']), 2);
                    $this->view->provisionFee = $getProvisionFee['provisionFee'];
                    $this->view->adminFee = $getProvisionFee['adminFee'];
                    $this->view->stampFee = $getProvisionFee['stampFee'];

                    $date_start = date_create($data["TIME_PERIOD_START"]);
                    $date_end = date_create($data["TIME_PERIOD_END"]);
                    $tenor = intval(date_diff($date_start, $date_end)->format('%a')) + 1;

                    $prorata = $tenor / 360;

                    $percentage = $getProvisionFee['provisionFee'];

                    // rebate ----------------------------------------------------------------

                    if ($data["REDEBATE_STATUS"] == "0") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();

                        if (count($cekrebate) > 0) {
                            if ($cekrebate[0]["FLAG"] == "1") {
                                $this->view->statusrebate = 1;
                                $this->view->REBATE_REVIEWEDBY = $cekrebate[0]["REBATE_REVIEWEDBY"];

                                $this->view->REBATE_REVIEWED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REVIEWED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                            } else {
                                $this->view->statusrebate = 4;
                                $this->view->REBATE_SUGGESTEDBY = $cekrebate[0]["REBATE_SUGGESTEDBY"];
                                $this->view->REBATE_SUGGESTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_SUGGESTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                            }
                        } else {

                            $this->view->statusrebate = 0;
                        }
                    } elseif ($data["REDEBATE_STATUS"] == "2") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();
                        $this->view->statusrebate = 2;

                        $this->view->REBATE_REJECTEDBY = $cekrebate[0]["REBATE_REJECTEDBY"];

                        $this->view->REBATE_REJECTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REJECTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                    } else {
                        $this->view->statusrebate = 3;
                    }

                    if ($data["REDEBATE_STATUS"] == "1") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();
                        $this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

                        $this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

                        $percentage = floatval($data['REDEBATE_AMOUNT']);
                    }

                    // end rebate ------------------------------------------------------------

                    if (in_array($data['CHANGE_TYPE'], [1, 2])) {
                        $provisi = Application_Helper_General::countProvision($data['COUNTER_WARRANTY_TYPE'], ['TIME_PERIOD_START' => $data['TIME_PERIOD_START'], 'TIME_PERIOD_END' => $data['TIME_PERIOD_END'], 'BG_AMOUNT' => $data['BG_AMOUNT']], $percentage);

                        $previousProvision = $bgOld['PROVISION_FEE'];

                        $getMinGlobalProvisi = $this->_db->select()
                            ->from('M_CHARGES_BG')
                            ->where('CHARGES_ID = ?', 'MINPROV01')
                            ->query()->fetch();

                        $minGlobalProvisi = $getMinGlobalProvisi['CHARGES_AMT'];

                        $selisihProvisi = $provisi - $previousProvision;

                        if ($provisi <= $previousProvision) {
                            $biaya_provisi = 0;
                        } elseif ($selisihProvisi < $minGlobalProvisi) {
                            $biaya_provisi = $minGlobalProvisi * 100;
                        } else {
                            $biaya_provisi = $selisihProvisi * 100;
                        }
                    } else {
                        $biaya_provisi = round($prorata * ($percentage / 100) * $data["BG_AMOUNT"], 2) * 100;

                        if (($biaya_provisi / 100) < intval($getMinCharge["CHARGES_AMT"])) {
                            $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]) * 100;
                        }
                    }

                    $biaya_administrasi = round($data["ADM_FEE"], 2) * 100;
                    $biaya_materai = round($data["STAMP_FEE"], 2) * 100;


                    $this->view->percentage = $percentage;
                    $this->view->biaya_provisi = $biaya_provisi;
                    $this->view->biaya_administrasi = $biaya_administrasi;
                    $this->view->biaya_materai = $biaya_materai;

                    $total_biaya = $biaya_provisi + $biaya_administrasi + $biaya_materai + (round(floatval($principleData["Principle Insurance Premium"]), 2) * 100) + (round(floatval($principleData["Principle Insurance Administration"]), 2) * 100) + (round(floatval($principleData["Principle Insurance Stamp"]), 2) * 100);

                    // cek model asuransi
                    $cekModelAsuransi = $this->_db->select()
                        ->from("M_CUST_LINEFACILITY")
                        ->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
                        ->query()->fetchAll();
                    $this->view->feedebited = $cekModelAsuransi[0]["FEE_DEBITED"];

                    //	
                }

                // --------------------------------------------------------------------------

                // kontra garansi lf ----------------------------------------------------------

                if ($data["COUNTER_WARRANTY_TYPE"] == "2") {
                    $get_linefacility = $this->_db->select()
                        ->from("M_CUST_LINEFACILITY")
                        ->where("CUST_ID = ?", $data["CUST_ID"])
                        ->query()->fetch();

                    if ($get_linefacility["STATUS"] != 1) {
                        $check_error = true;
                        switch ($get_linefacility["STATUS"]) {
                            case '2':
                                $wording = "Perjanjian Berhenti";
                                break;
                            case '3':
                                $wording = "Kadaluarsa";
                                break;
                            case '4':
                                $wording = "Freeze Pengajuan";
                                break;
                        }

                        // $errmsg["counter_warranty_type"] = "Status linefacility " . $wording;
                        $errmsg["counter_warranty_type"] = "Pengajuan belum dapat diproses. Cek status perjanjian Debitur";
                        $this->view->errmsg = $errmsg;
                    }

                    $get_customer = $this->_db->select()
                        ->from("M_CUSTOMER")
                        ->where("CUST_ID = ?", $data["CUST_ID"])
                        ->query()->fetch();

                    if ($get_customer["CUST_STATUS"] != 1) {
                        $check_error = true;
                        switch ($get_customer["CUST_STATUS"]) {
                            case '2':
                                $wording = "Suspend";
                                break;
                            case '3':
                                $wording = "Deleted";
                                break;
                        }
                        // $errmsg["counter_warranty_type"] = "Status customer : " . $wording;
                        $errmsg["counter_warranty_type"] = "Pengajuan belum dapat diproses. Cek status Debitur";
                        $this->view->errmsg = $errmsg;
                    }

                    $get_bgamount_on_risks = $this->_db->select()
                        ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
                        ->where("(BG_STATUS = 15 OR BG_STATUS = 16) AND COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($get_linefacility["CUST_ID"]))
                        ->query()->fetchAll();


                    $total_bgamount_on_risk = 0;

                    foreach ($get_bgamount_on_risks as $key => $get_bgamount_on_risk) {
                        $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
                    }

                    $total_bgamount_on_temp = 0;

                    $get_bgamount_on_temps = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
                        ->where("COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($get_linefacility["CUST_ID"]))
                        ->query()->fetchAll();

                    foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
                        $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
                    }

                    $current_limit = $get_linefacility["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;

                    $paramLimit['CUST_ID'] =  $bgdata[0]["CUST_ID"];
                    $paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
                    $getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

                    $current_limit = $getLineFacility['currentLimit'];

                    if ($current_limit < 0) {
                        $check_error = true;
                        $errmsg["counter_warranty_type"] = "Limit linefacility tidak mencukupi ";
                        $this->view->errmsg = $errmsg;
                    }

                    $get_percentage = $this->_db->select()
                        ->from("M_CUST_LINEFACILITY_DETAIL")
                        ->where("CUST_ID = ?", $data["CUST_ID"])
                        ->where("OFFER_TYPE = ?", $data["USAGE_PURPOSE_DESC"])
                        ->where("GRUP_BUMN = ?", $data['GRUP_BUMN'])
                        ->query()->fetch();

                    $percentage = round(floatval($get_percentage["FEE_PROVISION"]), 2);

                    $date_start = date_create($data["TIME_PERIOD_START"]);
                    $date_end = date_create($data["TIME_PERIOD_END"]);
                    $tenor = intval(date_diff($date_start, $date_end)->format('%a')) + 1;

                    $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
                    $paramProvisionFee['CUST_ID'] = $data['CUST_ID'];
                    $paramProvisionFee['USAGE_PURPOSE'] = $data['USAGE_PURPOSE'];
                    $paramProvisionFee['USAGE_PURPOSE_DESC'] = $data['USAGE_PURPOSE_DESC'];
                    $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
                    $this->view->provisionFee = $getProvisionFee['provisionFee'];
                    $data["ADM_FEE"] = $getProvisionFee['adminFee'];
                    $data["STAMP_FEE"] = $getProvisionFee['stampFee'];

                    $prorata = $tenor / 360;

                    // rebate -----------------------------------------------------------

                    if ($data["REDEBATE_STATUS"] == "0") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();

                        if (count($cekrebate) > 0) {
                            if ($cekrebate[0]["FLAG"] == "1") {
                                $this->view->statusrebate = 1;
                                $this->view->REBATE_REVIEWEDBY = $cekrebate[0]["REBATE_REVIEWEDBY"];

                                $this->view->REBATE_REVIEWED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REVIEWED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                            } else {
                                $this->view->statusrebate = 4;
                                $this->view->REBATE_SUGGESTEDBY = $cekrebate[0]["REBATE_SUGGESTEDBY"];
                                $this->view->REBATE_SUGGESTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_SUGGESTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                            }
                        } else {

                            $this->view->statusrebate = 0;
                        }
                    } elseif ($data["REDEBATE_STATUS"] == "2") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();
                        $this->view->statusrebate = 2;

                        $this->view->REBATE_REJECTEDBY = $cekrebate[0]["REBATE_REJECTEDBY"];

                        $this->view->REBATE_REJECTED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_REJECTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                    } else {
                        $this->view->statusrebate = 3;
                    }

                    if ($data["REDEBATE_STATUS"] == "1") {
                        $cekrebate = $this->_db->select()
                            ->from("TEMP_REQUEST_REBATE")
                            ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                            ->query()->fetchAll();
                        $this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

                        $this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

                        $percentage = floatval($data['REDEBATE_AMOUNT']);
                    }
                    // end rebate -------------------------------------------------------

                    if (in_array($data['CHANGE_TYPE'], [1, 2])) {
                        $provisi = Application_Helper_General::countProvision($data['COUNTER_WARRANTY_TYPE'], ['TIME_PERIOD_START' => $data['TIME_PERIOD_START'], 'TIME_PERIOD_END' => $data['TIME_PERIOD_END'], 'BG_AMOUNT' => $data['BG_AMOUNT']], $percentage);

                        $previousProvision = $bgOld['PROVISION_FEE'];

                        $getMinGlobalProvisi = $this->_db->select()
                            ->from('M_CHARGES_BG')
                            ->where('CHARGES_ID = ?', 'MINPROV01')
                            ->query()->fetch();

                        $minGlobalProvisi = $getMinGlobalProvisi['CHARGES_AMT'];

                        $selisihProvisi = $provisi - $previousProvision;

                        if ($provisi <= $previousProvision) {
                            $biaya_provisi = 0;
                        } elseif ($selisihProvisi < $minGlobalProvisi) {
                            $biaya_provisi = $minGlobalProvisi * 100;
                        } else {
                            $biaya_provisi = $selisihProvisi * 100;
                        }
                    } else {
                        $biaya_provisi = round($prorata * ($percentage / 100) * $data["BG_AMOUNT"], 2) * 100;
                        if (($biaya_provisi / 100) < intval($getMinCharge["CHARGES_AMT"])) {
                            $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]) * 100;
                        }
                    }

                    $biaya_administrasi = round($data["ADM_FEE"], 2) * 100;
                    $biaya_materai = round($data["STAMP_FEE"], 2) * 100;

                    $this->view->percentage = $percentage;
                    $this->view->biaya_provisi = $biaya_provisi;
                    $this->view->biaya_administrasi = $biaya_administrasi;
                    $this->view->biaya_materai = $biaya_materai;

                    $total_biaya = $biaya_provisi + $biaya_administrasi + $biaya_materai;
                }

                // ----------------------------------------------------------------------------

                // validasi rekening pembebanan biaya ---------------------------------------------

                $svcPembebanan = new Service_Account($data["FEE_CHARGE_TO"], "IDR");
                $get_acct_balance = $svcPembebanan->inquiryAccountBalance();

                if ($get_acct_balance['response_code'] == '00' || $get_acct_balance['response_code'] == '0000') {
                    if ($total_biaya > $get_acct_balance["balance_active"] * 100) {
                        $check_error = true;
                        $errmsg["pembebanan_biaya"] = "Saldo tidak mencukupi";
                        $this->view->errmsg = $errmsg;
                    }

                    if ($get_acct_balance["status"] != 1 && $get_acct_balance["status"] != 4) {
                        $check_error = true;
                        $errmsg["pembebanan_biaya"] = "Status rekening tidak aktif ";
                        $this->view->errmsg = $errmsg;
                    }

                    $getProductType = $this->_db->select()
                        ->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
                        ->where('A.PRODUCT_CODE IN (?)', $get_acct_balance['account_type'])
                        ->query()->fetchAll();
                    if (empty($getProductType)) {
                        //$err = true;
                        $errmsg["pembebanan_biaya"] = 'Product type tidak terdaftar di product management';
                        $this->view->errmsg = $errmsg;
                        $check_error = true;
                    }
                } else {
                    $svcPembebanan = new Service_Account($data["FEE_CHARGE_TO"], "IDR");
                    $get_error = $svcPembebanan->inquiryAccontInfo();
                    $errmsg["pembebanan_biaya"] = $get_error['response_desc'];
                    $this->view->errmsg = $errmsg;
                    $check_error = true;
                }

                // var_dump($total_biaya, $get_acct_balance["balance_active"] * 100);
                // die();

                // ----------------------------------------------------------------------------------

                // validasi jaminan -----------------------------------------------------------------

                // start validasi jaminan

                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '2') {

                    $cust_id = $bgdata[0]["CUST_ID"];
                    // $jenisjaminan = strtoupper($arrbgType[$data['USAGE_PURPOSE']]);
                    $jenisjaminan = ($data['USAGE_PURPOSE'] == 'F4299') ? strtoupper($data['USAGE_PURPOSE_DESC']) : strtoupper($arrbgType[$data['USAGE_PURPOSE']]);

                    $check_grup_bumn = $this->_db->select()
                        ->from("M_CUSTOMER")
                        ->where("CUST_STATUS = 1")
                        ->where("CUST_ID = ?", $cust_id)
                        ->query()->fetchAll();


                    $check_charges_bg = $this->_db->select()
                        ->from("M_CHARGES_BG")
                        ->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'LFBUMN' : 'LFNONBUMN') . '%')
                        ->where("CHARGES_NAME LIKE ?", '' . 'LF ' . $jenisjaminan . '%')
                        ->query()->fetchAll();

                    $check_lf_detail = $this->_db->select()
                        ->from("M_CUST_LINEFACILITY_DETAIL")
                        ->where("OFFER_TYPE = ?", $jenisjaminan)
                        ->where("FLAG = 1")
                        ->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
                        ->where("CUST_ID = ?", $cust_id)
                        ->query()->fetchAll();
                    $check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];

                    if ($check_lf_detail[0]['FLAG'] == '1' && $check_lf_detail[0]['ACTIVE'] == 'Y') {
                        // $check_error = false;
                    } else {
                        $errmsg["counter_warranty_type"] = "Jaminan tidak tersedia";
                        $this->view->errmsg = $errmsg;
                        $check_error = true;
                    }
                }

                if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {

                    $cust_id = $data['BG_INSURANCE_CODE'];
                    // $jenisjaminan = strtoupper($arrbgType[$data['USAGE_PURPOSE']]);
                    $jenisjaminan = ($data['USAGE_PURPOSE'] == 'F4299') ? strtoupper($data['USAGE_PURPOSE_DESC']) : strtoupper($arrbgType[$data['USAGE_PURPOSE']]);

                    $check_grup_bumn = $this->_db->select()
                        ->from("M_CUSTOMER")
                        ->where("CUST_STATUS = 1")
                        ->where("CUST_ID = ?", $cust_id)
                        ->query()->fetchAll();

                    $insDetail = $this->_db->select()
                        ->from("M_CUST_LINEFACILITY", ['FEE_DEBITED'])
                        ->where("CUST_ID = ?", $data['BG_INSURANCE_CODE'])
                        ->query()->fetch();

                    $this->view->insDetail = $insDetail;

                    $check_charges_bg = $this->_db->select()
                        ->from("M_CHARGES_BG")
                        ->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'INSBUMN' : 'INSNONBUMN') . '%')
                        ->where("CHARGES_NAME LIKE ?", '' . 'ASURANSI ' . $jenisjaminan . '%')
                        ->query()->fetchAll();



                    $check_lf_detail = $this->_db->select()
                        ->from("M_CUST_LINEFACILITY_DETAIL")
                        ->where("OFFER_TYPE = ?", $jenisjaminan)
                        ->where("FLAG = 1")
                        ->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
                        ->where("CUST_ID = ?", $cust_id)
                        ->query()->fetchAll();

                    $check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];

                    if ($check_lf_detail[0]['FLAG'] == '1' && $check_lf_detail[0]['ACTIVE'] == 'Y') {
                        // $check_error = false;
                    } else {
                        $errmsg["counter_warranty_type"] = "Jaminan tidak tersedia";
                        $this->view->errmsg = $errmsg;
                        $check_error = true;
                    }
                }


                //echo $errValidasi;
                //die;
                // end validasi jaminan

                // ----------------------------------------------------------------------------------


                // QR CODE --------------------------------------------------------------

                $this->view->qrposition = [
                    "ds" => "Jangan Tampilkan",
                    "top-left" => "Atas Kiri",
                    "top-right" => "Atas Kanan",
                    "bottom-left" => "Bawah Kiri",
                    "bottom-right" => "Bawah Kanan",
                ];

                $size = [];
                for ($i = 15; $i <= 40; $i++) {
                    $size[strval($i)] = $i;
                }

                $this->view->size = $size;

                // end QR CODE ----------------------------------------------------------

                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))
                    ->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
                    ->where("C.REG_NUMBER = ?", (string) $BG_NUMBER);
                // ->where("C.GROUP = ?" , (string)$value);
                // echo $selectapprover;die;
                $usergroup = $this->_db->fetchAll($selectapprover);

                if (!empty($usergroup)) {
                    // die;
                    // $this->view->pdf = true;
                    $this->view->validbtn = false;
                } else {
                    if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
                        $boundary = $this->validatebtn('50', $bgdata['0']['BG_AMOUNT'], 'IDR', $BG_NUMBER);
                    } else {
                        $boundary = $this->validatebtn('51', $bgdata['0']['BG_AMOUNT'], 'IDR', $BG_NUMBER);
                    }
                    if ($boundary) {
                        // die;
                        $this->view->validbtn = false;
                    } else {
                        // die;
                        $this->view->validbtn = true;
                    }
                }
                if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
                    $policyBoundary = $this->findPolicyBoundary(50, $bgdata['0']['BG_AMOUNT']);
                    $approverUserList = $this->findUserBoundary(50, $bgdata['0']['BG_AMOUNT']);
                } else {
                    $policyBoundary = $this->findPolicyBoundary(51, $bgdata['0']['BG_AMOUNT']);
                    $approverUserList = $this->findUserBoundary(51, $bgdata['0']['BG_AMOUNT']);
                }
                //echo '<pre>';
                //var_dump($policyBoundary);die;
                $this->view->policyBoundary = $policyBoundary;


                // get linefacillity

                $get_linefacility = $this->_db->select()
                    ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_ID"])
                    ->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
                    ->query()->fetchAll();

                $get_bgamount_on_risks = $this->_db->select()
                    ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
                    ->where("BG_STATUS IN (?)", ["15", "16"])
                    ->where("COUNTER_WARRANTY_TYPE = ?", "2")
                    ->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
                    ->query()->fetchAll();

                $total_bgamount_on_risk = 0;

                foreach ($get_bgamount_on_risks as $key => $get_bgamount_on_risk) {
                    $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
                }

                $total_bgamount_on_temp = 0;

                $get_bgamount_on_temps = $this->_db->select()
                    ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
                    ->where("COUNTER_WARRANTY_TYPE = '2'")
                    ->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
                    ->query()->fetchAll();

                foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
                    $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
                }

                $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
                $this->view->current_limit = $current_limit;
                $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

                $this->view->linefacility = $get_linefacility[0];

                // end get linefacility


                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

                $this->view->BG_PUBLISH = $arrbgpublish[$data['BG_PUBLISH']];

                if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == "3") {
                    $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
                    $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

                    $insuranceBranch = $this->_db->select()
                        ->from("M_INS_BRANCH")
                        ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
                        ->where("CUST_ID = ?", $principleData["Insurance Name"])
                        ->query()->fetchAll();

                    $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
                    $this->view->insuranceAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];
                }

                // cek split untuk rebate
                $isTransSplit = false;


                if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1' || $bgdata['0']['COUNTER_WARRANTY_TYPE'] == '3') {
                    $bgdatasplit = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                        ->where('A.IS_TRANSFER != ?', '1')
                        // ->where("A.ACCT_DESC IS NULL")
                        ->query()->fetchAll();

                    foreach ($bgdatasplit as $key => $value) {
                        if (!$isTransSplit && $value['IS_TRANSFER'] == 1) {
                            $isTransSplit = true;
                        }
                        $temp_save = $this->_db->select()
                            ->from("M_CUSTOMER_ACCT")
                            ->where("ACCT_NO = ?", $value["ACCT"])
                            ->where("CUST_ID = ?", $data["CUST_ID"])
                            ->query()->fetchAll();


                        $bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
                        $bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
                    }

                    $bgdatasplitmember = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                        // ->where("A.ACCT_DESC IS NULL")
                        ->query()->fetchAll();

                    $this->view->fullmember = $bgdatasplitmember;
                }

                $this->view->canRebate = true;
                if ($biaya_provisi / 100 == intval($getMinCharge["CHARGES_AMT"]) || $isTransSplit || $cekModelAsuransi[0]["FEE_DEBITED"] == '2') {
                    $this->view->canRebate = false;
                }

                $selectHistory    = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $BG_NUMBER);

                $history = $this->_db->fetchAll($selectHistory);

                $cust_approver = 1;

                $bg_submission_hisotrys = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                    ->where("CUST_ID IN (?) ", [$selectcomp[0]["CUST_ID"], $bgdata[0]["BG_INSURANCE_CODE"]]);

                $bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

                $getBoundary = $this->_db->select()
                    ->from('M_APP_BOUNDARY')
                    ->where('CUST_ID = ?', $data['CUST_ID'])
                    ->where('BOUNDARY_MIN <= \'' . $data['BG_AMOUNT'] . '\'')
                    ->where('BOUNDARY_MAX >= \'' . $data['BG_AMOUNT'] . '\'')
                    ->query()->fetch();

                $checkBoundary = strpos($getBoundary['POLICY'], 'THEN');
                if ($checkBoundary === false) $checkBoundary = strpos($getBoundary['POLICY'], 'AND');
                $checkCount = 1;

                foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {

                    // maker
                    if ($bg_submission_hisotry['HISTORY_STATUS'] == 1 || $bg_submission_hisotry['HISTORY_STATUS'] == 33) {
                        $makerStatus = 'active';
                        $makerIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['USER_FULLNAME'];
                        // $custFullname = $customer[0]['USER_ID'];

                        $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                        $align = 'align="center"';

                        $this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }

                    // approver
                    if ($checkBoundary === false) {
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
                            $approverStatus = 'active';
                            $approverIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custFullname = $customer[0]['USER_ID'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

                            $this->view->approverStatus = $approverStatus;
                        }
                    } else {
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
                            $approverStatus = 'active';
                            $approverIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            // $custFullname = $customer[0]['USER_FULLNAME'];
                            if ($checkCount === 1) {
                                $custFullname = $customer[0]['USER_FULLNAME'];
                                $checkCount++;
                            } else {
                                $custFullname = $custFullname . "<br><span>" . $customer[0]['USER_FULLNAME'] . "</span>";
                                $checkCount = 1;
                            }

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

                            $this->view->approverStatus = $approverStatus;
                        }
                    }

                    if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
                        //if releaser
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5 || $bg_submission_hisotry['HISTORY_STATUS'] == 10) {
                            $releaserStatus = 'active';
                            $releaserIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custFullname = $customer[0]['USER_ID'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                            $this->view->releaserStatus = $releaserStatus;
                        }
                    } else {
                        //if releaser
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5) {
                            $releaserStatus = 'active';
                            $releaserIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custFullname = $customer[0]['USER_ID'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                            $this->view->releaserStatus = $releaserStatus;
                        }
                    }


                    // if ($bg_submission_hisotry['HISTORY_STATUS'] == 4 || $bg_submission_hisotry['HISTORY_STATUS'] == 14) {
                    // 	$releaserStatus = 'active';
                    // 	$releaserIcon = '<i class="fas fa-check"></i>';

                    // 	$custlogin = $bg_submission_hisotry['USER_LOGIN'];

                    // 	$selectCust	= $this->_db->select()
                    // 		->from('M_USER')
                    // 		->where("USER_ID = ?", $custlogin)
                    // 		->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                    // 	$customer = $this->_db->fetchAll($selectCust);

                    // 	$custFullname = $customer[0]['USER_FULLNAME'];

                    // 	$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                    // 	$align = 'align="center"';

                    // 	$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span></span></div>';
                    // 	$this->view->releaserStatus = $releaserStatus;
                    // }
                }

                foreach ($history as $row) {

                    if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
                        if ($row['HISTORY_STATUS'] == 8 || $row['HISTORY_STATUS'] == 24) {
                            $makerStatus = 'active';
                            $insuranceIcon = '<i class="fas fa-check"></i>';
                            /*$approveStatus = 'active';
							$reviewStatus = 'active';
							$releaseStatus = 'active';
							$releaseIcon = '<i class="fas fa-check"></i>';*/
                            $insuranceStatus = 'active';
                            $reviewStatus = '';
                            //$releaseStatus = '';
                            //$releaseIcon = '';

                            $makerOngoing = '';
                            $reviewerOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = '';

                            $custlogin = $row['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin);
                            //->where("CUST_ID = ?", $row['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            // $custFullname = $customer[0]['USER_ID'];
                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custEmail 	  = $customer[0]['USER_EMAIL'];
                            // $custPhone	  = $customer[0]['USER_PHONE'];

                            $insuranceApprovedBy = $custFullname;

                            $align = 'align="center"';
                            $marginLeft = '';
                            if ($cust_reviewer == 0 && $cust_approver == 0) {
                                $align = '';
                                $marginLeft = 'style="margin-left: 15px;"';
                            }

                            $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                            $this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                        }
                    }

                    if ($row['HISTORY_STATUS'] == 6) {
                        $verifyStatus = 'active';
                        $verifyIcon = '<i class="fas fa-check"></i>';

                        $verifyOngoing = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = 'ongoing';
                            $releaserOngoing = '';
                        } else {
                            //$reviewerOngoing = 'ongoing';
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            $releaserOngoing = '';
                        }

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

                        $align = 'align="center"';
                        $marginRight = '';

                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginRight = 'style="margin-right: 15px;"';
                        }

                        $this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
                    }

                    //if reviewer done
                    if ($row['HISTORY_STATUS'] == 7) {
                        $bankReviewStatus = "active";
                        $bankReviewOngoing = "ongoing";
                        $bankReviewIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        $this->view->bankReviewedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }

                    //if releaser done
                    if ($row['HISTORY_STATUS'] == 14) {
                        $makerStatus = 'active';
                        /*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
                        $bankApproveStatus = 'active';
                        $bankApproveIcon = '<i class="fas fa-check"></i>';
                        $bankApproveOngoing = '';
                        $reviewStatus = '';
                        $releaseStatus = '';
                        $releaseIcon = '';

                        $makerOngoing = '';
                        $reviewerOngoing = '';
                        $approverOngoing = '';
                        $releaserOngoing = '';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $row['USER_LOGIN']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];
                        // $custEmail 	  = $customer[0]['USER_EMAIL'];
                        // $custPhone	  = $customer[0]['USER_PHONE'];

                        $releaserApprovedBy = $custFullname;

                        $align = 'align="center"';
                        $marginLeft = '';
                        // if ($cust_reviewer == 0 && $cust_approver == 0) {
                        //     $align = '';
                        //     $marginLeft = 'style="margin-left: 15px;"';
                        // }

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

                        $getApprover = $this->_db->select()
                            ->from(['TBA' => 'T_BGAPPROVAL'], ['*'])
                            ->joinLeft(['MB' => 'M_BUSER'], 'TBA.USER_ID = MB.BUSER_ID', ['BUSER_NAME'])
                            ->where('REG_NUMBER = ?', $bgdata['0']['BG_REG_NUMBER'])
                            ->query()->fetchAll();

                        $approverText = '';
                        foreach ($getApprover as $userApprove) {
                            # code...
                            $approverText .= '<div ' . $align . ' class="textTheme">' . date('d-M-Y', strtotime($userApprove['CREATED'])) . '<br><span ' . $marginRight . '>' . $userApprove['BUSER_NAME'] . '</span></div>';
                        }

                        $this->view->bankApproverBy = $approverText;
                    }

                    if ($row['HISTORY_STATUS'] == 17) {
                        $signPrepareStatus = 'active';
                        $signPrepareIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $row['USER_LOGIN']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        // $releaserApprovedBy = $custFullname;

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        $this->view->signPrepareBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }
                }

                if ($data["DOCUMENT_ID"]) {
                    $checkSigner = $this->_db->select()
                        ->from("T_BANK_GUARANTEE_SIGNER")
                        ->where("DOCUMENT_ID = ?", $data["DOCUMENT_ID"])
                        ->query()->fetchAll();

                    if ($checkSigner) {
                        $getCustFullName = $this->_db->select()
                            ->from("M_BUSER", ["BUSER_NAME"])
                            // ->where("BUSER_EMAIL = ?", $checkSigner[0]["SIGNER_EMAIL"])
                            ->where("BUSER_ID = ?", $checkSigner[0]["SIGNER_NAME"])
                            ->query()->fetch();

                        $efdate = date("d-M-Y", strtotime($checkSigner[0]["SIGNER_DATE"]));
                        $this->view->checkSign = true;
                        $this->view->signer1 = '<div align="center" class="textTheme">' . $efdate . '<br><span>' . $getCustFullName["BUSER_NAME"] . '</span></div>';
                    }
                }

                //approvernamecircle jika sudah ada yang approve
                if (!empty($userid)) {

                    $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

                    $flipAlphabet = array_flip($alphabet);

                    $approvedNameList = array();
                    $i = 0;
                    //var_dump($userid);die;
                    foreach ($userid as $key => $value) {

                        //select utk nama dan email
                        $selectusername = $this->_db->select()
                            ->from(array('M_BUSER'), array(
                                '*'
                            ))
                            ->where("BUSER_ID = ?", (string) $value);

                        $username = $this->_db->fetchAll($selectusername);

                        //select utk cek user berada di grup apa
                        $selectusergroup    = $this->_db->select()
                            ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
                                '*'
                            ))

                            ->where("C.BUSER_ID 	= ?", (string) $value);

                        $usergroup = $this->_db->fetchAll($selectusergroup);

                        $groupuserid = $usergroup[0]['GROUP_BUSER_ID'];
                        $groupusername = $usergroup[0]['BUSER_ID'];
                        $groupuseridexplode = explode("_", $groupuserid);

                        if ($groupuseridexplode[0] == "S") {
                            $usergroupid = "SG";
                        } else {
                            $usergroupid = $alphabet[$groupuseridexplode[2]];
                        }

                        // $tempuserid = "";
                        // foreach ($approverNameCircle as $row => $data) {
                        // 	foreach ($data as $keys => $val) {
                        // 		if ($keys == $usergroupid) {
                        // 			if (preg_match("/active/", $val)) {
                        // 				continue;
                        // 			}else{
                        // 				if ($groupuserid == $tempuserid) {
                        // 					continue;
                        // 				}else{
                        // 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
                        // 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
                        // 				}
                        // 				$tempuserid = $groupuserid;
                        // 			}
                        // 		}
                        // 	}
                        // }

                        array_push($approvedNameList, $username[0]['BUSER_NAME']);

                        $efdate = $approveEfDate[$i];

                        $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['BUSER_NAME'] . ' (' . $usergroupid . ')</div>';
                        $i++;
                    }

                    $this->view->approverApprovedBy = $approverApprovedBy;


                    //kalau sudah approve semua
                    if (!$checkBoundary) {
                        $approveStatus = '';
                        $approverOngoing = '';
                        $approveIcon = '';
                        $releaserOngoing = 'ongoing';
                    }
                }



                $selectsuperuser = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'))
                    ->where("C.REG_NUMBER 	= ?", $BG_NUMBER)
                    ->where("C.GROUP 	= 'SG'");

                $superuser = $this->_db->fetchAll($selectsuperuser);

                if (!empty($superuser)) {
                    $userid = $superuser[0]['USER_ID'];

                    //select utk nama dan email
                    $selectusername = $this->_db->select()
                        ->from(array('M_BUSER'), array(
                            '*'
                        ))
                        ->where("BUSER_ID = ?", (string) $userid);

                    $username = $this->_db->fetchAll($selectusername);

                    $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['BUSER_NAME'] . ' (' . $usergroupid . ')</div>';

                    $approveStatus = '';
                    $approverOngoing = '';
                    $approveIcon = '';
                    $releaserOngoing = 'ongoing';
                }
                // <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
                //define circle
                $makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . ' </button>';

                $insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . ' </button>';

                foreach ($reviewerList as $key => $value) {

                    $textColor = '';
                    if ($value == $reviewerApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }
                // 
                $bankReviewNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $bankReviewStatus . ' hovertext" disabled>' . $bankReviewIcon . ' </button>';

                $groupNameList = $approverUserList['GROUP_NAME'];
                unset($approverUserList['GROUP_NAME']);

                if ($approverUserList != '') {
                    //echo '<pre>';
                    //var_dump($approverUserList);die;
                    foreach ($approverUserList as $key => $value) {
                        $approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
                        $i = 1;
                        foreach ($value as $key2 => $value2) {

                            $textColor = '';
                            if (in_array($value2, $approvedNameList)) {
                                $textColor = 'text-white-50';
                            }

                            if ($i == count($value)) {
                                $approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
                            } else {
                                $approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
                            }
                            $i++;
                        }
                    }
                } else {
                    $approverListdata = 'There is no Approver User';
                }
                // 
                $spandata = '';
                if (!empty($approverListdata) && !$error_msg2) {
                    $spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
                }

                $approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '
						' . $spandata . '
					</button>';

                foreach ($releaserList as $key => $value) {

                    $textColor = '';
                    if ($value == $releaserApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }
                // 
                // $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span> </button>';
                $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '</button>';

                $verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $verifyStatus . ' ' . $verifyOngoing . ' hovertext" disabled>' . $verifyIcon . ' </button>';

                $bankApproverNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $bankApproveStatus . ' ' . $bankApproveOngoing . ' hovertext" disabled>' . $bankApproveIcon . ' </button>';

                $signPrepareNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $signPrepareStatus . ' hovertext" disabled>' . $signPrepareIcon . ' </button>';

                $this->view->policyBoundary = $policyBoundary;
                $this->view->makerNameCircle = $makerNameCircle;
                $this->view->bankReviewNameCircle = $bankReviewNameCircle;
                $this->view->approverNameCircle = $approverNameCircle;
                $this->view->signPrepareNameCircle = $signPrepareNameCircle;
                $this->view->bankApprover = $bankApproverNameCircle;
                $this->view->releaserNameCircle = $releaserNameCircle;
                $this->view->verifyNameCircle = $verifyNameCircle;
                $this->view->insuranceNameCircle = $insuranceNameCircle;

                $this->view->verifyStatus = $verifyStatus;
                $this->view->makerStatus = $makerStatus;
                $this->view->approveStatus = $approveStatus;
                $this->view->signPrepareStatus = $signPrepareStatus;
                $this->view->reviewStatus = $reviewStatus;
                $this->view->releaseStatus = $releaseStatus;


                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                $this->view->arrbgType = $arrbgType;

                $data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }


                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


                $config            = Zend_Registry::get('config');
                $BgType         = $config["bg"]["status"]["desc"];
                $BgCode         = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

                $this->view->arrStatus = $arrStatus;

                $CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);
                $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                $AccArr = $CustomerUser->getAccounts($param);

                // var_dump($data["FEE_CHARGE_TO"]);
                // die();

                if ($data["COUNTER_WARRANTY_TYPE"] != 3) {
                    $cust_id = $data['CUST_ID'];
                } else {
                    $cust_id = $principleData["Insurance Name"];
                }

                $get_name_account = $this->_db->select()
                    ->from("M_CUSTOMER_ACCT")
                    ->where("ACCT_NO = ?", $data['FEE_CHARGE_TO'])
                    // ->where("CUST_ID = ?", $cust_id)
                    ->query()->fetch();

                $this->view->src_name = $get_name_account["ACCT_NAME"];
                $this->view->ccy_pembebanan = $get_name_account["CCY_ID"];
                $this->view->acct_desc = $get_name_account["ACCT_DESC"];

                // if (!empty($AccArr)) {
                //     $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                // }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                $this->view->bankFormatNumber = $data['BG_FORMAT'];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];
                $this->view->bahasa = $data["BG_LANGUAGE"];

                // $arrWaranty = array(
                // 	1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                // 	2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                // 	3 => 'Insurance'

                // );

                // $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

                // if (!empty($data['USAGE_PURPOSE'])) {
                //     $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                //     foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                //         $str = 'checkp' . $val;
                //         //var_dump($str);
                //         $this->view->$str =  'checked';
                //     }
                // }

                $this->view->usage_purpose = $data['USAGE_PURPOSE'];

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];
                $this->view->data = $data;
                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->contact_person = $data['RECIPIENT_CP'];
                $this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->contact_email = $data['RECIPIENT_EMAIL'];


                // $obligeename = $this->_db->select()
                // ->from('M_CUSTOMER', array('CUST_NAME'
                // ))
                // ->where('CUST_ID = ?',$data['SP_OBLIGEE_CODE']);

                // $custname = $db->fetchRow($obligeename);


                $this->view->obligee_name = $data['CUST_NAME'];

                $this->view->obligee_code = $data['SP_OBLIGEE_CODE'];

                $this->view->comment = $data['SERVICE'];

                $this->view->fileName = $data['FILE'];
                $this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                $this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];
                //$this->view->GT_TYPE = $data['GT_DOC_TYPE'];
                $this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

                $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];


                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];
                $Settings = new Settings();
                $claim_period = $Settings->getSettingNew('max_claim_period');
                $this->view->claim_period = $claim_period;

                if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                                a.USER_LOGIN,
                                b.`USER_FULLNAME` AS u_name,
                                c.`BUSER_NAME` AS b_name,
                                a.DATE_TIME,
                                a.BG_REASON,
                                a.HISTORY_STATUS,
                                a.BG_REG_NUMBER
                                
                                
                              FROM
                                T_BANK_GUARANTEE_HISTORY AS a
                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    $result =  $this->_db->fetchAll($selectQuery);
                    if (!empty($result)) {
                        $data['REASON'] = $result['0']['BG_REASON'];
                    }
                    $this->view->reqrepair = true;
                    $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $getInsName = $this->_db->select()
                                    ->from("M_CUSTOMER")
                                    ->where('CUST_ID = ?', $value['PS_FIELDVALUE'])
                                    ->query()->fetch();
                                // $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                                $this->view->insuranceName =   $getInsName['CUST_NAME'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }

                if (!empty($tbgdatadetail)) {
                    foreach ($tbgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->tinsuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->tPrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->tinsurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->tpaDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->tpaDateEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
                                $this->view->towner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner') {
                                $this->view->tamountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->towner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->tamountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->towner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->tamountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);

                $download = $this->_getParam('download');
                //print_r($edit);die;
                if ($download) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                }

                if ($data["COUNTER_WARRANTY_TYPE"] == "1" || $data["COUNTER_WARRANTY_TYPE"] == "2") {
                    switch ($data["USAGE_PURPOSE_DESC"]) {
                        case 'JAMINAN PENAWARAN':
                            $coa_provisi = '4671154PC96240';
                            break;
                        case 'JAMINAN UANG MUKA':
                            $coa_provisi = '4671154PC96260';
                            break;
                        case 'JAMINAN PELAKSANAAN':
                            $coa_provisi = '4671154PC96250';
                            break;
                        case 'JAMINAN PEMELIHARAAN':
                            $coa_provisi = '4671154PC96320';
                            break;
                        case 'JAMINAN PEMBAYARAN':
                            $coa_provisi = '4671154PC96330';
                            break;
                        default:
                            $coa_provisi = '0';
                            break;
                    }
                    $coa_administrasi = '4679484PC00000';
                } else {
                    switch ($data["USAGE_PURPOSE_DESC"]) {
                        case 'JAMINAN PENAWARAN':
                            $coa_provisi = '4671204PC96240';
                            break;
                        case 'JAMINAN UANG MUKA':
                            $coa_provisi = '4671204PC96260';
                            break;
                        case 'JAMINAN PELAKSANAAN':
                            $coa_provisi = '4671204PC96250';
                            break;
                        case 'JAMINAN PEMELIHARAAN':
                            $coa_provisi = '4671204PC96320';
                            break;
                        case 'JAMINAN PEMBAYARAN':
                            $coa_provisi = '4671204PC96330';
                            break;
                        default:
                            $coa_provisi = '0';
                            break;
                    }
                    $coa_administrasi = '4679484PC00000';
                }

                $this->view->coa_provisi1 = $coa_provisi;
                $this->view->coa_administrasi1 = $coa_administrasi;
                // $this->view->coa_meterai1 = "1899910PC81011";
                $this->view->coa_meterai1 = "2111271CC13220";
                $coa_materai = '211127113220';

                // Marginal Deposit View ------------------------
                // Marginal Deposit Principal ---------------
                $numb = $data['BG_REG_NUMBER'];
                $bgdatadetailmd = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                if (!empty($bgdatadetailmd)) {
                    foreach ($bgdatadetailmd as $key => $value) {
                        if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
                            $this->view->marginalDepositPercentage =   $value['PS_FIELDVALUE'];
                        }
                    }
                }
                // Marginal Deposit Principal ---------------

                // Marginal Deposit Eksisting ---------------
                $bgdatamdeks = $this->_db->select()
                    ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                    // ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                    // ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamdeks = $bgdatamdeks;
                // Marginal Deposit Eksisting ---------------

                // Top Up Marginal Deposit ---------------
                $bgdatamd = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('B.IS_TRANSFER != ?', '1')
                    ->where('B.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamd = $bgdatamd;
                // Top Up Marginal Deposit ---------------
                // Marginal Deposit View ------------------------

                if ($this->_request->isPost()) {
                    //var_dump($this->_request->getParams());
                    // die();
                    $redebate = $this->_getParam('redebate');

                    if ($redebate) {
                        // add atan
                        $change_id = $this->suggestionWaitingApproval('Request Rebate', "Request Rebate", $this->_changeType['code']['new'], null, 'TEMP_BANK_GUARANTEE', 'TEMP_REQUEST_REBATE', '-', '-', '-', '-');

                        $insertData = [
                            "CHANGES_ID" => $change_id,
                            "PROVISION_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam('REDEBATE_AMOUNT')),
                            "BG_REG_NUMBER" => $BG_NUMBER,
                            "REBATE_SUGGESTED" => new Zend_Db_Expr("now()"),
                            "REBATE_SUGGESTEDBY" => $this->_userIdLogin
                        ];

                        $this->_db->beginTransaction();
                        $this->_db->insert("TEMP_REQUEST_REBATE", $insertData);
                        $this->_db->commit();

                        $this->_db->update("T_GLOBAL_CHANGES", ["module" => "requestrebate"], ["CHANGES_ID = ?" => $change_id]);

                        $this->setbackURL('/signingstage/');
                        $this->_redirect('/notification/success/index');

                        // $data = array(
                        // 	//'REDEBATE_STATUS' => $this->_getParam('verifyindex'),
                        // 	'REDEBATE_TYPE' => $this->_getParam('redebate_type'),
                        // 	'REDEBATE_AMOUNT' => Application_Helper_General::convertDisplayMoney($this->_getParam('REDEBATE_AMOUNT'))
                        // );

                        // $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                        // $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);
                        // $this->setbackURL('/' . $this->_request->getModuleName() . '/verification/');
                        // $this->_redirect('/notification/success/index');
                    } else {
                        $prepare = $this->_getParam('prepare');
                        $get_all_params = $this->_request->getParams();

                        // if ($prepare) {
                        if ($prepare && !$check_error) {

                            $cutOffTime = $setting->getSetting('cut_off_persiapan_selesai');

                            if (strtotime(date('H:i:s')) > strtotime($cutOffTime)) { //lewat waktu
                                $flashMessanger->addMessage(["error" => $this->language->_('Telah melewati batas waktu operasional Bank (di pukul ' . $cutOffTime . ' WIB), silahkan lanjutkan pada hari kerja berikutnya.')]);

                                $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                            }

                            $this->_db->update('TEMP_BANK_GUARANTEE', ["IS_VALID" => 1, "PROVISION_FEE" => $biaya_provisi / 100, "STAMP_FEE" => $biaya_materai / 100, "ADM_FEE" => $biaya_administrasi / 100], ["BG_REG_NUMBER = ?" => $BG_NUMBER]);

                            $clientUser  =  new SGO_Soap_ClientUser();

                            // $totalamount = $data["PROVISION_FEE"] + $data["ADM_FEE"] + $data["STAMP_FEE"] + $principleData["Principle Insurance Premium"] + $principleData["Principle Insurance Administration"] + $principleData["Principle Insurance Stamp"];
                            // if ($totalamount > $resultbalance[['balance_hold']]) {
                            //     //die('here');
                            //     $err = true;
                            // }

                            $gagal = false;
                            // transfer coa --------------------------------------------------------------------

                            if ($data["COUNTER_WARRANTY_TYPE"] == "1" || $data["COUNTER_WARRANTY_TYPE"] == "2") {
                                switch ($data["USAGE_PURPOSE_DESC"]) {
                                    case 'JAMINAN PENAWARAN':
                                        $coa_provisi = '467115496240';
                                        break;
                                    case 'JAMINAN UANG MUKA':
                                        $coa_provisi = '467115496260';
                                        break;
                                    case 'JAMINAN PELAKSANAAN':
                                        $coa_provisi = '467115496250';
                                        break;
                                    case 'JAMINAN PEMELIHARAAN':
                                        $coa_provisi = '467115496320';
                                        break;
                                    case 'JAMINAN PEMBAYARAN':
                                        $coa_provisi = '467115496330';
                                        break;
                                    default:
                                        $coa_provisi = '0';
                                        break;
                                }
                                $coa_administrasi = '467948400000';
                            } else {
                                switch ($data["USAGE_PURPOSE_DESC"]) {
                                    case 'JAMINAN PENAWARAN':
                                        $coa_provisi = '467120496240';
                                        break;
                                    case 'JAMINAN UANG MUKA':
                                        $coa_provisi = '467120496260';
                                        break;
                                    case 'JAMINAN PELAKSANAAN':
                                        $coa_provisi = '467120496250';
                                        break;
                                    case 'JAMINAN PEMELIHARAAN':
                                        $coa_provisi = '467120496320';
                                        break;
                                    case 'JAMINAN PEMBAYARAN':
                                        $coa_provisi = '467120496330';
                                        break;
                                    default:
                                        $coa_provisi = '0';
                                        break;
                                }
                                $coa_administrasi = '467948400000';
                            }

                            // $this->view->coa_provisi = $coa_provisi;
                            // $this->view->coa_administrasi = $coa_provisi;
                            // $this->view->coa_meterai = "189991081011";

                            if (empty($data["IS_TRANSFER_COA"]) || !empty($data["IS_TRANSFER_COA"])) {

                                $cek_time_out = explode("|", $data["IS_TRANSFER_COA"])[0];
                                $svcTransferCoa = new SGO_Soap_ClientUser();


                                if ($data["COUNTER_WARRANTY_TYPE"] == "1" || $data["COUNTER_WARRANTY_TYPE"] == "2") {

                                    if ($cek_time_out == '2' || $cek_time_out == 2) {
                                        $svcCekStatus = new SGO_Soap_ClientUser();
                                        $svcCekStatus->callapi("inqtransfer", null, [
                                            "ref_trace" => explode("|", $data["IS_TRANSFER_COA"])[1],
                                            "trans_code" => "8774"
                                        ]);

                                        $status_result = $svcCekStatus->getResult();

                                        if ($status_result["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } else {
                                            $svcTransferCoa->callapi("transfercoa", null, [
                                                "source_account_currency" => "IDR",
                                                "source_account_number" => $data["FEE_CHARGE_TO"],
                                                "source_amount" => $total_biaya, //debit amount
                                                "beneficiary_account_currency" => "IDR",
                                                "beneficiary_account_number" => $coa_provisi,
                                                "branch_code" => $data["BG_BRANCH"],
                                                "beneficiary_amount" => $biaya_provisi, //credit amount
                                                "fee1" => $biaya_administrasi,
                                                "fee2" => $biaya_materai,
                                                "rate1" => "10000000",
                                                "rate2" => "10000000",
                                                "rate3" => "10000000",
                                                "rate4" => "10000000",
                                                "rate5" => "10000000",
                                                "remark1" => $data["BG_REG_NUMBER"],
                                                "remark2" => $data["BG_REG_NUMBER"],
                                                "remark3" => $data["BG_REG_NUMBER"],
                                                "gl_account1" => $coa_provisi,
                                                "gl_account2" => $coa_administrasi,
                                                "gl_account3" => $coa_materai
                                            ]);

                                            if ($svcTransferCoa->getResult()["response_code"] == "0000") {
                                                $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                    "IS_TRANSFER_COA" => "1",
                                                ], [
                                                    "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                ]);
                                            } elseif ($svcTransferCoa->getResult()["response_code"] == "9999") {
                                                $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                    "IS_TRANSFER_COA" => "2|" . $svcTransferCoa->getResult()["ref"],
                                                ], [
                                                    "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                ]);
                                                $gagal = true;
                                            } else {
                                                $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                    "IS_TRANSFER_COA" => "3",
                                                ], [
                                                    "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                ]);
                                                $gagal = true;
                                            }
                                        }
                                    } elseif ($cek_time_out == '3' || $cek_time_out == 3) {
                                        $svcTransferCoa->callapi("transfercoa", null, [
                                            "source_account_currency" => "IDR",
                                            "source_account_number" => $data["FEE_CHARGE_TO"],
                                            "source_amount" => $total_biaya, //debit amount
                                            "beneficiary_account_currency" => "IDR",
                                            "beneficiary_account_number" => $coa_provisi,
                                            "branch_code" => $data["BG_BRANCH"],
                                            "beneficiary_amount" => $biaya_provisi, //credit amount
                                            "fee1" => $biaya_administrasi,
                                            "fee2" => $biaya_materai,
                                            "rate1" => "10000000",
                                            "rate2" => "10000000",
                                            "rate3" => "10000000",
                                            "rate4" => "10000000",
                                            "rate5" => "10000000",
                                            "remark1" => $data["BG_REG_NUMBER"],
                                            "remark2" => $data["BG_REG_NUMBER"],
                                            "remark3" => $data["BG_REG_NUMBER"],
                                            "gl_account1" => $coa_provisi,
                                            "gl_account2" => $coa_administrasi,
                                            "gl_account3" => $coa_materai
                                        ]);

                                        if ($svcTransferCoa->getResult()["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } elseif ($svcTransferCoa->getResult()["response_code"] == "9999") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "2|" . $svcTransferCoa->getResult()["ref"],
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        } else {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "3",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        }
                                    }

                                    if (empty($data["IS_TRANSFER_COA"])) {
                                        $svcTransferCoa->callapi("transfercoa", null, [
                                            "source_account_currency" => "IDR",
                                            "source_account_number" => $data["FEE_CHARGE_TO"],
                                            "source_amount" => $total_biaya, //debit amount
                                            "beneficiary_account_currency" => "IDR",
                                            "beneficiary_account_number" => $coa_provisi,
                                            "branch_code" => $data["BG_BRANCH"],
                                            "beneficiary_amount" => $biaya_provisi, //credit amount
                                            "fee1" => $biaya_administrasi,
                                            "fee2" => $biaya_materai,
                                            "rate1" => "10000000",
                                            "rate2" => "10000000",
                                            "rate3" => "10000000",
                                            "rate4" => "10000000",
                                            "rate5" => "10000000",
                                            "remark1" => $data["BG_REG_NUMBER"],
                                            "remark2" => $data["BG_REG_NUMBER"],
                                            "remark3" => $data["BG_REG_NUMBER"],
                                            "gl_account1" => $coa_provisi,
                                            "gl_account2" => $coa_administrasi,
                                            "gl_account3" => $coa_materai
                                        ]);

                                        if ($svcTransferCoa->getResult()["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } elseif ($svcTransferCoa->getResult()["response_code"] == "9999") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "2|" . $svcTransferCoa->getResult()["ref"],
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        } else {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "3",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        }
                                    }
                                } else {
                                    if ($cek_time_out == '2' || $cek_time_out == 2) {
                                        $svcCekStatus = new SGO_Soap_ClientUser();
                                        $svcCekStatus->callapi("inqtransfer", null, [
                                            "ref_trace" => explode("|", $data["IS_TRANSFER_COA"])[1],
                                            "trans_code" => "8774"
                                        ]);

                                        $status_result = $svcCekStatus->getResult();
                                        if ($status_result["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } else {
                                            $svcTransferCoa->callapi("transfercoa", null, [
                                                "source_account_currency" => "IDR",
                                                "source_account_number" => $data["FEE_CHARGE_TO"],
                                                "source_amount" => $biaya_provisi + $biaya_administrasi + $biaya_materai, //debit amount
                                                "beneficiary_account_currency" => "IDR",
                                                "beneficiary_account_number" => $coa_provisi,
                                                "branch_code" => $data["BG_BRANCH"],
                                                "beneficiary_amount" => $biaya_provisi, //credit amount
                                                "fee1" => $biaya_administrasi,
                                                "fee2" => $biaya_materai,
                                                "rate1" => "10000000",
                                                "rate2" => "10000000",
                                                "rate3" => "10000000",
                                                "rate4" => "10000000",
                                                "rate5" => "10000000",
                                                "remark1" => $data["BG_REG_NUMBER"],
                                                "remark2" => $data["BG_REG_NUMBER"],
                                                "remark3" => $data["BG_REG_NUMBER"],
                                                "gl_account1" => $coa_provisi,
                                                "gl_account2" => $coa_administrasi,
                                                "gl_account3" => $coa_materai
                                            ]);

                                            if ($svcTransferCoa->getResult()["response_code"] == "0000") {
                                                $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                    "IS_TRANSFER_COA" => "1",
                                                ], [
                                                    "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                ]);
                                            } elseif ($svcTransferCoa->getResult()["response_code"] == "9999") {
                                                $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                    "IS_TRANSFER_COA" => "2|" . $svcTransferCoa->getResult()["ref"],
                                                ], [
                                                    "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                ]);
                                                $gagal = true;
                                            } else {
                                                $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                    "IS_TRANSFER_COA" => "3",
                                                ], [
                                                    "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                ]);
                                                $gagal = true;
                                            }
                                        }
                                    } elseif ($cek_time_out == '3' || $cek_time_out == 3) {
                                        $svcTransferCoa->callapi("transfercoa", null, [
                                            "source_account_currency" => "IDR",
                                            "source_account_number" => $data["FEE_CHARGE_TO"],
                                            "source_amount" => $biaya_provisi + $biaya_administrasi + $biaya_materai, //debit amount
                                            "beneficiary_account_currency" => "IDR",
                                            "beneficiary_account_number" => $coa_provisi,
                                            "branch_code" => $data["BG_BRANCH"],
                                            "beneficiary_amount" => $biaya_provisi, //credit amount
                                            "fee1" => $biaya_administrasi,
                                            "fee2" => $biaya_materai,
                                            "rate1" => "10000000",
                                            "rate2" => "10000000",
                                            "rate3" => "10000000",
                                            "rate4" => "10000000",
                                            "rate5" => "10000000",
                                            "remark1" => $data["BG_REG_NUMBER"],
                                            "remark2" => $data["BG_REG_NUMBER"],
                                            "remark3" => $data["BG_REG_NUMBER"],
                                            "gl_account1" => $coa_provisi,
                                            "gl_account2" => $coa_administrasi,
                                            "gl_account3" => $coa_materai
                                        ]);

                                        if ($svcTransferCoa->getResult()["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } elseif ($svcTransferCoa->getResult()["response_code"] == "9999") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "2|" . $svcTransferCoa->getResult()["ref"],
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        } else {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "3",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        }
                                    }

                                    if (empty($data["IS_TRANSFER_COA"])) {
                                        $svcTransferCoa->callapi("transfercoa", null, [
                                            "source_account_currency" => "IDR",
                                            "source_account_number" => $data["FEE_CHARGE_TO"],
                                            "source_amount" => $biaya_provisi + $biaya_administrasi + $biaya_materai, //debit amount
                                            "beneficiary_account_currency" => "IDR",
                                            "beneficiary_account_number" => $coa_provisi,
                                            "branch_code" => $data["BG_BRANCH"],
                                            "beneficiary_amount" => $biaya_provisi, //credit amount
                                            "fee1" => $biaya_administrasi,
                                            "fee2" => $biaya_materai,
                                            "rate1" => "10000000",
                                            "rate2" => "10000000",
                                            "rate3" => "10000000",
                                            "rate4" => "10000000",
                                            "rate5" => "10000000",
                                            "remark1" => $data["BG_REG_NUMBER"],
                                            "remark2" => $data["BG_REG_NUMBER"],
                                            "remark3" => $data["BG_REG_NUMBER"],
                                            "gl_account1" => $coa_provisi,
                                            "gl_account2" => $coa_administrasi,
                                            "gl_account3" => $coa_materai
                                        ]);

                                        if ($svcTransferCoa->getResult()["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } elseif ($svcTransferCoa->getResult()["response_code"] == "9999") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "2|" . $svcTransferCoa->getResult()["ref"],
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        } else {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_COA" => "3",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        }
                                    }
                                }

                                // if ($svcTransferCoa->isTimedOut()) {
                                //     $gagal = true;
                                //     $this->_db->update("TEMP_BANK_GUARANTEE", [
                                //         "TRANSFER_COA_TIMEOUT" => 1,
                                //     ], [
                                //         "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                //     ]);
                                // }
                            }

                            // -----------------------------------------------------------------------------------

                            // jika kontra insurance ---------------------------------------------------------------

                            if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
                                $svcSource = new Service_Account($data["FEE_CHARGE_TO"], "IDR");
                                $source_name = $svcSource->inquiryAccontInfo()["account_name"];
                                $source_phone = $svcSource->inquiryAccontInfo()["phone_number"];

                                $svcBenef = new Service_Account($insuranceBranch[0]["INS_BRANCH_ACCT"], "IDR");
                                $benef_name = $svcBenef->inquiryAccontInfo()["account_name"];
                                $benef_phone = $svcBenef->inquiryAccontInfo()["phone_number"];

                                $total_insurance = (round(floatval($principleData["Principle Insurance Premium"]), 2) * 100) + (round(floatval($principleData["Principle Insurance Administration"]), 2) * 100) + (round(floatval($principleData["Principle Insurance Stamp"]), 2) * 100);

                                $temp_params = [
                                    [
                                        "SOURCE_ACCOUNT_NUMBER" => $data["FEE_CHARGE_TO"],
                                        "SOURCE_ACCOUNT_NAME" => $source_name,
                                        "BENEFICIARY_ACCOUNT_NUMBER" => $insuranceBranch[0]["INS_BRANCH_ACCT"],
                                        "BENEFICIARY_ACCOUNT_NAME" => $benef_name,
                                        "PHONE_NUMBER_FROM" => $source_phone,
                                        "AMOUNT" => round(floatval($principleData["Principle Insurance Premium"]), 2),
                                        "PHONE_NUMBER_TO" => $benef_phone,
                                        "RATE1" => "10000000",
                                        "RATE2" => "10000000",
                                        "RATE3" => "10000000",
                                        "RATE4" => "10000000",
                                        "DESCRIPTION" => strval($data["BG_REG_NUMBER"] . " BeaPremi"),
                                        "DESCRIPTION_DETAIL" => substr($data["APPLICANT_NAME"], 0, 40),

                                    ], [
                                        "SOURCE_ACCOUNT_NUMBER" => $data["FEE_CHARGE_TO"],
                                        "SOURCE_ACCOUNT_NAME" => $source_name,
                                        "BENEFICIARY_ACCOUNT_NUMBER" => $insuranceBranch[0]["INS_BRANCH_ACCT"],
                                        "BENEFICIARY_ACCOUNT_NAME" => $benef_name,
                                        "PHONE_NUMBER_FROM" => $source_phone,
                                        "AMOUNT" => round(floatval($principleData["Principle Insurance Administration"]), 2),
                                        "PHONE_NUMBER_TO" => $benef_phone,
                                        "RATE1" => "10000000",
                                        "RATE2" => "10000000",
                                        "RATE3" => "10000000",
                                        "RATE4" => "10000000",
                                        "DESCRIPTION" => strval($data["BG_REG_NUMBER"] . " BeaAdm"),
                                        "DESCRIPTION_DETAIL" => substr($data["APPLICANT_NAME"], 0, 40),

                                    ], [
                                        "SOURCE_ACCOUNT_NUMBER" => $data["FEE_CHARGE_TO"],
                                        "SOURCE_ACCOUNT_NAME" => $source_name,
                                        "BENEFICIARY_ACCOUNT_NUMBER" => $insuranceBranch[0]["INS_BRANCH_ACCT"],
                                        "BENEFICIARY_ACCOUNT_NAME" => $benef_name,
                                        "PHONE_NUMBER_FROM" => $source_phone,
                                        "AMOUNT" => round(floatval($principleData["Principle Insurance Stamp"]), 2),
                                        "PHONE_NUMBER_TO" => $benef_phone,
                                        "RATE1" => "10000000",
                                        "RATE2" => "10000000",
                                        "RATE3" => "10000000",
                                        "RATE4" => "10000000",
                                        "DESCRIPTION" => strval($data["BG_REG_NUMBER"] . " BeaMeterai"),
                                        "DESCRIPTION_DETAIL" => substr($data["APPLICANT_NAME"], 0, 40),

                                    ],
                                ];

                                $cekZeroAmount = [round(floatval($principleData["Principle Insurance Premium"]), 2), round(floatval($principleData["Principle Insurance Administration"]), 2), round(floatval($principleData["Principle Insurance Stamp"]), 2)];

                                $cek_time_out = explode("|", $data["IS_TRANSFER_INS"])[0];

                                if ($cek_time_out == '2' || $cek_time_out == 2) {

                                    foreach ($temp_params as $key => $temp_param) {
                                        if ($cekZeroAmount[$key] == 0) {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);

                                            continue;
                                        }

                                        $temp_service =  new Service_TransferWithin($temp_param);
                                        $temp_result = $temp_service->sendTransfer();
                                        $temp_result = $temp_result["RawResult"];


                                        if ($temp_result["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } elseif ($temp_result["response_code"] == "9999") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "2|" . $svcTransferCoa->getResult()["ref"],
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        } else {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "3",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        }
                                    }
                                } elseif ($cek_time_out == '3' || $cek_time_out == 3) {

                                    foreach ($temp_params as $key => $temp_param) {

                                        if ($cekZeroAmount[$key] == 0) {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);

                                            continue;
                                        }

                                        $temp_service =  new Service_TransferWithin($temp_param);
                                        $temp_result = $temp_service->sendTransfer();

                                        $temp_result = $temp_result["RawResult"];

                                        if ($temp_result["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } elseif ($temp_result["response_code"] == "9999") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "2|" . $svcTransferCoa->getResult()["ref"],
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        } else {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "3",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        }
                                    }
                                }

                                if (empty($data["IS_TRANSFER_INS"])) {
                                    foreach ($temp_params as $key => $temp_param) {
                                        if ($cekZeroAmount[$key] == 0) {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);

                                            continue;
                                        }

                                        $temp_service =  new Service_TransferWithin($temp_param);
                                        $temp_result = $temp_service->sendTransfer();
                                        $temp_result = $temp_result["RawResult"];

                                        if ($temp_result["response_code"] == "0000") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "1",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                        } elseif ($temp_result["response_code"] == "9999") {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "2|" . $svcTransferCoa->getResult()["ref"],
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        } else {
                                            $this->_db->update("TEMP_BANK_GUARANTEE", [
                                                "IS_TRANSFER_INS" => "3",
                                            ], [
                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                            ]);
                                            $gagal = true;
                                        }
                                    }
                                }
                            }

                            // -------------------------------------------------------------------------------------


                            if (!empty($bgdatasplit) && ($data["COUNTER_WARRANTY_TYPE"] == "1" || $data["COUNTER_WARRANTY_TYPE"] == "3")) {

                                $bgdatasplitLower = array_map('strtolwer', array_column($bgdatasplit, 'ACCT_DESC'));
                                $bgdatasplitLower2 = array_map('strtolwer', array_column($bgdatasplit, 'ACCT_TYPE'));
                                if (array_search("Giro", array_column($bgdatasplit, "TYPE")) !== false || array_search("giro", $bgdatasplitLower) || array_search("giro", $bgdatasplitLower2) || array_search("d", $bgdatasplitLower)) {

                                    $check_escrow_account = $this->_db->select()
                                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                                        ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                                        ->where("A.ACCT_DESC = ?", "Escrow")
                                        ->query()->fetch();

                                    $exist_escrow_acct = false;

                                    if (empty($check_escrow_account)) {
                                        $requestopen = array();
                                        $requestopen['cif'] = $data["CUSTOMER_CIF"]; //account number
                                        // $requestopen['cif'] = "AE54413"; //account number
                                        $requestopen['branch_code'] = $data['BG_BRANCH'];
                                        // $requestopen['product_code'] = 'TB'; //$resultbalance['account_type'];
                                        $requestopen['product_code'] = 'K2'; //$resultbalance['account_type'];
                                        $requestopen['currency'] = 'IDR';

                                        $success = $clientUser->callapi('createaccount', null, $requestopen);
                                        $resultopen  = $clientUser->getResult();

                                        // if ($resultopen["response_code"] == "0050") {
                                        //     $flashMessanger->addMessage(["error" => $resultopen["response_desc"]]);
                                        //     $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                        // }

                                        if ($resultopen["response_code"] == "0000") {
                                            $exist_escrow_acct = true;
                                            $save_create_account = $resultopen["account_number"];

                                            $app      = Zend_Registry::get('config');
                                            $bankCode = $app['app']['bankcode'];

                                            $ccy = Application_Helper_General::getCurrNum('IDR');

                                            $svcAccount = new Service_Account($save_create_account, $ccy, $bankCode);
                                            $detailEscrow = $svcAccount->inquiryAccontInfo('AB', TRUE);

                                            $save_to_split = [
                                                "BG_REG_NUMBER" => $data["BG_REG_NUMBER"],
                                                "ACCT" => $save_create_account,
                                                "NAME" => $detailEscrow["account_name"],
                                                "ACCT_DESC" => "Escrow",
                                                "IS_TRANSFER" => 1
                                            ];

                                            $this->_db->insert("TEMP_BANK_GUARANTEE_SPLIT", $save_to_split);
                                        } else {
                                            $gagal = true;
                                        }
                                    } else {
                                        $save_create_account = $check_escrow_account["ACCT"];
                                        $app      = Zend_Registry::get('config');
                                        $bankCode = $app['app']['bankcode'];

                                        $ccy = Application_Helper_General::getCurrNum('IDR');

                                        $exist_escrow_acct = true;

                                        $svcAccount = new Service_Account($save_create_account, $ccy, $bankCode);
                                        $detailEscrow = $svcAccount->inquiryAccontInfo('AB', TRUE);
                                    }
                                }

                                $check_bg_split = $this->_db->select()
                                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                                    ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                                    ->where("LOWER(A.ACCT_DESC) != 'escrow' OR A.ACCT_DESC IS NULL OR LOWER(A.ACCT_TYPE) != 'escrow'")
                                    ->where("A.IS_TRANSFER IS NULL OR A.IS_TRANSFER = 0 OR A.IS_TRANSFER = 2 OR A.IS_TRANSFER = 3")
                                    ->query()->fetchAll();

                                foreach ($check_bg_split as $key => $value) {
                                    $temp_save = $this->_db->select()
                                        ->from("M_CUSTOMER_ACCT")
                                        ->where("ACCT_NO = ?", $value["ACCT"])
                                        ->query()->fetchAll();

                                    $check_bg_split[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
                                    $check_bg_split[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
                                }

                                if (!empty($check_bg_split)) {

                                    foreach ($check_bg_split as $value) {

                                        $svcBalance = new Service_Account($value["ACCT"], "IDR");
                                        $get_acct_type = $svcBalance->inquiryAccountBalance();

                                        $cek_time_out = explode("|", $value["IS_TRANSFER"])[0];

                                        if ($get_acct_type["response_code"] == "0000") {
                                            if ($get_acct_type["account_type"] == "D") {


                                                if ($cek_time_out == 1 || $cek_time_out == '1') {
                                                    continue;
                                                }

                                                if ($exist_escrow_acct !== true) {
                                                    continue;
                                                }

                                                $temp_param = [
                                                    "SOURCE_ACCOUNT_NUMBER" => $value["ACCT"],
                                                    "SOURCE_ACCOUNT_NAME" => $value["NAME"],
                                                    "BENEFICIARY_ACCOUNT_NUMBER" => $save_create_account,
                                                    "BENEFICIARY_ACCOUNT_NAME" => $detailEscrow["account_name"],
                                                    "PHONE_NUMBER_FROM" => $data["CUSTOMER_PHONE"],
                                                    "AMOUNT" => $value["AMOUNT"],
                                                    "PHONE_NUMBER_TO" => $detailEscrow["phone_number"],
                                                    "RATE1" => 0,
                                                    "RATE2" => 0,
                                                    "RATE3" => 0,
                                                    "RATE4" => 0,
                                                    "DESCRIPTION" => "Pemindahan Dana",
                                                    "DESCRIPTION_DETAIL" => "dari Giro ke Escrow",

                                                ];

                                                if ($cek_time_out == 2 || $cek_time_out == '2') {
                                                    // if (false) {
                                                    $temp_service =  new Service_TransferWithin($temp_param);
                                                    // $temp_result = $temp_service->sendTransfer();

                                                    $svcCekStatus = new SGO_Soap_ClientUser();
                                                    $svcCekStatus->callapi("inqtransfer", null, [
                                                        "ref_trace" => explode("|", $value["IS_TRANSFER"])[1],
                                                        "trans_code" => "8766"
                                                    ]);

                                                    $cekTrans = $svcCekStatus->getResult();

                                                    // if ($temp_result["RawResult"]["response_code"] != "0000") {
                                                    //     $gagal = true;
                                                    //     $flashMessanger->addMessage(["error" => $temp_result["RawResult"]["response_desc"]]);
                                                    //     return $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                                    // }

                                                    if ($cekTrans["response_code"] == "0000") {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "1"
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);

                                                        $get_escrow_amount = $this->_db->select()
                                                            ->from("TEMP_BANK_GUARANTEE_SPLIT", ["AMOUNT"])
                                                            ->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
                                                            ->query()->fetch();

                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "AMOUNT" => round($get_escrow_amount["AMOUNT"] + $value["AMOUNT"], 2)
                                                        ], [
                                                            // "BG_REG_NUMBER" => $data["BG_REG_NUMBER"],
                                                            "BG_REG_NUMBER = '" . $data["BG_REG_NUMBER"] . "' AND ACCT_DESC = 'Escrow'"
                                                        ]);
                                                    } else {
                                                        $temp_result = $temp_service->sendTransfer();

                                                        if ($temp_result["RawResult"]["response_code"] == "0000") {
                                                            $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                                "IS_TRANSFER" => "1"
                                                            ], [
                                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                                "ACCT = ?" => $value["ACCT"]
                                                            ]);

                                                            $get_escrow_amount = $this->_db->select()
                                                                ->from("TEMP_BANK_GUARANTEE_SPLIT", ["AMOUNT"])
                                                                ->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
                                                                ->query()->fetch();

                                                            $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                                "AMOUNT" => round($get_escrow_amount["AMOUNT"] + $value["AMOUNT"], 2)
                                                            ], [
                                                                // "BG_REG_NUMBER" => $data["BG_REG_NUMBER"],
                                                                "BG_REG_NUMBER = '" . $data["BG_REG_NUMBER"] . "' AND ACCT_DESC = 'Escrow'"
                                                            ]);
                                                        } elseif ($temp_result["RawResult"]["response_code"] == "9999") {
                                                            $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                                "IS_TRANSFER" => "2|" . $temp_result["RawResult"]["ref"],
                                                            ], [
                                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                                "ACCT = ?" => $value["ACCT"]
                                                            ]);
                                                            $gagal = true;
                                                        } else {

                                                            $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                                "IS_TRANSFER" => "3",
                                                            ], [
                                                                "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                                "ACCT = ?" => $value["ACCT"]
                                                            ]);
                                                            $gagal = true;
                                                        }
                                                    }
                                                    // }
                                                } elseif ($cek_time_out == 3 || $cek_time_out == '3') {
                                                    $temp_service =  new Service_TransferWithin($temp_param);
                                                    $temp_result = $temp_service->sendTransfer();

                                                    // if ($temp_result["RawResult"]["response_code"] != "0000") {
                                                    //     $gagal = true;
                                                    //     $flashMessanger->addMessage(["error" => $temp_result["RawResult"]["response_desc"]]);
                                                    //     return $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                                    // }

                                                    if ($temp_result["RawResult"]["response_code"] == "0000") {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "1"
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);

                                                        $get_escrow_amount = $this->_db->select()
                                                            ->from("TEMP_BANK_GUARANTEE_SPLIT", ["AMOUNT"])
                                                            ->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
                                                            ->query()->fetch();

                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "AMOUNT" => round($get_escrow_amount["AMOUNT"] + $value["AMOUNT"], 2)
                                                        ], [
                                                            // "BG_REG_NUMBER" => $data["BG_REG_NUMBER"],
                                                            "BG_REG_NUMBER = '" . $data["BG_REG_NUMBER"] . "' AND ACCT_DESC = 'Escrow'"
                                                        ]);
                                                    } elseif ($temp_result["RawResult"]["response_code"] == "9999") {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "2|" . $temp_result["RawResult"]["ref"],
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);
                                                        $gagal = true;
                                                    } else {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "3",
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);
                                                        $gagal = true;
                                                    }
                                                }

                                                if (empty($cek_time_out)) {
                                                    $temp_service =  new Service_TransferWithin($temp_param);
                                                    $temp_result = $temp_service->sendTransfer();


                                                    // if ($temp_result["RawResult"]["response_code"] != "0000") {
                                                    //     $gagal = true;
                                                    //     $flashMessanger->addMessage(["error" => $temp_result["RawResult"]["response_desc"]]);
                                                    //     return $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                                    // }

                                                    if ($temp_result["RawResult"]["response_code"] == "0000") {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "1"
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);

                                                        $get_escrow_amount = $this->_db->select()
                                                            ->from("TEMP_BANK_GUARANTEE_SPLIT", ["AMOUNT"])
                                                            ->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
                                                            ->where("ACCT_DESC = 'Escrow'")
                                                            ->query()->fetch();

                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "AMOUNT" => round($get_escrow_amount["AMOUNT"] + $value["AMOUNT"], 2)
                                                        ], [
                                                            // "BG_REG_NUMBER" => $data["BG_REG_NUMBER"],
                                                            "BG_REG_NUMBER = '" . $data["BG_REG_NUMBER"] . "' AND ACCT_DESC = 'Escrow'"
                                                        ]);
                                                    } elseif ($temp_result["RawResult"]["response_code"] == "9999") {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "2|" . $temp_result["RawResult"]["ref"],
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);
                                                        $gagal = true;
                                                    } else {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "3",
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);
                                                        $gagal = true;
                                                    }
                                                }
                                            }

                                            if ($get_acct_type["account_type"] == "S") {
                                                if ($cek_time_out == 1 || $cek_time_out == '1') {
                                                    continue;
                                                }

                                                $serviceCall = new Service_Account($value['ACCT'], 'IDR');
                                                $lockSaving = $serviceCall->lockSaving(['balance' => $value['AMOUNT']]);

                                                if ($lockSaving['response_code'] == '0000' || $lockSaving['response_code'] == '00') {
                                                    $saveSequence = $lockSaving['sequence_number'];
                                                    $saveHoldBranch = $lockSaving['hold_by_branch'];
                                                    $saveRemark = $lockSaving['remark'];

                                                    $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                        "IS_TRANSFER" => "1",
                                                        'HOLD_SEQUENCE' => $saveSequence,
                                                        'HOLD_BY_BRANCH' => $saveHoldBranch,
                                                        'HOLD_REMARK' => $saveRemark
                                                    ], [
                                                        "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                        "ACCT = ?" => $value["ACCT"],
                                                        "IS_TRANSFER != ?" => "1"
                                                    ]);
                                                } else {
                                                    $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                        "IS_TRANSFER" => "3",
                                                    ], [
                                                        "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                        "ACCT = ?" => $value["ACCT"]
                                                    ]);
                                                    $gagal = true;
                                                }
                                            }
                                        } else {
                                            if ($cek_time_out == 1 || $cek_time_out == '1') {
                                                continue;
                                            }
                                            $app      = Zend_Registry::get('config');
                                            $bankCode = $app['app']['bankcode'];

                                            $ccy = Application_Helper_General::getCurrNum('IDR');

                                            if ($cek_time_out == 2 || $cek_time_out == '2') {

                                                $svcAccount_temp = new Service_Account($value["ACCT"], $ccy, $bankCode, null, null, null, $this->_userIdLogin);
                                                $check_lock = $svcAccount_temp->inquiryDeposito();
                                                if ($check_lock["hold_description"] != "Y") {
                                                    $temp_lock_deposito = $svcAccount_temp->lockDeposito(["balance" => $value["AMOUNT"]]);

                                                    // if ($temp_lock_deposito["RawResult"]["response_code"] != "0000") {
                                                    //     $gagal = true;
                                                    //     $flashMessanger->addMessage(["error" => $temp_lock_deposito["RawResult"]["response_desc"]]);
                                                    //     return $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                                    // }

                                                    if ($temp_lock_deposito["response_code"] == "0000") {

                                                        $saveSequence = $temp_lock_deposito['sequence_number'];
                                                        $saveHoldBranch = $temp_lock_deposito['branch_code'];
                                                        $saveRemark = $temp_lock_deposito['remark'];

                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "1",
                                                            'HOLD_SEQUENCE' => $saveSequence,
                                                            'HOLD_BY_BRANCH' => $saveHoldBranch,
                                                            'HOLD_REMARK' => $saveRemark
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"],
                                                            "IS_TRANSFER != ?" => "1",
                                                        ]);
                                                    } elseif ($temp_lock_deposito["response_code"] == "9999") {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "2|" . $temp_lock_deposito["ref"]
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);
                                                        $gagal = true;
                                                    } else {
                                                        $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                            "IS_TRANSFER" => "3"
                                                        ], [
                                                            "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                            "ACCT = ?" => $value["ACCT"]
                                                        ]);
                                                        $gagal = true;
                                                    }

                                                    // if ($get_acct_type["response_code"] == "0001") {
                                                    //     $gagal = true;
                                                    //     $flashMessanger->addMessage(["error" => $get_acct_type["response_desc"]]);
                                                    //     $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                                    // }
                                                }
                                            } elseif ($cek_time_out == 3 || $cek_time_out == "3" || empty($cek_time_out)) {
                                                $svcAccount_temp = new Service_Account($value["ACCT"], $ccy, $bankCode, null, null, null, $this->_userIdLogin);
                                                $temp_lock_deposito = $svcAccount_temp->lockDeposito(["balance" => $value["AMOUNT"]]);

                                                // if ($temp_lock_deposito["RawResult"]["response_code"] != "0000") {
                                                //     $gagal = true;
                                                //     $flashMessanger->addMessage(["error" => $temp_lock_deposito["RawResult"]["response_desc"]]);
                                                //     return $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                                // }

                                                if ($temp_lock_deposito["response_code"] == "0000") {

                                                    $saveSequence = $temp_lock_deposito['sequence_number'];
                                                    $saveHoldBranch = $temp_lock_deposito['branch_code'];
                                                    $saveRemark = $temp_lock_deposito['remark'];

                                                    $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                        "IS_TRANSFER" => "1",
                                                        'HOLD_SEQUENCE' => $saveSequence,
                                                        'HOLD_BY_BRANCH' => $saveHoldBranch,
                                                        'HOLD_REMARK' => $saveRemark
                                                    ], [
                                                        "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                        "ACCT = ?" => $value["ACCT"],
                                                        "IS_TRANSFER != ?" => "1",
                                                    ]);
                                                } elseif ($temp_lock_deposito["response_code"] == "9999") {
                                                    $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                        "IS_TRANSFER" => "2|" . $temp_lock_deposito["ref"]
                                                    ], [
                                                        "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                        "ACCT = ?" => $value["ACCT"]
                                                    ]);
                                                    $gagal = true;
                                                } else {
                                                    $this->_db->update("TEMP_BANK_GUARANTEE_SPLIT", [
                                                        "IS_TRANSFER" => "3"
                                                    ], [
                                                        "BG_REG_NUMBER = ?" => $data["BG_REG_NUMBER"],
                                                        "ACCT = ?" => $value["ACCT"]
                                                    ]);
                                                    $gagal = true;
                                                }

                                                // if ($get_acct_type["response_code"] == "0001") {
                                                //     $gagal = true;
                                                //     $flashMessanger->addMessage(["error" => $get_acct_type["response_desc"]]);
                                                //     $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                                // }
                                            }
                                        }
                                    }
                                }
                            }

                            //var_dump($resultopen);die;

                            $gen_bg_number = $this->generateTransactionID($data["BG_BRANCH"], $data["USAGE_PURPOSE"]);

                            if ($bgdata[0]["BG_FORMAT"] == '1') {
                                if ($bgdata['0']['BG_LANGUAGE'] == '3') {
                                    $outputHTML = "<tr><td>" . $this->view->render('/detail/indexpdfbi.phtml') . "</td></tr>";
                                } elseif ($bgdata['0']['BG_LANGUAGE'] == '2') {
                                    $outputHTML = "<tr><td>" . $this->view->render('/detail/indexpdfen.phtml') . "</td></tr>";
                                } else {
                                    $outputHTML = "<tr><td>" . $this->view->render('/detail/indexpdf.phtml') . "</td></tr>";
                                }

                                $tempdir = APPLICATION_PATH . "/../public/QRImages/";

                                $teks_qrcode    = 'http://' . $_SERVER['HTTP_HOST'] . "/bgchecking/detail/index/bgnumb/" . $bgdata['0']['BG_NUMBER'];
                                $namafile       = $bgdata['0']['BG_NUMBER'] . ".png";
                                $quality        = "H";
                                $ukuran         = 5;
                                $padding        = 1;
                                $qrImage        = $tempdir . $namafile;

                                $path  = APPLICATION_PATH . '/../public/images/image001.jpg';
                                $imageData = base64_encode(file_get_contents($path));
                                $image = $path;
                                $srcbank = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;
                                $terbilangs = $this->Terbilang($data['BG_AMOUNT']);
                                $terbilang_en = $this->Terbilangen($data['BG_AMOUNT']);

                                $arrPURPOSE = array(
                                    1 => 'Bidding Guarantee',
                                    2 => 'Advance Payment Guarantee',
                                    3 => 'Performance Bond',
                                    4 => 'Maintenance Guarantee',
                                    5 => 'Others',
                                    6 => 'Payment Guarantee',
                                );

                                $USAGE_PURPOSE = array();
                                if (!empty($data['USAGE_PURPOSE'])) {
                                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                                        $USAGE_PURPOSE[] = $arrPURPOSE[$val];
                                    }
                                }

                                $get_address = $this->_db->select()
                                    ->from(["MC" => "M_CUSTOMER"])
                                    ->joinLeft(["MCITY" => "M_CITYLIST"], "MC.CUST_CITY = MCITY.CITY_CODE", ["CITY_NAME"])
                                    ->where("MC.CUST_ID = ?", $data["CUST_ID"])
                                    ->query()->fetch();

                                $format_address = $get_address["CUST_ADDRESS"] . ", " . $get_address["CUST_DISTRICT"] . ", " . $get_address["CITY_NAME"] . " " . $get_address["CUST_ZIP"];

                                $hitung_hari = date_diff(date_create($data['TIME_PERIOD_START']), date_create($data['TIME_PERIOD_END']))->format("%a") + 1;

                                $get_ttd_detail1 = $this->_db->select()
                                    ->from("M_BUSER")
                                    // ->where("BUSER_ID = '" . $data["TTD1_USERID"] . "' OR BUSER_ID = '" . $data["TTD2_USERID"] . "'")
                                    ->where("BUSER_ID IN (?)", [$this->_request->getParam("ttd1userid")])
                                    ->query()->fetch();

                                $get_ttd_detail2 = $this->_db->select()
                                    ->from("M_BUSER")
                                    // ->where("BUSER_ID = '" . $data["TTD1_USERID"] . "' OR BUSER_ID = '" . $data["TTD2_USERID"] . "'")
                                    ->where("BUSER_ID IN (?)", [$this->_request->getParam("ttd2userid")])
                                    ->query()->fetch();

                                $getUnderlying = $this->_db->select()
                                    ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
                                    ->where('BG_REG_NUMBER = ?', $BG_NUMBER)
                                    ->query()->fetchAll();

                                $saveText = [];
                                foreach ($getUnderlying as $key => $value) {
                                    $tempSave = implode(' ', [$value['DOC_NAME'], 'Nomor ' . $value['DOC_NUMBER'], 'tanggal ' . $this->indodate(strval(date('Y-m-d', strtotime($value['DOC_DATE']))))]);
                                    $saveText[] = $tempSave;
                                }
                                $finalText = implode(' ; ', $saveText);

                                $translate = array(
                                    '[[qrcode]]'             => "",
                                    '[[bankimage]]'            => "",
                                    '[[bg_numb]]'            => $gen_bg_number,
                                    '[[date_start]]'        => $data['TIME_PERIOD_START'],
                                    '[[subject]]'           => $data["BG_SUBJECT"],
                                    '[[city]]'        => $data['RECIPIENT_CITY'],
                                    '[[benef_name]]'        => $data['RECIPIENT_NAME'],
                                    '[[benef_add]]'        => $data['RECIPIENT_ADDRES'],
                                    '[[benef_city]]'        => $data['RECIPIENT_CITY'],
                                    // '[[service]]'        => $getUnderlying[0]['DOC_NAME'],
                                    '[[service]]'        => $finalText,
                                    // '[[gt_doc_number]]'        => $data['GT_DOC_NUMBER'],
                                    '[[gt_doc_number]]'        => $getUnderlying[0]['DOC_NUMBER'],
                                    '[[tipe_jaminan]]'        => $data['USAGE_PURPOSE_DESC'],
                                    '[[tipe_jaminan2]]'        => ucwords(strtolower($data['USAGE_PURPOSE_DESC'])),
                                    '[[bank_branch_address]]'        => $data['BANK_ADDRESS'],
                                    '[[tanggal_terbit]]'        => $this->indodate(date("Y-m-d")),
                                    '[[app_name]]'        => $selectcomp[0]['CUST_NAME'],
                                    '[[app_add]]'        => $selectcomp[0]['CUST_ADDRESS'],
                                    '[[hitung_hari]]'      => trim($this->Terbilang($hitung_hari)),
                                    '[[local_pn]]'      => $data["LOCAL_PN"],
                                    // '[[local_pn]]'      => ucwords(strtolower($data["LOCAL_PN"])),
                                    '[[hitung_hari_angka]]'      => $hitung_hari,
                                    '[[bank_branch]]'      => $selectbranch[0]['BRANCH_NAME'],
                                    // '[[app_city]]'        => $selectcomp[0]['CITY_NAME'],
                                    '[[app_city]]'        => $format_address,
                                    '[[amount]]'        => number_format($data['BG_AMOUNT'], 2, ',', '.'),
                                    '[[terbilang]]'        => trim($terbilangs),
                                    '[[terbilang_en]]'        => strtolower(trim($terbilang_en)),
                                    '[[guarantee_letter]]'        => $data['GUARANTEE_TRANSACTION'],
                                    '[[gt_doc_date]]'        => $this->indodate(strval(date('Y-m-d', strtotime($getUnderlying[0]['DOC_DATE'])))),
                                    '[[date_start]]'        => $this->indodate($data['TIME_PERIOD_START']),
                                    '[[date_start_en]]'        => date("M j\<\s\u\p\>S\<\/\s\u\p\> Y", strtotime($data['TIME_PERIOD_START'])),
                                    '[[date_end]]'        => $this->indodate($data['TIME_PERIOD_END']),
                                    '[[date_end_en]]'        => date("M j\<\s\u\p\>S\<\/\s\u\p\> Y", strtotime($data['TIME_PERIOD_END'])),
                                    '[[usage_purpose]]'        => implode(", ", $USAGE_PURPOSE),
                                    '[[ttd1_nama]]' => strtoupper($get_ttd_detail2['BUSER_NAME']),
                                    '[[ttd2_nama]]' => strtoupper($get_ttd_detail1['BUSER_NAME']),

                                );

                                $outputHTMLnew = strtr($outputHTML, $translate);

                                $file_name_with_full_path = $this->_helper->download->newPdfblankDownload(null, null, null, "BG_NO_" . $gen_bg_number, $outputHTMLnew);
                            } else {
                                $path = UPLOAD_PATH . '/document/submit/';
                                $file_name_with_full_path = $bgdata[0]["AGREE_FORMAT"];

                                // var_dump($file_name_with_full_path);
                                // die();

                                $tempNamaFile = str_replace(".pdf", "_temp.pdf", $file_name_with_full_path);

                                rename($path . $file_name_with_full_path, $path . $tempNamaFile);

                                $command = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.6  -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile='" . $path . $file_name_with_full_path . "' '" . $path . $tempNamaFile . "'";
                                shell_exec($command);

                                @unlink($path . $tempNamaFile);

                                $file_name_with_full_path = $path . $file_name_with_full_path;
                            }

                            // die();
                            if (function_exists('curl_file_create')) { // php 5.5+
                                $cFile = curl_file_create($file_name_with_full_path, mime_content_type($file_name_with_full_path), "BG_NO_" . $gen_bg_number . ".pdf");
                            } else { // 
                                $cFile = '@' . realpath($file_name_with_full_path);
                            }

                            $requestcheck = [];

                            $requestcheck["document"] = $cFile;
                            // $requestcheck["document"] = $cFile;

                            $ttd1hal = $this->_request->getParam("ttd1hal");
                            $ttd1hal = str_replace(" ", "", $ttd1hal);
                            $ttd1hal = explode(",", $ttd1hal);

                            $ttd1detail = [];
                            foreach ($ttd1hal as $key => $value) {
                                $temp = [
                                    "p" => $value,
                                    "x" => $this->_request->getParam("ttd1posisix"),
                                    "y" => $this->_request->getParam("ttd1posisiy"),
                                    "w" => $this->_request->getParam("ttd1ukuranl"),
                                    "h" => $this->_request->getParam("ttd1ukuranp"),
                                ];

                                array_push($ttd1detail, $temp);
                            }

                            $ttd2hal = $this->_request->getParam("ttd2hal");
                            $ttd2hal = str_replace(" ", "", $ttd2hal);
                            $ttd2hal = explode(",", $ttd2hal);

                            $ttd2detail = [];
                            foreach ($ttd2hal as $key => $value) {
                                $temp = [
                                    "p" => $value,
                                    "x" => $this->_request->getParam("ttd2posisix"),
                                    "y" => $this->_request->getParam("ttd2posisiy"),
                                    "w" => $this->_request->getParam("ttd2ukuranl"),
                                    "h" => $this->_request->getParam("ttd2ukuranp"),
                                ];

                                array_push($ttd2detail, $temp);
                            }

                            $requestcheck["signature"] = json_encode([
                                [
                                    "email" => $this->_request->getParam("ttd1useremail"),
                                    "detail" => [
                                        [
                                            'visible_signature' => 0
                                        ]
                                    ],
                                    // "detail" => $ttd1detail,
                                ],
                                [
                                    "email" => $this->_request->getParam("ttd2useremail"),
                                    "detail" => [
                                        [
                                            'visible_signature' => 0
                                        ]
                                    ],
                                    // "detail" => $ttd2detail,
                                ]
                            ]);

                            if ($this->_request->getParam("berurutan")) {
                                $requestcheck["is_in_order"] = "1";
                            }

                            $qr_page_number = ($bgdata[0]["BG_FORMAT"] == '1' && $bgdata['0']['BG_LANGUAGE'] == '3') ? 2 : $this->_request->getParam("qrhalaman");

                            if ($this->_request->getParam("qrposisi") != "ds") {
                                $requestcheck["show_qrcode"] = "1";
                                $requestcheck["qrcode_position"] = strval($this->_request->getParam("qrposisi"));
                                $requestcheck["qrcode_page"] = "page_number";
                                $requestcheck["page_number"] = strval($qr_page_number);
                                $requestcheck["qrcode_size"] = strval($this->_request->getParam("qrsize"));
                            }

                            $requestcheck['expiration_date'] = date('Y-m-d', strtotime(date('Y-m-d') . '+1 day'));

                            // $requestcheck["ematerai"] = json_encode([
                            //     "document_type" => "Surat Berharga",
                            //     "details" => [
                            //         [
                            //             "x" => intval(str_replace(" ", "", $this->_request->getParam("emeteraiposisix") - 20)), // default158
                            //             "y" => intval(str_replace(" ", "", $this->_request->getParam("emeteraiposisiy"))),
                            //             "page" => intval($this->_request->getParam("emeteraihal")),
                            //         ]
                            //     ]
                            // ]);

                            // $requestcheck["estamp"] = json_encode([
                            //     [
                            //         "email" => $this->_request->getParam("ttd1useremail"),
                            //         "details" => [
                            //             "p" => str_replace(" ", "", $this->_request->getParam("emeteraihal")),
                            //             "x" => str_replace(" ", "", $this->_request->getParam("emeteraiposisix")),
                            //             "y" => str_replace(" ", "", $this->_request->getParam("emeteraiposisiy")),
                            //             "w" => str_replace(" ", "", $this->_request->getParam("emeteraiukuranl")),
                            //             "h" => str_replace(" ", "", $this->_request->getParam("emeteraiukuranp")),
                            //         ]
                            //     ],
                            //     [
                            //         "email" => $this->_request->getParam("ttd2useremail"),
                            //         "details" => [
                            //             "p" => str_replace(" ", "", $this->_request->getParam("emeteraihal")),
                            //             "x" => str_replace(" ", "", $this->_request->getParam("emeteraiposisix")),
                            //             "y" => str_replace(" ", "", $this->_request->getParam("emeteraiposisiy")),
                            //             "w" => str_replace(" ", "", $this->_request->getParam("emeteraiukuranl")),
                            //             "h" => str_replace(" ", "", $this->_request->getParam("emeteraiukuranp")),
                            //         ]
                            //     ]
                            // ]);

                            if (!$gagal) {
                                $success = $clientUser->callapi('uploadDS', $requestcheck);
                                $success = $clientUser->getResult();

                                $save_response = [];
                                if (strval($success->status) === "OK") {
                                    $decode_signature = json_decode($requestcheck["signature"]);

                                    foreach ($decode_signature as $key => $value) {
                                        $signDsArr = [];
                                        $signDsArr["req_id"] = $success->data->req_id;
                                        $signDsArr["email"] = $value->email;
                                        $signDsArr["document_id"] = $success->data->id;
                                        $temp_response = $clientUser->callapi('signDS', $signDsArr);
                                        array_push($save_response, $temp_response);
                                    }

                                    // var_dump($success);
                                    // die();

                                    // TEMP BANK GUARANTEE

                                    // $get_TEMP_BANK_GUARANTEE = $this->_db->select()
                                    //     ->from("TEMP_BANK_GUARANTEE")
                                    //     ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                                    //     ->query()->fetchAll();

                                    // $get_TEMP_BANK_GUARANTEE = $get_TEMP_BANK_GUARANTEE[0];
                                    $get_TEMP_BANK_GUARANTEE = [];

                                    // $get_TEMP_BANK_GUARANTEE["REBATE_STATUS"] = $get_TEMP_BANK_GUARANTEE["REDEBATE_STATUS"];
                                    // unset($get_TEMP_BANK_GUARANTEE["REDEBATE_STATUS"]);
                                    // $get_TEMP_BANK_GUARANTEE["REBATE_AMOUNT"] = $get_TEMP_BANK_GUARANTEE["REDEBATE_AMOUNT"];
                                    // unset($get_TEMP_BANK_GUARANTEE["REDEBATE_AMOUNT"]);
                                    // $get_TEMP_BANK_GUARANTEE["REBATE_TYPE"] = $get_TEMP_BANK_GUARANTEE["REDEBATE_TYPE"];
                                    // unset($get_TEMP_BANK_GUARANTEE["REDEBATE_TYPE"]);

                                    // $get_TEMP_BANK_GUARANTEE["BG_STATUS"] = 15;
                                    // $get_TEMP_BANK_GUARANTEE["BG_NUMBER"] = $this->generateTransactionID($get_TEMP_BANK_GUARANTEE["BG_BRANCH"], $get_TEMP_BANK_GUARANTEE["USAGE_PURPOSE"]);
                                    $get_TEMP_BANK_GUARANTEE["DOCUMENT_ID"] = $success->data->id;
                                    $get_TEMP_BANK_GUARANTEE["SAVE_LINK"] = $save_response[0]->data->url . " -- " . $save_response[1]->data->url;

                                    $get_TEMP_BANK_GUARANTEE["BG_NUMBER"] = $gen_bg_number;

                                    $get_TEMP_BANK_GUARANTEE["STAMP_PAGE"] = $this->_request->getParam("emateraihal");
                                    $get_TEMP_BANK_GUARANTEE["STAMP_COORDINATES_X"] = $this->_request->getParam("emateraiposisix");
                                    $get_TEMP_BANK_GUARANTEE["STAMP_COORDINATES_Y"] = $this->_request->getParam("emateraiposisiy");

                                    $get_TEMP_BANK_GUARANTEE["TTD1_PAGE"] = $this->_request->getParam("ttd1hal");
                                    $get_TEMP_BANK_GUARANTEE["TTD1_SIZE_P"] = $this->_request->getParam("ttd1ukuranp");
                                    $get_TEMP_BANK_GUARANTEE["TTD1_SIZE_L"] = $this->_request->getParam("ttd1ukuranl");
                                    $get_TEMP_BANK_GUARANTEE["TTD1_COORDINATES_X"] = $this->_request->getParam("ttd1posisix");
                                    $get_TEMP_BANK_GUARANTEE["TTD1_COORDINATES_Y"] = $this->_request->getParam("ttd1posisiy");
                                    $get_TEMP_BANK_GUARANTEE["TTD1_USERID"] = $this->_request->getParam("ttd1userid");

                                    $get_TEMP_BANK_GUARANTEE["TTD2_PAGE"] = $this->_request->getParam("ttd2hal");
                                    $get_TEMP_BANK_GUARANTEE["TTD2_SIZE_P"] = $this->_request->getParam("ttd2ukuranp");
                                    $get_TEMP_BANK_GUARANTEE["TTD2_SIZE_L"] = $this->_request->getParam("ttd2ukuranl");
                                    $get_TEMP_BANK_GUARANTEE["TTD2_COORDINATES_X"] = $this->_request->getParam("ttd2posisix");
                                    $get_TEMP_BANK_GUARANTEE["TTD2_COORDINATES_Y"] = $this->_request->getParam("ttd2posisiy");
                                    $get_TEMP_BANK_GUARANTEE["TTD2_USERID"] = $this->_request->getParam("ttd2userid");

                                    $get_TEMP_BANK_GUARANTEE["TTD_SEQUENTIAL"] = 0;

                                    $get_TEMP_BANK_GUARANTEE["BG_STATUS"] = 17;

                                    if ($this->_request->getParam("berurutan")) {
                                        $get_TEMP_BANK_GUARANTEE["TTD_SEQUENTIAL"] = 1;
                                    }

                                    $get_TEMP_BANK_GUARANTEE["BG_UPDATED"] = new Zend_Db_Expr('now()');
                                    $get_TEMP_BANK_GUARANTEE["BG_UPDATEDBY"] = strval($this->_userIdLogin);

                                    $this->_db->update('TEMP_BANK_GUARANTEE', $get_TEMP_BANK_GUARANTEE, ["BG_REG_NUMBER = ?" => $BG_NUMBER]);

                                    // $this->_db->insert("T_BANK_GUARANTEE", $get_TEMP_BANK_GUARANTEE);


                                    // TEMP BANK GUARANTEE DETAIL
                                    // $get_TEMP_BANK_GUARANTEE_DETAIL = $this->_db->select()
                                    //     ->from("TEMP_BANK_GUARANTEE_DETAIL")
                                    //     ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                                    //     ->query()->fetchAll();

                                    // if (count($get_TEMP_BANK_GUARANTEE_DETAIL) > 0) {
                                    //     foreach ($get_TEMP_BANK_GUARANTEE_DETAIL as $key => $value) {
                                    //         $this->_db->insert("T_BANK_GUARANTEE_DETAIL", $value);
                                    //     }
                                    // }

                                    // TEMP BANK GUARANTEE FILE
                                    // $get_TEMP_BANK_GUARANTEE_FILE = $this->_db->select()
                                    //     ->from("TEMP_BANK_GUARANTEE_FILE")
                                    //     ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                                    //     ->query()->fetchAll();

                                    // if (count($get_TEMP_BANK_GUARANTEE_FILE) > 0) {
                                    //     foreach ($get_TEMP_BANK_GUARANTEE_FILE as $key => $value) {
                                    //         $value["BG_NUMBER"] = $get_TEMP_BANK_GUARANTEE["BG_NUMBER"];
                                    //         $this->_db->insert("T_BANK_GUARANTEE_FILE", $value);
                                    //     }
                                    // }

                                    // TEMP BANK GUARANTEE SPLIT
                                    // $get_TEMP_BANK_GUARANTEE_SPLIT = $this->_db->select()
                                    //     ->from("TEMP_BANK_GUARANTEE_SPLIT")
                                    //     ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
                                    //     ->query()->fetchAll();

                                    // if (count($get_TEMP_BANK_GUARANTEE_SPLIT) > 0) {
                                    //     foreach ($get_TEMP_BANK_GUARANTEE_SPLIT as $key => $value) {
                                    //         $value["BG_NUMBER"] = $get_TEMP_BANK_GUARANTEE["BG_NUMBER"];
                                    //         $this->_db->insert("T_BANK_GUARANTEE_SPLIT", $value);
                                    //     }
                                    // }

                                    $historyInsert = array(
                                        'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                        'BG_REG_NUMBER'         => $BG_NUMBER,
                                        'CUST_ID'           => $this->_custIdLogin,
                                        'USER_LOGIN'        => $this->_userIdLogin,
                                        'HISTORY_STATUS'    => 17
                                    );

                                    $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                                    Application_Helper_General::writeLog('SBGP', "Signing Preparation Completed, for BG Number : " . $gen_bg_number . " Principal : " . $bgdata[0]["APPLICANT_NAME"] . " (" . $bgdata[0]["CUST_ID"] . ")");

                                    $this->setbackURL('/' . $this->_request->getModuleName() . '/list/');
                                    return $this->_redirect('/notification/success/index');
                                } else {
                                    if (isset($success->status)) {
                                        $flashMessanger->addMessage(["error" => $success->code . " " . (gettype($success->message) === "array" ? join(",", $success->message) : $success->message)]);
                                    } else {
                                        $flashMessanger->addMessage(["error" => $success->rspdesc ?: $success->message]);
                                    }
                                    // if (gettype($success->message) === "string") $flashMessanger->addMessage(["error" => $success->code . " " . $success->message]);
                                    // else $flashMessanger->addMessage(["error" => gettype($success->message[0]) === 'array' ? $success->rspdesc : $success->message[0]]);

                                    $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                                }
                            } else {
                                $this->_redirect("/signingstage/detail/index/bgnumb/" . urlencode($this->_request->getParam("bgnumb")));
                            }
                        }
                    }

                    $back = $this->_getParam('back');
                    if ($back) {
                        $this->_redirect('/eformworkflow/approve');
                    }
                }
            }
        }
    }


    public function validateapproval($policy, $list, $bgnumb)
    {
        die('a');
        $command = ' ' . $policy . ' ';
        $command = strtoupper($command);

        $cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
        var_dump($cleanCommand);
        $commandspli = explode('THEN', $cleanCommand);
        $commandnew = '';
        foreach ($commandspli as $ky => $valky) {
            if ($commandspli[$ky + 1] != '') {
                $commandnew .= '(' . $valky . ') THEN ';
            } else {
                $commandnew .= '(' . $valky . ')';
            }
        }

        //transform to php logical operator syntak
        $translate = array(
            'AND' => '&&',
            'OR' => '||',
            'THEN' => '&&',
            'A' => '$A',
            'B' => '$B',
            'C' => '$C',
            'D' => '$D',
            'E' => '$E',
            'F' => '$F',
            'G' => '$G',
            'H' => '$H',
            'I' => '$I',
            'J' => '$J',
            'K' => '$K',
            'L' => '$L',
            'M' => '$M',
            'N' => '$N',
            'O' => '$O',
            'P' => '$P',
            'Q' => '$Q',
            'R' => '$R',
            // 'S' => '$S',
            'T' => '$T',
            'U' => '$U',
            'V' => '$V',
            'W' => '$W',
            'X' => '$X',
            'Y' => '$Y',
            'Z' => '$Z',
            'SG' => '$SG',
        );

        $phpCommand =  strtr($commandnew, $translate);

        $param = array(
            '0' => '$A',
            '1' => '$B',
            '2' => '$C',
            '3' => '$D',
            '4' => '$E',
            '5' => '$F',
            '6' => '$G',
            '7' => '$H',
            '8' => '$I',
            '9' => '$J',
            '10' => '$K',
            '11' => '$L',
            '12' => '$M',
            '13' => '$N',
            '14' => '$O',
            '15' => '$P',
            '16' => '$Q',
            '17' => '$R',
            // '18' => '$S',
            '19' => '$T',
            '20' => '$U',
            '21' => '$V',
            '22' => '$W',
            '23' => '$X',
            '24' => '$Y',
            '25' => '$Z',
            '26' => '$SG',
        );
        // print_r($phpCommand);die;
        // function str_replace_first($from, $to, $content,$row)
        // {
        //     $from = '/'.preg_quote($from, '/').'/';
        //     return preg_replace($from, $to, $content, $row);
        // }

        $command = str_replace('(', '', $policy);
        $command = str_replace(')', '', $command);
        $list = explode(' ', $command);
        // print_r($list);die;
        $approver = array();
        foreach ($list as $key => $value) {
            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))

                    ->where("C.REG_NUMBER = ?", (string) $bgnumb)
                    ->where("C.GROUP = ?", (string) $value);
                // echo $selectapprover;die;
                $usergroup = $this->_db->fetchAll($selectapprover);
                // print_r($usergroup);
                $approver[$value] = $usergroup;
            }
        }
        // print_r($approver);die;
        foreach ($param as $url) {
            if (strpos($phpCommand, $url) !== FALSE) {
                $ta = substr_count($phpCommand, $url);
                // print_r($list);die;

                if (!empty($approver)) {
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                            foreach ($approver[$value] as $row => $val) {
                                if (!empty($val)) {
                                    $values = 'G' . $value;
                                    ${$values}[$row + 1] = true;
                                }
                            }
                        }
                    }
                }
                for ($i = 1; $i <= $ta; $i++) {
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                            $values = 'G' . $value;
                            if (empty(${$values}[$i])) {
                                ${$values}[$i] = false;
                            }
                        }
                    }

                    $BG_NUMBER = $i;
                    $label = str_replace('$', '$G', $url);

                    $replace = $label . '[' . $BG_NUMBER . ']';

                    $alf = str_replace('$', '', $url);
                    $values = 'G' . $alf;

                    if (${$values}[$i] == $replace) {
                        $phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
                        //	 print_r($phpCommand);
                    } else {
                        $phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
                        //	 print_r($phpCommand);
                    }
                }
            }
        }
        $keywords = preg_split("/[\s,]+/", $cleanCommand);
        $result =  false;
        //print_r($phpCommand);
        eval('$result = ' . "$phpCommand;");
        //var_dump ($result);die;
        return $result;
    }



    public function findPolicyBoundary($transfertype, $amount)
    {


        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BGBOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        //echo $selectuser;die;
        $datauser = $this->_db->fetchAll($selectuser);

        return $datauser[0]['POLICY'];
    }

    //return tombol jika blm ada yg approve
    public function findUserBoundary($transfertype, $amount)
    {



        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        //echo $selectuser;die();
        $datauser = $this->_db->fetchAll($selectuser);

        $command = str_replace('(', '', $datauser[0]['POLICY']);
        $command = str_replace(')', '', $command);
        $command = $command . ' SG';
        $list = explode(' ', $command);

        $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

        $flipAlphabet = array_flip($alphabet);

        foreach ($list as $row => $data) {
            foreach ($alphabet as $key => $value) {
                if ($data == $value) {
                    $groupuser[] = $flipAlphabet[$data];
                }
            }
        }

        $uniqueGroupUser = array_unique($groupuser);

        foreach ($uniqueGroupUser as $key => $value) {
            if ($value == '27') {
                $selectGroupName    = $this->_db->select()
                    ->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
                    ->where("C.GROUP_BUSER_ID LIKE ?", '%S_%');
            } else {
                $selectGroupName    = $this->_db->select()
                    ->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
                    ->where("C.GROUP_BUSER_ID LIKE ?", '%_' . $value . '%');
            }

            $groupNameList = $this->_db->fetchAll($selectGroupName);

            array_unique($groupNameList[0]);

            $uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
        }

        foreach ($uniqueGroupName as $row => $data) {
            foreach ($alphabet as $key => $value) {
                if ($row == $key) {
                    $newUniqueGroupName[$value] = $data;
                }
            }
        }

        foreach ($groupuser as $key => $value) {

            //if special group
            if ($value == 27) {
                $likecondition = "S_%";
            } else {
                $likecondition = "%_" . $value . "%";
            }

            $selectgroup = $this->_db->select()
                ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
                    'BUSER_ID'
                ))

                ->where("C.GROUP_BUSER_ID LIKE ?", (string) $likecondition);

            $group_user = $this->_db->fetchAll($selectgroup);

            $groups[][$alphabet[$value]] = $group_user;
        }
        //	echo '<pre>';
        //var_dump($groups);
        $tempGroup = array();
        foreach ($groups as $key => $value) {

            foreach ($value as $data => $values) {

                foreach ($values as $row => $val) {
                    $userid = $val['BUSER_ID'];

                    $selectusername = $this->_db->select()
                        ->from(array('M_BUSER'), array(
                            '*'
                        ))

                        ->where("BUSER_ID = ?", (string) $userid);
                    //echo $selectusername;echo ' ';
                    $username = $this->_db->fetchAll($selectusername);

                    if (!in_array($data, $tempGroup)) {
                        $userlist[$data][] = $username[0]['BUSER_NAME'];
                    }
                }

                array_push($tempGroup, $data);

                // $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
                // 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
            }
        }

        $userlist['GROUP_NAME'] = $newUniqueGroupName;

        return $userlist;
    }




    public function validatebtn($transfertype, $amount, $ccy, $psnumb)
    {
        //die;


        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BGBOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        // echo $selectuser;

        $datauser = $this->_db->fetchAll($selectuser);
        if (empty($datauser)) {

            return true;
        }

        $selectusergroup    = $this->_db->select()
            ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
                '*'
            ))

            ->where("C.BUSER_ID 	= ?", (string) $this->_userIdLogin);

        $usergroup = $this->_db->fetchAll($selectusergroup);




        $this->view->boundarydata = $datauser;
        //var_dump($usergroup);die;
        // print_r($this->view->boundarydata);die;
        if (!empty($usergroup)) {
            $cek = false;

            foreach ($usergroup as $key => $value) {
                $group = explode('_', $value['GROUP_BUSER_ID']);
                $alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
                $groupalfa = $alphabet[(int) $group[2]];
                // print_r($groupalfa);echo '-';
                $usergroup[$key]['GROUP'] = $groupalfa;
                //var_dump($usergroup);
                // print_r($datauser);die;
                foreach ($datauser as $nub => $val) {
                    $command = str_replace('(', '', $val['POLICY']);
                    $command = str_replace(')', '', $command);
                    $list = explode(' ', $command);

                    //var_dump($list);
                    foreach ($list as $row => $data) {

                        if ($data == $groupalfa) {
                            $cek = true;
                            // die('ter');
                            break;
                        }
                    }
                }
            }
            //die;



            if ($group[0] == 'S') {
                $cek = true;
            }
            // echo $cek;
            // print_r($cek);die;
            if (!$cek) {
                // die('here');
                return false;
            }
        }
        $tempusergroup = $usergroup;

        if ($cek) {




            $command = ' ' . $datauser['0']['POLICY'] . ' ';
            $command = strtoupper($command);

            $cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

            //transform to php logical operator syntak
            $translate = array(
                'AND' => '&&',
                'OR' => '||',
                'THEN' => 'THEN$',
                'A' => '$A',
                'B' => '$B',
                'C' => '$C',
                'D' => '$D',
                'E' => '$E',
                'F' => '$F',
                'G' => '$G',
                'H' => '$H',
                'I' => '$I',
                'J' => '$J',
                'K' => '$K',
                'L' => '$L',
                'M' => '$M',
                'N' => '$N',
                'O' => '$O',
                'P' => '$P',
                'Q' => '$Q',
                'R' => '$R',
                // 'S' => '$S',
                'T' => '$T',
                'U' => '$U',
                'V' => '$V',
                'W' => '$W',
                'X' => '$X',
                'Y' => '$Y',
                'Z' => '$Z',
                'SG' => '$SG',
            );

            $phpCommand =  strtr($cleanCommand, $translate);
            //var_dump($phpCommand);die;
            $param = array(
                '0' => '$A',
                '1' => '$B',
                '2' => '$C',
                '3' => '$D',
                '4' => '$E',
                '5' => '$F',
                '6' => '$G',
                '7' => '$H',
                '8' => '$I',
                '9' => '$J',
                '10' => '$K',
                '11' => '$L',
                '12' => '$M',
                '13' => '$N',
                '14' => '$O',
                '15' => '$P',
                '16' => '$Q',
                '17' => '$R',
                // '18' => '$S',
                '19' => '$T',
                '20' => '$U',
                '21' => '$V',
                '22' => '$W',
                '23' => '$X',
                '24' => '$Y',
                '25' => '$Z',
                '26' => '$SG',
            );
            // print_r($phpCommand);die;
            function str_replace_first($from, $to, $content, $row)
            {
                $from = '/' . preg_quote($from, '/') . '/';
                return preg_replace($from, $to, $content, $row);
            }

            $command = str_replace('(', ' ', $val['POLICY']);
            $command = str_replace(')', ' ', $command);
            $list = explode(' ', $command);
            // print_r($list);die;
            // var_dump($command)

            $thendata = explode('THEN', $command);
            // print_r($thendata);echo '<br>';die;
            $cthen = count($thendata);
            // print_r($thendata);die;
            $secondcommand = str_replace('(', '', trim($thendata[0]));
            $secondcommand = str_replace(')', '', $secondcommand);
            $secondcommand = str_replace('AND', '', $secondcommand);
            $secondcommand = str_replace('OR', '', $secondcommand);
            $secondlist = explode(' ', $secondcommand);
            // print_r($secondlist);die;
            // print_r($grouplist);die;
            // print_r($thendata[$i]);die;
            // return true;
            if (!empty($secondlist)) {
                foreach ($usergroup as $key => $value) {
                    foreach ($secondlist as $row => $thenval) {
                        // print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
                        if (trim($value['GROUP']) == trim($thenval)) {
                            $thengroup = true;
                            $grouplist[] = trim($thenval);
                            //die('here');
                        }
                    }
                }
            }

            //var_dump($cthen);
            if ($cthen >= 2) {
                foreach ($usergroup as $key => $value) {
                    // print_r($value);
                    foreach ($thendata as $row => $thenval) {
                        // echo '|';print_r($thenval);echo '==';
                        // print_r($value['GROUP']);echo '|';echo '<br/>';
                        // $thengroup = true;
                        $newsecondcommand = str_replace('(', '', trim($thenval));
                        $newsecondcommand = str_replace(')', '', $newsecondcommand);
                        $newsecondcommand = str_replace('AND', '', $newsecondcommand);
                        $newsecondcommand = str_replace('OR', '', $newsecondcommand);
                        $newsecondlist = explode(' ', $newsecondcommand);
                        //var_dump($newsecondcommand);
                        if (in_array(trim($value['GROUP']), $newsecondlist)) {
                            //if (trim($value['GROUP']) == trim($thenval)) {
                            $thengroup = true;
                            $grouplist[] = trim($thenval);
                            //die('here');
                        }
                    }
                }
            }
            //var_dump($grouplist);die;
            //var_dump($thengroup);die;
            // var_dump($thengroup);die;
            // // print_r($group);die;
            // // echo $thengroup;die;
            //echo '<pre>';
            //var_dump($thengroup);
            //var_dump($thendata);
            //print_r($thendata);echo '<br/>';die('here');
            if ($thengroup == true) {


                for ($i = 1; $i <= $cthen; ++$i) {
                    $oriCommand = $phpCommand;
                    //echo $oriCommand;die;
                    $indno = $i;
                    //echo $oriCommand;echo '<br>';

                    for ($a = $cthen - $indno; $a >= 1; --$a) {

                        if ($i > 1) {
                            $replace = 'THEN$ $' . trim($thendata[$a + 1]);
                        } else {
                            $replace = 'THEN$ $' . trim($thendata[$a]);
                        }

                        $oriCommand = str_replace($replace, "", $oriCommand);
                    }


                    //print_r($thendata);echo '<br>';die();

                    //die;
                    // if($i == 3){
                    // echo 'command : ';echo $oriCommand;echo '<br/>';
                    // }
                    //print_r($oriCommand);echo '<br>';
                    //print_r($list);echo '<br>';die;


                    $result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
                    // print_r($i);
                    //var_dump($result);
                    //echo 'result-';var_dump($result);echo '-';die;
                    if ($result) {
                        // die;
                        // echo $thendata[$i+1];die('eere');
                        // print_r($i);
                        // print_r($thendata);die;



                        $replace = 'THEN$ $' . trim($thendata[$i + 1]);
                        // var_dump($replace);die;
                        // print_r($i);
                        if (!empty($thendata[$i + 1])) {
                            //die;
                            $oriCommand = str_replace($replace, "", $phpCommand);
                        } else {
                            // die;
                            $thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
                            $thirdcommand = str_replace(')', '', $thirdcommand);
                            $thirdcommand = str_replace('AND', '', $thirdcommand);
                            $thirdcommand = str_replace('OR', '', $thirdcommand);
                            $thirdlist = explode(' ', $thirdcommand);
                            //						var_dump($secondlist);
                            //						var_dump($grouplist);
                            //							die;

                            if (!empty($secondlist)) {
                                foreach ($grouplist as $key => $valg) {
                                    foreach ($secondlist as $row => $value) {
                                        if ($value == $valg) {
                                            //echo 'sini';
                                            return false;
                                        }
                                    }
                                }
                            }
                            $oriCommand = $phpCommand;
                        }
                        // print_r($thendata[$i]);die;
                        // if($i == 3){
                        // echo $oriCommand;die;
                        // echo '<br/>';	
                        // }
                        // print_r($i);
                        //echo $oriCommand;
                        $result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
                        //var_dump($result);echo '<br/>';
                        if (!$result) {
                            // die;
                            //print_r($groupalfa);
                            //print_r($thendata[$i]);

                            if ($groupalfa == trim($thendata[$i])) {
                                //echo 'heer';
                                return true;
                            } else {

                                //return true;
                            }
                            /*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
                        } else {

                            //return false;

                            // $result = $this->generate($phpCommand,$list,$param,$psnumb);
                            // print_r($phpCommand);
                            // if($result){}
                            // die('here');
                        }
                        // var_dump($result);
                        // die;


                    } else {
                        //die('here');
                        $secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
                        $secondcommand = str_replace(')', '', $secondcommand);
                        $secondcommand = str_replace('AND', '', $secondcommand);
                        $secondcommand = str_replace('OR', '', $secondcommand);
                        $secondlist = explode(' ', $secondcommand);
                        //var_dump($i);
                        // var_dump($thendata);
                        //print_r($grouplist);
                        //die;
                        //if($groupalfa == $gro)
                        $approver = array();
                        $countlist = array_count_values($list);
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                                $selectapprover    = $this->_db->select()
                                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                                        'USER_ID'
                                    ))

                                    // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                                    ->where("C.REG_NUMBER = ?", (string) $psnumb)
                                    ->where("C.GROUP = ?", (string) $value);
                                //	 echo $selectapprover;
                                $usergroup = $this->_db->fetchAll($selectapprover);
                                // print_r($usergroup);
                                $approver[$value] = $usergroup;
                                if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
                                    //var_dump($countlist[$value]);
                                    //var_dump($tempusergroup['0']['GROUP']);  
                                    //die('gere');
                                    return false;
                                }
                            }
                        }

                        //$array = array(1, "hello", 1, "world", "hello");


                        //echo '<pre>';
                        //var_dump($list);
                        //var_dump($approver);
                        //die;
                        //var_dump($secondlist[0]);die;
                        //foreach($approver as $app => $valpp){
                        if (!empty($thendata[$i])) {
                            if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
                                //die('gere');
                                return false;
                            }
                        }

                        //echo '<pre>';
                        //var_dump($thendata);
                        //var_dump($grouplist);
                        //var_dump($approver);
                        //var_dump($groupalfa);
                        //die;
                        foreach ($grouplist as $key => $vg) {
                            $newgroupalpa = str_replace('AND', '', $vg);
                            $newgroupalpa = str_replace('OR', '', $newgroupalpa);
                            $groupsecondlist = explode(' ', $newgroupalpa);
                            //var_dump($newgroupalpa);
                            //&& empty($approver[$groupalfa])
                            if (in_array($groupalfa, $groupsecondlist)) {
                                // echo 'gere';die;
                                return true;
                                //die('ge');
                            }

                            if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
                                // echo 'gere';die;
                                return true;
                                //die('ge');
                            }
                        }

                        // var_dump($approver[$groupalfa]);
                        // var_dump($grouplist);
                        //var_dump($groupalfa);die;

                        //echo 'ger';die;
                        //print_r($secondlist);die;
                        //	 return true;
                        if (!empty($secondlist)) {
                            foreach ($grouplist as $key => $valg) {
                                foreach ($secondlist as $row => $value) {
                                    if ($value == $valg) {

                                        if (empty($thendata[1])) {
                                            //die;
                                            return true;
                                        }
                                        //else{
                                        //	return false;
                                        //	}
                                    }
                                }
                            }
                        }

                        //	echo 'here';die;
                        $secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);
                        // var_dump($secondresult);
                        //print_r($thendata[$i-1]);
                        //die;
                        //if()
                        //	 echo '<pre>';
                        //var_dump($grouplist);die;
                        foreach ($grouplist as $key => $valgroup) {
                            //print_r($valgroup);
                            //var_dump($thendata[$i]); 


                            if (trim($valg) == trim($thendata[$i])) {
                                $cekgroup = false;
                                //die('here');
                                if ($secondresult) {
                                    return false;
                                } else {
                                    return true;
                                }

                                //break;
                            }

                            //else if (trim($valg) == trim($thendata[$i - 1])) {
                            //		$cekgroup = false;
                            // die('here');
                            //	return false;
                            //	}
                        }
                        //die;
                        //if (!$cekgroup) {
                        // die('here');
                        //return false;
                        //}
                    }

                    //		    echo '<br/>';
                    //	${$command} = $oriCommand;
                }
            } else if (!empty($thendata) && $thengroup == false) {

                //var_dump($groupalfa)die;
                foreach ($thendata as $ky => $vlue) {
                    $newsecondcommand = str_replace('(', '', trim($vlue));
                    $newsecondcommand = str_replace(')', '', $newsecondcommand);
                    $newsecondcommand = str_replace('AND', '', $newsecondcommand);
                    $newsecondcommand = str_replace('OR', '', $newsecondcommand);
                    $newsecondlist = explode(' ', $newsecondcommand);
                    if ($newsecondlist['0'] == $groupalfa) {
                        return true;
                    }
                }
                return false;
            }




            $approver = array();
            // print_r($list);die;  	
            foreach ($list as $key => $value) {
                if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                    $selectapprover    = $this->_db->select()
                        ->from(array('C' => 'T_BGAPPROVAL'), array(
                            'USER_ID'
                        ))

                        // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                        ->where("C.REG_NUMBER = ?", (string) $psnumb)
                        ->where("C.GROUP = ?", (string) $value);
                    //	 echo $selectapprover;
                    $usergroup = $this->_db->fetchAll($selectapprover);
                    // print_r($usergroup);
                    $approver[$value] = $usergroup;
                }
            }
            //die;




            // print_r($phpCommand);die;
            foreach ($param as $url) {
                if (strpos($phpCommand, $url) !== FALSE) {
                    $ta = substr_count($phpCommand, $url);
                    // print_r($list);die;

                    if (!empty($approver)) {
                        // print_r($approver);die;
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                                foreach ($approver[$value] as $row => $val) {
                                    // print_r($approver);die;
                                    if (!empty($val)) {
                                        $values = 'G' . $value;
                                        ${$values}[$row + 1] = true;
                                        // print_r($B);
                                    }

                                    // print_r($val);
                                }
                            }
                        }
                    }

                    for ($i = 1; $i <= $ta; $i++) {
                        // print_r($list);die;
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                                $values = 'G' . $value;
                                // print_r(${$values});
                                if (empty(${$values}[$i])) {
                                    ${$values}[$i] = false;
                                }
                                // if(${$value}[$i])
                            }
                        }
                        // print_r($phpCommand);die;
                        $BG_NUMBER = $i;
                        $label = str_replace('$', '$G', $url);

                        $replace = $label . '[' . $BG_NUMBER . ']';

                        $alf = str_replace('$', '', $url);
                        $values = 'G' . $alf;
                        // print_r($values);die;
                        if (${$values}[$i] == $replace) {
                            $phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
                            // print_r($phpCommand);
                        } else {
                            $phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
                            // print_r($phpCommand);die;
                        }
                        // }
                        // }

                    }
                    // print_r($GB);die;

                }
            }

            $keywords = preg_split("/[\s,]+/", $cleanCommand);
            $result =  false;
            $thendata = explode('THEN$', $phpCommand);
            //var_dump($thendata);die;
            if (!empty($thendata['1'])) {
                $phpCommand = '';
                foreach ($thendata as $tkey => $tval) {
                    $phpCommand .= '(';
                    $phpCommand .= $tval . ')';
                    if (!empty($thendata[$tkey + 1])) {
                        $phpCommand .= ' && ';
                    }
                }
            } else {

                $phpCommand = str_replace('THEN$', '&&', $phpCommand);
            }
            //var_dump($phpCommand);
            if (!empty($phpCommand)) {
                eval('$result = ' . "$phpCommand;");
            } else {
                return false;
            }
            //var_dump($result);die;
            if (!$result) {

                return true;
            }
            // die('here2');
            //var_dump ($result);die;
            //return $result;
        } else {
            // die('here');
            return true;
        }
    }



    public function generate($command, $list, $param, $psnumb, $group, $thengroup)
    {

        $phpCommand = $command;

        // echo $command;die;

        $approver = array();

        $count_list = array_count_values($list);
        //print_r($count_list);
        foreach ($list as $key => $value) {
            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))

                    // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                    ->where("C.REG_NUMBER = ?", (string) $psnumb)
                    ->where("C.GROUP = ?", (string) $value);
                // echo $selectapprover;
                $usergroup = $this->_db->fetchAll($selectapprover);
                // print_r($usergroup);
                $approver[$value] = $usergroup;
            }
        }
        //var_dump($param);die;
        //var_dump($group);
        foreach ($approver as $appval) {
            $totaldata = count($approver[$group]);
            $totalgroup = $count_list[$group];
            //var_dump($totaldata);
            //var_dump($totalgroup);
            if ($totalgroup == $totaldata && $totalgroup != 0) {

                return false;
            }
        } //die;
        //die;





        foreach ($param as $url) {

            if (strpos($phpCommand, $url) !== FALSE) {
                $ta = substr_count($phpCommand, $url);
                // print_r($list);die;

                if (!empty($approver)) {
                    // print_r($approver);die;
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                            foreach ($approver[$value] as $row => $val) {
                                // print_r($approver);die;
                                if (!empty($val)) {
                                    $values = 'G' . $value;
                                    ${$values}[$row + 1] = true;
                                    // print_r($B);
                                }

                                // print_r($val);
                            }
                        }
                    }
                }


                // print_r($approver);die;

                for ($i = 1; $i <= $ta; $i++) {

                    foreach ($list as $key => $value) {
                        if (!empty($value)) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                                $values = 'G' . $value;
                                // print_r(${$values});
                                if (empty(${$values}[$i])) {
                                    ${$values}[$i] = false;
                                }
                                // if(${$value}[$i])
                            }
                        }
                    }


                    $BG_NUMBER = $i;
                    $label = str_replace('$', '$G', $url);
                    // print_r($phpCommand);die('here');
                    $replace = $label . '[' . $BG_NUMBER . ']';

                    $alf = str_replace('$', '', $url);
                    $values = 'G' . $alf;

                    if (${$values}[$i] == $replace) {
                        $phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
                        // print_r($phpCommand);
                    } else {
                        $phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
                        // print_r($phpCommand);die;
                    }
                    // }
                    // }

                }
                // print_r($GB);die;

            }
        }

        $keywords = preg_split("/[\s,]+/", $cleanCommand);
        $result =  false;
        $phpCommand = str_replace('THEN$', '&&', $phpCommand);
        //print_r($phpCommand);echo '<br/>';

        if (!empty($phpCommand)) {
            eval('$result = ' . "$phpCommand;");
            //var_dump($thengroup);
            // var_dump($result);

            if ($result) {
                //var_dump($thengroup);die;
                if ($thengroup) {
                    return true;
                } else {
                    return false;
                }
            } else {

                if ($thengroup) {
                    return false;
                } else {

                    return true;
                }
            }
            // return $result;
        } else {
            return false;
        }

        // var_dump ($result);die;




    }

    public function getsignAction()
    {
        $this->_helper->layout()->setLayout('popup');
        $this->view->who = $this->_request->getParam("who");

        $filter = $this->_request->getParam("filter");
        $select = $this->_db->select()
            ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH'))
            ->joinLeft(["MB" => "M_BRANCH"], "A.BUSER_BRANCH = MB.ID", ["BUSER_BRANCH_CODE" => "MB.BRANCH_CODE", "MB.BRANCH_NAME"])
            ->where('A.BUSER_ID = ?', $this->_userIdLogin);
        $buser = $this->_db->fetchRow($select);

        $this->view->branch_name = $buser["BRANCH_NAME"];

        $branchUser = $buser['BUSER_BRANCH'];

        $allbranch = $this->_db->select()
            ->from("M_BRANCH", [
                'value' => new Zend_Db_Expr("CONCAT(BRANCH_NAME)"),
                "BRANCH_NAME", "ID"
            ])
            ->query()->fetchAll();

        $this->view->brancharr = json_encode($allbranch);

        $getprivi = $this->_db->select()
            ->from(["MBPG" => "M_BPRIVI_GROUP"], ["*"])
            ->where("MBPG.BPRIVI_ID = ?", "SGBC");

        $getbuser = $this->_db->select()
            ->from(["MB" => "M_BUSER"], ["*"])
            ->joinleft(["BRANCH" => "M_BRANCH"], "BRANCH.ID = MB.BUSER_BRANCH", ["BRANCH.ID", "BRANCH.BRANCH_NAME"])
            ->joinleft(["BGROUP" => "M_BGROUP"], "BGROUP.BGROUP_ID = MB.BGROUP_ID", ["BGROUP.BGROUP_DESC"])
            ->where("BRANCH.ID = ?", $branchUser);

        if ($filter) {
            // filter

            if (!empty($this->_request->getParam("bank_branch"))) {
                $getbuser->where("BRANCH.ID = ?", $this->_request->getParam("bank_branch"));
            }

            if (!empty($this->_request->getParam("cust_name"))) {
                $getbuser->where("MB.BUSER_NAME LIKE ?", "%" . $this->_request->getParam("cust_name") . "%");
            }

            if (!empty($this->_request->getParam("bgroup"))) {
                if ($this->_request->getParam("bgroup") != "all") {
                    $getbuser->where("MB.BGROUP_ID = ?", $this->_request->getParam("bgroup"));
                }
            }
            // $getbuser->where("MB.BUSER_BRANCH = ?", "26");

            // $getbuser->where("MB.BUSER_NAME LIKE ?", "%" . "KRI" . "%");
        }

        $getprivi = $getprivi->query()->fetchAll();
        $savegroup = [];
        foreach ($getprivi as $key => $value) {
            array_push($savegroup, $value["BGROUP_ID"]);
        }


        $getbuser->where("MB.BGROUP_ID IN (?)", $savegroup);

        $getbuser = $getbuser->query()->fetchAll();

        $getAllGroup = $this->_db->select()->distinct()
            ->from(["MB" => "M_BUSER"], ["MB.BGROUP_ID"])
            ->joinleft(["BRANCH" => "M_BRANCH"], "BRANCH.ID = MB.BUSER_BRANCH", [])
            ->joinleft(["BGROUP" => "M_BGROUP"], "BGROUP.BGROUP_ID = MB.BGROUP_ID", ['BGROUP.BGROUP_DESC'])
            ->where("BRANCH.ID = ?", $branchUser);
        $getAllGroup->where("MB.BGROUP_ID IN (?)", $savegroup);

        $getAllGroup = $getAllGroup->query()->fetchAll();

        $allGroup = [];
        $allGroup["all"] = "Semua";

        foreach ($getAllGroup as $key => $value) {
            $allGroup[$value["BGROUP_ID"]] = $value["BGROUP_DESC"];
        }

        $this->view->allGroup = $allGroup;

        $this->paging($getbuser);
    }

    public function checkquotaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $checkQuota = new SGO_Soap_ClientUser();
        $checkQuota->callapi("checkquota", null);

        $saveResult = json_decode(json_encode($checkQuota->getResult()), true);

        $saveResult = array_shift(array_filter($saveResult['Data'], function ($item) {
            return strtolower($item['path']) == 'e-meterai';
        }));

        $remaining_token = $saveResult['remaining_token'];

        header("Content-Type: application/json");
        echo json_encode([
            "remaining_token" => $remaining_token + 1
            // "remaining_token" => 10
        ]);
    }

    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function Terbilangen($nilai)
    {
        $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 20 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 100) {
            return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilangen($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilangen($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function generateTransactionID($branch, $bgtype)
    {

        $currentDate = date("ymd");
        $seqNumber   = mt_rand(1111, 9999);
        $trxId = strval($branch) . strval($currentDate) . strval($bgtype) . strval($seqNumber);

        return $trxId;
    }

    public function checknikAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $get_nik = $this->_request->getParam("nik");

        $get_nik = $get_nik ?: "0000000000000000";

        $callApi = new SGO_Soap_ClientUser();
        $check_nik = $callApi->callapi('checkDS', ["nik" => $get_nik]);
        $check_nik = $callApi->getResult();

        if ($check_nik->code == "USER_DO_NOT_EXISTS") {
            echo json_encode([
                "status" => "not registered"
            ]);
        } else {
            echo json_encode([
                "status" => "registered"
            ]);
        }
    }
}
