<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
class accountstatement_ViewreportmtController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	protected $_acctArr = array();
	protected $_cache;
	
	public function initController()
	{
		$Customer = new CustomerUser($this->_custIdLogin,$this->_userIdLogin);
		$this->_acctArr = $Customer->getAccounts();	
		$this->view->AccArr =  $this->_acctArr;
		$frontendOptions = array(
            'cache_id_prefix' 			=> 'test', 
            'lifetime' 					=> 3600,
            'automatic_serialization' 	=> true
        );

		$backendOptions = array('cache_dir' => LIBRARY_PATH.'/data/cache/transactionHistory');
		$this->_cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
	}
	
	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$this->view->PERIOD 		= 'today';
		Application_Helper_General::writeLog('ACDT', 'Viewing Transaction Inquiry');
	}

	public function viewAction(){
	
	$this->_helper->_layout->setLayout('newlayout');
		$set = new Settings();
		$acct_stat_layout = $set->getSettingVal('acct_stat_layout');
		$this->view->acct_stat_layout = $acct_stat_layout;		
		$listAccValidate = Application_Helper_Array::simpleArray($this->_acctArr,'ACCT_NO');

		$page     = $this->_getParam('page');
		$back     = $this->_getParam('back');
		$mt      = $this->_getParam('mt');
		$pdf      = $this->_getParam('pdf');
		$account  = $this->_getParam('ACCTSRC');
		
		$isNew = (empty($page) && empty($csv) && empty($pdf))? true: false;	// isNew = true, then inquiry to host
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$filterArr = array	( 	
								'ACCTSRC'		=> 	array('StringTrim','StripTags')
							);
							
		$validators = array(
								'periodtype' => array(
													'NotEmpty',
													array('InArray', array('haystack' => array('today', 'history'))),
													'messages' => array($this->language->_('Error: You must select Period'),
																		//'Error: You must select Period',
																		//'Error: Period must be today or history',
																		$this->language->_('Error: Period must be today or history'),
																		),
												),			
								'ACCTSRC' => array(
													'NotEmpty',
													array('InArray', array('haystack' => $listAccValidate)),
													'messages' => array($this->language->_('Error: You must select account'),
																		//'Error: You must select account',
																		$this->language->_('Error: You dont have right to this account'),
																		//'Error: You dont have right to this account',
																	),
												)		
							);
		
		$zf_filter_input = new Zend_Filter_Input($filterArr,$validators,$this->_request->getParams(),$this->_optionsValidator);
		
		$period = $zf_filter_input->periodtype;
		
		$dateFrom = '';
		$dateTo   = '';
		if ($period == 'history')
		{
			$dateFrom = $this->_getParam('history_from');
			$dateTo   = $this->_getParam('history_to');
			if(!empty($dateFrom))
			{
				$this->view->DATE_FROM		=$dateFrom;
				$FormatDate = new Zend_Date($dateFrom, $this->_dateDisplayFormat);
				
				$dateFromView  = $FormatDate->toString($this->_dateViewFormat);
				$startDate = date("Ymd", strtotime($dateFromView));
				$dualstartDate = date("ymd", strtotime($dateNow));
				$this->view->DATE_FROM_view	= $dateFromView;
				$dateFrom  = $FormatDate->toString($this->_dateDBFormat);
			}
			if(!empty($dateTo))
			{
				$this->view->DATE_TO		= $dateTo;
				$FormatDate = new Zend_Date($dateTo, $this->_dateDisplayFormat);
				
				$dateToView    = $FormatDate->toString($this->_dateViewFormat);
				$endDate = date("Ymd", strtotime($dateToView));
				$dualendDate = date("ymd", strtotime($dateNow));
				$this->view->DATE_TO_view	= $dateToView;
				$dateTo    = $FormatDate->toString($this->_dateDBFormat);
			}
		}
		else{
			$FormatDate = Zend_Date::now();
			$dateNow    = $FormatDate->toString($this->_dateViewFormat);
			$startDate = date("Ymd", strtotime($dateNow));
			$dualstartDate = date("ymd", strtotime($dateNow));
			$this->view->DATE_FROM_view = $dateNow;
			$this->view->DATE_TO_view = $dateNow;
		}
		
		$this->view->ACCTSRC 		= $zf_filter_input->ACCTSRC;
		$this->view->PERIOD 		= $period;

		if (!empty($back))
		{
			$this->render( 'index' );
		}
		else
		{
			$errorMsg = "";
			if ($period == 'history' && (empty($dateFrom) || empty($dateTo)))
			{
				$errorMsg = $this->language->_('Error: History date range has not been chosen yet');
			}
			elseif ($period == 'history')
			{
				$dateFromDB = Application_Helper_General::convertDate($dateFrom, $this->_dateDBFormat, $this->_dateDisplayFormat);
				$dateToDB   = Application_Helper_General::convertDate($dateTo, $this->_dateDBFormat, $this->_dateDisplayFormat);
				$dateHistory = Application_Helper_General::convertDate((date("d/m/Y")), $this->_dateDBFormat, $this->_dateDisplayFormat);
//				$dateDiff	= Application_Helper_General::date_diff_days($dateFromDB, $dateHistory);
				$dateDiff	= Application_Helper_General::date_diff_days($dateFromDB, $dateToDB);
				
				$maxInq 	= date('Y-m-d', strtotime('-89 days', strtotime(date('Y-m-d'))));
				/*
				if (strtotime($dateFromDB) > strtotime($dateToDB))
				{
					$errorMsg = $this->language->_('Error: From Date must not be greater than To Date');
				}
				else 
				if (strtotime($dateToDB) > strtotime(date('Y-m-d')))
				{
					//$errorMsg = $this->language->_('Error: History period must be less than today');
					$errorMsg = $this->language->_('Error: End date greater than today date');
				}
				else */
				if ($dateDiff > 31)
				{
					$errorMsg = $this->language->_('Error: Account statement are retained for the last 31 days');
				}
				elseif ($dateFromDB < $maxInq)
				{
					$errorMsg = $this->language->_('Error: Account statement are retained for the last 90 days');
				}
			}
			
			if($zf_filter_input->isValid() && $errorMsg == "")
			{
				$accData = $this->_db->fetchRow(
					$this->_db->select()
					->from('M_CUSTOMER_ACCT',array('ACCT_NAME', 'ACCT_ALIAS_NAME','ACCT_TYPE', 'CCY_ID'))
					->where('ACCT_NO = '.$this->_db->quote($zf_filter_input->ACCTSRC))
				);
				
				$ccy 		= $accData['CCY_ID'];
				$ACCT_TYPE 	= $accData['ACCT_TYPE'];
				if(!empty($ccy)){
				$currCode 	= Application_Helper_General::getCurrNum($ccy);
				}
				if ($isNew === true)	
				{						   
					$data =  new Service_Account($zf_filter_input->ACCTSRC,$currCode);
					$dataInquiry = $data->accountInquiry('AB',FALSE); 
					$dataAccountType = $dataInquiry['AccountType'];
					
				    if ($period == 'today')
				    	$dataStatement = $data->accountStatementToday($isMobile = FALSE, $dataAccountType); 
					else
						$dataStatement = $data->accountStatementHistory($dateFromDB, $dateToDB, $isMobile = FALSE, $dataAccountType); 
						
						
					if($dataInquiry['ResponseCode'] == '00' && $dataStatement['ResponseCode'] == '00'){
						$this->view->messageResponse = '1';
					}
					elseif ($dataStatement['ResponseCode'] =='45'){
						$this->view->messageError = $this->language->_('No Transaction History');
						$this->view->messageResponse = '0';
					}
					else{
						$this->view->messageError = $this->language->_('Error : Cant Continue the Transaction, Please Retry in a few moment');
						$this->view->messageResponse = '0';
					}	
						
					$sessionData = new Zend_Session_Namespace('TRX_INQ');
					$sessionData->Detail 			= $dataStatement['Detail'];	
					$sessionData->OpeningBalance 	= $dataStatement['OpeningBalance'];	
					$sessionData->ClosingBalance 	= $dataStatement['ClosingBalance'];	
					$sessionData->HoldAmount 		= $dataInquiry['HoldAmount'];	
				}
				else
				{
					$sessionData = new Zend_Session_Namespace('TRX_INQ');
					
					$dataStatement['Detail'] 			= $sessionData->Detail;
					$dataStatement['OpeningBalance'] 	= $sessionData->OpeningBalance; 
					$dataStatement['ClosingBalance'] 	= $sessionData->ClosingBalance;
					$dataInquiry['HoldAmount'] 			= $sessionData->HoldAmount;
				}
				//echo "<pre>"; print_r($dataStatement);	die();
				
				$dataDetails = array();
				$totalDebet = "";
				$totalKredit = "";
								
				foreach($dataStatement['Detail'] as $dt => $datadtl)
				{

					if($acct_stat_layout == '1'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
							if( $dt == 0 )
							{
//								$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
							}
							else
							{
								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//								$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							}
							
							
							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}
					elseif($acct_stat_layout == '2'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
							if( $dt == 0 )
							{
//								$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
							}
							else
							{
								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//								$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							}
							
							
							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}
					elseif($acct_stat_layout == '1R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
//						$balance = $datadtl->Balance;
//						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);

						if( $dt == 0 )
						{
//							$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
							$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
						}
						else
						{
							$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//							$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
						}
						
						
						$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;

					}
					elseif($acct_stat_layout == '2R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance - $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance + $datadtl->Amount;
						}
						
						
//						$typeTransaction = $datadtl->DBCR;
//						$dataAmount = $datadtl->Amount;
//						
//						
//						if($typeTransaction == 'D'){
//							$debetAmount = $dataAmount;
//							
//
//						}
//						elseif($typeTransaction == 'C'){
//							$creditAmount = $dataAmount;
//						}
						
//						$totBalance = ($dataStatement['ClosingBalance']-$totalKredit+$totalDebet);
//						$tot = $totBalance + $debetAmount - $creditAmount;
						
						if( $dt == 0 )
						{
//							$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
							$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
						}
						else
						{
							$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//							$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
						}
						
						
						$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
						
						
//						$dataStatement['Detail'][$dt]->Balance = $tot;
						//$runningBalance = ($totBalance-$debetAmount)+$creditAmount;
						//$balance = $datadtl->Balance;
//						$balance = $tot;
						
//						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);
						
					}
				
					
					if ($period == 'history')
					{	
//						$balanceSign = substr($datadtl->Balance, 0, 1);
//						$balance	 = substr($datadtl->Balance, 1);
//						if ($balanceSign != '+')
//			   			{	$balance = (-1) * $balance;		}
			   			
			   			$balance = $datadtl->Balance;
	   			
						//$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);		
					}
		
				}
				
				$dataStatement['Detail'] = array_reverse($dataStatement['Detail'], true); //di sorting
				
				
				
				if ($mt) {
					$dataDetailsOri = $dataStatement['Detail'];
					// print_r($dataStatement);die;
					// $handle = fopen($startDate.".txt", "w");
					// // print_r($)
				 //    fwrite($handle, "{1:003BANK PERMATA}{2:I940".strtoupper($accData['ACCT_NAME'])."}"."{4:0-");
				 //    $randomNum = substr(str_shuffle("0123456789"), 0, 6);
				 //    fwrite($handle, "{:20:".$startDate."".$randomNum);
				 //    fwrite($handle, "{:25:".$zf_filter_input->ACCTSRC);
				 //    fwrite($handle, "{:28C:00001");
				 //    if ($period == 'today'){
				 //    	fwrite($handle, "{:60F:C".$dualstartDate."".$accData['CCY_ID']."".$openingBalance);	
				 //    }else{
				 //    	fwrite($handle, "{:60M:C".$dualstartDate."".$accData['CCY_ID']."".$openingBalance);	
				 //    }
				 //    $trxdate = '';
				 //    foreach ($dataStatement['Detail'] as $key => $value) {
				    	
				 //    	// $trxdate = date("ymd", strtotime($st));
				 //    	// fwrite($handle, "{:61:".$startDate."".$value['DBCR']."".$value['Amount']."");
				 //    	// fwrite($handle, "{:86:".$value['Description']."");
				 //    }

				 //    // fwrite($handle, "{:62F:".$dataStatement['ClosingBalance']."");
				 //    fwrite($handle, "{:64:".$dataStatement['ClosingBalance']."");
				 //    fclose($handle);

				 //    header('Content-Type: application/octet-stream');
				 //    header('Content-Disposition: attachment; filename='.basename($startDate.'.txt'));
				 //    header('Expires: 0');
				 //    header('Cache-Control: must-revalidate');
				 //    header('Pragma: public');
				 //    header('Content-Length: ' . filesize($startDate.'.txt'));
				 //    readfile($startDate.'.txt');
				 //    exit;
					// startDate


				} else {
					$this->paging($dataStatement['Detail']);	
					$dataDetailsOri = $this->view->paginator;
				}

				$date_language = $this->language->_('Date');
				$description_language = $this->language->_('Description');
				$ccy_language = $this->language->_('CCY');
				$debet_language = $this->language->_('Debet');
				$credit_language = $this->language->_('Credit');
				$amount_language = $this->language->_('Amount');
				$balance_language = $this->language->_('Balance');
				
				//$header = array($date_language, $description_language, $ccy_language, $debet_language, $credit_language);
				if($acct_stat_layout == '1'){
					$header = array($date_language, $description_language, $debet_language, $credit_language);
				}
				elseif($acct_stat_layout == '2'){
					$header = array($date_language, $description_language, $amount_language);
				}
				elseif($acct_stat_layout == '1R'){
					$header = array($date_language, $description_language, $debet_language, $credit_language, $balance_language);
				}
				elseif($acct_stat_layout == '2R'){
					$header = array($date_language, $description_language, $amount_language, '', $balance_language);
				}
				
				if ($period == 'history')
				{	
					//$header[] = 'Balance';		
				}
				
				$dataDetails = array();
				//Zend_Debug::dump($dataDetailsOri); die;
				$totalDebet = "";
				$totalKredit = "";
				foreach($dataDetailsOri as $dt => $datadtl)
				{
					$limiter = ($csv)? " ": "<br>";
					$univDesc  = (!empty($datadtl->UniversalDescription1))? $limiter.$datadtl->UniversalDescription1: "";
					$univDesc .= (!empty($datadtl->UniversalDescription2))? $limiter.$datadtl->UniversalDescription2: "";
					$univDesc .= (!empty($datadtl->UniversalDescription3))? $limiter.$datadtl->UniversalDescription3: "";
					$univDesc .= (!empty($datadtl->UniversalDescription4))? $limiter.$datadtl->UniversalDescription4: "";
					//$dataDetails[$dt]['datetime'] 	 = Application_Helper_General::convertDate($datadtl->Date)." ".$datadtl->Time;
					//$dataDetails[$dt]['datetime'] 	 = Application_Helper_General::convertDate(strtotime($datadtl->Datetime), $this->_dateTimeDisplayFormat);
					
					$dataDetails[$dt]['datetime'] 	 = $datadtl->Datetime;//Application_Helper_General::convertDate($datadtl->Datetime, $this->_dateDisplayFormat);
					$dataDetails[$dt]['description'] = $datadtl->Description.$univDesc;
					$dataDetails[$dt]['ccy'] 		 = $ccy;
										
					if($acct_stat_layout == '1'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = $datadtl->Amount;
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = $datadtl->Amount;
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
					}
					elseif($acct_stat_layout == '2'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = $datadtl->Amount;
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = $datadtl->Amount;
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
					}
					elseif($acct_stat_layout == '1R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = $datadtl->Amount;
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = $datadtl->Amount;
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						$balance = $datadtl->Balance;
						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);

					}
					elseif($acct_stat_layout == '2R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = $datadtl->Amount;
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = $datadtl->Amount;
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						$balance = $datadtl->Balance;
						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);
					}
					
					if ($period == 'history')
					{	
//						$balanceSign = substr($datadtl->Balance, 0, 1);
//						$balance	 = substr($datadtl->Balance, 1);
//						if ($balanceSign != '+')
//			   			{	$balance = (-1) * $balance;		}
			   			
			   			$balance = $datadtl->Balance;
	   			
						//$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);		
					}
					//echo $datadtl->Amount."<br>";
				}
				//echo $totalDebet; die;
				$stringParam = array('ACCTSRC'		=> $zf_filter_input->ACCTSRC,
									 'periodtype'	=> $period,
									 'history_from'	=> $dateFrom,
									 'history_to'	=> $dateTo,
								    );
				
				$this->view->dataDetails 	= $dataDetails;
				$this->view->accData 		= $accData;
				$this->view->openingBalance = $openingBalance;
				$this->view->closingBalance = $dataStatement['ClosingBalance'];
				$this->view->holdAmount 	= $dataInquiry['HoldAmount'];
				$this->view->totalDebet 	= $totalDebet == 0 ? 0.00 : $totalDebet;
				$this->view->totalKredit 	= $totalKredit == 0 ? 0.00 : $totalKredit;
				
				$this->view->ccy 			= $ccy;
				$this->view->header			= $header;
				$this->view->query_string_params = $stringParam;
				$this->view->pdf 			= ($pdf)? true: false;
				$this->updateQstring();
				if ($mt) {
					$dataDetailsOri = $dataStatement['Detail'];
					// print_r($dataDetails);die;
					$handle = fopen($startDate.".txt", "w");
					// print_r($)
				    fwrite($handle, "{1:003BANK PERMATA}{2:I940".strtoupper($accData['ACCT_NAME'])."}"."{4:0-");
				    $randomNum = substr(str_shuffle("0123456789"), 0, 6);
				    fwrite($handle, ":20:".$startDate."".$randomNum);
				    fwrite($handle, ":25:".$zf_filter_input->ACCTSRC);
				    fwrite($handle, ":28C:00001");
				    if ($period == 'today'){
				    	fwrite($handle, ":60F:C".$dualstartDate."".$accData['CCY_ID']."".$openingBalance);	
				    }else{
				    	fwrite($handle, ":60M:C".$dualstartDate."".$accData['CCY_ID']."".$openingBalance);	
				    }
				    $trxdate = '';
				    foreach ($dataDetails as $key => $value) {
				    	
				    	$trxdate = date("ymd", strtotime($value['datetime']));
				    	if($value['type'] == 'DB'){
				    		$value['type'] = 'D';
				    	}else{
				    		$value['type'] = 'C';
				    	}
				    	fwrite($handle, ":61:".$startDate."".$value['type']."".$value['amount']."NTRF");
				    	fwrite($handle, ":86:".str_pad($value['description'],76,' ')."");
				    }
				    if ($period == 'today'){
				    	fwrite($handle, ":62F:C".$dualstartDate."IDR".$dataStatement['ClosingBalance']."}");
				    }else{
				    	fwrite($handle, ":62F:C".$dualendDate."IDR".$dataStatement['ClosingBalance']."}");
				    }
				    
				    // fwrite($handle, "{:64:".$dataStatement['ClosingBalance']."");
				    fclose($handle);

				    header('Content-Type: application/octet-stream');
				    header('Content-Disposition: attachment; filename='.basename($startDate.'.txt'));
				    header('Expires: 0');
				    header('Cache-Control: must-revalidate');
				    header('Pragma: public');
				    header('Content-Length: ' . filesize($startDate.'.txt'));
				    readfile($startDate.'.txt');
				    exit;
					// startDate


				}
				$logDesc = 'Account: '.$zf_filter_input->ACCTSRC;
				if($csv)
				{
					$logDesc .= '. Export to CSV';
					
					// Set Information Data
					$n = count($dataDetails);
						  $dataDetails[$n] = array("");
					$n++; $dataDetails[$n] = array($this->language->_('Information'));
					$n++; $dataDetails[$n] = array($this->language->_('Account'), $zf_filter_input->ACCTSRC.' ('.$accData['ACCT_NAME'].' '.$accData['ACCT_ALIAS_NAME'].' / '.$ccy.')');
					//$n++; $dataDetails[$n] = array("Opening Balance", $ccy.' '.Application_Helper_General::displayMoney($this->view->openingBalance));
					$n++; $dataDetails[$n] = array($this->language->_('Opening Balance'), $ccy.' '.Application_Helper_General::displayMoney($this->view->openingBalance));
					$n++; $dataDetails[$n] = array($this->language->_('Closing Balance'), $ccy.' '.Application_Helper_General::displayMoney($this->view->closingBalance));
					$n++; $dataDetails[$n] = array($this->language->_('Total Debit'), $ccy.' '.Application_Helper_General::displayMoney($this->view->totalDebet));
					$n++; $dataDetails[$n] = array($this->language->_('Total Credit'), $ccy.' '.Application_Helper_General::displayMoney($this->view->totalKredit));
					if ($period == 'today')
					{
					//$n++; $dataDetails[$n] = array($this->language->_('Hold Amount'), $ccy.' '.Application_Helper_General::displayMoney($this->view->holdAmount));
					}
					$n++; $dataDetails[$n] = array($this->language->_('Start Date'), $this->view->DATE_FROM_view);
					$n++; $dataDetails[$n] = array($this->language->_('End Date'), $this->view->DATE_TO_view);
					
					$this->_helper->download->csv($header,$dataDetails,null,'TransactionInquiry');  
				}
				elseif(!empty($pdf))
				{
					$logDesc .= '. Print PDF';
					Application_Helper_General::writeLog('ACDT', $logDesc);
					$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/view.phtml')."</td></tr>";
					$this->_helper->download->pdf(null,null,null,'Transaction Inquiry',$outputHTML);    
				}
				
				Application_Helper_General::writeLog('ACDT', $logDesc);
			}
			else
			{
				$this->view->error 		= true;
				$errors 				= $zf_filter_input->getMessages();
				
				$this->view->ACCTSRCERR		= (isset($errors['ACCTSRC']))? reset($errors['ACCTSRC']) : null;
				
				if ($this->view->ACCTSRCERR == null)
				{	$this->view->ACCTSRCERR = $errorMsg;	}
				
				$this->view->ACCTSRC 		= $account;
				
				$this->render( 'index' );
			}
		}
		
		
	
	}

	

}
