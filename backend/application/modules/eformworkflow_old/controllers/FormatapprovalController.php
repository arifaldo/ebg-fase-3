<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_FormatapprovalController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                 'label' => $this->language->_('Alias Name'),
                                 'sortable' => true),*/
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('RegNumber') . ' / ' . $this->language->_('Subject'),
      ),
      'app' => array(
        'field' => 'CUST_ID',
        'label' => $this->language->_('Principal'),
      ),
      'obligee' => array(
        'field' => 'RECIPIENT_NAME',
        'label' => $this->language->_('Obligee'),
      ),
      'branch'     => array(
        'field'    => 'BG_BRANCH',
        'label'    => $this->language->_('Bank Branch'),
      ),
      'counter'     => array(
        'field'    => 'COUNTER_WARRANTY_TYPE',
        'label'    => $this->language->_('Counter'),
      ),
      'amount' => array(
        'field' => 'BG_AMOUNT',
        'label' => $this->language->_('BG Amount'),
      ),
      'type' => array(
        'field' => 'CHANGE_TYPE',
        'label' => $this->language->_('Type'),
      ),


      // 'startdate' => array(
      //   'field' => 'TIME_PERIOD_START',
      //   'label' => $this->language->_('Start Date'),
      // ),
      // 'enddate'   => array(
      //   'field'    => 'TIME_PERIOD_END',
      //   'label'    => $this->language->_('End Date'),
      // )
      /*'lastsave'  => array(
        'field'    => 'VERIFIED',
        'label'    => $this->language->_('Last Save')
                      )*/
    );

    //add reza
    $filterlist = array(
      'BG_NUMBER', 'BG_SUBJECT', 'CUST_NAME', 'RECIPIENT_NAME', 'BRANCH_NAME',
      'COUNTER_WARRANTY_TYPE', 'SUBMISSION_TYPE', 'LAST_SAVED_BY'
    );

    $this->view->filterlist = $filterlist;

    $page    = $this->_getParam('page');

    $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

    $this->view->currentPage = $page;
    $this->view->sortBy      = $sortBy;
    $this->view->sortDir     = $sortDir;

    //end reza




    $selectbg = $this->_db->select()
      ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array(
        'REG_NUMBER' => 'BG_REG_NUMBER',
        'SUBJECT' => 'BG_SUBJECT',
        'CREATED' => 'BG_CREATED',
        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_START',
        'TIME_PERIOD_END',
        'CREATEDBY' => 'BG_CREATEDBY',
        'AMOUNT' => 'BG_AMOUNT',
        'COUNTER_WARRANTY_TYPE',
        'BG_INSURANCE_CODE',
        'FULLNAME' => 'T.USER_FULLNAME',
        'B.BRANCH_NAME',
        'C.CUST_NAME',
        'A.RECIPIENT_NAME',
        'A.SP_OBLIGEE_CODE',
        'IS_AMENDMENT' => "A.CHANGE_TYPE"

      ))
      ->joinleft(array('V' => 'TEMP_BGVERIFY_DETAIL'), 'A.BG_REG_NUMBER = V.BG_REG_NUMBER')
      ->join(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID')
      ->join(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID')
      ->join(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE')
      //->where('A.BG_STATUS = 2 AND PRINCIPLE_APPROVE = 1')
      ->where('A.BG_FORMAT = 2')
      ->group('A.BG_REG_NUMBER')
      ->order('A.BG_CREATED DESC');
    //->query()->fetchAll();

    $selectlc = $this->_db->select()
      ->from(array('A' => 'T_LC'), array(
        'REG_NUMBER' => 'LC_REG_NUMBER',
        'SUBJECT' => 'LC_CREDIT_TYPE',
        'CREATED' => 'LC_CREATED',
        'CCYID' => 'LC_CCY',
        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_END' => 'LC_EXPDATE',
        'CREATEDBY' => 'LC_CREATEDBY',
        'AMOUNT' => 'LC_AMOUNT',
        'FULLNAME' => 'T.USER_FULLNAME'
      ))
      ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
      ->where('A.LC_STATUS = 3')
      ->order('A.LC_CREATED DESC');
    //->query()->fetchAll();


    //$result = array_merge($selectbg, $selectlc);

    //$this->paging($result);

    // select branch ------------------------------------------------------------
    $select_branch = $this->_db->select()
      ->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
      ->query()->fetchAll();

    $save_branch = [];

    foreach ($select_branch as $key => $value) {
      $save_branch[$value["BRANCH_CODE"]] = $value["BRANCH_NAME"];
    }

    $this->view->sel_branch = $save_branch;
    //  --------------------------------------------------------------------------

    // select counter type ------------------------------------------------------------
    $save_counter_type = [
      1 => 'FC',
      2 => 'LF',
      3 => 'Insurance'
    ];

    $this->view->counter_type = $save_counter_type;
    //  -------------------------------------------------------------------------- 

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrWaranty = array(
      1 => 'FC',
      2 => 'LF',
      3 => 'INS'
    );
    $this->view->arrWaranty = $arrWaranty;


    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;

    $filterArr = array(
      'BG_NUMBER'   => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BG_SUBJECT'      => array('StripTags', 'StringTrim', 'StringToUpper'),
      'CUST_NAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BRANCH_NAME' => array('StripTags', 'StringTrim', 'StringToUpper'),
      'COUNTER_WARRANTY_TYPE' => array('StripTags', 'StringTrim', 'StringToUpper'),
      'SUBMISSION_TYPE' => array('StripTags', 'StringTrim', 'StringToUpper'),
      'LAST_SAVED_BY' => array('StripTags', 'StringTrim', 'StringToUpper'),
    );

    $dataParam = array("BG_NUMBER", "BG_SUBJECT", "BG_PERIOD", "CUST_NAME", "BRANCH_NAME", "COUNTER_WARRANTY_TYPE", "SUBMISSION_TYPE", "LAST_SAVED_BY");
    $dataParamValue = array();

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }
    }

    if (!empty($this->_request->getParam('efdate'))) {
      $efdatearr = $this->_request->getParam('efdate');
      $dataParamValue['BG_PERIOD'] = $efdatearr[0];
      $dataParamValue['BG_PERIOD_END'] = $efdatearr[1];
    }

    $validator = array(
      'BG_NUMBER'  => array(),
      'BG_SUBJECT'      => array(),
      'CUST_NAME'  => array(),
      'BRANCH_NAME' => array(),
      'COUNTER_WARRANTY_TYPE' => array(),
      'SUBMISSION_TYPE' => array(),
      'LAST_SAVED_BY' => array(),
      'BG_PERIOD'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BG_PERIOD_END'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    );

    // echo "<pre>";
    // print_r($dataParamValue);die;


    $zf_filter   = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

    if ($zf_filter->isValid()) {
      $filter     = TRUE;
    }


    $bgNubmer  = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));
    $bgSubject  = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
    $datefrom    = html_entity_decode($zf_filter->getEscaped('BG_PERIOD'));
    $dateto    = html_entity_decode($zf_filter->getEscaped('BG_PERIOD_END'));
    $CUSTNAME  = html_entity_decode($zf_filter->getEscaped('CUST_NAME'));
    $BRANCHNAME  = html_entity_decode($zf_filter->getEscaped('BRANCH_NAME'));
    $COUNTERWAR  = html_entity_decode($zf_filter->getEscaped('COUNTER_WARRANTY_TYPE'));
    $SUBTYPE  = html_entity_decode($zf_filter->getEscaped('SUBMISSION_TYPE'));
    $LAST_SAVED_BY  = html_entity_decode($zf_filter->getEscaped('LAST_SAVED_BY'));


    if ($filter == null) {
      $datefrom = (date("d/m/Y"));
      $dateto = (date("d/m/Y"));
      $this->view->fDateFrom  = (date("d/m/Y"));
      $this->view->fDateTo  = (date("d/m/Y"));
    }

    if ($filter_clear == '1') {
      $this->view->fDateFrom  = '';
      $this->view->fDateTo  = '';
      $datefrom = '';
      $dateto = '';
    }

    if ($filter == null || $filter == TRUE) {
      $this->view->fDateFrom = $datefrom;
      $this->view->fDateTo = $dateto;
      if (!empty($datefrom)) {
        $FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
        $datefrom  = $FormatDate->toString($this->_dateDBFormat);
      }

      if (!empty($dateto)) {
        $FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
        $dateto    = $FormatDate->toString($this->_dateDBFormat);
      }

      if (!empty($datefrom) && empty($dateto))
        $selectbg->where("A.TIME_PERIOD_START >= " . $this->_db->quote($datefrom));

      if (empty($datefrom) && !empty($dateto))
        $selectbg->where("A.TIME_PERIOD_END <= " . $this->_db->quote($dateto));

      if (!empty($datefrom) && !empty($dateto))
        $selectbg->where("A.TIME_PERIOD_START >= " . $this->_db->quote($datefrom) . " and A.TIME_PERIOD_END <= " . $this->_db->quote($dateto));
    }


    if ($filter == TRUE) {
      //   echo"<pre>";
      // print_r($bgNubmer);die;


      if ($bgNubmer != null) {
        $this->view->bgNubmer = $bgNubmer;
        $selectbg->where("A.BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $bgNubmer . '%'));

        // echo"<pre>";
        // print_r($selectbg);die;


      }

      if ($bgSubject != null) {
        $this->view->bgSubject = $bgSubject;
        $selectbg->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $bgSubject . '%'));
        // echo"<pre>";
        // print_r($selectbg);die;
      }

      if ($CUSTNAME   != null) {
        $this->view->$CUSTNAME   = $CUSTNAME;
        $selectbg->where("C.CUST_ID LIKE " . $this->_db->quote('%' . $CUSTNAME . '%'));
        // echo"<pre>";
        // print_r($selectbg);die;
      }

      if ($BRANCHNAME     != null) {
        $this->view->$BRANCHNAME   = $BRANCHNAME;
        $selectbg->where("B.BRANCH_NAME LIKE " . $this->_db->quote('%' . $BRANCHNAME . '%'));
        // echo"<pre>";
        // print_r($selectbg);die;
      }

      if ($COUNTERWAR     != null) {
        $this->view->$COUNTERWAR   = $COUNTERWAR;
        $selectbg->where("A.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $COUNTERWAR . '%'));
        // echo"<pre>";
        // print_r($selectbg);die;
      }

      if ($LAST_SAVED_BY     != null) {
        $this->view->$LAST_SAVED_BY   = $LAST_SAVED_BY;
        $selectbg->where("V.VERIFIEDBY LIKE " . $this->_db->quote('%' . $LAST_SAVED_BY . '%'));
        // echo"<pre>";
        // print_r($selectbg);die;
      }
    }

    // $selectbg->order($sortBy.' '.$sortDir);


    $this->view->fields = $fields;
    $this->view->filter = $filter;

    $selectbg = $this->_db->fetchAll($selectbg);
    $selectlc = $this->_db->fetchAll($selectlc);
    $result = array_merge($selectbg, $selectlc);

    $setting = new Settings();
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $pw_hash = md5($enc_salt . $enc_pass);
    $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
    $sessionNamespace->token   = $rand;
    $this->view->token = $sessionNamespace->token;

    foreach ($result as $key => $value) {
      $get_reg_number = $value["REG_NUMBER"];

      $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
      $encpayreff = urlencode($encrypted_payreff);

      $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
    }

    //Zend_Debug::dump($result);

    $this->paging($result);

    if (!empty($dataParamValue)) {

      $this->view->efdateStart = $dataParamValue['BG_PERIOD'];
      $this->view->efdateEnd = $dataParamValue['BG_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[]  = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[]  = $key;
          $whereval[] = $value;
        }
      }
      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }
  }
}
