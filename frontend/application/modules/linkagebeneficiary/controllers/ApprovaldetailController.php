<?php

require_once 'Zend/Controller/Action.php';


class linkagebeneficiary_ApprovaldetailController extends Application_Main {

	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');
		$paymenttype = $this->_paymenttype;
		$paymenttypecode = $paymenttype['code'];
		$paymenttypecode = array_flip($paymenttypecode);
		$paymenttypedesc = $paymenttype['desc'];

		$aprovalstatus = $this->_aprovalstatus;
		$aprovalstatuscode = $aprovalstatus['code'];
		$aprovalstatusdesc = $aprovalstatus['desc'];
		$this->view->aprovalstatuscode = array_flip($aprovalstatuscode);
		$this->view->aprovalstatusdesc = $aprovalstatusdesc;

		$SUGGEST_ID = $this->_getParam('SUGGEST_ID');
		$this->view->SUGGEST_ID = $SUGGEST_ID;

		$select = $this->_db->select()
								->from(	array('TB' => 'T_BENEFICIARY_USER'),
										array(
												'SUGGEST_ID'=>'TB.SUGGEST_ID',
												'SUGGEST_USER'=>'TB.SUGGEST_USER',
												'SUGGEST_DATE'=>'TB.SUGGEST_DATE',
												'SUGGEST_STATUS'=>'TB.SUGGEST_STATUS',
												'SUGGEST_GRANTER'=>'TB.SUGGEST_GRANTER',
												'SUGGEST_GRANTERDATE'=>'TB.SUGGEST_GRANTERDATE',
												'SUGGEST_REASON'=>'TB.SUGGEST_REASON',
												'CUST_ID'=>'TB.CUST_ID',
												'USER_ID'=>'TE.USER_ID',
												'USER_FULLNAME'=>'MU.USER_FULLNAME',
												'SUGGEST_ID'=>'TB.SUGGEST_ID'))
								->joinLeft	(
												array('TE' => 'TEMP_BENEFICIARY_USER'),
												'TE.SUGGEST_ID = TB.SUGGEST_ID',
												array()
											)
								->joinLeft	(
												array('MU' => 'M_USER'),
												'MU.USER_ID = TE.USER_ID',
												array()
											)
								->where('TB.SUGGEST_ID = ?' , $SUGGEST_ID);
		$arr = $this->_db->fetchRow($select);
		$this->view->arr = $arr;

		if($arr['SUGGEST_STATUS'] == 0)
		{
			try
			{
				$param = array ("SUGGEST_STATUS" => 1);
				$where["SUGGEST_ID = ?"] = $arr['SUGGEST_ID'];
				$query = $this->_db->update ( "T_BENEFICIARY_USER", $param, $where );
			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
			}
		}

		//Zend_Debug::dump($arr['SUGGEST_STATUS']);die;
		if($this->_getParam('approve') && $arr['SUGGEST_STATUS'] != 4)
		{
			try
			{
				$select = $this->_db->select()
								->from(	array('TB' => 'TEMP_BENEFICIARY_USER'),
										array(
												'BENEFICIARY_ID',
												'USER_ID'
											))
								->where('TB.SUGGEST_ID = ?' , $SUGGEST_ID);
				$array = $this->_db->fetchAll($select);

				$whereApp = array(
									'USER_ID = ?' => $arr['USER_ID'],
									'CUST_ID = ?' => $arr['CUST_ID'],
								 );

				$this->_db->delete('M_BENEFICIARY_USER', $whereApp);

				foreach($array as $row)
				{
					$param = array 	(
									"USER_ID" =>$arr['USER_ID'] ,
									"CUST_ID" => $arr['CUST_ID'],
									"BENEFICIARY_ID" =>$row['BENEFICIARY_ID']
								);

					$this->_db->insert("M_BENEFICIARY_USER",$param);
				}

				$deleteWhere['SUGGEST_ID = ?'] = $SUGGEST_ID;
				$this->_db->delete('TEMP_BENEFICIARY_USER',$deleteWhere);
				$this->_db->delete('T_BENEFICIARY_USER',$deleteWhere);

				Application_Helper_General::writeLog('BABU',"Approve Pairing Beneficiary Sugggestion for User=".$arr['SUGGEST_USER']."; for Company= ".$arr['USER_ID']);

				$this->setbackURL('/'.$this->_request->getModuleName().'/approve/index/');
				$this->_redirect('/notification/success/index');

			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
			}
		}

		if($this->_getParam('reject') && $arr['SUGGEST_STATUS'] != 4)
		{
			try
			{
				Application_Helper_General::writeLog('BABU',"Reject Beneficiary Link User");
				$deleteWhere['SUGGEST_ID = ?'] = $SUGGEST_ID;
				$this->_db->delete('TEMP_BENEFICIARY_USER',$deleteWhere);
				$this->_db->delete('T_BENEFICIARY_USER',$deleteWhere);
				$this->setbackURL('/'.$this->_request->getModuleName().'/approve/index/');
				$this->_redirect('/notification/success/index');


			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
			}

		}

		if($this->_getParam('requestrepair') && $arr['SUGGEST_STATUS'] != 4)
		{
			$status = 4;
			//echo("status = ".$status);
			try
			{
				Application_Helper_General::writeLog('BABU',"Request Repair Beneficiary Link User");
				$param = array (
									"SUGGEST_STATUS" => $status,
									"SUGGEST_REASON" =>$this->_getParam('SUGGEST_REASON')
								);
				$where["SUGGEST_ID = ?"] = $arr['SUGGEST_ID'];
				$query = $this->_db->update ( "T_BENEFICIARY_USER", $param, $where );
				$this->setbackURL('/'.$this->_request->getModuleName().'/approve/index/');
				$this->_redirect('/notification/success/index');

			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
			}

		}

		$select = $this->_db->select()
							->from(	array('MBU'=>'M_BENEFICIARY_USER'),
									array(
											'BENEFICIARY_ID' =>'MBU.BENEFICIARY_ID',
											'BENEFICIARY_NAME' =>'MB.BENEFICIARY_NAME',
											'BENEFICIARY_TYPE' =>'MB.BENEFICIARY_TYPE',
											'CURR_CODE' =>'MB.CURR_CODE'
										))
							->where('MBU.USER_ID   = ?' , $arr['USER_ID'])
							->where('MBU.CUST_ID   = ?' , $this->_custIdLogin)
							->joinLeft	(
											array('MB' => 'M_BENEFICIARY'),
											'MB.BENEFICIARY_ID = MBU.BENEFICIARY_ID',
											array('BENEFICIARY_ACCOUNT' =>'MB.BENEFICIARY_ACCOUNT')
										)
							->joinLeft	(
											array('B' => 'M_BANK_TABLE'),
											'B.BANK_CODE = MB.BANK_CODE',
											array('BANK_NAME' =>'B.BANK_NAME'
											)
										);
		
		$arrBenef = $this->_db->fetchAll($select);
		//$countBenef = count($arrBenef);

		$select = $this->_db->select()
							->from(	array('TB' => 'TEMP_BENEFICIARY_USER'),
									array(
											'BENEFICIARY_ID' =>'TB.BENEFICIARY_ID',
											'BENEFICIARY_NAME' =>'MB.BENEFICIARY_NAME',
											'BENEFICIARY_TYPE' =>'MB.BENEFICIARY_TYPE',
											'CURR_CODE' =>'MB.CURR_CODE'
										)

									)
							->where('TB.SUGGEST_ID   = ?' , $arr['SUGGEST_ID'])
							->where('TB.USER_ID   = ?' , $arr['USER_ID'])
							->joinLeft	(
											array('MB' => 'M_BENEFICIARY'),
											'MB.BENEFICIARY_ID = TB.BENEFICIARY_ID',
											array('BENEFICIARY_ACCOUNT' =>'MB.BENEFICIARY_ACCOUNT',
											)
										)
							->joinLeft	(
											array('B' => 'M_BANK_TABLE'),
											'B.BANK_CODE = MB.BANK_CODE',
											array('BANK_NAME' =>'B.BANK_NAME'
											)
										);
										//);
		//echo $select;die;
		$arrBenefTemp = $this->_db->fetchAll($select);
		//$countBenefTemp = count($arrBenefTemp);

		$arrBenefTempAccount = array();
		foreach($arrBenefTemp as $row)
		{			
			$arrBenefTempAccount[]=$row['BENEFICIARY_ACCOUNT'];
		}

		$i = 0;
		$arrBenefAccount = array();
		//echo '<pre>';
		//var_dump($arrBenef);
		foreach($arrBenef as $row)
		{
			$type = (array_key_exists($row['BENEFICIARY_TYPE'],$paymenttypecode))? $paymenttypedesc[$paymenttypecode[$row['BENEFICIARY_TYPE']]] : '&nbsp;';

			$param[$i]["benef01"] = $row['BENEFICIARY_ACCOUNT']." (".$row['CURR_CODE'].") / ".$row['BANK_NAME']." / ".$row['BENEFICIARY_NAME']." - ".$type;

			if (!in_array($row['BENEFICIARY_ACCOUNT'],$arrBenefTempAccount)){
				$param[$i]["dif01"] = 1;
			}
			$i++;
			$arrBenefAccount[]=$row['BENEFICIARY_ACCOUNT'];
		}
		
		$j = 0;
		$arrBenefTempAccount = array();
		//echo '<pre>';
		//var_dump($arrBenefTemp);die;
		foreach($arrBenefTemp as $row)
		{
			$type = (array_key_exists($row['BENEFICIARY_TYPE'],$paymenttypecode))? $paymenttypedesc[$paymenttypecode[$row['BENEFICIARY_TYPE']]] : '&nbsp;';

			$param[$j]["benef02"] = $row['BENEFICIARY_ACCOUNT']." (".$row['CURR_CODE'].") / ".$row['BANK_NAME']." / ".$row['BENEFICIARY_NAME']." - ".$type;

			if (!in_array($row['BENEFICIARY_ACCOUNT'],$arrBenefAccount)){
				$param[$j]["dif02"] = 1;
			}
			$j++;
			$arrBenefTempAccount[]=$row['BENEFICIARY_ACCOUNT'];
		}

		if($i<$j)
		{
			while($i<$j)
			{
				$param[$i]["benef01"] = null;
				$i++;
			}

		}
		else
		{
			while($j<$i)
			{
				$param[$j]["benef02"] = null;
				$j++;
			}
		}
		//Zend_Debug::dump($param);die;
		$this->view->param = $param;

		$this->view->count = $i;

	}
}
