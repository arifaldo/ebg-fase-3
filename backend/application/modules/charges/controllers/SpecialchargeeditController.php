<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class charges_SpecialchargeeditController extends Application_Main{

	public function indexAction()
	{

        $setting= new Settings();
		$this->_helper->layout()->setLayout('newlayout');
		$settingValue = $setting->getSetting('autocharges_isenabled');
		//Zend_Debug::dump($settingValue); die;
		$value = $this->_getParam('s');
		$save = $this->_getParam('m');
		$system_type = $setting->getSetting('system_type');
		if($system_type == '2' || $system_type == '3'){
		$this->view->showdiv = true;
		}else{ 
		$this->view->showdiv = false;	
		}
		 
		
		$selectdomglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
		$selectdomglobal ->where("A.CUST_ID IN ('GLOBAL','SPECIAL')");
		
		$tempdom = $this->_db->fetchAll($selectdomglobal);

		$tempselectpb = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_WITHIN'),array('*'));
		$tempselectpb ->where("A.CUST_ID IN ('GLOBAL','SPECIAL')");
		

		$temppb = $this->_db->fetchAll($tempselectpb);
		 //var_dump($temppb);die;
		if(empty($tempdom) && empty($temppb)){ 
			 
			$this->view->edit = 1; 	 
		 } 
		
		
		
		
		$pstypeArr = array(1 => 'RTGS',2 => 'SKN',8 => 'Domestic Online',0 => 'Inhouse');
		
		$this->view->pstype = $pstypeArr;
		
		$selectpbglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_WITHIN'),array('*'));
		$selectpbglobal ->where("A.CUST_ID = 'GLOBAL'");
		$pbglobal = $this->_db->fetchAll($selectpbglobal);
		 if(!empty($pbglobal)){
			 $pbamount = $pbglobal['0']['AMOUNT'];
		 }else{
			 $pbamount = 0;
		 }
		 //var_dump($pbamount);
		$this->view->pbglobal = $pbamount;
		
		
		$selectpbspecial = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_WITHIN'),array('*'));
		$selectpbspecial ->where("A.CUST_ID = 'SPECIAL'");
		$pbspecial = $this->_db->fetchAll($selectpbspecial);
		 if(!empty($pbspecial)){
			 $pbspecialamount = $pbspecial['0']['AMOUNT'];
		 }else{
			 $pbspecialamount = 0;
		 }
		 //var_dump($pbamount); 
		$this->view->pbspecial = $pbspecialamount;
		
		$selectdomglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_OTHER'),array('*'));
		$selectdomglobal ->where("A.CUST_ID = 'GLOBAL'");
		$domglobal = $this->_db->fetchAll($selectdomglobal);
		 
		$this->view->domglobal = $domglobal;
		
		$selectdomspecial = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_OTHER'),array('*'));
		$selectdomspecial ->where("A.CUST_ID = 'SPECIAL'");
		$domspecial = $this->_db->fetchAll($selectdomspecial);
		 
		$this->view->domspecial = $domspecial;
		
		
	
 
		
		
		$cust_id = 'GLOBAL';
		$this->view->custid = $cust_id;
		
		$selectccy = $this->_db->select()
             ->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
            
	    $listccy = $selectccy->query()->fetchAll();
		$this->view->listccy = $listccy;
//var_dump($listccy);die;		

		$submit = $this->_getParam('submit');
		if($submit)
			{

					$this->_db->beginTransaction();
					//die;
					try
					{
				    //Zend_Debug::dump($resultunion); die;
					// $info = 'Charges';
					// $info2 = $this->language->_('Set Charges Monthly per Account');
					
					// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_MONTHLY','TEMP_CHARGES_MONTHLY',$custid,$custname,$custid,null,'monthlyaccount');
					$cust_id = 'SPECIAL';
					$info = 'Global Charges';
						$info2 = 'Set Realtime Charges';
						
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_WITHIN','TEMP_CHARGES_WITHIN','','Realtime Local Transfer Charges','',null,'specialcharges');
					// print_r($resultunion);die;
					$resultunion= array('1','2','8');

					$idamt = 'pbspecial';
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);

					$data= array(
												'CHANGES_ID' 		=> $change_id,
												'CUST_ID' 			=> $cust_id,
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $amt,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> NULL,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);	

					$idamt = 'pbglobal';
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);

					$data= array(
												'CHANGES_ID' 		=> $change_id,
												'CUST_ID' 			=> 'GLOBAL',
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $amt,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> NULL,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);	

					$resultunion= array('1','2','8');
					foreach($resultunion as $row)
					{
						
							$idamt = 'typespecial'.$row;
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
							
								$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> $row,
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $amt,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

							$this->_db->insert('TEMP_CHARGES_OTHER',$data1);									
								//echo $row['ACCT_NO'].' aaa<br />';
								//Zend_Debug::dump($data);
									
					}

					foreach($resultunion as $row)
					{
						
							$idamt = 'pstype'.$row;
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
							
								$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> 'GLOBAL',
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> $row,
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $amt,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

							$this->_db->insert('TEMP_CHARGES_OTHER',$data1);									
								//echo $row['ACCT_NO'].' aaa<br />';
								//Zend_Debug::dump($data);
									
					}
					
					
					
					//if($error == 0)
				//	{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/charges/specialcharge');
						$this->_redirect('/notification/submited/index');
				//	}
				}
				
				catch(Exception $e)
				{
					var_dump($e);die;
					//die;
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}

			}
		
			
		


	}



}
