<?php

require_once 'Zend/Controller/Action.php';


class important_EditController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$HELP_ID   	= $this->_getParam('ID');
		$this->view->HELP_ID = $HELP_ID;
		
		$filterArr = array(	'filter'=> array('StringTrim','StripTags'),
							'submit'=> array('StringTrim','StripTags'));
		
		$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$filter 	= $zf_filter->getEscaped('filter');
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}	
		}

		$select = $this->_db->select()
										->from(	'M_NOTIFICATION',
												array(
														'ID',
												'SUBJECT', 
												'DATE_TYPE',
												'EXP_DATE', 
												'EXP_TIME', 
												'EFDATE',
												'EFTIME',
												'CONTENT',
												'TARGET',
												'SEND_TO',
												'RECEIVER',
												'FORCE_READ',
												'CREATED',
												'CREATEDBY', 
												'STATUS'))
										->where('ID =?',$HELP_ID);

		$data = $this->_db->fetchRow($select);

			if($this->_request->isPost())
			{

				$params = $this->_request->getParams();

				$errorRemark = null;
				
				$filters = array(
									'helpTopic'	=> array('StripTags', 'StringTrim'),
									'ckeditor'	=> array('StripTags', 'StringTrim'),
									'to_date'	=> array('StripTags', 'StringTrim'),
									'to_time'	=> array('StripTags', 'StringTrim'),
								);
				$exclude = "HELP_ID != $HELP_ID and HELP_ISDELETED != 1";
				$validators = array(
										'helpTopic' => array(
																	'NotEmpty',
																	array('StringLength', array('max' => 50)),
																	new Zend_Validate_Regex('/^[a-zA-Z 0-9]*$/'),
																	array('Db_NoRecordExists',array(
																									'table'=>'T_HELP',
																									'field'=>'HELP_TOPIC',
																									'exclude'=>$exclude
																								)
																			),
																	'messages' => array(
																							$this->language->_("Error : Help Topic cannot be left blank"),
																							$this->language->_("Error : Help Topic size more than 50 char"),
																							$this->language->_("Error : Help Topic Format Must Alpha Numeric"),
																							$this->language->_("Error : Help Topic Already Exist")
																						)
															),
										'ckeditor' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Can not be empty'),
																				)
														),
										'to_date' => array(
																'NotEmpty',
																'messages' => array(
																						$this->language->_('Can not be empty'),
																					)
															),
										'to_time' => array(
																'NotEmpty',
																'messages' => array(
																						$this->language->_('Can not be empty'),
																					)
															),
									);
									
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
				$description 	= ($zf_filter_input->isValid('description'))? $zf_filter_input->description : $this->_request->getParam('description'); 
				$helpTopic 		= ($zf_filter_input->isValid('helpTopic'))? $zf_filter_input->helpTopic : $this->_request->getParam('helpTopic'); 
			
				// $helpTopic 		= $this->getRequest()->getParam('helpTopic');
				// $description 	= $this->getRequest()->getParam('description');

				$expdate = Application_Helper_General::convertDate($params['to_date'],$this->_dateDBFormat,$this->defaultDateFormat);
				$exptime = $params['to_time'];

				$fexpdate = $expdate.' '.$exptime; 

				$curDate = date("Y-m-d H:i:s");

				if($params['postdatetype'] == 1){
					$postdate = $data['EFDATE'];
					$posttime = $data['EFTIME'];
					$fpostdate = $postdate.' '.$posttime;

					if ($fpostdate > $fexpdate) {
						$status = 2;
					}
					else if ($curDate < $fexpdate) {
						$status = 3;
					}
					else{
						$status = 1;
					}

				}else{
					$postdate = Application_Helper_General::convertDate($params['from_date'],$this->_dateDBFormat,$this->defaultDateFormat);
					$posttime = $params['from_time'];
					$fpostdate = $postdate.' '.$posttime;

					if ($fpostdate > $fexpdate) {
						$status = 2;
					}
					else if ($curDate < $fpostdate) {
						$status = 3;
					}
					else{
						$status = 1;
					}
				}


				if($zf_filter_input->isValid())
				{
					try
					{
						$this->_db->beginTransaction();

						$param = array(
							'CREATEDBY'			=> $this->_userIdLogin, 
							'CREATED' 		=> new Zend_Db_Expr('now()'), 
							'SUBJECT' 			=> $params['helpTopic'],
							'EFDATE'		=> $postdate,
							'EFTIME'		=> $posttime,
							'EXP_DATE'		=> $expdate,
							'EXP_TIME' 		=> $exptime,
							'CONTENT'		=> $params['ckeditor'],
							'TARGET'		=> $params['target_type'],
							'SEND_TO'		=> $params['sendto'],
							'RECEIVER'		=> $params['receiver_name'],
							'FORCE_READ'		=> $params['force_read'],
							'DATE_TYPE'		=> $params['postdatetype'],
							'STATUS'		=> $status,
						);	
						
						$where = array('ID = ?' => $HELP_ID);
						$query = $this->_db->update ( "M_NOTIFICATION", $param, $where );
						$this->_db->commit();
						Application_Helper_General::writeLog('HLUD','Edit Important Notification. Important Notification ID: ['.$HELP_ID.'], File name : ['.$data['HELP_FILENAME'].']');
						$this->setbackURL('/important');
						$this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						$this->_db->rollBack();
						/*die($e);
						$errorRemark = $this->getErrorRemark('82');
						$this->view->errorMsg = array(array($errorRemark));*/
					//	SGO_Helper_GeneralLog::technicalLog($e);
					}
				}
				else
				{
					$this->view->error 				= true;
					$errors 						= $zf_filter_input->getMessages();
					
					$this->view->helpTopicErr			= (isset($errors['helpTopic']))? $errors['helpTopic'] : null;
					$this->view->descriptionErr 		= (isset($errors['description']))? $errors['description'] : null;
				}	
			}
		$this->view->file = $data['HELP_FILENAME'];
		if($this->_request->isPost())
		{
			$this->view->helpTopic = (isset($zf_filter_input->helpTopic)) ? $zf_filter_input->helpTopic : $this->getRequest()->getParam('helpTopic');
			$this->view->description = (isset($zf_filter_input->description)) ? $zf_filter_input->description : $this->getRequest()->getParam('description');
		}
		else
		{
			$this->view->subject = $data['SUBJECT'];
			$this->view->datetype = $data['DATE_TYPE'];
			$this->view->expdate = $data['EXP_DATE'];
			$this->view->exptime = $data['EXP_TIME'];
			$this->view->efdate = $data['EFDATE'];
			$this->view->eftime = $data['EFTIME'];
			$this->view->content = $data['CONTENT'];
			$this->view->target = $data['TARGET'];
			$this->view->sendto = $data['SEND_TO'];
			$this->view->receiver = $data['RECEIVER'];
			$this->view->forceread = $data['FORCE_READ'];
			
			
			
			
			
			$description = $data['NEWS'];
			$this->view->news = $description;
		}
		$disable_note_len 	 = (isset($description))  ? strlen($description)  : 0;	
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
		
		$disable_note_len 	 = 100 - $disable_note_len;		
		$note_len 	 = 100 - $note_len;	

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('HLUD','Viewing Edit Help');
		}
						
	}
}
