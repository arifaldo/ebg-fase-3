<?php
require_once 'Zend/Controller/Action.php';
class Debitgroup_approveacctgroupController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');

		$fields = array(
						'name'  => array('field' => 'GROUP_ID',
											   'label' => $this->language->_('Card Group Name'),
											   'sortable' => false),
						'debitnumber'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Card Group Number'),
											   'sortable' => false)
						
				);

		//get sortby, sortdir
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	=> array('StringTrim','StripTags'),
							'number'    => array('StringTrim','StripTags'),
							'name'    	=> array('StringTrim','StripTags','StringToUpper'),
							'alias' 	=> array('StringTrim','StripTags','StringToUpper'),
							'group'     => array('StringTrim','StripTags')
		);

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$info = $this->_db->fetchRow(
				  $this->_db->select()
					   ->from("T_CUST_ACCT_GROUP_CHANGES")
					   ->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   //->where("SUGGEST_STATUS <> 4 ")
		);
		//var_dump($info); die;
		$suggest = $this->_db->select()
					   ->from(array('A' => 'TEMP_DEBIT_GROUP'))
					   ->joinLeft(array('G' => 'M_DEBITGROUP'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'))
					   ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   ->where("A.GROUP_ID>0 AND A.GROUP_ID IS NOT NULL")
					   ->order('G.GROUP_NAME ASC')
					   ->order($sortBy.' '.$sortDir)
					   ->query()->fetchAll();

		$current = $this->_db->select()
					   ->from(array('A' => 'T_DEBIT_GROUP'),array('*'))
					   ->joinLeft(array('G' => 'M_DEBITGROUP'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'))
					   ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   ->where("A.GROUP_ID>0 AND A.GROUP_ID IS NOT NULL")
					    ->order('G.GROUP_NAME ASC')
					   ->order($sortBy.' '.$sortDir)
					   ->query()->fetchAll();
		
		 
		// Zend_Debug::dump($current);
		$suggestArr = array();
		if(count($current) > 0 && count($suggest) > 0)
		{
			$arrCurrentAcct = array();
			foreach($current as $row) {
				$arrCurrentAcct[]=$row['ACCT_NO'];
			}
			$this->view->arrCurrentAcct = $arrCurrentAcct;

			$arrSuggestAcct = array();
			foreach($suggest as $row)
			{			
			//	$suggestArr[$row['GROUP_ID']] = $row;
				$arrSuggestAcct[]=$row['ACCT_NO'];
			}
			$this->view->arrSuggestAcct = $arrSuggestAcct;
		}
// 
			if(!empty($suggest)){
				$temp = '';
				foreach($suggest as $row)
				{
					$temp .= ','.$row['DEBIT_NUMBER'];
					//$row['DEBIT_NUMBER_ALL'] .= $temp; 
					if($suggestArr[$row['GROUP_ID']]['GROUP_ID'] == $row['GROUP_ID']){
						$row['DEBIT_NUMBER_ALL'] .= $suggestArr[$row['GROUP_ID']]['DEBIT_NUMBER'].','.$row['DEBIT_NUMBER']; 
					}
					$suggestArr[$row['GROUP_ID']] = $row;
					//$arrSuggestAcct[]=$row['ACCT_NO'];
				}
			}

		//Zend_Debug::dump($suggestArr);die;
		$this->view->suggestInfo = $info;
		$this->view->suggestData = $suggestArr;
		$this->view->currentData = $current;
		$this->view->fields = $fields;

		//ganti status read jika masih unread
		$status = $this->_db->fetchOne(
				  $this->_db->select()
					   ->from("T_CUST_ACCT_GROUP_CHANGES",array("SUGGEST_STATUS"))
					   ->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   );
		if($status==0)
		{
			$where['CUST_ID = ?'] = $this->_custIdLogin;
			$updateArr = array('SUGGEST_STATUS' => 1);
			$this->_db->update('T_CUST_ACCT_GROUP_CHANGES',$updateArr,$where);
		}

		if($this->_request->isPost() )
		{
			//Zend_Debug::Dump($this->getRequest()->getParams());die;
			//$submit = $this->getRequest()->getParam('submit');

			$submit_approve = $this->getRequest()->getParam('submit_approve');
			$submit_reject = $this->getRequest()->getParam('submit_reject');
			$submit_repair = $this->getRequest()->getParam('submit_repair');

			$suggestId = $this->getRequest()->getParam('suggest_id');
			$suggestId = (Zend_Validate::is($suggestId,'Digits'))? $suggestId : null;
			if($suggestId)
			{
				//if($submit=='Approve')
				if($submit_approve==TRUE)
				{
					try
					{
						$this->_db->beginTransaction();

						$tempData = $this->_db->select()
									   ->from('TEMP_CUSTOMER_ACCT_GROUP',array('ACCT_NO','GROUP_ID','ACCT_ALIAS_NAME','CUST_ID'))
									   ->where("SUGGEST_ID = ?",$suggestId)
									   ->order($sortBy.' '.$sortDir)
									   ->query()->fetchAll();

							$updateAllArr = array('GROUP_ID' => NULL);
							$updateallWhere['CUST_ID = ?'] = $tempData['0']['CUST_ID'];
							$this->_db->update('M_CUSTOMER_ACCT',$updateAllArr,$updateallWhere);

						foreach ($tempData as $row)
						{
							$updateArr = array_diff_key($row,array('ACCT_NO'=>''));
							$updateWhere['ACCT_NO = ?'] = (string)$row['ACCT_NO'];
							$this->_db->update('M_CUSTOMER_ACCT',$updateArr,$updateWhere);
						}

						$deleteWhere['SUGGEST_ID = ?'] = $suggestId;
						$this->_db->delete('T_CUST_ACCT_GROUP_CHANGES',$deleteWhere);
						$this->_db->delete('TEMP_CUSTOMER_ACCT_GROUP',$deleteWhere);

						$this->_db->commit();
						Application_Helper_General::writeLog('GRAC','Approving Account Group');

						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();

						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						Application_Helper_General::writeLog('GRAC','Roll Back Approving Delete Account Group');
					}
				}
				//else if($submit=="Request Repair")
				else if($submit_repair==$this->language->_('Request Repair'))
				{
					try
					{
						$this->_db->beginTransaction();

						$updateArr = array(
										'SUGGEST_STATUS'=>	4,
										'SUGGEST_GRANTER'=>	$this->_userIdLogin,
										'SUGGEST_GRANTDATE'=> new Zend_Db_Expr("now()"),
										'SUGGEST_REASON'=>	$this->getRequest()->getParam('PS_REASON')
										);
						$where1['SUGGEST_ID = ?'] = $suggestId;
						$this->_db->update('T_CUST_ACCT_GROUP_CHANGES',$updateArr,$where1);

						$this->_db->commit();

						Application_Helper_General::writeLog('GRAC','Approving Request Repair Account Group');
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();

						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						Application_Helper_General::writeLog('GRAC','Roll Back Approving Delete Account Group');
					}
				}
				//else if($submit=="Reject")
				else if($submit_reject==TRUE)
				{
					try
					{
						$this->_db->beginTransaction();

						$deleteWhere['SUGGEST_ID = ?'] = $suggestId;
						$this->_db->delete('T_CUST_ACCT_GROUP_CHANGES',$deleteWhere);
						$this->_db->delete('TEMP_CUSTOMER_ACCT_GROUP',$deleteWhere);

						$this->_db->commit();

						Application_Helper_General::writeLog('GRAC','Roll Back Rejecting Account Group');

						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();

						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						Application_Helper_General::writeLog('GRAC','Roll Back Approving Rejecting Account Group');
					}
				}
			}
			else
			{
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			}

			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}
			}
		}
		Application_Helper_General::writeLog('GRAC','Viewing Approve Account Group');
	}

	public function deleteAction()
	{
		$suggestId = $this->_getParam('suggest_id');
		$suggestId = (Zend_Validate::is($suggestId,'Digits'))? $suggestId : null;
		if($suggestId)
		{
			try
			{
				$this->_db->beginTransaction();

				$where['SUGGEST_ID = ?'] = $suggestId;
				$this->_db->delete('T_CUST_ACCT_GROUP_CHANGES',$where);
				$this->_db->delete('TEMP_CUSTOMER_ACCT_GROUP',$where);

				$this->_db->commit();

				Application_Helper_General::writeLog('GRAM','Deleting Account Group'.$suggestId);

				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
				$this->_redirect('/notification/success');
			}
			catch(Exception $e)
			{
				//rollback changes
				$this->_db->rollBack();
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

				Application_Helper_General::writeLog('GRAM','Roll Back Deleting Account Group'.$suggestId);
			}
		}
		else
		{
		   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
		   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
		   Application_Helper_General::writeLog('GRAM','Invalid Data Deleting Account Group'.$suggestId);
		}

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
	}
}
