<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class binsetup_RepairController extends binsetup_Model_Binsetup{

  public function indexAction(){
    
    $this->_helper->layout()->setLayout('popup');
    
    $this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');
    $this->view->customer_msg = array();

  	$changes_id = $this->_getParam('changes_id');
  	$changes_id = (Zend_Validate::is($changes_id,'Digits'))? $changes_id : 0;
  	$customer_view = 1; $error_remark = null; $key_value = null;
  	
  	//jika change id ada isinya maka true
  	if($changes_id)
  	{ 
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID','CHANGES_TYPE','CHANGES_REASON'))
                             ->where('CHANGES_ID='.$this->_db->quote($changes_id));
      $globalChanges = $this->_db->fetchRow($select);
      
      //jika change id ada di database maka true
      if($globalChanges['CHANGES_ID'])
      {
         $select = $this->_db->select()
                               ->from('TEMP_CUSTOMER_BIN',array('TEMP_ID','CUST_ID','CUST_BIN'))
      	                       ->where('CHANGES_ID='.$this->_db->quote($changes_id));
      	 $result = $this->_db->fetchRow($select);
      	
      	 $expid 			= "CUST_BIN != ".$this->_db->quote($result['CUST_BIN']);
		
      	 if($result['TEMP_ID'])
      	 {
      	   $change_type = strtoupper($globalChanges['CHANGES_TYPE']);
      	   $changes_reason = $globalChanges['CHANGES_REASON'];
      	   if($change_type==strtoupper($this->_changeType['code']['edit']))$key_value = strtoupper($result['CUST_ID']);
      	 }
      	 else{ $changes_id = 0; }
      }
      else{ $changes_id = 0; } 
  	}
  	
  	//jika change id tidak ada isinya maka error
    if(!$changes_id)
    {
      $error_remark = 'Changes Id not found';
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL); 	
    }
  	
    
    
    
  	if($this->_request->isPost())
  	{
  	
  	    $haystack_cust_type = array($this->_custType['code']['individual'],$this->_custType['code']['company']);
        // $haystack_step_approve = array($this->_masterhasStatus['code']['yes'],$this->_masterhasStatus['code']['no']);
		 $customer_filter = array();

            $filters = array('cust_id'           => array('StripTags','StringTrim','StringToUpper'),
			                 'bin'         		 => array('StripTags','StringTrim'),
			                 'cust_bin_status'   => array('StripTags','StringTrim'),
            );

	     $validators =  array(
						  	'cust_id'        => array('NotEmpty',
      	                                                   'Alnum',
      	                                                    array('StringLength',array('min'=>5,'max'=>12)),
      	                                                       'messages' => array($this->language->_('Can not be empty'),
      	                                                                           $this->language->_('Invalid Company Code Format'),
      	                                                                           $this->language->_('Invalid Company Code Format'))
      	                                                      ),

							 'bin'        => array('NotEmpty',
      	                                                   'Digits',
      	                                                    array('StringLength',array('min'=>1,'max'=>5)),
      	                                                       array('Db_NoRecordExists',array('table'=>'M_CUSTOMER_BIN','field'=>'CUST_BIN','exclude'=>$expid)),
      	                                                       array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER_BIN','field'=>'CUST_BIN','exclude'=>$expid)),
      	                                                       'messages' => array($this->language->_('Can not be empty'),
      	                                                                           $this->language->_('Invalid BIN Format'),
      	                                                                           $this->language->_('BIN lenght cannot be more than 5'), //length
      	                                                                           $this->language->_('BIN already in use. Please use another'),
      	                                                                           $this->language->_('BIN already suggested. Please use another'))
      	                                                      ),
						                                                       
							);
  	
  	
     	 $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
      
	  if($zf_filter_input->isValid())
	  {
	           $info = "BIN";
	  		   $cust_data = $this->_custData;
	  	       foreach($validators as $key=>$value)
	  	       {
	  	          if($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;
	  	       }
	  	
	  	try 
	  	{
		  $this->_db->beginTransaction();
		  
		  $this->updateGlobalChanges($changes_id,$info,null,null,$cust_data['CUST_ID'],$cust_data['CUST_NAME'],$cust_data['CUST_NAME']);
	  	  //$this->insertTempCustomer($zf_filter_input->changes_id,$cust_data);
	  	  
		  //log CRUD
		  $cust_data['CUST_BIN_STATUS'] = $this->_getParam('cust_bin_status');
		  Application_Helper_General::writeLog('BNCR','Customer BIN is repaired, Cust Id : '.$cust_data['CUST_ID']. ' Bin : '.$cust_data['BIN'].' Change id : '.$changes_id);
	  	  $this->updateTempCustomer($changes_id,$cust_data);
	  	  
          $this->_db->commit();
          $this->_redirect('/popup/successpopup');
	  	}
		catch(Exception $e){
		  $this->_db->rollBack();
		  $error_remark = $this->language->_('database error');
		  // SGO_Helper_GeneralLog::technicalLog($e);
		}
	    //insert log
	    if(isset($error_remark)){ $class = 'F'; $msg = $error_remark; }
		else
		{ 
		  $class = 'S';
		  $msg = 'success';
		}
		
		$this->_helper->getHelper('FlashMessenger')->addMessage($class);
		$this->_helper->getHelper('FlashMessenger')->addMessage($msg);
        $this->_redirect($this->_backURL); 
        
	  }//END if($zf_filter_input->isValid())
	  else
	  { 
	    $this->view->cust_id   = ($zf_filter_input->isValid('cust_id'))? $zf_filter_input->cust_id : $this->_getParam('cust_id'); 
	  	$this->view->bin = ($zf_filter_input->isValid('bin'))? $zf_filter_input->bin : $this->_getParam('bin'); 
        $this->view->cust_bin_status = ($zf_filter_input->isValid('cust_bin_status'))? $zf_filter_input->cust_bin_status : $this->_getParam('cust_bin_status'); 
        
        
		$error = $zf_filter_input->getMessages();
		
		//format error utk ditampilkan di view html 
        $errorArray = null;
        foreach($error as $keyRoot => $rowError)
        {
           foreach($rowError as $errorString)
           {
              $errorArray[$keyRoot] = $errorString;
           }
        }

        //pengaturan error untuk cust emobile, phone dan fax
        $this->view->customer_msg  = $errorArray;

	  }
    }//END if($this->_request->isPost())
    else 
    {
       $resultdata = $this->getTempCustomer($changes_id);  
       
       $this->view->cust_id        = strtoupper($resultdata['CUST_ID']);
       $this->view->bin      	   = $resultdata['CUST_BIN'];
       $this->view->cust_bin_status    = $resultdata['CUST_BIN_STATUS'];
    }
    
    
    
    $this->view->changes_id = $changes_id;
    $this->view->changes_type = $change_type;
    $this->view->cust_status = $this->_masterStatus['desc']['active'];
    $this->view->change_type = $this->_changeType;
    $this->view->modulename = $this->_request->getModuleName();
    $this->view->customer_type = $this->_custType;
    $this->view->approve_type = $this->_masterhasStatus;
    $this->view->lengthCustId = $this->_custIdLength;
    $this->view->changes_reason   = $changes_reason;

    //insert log
	try 
	{
	  $this->_db->beginTransaction();
	 
	  Application_Helper_General::writeLog('BNCR','View Repair Customer BIN .Changes ID :['.$changes_id.']');
	  
	  $this->_db->commit();
	}
    catch(Exception $e)
    {
 	  $this->_db->rollBack();
	  // SGO_Helper_GeneralLog::technicalLog($e);
	}
	
	
  }
  
  
}



