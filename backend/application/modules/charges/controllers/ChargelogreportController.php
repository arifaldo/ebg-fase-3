<?php
require_once 'Zend/Controller/Action.php';
class charges_ChargelogreportController extends customer_Model_Customer
{	

	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{	
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->params = $this->_request->getParams();

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->customer_msg = $msg;
			}	
		}
		
		$custArr  = Application_Helper_Array::listArray($this->getAllCustomer(),'CUST_ID','CUST_ID');
		//$custArr  = array_merge(array(''=>'-- Any Value --'),$custArr);
		
		/*	charges status:
				1 = Not Charged
				2 = Charged
		*/
		
		//Zend_Debug::Dump($this->_chargestatus["code"]);
		//Zend_Debug::Dump($this->_chargestatus["desc"]);
		
		$arrChargeStatus = array_combine($this->_chargestatus["code"],$this->_chargestatus["desc"]);
		foreach($arrChargeStatus as $key => $value){
			if($key != '13'){ $arrStatusK[$key] = $this->language->_($value); }
		
		}
		
		
		$arrtype 	= array_combine($this->_chargestype["code"], $this->_chargestype["desc"]);
		$chargeType  = array_merge(array(''=>'--  '.$this->language->_('Any Value'). ' --'),$arrtype);
		
		$this->view->custArr 	= $custArr;
		$this->view->arrStatus 	= $arrStatusK;
		$this->view->chargeType = $chargeType;
		
		$fields = array	(
							'company'  			=> array	(	
																'field' => 'company',
																'label' => $this->language->_('Company'),
																'sortable' => true
															),
							// 'custname'  		=> array	(
							// 									'field' => 'custname',
							// 									'label' => $this->language->_('Company Name'),
							// 									'sortable' => true
							// 								),
							'logdatetime' 		=> array	(
																'field' => 'logdatetime',
																'label' => $this->language->_('Charging Date'),
																'sortable' => true
															),
							'acctno'  			=> array	(
																'field' => 'acctno',
																'label' => $this->language->_('Source Account'),
																'sortable' => true
															),
							// 'ccy'  				=> array	(
							// 									'field' => 'ccy',
							// 									'label' => $this->language->_('Currency'),
							// 									'sortable' => true
							// 								),
							'acctcoa'  			=> array	(
																'field' => 'acctcoa',
																'label' => $this->language->_('COA Account'),
																'sortable' => true
															),
							'amount'  			=> array	(
																'field' => 'amount',
																'label' => $this->language->_('Amount'),
																'sortable' => true
															),
							'status'  			=> array	(
																'field' => 'status',
																'label' => $this->language->_('Status'),
																'sortable' => true
															),
							'chargestype'  		=> array	(
																'field' => 'chargestype',
																'label' => $this->language->_('Charge Type'),
																'sortable' => true
															),
							'paymenttype'		=> array	(	
																'field'	=>'paymenttype',
																'label'	=> $this->language->_('Payment Type'),
																'sortable'=>true
																),
							'description'  		=> array	(
																'field' => 'description',
																'label' => $this->language->_('Description'),
																'sortable' => true
															)
						);

		$filterlist = array('PS_CHARGES','COMP_ID','COMP_NAME','SOURCE_ACCOUNT','STAT_CHARGES','CHARGES_TYPE','Description');
		
		$this->view->filterlist = $filterlist;
						
		$page 	= $this->_getParam('page');
		$csv 	= $this->_getParam('csv');
		$pdf 	= $this->_getParam('pdf');
		$filter = $this->_getParam('filter');
		$filter_clear = $this->_getParam('filter_clear');
		
		//validasi page, jika input page bukan angka               
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortDir = $this->_getParam('sortdir');
		//$filter 	= $this->_getParam('filter');

		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : 'logdatetime';
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$filterArr = array('COMP_ID'     	=> array('StripTags','StringTrim','StringToUpper'),
						   'COMP_NAME'   	=> array('StripTags','StringTrim','StringToUpper'),
						   'PS_CHARGES'  	=> array('StripTags','StringTrim'),
						   'PS_CHARGES_END' 		=> array('StripTags','StringTrim'),
						   'SOURCE_ACCOUNT'  => array('StripTags','StringTrim'),
		
						   'STAT_CHARGES' 	=> array('StripTags','StringTrim'),
						   'CHARGES_TYPE'     => array('StripTags','StringTrim'),
						   'Description'    => array('StripTags','StringTrim')
						  );
		
		// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','COMP_NAME','SOURCE_ACCOUNT','STAT_CHARGES','CHARGES_TYPE','Description');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_CHARGES"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('chargesdate'))){
				$chargesarr = $this->_request->getParam('chargesdate');
					$dataParamValue['PS_CHARGES'] = $chargesarr[0];
					$dataParamValue['PS_CHARGES_END'] = $chargesarr[1];
			}
		
		//echo "<pre>";
		//print_r ($dataParamValue);
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($custArr)))),
						'COMP_NAME' 	=> array(),	
						'PS_CHARGES' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_CHARGES_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'SOURCE_ACCOUNT' => array(),	
						'STAT_CHARGES' 	=> array(array('InArray', array('haystack' => array_keys($arrStatusK)))),	
						'CHARGES_TYPE' 	=> array(array('InArray', array('haystack' => array_keys($chargeType)))),	
						'Description' 	=> array(),
							
						);

		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');	
		$fCOMPANYID      = $zf_filter->getEscaped('COMP_ID');
		$fCOMPANYNAME    = $zf_filter->getEscaped('COMP_NAME');

		if($filter == TRUE){
			$fCHARGEFROM     = $zf_filter->getEscaped('PS_CHARGES');
			$fCHARGETO       = $zf_filter->getEscaped('PS_CHARGES_END');
		}
		if($filter_clear == TRUE){
			$fCHARGEFROM = "";
			$fCHARGETO = "";
		}
		else if(!$filter){
			$fCHARGEFROM = NULL; //(date("d/m/Y"));
			$fCHARGETO = NULL; //(date("d/m/Y"));
		}

		$fACCOUNTSOURCE  = $zf_filter->getEscaped('SOURCE_ACCOUNT');
		
		$fSTATUSCHARGE   = $zf_filter->getEscaped('STAT_CHARGES');
		//echo "statusCHARGE: $fSTATUSCHARGE";
		
		$fCHARGETYPE     = $zf_filter->getEscaped('CHARGES_TYPE');
		$fDESCRIPTION    = $zf_filter->getEscaped('Description');
		
		//echo "chargetype:$fCHARGETYPE";
		
		// Select Data
		$select = array();
	    $select = $this->_db->select()
							->from(array('T'=>'T_CHARGES_LOG'),array(	'logdatetime'	=>'LOG_DATETIME',
																		'acctno'		=>'CHARGES_ACCT_NO',
																		'ccy'			=>'CCY',
																		'amount'		=>'AMOUNT',
																		'status'		=>'STATUS',
																		'chargestype'	=>'CHARGES_TYPE',
																		'paymenttype'	=>'PAYMENT_TYPE',
																		'acctcoa'		=>'COA',
																		'description'	=>'DESCRIPTION',
																		'company'	=> new Zend_Db_Expr("CONCAT(M.CUST_NAME , ' (' , M.CUST_ID , ')  ' )")
																	)
																	)
							->joinLeft(array('M'=>'M_CUSTOMER'),'T.CUST_ID = M.CUST_ID',array(	'custid'=>'M.CUST_ID',
									'custname'=>'M.CUST_NAME'
									
																				 ))
							->where("STATUS != '13'");
		// echo "<pre>";
		// echo $select;die;
		// echo $select->__toString();
		// echo "<br />Data sql: ";
		// print_r($dataSQL);
		// die();
		
		if($fCOMPANYID)		$select->where('UPPER(M.CUST_ID) = '.$this->_db->quote(strtoupper($fCOMPANYID))); 
		if($fCOMPANYNAME)	$select->where('UPPER(M.CUST_NAME) LIKE '.$this->_db->quote(strtoupper('%'.$fCOMPANYNAME.'%')));
			
		if($fCHARGEFROM)	{
				
			$FormatDate = new Zend_Date($fCHARGEFROM, $this->_dateDisplayFormat);
			$datefrom   = $FormatDate->toString($this->_dateDBFormat);				
			$select->where('DATE(T.LOG_DATETIME) >= '.$this->_db->quote(strtoupper($datefrom)));
			
		}
		if($fCHARGETO) {
				
			$FormatDate = new Zend_Date($fCHARGETO, $this->_dateDisplayFormat);
			$dateto  	= $FormatDate->toString($this->_dateDBFormat);				
			$select->where('DATE(T.LOG_DATETIME) <= '.$this->_db->quote(strtoupper($dateto)));
				
		}
			
		if($fACCOUNTSOURCE)	$select->where('T.CHARGES_ACCT_NO LIKE '.$this->_db->quote(strtoupper('%'.$fACCOUNTSOURCE.'%')));
		if($fSTATUSCHARGE != NULL) $select->where('T.STATUS = '.$this->_db->quote(strtoupper($fSTATUSCHARGE)));
		
		if($fCHARGETYPE !="")	$select->where("T.CHARGES_TYPE = $fCHARGETYPE");
		if($fDESCRIPTION)	$select->where('T.DESCRIPTION LIKE '.$this->_db->quote(strtoupper('%'.$fDESCRIPTION.'%')));
		
		$select->order($sortBy.' '.$sortDir);
		/*
		if($sortBy != 'logdatetime')
		{
			$sortBy2 = "logdatetime";
			$sortDir3 = "desc";
			$select->order($sortBy2.' '.$sortDir3);
		}*/
		//die($select);
		$dataSQL = $this->_db->fetchAll($select);
		//echo "<pre>";
		//echo $select->__toString();

		
		
		if ($csv || $pdf)
		{	$header  = Application_Helper_Array::simpleArray($fields, "label");}
		else
		{
			$this->paging($dataSQL);
			$dataSQL = $this->view->paginator;
			
		}
		
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));

		if(!empty($dataSQL) && count($dataSQL) > 0){
			
			foreach ($dataSQL as $d => $dt){	
				// print_r($dt);die;
				foreach ($fields as $key => $field)
				{
									
					$value = $dt[$key];
					$paytype = $dt["paymenttype"];
									
					if ($key == "amount" && !$csv){	$value = $dt['ccy'].' '.Application_Helper_General::displayMoney($value); }
					elseif($key == "logdatetime"){ $value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat); }
					elseif($key == "status"){ $value = $this->getStatusCharge($value); }
					elseif($key == "chargestype"){ $value = $this->getChargesType($value,$paytype);}
					elseif($key == "paymenttype"){ $value = isset($typearr[$value]) ? $typearr[$value] : 'N/A';}
					
					
					$value = ($value == "" && !$csv)? "&nbsp;": $value;
					
					$data[$d][$key] = $value;
					$paytype = "";
				}
			
			}
		}
		else $data = array();
		
		//echo "<pre>";
		// Zend_Debug::Dump($data);die;
		//Zend_Debug::Dump($dataSQL);
		
		
		if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Charge Log Report', 'data_header' => $fields));
		}
		
		if($csv)
		{
			$this->_helper->download->csv($header,$data,null,'Charge Log Report');  
			Application_Helper_General::writeLog('RPCL','Export CSV Charge Log Report');

		}
		elseif($pdf)
		{
			
			$this->_helper->download->pdf($header,$data,null,'Charge Log Report');  
			Application_Helper_General::writeLog('RPCL','Export PDF Charge Log Report');
		
		}
		else
		{	
		
			$this->view->data = $data;
			
			$stringParam = array('COMP_ID'		=> $fCOMPANYID,
								 'COMP_NAME'		=> $fCOMPANYNAME,
								 'PS_CHARGES'		=> $fCHARGEFROM,
								 'PS_CHARGES_END'			=> $fCHARGETO,
								 'SOURCE_ACCOUNT'	=> $fACCOUNTSOURCE,
								 'STAT_CHARGES'		=> $fSTATUSCHARGE,
								 'CHARGES_TYPE'		=> $fCHARGETYPE,
								 'DESCRIPTION'		=> $fDESCRIPTION,
								 'filter'			=> $filter,
							    );	
							    
			$this->view->COMPANYID 		= $fCOMPANYID;
			$this->view->COMPANYNAME 	= $fCOMPANYNAME;
			$this->view->CHARGEFROM 	= $fCHARGEFROM;
			$this->view->CHARGETO 		= $fCHARGETO;
			$this->view->ACCOUNTSOURCE 	= $fACCOUNTSOURCE;
			$this->view->STATUSCHARGE 	= $fSTATUSCHARGE;
			
			$this->view->CHARGETYPE 	= $fCHARGETYPE;
			$this->view->DESCRIPTION 	= $fDESCRIPTION;
			
			// $this->view->query_string_params = $stringParam;
			// $this->updateQstring();
			
			$this->view->data 			= $data;
			$this->view->fields 		= $fields;
			$this->view->filter 		= $filter;
			
			$this->view->custArr 		= $custArr;
			$this->view->arrStatus 		= $arrStatusK;
			$this->view->chargeType		= $chargeType;
			
			Application_Helper_General::writeLog('RPCL','View Charges Log Report');

				if(!empty($dataParamValue)){
	    		$this->view->chargesdateStart = $dataParamValue['PS_CHARGES'];
	    		$this->view->chargesdateEnd = $dataParamValue['PS_CHARGES_END'];

	    	  	unset($dataParamValue['PS_CHARGES_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
		}
		
		
		
	}	
	
	function getStatusCharge($value){
	
		/*	charges status:
				1 = Not Charged
				2 = Charged
		*/
		
		$arrStatus = array_combine($this->_chargestatus["code"],$this->_chargestatus["desc"]);
		
		foreach($arrStatus as $key => $val){
			if($key == $value){	$status = $val; }
			
		}
		
		return $status;
	}
	
	function getChargesType($value,$paytype){
	
		//charges.type.code
		//charges.type.desc
		//echo "paytype: $paytype";
		$config = Zend_Registry::get('config');
		
		$ctype 	= $config["charges"]["type"]["code"];
		$cdesc 	= $config["charges"]["type"]["desc"];
		$payall = array_combine($ctype, $cdesc);
		
		$paytypenoempty = array(2,3,4,5,6,7,8);
		$emptyok = array(1,9); // Payment type: Within, Settlement
		
		if(($paytype == 1 && $value == 0) || ($paytype == 9 && $value == 0)) {  
			$chargetype = "-";
		}
		else{
			foreach($payall as $key => $val){
				if($key == $value){	$chargetype = $val; }
			}
		}
			
		return $chargetype;
	
	}
	
	function getPaymentTypeCharges($value){

		//monthlyfee.type.desc.
		$config    		= Zend_Registry::get('config');
		$monthlyfee 	= $config["monthlyfee"]["type"]["desc"];

		//featureid
		$select = $this->_db->select()
							->from(array('T'=>'M_BILLER'),array('FEATURE_ID','FEATURE_NAME'));
		$featuredata = $this->_db->fetchAll($select);

		//paymenttype: payment.type.code
		$paycode 	= $config["payment"]["type"]["code"];
		$paydesc 	= $config["payment"]["type"]["desc"];
		$payall = array_combine($paycode, $paydesc);
		
		//$arrayall = array_merge($monthlyfee,$featuredata,$payall);
		
		//$arrayall = $monthlyfee;
		$arrayall = $monthlyfee + $featuredata + $payall;
		foreach($arrayall as $key => $val){
			if($key == $value){	$paytype = $val; }
			
		}
		
		//print_r($arrayall);
		return $paytype;

		
	}
}
