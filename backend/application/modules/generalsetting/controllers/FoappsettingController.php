<?php

require_once 'Zend/Controller/Action.php';

class Generalsetting_FoappsettingController extends Application_Main
{

	public function indexAction()
	{

		// if (!$this->view->hasPrivilege("STLS")) {
		// 	return $this->_redirect($this->view->url(["module" => "notification", "controller" => "notauthorized"]));
		// }

		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()
			->from(array('A' => 'M_SETTING'), array('SETTING_ID', 'SETTING_VALUE'))
			->where('MODULE_ID = ?', 'GNS')
			->query()->FetchAll();

		$setting = Application_Helper_Array::listArray($select, 'SETTING_ID', 'SETTING_VALUE');

		$tokenTypeCode = $this->_tokenType['code'];

		$tokenTypeDesc = $this->_tokenType['desc'];

		$systemArr = array(
			1 => 'Dual System',
			2 => 'Core Banking',
			3 => 'Open Banking'
		);
		$this->view->systemarr = $systemArr;

		$arr_isenabled = array(
			0 => 'Tidak Aktif',
			1 => 'Aktif'
			
		);
		$this->view->arr_isenabled = $arr_isenabled;

		unset($tokenTypeCode['notoken']);
		unset($tokenTypeCode['smstoken']);
		unset($tokenTypeCode['mobiletoken']);
		unset($tokenTypeCode['hardtoken']);
		unset($tokenTypeCode['emailotp']);

		$this->view->tokenTypeCode = $tokenTypeCode;
		$this->view->tokenTypeDesc = $tokenTypeDesc;

		foreach ($setting as $key => $value) {
			if (!empty($key))
				$this->view->$key = $value;
		}

		//FILTER
		{
			$filters = array(
				'tokentype' => array('StringTrim', 'StripTags'),
				'isenabled' => array('StringTrim', 'StripTags'),
				'master_bank_email' => array('StringTrim', 'StripTags'),
				'master_bank_telp' => array('StringTrim', 'StripTags'),
				'disable_note' => array('StringTrim', 'StripTags'),
				'note' => array('StringTrim', 'StripTags'),
			);
		}

		//VALIDATE
		{
			$validators = array(
				'tokentype' => array(
					'NotEmpty',
					'messages' => array('Can not be empty')
				),
				'isenabled' => array(
					'NotEmpty',
					'messages' => array('Can not be empty')
				),
				'master_bank_email' => array(
					'NotEmpty',
					'messages' => array('Can not be empty')
				),
				'master_bank_telp' => array(
					'NotEmpty',
					'messages' => array('Can not be empty')
				),
				'disable_note' => array(
					'NotEmpty',
					'messages' => array('Can not be empty')
				),
				'note' => array(
					'NotEmpty',
					'messages' => array('Can not be empty')
				),
			);
		}

		$zf_filter = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

		$arrfck = array('isenabled', 'master_bank_email', 'master_bank_telp', 'tokentype', 'disable_note', 'note');

		$cek = $this->_db->select()
			->FROM(array('B' => 'TEMP_SETTING'), array('MODULE_ID'))
			->WHERE('MODULE_ID = ?', 'GNS')
			->query()->FetchAll();
		if ($cek == null) {
			if ($this->_request->isPost() && $this->view->hasPrivilege('STUD')) {
				//validasi global admin fee

				if ($zf_filter->isValid()) {
					$this->_db->beginTransaction();
					try {

						$info = "GENERAL SETTING";
						$change_id = $this->suggestionWaitingApproval('Front End Global Setting', $info, $this->_changeType['code']['edit'], null, 'M_SETTING', 'TEMP_SETTING', '', '');
						//$change_id = 1;

						foreach ($setting as $key => $value) {
							// print_r($setting);die;
							if (in_array($key, $arrfck)) {
								$valuee = $this->_getParam2($key);
								$data = array(
									'CHANGES_ID' => $change_id,
									'SETTING_ID' => $key,
									'SETTING_VALUE' => htmlspecialchars_decode($valuee),
									'MODULE_ID' => 'GNS',
								);
								$this->_db->insert('TEMP_SETTING', $data);
							}
						}

						Application_Helper_General::writeLog('STUD', 'Update Front End Global Setting');
						$this->_db->commit();
						$this->view->success = true;
						$msg = "Settings Change Request Saved";
						$this->view->report_msg = $msg;
					} catch (Exception $e) {
						$this->_db->rollBack();
					}
				} else {
					$docErr = 'Error in processing form values. Please correct values and re-submit.';
					$this->view->report_msg = $docErr;
					// print_r($zf_filter->getMessages());die;
					foreach ($zf_filter->getMessages() as $key => $err) {
						$xxx = 'x' . $key;
						$this->view->$xxx = $this->displayError($err);
					}
				}
			}
		} else {
			$this->view->error = true;
			//$this->fillParam($zf_filter_input);
			$this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
		}


		if ($this->_request->isPost()) {

			foreach ($setting as $key => $value) {
				if (in_array($key, $arrfck)) {
					$this->view->$key =   $this->_getParam($key);
				} else {
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
				}
			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
			$master_bank_email = $zf_filter->masterEmail;
			$master_bank_telp = $zf_filter->masterTelp;
		} else {
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			$master_bank_email = $setting['master_bank_email'];
			$master_bank_telp = $setting['master_bank_telp'];
			Application_Helper_General::writeLog('STLS', 'View Frontend Global Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 160 - $disable_note_len;
		$note_len 	 = 160 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;
		$this->view->master_bank_email		= $master_bank_email;
		$this->view->master_bank_telp		= $master_bank_telp;
	}
}
