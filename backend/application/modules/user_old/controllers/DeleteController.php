<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class user_DeleteController extends user_Model_User 
{

  public function indexAction() 
  {
    
  	$this->_moduleDB = strtoupper($this->_moduleID['user']);

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));
//    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
    $error_remark = null;
    
    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_STATUS','CUST_ID'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $cust_data = $this->_db->fetchRow($select);
      if(!$cust_data['CUST_ID'])$cust_id = null;
    }
    
    
    if(!$cust_id)
    {
      $error_remark = $this->getErrorRemark('22','Customer ID');
      //insert log
      try 
      {
	    $this->_db->beginTransaction();
	    $this->backendLog(strtoupper($this->_changeType['code']['activate']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
        $this->_db->commit();
	  }
	  catch(Exception $e) 
	  {
	    $this->_db->rollBack();
  	    SGO_Helper_GeneralLog::technicalLog($e);
	  }
	  
	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
    $setting = new Settings();          
                $enc_pass = $setting->getSetting('enc_pass');
                $enc_salt = $setting->getSetting('enc_salt');
                $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
                $pw_hash = md5($enc_salt.$enc_pass);
                $rand = $this->_userIdLogin.date('dHis').$pw_hash;
                $sessionNamespace->token  = $rand;
                // $this->view->token = $sessionNamespace->token;
                
                $AESMYSQL = new Crypt_AESMYSQL();
                $encrypted_custid = $AESMYSQL->encrypt($cust_id, $rand);
                $enccustid = urlencode($encrypted_custid);
                // die;
                $buttonDetail = '/customer/view/index/cust_id/'.$enccustid;
               $this->setbackURL($buttonDetail);
      $this->_redirect($this->_backURL);
    }
    
    
    if($user_id)
    {
       $select = $this->_db->select()
                             ->from('M_USER')
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
                             // REMARK CONDITION BY VENNY - 4 FEB 11 -> BIKIN ERR 22 KLO STATUS USER = S/D ->where('UPPER(USER_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['inactive'])));
                             //->where('UPPER(USER_STATUS)<>'.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
      $user_data = $this->_db->fetchRow($select);
      
      if($user_data['USER_ID'])
      {
      	
      	  $select = $this->_db->select()
                                 ->from('TEMP_USER',array('TEMP_ID'))
                                 ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                                 ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
          $result = $this->_db->fetchOne($select);
          if(!$result)
          {
            $info = 'User ID = '.$user_id.', User Name = '.$user_data['USER_NAME'];
            //$user_data['USER_STATUS'] = strtoupper($this->_masterStatus['code']['active']);
            $user_data['USER_STATUS'] = 3;
            try 
            {
		      $this->_db->beginTransaction();
		  	  $change_id = $this->suggestionWaitingApproval('User Account',$info,strtoupper($this->_changeType['code']['delete']),null,'M_USER','TEMP_USER',$user_id,$user_data['USER_FULLNAME'],$cust_id);
			  $this->insertTempUser($change_id,$user_data);
			  
			  //log CRUD
			  Application_Helper_General::writeLog('CSUD','User has been Updated (delete), User ID : '.$user_id. ' User Name : '.$user_data['USER_FULLNAME'].' Change id : '.$change_id);
			  
			  $this->_db->commit();
			  
			  //$this->setbackURL('/customer/view/index/cust_id/'.$cust_id);
			  $setting = new Settings();          
                $enc_pass = $setting->getSetting('enc_pass');
                $enc_salt = $setting->getSetting('enc_salt');
                $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
                $pw_hash = md5($enc_salt.$enc_pass);
                $rand = $this->_userIdLogin.date('dHis').$pw_hash;
                $sessionNamespace->token  = $rand;
                // $this->view->token = $sessionNamespace->token;
                
                $AESMYSQL = new Crypt_AESMYSQL();
                $encrypted_custid = $AESMYSQL->encrypt($cust_id, $rand);
                $enccustid = urlencode($encrypted_custid);
                // die;
                $buttonDetail = '/customer/view/index/cust_id/'.$enccustid;
               $this->setbackURL($buttonDetail);
			  $this->_redirect('/notification/submited/index');
			  //$this->_redirect('/notification/success/index');
		    }
		    catch(Exception $e) 
		    {
			  $this->_db->rollBack();
			  $error_remark = $this->getErrorRemark('82');
			  SGO_Helper_GeneralLog::technicalLog($e);
		    }
          }
          else
          { 
              $error_remark = $this->getErrorRemark('03','User ID'); 
          }
      	}
      	else
      	{
      	    $error_remark = $this->getErrorRemark('73'); 
      	}
      
    }
    
    
    if(!$user_id)
    {
      $error_remark = $this->getErrorRemark('22','User ID');
      $fulldesc = 'CUST_ID:'.$cust_id;
      $key_value = null;
    }
    else
    {
      $fulldesc = null;
      $key_value = $cust_id.','.$user_id;
    }
    
    
    if($error_remark){ $class = 'F'; $msg = $error_remark; }
    else
    {
      $msg = $this->getErrorRemark('00','User ID',$user_id);
      $class = 'S';
    }
    
    
    //insert log
    try 
    {
	  $this->_db->beginTransaction();
	 
	  Application_Helper_General::writeLog('CSUD','');
	 
      $this->_db->commit();
	}
	catch(Exception $e) 
	{
	  $this->_db->rollBack();
  	  SGO_Helper_GeneralLog::technicalLog($e);
	}
	
	
	$this->_helper->getHelper('FlashMessenger')->addMessage($class);
    $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
    $setting = new Settings();          
                $enc_pass = $setting->getSetting('enc_pass');
                $enc_salt = $setting->getSetting('enc_salt');
                $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
                $pw_hash = md5($enc_salt.$enc_pass);
                $rand = $this->_userIdLogin.date('dHis').$pw_hash;
                $sessionNamespace->token  = $rand;
                // $this->view->token = $sessionNamespace->token;
                
                $AESMYSQL = new Crypt_AESMYSQL();
                $encrypted_custid = $AESMYSQL->encrypt($cust_id, $rand);
                $enccustid = urlencode($encrypted_custid);
                // die;
                $buttonDetail = '/customer/view/index/cust_id/'.$enccustid;
               $this->setbackURL($buttonDetail);
    $this->_redirect($this->_backURL);
  }
}
