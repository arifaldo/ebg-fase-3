<?php

class virtualpayment_ChartController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';
	
	
	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
	}
	
	private function generateRandomString($length = 10) {
		$char = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $char[rand(0, strlen($char) - 1)];
		}
		return date('Ymd').$randomString;
	}
	
	public function indexAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
			'payReff'	=> array(
				'field'	=> 'INVOICE_NO',
				'label'	=> $this->language->_('Payment Ref'),
				'sortable'	=> false
			),
			'institusi'	=> array(
				'field' => 'INSTITUSI',
				'label' => $this->language->_('Institusi'),
				'sortable'	=> true
			),
			'invoice'	=> array(
				'field' => 'INVOICE_NO',
				'label' => $this->language->_('Invoice'),
				'sortable' => true
			),
			'invoiceName'	=> array(
				'field' => 'INVOICE_NAME',
				'label' => $this->language->_('Invoice Name'),
				'sortable' => true
			),
			'amount' => array(
				'field' => 'AMOUNT',
				'label' => $this->language->_('Amount'),
				'sortable' => true
			),
			'trxDate'	=> array(
				'field' => 'TRX_DATE',
				'label' => $this->language->_('Transaction Date'),
				'sortable' => true
			),
			'lastUpdate'	=> array(
				'field' => 'LAST_UPDATE',
				'label' => $this->language->_('Last Update'),
				'sortable' => true
			),
			'sync' 					 => array	(
				'field' => 'SYNC',
				'label' => $this->language->_('Sync.'),
				'sortable' => true
			),
			'paymentType'	=> array	(
				'field' => 'PAYMENT_TYPE',
				'label' => $this->language->_('Payment Type'),
				'sortable' => true
			),
			'paid'	=> array	(
				'field' => 'PAID_STATUS',
				'label' => $this->language->_('Paid Status'),
				'sortable' => true
			),
			'paymentDateStart'	=> array	(
				'field' => 'PAYMENT_DATE_START',
				'label' => $this->language->_('Payment Date Start'),
				'sortable' => true
			),
		);
		
		$params 	= $this->_request->getParams();
		$field 		= Application_Helper_Array::simpleArray($fields, 'field');
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[1]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$optSync = array(
			'' => '-- '.$this->language->_('Please Select').' --',
			'Y' => $this->language->_('Yes'),
			'N' => $this->language->_('No'),
		);
		
		$select = $this->_db->select()
			->from(
				array('X_M_INSTITUSI'),
				array('INSTITUSI','NAMA')
			)
			->group('NAMA')
		;
		
		$opt = $this->_db->fetchAll($select);
		$optPaymentType = array(''=>'-- Please Select --');
		foreach ($opt as $key){
			$optPaymentType[$key['INSTITUSI']] = $key['NAMA'];
		}
		
		$filter = array(
			'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
			'paymentDateStart' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'paymentDateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'lastUpdateStart'	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'lastUpdateEnd' 	=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'paymentStart' 		=> array(new Zend_Validate_Date($this->_dateDBFormat)),
			'institusi'			=> array(),
			'sync' 				=> array(array('InArray', array('haystack' => array_keys($optSync)))),
			'paymentType' 		=> array(array('InArray', array('haystack' => array_keys($optPaymentType)))),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
				
			$select = $this->_db->select()
			->from(
				array('X_PAYMENT_DETAIL'),
				array('PAYREFF' => 'INVOICE_NO','INSTITUSI','INVOICE_NO','INVOICE_NAME','AMOUNT','TRX_DATE','LAST_UPDATE','SYNC','PAYMENT_TYPE','PAID_STATUS')
			)
			->order($sortBy.' '.$sortDir)
			;
				
			if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
			}else{
				if (!empty($paymentDateStart)){
					$select->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
				}
		
				if (!empty($paymentDateEnd)){
					$select->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				}
			}
				
			if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
				$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
			}else{
				if (!empty($lastUpdateStart)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
				}
					
				if (!empty($lastUpdateEnd)){
					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}
			}
				
			if (!empty($institusi)){
				$select->where('INSTITUSI = ?', $institusi);
			}
			if (!empty($sync)){
				$select->where('SYNC = ?', $sync);
			}
			if (!empty($paymentType)){
				$select->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
			}
			
			$datadb = $this->_db->fetchAll($select);
			$data = array();
			
			foreach ($datadb as $row => $arr){
				foreach ($arr as $key => $val){
					if ($key == 'PAYREFF'){
						$data[$row][$key] = $this->generateRandomString(9);
					}else{
						$data[$row][$key] = $val;
					}
				}
			}
		}
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->optSync 		= $optSync;
		$this->view->optPaymentType = $optPaymentType;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
//======================= QUERY GRAPHIC PEMDA Papua ==================================		
//				$select = $this->_db->select()
//					->from(
//						array('XP'=>'X_PAYMENT_DETAIL'),
//						array('XP.INSTITUSI')
//					)
//					->where('XP.PAID_STATUS = "Y"')
//					//->order($sortBy.' '.$sortDir)
//					->joinLeft(array('XM'=>'X_M_INSTITUSI'),'XM.INSTITUSI = XP.INSTITUSI', array('AREA'=>'XM.NAMA','XM.NAMA','AMOUNT'=>'SUM(XP.AMOUNT)'))
//					->group(array('XP.INSTITUSI'));
//					
//				;
				$select = $this->_db->select()
					->from(
						array('XP'=>'T_VP_DETAIL'),
						array('XP.INSTITUTIONS_ID')
					)
					//->order($sortBy.' '.$sortDir)
					->joinLeft(array('XM'=>'M_INSTITUSI'),'XM.INST_ID = XP.INSTITUTIONS_ID', array('AREA'=>'XM.INST_NAME','AMOUNT'=>'SUM(XP.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'XP.VP_NUMBER = MB.VP_NUMBER', array(''))		
					//->where('MB.PAID_STATUS = "Y"')
					->group(array('XP.INSTITUTIONS_ID'));
				;
				
//				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
//				$select->where('TRX_DATE >= "'.$paymentDateStart.'" AND TRX_DATE <= "'.$paymentDateEnd.'"');
//				//die("123");
//				}else{
//					if (!empty($paymentDateStart)){
//						$select->where('TRX_DATE >= "'.$paymentDateStart.'"');
//					}
//			
//					if (!empty($paymentDateEnd)){
//						$select->where('TRX_DATE <= "'.$paymentDateEnd.'"');
//					}
//				}
//					
//				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
//					$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= "'.$lastUpdateStart.'" AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= "'.$lastUpdateEnd.'"');
//				}else{
//					if (!empty($lastUpdateStart)){
//						$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= "'.$lastUpdateStart.'"');
//					}
//						
//					if (!empty($lastUpdateEnd)){
//						$select->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= "'.$lastUpdateEnd.'"');
//					}
//				}
//					
//				if (!empty($institusi)){
//					$select->where('INSTITUSI = ?', $institusi);
//				}
//				if (!empty($sync)){
//					$select->where('SYNC = ?', $sync);
//				}
//				if (!empty($paymentType)){
//					$select->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
//				}

				$data = $this->_db->fetchAll($select);
				$this->view->summary = $data;
				
				//echo "</br>".$select;
				//Zend_Debug::dump($data);
				//die("");
				
//======================= QUERY GRAPHIC RESTRIBUSI PASAR ==================================
//				$pasar = $this->_db->select()
//					->from(array('X' => 'X_PAYMENT_DETAIL'),
//						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
//						   )
//					->where('X.PAYMENT_TYPE = \'Retribusi Pasar\'')
//					->where('X.PAID_STATUS = "Y"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.AREA'));
//					
//				;
				
				$pasar = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array('X.INSTITUTIONS_ID', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array(''))
					->where('MB.PAID_STATUS = "Y"')
					->where('MB.PAYMENT_TYPE = \'Retribusi Pasar\'')
					->group(array('X.AREA'));
				;
				
	
//				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
//				$pasar->where('TRX_DATE >= "'.$paymentDateStart.'" AND TRX_DATE <= "'.$paymentDateEnd.'"');
//				//die("123");
//				}else{
//					if (!empty($paymentDateStart)){
//						$pasar->where('TRX_DATE >= "'.$paymentDateStart.'"');
//					}
//			
//					if (!empty($paymentDateEnd)){
//						$pasar->where('TRX_DATE <= "'.$paymentDateEnd.'"');
//					}
//				}
//					
//				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
//					$pasar->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= "'.$lastUpdateStart.'" AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= "'.$lastUpdateEnd.'"');
//				}else{
//					if (!empty($lastUpdateStart)){
//						$pasar->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= "'.$lastUpdateStart.'"');
//					}
//						
//					if (!empty($lastUpdateEnd)){
//						$pasar->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= "'.$lastUpdateEnd.'"');
//					}
//				}
//					
//				if (!empty($institusi)){
//					$pasar->where('INSTITUSI = ?', $institusi);
//				}
//				if (!empty($sync)){
//					$pasar->where('SYNC = ?', $sync);
//				}
//				if (!empty($paymentType)){
//					$pasar->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
//				}
				
				$data = $this->_db->fetchAll($pasar);
				$this->view->pasar = $data;
				
				//echo "</br>".$pasar;
				//Zend_Debug::dump($data);
				//die("123");
				
//======================= QUERY GRAPHIC RESTRIBUSI RUSUN ==================================
//				$rusun = $this->_db->select()
//					->from(array('X' => 'X_PAYMENT_DETAIL'),
//						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
//						   )
//					->where('X.PAYMENT_TYPE = \'Retribusi Rusun\'')
//					->where('X.PAID_STATUS = "Y"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.AREA'));
//					
//				;
				
				$rusun = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array('X.INSTITUTIONS_ID', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array(''))
					->where('MB.PAID_STATUS = "Y"')
					->where('MB.PAYMENT_TYPE = \'Retribusi Rusun\'')
					->group(array('X.AREA'));
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rusun->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rusun->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rusun->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rusun->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rusun->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rusun->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rusun->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rusun->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rusun->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rusun);
				$this->view->rusun = $data;
				//Zend_Debug::dump($data);
				//die("123");

//======================= QUERY GRAPHIC SAMSAT Papua ==================================				
//				$samsat = $this->_db->select()
//					->from(array('X' => 'X_PAYMENT_DETAIL'),
//						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
//						   )
//					->where('X.PAYMENT_TYPE = \'RETRIBUSI SAMSAT\'')
//					->where('X.PAID_STATUS = "Y"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.AREA'));
//					
//				;

				$samsat = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array('X.INSTITUTIONS_ID', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array(''))
					->where('MB.PAID_STATUS = "Y"')
					->where('MB.PAYMENT_TYPE = \'RETRIBUSI SAMSAT\'')
					->group(array('X.AREA'));
				;
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$samsat->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$samsat->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$samsat->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$samsat->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$samsat->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$samsat->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$samsat->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$samsat->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$samsat->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($samsat);
				$this->view->samsat = $data;
//				Zend_Debug::dump($data);
//				die("123");
				
//======================= QUERY GRAPHIC KIR Papua ==================================				
//				$kir = $this->_db->select()
//					->from(array('X' => 'X_PAYMENT_DETAIL'),
//						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
//						   )
//					->where('X.PAYMENT_TYPE = \'RETRIBUSI KIR\'')
//					->where('X.PAID_STATUS = "Y"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.AREA'));
//					
//				;

				$kir = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array('X.INSTITUTIONS_ID', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array(''))
					->where('MB.PAID_STATUS = "Y"')
					->where('MB.PAYMENT_TYPE = \'RETRIBUSI KIR\'')
					->group(array('X.AREA'));
				;
				
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				//$kir->where('TRX_DATE >= "'.$paymentDateStart.'" AND TRX_DATE <= "'.$paymentDateEnd.'"');
				$kir->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$kir->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$kir->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$kir->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$kir->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$kir->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$kir->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$kir->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$kir->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($kir);
				$this->view->kir = $data;
				//Zend_Debug::dump($data);
				//die("123");				
				
//======================= QUERY GRAPHIC RSUD Papua ==================================				
//				$rsud = $this->_db->select()
//					->from(array('X' => 'X_PAYMENT_DETAIL'),
//						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
//						   )
//					->where('X.PAYMENT_TYPE = \'RSUD\'')
//					->where('X.PAID_STATUS = "Y"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.AREA'));
//					
//				;
				
				$rsud = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array('X.INSTITUTIONS_ID', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array(''))
					->where('MB.PAID_STATUS = "Y"')
					->where('MB.PAYMENT_TYPE = \'RSUD\'')
					->group(array('X.AREA'));
				;
				
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rsud);
				$this->view->rsud = $data;
				//Zend_Debug::dump($data);
				//die("123");				
				
//======================= QUERY GRAPHIC DISHUB ==================================				
//				$rsud = $this->_db->select()
//					->from(array('X' => 'X_PAYMENT_DETAIL'),
//						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
//						   )
//					->where('X.PAYMENT_TYPE = \''.strtoupper('Retribusi Perhubungan').'\'')
//					->where('X.PAID_STATUS = "Y"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.AREA'));
//					
//				;

				$rsud = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array('X.INSTITUTIONS_ID', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array(''))
					->where('MB.PAID_STATUS = "Y"')
					->where('MB.PAYMENT_TYPE = \''.strtoupper('Retribusi Perhubungan').'\'')
					//->where('X.AREA = \'RSUD\'')
					->group(array('X.AREA'));
				;
				
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rsud);
				$this->view->dishub = $data;
				
//======================= QUERY GRAPHIC DKP ==================================				
//				$rsud = $this->_db->select()
//					->from(array('X' => 'X_PAYMENT_DETAIL'),
//						   array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
//						   )
//					->where('X.PAYMENT_TYPE = \''.strtoupper('Retribusi Perikanan').'\'')
//					->where('X.PAID_STATUS = "Y"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.AREA'));
//					
//				;

				
				$rsud = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array('X.INSTITUTIONS_ID', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array(''))
					->where('MB.PAID_STATUS = "Y"')
					->where('MB.PAYMENT_TYPE = \''.strtoupper('Retribusi Perikanan').'\'')
					//->where('X.AREA = \'RSUD\'')
					->group(array('X.AREA'));
				;
				
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart).' AND TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('TRX_DATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('TRX_DATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				//echo $rsud;
				$data = $this->_db->fetchAll($rsud);
				$this->view->kp = $data;
				//Zend_Debug::dump($data);
				//die("123");
				
				//======================= QUERY GRAPHIC DISHUB ==================================
//				$rsud = $this->_db->select()
//				->from(array('X' => 'X_PAYMENT_DETAIL'),
//						array('X.INSTITUSI', 'X.AREA', 'AMOUNT'=>'SUM(X.INVOICE_NAME)')
//				)
//				->where('X.PAYMENT_TYPE = \''.strtoupper('Retribusi Perhubungan').'\'')
//				->where('X.PAID_STATUS = "Y"')
//				//->order($sortBy.' '.$sortDir)
//				//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//				->group(array('X.AREA'));
//					
//				;

				$rsud = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array('X.INSTITUTIONS_ID', 'X.AREA', 'AMOUNT'=>'SUM(X.AMOUNT)')
						   )
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array(''))
					->where('MB.PAID_STATUS = "Y"')
					->where('MB.PAYMENT_TYPE = \''.strtoupper('Retribusi Perhubungan').'\'')
					//->where('X.AREA = \'RSUD\'')
					->group(array('X.AREA'));
				;
					
				
				
				
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
					$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart).' AND LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
					//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart));
					}
						
					if (!empty($paymentDateEnd)){
						$rsud->where('LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
				
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
				
				$data = $this->_db->fetchAll($rsud);
				$this->view->weight = $data;

//======================= QUERY GRAPHIC COA ==================================				
//				$rsud = $this->_db->select()
//					->from(
//						array('X' => 'X_PAYMENT_DETAIL'),
//						array(
//							'X.INSTITUSI', 
//							'AREA'			=>	'X.PAYMENT_TYPE', 
//							'AMOUNT'		=>	'SUM(X.AMOUNT)',
//							'MONTHXYEAR'	=>	'CONCAT(MONTHNAME(X.LAST_UPDATE)," ",YEAR(X.LAST_UPDATE))'
//						)
//						   
//					)
//					//->where('X.PAYMENT_TYPE = \'Rertribusi Perikanan\'')
//					->where('X.PAID_STATUS = "Y"')
//					->where('NOT ISNULL (MONTHNAME(X.LAST_UPDATE))')
//					->where('YEAR(X.LAST_UPDATE) = "2014"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.PAYMENT_TYPE', 'MONTHXYEAR'));
//				;
				
				
				$rsud = $this->_db->select()
					->from(
						array('X' => 'T_VP_DETAIL'),
						array(
							'X.INSTITUTIONS_ID', 
							//'AREA'			=>	'X.PAYMENT_TYPE', 
							'AMOUNT'		=>	'SUM(X.AMOUNT)',
							'MONTHXYEAR'	=>	'CONCAT(MONTHNAME(X.VP_CREATED)," ",YEAR(X.VP_CREATED))'
						)
						   
					)
					//->where('X.PAYMENT_TYPE = \'Rertribusi Perikanan\'')
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array('AREA'=>'MB.PAYMENT_TYPE'))
					->where('MB.PAID_STATUS = "Y"')
					->where('NOT ISNULL (MONTHNAME(X.VP_CREATED))')
					->where('YEAR(X.VP_CREATED) = "2015"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('MB.PAYMENT_TYPE', 'MONTHXYEAR'));
				;
				
				
				
				
				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
				$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart).' AND LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
				//die("123");
				}else{
					if (!empty($paymentDateStart)){
						$rsud->where('LAST_UPDATE >= '.$this->_db->quote($paymentDateStart));
					}
			
					if (!empty($paymentDateEnd)){
						$rsud->where('LAST_UPDATE <= '.$this->_db->quote($paymentDateEnd));
					}
				}
					
				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart).' AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
				}else{
					if (!empty($lastUpdateStart)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= '.$this->_db->quote($lastUpdateStart));
					}
						
					if (!empty($lastUpdateEnd)){
						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= '.$this->_db->quote($lastUpdateEnd));
					}
				}
					
				if (!empty($institusi)){
					$rsud->where('INSTITUSI = ?', $institusi);
				}
				if (!empty($sync)){
					$rsud->where('SYNC = ?', $sync);
				}
				if (!empty($paymentType)){
					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
				}
// 				echo $rsud;
				$data = $this->_db->fetchAll($rsud);
				$this->view->coa = $data;
				//Zend_Debug::dump($data);
				//die("123");	
							
//======================= QUERY GRAPHIC DEVICE ==================================				
//				$rsud = $this->_db->select()
//					->from(array('X' => 'X_PAYMENT_DETAIL'),
//						   array(
//						   	'X.INSTITUSI', 
//						   	'AREA'=>'X.PAID_FROM', 
//						   	'AMOUNT'=>'SUM(X.AMOUNT)',
//						   	'MONTHXYEAR'	=>	'CONCAT(MONTHNAME(X.LAST_UPDATE)," ",YEAR(X.LAST_UPDATE))'
//						   )
//					   )
//					//->where('X.PAYMENT_TYPE = \'Rertribusi Perikanan\'')
//					->where('X.PAID_STATUS = "Y"')
//					->where('NOT ISNULL (MONTHNAME(X.LAST_UPDATE))')
//					->where('YEAR(X.LAST_UPDATE) = "2014"')
//					//->order($sortBy.' '.$sortDir)
//					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
//					->group(array('X.PAID_FROM','MONTHXYEAR'));
//					
//				;

				$rsud = $this->_db->select()
					->from(array('X' => 'T_VP_DETAIL'),
						   array(
						   	'X.INSTITUTIONS_ID', 
						   	//'AREA'=>'X.PAID_FROM', //T_VP_TRANSACTION
						   	'AMOUNT'=>'SUM(X.AMOUNT)',
						   	'MONTHXYEAR'	=>	'CONCAT(MONTHNAME(X.VP_CREATED)," ",YEAR(X.VP_CREATED))'
						   )
					   )
					//->where('X.PAYMENT_TYPE = \'Rertribusi Perikanan\'')
					->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'X.VP_NUMBER = MB.VP_NUMBER', array('AREA'=>'MB.PAID_FROM'))
					->where('MB.PAID_STATUS = "Y"')
					->where('NOT ISNULL (MONTHNAME(X.VP_CREATED))')
					->where('YEAR(X.VP_CREATED) = "2015"')
					//->order($sortBy.' '.$sortDir)
					//->joinLeft(array('XM'=>'X_M_INSTITUSI'), 'XM.INSTITUSI = X.INSTITUSI', array('XM.','AMOUNT'=>'SUM(X.AMOUNT)'))
					->group(array('MB.PAID_FROM','MONTHXYEAR'));
					
				;
				
//				if (!empty($paymentDateStart) && !empty($paymentDateEnd)){
//				$rsud->where('LAST_UPDATE >= "'.$paymentDateStart.'" AND LAST_UPDATE <= "'.$paymentDateEnd.'"');
//				//die("123");
//				}else{
//					if (!empty($paymentDateStart)){
//						$rsud->where('LAST_UPDATE >= "'.$paymentDateStart.'"');
//					}
//			
//					if (!empty($paymentDateEnd)){
//						$rsud->where('LAST_UPDATE <= "'.$paymentDateEnd.'"');
//					}
//				}
//					
//				if (!empty($lastUpdateStart) && !empty($lastUpdateEnd)){
//					$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= "'.$lastUpdateStart.'" AND DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= "'.$lastUpdateEnd.'"');
//				}else{
//					if (!empty($lastUpdateStart)){
//						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") >= "'.$lastUpdateStart.'"');
//					}
//						
//					if (!empty($lastUpdateEnd)){
//						$rsud->where('DATE_FORMAT(LAST_UPDATE,"%Y-%m-%d") <= "'.$lastUpdateEnd.'"');
//					}
//				}
//					
//				if (!empty($institusi)){
//					$rsud->where('INSTITUSI = ?', $institusi);
//				}
//				if (!empty($sync)){
//					$rsud->where('SYNC = ?', $sync);
//				}
//				if (!empty($paymentType)){
//					$rsud->where('PAYMENT_TYPE = ?', $optPaymentType[$paymentType]);
//				}
				
				$data = $this->_db->fetchAll($rsud);
				$this->view->dev = $data;
//				//Zend_Debug::dump($data);
//				//die("123");				
//				$data = $this->_db->query('
//					SELECT
//					XP.INSTITUSI,
//					XM.NAMA,
//					MONTHNAME(XP.TRX_DATE) AS "MONTH",
//					WEEK(XP.TRX_DATE) AS "WEEK",
//					SUM(XP.AMOUNT) AS AMOUNT
//					FROM 
//					X_PAYMENT_DETAIL XP
//					LEFT JOIN X_M_INSTITUSI XM ON XM.INSTITUSI = XP.INSTITUSI
//					GROUP BY 
//					XP.INSTITUSI, 
//					XM.NAMA, 
//					WEEK
//				')->fetchAll();
				$data = $this->_db->query('
					SELECT
					XP.INSTITUTIONS_ID,
					XM.INST_NAME, 
					MONTHNAME(XP.VP_CREATED) AS "MONTH",
					WEEK(XP.VP_CREATED) AS "WEEK",
					SUM(XP.AMOUNT) AS AMOUNT
					FROM 
					T_VP_DETAIL XP
					LEFT JOIN M_INSTITUSI XM ON XM.INST_ID = XP.INSTITUTIONS_ID
					GROUP BY 
					XP.INSTITUTIONS_ID, 
					XM.INST_NAME, 
					WEEK
				')->fetchAll();
				$this->view->group = $data;
			
			Application_Helper_General::writeLog('DTRX','View Transaction Report Chart');
		
	}
	
}
