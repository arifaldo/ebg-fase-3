<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/Payment.php';

class adapter_IndexController extends Application_Main
{
    protected $_moduleDB = 'RTF'; //masih harus diganti

    protected $_destinationUploadDir = '';
    protected $_maxRow = '';

    public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

    public function indexAction()
    {
        $this->setbackURL();
$this->_helper->_layout->setLayout('newlayout');
        $settingObj = new Settings();
        $this->view->THRESHOLD_LLD      = $settingObj->getSetting("threshold_lld"   , 0);

        $this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        $AccArr       = $CustomerUser->getAccounts();
        $this->view->AccArr =  $AccArr;
        $listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');

        if($this->_request->isPost() )
        {
            $this->_request->getParams();
$data=                      $this->_request->getParams();
// print_r($data);die;


            $filter = new Application_Filtering();
            $confirm = false;
            $error_msg[0] = "";

            $PS_SUBJECT     = $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
            // $PS_EFDATE      = $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
            // $ACCTSRC        = $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");

            // if(!$ACCTSRC)
            // {
            //     $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Source Account cannot be left blank.').'';
            //     $this->view->error      = true;
            //     $this->view->report_msg = $this->displayError($error_msg);
            // }
            // else if(!$PS_EFDATE)
            // {
            //     $error_msg[0] = 'Error: Payment Date can not be left blank.';
            //     $this->view->error      = true;
            //     $this->view->report_msg = $this->displayError($error_msg);
            // }
            // else
            // {
                $paramSettingID = array('range_futuredate', 'auto_release_payment');

                $settings = new Application_Settings();
                $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
                $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));
// die;
                if ($adapter->isValid ())
                {
// die;
                    $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                    $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                    $adapter->addFilter ( 'Rename',$newFileName  );

                    if ($adapter->receive ())
                    {
                        //PARSING CSV HERE
                        $csvData = $this->_helper->parser->parseCSV($newFileName);
                        $csvData2 = $this->_helper->parser->parseCSV($newFileName);
                        //after parse delete document temporary
                        // echo "<pre>";
                        // var_dump($csvData);
                        // Zend_Debug::dump($csvData2);die;
                        @unlink($newFileName);
                        //end

                           

                        if(!empty($csvData2))
                        {   
                            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                            $sessionNamespace->content = $csvData2;
                            $this->_redirect('/adapter/index/confirm');
                                    
                        }
                        else //kalo total record = 0
                        {
                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
                            $this->view->error      = true;
                            $this->view->report_msg = $this->displayError($error_msg);
                        }

                }
            }
            else
            {
                $this->view->error = true;
                foreach($adapter->getMessages() as $key=>$val)
                {
                    if($key=='fileUploadErrorNoFile')
                        $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                    else
                        $error_msg[0] = $val;
                    break;
                }
                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;
            }

            if($confirm)
            {
                $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                $sessionNamespace->content = $csvData2;
                $this->_redirect('/adapter/index/confirm');
            }

            $this->view->PSSUBJECT = $PS_SUBJECT;
            $this->view->ACCTSRC = $ACCTSRC;
            $this->view->PSEFDATE = $PS_EFDATE;
        }
        Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
        Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
    }

    public function confirmAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
        $data = $sessionNamespace->content;
        $this->view->refresh = '0';
        $this->view->data = $data;
        $param  =                      $this->_request->getParams();
        // Zend_Debug::dump($param);

        // print_r($data);die();
       
        if($this->_request->isPost()  )
        {
            // print_r($data[0]);die();
            $label = $this->_getParam('label');
            $index = $this->_getParam('index');
            $value = $this->_getParam('value');

            // print_r($data);die();

            // $arrvalue = json_decode($value, 1);

       // Zend_Debug::dump($data);die;     

            $fields = array (   'bene'                   => array    (
                                                                        'field' => 'bene',
                                                                        'label' => $this->language->_('BENEFICIARY ACC'),
                                                                        'sortable' => true
                                                                ),
                            'ccy'                   => array    (
                                                                        'field' => 'ccy',
                                                                        'label' => $this->language->_('CCY'),
                                                                        'sortable' => true
                                                                    ),
                            'amount'                   => array    (
                                                                        'field' => 'amount',
                                                                        'label' => $this->language->_('AMOUNT'),
                                                                        'sortable' => true
                                                                    ),  
                            'message'                    => array    (
                                                                        'field' => 'message',
                                                                        'label' => $this->language->_('MESSAGE'),
                                                                        'sortable' => true
                                                                    ),
                            'additional'               => array    (
                                                                        'field' => 'additional',
                                                                        'label' => $this->language->_('ADDITIONAL MESSAGE'),
                                                                        'sortable' => true
                                                                    )
                        );

            $header  = Application_Helper_Array::simpleArray($fields, "label");
            $field  = Application_Helper_Array::simpleArray($fields, "field");
                // print_r($param['index']);
            $total = count($data[0]);
            $totalcolom = count($data["0"]);

            // print_r($index);die();
            
               // print_r($header);
                // print_r($data["payment"][0]);
                // foreach ($data["payment"][0] as $key => $value) {
                    // if)
            // print_r($data["payment"]);
                for ($i=1; $i < $totalcolom; $i++){
                     foreach ($index as $no => $val) {
                        // print_r($val);print_r($total);die;
                        
                        if($val == $total){
                            $dataexcel[$i-1]['ccy'] = 'IDR';    
                        }elseif($val == $total+1){
                            foreach ($param['wherecol'] as $key => $value) {
                               $dataexcel[$i-1]['message'] .= $data[$value][$val];
                            }
                                
                        }elseif($val == $total+2){
                            // $dataexcel[$i-1]['additional'] = '';    
                            foreach ($param['wherecoladd'] as $key => $value) {
                               $dataexcel[$i-1]['additional'] .= $data[$value][$val];
                            }
                        }else{
                            $tagfield = $field[$no];
                            $dataexcel[$i-1][$tagfield] = $data[$i][$val];
                        }
                    }
               }

               for ($i=1; $i < $totalcolom; $i++){
                    if(!empty($param['wherecol'])){
                        foreach ($param['wherecol'] as $key => $value) {
                            if(empty($param['wherecol'][$key+1])){
                                $dataexcel[$i-1]['message'] .= $data[$i][$value];
                            }else{
                                $dataexcel[$i-1]['message'] .= $data[$i][$value].' - ';
                            }
                           
                        }

                    }
                    if(!empty($param['wherecoladd'])){
                        foreach ($param['wherecoladd'] as $key => $value) {
                            if(empty($param['wherecoladd'][$key+1])){
                           $dataexcel[$i-1]['additional'] .= $data[$i][$value];
                            }else{
                            $dataexcel[$i-1]['additional'] .= $data[$i][$value].' - ';
                            }
                        }
                    }   
               } 
                // }
               // echo "<pre>";
               //  print_r($dataexcel);die;
             

                // foreach ($data["payment"] as $key => $value) {
                    
                // }

                $save = $this->_getParam('finish');

                if($save == 'Save'){

                    foreach ($index as $key => $value) {

                        foreach ($data[0] as $key2 => $value2) {
                            
                            if ($key == $key2) {
                                $row = $key;
                            }
                        }


                        $insArr = array("ADP_CUST" => $this->_custIdLogin,
                                "ADP_USER" => $this->_userIdLogin,
                                "ADP_ROW" =>  $row,
                                "ADP_INDEX" => $value
                        );

                        // print_r($insArr);die();
                        $this->_db->insert('M_ADAPTER',$insArr);

                    }
                    if(!empty($param['wherecoladd'])){
                    foreach ($param['wherecoladd'] as $key => $value) {
                        if(empty($param['wherecoladd'][$key+1])){
                            $colmnadd .= $value;
                        }else{
                            $colmnadd .= $value.',';
                        }
                        
                    }
                          $insmessageadd = array("ADP_CUST" => $this->_custIdLogin,
                                "ADP_USER" => $this->_userIdLogin,
                                "ADP_ROW" => $colmnadd,
                                "ADP_INDEX" => '3'
                        );
                        $this->_db->insert('M_ADAPTER',$insmessageadd);
                    }
                    // print_r($param);die;
                  

                        if(!empty($param['wherecol'])){
                            foreach ($param['wherecol'] as $key => $value) {
                            if(empty($param['wherecol'][$key+1])){
                                $colmn .= $value;
                            }else{
                                $colmn .= $value.',';
                            }
                            
                        }
                        $insmessage = array("ADP_CUST" => $this->_custIdLogin,
                                "ADP_USER" => $this->_userIdLogin,
                                "ADP_ROW" => $colmn,
                                "ADP_INDEX" => '3'
                        );
                        $this->_db->insert('M_ADAPTER',$insmessage);
                        // die;
                    }
                        $this->_redirect('/notification/success');
                }else if($save == 'Download'){

               try{
                // print_r($header);
                // echo "<pre>";
                // print_r($dataexcel);die();
                $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                $content['header'] = $header;
                $content['dataexcel'] = $dataexcel;
                $sessionNamespace->content = $content;
                $this->_redirect('/notification/successdownload/index/csv/CSV');

                // $result = $this->_helper->download->csv($header,$dataexcel,null,'List View Payment');  
            // Application_Helper_General::writeLog('DARC','Export CSV View Payment');    
                // $success = true;
                // $this->view->refresh = '1';
               }catch ( Exception $e ) {
                // print_r($e);die;
            
                }

                }


            // print_r($result);
            // die;
            // if($success)
            // {

            //     unset($_SESSION['confirmBulkCredit']);
            //     $this->_redirect('/notification/success');
            // }
            
        }

    }

    public function wherecolumnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
        $data = $sessionNamespace->content;
        // print_r($data);die('here');
        // $tblName = $this->_getParam('id');
//         $payType    = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
//         foreach($payType as $key => $value){ 
// //          if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);
                
//              $optpaytypeRaw[$key] = $this->language->_($value); 
//         }
        

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($data[0] as $key => $row){
            $optHtml.="<option value='".$key."'>".$row."</option>";
        }

        echo $optHtml;
    }
}
