<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class customer_EditController extends customer_Model_Customer
{

	public function indexAction()
	{
		//pengaturan url untuk button back
		$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL('/' . $this->_request->getModuleName() . '/view/index/cust_id/' . $this->_getParam('cust_id'));

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password));
		$cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;


		$custModelArr = [
			'1' => $this->language->_('Applicant'),
			'2' => $this->language->_('Insurance'),
			'3' => $this->language->_('Special Obligee')
		];

		$this->view->custModelArr = $custModelArr;



		$cust_view = 1;
		$error_remark = null;
		$flag = 0;

		$this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(), 'COUNTRY_CODE', 'COUNTRY_NAME');

		$this->view->companytypeArr = Application_Helper_Array::listArray($this->getCompanyType(), 'COMPANY_TYPE_CODE', 'COMPANY_TYPE_DESC');

		$this->view->citylistArr = Application_Helper_Array::listArray($this->getCity(), 'CITY_CODE', 'CITY_NAME');


		$collectibility_code = $this->getCreditQuality();
		$this->view->collectCodeArr =  $collectibility_code;




		$debitur_code = $this->getDebitur();
		$this->view->debiturcodeArr = $debitur_code;

		$this->view->businesstypeArr = Application_Helper_Array::listArray($this->getBusinessType(), 'BUSINESS_ENTITY_CODE', 'BUSINESS_ENTITY_DESC');


		//data customer
		$resultdata = $this->getCustomer($cust_id);
		//    Zend_Debug::dump($resultdata);
		// print_r($resultdata);die;

		if ($cust_id) {
			$tempCustomerId = $this->getTempCustomerId($cust_id);
			if ($tempCustomerId) $error_remark = 'invalid format';
			else $flag = 1;
		} else {
			$error_remark = 'invalid format';
		}


		$select = $this->_db->select()->distinct()
			->from(array('M_CUSTOMER'), array('CUST_ID'))
			->where("CUST_STATUS != '3'")
			->order('CUST_ID ASC')
			->query()->fetchAll();
		$comp = array();
		foreach ($select as $vl) {
			$comp[] = $vl['CUST_ID'];
		}
		$selectglobal = $this->_db->select()
			->from(array('B' => 'M_CHARGES_WITHIN'), array('*'))
			->joinleft(array('D' => 'M_CHARGES_OTHER'), 'B.CUST_ID = D.CUST_ID', array('*'))
			->where("B.CUST_ID NOT IN (?) ", $comp);


		$arrglobal = $this->_db->fetchAll($selectglobal);
		$chargeArr = array();
		foreach ($arrglobal as $ky => $vl) {
			if ($vl['CHARGES_TYPE'] == '1') {
				//die;
				$chargeArr[$vl['CUST_ID']]['RTGS'] = $vl['CHARGES_AMT'];
			}
			if ($vl['CHARGES_TYPE'] == '2') {
				$chargeArr[$vl['CUST_ID']]['SKN'] = $vl['CHARGES_AMT'];
			}
			if ($vl['CHARGES_TYPE'] == '8') {
				$chargeArr[$vl['CUST_ID']]['ONLINE'] = $vl['CHARGES_AMT'];
			}
			$chargeArr[$vl['CUST_ID']]['INHOUSE'] = $vl['AMOUNT'];

			$chargeArr[$vl['CUST_ID']]['INHOUSE'] = $vl['AMOUNT'];
			$chargeArr[$vl['CUST_ID']]['APPROVED'] =  $vl['CHARGES_APPROVEDBY'] . '(' . $vl['CHARGES_APPROVED'] . ')';
			$chargeArr[$vl['CUST_ID']]['SUGGESTED'] =  $vl['CHARGES_SUGGESTEDBY'] . '(' . $vl['CHARGES_SUGGESTED'] . ')';
			$chargeArr[$vl['CUST_ID']]['PACKAGE_NAME'] =  $vl['PACKAGE_NAME'];
		}

		$selectcomp = $this->_db->select()->distinct()
			->from(array('M_CUSTOMER'), array('CUST_CHARGESID'))
			->where("CUST_STATUS != '3'")
			->where("CUST_ID = ?", $cust_id)
			->order('CUST_ID ASC')
			->query()->fetchAll();

		$this->view->chargelist = $chargeArr;

		if (!empty($selectcomp['0']['CUST_CHARGESID'])) {
			foreach ($arrglobal as $val) {
				if ($val['CHARGES_ID'] == $selectcomp['0']['CUST_CHARGESID']) {
					$chargeid = $val['CUST_ID'];
				}
			}
		}
		//echo '<pre>';
		if (!empty($chargeArr)) {
			foreach ($chargeArr as $ky => $val) {
				if ($chargeid == $ky) {
					$this->view->chargeonline = Application_Helper_General::displayMoney($val['ONLINE']);
					$this->view->chargeinhouse = Application_Helper_General::displayMoney($val['INHOUSE']);
					$this->view->chargertgs = Application_Helper_General::displayMoney($val['RTGS']);
					$this->view->chargeskn = Application_Helper_General::displayMoney($val['SKN']);
				}
			}
		}

		//var_dump($arrglobal);
		//var_dump($chargeArr);die;

		$setting = new Settings();
		$tokenType = $setting->getSetting('tokentype');

		$tokenTypeCode = array_flip($this->_tokenType['code']);
		$tokenTypeDesc = $this->_tokenType['desc'];

		$this->view->tokenTypeText = $tokenTypeDesc[$tokenTypeCode[$tokenType]];

		//convert limit idr & usd agar bisa masuk ke database
		$limitidr = $this->_getParam('cust_limit_idr');
		$limitidr = Application_Helper_General::convertDisplayMoney($limitidr);
		$this->_setParam('cust_limit_idr', $limitidr);

		$limitusd = $this->_getParam('cust_limit_usd');
		$limitusd = Application_Helper_General::convertDisplayMoney($limitusd);
		$this->_setParam('cust_limit_usd', $limitusd);
		//END convert limit idr & usd agar bisa masuk ke database 

		$arrSecure = array(0 => "No", 1 => "Yes");
		$this->view->secure = $arrSecure;


		//get adapter profile list
		$select = $this->_db->select()
			->from(array('H' => 'M_ADAPTER_PROFILE'), array('*'))
			->where('H.STATUS = ?', '1');


		$profileList = $this->_db->fetchAll($select);

		//get adapter profile data
		$select = $this->_db->select()
			->from(array('H' => 'M_CUST_ADAPTER_PROFILE'), array('*'))
			->where('H.CUST_ID = ' . $this->_db->quote($cust_id));

		$profileData = $this->_db->fetchAll($select);

		$this->view->profileList = $profileList;
		$this->view->profileData = $profileData;

		if ($this->_request->isPost()) {

			if ($this->_getParam('pdf')) {

				foreach ($this->_request->getParams() as $key => $value) {
					$this->view->$key = $value;
				}

				$setting = new Settings();
				$selectAggrement = $this->_db->select()
					->from('M_SETTING')
					->where('SETTING_ID = ?', 'ftemplate_agreement');
				$terms = $this->_db->fetchRow($selectAggrement);

				$templateEmailMasterBankName = $setting->getSetting('master_bank_email');
				$terms['SETTING_VALUE'] = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankName, $terms['SETTING_VALUE']);

				$this->view->terms = $terms['SETTING_VALUE'];

				$outputHTML = "<tr><td>" . $this->view->render('/edit/indexpdf.phtml') . "</td></tr>";

				// echo $outputHTML;die();

				$this->_helper->download->newPdf(null, null, null, $this->language->_('Edit_Customer_') . $this->view->cust_name, $outputHTML);
			} else {

				// echo"cek";
				// Zend_Debug::dump($this->_request->getParams());

				$haystack_cust_type = array($this->_custType['code']['individual'], $this->_custType['code']['company']);
				$customer_filter = array();
				$filters = array(
					'cust_name'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_type'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_model'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_npwp'      => array('StripTags', 'StringTrim', 'HtmlEntities'),

					'cust_workfield'    => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'business_type'    => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'go_public'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'grup_bumn'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'debitur_code'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_address'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_village'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_district'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_city'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_zip'          => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'country_code'      => array('StripTags', 'StringTrim', 'HtmlEntities'),

					'cust_contact'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_contact_phone'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_phone'        => array('StripTags', 'StringTrim', 'HtmlEntities'),

					'cust_email'        => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_website'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'cust_approver'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'same_release'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
					'collectibility_code'      => array('StripTags', 'StringTrim', 'HtmlEntities'),

				);



				$validators = array(
					'cust_name'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Company Name cannot be empty'))
					),
					'cust_npwp'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Company NPWP cannot be empty'))
					),
					'cust_type'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Company Type cannot be empty'))
					),
					'cust_model'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Company Type cannot be empty'))
					),
					'cust_type'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Company Type cannot be empty'))
					),
					'cust_workfield'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Business Type cannot be empty'))
					),

					'business_type'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Business Type cannot be empty'))
					),

					'debitur_code'  => array(
						'allowEmpty' => true
					),
					'go_public'   => array(
						'NotEmpty',
						'messages' => array($this->language->_('Go Public cannot be empty'))
					),
					'grup_bumn'   => array(
						'NotEmpty',
						'messages' => array($this->language->_('Grup BUMN cannot be empty'))
					),
					'cust_address'   => array(
						'NotEmpty',
						'messages' => array($this->language->_('Address cannot be empty'))
					),
					'cust_village'   => array(
						'NotEmpty',
						'messages' => array($this->language->_('Kelurahan cannot be empty'))
					),
					'cust_district'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Kecamatan cannot be empty'))
					),
					'cust_city'  => array(
						'NotEmpty',
						'messages' => array($this->language->_('Kabupaten/City cannot be empty'))
					),
					'cust_zip'   => array(
						'NotEmpty',
						'messages' => array($this->language->_('Zip cannot be empty'))
					),
					'country_code'    => array(
						'NotEmpty',
						'messages' => array($this->language->_('Country cannot be empty'))
					),
					'cust_contact'	 => array('allowEmpty' => true),
					'cust_contact_phone'     => array(
						'allowEmpty' => true,
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),
					'cust_phone'       => array(
						'allowEmpty' => true,
						array('StringLength', array('min' => 1, 'max' => 128)),
						'messages' => array(
							$this->language->_('Contact Number maximum characters allowed is') . ' 128'
						)
					),

					'cust_email' => array(
						'NotEmpty',
						'messages' => array($this->language->_('Contact Email cannot be empty'))
					),
					'collectibility_code' => array(
						'NotEmpty',
						'messages' => array($this->language->_('Contact Email cannot be empty'))
					),
					'cust_website'  => array(
						'allowEmpty' => true
					),

					'same_release'  => array(
						'allowEmpty' => true
					),
					'cust_approver'  => array(
						'allowEmpty' => true
					),
				);

				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

				// echo "<pre>";
				// print_r($zf_filter_input);
				// print_r($zf_filter_input->isValid());
				// echo "tes";

				//validasi multiple email
				if ($this->_getParam('cust_email')) {
					$validate = new validate;
					$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('cust_email'));
				} else {
					$cek_multiple_email = true;
				}

				//validasi emobile, cust phone dan cust fax = harus dimulai dengan '62'

				//Zend_Debug::dump($cek_multiple_email);

				$flag_cust_phone = 'T';
				/*if($zf_filter_input->cust_phone)
       		{
          		$cust_phone_62   = substr($zf_filter_input->cust_phone,0,2);
          
          		if($cust_phone_62 != '62') $flag_cust_phone = 'F';
          		else $flag_cust_phone = 'T';
       		}
       		else $flag_cust_phone = 'T';*/

				$flag_cust_fax = 'T';
				/*if($zf_filter_input->cust_fax)
       		{
          		$cust_fax_62     = substr($zf_filter_input->cust_fax,0,2);
     
          		if($cust_fax_62 != '62') $flag_cust_fax = 'F';
          		else $flag_cust_fax = 'T';
       		}
       		else $flag_cust_fax = 'T';*/
				//END validasi emobile = harus dimulai dengan '62'




				if ($zf_filter_input->isValid() && $flag_cust_phone == 'T' && $flag_cust_fax == 'T' && $cek_multiple_email == true) {
					// echo "masuk";
					// Zend_Debug::dump($this->_request->getParams());

					$info = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $zf_filter_input->cust_name;
					$cust_data = $this->_custData;
					
					foreach ($validators as $key => $value) {
						if ($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;
					}

					$cust_data['CUST_ID'] = $cust_id;

					try {
						$this->_db->beginTransaction();


						if (is_null($cust_data['CUST_CHARGES_STATUS']) || $cust_data['CUST_CHARGES_STATUS'] == '') $cust_data['CUST_CHARGES_STATUS'] = 0;
						if (is_null($cust_data['CUST_MONTHLYFEE_STATUS']) || $cust_data['CUST_MONTHLYFEE_STATUS'] == '')   $cust_data['CUST_MONTHLYFEE_STATUS'] = 0;
						if (is_null($cust_data['CUST_TOKEN_AUTH']) || $cust_data['CUST_TOKEN_AUTH'] == '')   $cust_data['CUST_TOKEN_AUTH'] = 'N';
						if (is_null($cust_data['CUST_SPECIAL']) || $cust_data['CUST_SPECIAL'] == '')     $cust_data['CUST_SPECIAL'] = 'N';
						if (is_null($cust_data['CUST_LIMIT_IDR']) || $cust_data['CUST_LIMIT_IDR'] == '')     $cust_data['CUST_LIMIT_IDR'] = 1;
						if (is_null($cust_data['CUST_LIMIT_USD']) || $cust_data['CUST_LIMIT_USD'] == '')     $cust_data['CUST_LIMIT_USD'] = 1;
						// print_r($resultdata);die;
						$cust_data['CUST_CIF'] = $resultdata['CUST_CIF'];
						$cust_data['CUST_STATUS']      = $resultdata['CUST_STATUS'];
						$cust_data['CUST_CREATED']     = $resultdata['CUST_CREATED'];
						$cust_data['CUST_CREATEDBY']   = $resultdata['CUST_CREATEDBY'];
						$cust_data['CUST_UPDATED']     = $resultdata['CUST_UPDATED'];
						$cust_data['CUST_UPDATEDBY']   = $resultdata['CUST_UPDATEDBY'];
						$cust_data['CUST_SUGGESTED']    = new Zend_Db_Expr('now()');
						$cust_data['CUST_SUGGESTEDBY']  = $this->_userIdLogin;

						if ($cust_data['CUST_APP_TOKEN'] == 'on') {
							$cust_data['CUST_APP_TOKEN'] = '1';
						}

						if ($cust_data['GRUP_BUMN'] == "Y") {
							$cust_data['GRUP_BUMN'] = 1;
						} else {
							$cust_data['GRUP_BUMN'] = 0;
						}


						if ($cust_data['CUST_FINANCE'] == '1') {
							$cust_data['CUST_RLS_TOKEN'] = '1';
						}
						if ($zf_filter_input->cust_role_adm == 'on') {
							$cust_data['CUST_ADM_ROLE'] = '1';
						}

						if (!empty($this->_getParam('same_release'))) {
							$cust_data['CUST_SAME_USER'] = $this->_getParam('same_release');
						}
						// echo '<pre>';
						// print_r($this->_request->getParams());
						// print_r($cust_data);die;



						// insert ke T_GLOBAL_CHANGES
						$change_id = $this->suggestionWaitingApproval('Customer', $info, $this->_changeType['code']['edit'], null, 'M_CUSTOMER', 'TEMP_CUSTOMER', $cust_id, $zf_filter_input->cust_name, $cust_id);

						if (!empty($cust_data['COUNTRY_CODE'])) {
							$selectcountry = $this->_db->select()
								->from('M_COUNTRY', array('*'))
								// ->where('CUST_NAME = ? ',$tblName)
								->where('COUNTRY_NAME = ? ', $cust_data['COUNTRY_CODE']);

							$country_data = $this->_db->fetchRow($selectcountry);
							if (!empty($country_data)) {
								$cust_data['COUNTRY_CODE'] = $country_data['COUNTRY_CODE'];
							}
						}

						if ($cust_data['CUST_FINANCE'] == '3') {
							$cust_data['CUST_LIMIT_IDR'] = 1;
							$cust_data['CUST_LIMIT_USD'] = 1;
						}

						if ($this->_getParam('charges') == 'global') {
							$cust_data['CUST_SPECIAL']   = 'N';
						}
						if ($this->_getParam('charges') == 'special') {
							$cust_data['CUST_SPECIAL']   = 'Y';
							$cust_charge = $this->_getParam('chargesspecial');
							$selectcharge = $this->_db->select()
								->from('M_CHARGES_WITHIN', array('*'))
								->where('CUST_ID = ? ', $cust_charge);

							$charge_data = $this->_db->fetchRow($selectcharge);
							if (!empty($charge_data)) {
								$cust_data['CUST_CHARGESID'] = $charge_data['CHARGES_ID'];
							}
						}

						// //  insert temp_cust
						//  $datatempcust = [
						// 	'CHANGES_ID'               => $change_id,
						// 	'CUST_ID'              => $cust_id,
						// ];

						if ($this->_getParam('charges') == 'custom') {
							$inhousecharge = $this->_getParam('chargeinhouse');
							if (empty($this->_getParam('chargeinhouse'))) {
								$inhousecharge = 0;
							}
							$data = array(
								'CHANGES_ID' 		=> $change_id,
								'PACKAGE_NAME'		=> NULL,
								'CUST_ID' 			=> $cust_id,
								'ACCT_NO' 			=> '-',
								'AMOUNT' 			=> Application_Helper_General::convertDisplayMoney($inhousecharge),
								'BUSINESS_TYPE' 	=> '1',
								'CHARGES_ACCT_NO'	=> NULL,
								'CCY'				=> 'IDR',
								'CHARGES_SUGGESTEDBY' => $this->_userIdLogin,
								'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
							);
							// print_r($data);die;
							$this->_db->insert('TEMP_CHARGES_WITHIN', $data);
						}

						$rtgscharge = $this->_getParam('chargertgs');
						if (empty($this->_getParam('chargertgs'))) {
							$rtgscharge = 0;
						}

						$data1 = array(
							'CHANGES_ID'	=> $change_id,
							'CUST_ID' 		=> $cust_id,
							'CHARGES_NO'	=> NULL,
							'CHARGES_TYPE'	=> '1',
							'CHARGES_CCY' 	=> 'IDR',
							'CHARGES_AMT' 	=> Application_Helper_General::convertDisplayMoney($rtgscharge),
							'CHARGES_SUGGESTEDBY' => $this->_userIdLogin,
							'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
						);

						$this->_db->insert('TEMP_CHARGES_OTHER', $data1);

						$skncharge = $this->_getParam('chargeskn');
						if (empty($this->_getParam('chargeskn'))) {
							$skncharge = 0;
						}

						$data2 = array(
							'CHANGES_ID'	=> $change_id,
							'CUST_ID' 		=> $cust_id,
							'CHARGES_NO'	=> NULL,
							'CHARGES_TYPE'	=> '2',
							'CHARGES_CCY' 	=> 'IDR',
							'CHARGES_AMT' 	=> Application_Helper_General::convertDisplayMoney($skncharge),
							'CHARGES_SUGGESTEDBY' => $this->_userIdLogin,
							'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
						);

						$this->_db->insert('TEMP_CHARGES_OTHER', $data2);

						$onlinecharge = $this->_getParam('chargeonline');
						if (empty($this->_getParam('chargeonline'))) {
							$onlinecharge = 0;
						}

						$data8 = array(
							'CHANGES_ID'	=> $change_id,
							'CUST_ID' 		=> $cust_id,
							'CHARGES_NO'	=> NULL,
							'CHARGES_TYPE'	=> '8',
							'CHARGES_CCY' 	=> 'IDR',
							'CHARGES_AMT' 	=> Application_Helper_General::convertDisplayMoney($onlinecharge),
							'CHARGES_SUGGESTEDBY' => $this->_userIdLogin,
							'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
						);

						$this->_db->insert('TEMP_CHARGES_OTHER', $data8);

						// Zend_Debug::dump($cust_data);die("SUKSES");
						$this->insertTempCustomer($change_id, $cust_data);

						$comp_nameOriSub = preg_replace("/[^0-9a-zA-Z]/", "", $cust_data['CUST_NAME']);
						$generateuserid = strtoupper(substr($comp_nameOriSub, 0, 4)) . '_';

						// echo $generateuserid;die;

						if (!empty($resultdata['CUST_ADM_ID'])) {
							$admin = $this->getUserCustEdit($resultdata['CUST_ADM_ID']);
							$cust_data['ADMIN1']  	  = $admin['USER_FULLNAME'];
							$cust_data['EMAILADMIN1']  = $admin['USER_EMAIL'];
							$cust_data['PHONEADMIN1']  = $admin['USER_PHONE'];
						}

						if (!empty($resultdata['CUST_ADM1_ID'])) {
							$admin2 = $this->getUserCustEdit($resultdata['CUST_ADM1_ID']);
							$cust_data['ADMIN2']  	  = $admin2['USER_FULLNAME'];
							$cust_data['EMAILADMIN2']  = $admin2['USER_EMAIL'];
							$cust_data['PHONEADMIN2']  = $admin2['USER_PHONE'];
						}

						try {
							$count = 0;
							if (!empty($cust_data['ADMIN1'])) {
								$count += 1;
								$userfullnameGet = $cust_data['ADMIN1'];
								$userfullnameGetSub = preg_replace("/[^0-9a-zA-Z]/", "", $userfullnameGet);
								$userfullname = strtoupper(substr($userfullnameGetSub, 0, 9));

								$generate_userid = $generateuserid;
								$squenceNumberUser = '0' . $count;
								$hasil_userid = $generate_userid . 'SYSADM01';
								$selectcomp = $this->_db->select()
									->from('M_USER', array('*'))
									// ->where('CUST_NAME = ? ',$tblName)
									->where('USER_ID = ? ', $hasil_userid);

								$c_data = $this->_db->fetchRow($selectcomp);

								$c_data['USER_FULLNAME']  = $cust_data['ADMIN1'];
								$c_data['USER_EMAIL']     = $cust_data['EMAILADMIN1'];
								$c_data['USER_PHONE']     = $cust_data['PHONEADMIN1'];

								$this->insertTempUsersysadm($change_id, $c_data);

								// 			$c_data = array('CUST_ID'        => $cust_data['CUST_ID'],
								// //  	                       'USER_ID'        => null,
								// 		                   'USER_FULLNAME'  => $cust_data['ADMIN1'],
								//         				   'USER_PASSWORD'  => null,
								//                            'USER_EMAIL'     => $cust_data['EMAILADMIN1'],
								//                            'USER_PHONE'     => $cust_data['PHONEADMIN1'],
								//                            'USER_STATUS'    => 2,
								//                            'USER_EXT'       => null,
								//                            'TOKEN_ID'       => null,
								//                            'USER_ISWEBSERVICES' => null,
								//                            'USER_ISEMAIL'       => 1,
								//   	                       //'FGROUP_ID'      => null,
								//   	                       //'USER_HASTOKEN'  => null
								//   	                       'USER_SUGGESTED'   => null,
								//        					   'USER_SUGGESTEDBY' => null,
								//                             );
								// 			$c_data['USER_ID'] = $hasil_userid;
								// 			$c_data['USER_CREATED']     = null;
								// 			$c_data['USER_CREATEDBY']   = null;
								// 			$c_data['USER_UPDATED']     = null;
								// 			$c_data['USER_UPDATEDBY']   = null;
								// 			$c_data['FGROUP_ID']			= 'A02';
								// 		  	$c_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
								// 			$c_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;

								// 			$selectcomp = $this->_db->select()
								//                           ->from('M_FPRIVILEGE_TEMPLATE', array('FPRIVI_ID'))
								//                           // ->where('CUST_NAME = ? ',$tblName)
								//                           ->where('FTEMPLATE_ID = ? ','A02');

								//         	$tempColumn = $this->_db->fetchAll($selectcomp);
								//         // print_r($cust_data);die;

								//      	 	$this->insertTempPrivilege($change_id,$tempColumn,$c_data);
								//      	 	// print_r($cust_data);die;
								//      	 	$this->updateTempadm($change_id,$hasil_userid);
								//   			$this->insertTempUseradmin1($change_id,$c_data);


							}
							if (!empty($cust_data['ADMIN2'])) {
								$count += 1;
								$userfullnameGet = $cust_data['ADMIN2'];
								$userfullnameGetSub = preg_replace("/[^0-9a-zA-Z]/", "", $userfullnameGet);
								$userfullname = strtoupper(substr($userfullnameGetSub, 0, 9));

								$generate_userid = $generateuserid;
								$squenceNumberUser = '0' . $count;
								$hasil_userid = $generate_userid . 'SYSADM02';

								$selectcomp = $this->_db->select()
									->from('M_USER', array('*'))
									// ->where('CUST_NAME = ? ',$tblName)
									->where('USER_ID = ? ', $hasil_userid);

								$c_data = $this->_db->fetchRow($selectcomp);

								$c_data['USER_FULLNAME']  = $cust_data['ADMIN2'];
								$c_data['USER_EMAIL']     = $cust_data['EMAILADMIN2'];
								$c_data['USER_PHONE']     = $cust_data['PHONEADMIN2'];
								// );

								// $where = array('USER_ID' => $hasil_userid;
								// )
								// $c_data['USER_CREATED']     = null;
								// $c_data['USER_CREATEDBY']   = null;
								// $c_data['USER_UPDATED']     = null;
								// $c_data['USER_UPDATEDBY']   = null;
								// $c_data['FGROUP_ID']			= 'A02';
								//  	$c_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
								// $c_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;

								// $selectcomp = $this->_db->select()
								//                        ->from('M_FPRIVILEGE_TEMPLATE', array('FPRIVI_ID'))
								//                        // ->where('CUST_NAME = ? ',$tblName)
								//                        ->where('FTEMPLATE_ID = ? ','A02');

								//      	$tempColumn = $this->_db->fetchAll($selectcomp);
								// print_r($cust_data);die;

								// $this->insertTempPrivilege($change_id,$tempColumn,$c_data);
								$this->insertTempUsersysadm($change_id, $c_data);
								// $this->insertTempUseradmin2($change_id,$c_data);
							}
							// 		if(!empty($cust_data['ADMIN3'])){
							// 			$count += 1;

							// 			$userfullnameGet = $cust_data['ADMIN3'];
							// 			$userfullnameGetSub = preg_replace("/[^0-9a-zA-Z]/", "", $userfullnameGet);
							// 			$userfullname = strtoupper(substr($userfullnameGetSub, 0,9));

							// 			$generate_userid = $generateuserid;
							// 			$squenceNumberUser = '0'.$count;
							// 			$hasil_userid = $generate_userid.$userfullname.$squenceNumberUser;

							// 			$c_data = array('CUST_ID'        => $cust_data['CUST_ID'],
							// //  	                       'USER_ID'        => null,
							// 		                   'USER_FULLNAME'  => $cust_data['ADMIN3'],
							//         				   'USER_PASSWORD'  => null,
							//                            'USER_EMAIL'     => $cust_data['EMAILADMIN3'],
							//                            'USER_PHONE'     => $cust_data['PHONEADMIN3'],
							//                            'USER_STATUS'    => 2,
							//                            'USER_EXT'       => null,
							//                            'TOKEN_ID'       => null,
							//                            'USER_ISWEBSERVICES' => null,
							//                            'USER_ISEMAIL'       => 1,
							//   	                       //'FGROUP_ID'      => null,
							//   	                       //'USER_HASTOKEN'  => null
							//   	                       'USER_SUGGESTED'   => null,
							//        					   'USER_SUGGESTEDBY' => null,
							//                             );
							// 			$c_data['USER_ID'] = $hasil_userid;
							// 			$c_data['USER_CREATED']     = null;
							// 			$c_data['USER_CREATEDBY']   = null;
							// 			$c_data['USER_UPDATED']     = null;
							// 			$c_data['USER_UPDATEDBY']   = null;
							// 			$c_data['FGROUP_ID']			= 'A02';
							// 		  	$c_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
							// 			$c_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;

							// 			$selectcomp = $this->_db->select()
							//                           ->from('M_FPRIVILEGE_TEMPLATE', array('FPRIVI_ID'))
							//                           // ->where('CUST_NAME = ? ',$tblName)
							//                           ->where('FTEMPLATE_ID = ? ','A02');

							//         	$tempColumn = $this->_db->fetchAll($selectcomp);
							//         // print_r($cust_data);die;

							//      	 	$this->insertTempPrivilege($change_id,$tempColumn,$c_data);
							//      	 	$this->updateTempsys($change_id,$hasil_userid);
							//   			$this->insertTempUseradmin3($change_id,$c_data);
							// 		}

							$adapterData = array(
								'payroll' => $cust_data['SELECTADAPTERPAYROLL'],
								'manytomany' => $cust_data['SELECTADAPTERMANYTOMANY'],
								'emoney' => $cust_data['SELECTADAPTEREMONEY'],
								'onetomany' => $cust_data['SELECTADAPTERONETOMANY'],
								'manytoone' => $cust_data['SELECTADAPTERMANYTOONE']
							);

							//tambahan utk adapter
							//$this->insertTempAdapterProfile($change_id, $cust_data['CUST_ID'], $adapterData);

						} catch (Exception $e) {
							print_r($e);
							die;
						}

						//log CRUD
						Application_Helper_General::writeLog('CCUD', 'Customer has been updated (edit), Cust ID : ' . $cust_id . ' Cust Name : ' . $zf_filter_input->cust_name . ' Change id : ' . $change_id);

						$this->_db->commit();

						$this->_redirect('/notification/submited/index');
					} catch (Exception $e) {
						echo $e->getMessage();
						die("haha");
						//print_r($e);die;
						$this->_db->rollBack();
						$error_remark = $this->language->_('Database Error');
					}

					if (isset($error_remark)) {
						$msg = $error_remark;
						$class = 'F';
					} else {
						$msg = 'Success';
						$class = 'S';
					}

					$this->_helper->getHelper('FlashMessenger')->addMessage($class);
					$this->_helper->getHelper('FlashMessenger')->addMessage($msg);
					// $this->_redirect($this->_backURL);

				} //END IF is VALID
				else {

					die("gagal");
					$this->view->cust_id   = $cust_id;
					$this->view->cust_name = ($zf_filter_input->isValid('cust_name')) ? $zf_filter_input->cust_name : $this->_getParam('cust_name');
					$this->view->cust_type = ($zf_filter_input->isValid('cust_type')) ? $zf_filter_input->cust_type : $this->_getParam('cust_type');
					$this->view->cust_securities = ($zf_filter_input->isValid('cust_securities')) ? $zf_filter_input->cust_securities : $this->_getParam('cust_securities');
					$this->view->cust_code = ($zf_filter_input->isValid('cust_code')) ? $zf_filter_input->cust_code : $this->_getParam('cust_code');
					$this->view->cust_cif = ($zf_filter_input->isValid('cust_cif')) ? $zf_filter_input->cust_cif : $this->_getParam('cust_cif');
					$this->view->CUST_SPECIAL = ($zf_filter_input->isValid('cust_special')) ? $zf_filter_input->cust_special : $this->_getParam('cust_special');

					$this->view->cust_address = ($zf_filter_input->isValid('cust_address')) ? $zf_filter_input->cust_address : $this->_getParam('cust_address');
					$this->view->cust_city = ($zf_filter_input->isValid('cust_city')) ? $zf_filter_input->cust_city : $this->_getParam('cust_city');
					$this->view->cust_zip  = ($zf_filter_input->isValid('cust_zip')) ? $zf_filter_input->cust_zip : $this->_getParam('cust_zip');
					$this->view->cust_province = ($zf_filter_input->isValid('cust_province')) ? $zf_filter_input->cust_province : $this->_getParam('cust_province');
					$this->view->cust_workfield = ($zf_filter_input->isValid('cust_workfield')) ? $zf_filter_input->cust_workfield : $this->_getParam('cust_workfield');
					$this->view->business_type = ($zf_filter_input->isValid('business_type')) ? $zf_filter_input->business_type : $this->_getParam('business_type');
					$this->view->cust_contact  = ($zf_filter_input->isValid('cust_contact')) ? $zf_filter_input->cust_contact : $this->_getParam('cust_contact');
					$this->view->cust_phone    = ($zf_filter_input->isValid('cust_phone')) ? $zf_filter_input->cust_phone : $this->_getParam('cust_phone');
					$this->view->cust_ext      = ($zf_filter_input->isValid('cust_ext')) ? $zf_filter_input->cust_ext : $this->_getParam('cust_ext');
					$this->view->cust_fax      = ($zf_filter_input->isValid('cust_fax')) ? $zf_filter_input->cust_fax : $this->_getParam('cust_fax');
					$this->view->cust_email    = ($zf_filter_input->isValid('cust_email')) ? $zf_filter_input->cust_email : $this->_getParam('cust_email');
					$this->view->cust_website  = ($zf_filter_input->isValid('cust_website')) ? $zf_filter_input->cust_website : $this->_getParam('cust_website');
					$this->view->country_code  = ($zf_filter_input->isValid('country_code')) ? $zf_filter_input->country_code : $this->_getParam('country_code');
					$this->view->cust_limit_idr  = ($zf_filter_input->isValid('cust_limit_idr')) ? $zf_filter_input->cust_limit_idr : $this->_getParam('cust_limit_idr');
					$this->view->cust_limit_usd  = ($zf_filter_input->isValid('cust_limit_usd')) ? $zf_filter_input->cust_limit_usd : $this->_getParam('cust_limit_usd');

					$this->view->same_release  = $this->_getParam('same_release');

					$this->view->cust_review  = ($zf_filter_input->isValid('cust_review')) ? $zf_filter_input->cust_review : $this->_getParam('cust_review');
					$this->view->cust_approver  = ($zf_filter_input->isValid('cust_approver')) ? $zf_filter_input->cust_approver : $this->_getParam('cust_approver');
					$this->view->cust_app_token  = ($zf_filter_input->isValid('cust_app_token')) ? $zf_filter_input->cust_app_token : $this->_getParam('cust_app_token');
					$this->view->cust_rls_token  = ($zf_filter_input->isValid('cust_rls_token')) ? $zf_filter_input->cust_rls_token : $this->_getParam('cust_rls_token');
					$this->view->cust_finance  = ($zf_filter_input->isValid('cust_finance')) ? $zf_filter_input->cust_finance : $this->_getParam('cust_finance');
					$this->view->admin1  = ($zf_filter_input->isValid('admin1')) ? $zf_filter_input->admin1 : $this->_getParam('admin1');
					$this->view->emailadmin1  = ($zf_filter_input->isValid('emailadmin1')) ? $zf_filter_input->emailadmin1 : $this->_getParam('emailadmin1');
					$this->view->phoneadmin1  = ($zf_filter_input->isValid('phoneadmin1')) ? $zf_filter_input->phoneadmin1 : $this->_getParam('phoneadmin1');
					$this->view->tokenadmin1  = ($zf_filter_input->isValid('tokenadmin1')) ? $zf_filter_input->tokenadmin1 : $this->_getParam('tokenadmin1');
					$this->view->admin2  = ($zf_filter_input->isValid('admin2')) ? $zf_filter_input->admin2 : $this->_getParam('admin2');
					$this->view->emailadmin2  = ($zf_filter_input->isValid('emailadmin2')) ? $zf_filter_input->emailadmin2 : $this->_getParam('emailadmin2');
					$this->view->phoneadmin2  = ($zf_filter_input->isValid('phoneadmin2')) ? $zf_filter_input->phoneadmin2 : $this->_getParam('phoneadmin2');
					$this->view->tokenadmin2  = ($zf_filter_input->isValid('tokenadmin2')) ? $zf_filter_input->tokenadmin2 : $this->_getParam('tokenadmin2');

					// $this->view->admin3  = ($zf_filter_input->isValid('admin3'))? $zf_filter_input->admin3 : $this->_getParam('admin3');
					// $this->view->emailadmin3  = ($zf_filter_input->isValid('emailadmin3'))? $zf_filter_input->emailadmin3 : $this->_getParam('emailadmin3');
					// $this->view->phoneadmin3  = ($zf_filter_input->isValid('phoneadmin3'))? $zf_filter_input->phoneadmin3 : $this->_getParam('phoneadmin3');
					// $this->view->tokenadmin3  = ($zf_filter_input->isValid('tokenadmin3'))? $zf_filter_input->tokenadmin3 : $this->_getParam('tokenadmin3');

					$this->view->cust_rdn_key  = ($zf_filter_input->isValid('cust_rdn_key')) ? $zf_filter_input->cust_rdn_key : $this->_getParam('cust_rdn_key');

					$this->view->cust_role_adm = ($zf_filter_input->isValid('cust_role_adm')) ? $zf_filter_input->cust_role_adm : $this->_getParam('cust_role_adm');

					$this->view->collectibility_code    = ($zf_filter_input->isValid('collectibility_code')) ? $zf_filter_input->collectibility_code : $this->_getParam('collectibility_code');


					//$this->view->cust_charges_status  = ($zf_filter_input->isValid('cust_charges_status'))? $zf_filter_input->cust_charges_status : $this->_getParam('cust_charges_status');
					//$this->view->cust_monthlyfee_status   = ($zf_filter_input->isValid('cust_monthlyfee_status'))? $zf_filter_input->cust_monthlyfee_status : $this->_getParam('cust_monthlyfee_status');
					//$this->view->cust_token_auth   = ($zf_filter_input->isValid('cust_token_auth'))? $zf_filter_input->cust_token_auth : $this->_getParam('cust_token_auth');

					$error = $zf_filter_input->getMessages();

					//format error utk ditampilkan di view html 
					$errorArray = null;
					foreach ($error as $keyRoot => $rowError) {
						foreach ($rowError as $errorString) {
							$errorArray[$keyRoot] = $errorString;
						}
					}

					//pengaturan error untuk cust emobile, phone dan fax
					if ($flag_cust_phone == 'F')    $errorArray['cust_phone'] = $this->language->_('Invalid phone number format');
					if ($flag_cust_fax == 'F')      $errorArray['cust_fax'] = $this->language->_('Invalid fax number format');
					//END pengaturan error untuk cust emobile, phone dan fax

					if (isset($cek_multiple_email) && $cek_multiple_email == false) $errorArray['cust_email'] = $this->language->_('Invalid format');

					$this->view->customer_msg  = $errorArray;
				}
			}
		} // END if($this->_request->isPost())
		else {
			//$resultdata = $this->getCustomer($cust_id);  

			// echo "<pre>";
			// var_dump($resultdata);die;

			if (empty($resultdata['CUST_CHARGESID'])) {
				$selectcharge = $this->_db->select()
					->from(array('B' => 'M_CHARGES_WITHIN'), array('*'))
					->where('B.CUST_ID = ?', $resultdata['CUST_ID']);

				$charge = $this->_db->fetchAll($selectcharge);
				if (!empty($charge)) {
					$this->view->customcharge = true;
				}
			} else {
				$selectcharge = $this->_db->select()
					->from(array('B' => 'M_CHARGES_WITHIN'), array('*'))
					->where('B.CHARGES_ID = ?', $resultdata['CUST_CHARGESID']);

				$charge = $this->_db->fetchAll($selectcharge);
				$this->view->packagename = $charge['0']['CUST_ID'];
			}

			$this->view->go_publicArr = [['VALUE' => "Y", "DESC" => "Ya"], ["VALUE" => "N", "DESC" => "Tidak"]];
			$this->view->bumn_grupArr = [['VALUE' => "Y", "DESC" => "Ya"], ["VALUE" => "N", "DESC" => "Tidak"]];

			$this->view->cust_id        = strtoupper($resultdata['CUST_ID']);
			$this->view->cust_cif       = $resultdata['CUST_CIF'];
			$this->view->cust_name      = $resultdata['CUST_NAME'];
			$this->view->cust_type      = $resultdata['CUST_TYPE'];
			$this->view->cust_securities = $resultdata['CUST_SECURITIES'];
			$this->view->cust_code 		= $resultdata['CUST_CODE'];
			$this->view->CUST_SPECIAL 		= $resultdata['CUST_SPECIAL'];
			$this->view->cust_workfield = $resultdata['CUST_WORKFIELD'];
			$this->view->business_type = $resultdata['BUSINESS_TYPE'];
			$this->view->cust_address   = $resultdata['CUST_ADDRESS'];
			$this->view->cust_city      = $resultdata['CUST_CITY'];
			$this->view->cust_zip       = $resultdata['CUST_ZIP'];
			$this->view->cust_province  = $resultdata['CUST_PROVINCE'];
			$this->view->cust_village  = $resultdata['CUST_VILLAGE'];
			$this->view->cust_district  = $resultdata['CUST_DISTRICT'];
			$this->view->cust_contact   = $resultdata['CUST_CONTACT'];
			$this->view->cust_phone     = $resultdata['CUST_PHONE'];
			$this->view->cust_ext       = $resultdata['CUST_EXT'];
			$this->view->cust_fax       = $resultdata['CUST_FAX'];
			$this->view->cust_email     = $resultdata['CUST_EMAIL'];
			$this->view->cust_contact_phone     = $resultdata['CUST_CONTACT_PHONE'];
			$this->view->cust_website   = $resultdata['CUST_WEBSITE'];
			$this->view->country_code   = $resultdata['COUNTRY_CODE'];
			$this->view->cust_limit_idr   = $resultdata['CUST_LIMIT_IDR'];
			$this->view->cust_limit_usd   = $resultdata['CUST_LIMIT_USD'];
			$this->view->cust_model    = $resultdata['CUST_MODEL'];
			$this->view->go_public  = $resultdata['GO_PUBLIC'];
			$this->view->grup_bumn = $resultdata['GRUP_BUMN'] == 1 ? "Y" : "N";
			$this->view->debitur_code  = $resultdata['DEBITUR_CODE'];
			$this->view->cust_workfield  = $resultdata['CUST_WORKFIELD'];
			$this->view->cust_npwp  = $resultdata['CUST_NPWP'];
			$this->view->collectibility_code  = $resultdata['COLLECTIBILITY_CODE'];
			$this->view->same_release   = $resultdata['CUST_SAME_USER'];
			$this->view->cust_review  = $resultdata['CUST_REVIEW'];
			$this->view->cust_approver  = $resultdata['CUST_APPROVER'];
			$this->view->cust_app_token  = $resultdata['CUST_APP_TOKEN'];
			$this->view->cust_rls_token  = $resultdata['CUST_RLS_TOKEN'];
			$this->view->cust_finance  = $resultdata['CUST_FINANCE'];


			if (!empty($resultdata['CUST_ADM_ID'])) {
				$admin = $this->getUserCustEdit($resultdata['CUST_ADM_ID']);
				// print_r($data);die;
				$this->view->admin1  = $admin['USER_FULLNAME'];
				$this->view->emailadmin1  = $admin['USER_EMAIL'];
				$this->view->phoneadmin1  = $admin['USER_PHONE'];
				// $this->view->tokenadmin1  = $resultdata['CUST_LIMIT_USD'];
			}

			if (!empty($resultdata['CUST_ADM1_ID'])) {
				$admin2 = $this->getUserCustEdit($resultdata['CUST_ADM1_ID']);
				$this->view->admin2  = $admin2['USER_FULLNAME'];
				$this->view->emailadmin2  = $admin2['USER_EMAIL'];
				$this->view->phoneadmin2  = $admin2['USER_PHONE'];
				$this->view->tokenadmin2  = $admin2['TOKEN_TYPE'];
			}
			// $sysadmin = $this->getUserCustEdit($resultdata['CUST_SYS_ID']); 
			// $this->view->admin3  = $sysadmin['USER_FULLNAME'];
			// $this->view->emailadmin3  = $sysadmin['USER_EMAIL'];
			// $this->view->phoneadmin3  = $sysadmin['USER_PHONE'];
			// $this->view->tokenadmin3  = $sysadmin['TOKEN_TYPE'];
			$this->view->cust_role_adm  = $resultdata['CUST_ADM_ROLE'];
			$this->view->cust_role  = $resultdata['CUST_RDN_KEY'];
			/*$this->view->cust_charges_status  = $resultdata['CUST_CHARGES_STATUS'];
       $this->view->cust_monthlyfee_status   = $resultdata['CUST_MONTHLYFEE_STATUS'];
       $this->view->cust_token_auth   = $resultdata['CUST_TOKEN_AUTH'];*/
		}

		$this->view->modulename    = $this->_request->getModuleName();
		$this->view->lengthCustId  = $this->_custIdLength;
		$this->view->approve_type  =  $this->_masterhasStatus;



		//insert log
		try {
			$this->_db->beginTransaction();

			Application_Helper_General::writeLog('CCUD', 'Edit Customer [' . $this->view->cust_id . ']');

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollBack();
			Application_Log_GeneralLog::technicalLog($e);
		}
	}
}
