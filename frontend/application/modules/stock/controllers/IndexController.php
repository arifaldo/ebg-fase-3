<?php

require_once 'Zend/Controller/Action.php';

class Stock_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{

$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
    	$stockcode = $this->language->_('Stock Code');
    	$stockname = $this->language->_('Stock Name');
		
		
	    $fields = array(
	    
						'stock_code'  => array('field' => 'stock_code',
										      'label' => $stockcode,
										      'sortable' => true),
						'stock_name'     => array('field' => 'stock_name',
											      'label' => $stockname,
											      'sortable' => true)
				      );

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','stock_code');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'stock_code'  => array('StringTrim','StripTags'),
							'stock_name'      => array('StringTrim','StripTags')
		);

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
		//$filter_lg = $this->_getParam('filter');	
		
		//$tempfields = $fields;
		//unset($tempfields['country_name']);
		//$getData = Application_Helper_Array::SimpleArray($tempfields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_STOCK'),array('STOCK_CODE','STOCK_NAME','CUST_ID'))
					        ->where('CUST_ID = ? ', $this->_custIdLogin);


		
		if($filter == true || $csv || $pdf || $this->_request->getParam('print')) 
		{  
			
		    $header = Application_Helper_Array::simpleArray($fields,'label');
			$fstock_code = $zf_filter->getEscaped('stock_code');
			$fstock_name     = $zf_filter->getEscaped('stock_name');
			
	        if($fstock_code) $select->where('UPPER(STOCK_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fstock_code).'%'));
	        if($fstock_name)     $select->where('UPPER(STOCK_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fstock_name).'%'));
	        
			
			$this->view->stock_code = $fstock_code;
			$this->view->stock_name  = $fstock_name;
			
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);  
		
		$select = $this->_db->fetchall($select);
			
		
		
		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
	    	foreach($select as $key=>$value)
	    	{
	    		unset($select[$key]['country_code']);
	    	}
				$this->_helper->download->csv($header,$select,null,'STOCK LIST');
				Application_Helper_General::writeLog('RBLS','Download CSV Stock Bank');
		}
		else if($pdf)
		{
			foreach($select as $key=>$value)
			{
				unset($select[$key]['country_code']);
			}
				$this->_helper->download->pdf($header,$select,null,'STOCK LIST');
				Application_Helper_General::writeLog('RBLS','Download PDF Stock Bank');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Remittance Bank', 'data_header' => $fields));
       	}
		else
		{		
				Application_Helper_General::writeLog('RBLS','View Remittance Bank');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

	}

}