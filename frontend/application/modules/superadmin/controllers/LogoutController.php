<?php

class superadmin_LogoutController extends Application_Main {
	
	
	public function indexAction()
	{
		$objSessionNamespace = new Zend_Session_Namespace( 'sysadmin' );
		
		unset($objSessionNamespace->userIdLogin);
		unset($objSessionNamespace->userNameLogin);
		unset($objSessionNamespace->isSuperUser);
		unset($objSessionNamespace->userGroupLogin);
		unset($objSessionNamespace->custIdLogin);
		$objSessionNamespace->setExpirationSeconds(0);
		
		$objSessionNamespace->destroy();
		
		$this->_redirect('/');
	}
}
?>