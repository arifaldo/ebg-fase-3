<?php

require_once 'Zend/Controller/Action.php';

class Nostrobank_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
	    //pengaturan url untuk button back
			$this->_helper->layout()->setLayout('newlayout');
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}


    	$this->view->report_msg = array();

    	$model = new nostrobank_Model_Nostrobank();
	    $this->view->ccyArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCCYList(),'CCY_ID','CCY_ID'));

		// var_dump($nostroid); die;
		if(!$this->_request->isPost())
		{
			$nostroid = $this->_getParam('nostroid');
			// $nostroid = '2';
			// die('here');

			if(!empty($nostroid))
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('A'=>'M_NOSTRO_BANK'),array('NOSTRO_ID','CCY','NOSTRO_NAME','NOSTRO_SWIFTCODE'))
									 ->where("NOSTRO_ID = ?", $nostroid)
							);

			  if($resultdata)
			  {
			  		$this->view->nostroid = $resultdata['NOSTRO_ID'];
			        $this->view->ccy  = $resultdata['CCY'];
					//$this->view->country_name      = $resultdata['COUNTRY_NAME'];
					//$this->view->benef_bank  = $resultdata['BANK_NAME'] . " - " . $resultdata['BANK_CODE'];
					//$this->view->bank_code   	= $resultdata['BANK_CODE'];
					$this->view->nostro_name   	= $resultdata['NOSTRO_NAME'];
					$this->view->nostro_swiftcode   	= $resultdata['NOSTRO_SWIFTCODE'];
			  }
			}
			else
			{
			   $error_remark = 'Nostro Bank not found';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/index');
			}
		}
		else
		{
			$filters = array(
							 'nostroid'     => array('StringTrim','StripTags'),
							 'ccy'     => array('StringTrim','StripTags','StringToUpper'),
							 /*'country_name' => array('StringTrim','StripTags','StringToUpper'),
							 'benef_bank'    => array('StringTrim','StripTags','StringToUpper'),
							 'bank_code'    => array('StringTrim','StripTags','StringToUpper'),*/
							 'nostro_name'     => array('StringTrim','StripTags','StringToUpper'),
							 'nostro_swiftcode'=> array('StringTrim','StripTags','StringToUpper'),
							);

			$validators = array(
								'nostroid' => array('NotEmpty',
													 'messages' => array(
																         $this->language->_('Can not be empty'),
			                                                             )
													),
								'ccy' => array('NotEmpty',
													 'messages' => array(
																         $this->language->_('Can not be empty'),
			                                                             )
													),
								/*'country_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>128)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Data too long (max 128 chars)')
																         )
														),
								'benef_bank'      => array('NotEmpty',
													 //new Zend_Validate_StringLength(array('max'=>8)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		//$this->language->_( 'Data too long (max 8 chars)')
																         )
														),
								'bank_code'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>8)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Bank Code too long (max 8 chars)')
																         )
														),*/
								'nostro_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>50)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																		$this->language->_( 'Data too long (max 50 chars)')
																         )
														),
								'nostro_swiftcode' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>11)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																		 $this->language->_( 'Data too long (max 11 chars)')
			                                                             )
													),
							   );

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				$checkUnique = $model->checkNostroBankEdit($zf_filter_input->ccy,$zf_filter_input->nostro_name,$zf_filter_input->nostroid);
				if($checkUnique == true){

					$content = array(
									'CCY'	=> $zf_filter_input->ccy,
									'NOSTRO_NAME' 	 => $zf_filter_input->nostro_name,
									'NOSTRO_SWIFTCODE' 	=> $zf_filter_input->nostro_swiftcode,
									'UPDATED'	=> date('Y-m-d H:i:s'),
									'UPDATEDBY'	=> $this->_userIdLogin
							       );

					try
					{
					    //-----insert--------------
						$this->_db->beginTransaction();

						$whereArr  = array('NOSTRO_ID = ?'=>$zf_filter_input->nostroid);
						$this->_db->update('M_NOSTRO_BANK',$content,$whereArr);

						$this->_db->commit();

						foreach(array_keys($filters) as $field)
							$this->view->$field = $zf_filter_input->getEscaped($field);

						Application_Helper_General::writeLog('NBUD','Edit Nostro Bank. CCY : ['.$zf_filter_input->ccy.'] , Nostro Bank Name : ['.$zf_filter_input->nostro_name.']');
						$this->view->success = true;
						$this->view->report_msg = array();

						$this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();

					    foreach(array_keys($filters) as $field)
							$this->view->$field = $zf_filter_input->getEscaped($field);
						$errorMsg = 'exception';
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					}
				}
				else{
					$this->view->notunique = true;
					foreach(array_keys($filters) as $field)
							$this->view->$field = $zf_filter_input->getEscaped($field);
				}
			}
			else
			{
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->success = false;
                $this->view->report_msg = $errorArray;
			}
		}
	}

}
