<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class paymentworkflow_RequestrepairController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}	

    	$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
    	
    	$payType = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
    	$trfType = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		
		foreach($payType as $key => $value){  $filterPayType[$key] = $value; }
		foreach($trfType as $key => $value){ if($key != 3 && $key != 4) $filterTrfType[$key] = $value; }
		
		$fields = array('payReff'  		=> array('field' 	=> 'payReff',
												 'label' 	=> $this->language->_('Payment Ref').'#',
												 'sortable' => true),
						'paySubj'  		=> array('field' 	=> 'paySubj',
												 'label' 	=> $this->language->_('Subject'),
												 'sortable' => true),
						//'created'  		=> array('field' 	=> 'created',
						//						 'label' 	=> $this->language->_('Created Date'),
						//						 'sortable' => true),	
						'efdate'  		=> array('field' 	=> 'efdate',
												 'label' 	=> $this->language->_('Payment Date'),
												 'sortable' => true),
						'updated'  		=> array(
												'field' 	=> 'updated',
												'label' 	=> $this->language->_('Last Updated'),
												'sortable' => true
						),
						'accsrc'  		=> array('field' 	=> 'accsrc',
												 'label' 	=> $this->language->_('Source Account'),
												 'sortable' => true),
						/*'accsrc_name'  	=> array('field' 	=> 'accsrc_name',
												 'label' 	=> $this->language->_('Source Account Name'),
												 'sortable' => true),*/
						'acbenef'  		=> array('field' 	=> 'acbenef',
												 'label' 	=> $this->language->_('Beneficiary Account'),
												 'sortable' => true),
						/*'acbenef_name'  => array('field' 	=> 'acbenef_name',
												 'label'	=> $this->language->_('Beneficiary Name'),
												 'sortable' => true),*/
						/*'numtrx'  		=> array('field' 	=> 'numtrx',
												 'label' 	=> '#'.$this->language->_('Trans'),
												 'sortable' => true),
						'amount'  			=> array('field' 	=> 'amount',
												 'label' 	=> $this->language->_('Amount'),
												 'sortable' => true), */
						/*'amount'  		=> array('field' 	=> 'amount',
												 'label' 	=> $this->language->_('Amount'),
												 'sortable' => true),*/
						'REASON'  		=> array('field'		=> 'REASON',
												 'label' 	=> $this->language->_('Reason'),
												 'sortable' => true),
						'payType'  		=> array('field'	=> 'payType',
												 'label' 	=> $this->language->_('Payment Type'),
												 'sortable' => true),
						
						);
		
		$filterlist = array('PS_CREATED','PS_EFDATE','PS_NUMBER','PS_TYPE','TRANSFER_TYPE');
		
		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','created');
		$sortDir = $this->_getParam('sortdir','desc');
		
		$this->view->sortBy 	= $sortBy;
		$this->view->sortDir 	= $sortDir;
		
//		echo "<pre>";
//		//print_r($this->_getParam());
//		echo $sortBy."<br>";
//		echo $sortDir."<br>";
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';				
						
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'PS_NUMBER'  		=> array('StringTrim','StripTags','StringToUpper'),
							'PS_TYPE'  		=> array('StringTrim','StripTags'),
							'TRANSFER_TYPE'  		=> array('StringTrim','StripTags'),
							'PS_CREATED'   => array('StringTrim','StripTags'),
							'PS_CREATED_END'     => array('StringTrim','StripTags'),
							'PS_EFDATE'   => array('StringTrim','StripTags'),
							'PS_EFDATE_END'     => array('StringTrim','StripTags'),
		);
		
		// if POST value not null, get post, else get param
		$dataParam = array("PS_NUMBER", "PS_TYPE", "TRANSFER_TYPE", "PS_CREATED", "PS_CREATED_END", "PS_EFDATE", "PS_EFDATE_END");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	


		$dataParam = array("PS_NUMBER","PS_TYPE","TRANSFER_TYPE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
			if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}
			

			if(!empty($this->_request->getParam('efdate'))){
				$efdatearr = $this->_request->getParam('efdate');
					$dataParamValue['PS_EFDATE'] = $efdatearr[0];
					$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
			}
		
		// The default is set so all fields allow an empty string
		$options = array('allowEmpty' => true);
		$validators = array(
						'PS_NUMBER'		=> array(),
        				'PS_TYPE' 		=> array(),
        				'TRANSFER_TYPE' 		=> array(),
						'PS_CREATED'   	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_CREATED_END'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_EFDATE'   	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'PS_EFDATE_END'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
							);
											 
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter 	= $this->_getParam('filter');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');
		$fromMenu	= $this->_getParam('m');
		
		$fPaymentReff = $zf_filter->getEscaped('PS_NUMBER');
		$fPaymentType = $zf_filter->getEscaped('PS_TYPE');
		$fTrfType 	  = $zf_filter->getEscaped('TRANSFER_TYPE');
		$fCreatedFrom = $zf_filter->getEscaped('PS_CREATED');
		$fCreatedTo   = $zf_filter->getEscaped('PS_CREATED_END');
		$fPayDateFrom = $zf_filter->getEscaped('PS_EFDATE');
		$fPayDateTo   = $zf_filter->getEscaped('PS_EFDATE_END');
		$fPaymentStart 	= $zf_filter->getEscaped('PS_EFDATE');
		$fPaymentEnd 	= $zf_filter->getEscaped('PS_EFDATE_END');


		$paramPayment = array("WA" 				=> true,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
		
		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		
		$select->where('P.PS_STATUS = ?', (string)$this->_paymentstatus["code"]["requestrepair"]);			
		/*
		if ($fromMenu == 1)
		{
			//$defaultFromDate = Application_Helper_General::addDate(date('Y-m-d'), 0, -1);	// 1 month
			$defaultFromDate = date('Y-m-d');
			$fCreatedFrom 	 = ($fCreatedFrom)? $fCreatedFrom: Application_Helper_General::convertDate($defaultFromDate);
			$fCreatedTo   	 = ($fCreatedTo  )? $fCreatedTo  : Application_Helper_General::convertDate(date('Y-m-d'));
			//var_dump($fCreatedTo);
			//var_dump($fCreatedFrom);
		} */
								
		// Filter Data
		if($fCreatedFrom)
		{
			$FormatDate 	= new Zend_Date($fCreatedFrom, $this->_dateDisplayFormat);
			$createdFrom   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) >= ?', $createdFrom);
		}
		
		if($fCreatedTo)
		{
			$FormatDate 	= new Zend_Date($fCreatedTo, $this->_dateDisplayFormat);
			$createdTo  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) <= ?', $createdTo);
		}
		
		if($fPayDateFrom)
		{
			$FormatDate 	= new Zend_Date($fPayDateFrom, $this->_dateDisplayFormat);
			$payDateFrom   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) >= ?', $payDateFrom);
		}
		
		if($fPayDateTo)
		{
			$FormatDate 	= new Zend_Date($fPayDateTo, $this->_dateDisplayFormat);
			$payDateTo  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) <= ?', $payDateTo);
		}
		
		if($fPaymentReff)
		{	//$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPaymentReff.'%'));
			$fPaymentReffArr = explode(',', $fPaymentReff);
			$select->where("UPPER(P.PS_NUMBER)  in (?)",$fPaymentReffArr );	
		}	
		
		if($fPaymentType)
		{	
			$fPayType = explode(',', $fPaymentType);
			$select->where("P.PS_TYPE in (?) ", $fPayType);					
		}
		
		if ($fTrfType != "")
		{	$select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and TRANSFER_TYPE = ?) > 0", (string)$fTrfType);		}
//		{	$select->where("T.TRANSFER_TYPE = ? ", (string)$fTrfType);		}
		
		// $select->order($sortBy.' '.$sortDir);
		
	//	$a = Application_Helper_General::casePaymentType($this->_paymenttype, $this->_transfertype);
		
		// echo $select;die();
		// echo "<pre>";
		 
		$dataSQL = $this->_db->fetchAll($select);
		
// 		echo "<pre>";
		//print_r($select);
// 		echo $select->__toString();
		//echo "<br />Data sql: ";
		// echo "<pre>";
		// print_r($dataSQL);
		// die();
		
		if ($csv || $pdf || $this->_request->getParam('print'))
		{	$header  = Application_Helper_Array::simpleArray($fields, "label");		}
		else
		{
			$this->paging($dataSQL);	
			$dataSQL = $this->view->paginator;
		}
		
		// arrange data for viewing in html, csv, pdf
		$data = array();
		foreach ($dataSQL as $d => $dt)
		{

			
// 			print_r($dt);
			
			//if (!$csv && !$pdf)		
			//{	$data[$d]["allowMultiAction"] = ($dt["PS_CATEGORY"] == "SINGLE PAYMENT")? true: false;	}
			foreach ($fields as $key => $field)
			{
// 				Zend_Debug::dump($fields);
				$value = $dt[$key];
				/*if ($key == "amount" && !$csv)	{	$value = Application_Helper_General::displayMoney($value);}
				else*/if ($key == "created") 		{ $value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat); }
				elseif ($key == "updated") 		{ $value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat); }
				elseif ($key == "efdate") 		{ $value = Application_Helper_General::convertDate($value, $this->_dateViewFormat); }
				elseif($key=="REASON"){
					 $select = $this->_db->select()
         						->from(array('H' => 'T_PSLIP_HISTORY'),array())
								->join(array('M'=>'M_USER'), 'M.USER_ID = H.USER_LOGIN', array('M.USER_FULLNAME','H.*'))
         						->where('H.HISTORY_STATUS = ?','3')
								->where('M.CUST_ID = ?',$this->_custIdLogin)
         						->where('H.PS_NUMBER = ?',$dt['PS_NUMBER'])
								->limit(1)
								->ORDER('H.HISTORY_ID DESC')
									;
					 $reason = $this->_db->fetchAll($select);
					 //var_dump($reason);die;
					 $value = $reason['0']['PS_REASON'].' (By : '.$reason['0']['USER_FULLNAME'].' )';
				}
				elseif (($key == "accsrc") && $value != "-")
				{	
					/*$value = $value."[".$dt[$key.'_ccy']."]";*/
					if (empty($dt['SOURCE_ACCOUNT_BANK'])) {
						$setting = new Settings();
						$master_bank_name1 = $setting->getSetting('master_bank_name');
					}else{
						$master_bank_name1 = $dt['SOURCE_ACCOUNT_BANK'];
					}

					if(empty($dt["accsrc_alias"]) || $dt["accsrc_alias"] == '-'){
						$value = $dt['accsrc'].' ('.$dt['accsrc_ccy'].')'." / ".$master_bank_name1." / ".$dt['accsrc_name']." (S)";
					}
					else{
						$value = $dt['accsrc'].' ('.$dt['accsrc_ccy'].')'." / ".$master_bank_name1." / ".$dt['accsrc_alias']." (A)";
					}
				}

				elseif ($key == "acbenef" && $value != "-"){

					if (empty($dt['BENEFICIARY_ACCOUNT_BANK'])) {
						$setting = new Settings();
						$master_bank_name2 = $setting->getSetting('master_bank_name');
					}else{
						$master_bank_name2 = $dt['BENEFICIARY_ACCOUNT_BANK'];
					}

					if(empty($dt["acbenef_alias"]) || $dt["acbenef_alias"] == '-'){
						$value = $dt['acbenef'].' ('.$dt['acbenef_ccy'].')'." / ".$master_bank_name2." / ".$dt['acbenef_name']." (S)";
					}
					else{
						$value = $dt['acbenef'].' ('.$dt['acbenef_ccy'].')'." / ".$master_bank_name2." / ".$dt['acbenef_alias']." (A)";
					}
				}
				elseif ($key == "amount" && $value != "-"){
					$value = $dt['ccy']." ".Application_Helper_General::displayMoney($dt['amount']);
				}
				else if($key == 'payType' && $value != '-'){
					if ($dt['TRANSFER_TYPE'] == 0) {
						if ($dt['SOURCE_ACCT_BANK_CODE'] == $dt['BENEF_ACCT_BANK_CODE']) {
							$value = $dt['payType'].' - '.'PB';
						}
						else{
							$value = $dt['payType'].' - '.'ONLINE';
						}
					}
					else if($dt['TRANSFER_TYPE'] == 1){
						$value = $dt['payType'].' - '.'SKN';
					}
					else if($dt['TRANSFER_TYPE'] == 2){
						$value = $dt['payType'].' - '.'RTGS';
					}
				}
				
				$value = ($value == "" && !$csv)? "&nbsp;": $value;
				$data[$d][$key] = $this->language->_($value);
				$data[$d]['transfer_type'] = $dt['TRANSFER_TYPE'];
				$data[$d]['ps_type'] = $dt['PS_TYPE'];
			}
		}
 		//print_r($data);
 		//die;
		$filterlistdata = array("Filter");
		foreach($dataParamValue as $fil => $val){
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
		}
		if($csv)
		{
			foreach($data as $key => $val)
			{
				unset($data[$key]['transfer_type']);
			}
			$listable = array_merge_recursive(array($header), $data);
			$this->_helper->download->csv($filterlistdata,$listable,null,'List Repair');  
			Application_Helper_General::writeLog('PRLP','Export CSV Repair');
		}
		elseif($pdf)
		{
			foreach($data as $key => $val)
			{
				unset($data[$key]['transfer_type']);
			}
			$this->_helper->download->pdf($header,$data,null,'List Repair');  
			Application_Helper_General::writeLog('PRLP','Export PDF Repair');
		}
		elseif($this->_request->getParam('print')){
		// $fields = array('payReff'  		=> array('field' 	=> 'payReff',
		// 										 'label' 	=> $this->language->_('Payment Ref').'#',
		// 										 'sortable' => true),
		// 				'paySubj'  		=> array('field' 	=> 'paySubj',
		// 										 'label' 	=> $this->language->_('Subject'),
		// 										 'sortable' => true),
		// 				'created'  		=> array('field' 	=> 'created',
		// 										 'label' 	=> $this->language->_('Created Date'),
		// 										 'sortable' => true),	
		// 				'efdate'  		=> array('field' 	=> 'efdate',
		// 										 'label' 	=> $this->language->_('Payment Date'),
		// 										 'sortable' => true),
		// 				'accsrc'  		=> array('field' 	=> 'accsrc',
		// 										 'label' 	=> $this->language->_('Source Account'),
		// 										 'sortable' => true),
		// 				'accsrc_name'  	=> array('field' 	=> 'accsrc_name',
		// 										 'label' 	=> $this->language->_('Source Account Name / Alias'),
		// 										 'sortable' => true),
		// 				'acbenef'  		=> array('field' 	=> 'acbenef',
		// 										 'label' 	=> $this->language->_('Beneficiary Account'),
		// 										 'sortable' => true),
		// 				'acbenef_name'  => array('field' 	=> 'acbenef_name',
		// 										 'label'	=> $this->language->_('Beneficiary Name / Alias'),
		// 										 'sortable' => true),
		// 				'numtrx'  		=> array('field' 	=> 'numtrx',
		// 										 'label' 	=> '#'.$this->language->_('Trans'),
		// 										 'sortable' => true),
		// 				'ccy'  			=> array('field' 	=> 'ccy',
		// 										 'label' 	=> $this->language->_('CCY'),
		// 										 'sortable' => true),
		// 				'amount'  		=> array('field' 	=> 'amount',
		// 										 'label' 	=> $this->language->_('Amount'),
		// 										 'sortable' => true),
		// 				'payType'  		=> array('field'	=> 'payType',
		// 										 'label' 	=> $this->language->_('Payment Type'),
		// 										 'sortable' => true),
		// 				);	
			$filterlistdatax = $this->_request->getParam('data_filter');
            $this->_forward('printtable', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'List Repair', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
        }
		else
		{	
			$stringParam = array('PS_NUMBER'		=> $fPaymentReff,
								 'PS_TYPE'		=> $fPaymentType,
								 'TRANSFER_TYPE'		=> $fTrfType,
								 'PS_CREATED'	=> $fCreatedFrom,
								 'PS_CREATED_END'	=> $fCreatedTo,
								 'PS_EFDATE'	=> $fPayDateFrom,
								 'PS_EFDATE_END'	=> $fPayDateTo,
							    );	
							    
			$this->view->filterPayType 	= $filterPayType;
			$this->view->filterTrfType 	= $filterTrfType;
			$this->view->payReff 		= $fPaymentReff;
			$this->view->payType 		= $fPaymentType;
			$this->view->trfType 		= $fTrfType;
			$this->view->createdFrom 	= $fCreatedFrom;
			$this->view->createdTo 		= $fCreatedTo;
			$this->view->paymentStart 	= $fPaymentStart;
			$this->view->paymentEnd 	= $fPaymentEnd;
			
			$this->view->payDateFrom 	= $fPayDateFrom;
			$this->view->payDateTo 		= $fPayDateTo;
			$this->view->query_string_params = $stringParam;

			if(!empty($this->_request->getParam('wherecol'))){
				$this->view->wherecol			= $this->_request->getParam('wherecol');
			}

			if(!empty($this->_request->getParam('whereopt'))){
				$this->view->whereopt			= $this->_request->getParam('whereopt');
			}

			if(!empty($this->_request->getParam('whereval'))){
				$this->view->whereval			= $this->_request->getParam('whereval');
			}
			
			$this->view->data 				= $data;
			$this->view->fields 			= $fields;
			$this->view->filter 			= $filter;
	        $this->view->sortBy 			= $sortBy;
			$this->view->sortDir 			= $sortDir;
			
			Application_Helper_General::writeLog('PRPP','View Repair');
			
			//$this->view->allowMultiApprove 	= ($this->view->hasPrivilege('PAPV') && count($data) > 0);
			//$this->view->allowMultiReject  	= ($this->view->hasPrivilege('PRJT') && count($data) > 0);

			if(!empty($dataParamValue)){
	    		$this->view->createdStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
	    		$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
	    		$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];
	    		
	    	  	unset($dataParamValue['PS_CREATED_END']);
				unset($dataParamValue['PS_EFDATE_END']);	    	  	

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',',$value);
								if(!empty($duparr)){
									
									foreach($duparr as $ss => $vs){
										$wherecol[]	= $key;
										$whereval[] = $vs;
									}
								}else{
										$wherecol[]	= $key;
										$whereval[] = $value;
								} 
			}
		        $this->view->wherecol     = $wherecol;
		        $this->view->whereval     = $whereval;
		     // print_r($whereval);die;
			  }
			  
			$this->view->filterlistdata = $filterlistdata;
		}		
	}
}
