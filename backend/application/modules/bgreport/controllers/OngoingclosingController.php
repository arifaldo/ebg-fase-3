<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';

require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';
require_once("Service/Account.php");

class bgreport_OngoingclosingController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$settings = new Settings();
		$conf = Zend_Registry::get('config');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$AESMYSQL = new Crypt_AESMYSQL();

		$system_type = $settings->getSetting('system_type');
		$stamp_fee = $settings->getSetting('stamp_fee');
		$adm_fee = $settings->getSetting('adm_fee');

		$claim_period = $settings->getSetting('max_claim_period');
		$getOtpTrx = $this->_db->select()->from("M_SETTING")->where("SETTING_ID = ?", "otp_exp_trx")->query()->fetchAll();

		$bankName = $conf['app']['bankname'];

		$toc_ind = $settings->getSetting('ftemplate_bg_ind');
		$toc_eng = $settings->getSetting('ftemplate_bg_eng');

		$ctdesc = $conf["bgclosing"]["changetype"]["desc"];
		$ctcode = $conf["bgclosing"]["changetype"]["code"];
		$suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

		$get_cash_collateral = $this->_db->select()
			->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
			->where("CUST_ID = ?", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$this->view->systemType = $system_type;
		$this->view->stamp_fee = $stamp_fee;
		$this->view->adm_fee = $adm_fee;
		$this->view->BG_CLAIM_PERIOD = $claim_period;
		$this->view->timeExpOtp = $getOtpTrx[0]["SETTING_VALUE"];
		$this->view->ProvFee = 2000000;
		$this->view->masterbankname = $bankName;
		$this->view->toc_ind = $toc_ind;
		$this->view->toc_eng = $toc_eng;
		$this->view->userid = $this->_userIdLogin;
		$this->view->custid = $this->_custIdLogin;
		$this->view->googleauth = true;
		$this->view->cash_collateral = $get_cash_collateral[0];
		$this->view->suggestion_type = $suggestion_type;

		$enc_pass = $settings->getSetting('enc_pass');
		$enc_salt = $settings->getSetting('enc_salt');

		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token = $rand;
		$password = $sessionNamespace->token;

		$numb = $this->_getParam('bgnumb');
		$refid = $this->_getParam('refid');

		$BG_NUMBER = urldecode($numb);
		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
		$numb = $BG_NUMBER;

		$closeRefNumber = urldecode($refid);
		$closeRefNumber = $AESMYSQL->decrypt($closeRefNumber, $password);
		$refid = $closeRefNumber;

		$this->view->bgnumb_enc = urlencode($AESMYSQL->encrypt($numb, uniqid()));

		$this->view->token = $sessionNamespace->token;
		$this->view->numb = $numb;

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*', 'CHANGE_TYPE_CLOSE' => 'D.CHANGE_TYPE', 'BG_REG' => 'A.BG_REG_NUMBER', 'T_CUST_EMAIL' => 'A.CUST_EMAIL'))
				->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
				->where('A.BG_NUMBER = ?', $numb)
				->where('D.CLOSE_REF_NUMBER = ?', $refid)
				->query()->fetchAll();
				
			if (!empty($bgdata)) {
				$data = $bgdata['0'];

				$bgdatadetail = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $data['BG_REG'])
					->query()->fetchAll();

				$bgdatadetailclose = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$selectcomp = $this->_db->select()
					->from(array('A' => 'M_CUSTOMER'), array('*'))
					->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
					->where('A.CUST_ID = ?', $data['CUST_ID'])
					->query()->fetchAll();
				$this->view->compinfo = $selectcomp[0];

				$closeDocument = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$bgpublishType = $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode = $conf["bgpublish"]["type"]["code"];
				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
				$this->view->publishDesc = $arrbgpublish[$data['BG_PUBLISH']];

				if ($data['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('NOTES = ?', $data['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();
					$totalKertas = count($getPaperPrint);
					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
					$this->view->isinsurance = true;
					$this->view->stamp_fee = $data['STAMP_FEE'];
					$this->view->adm_fee = $data['ADM_FEE'];
					$this->view->ProvFee = $data['PROVISION_FEE'];
				}
				$this->view->closeDocument = $closeDocument;
				$this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];
				// $this->view->policyBoundary = $policyBoundary;

				if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceName = array_search("Insurance Name", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];
					$getInsuranceName = $bgdatadetail[$getInsuranceName];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();
					$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];

					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $getInsuranceName["PS_FIELDVALUE"];
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
					// end get linefacility
				}

				if ($data['COUNTER_WARRANTY_TYPE'] == '2') {
					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $this->_custIdLogin;
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
				}

				if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $this->_custIdLogin;
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
				}

				// ========================= History Workflow =========================
				$historyClosing = $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY_CLOSE')
					->order('DATE_TIME ASC')
					->where("BG_NUMBER = ?", $numb)
					->where("CLOSE_REF_NUMBER = ?", $refid);
				$historyClosing = $this->_db->fetchAll($historyClosing);
				
				// Boundary FO
				$getBoundary = $this->_db->select()
					->from('M_APP_BOUNDARY')
					->where('CUST_ID = ?', $data['CUST_ID'])
					->where('BOUNDARY_MIN <= \'' . $data['BG_AMOUNT'] . '\'')
					->where('BOUNDARY_MAX >= \'' . $data['BG_AMOUNT'] . '\'')
					->query()->fetch();

				if (!empty($getBoundary)) {
					$policy = explode(' ', $getBoundary['POLICY']);
					if (count($policy) > 1) {
						$policyBoundary = $getBoundary['POLICY'];
					}  else {
						$policyBoundary = '';
					}
				}

				$this->view->policyBoundary = $policyBoundary;

				// Boundary BO
				$transferType =  $data['COUNTER_WARRANTY_TYPE'] == 1 ? '50' : '51';
				$getBoundaryBank	= $this->_db->select()
					->from(array('C' => 'M_APP_BGBOUNDARY'), array(
						'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
						'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
						'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
						'C.TRANSFER_TYPE',
						'C.POLICY'
					))
					->where("C.TRANSFER_TYPE 	= ?", (string) $transferType)
					->where("C.BOUNDARY_MIN 	<= ?", $data['BG_AMOUNT'])
					->where("C.BOUNDARY_MAX 	>= ?", $data['BG_AMOUNT']);
				$getBoundaryBank = $this->_db->fetchAll($getBoundaryBank);
				
				$this->view->policyBoundaryBank = $getBoundaryBank[0]['POLICY'];
				
				// Mapping ulang data history untuk push fullname dari tabel user
				$historyClosing = array_map(function($item) {
					$userLogin = $item['USER_LOGIN'];
					$custId = $item['CUST_ID'];

					if ($userLogin != 'SYSTEM') {
						$getUser = $this->_db->select()
							->from(['M' => 'M_USER'], 'USER_FULLNAME')
							->where("USER_ID = ?", $userLogin)
							->where("CUST_ID = ?", $custId);
						$getUser = $this->_db->fetchOne($getUser);
	
						if (!empty($getUser)) {
							$item['USER_FULLNAME'] = $getUser;
						} else {
							$getBuser = $this->_db->select()
								->from(['MB' => 'M_BUSER'], 'BUSER_NAME')
								->where("BUSER_ID = ?", $userLogin);
							$getBuser = $this->_db->fetchOne($getBuser);
							
							$item['USER_FULLNAME'] = $getBuser;
						}
					} else {
						$item['USER_FULLNAME'] = $userLogin;
					}

					return $item;
				}, $historyClosing);
				// end

				foreach ($historyClosing as $historyClose) {
					// Pemeriksaan awal dokumen klaim
					if (in_array($historyClose['HISTORY_STATUS'], [22, 21, 14, 13])) {
						$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
						$this->view->padkBy = '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
					}

					// Maker
					if (in_array($historyClose['HISTORY_STATUS'], [1, 24])) {
						$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
						$this->view->makerBy = '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
					}

					// Approver
					if ($historyClose['HISTORY_STATUS'] == 2) {
						if (!empty($policyBoundary)) {
							$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
							$this->view->approverBy .= '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
						} else {
							$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
							$this->view->approverBy = '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
						}
					}

					// Releaser
					if ($historyClose['HISTORY_STATUS'] == 3) {
						$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
						$this->view->releaserBy = '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
					}

					// Verifikasi bank
					if (in_array($historyClose['HISTORY_STATUS'], [8, 13, 21])) {
						$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
						$this->view->verifyByBank = '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
					}

					// Review bank
					if ($historyClose['HISTORY_STATUS'] == 10) {
						$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
						$this->view->reviewByBank = '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
					}

					// Approver (penutupan & klaim) bank
					if (in_array($historyClose['HISTORY_STATUS'], [11, 12])) {
						if (!empty($policyBoundaryBank)) {
							$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
							$this->view->approveByBank .= '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
						} else {
							$efdate = date('d M Y', strtotime($historyClose['DATE_TIME']));
							$this->view->approveByBank = '<div align="center" class="textTheme text-primary">' . $efdate . '<br><span>' . ucfirst(strtolower($historyClose['USER_FULLNAME']))  . '</span></div>';
						}
					}

					// Reason
					if(in_array($historyClose['HISTORY_STATUS'], [4, 5, 6, 7, 17])) {
						$this->view->reasonClose = $historyClose['BG_REASON'];
					}
				}

				$historyStatusCode = $conf["bgclosinghistory"]["status"]["code"];
				$historyStatusDesc = $conf["bgclosinghistory"]["status"]["desc"];
				$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));
				
				$this->view->dataHistory = $historyClosing;
				$this->view->historyStatusArr = $historyStatusArr;
				// ========================= History Workflow =========================

				$bgType = $conf["bg"]["type"]["desc"];
				$bgCode = $conf["bg"]["type"]["code"];
				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));
				$this->view->arrbgType = $arrbgType;

				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				$BgType = $conf["bgclosing"]["status"]["desc"];
				$BgCode = $conf["bgclosing"]["status"]["code"];
				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));
				$this->view->arrStatus = $arrStatus;

				$bgcgType = $conf["bgcg"]["type"]["desc"];
				$bgcgCode = $conf["bgcg"]["type"]["code"];
				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
				$this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->BG_REG_NUMBER = $data['BG_REG'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];
				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->contact_person = $data['RECIPIENT_CP'];
				$this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
				$this->view->contact_email = $data['RECIPIENT_EMAIL'];
				$this->view->data = $data;
				$this->view->fileName = $data['FILE'];
				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];
				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];
				$this->view->status = $data['BG_STATUS'];
				$this->view->claim_period = $settings->getSetting('claim_period');
				$this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
				$this->view->timeDifference = $this->calculateTimeSpan($data['CLAIM_FIRST_VERIFIED']);

				$bgdocType = $conf["bgdoc"]["type"]["desc"];
				$bgdocCode = $conf["bgdoc"]["type"]["code"];
				$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
				$this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];

				$this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
				$this->view->usage_purpose = $data['USAGE_PURPOSE'];
				$this->view->comment = $data['SERVICE'];

				if (!empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$select = $this->_db->select()
						->from(
							array('a' => 'T_BANK_GUARANTEE_HISTORY'),
							array(
								'a.USER_LOGIN', 'u_name' => 'b.USER_FULLNAME', 'b_name' => 'c.BUSER_NAME',
								'a.DATE_TIME', 'a.BG_REASON', 'a.HISTORY_STATUS', 'a.BG_REG_NUMBER'
							)
						)
						->joinLeft(
							array('b' => 'M_USER'),
							'a.USER_LOGIN = b.USER_ID AND a.CUST_ID = b.CUST_ID',
							array()
						)
						->joinLeft(array('c' => 'M_BUSER'), 'a.USER_LOGIN = c.BUSER_ID', array())
						->where('a.BG_REG_NUMBER = ?', (string) $data['BG_REG'])
						->where('a.HISTORY_STATUS = ?', (string) $data['BG_STATUS'])
						->group('a.HISTORY_ID')
						->order('a.DATE_TIME');
					$result = $this->_db->fetchAll($select);

					if (!empty($result)) {
						$data['REASON'] = $result['0']['BG_REASON'];
					}
					$this->view->reqrepair = true;
				}

				// REASON
				$cekHistory = $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY_CLOSE')
					->where('BG_NUMBER = ?', $data['BG_NUMBER'])
					->order('DATE_TIME DESC')
					->query()->fetch();

				if ($cekHistory['HISTORY_STATUS'] == '9' || $cekHistory['HISTORY_STATUS'] == '13') {
					$this->view->reason = $cekHistory['BG_REASON'] . ' (' . $cekHistory['USER_FROM'] . ' - ' .  $cekHistory['USER_LOGIN'] . ')';
				}

				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {
						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
								$insBranchCode =   $value['PS_FIELDVALUE'];
							}
						} else {
							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}

				if (!empty($bgdatadetailclose)) {
					foreach ($bgdatadetailclose as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Beneficiary is own account') {
							$this->view->rekeningPenerima =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'SWIFT CODE') {
							$domesticBanks = $this->_db->select()
								->from(array('A' => 'M_DOMESTIC_BANK_TABLE'), array('*'))
								->where('A.SWIFT_CODE = ?', $value['PS_FIELDVALUE'])
								->query()->fetchAll();

							$this->view->swiftCode = $domesticBanks[0]['BANK_NAME'];
							$this->view->swiftCodeParam = $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account CCY') {
							$this->view->valutaRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account Number') {
							$this->view->nomorRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account Name') {
							$this->view->namaRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Address') {
							$this->view->alamatRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Transaction CCY') {
							$this->view->valutaTrasaksi =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Transaction Amount') {
							$this->view->totalTransaksi =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Remark 1') {
							$this->view->berita =   $value['PS_FIELDVALUE'];
						}
					}
				}

				$this->view->bankname = $conf['app']['bankname'];

				$download = $this->_getParam('download');
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				// Marginal Deposit Eksisting ---------------
				$bgdatamdeks = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID =' . $this->_db->quote($data['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $data['BG_REG'])
					->query()->fetchAll();

				$mdeksisting = null;
				$saveEscrow = 0;

				if (count($bgdatamdeks) > 1) {
					foreach ($bgdatamdeks as $key => $value) {
						if (strtolower($value["M_ACCT_DESC"]) == "giro" || $value["M_ACCT_TYPE"] == "D") {
							continue;
						};
						$serviceAccount = new Service_Account($value['ACCT'], null);
						$accountInfo = $serviceAccount->inquiryAccontInfo();
						$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
						$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
						$listAccount = array_search($value['ACCT'], array_column($saveResult, "account_number"));

						if ($saveResult[$listAccount]["product_type"] == 'J1') {
							continue;
						} else {
							$mdeksisting += floatval($value["AMOUNT"]);
						}

						if (strtolower($value["ACCT_DESC"] == 'Escrow')) $saveEscrow += floatval($value["AMOUNT"]);
					}
				} else {
					$mdeksisting += floatval($bgdatamdeks[0]['AMOUNT']);
				}
				$this->view->totaleksisting = $mdeksisting;
				$this->view->escrow = $saveEscrow;

				// Pelepasan dana ---------------
				$mdclose = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$groupedArray = array();
				$totalClose = null;
				foreach ($mdclose as $row) {
					$index = $row['ACCT_INDEX'];
					if (!isset($groupedArray[$index])) {
						$groupedArray[$index] = array(
							'ownAccount' => '',
							'ccyAccount' => '',
							'numberAccount' => '',
							'nameAccount' => '',
							'ccyTransaction' => '',
							'amountTransaction' => '',
						);
					}
					if ($row['PS_FIELDNAME'] == 'Beneficiary is own account') {
						$groupedArray[$index]['ownAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account CCY') {
						$groupedArray[$index]['ccyAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account Number') {
						$groupedArray[$index]['numberAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account Name') {
						$groupedArray[$index]['nameAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Transaction CCY') {
						$groupedArray[$index]['ccyTransaction'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Transaction Amount') {
						$groupedArray[$index]['amountTransaction'] = $row['PS_FIELDVALUE'];
						$totalClose += $row['PS_FIELDVALUE'];
					}
				}
				$groupedArray = array_values($groupedArray);

				$this->view->totalclose = $totalClose;
				$this->view->mdclose = $groupedArray;

				$download = $this->_getParam('certfinal');
				if ($download == 1) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					return $this->_helper->download->file("BG_NO_" . $data["BG_NUMBER"] . ".pdf", $attahmentDestination . $data['DOCUMENT_ID'] . ".pdf");
				}

				// Simpan log aktifitas user ke table T_BACTIVITY
				$privilege = 'RBGL';
				Application_Helper_General::writeLog($privilege, $this->language->_('Lihat Detail Data Bank Garansi Nomor') . ' : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', Principal : ' . $data['CUST_ID']);
			}
		}
	}

	function calculateTimeSpan($date)
	{
		$now = new DateTime();
		$dateObj = new DateTime($date);
		$interval = $now->diff($dateObj);

		$days = $interval->days;
		$hours = $interval->h;
		$minutes = $interval->i;

		if ($days > 0) {
			$time = $days . " hari " . $hours . " jam " . $minutes . " menit";
		} else if ($hours > 0) {
			$time = $hours . " jam " . $minutes . " menit";
		} else {
			$time = $minutes . " menit";
		}

		return $time;
	}

	public function downloadlampiranAction()
	{
		// decrypt numb
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token = $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();

		$bgfile     = urldecode($this->_getParam('bgfile'));

		$attahmentDestination = UPLOAD_PATH . '/document/closing/';

		return $this->_helper->download->file($bgfile, $attahmentDestination . $bgfile);
	}

	public function findPolicyBoundary($transfertype, $amount)
	{
		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);

		$datauser = $this->_db->fetchAll($selectuser);
		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{
		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);

		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
		//	echo '<pre>';
		//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;
		return $userlist;
	}

	public function findUserPrivi($privID)
	{
		$selectuser	= $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'))
			->joinLeft(array('B' => 'M_USER'), 'A.FUSER_ID = CONCAT(B.CUST_ID,B.USER_ID)', array('USER_FULLNAME'))
			->where("A.FPRIVI_ID 	= ?", (string) $privID)
			->where("B.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->order('B.USER_FULLNAME ASC');

		//echo $selectuser;die();
		return $datauser = $this->_db->fetchAll($selectuser);
	}

	public function inquirymarginaldepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$regNumber = $this->_getParam('reqNumber');

		$bgdata = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		$optHtml = '<table class="table table-sm mb-1" style="font-size: 1.1em;" id="guarantedTransactions">
						<thead style="background-color: #c0c2c4;">
							<td><i>' . $this->language->_('Hold Seq') . '</i></td>
							<td><i>' . $this->language->_('Nomor Rekening') . '</i></td>
							<td><i>' . $this->language->_('Nama Rekening') . '</i></td>
							<td><i>' . $this->language->_('Tipe') . '</i></td>
							<td><i>' . $this->language->_('Valuta') . '</i></td>
							<td><i>' . $this->language->_('Nominal') . '</i></td>
						</thead>
						<tbody>';

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($bgdata[0]['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		if (!empty($bgdatasplit)) {
			foreach ($bgdatasplit as $key => $row) {
				if (strtolower($row["M_ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D") continue;
				// GET SERVICE 
				$serviceAccount = new Service_Account($row['ACCT'], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($row['ACCT'], array_column($saveResult, "account_number"));

				$row['HOLD_SEQUENCE'] ? $holdSequence = $row['HOLD_SEQUENCE'] : $holdSequence = '-';
				if ($saveResult[$listAccount]["product_type"] == 'J1') {
					continue;
				}
				$optHtml .= '<tr class="table-secondary">
					<td>' . $holdSequence . '</td>
					<td>' . $row['ACCT'] . '</td>
					<td>' . $row['NAME'] . '</td>
					<td>' . $saveResult[$listAccount]["type_desc"] . " " . $saveResult[$listAccount]["product_type"] . '</td>
					<td>' . $saveResult[$listAccount]["currency"] . '</td>
					<td>' . Application_Helper_General::displayMoney($row['AMOUNT']) . '</td>
				</tr>';
			}
		} else {
			$optHtml .= '<tr><td colspan="7" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}
		$optHtml .= '</tbody></table>';
		$optHtml .= '<p style="padding-left:2%; font-size: 1.2em;" class="mb-1"><font class="text-danger">* </font><i>' . $this->language->_("Rekening Tipe Deposito dan Saving akan dilakukan pelepasan dana ditahan (tanpa pemindahan dana)") . '</i></p>';
		echo $optHtml;
	}

	public function inquiryaccountinfoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);
		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function inquiryrekeningklaimAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		$swiftcode = $this->_request->getParam('swiftcode');
		$acct_name = $this->_request->getParam('acct_name');

		if (strtolower($swiftcode) == 'btanidja') {
			if ($result['response_desc'] == 'Success') //00 = success
			{
				$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
				$result2 = $svcAccountCIF->inquiryCIFAccount();

				$filterBy = $result['account_number']; // or Finance etc.
				$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
					return ($var['account_number'] == $filterBy);
				});

				$singleArr = array();
				foreach ($new as $key => $val) {
					$singleArr = $val;
				}
			} else {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($result);
				return 0;
			}

			if ($singleArr['type_desc'] == 'Deposito') {
				$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountDeposito->inquiryDeposito();

				if ($result3["response_code"] != "0000") {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				$sqlRekeningJaminanExist = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
					->where('A.ACCT = ?', $result['account_number'])
					->query()->fetchAll();

				$result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;

				$get_product_type = current($new)["type"];

				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			} else {
				$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountBalance->inquiryAccountBalance();

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode($result3);
					return 0;
				}

				$get_product_type = current($new)["type"];
				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			}

			$return = array_merge($result, $singleArr, $result3);

			$data['data'] = false;
			is_array($return) ? $return :  $return = $data;

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($return);
		} else {
			$clientUser  =  new SGO_Soap_ClientUser();

			$getInfoBank = $this->_db->select()
				->from('M_DOMESTIC_BANK_TABLE')
				->where('SWIFT_CODE = ?', $swiftcode)
				->query()->fetch();

			$request = [
				'account_number' => $acct_no,
				'bank_code' => $getInfoBank['BANK_CODE']
			];

			$clientUser->callapi('otherbank', NULL, $request);
			$responseService = $clientUser->getResult();

			$result = [
				'valid' => true,
				'account_name' => $responseService['account_name']
			];

			if ($responseService['response_code'] != '0000') {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Rekening tidak ditemukan')
				];
			}

			if (strtolower($responseService['account_name']) !== strtolower($acct_name)) {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Nama dan Nomor Rekening tidak sesuai')
				];
			}

			$result = array_merge($result, array_intersect_key($responseService, ['response_code' => '']));
			echo json_encode($result);
			return;
		}
	}

	public function checkvalidasiAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$bgNumber = $this->_getParam('bgNumber');

		$data = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE')
			->where('BG_NUMBER = ?', $bgNumber)
			->where('CHANGE_TYPE = 3')
			->where('SUGGESTION_STATUS NOT IN (7,11)')
			->query()->fetch();

		if (!empty($data)) {
			$return['status'] = true;
		} else {
			$return['status'] = false;
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($return);
	}

	public function showfinaldocAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// decrypt numb

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

		$file = $this->_request->getParam("file");

		$get_document_id = $this->_db->select()
			->from("T_BANK_GUARANTEE", ["DOCUMENT_ID"])
			->where("BG_NUMBER = ?", $BG_NUMBER)
			->query()->fetch();

		$get_file_name = $get_document_id["DOCUMENT_ID"] . ".pdf";

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=" . $get_file_name);
		// @readfile('/app/bg/library/data/uploads/document/submit/' . $get_file_name);
		@readfile(UPLOAD_PATH . '/document/submit/' . $get_file_name);
	}

	public function removetempAction()
	{
		$this->_db->delete("TEMP_REQUEST_REBATE", "1");
	}

	public function getbusersbgcAction()
	{
		$getprivi = $this->_db->select()
			->from(["MBPG" => "M_BPRIVI_GROUP"], ["*"])
			->where("MBPG.BPRIVI_ID = ?", "SBGC")
			->query()->fetchAll();

		$savegroup = [];
		foreach ($getprivi as $key => $value) {
			array_push($savegroup, $value["BGROUP_ID"]);
		}

		$getbuser = $this->_db->select()
			->from(["MB" => "M_BUSER"], ["*"])
			->joinleft(["BRANCH" => "M_BRANCH"], "BRANCH.ID = MB.BUSER_BRANCH", ["BRANCH.BRANCH_NAME"])
			->where("MB.BGROUP_ID IN (?)", $savegroup);

		$getbuser->where("MB.BUSER_BRANCH = ?", "26");

		$getbuser->where("MB.BUSER_NAME LIKE ?", "%" . "KRI" . "%");

		$getbuser = $getbuser->query()->fetchAll();

		var_dump($getbuser);
		die();
	}

	private function cekProductType($acct)
	{
		$getProductType = $this->_db->select()
			->from('M_PRODUCT')
			->query()->fetchAll();

		$serviceCall = new Service_Account($acct, 'IDR');
		$getBalanceNotDeposito = $serviceCall->inquiryAccountBalance();
		$getBalanceDeposito = $serviceCall->inquiryDeposito();

		$productTypeAcct = ($getBalanceDeposito['response_code'] == '0000' || $getBalanceDeposito['response_code'] == '00') ? $getBalanceDeposito['product_type'] : $getBalanceNotDeposito['product_type'];

		$cekArray = array_filter($getProductType, function ($array) use ($productTypeAcct) {
			return $productTypeAcct == $array['PRODUCT_PLAN'];
		});

		$result = array_shift($cekArray);

		if (!$result) return [false, $productTypeAcct];

		return [true, $productTypeAcct];
	}
}
