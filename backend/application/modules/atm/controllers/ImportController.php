<?php

require_once 'Zend/Controller/Action.php';

class Atm_ImportController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$attahmentDestination 	= UPLOAD_PATH.'/document/temp';
		$adapter 				= new Zend_File_Transfer_Adapter_Http();

		$field = array(
			'ATM_CODE' 		=> 25,
			'ATM_ADDRESS' 		=> 140,
		    'ATM_CITY' 		=> 50,
		);

		$this->view->field = $field;
		if($this->_request->isPost())
		{
			$params = $this->_request->getParams();
			$adapter->setDestination ($attahmentDestination);
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
			$extensionValidator->setMessage(
				'Error: Extension file must be *.txt'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
			);

			$adapter->setValidators(array($extensionValidator,$sizeValidator));

			if($adapter->isValid())
			{

				$success = 1;
				$sourceFileName = $adapter->getFileName();
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				$adapter->addFilter ( 'Rename',$newFileName);
				$adapter->receive();
				//Zend_Debug::dump($newFileName);die;
				$myFile = file_get_contents($newFileName);
				$arry_myFile = explode("\n", $myFile);
				@unlink($newFileName);
				//Zend_Debug::dump($arry_myFile);die;
				if($adapter->receive())
				{
					$this->_db->beginTransaction();
					/*$master = $this->_db->FetchRow(
						$this->_db->select()
							->from('M_DOMESTIC_BANK_TABLE')
							->where('BANK_ISMASTER = 1')
					);*/

					$this->_db->query('TRUNCATE TABLE M_ATM');

					/*if(!empty($master))
					{
						unset($master['BANK_ID']);
						$this->_db->insert("M_DOMESTIC_BANK_TABLE",$master);
					}*/

					foreach ($arry_myFile as $row)
					{

						$start = 0;
						foreach($field as $key=>$length)
						{
							$var[$key] = trim(str_replace("'","\'",substr($row,$start,$length)));
							$start += $length;
						}

						//$var['created'] = date('Y-m-d H:i:s');
						//$var['createdby'] = $this->_userIdLogin;

						try
						{
							if( !empty($var['ATM_CODE']) )
							{
								// echo '<pre>';
								// print_r($var);die;
								$this->_db->insert("M_ATM",$var);
							}
						}
						catch(Exception $e)
						{
							$success = 0;
						}
					}
				}
				if($success == 1)
				{
					$this->_db->commit();
					Application_Helper_General::writeLog('COIM','Import ATM List');
					$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
					$this->_redirect('/notification/success/index');
				}else{
					$this->_db->rollBack();
				}
			}
			else
			{
				$this->view->errorMsg = $adapter->getMessages();
				//Zend_Debug::dump($adapter->getMessages());die;
			}
		}
		Application_Helper_General::writeLog('COIM','Import ATM List');
	}
}