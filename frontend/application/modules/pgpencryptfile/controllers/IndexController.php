<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/Payment.php';

class pgpencryptfile_IndexController extends Application_Main
{
    protected $_moduleDB = 'RTF'; //masih harus diganti

    protected $_destinationUploadDir = '';
    protected $_maxRow = '';

    public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

    public function indexAction()
    {
        $this->setbackURL();
		$this->_helper->_layout->setLayout('newlayout');
        $settingObj = new Settings();
        //$this->view->THRESHOLD_LLD      = $settingObj->getSetting("threshold_lld"   , 0);

        $this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        $AccArr       = $CustomerUser->getAccounts();
        $this->view->AccArr =  $AccArr;
        $listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');

        if($this->_request->isPost() )
        {
            $this->_request->getParams();
$data=                      $this->_request->getParams();
// print_r($data);die;


            $filter = new Application_Filtering();
            $confirm = false;
            $error_msg[0] = "";

            $PS_SUBJECT     = $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
            
                $paramSettingID = array('range_futuredate', 'auto_release_payment');

                $settings = new Application_Settings();
                $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
                $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));
// die;
                if ($adapter->isValid ())
                {
// die;
                    $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                    $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                    $adapter->addFilter ( 'Rename',$newFileName  );

                    if ($adapter->receive ())
                    {
                        //PARSING CSV HERE
                        $csvData = $this->parseCSV($newFileName);
                        $csvData2 = $this->parseCSV($newFileName);
                        //after parse delete document temporary
                        // Zend_Debug::dump($csvData);die;
                        @unlink($newFileName);
                        //end

                        $totalRecords2 = count($csvData2);

                        if($totalRecords2)
                            {
                                for ($a= 1; $a<$totalRecords2; $a++ ){
                                    unset($csvData2[$a]);
                                }
//                              unset($csvData2[1]);
//                              unset($csvData2[2]);
//                              unset($csvData2[3]);
                                $totalRecords2 = count($csvData2);
                        }


                        foreach ( $csvData2 as $row )
                        {
                            $type =  trim($row[0]);
                        }
//                      print_r($type);d

                        // if (strtoupper(trim($type)) == 'PAYROLL'){
//                          echo 'berhasil';
//  die;

                            $totalRecords = count($csvData);
                            // if($totalRecords)
                            // {
                            //     unset($csvData[0]);
                            //     unset($csvData[1]);
                            //     $totalRecords = count($csvData);
                            // }
                            // Zend_Debug::dump($csvData);die;
                            if($totalRecords)
                            {
                                if($totalRecords <= $this->_maxRow)
                                {
                                    $rowNum = 0;

                                    // print_r($params);die;
                                    $params = $csvData;
                
                                            $content['payment'] = $params;
                                            
                                            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                                            $sessionNamespace->content = $content;
                                            $this->_redirect('/adapter/index/confirm');
                                        
                                }
                                // kalo jumlah trx lebih dari setting
                               

                                // kalo gak ada error
                                
                            }
                            else //kalo total record = 0
                            {
//                              echo 'here';die;
                                //$error_msg[0] = 'Error: Wrong File Format. There is no data on csv File.';
                                $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
                                $this->view->error      = true;
                                $this->view->report_msg = $this->displayError($error_msg);
                            }
                        // }

                    // }

                }
                else
                {
                    $this->view->error = true;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                        if($key=='fileUploadErrorNoFile')
                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                        else
                            $error_msg[0] = $val;
                        break;
                    }
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;
                }
            }

            /*if($confirm)
            {
                
                
                $content['payment'] = $params;
                // echo '<pre>';
                // print_r($payment);die;
                $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
                $sessionNamespace->content = $content;
                $this->_redirect('/adapter/index/confirm');
            }*/

            $this->view->PSSUBJECT = $PS_SUBJECT;
            $this->view->ACCTSRC = $ACCTSRC;
            $this->view->PSEFDATE = $PS_EFDATE;
        }
        Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
        Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
    }

    public function confirmAction()
    {
        

    }

    public function wherecolumnAction()
    {
        
    }

    private function parseCSV($fileName){
        $csvData = false;
        try {
                $Csv = new Application_Csv (  $fileName, $separator = "," );
                $csvData = $Csv->readAll ();
            } catch ( Exception $e ) {
                echo nl2br ( $e->getTraceAsString () );
            }
            return $csvData;
    }
}
