<?php
require_once 'CMD/SinglePayment.php';
require_once 'General/Settings.php';
//class Testcase_IndexController extends Application_Main
class Testcase_IndexController extends Zend_Controller_Action
{
	protected $_paymentRef = '2012030488983154';
	protected $_custId = 'YDS';
	protected $_userId = 'SUPER1';
	public function initController(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function init(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	function inverse($x) {
		if ( ! $x) {
			throw new Exception('Division by zero.');
		}
		return 1/$x;
	}
	public function inverseAction()
	{
		try {
			echo $this->inverse(0) . "\n";
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		} catch (MyException $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		} /* finally {
			echo "First finally.\n";
		} */ 
	}
	public function varexportAction(){
	    // static public function groupArray($data,$optionValue)
	// {
	 $data = array(
							0=> array('test'=> 'a','b','c','d'),
							1=> array('test'=> 'b','b','c','d'),
							2=> array('test'=> 'test','b','c','d'),
						);
		$arrayListJob = array();
		foreach ($data as $key=>$value) 
		{
			$arrayListJob[$value['test']] = $data[$key];
		}
		var_dump( $arrayListJob );
	// }
		/* $data = array(1,2,3,4,5);
		// $a = var_export($data);
		eval('$b = '.var_export($data).';');
		// echo $a;
		// echo $a;
		// echo var_export($a);
		// echo '$b = '.$a.';';
		var_dump($b);
		die; */
	}
	
	public function test2Action(){
	var_dump(get_current_user());
		Zend_Debug::dump($_SERVER);
		die;
	}
	
	public function testtestAction(){
		require_once 'Service/ServerData.php';
		require_once 'Service/ServiceObj.php';

		$channelHeader = new SGOChannelHeader();
		$channelHeader->channelID 	= 'FSCM';		// CMD / FSCM / EMoney
		$channelHeader->messageID 	= strtoupper(Application_Helper_General::get_rand_id(32));		// unique ID (optional)
		$channelHeader->referenceID = 'SGO006';		// cust id or user id, or both
		
		$wsdl 	= $this->_getParam('wsdl');
		ini_set ( "soap.wsdl_cache_enabled", 0 );
	    if($wsdl == 1) 
	    {
			$autodiscover = new Zend_Soap_AutoDiscover();
		    $autodiscover->setClass('ServerData');
		    $autodiscover->handle();
		} 
		else{
			// get Date
			$reqData =  new SGOGetHolidayInfoReqData();
			$reqData->year = '2014';
	//    	$reqData->userID = 'ANGEL01'.'@HOST'.$_SERVER['REMOTE_ADDR'];
			// $reqData->userID = 'ANGEL01';
			// $reqData->pass   = '12345';

			$reqObj 							= new SGOGetHolidayInfoReq();
			$reqObj->SGOChannelHeader 			= $channelHeader;
			$reqObj->SGOGetHolidayInfoReqData 	= $reqData;
			
			try
			{
				$ServerUser = new ServerData();
				$result = $ServerUser->getHolidayInfo($reqObj);
			}
			catch(Exception $e) {
				echo "Error: ".$e->getMessage();
			}
			
			echo "<pre>";
			print_r($reqObj);
			print_r($result);
		}
	}
	
	public function tesaAction(){
		require_once 'Service/ServerUser.php';
		require_once 'Service/ServiceObj.php';
		
		$channelHeader = new SGOChannelHeader();
		$channelHeader->channelID 	= 'CMD';		// CMD / FSCM / EMoney
		$channelHeader->messageID 	= strtoupper(Application_Helper_General::get_rand_id(32));		// unique ID (optional)
		$channelHeader->referenceID = 'SGO006';		// cust id or user id, or both
		
		// Authen user
		$reqData =  new SGOAuthenticateUserReqData();
    	$reqData->custID = 'SDD';
//    	$reqData->userID = 'ANGEL01'.'@HOST'.$_SERVER['REMOTE_ADDR'];
    	$reqData->userID = 'ANGEL01';
    	$reqData->pass   = '12345';
    	
    	$reqObj 							= new SGOAuthenticateUserReq();
    	$reqObj->SGOChannelHeader 			= $channelHeader;
    	$reqObj->SGOAuthenticateUserReqData = $reqData;
		
    	try
		{
	    	$ServerUser = new ServerUser();
			$result = $ServerUser->authenticateUser($reqObj);
		}
		catch(Exception $e) {
			echo "Error: ".$e->getMessage();
		}
		
		echo "<pre>";
		print_r($reqObj);
		print_r($result);
	}
	
	public function soapclientAction(){
		require_once 'Service/ServiceObj.php';
		
		$channelHeader = new SGOChannelHeader();
		$channelHeader->channelID 	= 'CMD';		// CMD / FSCM / EMoney
		$channelHeader->messageID 	= strtoupper(Application_Helper_General::get_rand_id(32));		// unique ID (optional)
		$channelHeader->referenceID = 'SGO006';		// cust id or user id, or both
		
		
//		$reqData =  new SGOGetCustInfoReqData();
//    	$reqData->custID = 'PERSONAL';
//    	
//    	$reqObj 							= new SGOGetCustInfoReq();
//    	$reqObj->SGOChannelHeader 			= $channelHeader;
//    	$reqObj->SGOGetCustInfoReqData = $reqData;
//		
//    	try {
//		$SoapClient 	= new SGO_Soap_ClientApp();
//		$returnService 	= $SoapClient->call('getCustInfo', $reqObj);
//		
//		$result  = $SoapClient->getResult();
//	   	$rawRequest  = $SoapClient->getRequest();
//    	} catch(Exception $e) {
//	   		echo $e->getMessage();
//	   	}
		
		// Authen user
		$reqData =  new SGOAuthenticateUserReqData();
    	$reqData->custID = 'PERSONAL';
//    	$reqData->userID = 'ANGEL01'.'@HOST'.$_SERVER['REMOTE_ADDR'];
    	$reqData->userID = 'ANGEL01';
    	$reqData->pass   = '12345';
    	
    	$reqObj 							= new SGOAuthenticateUserReq();
    	$reqObj->SGOChannelHeader 			= $channelHeader;
    	$reqObj->SGOAuthenticateUserReqData = $reqData;
		
    	try {
		$SoapClient 	= new SGO_Soap_ClientApp();
		$returnService 	= $SoapClient->call('authenticateUser', $reqObj);
		
		$result  = $SoapClient->getResult();
	   	$rawRequest  = $SoapClient->getRequest();
    	} catch(Exception $e) {
	   		echo $e->getMessage();
	   	}
		
		echo "<pre>";
		print_r($rawRequest);
		print_r($result);
	}
	
	public function unserializeAction(){
		$msg = 'O:8:"stdClass":2:{s:16:"SGOChannelHeader";O:8:"stdClass":3:{s:9:"channelID";s:3:"CMD";s:9:"messageID";s:32:"CPTS1XUYJ2528OEKLC5OB2TIZD3RNWF2";s:11:"referenceID";s:6:"SGO006";}s:26:"SGOAuthenticateUserReqData";O:8:"stdClass":3:{s:6:"custID";s:6:"SGO006";s:6:"userID";s:7:"ANGEL01";s:4:"pass";s:5:"12345";}}';
	
		$qwe = unserialize($msg);
		echo "<pre>";
		print_r($qwe);
	}
	
	public function soapserverAction(){
		$SoapServer 	= new SGO_Soap_Server();
		
		$wsdl 	= $this->_getParam('wsdl');
		$wsdl	= ($wsdl == 1)? true: false;
		
		try {
			$SoapServer->call('ServerUser', $wsdl);
		} catch(Exception $e) {
			echo "Error :".$e->getMessage();
		}
	}
	
	public function logAction(){
		$paramLog = array();
		$paramLog['LOG_REQ'] 			= new Zend_Db_Expr('now()');
		$paramLog['LOG_RESP'] 			= new Zend_Db_Expr('now()');
			
		Application_Helper_General::insertInterfaceLog($paramLog);
	}
	
    public function createwithinAction(){
    		$param['CUST_ID'] =$this->_custId;
			$param['USER_LOGIN'] = $this->_userId;
  			$param['SOURCE_ACCOUNT'] = '2000184756';
  			$param['SOURCE_ACCOUNT_CCY'] = 'IDR';
  			$param['PS_EFDATE'] = date("Y-m-d");
  			$param['PS_TOTAL_AMOUNT'] = '150000';
  			$param['PS_SUBJECT'] = 'Payment Subject';
  			$param['FEATURE_ID'] = "";
  			$param['BENEFICIARY_ID'] = "";
  			$param['BENEFICIARY_ACCOUNT'] = '2000456456';
  			$param['BENEFICIARY_ACCOUNT_CCY'] = 'IDR';
 			$param['BENEFICIARY_ALIAS_NAME'] = 'Yendra lianto';
  			$param['MESSAGE'] = 'MESSAGE TRF';
  			$param['REFNO'] = 'ADD MESSAGE TRF';
			$param['_addBeneficiary'] = true;
			$param['_isBeneLinkage'] = false;
			$SinglePayment = new SinglePayment();
			$SinglePayment->createPaymentWithin($param);
    }

    public function rejectpaymentAction(){
    	$paymentRef = $this->_paymentRef;
		$custId = 'yds';
		$userLogin  = 'RAMLAN01';
    	$SinglePayment = new SinglePayment($paymentRef,$custId,$userLogin);
    	var_dump($SinglePayment->rejectPayment("MANUAL REJECT"));
    }


	public function approvepaymentAction() 
	{
		$paymentRef = $this->_paymentRef;
		$custId = $this->_custId;
		$userLogin  = $this->_userId;
    	$SinglePayment = new SinglePayment($paymentRef,$custId,$userLogin);
    	var_dump($SinglePayment->approvePayment());
	}
	
	public function suggestAction(){
		$account  = '2000184756';
		$custId = 'YDS';
		$info = 'Customer ID = '.$custId;	
		$this->_db->beginTransaction();
	     // insert ke T_GLOBAL_CHANGES
	     $change_id = $this->suggestionWaitingApproval('Set System Balance',$info,$this->_changeType['code']['edit'],null,'M_CUSTOMER_ACCT','TEMP_SET_SYSTEMBALANCE','CUST_ID',$zf_filter_input->cust_id);

     	$data = array(	'CHANGES_ID' => $change_id,
							'ACCT_NO' => $account,
							'CUST_ID' => $custId,
							'ACCT_NAME' => "Ramlan Gustian",
							'ISSYSTEMBALANCE' => 1,																				
							'PLAFOND' => 100000,
							'ISPLAFONDDATE' => 0,
							'PLAFOND_START' => NULL,
							'PLAFOND_END' => NULL,
						);
		$this->_db->insert('TEMP_SET_SYSTEMBALANCE',$data);
	     $this->_db->commit();
	}
	
	
	public function requestRepairAction(){
		
	}
	
	public function fckeditorAction(){
		require_once(APPLICATION_PATH .'/../public/js/fckeditor/fckeditor.php');
		$errimg="";
//		if ($this->errgraph!="") $errimg="<img src=\"".$this->errgraph."\" border=\"0\" align=\"left\">";
//		if ($errormsg!="") $writeerror = "<br><span ".$this->errattr.">$errimg $errormsg</span>";
		
		$temp = "<tr><td valign=\"top\"  ".$this->tblattr."><span class=\"".$this->caption_css."\">$recdesc</span> </td>
				<td valign=\"top\"  ".$this->tblattr.">";
		
		$oFCKeditor = new FCKeditor($recfield);
		$oFCKeditor->BasePath = '/js/fckeditor/';
		$oFCKeditor->Value = "halo";
		$oFCKeditor->Width  = 650 ;
		$oFCKeditor->Height = 500 ;

		$temp.=	$oFCKeditor->CreateHtml().'</td></tr>';
		echo $temp;
	}
	
	public function validateAction(){
		echo "\n<br>";
		$validator =		new Zend_Validate_Regex('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/');
		var_dump($validator->isValid ( '10:10:00' )); // true
		var_dump($validator->isValid('10-10:20')); // true
		var_dump($validator->isValid('abcd')); // true$validator->isValid(1.0); // true$validator->isValid('1.1'); // false$validator->isValid('-0.4'); // false$validator->isValid(1.00001); // false$validator->isValid(0xFF); // false$validator->isValid('foo'); // false
	}
	
	public function privilegeAction(){
		var_dump($this->view->hasPrivilege('BADA'));
	}
	
	public function failedtokenAction(){
		require_once 'General/CustomerUser.php';
		$CustomerUser =  new CustomerUser('SGO006', 'MAKER1');
		var_dump($CustomerUser->setFailedTokenMustLogout());
		var_dump($CustomerUser->getUserFailToken());
	} 

	public function changePasswordAction(){
		require_once 'General/CustomerUser.php';
		$CustomerUser =  new CustomerUser('SGO006', 'MAKER1');
//		var_dump($CustomerUser->changePassword('asdfasd', 'asdfkj34324234'));
//		var_dump($CustomerUser->changePassword('Yanti132534', 'UserPG12345'));
		var_dump($CustomerUser->changePassword('UserPG12345', 'RamlanGU1234'));
		var_dump($CustomerUser->changePassword('RamlanGU1234', 'RamlanGU123'));
		var_dump($CustomerUser->changePassword('RamlanGU123', 'UserPG12345'));
		var_dump($CustomerUser->changePassword('UserPG12345', 'Yanti1234'));
		var_dump($CustomerUser->changePassword('Yanti1234', 'UserPG123456'));
		var_dump($CustomerUser->changePassword('UserPG123456', 'Ramlan1245678'));
		var_dump($CustomerUser->changePassword('Ramlan1245678', 'YantiU1235'));
		var_dump($CustomerUser->changePassword('YantiU1235', 'YantiUR1235'));
		var_dump($CustomerUser->changePassword('YantiUR1235', 'Ramlan662336'));
		var_dump($CustomerUser->changePassword('Ramlan662336', 'Yanti662336'));
		var_dump($CustomerUser->changePassword('Yanti662336', 'Ramlan662339'));
		var_dump($CustomerUser->changePassword('Ramlan662339', 'Yanti132534'));
	} 
	
	public function resetPasswordAction(){
		require_once 'General/CustomerUser.php';
		$CustomerUser =  new CustomerUser('SGO006', 'ELIS2');
		$CustomerUser->requestUnlockResetPassword(1,0);
		$CustomerUser->approveRequestResetPassword();
	} 
	
	public function testsettingAction(){
		require_once 'General/Settings.php';
		$Settings =  new Settings();
		var_dump($Settings->getSetting('auto_release_payment'));
	}
	public function wsdlAction(){
		$zend = new Zend_Soap_AutoDiscover();
		$zend->setClass('Login');
		$zend->handle();
	}
	
	public function wsdlresetpasswordAction(){
		require_once 'General/ForgotPassword.php';
		$zend = new Zend_Soap_AutoDiscover();
		$zend->setClass('ForgotPassword');
		$zend->handle();
		die();
	}
	
	public function documentAction(){
		require_once 'SCM/Validate/ValidateDocument.php';
		$custId = 'SGO006';
		$userId = 'MAKER1';
		$commCode = 'NEST002';
		$validateObj = new SCM_Validate_ValidateDocument($commCode, $custId, $userId);
		
		$arrayParam  =  array(
		  "TKALIL201201010000001" => array(
		    "MEMBER_NAME" =>"TKALI",
		    "MEMBER_CODE" =>"M0003",
		    "DOC_NO" =>"L201201010000001",
		    "DOC_OPTION" => "Item",
		    "DOC_DATE" => "01/01/12",
		    "MATURITY_DATE" => "01/01/12",
//		    "DOC_REF_NO_ID" => "L201206119768112",
		    "DOC_REF_NO_ID" => "L201206119768112;L201206114789827",
		    "DOC_CCY" => "IDR",
			"DOC_TYPE" =>  "I",
			"CUSTOMER_ROLE" =>  'S',
		    "DOC_AMOUNT" => 10000000,
		    "DETAIL" => array(
					1 => array(
						        "ITEM_ID" =>"B001",
						        "ITEM_NAME" =>"BABI GULING",
						        "ITEM_QTY" =>"100",
						        "ITEM_PRICE" =>"100000",
								"rowNum" => 1,
						      ),
					2 => array(
						        "ITEM_ID" =>"B001",
						        "ITEM_NAME" =>"BABI GULING",
						        "ITEM_QTY" =>"100",
						        "ITEM_PRICE" =>"100000",
								"rowNum" => 2,
						      ),
//						      0 => array(
//						        "ITEM_ID" =>"DH01",
//						        "ITEM_NAME" =>"Dancow 1 Honey",
//						        "ITEM_QTY" =>"25",
//						        "ITEM_PRICE" =>"25000",
//						        "rowNum" => 1,
//						      ),
//						      1 => array(
//						        "ITEM_ID" =>"DH01",
//						        "ITEM_NAME" =>"Dancow 1 Honey",
//						        "ITEM_QTY" =>"25",
//						        "ITEM_PRICE" =>"25000",
//						        "rowNum" => 2,
//						      ),
						      
		  				),
		  			),
		);
		$return = $validateObj-> validateAll($arrayParam);
		var_dump($return);
		
		
		
	}
	
	public function monthlyAction(){
		require_once ('Service/TransferCharge.php');
		echo "<pre>";
		
		// IDR 2003000176
		// IDR 2001397423
		// IDR 1003918633
		// USD 2003601155
		
		$param = array();	  
		$param['SOURCE_AMOUNT']   			= 2;  	 // amount in source CCY
     	$param['SOURCE_ACCOUNT_CCY'] 		= '039'; 
     	$param['SOURCE_ACCOUNT']   			= '2003601155'; 		// 2003000176  2003601155 (USD)
     	$param['BENEFICIARY_ACCOUNT']    	= '461001131';  		// not sent 461001196
     	$param['SOURCE_ACCOUNT_NAME'] 		= 'Erica'; 
     
     	echo "param request: <br>"; print_r($param); echo "<hr>";
		$data =  new Service_TransferCharge($param);
		$result = $data->sendTransfer();
		print_r($result);
		
//     	$param['AMOUNT_CCY'] 				= '016'; 
//     	$param['BENEFICIARY_ACCOUNT_CCY'] 	= '016'; 
//     	$param['TRA_AMOUNT']   				= 20000;  	 // amount in source CCY
     
     	
     	
			
		die();
	}
	
	public function integrationAction(){
		
		require_once ('Service/TransferWithinPartner.php');
		require_once ('Service/TransferDomestic.php');
		require_once ('Service/TransferDisbursementWithin.php');
		require_once ('Service/TransferDisbursementDomestic.php');
		require_once ('Service/TransferMulti.php');
		echo "<pre>";
		
		// Disbursement Fee Within Single
//		$CCY 			= "IDR";
//			$PRKACCT 		= "2003500303";
//		$ACCTSRC 		= "2003500320";
//			$ACBENEF 		= "2003500365";  
//		$CHARGESACCT 	= "2003500348";
//		$CHARGESACCT2 	= "2003500334";
//			$sourceAmount1	= 1000;
//		$sourceAmount2	= 500;
//			$amtSCMinterest	= 50;
//		$amtSCMfee		= 30;
//		$amtCPfee		= 20;
//		$amtCPfee2		= 40;
//		$transferType 	= 1;	// 0: PB, 1: RTGS, 2: SKN
//		
//		$setting 		= new Settings();
//		$coaSCMid		= ($CCY == 'IDR')? 'charges_acct_SCMFEE_IDR': 'charges_acct_SCMFEE_VLS';
//		$coaCPid		= ($transferType == '0')? 'charges_acct_IDR': 'charges_acct_DOM';
//		$COA_SCM_INT 	= $setting->getSetting('charges_acct_SCM_INT', 0);
//		$COA_SCM_FEE 	= $setting->getSetting($coaSCMid, 0);
//		$COA_CP_FEE 	= $setting->getSetting($coaCPid, 0);
//		
//		$currNumCCY		= Application_Helper_General::getCurrNum($CCY);
//		
//		// replace XX with LOB
//		$LOB			= Application_Helper_General::getLOB($PRKACCT, $currNumCCY);
//		$COA_SCM_INT 	= str_replace("XX", $LOB, $COA_SCM_INT);
//		$COA_SCM_FEE 	= str_replace("XX", $LOB, $COA_SCM_FEE);
//		$COA_CP_FEE 	= str_replace("XX", $LOB, $COA_CP_FEE);
//
//		$param = array();	  
//		$param['PAYMENT_REF']   			= '20120726000001ASD';
//		$param['TRANSACTION_ID']   			= '20120726000001ASD01';
//		$param['CUST_ID']   				= 'SGO006';
//		$param['USER_ID']   				= 'RELEASE2'; 
//	    $param['SOURCE_ACCOUNT']   			= $PRKACCT;
//	    $param['SOURCE_ACCOUNT_NAME'] 		= "PRK Erica"; 
//	    $param['BENEFICIARY_ACCOUNT'] 		= $ACBENEF; 
//	    $param['BENEFICIARY_ACCOUNT_NAME'] 	= "Ramlan"; 
//	    $param['CHARGES_ACCOUNT']   		= $CHARGESACCT;
//	    $param['AMOUNT_CCY'] 				= $currNumCCY;
//	    $param['SOURCE_AMOUNT'] 			= $sourceAmount1;
//	    $param['MESSAGE'] 					= 'message disb'; 
//	    $param['REF_NO'] 					= 'add message disb'; 
//	    $param['TRANSFER_TYPE'] 			= $transferType; 
//	    $param['COA_SCM_INTEREST'] 			= $COA_SCM_INT; 
//	    $param['COA_SCM_FEE'] 				= $COA_SCM_FEE; 
//	    $param['COA_CP_FEE'] 				= $COA_CP_FEE; 
//	    $param['AMT_SCM_INTEREST'] 			= $amtSCMinterest; 
//	    $param['AMT_SCM_FEE'] 				= $amtSCMfee;
//	    $param['AMT_CP_FEE'] 				= $amtCPfee; 
//	    $param['MEMBER_CODE'] 				= "M012314567";  
//	    $param['DOC_NO'] 					= "I231235567";  
//	    
//	    // utk second trx
//	    $param['SOURCE_ACCOUNT2']   		= $ACCTSRC;
//	    $param['SOURCE_ACCOUNT2_NAME'] 		= "Giro Erica"; 
//	    $param['SOURCE2_AMOUNT'] 			= $sourceAmount2;
//	    $param['CHARGES_ACCOUNT2']   		= $CHARGESACCT2;
//	    $param['AMT_CP_FEE2'] 				= $amtCPfee2;
//	    
//	    $param['BENEFICIARY_CITIZENSHIP'] 	= 'R'; 	// R/NR
//     	$param['BENEFICIARY_BANK_NAME'] 	= 'AGRIS, BDLPG';
//     	$param['CLR_CODE'] 					= '9450017';
//     	$param['SWIFT_CODE'] 				= '';
//		
//		$data 			= new Service_TransferDisbursementDomestic($param);
//		$sendTransfer 	= $data->sendTransfer();
//		
//		echo "<pre>";
//		print_r($sendTransfer);
//		
//		die();
		
		// Disbursement Fee Within Single
//		$CCY 			= "IDR";
//		$PRKACCT 		= "2003500303";
//		$ACCTSRC 		= "2003500320";
//		$ACBENEF 		= "2003500365";  
//		$CHARGESACCT 	= "2003500348";
//		$CHARGESACCT2 	= "2003500334";
//		$sourceAmount1	= 1000;
//		$sourceAmount2	= 500;
//		$amtSCMinterest	= 50;
//		$amtSCMfee		= 30;
//		$amtCPfee		= 20;
//		$amtCPfee2		= 40;
//		$transferType 	= 0;	// 0: PB, 1: RTGS, 2: SKN
//		
//		$setting 		= new Settings();
//		$coaSCMid		= ($CCY == 'IDR')? 'charges_acct_SCMFEE_IDR': 'charges_acct_SCMFEE_VLS';
//		$coaCPid		= ($transferType == '0')? 'charges_acct_IDR': 'charges_acct_DOM';
//		$COA_SCM_INT 	= $setting->getSetting('charges_acct_SCM_INT', 0);
//		$COA_SCM_FEE 	= $setting->getSetting($coaSCMid, 0);
//		$COA_CP_FEE 	= $setting->getSetting($coaCPid, 0);
//		
//		$currNumCCY		= Application_Helper_General::getCurrNum($CCY);
//		
//		// replace XX with LOB
//		$LOB			= Application_Helper_General::getLOB($PRKACCT, $currNumCCY);
//		$COA_SCM_INT 	= str_replace("XX", $LOB, $COA_SCM_INT);
//		$COA_SCM_FEE 	= str_replace("XX", $LOB, $COA_SCM_FEE);
//		$COA_CP_FEE 	= str_replace("XX", $LOB, $COA_CP_FEE);
//
//		$param = array();	  
//		$param['PAYMENT_REF']   			= '20120726000001ASD';
//		$param['TRANSACTION_ID']   			= '20120726000001ASD01';
//		$param['CUST_ID']   				= 'SGO006';
//		$param['USER_ID']   				= 'RELEASE2'; 
//	    $param['SOURCE_ACCOUNT']   			= $PRKACCT;
//	    $param['SOURCE_ACCOUNT_NAME'] 		= "PRK Erica"; 
//	    $param['BENEFICIARY_ACCOUNT'] 		= $ACBENEF; 
//	    $param['BENEFICIARY_ACCOUNT_NAME'] 	= "Ramlan"; 
//	    $param['CHARGES_ACCOUNT']   		= $CHARGESACCT;
//	    $param['AMOUNT_CCY'] 				= $currNumCCY;
//	    $param['SOURCE_AMOUNT'] 			= $sourceAmount1;
//	    $param['MESSAGE'] 					= 'message disb'; 
//	    $param['REF_NO'] 					= 'add message disb'; 
//	    $param['TRANSFER_TYPE'] 			= $transferType; 
//	    $param['COA_SCM_INTEREST'] 			= $COA_SCM_INT; 
//	    $param['COA_SCM_FEE'] 				= $COA_SCM_FEE; 
//	    $param['COA_CP_FEE'] 				= $COA_CP_FEE; 
//	    $param['AMT_SCM_INTEREST'] 			= $amtSCMinterest; 
//	    $param['AMT_SCM_FEE'] 				= $amtSCMfee;
//	    $param['AMT_CP_FEE'] 				= $amtCPfee; 
//	    $param['MEMBER_CODE'] 				= "M012314567";  
//	    $param['DOC_NO'] 					= "I231235567";  
//	    
//	    // utk second trx
//	    $param['SOURCE_ACCOUNT2']   		= $ACCTSRC;
//	    $param['SOURCE_ACCOUNT2_NAME'] 		= "Giro Erica"; 
//	    $param['SOURCE2_AMOUNT'] 			= $sourceAmount2;
//	    $param['CHARGES_ACCOUNT2']   		= $CHARGESACCT2;
//	    $param['AMT_CP_FEE2'] 				= $amtCPfee2;
//		
//		$data 			= new Service_TransferDisbursement($param);
//		$sendTransfer 	= $data->sendTransfer();
//		
//		echo "<pre>";
//		print_r($sendTransfer);
//		
//		die();
		
		// Disbursement Fee scheduler
//		$CCY = "USD";
//		$ACCSRC = "2003500303";
//		$CHARGESACCT = "1003005098";
//		
//		$setting 		= new Settings();
//		$COA_IDR 		= $setting->getSetting("charges_acct_SCMFEE_IDR", 0);
//		$COA_VLS 		= $setting->getSetting("charges_acct_SCMFEE_VLS", 0);
//		
//		$currNumCCY	= Application_Helper_General::getCurrNum($CCY);
//		$realCOA	= ($CCY == 'IDR')? $COA_IDR: $COA_VLS;	// 123XX123
//		
//		$LOB		= Application_Helper_General::getLOB($ACCSRC, $currNumCCY);
//		$realCOA 	= str_replace("XX", $LOB, $realCOA);
//
//		$param = array();	  
//		$param['PAYMENT_REF']   			= '20120726000001ASD';
//		$param['CUST_ID']   				= 'SGO006';
//		$param['USER_ID']   				= 'RELEASE2'; 
//	    $param['CHARGES_ACCOUNT']   		= $ACCSRC;
//	    $param['SOURCE_ACCOUNT']   			= $ACCSRC;
//	    $param['SOURCE_ACCOUNT_CCY'] 		= $currNumCCY; 
//	    $param['SOURCE_ACCOUNT_NAME'] 		= "Susiana"; 
//	    $param['BENEFICIARY_ACCOUNT'] 		= $ACCSRC; 
//	    $param['BENEFICIARY_ACCOUNT_CCY'] 	= $currNumCCY;
//	    $param['BENEFICIARY_ACCOUNT_NAME'] 	= "Susiana"; 
//	    $param['AMOUNT_CCY'] 				= $currNumCCY;
//	    $param['SOURCE_AMOUNT'] 			= 0;
//	    $param['MESSAGE'] 					= ''; 
//	    $param['REF_NO'] 					= ''; 
//	    $param['TRANSFER_TYPE'] 			= 'PB'; 
//	    $param['COA_SCM_INTEREST'] 			= $realCOA; 
//	    $param['COA_SCM_FEE'] 				= $realCOA; 
//	    $param['COA_CP_FEE'] 				= $realCOA; 
//	    $param['AMT_SCM_INTEREST'] 			= 0; 
//	    $param['AMT_SCM_FEE'] 				= 2; 	// TODO: RECALCULATE AMOUNT??
//	    $param['AMT_CP_FEE'] 				= 0; 
//	    $param['AMT_CP_FEE_BI'] 			= 0; 
//	    $param['MEMBER_CODE'] 				= "M012314";  
//	    $param['DOC_NO'] 					= "I23123";  
//		
//		$data 			= new Service_TransferDisbursement($param);
//		$sendTransfer 	= $data->sendTransfer();
//		
//		echo "<pre>";
//		print_r($sendTransfer);
		
//		die();
		
		// Domestic
//		$param = array();	    
//    	$param['TRA_AMOUNT']    	= 1500; 	 // amount in payment CCY  
//     	$param['SOURCE_AMOUNT']   	= 1500;  	 // amount in source CCY
//     	$param['TRX_FEE']     		= 1000;   // amount fee
//      
//     	$param['SOURCE_ACCOUNT']   		= '1003005098'; 		// 1003005098  2003500303
//     	$param['BENEFICIARY_ACCOUNT']  	= '999888777555'; 
//     	$param['COA_ACCOUNT']    		= '460501001';  // not sent 461001131  460501001
//     
//    	$param['PAYMENT_REF'] 				= '20120724000001ASD'; 
//     	$param['CUST_ID'] 					= 'SGO006'; 
//     	$param['USER_ID'] 					= 'MAKER1'; 
//     	$param['AMOUNT_CCY'] 				= '016';	// 039
//     	$param['SOURCE_ACCOUNT_CCY'] 		= '016'; 
//     	$param['BENEFICIARY_ACCOUNT_CCY'] 	= '016'; 
////     	$param['CHARGES_ACCOUNT_CCY'] = '039';
//     
//     	$param['SOURCE_ACCOUNT_NAME'] 		= 'Erica'; 
//     	$param['BENEFICIARY_ACCOUNT_NAME'] 	= 'Susiana'; 
//    	$param['MESSAGE']	 				= 'desc1     bla bla bla                desc2 desc412123123123123123123'; 
//     	$param['REF_NO'] 					= 'desc refno 12345 bla bla bla'; 
//     	$param['TRANSFER_TYPE'] 			= 2; // 1.RTGS  2. SKN
//     	$param['TRANSACTION_ID'] 			= '20120717000005ASD12'; 
//     	$param['BENEFICIARY_CITIZENSHIP'] 	= 'R'; 	// R/NR
//     	$param['BENEFICIARY_BANK_NAME'] 	= 'AGRIS, BDLPG';
//     	$param['CLR_CODE'] 					= '9450017';
//     	$param['SWIFT_CODE'] 				= '';
//     	
//     	echo "param request: <br>"; print_r($param); echo "<hr>";
//		$data =  new Service_TransferDomestic($param);
//		$result = $data->sendTransfer();
//		print_r($result);
//		
//		die();
		
//		// Within
//		$param = array();
//	    
//    	$param['TOTAL_AMOUNT']    = 2; // amount in payment CCY  
//     	$param['SOURCE_AMOUNT']   = 2;  // amount in source CCY
//     	$param['TRX_FEE']     	  = 1;   // amount fee
//     
//     	$param['SOURCE_ACCOUNT']   		= '2003601155'; 
//     	$param['BENEFICIARY_ACCOUNT']  	= '2003601155'; 
//     
//    	$param['PAYMENT_REF'] 				= '20120717000004ASD'; 
//     	$param['CUST_ID'] 					= 'SGO006'; 
//     	$param['USER_ID'] 					= 'MAKER1'; 
//     	$param['COA_ACCOUNT'] 				= '461001131'; // 461001186
//     	$param['AMOUNT_CCY'] 				= '039';
//     	$param['SOURCE_ACCOUNT_CCY'] 		= '039'; 
//     	$param['BENEFICIARY_ACCOUNT_CCY'] 	= '039'; 
//     
//     	$param['SOURCE_ACCOUNT_NAME'] 		= 'Erica'; 
//     	$param['BENEFICIARY_ACCOUNT_NAME'] 	= 'Susiana'; 
//    	$param['MESSAGE'] 					= 'desc1                      desc2 desc412123123123123123123'; 
//     	$param['REF_NO'] 					= 'refno1'; 
//		$data =  new Service_TransferWithin($param);
//		$result = $data->sendTransfer();
//		print_r($result);
//		
//		die();

		// Within Partner
//		$param = array();
//    	$param['CONTENT_TYPE'] 			= 1; 		// 1: TBR DATA + FEE, 2: TBR DATA, 3: FEE
//		$param['TOTAL_AMOUNT'] 			= 8000;		// amount in payment CCY  
//    	$param['SOURCE_AMOUNT'] 		= 8000; 	// amount in source CCY
//    	$param['TRX_FEE'] 				= 2000;  	// amount fee
//    	
//    	$param['SOURCE_ACCOUNT'] 		= '2003000176'; 
//    	$param['BENEFICIARY_ACCOUNT'] 	= '2001397423'; 
//    	$param['COA_ACCOUNT'] 			= '461001131';		// not sent 
//    	$param['CHARGES_ACCOUNT'] 		= '1003913333'; 	//   2003112890	1003918633  1003918633
//    	
//    	$param['PAYMENT_REF'] = '20120717000002ASD'; 
//    	$param['CUST_ID'] = 'SGO006'; 
//    	$param['USER_ID'] = 'MAKER1'; 
//    	$param['AMOUNT_CCY'] = '016';
//    	$param['SOURCE_ACCOUNT_CCY'] = '016'; 
//    	$param['BENEFICIARY_ACCOUNT_CCY'] = '016'; 
//    	$param['CHARGES_ACCOUNT_CCY'] = '016';
//    	
//    	$param['SOURCE_ACCOUNT_NAME'] = 'Erica'; 
//    	$param['BENEFICIARY_ACCOUNT_NAME'] = 'Susiana'; 
//    	$param['MESSAGE'] = 'desc1                      desc2 desc412123123123123123123'; 
//    	$param['REF_NO'] = 'refno1'; 
//    	$param['FEATURE_ID'] = 'NYK'; 
//		$data =  new Service_TransferWithinPartner($param);
//		$result = $data->sendTransfer();
//		print_r($result);
//		
//		die();
		
//		echo '<pre>';
//		require_once ('General/ExchangeRate.php');
//		$rate		= new ExchangeRate();
//		$result		= $rate->buyRate('AUD');
//		echo "<hr>";print_r($result);
//		$result		= $rate->sellRate('AUD');
//		echo "<hr>";print_r($result);
//		die();
//		$data =  new Service_Inquiry();
//		$result = $data->getExchangeRate("USD");	

		// multi
		echo "<pre>";
		$param = array();
	    
		for($a=1; $a<=2; $a++)
		{
//	    	$param[$a]['TOTAL_AMOUNT']    = 2000; // amount in payment CCY  
//	     	$param[$a]['SOURCE_AMOUNT']   = 2000;  // amount in source CCY
//	     	$param[$a]['TRX_FEE']     	  = 100;   // amount fee
//	     
//	     	$param[$a]['SOURCE_ACCOUNT']   		= '2003500303'; 
//	     	$param[$a]['BENEFICIARY_ACCOUNT']  	= '2003500320'; 
//	     
//	    	$param[$a]['PAYMENT_REF'] 				= '20120721000004ASF'; 
//	    	$param[$a]['TRANSACTION_ID'] 			= '20120721000004ASF0'.$a; 
//	     	$param[$a]['CUST_ID'] 					= 'SGO006'; 
//	     	$param[$a]['USER_ID'] 					= 'MAKER1'; 
//	     	$param[$a]['COA_ACCOUNT'] 				= '461001131'; // 461001186
//	     	$param[$a]['AMOUNT_CCY'] 				= '016';
//	     	$param[$a]['SOURCE_ACCOUNT_CCY'] 		= '016'; 
//	     	$param[$a]['BENEFICIARY_ACCOUNT_CCY'] 	= '016'; 
//	     
//	     	$param[$a]['SOURCE_ACCOUNT_NAME'] 		= 'Ramlan'; 
//	     	$param[$a]['BENEFICIARY_ACCOUNT_NAME'] 	= 'Freddy'; 
//	    	$param[$a]['MESSAGE'] 					= 'desc1                      desc2 desc412123123123123123123'; 
//	     	$param[$a]['REF_NO'] 					= 'refno1'; 
//	     	$param[$a]['TRANSFER_TYPE'] 			= '0'; 
	     	
	     	$param['TRA_AMOUNT']    	= 1500; 	 // amount in payment CCY  
     	$param['SOURCE_AMOUNT']   	= 1500;  	 // amount in source CCY
     	$param['TRX_FEE']     		= 1000;   // amount fee
      
     	$param['SOURCE_ACCOUNT']   		= '1003005098'; 		// 1003005098  2003500303
     	$param['BENEFICIARY_ACCOUNT']  	= '999888777555'; 
     	$param['COA_ACCOUNT']    		= '460501001';  // not sent 461001131  460501001
     
    	$param['PAYMENT_REF'] 				= '20120724000001ASD'; 
     	$param['CUST_ID'] 					= 'SGO006'; 
     	$param['USER_ID'] 					= 'MAKER1'; 
     	$param['AMOUNT_CCY'] 				= '016';	// 039
     	$param['SOURCE_ACCOUNT_CCY'] 		= '016'; 
     	$param['BENEFICIARY_ACCOUNT_CCY'] 	= '016'; 
//     	$param['CHARGES_ACCOUNT_CCY'] = '039';
     
     	$param['SOURCE_ACCOUNT_NAME'] 		= 'Erica'; 
     	$param['BENEFICIARY_ACCOUNT_NAME'] 	= 'Susiana'; 
    	$param['MESSAGE']	 				= 'desc1     bla bla bla                desc2 desc412123123123123123123'; 
     	$param['REF_NO'] 					= 'desc refno 12345 bla bla bla'; 
     	$param['TRANSFER_TYPE'] 			= 2; // 1.RTGS  2. SKN
     	$param['TRANSACTION_ID'] 			= '20120717000005ASD12'.$a; 
     	$param['BENEFICIARY_CITIZENSHIP'] 	= 'R'; 	// R/NR
     	$param['BENEFICIARY_BANK_NAME'] 	= 'AGRIS, BDLPG';
     	$param['CLR_CODE'] 					= '9450017';
     	$param['SWIFT_CODE'] 				= '';

     	$params[$a] =  $param;
	     	
		}
		
		$data =  new Service_TransferMulti($params);
		$result = $data->sendTransfer();
		print_r($result);
		
		die();
		
		// ADD TOKEN
		$param = array();
		$param['TokenSerial'] 	= '3128959625';
		$param['UserName'] 		= 'RELEASE';
		
		$param['CustAddress'] 	= 'JL. BULEVAR EROPA 18 PIAZZA EROPA LIPPO KARAWACI';
		$param['CustCity'] 		= 'JAKARTA PUSAT';
		$param['CustName'] 		= 'SQUARE GATE ONE';
		$param['CustZipCode'] 	= '10350';
		$data =  new Service_Token('SGO006', 'RELEASE2');
		
//		$param['TokenSerial'] 	= '3107073502';
//		$param['UserName'] 		= 'SONY PO';
//		$param['CustAddress'] 	= 'GTS CASH MANAGEMENT PLASA BII TOWER 1 LT. 11 JL. MH THAMRIN NO.51 KAV. 22 JAKARTA PUSAT';
//		$param['CustCity'] 		= '12345678901234567890';
//		$param['CustName'] 		= 'BII COOLPAY';
//		$param['CustZipCode'] 	= '15811';
//		$data =  new Service_Token('BCL400', 'TIEPALU01');
		$result = $data->addPairToken($param);	
		
		// Invalid Token Serial
//		Array
//(
//    [ResponseCode] => T2
//    [ResponseDesc] => 9998 Admin Error(addCorporateUserwithChannel): Customer Additional ID cannot be null
//)

//		 REMOVE TOKEN
//		$data =  new Service_Token('SGO006', 'RELEASE2');
//		$result = $data->removePairToken();	



		echo "<pre>";
		print_r($result);
		
		die();
		
//		$data =  new Service_Account('2003115103', '');
//		$data =  new Service_Account('2001074672', '');
//		$result = $data->accountInquiry();		
//		var_dump($result);

//		$data =  new Service_Inquiry();
//		$result = $data->getExchangeRate();		
//		var_dump($result);
	}
	
	
	public function fillaccountAction(){
		$this->_db =  Zend_Db_Table::getDefaultAdapter();
		require_once ('Service/Account.php');
		$string = "
		'AJENG01',
		'AJENG02'
		";
				 $data = $this->_db->fetchAll("SELECT * FROM M_CUSTOMER_ACCT WHERE CUST_ID IN($string)");
				 if(is_array($data)){
				 	foreach ($data as $row) {
				 		$currNum = Application_Helper_General::getCurrNum($row['CCY_ID']);
				 		$service = new Service_Account($row['ACCT_NO'],$currNum);
						$result = $service->accountInquiry();
						
					 	$first_digit = substr($row['ACCT_NO'],0,1);
					    if($first_digit == '1')  
					    {
					        $acct_source = 3;    //3 = saving  
					        $acct_desc   = 'SAVING';
					    }
					    else
					    {
					         if($result['ProductType'] == '070')      
					         {
					             $acct_source = 1;     //1 = prk scm 
					             $acct_desc   = 'PRK FSCM';
					         }
					         else if($result['ProductType'] == '020') 
					         {
					         	if($result['Plafond'] == 0)       
					            {
					               $acct_source = 2;    //2 = prk non scm atau giro
					               $acct_desc   = 'GIRO';
					            }
					            else if($result['Plafond'] > 0)   
					            {
					               $acct_source = 2;    //2 = prk non scm atau giro
					               $acct_desc   = 'PRK';
					            }
					         }
					         else
					         {
					            if($result['Plafond'] == 0)       
					            {
					               $acct_source = 2;    //2 = prk non scm atau giro
					               $acct_desc   = 'GIRO';
					            }
					            else if($result['Plafond'] > 0)   
					            {
					               $acct_source = 2;    //2 = prk non scm atau giro
					               $acct_desc   = 'PRK';
					            }
					         }
						}
		
						$this->_db->update('M_CUSTOMER_ACCT',array(
																	'ACCT_DESC'=>$acct_desc,
																	'ACCT_SOURCE'=>$acct_source,
																	'ACCT_TYPE'=>$result['ProductType'],
														),array('ACCT_NO = ? '=>$row['ACCT_NO'] ));
						
				 	}
				 }
	}
	
	public function verifytokenAction()
	{
		//		 VERIFY
		$data =  new Service_Token('SGOER01', 'ERICA05');
		$result = $data->verify('22334455', '79144411');	
		
		echo "<pre>";
		print_r($result);
	}
	
	public function inquiryAction(){
		
		// IDR 2003000176
		// IDR 2001397423
		// IDR 1003918633
		// USD 2003601155
		
//		1003005098 IDR
//		1003005108 IDR
//		1003005125 IDR
//		1003005173
//		1003005190
//		1003005200
//		1003005231
//		1003005406
//		1003005454
//		1003005509
		$CCY 	= $_GET['ccy'];
		$ACCSRC = $_GET['acc'];
		
		echo "<pre>";
		echo "CCY: $CCY <BR>ACCOUNT: $ACCSRC";
		$currNum = Application_Helper_General::getCurrNum($CCY);
		
		$data =  new Service_Account($ACCSRC, $currNum);	
		$result = $data->accountInquiry();	
		
		echo "<br>"; print_r($result);
	}
	
	public function unpairtokenAction()
	{
		require_once ('Service/Token.php');
		$COMP 	= $_GET['comp'];
		$USER 	= $_GET['user'];
		
		//REMOVE TOKEN
		$data =  new Service_Token($COMP, $USER);
		$result = $data->removePairToken();	
		echo "<pre>";
		print_r($result);
	}
	
	public function lobAction()
	{
		require_once ('Service/Account.php');
		$CCY 	= $_GET['ccy'];
		$ACCSRC = $_GET['acc'];
		
		echo "<pre>";
		echo "CCY: $CCY <BR>ACCOUNT: $ACCSRC";
		$currNum = Application_Helper_General::getCurrNum($CCY);
		
		$data =  new Service_Account($ACCSRC, $currNum);
		$result = $data->getLOB();	
		
		echo "<br>"; print_r($result);
	}
	
	public function pairtokenAction()
	{
		require_once ('Service/Token.php');
		$COMP 	= $_GET['comp'];
		$USER 	= $_GET['user'];
		
		// ADD TOKEN
		$param = array();
		$param['TokenSerial'] 	= '3128950110';
		$param['UserName'] 		= 'RELEASE';
		
		$param['CustAddress'] 	= 'JL. BULEVAR EROPA 18 PIAZZA EROPA LIPPO KARAWACI';
		$param['CustCity'] 		= 'JAKARTA PUSAT';
		$param['CustName'] 		= 'SQUARE GATE ONE';
		$param['CustZipCode'] 	= '10350';
		$data =  new Service_Token('SGOER01', 'ERICA05');
		$result = $data->addPairToken($param);	
		echo "<pre>";
		print_r($result);
	}
	
	public function accstatementAction(){
		
		// IDR 2003000176
		// IDR 2001397423
		// IDR 1003918633
		// USD 2003601155
		$CCY 	= $_GET['ccy'];
		$ACCSRC = $_GET['acc'];
		
		echo "<pre>";
		echo "CCY: $CCY <BR>ACCOUNT: $ACCSRC";
		$currNum = Application_Helper_General::getCurrNum($CCY);
		
		$data =  new Service_Account($ACCSRC, $currNum);
		$result = $data->accountStatementToday();	
		
		echo "<br>"; print_r($result);
	}
	
	public function accstatementhistoryAction(){
		
		// IDR 2003000176
		// IDR 2001397423
		// IDR 1003918633
		// USD 2003601155
		$CCY 	= $_GET['ccy'];
		$ACCSRC = $_GET['acc'];
		
		$dateTo   = date('Y-m-d');
		$dateFrom = Application_Helper_General::addDate($dateTo, 0, 1);
		
		
		echo "<pre>";
		echo "CCY: $CCY <BR>ACCOUNT: $ACCSRC";
		$currNum = Application_Helper_General::getCurrNum($CCY);
		
		$data =  new Service_Account($ACCSRC, $currNum);
		$result = $data->accountStatementHistory($dateFrom, $dateTo);	
		
		echo "<br>"; print_r($result);
	}
	
	public function totalcreditAction(){
		
		// IDR 2003000176
		// IDR 2001397423
		// IDR 1003918633
		// USD 2003601155
		$CCY 	= $_GET['ccy'];
		$ACCSRC = $_GET['acc'];
		
		echo "<pre>";
		echo "CCY: $CCY <BR>ACCOUNT: $ACCSRC";
		$currNum = Application_Helper_General::getCurrNum($CCY);

		$data =  new SystemBalance("YDS", $ACCSRC, $currNum);
		$result = $data->getTotalCredit();	
		
		echo "<br>Total Credit: "; print_r($result);
	}
	
	public function disbursewithincombinationAction()
	{
		echo "<br>Disbursement Within Combination<hr>";
		$CCY 			= "IDR";
		$PRKACCT 		= "2003500303";
		$ACCTSRC 		= "2003500320";
		$ACBENEF 		= "2003500365";  
		$CHARGESACCT 	= "2003500348";
		$CHARGESACCT2 	= "2003500334";
		$sourceAmount1	= 1000;
		$sourceAmount2	= 500;
		$amtSCMinterest	= 50;
		$amtSCMfee		= 30;
		$amtCPfee		= 20;
		$amtCPfee2		= 40;
		$transferType 	= 0;	// 0: PB, 1: RTGS, 2: SKN
		
		$setting 		= new Settings();
		$coaSCMid		= ($CCY == 'IDR')? 'charges_acct_SCMFEE_IDR': 'charges_acct_SCMFEE_VLS';
		$coaCPid		= ($transferType == '0')? 'charges_acct_IDR': 'charges_acct_DOM';
		$COA_SCM_INT 	= $setting->getSetting('charges_acct_SCM_INT', 0);
		$COA_SCM_FEE 	= $setting->getSetting($coaSCMid, 0);
		$COA_CP_FEE 	= $setting->getSetting($coaCPid, 0);
		
		$currNumCCY		= Application_Helper_General::getCurrNum($CCY);
		
		// replace XX with LOB
		$LOB			= Application_Helper_General::getLOB($PRKACCT, $currNumCCY);
		$COA_SCM_INT 	= str_replace("XX", $LOB, $COA_SCM_INT);
		$COA_SCM_FEE 	= str_replace("XX", $LOB, $COA_SCM_FEE);
		$COA_CP_FEE 	= str_replace("XX", $LOB, $COA_CP_FEE);

		$param = array();	  
		$param['PAYMENT_REF']   			= '20120731000026ABC';
		$param['TRANSACTION_ID']   			= '20120731000026ABC01';
		$param['CUST_ID']   				= 'SGO006';
		$param['USER_ID']   				= 'RELEASE2'; 
	    $param['SOURCE_ACCOUNT']   			= $PRKACCT;
	    $param['SOURCE_ACCOUNT_NAME'] 		= "PRK Erica"; 
	    $param['BENEFICIARY_ACCOUNT'] 		= $ACBENEF; 
	    $param['BENEFICIARY_ACCOUNT_NAME'] 	= "Ramlan"; 
	    $param['CHARGES_ACCOUNT']   		= $CHARGESACCT;
	    $param['AMOUNT_CCY'] 				= $currNumCCY;
	    $param['SOURCE_AMOUNT'] 			= $sourceAmount1;
	    $param['MESSAGE'] 					= 'message disb'; 
	    $param['REF_NO'] 					= 'add message disb'; 
	    $param['TRANSFER_TYPE'] 			= $transferType; 
	    $param['COA_SCM_INTEREST'] 			= $COA_SCM_INT; 
	    $param['COA_SCM_FEE'] 				= $COA_SCM_FEE; 
	    $param['COA_CP_FEE'] 				= $COA_CP_FEE; 
	    $param['AMT_SCM_INTEREST'] 			= $amtSCMinterest; 
	    $param['AMT_SCM_FEE'] 				= $amtSCMfee;
	    $param['AMT_CP_FEE'] 				= $amtCPfee; 
	    $param['MEMBER_CODE'] 				= "M012314567";  
	    $param['DOC_NO'] 					= "I231235567";  
	    
	    // utk second trx
//	    $param['SOURCE_ACCOUNT2']   		= $ACCTSRC;
//	    $param['SOURCE_ACCOUNT2_NAME'] 		= "Giro Erica"; 
//	    $param['SOURCE2_AMOUNT'] 			= $sourceAmount2;
//	    $param['CHARGES_ACCOUNT2']   		= $CHARGESACCT2;
//	    $param['AMT_CP_FEE2'] 				= $amtCPfee2;
		
		$data 			= new Service_TransferDisbursementWithin($param);
		$sendTransfer 	= $data->sendTransfer();
		
		$transactionID = $param['TRANSACTION_ID'];
		
		$reversalCode = "";
		$reversalDesc = "";
		if (isset($sendTransfer['RawResult']->TransactionReport->TransactionDetail[$idxTBR]->ReversalCode))
		{
			$reversalCode = $sendTransfer['RawResult']->TransactionReport->TransactionDetail[$idxTBR]->ReversalCode;
			$reversalDesc = $sendTransfer['RawResult']->TransactionReport->TransactionDetail[$idxTBR]->ReversalData;
		}
			
		$result['TRX'][$transactionID]['ResponseCode'] 		= $sendTransfer['RawResult']->responseDetail->response_code;
		$result['TRX'][$transactionID]['ResponseDesc'] 		= $sendTransfer['RawResult']->responseDetail->response_data;
		$result['TRX'][$transactionID]['ReversalCode'] 		= $reversalCode;
		$result['TRX'][$transactionID]['ReversalData'] 		= $reversalDesc;
		
		$result['TRX'][$transactionID]['EFTResponseCode'] 	= "";
		$result['TRX'][$transactionID]['EFTResponseDesc'] 	= "";
		echo "<pre>";
		print_r($sendTransfer);
		
	}
	
	public function domesticAction(){
		$param = array();	    
    	$param['TRA_AMOUNT']    	= 1500; 	 // amount in payment CCY  
     	$param['SOURCE_AMOUNT']   	= 1500;  	 // amount in source CCY
     	$param['TRX_FEE']     		= 1000;   // amount fee
      
     	$param['SOURCE_ACCOUNT']   		= '1003005098'; 		// 1003005098  2003500303
     	$param['BENEFICIARY_ACCOUNT']  	= '999888777555'; 
     	$param['COA_ACCOUNT']    		= '460501001';  // not sent 461001131  460501001
     
    	$param['PAYMENT_REF'] 				= '20120724000001ASD'; 
     	$param['CUST_ID'] 					= 'SGO006'; 
     	$param['USER_ID'] 					= 'MAKER1'; 
     	$param['AMOUNT_CCY'] 				= '016';	// 039
     	$param['SOURCE_ACCOUNT_CCY'] 		= '016'; 
     	$param['BENEFICIARY_ACCOUNT_CCY'] 	= '016'; 
     
     	$param['SOURCE_ACCOUNT_NAME'] 		= 'Erica'; 
     	$param['BENEFICIARY_ACCOUNT_NAME'] 	= 'Susiana'; 
    	$param['MESSAGE']	 				= 'desc1     bla bla bla                desc2 desc412123123123123123123'; 
     	$param['REF_NO'] 					= 'desc refno 12345 bla bla bla'; 
     	$param['TRANSFER_TYPE'] 			= 2; // 1.RTGS  2. SKN
     	$param['TRANSACTION_ID'] 			= '20120717000005ASD12'; 
     	$param['BENEFICIARY_CITIZENSHIP'] 	= 'R'; 	// R/NR
     	$param['BENEFICIARY_BANK_NAME'] 	= 'AGRIS, BDLPG';
     	$param['CLR_CODE'] 					= '9450017';
     	$param['SWIFT_CODE'] 				= '';
     	
     	echo "param request: <br>"; print_r($param); echo "<hr>";
		$data =  new Service_TransferDomestic($param);
		$result = $data->sendTransfer();
		print_r($result);
		
		die();
		
	}
	
	public function domesticsknsavingAction()
	{
		// Domestic
		echo "<pre><br>CMD Domestic SKN saving<hr>";
		$param = array();	    
    	$param['TRA_AMOUNT']    	= 1500; 	 // amount in payment CCY  
     	$param['SOURCE_AMOUNT']   	= 1500;  	 // amount in source CCY
     	$param['TRX_FEE']     		= 1000;   // amount fee
      
     	$param['SOURCE_ACCOUNT']   		= '2001021956';// '1003005098'; 		// 1003005098  2003500303
     	$param['BENEFICIARY_ACCOUNT']  	= '999888777555'; 
     	$param['COA_ACCOUNT']    		= '460501001';  // not sent 461001131  460501001
     
    	$param['PAYMENT_REF'] 				= '20120731000129ABC'; 
     	$param['CUST_ID'] 					= 'SGO006'; 
     	$param['USER_ID'] 					= 'MAKER1'; 
     	$param['AMOUNT_CCY'] 				= '016';	// 039
     	$param['SOURCE_ACCOUNT_CCY'] 		= '016'; 
     	$param['BENEFICIARY_ACCOUNT_CCY'] 	= '016'; 
//     	$param['CHARGES_ACCOUNT_CCY'] = '039';
     
     	$param['SOURCE_ACCOUNT_NAME'] 		= 'Erica'; 
     	$param['BENEFICIARY_ACCOUNT_NAME'] 	= 'Susiana'; 
    	$param['MESSAGE']	 				= 'desc1     bla bla bla                desc2 desc412123123123123123123'; 
     	$param['REF_NO'] 					= 'desc refno 12345 bla bla bla'; 
     	$param['TRANSFER_TYPE'] 			= 2; // 1.RTGS  2. SKN
     	$param['TRANSACTION_ID'] 			= '20120731000029ABC01'; 
     	$param['BENEFICIARY_CITIZENSHIP'] 	= 'R'; 	// R/NR
     	$param['BENEFICIARY_BANK_NAME'] 	= 'ANZ PANIN, JKT';
     	$param['CLR_CODE'] 					= '0610306';
     	$param['SWIFT_CODE'] 				= '';
     	
     	echo "param request: <br>"; print_r($param); echo "<hr>";
		$data =  new Service_TransferDomestic($param);
		$result = $data->sendTransfer();
		print_r($result);
		
		$rawResult = $result['RawResult'];
		
		$reversalCode = "";
		$reversalDesc = "";
		if (is_array($rawResult->TransactionReport->TransactionDetail))
		{
			if ($rawResult->TransactionReport->TransactionDetail[1]->TBRCode == 'EF01')
			{	$idxTBR = 0;	$idxEFT = 1;		}
			else
			{	$idxTBR = 1;	$idxEFT = 0;		}
			
			
			if (isset($rawResult->TransactionReport->TransactionDetail[$idxTBR]->ReversalCode))
			{
				$reversalCode = $rawResult->TransactionReport->TransactionDetail[$idxTBR]->ReversalCode;
				$reversalDesc = $rawResult->TransactionReport->TransactionDetail[$idxTBR]->ReversalData;
			}
			
			$EFTResponseCode = $rawResult->TransactionReport->TransactionDetail[$idxEFT]->ResponseCode;
			$EFTResponseDesc = $rawResult->TransactionReport->TransactionDetail[$idxEFT]->ResponseData;
		}
		else
		{
			$reversalCode = $rawResult->TransactionReport->TransactionDetail->ReversalCode;
			$reversalDesc = $rawResult->TransactionReport->TransactionDetail->ReversalData;
			
			$EFTResponseCode = "XF";
			$EFTResponseDesc = "EFT not send, due to failed trx";
		}
		
		$result['TRX'][$trx["TRANSACTION_ID"]]['ResponseCode'] 		= $result['ResponseCode'];
		$result['TRX'][$trx["TRANSACTION_ID"]]['ResponseDesc'] 		= $result['ResponseDesc'];
		$result['TRX'][$trx["TRANSACTION_ID"]]['ReversalCode'] 		= $reversalCode;
		$result['TRX'][$trx["TRANSACTION_ID"]]['ReversalData'] 		= $reversalDesc;
		$result['TRX'][$trx["TRANSACTION_ID"]]['EFTResponseCode'] 	= $EFTResponseCode;
		$result['TRX'][$trx["TRANSACTION_ID"]]['EFTResponseDesc'] 	= $EFTResponseDesc;
		
		echo "<hr>";
		print_r($result);
	}
	
	public function withinsingleAction()
	{
		echo "<br>CMD Within current-saving<hr><pre>";
		$param = array();
	    
    	$param['TRA_AMOUNT']    	= 10000; // amount in payment CCY  
     	$param['SOURCE_AMOUNT']   	= 10000;  // amount in source CCY
     	$param['TRX_FEE']     	  	= 0;   // amount fee
     
     	$param['SOURCE_ACCOUNT']   		= '2003603426'; // 1003005125 1003005098
     	$param['BENEFICIARY_ACCOUNT']  	= '2003039084'; // 2003500303 2003500320
     
    	$param['PAYMENT_REF'] 				= '20120731000036ABCQ'; 
     	$param['CUST_ID'] 					= 'SGO006'; 
     	$param['USER_ID'] 					= 'MAKER1'; 
     	$param['COA_ACCOUNT'] 				= '461001131'; // 461001186
     	$param['AMOUNT_CCY'] 				= '039';
     	$param['SOURCE_ACCOUNT_CCY'] 		= '039'; 
     	$param['BENEFICIARY_ACCOUNT_CCY'] 	= '039'; 
     
     	$param['SOURCE_ACCOUNT_NAME'] 		= 'Erica'; 
     	$param['BENEFICIARY_ACCOUNT_NAME'] 	= 'Susiana'; 
    	$param['MESSAGE'] 					= 'desc1                      desc2 desc412123123123123123123'; 
     	$param['REF_NO'] 					= 'refno1'; 
		$data =  new Service_TransferWithin($param);
		$result = $data->sendTransfer();
		print_r($result);
	}
	
	public function withintransferAction()
	{
		echo "<br>CMD Within current-saving<hr><pre>";
		$param = array();
		
		$CCY 	= $_GET['ccy'];
		$ACCSRC = $_GET['acc'];
		$BENE   = $_GET['bene'];
		$AMOUNT = $_GET['amt'];
		$currNumCCY		= Application_Helper_General::getCurrNum($CCY);
	    
    	$param['TRA_AMOUNT']    	= $AMOUNT; // amount in payment CCY  
     	$param['SOURCE_AMOUNT']   	= $AMOUNT;  // amount in source CCY
     	$param['TRX_FEE']     	  	= 0;   // amount fee
     
     	$param['SOURCE_ACCOUNT']   		= $ACCSRC; // 1003005125 1003005098
     	$param['BENEFICIARY_ACCOUNT']  	= $BENE; // 2003500303 2003500320
     
    	$param['PAYMENT_REF'] 				= '20120731000036ABCQ'; 
     	$param['CUST_ID'] 					= 'SGO006'; 
     	$param['USER_ID'] 					= 'MAKER1'; 
     	$param['COA_ACCOUNT'] 				= '461001131'; // 461001186
     	$param['AMOUNT_CCY'] 				= $currNumCCY;
     	$param['SOURCE_ACCOUNT_CCY'] 		= $currNumCCY; 
     	$param['BENEFICIARY_ACCOUNT_CCY'] 	= $currNumCCY; 
     
     	$param['SOURCE_ACCOUNT_NAME'] 		= 'Erica'; 
     	$param['BENEFICIARY_ACCOUNT_NAME'] 	= 'Susiana'; 
    	$param['MESSAGE'] 					= 'testcase integration'; 
     	$param['REF_NO'] 					= 'refno1'; 
		$data =  new Service_TransferWithin($param);
		$result = $data->sendTransfer();
		print_r($result);
	}
	
	public function disbursementwithinsingleAction()
	{
		// Disbursement Within Single
		echo "<br>Disbursement Within Single<hr>";
		$CCY 			= "USD";
		$PRKACCT 		= "2003039084";
			$ACCTSRC 		= "2003500320";
		$ACBENEF 		= "2003039005";  
			$CHARGESACCT 	= "2003500348";
			$CHARGESACCT2 	= "2003500334";
		$sourceAmount1	= 99.88;
		$sourceAmount2	= 0;
		$amtSCMinterest	= 0;
		$amtSCMfee		= 0;
		$amtCPfee		= 0;
		$amtCPfee2		= 0;
		$transferType 	= 0;	// 0: PB, 1: RTGS, 2: SKN
		
		$setting 		= new Settings();
		$coaSCMid		= ($CCY == 'IDR')? 'charges_acct_SCMFEE_IDR': 'charges_acct_SCMFEE_VLS';
		$coaCPid		= ($transferType == '0')? 'charges_acct_IDR': 'charges_acct_DOM';
		$COA_SCM_INT 	= $setting->getSetting('charges_acct_SCM_INT', 0);
		$COA_SCM_FEE 	= $setting->getSetting($coaSCMid, 0);
		$COA_CP_FEE 	= $setting->getSetting($coaCPid, 0);
		
		$currNumCCY		= Application_Helper_General::getCurrNum($CCY);
		
		// replace XX with LOB
		$LOB			= Application_Helper_General::getLOB($PRKACCT, $currNumCCY);
		$COA_SCM_INT 	= str_replace("XX", $LOB, $COA_SCM_INT);
		$COA_SCM_FEE 	= str_replace("XX", $LOB, $COA_SCM_FEE);
		$COA_CP_FEE 	= str_replace("XX", $LOB, $COA_CP_FEE);

		$param = array();	  
		$param['PAYMENT_REF']   			= '20120731000025ABC';
		$param['TRANSACTION_ID']   			= '20120731000025ABC01';
		$param['CUST_ID']   				= 'SGO006';
		$param['USER_ID']   				= 'RELEASE2'; 
	    $param['SOURCE_ACCOUNT']   			= $PRKACCT;
	    $param['SOURCE_ACCOUNT_NAME'] 		= "PRK Erica"; 
	    $param['BENEFICIARY_ACCOUNT'] 		= $ACBENEF; 
	    $param['BENEFICIARY_ACCOUNT_NAME'] 	= "Ramlan"; 
	    $param['CHARGES_ACCOUNT']   		= $CHARGESACCT;
	    $param['AMOUNT_CCY'] 				= $currNumCCY;
	    $param['SOURCE_AMOUNT'] 			= $sourceAmount1;
	    $param['MESSAGE'] 					= 'message disb'; 
	    $param['REF_NO'] 					= 'add message disb'; 
	    $param['TRANSFER_TYPE'] 			= $transferType; 
	    $param['COA_SCM_INTEREST'] 			= $COA_SCM_INT; 
	    $param['COA_SCM_FEE'] 				= $COA_SCM_FEE; 
	    $param['COA_CP_FEE'] 				= $COA_CP_FEE; 
	    $param['AMT_SCM_INTEREST'] 			= $amtSCMinterest; 
	    $param['AMT_SCM_FEE'] 				= $amtSCMfee;
	    $param['AMT_CP_FEE'] 				= $amtCPfee; 
	    $param['MEMBER_CODE'] 				= "M012314567";  
	    $param['DOC_NO'] 					= "I231235567";  
	    
	    // utk second trx
//	    $param['SOURCE_ACCOUNT2']   		= $ACCTSRC;
//	    $param['SOURCE_ACCOUNT2_NAME'] 		= "Giro Erica"; 
//	    $param['SOURCE2_AMOUNT'] 			= $sourceAmount2;
//	    $param['CHARGES_ACCOUNT2']   		= $CHARGESACCT2;
//	    $param['AMT_CP_FEE2'] 				= $amtCPfee2;
		
		$data 			= new Service_TransferDisbursementWithin($param);
		$sendTransfer 	= $data->sendTransfer();
	
		echo "<pre>";
		print_r($sendTransfer);
	}
	
	public function settlementAction()
	{
		echo "<br>Settlement<hr>";
		$CCY 			= "USD";	// TX_CCY
		$MEMBER_CODE 	= "MEMBER_NESTLE06";
		$PRKACCT 		= "2003601542";
		$ACTSRC 		= "2003603426";  
		$sourceAmount	= 25;
		$payReff		= 'S201208230000ABCD';
		$CHARGESACCT 	= "2003500348";
		
		$transferType 	= 0;	// 0: PB, 1: RTGS, 2: SKN
		
		$setting 		= new Settings();
		$coaSCMid		= ($CCY == 'IDR')? 'charges_acct_SCMFEE_IDR': 'charges_acct_SCMFEE_VLS';
		$coaCPid		= ($transferType == '0')? 'charges_acct_IDR': 'charges_acct_DOM';
		$COA_SCM_INT 	= $setting->getSetting('charges_acct_SCM_INT', 0);
		$COA_SCM_FEE 	= $setting->getSetting($coaSCMid, 0);
		$COA_CP_FEE 	= $setting->getSetting($coaCPid, 0);
		
		$currNumCCY		= Application_Helper_General::getCurrNum($CCY);
		
		// replace XX with LOB
		$LOB			= Application_Helper_General::getLOB($PRKACCT, $currNumCCY);
		$COA_SCM_INT 	= str_replace("XX", $LOB, $COA_SCM_INT);
		$COA_SCM_FEE 	= str_replace("XX", $LOB, $COA_SCM_FEE);
		$COA_CP_FEE 	= str_replace("XX", $LOB, $COA_CP_FEE);

		$param = array();	  
		$param['PAYMENT_REF']   			= $payReff;
		$param['CUST_ID']   				= 'NESTLE6';
		$param['USER_ID']   				= '';
		$param['SOURCE_ACCOUNT']   			= $ACTSRC;
		$param['SOURCE_ACCOUNT_CCY']   		= $currNumCCY;
		$param['SOURCE_ACCOUNT_NAME']   	= 'accsrc tes';
		$param['BENEFICIARY_ACCOUNT']   	= $PRKACCT;
		$param['BENEFICIARY_ACCOUNT_CCY']   = $currNumCCY;
		$param['BENEFICIARY_ACCOUNT_NAME']  = 'PRK tes';
		$param['AMOUNT_CCY']   				= $currNumCCY;
		$param['SOURCE_AMOUNT']   			= $sourceAmount;
		$param['MESSAGE']   				= '';
		$param['REF_NO']   					= '';
		$param['COA_SCM_INTEREST']   		= $COA_SCM_INT;
		$param['COA_SCM_FEE']   			= $COA_SCM_FEE;
		$param['COA_CP_FEE']   				= $COA_CP_FEE;
		$param['AMT_SCM_FEE']   			= 0;	// ??
		$param['AMT_CP_FEE']   				= 0;	// ??
		$param['CHARGES_ACCOUNT']   		= $CHARGESACCT;
		$param['MEMBER_CODE']   			= 'member code';
		$param['DOC_NO']   					= 'doc no';
		
		$data 			= new Service_TransferSettlement($param);
		$sendTransfer 	= $data->sendTransfer();
	
		echo "<pre>";
		print_r($sendTransfer);
	}
	
	public function resendeftAction()
	{
		require_once 'Service/TBRObj.php';
		require_once 'Service/ServiceObj.php';
		echo "<pre>Multi: <br>";
		$db 			=  Zend_Db_Table::getDefaultAdapter();
		
		$ps_number 		= 'B120831itij';	$type = 'multi';
		$selectQuery 	= "SELECT RAW_REQUEST FROM T_TRANSACTION T WHERE T.TRANSACTION_ID = ".$db->quote((string) $ps_number);
		$trx 			=  $db->fetchRow($selectQuery);
		
		$abc = unserialize($trx['RAW_REQUEST']);
		print_r($abc);echo  "<hr>Single:<br>";
		
		$ps_number 		= 'B120831j2ua';	$type = 'single';
		$selectQuery 	= "SELECT RAW_REQUEST FROM T_TRANSACTION T WHERE T.TRANSACTION_ID = ".$db->quote((string) $ps_number);
		$trx 			=  $db->fetchRow($selectQuery);
		
		$abc = unserialize($trx['RAW_REQUEST']);
		print_r($abc);
		
//		$params = array();
//		$params['TYPE']				= $type;	// single/multi
//		$params['PS_NUMBER']		= substr($ps_number, 0, 17);
//		$params['RAW_REQUEST']		= $trx['RAW_REQUEST'];
//		print_r($params);
//		$data 			= new Service_ResendEFT($params);
//		$sendTransfer 	= $data->sendTransfer();

		print_r($sendTransfer);
	}
	
	public function tesAction()
	{
		require_once 'General/SystemBalance.php';
		$sysbal = new SystemBalance('YDS', '2000184756', '016');
		$a = $sysbal->getTempMakerAmountHold('M20120512000006E4');
		
		var_dump($a);
	}
	
	public function generateaclAction(){
		
		$moduleDir = APPLICATION_PATH."/modules/";
		
		$modulePrivilegeMap = $this->listFolderFiles($moduleDir, array('.svn'));
		
		$handle = '';
		$this->arrayACL = '';
		
		foreach ($modulePrivilegeMap as $modulename => $value) {
//			echo $moduleDir.$modulename.'/controllers/',$modulename;
			$this->getstruct($moduleDir.$modulename.'/controllers/',$modulename);
		}
		var_dump($this->arrayACL);
		$this->writeAclConfiguration($this->arrayACL);
			die();
	}
			
	public function generateaclbackendAction(){
	
		$moduleDir = "C:/Program Files (x86)/Zend/Apache2/htdocs/coolpay/coolPayBackend/application/modules/";
		
		$modulePrivilegeMap = $this->listFolderFiles($moduleDir, array('.svn'));
		
		$handle = '';
		$this->arrayACL = '';
		
		foreach ($modulePrivilegeMap as $modulename => $value) {
	//			echo $moduleDir.$modulename.'/controllers/',$modulename;
			$this->getstruct($moduleDir.$modulename.'/controllers/',$modulename);
		}
		var_dump($this->arrayACL);
		$this->writeAclConfigurationBackend($this->arrayACL);
			die();
	}	

	private function getstruct($controllerDir,$module){
		if ($handle = opendir($controllerDir)) {
	//				    echo "Directory handle: $handle<br/>";
	//				    echo "Files:</br>";
	//				    echo "module $module:</br>";
					
					    /* This is the correct way to loop over the directory. */
					    while (false !== ($file = readdir($handle))) {
					    	if(substr($file,-4)=='.php'){
						        $classname = str_replace('.php','',$file);
						        $controllerName = str_replace('.php','',$file);
						    	
						    	
						    	require_once ($controllerDir.'/' . $file);
						    	
						    	
								if($classname == 'ToolsController' || $classname == 'ErrorController' || $classname == 'LoginController' || $classname == 'AuthorizationController' ) continue;
	//							echo "classname = $classname\n<br/>";
								if($module!="default")$classname = ucfirst(strtolower($module)).'_'.$classname;
								$class_methods = get_class_methods($classname);
								$classname = strtolower(str_ireplace('Controller','',$classname));
								
								if(is_array($class_methods)){
									foreach ($class_methods as $method_name) {
										
										
										if(substr($method_name,-6)=='Action'){
		//									echo $method_name;
									    $this->arrayACL['acl'][$module][strtolower(str_ireplace('Controller','',$controllerName))][strtolower(substr($method_name,0,-6))] = '';
										}
									}
								}
						    	
						    	
					    	}
					    }
					
					    
					
					    closedir($handle);
	//				    Zend_Debug::dump($this->arrayACL);
					    
				    
		//			    $this->writeListMenu($this->arrayACL['resources']);
					       
					}
	}
	
	private function writeAclConfiguration(array $acl){
		
		$config =  new Zend_Config($acl);
		
		
		
		$writer = new Zend_Config_Writer_Ini(array('config'   => $config,
		                                           'filename' => LIBRARY_PATH."/Application/Frontend/acledit.ini"));
		$writer->write();
		
	}
			
	private function writeAclConfigurationBackend(array $acl){
		
		$config =  new Zend_Config($acl);
		
		
		
		$writer = new Zend_Config_Writer_Ini(array('config'   => $config,
		                                           'filename' => LIBRARY_PATH."/Application/Backend/acledit.ini"));
		$writer->write();
		
	}

	function listFolderFiles($dir,$exclude){
			$directory = $dir;
 
		//get all files in specified directory
		$files = glob($directory . "*");
		 
		//print each file name
		foreach($files as $file)
		{
		 //check to see if the file is a folder/directory
		 if(is_dir($file))
		 {
		  	$dataModule[str_ireplace( dirname($file).'/','', $file)] = array('systemPrivilege');
		 }
	    
	    
		}
		return  $dataModule;
	}
		
}