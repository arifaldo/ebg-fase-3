<?php

class eformworkflow_DownloadfileController extends Application_Main
{

    public function indexAction()
    {

        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");

        if ($bg_number == "" && $download = "") {
            return $this->_redirect("/home/dashboard");
        }

        $checkOthersAttachment = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
            ->where("BG_REG_NUMBER = '$bg_number'")
            ->query()->fetchAll();

        if (count($checkOthersAttachment) == 0) {
            return $this->_redirect("/home/dashboard");
        }

        $getFileName = $checkOthersAttachment[$download]["BG_FILE"];

        $attahmentDestination = UPLOAD_PATH . '/document/submit/';
        return $this->_helper->download->file($getFileName, $attahmentDestination . $getFileName);
    }

    public function specialAction()
    {
        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");

        if ($bg_number == "" && $download == "") {
            return $this->_redirect("/home/dashboard");
        }

        $attahmentDestination = UPLOAD_PATH . '/document/submit/';


        $getSpecialFormatFile = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE"], ["SPECIAL_FORMAT_DOC"])
            ->where("BG_REG_NUMBER = '$bg_number'")
            ->query()->fetchAll();

        $getFileName = $getSpecialFormatFile[0]["SPECIAL_FORMAT_DOC"];

        return $this->_helper->download->file($getFileName, $attahmentDestination . $getFileName);
    }

    public function slikojkAction()
    {
        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");
        $onrisk = $this->_getParam("onrisk");

        if ($bg_number == "" && $download == "") {
            return $this->_redirect("/home/dashboard");
        }

        $table = intval($onrisk) === 1 ? "T_BANK_GUARANTEE_DETAIL" : "TEMP_BANK_GUARANTEE_DETAIL";

        $attachmentDestination = UPLOAD_PATH . '/document/submit/';
        $getSpecialFormatFile = $this->_db->select()
            ->from(["A" => $table], ["PS_FIELDVALUE"])
            ->where("BG_REG_NUMBER = '$bg_number'")
            ->where("PS_FIELDNAME = 'Info Slik OJK'")
            ->query()->fetchAll();

        $getFileName = $getSpecialFormatFile[0]["PS_FIELDVALUE"];

        return $this->_helper->download->file($getFileName, $attachmentDestination . $getFileName);
    }

    public function agreeformatAction()
    {
        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");

        if ($bg_number == "" && $download == "") {
            return $this->_redirect("/home/dashboard");
        }

        $attachmentDestination = UPLOAD_PATH . '/document/submit/';

        $formatFile = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE"], ["AGREE_FORMAT", "MEMO_LEGAL"])
            ->where("BG_REG_NUMBER = '$bg_number'")
            ->query()->fetchAll();

        if ($download == 1) {

            $getFileName = $formatFile[0]["AGREE_FORMAT"];

            return $this->_helper->download->file($getFileName, $attachmentDestination . $getFileName);
        }

        if ($download == 2) {
            $getFileName = $formatFile[0]["MEMO_LEGAL"];

            return $this->_helper->download->file($getFileName, $attachmentDestination . $getFileName);
        }
    }

    public function suratpermohonanAction()
    {
        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");

        if ($bg_number == "" && $download == "") {
            return $this->_redirect("/home/dashboard");
        }

        $attachmentDestination = UPLOAD_PATH . '/document/submit/';
        $getSpecialFormatFile = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE"], ["KUASA_DIREKSI_FILE"])
            ->where("BG_REG_NUMBER = '$bg_number'")
            ->query()->fetchAll();

        $getFileName = $getSpecialFormatFile[0]["KUASA_DIREKSI_FILE"];

        return $this->_helper->download->file($getFileName, $attachmentDestination . $getFileName);
    }

    public function ijinprinsipAction()
    {
        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");
        $onrisk = $this->_getParam("onrisk");

        if ($bg_number == "" && $download == "") {
            return $this->_redirect("/home/dashboard");
        }

        $table = intval($onrisk) == 1 ? "T_BANK_GUARANTEE_DETAIL" : "TEMP_BANK_GUARANTEE_DETAIL";

        $attachmentDestination = UPLOAD_PATH . '/document/submit/';
        $getSpecialFormatFile = $this->_db->select()
            ->from(["A" => "T_BANK_GUARANTEE_DETAIL"], ["PS_FIELDVALUE"])
            ->where("A.BG_REG_NUMBER = ?", $bg_number)
            ->where("A.PS_FIELDNAME = ?", "Principle Insurance Document")
            ->query()->fetchAll();

        if (!$getSpecialFormatFile) {
            $getSpecialFormatFile = $this->_db->select()
                ->from(["A" => "TEMP_BANK_GUARANTEE_DETAIL"], ["PS_FIELDVALUE"])
                ->where("A.BG_REG_NUMBER = ?", $bg_number)
                ->where("A.PS_FIELDNAME = ?", "Principle Insurance Document")
                ->query()->fetchAll();
        }

        $getFileName = $getSpecialFormatFile[0]["PS_FIELDVALUE"];

        return $this->_helper->download->file($getFileName, $attachmentDestination . $getFileName);
    }

    public function cgdocumentAction()
    {
        $bg_number = $this->_getParam("bgnumb");
        $download = $this->_getParam("download");
        $onrisk = $this->_getParam("onrisk");

        if ($bg_number == "" && $download == "") {
            return $this->_redirect("/home/dashboard");
        }

        $table = intval($onrisk) == 1 ? "T_BANK_GUARANTEE_DETAIL" : "TEMP_BANK_GUARANTEE_DETAIL";

        $attachmentDestination = UPLOAD_PATH . '/document/submit/';
        $getSpecialFormatFile = $this->_db->select()
            ->from(["A" => "T_BANK_GUARANTEE_DETAIL"], ["PS_FIELDVALUE"])
            ->where("A.BG_REG_NUMBER = ?", $bg_number)
            // ->where("A.BG_REG_NUMBER = '$bg_number'")
            ->where("A.PS_FIELDNAME = ?", "Counter Guarantee Document")
            ->query()->fetchAll();

        if (!$getSpecialFormatFile) {
            $getSpecialFormatFile = $this->_db->select()
                ->from(["A" => "TEMP_BANK_GUARANTEE_DETAIL"], ["PS_FIELDVALUE"])
                ->where("A.BG_REG_NUMBER = ?", $bg_number)
                // ->where("A.BG_REG_NUMBER = '$bg_number'")
                ->where("A.PS_FIELDNAME = ?", "Counter Guarantee Document")
                ->query()->fetchAll();
        }

        $getFileName = $getSpecialFormatFile[0]["PS_FIELDVALUE"];

        return $this->_helper->download->file($getFileName, $attachmentDestination . $getFileName);
    }
}
