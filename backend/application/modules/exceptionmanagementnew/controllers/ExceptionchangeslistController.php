<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class exceptionmanagementnew_ExceptionchangeslistController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('EXCL','Viewing Payment Exception Changes List');
		}
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$this->view->arrPayType = $arrPayType;
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
		
		$approvalstatus = $this->_aprovalstatus;
		$statusarr = array_combine(array_values($approvalstatus['code']),array_values($approvalstatus['desc']));
		
		$select = $this->_db->select()->distinct()
							->from(array('A' => 'M_CUSTOMER'),array('CUST_ID'))
							->order('CUST_ID ASC')
				 			-> query()->fetchAll();
		$this->view->var=$select;

		$fields = array(
						'payref'      		=> array('field' => 'payref',
											      'label' => $this->language->_('Payment Ref#'),
											      'sortable' => true),
						'custid'           => array('field' => 'custid',
											      'label' => $this->language->_('Company Code'),
											      'sortable' => true),
						'custname'      	=> array('field' => 'custname',
											      'label' => $this->language->_('Company Name'),
											      'sortable' => true),
						'type'     			=> array('field' => 'type',
											      'label' => $this->language->_('Payment Type'),
											      'sortable' => true),
						'suggestdate'      	=> array('field' => 'suggestdate',
											      'label' => $this->language->_('Suggestion Date'),
											      'sortable' => true),
						'suggestor'      	=> array('field' => 'suggestor',
											      'label' => $this->language->_('Suggester'),
											      'sortable' => true),
						'status'      		=> array('field' => 'status',
											      'label' => $this->language->_('Suggestion Status'),
											      'sortable' => true),);
		$filterlist = array('SUGEST_DATE','COMP_CODE','COMP_NAME','PS_NUM','PS_TYPE','SUGGEST_USER','SUGGEST_STATUS');
		
		$this->view->filterlist = $filterlist;
	      
		     
		
		$page    = $this->_getParam('page');
		
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$caseType = "(CASE A.TYPE ";
  		foreach($typearr as $key=>$val)
  		{
   			$caseType .= " WHEN ".$key." THEN '".$val."'";
  		}
		$caseType .= " END)";
 
  		$caseStatus = "(CASE A.SUGGEST_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";
  			
  		$select2 = $this->_db->select()
  							->from(array('A' => 'T_PSLIP_EXCEPTION_REPAIR'),array())
					        ->joinleft(array('B' => 'M_CUSTOMER'), 'A.MEMBER_CUST_ID = B.CUST_ID',array('payref'			=>	'A.PS_NUMBER',
								        																'custid'			=>	'A.CUST_ID',
								        																'custname'			=>	'A.CUST_NAME',
								        																'suggestdate'		=>	'A.SUGGEST_DATE',
						        																		'suggestor'			=>	'A.SUGGEST_USER',
						        																		'type'				=>	$caseType,
						        																		'status'			=>	$caseStatus,
								        																'suggestid'			=>	'A.SUGGEST_ID',
										        														'extype'			=>	'A.EXCEPTION_TYPE'));
		// $select2 -> where("A.EXCEPTION_TYPE = '1' OR A.EXCEPTION_TYPE = '2' OR A.EXCEPTION_TYPE = '3'");
		// $select2 -> where("A.SUGGEST_STATUS = '0' OR A.SUGGEST_STATUS = '1'");

		$filterArr = array(	'filter' 			=> array('StripTags','StringTrim'),
						   	'PS_TYPE'  			=> array('StripTags'),
						   	'PS_NUM' 		=> array('StripTags','StringTrim','StringToUpper'),
	                       	'COMP_CODE'    		=> array('StripTags','StringTrim','StringToUpper'),
	                       	'COMP_NAME'  		=> array('StripTags','StringToUpper'),
							'SUGGEST_STATUS'  			=> array('StripTags'),
							'SUGEST_DATE' 		=> array('StripTags','StringTrim'),
							'SUGEST_DATE_END' 			=> array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array(	'filter' 			=> array(),
						   	'PS_TYPE'  			=> array(),
						   	'PS_NUM'			=> array(),
	                       	'COMP_CODE'    		=> array(),
	                       	'COMP_NAME'  		=> array(),
							'SUGGEST_STATUS'  			=> array(),
							'SUGEST_DATE' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
							'SUGEST_DATE_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );

	     $dataParam = array("PS_TYPE","PS_NUM","COMP_CODE","COMP_NAME","SUGGEST_USER","SUGGEST_STATUS");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
			if(!empty($this->_request->getParam('sugestdate'))){
				$createarr = $this->_request->getParam('sugestdate');
					$dataParamValue['SUGEST_DATE'] = $createarr[0];
					$dataParamValue['SUGEST_DATE_END'] = $createarr[1];
			}
	                      
	    $zf_filter 		= new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    // $filter 		= $zf_filter->getEscaped('filter');
	    $filter 		= $this->_getParam('filter');
	    $paytype 		= html_entity_decode($zf_filter->getEscaped('PS_TYPE'));
	    $paymenref 		= html_entity_decode($zf_filter->getEscaped('PS_NUM'));
	    $custid 		= html_entity_decode($zf_filter->getEscaped('COMP_CODE'));
		$custname 		= html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
	    $sugest 		= html_entity_decode($zf_filter->getEscaped('SUGGEST_USER'));
	    $status 		= html_entity_decode($zf_filter->getEscaped('SUGGEST_STATUS'));
	    $datefrom 		= html_entity_decode($zf_filter->getEscaped('SUGEST_DATE'));
	    $dateto 		= html_entity_decode($zf_filter->getEscaped('SUGEST_DATE_END'));
		
	    $statusSuggest="";
	    switch ($status){
	    	case $this->language->_("Unread Suggestion"):
	    		$statusSuggest = 0;
	    		break;
	    	case $this->language->_("Read Suggestion"):
	    		$statusSuggest = 1;
	    		break;
	    	case $this->language->_("Granted Suggestion"):
	    		$statusSuggest = 2;
	    		break;
	    	case $this->language->_("Rejected Suggestion"):
	    		$statusSuggest = 3;
	    		break;
	    	case $this->language->_("Request Suggestion"):
	    		$statusSuggest = 4;
	    		break;
	    	case $this->language->_("Repaired Suggestion"):
	    		$statusSuggest = 5;
	    		break;
	    	case $this->language->_("Deleted Suggestion"):
	    		$statusSuggest = 6;
	    		break;
	    }	    
	    
		if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}

		if($filter ==$this->language->_('Set Filter') || $filter == null)
		{
			// die;
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;

			if(!empty($datefrom))
			{
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
			}
			if(!empty($datefrom))
			{
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
			}
		            
		    if(!empty($dateto))
			{
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto    = $FormatDate->toString($this->_dateDBFormat);
			}
			
			if(!empty($datefrom) && empty($dateto))
				$select2->where("DATE(A.SUGGEST_DATE) >= ".$this->_db->quote($datefrom));
		            
		   	if(empty($datefrom) && !empty($dateto))
				$select2->where("DATE(A.SUGGEST_DATE) <= ".$this->_db->quote($dateto));
		            
		    if(!empty($datefrom) && !empty($dateto))
				$select2->where("DATE(A.SUGGEST_DATE) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}
		
		if($filter == $this->language->_('Set Filter'))
		{  

			if($paytype){
				$this->view->paytype = $paytype;
				$paytype 	 	= explode(",", $paytype);
				$select2->where("A.TYPE in (?) ", $paytype);		
			}
			
			if($paymenref){
	       		$this->view->paymenref = $paymenref;
				$paymenref = '%'.$paymenref.'%';
	       		$select2->where("A.PS_NUMBER LIKE ".$this->_db->quote($paymenref));}
	       
			if($custid){
	       		$this->view->custid = $custid;
	       		$select2->where("A.CUST_ID LIKE ".$this->_db->quote($custid));}
	     
			if($custname){
	       		$this->view->custname = $custname;
	       		$select2->where("A.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));}

	       	if($sugest){
	       		$this->view->sugest = $sugest;
	       		$select2->where("A.SUGGEST_USER LIKE ".$this->_db->quote('%'.$sugest.'%'));}

			if($status){
	       		$this->view->status = $status;
	       		$select2->where("A.SUGGEST_STATUS = ".$this->_db->quote($statusSuggest));}
		}
		$select2->order($sortBy.' '.$sortDir);
	 	//Zend_Debug::dump($select2->query());
	    $result = $select2->query()->FetchAll();
	    
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($result);
		unset($dataParamValue['SUGEST_DATE_END']);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        
     
      }else{
      		$wherecol = array();
      		$whereval = array();	
      }
      $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
	}
}
?>
