<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';

class Remittancechar_IndexController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();

		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

		$buser_id = $this->_userIdLogin;
		$this->view->buser_id 	= $buser_id;

		$settings =  new Settings();

		$minLengthPassword = $settings->getSetting('minbpassword');
		$maxLengthPassword = $settings->getSetting('maxbpassword');
		
		
		
		$this->view->minlengthpass = $minLengthPassword;
		$this->view->maxlengthpass = $maxLengthPassword;
		$model = new remittancechar_Model_Remittancechar();
		
		$this->view->tempcek = $model->cekTemp();
// 		$this->view->paginator = $model->getRemittanceType($charge_type);
		$this->view->transferfee = $model->getRemittanceTypeIndex('3');
		$this->view->fullamount = $model->getRemittanceTypeIndex('4');
		$this->view->provfee = $model->getRemittanceTypeIndex('5');
		
		if($this->_request->isPost())
		{
			$errDesc = array();
			
			$filters = array('old_password'     => array('StripTags','StringTrim'),
								 'new_password'     => array('StripTags','StringTrim'),
								 'confirm_password' => array('StripTags','StringTrim'),
								);

			$zf_filter_input = new Zend_Filter_Input($filters,null,$this->_request->getPost());

			$oldpass 				= $zf_filter_input->old_password;
			$newpass				= $zf_filter_input->new_password;
			$CON_NEWBUSER_PASSWORD	= $zf_filter_input->confirm_password;	
			
			$errDesc = array();
			if (!$oldpass){
				$errDesc['oldpass'] = "Error: Current Password cannot be left blank. Please correct it.";
			}
			
			if (!$CON_NEWBUSER_PASSWORD){
				$errDesc['conpass'] = "Error: Confirm New Password cannot be left blank. Please correct it.";				
			}elseif ($newpass <> $CON_NEWBUSER_PASSWORD){
				$errDesc['conpass'] = "Error: Sorry, but the two passwords you entered are not the same.";  
			}
			if (!$newpass){
				$errDesc['newpass'] = "Error: New Password cannot be left blank. Please correct it.";
			}elseif ((strlen($newpass) < $minLengthPassword) || (strlen($newpass) > $maxLengthPassword)){
				//$errDesc['newpass'] = "Error: Minimum char ".$minLengthPassword." and maximum char ".$maxLengthPassword." for New Password length. Please correct it.";
				//$errDesc['newpass'] = $this->language->_("Error: Minimum char ".$minLengthPassword." and maximum char ".$maxLengthPassword." for New Password length. Please correct it.");
				$errDesc['newpass'] = $this->language->_('Minimum char')." ".$minLengthPassword. " ".$this->language->_('and maximum char')." ".$maxLengthPassword." ".$this->language->_('for New Password length. Please correct it').".";
			}elseif (Application_Helper_General::checkPasswordStrength($newpass) < 3){
				$errDesc['newpass'] = "Error: New Password must containt at least one uppercase character, one lowercase character and one number.Please correct it.";													
			}elseif ($newpass == $oldpass){
				$errDesc['newpass'] = "Error: Password must be different from old password";
			}	

			if(!empty($oldpass) && !empty($newpass))
			{
				$BankUser =  new BankUser($buser_id);
				$result = array();
				$failed = 0;
				if(count($errDesc)>0) $failed = 1;

				$result = $BankUser->changePassword($oldpass,$newpass,$failed); // 1 = no Update;

				if( ( is_array($result) && $result !== true ) || $failed == 1)
				{
					if(count($result)>0)
					{
						foreach($result as $key=>$value)	
						{						
							$errDesc[$key] = $value;							
						}
					}
					$this->view->error 	= true;
					$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';

						$this->view->errDesc = $errDesc;
				}
				else{
					Application_Helper_General::writeLog('CHOP','Change My Password Buser ID : '.$buser_id);

					$this->setbackURL('/home');
					$this->_redirect('/notification/success/index');
				}	
			}
			else
			{
				$this->view->error 	= true;
				$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';
				$this->view->errDesc = $errDesc;
			}
		}
		Application_Helper_General::writeLog('CHOP','Change My Password');
	}

}

