<?php
require_once 'Zend/Controller/Action.php';

class fprivilagetemplate_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$fields = array(
			'TemplateCode'  			=> array(
				'field' => 'FTEMPLATE_ID',
				'label' => $this->language->_('Template Code'),
				'sortable' => true
			),
			'TemplateName'  			=> array(
				'field' => 'FTEMPLATE_DESC',
				'label' => $this->language->_('Template Name'),
				'sortable' => true
			),
		);



		$select = $this->_db->SELECT()
			->FROM('M_FTEMPLATE');

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'FTEMPLATE_ID');
		$sortDir = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		$select->order($sortBy . ' ' . $sortDir);
		// echo $select;die;
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->modulename = $this->_request->getModuleName();
		Application_Helper_General::writeLog('PTLS', 'View Frontend Privilege Template');
	}
}
