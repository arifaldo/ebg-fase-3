<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/Settings.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	$config    = Zend_Registry::get('config');

	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	
	$template 		= file_get_contents(LIBRARY_PATH.'/email template/email submition.html');
	$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
	$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
	
	$transFT   = array( '[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
						'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
						'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl													
					  );
	
	$FOOTER_ID = strtr($FOOTER_ID, $transFT);
	$FOOTER_EN = strtr($FOOTER_EN, $transFT);	

	$setting = new Settings();
	$OpsEmail = $setting->getSetting('email_submission');
// 	$OpsEmail = 'caroluz_wadhez@yahoo.com';
// 	$OpsEmail = 'hary@sgo.co.id';
	
	$string = NULL;
	
	if($OpsEmail)	
	{	
		$Type = "data_submission";
		$date 	     = (date("Y-m-d")); //get today date, format yyyy-mm-dd, used on query

		$select = $db->select()->distinct()
						->from(array('A' => 'T_FILE_SUBMIT'),array('*'))
						->joinright(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID',array('*'))
						->where('DATE(A.FILE_UPLOADED_TIME) = DATE('.$db->quote($date).')')
						->where('A.FILE_ISEMAILED = 0')
						->where('A.FILE_DELETED = 0')
						->query() ->fetchAll()
		;
// 						echo $select;die;
// 						Zend_Debug::dump($select);die;				  

		if($select) 
		{
			try
			{
				$db->beginTransaction();
				$select2 = $db->select()->distinct()
							->from(array('T_FILE_SUBMIT'),array('CUST_ID'))
							->where('DATE(FILE_UPLOADED_TIME) = '.$db->quote($date))
							->where('FILE_ISEMAILED = 0')
							->where('FILE_DELETED = 0')
							->query() ->fetchAll();

				$i 	= 0;
				$no = 1;
				$emailSent = 0;
				foreach($select2 as $result2)
				{
					$custid = $result2['CUST_ID'];
					
					//select data from database where is uploaded today and not email yet and not deleted yet
					$select3 = $db->select()->distinct()
											->from(array('A' => 'T_FILE_SUBMIT'),array('*'))
											->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID',array('*'))
											->where('DATE(A.FILE_UPLOADED_TIME) = '.$db->quote($date))
											->where('A.FILE_ISEMAILED = 0')
											->where('A.FILE_DELETED = 0')
											->where('B.CUST_ID LIKE '.$db->quote($custid))
											->order('A.FILE_UPLOADED_TIME DESC')
											->query()
											->fetchAll();
				
					if($select3)
					{
						foreach($select3 as $result3) 
						{
							if($i == 0)
							{
								$datalist = "<tr>
												<td>".$no."</td>
												<td>".$result3['CUST_ID']."</td>
												<td>".$result3['CUST_NAME']."</td>
												<td>".$result3['FILE_NAME']."</td>
												<td>".$result3['FILE_DESCRIPTION']."</td>
												<td>".Application_Helper_General::convertDate($result3['FILE_UPLOADED_TIME'],'dd MMMM yyyy HH:mm:ss')."</td>
											</tr>";
								$i++;
							}
							else
							{
								$datalist = $datalist."<tr>
															<td>".$no."</td>
															<td>".$result3['CUST_ID']."</td>
															<td>".$result3['CUST_NAME']."</td>
															<td>".$result3['FILE_NAME']."</td>
															<td>".$result3['FILE_DESCRIPTION']."</td>
															<td>".Application_Helper_General::convertDate($result3['FILE_UPLOADED_TIME'],'dd MMMM yyyy HH:mm:ss')."</td>
														</tr>";
							}
							
							$fileid = $result3['FILE_ID'];
							//Zend_Debug::dump($result3);die;
							$update = array('FILE_ISEMAILED'  	=> '1');		
							$where = "FILE_ID = '".$fileid."'";
							
							$no++;
							$custname = $result3['CUST_NAME'];
							$db->update('T_FILE_SUBMIT', $update, $where);
						}
					}
				}
				
				$subject 		= 'Data File Sharing Notification';
				
				$translate 		= array( 
														'[[DATA_CONTENT]]' => $datalist,
														'[[FOOTER_ID]]' 		=> $FOOTER_ID,
														'[[FOOTER_EN]]' 		=> $FOOTER_EN,
														'[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
														'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
														'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl,
														'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
														'[[master_bank_fax]]' 	=> $templateEmailMasterBankFax,
														'[[master_bank_address]]' 	=> $templateEmailMasterBankAddress,
														'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,		
													);
				
				$mailContent 	= strtr($template,$translate);
				
				$result 		= Application_Helper_Email::sendEmail($OpsEmail,$subject,$mailContent);
				if($result)
				{
					$emailSent++;
				}
				$string  = "mail send $emailSent";
				
				$db->commit();
			}
			catch(Exception $e) 
			{
				echo "mati";die;
				$db->rollBack();
				$string  = "Fail Sending Email For $subject";
			}
			
			Application_Helper_General::cronLog(basename(__FILE__),$string);
		}	
	}
	echo $string;
?>
