<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';

class rdntrans_BulkcreditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti			
	
	protected $_destinationUploadDir = '';
	protected $_maxRow = '';
	
	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');		
	}

	public function indexAction()
	{
		$this->setbackURL();
		
		$settingObj = new Settings();
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$param = array();
		$param['CCY_IN'] = 'IDR';
		$param['ISRDN_TRX'] = '1';
		$AccArr 	  = $CustomerUser->getAccounts($param);
		$this->view->AccArr =  $AccArr;
		$listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');
		$this->view->BULK_TYPE  = 0;
		$BULK_TYPEARR = $this->_request->getParam('BULK_TYPE');
		if(!empty($BULK_TYPEARR)){
			$this->view->BULK_TYPE  = $BULK_TYPEARR;
		}

		$bulkarr = array(
    								'0' => $this->language->_('Multi Credit'),
    								'1' => $this->language->_('Multi Debet'),
    								'2' => $this->language->_('Import RTGS KSEI')
    								);
		$this->view->BulkType = $bulkarr;


		$this->view->ccyArr = $this->getCcy();
		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustomerUser->getBeneficiaries();		
		$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		$select->where("B.BENEFICIARY_BANKSTATUS = 1");
		$select->where("B.BENEFICIARY_ISAPPROVE = 1");
		$select->where("B.CURR_CODE = 'IDR'");
		$select->joinleft(array('MCA' => 'M_CUSTOMER_ACCT'), 'B.BENEFICIARY_ACCOUNT = MCA.ACCT_NO ', array());
		$select->where("MCA.CUST_ID = ?", (string)$this->_custIdLogin);
		//$select->where("MCA.ISRDN_TRX = ?", '1');
		//echo $select;die;		
		$resultBeneficiary = $this->_db->fetchAll($select);				
		$this->view->AccBenefArr =  $resultBeneficiary;

		$filterlist = array('STATUS','TRANSFER_TYPE','SUBJECT','LAST_UPLOADED_BY','LAST_UPLOADED_DATE','EFFECTIVE_DATE','CCY');
		$this->view->filterlist = $filterlist;
  
		$select2	= $this->_db->select()
			->from(	array('TEMP_BULKPSLIP'),array('*'))
			->where( "PS_CATEGORY = ?","BULKRDN PAYMENT")
			->order("PS_CREATED DESC");
			 
			 
		//$pslipTrxData = $this->_db->fetchAll($selectTrx);

		$filterArr = array(	'STATUS' 					=> array('StringTrim','StripTags'),
							'TRANSFER_TYPE' 			=> array('StringTrim','StripTags'),
							'SUBJECT' 					=> array('StringTrim','StripTags'),
							'LAST_UPLOADED_BY' 			=> array('StringTrim','StripTags'),
							'LAST_UPLOADED_DATE' 		=> array('StringTrim','StripTags'),
							'LAST_UPLOADED_DATE_END' 	=> array('StringTrim','StripTags'),
							'EFFECTIVE_DATE' 			=> array('StringTrim','StripTags'),
							'EFFECTIVE_DATE_END' 		=> array('StringTrim','StripTags'),
							'CCY' 						=> array('StringTrim','StripTags'),
		);
	                      
		$dataParam = array('STATUS','TRANSFER_TYPE','CCY');
		$dataParamValue = array();
		$paramprint = $this->_request->getParams();

		$wherecol =  $this->_request->getParam('wherecol');

		$clean2 = array_diff( $wherecol,$dataParam); 
		$dataParam = array_diff( $wherecol,$clean2);

		foreach ($dataParam as $dtParam)
		{

			if(!empty($wherecol)){
				$dataval = $this->_request->getParam('whereval');

				$order = 0;
				foreach ($wherecol as $key => $value) {
					if($value == "LAST_UPLOADED_DATE"){
							$order--;
						}
					if($dtParam==$value){
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;

				}

			}

		}

		$createarr = $this->_request->getParam('lastuploaded');
		if ($createarr != NULL) {
			$dataParamValue['LAST_UPLOADED_DATE'] 		= $createarr[0];
			$dataParamValue['LAST_UPLOADED_DATE_END'] 	= $createarr[1];
		}

		$createarr = $this->_request->getParam('effective');
		if ($createarr != NULL) {
			$dataParamValue['EFFECTIVE_DATE'] 		= $createarr[0];
			$dataParamValue['EFFECTIVE_DATE_END'] 	= $createarr[1];
		}

		$options = array('allowEmpty' => true);
		$validators = array(
					
						'LAST_UPLOADED_DATE' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'LAST_UPLOADED_DATE_END' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'EFFECTIVE_DATE'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'EFFECTIVE_DATE_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	

						'STATUS'			=> array(),
						'TRANSFER_TYPE'		=> array(),
						'CCY'				=> array(),
						'SUBJECT'			=> array(),
						'LAST_UPLOADED_BY'	=> array(),

						);
	   
	     $zf_filter 	= new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
	     $filter 		= $this->_getParam('filter');
	     $filter_clear 	= $this->_getParam('clearfilter');

	    $datefrom 		= $zf_filter->getEscaped('LAST_UPLOADED_DATE');
		$dateto 		= $zf_filter->getEscaped('LAST_UPLOADED_DATE_END');
		$effectivefrom 	= $zf_filter->getEscaped('EFFECTIVE_DATE');
		$effectiveend 	= $zf_filter->getEscaped('EFFECTIVE_DATE_END');
		$payment 		= $zf_filter->getEscaped('CCY');
		$status 		= $zf_filter->getEscaped('STATUS');
		$transtype 		= $zf_filter->getEscaped('TRANSFER_TYPE');

	     if($filter == null)
		{	
			// $datefrom 		= (date("d/m/Y"));
			// $dateto 		= (date("d/m/Y"));
			// $effectivefrom 	= (date("d/m/Y"));
			// $effectiveend 	= (date("d/m/Y"));
			// $this->view->fDateFrom  	= (date("d/m/Y"));
			// $this->view->fDateTo  		= (date("d/m/Y"));
			// $this->view->effectivefrom  = (date("d/m/Y"));
			// $this->view->effectiveend  	= (date("d/m/Y"));
		}
		
		if($filter_clear == '1'){
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';
			$datefrom = '';
			$dateto = '';

			$this->view->effectivefrom  = '';
			$this->view->effectiveend  	= '';
			$effectivefrom 	= '';
			$effectiveend 	= '';
			
		}
		
		if($filter == null || $filter ==TRUE)
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;	 
			if(!empty($datefrom))
		            {
		            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
						$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
		            }
		            
		    if(!empty($dateto))
		            {
		            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
						$dateto    = $FormatDate->toString($this->_dateDBFormat);
		            }
			
			if(!empty($datefrom) && empty($dateto))
		            $select2->where("DATE(PS_UPLOADED) >= ".$this->_db->quote($datefrom));
		            
		   	if(empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(PS_UPLOADED) <= ".$this->_db->quote($dateto));
		            
		    if(!empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(PS_UPLOADED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
			
			$this->view->effectivefrom 	= $effectivefrom;
			$this->view->effectiveend 	= $effectiveend;	 
			if(!empty($effectivefrom))
            {
            	$FormatDate 	= new Zend_Date($effectivefrom, $this->_dateDisplayFormat);
				$effectivefrom  = $FormatDate->toString($this->_dateDBFormat);	
            }
		            
		    if(!empty($effectiveend))
            {
            	$FormatDate 	= new Zend_Date($effectiveend, $this->_dateDisplayFormat);
				$effectiveend   = $FormatDate->toString($this->_dateDBFormat);
            }
			
			if(!empty($effectivefrom) && empty($effectiveend))
		            $select2->where("DATE(PS_EFDATE) >= ".$this->_db->quote($effectivefrom));
		            
		   	if(empty($effectivefrom) && !empty($effectiveend))
		            $select2->where("DATE(PS_EFDATE) <= ".$this->_db->quote($effectiveend));
		            
		    if(!empty($effectivefrom) && !empty($effectiveend))
		            $select2->where("DATE(PS_EFDATE) between ".$this->_db->quote($effectivefrom)." and ".$this->_db->quote($effectiveend));

			if($filter == TRUE)
		    {		
		    	
			    if($payment != null)
			    {
			    	$this->view->payment = $payment;
			       	$select2->where("PS_CCY LIKE ".$this->_db->quote('%'.$payment.'%'));
			    }
			    
		     	if($transtype != null)
		     	{
			    	$this->view->transtype = $transtype;
			    	$select2->where("PS_TYPE LIKE ".$this->_db->quote($transtype));
		     	}
			    
			    if($status != null)
			    {
			    	$this->view->status = $status;
			    	//$status = 
			    	$select2->where("VALIDATION LIKE ".$this->_db->quote($status));
			    }
			}
		}

		if(!empty($dataParamValue)){

				$this->view->lastuploadedstart 	= $dataParamValue['LAST_UPLOADED_DATE'];
	    		$this->view->lastuploadedend 	= $dataParamValue['LAST_UPLOADED_DATE_END'];
	    		$this->view->effectivestart 	= $dataParamValue['EFFECTIVE_DATE'];
	    		$this->view->effectiveend 		= $dataParamValue['EFFECTIVE_DATE_END'];
	    		
	    	  	unset($dataParamValue['LAST_UPLOADED_DATE_END']);
				unset($dataParamValue['EFFECTIVE_DATE_END']);    	  	

					foreach ($dataParamValue as $key => $value) {
						$wherecol[]	= $key;
						$whereval[] = $value;
					}

		        $this->view->wherecol     = array_unique($wherecol);
	        	$this->view->whereval     = array_unique($whereval);
		     
		      }

		$this->view->filter = $filter;
		$this->paging($select2); 

		
		if($this->_request->isPost() )
		{



		    $this->_request->getParams();
		    
		    $TYPE = $this->_request->getParam('BULK_TYPE');
		    if($TYPE == '0'){
			$filter = new Application_Filtering();
			$confirm = false;
			$error_msg[0] = "";

			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");

			

			
			if(!$ACCTSRC)
			{
				$error_msg[0] = $this->language->_('Source Account cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if(!$PS_EFDATE)
			{
				$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Payment Date cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else
			{
				
				$acct = $this->_db->fetchRow(
						$this->_db->select()
						->from(array('C' => 'M_CUSTOMER_ACCT'))
						->where("ACCT_NO = ".$this->_db->quote($ACCTSRC))
						->limit(1)
					);
	
				$validateacct   = new ValidateAccountSource($ACCTSRC,$this->_custIdLogin,$this->_userIdLogin,$acct['CCY_ID']);
				
				$PECode = $this->_db->fetchRow(
					$this->_db->select()
					->from(array('C' => 'M_USER'))
					->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CODE'))
					->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
					->limit(1)
				);

				if ($validateacct->checkValidateRdnAcct($this->_custIdLogin,$PECode['CUST_CODE']) == FALSE){
					$error_msg[0] = $ACCTSRC.' '.$this->language->_('accounts are not allowed to do this transaction').".";
					$this->view->error 		= true;
					$this->view->report_msg	= $this->displayError($error_msg);
	
				}
				
				$paramSettingID = array('range_futuredate', 'auto_release_payment');
				
				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only
				
				$adapter = new Zend_File_Transfer_Adapter_Http();
				
				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'txt'));
				$extensionValidator->setMessage(
					$this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv or *.txt'
				);
				
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);
				
				$adapter->setValidators ( array (			
					$extensionValidator, 
					$sizeValidator,
				));
				
				if ($adapter->isValid ()) 
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
					
					$adapter->addFilter ( 'Rename',$newFileName  );
										
					$getTypeArr = explode(".", $sourceFileName);
					$getType = $getTypeArr[1];

					if (strtolower($getType) == "csv"){

						if ($adapter->receive ())
						{
							//PARSING CSV HERE
							$csvData = $this->parseCSV($newFileName);
							//after parse delete document temporary
							@unlink($newFileName);
							//end 
							
							$totalRecords = count($csvData);
							if($totalRecords)
							{
								unset($csvData[0]);
								$totalRecords = count($csvData);
							}
										
							if($totalRecords)
							{
								if($totalRecords <= $this->_maxRow)
								{
									$rowNum = 0;
															 
									$paramPayment = array( 	"CATEGORY"      	=> "BULK CREDITT",
															"FROM"       		=> "I",
															"PS_NUMBER"     	=> "",
															"PS_SUBJECT"   	 	=> $PS_SUBJECT,
															"PS_EFDATE"     	=> $PS_EFDATE,
															"_dateFormat"    	=> $this->_dateDisplayFormat,
															"_dateDBFormat"    	=> $this->_dateDBFormat,
															"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
															"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
															"_createPB"     	=> $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
															"_createDOM"    	=> $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
															"_createREM"    	=> false,        // cannot create REM trx 
															"ISRDN"      		=> "1",	
														  );  
									
									$paramTrxArr = array();
									
									foreach ( $csvData as $row ) 
									{										
										if(count($row)==5)
										{
											$rowNum++;
											$benefAcct = trim($row[0]);
											$ccy = strtoupper(trim($row[1]));
											$amount = trim($row[2]);
											$message = trim($row[3]);
											$addMessage = trim($row[4]);
												
											$fullDesc = array(
												'BENEFICIARY_ACCOUNT' => $benefAcct, 
												'BENEFICIARY_ACCOUNT_CCY' => $ccy, 
												'TRA_AMOUNT' => $amount, 
												'TRA_MESSAGE' => $message, 
												'REFNO' => $addMessage, 
												'TRANSFER_TYPE' => 'PB',
											);
	
											$filter = new Application_Filtering();
											
											$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
											$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
											$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
											$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
											$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
	
											$TRANSFER_TYPE 		= 'PB';											
											$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
											$chargeAmt = '0';
																						
											$filter->__destruct();
											unset($filter);
																 
											$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
																"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
																"TRANSFER_FEE" 				=> $chargeAmt,	
																"TRA_MESSAGE" 				=> $TRA_MESSAGE,	
																"TRA_REFNO" 				=> $TRA_REFNO,
																"ACCTSRC" 					=> $ACCTSRC,	
																"ACBENEF" 					=> $ACBENEF,	
																"ACBENEF_CCY" 				=> $ACBENEF_CCY,	
																"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,	
				
															 );	
											
											array_push($paramTrxArr,$paramTrx);
										}
										else
										{				
											$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
											break;
										}			
									}
								}
								// kalo jumlah trx lebih dari setting
								else
								{				
									$error_msg[0] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
								}	
								
								// kalo gak ada error
								if(!$error_msg[0])
								{
									$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
									$resWs = array();
									
									$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);
									$payment 		= $validate->getPaymentInfo();
									$sourceAccountType = $resWs['accountType'];
				
									if($validate->isError() === false)	// payment data is valid
									{
										$confirm = true;
									
										$validate->__destruct();
										unset($validate);
									}
									else
									{
										$errorMsg 		= $validate->getErrorMsg();
										$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
										
										$validate->__destruct();
										unset($validate);
										
										if($errorMsg)
										{
											$error_msg[0] = $errorMsg;
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
										}
										else
										{
											$confirm = true;
										}
									}
								}
							}
							else //kalo total record = 0
							{							
								$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}	
						}
						
					}elseif (strtolower($getType) == "txt"){
						
						$success = 0;
						$adapter->receive();
						$myFile = file_get_contents($newFileName);
						$arry_myFile = explode("\n", $myFile);
						@unlink($newFileName);
						
						if($adapter->receive())
						{
							unset($arry_myFile[0]);
							
							$paramPayment = array( 	"CATEGORY"      	=> "BULK CREDITT",
															"FROM"       		=> "I",
															"PS_NUMBER"     	=> "",
															"PS_SUBJECT"   	 	=> $PS_SUBJECT,
															"PS_EFDATE"     	=> $PS_EFDATE,
															"_dateFormat"    	=> $this->_dateDisplayFormat,
															"_dateDBFormat"    	=> $this->_dateDBFormat,
															"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
															"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
															"_createPB"     	=> $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
															"_createDOM"    	=> $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
															"_createREM"    	=> false,        // cannot create REM trx 
															 "ISRDN"      		=> "1",	
														  );  
									
									$paramTrxArr = array();
							
							foreach ($arry_myFile as $row) 
							{	
								$getRowArr = explode(",", $row);
								
								if(count($getRowArr)==5)
								{
									$benefAcct = trim($getRowArr[0]);								
									$ccy = strtoupper(trim($getRowArr[1]));
									$amount = trim($getRowArr[2]);
									$message = trim($getRowArr[3]);
									$addMessage = trim($getRowArr[4]);	
																	
										
									$fullDesc = array(
										'BENEFICIARY_ACCOUNT' => $benefAcct, 
										'BENEFICIARY_ACCOUNT_CCY' => $ccy, 
										'TRA_AMOUNT' => $amount, 
										'TRA_MESSAGE' => $message, 
										'REFNO' => $addMessage, 
										'TRANSFER_TYPE' => 'PB',
									);
	
									$filter = new Application_Filtering();
									
									$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
									$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
									$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
									$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
									$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
	
									$TRANSFER_TYPE 		= 'PB';											
									$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
									$chargeAmt = '0';
																				
									$filter->__destruct();
									unset($filter);
														 
									$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"TRANSFER_FEE" 				=> $chargeAmt,	
														"TRA_MESSAGE" 				=> $TRA_MESSAGE,	
														"TRA_REFNO" 				=> $TRA_REFNO,
														"ACCTSRC" 					=> $ACCTSRC,	
														"ACBENEF" 					=> $ACBENEF,	
														"ACBENEF_CCY" 				=> $ACBENEF_CCY,	
														"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,	
		
													 );	
									
									array_push($paramTrxArr,$paramTrx);
									$success = 1;
								}else{
									$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
								}								
								
							}
						}
						
						if($success == 1)
						{
							$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
							$resWs = array();
		
							$paramPayment["ISRDN"]  = '1';
							
							$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);
							$payment 		= $validate->getPaymentInfo();
							//print_r($resWs);die;
							$sourceAccountType = $resWs['accountType'];
		
							if($validate->isError() === false)	// payment data is valid
							{
								$confirm = true;
							
								$validate->__destruct();
								unset($validate);
							}
							else
							{
								$errorMsg 		= $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								
								$validate->__destruct();
								unset($validate);
								
								if($errorMsg)
								{
									$error_msg[0] = $errorMsg;
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
								}
								else
								{
									$confirm = true;
								}
							}
						}
					}						
					
				} 
				else 
				{		
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
			}
			
			if($confirm)
			{
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				//print_r($payment);die;
				$content['sourceAccountType'] = $sourceAccountType;
				
				
				$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
				$sessionNamespace->content = $content;
				$this->_redirect('/rdntrans/bulkcredit/confirm');
			}
			
			$this->view->PSSUBJECT = $PS_SUBJECT; 
			$this->view->ACCTSRC = $ACCTSRC;
			$this->view->PSEFDATE = $PS_EFDATE;


			}else if($TYPE == '1'){

				$filter = new Application_Filtering();
			$confirm = false;

			$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 			= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACBENEF 			= $filter->filter($this->_request->getParam('ACBENEF'), "ACCOUNT_NO");
//			$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
//			$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
//			$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");

			$minLen = 10;
			$maxLen = 20;
			$error_msg[0] = "";
			
				

			if (Zend_Validate::is($ACBENEF, 'NotEmpty') == false)
				$error_msg[0] = $this->language->_('Beneficiary Account cannot be left blank').".";
			elseif (Zend_Validate::is($ACBENEF, 'Digits') == false)
				$error_msg[0] = $this->language->_('Beneficiary Account must be numeric').".";
			elseif (strlen($ACBENEF) < $minLen || strlen($ACBENEF) > $maxLen)
				//$error_msg[0] = "Beneficiary Account length should be between $minLen and $maxLen.";
				$error_msg[0] = $this->language->_('Beneficiary Account length should be between 10 and 20.')."";

			/*elseif ($ACBENEF_ALIAS == "")
				$error_msg[0] = $this->language->_('Beneficiary Alias Name cannot be left blank.')."";*/
//			elseif (strlen($ACBENEF_ALIAS) > 35)
//				$error_msg[0] = $this->language->_('Maximum lengths of Alias Name is 35 characters. Please correct it').".";
//			else if ($ACBENEF_CCY == "")
//				$error_msg[0] = $this->language->_('Currency cannot be left blank').".";
			else if(!$PS_EFDATE)
				$error_msg[0] = $this->language->_('Payment Date can not be left blank').".";
			else
			{
				
				$acct = $this->_db->fetchRow(
					$this->_db->select()
					->from(array('C' => 'M_CUSTOMER_ACCT'))
					->where("ACCT_NO = ".$this->_db->quote($ACBENEF))
					->limit(1)
				);

				$validateacct   = new ValidateAccountSource($ACBENEF,$this->_custIdLogin,$this->_userIdLogin,$acct['CCY_ID']);				
				$PECode = $this->_db->fetchRow(
					$this->_db->select()
					->from(array('C' => 'M_USER'))
					->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CODE'))
					->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
					->limit(1)
				);
				//print_r($PECode);die;
				if ($validateacct->checkValidateRdnAcct($this->_custIdLogin,$PECode['CUST_CODE']) == FALSE){
					$error_msg[0] = $ACBENEF.' '.$this->language->_('accounts are not allowed to do this transaction').".";
				}
				
				
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );

				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'txt'));
				$extensionValidator->setMessage(
					$this->language->_('Extension file must be').' *.csv or *.txt'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );
					
					$getTypeArr = explode(".", $sourceFileName);
					$getType = $getTypeArr[1];

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}

						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{
								$rowNum = 0;

								$paramPayment = array( "CATEGORY"      		=> "BULK DEBET",
													   "FROM"       		=> "I",
													   "PS_NUMBER"     		=> "",
													   "PS_SUBJECT"    		=> $PS_SUBJECT,
													   "PS_EFDATE"     		=> $PS_EFDATE,
													   "_dateFormat"    	=> $this->_dateDisplayFormat,
													   "_dateDBFormat"    	=> $this->_dateDBFormat,
													   "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
													   "_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
													   "_createPB"     		=> $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
													   "_createDOM"    		=> false,        // cannot create DOM trx
													   "_createREM"    		=> false,        // cannot create REM trx
													   "ISRDN"      		=> "1",	
													  );

								$paramTrxArr = array();
								$totalamt = 0;
								foreach ( $csvData as $row )
								{									
									if(count($row)==11)
									{										
										$rowNum++;
										$sourceAcct = trim($row[0]);
										$sourceAcctName = trim($row[1]);
										$ccy = strtoupper(trim($row[2]));
										$stockCode = trim($row[3]);
										$stockName = trim($row[4]);
										$qty = strtoupper(trim($row[5]));
										$price = trim($row[6]);
										$amount = trim($row[7]);
										$message = trim($row[8]);
										$addMessage = trim($row[9]);
										$email = strtoupper(trim($row[10]));
				
										$totalamt = $totalamt+$amount;
				
										$filter = new Application_Filtering();

										$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
										$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
										$EMAIL 				= $filter->filter($email, "EMAIL");
										// $ACCTSRC 			= $filter->filter($sourceAcct, "ACCOUNT_NO");
										$ACCTSRC 			= $sourceAcct;
										$TRANSFER_TYPE 		= 'PB';

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										$filter->__destruct();
										unset($filter);
										$srcData = $this->_db->select()
										->from(array('M_BENEFICIARY'),array('BENEFICIARY_ALIAS'))
										->where('BENEFICIARY_ACCOUNT = ?', $ACBENEF)
										->limit(1)
										;
										//echo $srcData;die;
										$acsrcData = $this->_db->fetchRow($srcData);
										//print_r($acsrcData);die;
										$paramTrx = array(	"TRANSFER_TYPE" 	=> $TRANSFER_TYPE,
															"TRA_AMOUNT" 		=> $TRA_AMOUNT_num,
															"TRA_MESSAGE" 		=> $TRA_MESSAGE,
															"TRA_REFNO" 		=> $TRA_REFNO,
															"ACCTSRC" 			=> $ACCTSRC,
															"ACBENEF" 			=> $ACBENEF,
															"ACBENEF_NAME"		=> $sourceAcctName,
															"ACBENEF_CCY" 		=> $ACBENEF_CCY,
															"ACBENEF_EMAIL" 	=> $EMAIL,

														// for Beneficiary data, except (bene CCY and email), must be passed by reference
															"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
															"ACBENEF_ALIAS" 			=> $acsrcData['BENEFICIARY_ALIAS'],	
															"STOCK_CODE" 			=> $stockCode ,
															"STOCK_NAME" 			=> $stockName,
															"QTY" 				=> $qty,
															"PRICE" 			=> $price,
															"TOTAL" 			=> $amount																			
														 );

										array_push($paramTrxArr,$paramTrx);
									}
									else
									{										
										$error_msg[0] = $this->language->_('Wrong File Format').'';
										break;
									}
								}
							}
							// kalo jumlah trx lebih dari setting
							else
							{
								$error_msg[0] = 'The number of rows to be imported should not more than '.$this->_maxRow.'.';
							}

							// kalo gak ada error
							if(!$error_msg[0])
							{
								$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								//print_r($paramPayment);die;
								$paramPayment['TOTAL_AMOUNT_RDN'] = $totalamt;
								//print_r($paramTrxArr);die;
								$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr);
								
								$payment 		= $validate->getPaymentInfo();								
								
								$i = 0;
								foreach($payment['acctsrcArr'] as $key=>$dataAcctType){
									//Zend_Debug::dump($dataAcctType);
									$paramTrxArr[$i++]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
								}

								if($validate->isError() === false)	// payment data is valid
								{
									$confirm = true;

									$validate->__destruct();
									unset($validate);
								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
									//var_dump($errorTrxMsg);die;
									$validate->__destruct();
									unset($validate);

									if($errorMsg)
									{
										$error_msg[0] = $errorMsg;
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else //kalo total record = 0
						{
							//$error_msg[0] = 'Wrong File Format. There is no data on csv File.';
							$error_msg[0] = $this->language->_('Wrong File Format').'.';
						}
					}
				}
				else
				{
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
				}

			}

			if($confirm)
			{
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				//echo '<pre>';
				//print_r($content);die;
				$sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
				$sessionNamespace->content = $content;
				$this->_redirect('/rdntrans/bulkdebit/confirm');
			}

			$this->view->error 		= true;
			//$error_msg[0] = $this->language->_('Error').': '.$error_msg[0];
			$error_msg[0] = $error_msg[0];
			$this->view->report_msg	= $this->displayError($error_msg);

			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACBENEF = $ACBENEF;
			$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
			$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
			$this->view->CURR_CODE = $ACBENEF_CCY;
			$this->view->PSEFDATE = $PS_EFDATE;




			}else if($TYPE == '3'){
					$adapter1 					= new Zend_File_Transfer_Adapter_Http();
			$params 					= $this->_request->getParams();
			//print_r($params);die;
			$sourceFileName 			= $adapter1->getFileName();
			
			if($sourceFileName == null)
			{
				$sourceFileName = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter1->getFileName()), 0);
			}
			
			$paramsName['sourceFileName'] 	= $sourceFileName;
			
			$filtersName = array(
									'sourceFileName'		=> array('StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File size too big or left blank"
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			if($zf_filter_input_name->isValid())
			{
				$filter 	= new Application_Filtering();
				$adapter 	= new Zend_File_Transfer_Adapter_Http ();
				$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
				$max		= $this->getSetting('max_import_single_payment');
				
				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'txt'));
				$extensionValidator->setMessage(
													'Error: Extension file must be *.csv or *.txt'
												);
				
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
											'Error: File size must not more than '.$this->getSetting('Fe_attachment_maxbyte')
										);
						
				$adapter->setValidators(array($extensionValidator,$sizeValidator));
					
				if ($adapter->isValid ()) 
				{
					//$sourceFileName = $adapter->getFileName();
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
						
					$adapter->addFilter ( 'Rename',$newFileName  );
					
					$getTypeArr = explode(".", $sourceFileName);
					$getType = $getTypeArr[1];
					
					if (strtolower($getType) == "csv"){
						if ($adapter->receive ())
						{
							$Csv = new Application_Csv ($newFileName,",");
							$csvData = $Csv->readAll ();
							//print_r($csvData);die;
							if(!empty($csvData['0']['0'])){
							$searchString = ';';

							 if( strpos($csvData['0']['0'], $searchString) !== false ) {
								 try {
									$Csv = new Application_Csv (  $fileName, $separator = ";" );
										$csvData = $Csv->readAll ();	
								} catch (Exception $e) {
									$this->view->error = true;
									//break;
								}						 }

							}
							@unlink($newFileName);
							
							$totalRecords = count($csvData);
							
							if($totalRecords)
							{
								unset($csvData[0]);
								$totalRecords = count($csvData);
							}
							
							if ($totalRecords){
								if($totalRecords <= $max)
								{
									$no =0;
									foreach ( $csvData as $columns )
									{
										if(count($columns)==9)
										{
											
											if(trim(strtoupper($columns[3]))=='USD'){											
												$this->view->error5 		= true;
												break;
											}
											$params['PAYMENT SUBJECT'] 			= trim($columns[0]);
											$params['SOURCE ACCT NO'] 			= trim($columns[1]);
											$params['BENEFICIARY ACCT NO'] 		= trim($columns[2]);
											$params['CCY'] 						= trim($columns[3]);
											$params['AMOUNT'] 					= trim($columns[4]);
											$params['MESSAGE'] 					= trim($columns[5]);
											$params['ADDITIONAL MESSAGE'] 		= trim($columns[6]);
											$params['PAYMENT DATE'] 			= trim($columns[7]);
											//$params['TRANSFER TYPE'] 			= trim($columns[8]);
											$params['BANK CODE'] 				= trim($columns[8]);
												
											$PS_SUBJECT 		= $filter->filter($params['PAYMENT SUBJECT'],"PS_SUBJECT");
											$PS_EFDATE 			= $filter->filter($params['PAYMENT DATE'],"PS_DATE");
											$TRA_AMOUNT 		= $filter->filter($params['AMOUNT'],"AMOUNT");
											$TRA_MESSAGE 		= $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
											$TRA_REFNO 			= $filter->filter($params['ADDITIONAL MESSAGE'],"TRA_REFNO");
											$ACCTSRC 			= $filter->filter($params['SOURCE ACCT NO'],"ACCOUNT_NO");
											$ACBENEF 			= $params['BENEFICIARY ACCT NO']; //$filter->filter($params['BENEFICIARY ACCT NO'],"ACCOUNT_NO");
											$ACBENEF_BANKNAME 	= $filter->filter($params['BENEFICIARY NAME'],"ACCOUNT_NAME");
											$ACBENEF_CCY 		= $filter->filter($params['CCY'],"SELECTION");
											$CLR_CODE			= $filter->filter($params['BANK CODE'], "BANK_CODE");
											$TRANSFER_TYPE 		= "RTGS";
												
											$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
											
											if($TRANSFER_TYPE == 'RTGS'){
												$chargeType = '1';
												$select = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType);
												$resultSelecet = $this->_db->FetchAll($select);
												$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
												
												//$param['TRANSFER_FEE'] = $chargeAmt;
											}
											else if($TRANSFER_TYPE == 'SKN'){
												$chargeType1 = '2';
												$select1 = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType1);
												$resultSelecet1 = $this->_db->FetchAll($select1);
												$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
												
												//$param['TRANSFER_FEE'] = $chargeAmt1;
											}
											else{
												$chargeAmt = '0';
												//$param['TRANSFER_FEE'] = $chargeAmt2;
											}

			
											$paramPayment = array(
													"CATEGORY" 					=> "SINGLE PAYMENT",
													"FROM" 						=> "I",				// F: Form, I: Import
													"PS_NUMBER"					=> "",
													"PS_SUBJECT"				=> $PS_SUBJECT,
													"PS_EFDATE"					=> $PS_EFDATE,
													"_dateFormat"				=> $this->_dateUploadFormat,
													"_dateDBFormat"				=> $this->_dateDBFormat,
													"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
													"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
													"_createPB"					=> $this->view->hasPrivilege('CRIP'),								// cannot create PB trx
													"_createDOM"				=> $this->view->hasPrivilege('CRDI'),	// privi CDFT (Create Domestic Fund Transfer)
													"_createREM"				=> $this->view->hasPrivilege('CRIR'),								// cannot create REM trx
													"ISRDN"      				=> "1",
											);

											$select1 = $this->_db->select()
																->from('M_BENEFICIARY',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("BENEFICIARY_ACCOUNT = ?",$ACBENEF);
												$resultdata = $this->_db->FetchAll($select1);
												$identificationType = $resultdata['0']['BENEFICIARY_ID_TYPE'];
												$benenumber = $resultdata['0']['BENEFICIARY_ID_NUMBER'];
												$paramTrxArr[0] = array(
													"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
													"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
													"TRANSFER_FEE" 				=> $chargeAmt,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_REFNO" 				=> $TRA_REFNO,
													"ACCTSRC" 					=> $ACCTSRC,
													"ACBENEF" 					=> $ACBENEF,
													"ACBENEF_CCY" 				=> $ACBENEF_CCY,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
													"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
													"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
													// 																"ACBENEF_ADDRESS1" 			=> $BANK_CITY,

													"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
													"BANK_CODE" 				=> $CLR_CODE,
													"LLD_BENEIDENTIF"			=>$identificationType,
													"LLD_BENENUMBER"			=>$benenumber,
													"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
													"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
													"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
													"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
													"BANK_NAME" 	=> $BANK_NAME,
													"KSIE"						=> "1",
											
											);
												
											$arr[$no]['paramPayment'] = $paramPayment;
											$arr[$no]['paramTrxArr'] = $paramTrxArr;
										}
										else
										{
											$this->view->error4 		= true;
											break;
										}
										$no++;
									}
										
									if(!$this->view->error && !$this->view->error4 && !$this->view->error5)
									{
										$resWs = array();
										$err 	= array();
										
										$resultVal	= $validate->checkCreate($arr, $resWs);
										$payment 	= $validate->getPaymentInfo();
										$errorTrxMsg 	= $validate->getErrorTrxMsg();
										
										//Zend_Debug::dump($resWs);die;
										$i = 0;
										foreach($resWs as $key=>$dataAcctType){
											//Zend_Debug::dump($dataAcctType);	
											$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
										}
										
											
										//die;
										
										$sourceAccountType 	= $resWs['accountType'];
										
										$content['payment'] = $payment;
										$content['arr'] 	= $arr;
										$content['errorTrxMsg'] 	= $errorTrxMsg;
										$content['sourceAccountType'] 	= $sourceAccountType;
																
										$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
										$sessionNamespace->content = $content;
											
										$this->_redirect('/rdntrans/importrtgsksei/confirm');
									}
										
								}
								else
								{
									$this->view->error2 = true;
									$this->view->max 	= $max;
								}
							}else{
								$this->view->error = true;
							}
						}
					}elseif (strtolower($getType) == "txt"){
						
						$success = 0;
						$adapter->receive();
						$myFile = file_get_contents($newFileName);
						$arry_myFile = explode("\n", $myFile);
						@unlink($newFileName);
						
						if($adapter->receive())
						{
						
							unset($arry_myFile[0]);
							
							if (!empty($arry_myFile)){
								$no =0;
								foreach ($arry_myFile as $row) 
								{
									$getRowArr = explode(",", $row);									
									if(count($getRowArr)==9)
									{
										if(trim(strtoupper($getRowArr[3]))=='USD'){											
											$this->view->error5 		= true;
											break;
										}
										
										
										$params['PAYMENT SUBJECT'] 			= trim($getRowArr[0]);
										$params['SOURCE ACCT NO'] 			= trim($getRowArr[1]);
										$params['BENEFICIARY ACCT NO'] 		= trim($getRowArr[2]);
										$params['CCY'] 						= trim($getRowArr[3]);
										$params['AMOUNT'] 					= trim($getRowArr[4]);
										$params['MESSAGE'] 					= trim($getRowArr[5]);
										$params['ADDITIONAL MESSAGE'] 		= trim($getRowArr[6]);
										$params['PAYMENT DATE'] 			= trim($getRowArr[7]);
										$params['BANK CODE'] 				= trim($getRowArr[8]);
											
										$PS_SUBJECT 		= $filter->filter($params['PAYMENT SUBJECT'],"PS_SUBJECT");
										$PS_EFDATE 			= $filter->filter($params['PAYMENT DATE'],"PS_DATE");
										$TRA_AMOUNT 		= $filter->filter($params['AMOUNT'],"AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($params['ADDITIONAL MESSAGE'],"TRA_REFNO");
										$ACCTSRC 			= $filter->filter($params['SOURCE ACCT NO'],"ACCOUNT_NO");
										$ACBENEF 			= $params['BENEFICIARY ACCT NO']; //$filter->filter($params['BENEFICIARY ACCT NO'],"ACCOUNT_NO");
										$ACBENEF_BANKNAME 	= $filter->filter($params['BENEFICIARY NAME'],"ACCOUNT_NAME");
										$ACBENEF_CCY 		= $filter->filter($params['CCY'],"SELECTION");
										$CLR_CODE			= $filter->filter($params['BANK CODE'], "BANK_CODE");
										$TRANSFER_TYPE 		= "RTGS";
											
										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);								
										
										$chargeType = '1';
										$select = $this->_db->select()
														->from('M_CHARGES_OTHER',array('*'))
														->where("CUST_ID = ?",$this->_custIdLogin)
														->where("CHARGES_TYPE = ?",$chargeType);
										$resultSelecet = $this->_db->FetchAll($select);
										$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];	
		
										$paramPayment = array(
												"CATEGORY" 					=> "SINGLE PAYMENT",
												"FROM" 						=> "I",				// F: Form, I: Import
												"PS_NUMBER"					=> "",
												"PS_SUBJECT"				=> $PS_SUBJECT,
												"PS_EFDATE"					=> $PS_EFDATE,
												"_dateFormat"				=> $this->_dateUploadFormat,
												"_dateDBFormat"				=> $this->_dateDBFormat,
												"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
												"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
												"_createPB"					=> $this->view->hasPrivilege('CRIP'),								// cannot create PB trx
												"_createDOM"				=> $this->view->hasPrivilege('CRDI'),	// privi CDFT (Create Domestic Fund Transfer)
												"_createREM"				=> $this->view->hasPrivilege('CRIR'),								// cannot create REM trx
												"ISRDN"      				=> "1",
										);
		
										$select1 = $this->_db->select()
															->from('M_BENEFICIARY',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("BENEFICIARY_ACCOUNT = ?",$ACBENEF);
											$resultdata = $this->_db->FetchAll($select1);
											$identificationType = $resultdata['0']['BENEFICIARY_ID_TYPE'];
											$benenumber = $resultdata['0']['BENEFICIARY_ID_NUMBER'];
											$paramTrxArr[0] = array(
												"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
												"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
												"TRANSFER_FEE" 				=> $chargeAmt,
												"TRA_MESSAGE" 				=> $TRA_MESSAGE,
												"TRA_REFNO" 				=> $TRA_REFNO,
												"ACCTSRC" 					=> $ACCTSRC,
												"ACBENEF" 					=> $ACBENEF,
												"ACBENEF_CCY" 				=> $ACBENEF_CCY,
												"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
												"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
												"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
												// 																"ACBENEF_ADDRESS1" 			=> $BANK_CITY,
		
												"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
												"BANK_CODE" 				=> $CLR_CODE,
												"LLD_BENEIDENTIF"			=>$identificationType,
												"LLD_BENENUMBER"			=>$benenumber,
												"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
												"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
												"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
												"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
												"BANK_NAME" 				=> $BANK_NAME,
												"KSIE"						=> "1",
										
										);
											
										$arr[$no]['paramPayment'] = $paramPayment;
										$arr[$no]['paramTrxArr'] = $paramTrxArr;
										
										$success = 1;								
										$no++;	
									}else{
										$this->view->error 		= true;
										break;	
									}
									
								}		
							}else{								
								$this->view->error 		= true;								
							}
						}
						
						if($success == 1)
						{
							$resWs = array();
							$err 	= array();
							//Zend_Debug::dump($arr);die;
							//echo '<pre>';
							//			print_r($arr);die;
							$resultVal	= $validate->checkCreate($arr, $resWs);
							$payment 	= $validate->getPaymentInfo();
							$errorTrxMsg 	= $validate->getErrorTrxMsg();
							
							//Zend_Debug::dump($resWs);die;
							$i = 0;
							foreach($resWs as $key=>$dataAcctType){
								//Zend_Debug::dump($dataAcctType);	
								$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
							}
							
							$sourceAccountType 	= $resWs['accountType'];
							
							$content['payment'] = $payment;
							$content['arr'] 	= $arr;
							$content['errorTrxMsg'] 	= $errorTrxMsg;
							$content['sourceAccountType'] 	= $sourceAccountType;
													
							$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
							$sessionNamespace->content = $content;
								
							$this->_redirect('/rdntrans/importrtgsksei/confirm');
						}
					}						
					
				}
				else
				{
//die;
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}
			}
			else
			{
				$this->view->error3		= true;
			}


			}



		}
		Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
	}
	
	private function resData($benefAccount){
			$select	= $this->_db->select()
								->from(array('B'	 			=> 'M_BENEFICIARY'), array('BANK_NAME','BENEFICIARY_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_CITIZENSHIP','BENEFICIARY_RESIDENT','BENEFICIARY_ID_NUMBER','BENEFICIARY_ID_TYPE','BENEFICIARY_CITY_CODE','BENEFICIARY_CATEGORY')
									   );
			$select->where("B.BENEFICIARY_ACCOUNT = ?", $benefAccount);
			
			$bene = $this->_db->fetchAll($select);
			return $bene;
	}
	
	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$data = $sessionNamespace->content;
//		die;
		$sourceAcct = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']];
//		echo '<pre>';
//var_dump($data['paramTrxArr'][0]['ACCTSRC']);
		//print_r($sourceAcct);
		//print_r($data);die;
		$this->view->CCY = $sourceAcct['CCY_ID'];
		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'].' / '.$sourceAcct['accountName'];
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$param = array();
		$AccArr 	  = $CustomerUser->getAccounts($param,1);
		//print_r($AccArr);
		//print_r($data['paramTrxArr'][0]['ACCTSRC']);	die;
		foreach($AccArr as $val){
		//var_dump($val['ACCT_NO']);var_dump($data['paramTrxArr'][0]['ACCTSRC']);
			if($val['ACCT_NO']==$data['paramTrxArr'][0]['ACCTSRC']){
//die('here');
				$sourceAcct['ACCT_ALIAS'] = $val['ACCT_ALIAS_NAME'];
			}
		}//die;
		if($sourceAcct['ACCT_ALIAS']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS'];
		$this->view->ACCTSRC .= ' / '.$sourceAcct['CCY_ID'];
		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];
		//echo '<pre>';print_r($data);die;
		$chargesAmt = array();
		$totalChargesAmt = 0;
		
		foreach($data['paramTrxArr'] as $row)
		{
//			$benefAccount = $row["ACBENEF"];
//			if($row["TRANSFER_TYPE"] !="PB"){
//				$dataRes = $this->resData($benefAccount);
//				
//				$row['BENEFICIARY_ID_NUMBER'] = $dataRes[0]['BENEFICIARY_ID_NUMBER'];
//				$row['BENEFICIARY_ID_TYPE'] = $dataRes[0]['BENEFICIARY_ID_TYPE'];
//				$row['BENEFICIARY_CITY_CODE'] = $dataRes[0]['BENEFICIARY_CITY_CODE'];
//				$row['BENEFICIARY_CATEGORY'] = $dataRes[0]['BENEFICIARY_CATEGORY'];
//				$row['BENEFICIARY_BANK_NAME'] = $dataRes[0]['BANK_NAME'];
//				$row['BENEFICIARY_CITIZENSHIP'] = $dataRes[0]['BENEFICIARY_CITIZENSHIP'];
//				$row['BENEFICIARY_RESIDENT'] = $dataRes[0]['BENEFICIARY_RESIDENT'];
//				$row['BENEFICIARY_ACCOUNT_NAME'] = $dataRes[0]['BENEFICIARY_NAME'];
//			}
			
			if(!isset($chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']])) $chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] = 0;
			//$chargesObj = Charges::factory($this->_custIdLogin, $row['TRANSFER_TYPE']);

//			if(empty($row['TRANSFER_TYPE'])){
//				$transferType = '0';
//			}
//			else{
//				$transferType = $row['TRANSFER_TYPE'];
//			}
			$chargesObj = Charges::factory($this->_custIdLogin, isset($row['TRANSFER_TYPE']));
			
			$paramCharges = array("accsrc" => $row['ACCTSRC'], "transferType" => $row['TRANSFER_TYPE']);
			if(!isset($data['errorTrxMsg'][$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']]))
			{
// 				$chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] += $chargesObj->getCharges($paramCharges);
// 				$totalChargesAmt += $chargesObj->getCharges($paramCharges);
			}
		}
		$this->view->chargesAmt = $chargesAmt;
		$this->view->totalChargesAmt = $totalChargesAmt;
		
		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}
		
		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}
		
		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}
		
		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}
				
		$settings = new Settings();
		$this->view->cutOffSKN = $settings->getSetting('cut_off_time_skn');
		$this->view->cutOffRTGS = $settings->getSetting('cut_off_time_rtgs');
		$this->view->cutOffBI = $settings->getSetting('cut_off_time_bi');
		
		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;	
		
		if($this->_request->isPost() )
		{			
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']); 
				$this->_redirect('/rdntrans/bulkcredit');
			}
			
			if ($data["payment"]["countTrxPB"] == 0)	
				$priviCreate = 'CBPI';
			else				
				$priviCreate = 'CBPW';
			
			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= $this->_paymenttype['code']['bulkcredit'];
			$param['PS_CCY']  = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];
			
			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
						'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
						'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
						'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
						'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
//						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'], //ADA
						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
						'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
						'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'], //ADA
						'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
						//'ACBENEF_ADDRESS1' 			=> $row['ACBENEF_ADDRESS1'],
						'BENEFICIARY_RESIDENT' 		=> $row['BENEFICIARY_RESIDENT'],
						'CLR_CODE' 					=> $row['BANK_CODE'],
						'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'], //SKN,RTGS,PB
						'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
						'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
						'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
						'TRA_REFNO' 				=> $row['TRA_REFNO'],
						'sourceAccountType' 		=> $data['sourceAccountType'],
				
						'BENEFICIARY_BANK_NAME' 	=> $row['BANK_NAME'], //ADA
						'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
						'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
						'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
						'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],
				
				);
			}
			
			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = $priviCreate;
			//$param['sourceAccountType'] = $data['sourceAccountType'];
			
			
			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin, TRUE);
//			Zend_Debug::dump($param);die;
			$result = $BulkPayment->createPayment($param);
			if($result)
			{
				unset($_SESSION['confirmBulkCredit']); 
				$this->_redirect('/notification/success');
			}
			else
			{	
				$this->view->error = true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/rdntrans/bulkcredit');
			}
		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
								if(!empty($csvData['0']['0'])){
					$searchString = ';';

					 if( strpos($csvData['0']['0'], $searchString) !== false ) {
						     $Csv = new Application_Csv (  $fileName, $separator = ";" );
				    		     $csvData = $Csv->readAll ();
					 }

				}
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;		
	}
	
	public function documentAction()
  	{ 
		

		$this->_helper->layout()->setLayout('newlayout');

		$this->view->suggestionType = $this->_suggestType;

		$change_id = $this->_getParam('BS_ID');

		$select = $this->_db->select()
		                 // ->from('TEMP_BULKPSLIP',array('*'))
		            ->from(array('A' => 'TEMP_BULKPSLIP'),array())
		            ->join(array('B' => 'TEMP_BULKTRANSACTION'),'A.BS_ID = B.BS_ID',array('A.*','B.SOURCE_ACCOUNT','B.BENEFICIARY_ACCOUNT','B.BENEFICIARY_ACCOUNT_NAME','B.BENEFICIARY_ACCOUNT_CCY'))
		            ->where('A.BS_ID = ?',$change_id);
		                
		$resultdata = $this->_db->fetchAll($select);

		$conf = Zend_Registry::get('config');
		$paymentType = $conf['payment']['type'];
		$paymentTypeFlip = array_flip($paymentType['code']);

		$this->view->paymentType = $paymentType;
		$this->view->paymentTypeFlip = $paymentTypeFlip;

		$frontendOptions = array(
		'lifetime' => 86400,
		'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

		$cacheID = 'USERLIST';

		$userlist = $cache->load($cacheID);
		$this->view->pdfUserlist  = $userlist;

		$this->view->bs_id            = $resultdata[0]['BS_ID'];
		$this->view->ps_subject       = $resultdata[0]['PS_SUBJECT'];
		$this->view->upload_by        = $resultdata[0]['UPLOADBY'];
		$this->view->ef_date          = $resultdata[0]['PS_EFDATE'];
		$this->view->total_amount     = $resultdata[0]['PS_TOTAL_AMOUNT'];
		$this->view->success_amount   = $resultdata[0]['PS_SUCCESS_AMOUNT'];
		$this->view->failed_amount    = $resultdata[0]['PS_FAILED_AMOUNT']; 
		$this->view->ps_type          = $resultdata[0]['PS_TYPE'];  
		$this->view->ps_ccy           = $resultdata[0]['PS_CCY'];    
		$this->view->ps_suc           = $resultdata[0]['PS_SUCCESS_TXAMOUNT'];  
		$this->view->ps_tot           = $resultdata[0]['PS_TXCOUNT'];  
		$this->view->ps_fai           = $resultdata[0]['PS_FAILED_TXAMOUNT'];  
		$this->view->ps_uploaded      = $resultdata[0]['PS_UPLOADED']; 
		$this->view->validation       = $resultdata[0]['VALIDATION'];
		$this->view->source_acct      = $resultdata[0]['SOURCE_ACCOUNT']; 
		$this->view->file_id          = $resultdata[0]['FILE_ID'];
		$this->view->beneficiary_acct = $resultdata[0]['BENEFICIARY_ACCOUNT'];
		$this->view->beneficiary_name = $resultdata[0]['BENEFICIARY_ACCOUNT_NAME'];
		$this->view->beneficiary_ccy  = $resultdata[0]['BENEFICIARY_ACCOUNT_CCY'];

		$downloadURL2 = $this->view->url(array('module'=>'newbatchpayment','controller'=>'index','action'=>'downloadtrx2','txt'=>'1','BS_ID'=>$change_id),null,true);

		$this->view->download2 = $this->view->formButton('download','download',array('class'=>'btngreen hov', 'onclick'=>"window.location = ".$this->_db->quote($downloadURL2).";")); 

		//reupload
		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'payroll');
		$adapterDataPayroll = $this->_db->FetchAll($select);

		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'manytomany');
		$adapterDataMtm = $this->_db->FetchAll($select);

		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'emoney');
		$adapterDataEmoney = $this->_db->FetchAll($select);

		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'onetomany');
		$adapterDataOtm = $this->_db->FetchAll($select);

		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'manytoone');
		$adapterDataMto = $this->_db->FetchAll($select);

      if($this->_request->isPost())
      {
      
        $filter = new Application_Filtering();
        $confirm = false;
        $error_msg[] = "";
        
        $BULK_TYPE  = $this->_request->getParam('ps_type');
        
        if($BULK_TYPE == '4'){
        //awal
          
              $BS_ID  = $this->_request->getParam('bs_id');

              // $where = array(
              //        'BS_ID = ?' => $BS_ID
              //      );
              // $this->_db->delete('TEMP_BULKTRANSACTION',$where);

              $extension = 'txt';
              $fileName = $adapterDataOtm[0]['FILE_PATH'];
              $fixLength = $adapterDataOtm[0]['FIXLENGTH'];
              $fixLengthType = $adapterDataOtm[0]['FIXLENGTH_TYPE'];
              $fixLengthHeader = $adapterDataOtm[0]['FIXLENGTH_HEADER_ORDER'];
              $fixLengthHeaderName = $adapterDataOtm[0]['FIXLENGTH_HEADER_NAME'];
              $fixLengthContent = $adapterDataOtm[0]['FIXLENGTH_CONTENT_ORDER'];
              //$delimitedWith = $adapterDataOtm[0]['DELIMITED_WITH'];
              $delimitedWith = '|';

              $PS_SUBJECT   = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
              $PS_EFDATE    = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
              $ACCTSRC    = $filter->filter($this->_request->getParam('source_acct'), "ACCOUNT_NO");
              $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID");

              $paramSettingID = array('range_futuredate', 'auto_release_payment');

              $settings = new Application_Settings();
              $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
              $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
              $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
              $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

              $adapter = new Zend_File_Transfer_Adapter_Http();
              $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
              $adapter->setDestination ( $this->_destinationUploadDir );
              $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
              $extensionValidator->setMessage(
                                'Error: Extension file must be *.'.$extension
                              );

              $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
              $sizeValidator->setMessage(
                'Error: File exceeds maximum size'
              );

              $adapter->setValidators ( array (
                $extensionValidator,
                $sizeValidator,
              ));

              if ($adapter->isValid ())
              {

                          $srcData = $this->_db->select()
                          ->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE','CCY_ID'))
                          ->where('ACCT_NO = ?', $ACCTSRC)
                          ->limit(1);

                          $acsrcData = $this->_db->fetchRow($srcData);

                          if(!empty($acsrcData)){
                            $accsrc = $acsrcData['ACCT_NO'];
                            $accsrctype = $acsrcData['ACCT_TYPE'];
                            $accsccy = $acsrcData['CCY_ID'];
                          }

                          $svcInquiry = new Service_Inquiry($this->_userIdLogin,$ACCTSRC,$acsrcData['ACCT_TYPE']);
                          $resultKursEx = $svcInquiry->rateInquiry();

                          //rate inquiry for display
                          $kurssell = '';
                          $kurs = '';
                          $book = '';
                          if($resultKursEx['ResponseCode']=='00'){
                            $kursList = $resultKursEx['DataList'];
                            $kurssell = '';
                            $kurs = '';

                            foreach($kursList as $row){
                              if($row["currency"] == 'USD'){
                                $row["sell"] = str_replace(',','',$row["sell"]);
                                $row["book"] = str_replace(',','',$row["book"]);
                                $row["buy"] = str_replace(',','',$row["buy"]);
                                $kurssell = $row["sell"];
                                $book = $row["book"];
                                if($ACCTSRC_CURRECY=='IDR'){
                                  $kurs = $row["sell"];
                                }else{
                                  $kurs = $row["buy"];
                                }
                              }
                            }

                          }

                $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                $NUMBER = Rand();
                  $random = md5($NUMBER);
                $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
            
                $fileName = substr($newFileName,85);

                $adapter->addFilter ( 'Rename',$newFileName  );

                if ($adapter->receive ())
                { 
                  $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);
                  //var_dump($adapterDataOtm);die;
                  //@unlink($newFileName);
                  //end
                  
                  $dateNow = date("Y-m-d");
                  $dateUpload = $data[0][2];
                  if ($dateUpload != $dateNow) {
                    
                    $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                    $this->view->error    = true;
                    $this->view->report_msg = $this->displayError($error_msg);


                  }else{

                    $psefdatenow = date("Y-m-d");

                    if (!empty($adapterDataOtm)) {
                      //unset header if not fixlength
                      if ($fixLength != 1) {
                        unset($data[0]);
                        unset($data[1]);  
                      }
                    }
                    else{
                      //unset defaults
                      unset($data[0]);
                      unset($data[1]);
                     
                    }

                    $totalRecords = count($data);

                    //proses convert ke order yg benar
                    if($totalRecords)
                    {

                      foreach ($adapterDataOtm as $key => $value) {
                        $headerOrder[] = $value['HEADER_CONTENT'];
                      }

                      foreach ($data as $datakey => $datavalue) {

                        if (!empty($headerOrder)) {
                          foreach ($headerOrder as $key => $value) {

                            $headerOrderArr = explode(',', $value);

                            if (count($headerOrderArr) > 1) {
                              $i = 0;
                              $contentStr = '';
                              foreach ($headerOrderArr as $key2 => $value2) {

                                if ($i != count($headerOrderArr)) {
                                  $contentStr .= $data[$datakey][$value2].' ';
                                }
                                $i++;
                              } 
                              $newData[$datakey][] = $contentStr;
                            }
                            else{
                              $newData[$datakey][] = $data[$datakey][$value];
                            }
                          } 
                        }
                        else{
                          $newData[$datakey][] = null;
                        }
                      }
                    }

                    //check mandatory yang bo setup saat buat business adapter profile
                    $mandatoryCheck = $this->validateField($newData, $adapterDataOtm);

                    //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                         
                    //$fixData = $this->autoMapping($newData, $adapterDataOtm);
                    $fixData = $data;
                    if($totalRecords)
                    {
                      if($totalRecords <= $this->_maxRow)
                      {
                        //die('here');
                        $rowNum = 0;

                        $paramPayment = array(  "CATEGORY"        => "BULK CREDITT",
                                                "FROM"            => "I",
                                                "PS_NUMBER"       => "",
                                                "BS_ID"           => $BS_ID,
                                                "PS_SUBJECT"      => $PS_SUBJECT,
                                                "PS_EFDATE"       => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                "PSFILEID"        => $PSFILEID,
                                                "PS_FILE"         => $fileName,
                                                "_dateFormat"     => $this->_dateDisplayFormat,
                                                "_dateDBFormat"   => $this->_dateDBFormat,
                                                "_addBeneficiary" => $this->view->hasPrivilege('BADA'),
                                                "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
                                                "_createPB"       => $this->view->hasPrivilege('CBPW'),
                                                "_createDOM"      => $this->view->hasPrivilege('CBPI'),
                                                "_createREM"      => false,
                                                );

                        $paramTrxArr = array();
                         
                        foreach ( $fixData as $row )
                        {
                           //print_r(count($row));
                          // if(count($row)==8)
                          // {
                            $rowNum++;
                            $benefAcct  = trim($row[1]);
                            $ccy    = 'IDR';
                            $amount   = trim($row[2]);
                            $purpose  = '';
                            $message  = trim($row[5]);
                            $addMessage = trim($row[6]);
                            $type     = trim($row[3]);
                            $bankCode   = trim($row[4]);
                            $cust_ref   = '';

                            //$TRA_SMS          = trim($row[7]);
                            $TRA_SMS  = '';
                            $TRA_EMAIL  = '';
                            $REFRENCE   = '';

                            $fullDesc = array(
                              'BENEFICIARY_ACCOUNT'    => $benefAcct,
                              'BENEFICIARY_ACCOUNT_CCY'    => $ccy,
                              'TRA_AMOUNT'         => $amount,
                              'TRA_MESSAGE'          => $message,
                              'TRA_PURPOSE'          => $purpose,
                              'REFNO'            => $addMessage,
                              'TRANSFER_TYPE'        => $type,
                              'CLR_CODE'           => $bankCode,
                              'CUST_REF'           => $cust_ref,
                            );
                            // print_r($fullDesc);die;

                            $filter = new Application_Filtering();

                            $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                            $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                            $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                            $ACBENEF      = $filter->filter($benefAcct, "ACCOUNT_NO");
                            $ACBENEF_EMAIL    = $filter->filter($email, "EMAIL");
                            $ACBENEF_CCY    = $filter->filter($ccy, "SELECTION");
                            $ACBENEF_ADDRESS  = $filter->filter($bankCity, "ADDRESS");
                            $CLR_CODE     = $filter->filter($bankCode, "BANK_CODE");
                            $TRANSFER_TYPE    = $filter->filter($type, "SELECTION");
                            $TRANS_PURPOSE    = $filter->filter($purpose, "SELECTION");
                            $CUST_REF       = $filter->filter($cust_ref, "CUST_REF");
                            $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                            if($TRANSFER_TYPE == 'RTGS'){
                              $chargeType = '1';
                              $select = $this->_db->select()
                                      ->from('M_CHARGES_OTHER',array('*'))
                                      ->where("CUST_ID = ?",$this->_custIdLogin)
                                      ->where("CHARGES_TYPE = ?",$chargeType);
                              $resultSelecet = $this->_db->FetchAll($select);
                              $chargeAmt = isset($resultSelecet['0']['CHARGES_AMT']);
                            }
                            else if($TRANSFER_TYPE == 'SKN'){
                              $chargeType1 = '2';
                              $select1 = $this->_db->select()
                                      ->from('M_CHARGES_OTHER',array('*'))
                                      ->where("CUST_ID = ?",$this->_custIdLogin)
                                      ->where("CHARGES_TYPE = ?",$chargeType1);
                              $resultSelecet1 = $this->_db->FetchAll($select1);
                              $chargeAmt = isset($resultSelecet1['0']['CHARGES_AMT']);
                            }
                            else{
                              $chargeAmt = '0';
                            }

                            $filter->__destruct();
                            unset($filter);
                            if($accsccy != 'IDR' ){
                              // print_r($kurs);
                              // print_r($TRA_AMOUNT_num);die;
                              $TRA_AMOUNT_NET = $TRA_AMOUNT_num;
                              $TRA_AMOUNT_EQ  =  $TRA_AMOUNT_num/$kurs;
                            }else{
                              $TRA_AMOUNT_NET = $TRA_AMOUNT_num;
                              $TRA_AMOUNT_EQ = $TRA_AMOUNT_num;
                            }

                            $paramTrx = array("TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                      "TRA_AMOUNT"        => $TRA_AMOUNT_NET,
                                      "TRA_AMOUNTEQ"        => $TRA_AMOUNT_EQ,
                                      "TRANSFER_FEE"        => $chargeAmt,
                                      "TRA_MESSAGE"         => $TRA_MESSAGE,
                                      "TRA_REFNO"         => $TRA_REFNO,
                                      "ACCTSRC"           => $ACCTSRC,
                                      "ACBENEF"           => $ACBENEF,
                                      "ACBENEF_CCY"         => $ACBENEF_CCY,
                                      "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                      "BENEFICIARY_RESIDENT"    => $BENEFICIARY_RESIDENT,
                                      "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,
                                      "BANK_CODE"         => $CLR_CODE,
                                      "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                      "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                      "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                      "BENEFICIARY_CATEGORY"    => $BENEFICIARY_CATEGORY,
                                      "BANK_NAME"         => $BANK_NAME,
                                      "TRANS_PURPOSE"       => $TRANS_PURPOSE,
                                      "PS_NOTIF"          => $TRA_NOTIF,
                                      "CUST_REF"          => $CUST_REF,
                                      "PS_SMS"          => $TRA_SMS,
                                      "PS_EMAIL"          => $TRA_EMAIL,
                                      "REFRENCE"          => $REFRENCE,

                                     );
                            if($accsccy != 'IDR' ){
                              $paramTrx['RATE']     = $kurssell;
                              $paramTrx['RATE_BUY']   = $kurs;
                              $paramTrx['BOOKRATE']   = $book;
                            }
                          
                            array_push($paramTrxArr,$paramTrx);
                            
                          // }
                          // else
                          // {
                          // // die('here');
                          //  $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
                          //  $this->view->error    = true;
                          //  $this->view->report_msg = $this->displayError($error_msg);
                            
                            
                          //  break;
                          // }
                        }
                      }

                      else
                      {
                        $error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }
                      
                      $confirm = true;

                    }
                    else
                    {
                      $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
                      $this->view->error    = true;
                      $this->view->report_msg = $this->displayError($error_msg);
                    }
                    
                  }

                }
              }
              else
              {
                // print_r($adapter->getMessages());die;
                $this->view->error = true;
                foreach($adapter->getMessages() as $key=>$val)
                {
                  if($key=='fileUploadErrorNoFile')
                    $error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                  else
                    $error_msg[] = $val;
                  break;
                }
                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;
              }

        //akhir
        }else if($BULK_TYPE == '5'){
        //awal
              $BS_ID  = $this->_request->getParam('bs_id');

              $extension = 'txt';
              $fileName = $adapterDataMto[0]['FILE_PATH'];
              $fixLength = $adapterDataMto[0]['FIXLENGTH'];
              $fixLengthType = $adapterDataMto[0]['FIXLENGTH_TYPE'];
              $fixLengthHeader = $adapterDataMto[0]['FIXLENGTH_HEADER_ORDER'];
              $fixLengthHeaderName = $adapterDataMto[0]['FIXLENGTH_HEADER_NAME'];
              $fixLengthContent = $adapterDataMto[0]['FIXLENGTH_CONTENT_ORDER'];
              //$delimitedWith = $adapterDataMto[0]['DELIMITED_WITH'];
              $delimitedWith = '|';

              $PS_SUBJECT         = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
              $PS_EFDATE          = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
              $ACBENEF            = $filter->filter($this->_request->getParam('beneficiary_acct'), "ACCOUNT_NO");
              $ACBENEF_BANKNAME   = $filter->filter($this->_request->getParam('beneficiary_name'), "ACCOUNT_NAME");
              $ACBENEF_ALIAS      = $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
              $ACBENEF_CCY        = $filter->filter($this->_request->getParam('beneficiary_ccy'), "SELECTION");
              $PSFILEID           = $filter->filter($this->_request->getParam('file_id'), "FILE_ID");

              $minLen = 10;
              $maxLen = 20;
              $error_msg[] = "";

                  $paramSettingID = array('range_futuredate', 'auto_release_payment');

                  $settings = new Application_Settings();
                  $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
                  $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
                  $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                  $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

                  $adapter = new Zend_File_Transfer_Adapter_Http();

                  $adapter->setDestination ( $this->_destinationUploadDir );

                  $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
                  $extensionValidator->setMessage(
                    'Error: Extension file must be *.'.$extension
                  );

                  // $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                  // $sizeValidator->setMessage(
                  //  'File exceeds maximum size'
                  // );

                  // $adapter->setValidators ( array (
                  //  $extensionValidator,
                  //  $sizeValidator,
                  // ));

                  if ($adapter->isValid ())
                  {
                    // die('ger');
                    $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                    $NUMBER = Rand();
                      $random = md5($NUMBER);
                    $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
                    
                    // $NUMBER = Rand();
                //      $random = md5($NUMBER);
                    // $fileName = $random. '-' .$sourceFileName. '.tmp';
                    $fileName = substr($newFileName,85);

                    $adapter->addFilter ('Rename',$newFileName);

                    if ($adapter->receive ())
                    {
                      $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                      $dateNow = date("Y-m-d");
                      $dateUpload = $data[0][2];
                      if ($dateUpload != $dateNow) {
                        
                        $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);

                      }else{
                          $psefdatenow = date("Y-m-d");
                    
                          //@unlink($newFileName);
                      
                          if (!empty($adapterDataMto)) {
                            //unset header if not fixlength
                            if ($fixLength != 1) {
                              unset($data[0]);
                              unset($data[1]);  
                            }
                          }
                          else{
                            //unset defaults
                            unset($data[0]);
                            unset($data[1]);
                            
                          }

                          $totalRecords = count($data);

                          //proses convert ke order yg benar
                          if($totalRecords)
                          {

                            foreach ($adapterDataMto as $key => $value) {
                              $headerOrder[] = $value['HEADER_CONTENT'];
                            }

                            foreach ($data as $datakey => $datavalue) {

                              if (!empty($headerOrder)) {
                                foreach ($headerOrder as $key => $value) {

                                  $headerOrderArr = explode(',', $value);

                                  if (count($headerOrderArr) > 1) {
                                    $i = 0;
                                    $contentStr = '';
                                    foreach ($headerOrderArr as $key2 => $value2) {

                                      if ($i != count($headerOrderArr)) {
                                        $contentStr .= $data[$datakey][$value2].' ';
                                      }
                                      $i++;
                                    } 
                                    $newData[$datakey][] = $contentStr;
                                  }
                                  else{
                                    $newData[$datakey][] = $data[$datakey][$value];
                                  }
                                } 
                              }
                              else{
                                $newData[$datakey][] = null;
                              }
                            }
                          }

                          //check mandatory yang bo setup saat buat business adapter profile
                          $mandatoryCheck = $this->validateField($newData, $adapterDataMto);

                          //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                          //$fixData = $this->autoMapping($newData, $adapterDataMto);
                        //  $fixData = $data;
                          //var_dump($data);die('here');
                          //if(empty($fixData)){
                            $fixData = $data;
                            $totalRecords = count($data);
                          //}
                          if($totalRecords)
                          {
                            if($totalRecords <= $this->_maxRow)
                            {
                              // die('here');
                              $rowNum = 0;

                              $paramPayment = array( "CATEGORY"         => "BULK DEBET",
                                                     "FROM"             => "I",
                                                     "PS_NUMBER"        => "",
                                                     "BS_ID"            => $BS_ID,
                                                     "PS_SUBJECT"       => $PS_SUBJECT,
                                                     "PS_EFDATE"        => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                     "PSFILEID"         => $PSFILEID,
                                                     "PS_FILE"          => $fileName,
                                                     "_dateFormat"      => $this->_dateDisplayFormat,
                                                     "_dateDBFormat"    => $this->_dateDBFormat,
                                                     "_addBeneficiary"  => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                                     "_beneLinkage"     => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                                     "_createPB"        => $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
                                                     "_createDOM"       => false,        // cannot create DOM trx
                                                     "_createREM"       => false,        // cannot create REM trx
                                                    );

                              $paramTrxArr = array();
                              
                              foreach ( $fixData as $key=>$row)
                              {
                                // var_dump(count($row));
                                // if(count($row)==5)
                                // {
                                  // die('here');
                                  $rowNum++;
                                  $sourceAcct   = trim($row[1]);
                                  $ccy          = "IDR";
                                  $amount       = trim($row[2]);
                                  $purpose      = '';
                                  $message      = trim($row[3]);
                                  $cust_ref     = '';
                                  $addMessage   = trim($row[4]);

                                  // if(!empty($row[4]) || !empty($row[5])){
                                    $TRA_NOTIF      = '2';
                                  // }else{
                                  //  $TRA_NOTIF      = '1';
                                  // }
                                  $TRA_SMS        = '';
                                  $TRA_EMAIL        = '';
                                  $REFRENCE       = '';


                                  $filter = new Application_Filtering();

                                  $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                                  $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                                  $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                                  // $ACCTSRC       = $filter->filter($sourceAcct, "ACCOUNT_NO");
                                  $ACCTSRC      = $sourceAcct;
                                  $TRANSFER_TYPE    = 'PB';

                                  $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                                  $filter->__destruct();
                                  unset($filter);

                                  $paramTrx = array(  "TRANSFER_TYPE"   => $TRANSFER_TYPE,
                                            "TRA_AMOUNT"    => $TRA_AMOUNT_num,
                                            "TRA_MESSAGE"     => $TRA_MESSAGE,
                                            "TRA_REFNO"     => $TRA_REFNO,
                                            "ACCTSRC"       => $ACCTSRC,
                                            "ACBENEF"       => $ACBENEF,
                                            "ACBENEF_CCY"     => $ACBENEF_CCY,
                                            "ACBENEF_EMAIL"   => '',

                                          // for Beneficiary data, except (bene CCY and email), must be passed by reference
                                            "ACBENEF_BANKNAME"      => &$ACBENEF_BANKNAME,
                                            "ACBENEF_ALIAS"       => &$ACBENEF_ALIAS,
                                            "PS_NOTIF"          => $TRA_NOTIF,
                                            "CUST_REF"          => $cust_ref,
                                            "PS_SMS"          => $TRA_SMS,
                                            "PS_EMAIL"          => $TRA_EMAIL,
                                            "REFRENCE"          => $REFRENCE,
                                          //  "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                          //  "ACBENEF_ADDRESS1"      => $ACBENEF_ADDRESS,
                                          //  "ACBENEF_ADDRESS2"      => &$ACBENEF_ADDRESS2,
                                          //  "ACBENEF_ADDRESS3"      => &$ACBENEF_ADDRESS3,

                                          //  "ORG_DIR"           => $ORG_DIR,
                                          //  "BANK_CODE"         => $CLR_CODE,
                                          //  "BANK_NAME"         => $BANK_NAME,
                                          //  "BANK_BRANCH"         => $BANK_BRANCH,
                                          //  "BANK_ADDRESS1"       => $BANK_ADDRESS1,
                                          //  "BANK_ADDRESS2"       => $BANK_ADDRESS2,
                                          //  "BANK_ADDRESS3"       => $BANK_ADDRESS3,
                                           );

                                  array_push($paramTrxArr,$paramTrx);
                                // }
                                // else
                                // {
                                //  $error_msg[] = $this->language->_('Wrong File Format').'';
                                //  $this->view->error    = true;
                                //  $this->view->report_msg = $this->displayError($error_msg);
                                //  break;
                                // }
                              }
                            }
                            // kalo jumlah trx lebih dari setting
                            else
                            {
                              // die('here1');
                              $error_msg[] = 'The number of rows to be imported should not more than '.$this->_maxRow.'.';
                              $this->view->error    = true;
                              $this->view->report_msg = $this->displayError($error_msg);
                            }
                            
                            $confirm = true;

                          }
                          else //kalo total record = 0
                          {
                            //$error_msg[] = 'Wrong File Format. There is no data on csv File.';
                            $error_msg[] = $this->language->_('Wrong File Format').'.';
                            $this->view->error    = true;
                            $this->view->report_msg = $this->displayError($error_msg);
                          }

                      }
                    }
                  }
                  else
                  {
                    // print_r($adapter->getMessages());die;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                      if($key=='fileUploadErrorNoFile'){
                        $error_msg[] = $this->language->_('File cannot be left blank. Please correct it').'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }else{
                        $error_msg[] = $val;
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      break;
                      }
                    }
                  }

        //akhir     
        }else if($BULK_TYPE == '11'){
        //awal
           
            $extension = 'txt';
            $fileName = $adapterDataPayroll[0]['FILE_PATH'];
            $fixLength = $adapterDataPayroll[0]['FIXLENGTH'];
            $fixLengthType = $adapterDataPayroll[0]['FIXLENGTH_TYPE'];
            $fixLengthHeader = $adapterDataPayroll[0]['FIXLENGTH_HEADER_ORDER'];
            $fixLengthHeaderName = $adapterDataPayroll[0]['FIXLENGTH_HEADER_NAME'];
            $fixLengthContent = $adapterDataPayroll[0]['FIXLENGTH_CONTENT_ORDER'];
            //$delimitedWith = $adapterDataPayroll[0]['DELIMITED_WITH'];
            $delimitedWith = '|';

            $PS_SUBJECT   = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
            $PS_EFDATE    = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
            $ACCTSRC      = $filter->filter($this->_request->getParam('source_acct'), "ACCOUNT_NO");
            $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID"); 
            $BS_ID        = $this->_request->getParam('bs_id');

            $paramSettingID = array('range_futuredate', 'auto_release_payment');

            $settings = new Application_Settings();
            $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
            $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
            $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
            $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $adapter->setDestination ( $this->_destinationUploadDir );
            $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
            $extensionValidator->setMessage(
              'Error: Extension file must be *.'.$extension
            );

            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
            $sizeValidator->setMessage(
              'Error: File exceeds maximum size'
            );

            $adapter->setValidators ( array (
              $extensionValidator,
              $sizeValidator,
            ));

            if ($adapter->isValid ())
            {
              $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
              $NUMBER = Rand();
              $random = md5($NUMBER);
              $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
              
              $fileName = substr($newFileName,85);

              $adapter->addFilter ('Rename',$newFileName);

              if ($adapter->receive ())
              {
                  $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                  $dateNow = date("Y-m-d");
                  $dateUpload = $data[0][2];
                  if ($dateUpload != $dateNow) {
                    
                    $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                    $this->view->error    = true;
                    $this->view->report_msg = $this->displayError($error_msg);

                  }else{
                      $psefdatenow = date("Y-m-d");

                      //@unlink($newFileName);

                      if (!empty($adapterDataPayroll)) {
                        //unset header if not fixlength
                        if ($fixLength != 1) {
                          unset($data[0]);
                          unset($data[1]);  
                        }
                      }
                      else{
                        
                        unset($data[0]);
                        unset($data[1]);              
                      } 

                      $totalRecords = count($data);
                      
                      //proses convert ke order yg benar
                      if($totalRecords)
                      {

                        foreach ($adapterDataPayroll as $key => $value) {
                          $headerOrder[] = $value['HEADER_CONTENT'];
                        }

                        foreach ($data as $datakey => $datavalue) {

                          if (!empty($headerOrder)) {
                            foreach ($headerOrder as $key => $value) {

                              $headerOrderArr = explode(',', $value);

                              if (count($headerOrderArr) > 1) {
                                $i = 0;
                                $contentStr = '';
                                foreach ($headerOrderArr as $key2 => $value2) {

                                  if ($i != count($headerOrderArr)) {
                                    $contentStr .= $data[$datakey][$value2].' ';
                                  }
                                  $i++;
                                } 
                                $newData[$datakey][] = $contentStr;
                              }
                              else{
                                $newData[$datakey][] = $data[$datakey][$value];
                              }
                            } 
                          }
                          else{
                            $newData[$datakey][] = null;
                          }
                        }
                      }
                      
                      $mandatoryCheck = $this->validateField($newData, $adapterDataPayroll);

                      //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                      $fixData = $this->autoMapping($newData, $adapterDataPayroll);
                      
                      if(empty($fixData)){
                        $fixData = $data; 
                        $totalRecords = count($data);
                      }
                      
                      if($totalRecords)
                      {
                        if($totalRecords <= $this->_maxRow)
                        {
                          $rowNum = 0;

                          $paramPayment = array(  "CATEGORY"        => "BULK PAYROLL",
                                                  "FROM"            => "I",
                                                  "PS_NUMBER"       => "",
                                                  "BS_ID"           => $BS_ID,
                                                  "PS_SUBJECT"      => $PS_SUBJECT,
                                                  "PSFILEID"        => $PSFILEID,
                                                  "PS_EFDATE"       => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                  "PS_FILE"         => $fileName,
                                                  "_dateFormat"     => $this->_dateDisplayFormat,
                                                  "_dateDBFormat"   => $this->_dateDBFormat,
                                                  "_addBeneficiary" => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                                  "_beneLinkage"    => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                                  "_createPB"       => $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
                                                  "_createDOM"      => $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
                                                  "_createREM"      => false,        // cannot create REM trx
                                                  );

                          $paramTrxArr = array();
                          // Zend_Debug::dump($fixData); die;

                          foreach ( $fixData as $row )
                          {
                            // if(count($row)==4)
                            // {
                              // var_dump($row);die;
                              $rowNum++;
                              $benefAcct    = trim($row[1]);
        //                      $benefName    = trim($row[1]);
                              $ccy      = "IDR";
                              $amount     = trim($row[2]);
                              $message    = trim($row[3]);
                              $reference    = '';
                              $sms_notif    = '';
                              $email_notif  = '';
                              $addMessage   = trim($row[4]);
        //                      $email      = trim($row[6]);
        //                      $phoneNumber  = trim($row[7]);
                              $type       = 'PB';
        //                      $bankCode     = trim($row[6]);
                              //$bankName     = trim($row[10]);
          //                    $bankCity = trim($row[10]);
        //                      $benefAdd   = trim($row[9]);
        //                      $citizenship  = strtoupper(trim($row[10]));
                              //$resident = strtoupper(trim($row[11]));

                              /*
                               * Change parameter into document
                               */
                              $fullDesc = array(
                                'BENEFICIARY_ACCOUNT'     => $benefAcct,
                                'BENEFICIARY_NAME'      => '',
                                'BENEFICIARY_ACCOUNT_CCY'   => $ccy,
                                'TRA_AMOUNT'        => $amount,
                                'TRA_MESSAGE'       => $message,
                                'REFNO'           => $addMessage,
                                'SMS_NOTIF'         => $sms_notif,
                                'EMAIL_NOTIF'         => $email_notif,
                                'BENEFICIARY_EMAIL'     => '',
                                'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
                                'TRANSFER_TYPE'       => 'PB',
                                //'CLR_CODE'          => $bankCode,
                                //'BENEFICIARY_BANK_NAME'   => $bankName,
          //                      'BENEFICIARY_CITY' => $bankCity,
                                'BENEFICIARY_ADDRESS'   => '',
                                'BENEFICIARY_CITIZENSHIP'   => ''
                                //'BENEFICIARY_RESIDENT' => $resident
                              );

                              $filter = new Application_Filtering();

                              $SMS_NOTIF    = $filter->filter($sms_notif, "SMS");
                              $EMAIL_NOTIF    = $filter->filter($email_notif, "EMAIL");

                              $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                              $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                              $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                              $ACBENEF      = $filter->filter($benefAcct, "ACCOUNT_NO");
                              $ACBENEF_BANKNAME   = $filter->filter('', "ACCOUNT_NAME");
                              $ACBENEF_ALIAS    = $filter->filter('', "ACCOUNT_ALIAS");
                              $ACBENEF_EMAIL    = $filter->filter('', "EMAIL");
                              $ACBENEF_PHONE    = $filter->filter('', "MOBILE_PHONE_NUMBER");
                              $ACBENEF_CCY    = $filter->filter($ccy, "SELECTION");
                              $ACBENEF_ADDRESS  = $filter->filter('', "ADDRESS");
                              $ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
                              //$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
                              //$BANK_NAME      = $filter->filter($bankName, "BANK_NAME");
                              //$BANK_CITY      = $filter->filter($bankCity, "ADDRESS");
                              //$CLR_CODE     = $filter->filter($bankCode, "BANK_CODE");
                              $TRANSFER_TYPE    = $filter->filter($type, "SELECTION");

                              $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                              if($TRANSFER_TYPE == 'RTGS'){
                                $chargeType = '1';
                                $select = $this->_db->select()
                                        ->from('M_CHARGES_OTHER',array('*'))
                                        ->where("CUST_ID = ?",$this->_custIdLogin)
                                        ->where("CHARGES_TYPE = ?",$chargeType);
                                $resultSelecet = $this->_db->FetchAll($select);
                                $chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
                              }
                              else if($TRANSFER_TYPE == 'SKN'){
                                $chargeType1 = '2';
                                $select1 = $this->_db->select()
                                        ->from('M_CHARGES_OTHER',array('*'))
                                        ->where("CUST_ID = ?",$this->_custIdLogin)
                                        ->where("CHARGES_TYPE = ?",$chargeType1);
                                $resultSelecet1 = $this->_db->FetchAll($select1);
                                $chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
                              }
                              else{
                                $chargeAmt = '0';
                              }

                              $filter->__destruct();
                              unset($filter);

                              $paramTrx = array("TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                        "TRA_AMOUNT"        => $TRA_AMOUNT_num,
                                        "TRANSFER_FEE"        => $chargeAmt,
                                        "TRA_MESSAGE"         => $TRA_MESSAGE,
                                        "TRA_REFNO"         => $TRA_REFNO,
                                        "ACCTSRC"           => $ACCTSRC,
                                        "ACBENEF"           => $ACBENEF,
                                        "ACBENEF_CCY"         => $ACBENEF_CCY,
                                        "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                        "ACBENEF_PHONE"       => $ACBENEF_PHONE,
                                        "SMS_NOTIF"         => $SMS_NOTIF,
                                        "EMAIL_NOTIF"       => $EMAIL_NOTIF,
                                      // for Beneficiary data, except (bene CCY and email), must be passed by reference
                                        "ACBENEF_BANKNAME"      => $ACBENEF_BANKNAME,
                                        "ACBENEF_ALIAS"       => $ACBENEF_ALIAS,
                                        "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // W: WNI, N: WNA
                                      //  "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,   // R: RESIDENT, NR: NON-RESIDENT
                                        "ACBENEF_ADDRESS1"      => $ACBENEF_ADDRESS,
                                        "REFERENCE"         => $reference,
                                      //  "ACBENEF_ADDRESS2"      => &$ACBENEF_ADDRESS2,
                                      //  "ACBENEF_ADDRESS3"      => &$ACBENEF_ADDRESS3,

                                      //  "ORG_DIR"           => $ORG_DIR,
                                        //"BANK_CODE"         => $CLR_CODE,
                                      //  "BANK_NAME"         => $BANK_NAME,
                                      //  "BANK_BRANCH"         => $BANK_BRANCH,
                                      //  "BANK_ADDRESS1"       => $BANK_ADDRESS1,
                                      //  "BANK_ADDRESS2"       => $BANK_ADDRESS2,
                                      //  "BANK_ADDRESS3"       => $BANK_ADDRESS3,
                                       );

                              array_push($paramTrxArr,$paramTrx);
                          }
                        }
                        // kalo jumlah trx lebih dari setting
                        else
                        {
                          $error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
                          $this->view->error    = true;
                          $this->view->report_msg = $this->displayError($error_msg);
                        }

                        $confirm = true;

                      }
                      else //kalo total record = 0
                      {
                        $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }

                  }
              //  
              }

            }
            else
            {
              $this->view->error = true;
              foreach($adapter->getMessages() as $key=>$val)
              {
                if($key=='fileUploadErrorNoFile')
                  $error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                else
                  $error_msg[] = $val;
                break;
              }
              $errors = $this->displayError($error_msg);
              $this->view->report_msg = $errors;
            }

        //akhir
        }else if($BULK_TYPE == '22'){
        //awal
            
            $extension = 'txt';
            $fileName = $adapterDataMtm[0]['FILE_PATH'];
            $fixLength = $adapterDataMtm[0]['FIXLENGTH'];
            $fixLengthType = $adapterData[0]['FIXLENGTH_TYPE'];
            $fixLengthHeader = $adapterDataMtm[0]['FIXLENGTH_HEADER_ORDER'];
            $fixLengthHeaderName = $adapterDataMtm[0]['FIXLENGTH_HEADER_NAME'];
            $fixLengthContent = $adapterDataMtm[0]['FIXLENGTH_CONTENT_ORDER'];
            //$delimitedWith = $adapterDataMtm[0]['DELIMITED_WITH'];
            $delimitedWith = '|';
            $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID"); 
            $BS_ID        = $this->_request->getParam('bs_id');

                $filter   = new Application_Filtering();
                $adapter  = new Zend_File_Transfer_Adapter_Http ();

                $max    = $this->getSetting('max_import_single_payment');

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
                $extensionValidator->setMessage(
                                  'Error: Extension file must be *.'.$extension
                                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                              'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
                            );

                $adapter->setValidators(array($extensionValidator,$sizeValidator));
                // die('here');
                if ($adapter->isValid ())
                {
                  
                  $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                  $NUMBER = Rand();
                    $random = md5($NUMBER);
                  $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
                  
                  $fileName = substr($newFileName,85);

                  $adapter->addFilter ( 'Rename',$newFileName  );

                  if ($adapter->receive ())
                  {

                    $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                    $dateNow = date("Y-m-d");
                    $dateUpload = $data[0][2];
                    if ($dateUpload != $dateNow) {
                      
                      $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                      $this->view->error    = true;
                      $this->view->report_msg = $this->displayError($error_msg);

                    }else{
                        $psefdatenow = date("Y-m-d");

                        //@unlink($newFileName);

                        if (!empty($adapterDataMtm)) {
                          //unset header if not fixlength
                          if ($fixLength != 1) {
                            unset($data[0]);  
                            unset($data[1]);
                          }
                        }
                        else{
                          //unset defaults
                          unset($data[0]);
                          unset($data[1]);
                         
                        }

                        $totalRecords = count($data);

                        //proses convert ke order yg benar
                        if($totalRecords)
                        {

                          foreach ($adapterDataMtm as $key => $value) {
                            $headerOrder[] = $value['HEADER_CONTENT'];
                          }

                          foreach ($data as $datakey => $datavalue) {

                            if (!empty($headerOrder)) {
                              foreach ($headerOrder as $key => $value) {

                                $headerOrderArr = explode(',', $value);

                                if (count($headerOrderArr) > 1) {
                                  $i = 0;
                                  $contentStr = '';
                                  foreach ($headerOrderArr as $key2 => $value2) {

                                    if ($i != count($headerOrderArr)) {
                                      $contentStr .= $data[$datakey][$value2].' ';
                                    }
                                    $i++;
                                  } 
                                  $newData[$datakey][] = $contentStr;
                                }
                                else{
                                  $newData[$datakey][] = $data[$datakey][$value];
                                }
                              } 
                            }
                            else{
                              $newData[$datakey][] = null;
                            }
                          }

                          //check mandatory yang bo setup saat buat business adapter profile
                          $mandatoryCheck = $this->validateField($newData, $adapterDataMtm);

                          //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                          $fixData = $this->autoMapping($newData, $adapterDataMtm);

                        }

                        if(empty($fixData)){
                          $fixData = $data;
                          $totalRecords = count($data);
                        }
                        
                        if ($totalRecords && empty($mandatoryCheck)){
                          if($totalRecords <= $max)
                          {
                            $no =0;
                            $paramTrxArray = array();

                            foreach ( $fixData as $columns )
                            {
                              // if(count($columns)==14)
                              // {
                                $params['PAYMENT_SUBJECT']      = trim($columns[1]);
                                $params['SOURCE_ACCT_NO']       = trim($columns[2]);
                                $params['CCY']            = trim('IDR');
                                $params['BENEFICIARY_ACCT_NO']    = trim($columns[3]);
                                $params['BENEFICIARY_ACCT_CCY']   = trim('IDR');
                                $params['AMOUNT']           = trim($columns[4]);
                                $params['MESSAGE']          = trim($columns[8]);
                                $params['TRANS_PURPOSE']      = '';
                                $params['ADDITIONAL_MESSAGE']     = trim($columns[9]);
                                // $origDate = trim($columns[8]);
                                // $date = str_replace('/', '-', $origDate );
                                // $date2 = date_create($date);
                                $params['PAYMENT_DATE']       = trim($columns[7]);
                                $params['TRANSFER_TYPE']      = trim($columns[5]);
                                $params['BANK_CODE']        = trim($columns[6]);
                                $params['CUST_REF']         = '';
                                // if(!empty($columns[12]) || !empty($columns[13])){
                                //  $params['TRA_NOTIF']      = '2';
                                // }else{
                                //  $params['TRA_NOTIF']      = '1';
                                // }
                                $params['BENEFICIARY_NAME']       = '';
                                $params['BENEFICIARY_NAME']       = '';
                                $params['TRA_NOTIF']        = '';
                                $params['PS_SMS']         = '';
                                $params['PS_EMAIL']         = '';
                                $params['TREASURY_NUM']       = '';
                                // $params['LLD_TRANSACTION_PURPOSE']  = trim($columns[14]);
                                $params['LLD_TRANSACTION_PURPOSE']  = '';

                                $PS_SUBJECT     = $filter->filter($params['PAYMENT_SUBJECT'],"PS_SUBJECT");
                                $PS_EFDATE      = $filter->filter($params['PAYMENT_DATE'],"PS_DATE");
                                $TRA_AMOUNT     = $filter->filter($params['AMOUNT'],"AMOUNT");
                                $TRA_MESSAGE    = $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
                                $TRA_REFNO      = $filter->filter($params['ADDITIONAL_MESSAGE'],"TRA_REFNO");
                                $ACCTSRC      = $filter->filter($params['SOURCE_ACCT_NO'],"ACCOUNT_NO");
                                $ACBENEF      = $filter->filter($params['BENEFICIARY_ACCT_NO'],"ACCOUNT_NO");
                                $ACBENEF_BANKNAME   = $filter->filter($params['BENEFICIARY_NAME'],"ACCOUNT_NAME");
                                $ACBENEF_CCY    = $filter->filter($params['CCY'],"SELECTION");
                                $CLR_CODE     = $filter->filter($params['BANK_CODE'], "BANK_CODE");
                                $TRANSFER_TYPE    = $filter->filter($params['TRANSFER_TYPE'], "SELECTION");
                                $CUST_REF       = $filter->filter($params['CUST_REF'], "SELECTION");
                                $BENEFICIARY_ACCT_CCY     = $filter->filter($params['BENEFICIARY_ACCT_CCY'], "BENEFICIARY_ACCT_CCY");
                                $TRA_NOTIF      = $filter->filter($params['TRA_NOTIF'], "TRA_NOTIF");
                                $TRA_SMS      = $filter->filter($params['PS_SMS'], "PS_SMS");
                                $TRA_EMAIL      = $filter->filter($params['PS_EMAIL'], "PS_EMAIL");
                                $TREASURY_NUM   = $filter->filter($params['TREASURY_NUM'], "TREASURY_NUM");
                                $LLD_TRANSACTION_PURPOSE    = $filter->filter($params['LLD_TRANSACTION_PURPOSE'], "LLD_TRANSACTION_PURPOSE");

                                $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                                if($TRANSFER_TYPE == 'RTGS'){
                                  $chargeType = '1';
                                  $select = $this->_db->select()
                                          ->from('M_CHARGES_OTHER',array('*'))
                                          ->where("CUST_ID = ?",$this->_custIdLogin)
                                          ->where("CHARGES_TYPE = ?",$chargeType);
                                  $resultSelecet = $this->_db->FetchAll($select);
                                  $chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

                                  //$param['TRANSFER_FEE'] = $chargeAmt;
                                }
                                else if($TRANSFER_TYPE == 'SKN'){
                                  $chargeType1 = '2';
                                  $select1 = $this->_db->select()
                                          ->from('M_CHARGES_OTHER',array('*'))
                                          ->where("CUST_ID = ?",$this->_custIdLogin)
                                          ->where("CHARGES_TYPE = ?",$chargeType1);
                                  $resultSelecet1 = $this->_db->FetchAll($select1);
                                  $chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

                                  //$param['TRANSFER_FEE'] = $chargeAmt1;
                                }
                                else{
                                  $chargeAmt = '0';
                                  //$param['TRANSFER_FEE'] = $chargeAmt2;
                                }

                                if($BENEFICIARY_ACCT_CCY != $ACBENEF_CCY){
                                  $CROSS_CURR = '2';
                                }else{
                                  $CROSS_CURR = '1';
                                }

                                $paramPayment = array(
                                    "CATEGORY"          => "SINGLE PAYMENT",
                                    "FROM"              => "I",       // F: Form, I: Import
                                    "PS_NUMBER"         => "",
                                    "PS_SUBJECT"        => $PS_SUBJECT,
                                    "BS_ID"             => $BS_ID,
                                    "PS_EFDATE"         => $psefdatenow,
                                    "PSFILEID"          => $PSFILEID,
                                    "PS_FILE"           => $fileName,
                                    "PS_CCY"			=> $ACBENEF_CCY,
                                    "_dateFormat"       => 'yyyy-MM-dd',
                                    "_dateDBFormat"     => $this->_dateDBFormat,
                                    "_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                    "_beneLinkage"      => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                    "_createPB"         => $this->view->hasPrivilege('CRIP'),               // cannot create PB trx
                                    "_createDOM"        => $this->view->hasPrivilege('CRDI'), // privi CDFT (Create Domestic Fund Transfer)
                                    "_createREM"        => $this->view->hasPrivilege('CRIR'),               // cannot create REM trx
                                    "TRA_CCY"           => $BENEFICIARY_ACCT_CCY,
                                    "CROSS_CURR"        => $CROSS_CURR
                                );

                                $paramTrxArr[0] = array(
                                    "TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                    "TRA_AMOUNT"          => $TRA_AMOUNT_num,
                                    "TRANSFER_FEE"        => $chargeAmt,
                                    "TRA_MESSAGE"         => $TRA_MESSAGE,
                                    "TRA_REFNO"           => $TRA_REFNO,
                                    "ACCTSRC"             => $ACCTSRC,
                                    "ACBENEF"             => $ACBENEF,
                                    "ACBENEF_CCY"         => $ACBENEF_CCY,
                                    "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                    "ACBENEF_BANKNAME"    => $ACBENEF_BANKNAME,
            //                        "ACBENEF_ALIAS"     => $ACBENEF_ALIAS,
                                    "ACBENEF_CITIZENSHIP" => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                    //                                "ACBENEF_ADDRESS1"      => $BANK_CITY,
            //                        "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,
                                    "CUST_REF"             => $CUST_REF,
                                    "BENEFICIARY_RESIDENT" => $BENEFICIARY_RESIDENT,
                                    "BANK_CODE"            => $CLR_CODE,
            //                        "BENEFICIARY_BANK_NAME"   => $BANK_NAME,
            //                        "LLD_IDENTICAL"       => "",
            //                        "LLD_CATEGORY"        => "",
            //                        "LLD_RELATIONSHIP"      => "",
            //                        "LLD_PURPOSE"         => "",
            //                        "LLD_DESCRIPTION"       => "",
                                    "LLD_TRANSACTION_PURPOSE" => $LLD_TRANSACTION_PURPOSE,
                                    "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                    "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                    "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                    "BENEFICIARY_ACCT_CCY"    => $BENEFICIARY_ACCT_CCY,
                                    "PS_NOTIF"                => $TRA_NOTIF,
                                    "PS_SMS"                  => $TRA_SMS,
                                    "PS_EMAIL"                => $TRA_EMAIL,
                                    "REFERENCE"               => $TREASURY_NUM,

                                    "BANK_NAME"   => $BANK_NAME,

                                );

                                $arr[$no]['paramPayment'] = $paramPayment;
                                $arr[$no]['paramTrxArr'] = $paramTrxArr;

                                $paramTrx = array(
                                    "PS_SUBJECT"        => $PS_SUBJECT,
                                    "TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                    "TRA_AMOUNT"        => $TRA_AMOUNT_num,
                                    "TRANSFER_FEE"        => $chargeAmt,
                                    "TRA_MESSAGE"         => $TRA_MESSAGE,
                                    "TRA_REFNO"         => $TRA_REFNO,
                                    "ACCTSRC"           => $ACCTSRC,
                                    "ACBENEF"           => $ACBENEF,
                                    "ACBENEF_CCY"         => $ACBENEF_CCY,
                                    "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                    "ACBENEF_BANKNAME"      => $ACBENEF_BANKNAME,
            //                        "ACBENEF_ALIAS"       => $ACBENEF_ALIAS,
                                    "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                    //                                "ACBENEF_ADDRESS1"      => $BANK_CITY,
            //                        "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,
                                    "CUST_REF"          => $CUST_REF,
                                    "BENEFICIARY_RESIDENT"    => $BENEFICIARY_RESIDENT,
                                    "BANK_CODE"         => $CLR_CODE,
            //                        "BENEFICIARY_BANK_NAME"   => $BANK_NAME,
            //                        "LLD_IDENTICAL"       => "",
            //                        "LLD_CATEGORY"        => "",
            //                        "LLD_RELATIONSHIP"      => "",
            //                        "LLD_PURPOSE"         => "",
            //                        "LLD_DESCRIPTION"       => "",
                                    "LLD_TRANSACTION_PURPOSE" => $LLD_TRANSACTION_PURPOSE,
                                    "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                    "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                    "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                    "BENEFICIARY_ACCT_CCY"    => $BENEFICIARY_ACCT_CCY,
                                    "PS_NOTIF"        => $TRA_NOTIF,
                                    "PS_SMS"          => $TRA_SMS,
                                    "PS_EMAIL"        => $TRA_EMAIL,
                                    "REFERENCE"         => $TREASURY_NUM,

                                    "BANK_NAME"   => $BANK_NAME,

                                );

                                array_push($paramTrxArray,$paramTrx);
                              // }
                              // else
                              // {
                              //  // die('ge');
                              //  $this->view->error    = true;
                              //  break;
                              // }
                              $no++;
                            }

                            if(!$this->view->error)
                            {
                             
                              $i = 0;
                              foreach($resWs as $key=>$dataAcctType){
                                //Zend_Debug::dump($dataAcctType);
                                $arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
                              }

                              $sourceAccountType  = $resWs['accountType'];

                              // echo "<pre>";
                              // var_dump($arr);
                              // die();

                              $content['payment'] = $payment;
                              $content['arr']   = $arr;
                              $content['errorTrxMsg']   = $errorTrxMsg;
                              $content['sourceAccountType']   = $sourceAccountType;
                              $content['paramPayment'] = $paramPayment;
                              $content['paramTrxArray'] = $paramTrxArray;

                              $sessionNamespace = new Zend_Session_Namespace('confirmImportCreditBatch');
                              $sessionNamespace->content = $content;

                              $this->_redirect('/singlepayment/importbatch/confirm');
                            }

                          }
                          else
                          {
                            // die('here');
                            $this->view->error2 = true;
                            $this->view->max  = $max;
                          }
                        }else{
                          // die('here1');
                          $this->view->error = true;

                          if (!empty($adapter->getMessages())) {
                            $error_msg = array($adapter->getMessages());
                          }

                          if (!empty($mandatoryCheck)) {
                            foreach ($mandatoryCheck as $key => $value) {
                              array_push($error_msg, $value.' Field Cannot be left blank');
                            }
                          }

                          $this->view->report_msg = $this->displayError($error_msg);
                        }

                    }
   
                  }
                }
                else
                {
                  // die('here3');
                  $this->view->error = true;
                  $error_msg = array($adapter->getMessages());
                  $this->view->report_msg = $this->displayError($error_msg);
                }

        //akhir
        }

        if($confirm)
        {
          $content['paramPayment'] = $paramPayment;
          $content['paramTrxArr']  = $paramTrxArr;
          $content['errorTrxMsg']  = $errorTrxMsg;
          $content['payment'] = $payment;
          if($BULK_TYPE=='4'){
            $content['sourceAccountType'] = $sourceAccountType;
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
            $sessionNamespace->content = $content;
            $this->_redirect('/multicredit/bulkbatch/confirm');

          }else if($BULK_TYPE=='5'){
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
            $sessionNamespace->content = $content;
            $this->_redirect('/multidebet/bulkbatch/confirm');
            
          }else if($BULK_TYPE=='11'){
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
            $sessionNamespace->content = $content;
            $this->_redirect('/payrollpayment/bulk/confirm');
          }
        }
        
      }

    }
	
	public function downloadtxtAction(){	

		$ns = new Zend_Session_Namespace('language');
		$sessionLanguage = $ns->langCode;
				
		$handle = fopen("file.txt", "w");
		if($sessionLanguage == 'id'){
			fwrite($handle, "NO REKENING PENERIMA,MATA UANG,JUMLAH,PESAN,PESAN TAMBAHAN");
			$filename = "RDN_Multi_Credit_Template_id.txt";
		}else{
			fwrite($handle, "DESTINATION ACCT,CCY,AMOUNT,MESSAGE,ADDITIONAL MESSAGE");
			$filename = "RDN_Multi_Credit_Template_en.txt";
		}
	    
	    fclose($handle);
	
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename='.basename($filename));
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize('file.txt'));
	    readfile('file.txt');
	    exit;
	}
}
