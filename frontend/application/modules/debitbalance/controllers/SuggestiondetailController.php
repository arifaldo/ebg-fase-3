<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
class debitbalance_SuggestiondetailController extends Application_Main
{
  	protected $_moduleDB = 'CST';

	public function indexAction()
  	{
		
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       	$password = $sessionNamespace->token; 
       	$this->view->token = $sessionNamespace->token;  

    	$AESMYSQL = new Crypt_AESMYSQL();
    	$PS_NUMBER      = urldecode($this->_getParam('changes_id'));
    	$change_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    	$change_id = (Zend_Validate::is($change_id,'Digits'))? $change_id : 0;
		//die($change_id);
  		$this->_helper->layout()->setLayout('popup');
  		
  		/*$params = $this->_request->getParams();
  		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params)) $fullDesc = SGO_Helper_GeneralFunction::displayFullDesc($params);
  		else $fullDesc = null;*/

  	    $this->view->suggestionType = $this->_suggestType;
  		
  		if($change_id)
  		{  
		
			$this->view->changes_id = $change_id;
			$this->view->status_type = $this->_masterglobalstatus;
			  //die($change_id);
			   //cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
  	    //$changes_id = strip_tags( trim($this->_getParam('changes_id')) );
  	      
	    $select = $this->_db->select()
		                     ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'))
		                     ->where('CHANGES_ID='.$this->_db->quote($change_id))
		                     ->where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$result = $this->_db->fetchRow($select);
		//die($select);
		$this->view->interface = $result['CHANGES_FLAG'];
      
		if(empty($result))  $this->_redirect('/notification/invalid/index');
		
		//END cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
		if($result)
		{
	  		    //$this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');
	  		    //$this->view->status_type = $this->_masterglobalstatus;
	  		    
	  			//$changeId  = $zf_filter_input->changes_id;
				//$this->view->changeId = $changeId;
	  			//$resultdata = $this->getTempCustomer($changeId);
				  $resultdata  = $this->_db->fetchRow(
					$this->_db->select()
							  ->from(array('T' => 'TEMP_DEBITCARD'))
							  ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
							  ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
							  ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=T.DEBIT_NUMBER',array('USER_ID'))
							  ->joinleft(array('H' => 'M_USER'),'F.USER_ID=H.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
							  ->where('T.CHANGES_ID = ?', $change_id)
							  );
							  // echo '<pre>';
				//var_dump($resultdata);die;
	  			$keyValue  = $resultdata['CUST_ID'];
				$total = count($resultdata);
				$this->view->totaldata = $total;
				//var_dump($total);die;

				//suggest data
				$this->view->changes_id     = $resultdata['CHANGES_ID'];
				$this->view->changes_type   = $resultdata['CHANGES_TYPE'];
				$this->view->changes_status = $resultdata['CHANGES_STATUS'];
				$this->view->read_status    = $resultdata['READ_STATUS'];
				$this->view->created        = $resultdata['CREATED'];
				$this->view->created_by     = $resultdata['CREATED_BY'];

				if($resultdata['CHANGES_TYPE'] == 'S'){
				   $this->view->changes_name  = 'Suspend';
				}elseif($resultdata['CHANGES_TYPE'] == 'U'){
					$this->view->changes_name  = 'Unsuspend';
				}elseif($resultdata['CHANGES_TYPE'] == 'L'){
					$this->view->changes_name  = 'Delete';
				}
			
				
				//temp corporate debit card data
				$this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
       			$this->view->cust_name    = $resultdata['CUST_NAME'];
				$this->view->debit_number      = strtoupper($resultdata['DEBIT_NUMBER']);

				if(!empty($resultdata['USER_FULLNAME'])){
					$this->view->debit_name    = $resultdata['USER_FULLNAME'];
				}else{
					$this->view->debit_name    = $resultdata['CUST_NAME'];
				}	
				
				if(!empty($resultdata['USER_EMAIL'])){
					$this->view->debit_email     = $resultdata['USER_EMAIL'];
				}else{
					$this->view->debit_email     = $resultdata['CUST_EMAIL'];
				}
				
				if($resultdata['DEBIT_STATUS'] == '1'){
					$this->view->status  = 'Active';
				}elseif($resultdata['DEBIT_STATUS'] == '4'){
					 $this->view->status  = 'Suspend';
				}elseif($resultdata['DEBIT_STATUS'] == '3'){
					 $this->view->status  = 'Delete';
				}
			    
				$data = $resultdata;
				$this->view->data = $data;
 
			 
				$m_resultdata  = $this->_db->fetchRow(
					$this->_db->select()
					        ->from(array('A' => 'T_DEBITCARD'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME','D.CUST_EMAIL'))
							->joinleft(array('E' => 'M_USER_DEBIT'),'E.USER_DEBITNUMBER=A.DEBIT_NUMBER',array('USER_ID'))
							->joinleft(array('F' => 'M_USER'),'E.USER_ID=F.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
							->where('A.CUST_ID = ?',$resultdata['CUST_ID'])
							->where('A.REG_NUMBER = ?',$resultdata['REG_NUMBER'])
							->where('A.DEBIT_NUMBER = ?',$resultdata['DEBIT_NUMBER'])
							  );
				
				//var_dump($m_resultdata);die;

				$keyValue  = $m_resultdata['CUST_ID'];
				
				$this->view->m_debit_number   = $m_resultdata['DEBIT_NUMBER'];

				if(!empty($m_resultdata['USER_FULLNAME'])){
					$this->view->m_debit_name     = $m_resultdata['USER_FULLNAME'];
				}else{
					$this->view->m_debit_name     = $m_resultdata['CUST_NAME'];
				}	
				
				if(!empty($m_resultdata['USER_EMAIL'])){
					$this->view->m_debit_email   = $m_resultdata['USER_EMAIL'];
				}else{
					$this->view->m_debit_email   = $m_resultdata['CUST_EMAIL'];
				}
				
				if($m_resultdata['DEBIT_STATUS'] == '1'){
					$this->view->m_status  = 'Active';
				}elseif($m_resultdata['DEBIT_STATUS'] == '4'){
					 $this->view->m_status  = 'Suspend';
				}elseif($m_resultdata['DEBIT_STATUS'] == '3'){
					 $this->view->m_status  = 'Delete';
				}


	  			//$this->view->date_establish = (($custData['DATE_ESTABLISH'])? SGO_Helper_GeneralFunction::convertDate($custData['DATE_ESTABLISH']) : null);
       	  		
	  			//Zend_Debug::dump($resultdata);
	  			//die;
	  			
	  			
              	
				
      			
      			
      			// Application_Helper_General::writeLog('CCCL','View customer changes list');
			}
  		}
  		else
  		{
			
  			$errorRemark = 'Changes Id not found';
			//Application_Helper_General::writeLog('CCCL','');

	  		$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
      		$this->_redirect('/popuperror/index/index');
  		}
  		
  		
  		try 
		{
			$this->_db->beginTransaction();
			$fulldesc = 'CHANGES_ID:'.$change_id;
			if(!$this->_request->isPost()){
  				Application_Helper_General::writeLog('CCCL','View customer changes list');
  			}
			
			$this->_db->commit();
		}
			catch(Exception $e){
			$this->_db->rollBack();
		}

	}
}



