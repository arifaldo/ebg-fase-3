<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class binsetup_EditController extends binsetup_Model_Binsetup
{

	private $_binType;

	public function initController()
	{       
		$this->_binType = $this->_bin['type']['code'];
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		//pengaturan url untuk button back
		$this->setbackURL('/' . $this->_request->getModuleName() . '/index');

		$error_remark = null;
		$key_value = null;

		$AESMYSQL = new Crypt_AESMYSQL();
        $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
        $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
        $cust_bin = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_bin'), $password));

        $this->view->enc_cust_id = $this->_getParam('cust_id');
        $this->view->enc_cust_bin = $this->_getParam('cust_bin');

        $this->view->status_type = $this->_masterglobalstatus;


		$resultdata = $this->getCustomer($cust_id,$cust_bin);
	//echo '<pre>';
	//	 var_dump($resultdata);die;
		if (!empty($resultdata)) {
			$this->view->cust_id = $resultdata['CUST_ID'];
			$this->view->cust_name = $resultdata['CUST_NAME'];
			$this->view->cif = $resultdata['CUST_CIF'];
			$this->view->bin_no = $resultdata['CUST_BIN'];
			$this->view->bin_acct = $resultdata['ACCT_NO'];
			$this->view->bin_ccy = $resultdata['CCY'];
			$this->view->bin_acct_name = $resultdata['ACCT_NAME'];
			$this->view->bin_status = $resultdata['CUST_BIN_STATUS'];

			$this->view->bin_type = $resultdata['BIN_TYPE'];
			
			if($resultdata['BIN_TYPE'] == 1){
			$this->view->bin_type_text = 'eCollection';
			}else{
			$this->view->bin_type_text = 'VA Debit';
			}
			$this->view->h2h = $resultdata['HOSTTOHOST'];
			$this->view->url_inquiry = $resultdata['URL_INQUIRY'];
			$this->view->url_payment = $resultdata['URL_PAYMENT'];
			$this->view->url_payment = $resultdata['URL_PAYMENT'];

			$this->view->ex_time_type = $resultdata['EX_TIME_TYPE'];

			//ecollection
			$this->view->vptype = $resultdata['VA_TYPE'];
			$this->view->expiredtime = $resultdata['EXP_TIME'];
			$this->view->ex_time = $resultdata['EX_TIME'];
			$this->view->allowskip = $resultdata['SKIP_ERROR'];
			$this->view->partial_payment = $resultdata['PARTIAL_TYPE'];
			$this->view->billing_method = $resultdata['BILLING_TYPE'];
			$this->view->buyer_fee = $resultdata['BUYER_FEE'];
			$this->view->seller_fee = $resultdata['SELLER_FEE'];
			$this->view->vp_min = $resultdata['VP_MIN'];
			$this->view->vp_max = $resultdata['VP_MAX'];

			//vadebit
			$this->view->allowskip_debit = $resultdata['SKIP_ERROR'];
			$this->view->va_credit_max = $resultdata['VA_CREDIT_MAX'];
			$this->view->va_daily_limit = $resultdata['VA_DAILY_LIMIT'];
			$this->view->remaining_available_credit = $resultdata['REMAINING_AVAILABLE_CREDIT'];

			//vadebit
			if ($resultdata['BIN_TYPE'] == $this->_binType['vadebit'] && $resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
				if ($resultdata['EX_TIME_TYPE'] == 2) {
					$this->view->ex_time_weekly = $resultdata['EX_TIME'];
				}
				else if ($resultdata['EX_TIME_TYPE'] == 3) {
					$this->view->ex_time_monthly = $resultdata['EX_TIME'];
				}
				else if ($resultdata['EX_TIME_TYPE'] == 4) {
					$exTime = explode('_', $resultdata['EX_TIME']); 
					$this->view->ex_time_yearly_date = $exTime[0];
					$this->view->ex_time_yearly_month = $exTime[1];
				}
			}
		}

		//table
		$fields = array(
			'BIN'    => array(
				'field' => 'BIN',
				'label' => $this->language->_('BIN'),
				'sortable' => true
			),
			'type'  => array(
				'field' => 'type',
				'label' =>  $this->language->_('Type'),
				'sortable' => true
			),
			'status' => array(
				'field' => 'status',
				'label' => $this->language->_('Status'),
				'sortable' => true
			),
			'masterAccount' => array(
				'field' => 'masterAccount',
				'label' => $this->language->_('Master Account'),
				'sortable' => true
			),
			'ccy' => array(
				'field' => 'ccy',
				'label' => $this->language->_('CCY'),
				'sortable' => true
			),
			'parameter' => array(
				'field' => 'parameter',
				'label' => $this->language->_('Parameter'),
				'sortable' => true
			),
		);
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'masterAccount');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->fields = $fields;

		$select = $this->_db->select()
			->from(array('A' => 'BIN_TEMPLATE'), array('*'));
		$this->view->bintemplate 					= $this->_db->fetchAll($select);

		if ($this->_request->isPost() && $this->_getParam('submit') == $this->language->_('Submit')) {
			$this->view->inputParam = $this->getRequest()->getParams();
			$cust_id = strtoupper($this->_getParam('cust_id'));
			$cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => 12))) ? $cust_id : null;

			/*echo strtoupper($this->_getParam('cust_id'));
			die;*/



			$customer_filter = array();
			$haystack_cust_type = array($this->_custType['code']['company'], $this->_custType['code']['individual']);
			$haystack_step_approve = array($this->_masterhasStatus['code']['yes'], $this->_masterhasStatus['code']['no']);
			$cust_acc = array();
			$cust_limit = array();
			$tempacc = array('idr', 'usd');

			$bin_type = $this->_request->getParams()['bin_type'];
			$h2h = $this->_request->getParams()['h2h'];

			$filters = array(
				'account'        => array('StripTags', 'StringTrim'),
				'account_name'        => array('StripTags', 'StringTrim'),
				'account_alias'        => array('StripTags', 'StringTrim'),
				'ccy'        => array('StripTags', 'StringTrim'),
				'bank_name'        => array('StripTags', 'StringTrim'),
				'branch'        => array('StripTags', 'StringTrim'),
				'cif'        => array('StripTags', 'StringTrim'),
				'bin'        => array('StripTags', 'StringTrim'),
				'bin_type'        => array('StripTags', 'StringTrim'),
				'ex_time'        => array('StripTags', 'StringTrim'),
				// 'patrtial_payment'        => array('StripTags','StringTrim'),
				// 'patrtial_payment'        => array('StripTags','StringTrim'),


				// 'patrtial_type'        => array('StripTags','StringTrim'),
				// 'unique'        => array('StripTags','StringTrim'),
				// 'calinv'        => array('StripTags','StringTrim'),
				// 'caldepo'        => array('StripTags','StringTrim'),
				// 'status_after'        => array('StripTags','StringTrim'),
				'h2h'        => array('StripTags', 'StringTrim'),
				'url_inquiry'        => array('StripTags', 'StringTrim'),
				'url_payment'        => array('StripTags', 'StringTrim'),
			);

			if ($bin_type == $this->_binType['ecollection']) {
				$filters['expiredtime'] = array('StripTags', 'StringTrim');
				$filters['ex_time_type'] = array('StripTags', 'StringTrim');
				$filters['vptype'] = array('StripTags', 'StringTrim');
				$filters['partial_payment'] = array('StripTags', 'StringTrim');
				$filters['billing_method'] = array('StripTags', 'StringTrim');
				$filters['buyer_fee'] = array('StripTags', 'StringTrim');
				$filters['seller_fee'] = array('StripTags', 'StringTrim');
				$filters['vp_min'] = array('StripTags', 'StringTrim');
				$filters['vp_max'] = array('StripTags', 'StringTrim');
				$filters['allowskip'] = array('StripTags', 'StringTrim');
			}
			else if($bin_type == $this->_binType['vadebit']){
				$filters['allowskip_debit'] = array('StripTags', 'StringTrim');
				$filters['va_credit_max'] = array('StripTags', 'StringTrim');
				$filters['va_daily_limit'] = array('StripTags', 'StringTrim');
				$filters['remaining_available_credit'] = array('StripTags', 'StringTrim');
				$filters['ex_time_type_debit'] = array('StripTags', 'StringTrim');
			}
			
			if ($bin_type == $this->_binType['vadebit']) {
				$validators =  array(
					'va_credit_max'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'va_daily_limit'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'remaining_available_credit'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'h2h'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'url_inquiry'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'url_payment'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),

					'bin_type'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'allowskip_debit' => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'ex_time'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'ex_time_type_debit'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
				);

				if ($h2h == '1') {
					$validators['url_inquiry'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
					$validators['url_payment'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
					$validators['va_credit_max'] = array(
						'allowEmpty' => true,

						'messages' => array()
					);
					$validators['va_daily_limit'] = array(
						'allowEmpty' => true,

						'messages' => array()
					);
					$validators['remaining_available_credit'] = array(
						'allowEmpty' => true,

						'messages' => array()
					);
				}

			} else {
				$validators =  array(
					// 'cust_bin'        => array('NotEmpty',
					//                                                   'Digits',
					//                                                    array('StringLength',array('min'=>1,'max'=>5)),
					//                                                       array('Db_NoRecordExists',array('table'=>'M_CUSTOMER_BIN','field'=>'CUST_BIN')),
					//                                                       array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER_BIN','field'=>'CUST_BIN')),
					//                                                       'messages' => array($this->language->_('Can not be empty'),
					//                                                                           $this->language->_('Invalid BIN Format'),
					//                                                                           $this->language->_('BIN lenght cannot be more than 5'), //length
					//                                                                           $this->language->_('BIN already in use. Please use another'),
					//                                                                           $this->language->_('BIN already suggested. Please use another'))
					//                                                      ),
					'buyer_fee'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'seller_fee'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),

					'ex_time'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'ex_time_type'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),


					'h2h'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'url_inquiry'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),
					'url_payment'        => array(
						'allowEmpty' => true,

						'messages' => array()
					),

					'vp_max'        => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'vp_min'        => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'bin_type'        => array(),
					// 'unique'        => array(),
					// 'caldepo'        => array(),
					// 'calinv'        => array(),
					'account'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'account_name'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'account_alias'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'cif'                => array(),
					'bin'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
					'expiredtime'        => array(),
					'vptype'             => array(),
					'partial_payment'    => array(),

					'billing_method'     => array(),
					'allowskip'          => array(),

					// 'patrtial_type'                => array('allowEmpty' => true,

					// 						   'messages' => array(

					// 											   )),
					// 'status_after'                => array('NotEmpty','messages' => array($this->language->_('Can not be empty'))),
				);

				if ($h2h == '1') {
					$validators['url_inquiry'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
					$validators['url_payment'] = array('NotEmpty', 'messages' => array($this->language->_('Can not be empty')));
					$validators['vp_max'] = array(
						'allowEmpty' => true,

						'messages' => array()
					);
					$validators['vp_min'] = array(
						'allowEmpty' => true,

						'messages' => array()
					);
				}
			}


			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

			//validasi emobile, cust phone dan cust fax = harus dimulai dengan '62'

			if ($zf_filter_input->isValid()) {
				//      	  $info = 'Customer ID = '.$zf_filter_input->cust_id.', Customer Name = '.$zf_filter_input->cust_name;

				$info = "EDIT BIN";
				$cust_data = $this->_custData;

				foreach ($validators as $key => $value) {
					if ($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;
				}

				try {


					$this->_db->beginTransaction();

					$cust_data['CUST_BIN_STATUS'] 	= 1;
					$cust_data['CUST_ID']     = $resultdata['CUST_ID'];
					$cust_data['CUST_BIN']     = $resultdata['CUST_BIN'];
					$cust_data['BIN_CREATED']     = $resultdata['BIN_CREATED'];
					$cust_data['BIN_CREATEDBY']   = $resultdata['BIN_CREATEDBY'];
					$cust_data['BIN_UPDATED']     = $resultdata['BIN_UPDATED'];
					$cust_data['BIN_UPDATEDBY']   = $resultdata['BIN_CREATEDBY'];


					$cust_data['BANK_NAME']   = $resultdata['BANK_NAME'];
					$cust_data['BRANCH_NAME']   = $resultdata['BRANCH_NAME'];
					$cust_data['ACCT_NO']   = $resultdata['ACCT_NO'];
					$cust_data['ACCT_NAME']   = $resultdata['ACCT_NAME'];
					$cust_data['ACCT_ALIAS_NAME']   = $resultdata['ACCT_ALIAS_NAME'];
					$cust_data['CCY']   = 'IDR';

					$cust_data['HOSTTOHOST']   = $zf_filter_input->h2h;
					$cust_data['URL_INQUIRY']   = $zf_filter_input->url_inquiry;
					$cust_data['URL_PAYMENT']   = $zf_filter_input->url_payment;

					//eCollection
					if ($bin_type == $this->_binType['ecollection']) {
						$cust_data['VA_TYPE']   = $zf_filter_input->vptype;

						$cust_data['BIN_TYPE']   = $zf_filter_input->bin_type;

						$cust_data['EXP_TIME']   = $zf_filter_input->expiredtime;
						$cust_data['EX_TIME']   = $zf_filter_input->ex_time;
						$cust_data['EX_TIME_TYPE']   = $zf_filter_input->ex_time_type;

						$cust_data['PARTIAL_TYPE']   = $zf_filter_input->partial_payment;
						$cust_data['BILLING_TYPE']   = $zf_filter_input->billing_method;

						$buyer_fee =	Application_Helper_General::convertDisplayMoney($zf_filter_input->buyer_fee);
						$seller_fee =	Application_Helper_General::convertDisplayMoney($zf_filter_input->seller_fee);
						$vp_min =	Application_Helper_General::convertDisplayMoney($zf_filter_input->vp_min);
						$vp_max =	Application_Helper_General::convertDisplayMoney($zf_filter_input->vp_max);
						$cust_data['BUYER_FEE']   = $buyer_fee;
						$cust_data['SELLER_FEE']   = $seller_fee;
						if ($bin_type == $this->_binType['vadebit']) {
							$cust_data['VP_MIN']   = 0;
							$cust_data['VP_MAX']   = 0;
						} else {
							$cust_data['VP_MIN']   = $vp_min;
							$cust_data['VP_MAX']   = $vp_max;
						}

						$cust_data['SKIP_ERROR']   = $zf_filter_input->allowskip;
					}
					// VA Debit
					else {
						$cust_data['SKIP_ERROR']   = $zf_filter_input->allowskip_debit;
						$vaCreditMax =	Application_Helper_General::convertDisplayMoney($zf_filter_input->va_credit_max);
						$vaDailyLimit =	Application_Helper_General::convertDisplayMoney($zf_filter_input->va_daily_limit);
						$cust_data['VA_CREDIT_MAX'] = $vaCreditMax;
						$cust_data['VA_DAILY_LIMIT'] = $vaDailyLimit;
						$cust_data['REMAINING_AVAILABLE_CREDIT'] = $zf_filter_input->remaining_available_credit;
						$cust_data['EX_TIME_TYPE']   = $zf_filter_input->ex_time_type_debit;

						// weekly
						if ($zf_filter_input->ex_time_type_debit == '2') {
							$ex_time = $this->_getParam('ex_time_weekly');
						}
						//monthly
						else if($zf_filter_input->ex_time_type_debit == '3') {
							$ex_time = $this->_getParam('ex_time_monthly');

						}
						//yearly
						else if($zf_filter_input->ex_time_type_debit == '4'){
							$ex_time = $this->_getParam('ex_time_yearly_date').'_'.$this->_getParam('ex_time_yearly_month');
						}
						//daily
						else{
							$ex_time = '';
						}

						$cust_data['EX_TIME']   = $ex_time;
					}

					$cust_data['BIN_SUGGESTED']    = new Zend_Db_Expr('now()');
					$cust_data['BIN_SUGGESTEDBY']  = $this->_userIdLogin;
					// echo '<pre>';
					// print_r($this->getRequest()->getParams());
					// print_r($cust_data);die;

					$cust_data = $this->resetData($cust_data);

					// insert ke T_GLOBAL_CHANGES
					$change_id = $this->suggestionWaitingApproval('BIN', $info, $this->_changeType['code']['edit'], null, 'M_CUSTOMER_BIN', 'TEMP_CUSTOMER_BIN', 'Bin', $resultdata['CUST_BIN'], $resultdata['CUST_ID'], $resultdata['CUST_NAME']);
					$this->insertTempCustomer($change_id, $cust_data);

					//log CRUD
					Application_Helper_General::writeLog('BNAD', 'Customer BIN has been added, Cust ID : ' . $resultdata['CUST_ID'] . ' ,BIN : ' . $resultdata['CUST_BIN'] . ' ,Change id : ' . $change_id);
					$this->_db->commit();

					$this->_redirect('/notification/submited/index');
				} catch (Exception $e) {
					// print_r($e);
					// die;
					$this->_db->rollBack();
					$error_remark = 'exception';
				}


				if (isset($error_remark)) {
				} else {
					$this->view->success = 1;
					$msg   = 'Record added sucessfully';
					$key_value = $zf_filter_input->cust_id;
					$this->view->customer_msg = $msg;
					//insert log
					try {
						$this->_db->beginTransaction();

						Application_Helper_General::writeLog('BNAD', ' Create Customer BIN [' . $this->view->cust_id . ']');

						$this->_db->commit();
					} catch (Exception $e) {
						$this->_db->rollBack();
						Application_Log_GeneralLog::technicalLog($e);
					}
				}
			} // END IF IS VALID
			else {
				$this->view->error = 1;
				$this->view->cust_id   = ($zf_filter_input->isValid('cust_id')) ? $zf_filter_input->cust_id : $this->_getParam('cust_id');
				$this->view->cust_name = ($zf_filter_input->isValid('cust_name')) ? $zf_filter_input->cust_name : $this->_getParam('cust_name');
				$this->view->cust_bin = ($zf_filter_input->isValid('cust_bin')) ? $zf_filter_input->cust_bin : $this->_getParam('cust_bin');

				// $this->view->stats_due   = ($zf_filter_input->isValid('stats_due'))? $zf_filter_input->stats_due : $this->_getParam('stats_due');
				$this->view->vp_max   = ($zf_filter_input->isValid('vp_max')) ? $zf_filter_input->vp_max : $this->_getParam('vp_max');
				$this->view->vp_min   = ($zf_filter_input->isValid('vp_min')) ? $zf_filter_input->vp_min : $this->_getParam('vp_min');
				$this->view->seller_fee   = ($zf_filter_input->isValid('seller_fee')) ? $zf_filter_input->seller_fee : $this->_getParam('seller_fee');
				$this->view->buyer_fee   = ($zf_filter_input->isValid('buyer_fee')) ? $zf_filter_input->buyer_fee : $this->_getParam('buyer_fee');
				$this->view->calinv   = ($zf_filter_input->isValid('buyer_fee')) ? $zf_filter_input->buyer_fee : $this->_getParam('buyer_fee');
				// $this->view->caldepo   = ($zf_filter_input->isValid('caldepo'))? $zf_filter_input->caldepo : $this->_getParam('caldepo');
				// $this->view->partial_type   = ($zf_filter_input->isValid('partial_type'))? $zf_filter_input->partial_type : $this->_getParam('partial_type');
				// $this->view->unique   = ($zf_filter_input->isValid('unique'))? $zf_filter_input->unique : $this->_getParam('unique');
				// $this->view->partial_type   = ($zf_filter_input->isValid('partial_type'))? $zf_filter_input->partial_type : $this->_getParam('partial_type');
				$this->view->partial_payment   = ($zf_filter_input->isValid('partial_payment')) ? $zf_filter_input->partial_payment : $this->_getParam('partial_payment');
				$this->view->billing_method   = ($zf_filter_input->isValid('billing_method')) ? $zf_filter_input->billing_method : $this->_getParam('billing_method');
				$this->view->vptype   = ($zf_filter_input->isValid('vptype')) ? $zf_filter_input->vptype : $this->_getParam('vptype');
				$this->view->expiredtime   = ($zf_filter_input->isValid('expiredtime')) ? $zf_filter_input->expiredtime : $this->_getParam('expiredtime');
				$this->view->ex_time   = ($zf_filter_input->isValid('ex_time')) ? $zf_filter_input->ex_time : $this->_getParam('ex_time');
				$this->view->ex_time_type   = ($zf_filter_input->isValid('ex_time_type')) ? $zf_filter_input->ex_time_type : $this->_getParam('ex_time_type');
				$this->view->allowskip   = ($zf_filter_input->isValid('allowskip')) ? $zf_filter_input->allowskip : $this->_getParam('allowskip');
				$this->view->amounttype   = ($zf_filter_input->isValid('amounttype')) ? $zf_filter_input->amounttype : $this->_getParam('amounttype');
				$this->view->ccy   = ($zf_filter_input->isValid('ccy')) ? $zf_filter_input->ccy : $this->_getParam('ccy');
				$this->view->cif   = ($zf_filter_input->isValid('cif')) ? $zf_filter_input->cif : $this->_getParam('cif');
				$this->view->bank   = ($zf_filter_input->isValid('bank')) ? $zf_filter_input->bank : $this->_getParam('bank');
				$this->view->branch   = ($zf_filter_input->isValid('branch')) ? $zf_filter_input->branch : $this->_getParam('branch');
				$this->view->account_alias   = ($zf_filter_input->isValid('account_alias')) ? $zf_filter_input->account_alias : $this->_getParam('account_alias');
				$this->view->account   = ($zf_filter_input->isValid('account')) ? $zf_filter_input->account : $this->_getParam('account');
				$this->view->account_name   = ($zf_filter_input->isValid('account_name')) ? $zf_filter_input->account_name : $this->_getParam('account_name');

				$this->view->url_payment   = ($zf_filter_input->isValid('url_payment')) ? $zf_filter_input->url_payment : $this->_getParam('url_payment');
				$this->view->url_inquiry   = ($zf_filter_input->isValid('url_inquiry')) ? $zf_filter_input->url_inquiry : $this->_getParam('url_inquiry');
				$this->view->h2h   = ($zf_filter_input->isValid('h2h')) ? $zf_filter_input->h2h : $this->_getParam('h2h');

				$this->view->bin_type = $bin_type;

				//va debit
				$this->view->allowskip_debit   = ($zf_filter_input->isValid('allowskip_debit')) ? $zf_filter_input->allowskip_debit : $this->_getParam('allowskip_debit');
				$this->view->va_credit_max   = ($zf_filter_input->isValid('va_credit_max')) ? $zf_filter_input->vacreditmax : $this->_getParam('va_credit_max');
				$this->view->va_daily_limit   = ($zf_filter_input->isValid('va_daily_limit')) ? $zf_filter_input->va_daily_limit : $this->_getParam('va_daily_limit');
				$this->view->remaining_available_credit   = ($zf_filter_input->isValid('remaining_available_credit')) ? $zf_filter_input->remaining_available_credit : $this->_getParam('remaining_available_credit');
				$this->view->ex_time_type_debit   = ($zf_filter_input->isValid('ex_time_type_debit')) ? $zf_filter_input->ex_time_type_debit : $this->_getParam('ex_time_type_debit');
				$this->view->ex_time_weekly   = ($zf_filter_input->isValid('ex_time_weekly')) ? $zf_filter_input->ex_time_weekly : $this->_getParam('ex_time_weekly');
				$this->view->ex_time_monthly   = ($zf_filter_input->isValid('ex_time_monthly')) ? $zf_filter_input->ex_time_monthly : $this->_getParam('ex_time_monthly');
				$this->view->ex_time_yearly_date  = ($zf_filter_input->isValid('ex_time_yearly_date')) ? $zf_filter_input->ex_time_yearly_date : $this->_getParam('ex_time_yearly_date');
				$this->view->ex_time_yearly_month  = ($zf_filter_input->isValid('ex_time_yearly_month')) ? $zf_filter_input->ex_time_yearly_month : $this->_getParam('ex_time_yearly_month');


				$error = $zf_filter_input->getMessages();
				// print_r($error);die;
				//format error utk ditampilkan di view html 
				$errorArray = null;
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$errorArray[$keyRoot] = $errorString;
					}
				}
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$keyname = "error_" . $keyRoot;
						// print_r($keyname);die;
						$this->view->$keyname = $this->language->_($errorString);
					}
				}

				if (isset($cek_multiple_email) && $cek_multiple_email == false) $errorArray['cust_email'] = $this->language->_('Invalid format');

				$this->view->customer_msg  = $errorArray;
			}
		} //END if($this->_request->isPost())

		$this->view->modulename      =  $this->_request->getModuleName();
	}

	public function resetData($cust_data){

		//ecollection
		if ($cust_data['HOSTTOHOST'] == '1') {

			//ecollection
			$cust_data['VA_TYPE'] = '';
			$cust_data['EXP_TIME'] = '';
			$cust_data['EX_TIME'] = '';
			$cust_data['EX_TIME_TYPE'] = '';
			$cust_data['SKIP_ERROR'] = '';
			$cust_data['PARTIAL_TYPE'] = '';
			$cust_data['BILLING_TYPE'] = '';
			$cust_data['BUYER_FEE'] = '0';
			$cust_data['SELLER_FEE'] = '0';
			$cust_data['VP_MIN'] = '0';
			$cust_data['VP_MAX'] = '0';

			//vadebit
			$cust_data['VA_CREDIT_MAX'] = '0';
			$cust_data['VA_DAILY_LIMIT'] = '0';
			$cust_data['REMAINING_AVAILABLE_CREDIT'] = '';
		}
		else if($cust_data['BIN_TYPE'] == $this->_binType['ecollection']){
			//vadebit
			$cust_data['VA_CREDIT_MAX'] = '0';
			$cust_data['VA_DAILY_LIMIT'] = '0';
			$cust_data['REMAINING_AVAILABLE_CREDIT'] = '';

			$cust_data['URL_PAYMENT'] = '';
			$cust_data['URL_INQUIRY'] = '';
		}
		else if($cust_data['BIN_TYPE'] == $this->_binType['vadebit']){
			$cust_data['VA_TYPE'] = '';
			$cust_data['EXP_TIME'] = '';
			$cust_data['PARTIAL_TYPE'] = '';
			$cust_data['BILLING_TYPE'] = '';
			$cust_data['BUYER_FEE'] = '0';
			$cust_data['SELLER_FEE'] = '0';
			$cust_data['VP_MIN'] = '0';
			$cust_data['VP_MAX'] = '0';

			$cust_data['URL_PAYMENT'] = '';
			$cust_data['URL_INQUIRY'] = '';
		}

		return $cust_data;
	}
}
