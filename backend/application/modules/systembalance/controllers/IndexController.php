<?php

require_once 'Zend/Controller/Action.php';

class systembalance_IndexController extends Application_Main 
{
	public function initController()
	{       			
		$companyCode = $this->_db->fetchAll(
					$this->_db->select()
					   ->from('M_CUSTOMER',array('CUST_ID'))
					   );
		$listCompCode = array(''=>'--- Any Value ---');
		$listCompCode += Application_Helper_Array::listArray($companyCode,'CUST_ID','CUST_ID');
		$this->view->listComp = $listCompCode; 
		
		$listCcy = array(''=>'--- Any Value ---');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->listCcy = $listCcy;

		$listYesNo = array('' => '--- Any Value ---',												
						   '0' => 'No',												
						   '1' => 'Yes'
						  );	
						  
		$this->view->listYesNo = $listYesNo;
	}
 
  public function indexAction() 
  {  
	$displayDateFormat  = $this->_dateDisplayFormat;
	$databaseSaveFormat = $this->_dateDBFormat;
		
    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$fields = array('cust_id'   => array('field'    => 'CUST_ID',
											  'label'    => 'Customer ID',
											  'sortable' => true),
					   'acct_no'  => array('field' => 'ACCT_NO',
											   'label' => 'Account No.',
											   'sortable' => true),
					   'acct_name'   => array('field'    => 'ACCT_NAME',
											  'label'    => 'Account Name',
											  'sortable' => true),
					   'ccy_id'   => array('field'    => 'CCY_ID',
											  'label'    => 'Currency',
											  'sortable' => true),
					   'issys_bal'   => array('field'    => 'ISSYSTEMBALANCE',
											  'label'    => 'System Balance',
											  'sortable' => true),
					   'tolerance'   => array('field'    => 'PLAFOND',
											  'label'    => 'Tolerance',
											  'sortable' => true),
					   'plafond_date'   => array('field'    => 'ISPLAFONDDATE',
											  'label'    => 'Tolerance Date',
											  'sortable' => true),
					   'plafond_start'   => array('field'    => 'PLAFOND_START',
											  'label'    => 'Tolerance Start Date',
											  'sortable' => true),
					   'plafond_end'   => array('field'    => 'PLAFOND_END',
											  'label'    => 'Tolerance End Date',
											  'sortable' => true),
					   'history'   => array('field'    => 'history',
											  'label'    => 'History',
											  'sortable' => false),
            		);
					
		//get page, sortby, sortdir
		$page = $this->_getParam('page');
		$sortBy = $this->_getParam('sortby');
		$sortDir = $this->_getParam('sortdir');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
     
         //get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array('filter' 	 		=> array('StringTrim','StripTags'),
						   'comp_code'   		=> array('StringTrim','StripTags','StringToUpper'),
						   'acct_source' 		=> array('StringTrim','StripTags'),
						   'acct_name'	 		=> array('StringTrim','StripTags','StringToUpper'),
						   'ccy'      	 		=> array('StringTrim','StripTags','StringToUpper'),
						   'system_balance' 	=> array('StringTrim','StripTags','StringToUpper'),
						   'tolerance_date' 	=> array('StringTrim','StripTags','StringToUpper'),
						   'start_date_from' 	=> array('StringTrim','StripTags'),
						   'start_date_to' 		=> array('StringTrim','StripTags'),
						   'end_date_from'		=> array('StringTrim','StripTags'),
						   'end_date_to'		=> array('StringTrim','StripTags')
						  );
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
			
		$filter = $zf_filter->getEscaped('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$select = $this->_db->select()
								->from(array('M_CUSTOMER_ACCT'),
									   array('CUST_ID', 'ACCT_NO', 'ACCT_NAME', 'CCY_ID','ISSYSTEMBALANCE','PLAFOND','ISPLAFONDDATE','PLAFOND_START','PLAFOND_END'));

		$select->order($sortBy.' '.$sortDir);
		
		if($filter == 'Set Filter' || $csv || $pdf)
		{
			$compCode = $zf_filter->getEscaped('comp_code');
			$acctSource = $zf_filter->getEscaped('acct_source');
			$acctName = $zf_filter->getEscaped('acct_name');
			$ccy = $zf_filter->getEscaped('ccy');
			$sysBal = $zf_filter->getEscaped('system_balance');
			$tolDate = $zf_filter->getEscaped('tolerance_date');
				
			$startFrom = (Zend_Date::isDate($zf_filter->getEscaped('start_date_from'),$displayDateFormat))?
								new Zend_Date($zf_filter->getEscaped('start_date_from'),$displayDateFormat):
								false;
			$startTo = (Zend_Date::isDate($zf_filter->getEscaped('start_date_to'),$displayDateFormat))?
								new Zend_Date($zf_filter->getEscaped('start_date_to'),$displayDateFormat):
								false;		
				
			$endFrom = (Zend_Date::isDate($zf_filter->getEscaped('end_date_from'),$displayDateFormat))?
								new Zend_Date($zf_filter->getEscaped('end_date_from'),$displayDateFormat):
								false;
			$endTo = (Zend_Date::isDate($zf_filter->getEscaped('end_date_to'),$displayDateFormat))?
								new Zend_Date($zf_filter->getEscaped('end_date_to'),$displayDateFormat):
								false;		
			
		   //where clauses
			if($compCode)
				$select->where("UPPER(CUST_ID) = ".$this->_db->quote($compCode));
			if($acctSource)
				$select->where("ACCT_NO LIKE ".$this->_db->quote('%'.$acctSource.'%'));
			if($acctName)
				$select->where("UPPER(ACCT_NAME) LIKE ".$this->_db->quote('%'.$acctName.'%'));
			if($ccy)
				$select->where("CCY_ID = ".$this->_db->quote($ccy));
			if($sysBal!=null)
				$select->where("ISSYSTEMBALANCE = ".$this->_db->quote($sysBal));
			if($tolDate!=null)
				$select->where("ISPLAFONDDATE = ".$this->_db->quote($tolDate));
			if($startFrom)
				$select->where("CONVERT(date,PLAFOND_START) >= CONVERT(DATE,".$this->_db->quote($startFrom->toString($databaseSaveFormat)).")");
			if($startTo)
				$select->where("CONVERT(date,PLAFOND_START) <= CONVERT(DATE,".$this->_db->quote($startTo->toString($databaseSaveFormat)).")");
			if($endFrom)
				$select->where("CONVERT(date,PLAFOND_END) >= CONVERT(DATE,".$this->_db->quote($endFrom->toString($databaseSaveFormat)).")");
			if($endTo)
				$select->where("CONVERT(date,PLAFOND_END) <= CONVERT(DATE,".$this->_db->quote($endTo->toString($databaseSaveFormat)).")");

			$this->view->comp_code = $compCode;
			$this->view->acct_source = $acctSource;
			$this->view->acct_name = $acctName;
			$this->view->ccy = $ccy;
			$this->view->system_balance = $sysBal;
			$this->view->tolerance_date = $tolDate;
			if($startFrom) $this->view->start_date_from = $startFrom->toString($displayDateFormat);
			if($startTo) $this->view->start_date_to = $startTo->toString($displayDateFormat);
			if($endFrom) $this->view->end_date_from = $endFrom->toString($displayDateFormat);
			if($endTo) $this->view->end_date_to = $endTo->toString($displayDateFormat);
			
			if($csv || $pdf)
			{
				$arr = $this->_db->fetchAll($select);
				//$select->order($sortBy.' '.$sortDir);  
				$header = array('Customer ID','Account No.','Account Name','Currency','System Balance','Tolerance','Tolerance Date','Tolerance Start Date','Tolerance End Date');
				
				foreach ($arr as $key => $row)
				{
					$arr[$key]["CUST_ID"]= isset($row['CUST_ID']) ? $row['CUST_ID'] : "N/A";
					$arr[$key]["ACCT_NO"]= isset($row['ACCT_NO']) ? $row['ACCT_NO'] : "N/A";
					$arr[$key]["ACCT_NAME"]= isset($row['ACCT_NAME']) ? $row['ACCT_NAME'] : "N/A";
					$arr[$key]["CCY_ID"]= isset($row['CCY_ID']) ? $row['CCY_ID'] : "N/A";
					$arr[$key]["ISSYSTEMBALANCE"]= ($row['ISSYSTEMBALANCE']==1) ? "Yes" : "No";
					$arr[$key]["PLAFOND"]= isset($row['PLAFOND']) ? Application_Helper_General::displayMoney($row['PLAFOND']) : "N/A";
					$arr[$key]["ISPLAFONDDATE"]= ($row['ISPLAFONDDATE']==1) ? "Yes" : "No";
					$arr[$key]["PLAFOND_START"]= isset($row['PLAFOND_START']) ? Application_Helper_General::convertDate($row['PLAFOND_START'],'dd/MM/yyyy','yyyy-MM-dd HH:mm:ss') : "N/A";
					$arr[$key]["PLAFOND_END"]= isset($row['PLAFOND_END']) ? Application_Helper_General::convertDate($row['PLAFOND_END'],'dd/MM/yyyy','yyyy-MM-dd HH:mm:ss') : "N/A";
				}
				if($csv)
				{
					Application_Helper_General::writeLog('BLLS','Downloading CSV System Balance List');
					$this->_helper->download->csv($header,$arr,null,'System Balance List');
				}
				
				if($pdf)
				{
					Application_Helper_General::writeLog('BLLS','Downloading PDF System Balance List');
					$this->_helper->download->pdf($header,$arr,null,'System Balance List');
				}
			}
			else
			{
				Application_Helper_General::writeLog('BLLS','Filtering System Balance List');
			}
		}			
        if(!$this->_request->isPost())
		{
			Application_Helper_General::writeLog('BLLS','Viewing System Balance List');
		}
		$this->paging($select);
		
		$this->view->fields = $fields;
		$this->view->filter = $filter;
  }
}


