<?php
require_once 'Zend/Controller/Action.php';

class token_IndexController extends Application_Main {

	public function initController()
	{
		$this->_helper->layout->setLayout('newlayout');
	}

	public function indexAction(){

		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

		$token = new Service_Token($custId, $userId);
		$res = $token->tokenHardwareList(array());
		$resData = (isset($res['ResponseData']) && count($res['ResponseData']) ? $res['ResponseData'] : array());
		$tokenData = $this->view->tokenData = $resData;
		
		$this->view->paginator = $resData;

	}
}