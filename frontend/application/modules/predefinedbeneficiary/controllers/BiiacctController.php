<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';

class predefinedbeneficiary_BiiacctController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	protected $_destinationUploadDir = '';
	protected $_listCCYValidate = '';
	
	public function initController()
	{       
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 	
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  
		
		$listCcy = array(''=>'-- Select Currency --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;		
	}

	public function indexAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		$fields = array(
						'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => 'Alias Name',
											   'sortable' => true),
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => 'Beneficiary Account',
											   'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => 'Beneficiary Account Name',
											   'sortable' => true),
						'email'  => array('field' => 'BENEFICIARY_EMAIL',
											   'label' => 'Email Address',
											   'sortable' => true),
						'ccy'   => array('field'    => 'CURR_CODE',
											  'label'    => 'CCY',
											  'sortable' => true),
						'favorite'   => array('field'    => 'ISFAVORITE',
											  'label'    => 'Favorite',
											  'sortable' => true),
						'checked'   => array('field'    => 'BENEFICIARY_BANKSTATUS_disp',
											  'label'    => 'Checked by Bank',
											  'sortable' => true),
						'status'   => array('field'    => 'BENEFICIARY_ISAPPROVE_disp',
											  'label'    => 'Status',
											  'sortable' => true),
						'date'   => array('field'    => 'BENEFICIARY_CREATED',
											  'label'    => 'Created Date',
											  'sortable' => true),
						'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
											  'label'    => 'Delete',
											  'sortable' => false)
				);
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','favorite');
		$sortDir = $this->_getParam('sortdir','desc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'alias' 	  	=> array('StringTrim','StripTags'),
							'benef_acct'    => array('StringTrim','StripTags','StringToUpper'),
							'benef_name'    => array('StringTrim','StripTags'),
							'benef_ccy'    	=> array('StringTrim','StripTags','StringToUpper'),
							'favorit'     	=> array('StringTrim','StripTags')
		);
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$alpha = $this->_getParam('alpha');
		$fav = $this->_getParam('fav');
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->alpha = $alpha;
		$this->view->fav = $fav;
		
		/*$select = $this->_db->select()
					   ->from('M_BENEFICIARY',array('BENEFICIARY_ID','BENEFICIARY_ALIAS','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BENEFICIARY_EMAIL','CURR_CODE','ISFAVORITE','BENEFICIARY_BANKSTATUS' => '(CASE BENEFICIARY_BANKSTATUS WHEN 0 THEN \'No\' WHEN 1 THEN \'Yes\' WHEN 2 THEN \'Failed\' END)','BENEFICIARY_ISREQUEST_DELETE','BENEFICIARY_ISAPPROVE' => '(CASE BENEFICIARY_ISAPPROVE WHEN 0 THEN \'Not Approved\' WHEN 1 THEN \'Approved\' END)','BENEFICIARY_CREATED')); 
		$select->where("BENEFICIARY_TYPE = 1");
		$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));*/
		
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		
		$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		if($alpha) $select->where("BENEFICIARY_ALIAS LIKE ".$this->_db->quote($alpha.'%'));
	    if($fav) $select->where('ISFAVORITE=1');
		
		// Zend_Debug::dump($select->query()->fetchAll());die;
		
		if($filter == 'Set Filter')
		{
			$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('benef_acct');
			$fName = $zf_filter->getEscaped('benef_name');
			$fCcy = $zf_filter->getEscaped('benef_ccy');
			$fFav = $zf_filter->getEscaped('favorit');
			
	        if($fAlias)$select->where('UPPER(BENEFICIARY_ALIAS) LIKE '.$this->_db->quote('%'.strtoupper($fAlias).'%'));
	        if($fAcct)$select->where('BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($fAcct).'%'));
	        if($fName)$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));
	        if($fCcy)$select->where('CURR_CODE = '.$this->_db->quote(strtoupper($fCcy)));
	        if($fFav==1)$select->where('ISFAVORITE=1');
			
			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->benef_ccy = $fCcy;
			if($fFav==1)$this->view->favorit = true;
			else $this->view->favorit = false;
		}
		else $this->view->favorit = false;
		
	    $select->order($sortBy.' '.$sortDir);   
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}
	
	public function addbeneficiaryAction()
	{
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();		
		//Zend_Debug::dump($ccyList);die;
		
		if(!$this->_request->isPost() && $this->_getParam('isback'))
		{
			$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
			$content = $sessionNamespace->content;
			$this->fillParams($content['BENEFICIARY_ACCOUNT'],$content['CURR_CODE'],$content['BENEFICIARY_ALIAS'],$content['BENEFICIARY_EMAIL']);
			$this->view->ACBENEF_NAME = $content['BENEFICIARY_NAME'];
			unset($_SESSION['beneficiaryAccountAddEdit']); 
		}
		
		if($this->_request->isPost() )
		{			
			$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
			$CURR_CODE = $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');
			
	//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//	
			$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT  , "ACCOUNT_NO");								
			$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS , "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL , "EMAIL");
			$ACBENEF_CCY   = $filter->filter($CURR_CODE  , "SELECTION");

			 $paramBene = array("TRANSFER_TYPE"     => 'PB',
								"FROM"      		=> 'B',
								"ACBENEF"     		=> $ACBENEF,  
								"ACBENEF_CCY"    	=> $ACBENEF_CCY,   
								"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,  
								"ACBENEF_EMAIL"    	=> $ACBENEF_EMAIL,  
								"_addBeneficiary"   => $this->view->hasPrivilege('BADA'),  
								"_beneLinkage"    	=> $privibenelinkage,  
							   );
												
			 $validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			 $validateACBENEF->setPrefixMessageAccount('', '');
			 $validateACBENEF->check($paramBene);
			
			 if (!$validateACBENEF->isError()){
				$content = array(
								'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
								'USER_ID_LOGIN' 			=> $this->_userIdLogin,
								'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
								'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
								'BENEFICIARY_NAME' 			=> $paramBene['ACBENEF_BANKNAME'],
								'CURR_CODE' 				=> $ACBENEF_CCY,
								'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
								'BENEFICIARY_CITIZENSHIP' 	=> null,
								'BENEFICIARY_ADDRESS'		=> null,
								'BENEFICIARY_TYPE' 			=> 1,
								'SWIFT_CODE' 				=> null,
								'CLR_CODE' 					=> null,
								'_beneLinkage'     			=> $privibenelinkage,
					);
				
					$content['paramAccount'] = $responseData;
					$content['payment'] = $payment;
					
					$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
					$sessionNamespace->mode = 'Add';
					$sessionNamespace->content = $content;
					$this->_redirect('/predefinedbeneficiary/biiacct/confirm');
				
			}
			else
			{			 	
				$this->view->error = true;
				$this->fillParams($ACBENEF,$ACBENEF_CCY,$ACBENEF_ALIAS,$ACBENEF_EMAIL);
				$docErr = 'Error: '.$validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;				
			}
		}
	}
	
	public function confirmAction()
	{
		
		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		$mode = $sessionNamespace->mode;
		
		$this->view->mode = $mode;
		$this->fillParams($content['BENEFICIARY_ACCOUNT'],$content['CURR_CODE'],$content['BENEFICIARY_ALIAS'],$content['BENEFICIARY_EMAIL']);
		$this->view->ACBENEF_NAME = $content['BENEFICIARY_NAME'];
		
		if($this->_request->isPost() )
		{	
			if($this->_getParam('submit')=='Back')
			{
				if($mode=='Add'){
					$this->_redirect('/predefinedbeneficiary/biiacct/addbeneficiary/isback/1');
				}elseif ($mode=='Edit'){
					$this->_redirect('/predefinedbeneficiary/biiacct/editbeneficiary/benef_id/'.$content['BENEFICIARY_ID'].'/isback/1');	
				}
			}
			else
			{
				try 
				{
					$Beneficiary = new Beneficiary();
					$this->_db->beginTransaction();
					if($mode=='Add'){
						$add = $Beneficiary->add($content);
						Application_Helper_General::writeLog('BADA','Add Beneficiary Account '.$content['BENEFICIARY_ACCOUNT']);
					}elseif ($mode=='Edit'){
						$update = $Beneficiary->update($content);
						Application_Helper_General::writeLog('BEDA','Edit Beneficiary Account '.$content['BENEFICIARY_ACCOUNT']);	
					}
					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e) 
				{
					$this->_db->rollBack();
					
				}
			}
		}
	}
	
	
	
	public function importbeneficiaryAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		if($this->_request->isPost())
		{
			$this->_destinationUploadDir = UPLOAD_PATH . '/Frontend/document/temp/'; 	
			//die($this->_destinationUploadDir);
			$adapter = new Zend_File_Transfer_Adapter_Http ();
			$adapter->setDestination ( $this->_destinationUploadDir );
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
			$extensionValidator->setMessage(
				'Error: Extension file must be *.csv'
			);
			
			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
							$this->getErrorRemark('13')
			);
			
			$adapter->setValidators ( array (			
				$extensionValidator, 
				$sizeValidator,
			));
			
			if ($adapter->isValid ()) 
			{
				$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				
				$adapter->addFilter ( 'Rename',$newFileName  );
					
				if ($adapter->receive ())
				{
					//PARSING CSV HERE
					$csvData = $this->parseCSV($newFileName);
					//after parse delete document temporary
					@unlink($newFileName);
					//end 
					
					$totalRecords = count($csvData);
					if($totalRecords)
					{
						unset($csvData[0]);
						$totalRecords = count($csvData);
					}
								
					if($totalRecords)
					{
						$rowNum = 0;
						$totalSucces = 0;
						$totalFailed = 0;

						foreach ( $csvData as $row ) 
						{
							$rowNum++;
							$benefAcct = trim($row[0]);
							$ccy = strtoupper(trim($row[1]));
							$alias = trim($row[2]);
							$email = trim($row[3]);
							
							$fullDesc = array(
								'BENEFICIARY_ACCOUNT' => $benefAcct,
								'CURR_CODE' => $ccy, 
								'BENEFICIARY_ALIAS' => $alias, 
								'BENEFICIARY_EMAIL' => $email
							);

							//validate field 
							$dataValidate =  array(					
								'benef_acct' => $benefAcct, 
								'ccy' => $ccy, 
								'alias' => $alias, 
								'email' => $email
							);
							
							//validate format from csv document
							$resultValidate = $this->validateField($dataValidate);
																											
							if($resultValidate===true)
							{													
								$totalSucces++;
								
								$benefData = array(
									'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
									'USER_ID_LOGIN' 			=> $this->_userIdLogin,
									'BENEFICIARY_ALIAS' 		=> $alias,
									'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
									'BENEFICIARY_NAME' 			=> '',
									'CURR_CODE' 				=> $ccy,
									'BENEFICIARY_EMAIL' 		=> $email,
									'BENEFICIARY_CITIZENSHIP' 	=> null,
									'BENEFICIARY_ADDRESS'		=> null,
									'BENEFICIARY_TYPE' 			=> 1,
									'SWIFT_CODE' 				=> null,
									'CLR_CODE' 					=> null
										);
				
								try 
								{
									$this->_db->beginTransaction ();
									//Zend_Debug::Dump($benefData);die;
									$Beneficiary = new Beneficiary();
									$add = $Beneficiary->add($content);
									$this->_db->commit ();
								}
								catch(Exception $e)
								{
									$this->_db->rollBack();
									$error_remark = 'Error: Database Process Failed';
									Application_Helper_GeneralLog::technicalLog($e);
								}
								
							}
							else // jika validasi salah
							{
								$errorArr[] = $resultValidate->getMessages();
							}					
						}
						
					}
					else
					{
					}	
				}
			} 
			else 
			{		
				$this->view->error = true;
				$errors = $this->displayError($adapter->getMessages());
				$this->view->report_msg = $errors;
			}
		}
	}
	
	public function editbeneficiaryAction()
	{		
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();
		
		$this->view->isEdited = true;
		
		if(!$this->_request->isPost() && $this->_getParam('isback'))
		{
			$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
			$content = $sessionNamespace->content;
			$this->fillParams($content['BENEFICIARY_ACCOUNT'],$content['CURR_CODE'],$content['BENEFICIARY_ALIAS'],$content['BENEFICIARY_EMAIL'],$content['BENEFICIARY_ID']);
			$this->view->ACBENEF_NAME = $content['BENEFICIARY_NAME'];
			unset($_SESSION['beneficiaryAccountAddEdit']); 
		}
		else if(!$this->_request->isPost() )
		{	
			$benef_id = $this->_getParam('benef_id');
			$benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY')) 
									 ->where("BENEFICIARY_ID=?", $benef_id)
				);		
				if($resultdata)
				{				
					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];																								
				} 
		
				$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
				$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);	
				$select->where("B.BENEFICIARY_ID = ?", (string) $benef_id);	
				
				$isEdited = $select->query()->fetchAll();
				
				if($isEdited)
					$this->view->isEdited = true;
				else
				{
					$this->view->isEdited = false;
					$this->view->error = true;
					$this->view->report_msg = 'Error: You have no right to edit beneficiary.';
				}
			}
			else
			{
			   $error_remark = 'Beneficiary ID does not exist.';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());	
			}
		}
		else
		{
			$ACBENEF_ID =  $this->_getParam('BENEFICIARY_ID');
			$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
			$CURR_CODE = $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');
			
	//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//	
			$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT  , "ACCOUNT_NO");								
			$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS , "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL , "EMAIL");
			$ACBENEF_CCY   = $filter->filter($CURR_CODE  , "SELECTION");

			$paramBene = array( "TRANSFER_TYPE"     => 'PB',
								"FROM"      		=> 'B',
								"ACBENEF_ID"     	=> $ACBENEF_ID,  
								"ACBENEF"     		=> $ACBENEF,  
								"ACBENEF_CCY"    	=> $ACBENEF_CCY,   
								"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,  
								"ACBENEF_EMAIL"   	=> $ACBENEF_EMAIL,  
								"_editBeneficiary"  => $this->view->hasPrivilege('BEDA'),  
								"_beneLinkage"    	=> $privibenelinkage,  
							  );
												
			$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			$validateACBENEF->check($paramBene);	
			if (!$validateACBENEF->isError())
			{
				$content = array(
								'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
								'USER_ID_LOGIN' 			=> $this->_userIdLogin,
								'BENEFICIARY_ID' 			=> $ACBENEF_ID,
								'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
								'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
								'BENEFICIARY_NAME' 			=> $paramBene['ACBENEF_BANKNAME'],
								'CURR_CODE' 				=> $ACBENEF_CCY,
								'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
								'BENEFICIARY_CITIZENSHIP' 	=> null,
								'BENEFICIARY_ADDRESS'		=> null,
								'BENEFICIARY_TYPE' 			=> 1,
								'SWIFT_CODE' 				=> null,
								'CLR_CODE' 					=> null,
								'_beneLinkage'     			=> $privibenelinkage,
					);
					//-----update benef--------------
					$content['paramAccount'] = $responseData;
					$content['payment'] = $payment;
					
					$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
					$sessionNamespace->mode = 'Edit';
					$sessionNamespace->content = $content;
					$this->_redirect('/predefinedbeneficiary/biiacct/confirm');
			}
			else
			{			 	
				$this->view->error = true;
				$this->fillParams($ACBENEF,$ACBENEF_CCY,$ACBENEF_ALIAS,$ACBENEF_EMAIL,$ACBENEF_ID);
				$docErr = 'Error: '.$validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;				
			}
		}
	}
	
	public function editemailAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		if(!$this->_request->isPost())
		{
			$benef_id = $this->_getParam('benef_id');
			$benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY')) 
									 ->where("BENEFICIARY_ID=?", $benef_id)
							);	
			  if($resultdata)
			  {
					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];		 		 
			   } 
			}
			else
			{
			   $error_remark = 'Beneficiary ID does not exist.';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());	
			}
		}
		if($this->_request->isPost())
		{
			$filters = array(  	'BENEFICIARY_ID' => array('StringTrim','StripTags'),
								'ACBENEF_EMAIL' => array('StringTrim','StripTags'));
								
			$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Error: Beneficiary ID cannot be left blank.',
																'Error: Wrong Format Beneficiary ID.')
														),
								'ACBENEF_EMAIL' => array(	//new SGO_Validate_EmailAddress(),
															'allowEmpty' => true,
															//'messages' => array(
															//	'Error: Wrong Format Email Address. Please correct it.')
														));
		
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{														
				$error = false;
				
				if (trim($zf_filter_input->ACBENEF_EMAIL) != "") 
				{
					$arr_email = explode(";",$zf_filter_input->ACBENEF_EMAIL);										
					foreach ($arr_email as $value) 
					{
						if (Zend_Validate::is(trim($value), 'EmailAddress') == false)  
						{	
							$error = true;
							break;
						}
					}
				}
				
				if (!$error)
				{
					try 
					{
						//-----edit email--------------
						$this->_db->beginTransaction();
						
						$Beneficiary = new Beneficiary();
						$Beneficiary->editEmail($zf_filter_input->BENEFICIARY_ID,$zf_filter_input->ACBENEF_EMAIL);
						
						$beneAcct = $this->_db->fetchOne('SELECT BENEFICIARY_ACCOUNT FROM M_BENEFICIARY WHERE BENEFICIARY_ID='.$zf_filter_input->BENEFICIARY_ID);
						
						Application_Helper_General::writeLog('BEDA','Edit Email Beneficiary Account '.$beneAcct);
						
						$this->_db->commit();
						
						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						//$this->view->success = true;
						//$msg = $this->getErrorRemark('00','Edit Email');
						//$this->view->report_msg = $msg;
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');
					}
					catch(Exception $e) 
					{
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				}
				else
				{
					$this->view->error = true;
					
					$resultdata = $this->_db->fetchRow(
									$this->_db->select()
										 ->from(array('M_BENEFICIARY')) 
										 ->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
								);	
									
					if($resultdata)
					{
						$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];	 		 
					}
					
					$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
					$docErr = 'Error: Invalid Format - Email Address.';
					$this->view->report_msg = $docErr;
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
				}
			}
			else
			{
				$this->view->error = true;
				$resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY')) 
									 ->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
							);	
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];	 		 
				} 
				$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}
		}
	}
	
	public function deleteAction()
	{		
		if(!$this->_request->isPost())
		{
			$benef_id = $this->_getParam('benef_id');
			$benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY')) 
									 ->where("BENEFICIARY_ID=?", $benef_id)
							);	
			  if($resultdata)
			  {
					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];		 		 
					$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];		 		 
			   } 
			}
			else
			{
			   $error_remark = 'Error: Beneficiary ID cannot be left blank.';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			}
		}
		else
		{
			$filters = array(  	'BENEFICIARY_ID' => array('StringTrim','StripTags'),
								'STATUS' => array('StringTrim','StripTags'));
								
			$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Error: Beneficiary ID cannot be left blank.',
																'Error: Wrong Format Beneficiary ID.')
														),
								'STATUS' 		=> array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Error: Status cannot be left blank.',
																'Error: Wrong Format Status.')
														));
		
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{								
				try 
				{
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					if($zf_filter_input->STATUS)
						$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
					else
						$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
					$this->_db->commit();
					
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}
			}
			else
			{
				$this->view->error = true;
				$resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY')) 
									 ->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
							);	
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];		 		 
					$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];		 
				} 
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}
		}
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	}
	
	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID'); 
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS'); 
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
	}
	
	private function fillParams($ACBENEF,$ACBENEF_CCY,$ACBENEF_ALIAS,$ACBENEF_EMAIL,$BENEFICIARY_ID=null)
	{
		if($BENEFICIARY_ID) $this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
	}
	
	private function validateField($data){
											
		$filters = array(
			'benef_acct' 	=> array('StripTags','StringTrim'), 			
			'ccy' 			=> array('StripTags','StringTrim'), 					
			'alias'			=> array('StripTags','StringTrim'),		
			'email' 		=> array('StripTags','StringTrim')
		);
		
		$validators = array(
			'benef_acct' => array(
				'NotEmpty',
				'Digits',
				new Zend_Validate_StringLength(array('max'=>10,'min'=>10)),
				'messages' => array(
					'Beneficiary Account cannot be left blank. Please correct it.',
					'Beneficiary Account must be numbers. Please correct it.',
					'Beneficiary Account length must be 10 digits. Please correct it.',
					)											   					   
			),
			
			'alias' => array(
				'NotEmpty',
				new Zend_Validate_StringLength(array('max'=>35)),
				'messages' => array(
					'Alias Name cannot be left blank. Please correct it.',
					'Alias Name length should be less than 35 characters. Please correct it.'
					)							   					   
			),
			'ccy' => 	array(	
				'NotEmpty',
				//new Zend_Validate_Alnum(),
				//new Zend_Validate_StringLength(array('min'=>3,'max'=>3)),
				array('InArray', array('haystack' => $this->_listCCYValidate)),
				'messages' => array	(
					'Currency cannot be left blank. Please correct it.',
					//'04',
					//'04',
					'Currency is not on the database.'
				)
			),
			
			'email' => array(	
				new SGO_Validate_EmailAddress(),
				'allowEmpty' => true,
				'messages' => array(
					'Wrong Format Email Address. Please correct it.'
				),										   					   
			),
		);
		
		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$data);
		if($zf_filter_input->isValid()) {
			return true;
		}else{
			return $zf_filter_input;
		}
	}
	
	public function setfavoriteAction()
	{
		$fav = $this->_request->getParam('isfavorite');
		$flag = false;
		foreach($fav as $val)
		{
			if($val == 1)
			{
				$flag = true;
				break;
			}			
		}
		
		// if($flag)
		// {
			foreach($fav as $key=>$value)
			{
				$Beneficiary = new Beneficiary();
				$Beneficiary->setFavorite($key,$value);
			}
			$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
			$this->_redirect('/notification/success');
		// }
		// else
		// {
			// $error_remark = 'Error: Please check selection.';
		    // $this->_helper->getHelper('FlashMessenger')->addMessage('F');
		    // $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
		    // $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
		// }
		
	}
	
	public function suggestdeleteAction()
	{
		$benef_id = $this->_getParam('benef_id');
		$benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
		
		if($benef_id)
		{
			$Beneficiary = new Beneficiary();
			$Beneficiary->suggestDelete($benef_id);
		}
		$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
		$this->_redirect('/notification/success');
	}
}
