<?php
Class group_Model_Group extends Application_Main
{
	
    protected $_moduleDB  = 'BGR';

    // constructor
    public function initModel()
    { 
      	
    }
	
    public function getTemplate()
    {
	   $select = $this->_db->select()
						   ->from(array('M_BTEMPLATE'),
							      array('BTEMPLATE_ID', 'BTEMPLATE_DESC','PARENT_BTEMPLATE_ID'))
                    	   ->query()->fetchAll();
       return $select;             	   
    }

    public function getPrivilege()
    {
       $select = $this->_db->select()
							  ->from(array('M_BPRIVILEGE'),
							        array('BPRIVI_ID', 'BPRIVI_DESC','BTEMPLATE_ID'))
							  ->where("BPRIVI_STATUS = ?",$this->_masterStatus['code']['active'])
                    		  ->query()->fetchAll();
       return $select;  
    }
    
    public function getTemplateId($groupArr)
    {
       $select = $this->_db->select()
						    ->from(array('BPG' => 'M_BPRIVILEGE'),
							       array('BTEMPLATE_ID'))
							->join(array('BT' => 'M_BTEMPLATE'),'BPG.BTEMPLATE_ID = BT.BTEMPLATE_ID',array('PARENT_BTEMPLATE_ID'))
							->where("BPRIVI_ID IN({$groupArr})")
							->query()->fetchAll();
	   return $select;  
    }
    
    public function getPriviGroup($groupCode)
    {
       $select = $this->_db->select()
					       ->from(array('M_BPRIVI_GROUP'),
					              array('BPRIVI_ID', 'BG_ID'))
						   ->where("BGROUP_ID = ?",$groupCode)
                    	   ->query()->fetchAll();
       return $select; 
    }
    
    public function getGroup($groupCode)
    {
       $select = $this->_db->select()
					       ->from(array('M_BGROUP'),
					             array('BGROUP_DESC','BGROUP_STATUS'))
						   ->where("BGROUP_ID = ?",$groupCode)
                    	   ->query()->fetch();
       return $select; 
    }
    
    
    public function insertTempGroup($groupCode,$groupName,$changesId,$status)
    {
      $this->_db->insert('TEMP_BGROUP', array(
	 										  'CHANGES_ID'    => $changesId,
											  'BGROUP_ID'     => $groupCode,
											  'BGROUP_DESC'   => $groupName,
											  'BGROUP_STATUS' => $status,
											  ));
    }
    
    public function insertTempPriviGroup($groupCode,$privi_id,$changesId)
    {
       $this->_db->insert('TEMP_BPRIVI_GROUP', array(
                                                     'CHANGES_ID' => $changesId,
													 'BPRIVI_ID'  => $privi_id,
													 'BGROUP_ID'  => $groupCode,
									                ));
    }
    
}




