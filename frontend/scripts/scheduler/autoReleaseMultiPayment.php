<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	
	$truecounter = 0;
	$falsecounter = 0;
	$cronResult = "";

	// INNER JOIN KE T_TRANSACTION
	// PS STATUS  7
	// EFTDATE (today)
	// PS CATEGORY (MULTI PAYMENT)
	$data = $db->select	()
				->FROM	(array('P' => 'T_PSLIP'),array('PS_NUMBER','CUST_ID'))
//				->JOIN	(array('T' => 'T_TRANSACTION'),'T.PS_NUMBER = P.PS_NUMBER',array('*'))
				->WHERE('P.PS_STATUS = 7')						
				->WHERE('DATE(P.PS_EFDATE) = DATE(now())')
				->WHERE("P.PS_CATEGORY = 'MULTI PAYMENT'")
				->QUERY()->FETCHALL();
				
	if($data)
	{
		foreach($data as $row)
		{
			$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], 'System');
			$resultRelease = $Payment->releasePayment();
					
			if ($resultRelease['status'] == '00')
			{
				$truecounter++;
			}
			else
			{
				$falsecounter++;
			}
		}
		
		if($truecounter>0)
		{
			$cronResult .= " ".$truecounter." payment(s) success";
		}
		if($falsecounter>0)
		{
			$cronResult .= " ".$falsecounter." payment(s) failed";
		}		
	}
	else
	{
		$cronResult .= "No data released";
	}

	$filename = basename(__FILE__);
	Application_Helper_General::cronLog($filename, $cronResult, 1);
?>