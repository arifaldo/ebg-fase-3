<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once "Service/Account.php";
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/Settings.php';

class user_EditController extends user_Model_User
{
  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');
    $cust_id = strtoupper($this->_getParam('cust_id'));
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;  


    $AESMYSQL = new Crypt_AESMYSQL();
    $PS_NUMBER      = urldecode($this->_getParam('user_id'));
  
    $this->view->encruser_id = $this->_getParam('user_id');
    $user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    $user_id = strtoupper($user_id); 

    if($this->_request->isPost())
    {
      $user_id = $this->_getParam('userid');
    }

    //$user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
    $error_remark = null;
    $this->view->user_msg  = array();

    //get user data
    $select = $this->_db->select()
                           ->from('M_USER')
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
    $resultdata = $this->_db->fetchRow($select);

     $setting = new Settings();
    $system_type = $setting->getSetting('system_type');
    $getprivilege = $this->getprivilege($system_type);

    if($cust_id)
    {

      $selectccy = $this->_db->select()
      ->from('M_MINAMT_CCY',array('CCY_ID','CCY_NUM'))
      ->where('CCY_ID IN ("IDR","USD")');


      $ccylist = $this->_db->fetchAll($selectccy);
      $this->view->ccylist = $ccylist;

      $result = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_STATUS','CUST_CIF','CUST_NAME','CUST_LIMIT_IDR','CUST_LIMIT_USD'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->query()->fetch();

      if(!$result['CUST_ID']) $cust_id = null;
      else{
        $companyLim = array();
        $companyLim['IDR'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_IDR']);
        $companyLim['USD'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_USD']);
        $this->view->limitidr = $result['CUST_LIMIT_IDR'];
        $this->view->limitusd = $result['CUST_LIMIT_USD'];
      }
      
      $cust_name = $result['CUST_NAME'];
    }
  
    if(!$cust_id)
    {
      $error_remark = 'Cust ID is not found';
      //insert log
      Application_Helper_General::writeLog('UPUS','Edit User');
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }
  //var_dump($user_id);die;
    if($user_id)
    {
  $edit = $this->_getParam('edit');
  //print_r($edit);die;
  if($edit){
  $this->view->rmvdisabled = true;
  }
      $select = $this->_db->select()
                           ->from('M_USER',array('USER_ID'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
                           //->where('USER_STATUS=1');
      $resultUser = $this->_db->fetchOne($select);

      if($resultUser)
      {
        $select = $this->_db->select()
                             ->from('TEMP_USER',array('TEMP_ID'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
        $resultUserTemp = $this->_db->fetchOne($select);

        if($resultUserTemp)
        {
          $user_id = null;
          $error_remark = 'No changes allowed for this record while awaiting approval for previous change';
        }
      }
      else{ $user_id = null; }
    }

    if(!$user_id)
    {

      if(!$error_remark)$error_remark = 'User ID is not found';
      //insert log
      Application_Helper_General::writeLog('UPUS','Edit User');
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }
  
  $page    = $this->_getParam('clearfilter');

  $setting  = new Settings();
  $tokenType = $setting->getSetting('tokentype','');
  $this->view->tokenType = $tokenType;

  $hardTokenTypeCode = $this->_tokenType['code']['hardtoken'];
  $this->view->hardTokenTypeCode = $hardTokenTypeCode;

  //---------------------------------------------

    $app = Zend_Registry::get('config');
    $appBankname = $app['app']['bankname'];
    $app = $app['app']['bankcode'];

    $this->view->appBankname = $appBankname;
    
    $core = array();

    if(substr($result['CUST_CIF'],0,3) != 'CIF'){
      //die('here')
    $svcAccount = new Service_Account($result['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
    $result   = $svcAccount->cifaccountInquiry($core);
    }else{
      $result = array();
    }
    if($core->responseCode == '0000' || $result['ResponseCode']=='0000' ){
          $arrResult = array();
          $err = array();
          
          if(count($core->accountList) > 1){
            //array data lebih dari 1
            foreach ($core->accountList as $key => $val){
              if($val->status == '0'){  
                $arrResult = array_merge($arrResult, array($val->accountNo => $val));
                array_push($err, $val->accountNo);
              }
            }
          }
          else{
            //array kurang dari 2
            foreach ($core as $data){
              if($data->status == '0'){ 
                $arrResult = array_merge($arrResult, array($data->accountNo => $data));
                array_push($err, $data->accountNo);
              }
            }
          }

        $final = $arrResult;

        asort($final);
        
        if(count($final) < 1){
          $this->view->data1 = '0';
        }
        else{
          $this->view->data = $arrResult;
        }
        $dataProPlan = array('xxxxx');
          foreach ($final as $dataProductPlan){
              if($dataProductPlan->status == '0'){  
                if (isset($dataProductPlan->productType))
                {
                $select_product_type  = $this->_db->select()
                      ->from(array('M_PRODUCT_TYPE'),array('PRODUCT_NAME'))
                      ->where("PRODUCT_CODE = ?", $dataProductPlan->productType);
                $userData_product_type = $this->_db->fetchAll($select_product_type);
                $dataProductPlan->planName = $userData_product_type[0]['PRODUCT_NAME'];
                }else{
                  $dataProductPlan = $dataProductPlan; 
                }             
             $dataUser[] = $dataProductPlan;
              }
          }

     }
    $this->view->data = $dataUser;

    //data makerlimit
    $select = $this->_db->select()
              ->from(array('M_MAKERLIMIT'),array('*'))
              ->where("USER_LOGIN = ?", $user_id)
              ->where('CUST_ID = '.$this->_db->quote($this->_custIdLogin).' OR CUST_ID = '.$this->_db->quote(BANK).'');
              
    $makerLimitData = $this->_db->fetchAll($select);

    foreach ($makerLimitData as $key => $value) {
      $newMakerLimitData[$value['ACCT_NO']] = $value['MAXLIMIT'];
    }

    $this->view->makerLimitData = $newMakerLimitData;

    //data dailylimit
    $select = $this->_db->select()
              ->from(array('M_DAILYLIMIT'),array('*'))
              ->where("USER_LOGIN = ?", $user_id)
              ->where('CUST_ID = '.$this->_db->quote($this->_custIdLogin).' OR CUST_ID = '.$this->_db->quote(BANK).'');
              
    $dailyLimitData = $this->_db->fetchAll($select);

    foreach ($dailyLimitData as $key => $value) {
      $newDailyLimitData[$value['CCY_ID']] = $value['DAILYLIMIT'];
    }

    $this->view->dailyLimitData = $newDailyLimitData;

  //echo $page; 
  //print_r($page);die;
    if($this->_request->isPost())
    {
      $exclude_fgroup_id = '(UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id);
      $exclude_fgroup_id .= ' OR UPPER(CUST_ID)='.$this->_db->quote('BANK');
      $exclude_fgroup_id .= ') AND UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active']));
  

      //get privilege id from html
       $priviId    = array();
       $filterPrivilege    = array();
       $validatorPrivilege = array();
    $privcheck = false;
       foreach($getprivilege as $privi)
       {
           $priviParam = $this->_getParam($privi['FPRIVI_ID']);

    
           if($priviParam == 'yes')
           {
         $privcheck = true;
             $priviId[] = strip_tags( trim($privi['FPRIVI_ID']) );
             $filterPrivilege[ $privi['FPRIVI_ID'] ]    = array('StripTags','StringTrim');
             $validatorPrivilege[ $privi['FPRIVI_ID'] ] = array('NotEmpty',
                                        array('StringLength',array('min'=>1,'max'=>3)),
                                  'messages' => array($this->language->_('invalid'),
                                              $this->language->_('invalid'))
                                        );
           }
        }


      $filters = array('user_id'        => array('StripTags','StringTrim','StringToUpper'),
                        'user_email'     => array('StripTags','StringTrim'),
                        'user_fullname'  => array('StripTags','StringTrim'),
                        'user_phone'     => array('StripTags','StringTrim'),
                        'user_ext'             => array('StripTags','StringTrim'),
						'user_debitnumber'             => array('StripTags','StringTrim'),
                        'token_id'             => array('StripTags','StringTrim'),
                        'user_isemail'         => array('StripTags','StringTrim'),
                        'cust_id'        => array('StripTags','StringTrim','StringToUpper'),
                      );


      //merge filter data user with privilege
      $filters = array_merge($filters,$filterPrivilege);
      $expid = "USER_ID != ".$this->_db->quote($user_id);

      $validators =  array('user_id'        => array(),
                           'cust_id'        => array(),

                           'user_email'     => array(//new Application_Validate_EmailAddress(),
                               array('StringLength',array('max'=>128)),
                               // array('Db_NoRecordExists',array('table'=>'M_USER','field'=>'USER_EMAIL','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))),
                               array('Db_NoRecordExists',array('table'=>'TEMP_USER','field'=>'USER_EMAIL','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))),
                               'NotEmpty',
                               'messages' => array(
                                          //'Invalid email format',
                                          $this->language->_('Email lenght cannot be more than').' 128',
                                          // $this->language->_('Email is already used in this company. Please use another'),
                                          $this->language->_('Email is already suggested for this company. Please use another'),
                                          $this->language->_('Can not be empty'),
                                )
                            ),

                           'user_fullname'      => array('NotEmpty',
                               array('StringLength',array('min'=>1,'max'=>50)),
                             'messages' => array($this->language->_('Can not be empty'),
                                                 $this->language->_('invalid'))
                              ),

               'user_phone'     => array( //array('StringLength',array('min'=>1,'max'=>20)),
                               'Digits',
                               'allowEmpty' => true,
                             'messages' => array(
                                                   $this->language->_('Invalid phone number format'),
                                                   )
                            ),

                         'user_ext'         => array('Digits',
                               array('StringLength',array('min'=>1,'max'=>20)),
                               'allowEmpty' => true,
                             'messages' => array(
                                                  $this->language->_('Must be numeric values'),
                                                  )
                            ),
							'user_debitnumber'     => array(//new Application_Validate_EmailAddress(),
                               array('StringLength',array('max'=>128)),
                                //array('Db_NoRecordExists',array('table'=>'M_USER','field'=>'USER_DEBITNUMBER','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))),
                               //array('Db_NoRecordExists',array('table'=>'TEMP_USER','field'=>'USER_DEBITNUMBER','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))),
                               //'NotEmpty',
							   'allowEmpty' => true,
                               'messages' => array(
                                          //'Invalid email format',
                                          $this->language->_('Email lenght cannot be more than').' 128',
                                           //$this->language->_('Debit Card Number is already used in this company. Please use another'),
                                          //$this->language->_('Debit Card Number is already suggested for this company. Please use another'),
                                          //$this->language->_('Can not be empty'),
                                )
                            ), 
              'token_id'        => array(
                'allowEmpty' => true,
                'Alnum',
                array('StringLength',array('min'=>1,'max'=>12)),
                array('Db_NoRecordExists',array('table'=>'M_USER','field'=>'TOKEN_ID','exclude'=>$expid)),
                array('Db_NoRecordExists',array('table'=>'TEMP_USER','field'=>'TOKEN_ID')),
                new Zend_Validate_Callback(
                  array(
                    'callback'  => array('SGO_Validate_Callback', 'checkTokenSerial'),
                    'options' => array($cust_id,$user_id)
                  )
                ),
                'messages' => array(
                  $this->language->_('Invalid Token Id Format'),
                  $this->language->_('Invalid Token Id Format'),
                  $this->language->_('Token Id already in use. Please use another'),
                  $this->language->_('Token Id already suggested. Please use another'),
                  $this->language->_('Token Id is invalid')
                )
              ),
               'user_isemail'   => array(array('StringLength',array('min'=>1,'max'=>25)),
                  'allowEmpty' => true,
                'messages' => array($this->language->_('invalid'))
              ),
            );


    //merge validator data user with privilege
      $validators = array_merge($validators,$validatorPrivilege);



      $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
  // $checkDuplicateEmail = true;
     //validasi multiple email
      if($zf_filter_input->user_email)
      {
        $validate = new validate;
        $cek_email = $validate->isValidEmail($zf_filter_input->user_email);

        $select = $this->_db->select()
                           ->from('M_USER',array('USER_EMAIL','USER_ID'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('USER_EMAIL = ?', $this->_getParam('user_email'))
                           ->where('USER_STATUS != 3');

        $data = $this->_db->fetchRow($select);
        
        $checkDuplicateEmail = false;
        if (empty($data)) {
      
          $checkDuplicateEmail = true;
        }else{
          if($data['USER_ID'] == $user_id){
              $checkDuplicateEmail = true;
          }
        }
      }

      $errorLim = false;
      foreach($ccylist as $key => $value){

            $limittag = 'daily'.(string)$value['CCY_ID'];
            $dailylimLeft = $companyLim[$value['CCY_ID']] ;
            if(empty($dailylimLeft)){
              $dailylimLeft = 0;
            }
      //echo $limittag;echo ' ';
            $dailylimit = $this->_getParam($limittag);
      $dailylimit = preg_replace("/([^0-9\\.])/i", "", $dailylimit);
          if($dailylimit != '0.00' && $dailylimit != ''){
               if($dailylimit > (int)$dailylimLeft){

                  $errorLim = true;
                  $error_remark = "Maximum Amount of Daily Limit in ".$value['CCY_ID']." is ".Application_Helper_General::displayMoney($dailylimLeft);
              }
          }
      }
	  
	  if(!empty($this->_getParam('user_debitnumber'))){
	  $debitarr = explode(';',$this->_getParam('user_debitnumber'));
	  $this->view->user_debitnumber = $this->_getParam('user_debitnumber');
	  foreach($debitarr as $dval){
		  $select = $this->_db->select()
			->from('TEMP_USER_DEBIT',array('USER_DEBITNUMBER','CUST_ID','USER_ID'))
			->where('USER_DEBITNUMBER = ?',$dval);
			
			$tempcekdebit = $this->_db->fetchRow($select);
			
			 $select = $this->_db->select()
			->from('M_USER_DEBIT',array('USER_DEBITNUMBER','CUST_ID','USER_ID'))
			->where('USER_DEBITNUMBER = ?',$dval);
			//->where('CUST_ID = ?',$this->_userIdLogin)
			//->where('USER_ID = ?',$this->_custIdLogin);
			
			$cekdebit = $this->_db->fetchRow($select);
			
			$select = $this->_db->select()
			->from('T_DEBITCARD',array('DEBIT_NUMBER'))
			->where('DEBIT_NUMBER = ?',$dval)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->where('DEBIT_STATUS = ?','1');
			//echo $select;
			$cekdebitlist = $this->_db->fetchOne($select);
			
			//var_dump($cekdebitlist);die;
			
			if($cekdebitlist){
				//var_dump($cekdebit);
			//var_dump($tempcekdebit);
				if(!empty($cekdebit) || !empty($tempcekdebit)){
					//var_dump($cekdebit);
					//var_dump($user_id);
					//var_dump($cust_id);die;
					if($cekdebit['USER_ID'] != $user_id && $cekdebit['CUST_ID'] != $cust_id){
						$errorLim = true;
						$error_remark = "Duplicate Debet Card Number ".$dval;
						break;
					}
				}
			}else{
				$errorLim = true;
					$error_remark = "Debet Card Number ".$dval." not found";
					break;
			}
	  }
	 }
    
    if(!$privcheck){
        $this->view->errcheckbox = $this->language->_('Please Select at least 1 Privilege');
      }
  //var_dump($privcheck);die;
      if($zf_filter_input->isValid() && $cek_email && $privcheck && $checkDuplicateEmail && !$errorLim)
      {
        $info = 'User ID = '.$user_id.', User Name = '.$zf_filter_input->user_fullname;

        $user_data = array(
            'CUST_ID'         => null,
            'USER_ID'         => null,
            'USER_FULLNAME'   => null,
            'USER_PASSWORD'   => null,
            'USER_EMAIL'      => null,
            'USER_PHONE'      => null,
            'USER_STATUS'     => null,
            'USER_EXT'        => null,
            'TOKEN_ID'        => null,
            'USER_ISEMAIL'    => null,
    );

      foreach($validators as $key=>$value)
      {
        if($zf_filter_input->$key)$user_data[strtoupper($key)] = $zf_filter_input->$key;
      }

      //jika tidak diisi maka bernilai = 0

        if(is_null($user_data['USER_ISEMAIL']) || $user_data['USER_ISEMAIL'] == '' ||  $user_data['USER_ISEMAIL'] == 0)
      {
         $user_data['USER_ISEMAIL'] = 0;
      }

      if (in_array("BAPA", $priviId))
      {
        $googlecode = $resultdata['GOOGLE_CODE'];
      }elseif (in_array("PAPV", $priviId)) {
        $googlecode = $resultdata['GOOGLE_CODE'];
      }elseif (in_array("PRLP", $priviId)) {
        $googlecode = $resultdata['GOOGLE_CODE'];
      }
      else
      {
        $googlecode = NULL;
      }

      //$user_data['USER_STATUS'] = 1;
      $user_data['USER_STATUS']      = $resultdata['USER_STATUS'];
      $user_data['USER_CREATED']     = $resultdata['USER_CREATED'];
      $user_data['USER_CREATEDBY']   = $resultdata['USER_CREATEDBY'];
      $user_data['USER_UPDATED']     = $resultdata['USER_UPDATED'];
      $user_data['USER_UPDATEDBY']   = $resultdata['USER_UPDATEDBY'];
      
      // $user_data['GOOGLE_CODE']      = $resultdata['GOOGLE_CODE'];
      $user_data['GOOGLE_CODE']      = $googlecode;
      $user_data['USER_HOME_CONTENT']      = $resultdata['USER_HOME_CONTENT'];
      $user_data['USER_THEME']      = $resultdata['USER_THEME'];
      $user_data['USER_INDEX']      = $resultdata['USER_INDEX'];
      $user_data['USER_CODE']      = $resultdata['USER_CODE'];
      $user_data['USER_IMAGE']      = $resultdata['USER_IMAGE'];
      $user_data['USER_DATEPASS']      = $resultdata['USER_DATEPASS'];
      

      $user_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
      $user_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
      $user_data['USER_ISLOCKED']   = $resultdata['USER_ISLOCKED'];
      $user_data['USER_ID']  = $user_id;
	  unset($user_data['USER_DEBITNUMBER']);
      try
      {
        $this->_db->beginTransaction();

        $change_id = $this->suggestionWaitingApproval('User List',$info,strtoupper($this->_changeType['code']['edit']),null,'M_USER','TEMP_USER',$user_id,$user_data['USER_FULLNAME'],$cust_id);

        $count_privi = count($priviId);
        if($count_privi > 0)  $this->insertTempPrivilege($change_id,$priviId,$user_data);

        $this->insertTempUser($change_id,$user_data);

        //-----------------------maker limit---------------------------
        $active = json_decode(json_encode($dataUser), true);
        $b=0;
        $final1 = array();          
        foreach ($active as $arr => $value){
          $final1[$b++] = array('accountNo'=>$value['accountNo'],
                                'accountName'=>$this->_request->getParam('accountName'.$value['accountNo']),
                                'ccy'=>$this->_request->getParam('ccy'.$value['accountNo']),
                                'productType'=>$this->_request->getParam('productType'.$value['accountNo']),
                                'productName'=>$this->_request->getParam('productName'.$value['accountNo']),
                                'acct_desc'=>$this->_request->getParam('acct_desc'.$value['accountNo'])
                              ); 
        }
		
		
		if(!empty($debitarr)){
			  $insetdebit = array();
			  foreach($debitarr as $val){
				  $insetdebit['CHANGES_ID'] = $change_id;
				  $insetdebit['USER_ID'] = $user_id;
				  $insetdebit['CUST_ID'] = $this->_custIdLogin;
				  $insetdebit['USER_DEBITNUMBER'] = $val;
				  $insert = $this->_db->insert('TEMP_USER_DEBIT',$insetdebit);
			  }
		  }
		

        // foreach ($final1 as $key => $value) {
        //     $accountNo = $value['accountNo'];
        //     $accountName = $value['accountName'];
        //     $email = '';
        //     $ccy = $value['ccy'];
        //     $productType = $value['productType'];
        //     $acct_desc = $value['acct_desc'];
            

        //     $acct_source = 3;     //1 = prk scm 
        //     $info = 'Account No = '.$accountNo;
        //     $acct_data['ACCT_STATUS']      = 1;     //1 = unsuspend
        //     $acct_data['ACCT_SUGGESTED']   = new Zend_Db_Expr('GETDATE()');
        //     $acct_data['ACCT_SUGGESTEDBY'] = $this->_userIdLogin;
        //     //data from core
        //     $acct_data['ACCT_NAME']   = $accountName;
        //     $acct_data['ACCT_ALIAS_NAME'] = $accountName;
        //     $acct_data['ACCT_NO'] = $accountNo;
        //     $acct_data['CUST_ID'] = $this->_custIdLogin;
        //     $acct_data['CCY_ID'] = $ccy;
        //     $acct_data['ACCT_DESC'] = $acct_desc;
        //     $acct_data['ACCT_TYPE'] = $productType;
        //     $acct_data['ACCT_EMAIL'] = $email;
        //     $acct_data['ACCT_SOURCE'] = $acct_source;
        //     try{
        //       $acct_data['CHANGES_ID'] = $change_id;
        //       $insert = $this->_db->insert('TEMP_CUSTOMER_ACCT',$acct_data);
        //       $limittag = 'limit'.(string)$accountNo;
        //       $maxlimit = $this->_getParam($limittag);
        //       if($maxlimit != '0.00' && $maxlimit != ''){
        //             $data = array('CHANGES_ID' => $change_id,
        //                     'USER_LOGIN' => $user_id,
        //                     'ACCT_NO' => (string)$accountNo,
        //                     'CUST_ID' => $this->_custIdLogin,
        //                     'MAXLIMIT' => Application_Helper_General::convertDisplayMoney($maxlimit),
        //                     'SUGGESTED' => new Zend_Db_Expr('now()'), 
        //                     'SUGGESTEDBY' => $this->_userIdLogin, 
        //                     'MAKERLIMIT_STATUS' => 1
        //                   );
        //             $this->_db->insert('TEMP_MAKERLIMIT',$data);
        //       }
            
        //             }catch(Exception $e)
        //       {
        //   //  echo '<pre>';
        //     //    var_dump($e);die;
        //       }
        // }

        //--------------------daily limit-----------------------
        // try{
        //     foreach($ccylist as $key => $value){
        //       $dailydata = array();
        //       $dailydata['CHANGES_ID'] = $change_id;
        //       $dailydata['USER_LOGIN'] = $user_id;
        //       $dailydata['CCY_ID'] = $value['CCY_ID'];
        //       $dailydata['CUST_ID'] = $this->_custIdLogin;
              
        //       $limittag = 'daily'.(string)$value['CCY_ID'];
        //             $dailylimit = $this->_getParam($limittag);
        //       if($dailylimit != '0.00' && $dailylimit != ''){
        //           $dailydata['DAILYLIMIT'] = Application_Helper_General::convertDisplayMoney($dailylimit);
        //           $dailydata['DAILYLIMIT_STATUS'] = '1';
                  
        //           $dailydata['SUGGESTED'] = new Zend_Db_Expr('now()');
        //           $dailydata['SUGGESTEDBY'] = $this->_userIdLogin;
        //           $this->_db->insert('TEMP_DAILYLIMIT',$dailydata);
        //       }
        //     }
        // }catch(Exception $e)
        // {
        //   // print_r($e);die;
        //   // $this->_db->rollBack();
        //   // SGO_Helper_GeneralLog::technicalLog($e);
        // }

        //log CRUD
        Application_Helper_General::writeLog('UPUS','User has been Updated (edit), User ID : '.$user_id. ' User Name : '.$user_data['USER_FULLNAME'].' Change id : '.$change_id);

        $this->_db->commit();


        //$this->setbackURL('/customer/view/index/cust_id/'.$cust_id);

        $this->setbackURL($this->view->backURL);
        $this->_redirect('/notification/submited/index');
      }
      catch(Exception $e)
      {
        
        $this->_db->rollBack();
        //Zend_Debug::dump($e);die;
        $error_remark = $this->language->_('An Error Occured. Please Try Again');
      }


      if(isset($error_remark)){ $msg = $error_remark; $class = 'F'; }
      // else
      // {
      //   $class = 'S';
      //   $msg = 'User ID ['.$zf_filter_input->user_id.']';
      // }
      Application_Helper_General::writeLog('UPUS','Edit User');
      $this->_helper->getHelper('FlashMessenger')->addMessage($class);
      $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
      $this->_redirect($this->view->backURL);
    }
    else
    {
      $this->view->error = 1;
      $this->view->user_fullname  = ($zf_filter_input->isValid('user_fullname'))? $zf_filter_input->user_fullname : $this->_getParam('user_fullname');
      $this->view->user_email = ($zf_filter_input->isValid('user_email'))? $zf_filter_input->user_email : $this->_getParam('user_email');
      $this->view->user_phone = ($zf_filter_input->isValid('user_phone'))? $zf_filter_input->user_phone : $this->_getParam('user_phone');
      $this->view->user_ext   = ($zf_filter_input->isValid('user_ext'))? $zf_filter_input->user_ext : $this->_getParam('user_ext');
      $this->view->token_id        = ($zf_filter_input->isValid('token_id'))? $zf_filter_input->token_id : $this->_getParam('token_id');

      $this->view->user_isemail        = ($zf_filter_input->isValid('user_isemail'))? $zf_filter_input->user_isemail : $this->_getParam('user_isemail');
	  $this->view->user_debitnumber        = ($zf_filter_input->isValid('user_debitnumber'))? $zf_filter_input->user_debitnumber : $this->_getParam('user_debitnumber');

      $this->view->priviView = $priviId;

       // $this->view->user_group = ($zf_filter_input->isValid('fgroup_id'))? $zf_filter_input->fgroup_id : $this->_getParam('fgroup_id');
        //$this->view->token_serialno = ($zf_filter_input->isValid('token_serialno'))? $zf_filter_input->token_serialno : $this->_getParam('token_serialno');
      $error = $zf_filter_input->getMessages();
    /*if(count($error))$error_remark = $this->displayErrorRemark($error);
      $this->view->user_msg = $this->displayError($error);*/

        //format error utk ditampilkan di view html
      $errorArray = null;
      foreach($error as $keyRoot => $rowError)
      {
         foreach($rowError as $errorString)
         {
            $errorArray[$keyRoot] = $errorString;
         }
      }

      if(isSet($cek_email) && $cek_email == false) $errorArray['user_email'] = $this->language->_('Invalid format');

      if(!$checkDuplicateEmail) $errorArray['user_email'] = $this->language->_('Email is already used in this company. Please use another');
		if(!empty($error_remark)){
			$errorArray[] = $error_remark;
		}
	
      $this->view->error_msg = $errorArray;
	  
    }
  }
  else
  {
    $this->view->user_fullname = $resultdata['USER_FULLNAME'];
    $this->view->user_email    = $resultdata['USER_EMAIL'];
    $this->view->user_phone    = $resultdata['USER_PHONE'];
    $this->view->token_id      = $resultdata['TOKEN_ID'];
    $this->view->user_ext      = $resultdata['USER_EXT'];
    $this->view->user_isemail  = $resultdata['USER_ISEMAIL'];

    //privilege
    $fuser_id  = $cust_id . $user_id;
    $privilege = $this->_db->select()
                  ->from('M_FPRIVI_USER')
                  ->where('FUSER_ID ='.$this->_db->quote($fuser_id))
                  ->query()->fetchAll();
				  
				  
	$debit  = $this->_db->select()
                                  ->from('M_USER_DEBIT')
                                  ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
								  ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                                  ->query()->fetchAll();  		
		$debitarr = '';
		if(!empty($debit)){
			
			foreach($debit as $kval => $dval){
				if(empty($debit[$kval+1]['USER_DEBITNUMBER'])){
					$debitarr .= $dval['USER_DEBITNUMBER'];
				}else{
					$debitarr .= $dval['USER_DEBITNUMBER'].';';
				}
			}
		}		
		$this->view->user_debitnumber = $debitarr;			  

    if(count($privilege) > 0)
    {
      $this->view->priviView = Application_Helper_Array::simpleArray($privilege,'FPRIVI_ID');
    }
    else
    {
       $this->view->priviView = array();
    }

 }

   //--------query privilege-------------
   //modifikasi privilege agar bisa ditampilkan sesuai golongan
   $this->view->getModuleDescArr = $this->getModuleDescArr();

   $privilege_final = array();
   $privilege_final_modif = array();
   foreach($getprivilege as $row)
   {
       $fprivi_moduleid = trim($row['FPRIVI_MODULEID']);
       $privilege_final[$fprivi_moduleid][] = $row;
       $privilege_final_modif = $this->modifPrivi($privilege_final);
   }

   $setting = new Settings();
      $system_type = $setting->getSetting('system_type');

   $this->view->fprivilege = $privilege_final_modif;
   $this->view->template   = Application_Helper_Array::listArray($this->getTemplate($system_type),'FTEMPLATE_ID','FTEMPLATE_DESC');

   $priviTemplate = array();
   foreach($this->getPriviTemplate() as $row)
   {
      $priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
   }

   $this->view->priviTemplate = $priviTemplate;
  //END query privilege


  $select = $this->_db->select()
                         ->from('M_FGROUP',array('FGROUP_ID','FGROUP_NAME'))
                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' OR UPPER(CUST_ID)='.$this->_db->quote('BANK'))
                         ->where('UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
  $this->view->fgroup = $this->_db->fetchAll($select);

  $select = $this->_db->select()
                         ->from('M_CUSTOMER',array('CUST_NAME'))
                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
  $this->view->cust_name = $this->_db->fetchOne($select);

  $this->view->user_status = $this->_masterStatus['desc']['active'];
  $this->view->cust_id = $cust_id;
  $this->view->user_id = $user_id;

  $this->view->modulename = $this->_request->getModuleName();

  Application_Helper_General::writeLog('UPUS','Edit User');
}

public function checkduplicateemailAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $email = $this->_getParam('email');
    $userid = $this->_getParam('userid');

    $select = $this->_db->select()
                       ->from('M_USER',array('USER_EMAIL','USER_ID'))
                       ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$this->_custIdLogin))
                       ->where('USER_EMAIL = ?', $email)
                       ->where('USER_STATUS != 3');

    $data = $this->_db->fetchRow($select);
    
    if (empty($data)) {
      $result = 'false';
    }else{
      $result = 'true';
      if($data['USER_ID'] == $userid){
          $result = 'false';
      }
    }

    echo $result;
}


}