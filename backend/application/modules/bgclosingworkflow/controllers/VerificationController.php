<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgclosingworkflow_VerificationController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$conf = Zend_Registry::get('config');

		$system_type = $settings->getSetting('system_type');
		$this->view->systemType = $system_type;

		$this->_bankName = $conf['app']['bankname'];
		$this->view->masterbankname = $this->_bankName;

		$this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

		$fields = array(
			'bgno'     => array(
				'field'    => 'BG_NUMBER',
				'label'    => $this->language->_('Nomor BG / Subjek'),
			),
			'principal'   => array(
				'field'    => 'CUST_ID',
				'label'    => $this->language->_('Principal'),
			),
			'obligee'     => array(
				'field'    => 'BG_SUBJECT',
				'label'    => $this->language->_('Obligee'),
			),
			'bank_branch'   => array(
				'field'    => 'BRANCH_NAME',
				'label'    => $this->language->_('Bank Cabang'),
			),
			'bgamount'  => array(
				'field'    => 'BG_AMOUNT',
				'label'    => $this->language->_('Nominal BG')
			),
			'change_type' => array(
				'field' => 'CHANGE_TYPE',
				'label' => $this->language->_('Tipe Usulan'),
			),
		);

		// if ($this->view->hasPrivilege("LPCC") && $this->view->hasPrivilege("VPCC")) $warantyIn = ['1']; // Cash Collateral 
		if ($this->view->hasPrivilege("LPCC")) $warantyIn = ['1']; // Cash Collateral 
		// if ($this->view->hasPrivilege("LPNC") && $this->view->hasPrivilege("VPNC")) $warantyIn = ['2', '3']; // Non-Cash Collateral
		if ($this->view->hasPrivilege("LPNC")) $warantyIn = ['2', '3']; // Non-Cash Collateral

		// if ($this->view->hasPrivilege("LPCC") && $this->view->hasPrivilege("LPNC") && $this->view->hasPrivilege("VPCC") && $this->view->hasPrivilege("VPNC")) $warantyIn = ['1','2', '3']; 
		if ($this->view->hasPrivilege("LPCC") && $this->view->hasPrivilege("LPNC")) $warantyIn = ['1', '2', '3'];

		$select = $this->_db->select()
			->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH'))
			->where('A.BUSER_ID = ?', $this->_userIdLogin);
		$buser = $this->_db->fetchRow($select);
		$branchUser = $buser['BUSER_BRANCH'];

		$select2 = $this->_db->select()
			->from(array('A' => 'M_BRANCH'), array('BRANCH_CODE'))
			->where('A.ID = ?', $buser['BUSER_BRANCH']);
		$branch = $this->_db->fetchRow($select2);

		$selecthq = $this->_db->select()
			->from(array('A' => 'M_BRANCH'), array('HEADQUARTER'))
			->where('A.ID = ?', $buser['BUSER_BRANCH']);
		$datahq = $this->_db->fetchRow($selecthq);

		$select3 = $this->_db->select()
			->from(
				array('A' => 'M_BRANCH_COVERAGE'),
				array('BUSER_BRANCH' => 'B.ID')
			)
			->join(
				array('B' => 'M_BRANCH'),
				'A.BRANCH_COVERAGE_CODE = B.BRANCH_CODE',
				array()
			)
			->where('A.BRANCH_CODE = ?', $branch['BRANCH_CODE']);
		$branchCoverage = $this->_db->fetchAll($select3);

		if (!empty($branchCoverage)) {
			foreach ($branchCoverage as $key => $value) {
				$branchCoveragetemp[] .= $value['BUSER_BRANCH'];
			}
		}

		$arr1[] = $branchUser;

		$selectbg = $this->_db->select()
			->from(
				array('A' => 'T_BANK_GUARANTEE'),
				array(
					'REG_NUMBER'                    => 'BG_REG_NUMBER',
					'BG_NUMBER'                    =>  'BG_NUMBER',
					'SUBJECT'                       => 'BG_SUBJECT',
					'APLICANT'                      => 'B.CUST_NAME',
					'RECIPIENT_NAME'                => 'RECIPIENT_NAME',
					'TIME_PERIOD_START'             => 'TIME_PERIOD_START',
					'TIME_PERIOD_END'               => 'TIME_PERIOD_END',
					'COUNTER_WARRANTY_TYPE'         => 'COUNTER_WARRANTY_TYPE',
					'COUNTER_WARRANTY_ACCT_NO'      => 'COUNTER_WARRANTY_ACCT_NO',
					'COUNTER_WARRANTY_ACCT_NAME'    => 'COUNTER_WARRANTY_ACCT_NAME',
					'CHANGE_TYPE_CLOSE'             => 'D.CHANGE_TYPE',
					'AMOUNT'                        => 'BG_AMOUNT',
					'BRANCH_NAME'                   => 'C.BRANCH_NAME',
					'SP_OBLIGEE_CODE'               => 'A.SP_OBLIGEE_CODE',
				)
			)
			->join(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID')
			->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE')
			->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
			->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn)
			->where('D.SUGGESTION_STATUS IN (?)', [4, 9])
			->order('A.BG_CREATED DESC');

		if ($datahq['HEADQUARTER'] == "NO") {
			$selectbg->where('C.ID IN (?)', $arr1);
		}


		// BRANCH COVERAGE
		if (!empty($branchCoverage)) {
			$selectbg2 = $this->_db->select()
				->from(
					array('A' => 'T_BANK_GUARANTEE'),
					array(
						'REG_NUMBER'                    => 'BG_REG_NUMBER',
						'BG_NUMBER'                    =>  'BG_NUMBER',
						'SUBJECT'                       => 'BG_SUBJECT',
						'APLICANT'                      => 'B.CUST_NAME',
						'RECIPIENT_NAME'                => 'RECIPIENT_NAME',
						'TIME_PERIOD_START'             => 'TIME_PERIOD_START',
						'TIME_PERIOD_END'               => 'TIME_PERIOD_END',
						'COUNTER_WARRANTY_TYPE'         => 'COUNTER_WARRANTY_TYPE',
						'COUNTER_WARRANTY_ACCT_NO'      => 'COUNTER_WARRANTY_ACCT_NO',
						'COUNTER_WARRANTY_ACCT_NAME'    => 'COUNTER_WARRANTY_ACCT_NAME',
						'CHANGE_TYPE_CLOSE'             => 'D.CHANGE_TYPE',
						'AMOUNT'                        => 'BG_AMOUNT',
						'BRANCH_NAME'                   => 'C.BRANCH_NAME',
						'SP_OBLIGEE_CODE'               => 'A.SP_OBLIGEE_CODE',
					)
				)
				->join(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID')
				->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE')
				->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
				->where('C.ID IN (?)', $branchCoveragetemp) // Add Bahri
				->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn)
				->where('D.SUGGESTION_STATUS IN (?)', [4, 9])
				->order('A.BG_CREATED DESC');
		} else {
			$selectbg2 = $this->_db->select()
				->from(
					array('A' => 'T_BANK_GUARANTEE'),
					array(
						'REG_NUMBER'                    => 'BG_REG_NUMBER',
						'BG_NUMBER'                    =>  'BG_NUMBER',
						'SUBJECT'                       => 'BG_SUBJECT',
						'APLICANT'                      => 'B.CUST_NAME',
						'RECIPIENT_NAME'                => 'RECIPIENT_NAME',
						'TIME_PERIOD_START'             => 'TIME_PERIOD_START',
						'TIME_PERIOD_END'               => 'TIME_PERIOD_END',
						'COUNTER_WARRANTY_TYPE'         => 'COUNTER_WARRANTY_TYPE',
						'COUNTER_WARRANTY_ACCT_NO'      => 'COUNTER_WARRANTY_ACCT_NO',
						'COUNTER_WARRANTY_ACCT_NAME'    => 'COUNTER_WARRANTY_ACCT_NAME',
						'CHANGE_TYPE_CLOSE'             => 'D.CHANGE_TYPE',
						'AMOUNT'                        => 'BG_AMOUNT',
						'BRANCH_NAME'                   => 'C.BRANCH_NAME',
						'SP_OBLIGEE_CODE'               => 'A.SP_OBLIGEE_CODE',
					)
				)
				->join(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID')
				->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE')
				->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
				->where('C.ID IN (?)', '0') // Add Bahri
				->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn)
				->where('D.SUGGESTION_STATUS IN (?)', [4, 9])
				->order('A.BG_CREATED DESC');
		}

		// filter ----------------------------------------------------------
		$filterlist = array('Nomor BG' => 'BG_NUMBER', 'Subject' => 'BG_SUBJECT', 'Principal' => 'CUST_ID', 'Obligee' => 'OBLIGEE_NAME', 'Spesial Obligee' => 'SP_OBLIGEE_CODE', 'Bank Cabang' => 'BRANCH_NAME', 'Tipe Usulan' => 'CHANGE_TYPE');
		$this->view->filterlist = $filterlist;

		$filterArr = array(
			'BG_NUMBER'  => array('StringTrim', 'StripTags'),
			'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
			'OBLIGEE_NAME'  => array('StringTrim', 'StripTags'),
			'SP_OBLIGEE_CODE'  => array('StringTrim', 'StripTags'),
			'BRANCH_NAME'   => array('StringTrim', 'StripTags'),
			'CUST_ID'   => array('StringTrim', 'StripTags'),
			'CHANGE_TYPE'   => array('StringTrim', 'StripTags'),
		);

		$dataParam = array('BG_NUMBER', 'BG_SUBJECT', 'OBLIGEE_NAME', 'SP_OBLIGEE_CODE', 'BRANCH_NAME', 'CUST_ID', 'CHANGE_TYPE');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}
		}

		$validators = array(
			'BG_NUMBER' => array(),
			'BG_SUBJECT' => array(),
			'OBLIGEE_NAME' => array(),
			'SP_OBLIGEE_CODE' => array(),
			'BRANCH_NAME' => array(),
			'CUST_ID' => array(),
			'CHANGE_TYPE' => array(),
		);

		$zf_filter = new Zend_Filter_Input($filterArr, $validators, $dataParamValue);
		$filter = $this->_getParam('filter');

		$fBgNumber = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));
		$fSubject = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
		$fObligateName = html_entity_decode($zf_filter->getEscaped('OBLIGEE_NAME'));
		$spObligee = html_entity_decode($zf_filter->getEscaped('SP_OBLIGEE_CODE'));
		$fBranchName = html_entity_decode($zf_filter->getEscaped('BRANCH_NAME'));
		$fPrincipal = html_entity_decode($zf_filter->getEscaped('CUST_ID'));
		$changeType = html_entity_decode($zf_filter->getEscaped('CHANGE_TYPE'));

		if ($filter == TRUE) {
			if ($fBgNumber) {
				$selectbg->where("A.BG_NUMBER LIKE " . $this->_db->quote('%' . $fBgNumber[0] . '%'));
				$selectbg2->where("A.BG_NUMBER LIKE " . $this->_db->quote('%' . $fBgNumber[0] . '%'));
			}
			if ($fSubject) {
				$selectbg->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $fSubject[0] . '%'));
				$selectbg2->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $fSubject[0] . '%'));
			}
			if ($fObligateName) {
				$selectbg->where("A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $fObligateName[0] . '%'));
				$selectbg2->where("A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $fObligateName[0] . '%'));
			}
			if ($spObligee) {
				if ($spObligee == "1") {
					$selectbg->where("A.SP_OBLIGEE_CODE <> ?", '');
					$selectbg2->where("A.SP_OBLIGEE_CODE <> ?", '');
				} else {
					$selectbg->where("A.SP_OBLIGEE_CODE = ?", '');
					$selectbg2->where("A.SP_OBLIGEE_CODE = ?", '');
				}
			}
			if ($fBranchName) {
				$selectbg->where("C.BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fBranchName[0]) . '%'));
				$selectbg2->where("C.BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fBranchName[0]) . '%'));
			}
			if ($fPrincipal) {
				$selectbg->where("B.CUST_NAME LIKE " . $this->_db->quote('%' . $fPrincipal . '%'));
				$selectbg2->where("B.CUST_NAME LIKE " . $this->_db->quote('%' . $fPrincipal . '%'));
			}
			if ($changeType) {
				$selectbg->where("D.CHANGE_TYPE = " . $this->_db->quote($changeType));
				$selectbg2->where("D.CHANGE_TYPE = " . $this->_db->quote($changeType));
			}
		}

		$this->view->fields = $fields;
		$this->view->filter = $filter;
		// filter -----------------------------------------------------------------

		$selectbg = $this->_db->fetchAll($selectbg);
		$selectbg2 = $this->_db->fetchAll($selectbg2);
		$result = array_merge($selectbg, $selectbg2);

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$sessionNamespace->token = $rand;
		$this->view->token = $sessionNamespace->token;

		foreach ($result as $key => $value) {
			$get_reg_number = $value["BG_NUMBER"];
			$AESMYSQL = new Crypt_AESMYSQL();
			$rand = $this->token;
			$encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
			$encpayreff = urlencode($encrypted_payreff);
			$result[$key]["BG_NUMBER_ENCRYPTED"] = $encpayreff;
		}

		$this->paging($result);

		$ctdesc = $conf["bgclosing"]["changetype"]["desc"];
		$ctcode = $conf["bgclosing"]["changetype"]["code"];
		$arr_type = array_combine(array_values($ctcode), array_values($ctdesc));
		$this->view->arr_type = $arr_type;

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[] = $key;
				$whereval[] = $value;
			}
			$this->view->wherecol = $wherecol;
			$this->view->whereval = $whereval;
		}

		// Simpan log aktifitas user ke table T_FACTIVITY
		if ($this->view->hasPrivilege("VVCC")) {
			Application_Helper_General::writeLog('VVCC', $this->language->_('Lihat Daftar Pengajuan Penutupan dan Klaim Menunggu Verifikasi Bank'));
		} elseif ($this->view->hasPrivilege("VVNC")) {
			Application_Helper_General::writeLog('VVNC', $this->language->_('Lihat Daftar Pengajuan Penutupan dan Klaim Menunggu Verifikasi Bank'));
		}
	}

	public function filterAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$param = $this->_getParam('type');

		if ($param == 'principal') {
			$tblName = $this->_getParam('id');
			$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
			$select_principal = $this->_db->select()
				->from(array('A' => 'M_CUST_LINEFACILITY'), array("A.CUST_ID", "B.CUST_NAME"))
				->join(['B' => 'M_CUSTOMER'], 'A.CUST_ID = B.CUST_ID')
				->where('A.CUST_SEGMENT = 4')
				->query()->fetchAll();
			$save_principal = [];

			foreach ($select_principal as $key => $value) {
				$save_principal[$key] = $value["CUST_NAME"];
			}
			if (!empty($tblName)) {
				foreach ($save_principal as $key => $row) {
					if ($tblName == $row) {
						$select = 'selected';
					} else {
						$select = '';
					}
					$optHtml .= "<option value='" . $row . "' " . $select . ">" . $row . "</option>";
				}
			} else {
				foreach ($save_principal as $key => $row) {
					$optHtml .= "<option value='" . $row . "'>" . $row . "</option>";
				}
			}
			echo $optHtml;
		}

		if ($param == 'spobligee') {
			$tblName = $this->_getParam('id');
			$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
			$spobligee = [
				1 => 'Y',
				2 => 'T',
			];
			if (!empty($tblName)) {
				foreach ($spobligee as $key => $row) {
					if ($key == 1 || $key == 2) {
						if ($tblName == $key) {
							$select = 'selected';
						} else {
							$select = '';
						}
						$optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
					}
				}
			} else {
				foreach ($spobligee as $key => $row) {
					$optHtml .= "<option value='" . $key . "'>" . $row . "</option>";
				}
			}
			echo $optHtml;
		}

		if ($param == 'branch') {
			$tblName = $this->_getParam('id');
			$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
			$select_branch = $this->_db->select()
				->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
				->order('A.BRANCH_NAME ASC')
				->query()->fetchAll();
			$save_branch = [];

			foreach ($select_branch as $key => $value) {
				$save_branch[$key] = $value["BRANCH_NAME"];
			}
			if (!empty($tblName)) {
				foreach ($save_branch as $key => $row) {
					if ($tblName == $row) {
						$select = 'selected';
					} else {
						$select = '';
					}
					$optHtml .= "<option value='" . $row . "' " . $select . ">" . $row . "</option>";
				}
			} else {
				foreach ($save_branch as $key => $row) {
					$optHtml .= "<option value='" . $row . "'>" . $row . "</option>";
				}
			}
			echo $optHtml;
		}

		if ($param == 'change_type') {
			$tblName = $this->_getParam('id');
			$optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
			$change_type = [
				1 => 'Penutupan Permintaan Prinsipal',
				2 => 'Pelepasan MD',
				3 => 'Klaim Obligee'
			];
			foreach ($change_type as $key => $row) {
				if ($key == 1 || $key == 2 || $key == 3) {
					if ($tblName == $key) {
						$select = 'selected';
					} else {
						$select = '';
					}
					$optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
				}
			}
			echo $optHtml;
		}
	}
}
