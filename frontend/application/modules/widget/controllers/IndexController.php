<?php
require_once 'Zend/Controller/Action.php';

class Widget_IndexController extends Application_Main
{

    public function indexAction()
    {
        // Not used
    }

    public function printAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        //        Zend_Debug::dump($param);
        //        die;
        $this->view->data_caption = $param['data_caption'];
        $this->view->data_header = $param['data_header'];
        $this->view->data_content = $param['data_content'];
        $this->view->data_filter = $param['data_filter'];
        $this->view->filter = $param['filter'];
    }

    public function printtableAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        //        Zend_Debug::dump($param);
        //        die;
        $this->view->data_caption = $param['data_caption'];
        $this->view->data_header = $param['data_header'];
        $this->view->data_content = $param['data_content'];
        $this->view->data_filter = $param['data_filter'];
        $this->view->val_filter = $param['val_filter'];
    }

    public function printcustomerdetailallAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();

        $this->view->compInfo        = $param['compInfo'];
        $this->view->accountInfo     = $param['accountInfo'];
        $this->view->groupUser       = $param['groupUser'];
        $this->view->dataBoundary    = $param['dataBoundary'];
        $this->view->transferTypeArr = $param['transferTypeArr'];
        $this->view->lineFacility      = $param['lineFacility'];
        $this->view->specialObligee      = $param['specialObligee'];
        $this->view->CUST_MODEL = $param['CUST_MODEL'];
    }


    public function printeformdetailbgAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();

        $viewHtml = $this->view->render('index/printeformdetailbg.phtml');

        $this->view->data        = $param['data'];
        $this->view->arrStatus        = $param['arrStatus'];

        $this->_helper->download->pdfModif(null, null, null, 'BG Detail ' . $param['data']['BG_NUMBER'], $viewHtml);
    }


    public function printcustomerdetailcurrentAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();

        $this->view->compInfo        = $param['compInfo'];
        $this->view->accountInfo     = $param['accountInfo'];
        $this->view->groupUser       = $param['groupUser'];
        $this->view->dataBoundary    = $param['dataBoundary'];
        $this->view->transferTypeArr = $param['transferTypeArr'];
        $this->view->currentTab      = $param['currentTab'];
        $this->view->lineFacility      = $param['lineFacility'];
        $this->view->specialObligee      = $param['specialObligee'];
        $this->view->CUST_MODEL = $param['CUST_MODEL'];
    }

    public function printautotopupAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();

        $this->view->data_caption       = $param['data_caption'];
        $this->view->airlinesData       = $param['airlinesData'];
        $this->view->airlinesImg        = $param['airlinesImg'];
        $this->view->data_compname      = $param['data_compname'];
        $this->view->data_status        = $param['data_status'];
    }

    public function printtrxxxAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        $this->view->psnumber = $param['psnumber'];
        $this->view->param = $param['param'];
    }

    public function printtrxtaxAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        $this->view->psnumber = $param['psnumber'];
        $this->view->param = $param['param'];
    }

    public function printtrxbinAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        $this->view->psnumber = $param['psnumber'];
        $this->view->param = $param['param'];
    }

    public function printtrxxxxAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        $psnumber = $param['payReff'];
        $getPaymentDetail   = new paymentreport_Model_Paymentreport();
        $detail = $getPaymentDetail->getPaymentDetail($psnumber);

        $this->view->PS_NUMBER      =   $paymentref = $detail[0]['PS_NUMBER'];
        $this->view->PS_STATUS      =   $paystatus = $detail[0]['PS_STATUS'];
        $this->view->SOURCE_ACCOUNT     =   $source = $detail[0]['SOURCE_ACCOUNT'];
        $this->view->PS_PERIODIC    =   $detail[0]['PS_PERIODIC'];
        $this->view->PS_CREATED     =   $created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_UPDATED     =   $updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_EFDATE      =   $efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
        $this->view->PS_TOTAL_AMOUNT = Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']);
        $this->view->PS_CCY = $detail[0]['PS_CCY'];
        $this->view->SOURCE_ACCOUNT_NAME = $detail[0]['SOURCE_ACCOUNT_NAME'];
        $this->view->SOURCE_ACCOUNT_TYPE = $detail[0]['SOURCE_ACCOUNT_TYPE'];
        $this->view->TRA_MESSAGE = $detail[0]['TRA_MESSAGE'];
        $this->view->BENEFICIARY_ACCOUNT_NAME = $detail[0]['BENEFICIARY_ACCOUNT_NAME'];
        $this->view->BENEFICIARY_ACCOUNT = $detail[0]['BENEFICIARY_ACCOUNT'];
        $this->view->BENEFICIARY_ACCOUNT_CCY = $detail[0]['BENEFICIARY_ACCOUNT_CCY'];
        $this->view->TRA_AMOUNT = Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']);
        $this->view->BENEFICIARY_EMAIL = Application_Helper_General::displayMoney($detail[0]['BENEFICIARY_EMAIL']);
        $this->view->PS_TYPE = $type = $detail[0]['PS_TYPE'];
        $this->view->PS_SUBJECT = $paysubject = $detail[0]['PS_SUBJECT'];

        $selectPer = $this->_db->select()
            ->from(array('u' => 'T_PERIODIC'), array('PS_PERIODIC_ENDDATE', 'PS_PERIODIC', 'PS_PERIODIC_NEXTDATE'))
            ->where('UPPER(u.PS_PERIODIC)=' . $this->_db->quote((string)$detail[0]['PS_PERIODIC']))
            ->query()->fetchAll();

        $this->view->PS_PERIODIC_ENDDATE =  $efdate = Application_Helper_General::convertDate($selectPer[0]['PS_PERIODIC_ENDDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

        $tanggal = $selectPer[0]['PS_PERIODIC_NEXTDATE'];
        $this->view->PS_PERIODIC_NEXTDATE = $day = date('l', strtotime($tanggal));


        $USER_ID = $detail[0]['USER_ID'];

        $select = $this->_db->select()
            ->from(array('u' => 'M_USER_ACCT'), array('USER_ID', 'ACCT_NO', 'ACCT_STATUS', 'ACCT_DESC'))
            ->where('UPPER(u.ACCT_NO)=' . $this->_db->quote((string)$source))
            ->where('UPPER(u.USER_ID)=' . $this->_db->quote((string)$USER_ID))
            ->query()->fetchAll();

        $this->view->ACCT_DESC = $select[0]['ACCT_DESC'];
        $date_val   = date('Y-m-d');
        echo '<BR>';
        if ($detail[0]['PS_EFDATE'] == $date_val) {
            $todaytranfer = $this->language->_('Today Transfer');
            $this->view->paymentType = $todaytranfer;
        } else {
            $pendingfuturedate = $this->language->_('Future Date Transfer');
            $this->view->paymentType = $pendingfuturedate;
        }

        if ($detail[0]['PS_EFDATE'] == $date_val) {
            if ($detail[0]['TRA_STATUS'] == '1') {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Suspect, Please Contact Customer Care');
            } elseif ($detail[0]['TRA_STATUS'] == '3') {
                $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
            } else {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Failed');
            }
        } else { //pending future date
            $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
        }
    }

    public function printtrxAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        $psnumber = $param['payReff'];
        $getPaymentDetail   = new paymentreport_Model_Paymentreport();
        $detail = $getPaymentDetail->getPaymentDetail($psnumber);

        $this->view->PS_NUMBER      =   $paymentref = $detail[0]['PS_NUMBER'];
        $this->view->PS_STATUS      =   $paystatus = $detail[0]['PS_STATUS'];
        $this->view->SOURCE_ACCOUNT     =   $source = $detail[0]['SOURCE_ACCOUNT'];
        $this->view->PS_PERIODIC    =   $detail[0]['PS_PERIODIC'];
        $this->view->PS_CREATED     =   $created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_UPDATED     =   $updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_EFDATE      =   $efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
        $this->view->PS_TOTAL_AMOUNT = Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']);
        $this->view->PS_CCY = $detail[0]['PS_CCY'];
        $this->view->SOURCE_ACCOUNT_NAME = $detail[0]['SOURCE_ACCOUNT_NAME'];
        $this->view->SOURCE_ACCOUNT_TYPE = $detail[0]['SOURCE_ACCOUNT_TYPE'];
        $this->view->TRA_MESSAGE = $detail[0]['TRA_MESSAGE'];
        $this->view->BENEFICIARY_ACCOUNT_NAME = $detail[0]['BENEFICIARY_ACCOUNT_NAME'];
        $this->view->BENEFICIARY_ACCOUNT = $detail[0]['BENEFICIARY_ACCOUNT'];
        $this->view->BENEFICIARY_ACCOUNT_CCY = $detail[0]['BENEFICIARY_ACCOUNT_CCY'];
        $this->view->TRA_AMOUNT = Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']);
        $this->view->BENEFICIARY_EMAIL = Application_Helper_General::displayMoney($detail[0]['BENEFICIARY_EMAIL']);
        $this->view->PS_TYPE = $type = $detail[0]['PS_TYPE'];
        $this->view->PS_SUBJECT = $paysubject = $detail[0]['PS_SUBJECT'];

        $selectPer = $this->_db->select()
            ->from(array('u' => 'T_PERIODIC'), array('PS_PERIODIC_ENDDATE', 'PS_PERIODIC', 'PS_PERIODIC_NEXTDATE'))
            ->where('UPPER(u.PS_PERIODIC)=' . $this->_db->quote((string)$detail[0]['PS_PERIODIC']))
            ->query()->fetchAll();

        $this->view->PS_PERIODIC_ENDDATE =  $efdate = Application_Helper_General::convertDate($selectPer[0]['PS_PERIODIC_ENDDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

        $tanggal = $selectPer[0]['PS_PERIODIC_NEXTDATE'];
        $this->view->PS_PERIODIC_NEXTDATE = $day = date('l', strtotime($tanggal));


        $USER_ID = $detail[0]['USER_ID'];

        $select = $this->_db->select()
            ->from(array('u' => 'M_USER_ACCT'), array('USER_ID', 'ACCT_NO', 'ACCT_STATUS', 'ACCT_DESC'))
            ->where('UPPER(u.ACCT_NO)=' . $this->_db->quote((string)$source))
            ->where('UPPER(u.USER_ID)=' . $this->_db->quote((string)$USER_ID))
            ->query()->fetchAll();

        $this->view->ACCT_DESC = $select[0]['ACCT_DESC'];
        $date_val   = date('Y-m-d');
        echo '<BR>';
        if ($detail[0]['PS_EFDATE'] == $date_val) {
            $todaytranfer = $this->language->_('Today Transfer');
            $this->view->paymentType = $todaytranfer;
        } else {
            $pendingfuturedate = $this->language->_('Future Date Transfer');
            $this->view->paymentType = $pendingfuturedate;
        }

        if ($detail[0]['PS_EFDATE'] == $date_val) {
            if ($detail[0]['TRA_STATUS'] == '1') {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Suspect, Please Contact Customer Care');
            } elseif ($detail[0]['TRA_STATUS'] == '3') {
                $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
            } else {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Failed');
            }
        } else { //pending future date
            $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
        }
    }


    public function printtrxloanAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        $psnumber = $param['payReff'];
        //print_r($param);die;
        $acct_no = $param['acct_no'];
        $ccy_id = $param['ccy_id'];
        $prod = $param['prod'];
        /*$Account = new Account($acct_no,Application_Helper_General::getCurrNum($ccy_id));
        $Account->setFlag(false);
        $Account->checkBalanceLoan();
        
        $this->view->product = $prod;
        $this->view->acct_no = $acct_no;
        $this->view->ccy_id = $ccy_id;
        $this->view->detail = $Account->getCoreAccountDetail();
        */
        $acct_no = $param['acct_no'];
        $ccy_id = $param['ccy_id'];
        $prod = $param['prod'];
        $status = $param['status'];
        $plafond = $param['plafond'];
        $numberOfInstallment = $param['numberOfInstallment'];
        $outstanding = $param['outstanding'];
        $interestObligations = $param['interestObligations'];
        $installment = $param['installment'];
        $totalPayment = $param['totalPayment'];
        $interestArrears = $param['interestArrears'];
        $arrearsInInstallments = $param['arrearsInInstallments'];
        $paymentOfFines = $param['paymentOfFines'];
        $termsLoansTimes = $param['termsLoansTimes'];
        $accountCreateDate = Application_Helper_General::convertDate($param['accountCreateDate'], $this->_dateDBFormat, $this->_dateDisplayFormat);
        $disbursementDate = Application_Helper_General::convertDate($param['disbursementDate'], $this->_dateDBFormat, $this->_dateDisplayFormat);
        $nextPaymentDate = Application_Helper_General::convertDate($param['nextPaymentDate'], $this->_dateDBFormat, $this->_dateDisplayFormat);;
        $dueDate = Application_Helper_General::convertDate($param['dueDate'], $this->_dateDBFormat, $this->_dateDisplayFormat);

        $this->view->product = $prod;
        $this->view->acct_no = $acct_no;
        $this->view->ccy_id = $ccy_id;
        $this->view->status = $status;
        $this->view->plafond = $plafond;
        $this->view->numberOfInstallment = $numberOfInstallment;
        $this->view->outstanding = $outstanding;
        $this->view->interestObligations = $interestObligations;
        $this->view->installment = $installment;
        $this->view->totalPayment = $totalPayment;
        $this->view->interestArrears = $interestArrears;
        $this->view->arrearsInInstallments = $arrearsInInstallments;
        $this->view->paymentOfFines = $paymentOfFines;
        $this->view->termsLoansTimes = $termsLoansTimes;
        $this->view->accountCreateDate = $accountCreateDate;
        $this->view->disbursementDate = $disbursementDate;
        $this->view->nextPaymentDate = $nextPaymentDate;
        $this->view->dueDate = $dueDate;
    }

    public function printaccstatAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();


        $this->view->header = $param['header'];
        $this->view->acct_stat_layout = $param['acct_stat_layout'];
        $this->view->dataDetails = $param['dataDetails'];

        $accInfo = $param['accInfo'];
        foreach ($accInfo as $key => $val) {
            if ($key == "totalDebet" || $key == "totalKredit")
                $val = $val == 0 ? 0.00 : $val;

            $this->view->$key = $val;
        }
    }

    public function printtrxremitAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        $psnumber = $param['payReff'];
        $getPaymentDetail   = new paymentreport_Model_Paymentreport();
        $detail = $getPaymentDetail->getPaymentDetail($psnumber);

        $this->view->PS_NUMBER      =   $paymentref = $detail[0]['PS_NUMBER'];
        $this->view->PS_STATUS      =   $paystatus = $detail[0]['PS_STATUS'];
        $this->view->SOURCE_ACCOUNT     =   $source = $detail[0]['SOURCE_ACCOUNT'];
        $this->view->PS_PERIODIC    =   $detail[0]['PS_PERIODIC'];
        $this->view->PS_CREATED     =   $created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_UPDATED     =   $updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_EFDATE      =   $efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);


        $forcharge = new Zend_Session_Namespace('forcharge');
        if (isset($forcharge->chargesDet)) {
            $this->view->CHARGE_DETAIL = $forcharge->chargesDet;
        }

        $this->view->CHARGE_TYPE = $detail[0]['TRANSFER_TYPE'];

        $this->view->PS_CCY = $detail[0]['PS_CCY'];
        $this->view->SOURCE_ACCOUNT_NAME = $detail[0]['SOURCE_ACCOUNT_NAME'];
        $this->view->SOURCE_ACCOUNT_TYPE = $detail[0]['SOURCE_ACCOUNT_TYPE'];
        $this->view->TRA_MESSAGE = $detail[0]['TRA_MESSAGE'];
        $this->view->BENEFICIARY_ACCOUNT_NAME = $detail[0]['BENEFICIARY_ACCOUNT_NAME'];
        $this->view->BENEFICIARY_ACCOUNT = $detail[0]['BENEFICIARY_ACCOUNT'];
        $this->view->BENEFICIARY_ACCOUNT_CCY = $detail[0]['BENEFICIARY_ACCOUNT_CCY'];
        $this->view->TRA_AMOUNT = Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']);
        $this->view->BENEFICIARY_EMAIL = Application_Helper_General::displayMoney($detail[0]['BENEFICIARY_EMAIL']);
        $this->view->PS_TYPE = $type = $detail[0]['PS_TYPE'];
        $this->view->PS_SUBJECT = $paysubject = $detail[0]['PS_SUBJECT'];

        $this->view->BENEFICIARY_ADDRESS = $detail[0]['BENEFICIARY_ADDRESS'];

        $citizenshipArr     = array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
        $nationalityArr     = array_combine($this->_nationality["code"], $this->_nationality["desc"]);

        $this->view->BENEFICIARY_RESIDENT  = $citizenshipArr[$detail[0]['BENEFICIARY_RESIDENT']];
        $this->view->BENEFICIARY_CITIZENSHIP  = $nationalityArr[$detail[0]['BENEFICIARY_CITIZENSHIP']];
        //$this->view->ACCTSRC_CATEGORY  = $detail[0]['BENEFICIARY_CATEGORY'];
        //$this->view->ACCTSRC_ID_TYPE  = $detail[0]['BENEFICIARY_ID_TYPE'];
        //$this->view->ACCTSRC_ID_NUMBER  = $detail[0]['BENEFICIARY_ID_NUMBER'];
        //$this->view->CURR_CODE  = 'USD';

        //$kurs = $detail[0]['EQUIVALENT_AMOUNT_IDR']/$detail[0]['TRA_AMOUNT'];
        //$this->view->kurs  = Application_Helper_General::displayMoney($kurs);
        $this->view->kurs = Application_Helper_General::displayMoney($detail[0]['RATE']);
        $this->view->TRA_MESSAGE  = $detail[0]['TRA_MESSAGE'];

        $this->view->eqamount  = Application_Helper_General::displayMoney($detail[0]['EQUIVALENT_AMOUNT_IDR']);



        $this->view->ACBENEF_IDENTYNUM  = $detail[0]['BENEFICIARY_ID_NUMBER'];


        $this->view->ACBENEF_BANKNAME = $detail[0]['BENEFICIARY_BANK_NAME'];
        $this->view->ACBENEF_BANK_ADD1 = $detail[0]['BENEFICIARY_BANK_ADDRESS1'];
        $this->view->ACBENEF_BANK_ADD2 = $detail[0]['BENEFICIARY_BANK_ADDRESS2'];
        $this->view->ACBENEF_CITYNAME = $detail[0]['BENEFICIARY_BANK_CITY'];
        $this->view->ACBENEF_POBNUM = $detail[0]['POB_NUMBER'];

        $selectCt = $this->_db->select()
            ->from('M_COUNTRY')
            ->where('COUNTRY_CODE = ?', $detail[0]['BENEFICIARY_BANK_COUNTRY']);
        $selectCt = $this->_db->fetchRow($selectCt);
        $countryName = $selectCt['COUNTRY_NAME'];

        $this->view->ACBENEF_COUNTRY = $countryName;

        if ($detail[0]['PS_CCY'] == "USD" && $detail[0]['TRA_AMOUNT'] >= 10000) {
            $LLD_HI = '';
        } else {
            $LLD_HI = 'none';
        }
        $settings = new Application_Settings();
        $lldIdenticalArr    = $settings->getLLDDOMIdentical();
        $lldRelationshipArr = $settings->getLLDDOMRelationship();
        $lldCategoryArr     = $settings->getLLDDOMCategory();
        $lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
        $lldBeneIdentifArr  = $settings->getLLDDOMBeneIdentification();

        $purchasing = new purchasing_Model_Purchasing();
        $purposeArr = $purchasing->getTranspurpose();

        foreach ($purposeArr as $row) {
            if ($row['CODE'] == $detail[0]['LLD_TRANSACTION_PURPOSE'])
                $lldpurpose = $row['DESCRIPTION'];
        }

        $this->view->LLD_TRANSACTION_PURPOSE  = $lldpurpose;
        $this->view->ACBENEF_CATEGORY = $lldCategoryArr[$detail[0]['BENEFICIARY_CATEGORY']];

        $this->view->LLD_HI  = $LLD_HI;
        $this->view->LLD_IDENTITY  = $detail[0]['LLD_IDENTITY'];
        $this->view->LLD_TRANSACTOR_RELATIONSHIP  = $detail[0]['LLD_TRANSACTOR_RELATIONSHIP'];
        $this->view->TransactorArr  = $lldRelationshipArr;
        $this->view->IdentyArr  = $lldIdenticalArr;

        $this->view->ACBENEF_IDENTY  = $lldBeneIdentifArr[$detail[0]['BENEFICIARY_ID_TYPE']];

        $this->view->SOURCE_RESIDENT = $citizenshipArr[$detail[0]['ACCT_RESIDENT']];
        $this->view->SOURCE_NATIONALITY = $nationalityArr[$detail[0]['ACCT_CITIZENSHIP']];
        $this->view->SOURCE_CATEGORY = $lldCategoryArr[$detail[0]['ACCT_CATEGORY']];
        $this->view->SOURCE_ID_TYPE = $lldSenderIdentifArr[$detail[0]['ACCT_ID_TYPE']];
        $this->view->SOURCE_ID_NUMBER = $detail[0]['ACCT_ID_NUMBER'];



        $selectPer = $this->_db->select()
            ->from(array('u' => 'T_PERIODIC'), array('PS_PERIODIC_ENDDATE', 'PS_PERIODIC', 'PS_PERIODIC_NEXTDATE'))
            ->where('UPPER(u.PS_PERIODIC)=' . $this->_db->quote((string)$detail[0]['PS_PERIODIC']))
            ->query()->fetchAll();

        $this->view->PS_PERIODIC_ENDDATE =  $efdate = Application_Helper_General::convertDate($selectPer[0]['PS_PERIODIC_ENDDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

        $tanggal = $selectPer[0]['PS_PERIODIC_NEXTDATE'];
        $this->view->PS_PERIODIC_NEXTDATE = $day = date('l', strtotime($tanggal));


        $USER_ID = $detail[0]['USER_ID'];

        $select = $this->_db->select()
            ->from(array('u' => 'M_USER_ACCT'), array('USER_ID', 'ACCT_NO', 'ACCT_STATUS', 'ACCT_DESC', 'ACCT_CITIZENSHIP', 'ACCT_RESIDENT', 'ACCT_CATEGORY'))
            ->where('UPPER(u.ACCT_NO)=' . $this->_db->quote((string)$source))
            ->where('UPPER(u.USER_ID)=' . $this->_db->quote((string)$USER_ID))
            ->query()->fetchAll();
        // if(!empty($select)){
        //     $this->view->ACBENEF_CITIZENSHIP  = $select[0]['ACCT_CITIZENSHIP'];
        //     $this->view->ACBENEF_NATION  = $select[0]['ACCT_RESIDENT'];
        //     $this->view->ACBENEF_CATEGORY  = $select[0]['ACCT_CATEGORY'];
        // }
        $this->view->paymentDate = $detail[0]['PS_EFDATE'];
        //      print_r($detail);
        //      print_r($select);die;

        $this->view->ACCT_DESC = $select[0]['ACCT_DESC'];
        $date_val   = date('Y-m-d');
        echo '<BR>';
        if ($detail[0]['PS_EFDATE'] == $date_val) {
            $todaytranfer = $this->language->_('Today Transfer');
            $this->view->paymentType = $todaytranfer;
        } else {
            $pendingfuturedate = $this->language->_('Future Date Transfer');
            $this->view->paymentType = $pendingfuturedate;
        }

        if ($detail[0]['PS_EFDATE'] == $date_val) {
            if ($detail[0]['TRA_STATUS'] == '1') {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Suspect, Please Contact Customer Care');
            } elseif ($detail[0]['TRA_STATUS'] == '3') {
                $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
            } else {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Failed');
            }
        } else { //pending future date
            $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
        }
    }

    public function printtrxvalasAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();
        $psnumber = $param['payReff'];
        $getPaymentDetail   = new paymentreport_Model_Paymentreport();
        $detail = $getPaymentDetail->getPaymentDetail($psnumber);
        //      echo "<pre>";
        //      print_r($detail);die;
        $this->view->PS_NUMBER      =   $paymentref = $detail[0]['PS_NUMBER'];
        $this->view->PS_STATUS      =   $paystatus = $detail[0]['PS_STATUS'];
        $this->view->SOURCE_ACCOUNT     =   $source = $detail[0]['SOURCE_ACCOUNT'];
        $this->view->PS_PERIODIC    =   $detail[0]['PS_PERIODIC'];
        $this->view->PS_CREATED     =   $created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_UPDATED     =   $updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_EFDATE      =   $efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
        $this->view->PS_TOTAL_AMOUNT = Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']);
        $this->view->PS_CCY = $detail[0]['PS_CCY'];
        $this->view->SOURCE_ACCOUNT_NAME = $detail[0]['SOURCE_ACCOUNT_NAME'];
        $this->view->SOURCE_ACCOUNT_TYPE = $detail[0]['SOURCE_ACCOUNT_TYPE'];
        $this->view->TRA_MESSAGE = $detail[0]['TRA_MESSAGE'];
        $this->view->BENEFICIARY_ACCOUNT_NAME = $detail[0]['BENEFICIARY_ACCOUNT_NAME'];
        $this->view->BENEFICIARY_ACCOUNT = $detail[0]['BENEFICIARY_ACCOUNT'];
        $this->view->BENEFICIARY_ACCOUNT_CCY = $detail[0]['BENEFICIARY_ACCOUNT_CCY'];
        $this->view->TRA_AMOUNT = Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']);
        $this->view->BENEFICIARY_EMAIL = Application_Helper_General::displayMoney($detail[0]['BENEFICIARY_EMAIL']);
        $this->view->PS_TYPE = $type = $detail[0]['PS_TYPE'];
        $this->view->PS_SUBJECT = $paysubject = $detail[0]['PS_SUBJECT'];


        //      $this->view->BENEFICIARY_RESIDENT  = $detail[0]['BENEFICIARY_RESIDENT'];
        //      $this->view->BENEFICIARY_CITIZENSHIP  = $detail[0]['BENEFICIARY_CITIZENSHIP'];

        if (!empty($detail[0]['ACCT_CITIZENSHIP'])) {
            if ($detail[0]['ACCT_CITIZENSHIP'] == 'W') {
                $detail[0]['ACCT_CITIZENSHIP'] = 'WNI';
            } else {
                $detail[0]['ACCT_CITIZENSHIP'] = 'WNA';
            }
        } else {
            $detail[0]['ACCT_CITIZENSHIP'] = '-';
        }

        if (!empty($detail[0]['ACCT_RESIDENT'])) {
            if ($detail[0]['ACCT_RESIDENT'] == 'R') {
                $detail[0]['ACCT_RESIDENT'] = 'Resident';
            } else {
                $detail[0]['ACCT_RESIDENT'] = 'Non Resident';
            }
        } else {
            $detail[0]['ACCT_RESIDENT'] = '-';
        }
        //              echo "<pre>";
        //              print_r($detail);die;

        $this->view->ACCTSRC_RESIDENT  = $detail[0]['ACCT_RESIDENT'];
        $this->view->ACCTSRC_CITIZENSHIP  = $detail[0]['ACCT_CITIZENSHIP'];

        $this->view->ACCTSRC_CATEGORY  = $detail[0]['ACCT_CATEGORY'];
        $this->view->ACCTSRC_ID_TYPE  = $detail[0]['ACCT_ID_TYPE'];
        $this->view->ACCTSRC_ID_NUMBER  = $detail[0]['ACCT_ID_NUMBER'];
        $this->view->CURR_CODE  = 'USD';

        $kurs = $detail[0]['EQUIVALENT_AMOUNT_IDR'] / $detail[0]['TRA_AMOUNT'];
        $this->view->kurs  = Application_Helper_General::displayMoney($kurs);
        $this->view->TRA_MESSAGE  = $detail[0]['TRA_MESSAGE'];

        $this->view->eqamount  = Application_Helper_General::displayMoney($detail[0]['EQUIVALENT_AMOUNT_IDR']);
        $this->view->LLD_TRANSACTION_PURPOSE  = $detail[0]['LLD_TRANSACTION_PURPOSE'];

        if (!empty($detail[0]['BENEFICIARY_CITIZENSHIP'])) {
            if ($detail[0]['BENEFICIARY_CITIZENSHIP'] == 'W') {
                $detail[0]['BENEFICIARY_CITIZENSHIP'] = 'WNI';
            } else {
                $detail[0]['BENEFICIARY_CITIZENSHIP'] = 'WNA';
            }
        } else {
            $detail[0]['BENEFICIARY_CITIZENSHIP'] = '-';
        }

        if (!empty($detail[0]['BENEFICIARY_RESIDENT'])) {
            if ($detail[0]['BENEFICIARY_RESIDENT'] == 'R') {
                $detail[0]['BENEFICIARY_RESIDENT'] = 'Resident';
            } else {
                $detail[0]['BENEFICIARY_RESIDENT'] = 'Non Resident';
            }
        } else {
            $detail[0]['BENEFICIARY_RESIDENT'] = '-';
        }
        $settings = new Application_Settings();

        $lldCategoryArr = $settings->getLLDDOMCategory();

        $this->view->ACBENEF_CITIZENSHIP  = $detail[0]['BENEFICIARY_CITIZENSHIP'];
        $this->view->ACBENEF_NATION  = $detail[0]['BENEFICIARY_RESIDENT'];
        $this->view->ACBENEF_IDENTY  = $detail[0]['BENEFICIARY_ID_TYPE'];
        $this->view->ACBENEF_IDENTYNUM  = $detail[0]['BENEFICIARY_ID_NUMBER'];
        $this->view->ACBENEF_CATEGORY  = $lldCategoryArr[$detail[0]['BENEFICIARY_CATEGORY']];


        if ($detail[0]['TRA_AMOUNT'] >= 10000) {
            $LLD_HI = '';
        } else {
            $LLD_HI = 'none';
        }
        $settings = new Application_Settings();
        $lldIdenticalArr    = $settings->getLLDDOMIdentical();
        $lldRelationshipArr = $settings->getLLDDOMRelationship();

        $this->view->LLD_HI  = $LLD_HI;
        $this->view->LLD_IDENTITY  = $detail[0]['LLD_IDENTITY'];
        $this->view->LLD_TRANSACTOR_RELATIONSHIP  = $detail[0]['LLD_TRANSACTOR_RELATIONSHIP'];
        $this->view->TransactorArr  = $lldRelationshipArr;
        $this->view->IdentyArr  = $lldIdenticalArr;








        $selectPer = $this->_db->select()
            ->from(array('u' => 'T_PERIODIC'), array('PS_PERIODIC_ENDDATE', 'PS_PERIODIC', 'PS_PERIODIC_NEXTDATE'))
            ->where('UPPER(u.PS_PERIODIC)=' . $this->_db->quote((string)$detail[0]['PS_PERIODIC']))
            ->query()->fetchAll();

        $this->view->PS_PERIODIC_ENDDATE =  $efdate = Application_Helper_General::convertDate($selectPer[0]['PS_PERIODIC_ENDDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

        $tanggal = $selectPer[0]['PS_PERIODIC_NEXTDATE'];
        $this->view->PS_PERIODIC_NEXTDATE = $day = date('l', strtotime($tanggal));


        $USER_ID = $detail[0]['USER_ID'];

        $select = $this->_db->select()
            ->from(array('u' => 'M_USER_ACCT'), array('USER_ID', 'ACCT_NO', 'ACCT_STATUS', 'ACCT_DESC', 'ACCT_CITIZENSHIP', 'ACCT_RESIDENT', 'ACCT_CATEGORY'))
            ->where('UPPER(u.ACCT_NO)=' . $this->_db->quote((string)$source))
            ->where('UPPER(u.USER_ID)=' . $this->_db->quote((string)$USER_ID))
            ->query()->fetchAll();
        //      if(!empty($select)){
        //      $this->view->ACBENEF_CITIZENSHIP  = $select[0]['ACCT_CITIZENSHIP'];
        //      $this->view->ACBENEF_NATION  = $select[0]['ACCT_RESIDENT'];
        //      $this->view->ACBENEF_CATEGORY  = $select[0]['ACCT_CATEGORY'];
        //      }
        $this->view->paymentDate = $detail[0]['PS_EFDATE'];
        //      print_r($detail);
        //      print_r($select);die;

        $this->view->ACCT_DESC = $select[0]['ACCT_DESC'];
        $date_val   = date('Y-m-d');
        echo '<BR>';
        if ($detail[0]['PS_EFDATE'] == $date_val) {
            $todaytranfer = $this->language->_('Today Transfer');
            $this->view->paymentType = $todaytranfer;
        } else {
            $pendingfuturedate = $this->language->_('Future Date Transfer');
            $this->view->paymentType = $pendingfuturedate;
        }

        if ($detail[0]['PS_EFDATE'] == $date_val) {
            if ($detail[0]['TRA_STATUS'] == '1') {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Suspect, Please Contact Customer Care');
            } elseif ($detail[0]['TRA_STATUS'] == '3') {
                $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
            } else {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Failed');
            }
        } else { //pending future date
            $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
        }
    }

    public function printtrxxAction()
    {
        $this->_helper->layout()->disableLayout();
        $param = $this->_request->getParams();

        $psnumber = $param['payReff'];
        $getPaymentDetail   = new paymentreport_Model_Paymentreport();
        $detail = $getPaymentDetail->getPaymentDetail($psnumber);

        $this->view->per            = $param['per'];
        $this->view->msgCutBi       = $param['msgCutBi'];
        $this->view->msgHoliday     = $param['msgHoliday'];
        $this->view->infoWarning    = $param['infoWarning'];
        $this->view->PS_NUMBER      =   $paymentref = $detail[0]['PS_NUMBER'];
        $this->view->PS_STATUS      =   $paystatus = $detail[0]['PS_STATUS'];
        $this->view->PS_PERIODIC    =   $detail[0]['PS_PERIODIC'];
        $this->view->SOURCE_ACCOUNT =   $source = $detail[0]['SOURCE_ACCOUNT'];
        $this->view->PS_CREATED     =   $created = Application_Helper_General::convertDate($detail[0]['PS_CREATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_UPDATED     =   $updated = Application_Helper_General::convertDate($detail[0]['PS_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $this->view->PS_EFDATE      =   $efdate = Application_Helper_General::convertDate($detail[0]['PS_EFDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
        $this->view->PS_TOTAL_AMOUNT = Application_Helper_General::displayMoney($detail[0]['PS_TOTAL_AMOUNT']);
        $this->view->PS_CCY = $detail[0]['PS_CCY'];
        $this->view->SOURCE_ACCOUNT_NAME = $detail[0]['SOURCE_ACCOUNT_NAME'];
        $this->view->SOURCE_ACCOUNT_TYPE = $detail[0]['SOURCE_ACCOUNT_TYPE'];
        $this->view->TRA_MESSAGE = $detail[0]['TRA_MESSAGE'];
        $this->view->BENEFICIARY_ACCOUNT_NAME = $detail[0]['BENEFICIARY_ACCOUNT_NAME'];
        $this->view->BENEFICIARY_ACCOUNT = $detail[0]['BENEFICIARY_ACCOUNT'];
        $this->view->BENEFICIARY_ACCOUNT_CCY = $detail[0]['BENEFICIARY_ACCOUNT_CCY'];

        $this->view->BENEFICIARY_ID_NUMBER = $detail[0]['BENEFICIARY_ID_NUMBER'];
        $this->view->BENEFICIARY_BANK_NAME = $detail[0]['BENEFICIARY_BANK_NAME'];

        $selectPer = $this->_db->select()
            ->from(array('u' => 'T_PERIODIC'), array('PS_PERIODIC_ENDDATE', 'PS_PERIODIC', 'PS_PERIODIC_NEXTDATE'))
            ->where('UPPER(u.PS_PERIODIC)=' . $this->_db->quote((string)$detail[0]['PS_PERIODIC']))
            ->query()->fetchAll();

        $this->view->PS_PERIODIC_ENDDATE =  $efdate = Application_Helper_General::convertDate($selectPer[0]['PS_PERIODIC_ENDDATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

        $tanggal = $selectPer[0]['PS_PERIODIC_NEXTDATE'];
        $this->view->PS_PERIODIC_NEXTDATE = $day = date('l', strtotime($tanggal));

        $CITY_CODE = $detail[0]['BENEFICIARY_CITY_CODE'];
        $modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();
        $CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
        $arr                    = $modelCity->getCityCode($CITY_CODEGet);

        // 9. Create LLD string
        $LLD_CATEGORY = $detail[0]['BENEFICIARY_CATEGORY'];
        $LLD_BENEIDENTIF = $detail[0]['BENEFICIARY_ID_TYPE'];
        $settings           = new Application_Settings();
        $LLD_array          = array();
        $LLD_DESC_arrayCat  = array();
        $lldTypeArr         = $settings->getLLDDOMType();

        if (!empty($LLD_CATEGORY)) {
            $lldCategoryArr     = $settings->getLLDDOMCategory();
            $LLD_array["CT"]    = $LLD_CATEGORY;
            $LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
        }

        if (!empty($LLD_BENEIDENTIF)) {
            $lldBeneIdentifArr      = $settings->getLLDDOMBeneIdentification();
            $LLD_array["CT"]    = $LLD_BENEIDENTIF;
            $LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$LLD_BENEIDENTIF];
        }

        $this->view->BENEFICIARY_CITY_CODE = $arr[0]['CITY_NAME'];
        $this->view->BENEFICIARY_CATEGORY = $LLD_CATEGORY_POST;
        $this->view->BENEFICIARY_ID_TYPE = $LLD_BENEIDENTIF_POST;

        $this->view->BENEFICIARY_CITIZENSHIP = $detail[0]['BENEFICIARY_CITIZENSHIP'];
        $this->view->BENEFICIARY_RESIDENT = $detail[0]['BENEFICIARY_RESIDENT'];
        $this->view->BENEFICIARY_ADDRESS = $detail[0]['BENEFICIARY_ADDRESS'];
        $this->view->TRANSFER_TYPE = $detail[0]['TRANSFER_TYPE'];

        $this->view->TRA_AMOUNT = Application_Helper_General::displayMoney($detail[0]['TRA_AMOUNT']);
        $this->view->BENEFICIARY_EMAIL = Application_Helper_General::displayMoney($detail[0]['BENEFICIARY_EMAIL']);
        $this->view->PS_TYPE = $type = $detail[0]['PS_TYPE'];
        $this->view->PS_SUBJECT = $paysubject = $detail[0]['PS_SUBJECT'];


        $USER_ID = $detail[0]['USER_ID'];

        $select = $this->_db->select()
            ->from(array('u' => 'M_USER_ACCT'), array('USER_ID', 'ACCT_NO', 'ACCT_STATUS', 'ACCT_DESC'))
            ->where('UPPER(u.ACCT_NO)=' . $this->_db->quote((string)$source))
            ->where('UPPER(u.USER_ID)=' . $this->_db->quote((string)$USER_ID))
            ->query()->fetchAll();

        $this->view->ACCT_DESC = $select[0]['ACCT_DESC'];
        $date_val   = date('Y-m-d');
        echo '<BR>';
        if ($detail[0]['PS_EFDATE'] == $date_val) {
            $todaytranfer = $this->language->_('Today Transfer');
            $this->view->paymentType = $todaytranfer;
        } else {
            $pendingfuturedate = $this->language->_('Future Date Transfer');
            $this->view->paymentType = $pendingfuturedate;
        }

        if ($detail[0]['PS_EFDATE'] == $date_val) {
            if ($detail[0]['TRA_STATUS'] == '1') {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Suspect, Please Contact Customer Care');
            } elseif ($detail[0]['TRA_STATUS'] == '3') {
                $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
            } else {
                $this->view->messageSuccess = $this->language->_('Your Transaction Is Failed');
            }
        } else { //pending future date
            $this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
        }
    }

    public function csvAction()
    {
        $param = $this->_request->getParams();

        $header    = array();
        $data    = array();
        $i        = 0;

        if (is_array($param['data_header']) && count($param['data_header'])) {
            foreach ($param['data_header'] as $key => $val) {
                $header[] = $param['data_header'][$key]['label'];
            }
        }

        foreach ($param['data_content'] as $row) {
            foreach ($param['data_header'] as $key => $val) {
                $data[$i][$param['data_header'][$key]['field']]    = (string) $row[$param['data_header'][$key]['field']];
            }
            $i++;
        }

        $this->_helper->download->csv(
            $header,
            $data,
            null,
            $param['data_caption']
        );
    }

    public function pdfAction()
    {
        require_once 'SGO/Helper/mpdf/mpdf.php';

        $params = $this->_request->getParams();
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->language = Zend_Registry::get('language');

        $html = '<html>
            <head>
                <style>
                    *{
                        font-family: Calibri;
                        font-size: 12px;
                    }
                    table{
                        border-collapse: collapse;
                        width: 100%;
                    }
                    h1{
                        font-size: 20px;
                        text-align: center;
                    }
                </style>
            </head>
            <body>
                <h1>' . $this->language->_($params['data_caption']) . '</h1>
                <table border="1">
                    <thead>
                        <tr>';

        foreach ($params['data_header'] as $key) {
            $html .= '<th>' . $this->language->_($key['label']) . '</th>';
        }

        $html .= '</tr>
                    </thead>
                    <tbody>';

        foreach ($params['data_content'] as $row) {
            $html .= '<tr>';

            foreach ($params['data_header'] as $key) {
                // For joined table
                $field = explode(".", $key['field']);
                $field = (isset($field[1])) ? $field[1] : $field[0];

                switch ($key['format']) {
                    case 'changes_status':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getChangesStatus($row[$field]) . '</td>';
                        break;
                    case 'currency':
                        $html .= '<td align="right">' . SGO_Helper_GeneralFunction::displayCurrency($row[$field]) . '</td>';
                        break;
                    case 'date':
                        $data = empty($row[$field]) ? '-' : date('j-M-Y', strtotime($row[$field]));
                        $html .= '<td>' . $data . '</td>';
                        break;
                    case 'datetime':
                        $data = empty($row[$field]) ? '-' : date('j-M-Y H:i:s', strtotime($row[$field]));
                        $html .= '<td>' . $data . '</td>';
                        break;
                    case 'documentMatchingStatus':
                        if ($row['AMOUNT'] == $row['OUTSTANDING_AMOUNT'])
                            $status = SGO_Helper_GeneralFunction::language('Not Matched');
                        else if ($row['AMOUNT'] != $row['OUTSTANDING_AMOUNT'] && intval($row['OUTSTANDING_AMOUNT']) != 0)
                            $status = SGO_Helper_GeneralFunction::language('Few Matched');
                        else
                            $status = SGO_Helper_GeneralFunction::language('Matched');

                        $html .= '<td>' . $status . '</td>';
                        break;
                    case 'doc_status':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getDocStatus($row[$field]) . '</td>';
                        break;
                    case 'master_status':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getMasterStatus($row[$field]) . '</td>';
                        break;
                    case 'sms_status':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getSmsStatus($row[$field]) . '</td>';
                        break;
                    case 'sub_tx_status':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getSubTransactionStatus($row[$field]) . '</td>';
                        break;
                    case 'time':
                        $html .= '<td>' . date('H:i:s', strtotime($row[$field])) . '</td>';
                        break;
                    case 'tx_channel':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getTransactionChannel($row[$field]) . '</td>';
                        break;
                    case 'tx_status':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getTransactionStatus($row[$field]) . '</td>';
                        break;
                    case 'user_status':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getUserStatus($row[$field]) . '</td>';
                        break;
                    case 'has_status':
                        $html .= '<td>' . SGO_Helper_GeneralFunction::getMasterHasStatus($row[$field]) . '</td>';
                        break;
                    default:
                        $show = (isset($key['language'])) ? $this->language->_($row[$field]) : $row[$field];
                        $html .= '<td>' . $show . '</td>';
                        break;
                }
            }
            $html .= '</tr>';
        }
        $html .= '</tbody>
                </table>
            </body>
        </html>';

        // Convert to PDF
        $filename = $params['data_caption'] . '_' . Zend_Registry::get('custLogin') . '_' . date('d-M-Y') . '.pdf';
        $path = 'temp/' . $filename;

        if (file_exists($path)) {
            unlink($path);
        }

        $mpdf = new mPDF('', 'A4', 0);
        $mpdf->WriteHTML($html);
        $mpdf->Output($path, 'D');

        chmod($path, 0777);

        $filesize = filesize($path);

        $chunksize = 4096;
        if ($filesize > $chunksize) {
            $srcStream = fopen($path, 'rb');
            $dstStream = fopen('php://output', 'wb');

            $offset = 0;
            while (!feof($srcStream)) {
                $offset += stream_copy_to_stream($srcStream, $dstStream, $chunksize, $offset);
            }

            fclose($dstStream);
            fclose($srcStream);
        } else {
            // stream_copy_to_stream behaves() strange when filesize > chunksize.
            // Seems to never hit the EOF.
            // On the other handside file_get_contents() is not scalable. 
            // Therefore we only use file_get_contents() on small files.
            echo file_get_contents($path);
        }
        //$this->_redirect($path);  
    }

    public function excelAction()
    {
        require_once 'PHPExcel.php';

        $params['data_caption'] = 'Testing Only';
        Zend_Registry::set('custLogin', 'abcde');

        $params['data_header']  = array(
            0 => array(
                'label' => 'Nama',
                'field' => 'name',
                'format' => '',
            ),
            1 => array(
                'label' => 'Nomor Rekening',
                'field' => 'acct',
                'format' => '',
            ),
            2 => array(
                'label' => 'Status',
                'field' => 'status',
                'format' => 'master_status',
            ),
        );

        $params['data_content'] = array(
            0 => array(
                'name' => 'Andi 1',
                'acct' => '00001',
                'status' => 'A',
            ),
            1 => array(
                'name' => 'Andi 2',
                'acct' => '00029',
                'status' => 'I',
            ),
        );

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("SGO")
            ->setLastModifiedBy("SGO")
            ->setTitle($this->language->_($params['data_caption']))
            ->setSubject($this->language->_($params['data_caption']))
            ->setDescription($this->language->_($params['data_caption']));



        $data_letters = array();
        $data_letters[0] = 'A';
        $data_letters[1] = 'B';
        $data_letters[2] = 'C';
        $data_letters[3] = 'D';
        $data_letters[4] = 'E';
        $data_letters[5] = 'F';
        $data_letters[6] = 'G';
        $data_letters[7] = 'H';
        $data_letters[8] = 'I';
        $data_letters[9] = 'J';
        $data_letters[10] = 'K';
        $data_letters[11] = 'L';
        $data_letters[12] = 'M';
        $data_letters[13] = 'N';
        $data_letters[14] = 'O';
        $data_letters[15] = 'P';
        $data_letters[16] = 'Q';
        $data_letters[17] = 'R';
        $data_letters[18] = 'S';

        $key_idx_letter = 1;
        foreach ($params['data_header'] as $key_idx => $key) {

            $letter = $data_letters[$key_idx];

            $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, $key['label'], PHPExcel_Cell_DataType::TYPE_STRING);
        }

        foreach ($params['data_content'] as $key_content => $row) {

            $key_idx_letter = $key_content + 2;



            foreach ($params['data_header'] as $key_idx => $key) {
                $letter = $data_letters[$key_idx];
                // For joined table
                $field = explode(".", $key['field']);
                $field = (isset($field[1])) ? $field[1] : $field[0];

                switch ($key['format']) {
                    case 'changes_status':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getChangesStatus($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);
                        break;
                    case 'currency':
                        //$html .= '<td align="right">'.SGO_Helper_GeneralFunction::displayCurrency($row[$field]).'</td>';
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::displayCurrency($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);
                        break;
                    case 'date':
                        $data = empty($row[$field]) ? '-' : date('j-M-Y', strtotime($row[$field]));
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, $data, PHPExcel_Cell_DataType::TYPE_STRING);

                        break;
                    case 'datetime':
                        $data = empty($row[$field]) ? '-' : date('j-M-Y H:i:s', strtotime($row[$field]));
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, $data, PHPExcel_Cell_DataType::TYPE_STRING);
                        break;
                    case 'documentMatchingStatus':
                        if ($row['AMOUNT'] == $row['OUTSTANDING_AMOUNT'])
                            $status = SGO_Helper_GeneralFunction::language('Not Matched');
                        else if ($row['AMOUNT'] != $row['OUTSTANDING_AMOUNT'] && intval($row['OUTSTANDING_AMOUNT']) != 0)
                            $status = SGO_Helper_GeneralFunction::language('Few Matched');
                        else
                            $status = SGO_Helper_GeneralFunction::language('Matched');

                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, $status, PHPExcel_Cell_DataType::TYPE_STRING);
                        break;
                    case 'doc_status':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getDocStatus($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);
                        break;
                    case 'master_status':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getMasterStatus($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);
                        break;
                    case 'sms_status':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getSmsStatus($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);
                        break;
                    case 'sub_tx_status':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getSubTransactionStatus($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);

                        break;
                    case 'time':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, date('H:i:s', strtotime($row[$field])), PHPExcel_Cell_DataType::TYPE_STRING);

                        break;
                    case 'tx_channel':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getTransactionChannel($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);

                        break;
                    case 'tx_status':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getTransactionStatus($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);

                        break;
                    case 'user_status':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getUserStatus($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);

                        break;
                    case 'has_status':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, SGO_Helper_GeneralFunction::getMasterHasStatus($row[$field]), PHPExcel_Cell_DataType::TYPE_STRING);

                        break;
                    default:
                        $show = (isset($key['language'])) ? $this->language->_($row[$field]) : $row[$field];
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter . $key_idx_letter, $show, PHPExcel_Cell_DataType::TYPE_STRING);

                        break;
                }
            }
        }




        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle($this->language->_($params['data_caption']));


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('D2')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $filename = $params['data_caption'] . '_' . Zend_Registry::get('custLogin') . '_' . date('d-M-Y') . '.xls';
        $path = 'temp/' . $filename;

        if (file_exists($path)) {
            unlink($path);
        }

        // Redirect output to a client�s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $params['data_caption'] . '_' . Zend_Registry::get('custLogin') . '_' . date('d-M-Y') . '.xls' . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save($path);
        echo file_get_contents($path);
        exit(0);
    }
}
