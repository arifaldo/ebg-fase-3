<?php

/**
 * Tempcompany model
 * 
 * @author 
 * @version
 */

//NOTE : Refactoring possibility: for all Changemanagement_Model_TempX
//just create an abstract class having all this class methods and properties (some methods might have to be set as abstract and detailed in descendant class), 
//define master table name (M_COMPANY or M_USER, etc), master table primary key (COMPANY_ID, USER_ID) as class properties and their values set in descendant class 

require_once 'Zend/Db/Table/Abstract.php';

class Changemanagement_Model_Tempcompany extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'TEMP_COMPANY';
	protected $_primary = 'TEMP_ID';
		
	protected $_referenceMap = array('GLOBALCHANGES' => array(
											            'columns'           => 'CHANGES_ID',
											            'refTableClass'     => 'Changemanagement_Model_Globalchanges',
											            'refColumns'        => 'CHANGES_ID'
        												)
        							 );
    protected $dbObj;

    protected $_excludeFromMaster = array('TEMP_ID','CHANGES_ID'); //fields to exclude when executing insert/update query in master table
	protected $_id;
	protected $_row;
	
    
	/**
     * Constructor
     * @param  String $id Optional Change Id
     */
    public function __construct($id) {
    	parent::__construct();
    	$this->dbObj = $this->getDefaultAdapter();
    	
    	if(isset($id)) 
			$this->setId($id);
	}    							 
	
	/**
     * set Id for this object
     * @param  String $id Change Id
     * @return boolean 
     */
	public function setId($id) {
		
		if(!isset($id))
			throw new Exception('Id has to be set');

		$row = $this->dbObj
    				->select()
					->from($this->_name)
    				->where('CHANGES_ID = ?', $id)
    				->query()
					->fetch(Zend_Db::FETCH_ASSOC);
		if($row) {
    		$this->_id = $id;
			$this->_row = $row;
			return true;
    	}
    	else 
    		throw new Exception('Record with given id not found');	
	}
    
    /**
     * update changes to master table (in the case of an approved edit change)
     *
     * @param  String $masterId Primary Key value of master table to update
     * @return boolean indicating operation success/failure
     */
    public function updateEdit($masterId) {
    	
    	if(!isset($this->_row))
    		throw new Exception('Cannot update. row is not set');
				   
		$updateArr = array();
    	foreach($this->_row as $key=>$val) {
    		if(!in_array($key,$this->_excludeFromMaster))
    			$updateArr[$key] = $val;
    	}	
    	//$extra_fields = array('COMP_UPDATED' => new Zend_Db_Expr("TO_CHAR(SYSDATE, 'DD-MON-YYYY')"));
    	//$updateArr = array_merge($updateArr,$extra_fields);
    	$row = $this->dbObj->update('M_COMPANY',$updateArr,$this->dbObj->quoteInto('COMP_ID = ?',$masterId));
    	
    	return (boolean)$row;
    }
    
    /**
     * update changes to master table (in the case of an approved add change)
     *
     * @return boolean indicating operation success/failure
     */
    public function updateInsert() {
    	
    	if(!isset($this->_row))
    		throw new Exception('Cannot update. row is not set');
    	
    	$insertArr = array();
    	foreach($this->_row as $key=>$val) {
    		if(!in_array($key,$this->_excludeFromMaster))
    			$insertArr[$key] = $val;
    	}
    	//$extra_fields = array('COMP_UPDATED' => new Zend_Db_Expr("TO_CHAR(SYSDATE, 'DD-MON-YYYY')"));
    	//$updateArr = array_merge($updateArr,$extra_fields);
    	$ins = $this->dbObj->insert('M_COMPANY',$insertArr);
    	
    	return (boolean)$ins;
    }
    
    /**
     * update changes to master table (in the case of an approved delete change)
     *
     * @param  String $masterId Primary Key value of master table to update
     * @return boolean indicating operation success/failure
     */
    public function updateDelete($masterId) {
    	
    	if(!isset($this->_row))
    		throw new Exception('Cannot update. row is not set');
    	
    	//TODO :
    	//delete all master records related to this company from database	
    		
    	$updateArr = array('COMP_STATUS' => 'D');	
    	//$extra_fields = array('COMP_UPDATED' => new Zend_Db_Expr("TO_CHAR(SYSDATE, 'DD-MON-YYYY')"));
    	//$updateArr = array_merge($updateArr,$extra_fields);
    	$row = $this->dbObj->update('M_COMPANY',$updateArr,$this->dbObj->quoteInto('COMP_ID = ?',$masterId));
    	
    	return (boolean)$row;    							 
	}
	
	/**
     * delete change data from temp table (for use in the case of rejected/approved change)
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteChanges() {
		if(!isset($this->_id))
    		throw new Exception('Cannot update. id is not set');

    	$row = $this->dbObj->delete($this->_name,$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_id));
    	return (boolean)$row;		
	}
	
	/**
     * get change details (as Add changes)
     *
     * @return array details array
     */
	public function getAddDetail() {
		if(!isset($this->_row))
    		throw new Exception('Cannot get detail. row is not set');
		
    	//create mapping of fields for display and its label
    	$detail = array('display_type' => 'single', //value to determine how to display fields
    					'label' => array('Company Id', 'Company Name', 'Address',
    									 'Zip', 'City', 'State', 'Country', 'Email',
    									 'Phone', 'Phone Ext', 'Contact Person', 'Website',
    									 'Fax', 'CIF', 'Prefix', 'Description'),
    					'data' => array($this->_row['COMP_ID'], $this->_row['COMP_NAME'],
    									$this->_row['COMP_ADDRESS'], $this->_row['COMP_ZIP'],
    									$this->_row['COMP_CITY'], $this->_row['COMP_STATE'],
    									$this->_row['COMP_COUNTRY'], $this->_row['COMP_EMAIL'],
    									$this->_row['COMP_PHONE'], $this->_row['COMP_EXT'],
    									$this->_row['COMP_CONTACTPERSON'], $this->_row['COMP_WEBSITE'],
    									$this->_row['COMP_FAX'], $this->_row['COMP_CIF'],
    									$this->_row['COMP_PREFIX'], $this->_row['COMP_DESC']
    									)
    					);
    		
    	return $detail;		
	}
	
	/**
     * get change details (as Edit changes)
     * @param  String $masterId Primary Key value of master table
     * @return array details array
     */
	public function getEditDetail($masterId) {
		if(!isset($this->_row))
    		throw new Exception('Cannot get detail. row is not set');
		
    	//get values from Master table
    	$masterRow = $this->dbObj->select()
    							 ->from('M_COMPANY')
    							 ->where($this->dbObj->quoteInto('COMP_ID = ?',$masterId))
    							 ->query()
    							 ->fetch(Zend_Db::FETCH_ASSOC);
		if(!$masterRow)
			throw new Exception('Cannot get detail. Master record not found');
    							 
    	//create mapping of fields for display and its label
    	$detail = array('display_type' => 'comparison', //value to determine how to display fields
    					'label' => array('Company Id', 'Company Name', 'Address',
    									 'Zip', 'City', 'State', 'Country', 'Email',
    									 'Phone', 'Phone Ext', 'Contact Person', 'Website',
    									 'Fax', 'CIF', 'Prefix', 'Description','Internal Approval','Internal Release','Bank Approval'),
    					'data' =>array('suggested' => array($this->_row['COMP_ID'], $this->_row['COMP_NAME'], 
    										 				$this->_row['COMP_ADDRESS'], $this->_row['COMP_ZIP'],
    										 				$this->_row['COMP_CITY'], $this->_row['COMP_STATE'],
    										 				$this->_row['COMP_COUNTRY'], $this->_row['COMP_EMAIL'],
    										 				$this->_row['COMP_PHONE'], $this->_row['COMP_EXT'],
    										 				$this->_row['COMP_CONTACTPERSON'], $this->_row['COMP_WEBSITE'],
    										 				$this->_row['COMP_FAX'], $this->_row['COMP_CIF'],
    										 				$this->_row['COMP_PREFIX'], $this->_row['COMP_DESC'],
    										 				$this->_row['SCF_WA'],$this->_row['SCF_WR'],$this->_row['SCF_WB'],
    													   ),
				    				   'current' => array($masterRow['COMP_ID'], $masterRow['COMP_NAME'], 
				    									  $masterRow['COMP_ADDRESS'], $masterRow['COMP_ZIP'],
				    									  $masterRow['COMP_CITY'], $masterRow['COMP_STATE'],
				    									  $masterRow['COMP_COUNTRY'], $masterRow['COMP_EMAIL'],
				    									  $masterRow['COMP_PHONE'], $masterRow['COMP_EXT'],
				    									  $masterRow['COMP_CONTACTPERSON'], $masterRow['COMP_WEBSITE'],
				    									  $masterRow['COMP_FAX'], $masterRow['COMP_CIF'],
				    									  $masterRow['COMP_PREFIX'], $masterRow['COMP_DESC'],
				    									  $masterRow['SCF_WA'],$masterRow['SCF_WR'],$masterRow['SCF_WB'],
				    									 )
				    					)
    					);
    	return $detail;		
	}
	
	/**
     * get change details (as Delete changes)
     * @param  String $masterId Primary Key value of master table
     * @return array details array
     */
	public function getDeleteDetail($masterId) {
		
		//get values from Master table
    	$masterRow = $this->dbObj->select()
    							 ->from('M_COMPANY')
    							 ->where($this->dbObj->quoteInto('COMP_ID = ?',$masterId))
    							 ->query()
    							 ->fetch(Zend_Db::FETCH_ASSOC);
		if(!$masterRow)
			throw new Exception('Cannot get detail. Master record not found');

		//create mapping of fields for display and its label
    	$detail = array('display_type' => 'single', //value to determine how to display fields
    					'label' => array('Company Id', 'Company Name', 'Address',
    									 'Zip', 'City', 'State', 'Country', 'Email',
    									 'Phone', 'Phone Ext', 'Contact Person', 'Website',
    									 'Fax', 'CIF', 'Prefix', 'Description'),
    					'data' => array($masterRow['COMP_ID'], $masterRow['COMP_NAME'],
    									$masterRow['COMP_ADDRESS'], $masterRow['COMP_ZIP'],
    									$masterRow['COMP_CITY'], $masterRow['COMP_STATE'],
    									$masterRow['COMP_COUNTRY'], $masterRow['COMP_EMAIL'],
    									$masterRow['COMP_PHONE'], $masterRow['COMP_EXT'],
    									$masterRow['COMP_CONTACTPERSON'], $masterRow['COMP_WEBSITE'],
    									$masterRow['COMP_FAX'], $masterRow['COMP_CIF'],
    									$masterRow['COMP_PREFIX'], $masterRow['COMP_DESC']
    									)
    					);
    	return $detail;					
	}
}