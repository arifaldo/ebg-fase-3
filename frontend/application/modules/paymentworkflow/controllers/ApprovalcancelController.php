<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/SystemBalance.php';

class paymentworkflow_ApprovalcancelController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	protected $_paymentRef;
	protected $_tableMst;
	protected $_hasPriviApprovePayment 	= false;
	protected $_hasPriviApproveBene 	= false;
	protected $_hasPriviRepairPayment 	= false;
	protected $_hasPriviRejectPayment 	= false;
	protected $_controllerList 	= "waitingforapprovalfuture";

	protected $_bankName;

	public function initController(){
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
	}

	public function indexAction()
	{

		$filter 			= new Application_Filtering();
		$PS_NUMBER 			= $filter->filter($this->_getParam('payReff')	, "PS_NUMBER");
		$pdf 				= $filter->filter($this->_getParam('pdf')		, "BUTTON");
		$process 			= $filter->filter($this->_getParam('process')	, "BUTTON");
		// $approve 			= ($process == "approve") 	? true: false;
		$approve 			= $filter->filter($this->_getParam('approve')		, "BUTTON");
		$repair 			= ($process == "repair") 	? true: false;
		$reject 			= ($process == "reject") 	? true: false;
		$this->_paymentRef 	= $PS_NUMBER;
		
		$sessionNamespace = new Zend_Session_Namespace('URL_CP_WA');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ? 
									   $sessionNamespace->URL : '/'.$this->view->modulename.'/'.$this->_controllerList.'/index';
		
		// check privi...
		$this->_hasPriviApprovePayment 	= $this->view->hasPrivilege('PAPV');
		$this->_hasPriviApproveBene 	= $this->view->hasPrivilege('BAPA');
		$this->_hasPriviRepairPayment 	= $this->view->hasPrivilege('PRRP');
		//$this->_hasPriviRepairPayment 	= $this->view->hasPrivilege('PRPP');
		$this->_hasPriviRejectPayment 	= $this->view->hasPrivilege('PRJT');
		
		$paramPayment = array("WA" 				=> true,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
		
		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);
		// echo $select;			
		$pslip = $this->_db->fetchRow($select);
		
		$selectTrx	= $this->_db->select()
		->from(	array(	'TT' => 'T_TRANSACTION'),
				array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
						'TRA_REFNO'				=> 'TT.TRA_REFNO',
						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'Mayapada (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'Mayapada (Buy)'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						// 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN B.BENEFICIARY_ISAPPROVE = '0' THEN 'Waiting Approval'
																				 WHEN B.BENEFICIARY_ISAPPROVE = '1' THEN 'Approved'
																				 ELSE 'N/A'
																			END"),
				)
		)
		->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
		->where('TT.PS_NUMBER = ?', $PS_NUMBER);
			// echo $selectTrx;die;
									// print_r($select->query());die;
		$pslipTrxData = $this->_db->fetchAll($selectTrx);
// 		print_r($pslipTrxData);die;
		if (!empty($pslip))	
		{
			// Payment Status is not Waiting Approval
			if ($pslip["PS_STATUS"] != $this->_paymentstatus["code"]["requestcancelfuturedate"])
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error').": ".$this->language->_('Payment Status has changed').".");
				$this->_redirect('/'.$this->view->modulename.'/'.$this->_controllerList.'/index');
			}
		}
		else	// ps_number is invalid, or not belong to customer, or user don't have right to view this payment
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
			$this->_redirect('/'.$this->view->modulename.'/'.$this->_controllerList.'/index');
		}
		
		
		if($reject)
		{
			$this->_db->beginTransaction();
			
			// update PSLIP
			$data = array ('PS_STATUS' => '7');
			$where['PS_NUMBER = ?'] = $PS_NUMBER;
			$this->_db->update('T_PSLIP',$data,$where);
			

			$historyInsert = array(	'DATE_TIME' 		=> new Zend_Db_Expr("now()"),
											'PS_NUMBER' 		=> $PS_NUMBER,
											'CUST_ID' 			=> $this->_custIdLogin,
											'USER_LOGIN' 		=> $this->_userIdLogin,
											'HISTORY_STATUS' 	=> 19);

			$this->_db->insert('T_PSLIP_HISTORY',$historyInsert);

			$this->_db->commit();
			$this->setbackURL('/'.$this->_request->getModuleName().'/waitingforapprovalfuture/index/');
			$this->_redirect('/notification/success');
		}
		
				
		$filter->__destruct();
		unset($filter);

		$this->view->payType = $pslip['payType'];

		

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }
		
		// View Data
		$this->_tableMst[0]["label"] = $this->language->_('Payment Ref').'#';
		$this->_tableMst[1]["label"] = $this->language->_('Created Date');
		$this->_tableMst[2]["label"] = $this->language->_('Updated Date');
		$this->_tableMst[3]["label"] = $this->language->_('Payment Date');
		$this->_tableMst[4]["label"] = $this->language->_('Payment Subject');
		$this->_tableMst[5]["label"] = $this->language->_('Master Account');
		$this->_tableMst[6]["label"] = $this->language->_('Payment Type');
		$this->_tableMst[7]["label"] = $this->language->_('Source Bank Name');
		
		$this->_tableMst[0]["value"] = $PS_NUMBER;
		$this->_tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->_dateTimeDisplayFormat);
		$this->_tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->_dateTimeDisplayFormat);
		$this->_tableMst[3]["value"] = Application_Helper_General::convertDate($pslip['efdate'], $this->_dateViewFormat);
		$this->_tableMst[4]["value"] = $pslip['paySubj'];
		$this->_tableMst[5]["value"] = "";
		if(!empty($pslipTrxData) && ($pslipTrxData['0']['TRANSFER_TYPE'] == '8' || $pslipTrxData['0']['TRANSFER_TYPE'] == '7' ) ){
			
			$pslip['payType'] = $pslipTrxData['0']['TRANSFER_TYPE_disp'];
		}
		$this->_tableMst[6]["value"] = $pslip['payType'];

		if(empty($pslip['SOURCE_ACCT_BANK_CODE'])){
			$bankname = $this->_bankName;
		}
		else{
			$bankname = $bankNameArr[$pslip['SOURCE_ACCT_BANK_CODE']];
		}

		$this->_tableMst[7]["value"] = $bankname;

		//print_r($pslip);die;
		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && !$pdf)
		{	
			// download trx bulk file
			$downloadURL = $this->view->url(array('module'=>'display','controller'=>'viewdetail','action'=>'downloadtrx','csv'=>'1','payReff'=>$PS_NUMBER),null,true);
			
		}

		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && !$pdf )
		{
			// download trx bulk file
			$downloadURL = $this->view->url(array('module'=>'display','controller'=>'viewdetail','action'=>'downloadtrx','csv'=>'1','payReff'=>$PS_NUMBER),null,true);
			$this->_tableMst[7]["label"] = "View File";
			$this->_tableMst[7]["value"] = $this->view->formButton('download',$this->language->_('download'),array('class'=>'inputbtn', 'onclick'=>"window.location = '".$downloadURL."';"));
		}

// 		print_r($pslip["PS_TYPE"]);die;
// 						print_r($this->_paymenttype["code"]);die;
		// separate credit and debet view
		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"] ){
			$this->sweepin($pslip);
			
		}elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"] ){
			$this->sweepout($pslip);
		}elseif ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"] ){
			$this->debet($pslip);
		}else{
			$this->credit($pslip);
		}	
		// var_dump($approve);
		// var_dump($this->view->valError);die;
		// approve after validate payment	
		if($approve && $this->view->valError === false)
		{
			$this->_db->beginTransaction();
			
			// update PSLIP
			$data = array ('PS_STATUS' => '14');
			$where['PS_NUMBER = ?'] = $PS_NUMBER;
			$this->_db->update('T_PSLIP',$data,$where);

			$historyInsert = array(	'DATE_TIME' 		=> new Zend_Db_Expr("now()"),
											'PS_NUMBER' 		=> $PS_NUMBER,
											'CUST_ID' 			=> $this->_custIdLogin,
											'USER_LOGIN' 		=> $this->_userIdLogin,
											'HISTORY_STATUS' 	=> 14);

			$this->_db->insert('T_PSLIP_HISTORY',$historyInsert);

			
			$this->_db->commit();
			$this->setbackURL('/'.$this->_request->getModuleName().'/waitingforapprovalfuture/index/');
			$this->_redirect('/notification/success');		}	
		
		if ($pslip["PS_CATEGORY"] == "BULK PAYMENT")
		{
			$this->view->fields			 = array();
			$this->view->tableDtl 		 = array();
			$this->view->TITLE_DTL		 = "";
		}
		
		$this->view->pslip 			= $pslip;
		$this->view->PS_NUMBER 			= $PS_NUMBER;
		$this->view->tableMst 			= $this->_tableMst;
		$this->view->totalTrx 			= $pslip["numtrx"];

		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepin"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["sweepout"] ){
// 			print_r($pslip);die;

			$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from	(array(	'TTS' => 'T_PERIODIC'))
			->joinLeft	(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*') )
			->where('TTS.PS_PERIODIC = ?', $pslip['PERIODIC']);
//echo $select;
			$sweepdetail = $this->_db->fetchAll($select);
//die('here');
//print_r($sweepdetail);die;
//			if($pslip['BALANCE_TYPE']=='2'){
				$totalamount = 0;
				$temptotal = 0;
				foreach ($sweepdetail as $key => $value){
					$totalamount = $temptotal + $value['TRA_REMAIN'];
					$temptotal = $totalamount;
				}
// 				echo '<pre>';
 //				print_r($temptotal);die;
				$this->view->totalAmt 			= $temptotal;
//			}else{
//				$this->view->totalAmt 			= $pslip["amount"];
//			}
			
		}else{
			//print_r($pslip);die;
			if($pslip['PS_TYPE']=='3' && $pslip['acbenef_ccy'] == 'USD' && $pslip['accsrc_ccy'] == 'USD' ){
				$this->view->totalAmt 			= $pslip["EQUIVALEN_IDR"];
				$this->view->ps_ccy 			= $pslip["ccy"];
			}else if($pslip['PS_TYPE']=='3'){
				$this->view->totalAmt 			= $pslip["EQUIVALEN_IDR"];
				$this->view->ps_ccy 			= 'IDR';
			}else{
				$this->view->totalAmt 			= $pslip["amount"];
				$this->view->ps_ccy 			= $pslip["ccy"];
			}
		}
		$this->view->allowRepair 	 	= ($this->_hasPriviRepairPayment && $pslip["PS_CATEGORY"] != "BULK PAYMENT" && $pslip["PS_CATEGORY"] != "SWEEP PAYMENT");
		$this->view->allowReject 	 	= $this->_hasPriviRejectPayment;
		$this->view->pdf 				= ($pdf)? true: false;
		
		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Approval',$outputHTML);    
		}
	}
	
	private function debet($pslip)
	{
		$PS_NUMBER = $this->_paymentRef;
		
		$this->_tableMst[5]["label"] = "Beneficiary Account";
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_bankname"], $pslip["acbenef_alias"]);
		
		// Table Detail Header
		$fields = array("ACCTSRC_NAME"		=> $this->language->_('Source Account Name (Alias Name)'),
						"ACCTSRC"			=> $this->language->_('Source Account'),
						"TRA_MESSAGE" 		=> $this->language->_('Message'),
						"TRA_REFNO"  	   	=> $this->language->_('Additional Message'),
						"ACCTSRC_CCY"  	   	=> $this->language->_('CCY'),
						"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
            			);
            			
        $select	= $this->_db->select()
							->from	(
										array(	'TT' => 'T_TRANSACTION'),
										array(
												'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
												'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
												'ACCTSRC_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																				 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME , ' (' , TT.SOURCE_ACCOUNT_ALIAS_NAME , ')')
																			END"),
												//'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
												'TRA_AMOUNT'				=> 'TT.TRA_AMOUNT',
												'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
												'TRA_REFNO'					=> 'TT.TRA_REFNO',
											  )
									)
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);
							
		$pslipTrx = $this->_db->fetchAll($select);
		
		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];
		
		$ACCTSRC_arr = array();
		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// Create array acctsrc for validation
			if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]]))
			{
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
			}
			else
			{
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
			}
			
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value = $pTrx[$key];
				
				if ($key == "TRA_AMOUNT")
				{	$value = Application_Helper_General::displayMoney($value);	}
				elseif ($key == "ACCTSRC")
				{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
				
				$value = ($value == "")? "&nbsp;": $value;
				
				$tableDtl[$p][$key] = $value;
			}
		}
		
		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT' 	 , array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array("FROM" 				=> "D",						// D: by Detail, M: by Multiple
							  "PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
							  "PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
							  "ACCTSRCarr" 			=> $ACCTSRC_arr,
							  "ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
							  "_approveBeneficiary" => $this->_hasPriviApproveBene,
							  "_approvePayment" 	=> $this->_hasPriviApprovePayment,			
							  );
		$validate->checkApprove($paramApprove);
		
		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment
		
		$this->view->fields			 = $fields;
		$this->view->tableDtl 		 = $tableDtl;
		$this->view->TITLE_MST		 = $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 = $this->language->_('Transfer From');
	}
	
	
	private function sweepin($pslip)
	{
		$PS_NUMBER = $this->_paymentRef;
		
		$select	= $this->_db->select()
		->from	(
				array(	'TP' => 'T_PSLIP'),
				array(
						'REF_ID'					=> 'TP.REFF_ID',
						'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
				)
		)
		->where('TP.PS_NUMBER = ?', $PS_NUMBER);
		$pslipTrx = $this->_db->fetchRow($select);
		
		
		$refIdSweepIn = $pslipTrx['REF_ID'];
		$PeriodIdSweepIn = $pslipTrx['PS_PERIODIC'];
		//print_r($pslipTrx);
		$select	= $this->_db->select()
// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
		->from	(array(	'TTS' => 'T_PERIODIC'))
		->joinLeft	(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*') )
		->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
// 		echo $select;die;
		$sweepScheme = $this->_db->fetchRow($select);


		//calculate

		$sqlTransaction  	= "SELECT * FROM T_TRANSACTION WHERE PS_NUMBER = ? ";
			$trx 				= $this->_db->fetchAll($sqlTransaction, (string)$PS_NUMBER);
			$countTrx			= count($trx);
			$counterException	= 0;
			$remaintrx = 0;
			$remainAmt = 0;
			//foreach ($trx as $val){
			for($i=0; $i<=$countTrx-1; $i++){
	
				// cek saldo
				$systemBalance = new SystemBalance($this->_customerId,$trx[$i]['SOURCE_ACCOUNT'],Application_Helper_General::getCurrNum($trx[$i]['SOURCE_ACCOUNT_CCY']));
				$systemBalance->checkBalance();
				$balance	= Application_Helper_General::convertDisplayMoney($systemBalance->getEffectiveBalance());
				// end cek saldo
	
				// Set Tra Amount
				$remainBalanceTypeVal = $this->_db->select()
				->from(array('PR' => 'T_PERIODIC_DETAIL'),array('BALANCE_TYPE'))
				->joinInner(array('P' => 'T_PSLIP'), 'PR.PS_PERIODIC = P.PS_PERIODIC', array() )
				->where('P.PS_NUMBER = ?', $trx[$i]['PS_NUMBER'])
				->limit(1)
				;
				$remainBalanceType = reset($this->_db->fetchAll($remainBalanceTypeVal));
	
	
				$remainBalance = $remainBalanceType['BALANCE_TYPE'];
				if ($remainBalance == '1'){ //nominal
					$TRA_AMOUNTARR = $balance - $trx[$i]['TRA_AMOUNT'];
					$trx[$i]['TRA_AMOUNT'] = (string)Application_Helper_General::convertDisplayMoney($TRA_AMOUNTARR);
				}elseif ($remainBalance == '2'){ //persen
					$TRA_AMOUNTARRPercent = $balance * ($trx[$i]['TRA_AMOUNT']/100);
					$TRA_AMOUNTARR = $balance - $TRA_AMOUNTARRPercent;
					$trx[$i]['TRA_AMOUNT'] = (string)Application_Helper_General::convertDisplayMoney($TRA_AMOUNTARR);
				}
				
				$remainAmt = $remainAmt + $TRA_AMOUNTARR;
				// end TraAmount
	
				// get Minimum Trf
				$settings = new Application_Settings();
				$settings->setSettings(null, null);
				$ccyList = $settings->setCurrencyRegistered();
				$validateACCTSRC = new ValidatePayment($this->_custIdLogin, $this->_userIdLogin);
				$minAmt		= $validateACCTSRC->getMinAmt($trx[$i]['SOURCE_ACCOUNT_CCY']);
				// end get Minimum Trf
	
	
				if ($TRA_AMOUNTARR >= $minAmt){
					// transfer
					// $sendTransfer = $this->sendTransferSingleWithin($pslip, $trx[$i]);
	
					// save raw request di transaction (single only)
					// if (!empty($sendTransfer))
					// {
						if($pslip['PS_TYPE'] == 1 || $pslip['PS_TYPE'] == 2 || $pslip['PS_TYPE'] == 3|| $pslip['PS_TYPE'] == 14|| $pslip['PS_TYPE'] == 15){
							$paymentData = array('RAW_REQUEST' => serialize($sendTransfer['RawRequest']) ,
												'TRA_AMOUNT' => $TRA_AMOUNTARR,
												'TRA_REMAIN' => $TRA_AMOUNTARR
									);
						//	print_r($TRA_AMOUNTARR);
	//		print_r($countTrx);
//			die;	
							$whereData   = array('PS_NUMBER = ?'	 => (string) $PS_NUMBER,
									     'SOURCE_ACCOUNT = ?'	 => (string)$trx[$i]['SOURCE_ACCOUNT']);
							$this->_db->update('T_TRANSACTION', $paymentData, $whereData);
						}
					// }
					//Zend_Debug::dump($pslip);
	
					// $Customer = new Customer($this->_customerId);
					// $releaserEmailList = $Customer->getEmailReleaserList();
						
					// TODO: cek if NYK, what to do?
					
				}
	
			}
			
			$pslipdata = array(
					'PS_REMAIN' => $remainAmt					
			);
			$whereData   = array('PS_NUMBER = ?'	 => (string) $PS_NUMBER);
			$this->_db->update('T_PSLIP', $pslipdata, $whereData);

		//end calculate


		//print_r($sweepScheme);die;
		if ($sweepScheme['SESSION_TYPE'] == "1"){
				$sessionSweep			= 'Morning (06.00 a.m - 08.00 a.m )';
			}elseif ($sweepScheme['SESSION_TYPE'] == "2"){
				$sessionSweep			= 'Evening (12.00 p.m - 2.00 p.m )';
			}elseif ($sweepScheme['SESSION_TYPE'] == "3"){
				$sessionSweep			= 'Night (05.00 p.m - 07.00 p.m )';
			}
			if($sweepScheme['PS_EVERY_PERIODIC']=='1'){
				$sweepScheme['DAYNAME'] = 'Monday';
			}else if($sweepScheme['PS_EVERY_PERIODIC']=='2'){
				$sweepScheme['DAYNAME'] = 'Tuesday';
			}else if($sweepScheme['PS_EVERY_PERIODIC']=='3'){
				$sweepScheme['DAYNAME'] = 'Wednesday';
			}else if($sweepScheme['PS_EVERY_PERIODIC']=='4'){
				$sweepScheme['DAYNAME'] = 'Thursday';
			}else if($sweepScheme['PS_EVERY_PERIODIC']=='5'){
				$sweepScheme['DAYNAME'] = 'Friday';
			}else if($sweepScheme['PS_EVERY_PERIODIC']=='6'){
				$sweepScheme['DAYNAME'] = 'Saturday';
			}else if($sweepScheme['PS_EVERY_PERIODIC']=='7'){
				$sweepScheme['DAYNAME'] = 'Sunday';
			}
			
// 		$sweepScheme['PS_EVERY_PERIODIC'];
		$this->_tableMst[5]["label"] = $this->language->_('Beneficiary Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_bankname"], $pslip["acbenef_alias"]);
	
		$this->_tableMst[6]["label"] = $this->language->_('Recurring');
		$this->_tableMst[6]["value"] = $sweepScheme['DAYNAME'];
	
		$this->_tableMst[7]["label"] = $this->language->_('Session');
		$this->_tableMst[7]["value"] = $sessionSweep;
	
		$this->_tableMst[8]["label"] = $this->language->_('End Date Scheme');
		$this->_tableMst[8]["value"] = Application_Helper_General::convertDate($sweepScheme['PS_PERIODIC_ENDDATE'],$this->_dateViewFormat);
	
		if ($pslip['BALANCE_TYPE'] == '2'){
			$persen	= "(%)";
		}else{
			$persen	= "";
		}
		
		
		// Table Detail Header
		$fields = array("ACCTSRC_NAME"		=> $this->language->_('Source Account Name (Alias Name)'),
				"ACCTSRC"			=> $this->language->_('Source Account'),
				"TRA_MESSAGE" 		=> $this->language->_('Message'),
				"TRA_ADDITIONAL_MESSAGE"  	   	=> $this->language->_('Additional Message'),
				"ACCTSRC_CCY"  	   	=> $this->language->_('CCY'),
				"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
				"RATE"  	   	=> $this->language->_('Rate'),
				"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
				"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
				"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
				"TRA_AMOUNT_TOTAL"  	=> $this->language->_('Total'),
				"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'),
				"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),
				
				"ACBENEF_EMAIL" 	=> $this->language->_('Email'),
				"ACBENEF_ISAPPROVE" => $this->language->_('Status'),
				"BALANCE_TYPE" 		=> $this->language->_('Balance Type'),
		);
		 
		$select	= $this->_db->select()
		->from	(
				array(	'TT' => 'T_TRANSACTION'),
				array(
						'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
						'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
						'ACCTSRC_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																				 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME , ' (' , TT.SOURCE_ACCOUNT_ALIAS_NAME , ')')
																			END"),
						//'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
						'TRA_AMOUNT'				=> 'TT.TRA_AMOUNT',
						'TT.FULL_AMOUNT_FEE','TT.PROVISION_FEE','TT.RATE',
						 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						
						'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
						'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL','TRA_AMOUNT_TOTAL'			=> 'TT.TRA_AMOUNT',
						'TRA_REFNO'					=> 'TT.TRA_REFNO',
						'TRA_ADDITIONAL_MESSAGE'=> 'TT.TRA_ADDITIONAL_MESSAGE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN B.PS_STATUS = '1' THEN 'Waiting Approval'
																				 WHEN B.PS_NUMBER = '1' THEN 'Approved'
																				 ELSE 'N/A'
																			END"),
						
						'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE 
        																		FROM T_PERIODIC_DETAIL Y 
        																		inner join T_PSLIP Z 
        																		on Y.PS_PERIODIC = Z.PS_PERIODIC 
        																		where 
        																		Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
				)
		)
		->joinLeft	(   array('B' => 'T_PSLIP'), 'TT.PS_NUMBER = B.PS_NUMBER', array() )
		->where('TT.PS_NUMBER = ?', $PS_NUMBER);
			//echo $select;die;
		$pslipTrx = $this->_db->fetchAll($select);
// 		print_r($pslipTrx);die;
		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$ACCTSRC_arr = array();
		$tableDtl = array();
//		print_r($pslipTrx);
//		die('here');

		if($pslip['PS_TYPE'] != '14'){
		
		foreach ($pslipTrx as $p => $pTrx)
		{

			// Create array acctsrc for validation
			if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]]))
			{
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
			}
			else
			{
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
				$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
			}
				
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value = $pTrx[$key];
				
				if ($key == "TRA_AMOUNT" || $key == "TRA_AMOUNT_TOTAL" )
					{	$value = Application_Helper_General::displayMoney($pslipTrx[$p]['TRA_REMAIN']);	}
					elseif ($key == "RATE")
					{	if($pslipTrx[$p]['RATE']==''){$pslipTrx[$p]['RATE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($pslipTrx[$p]['RATE']);	}
					elseif ($key == "TRANSFER_FEE")
					{	if($pslipTrx[$p]['TRANSFER_FEE']==''){$pslipTrx[$p]['TRANSFER_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($pslipTrx[$p]['TRANSFER_FEE']);	}
					elseif ($key == "PROVISION_FEE")
					{	if($pslipTrx[$p]['PROVISION_FEE']==''){$pslipTrx[$p]['PROVISION_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($pslipTrx[$p]['PROVISION_FEE']);	}
					elseif ($key == "FULL_AMOUNT_FEE")
					{	if($pslipTrx[$p]['FULL_AMOUNT_FEE']==''){$pslipTrx[$p]['FULL_AMOUNT_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($pslipTrx[$p]['FULL_AMOUNT_FEE']);	}
				elseif ($key == "ACCTSRC")
				{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
	
				$value = ($value == "")? "&nbsp;": $value;
	
				$tableDtl[$p][$key] = $value;
			}
		}
		}else{

			$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from	(array(	'TTS' => 'T_PERIODIC'))
			->joinLeft	(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*') )
			->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
			// 		echo $select;die;
			$sweepdetail = $this->_db->fetchAll($select);
// 			print_r($sweepdetail);die;
			foreach ($pslipTrx as $p => $pTrx)
			{
				// Create array acctsrc for validation
				if (isset($ACCTSRC_arr[$pTrx["ACCTSRC"]]))
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"]   += $pTrx["TRA_AMOUNT"];
				}
				else
				{
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["CCY"] 		= $pTrx["ACCTSRC_CCY"];
					$ACCTSRC_arr[$pTrx["ACCTSRC"]]["AMOUNT"] 	= $pTrx["TRA_AMOUNT"];
				}
					
				// create table detail data
				foreach ($fields as $key => $field)
				{
					$value = $pTrx[$key];
		
					
					if ($key == "TRA_AMOUNT" || $key == "TRA_AMOUNT_TOTAL" )
					{	$value = Application_Helper_General::displayMoney($sweepdetail[$p]['TRA_REMAIN']);	}
					elseif ($key == "RATE")
					{	if($sweepdetail[$p]['RATE']==''){$sweepdetail[$p]['RATE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($sweepdetail[$p]['RATE']);	}
					elseif ($key == "TRANSFER_FEE")
					{	if($sweepdetail[$p]['TRANSFER_FEE']==''){$sweepdetail[$p]['TRANSFER_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($sweepdetail[$p]['TRANSFER_FEE']);	}
					elseif ($key == "PROVISION_FEE")
					{	if($sweepdetail[$p]['PROVISION_FEE']==''){$sweepdetail[$p]['PROVISION_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($sweepdetail[$p]['PROVISION_FEE']);	}
					elseif ($key == "FULL_AMOUNT_FEE")
					{	if($sweepdetail[$p]['FULL_AMOUNT_FEE']==''){$sweepdetail[$p]['FULL_AMOUNT_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($sweepdetail[$p]['FULL_AMOUNT_FEE']);	}					elseif ($key == "ACCTSRC")
					{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
		
					$value = ($value == "")? "&nbsp;": $value;
		
					$tableDtl[$p][$key] = $value;
				}
			}
			
			
		}
		

		
	

		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT' 	 , array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array("FROM" 				=> "D",						// D: by Detail, M: by Multiple
				"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
				"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
				"ACCTSRCarr" 			=> $ACCTSRC_arr,
				"BALANCETYPE"			=> $pslip['BALANCE_TYPE'],
				"PS_TYPE"				=> $pslip["PS_TYPE"],
				"PS_REMAIN"				=> $sweepScheme["TRA_REMAIN"],
				"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
				"_approveBeneficiary" => $this->_hasPriviApproveBene,
				"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);

		$validate->checkApprove($paramApprove);
//	print_r($validate->getErrorMsg());die;
		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment

		$this->view->fields			 = $fields;
		$this->view->tableDtl 		 = $tableDtl;
		
		$this->view->TITLE_MST		 = $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 = $this->language->_('Transfer From');
		$this->view->labelpersen 	 = $persen;

	}
	
	
	private function sweepout($pslip)
	{
		require_once 'General/Charges.php';
		$PS_NUMBER = $this->_paymentRef;
	
		
	

		$select	= $this->_db->select()
		->from	(
				array(	'TP' => 'T_PSLIP'),
				array(
						'REF_ID'					=> 'TP.REFF_ID',
						'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
				)
		)
		->where('TP.PS_NUMBER = ?', $PS_NUMBER);
		$pslipTrx = $this->_db->fetchRow($select);
		
		$PeriodIdSweepIn = $pslipTrx['PS_PERIODIC'];
		$refIdSweepIn = $pslipTrx['REF_ID'];
		$select	= $this->_db->select()
// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
		->from	(array(	'TTS' => 'T_PERIODIC'))
		->joinLeft	(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*') )
		->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
		//echo $select;die;
		$sweepScheme = $this->_db->fetchRow($select);
		//print_r($sweepScheme);die;
		
		if ($sweepScheme['SESSION_TYPE'] == "1"){
			$sessionSweep			= 'Morning (06.00 a.m - 08.00 a.m )';
		}elseif ($sweepScheme['SESSION_TYPE'] == "2"){
			$sessionSweep			= 'Evening (12.00 p.m - 2.00 p.m )';
		}elseif ($sweepScheme['SESSION_TYPE'] == "3"){
			$sessionSweep			= 'Night (05.00 p.m - 07.00 p.m )';
		}
		if($sweepScheme['PS_EVERY_PERIODIC']=='1'){
			$sweepScheme['DAYNAME'] = 'Monday';
		}else if($sweepScheme['PS_EVERY_PERIODIC']=='2'){
			$sweepScheme['DAYNAME'] = 'Tuesday';
		}else if($sweepScheme['PS_EVERY_PERIODIC']=='3'){
			$sweepScheme['DAYNAME'] = 'Wednesday';
		}else if($sweepScheme['PS_EVERY_PERIODIC']=='4'){
			$sweepScheme['DAYNAME'] = 'Thursday';
		}else if($sweepScheme['PS_EVERY_PERIODIC']=='5'){
			$sweepScheme['DAYNAME'] = 'Friday';
		}else if($sweepScheme['PS_EVERY_PERIODIC']=='6'){
			$sweepScheme['DAYNAME'] = 'Saturday';
		}else if($sweepScheme['PS_EVERY_PERIODIC']=='7'){
			$sweepScheme['DAYNAME'] = 'Sunday';
		}
		
		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);
		
		$this->_tableMst[6]["label"] = $this->language->_('Recurring');
		$this->_tableMst[6]["value"] = $sweepScheme['DAYNAME'];
		
		$this->_tableMst[7]["label"] = $this->language->_('Session');
		$this->_tableMst[7]["value"] = $sessionSweep;
		
		$this->_tableMst[8]["label"] = $this->language->_('End Date Scheme');
		$this->_tableMst[8]["value"] = Application_Helper_General::convertDate($sweepScheme['PS_PERIODIC_ENDDATE'],$this->_dateViewFormat);
		
		if ($pslip['BALANCE_TYPE'] == '2'){
			$persen	= "(%)";
		}else{
			$persen	= "";
		}
		// Table Detail Header
		$fields = array("ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
				"ACBENEF"			=> $this->language->_('Beneficiary Account'),
				"TRA_MESSAGE" 		=> $this->language->_('Message'),
				"TRA_ADDITIONAL_MESSAGE"  	   	=> $this->language->_('Additional Message'),
				"TRANSFER_TYPE_disp"=> $this->language->_('Transfer Type'),
				"ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
				"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
// 				"TRANSFER_FEE"  	=> 'Transfer Charge',
				"RATE"  	   	=> $this->language->_('Rate'),
				"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
				"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
				"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
				"TRA_AMOUNT_TOTAL"  	=> $this->language->_('Total'),
				"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'),
				"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),
				
				"ACBENEF_EMAIL" 	=> $this->language->_('Email'),
				"ACBENEF_ISAPPROVE" => $this->language->_('Status')
		);
	
		$select	= $this->_db->select()
		->from(	array(	'TT' => 'T_TRANSACTION'),
				array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME , ' (' , TT.BENEFICIARY_ALIAS_NAME , ')')"),
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_REFNO'				=> 'TT.TRA_REFNO',
				        'TRA_ADDITIONAL_MESSAGE'=> 'TT.TRA_ADDITIONAL_MESSAGE',
						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL','TRA_AMOUNT_TOTAL'			=> 'TT.TRA_AMOUNT',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
				        'TT.FULL_AMOUNT_FEE','TT.PROVISION_FEE','TT.RATE',
						 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN B.BENEFICIARY_ISAPPROVE = '0' THEN 'Waiting Approval'
																				 WHEN B.BENEFICIARY_ISAPPROVE = '1' THEN 'Approved'
																				 ELSE 'N/A'
																			END"),
						'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
        																		FROM T_PERIODIC_DETAIL Y
        																		inner join T_PSLIP Z
        																		on Y.PS_PERIODIC = Z.PS_PERIODIC
        																		where
        																		Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
				)
		)
		->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
		->where('TT.PS_NUMBER = ?', $PS_NUMBER);
			
		$pslipTrx = $this->_db->fetchAll($select);
// 		print_r($pslipTrx);die;
		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];
	
		$ACBENEF_IDarr = array();
		$tableDtl = array();
		
		if($pslip['PS_TYPE'] != '15'){
			foreach ($pslipTrx as $p => $pTrx)
			{
				// Create array bene for validation
				if (!empty($pTrx["ACBENEF_ID"]))
				{
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}
			
				$trfType = $pTrx["TRANSFER_TYPE"];
			
				// create table detail data
				foreach ($fields as $key => $field)
				{
					$value = $pTrx[$key];
			
					/* if ($key == "TRANSFER_FEE")
					 {
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);
					} */
			
					if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE"  )
					{	$value = Application_Helper_General::displayMoney($value);	}
					elseif ($key == "ACBENEF")
					{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
					elseif($key == "TRANSFER_TYPE_disp")
					{
						if($value=='PB'){
							$value = 'Mayapada';
						}
					}
			
					$value = ($value == "")? "&nbsp;": $value;
			
					$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
				}
			}	
		}else{
		$select	= $this->_db->select()
			// 		->from	(array(	'TTS' => 'T_TEMP_SWEEP'))->where('TTS.TEMPLATE_REFF = ?', $refIdSweepIn);
			->from	(array(	'TTS' => 'T_PERIODIC'))
			->joinLeft	(array('B' => 'T_PERIODIC_DETAIL'), 'TTS.PS_PERIODIC = B.PS_PERIODIC', array('B.*') )
			->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);
			// 		echo $select;die;
			$sweepdetail = $this->_db->fetchAll($select);

			foreach ($pslipTrx as $p => $pTrx)
			{
				// Create array bene for validation
				if (!empty($pTrx["ACBENEF_ID"]))
				{
					$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
				}
			
				$trfType = $pTrx["TRANSFER_TYPE"];
			
				// create table detail data
				foreach ($fields as $key => $field)
				{
					$value = $pTrx[$key];
			
					/* if ($key == "TRANSFER_FEE")
					 {
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);
					} */
			
					if ($key == "TRA_AMOUNT" || $key == "TRA_AMOUNT_TOTAL" )
					{	$value = Application_Helper_General::displayMoney($sweepdetail[$p]['TRA_REMAIN']);	}
					elseif ($key == "RATE")
					{	if($sweepdetail[$p]['RATE']==''){$sweepdetail[$p]['RATE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($sweepdetail[$p]['RATE']);	}
					elseif ($key == "TRANSFER_FEE")
					{	if($sweepdetail[$p]['TRANSFER_FEE']==''){$sweepdetail[$p]['TRANSFER_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($sweepdetail[$p]['TRANSFER_FEE']);	}
					elseif ($key == "PROVISION_FEE")
					{	if($sweepdetail[$p]['PROVISION_FEE']==''){$sweepdetail[$p]['PROVISION_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($sweepdetail[$p]['PROVISION_FEE']);	}
					elseif ($key == "FULL_AMOUNT_FEE")
					{	if($sweepdetail[$p]['FULL_AMOUNT_FEE']==''){$sweepdetail[$p]['FULL_AMOUNT_FEE']='0.00';} $value = 'IDR '.Application_Helper_General::displayMoney($sweepdetail[$p]['FULL_AMOUNT_FEE']);	}
					elseif ($key == "ACBENEF")
					{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
					elseif($key == "TRANSFER_TYPE_disp")
					{
						if($value=='PB'){
							$value = 'Mayapada';
						}
					}
			
					$value = ($value == "")? "&nbsp;": $value;
			
					$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
				}
			}
		}
		
	
		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT' 	 , array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array("FROM" 				=> "D",	// D: by Detail, M: by Multiple
				"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
				"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
				"ACCTSRCarr" 			=> $ACCTSRC_arr,
				"BALANCETYPE"			=> $pslip['BALANCE_TYPE'],
				"PS_TYPE"				=> $pslip["PS_TYPE"],
				"PS_REMAIN"				=> $sweepScheme["TRA_REMAIN"],
				"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
				"_approveBeneficiary" => $this->_hasPriviApproveBene,
				"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);
		//print_r($paramApprove);die;
		$validate->checkApprove($paramApprove);
	
		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment
	
		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
		$this->view->TITLE_MST		 	= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer To');
		$this->view->labelpersen 	 	= $persen;
	}
	
	private function credit($pslip)
	{
		require_once 'General/Charges.php';
		$PS_NUMBER = $this->_paymentRef;
		
		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], empty($pslip["accsrc_alias"])?'':$pslip["accsrc_alias"]);
		
		// Table Detail Header
		$fields = array("ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
						"ACBENEF"			=> $this->language->_('Beneficiary Account'),
						"TRA_MESSAGE" 		=> $this->language->_('Message'),
						"TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
						"TRANSFER_TYPE_disp"=> $this->language->_('Transfer Type'),
						"ACBENEF_CCY"  	   	=> $this->language->_('CCY'),
						"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
						// "TRANSFER_FEE"  	=> 'Transfer Charge',
						"RATE"  	   	=> $this->language->_('Rate'),
				"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
				"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
				"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
				"TOTAL"  	=> $this->language->_('Total'),
				"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'),
				"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),
				
				"ACBENEF_EMAIL" 	=> $this->language->_('Email'),
				"ACBENEF_ISAPPROVE" => $this->language->_('Status')
            			);
		
		
		
		
		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];
		
		$ACBENEF_IDarr = array();
		$tableDtl = array();
		$selectTrx	= $this->_db->select()
		->from(	array(	'TT' => 'T_TRANSACTION'),
				array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME,' / ',TT.BENEFICIARY_ALIAS_NAME )"),
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
						'TRA_REFNO'				=> 'TT.TRA_REFNO',
						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'Mayapada (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'Mayapada (Buy)'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						// 'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'TT.RATE',
							'TT.PROVISION_FEE',
							'TT.NOSTRO_NAME',
							'TT.FULL_AMOUNT_FEE',
						'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'C.CUST_ID','TT.SOURCE_ACCOUNT_CCY',
						'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
							'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN C.PS_STATUS = '1' THEN 'Waiting Approval'
																				 WHEN C.PS_STATUS = '2' THEN 'Approved'
																				WHEN C.PS_STATUS = '17' THEN 'Request Cancel Future Date'
																				 ELSE 'N/A'
																			END"),
				)
		)
		->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
		->joinLeft	(   array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array() )
		->where('TT.PS_NUMBER = ?', $PS_NUMBER);
			
		 					//		 echo $selectTrx;die;
		$pslipTrx = $this->_db->fetchAll($selectTrx);

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }

 		//print_r($pslipTrx);die;
		foreach ($pslipTrx as $p => $pTrx)
		{
			// Create array bene for validation
			if (!empty($pTrx["ACBENEF_ID"]))
			{
				$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
			}
			
			$trfType = $pTrx["TRANSFER_TYPE"];
			
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value = $pTrx[$key];
				
				/* if ($key == "TRANSFER_FEE")
				{
					// get charges amount
					$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
					$paramCharges 	= array("accsrc" => $pslip["accsrc"], "transferType" => $trfType);
					$value 			= $chargesObj->getCharges($paramCharges);	
				} */
				/*
				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}
				elseif ($key == "ACBENEF")
				{	$value = $value." [".$pTrx[$key."_CCY"]."]";	}
				*/

				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN" || $key == "PROVISION_FEE")
				{	
						if($pslip['PS_TYPE']=='14' || $pslip['PS_TYPE']=='15'){
							$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);	
						}else{
							
							if($pTrx['PS_CCY'] != 'IDR' ){
								$value = Application_Helper_General::displayMoney($value);
								
							}else{

								$value = Application_Helper_General::displayMoney($value);
								
							}	
						}
						
				}
				
				if($key == "TRANSFER_FEE" && !empty($pTrx['SOURCE_ACCOUNT_CCY'])){

					$selecttrffee = $this->_db->select()->from(array('T'=>'M_CHARGES_REMITTANCE'),array('*'))
					->where("T.CHARGE_TYPE =?",'3')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY'])
			;
			//echo $selecttrffee;
			$trffee = $this->_db->fetchRow($selecttrffee);
			
					if($value !=''){
					$value = $trffee['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($value);
	
					}else{
					$value = $trffee['CHARGE_AMOUNT_CCY'].' 0.00';
					}
				}
			if($key == "FULL_AMOUNT_FEE" && !empty($pTrx['ACBENEF_CCY'])){
			    
								$selecttrfFA = $this->_db->select()
			->from(		array(	'T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'4')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
			;
// 								echo $selecttrfFA;die;
			$trfFA = $this->_db->fetchRow($selecttrfFA);

					$value = $trfFA['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);
					
				}

				if($key == 'PROVISION_FEE' && !empty($pTrx['SOURCE_ACCOUNT_CCY'])){

					


			$selecttrfpro = $this->_db->select()
			->from(		array(	'T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'5')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =?",$pTrx['SOURCE_ACCOUNT_CCY'])
			;
			$trfpro = $this->_db->fetchRow($selecttrfpro);
				$value = $trfpro['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($value);
				//print_r($value);die;
				}

				if($key == 'TOTAL'){
						if($pslip['PS_TYPE']=='14' || $pslip['PS_TYPE']=='15'){
							$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);	
						}else if($pslip['PS_TYPE']=='16' || $pslip['PS_TYPE']=='17'){
							$total = $pslip['TRA_AMOUNT'] + $pslip['TRANSFER_FEE'];
							//print_r($total);die;
							$value = 'IDR '.Application_Helper_General::displayMoney($total);	
						}else{
							if($pTrx['ACBENEF_CCY']=='USD' && $pTrx['SOURCE_ACCOUNT_CCY']=='USD' && $pTrx['TRANSFER_TYPE_disp'] == 'PB'){
								$value = 'USD '.Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);							
							}else if($pTrx['ACBENEF_CCY']=='USD' && $pTrx['SOURCE_ACCOUNT_CCY']=='USD'){
								$value = 'USD '.Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);							
							}else if($pTrx['PS_CCY'] == 'USD'){
								$value = $pTrx['PS_CCY'].' '.Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']).' (IDR '.Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']).')';
								
							}else if($pTrx['EQUIVALEN_IDR'] != '0.00'){
								$value = 'IDR '.Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);							
							}else{

								$value = 'IDR '.Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
								
							}	
						}	
				}
				//print_r($pTrx);die;
				if($key == 'RATE'){
					if($pTrx['ACBENEF_CCY']=='USD' && $pTrx['SOURCE_ACCOUNT_CCY']=='USD'){
					$value = 'IDR 0.00';
					}
					else if($pTrx['RATE']!='N/A'){
					$value = 'IDR '.Application_Helper_General::displayMoney($pTrx['RATE']);
					}else{
					$value = 'IDR 0.00';

					}
				}

				

				if($key == "TRANSFER_TYPE_disp")
					{
						if($value=='PB'){
							$value = 'Mayapada';
						}
					}
				$value = ($value == "")? "&nbsp;": $value;
				
				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if (!empty($pslip['BANK_CODE'])) {
				$bankcode = $pslip['BANK_CODE'];
			}
			else{
				$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
			}

			if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
				$bankname = '-';
			}
			else if (empty($bankcode)) {
				$bankname = $this->_bankName;
			}
			else{
				$bankname = $bankNameArr[$bankcode];
			}

			$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
		}
		
		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT' 	 , array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array("FROM" 				=> "D",	// D: by Detail, M: by Multiple
							  "PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
							  "PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
							  "ACCTSRCarr" 			=> $ACCTSRC_arr,
							  "ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
							  "_approveBeneficiary" => $this->_hasPriviApproveBene,
							  "_approvePayment" 	=> $this->_hasPriviApprovePayment,			
							  );
		$validate->checkApprove($paramApprove);
		
		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment
		
		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
		$this->view->TITLE_MST		 	= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer To');
	}
}
