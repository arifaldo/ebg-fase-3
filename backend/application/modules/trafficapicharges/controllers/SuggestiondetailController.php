<?php
require_once 'Zend/Controller/Action.php';

class trafficapicharges_SuggestiondetailController extends user_Model_User{

  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newpopup');
    $change_id = $this->_getParam('changes_id');
    $change_id = (Zend_Validate::is($change_id,'Digits'))? $change_id : 0;

    $this->view->suggestionType = $this->_suggestType;

    if($change_id)
    {
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID'))
                             ->where('CHANGES_ID='.$this->_db->quote($change_id))
                             // ->where("CHANGES_FLAG='B'")
                             ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
//                             ->where('UPPER(MODULE)='.$this->_db->quote(strtoupper($this->_request->getModuleName())));
      $result = $this->_db->fetchOne($select);

      if(empty($result))  $this->_redirect('/notification/invalid/index');


      if($result)
      {
        //content send to view
        $select = $this->_db->select()
                               ->from(array('T'=>'TEMP_CHARGES_API'))
      	                       ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
      	                       ->where('T.CHANGES_ID = ?', $change_id);
      	$resultdata = $this->_db->fetchRow($select);

      	if($result['TEMP_ID'])
      	{
          $this->view->cek = 1;
      	    //----------------------------------------------TEMP DATA--------------------------------------------------------------
  	  		$this->view->changes_id     = $resultdata['CHANGES_ID'];
  	  		$this->view->changes_type   = $resultdata['CHANGES_TYPE'];
  	  		$this->view->changes_status = $resultdata['CHANGES_STATUS'];
  	  		$this->view->read_status    = $resultdata['READ_STATUS'];
  	  		$this->view->created        = $resultdata['CREATED'];
  	  		$this->view->created_by     = $resultdata['CREATED_BY'];

        	$this->view->cust_id         = $resultdata['CUST_ID'];
          $this->view->balance_inquiry   = $resultdata['BALANCE_INQUIRY'];
        	$this->view->account_statement = $resultdata['ACCOUNT_STATEMENT'];
        	$this->view->inhouse_transfer   = $resultdata['INHOUSE_TRANSFER'];
        	$this->view->skn_transfer   = $resultdata['SKN_TRANSFER'];
        	$this->view->rtgs_transfer = $resultdata['RTGS_TRANSFER'];
        	$this->view->online_transfer     = $resultdata['ONLINE_TRANSFER'];
        	$this->view->kurs_rate_inquiry   = $resultdata['KURS_RATE_INQUIRY'];

          $this->view->charges_suggested   = $resultdata['CHARGES_SUGGESTED'];
          $this->view->charges_suggestedby = $resultdata['CHARGES_SUGGESTEDBY'];

          //new jadi Current Value tampil semua
          $selectCustId = $this->_db->select()
                             ->from('M_CHARGES_API',array('CUST_ID'))
                             ->where('CUST_ID='.$this->_db->quote($resultdata['CUST_ID']));
          $resultCustId = $this->_db->fetchOne($selectCustId);          
          
          $selectold = $this->_db->select()
                        ->from(array('T'=>'M_CHARGES_API'));
                        if(empty($resultCustId)){
                        $selectold->where("T.CUST_ID = 'GLOBAL'");
                        } else{
                        $selectold->where('T.CUST_ID = ?', $resultdata['CUST_ID']);
                        }          
          $resultdataold = $this->_db->fetchRow($selectold);        
         
          $this->view->oldcust_id         = $resultdataold['CUST_ID'];
          $this->view->oldbalance_inquiry   = $resultdataold['BALANCE_INQUIRY'];
        	$this->view->oldaccount_statement = $resultdataold['ACCOUNT_STATEMENT'];
        	$this->view->oldinhouse_transfer   = $resultdataold['INHOUSE_TRANSFER'];
        	$this->view->oldskn_transfer   = $resultdataold['SKN_TRANSFER'];
        	$this->view->oldrtgs_transfer = $resultdataold['RTGS_TRANSFER'];
        	$this->view->oldonline_transfer     = $resultdataold['ONLINE_TRANSFER'];
        	$this->view->oldkurs_rate_inquiry   = $resultdataold['KURS_RATE_INQUIRY'];
  	  

      }else{
        $this->view->cek = 0;
      }

    }
  }

    if(!$change_id)
    {
      $error_remark = $this->language->_('Changes Id is not found');
      $this->_redirect($this->_backURL);
      $this->_redirect('/popuperror/index/index');
    }

    //insert log
	try
	{
	  $this->_db->beginTransaction();
	  $fulldesc = 'CHANGES_ID:'.$change_id;
	  if(!$this->_request->isPost()){
	  Application_Helper_General::writeLog('CHCL','View Charges Changes List');
	  }

	  $this->_db->commit();
	}
    catch(Exception $e){
 	  $this->_db->rollBack();
	}

    $this->view->changes_id = $change_id;
  }
}
