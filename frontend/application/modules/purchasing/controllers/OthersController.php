<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CoreBank.php';
require_once 'General/CustomerUser.php';

class purchasing_OthersController extends Application_Main
{
	public function indexAction()
	{
		$sessionNamespace 		= new Zend_Session_Namespace('Others');
		$paramSession 			= $sessionNamespace->paramSession;
		$CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$AccArr 	  			= $CustomerUser->getAccounts();
		$this->view->AccArr 	= $AccArr;
		
		$model = new purchasing_Model_Purchasing();
		$listother = $model->getProviderList(15,2);
		$otherArr = array('' => ' --- Please Select ---');
		$otherArr += Application_Helper_Array::listArray($listother,'PROVIDER_ID','PROVIDER_NAME');
		$this->view->otherArr = $otherArr;
		
		if($this->_getParam('submit') == 'Next')
		{
			$inArrayOperator 		= Application_Helper_Array::listArray($listother,'PROVIDER_NAME','PROVIDER_ID');
			$inArraynominal 		= array(
												'100000'=>'100000',
												'200000'=>'200000',
												'300000'=>'300000',
												'400000'=>'400000',
												'500000'=>'500000',
											);
			
			$param['operator']		= $this->_getParam('operator');
			$param['nominal']		= $this->_getParam('nominal');
			$param['ACCTSRC']		= $this->_getParam('ACCTSRC');
			$param['noKartu']		= $this->_getParam('noKartu');
			
			$filters = 	array( 
									'ACCTSRC'	=> 	array('StringTrim','StripTags'),
									'operator'	=> 	array('StringTrim','StripTags'),
									'nominal'	=> 	array('StringTrim','StripTags'),
									'noKartu'	=> 	array('StringTrim','StripTags'),
								);
			$validator = array( 
									'ACCTSRC'	=> 	array	(	
																'NotEmpty',
																'messages' => array	(
																						"Error: Source Account Has Not Fill yet.",
																					)
															),
									'operator' 	=> 	array	(	
																'NotEmpty',
																array('inArray',$inArrayOperator),
																'messages' => array	(
																						"Error: Service Provider Has Not Fill yet.",
																						"Error: Service Provider not Exist.",
																					)
															),
									'nominal' 	=> 	array	(	
																'NotEmpty',
																array('inArray',$inArraynominal),
																'messages' => array	(
																						"Error: Voucher Nominal is Empty.",
																						"Error: Voucher Nominal not Exist.",
																					)
															),
									'noKartu' 	=> 	array	(	
																'NotEmpty',
																'Alnum',
																array	(
																			'StringLength',array	(
																										'min'=>1,
																										'max'=>50
																									)
																		),
																'messages' => array	(
																						"Error: Card No is Empty.",
																						"Error: Card No invalid format.",
																						"Error: Card No max length min 1 max 50.",
																					)
															),
								);
			$zf_filter_input = new Zend_Filter_Input($filters, $validator, $param, $this->_optionsValidator);
			
			$error = false;
			
			if($zf_filter_input->isValid())
			{
				$validateACCTSRC = new ValidateAccountSource($param['ACCTSRC'], $this->_userIdLogin); 
				$param['operator_name']	= $otherArr[$param['operator']];
				$charges = $this->getCharges($param['operator']);
				$fee = ($charges['IDR']['CHARGES_AMT']) ? $charges['IDR']['CHARGES_AMT'] : 0;
				$param['amount'] = $param['nominal'];
				$total = $param['amount'] + $fee;
				$check = $validateACCTSRC->check($total);

				if ($check == true)
				{
					$paramSession['operator']			= $param['operator'];
					$paramSession['operator_name']			= $param['operator_name'];
					$paramSession['nominal']			= $param['nominal'];
					$paramSession['amount']			= $param['amount'];
					$paramSession['noKartu']			= $param['noKartu'];
					$paramSession['ACCTSRC']			= $param['ACCTSRC'];
					$paramSession['fee']				= $fee;
					$paramSession['total']				= $total;
					$sessionNamespace->paramSession 	= $paramSession;
					
					$this->_redirect('/purchasing/others/next');
				}
				else
				{
					$errMsg 			= $validateACCTSRC->getErrorMsg();
					$error				= true;
				}
			}
			else
			{
				$errors 	= $zf_filter_input->getMessages();
				$error		= true;
				
			}
			
			if($error)
			{
				$this->view->error 					= $error;
				$this->view->operatorErr 			= (isset($errors['operator']))? $errors['operator'] : null;
				$this->view->nominalErr 			= (isset($errors['nominal']))? $errors['nominal'] : null;
				$this->view->ACCTSRCErr 			= (isset($errors['ACCTSRC']))? $errors['ACCTSRC'] : null;
				$this->view->noKartuErr 			= (isset($errors['noKartu']))? $errors['noKartu'] : null;
				$this->view->xACCTSRC 				= (isset($errMsg))? $errMsg : null;
				
				$this->view->operator				= $param['operator'];
				$this->view->nominal				= $param['nominal'];
				$this->view->noKartu				= $param['noKartu'];
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				
			}
		}
		else if($this->_getParam('submit') == 'Cancel')
		{
			$this->cancel();
		}
		else
		{
			unset($_SESSION['Others']);
		}
	
	}	
	
	public function nextAction() 
	{
		$model = new purchasing_Model_Purchasing();
		
		$SinglePayment 		= new SinglePayment( null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('Others');
		$paramSession 		= $sessionNamespace->paramSession;
		
		$acctInfo = $model->getAcctInfo($paramSession['ACCTSRC']);
		$ACCTSRC   		= $paramSession['ACCTSRC'];
		$ACCTSRC_CCY   	= $acctInfo["CCY_ID"];
		$ACCTSRC_NAME   = $acctInfo["ACCT_NAME"];
		$ACCTSRC_ALIAS  = $acctInfo["ACCT_ALIAS_NAME"];
		$ACCTSRC_TYPE   = $acctInfo["ACCT_TYPE"];
		$paramSession['ACCTSRC_view'] 	= Application_Helper_General::viewAccount($ACCTSRC, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);
		
									
		if(!$paramSession)
		{
			$this->cancel();
		}
		
		if($this->_getParam('submit') == 'Submit')
		{
			$PS_NUMBER 	= "P".$SinglePayment->generatePaymentID(1);
			$PS_EFDATE		= date("d M Y H:i:s");
			
			$SinglePayment 				= new SinglePayment( $PS_NUMBER, $this->_userIdLogin);
			
			if($paramSession['operator']==1120)
			{
				$operator = 'Kartu Blitz';
			}
					
			$setting 	= new Settings();
			$bank_code 	= $setting->getSetting('bank_code');
	
			$param	= array(
					'PS_CCY'			=> $ACCTSRC_CCY,
					'PS_TYPE'			=> 14,
					'PS_EFDATE'			=> Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat),
					'TRA_AMOUNT'		=> $paramSession['amount']+$paramSession['fee'],
					'SOURCE_ACCOUNT'	=> $paramSession['ACCTSRC'],
					'TRA_MESSAGE'		=> 'Pembelian lainnya operator = '.$operator.', No = '.$paramSession['noKartu'],
					'_addBeneficiary'	=> $this->view->hasPrivilege('BADA'),
					'_beneLinkage'		=> $this->view->hasPrivilege('BLBU'),
					'_priviCreate'		=> 'CRSP',
					'PS_CATEGORY'		=> 'PURCHASE OTHERS',
					'PROVIDER_ID'		=> $paramSession['operator'],
					'REF_NO'			=> $paramSession['noKartu'],
					'USER_BANK_CODE'	=> $bank_code,
					'DETAIL'			=>	array(
											'0' => array(
															'PS_FIELDNAME' 	=> 'Service Provider',
															'PS_FIELDTYPE'	=> '3',
															'PS_FIELDVALUE'	=> $operator
														),
											'1' => array(
															'PS_FIELDNAME' 	=> 'Card No',
															'PS_FIELDTYPE'	=> '3',
															'PS_FIELDVALUE'	=> $paramSession['noKartu']
														),
											'2' => array(
															'PS_FIELDNAME' 	=> 'Voucher Nominal',
															'PS_FIELDTYPE'	=> '1',
															'PS_FIELDVALUE'	=> $paramSession['nominal']
														),
											'3' => array(
															'PS_FIELDNAME' 	=> 'Fee',
															'PS_FIELDTYPE'	=> '1',
															'PS_FIELDVALUE'	=> $paramSession['fee']
														),
											'4' => array(
															'PS_FIELDNAME' 	=> 'Total Payment',
															'PS_FIELDTYPE'	=> '1',
															'PS_FIELDVALUE'	=> $paramSession['amount']+$paramSession['fee']
														),
											)
			);
						
			$return = $SinglePayment->createPembelianPembayaran($param);
			
			if($return)
			{
				$paramSession['PSNumber'] 		= $PS_NUMBER;
				$paramSession['date']			= $PS_EFDATE;
				$sessionNamespace->paramSession = $paramSession;
				
				$this->_redirect('/purchasing/others/confirm');
			}
		}
		else if ($this->_getParam('submit') == 'Back')
		{
			$this->_redirect('/purchasing/others/index');
		}
		
		$this->view->paramSession				= $paramSession;
	}
	
	public function confirmAction() 
	{
		$sessionNamespace 	= new Zend_Session_Namespace('Others');
		$paramSession 		= $sessionNamespace->paramSession;
		
		if(!$paramSession)
		{
			$this->cancel();
		}
		
		if ($this->_getParam('submit') == 'Back')
		{
			$this->_redirect('/purchasing/others/index');
		}
		
		$sessionNamespace->paramSession 	= $paramSession;
		$this->view->paramSession			= $paramSession;
	}
	
	public function cancel() 
	{
		unset($_SESSION['Others']);
		$this->_redirect("/home/index");
	}
}
