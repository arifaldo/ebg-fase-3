<?php
/**
 * Tempcustomer model
 *
 * @author
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';

class Changemanagement_Model_Tempbglist extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = 'CST';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null)
	{
		$set = new Settings();
		//ambil dari m_setting SKN RTGS
		$global_charges_skn_ori = 0; // $set->getSettingNew('global_charges_skn');
		$global_charges_rtgs_ori = 0; // $set->getSettingNew('global_charges_rtgs');

		//ambil dari m_setting FEE CHARGE
		$admin_fee_account_ori = 0; // $set->getSettingNew('admin_fee_account');
		$admin_fee_company_ori = 0; //$set->getSettingNew('admin_fee_company');

		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
        $cust_id 	= $customer['CUST_ID'];

		
		//==========================================================================================================
		//insert to master table M_CUSTOMER
		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		$insertArr['CUST_UPDATED']     = new Zend_Db_Expr('now()');
		$insertArr['CUST_UPDATEDBY']   = $actor;

	try
		{
			// die;

			// print_r($customerins);
			if(empty($insertArr['CUST_SPECIAL_RATE'])){
				unset($insertArr['CUST_SPECIAL_RATE']);
				unset($insertArr['CUST_TOKEN_AUTH']);
			}
			// die;
			// print_r($insertArr);die;
			$customerins = $this->dbObj->insert('M_CUSTOMER',$insertArr);

		}
	catch(exception $e)
		{
			 //Zend_Debug::dump($e->getMessage());die;
		}
		if(!(boolean)$customerins)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		

		$deleteChanges  = $this->deleteNew(1);
		if(!$deleteChanges)
			return false;

		/* send email notif  */


		return true;
	}

	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null)
	{

		$transFailed = false;
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
		else
		{
			$custId = $customer['CUST_ID'];
		}


		//update record customer
		$updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','CUST_CREATED'=>'','CUST_CREATEDBY'=>'','CUST_STATUS'=>'','CUST_MONTHLYFEE_TYPE'=>''));
		$updateArr['CUST_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['CUST_UPDATEDBY']   = $actor;

		$whereArr = array('CUST_ID = ?'=>(string)$customer['CUST_ID']);
		$customerupdate = $this->dbObj->update('M_CUSTOMER',$updateArr,$whereArr);

		

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		
		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_CUSTOMER
		

		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}


	/*
	 if delete customer :
	 - delete user
	 - delete bank account
	 - delete user limit
	 - delete daily limit
	*/
	public function approveDelete($actor = null)
	{
		
	}

	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew($new = null)
	{

		
		return true;
	}

	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		return true;
	}

	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		return true;
	}

	public function deleteDelete()
	{


		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;



	}

	public function sslPrm()
	{
	return array("6a1f325be4c0492063e83a8cb2cb9ae7","IV (optional)","aes-128-cbc");
	}

	public function sslEnc($msg)
	{
	  list ($pass, $iv, $method)=$this->sslPrm();
		 return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}


    public function approveDeactivate(){

	}

	public function approveActivate(){

	}

    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}





}
