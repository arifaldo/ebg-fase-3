<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class lcreport_DetailController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $Settings = new Settings();

        $toc = $Settings->getSetting('ftemplate_bg');
        $this->view->toc = $toc;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];
        
        $complist = $this->_db->fetchAll(
            $this->_db->select()
                 ->from(array('A' => 'M_USER'),array('CUST_ID'))
               
                 ->where("A.USER_ID = ? ", $this->_userIdLogin)
        );	
        // echo $complist;;die;
        // var_dump($complist);die;
        $comp = "'";
        // print_r($complist);die;
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";

        $acctlist = $this->_db->fetchAll(
            $this->_db->select()
                    ->from(array('A' => 'M_APIKEY'))
                    ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                    ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                    // ->where('A.ACCT_STATUS = ?','5')
                    ->where("A.CUST_ID IN (".$comp.")")
                    ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
        );

        $listbank = array();
        foreach ($acctlist as $key => $value) {
            $listbank[] = $value['BANK_NAME'];
        }
        $listBank = array_unique($listbank);
        $this->view->listBank = $listBank;

        // echo "<pre>";
        // var_dump($acctlist);

        $account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}
		// echo "<pre>";
		// var_dump($account);die;
		// $acct = array();
		$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];
			$i++;
        }
    
        $this->view->sourceAcct = $acct;
        
        $accData = $this->_db->select()
					->from('M_CUSTOMER_ACCT')
					->where('CUST_ID IN ('.$comp.')');

        $accData = $this->_db->fetchAll($accData);
        foreach($accData as $key => $value){
            $accdata[$i]['ACCT_NO'] = $value['ACCT_NO'];
            $accdata[$i]['ACCT_NAME'] = $value['ACCT_NAME'];
            $accdata[$i]['ACCT_BANK'] = $this->_bankName;
        }
        $this->view->AccArr = $accdata;

        // $paramac = array(
		// 	'CCY_IN' => 'IDR'
		// );
		// $AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
        // $this->view->AccArr 		= $AccArr;
        
        $numb = $this->_getParam('LC_NUMBER');

        if(!empty($numb)){

            $lcdata = $this->_db->select()
                             ->from(array('A' => 'T_LC'),array('*'))
                             ->where('A.LC_NUMBER = ?',$numb)
                             ->query()->fetchAll();

            if(!empty($lcdata)){

                $data = $lcdata['0'];

                $this->view->expiryDate = Application_Helper_General::convertDate($data['LC_EXPDATE'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
                $this->view->lastShipment = Application_Helper_General::convertDate($data['LC_LASTSHIP_DATE'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
                
                $arrcredit_type = array(
                    1 => 'Letter of Credit',
                    2 => 'Surat Kredit Berdokumen Dalam Negeri'
                );
                
                $this->view->credit_type_text = $arrcredit_type[$data['LC_CREDIT_TYPE']];
                
                $arrtransferable = array(
                    1 => 'Transferable',
                    2 => 'Non Transferable'
                );
                
                $this->view->transferable_text = $arrtransferable[$data['LC_TRANSFERABLE']];
                
                $arrconfirmation = array(
                    1 => 'Without',
                    2 => 'May Add',
                    3 => 'Confirmed'
                );
                
                $this->view->confirmation_text = $arrconfirmation[$data['LC_CONFIRMATION']];
                
                $arradvBank = array(
                    1 => 'PHILLIP BANK - SWIFT: HDSBKHHPP',
                );
                
                $this->view->advBank_text = $arradvBank[$data['LC_ADVISING']];
                
                $arrlcBy = array(
                    1 => 'Negotation',
                    2 => 'Acceptance',
                    3 => 'Confirmed',
                    4 => 'Payment'
                );
                
                $this->view->lcBy_text = $arrlcBy[$data['LC_BY']];
                
                $arrdifered_payment = array(
                    1 => 'delivery of goods but not exceeding the expiry date',
                    2 => 'proof of delivery goods but not exceeding the expired date'
                );
                
                $this->view->difered_payment_text = $arrdifered_payment[$data['LC_DOC_AFTER']];
                
                $arrtrade_terms = array(
                    0 => '---',
                    1 => 'EXW',
                    2 => 'FCA',
                    3 => 'CPT',
                    4 => 'CIP',
                    5 => 'DAT',
                    6 => 'DAP',
                    7 => 'DDP',
                    8 => 'FAS',
                    9 => 'FOB',
                    10 => 'CFR',
                    11 => 'CIF'
                );
                
                $this->view->trade_terms_text = $arrtrade_terms[$data['trade_terms']];
                
                $arrtenor = array(
                    1 => 'Sight',
                    2 => 'Usance',
                    3 => 'Other'
                );
                
                $this->view->tenor_text = $arrtenor[$data['LC_TENOR']];
                
                $arrconfirmationCharge = array(
                    1 => 'Not Applicable'            
                );
                
                $this->view->confirmationCharge_text = $arrconfirmationCharge[$data['LC_CONFIRM']];
                
                $arrcharge = array(
                    1 => 'Applicant',
                    2 => 'Beneficiary',
                );
                
                $this->view->issuance_text = $arrcharge[$data['LC_ISSUANCE']];
                $this->view->discrepancy_text = $arrcharge[$data['LC_DISCREPANCY']];
                $this->view->chargesOutside_text = $arrcharge[$data['LC_CHARGESOUTSIDE']];

                $this->view->lc_number = $data['LC_NUMBER'];
                $this->view->benef_name = $data['LC_BENEF_NAME'];
                $this->view->benef_address = $data['LC_BENEF_ADDRESS'];
                $this->view->bank_amount = Application_Helper_General::displayMoneyplain($data['LC_AMOUNT']);
                $this->view->tolerance = $data['LC_TOLERANCE_TYPE'];
                $this->view->toleranceMax = $data['LC_TOLERANCE_MAX'];
                $this->view->toleranceMin = $data['LC_TOLERANCE_MIN'];
                $this->view->expiryPlace = $data['LC_EXP_PLACE'];
                $this->view->shipmentPeriod = $data['LC_SHIP_PERIOD'];
                $this->view->tenor = $data['LC_TENOR'];
                $this->view->partial_shipment = $data['LC_TENORPARTIAL'];
                $this->view->trans_shipment = $data['LC_TENORTRANSHIP'];
                $this->view->confirming_bank = $data['LC_CONFIRMING'];
                $this->view->placeCharge = $data['LC_PLACETALK'];
                $this->view->portLoading = $data['LC_PORTLOAD'];
                $this->view->placeDestination = $data['LC_PLACEFINAL'];
                $this->view->portofDischarge = $data['LC_PORTDISCHARGE'];
                $this->view->availablewith = $data['LC_AVAIL_TYPE'];
                $this->view->otherAvailable = $data['LC_AVAIL_OTHER'];
                $this->view->documentPeriod = $data['LC_DOC_PERIOD'];
                $this->view->descGoods = $data['LC_DESC_GOOD'];
                $this->view->contractNo = $data['LC_CONTACT'];
                $this->view->hsCode = $data['LC_HSCODE'];
                $this->view->countryOrigin = $data['LC_COUNTRY'];
                $this->view->goodsVolume = $data['LC_VOLUME'];
                $this->view->unitPrice = Application_Helper_General::displayMoneyplain($data['LC_UNITAMOUNT']);
                $this->view->trade = $data['LC_TRADETYPE'];
                $this->view->tradeOthers = $data['LC_TRACEOTHER'];
                $this->view->comment = $data['LC_ADD_CONDITION'];
                $this->view->chargetoAccount = $data['LC_CHARGEACCOUNT'];

                if(!empty($data['LC_DOCREQ'])){
                    $docreq = (explode(",",$data['LC_DOCREQ']));

                    foreach($docreq as $key => $val){
                        $str = 'checkp'.$val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }

                    $detaillc = $this->_db->select()
                             ->from(array('A' => 'T_LC_DETAIL'),array('*'))
                             ->where('A.LC_NUMBER = ?',$numb)
                             ->query()->fetchAll();

                    // var_dump($detaillc);

                    foreach($detaillc as $key => $val){
                        $str = 'docReq'.$val['DOCREQ_INDEX'];
                        $this->view->$str =  $val['DOCREQ_VAL'];
                    }
                    
                }
                
            }
            
        }

    }
}