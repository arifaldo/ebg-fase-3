<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';


class singledirectdebit_BulkController extends Application_Main
{ 
	protected $_moduleDB = 'RTF'; //masih harus diganti

	// protected $_destinationUploadDir = '';
	protected $_maxRow = '';
	protected $_maxRowSingle = '';

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
		
		$this->_maxRowSingle = $setting->getSetting('max_import_single_payment');
		
		
	}

	public function indexAction()
	{
		

		$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL();
		$this->view->ccyArr = $this->getCcy();

		$settingObj = new Settings();
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$conf = Zend_Registry::get('config');
		$paymentType = $conf['payment']['type'];
		$paymentTypeFlip = array_flip($paymentType['code']);

		$this->view->paymentType = $paymentType;
		$this->view->paymentTypeFlip = $paymentTypeFlip;
		//var_dump($this->getCurrentDate());
		//var_dump(Application_Helper_General::convertDate($this->getCurrentDate()));die;
		//$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		$this->view->PSEFDATE = $newDate = date("Y-m-d", strtotime($this->getCurrentDate()));
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$param["CCY_IN"] = 'IDR';
		$AccArr 	  = $CustomerUser->getAccounts($param);
		$this->view->AccArr =  $AccArr;
		$listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');

		$BULK_TYPEARR = $this->_request->getParam('BULK_TYPE');
		
		$cancel = $this->_request->getParam('cancel');
		if($cancel){
			
			$this->view->error = false;
			$this->view->cancel = true;
		}
		
		if(!empty($BULK_TYPEARR)){
			$this->view->BULK_TYPE  = $BULK_TYPEARR;
		}else{
			$this->view->BULK_TYPE = '7';	
		}
		$bulkarr = array(
    								'6' => $this->language->_('Multi Credit'),
    								'7' => $this->language->_('Multi Debet'),
    								'4' => $this->language->_('Payroll'),
    								
    								'17' => $this->language->_('Many to many'),
									'18' => $this->language->_('E-Money'),
									'19' => $this->language->_('eTax'),
									'20' => $this->language->_('-'),
									'21' => $this->language->_('-'),
									'8' => $this->language->_('Payroll Other'),
    								);
		$this->view->BulkType = $bulkarr;

		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}
		
		$min_bulk		= $this->getSetting('min_amount_batch');
		$max_bulk		= $this->getSetting('max_amount_batch');
		$this->view->maxbulk = $max_bulk;
		$this->view->minbulk = $min_bulk;
		$this->view->maxrow = $this->_maxRow;
		$this->view->maxrowsingle = $this->_maxRowSingle; 
		

		//get all adapter profile type (payroll, manytomany, e-money, multi credit, multi debit)
		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'autodebit');
						//echo $select;
		$adapterDataAutodebit = $this->_db->FetchAll($select);

		


		$this->view->adapterDataAutodebit = $adapterDataAutodebit;
		

		$frontendOptions = array(
		      'lifetime' => 86400,
		      'automatic_serialization' => true
		    );
	    $backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
	    $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

	    $cacheID = 'USERLIST';

	    $userlist = $cache->load($cacheID);
		$this->view->pdfUserlist 	= $userlist;

		$save 	= $this->_getParam('save');
    	$cancel = $this->_getParam('cancel');

		if($save){

			$bsid = $this->_request->getParam('bs_id');
			$params = $this->_request->getParams();

            $validators = array (
                                    'bsid' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'bsid' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            foreach ($bsid as $key => $value) {
                if(!empty($value)){
                    $success = true;
                }
            }


            if($zf_filter_input->isValid() && $success && !empty($cancel))
            {

                foreach ($bsid as $key => $value)
                {

                    $bsid = $value;

                    $selectbulkplsip = $this->_db->select()
                        ->from('TEMP_BULKPSLIP',array('*'))
                    	->where('BS_ID = ?',$bsid);
                        
  					$resultbulkpslip = $this->_db->fetchAll($selectbulkplsip);

  					foreach ($resultbulkpslip as $dataTemp) {

						if ($dataTemp['PS_TYPE'] == '11') {

							if ($payment["countTrxPB"] == 0)
								$priviCreate = 'CBPI';
							else
								$priviCreate = 'CBPW';

							$addBeneficiary   = $this->view->hasPrivilege('BADA');
							$beneLinkage      = $this->view->hasPrivilege('BLBU');

							$param['PS_SUBJECT'] = $dataTemp['PS_SUBJECT'];
							$param['PS_EFDATE']  = Application_Helper_General::convertDate($dataTemp['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
							$param['PS_TYPE'] 	= $this->_paymenttype['code']['payroll'];
							$param['PS_CCY']  = $dataTemp['PS_CCY'];
							$param['PS_FILE'] = $dataTemp['PS_FILE'];

							$param['TRANSACTION_DATA'] = array();
							$selectbulktransaction = $this->_db->select()
	                            ->from('TEMP_BULKTRANSACTION',array('*'))
	                        	->where('BS_ID = ?',$bsid);
	                            
	      					$resultbulktransaction = $this->_db->fetchAll($selectbulktransaction);

							foreach($resultbulktransaction as $row)
							{

								$conf = Zend_Registry::get('config');
								$transferType = $conf['transfer']['type'];
								$transferTypeFlip = array_flip($transferType['code']);

								$param['TRANSACTION_DATA'][] = array(
									'SOURCE_ACCOUNT' 					=> $row['SOURCE_ACCOUNT'],
									'BENEFICIARY_ACCOUNT' 				=> $row['BENEFICIARY_ACCOUNT'],
									'BENEFICIARY_ACCOUNT_CCY' 			=> $row['BENEFICIARY_ACCOUNT_CCY'],
									'BENEFICIARY_ACCOUNT_NAME' 			=> $row['BENEFICIARY_ACCOUNT_NAME'],
									'BENEFICIARY_ALIAS_NAME' 			=> $row['BENEFICIARY_ALIAS_NAME'],
									'BENEFICIARY_EMAIL' 				=> $row['BENEFICIARY_EMAIL'],
									'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['BENEFICIARY_MOBILE_PHONE_NUMBER'],
				// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
									'BENEFICIARY_CITIZENSHIP' 			=> $row['BENEFICIARY_CITIZENSHIP'],
									'BENEFICIARY_RESIDENT' 				=> $row['BENEFICIARY_RESIDENT'],
									'CLR_CODE' 							=> $row['CLR_CODE'],
									'TRANSFER_TYPE' 					=> $transferType['desc'][$transferTypeFlip[$row['TRANSFER_TYPE']]],
									'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
									'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
									'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
									'TRA_REFNO' 						=> $row['TRA_REFNO'],
								);

							}

							$param['_addBeneficiary'] = $addBeneficiary;
							$param['_beneLinkage'] = $beneLinkage;
							$param['_priviCreate'] = $priviCreate;

							$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
							$paymentRef = NULL;
							$result = $BulkPayment->createPayment($param,$paymentRef);

						}


						if ($dataTemp['PS_TYPE'] == '25') {

							if ($payment["countTrxPB"] == 0)
								$priviCreate = 'CBPI';
							else
								$priviCreate = 'CBPW';

							$addBeneficiary   = $this->view->hasPrivilege('BADA');
							$beneLinkage      = $this->view->hasPrivilege('BLBU');

							$param['PS_SUBJECT'] = $dataTemp['PS_SUBJECT'];
							$param['PS_EFDATE']  = Application_Helper_General::convertDate($dataTemp['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
							$param['PS_TYPE'] 	= $this->_paymenttype['code']['payroll'];
							$param['PS_CCY']  = $dataTemp['PS_CCY'];
							$param['PS_FILE'] = $dataTemp['PS_FILE'];

							$param['TRANSACTION_DATA'] = array();
							$selectbulktransaction = $this->_db->select()
	                            ->from('TEMP_BULKTRANSACTION',array('*'))
	                        	->where('BS_ID = ?',$bsid);
	                            
	      					$resultbulktransaction = $this->_db->fetchAll($selectbulktransaction);

							foreach($resultbulktransaction as $row)
							{

								$conf = Zend_Registry::get('config');
								$transferType = $conf['transfer']['type'];
								$transferTypeFlip = array_flip($transferType['code']);

								$param['TRANSACTION_DATA'][] = array(
									'SOURCE_ACCOUNT' 					=> $row['SOURCE_ACCOUNT'],
									'BENEFICIARY_ACCOUNT' 				=> $row['BENEFICIARY_ACCOUNT'],
									'BENEFICIARY_ACCOUNT_CCY' 			=> $row['BENEFICIARY_ACCOUNT_CCY'],
									'BENEFICIARY_ACCOUNT_NAME' 			=> $row['BENEFICIARY_ACCOUNT_NAME'],
									'BENEFICIARY_ALIAS_NAME' 			=> $row['BENEFICIARY_ALIAS_NAME'],
									'BENEFICIARY_EMAIL' 				=> $row['BENEFICIARY_EMAIL'],
									'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['BENEFICIARY_MOBILE_PHONE_NUMBER'],
				// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
									'BENEFICIARY_CITIZENSHIP' 			=> $row['BENEFICIARY_CITIZENSHIP'],
									'BENEFICIARY_RESIDENT' 				=> $row['BENEFICIARY_RESIDENT'],
									'CLR_CODE' 							=> $row['CLR_CODE'],
									'TRANSFER_TYPE' 					=> $transferType['desc'][$transferTypeFlip[$row['TRANSFER_TYPE']]],
									'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
									'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
									'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
									'TRA_REFNO' 						=> $row['TRA_REFNO'],
								);

							}

							$param['_addBeneficiary'] = $addBeneficiary;
							$param['_beneLinkage'] = $beneLinkage;
							$param['_priviCreate'] = $priviCreate;

							$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
							$paymentRef = NULL;
							$result = $BulkPayment->createPayment($param,$paymentRef);

						}

						if ($dataTemp['PS_TYPE'] == '4') {

							if ($data["payment"]["countTrxPB"] == 0)
								$priviCreate = 'CBPI';
							else
								$priviCreate = 'CBPW';

							$param['PS_SUBJECT'] 	= $dataTemp['PS_SUBJECT'];
							$param['PS_EFDATE']  	= Application_Helper_General::convertDate($dataTemp['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
							$param['PS_TYPE'] 		= $this->_paymenttype['code']['bulkcredit'];
							// $param['PS_CCY']  	= $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];
							$param['PS_CCY']  		= $dataTemp['PS_CCY'];
							$param['PS_FILE']    	= $dataTemp['PS_FILE'];

							$param['TRANSACTION_DATA'] = array();

							$selectbulktransaction = $this->_db->select()
	                            ->from('TEMP_BULKTRANSACTION',array('*'))
	                        	->where('BS_ID = ?',$bsid);
	                            
	      					$resultbulktransaction = $this->_db->fetchAll($selectbulktransaction);

							foreach($resultbulktransaction as $row)
							{

								$conf = Zend_Registry::get('config');
								$transferType = $conf['transfer']['type'];
								$transferTypeFlip = array_flip($transferType['code']);

								$param['TRANSACTION_DATA'][] = array(
										'SOURCE_ACCOUNT' 			=> $row['SOURCE_ACCOUNT'],
										'BENEFICIARY_ACCOUNT' 		=> $row['BENEFICIARY_ACCOUNT'],
										'BENEFICIARY_ACCOUNT_CCY' 	=> $row['BENEFICIARY_ACCOUNT_CCY'],
										'BENEFICIARY_ACCOUNT_NAME' 	=> $row['BENEFICIARY_ACCOUNT_NAME'], //ADA
				//						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'], //ADA
										'BENEFICIARY_ALIAS_NAME' 	=> $row['BENEFICIARY_ALIAS_NAME'], //ADA
										'BENEFICIARY_EMAIL' 		=> $row['BENEFICIARY_EMAIL'],
										'BENEFICIARY_ADDRESS' 		=> $row['BENEFICIARY_ADDRESS'], //ADA
										'BENEFICIARY_CITIZENSHIP' 	=> $row['BENEFICIARY_CITIZENSHIP'],
										//'ACBENEF_ADDRESS1' 			=> $row['ACBENEF_ADDRESS1'],
										'BENEFICIARY_RESIDENT' 		=> $row['BENEFICIARY_RESIDENT'],
										'CLR_CODE' 					=> $row['CLR_CODE'],
										'TRANSFER_TYPE' 			=> $transferType['desc'][$transferTypeFlip[$row['TRANSFER_TYPE']]],
										'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
										'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
										'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
										'TRA_REFNO' 				=> $row['TRA_REFNO'],
										'sourceAccountType' 		=> '',

										'BENEFICIARY_BANK_NAME' 	=> $row['BENEFICIARY_BANK_NAME'], //ADA
										'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
										'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
										'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
										'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],
										'LLD_TRANSACTION_PURPOSE'	=> $row['LLD_TRANSACTION_PURPOSE'],
										'TRA_AMOUNTEQ'				=> '',
										'RATE'						=> $row['RATE'],
										'RATE_BUY'					=> $row['RATE_BUY'],
										'BOOKRATE'					=> $row['BOOK_RATE'],

								);

							}

							$addBeneficiary = $this->view->hasPrivilege('BADA');
							$beneLinkage    = $this->view->hasPrivilege('BLBU');

							$param['_addBeneficiary'] = $addBeneficiary;
							$param['_beneLinkage'] = $beneLinkage;
							$param['_priviCreate'] = $priviCreate;

							$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
							$paymentRef = NULL;
							$result = $BulkPayment->createPayment($param,$paymentRef);
							
						}

						if ($dataTemp['PS_TYPE'] == '5') {

							$param['PS_SUBJECT'] = $dataTemp['PS_SUBJECT'];
							$param['PS_EFDATE']  = Application_Helper_General::convertDate($dataTemp['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
							$param['PS_TYPE'] 	= $this->_paymenttype['code']['bulkdebet'];
							$param['PS_CCY']  = $dataTemp['PS_CCY'];
							$param['PS_FILE']    = $dataTemp['PS_FILE'];

							$param['TRANSACTION_DATA'] = array();

							$selectbulktransaction = $this->_db->select()
	                            ->from('TEMP_BULKTRANSACTION',array('*'))
	                        	->where('BS_ID = ?',$bsid);
	                            
	      					$resultbulktransaction = $this->_db->fetchAll($selectbulktransaction);

							foreach($resultbulktransaction as $row)
							{

								$conf = Zend_Registry::get('config');
								$transferType = $conf['transfer']['type'];
								$transferTypeFlip = array_flip($transferType['code']);

								$param['TRANSACTION_DATA'][] = array(
										'SOURCE_ACCOUNT' 			=> $row['SOURCE_ACCOUNT'],
										'BENEFICIARY_ACCOUNT' 		=> $row['BENEFICIARY_ACCOUNT'],
										'BENEFICIARY_ACCOUNT_CCY' 	=> $row['BENEFICIARY_ACCOUNT_CCY'],
										'BENEFICIARY_ACCOUNT_NAME' 	=> $row['BENEFICIARY_ACCOUNT_NAME'],
										'BENEFICIARY_ALIAS_NAME' 	=> $row['BENEFICIARY_ALIAS_NAME'],
										// 'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
										// 'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
										// 'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
										// 'CLR_CODE' 					=> $row['BANK_CODE'],
										'TRANSFER_TYPE' 			=> $transferType['desc'][$transferTypeFlip[$row['TRANSFER_TYPE']]],
										'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
										'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
										'TRA_REFNO' 				=> $row['TRA_REFNO'],
										'sourceAccountType' 		=> '',
								);

							}

							$addBeneficiary = $this->view->hasPrivilege('BADA');
							$beneLinkage    = $this->view->hasPrivilege('BLBU');

							$param['_addBeneficiary'] 	= $addBeneficiary;
							$param['_beneLinkage'] 		= $beneLinkage;
							$param['_priviCreate'] 		= 'IPMO';

							$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
							$paymentRef = NULL;
							$result = $BulkPayment->createPayment($param,$paymentRef);
							
						}

						if ($dataTemp['PS_TYPE'] == '22') {

							$selectbulktransaction = $this->_db->select()
	                            ->from('TEMP_BULKTRANSACTION',array('*'))
	                        	->where('BS_ID = ?',$bsid);
	                            
	      					$resultbulktransaction = $this->_db->fetchAll($selectbulktransaction);

							foreach($resultbulktransaction as $row)
							{

								$conf = Zend_Registry::get('config');
								$transferType = $conf['transfer']['type'];
								$transferTypeFlip = array_flip($transferType['code']);

								$dateFormat			= 'yyyy-MM-dd';
								$dateDBFormat		= $this->_dateDBFormat;
								$addBeneficiary		= $this->view->hasPrivilege('BADA');
								$beneLinkage		= $this->view->hasPrivilege('BLBU');
								$createPB			= $this->view->hasPrivilege('CRIP');
								$createDOM			= $this->view->hasPrivilege('CRDI');
								$createREM			= $this->view->hasPrivilege('CRIR');

								$param['PS_SUBJECT'] 				= $row['PS_SUBJECT'];
								$param['PS_FILE'] 					= $dataTemp['PS_FILE'];
								$param['PS_EFDATE']  				= $dataTemp['PS_EFDATE'];
								$param['PS_TYPE'] 					= $this->_paymenttype['code']['bulkcredit'];
								$param['PS_CCY']  					= $dataTemp['PS_CCY'];
								$param['SOURCE_ACCOUNT']			= $row['SOURCE_ACCOUNT'];
								$param['BENEFICIARY_ACCOUNT'] 		= $row['BENEFICIARY_ACCOUNT'];
								$param['BENEFICIARY_ACCOUNT_CCY'] 	= $row['BENEFICIARY_ACCOUNT_CCY'];
								$param['BENEFICIARY_ACCOUNT_NAME'] 	= $row['BENEFICIARY_ACCOUNT_NAME'];
				//				$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_ALIAS'];
								$param['BENEFICIARY_ALIAS_NAME'] 	= $row['BENEFICIARY_ALIAS_NAME'];
								$param['BENEFICIARY_EMAIL'] 		= $row['BENEFICIARY_EMAIL'];
								$param['BENEFICIARY_ADDRESS'] 		= $row['BENEFICIARY_ADDRESS'];
								$param['BENEFICIARY_CITIZENSHIP'] 	= $row['BENEFICIARY_CITIZENSHIP'];
								$param['CLR_CODE'] 					= $row['CLR_CODE'];
				//				$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BENEFICIARY_BANK_NAME'];
								$param['TRANSFER_TYPE'] 			= $transferType['desc'][$transferTypeFlip[$row['TRANSFER_TYPE']]];
								$param['TRA_AMOUNT'] 				= $row['TRA_AMOUNT'];
								$param['TRANSFER_FEE'] 				= $row['TRANSFER_FEE'];
								$param['TRA_MESSAGE'] 				= $row['TRA_MESSAGE'];
								$param['TRA_REFNO'] 				= $row['TRA_REFNO'];
								$param['_addBeneficiary'] 			= $addBeneficiary;
								$param['_beneLinkage'] 				= $beneLinkage;
								$param["_dateFormat"]				= $dateFormat;
								$param["_dateDBFormat"]				= $dateDBFormat;
								$param["_createPB"]					= $createPB;
								$param["_createDOM"]				= $createDOM;
								$param["_createREM"]				= $createREM;
								$param["sourceAccountType"]			= '';

								$param['BENEFICIARY_BANK_NAME'] 	= $row['BENEFICIARY_BANK_NAME'];
								$param['LLD_CATEGORY'] 				= $row['BENEFICIARY_CATEGORY'];
								$param['CITY_CODE'] 				= $row['BENEFICIARY_CITY_CODE'];
								$param['LLD_BENEIDENTIF'] 			= $row['BENEFICIARY_ID_TYPE'];
								$param['LLD_BENENUMBER'] 			= $row['BENEFICIARY_ID_NUMBER'];

								$SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);
								$result = $SinglePayment->createPayment($param);

							}
							
						}

					}

                    $validated['VALIDATION'] = 1;

					$where['BS_ID = ?'] = $bsid;
					$this->_db->update('TEMP_BULKPSLIP', $validated, $where);
				
                }

                $this->_redirect('/notification/success/index');
                
            }
            else
            {
                
                $this->view->error = true;
	        	$this->view->report_msg = 'Please Checked Selection';
            }

		}

		if ($cancel) {
			
			$bsid = $this->_request->getParam('bs_id');
			$params = $this->_request->getParams();

            $validators = array (
                                    'bsid' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'bsid' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            foreach ($bsid as $key => $value) {
                if(!empty($value)){
                    $success = true;
                }
            }


            if($zf_filter_input->isValid() && $success)
            {

                try
                {
                    foreach ($bsid as $key => $value)
                    {

                        $id = $value;

                        $where['BS_ID = ?'] = $id;
                        $this->_db->delete('TEMP_BULKTRANSACTION',$where);
                        $this->_db->delete('TEMP_BULKPSLIP',$where);

                    }
                    
                }
                catch(Exception $e)
                {
                    $this->_db->rollBack();
                }
                //$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                if(empty($cancel)){
                $this->view->error = true;
	        	$this->view->report_msg = 'Please Checked Selection';
				}
            }

		}

		$filterlist = array('STATUS','TRANSFER_TYPE','SUBJECT','LAST_UPLOADED_BY','LAST_UPLOADED_DATE','EFFECTIVE_DATE','CCY');
		$this->view->filterlist = $filterlist;

		$select2	= $this->_db->select()
			->from(	array('TEMP_BULKPSLIP'),array('*'))
			->where( "PS_TYPE = ?","28")
			->order("PS_CREATED DESC");
			
			
		//$pslipTrxData = $this->_db->fetchAll($selectTrx);

		$filterArr = array(	'STATUS' 					=> array('StringTrim','StripTags'),
							'TRANSFER_TYPE' 			=> array('StringTrim','StripTags'),
							'SUBJECT' 					=> array('StringTrim','StripTags'),
							'LAST_UPLOADED_BY' 			=> array('StringTrim','StripTags'),
							'LAST_UPLOADED_DATE' 		=> array('StringTrim','StripTags'),
							'LAST_UPLOADED_DATE_END' 	=> array('StringTrim','StripTags'),
							'EFFECTIVE_DATE' 			=> array('StringTrim','StripTags'),
							'EFFECTIVE_DATE_END' 		=> array('StringTrim','StripTags'),
							'CCY' 						=> array('StringTrim','StripTags'),
		);
	                      
		$dataParam = array('STATUS','TRANSFER_TYPE','CCY');
		$dataParamValue = array();
		$paramprint = $this->_request->getParams();

		$wherecol =  $this->_request->getParam('wherecol');

		$clean2 = array_diff( $wherecol,$dataParam); 
		$dataParam = array_diff( $wherecol,$clean2);

		foreach ($dataParam as $dtParam)
		{

			if(!empty($wherecol)){
				$dataval = $this->_request->getParam('whereval');

				$order = 0;
				foreach ($wherecol as $key => $value) {
					if($value == "LAST_UPLOADED_DATE"){
							$order--;
						}
					if($dtParam==$value){
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;

				}

			}

		}

		$createarr = $this->_request->getParam('lastuploaded');
		if ($createarr != NULL) {
			$dataParamValue['LAST_UPLOADED_DATE'] 		= $createarr[0];
			$dataParamValue['LAST_UPLOADED_DATE_END'] 	= $createarr[1];
		}

		$createarr = $this->_request->getParam('effective');
		if ($createarr != NULL) {
			$dataParamValue['EFFECTIVE_DATE'] 		= $createarr[0];
			$dataParamValue['EFFECTIVE_DATE_END'] 	= $createarr[1];
		}

		$options = array('allowEmpty' => true);
		$validators = array(
					
						'LAST_UPLOADED_DATE' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'LAST_UPLOADED_DATE_END' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'EFFECTIVE_DATE'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'EFFECTIVE_DATE_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	

						'STATUS'			=> array(),
						'TRANSFER_TYPE'		=> array(),
						'CCY'				=> array(),
						'SUBJECT'			=> array(),
						'LAST_UPLOADED_BY'	=> array(),

						);
	   
	     $zf_filter 	= new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
	     $filter 		= $this->_getParam('filter');
	     $filter_clear 	= $this->_getParam('clearfilter');

	    $datefrom 		= $zf_filter->getEscaped('LAST_UPLOADED_DATE');
		$dateto 		= $zf_filter->getEscaped('LAST_UPLOADED_DATE_END');
		$effectivefrom 	= $zf_filter->getEscaped('EFFECTIVE_DATE');
		$effectiveend 	= $zf_filter->getEscaped('EFFECTIVE_DATE_END');
		$payment 		= $zf_filter->getEscaped('CCY');
		$status 		= $zf_filter->getEscaped('STATUS');
		$transtype 		= $zf_filter->getEscaped('TRANSFER_TYPE');

	     if($filter == null)
		{	
			// $datefrom 		= (date("d/m/Y"));
			// $dateto 		= (date("d/m/Y"));
			// $effectivefrom 	= (date("d/m/Y"));
			// $effectiveend 	= (date("d/m/Y"));
			// $this->view->fDateFrom  	= (date("d/m/Y"));
			// $this->view->fDateTo  		= (date("d/m/Y"));
			// $this->view->effectivefrom  = (date("d/m/Y"));
			// $this->view->effectiveend  	= (date("d/m/Y"));
		}
		
		if($filter_clear == '1'){
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';
			$datefrom = '';
			$dateto = '';

			$this->view->effectivefrom  = '';
			$this->view->effectiveend  	= '';
			$effectivefrom 	= '';
			$effectiveend 	= '';
			
		}
		
		if($filter == null || $filter ==TRUE)
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;	 
			if(!empty($datefrom))
		            {
		            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
						$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
		            }
		            
		    if(!empty($dateto))
		            {
		            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
						$dateto    = $FormatDate->toString($this->_dateDBFormat);
		            }
			
			if(!empty($datefrom) && empty($dateto))
		            $select2->where("DATE(PS_UPLOADED) >= ".$this->_db->quote($datefrom));
		            
		   	if(empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(PS_UPLOADED) <= ".$this->_db->quote($dateto));
		            
		    if(!empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(PS_UPLOADED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
			
			$this->view->effectivefrom 	= $effectivefrom;
			$this->view->effectiveend 	= $effectiveend;	 
			if(!empty($effectivefrom))
            {
            	$FormatDate 	= new Zend_Date($effectivefrom, $this->_dateDisplayFormat);
				$effectivefrom  = $FormatDate->toString($this->_dateDBFormat);	
            }
		            
		    if(!empty($effectiveend))
            {
            	$FormatDate 	= new Zend_Date($effectiveend, $this->_dateDisplayFormat);
				$effectiveend   = $FormatDate->toString($this->_dateDBFormat);
            }
			
			if(!empty($effectivefrom) && empty($effectiveend))
		            $select2->where("DATE(PS_EFDATE) >= ".$this->_db->quote($effectivefrom));
		            
		   	if(empty($effectivefrom) && !empty($effectiveend))
		            $select2->where("DATE(PS_EFDATE) <= ".$this->_db->quote($effectiveend));
		            
		    if(!empty($effectivefrom) && !empty($effectiveend))
		            $select2->where("DATE(PS_EFDATE) between ".$this->_db->quote($effectivefrom)." and ".$this->_db->quote($effectiveend));

			if($filter == TRUE)
		    {		
		    	
			    if($payment != null)
			    {
			    	$this->view->payment = $payment;
			       	$select2->where("PS_CCY LIKE ".$this->_db->quote('%'.$payment.'%'));
			    }
			    
		     	if($transtype != null)
		     	{
			    	$this->view->transtype = $transtype;
			    	$select2->where("PS_TYPE LIKE ".$this->_db->quote($transtype));
		     	}
			    
			    if($status != null)
			    {
			    	$this->view->status = $status;
			    	//$status = 
			    	$select2->where("VALIDATION LIKE ".$this->_db->quote($status));
			    }
			}
		}

		if(!empty($dataParamValue)){

				$this->view->lastuploadedstart 	= $dataParamValue['LAST_UPLOADED_DATE'];
	    		$this->view->lastuploadedend 	= $dataParamValue['LAST_UPLOADED_DATE_END'];
	    		$this->view->effectivestart 	= $dataParamValue['EFFECTIVE_DATE'];
	    		$this->view->effectiveend 		= $dataParamValue['EFFECTIVE_DATE_END'];
	    		
	    	  	unset($dataParamValue['LAST_UPLOADED_DATE_END']);
				unset($dataParamValue['EFFECTIVE_DATE_END']);    	  	

					foreach ($dataParamValue as $key => $value) {
						$wherecol[]	= $key;
						$whereval[] = $value;
					}

		        $this->view->wherecol     = array_unique($wherecol);
	        	$this->view->whereval     = array_unique($whereval);
		     
		      }

		$this->view->filter = $filter;
		$this->paging($select2);



		if($this->_request->isPost() )
		{
$datas = $this->_request->getParams();
			//echo '<pre>';
			//var_dump($datas);die;
			if($this->_custSameUser){
				
				if(!$this->view->hasPrivilege('PRLP')){
					// die('here');
					$error_msg[] = $this->language->_("Error: You don't have privilege to release payment");
					$this->view->error = true;
					$this->view->report_msg = $this->displayError($error_msg);

					$checktoken = false;

				}else{
					// die('sini');

					$challengeCode		= $this->_getParam('challengeCode');

					$inputtoken1 		= $this->_getParam('inputtoken1');
					$inputtoken2 		= $this->_getParam('inputtoken2');
					$inputtoken3 		= $this->_getParam('inputtoken3');
					$inputtoken4 		= $this->_getParam('inputtoken4');
					$inputtoken5 		= $this->_getParam('inputtoken5');
					$inputtoken6 		= $this->_getParam('inputtoken6');

					$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;

					$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$verToken 	= $Token->verify($challengeCode, $responseCode);

					if ($verToken['ResponseCode'] != '00'){
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

						if ($tokenFailed === true)
						{
							$this->_redirect('/default/index/logout');
						}

						$checktoken = false;

					}else{

						$checktoken = true;
					}
				}
			} else{

				$checktoken = true;
			}


			if ($checktoken) {

				$this->_request->getParams();

			$filter = new Application_Filtering();
			$confirm = false;
			$error_msg[] = "";

			$BULK_TYPE 	= $filter->filter($this->_request->getParam('BULK_TYPE'), "BULK_TYPE");			
			//var_dump($BULK_TYPE);die;
		if($BULK_TYPE == '2'){
		//awal

			//adapter profile not defaults
			// if (!empty($adapterDataOtm)) {
			// 	$extension = $adapterDataOtm[0]['FILE_FORMAT'];
			// }
			// else{
			// 	$extension = 'csv';
			// }

			
			$fileName = $adapterDataAutodebit[0]['FILE_PATH'];
	        $fixLength = $adapterDataAutodebit[0]['FIXLENGTH'];
	        $fixLengthType = $adapterDataAutodebit[0]['FIXLENGTH_TYPE'];
	        $fixLengthHeader = $adapterDataAutodebit[0]['FIXLENGTH_HEADER_ORDER'];
	        $fixLengthHeaderName = $adapterDataAutodebit[0]['FIXLENGTH_HEADER_NAME'];
	        $fixLengthContent = $adapterDataAutodebit[0]['FIXLENGTH_CONTENT_ORDER'];
	        //$delimitedWith = $adapterDataOtm[0]['DELIMITED_WITH'];
	        $delimitedWith = '|';

	        if (!empty($adapterDataAutodebit)) {
				$extension = $adapterDataAutodebit[0]['FILE_FORMAT'];
			}
			else{
				$extension = 'txt';	
			}
			
			

			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");
			$PSFILEID 		= $filter->filter($this->_request->getParam('PSFILEID'), "FILE_ID");
			if($adapterDataAutodebit[0]['STATUS'] == '2'){
					$error_msg[] = $this->language->_('Your business profile adapter status is currently suspended. Please contact the Bank Admin for further assistance.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}else if($adapterDataAutodebit[0]['STATUS'] == '3'){
					$error_msg[] = $this->language->_('Your business profile adapter status is currently deleted. Please contact the Bank Admin for further assistance.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}else if(!$ACCTSRC)
			{
				$error_msg[] = $this->language->_('Credited Account cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if(!$PS_EFDATE)
			{
				$error_msg[] = $this->language->_('Error').': '.$this->language->_('Payment Date cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}else if(!empty($PS_EFDATE) && strtotime($PS_EFDATE)< strtotime(date("Y-m-d")))
			{
				$error_msg[] = $this->language->_('Error').': '.$this->language->_('Invalid Payment Date.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if (empty($PSFILEID)) {

				$error_msg[] = $this->language->_('Error').': '.$this->language->_('File ID cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
				
			}
			else
			{
				
			//awal else

				$selectFileId	= $this->_db->select()
					->from(	array('TEMP_BULKPSLIP'),array('*'))
					->where("DATE (PS_CREATED) = DATE(NOW())")
					->where("FILE_ID = ?",$PSFILEID);

				$fileid = $this->_db->fetchAll($selectFileId);		

				$datafile_id = $fileid[0]['FILE_ID'];
				/*
				if ($PSFILEID != 'BYPASSIDFU') {

					if (strtoupper($datafile_id) == strtoupper($PSFILEID)) {
						$error_msg[] = 'Error: File ID already exists.';
						$this->view->error 		= true;
						$this->view->report_msg	= $this->displayError($error_msg);
					}
					
				}else{ */

					$paramSettingID = array('range_futuredate', 'auto_release_payment');

					$settings = new Application_Settings();
					$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
					$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
					$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
					$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

					$adapter = new Zend_File_Transfer_Adapter_Http();
					$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
					$adapter->setDestination ( $this->_destinationUploadDir );
					$extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
					$extensionValidator->setMessage(
														'Error: Extension file must be *.'.$extension
													);

					$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
					$sizeValidator->setMessage(
						'Error: File exceeds maximum size'
					);

					$adapter->setValidators ( array (
						$extensionValidator,
						$sizeValidator,
					));
					
					if ($adapter->isValid ())
					{
						

											$srcData = $this->_db->select()
											->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE','CCY_ID'))
											->where('ACCT_NO = ?', $ACCTSRC)
											->limit(1);

											$acsrcData = $this->_db->fetchRow($srcData);

											if(!empty($acsrcData)){
												$accsrc = $acsrcData['ACCT_NO'];
												$accsrctype = $acsrcData['ACCT_TYPE'];
												$accsccy = $acsrcData['CCY_ID'];
											}

											/*$svcInquiry = new Service_Inquiry($this->_userIdLogin,$ACCTSRC,$acsrcData['ACCT_TYPE']);
											$resultKursEx = $svcInquiry->rateInquiry();

											//rate inquiry for display
											$kurssell = '';
											$kurs = '';
											$book = '';
											if($resultKursEx['ResponseCode']=='00'){
												$kursList = $resultKursEx['DataList'];
												$kurssell = '';
												$kurs = '';

												foreach($kursList as $row){
													if($row["currency"] == 'USD'){
														$row["sell"] = str_replace(',','',$row["sell"]);
														$row["book"] = str_replace(',','',$row["book"]);
														$row["buy"] = str_replace(',','',$row["buy"]);
														$kurssell = $row["sell"];
														$book = $row["book"];
														if($ACCTSRC_CURRECY=='IDR'){
															$kurs = $row["sell"];
														}else{
															$kurs = $row["buy"];
														}
													}
												}

											} */

						// $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
						// $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

						$sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
							//var_dump($sourceFileName);die;
							//	$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
						
							$date = date("dmy");
							$time = date("his");
							$newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
							$fileName = $newFileName;
							//var_dump($fileName);
							//var_dump($newFileName);
							$adapter->addFilter('Rename', $newFileName);
							$newFileName = $this->_destinationUploadDir.$fileName;
						//$adapter->addFilter ( 'Rename',$newFileName  );

						if ($adapter->receive ())
						{	
					
							$data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);
							//var_dump($adapterDataOtm);die;
							//@unlink($newFileName);
							//end

							if (!empty($adapterDataAutodebit)) {
								
										
								//unset header if not fixlength
								if ($fixLength != 1) {
									unset($data[0]);
									unset($data[1]);	
								}
							}
							else{
								$datafile = $data['0'];
										$file_id = $datafile['1'];
										$file_date = $datafile['2'];
										$total_trx = $datafile['3'];
										$totalamount_trx = $datafile['4'];
										$ccy_trx = $datafile['5'];
								//unset defaults
								unset($data[0]);
								unset($data[1]);
								// unset($data[2]);
								// unset($data[3]);
								// unset($data[4]);
								// unset($data[5]);
								// unset($data[6]);
								// unset($data[7]);
							}

							$totalRecords = count($data);

							//proses convert ke order yg benar
							if($totalRecords)
							{

								foreach ($adapterDataAutodebit as $key => $value) {
									$headerOrder[] = $value['HEADER_CONTENT'];
								}

								foreach ($data as $datakey => $datavalue) {

									if (!empty($headerOrder)) {
										foreach ($headerOrder as $key => $value) {

											$headerOrderArr = explode(',', $value);

											if (count($headerOrderArr) > 1) {
												$i = 0;
												$contentStr = '';
												foreach ($headerOrderArr as $key2 => $value2) {

													if ($i != count($headerOrderArr)) {
														$contentStr .= $data[$datakey][$value2].' ';
													}
													$i++;
												}	
												$newData[$datakey][] = $contentStr;
											}
											else{
												$newData[$datakey][] = $data[$datakey][$value];
											}
										}	
									}
									else{
										$newData[$datakey][] = null;
									}
								}
							}

							//check mandatory yang bo setup saat buat business adapter profile
							$mandatoryCheck = $this->validateField($newData, $adapterDataAutodebit);

							//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
							 		 
							//$fixData = $this->autoMapping($newData, $adapterDataOtm);
							$fixData = $data;
							
							if($totalRecords)
							{
							//	if($totalRecords <= $this->_maxRow)
							//	{
									//die('here');
									$rowNum = 0;

									$paramPayment = array( 	"CATEGORY"      	=> "BULK CREDITT",
															"FROM"       		=> "I",
															"PS_NUMBER"     	=> "",
															"PS_SUBJECT"   	 	=> $PS_SUBJECT,
															"PS_EFDATE"     	=> $PS_EFDATE,
															"PSFILEID"			=> $PSFILEID,
															"PS_FILE"     		=> $fileName,
															"_dateFormat"    	=> $this->_dateDisplayFormat,
															"_dateDBFormat"    	=> $this->_dateDBFormat,
															"_addBeneficiary"   => $this->view->hasPrivilege('BADA'),
															"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'),
															"_createPB"     	=> $this->view->hasPrivilege('CBPW'),
															"_createDOM"    	=> $this->view->hasPrivilege('CBPI'),
															"_createREM"    	=> false,
														  );

									$paramTrxArr = array();
									 $totalamount = 0;
									
									
									 
									 
									foreach ( $fixData as $row )
									{
										 //print_r(count($row));
										// if(count($row)==8)
										// {
											//var_dump($row);die;
											//if(!empty($adapterDataAutodebit)){
										//		$data = array(0=>'');
										//		$row = array_merge($data,$row);
											//	}
											$rowNum++;
											$benefAcct 	= trim($row[2]);
											$ccy 		= 'IDR';
											$amount 	= trim($row[3]);
											$email 		= trim($row[4]);
											$purpose 	= '';
											$message 	= trim($row[5]);
											$addMessage = '';
											$type 		= 'PB';
											//$bankCode 	= trim($row[4]);
											$cust_ref 	= trim($row[1]);

											//$TRA_SMS					= trim($row[7]);
											$TRA_SMS	= '';
											$TRA_EMAIL	= '';
											$REFRENCE 	= '';
											
											$totalamount = (int)$totalamount + (int)$amount;
											
											$fullDesc = array(
												'BENEFICIARY_ACCOUNT'		 => $benefAcct,
												'BENEFICIARY_ACCOUNT_CCY' 	 => $ccy,
												'TRA_AMOUNT' 				 => $amount,
												'TRA_MESSAGE' 				 => $message,
												
												'REFNO'	 					 => $addMessage,
												'TRANSFER_TYPE' 			 => $type,
												'BENEFICIARY_EMAIL'			 => $email,
												'CUST_REF'					 => $cust_ref,
											);
											// print_r($fullDesc);die;

											//validate dummy bank
											// $ccy_id_num = Application_Helper_General::getCurrNum('IDR');
											// $account    = new Service_Account($benefAcct,$ccy_id_num);
											// $result     = $account->accountInquiry();
											
											// if($result['ResponseCode'] == '00'){
											// 	$fullDesc['cif'] = $result['Cif'];
											// 	$fullDesc['acctname'] = $result['AccountName'];
											// 	$fullDesc['type'] = $result['ProductType'];
											// 	$fullDesc['productPlan'] = $result['productPlan'];
											// 	$fullDesc['planCode'] = $result['planCode'];
												
											// }else{
												
											// 	// var_dump($benefAcct);die;
											// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Invalid Debited Account ').$benefAcct.'.';
											// 	$this->view->error 		= true;
											// 	$this->view->report_msg	= $this->displayError($error_msg);
											// 	// $this->_redirect('/singledirectdebit/bulk/');
											// }
											// echo "<pre>";
											// print_r($fullDesc);die;

											// validate temp data
											// $currentD = $this->_db->select()
											// 			->from(array('C' => 'TEMP_DIRECTDEBIT'));

											// $currentD->where("CLIENT_REFF = ?",$cust_ref);
											// $currentD = $this->_db->fetchRow($currentD);

											// if($currentD){
											// 	// echo "Client Reff ".$cust_ref." is waiting to approval";die;
											// 	$error_msg[] = $this->language->_('Error').': Client Reff '.$cust_ref.' is waiting to approval.';
											// 	$this->view->error 		= true;
											// 	$this->view->report_msg	= $this->displayError($error_msg);
											// }

											$filter = new Application_Filtering();

											$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
											$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
											$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
											$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
											$ACBENEF_EMAIL 		= $filter->filter($email, "EMAIL");
											$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
											$ACBENEF_ADDRESS	= $filter->filter($bankCity, "ADDRESS");
											//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
											$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");
											$TRANS_PURPOSE 		= '';
											$CUST_REF 			= $filter->filter($cust_ref, "CUST_REF");
											$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

											if($TRANSFER_TYPE == 'RTGS'){
												$chargeType = '1';
												$select = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType);
												$resultSelecet = $this->_db->FetchAll($select);
												$chargeAmt = isset($resultSelecet['0']['CHARGES_AMT']);
											}
											else if($TRANSFER_TYPE == 'SKN'){
												$chargeType1 = '2';
												$select1 = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType1);
												$resultSelecet1 = $this->_db->FetchAll($select1);
												$chargeAmt = isset($resultSelecet1['0']['CHARGES_AMT']);
											}
											else{
												$chargeAmt = '0';
											}

											$filter->__destruct();
											unset($filter);
										//	if($accsccy != 'IDR' ){
												// print_r($kurs);
												// print_r($TRA_AMOUNT_num);die;
										//		$TRA_AMOUNT_NET =	$TRA_AMOUNT_num;
										//		$TRA_AMOUNT_EQ	=  $TRA_AMOUNT_num/$kurs;
										//	}else{
												$TRA_AMOUNT_NET = $TRA_AMOUNT_num;
												$TRA_AMOUNT_EQ = $TRA_AMOUNT_num;
										//	}

											$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
																"TRA_AMOUNT" 				=> $TRA_AMOUNT_NET,
																"TRA_AMOUNTEQ" 				=> $TRA_AMOUNT_EQ,
																"TRANSFER_FEE" 				=> $chargeAmt,
																"TRA_MESSAGE" 				=> $TRA_MESSAGE,
																"TRA_REFNO" 				=> $TRA_REFNO,
																"ACCTSRC" 					=> $ACBENEF,
																"ACBENEF" 					=> $ACCTSRC,
																"ACBENEF_CCY" 				=> 'IDR',
																"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
																"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
																"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,
																"BANK_CODE" 				=> $CLR_CODE,
																"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
																"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
																"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
																"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
																"BANK_NAME" 				=> $BANK_NAME,
																"TRANS_PURPOSE"				=> $TRANS_PURPOSE,
																"PS_NOTIF"					=> $TRA_NOTIF,
																"CUST_REF"					=> $CUST_REF,
																"PS_SMS"					=> $TRA_SMS,
																"PS_EMAIL"					=> $TRA_EMAIL,
																"REFRENCE"					=> $REFRENCE,

															 );
										/*	if($accsccy != 'IDR' ){
												$paramTrx['RATE'] 		=	$kurssell;
												$paramTrx['RATE_BUY'] 	=	$kurs;
												$paramTrx['BOOKRATE'] 	=	$book;
											}
										*/
											array_push($paramTrxArr,$paramTrx);
											
										// }
										// else
										// {
										// //	die('here');
										// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
										// 	$this->view->error 		= true;
										// 	$this->view->report_msg	= $this->displayError($error_msg);
											
											
										// 	break;
										// }
									}
								//}

							//	else
							//	{
							//		$error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
							//		$this->view->error 		= true;
							//		$this->view->report_msg	= $this->displayError($error_msg);
							//	}

								//validate dummy bank
								// foreach($paramTrxArr as $val){
								// 	$ccy_id_num = Application_Helper_General::getCurrNum('IDR');
								// 	$account    = new Service_Account($val['ACBENEF'],$ccy_id_num);
								// 	$result     = $account->accountInquiry();
									
								// 	if($result['ResponseCode'] == '00'){
								// 		// $paramTrxArr['cif'] = $result['Cif'];
								// 		// $paramTrxArr['acctname'] = $result['AccountName'];
								// 		// $paramTrxArr['type'] = $result['ProductType'];
								// 		// $paramTrxArr['productPlan'] = $result['productPlan'];
								// 		// $paramTrxArr['planCode'] = $result['planCode'];										
										
								// 	}else{
										
								// 		// var_dump($benefAcct);die;
								// 		$error_msg[] = $this->language->_('Error').': '.$this->language->_('Invalid Debited Account ').$val['ACBENEF'].'.';
								// 		$this->view->error 		= true;
								// 		$this->view->report_msg	= $this->displayError($error_msg);
								// 		$errcode = "1";break;
								// 	}
								// }

								// validate t_directdebit
								// foreach($paramTrxArr as $val){
								// 	$currentD = $this->_db->select()
								// 				->from(array('C' => 'T_DIRECTDEBIT'));

								// 	$currentD->where("CLIENT_REFF = ?",$val['CUST_REF']);
								// 	$currentD = $this->_db->fetchRow($currentD);

								// 	if(empty($currentD)){
								// 		// echo "Client Reff ".$val['CUST_REF']." is waiting to approval";die;
								// 		$error_msg[] = $this->language->_('Error').': Invalid Client Reff '.$val['CUST_REF'].'.';
								// 		$this->view->error 		= true;
								// 		$this->view->report_msg	= $this->displayError($error_msg);
								// 		$errcode = "1";break;
								// 	}
 
								// }
									
								if($errcode == "1"){
									$confirm = false;
								}else{
									$confirm = true;
								}
											$setting = new Settings();
											$min_bulk		= $setting->getSetting('min_amount_batch');
											$max_bulk		= $setting->getSetting('max_amount_batch');
											
											
											
											if(empty($adapterDataAutodebit)){
												//echo '<pre>';
												//var_dump($fixData);
												//var_dump($this->_maxRow);
												//var_dump($totalRecords);die;
												if($totalRecords > (int)$this->_maxRow)
												{
													$confirm = false;
													$error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
													$this->view->error 		= true;
													$this->view->report_msg	= $this->displayError($error_msg);
												}
											}
											if($totalamount <= $min_bulk || $totalamount >= $max_bulk){
												$confirm = false;
												$error_msg[] = 'Error: The total amount to be imported should not lower than IDR '.Application_Helper_General::DisplayMoney($min_bulk).' or more than IDR '.Application_Helper_General::DisplayMoney($max_bulk);
												$this->view->error 		= true;
												$this->view->report_msg	= $this->displayError($error_msg);
											}
								
											if ($PSFILEID != 'BYPASSIDFU') {
												//var_dump($file_id);
												//var_dump($PSFILEID);die;
												if (strtoupper($PSFILEID) != strtoupper($file_id) && empty($adapterDataAutodebit)) {
													$error = true;
													$confirm = false;
													$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File ID or Uploaded Date not matched. Please check your uploaded file') . '.';
													$this->view->report_msg	= $this->displayError($error_msg);
													$this->view->error      = true;
												}
											}
											
											
											if ($file_date != date('Y-m-d') && empty($adapterDataAutodebit)) {
													$error = true;
													$confirm = false;
													$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File Uploaded Date not matched. Please check your uploaded file') . '.';
													$this->view->report_msg	= $this->displayError($error_msg);
													$this->view->error      = true;
											}
											//var_dump($total_trx);
											//echo '<pre>';
											//var_dump($fixData);
											
											if ($total_trx != $totalRecords && empty($adapterDataAutodebit)) {
													$error = true;
													$confirm = false;
													$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Total file uploaded not matched. Please check your uploaded file') . '.';
													$this->view->report_msg	= $this->displayError($error_msg);
													$this->view->error      = true;
											}

								// if(!empty($error_msg))
								// {

								// 	$resWs = array();

								// 	$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								// 	$resWs = array();
								// 	$resultVal	= $validate->checkCreateBulk($paramPayment, $paramTrxArr,$resWs);
								// 	//$resultVal	= true;
								// 	$payment 		= $validate->getPaymentInfo();

								// 	// Zend_Debug::dump($validate->getErrorMsg(),'err');
								// 	// Zend_Debug::dump($validate->getErrorTrxMsg(),'errT');
								// 	 //die('asd');
									 
								// 	if($validate->isError() === false)	// payment data is valid
								// 	{

								// 		$confirm = true;

								// 		$validate->__destruct();
								// 		unset($validate);
								// 	}
								// 	else
								// 	{
								// 		// $errorMsg 		= $validate->getErrorMsg();
								// 		// $errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								// 		$confirm = true;
								// 		$validate->__destruct();

								// 		unset($validate);
								// 		 //print_r($);
										 
								// 		 //Zend_Debug::dump($errorMsg);die;
								// 		// if($errorMsg)
								// 		// {
								// 		// 	$confirm = true;
								// 		// 	$this->view->PSEFDATE	= $PS_EFDATE;
								// 		// 	$this->view->BULK_TYPE	= $BULK_TYPE;
								// 		// 	$error_msg[] = 'Error: '.$errorMsg;
								// 		// 	$this->view->error 		= true;
								// 		// 	$this->view->report_msg	= $this->displayError($error_msg);
								// 		// }
								// 		// else
								// 		// {
								// 		// 	$confirm = true;
								// 		// }
								// 	}
								// }
							}
							else
							{
								$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}
						}else{
							$error = $adapter->getMessages();
							//var_dump($error);
							//print_r($confirm);die;
						}
					}
					else
					{
						
						$this->view->error = true;
						foreach($adapter->getMessages() as $key=>$val)
						{
							if($key=='fileUploadErrorNoFile')
								$error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
							else
								$error_msg[] = $val;
							break;
						}
						$errors = $this->displayError($error_msg);
						$this->view->report_msg = $errors;
					}




				

			//akhir else
			}
		//var_dump($confirm);die;

		//akhir
		}

				
		//akhir
		}
//die($confirm);	
			if($confirm)
			{
				
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				// echo '<pre>';
				// var_dump($content);die;
				if($BULK_TYPE=='2'){
					$content['sourceAccountType'] = $sourceAccountType;
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
					$sessionNamespace->content = $content;
					$this->_redirect('/singledirectdebit/bulk/confirm');

				}
			}
			//echo '<pre>';
			$params = $this->_request->getParams();
			//var_dump($params);
			
			
				
			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACCTSRC = $ACCTSRC;
			$this->view->PSEFDATE = $PS_EFDATE;
			//var_dump($this->_getParam('PSEFDATE'));die;
			$this->view->PSSUBJECT = $this->_getParam('PSSUBJECT');
			$this->view->ACCTSRC = $this->_getParam('ACCTSRC');
			$this->view->PSEFDATE = $this->_getParam('PSEFDATE');
			$this->view->PSFILEID = $this->_getParam('PSFILEID');
			//$this->view->PSSUBJECT = $this->_getParam('PSSUBJECT');
				
			}
		}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$data = $sessionNamespace->content;
		//Zend_Debug::dump($data);die;

		$conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
			
		if($this->_custSameUser){
			$this->view->token = true;

			$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
			// echo $selectQuery;
			$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

			// var_dump($usergoogleAuth);die; 
			if (!empty($usergoogleAuth)) {
				$this->view->googleauth = true;
				//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
				$settingObj = new Settings();
				$maxtoken = $settingObj->getSetting("max_failed_token");
				$this->view->tokenfail = (int)$maxtoken-1;
				if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
					//die;  
					$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
					
					
					
					$tokenfail = (int)$maxtoken - ((int)$usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
					$this->view->tokenfail = $tokenfail;
				}
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}
			else{
				$this->view->nogoauth = '1';
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}

			

			$this->view->googleauth = true;
		}
		
		
		$sourceAcct = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']];

		$this->view->CCY = $sourceAcct['CCY_ID'];
		if(empty($data['paramPayment']['PS_SUBJECT'])){
			$subject = 'no subject';
		}else{
			$subject = $data['paramPayment']['PS_SUBJECT'];
		}
		$this->view->PS_SUBJECT = $subject;
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->PS_FILEID = $data['paramPayment']['PSFILEID'];
		$this->view->PS_CCY = $data['paramTrxArr'][0]['ACBENEF_CCY'];
		//$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'].' / '.$sourceAcct['ACCT_NAME'].' / '.$sourceAcct['CCY_ID'];
		//$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'];
		// if($sourceAcct['ACCT_ALIAS']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS'];

		$acct_no = $data['paramTrxArr'][0]['ACBENEF'];
		//echo '<pre>';
		//var_dump($data);die;
		$selectaccount = $this->_db->select()
								->from(	array(	'T'				=>'M_CUSTOMER_ACCT'),array('*'))
								->where('T.ACCT_NO =? ',$acct_no);
		$pslipaccount = $this->_db->fetchRow($selectaccount);

		if ($pslipaccount['ACCT_ALIAS_NAME'] != NULL || !empty($pslipaccount['ACCT_ALIAS_NAME'])) {
			$this->view->ACCTSRC = $pslipaccount['ACCT_NO'].' ('.$pslipaccount['CCY_ID'].') / '.$this->_bankName.' / '.$pslipaccount['ACCT_ALIAS_NAME'];
		}else{
			$this->view->ACCTSRC = $pslipaccount['ACCT_NO'].' ('.$pslipaccount['CCY_ID'].') / '.$this->_bankName.' / '.$pslipaccount['ACCT_NAME'];
		}

		
		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];

		$countData['TRANSACTION'] = array();
		foreach($data['paramTrxArr'] as $row)
		{
			$countData['TRANSACTION'][] = array(
				'SOURCE_ACCOUNT' 					=> $row['ACCTSRC'],
				'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
				'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
				'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
				'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
				'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
				'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
				'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
				'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
				'CLR_CODE' 							=> $row['BANK_CODE'],
				'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
				'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
				'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
				'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
				'TRA_REFNO' 						=> $row['TRA_REFNO'],
				'SMS_NOTIF' 						=> $row['SMS_NOTIF'],
				'EMAIL_NOTIF' 						=> $row['EMAIL_NOTIF'],
			);
		}

		$transactionCount 	= count($countData['TRANSACTION']);

		$this->view->countData = $transactionCount;

		$totalAmountData = 0;

		//Zend_Debug::dump($countData['TRANSACTION//']);die;
		if(is_array($countData['TRANSACTION']))
	 	{
	     	foreach ($countData['TRANSACTION'] as $key => $paramTransaction)
	     	{
	     		$totalAmountData = $totalAmountData + $paramTransaction['TRA_AMOUNT'];
	     		
	     	}
	    }

	    $this->view->totalAmountData = $totalAmountData;

	    $uploadDate = date("Y-m-d H:i:s");
	    $createDate = date("Y-m-d H:i:s");
	    $this->view->uploadDate = $uploadDate;

		$chargesAmt = array();
		$totalChargesAmt = 0;

		foreach($data['paramTrxArr'] as $row)
		{
			if(!isset($chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']])) $chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] = 0;
			//$chargesObj = Charges::factory($this->_custIdLogin, $row['TRANSFER_TYPE']);

//			if(empty($row['TRANSFER_TYPE'])){
//				$transferType = '0';
//			}
//			else{
//				$transferType = $row['TRANSFER_TYPE'];
//			}
			$chargesObj = Charges::factory($this->_custIdLogin, isset($row['TRANSFER_TYPE']));

			$paramCharges = array("accsrc" => $row['ACCTSRC'], "transferType" => $row['TRANSFER_TYPE']);
			if(!isset($data['errorTrxMsg'][$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']]))
			{
// 				$chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] += $chargesObj->getCharges($paramCharges);
// 				$totalChargesAmt += $chargesObj->getCharges($paramCharges);
			}
		}
		$this->view->chargesAmt = $chargesAmt;
		$this->view->totalChargesAmt = $totalChargesAmt;

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		//Zend_Debug::dump($data["payment"]["countTrxCCY"]);die;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$settings = new Settings();
		$this->view->cutOffSKN = $settings->getSetting('cut_off_time_skn');
		$this->view->cutOffRTGS = $settings->getSetting('cut_off_time_rtgs');

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/singledirectdebit/bulk/index/cancel/1');
			}

			if ($data["payment"]["countTrxPB"] == 0)
				$priviCreate = 'CBPI';
			else
				$priviCreate = 'CBPW';
			$param['BS_ID'] = $data['paramPayment']['BS_ID'];
			$param['PS_FILEID'] = $data['paramPayment']['PSFILEID'];
			if($data['paramPayment']['PS_SUBJECT'] == ''){
				$subject = 'no subject';
			}else{
				$subject = $data['paramPayment']['PS_SUBJECT'];
			}
			$this->view->PS_SUBJECT = $subject;
			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= 28;
			//$param['PS_CCY']  = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];
			$param['PS_FILE'] = $data['paramPayment']['PS_FILE'];
			$param['PS_CCY']  = $data['paramTrxArr'][0]['ACBENEF_CCY'];
			$param['UPLOAD_DATE'] = $uploadDate;
			$param['CREATE_DATE'] = $createDate;

			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
					'SOURCE_ACCOUNT' 					=> $row['ACBENEF'],
					'BENEFICIARY_ACCOUNT' 				=> $row['ACCTSRC'],
					'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
					'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
					'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
					'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
					'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
					'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
					'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
					'CLR_CODE' 							=> $row['BANK_CODE'],
					'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
					'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
					'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
					'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
					'TRA_REFNO' 						=> $row['TRA_REFNO'],
					'SMS_NOTIF' 						=> $row['SMS_NOTIF'],
					'EMAIL_NOTIF' 						=> $row['EMAIL_NOTIF'],
				);
			}

			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = $priviCreate;

			$sourceAcct = $data['paramTrxArr'][0]['ACCTSRC'];

			$select1	= $this->_db->select()
				->from(array('A' => 'M_CUSTOMER_ACCT'),
					array(
				   		'*'
					)
				)
				->where("A.ACCT_NO	= ?", $sourceAcct);
				

			$datacust1 		= $this->_db->fetchAll($select1);

			$ACCTNO = $datacust1['0']['ACCT_NO'];
			$CCYID	= $datacust1['0']['CCY_ID'];
			$ACCTNAME = $datacust1['0']['ACCT_NAME'];
			$ACCtTYPE = $datacust1['0']['ACCT_TYPE'];
			$ACCTALIAS = $datacust1['0']['ACCT_ALIAS_NAME'];

			$param['ACCTNO'] = $ACCTNO;
			$param['CCYID'] = $CCYID;
			$param['ACCTNAME'] = $ACCTNAME;
			$param['ACCtTYPE'] = $ACCtTYPE;
			$param['ACCTALIAS'] = $ACCTALIAS;

			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
			$paymentRef = NULL;
			
			
			
			if($this->_custSameUser){
										if(!$this->view->hasPrivilege('PRLP')){
											// die('here');
											
											$errMessage = $this->language->_("Error: You don't have privilege to release payment");
											$this->view->error = true;
											$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
										}else{
											
											///google auth
											$challengeCode		= $this->_getParam('challengeCode');

											$inputtoken1 		= $this->_getParam('inputtoken1');
											$inputtoken2 		= $this->_getParam('inputtoken2');
											$inputtoken3 		= $this->_getParam('inputtoken3');
											$inputtoken4 		= $this->_getParam('inputtoken4');
											$inputtoken5 		= $this->_getParam('inputtoken5');
											$inputtoken6 		= $this->_getParam('inputtoken6');

											$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;


											$select3 = $this->_db->select()
												 ->from(array('C' => 'M_USER'));
											$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
											// $select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
											$data2 = $this->_db->fetchRow($select3);

											// $code = $param['googleauth'];
		

											$pga = new PHPGangsta_GoogleAuthenticator();
									    	 //var_dump($data2['GOOGLE_CODE']);
									    	 //var_dump($code);
									    	 //print_r($responseCode);die();
											$setting 		= new Settings();
											$google_duration 	= $setting->getSetting('google_duration');
									        if($pga->verifyCode($data2['GOOGLE_CODE'], $responseCode, $google_duration))
									        {
									        	$datatoken = array(
															'USER_FAILEDTOKEN' => 0
														);

														$wheretoken =  array();
														$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
														$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
														$data = $this->_db->update('M_USER',$datatoken,$wheretoken);
									        	$resultToken = $resHard['ResponseCode'] == '0000';
									        	$tokenAuth = true;
									        }else{
									        	$tokenFailed = $CustomerUser->setLogToken();
									        	$tokenAuth = false;	
									        	$this->view->popauth = true;
									        	if ($tokenFailed === true) {
										 		$this->_redirect('/default/index/logout');
										 	}
									        }
										}
										//var_dump($tokenAuth);
					if($tokenAuth){
						$param['sameuser'] = 1;
						$result = $BulkPayment->createPaymentBatch($param,$paymentRef);
					}else{
						$this->view->error = true;
						// $docErr = $this->displayError($zf_filter->getMessages());
						// print_r($docErr);die;
						$this->view->tokenError = true;
						$docErr = 'Invalid Token';
						$this->view->report_msg = $docErr;
					}					
										
				}else{
					//echo '<pre>';
					//var_dump($param);die; 
					$result = $BulkPayment->createPaymentBatch($param,$paymentRef);
				}
			// echo "<pre>";
			// var_dump($param);
			// var_dump($result);
			// die();
		
			//var_dump($result);die();	

			if($result)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/notification/success');
			}
			else
			{
				$this->view->error = true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/payrollpayment/bulk');
			}
		}

	}	

    private function resData($benefAccount){
			$select	= $this->_db->select()
								->from(array('B'	 			=> 'M_BENEFICIARY'), array('BANK_NAME','BENEFICIARY_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_CITIZENSHIP','BENEFICIARY_RESIDENT','BENEFICIARY_ID_NUMBER','BENEFICIARY_ID_TYPE','BENEFICIARY_CITY_CODE','BENEFICIARY_CATEGORY')
									   );
			$select->where("B.BENEFICIARY_ACCOUNT = ?", $benefAccount);

			$bene = $this->_db->fetchAll($select);
			return $bene;
	}

	

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
				// var_dump($csvData);die;
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}

	private function convertFileToArray($newFileName, $extension, $delimitedWith){

		$file_contents = file_get_contents($newFileName);

        //if csv occured
        if ($extension === 'csv') {

            if (!empty($delimitedWith)) {
                $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
            }
            //if fix length
            else if ($fixLength == 1) {

            	$fileContents = file($newFileName);

            	$contentOrder = $fixLengthContent;

            	// if with header
                if ($fixLengthType == 1) {
                    //karena yg diambil hanya order dri row ke 2 saja
                    $startArrIndex = 1;
                    $surplusIndex = 0;
                }
                else if ($fixLengthType == 3) {
                    //karena yg diambil hanya order dri row ke 1 saja
                    $startArrIndex = 0;
                    $surplusIndex = 1;
                }

                $contentOrderArr = explode(',', $contentOrder);

                if (count($contentOrderArr) > 1) {
                    foreach ($fileContents as $key => $value) {
                        if ($key >= $startArrIndex) {
                            foreach ($contentOrderArr as $key2 => $value2) {
                                //first order, startIndex from 0
                                if ($key2 == 0) {
                                    $startIndex = 0;
                                    $endIndex = (int) $value2 + 1;

                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                                else{
                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                    $endIndex = (int) ($value2 - ($startIndex - 1));

                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                            }       
                        }   
                    }
                }
            }
            else{
                $data = $this->_helper->parser->parseCSV($newFileName);
            }
        }
        //if txt occured
        else if ($extension === 'txt') {
        	$delimitedWith = '|';
           $lines = file($newFileName);

           $checkMt940 = false;
           $checkMt101 = false;
           foreach ($lines as $line) {
               if (strpos($line, '{1:') !== false) {
                   $checkMt940 = true;
               }
               else if (strpos($line, ':20:') !== false) {
                   $checkMt101 = true;
               }
           }

           //if mt940 format
           if ($checkMt940) {
               
                $data = $this->_helper->parser->mt940($newFileName);
                $mtFile = true;
           }
           else if($checkMt101){
                $data = $this->_helper->parser->mt101($newFileName);
                $mtFile = true;
           }
           else{
           		//parse csv jg bs utk txt
                if (!empty($delimitedWith)) {
                    $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
                }
                //if fix length
                else if ($fixLength == 1) {

                    $fileContents = file($newFileName);

	            	$contentOrder = $fixLengthContent;

	            	// if with header
	                if ($fixLengthType == 1) {
	                    //karena yg diambil hanya order dri row ke 2 saja
	                    $startArrIndex = 1;
	                    $surplusIndex = 0;
	                }
	                else if ($fixLengthType == 3) {
	                    //karena yg diambil hanya order dri row ke 1 saja
	                    $startArrIndex = 0;
	                    $surplusIndex = 1;
	                }

	                $contentOrderArr = explode(',', $contentOrder);

	                if (count($contentOrderArr) > 1) {
	                    foreach ($fileContents as $key => $value) {
	                        if ($key >= $startArrIndex) {
	                            foreach ($contentOrderArr as $key2 => $value2) {
	                                //first order, startIndex from 0
	                                if ($key2 == 0) {
	                                    $startIndex = 0;
	                                    $endIndex = (int) $value2 + 1;

	                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
	                                }
	                                else{
	                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
	                                    $endIndex = (int) ($value2 - ($startIndex - 1));

	                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
	                                }
	                            }       
	                        }   
	                    }
	                }                   
                }
                else{
                 	$data = $this->_helper->parser->parseCSV($newFileName, '|');
                }
           }
        }
        //if json occured
        else if ($extension === 'json') {

            $datajson = json_decode($file_contents, 1);
            $i = 0;
            foreach ($datajson as $key => $value) {
                if ($i == 0) {
                    $data[$i] = array_keys($value);
                    $data[$i + 1] = array_values($value);
                }
                else{
                    $data[$i+1] = array_values($value);
                }

                $i++;
            }
        }
        //if xml occured
        else if($extension === 'xml'){

            $xml = (array) simplexml_load_string($file_contents);

             $i = 0;
            foreach ($xml as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if ($i == 0) {
                        $data[$i] = array_keys((array)$value2);
                        $data[$i + 1] = array_values((array)$value2);
                    }
                    else{
                        $data[$i+1] = array_values((array)$value2);
                    }

                    $i++;
                }
            }

            for($i=0; $i<count($data); $i++){
                if ($i > 0) {
                    foreach ($data[$i] as $key => $value) {
                        if (empty($value)) {
                            $data[$i][$key] = null;
                        }
                    }
                }
            }

            // print_r($data);die();
        }
        else if($extension === 'xls' || $extension === 'xlsx'){
            try {
                $inputFileType = IOFactory::identify($newFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($newFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($newFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 1; $row <= $highestRow; $row++){                        
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);
				//$data = array(0=>'');
				//$rowData[0] = array_merge($data,$rowData[0]);
                $data[$row-1] = $rowData[0];
            }
        }

        return $data;
	}

	//validate mandatory
	//validate mandatory
	private function validateField($data, $adapterData){

		foreach ($adapterData as $key => $value) {
			if ($value['MANDATORY'] == 1) {
				$mandatoryArr[] = $value['HEADER_INDEX'];
			}
		}

		$field = array();
		foreach ($data as $key => $value) {

			foreach ($mandatoryArr as $key2 => $value2) {
				if(empty($value[$value2])){
					$field[] = $adapterData[$value2]['HEADER_NAME'];
				}
			}
		}
		return $field;
	}

	//validate 
	private function autoMapping($data, $adapterData){

		foreach ($adapterData as $key => $value) {
			if (!empty($value['HEADER_FUNCTION'])) {

				$mappingTag = explode('_', $value['HEADER_FUNCTION']);

				$functionField[] = array(
					'headerIndex' => $value['HEADER_INDEX'],
					'mappingTag' => $mappingTag[0]
				);
			}
		}

		$newData = array();
		foreach ($functionField as $key => $value) {

			if (!empty($newData)) {

				foreach ($newData as $newdatakey => $newdatavalue) {
					//select m_mapping
					$select = $this->_db->select()
							->from('M_MAPPING',array('*'))
							->where("TAG = ?", $value['mappingTag'])
							->where("TEXT LIKE ?", $newdatavalue[$value['headerIndex']]);
					$mappingData = $this->_db->fetchRow($select);

					if (!empty($mappingData)) {
						$newdatavalue[$value['headerIndex']] = $mappingData['MAPPING_TO'];

					}
					$newData[$newdatakey] = $newdatavalue;
				}
			}
			else{

				foreach ($data as $datakey => $datavalue) {
					//select m_mapping
					$select = $this->_db->select()
							->from('M_MAPPING',array('*'))
							->where("TAG = ?", $value['mappingTag'])
							->where("TEXT LIKE ?", $datavalue[$value['headerIndex']]);
					$mappingData = $this->_db->fetchRow($select);

					if (!empty($mappingData)) {
						$datavalue[$value['headerIndex']] = $mappingData['MAPPING_TO'];

					}
					$newData[$datakey] = $datavalue;
				}
			}
		}

		return $newData;
	}

	public function transfertypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $conf = Zend_Registry::get('config');
		$paymentType = $conf['payment']['type'];
		$paymentTypeFlip = array_flip($paymentType['code']);

        $selectTrx	= $this->_db->select()
        	->distinct()
			->from(	array('TEMP_BULKPSLIP'),array('PS_TYPE'))
			->where( "PS_CATEGORY = ?","BULK PAYMENT");
			
		$ACBENEFArr = $this->_db->fetchAll($selectTrx);

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['PS_TYPE']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['PS_TYPE']."' ".$select.">".$paymentType['desc'][$paymentTypeFlip[$value['PS_TYPE']]]."</option>";
        }

        echo $optHtml;
    }

    public function currencyAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $selectTrx	= $this->_db->select()
        	->distinct()
			->from(	array('TEMP_BULKPSLIP'),array('PS_CCY'))
			->where( "PS_CATEGORY = ?","BULK PAYMENT");
			 
		$ACBENEFArr = $this->_db->fetchAll($selectTrx);

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['PS_CCY']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['PS_CCY']."' ".$select.">".$value['PS_CCY']."</option>";
        }

        echo $optHtml;
    }

     public function statusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $status = array(
            "1"=> $this->language->_('Success') ,
            "2"=> $this->language->_('In Progress'),
            "3"=> $this->language->_('Error')
        );

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($status as $key => $value) {
            
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }
	
	
	 
	public function documentAction()
  	{ 
		

		$this->_helper->layout()->setLayout('newlayout');

		$this->view->suggestionType = $this->_suggestType;

		$change_id = $this->_getParam('BS_ID');

		$select = $this->_db->select()
		                 // ->from('TEMP_BULKPSLIP',array('*'))
		            ->from(array('A' => 'TEMP_BULKPSLIP'),array())
		            ->join(array('B' => 'TEMP_BULKTRANSACTION'),'A.BS_ID = B.BS_ID',array('A.*','B.SOURCE_ACCOUNT','B.BENEFICIARY_ACCOUNT','B.BENEFICIARY_ACCOUNT_NAME','B.BENEFICIARY_ACCOUNT_CCY'))
		            ->where('A.BS_ID = ?',$change_id);
		                
		$resultdata = $this->_db->fetchAll($select);

		$conf = Zend_Registry::get('config');
		$paymentType = $conf['payment']['type'];
		$paymentTypeFlip = array_flip($paymentType['code']);

		$this->view->paymentType = $paymentType;
		$this->view->paymentTypeFlip = $paymentTypeFlip;

		$frontendOptions = array(
		'lifetime' => 86400,
		'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

		$cacheID = 'USERLIST';

		$userlist = $cache->load($cacheID);
		$this->view->pdfUserlist  = $userlist;
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        
            $param = array('CCY_IN' => 'IDR','ACCT_NO'=>$resultdata[0]['SOURCE_ACCOUNT']);
            $AccArr = $CustomerUser->getAccounts($param);
        
         $adapaterprof = 'DEFAULT';
		if($resultdata[0]['PS_TYPE'] == '11'){
		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'payroll');
						//echo $select;
		$adapterDataPayroll = $this->_db->FetchAll($select);
		if(!empty($adapterDataPayroll)){
			$adapaterprof = $adapterDataPayroll['0']['PROFILE_NAME'];
		}
		}
		//var_dump($adapterDataPayroll);die; 
		if($resultdata[0]['PS_TYPE'] == '22'){
		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'manytomany');
		$adapterDataMtm = $this->_db->FetchAll($select);
		if(!empty($adapterDataMtm)){
			$adapaterprof = $adapterDataMtm['0']['PROFILE_NAME'];
		}
		}
		/*$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'emoney');
		$adapterDataEmoney = $this->_db->FetchAll($select);

		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'onetomany');
		$adapterDataOtm = $this->_db->FetchAll($select);

		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'manytoone');
		$adapterDataMto = $this->_db->FetchAll($select); 
		 */
		 
        if(!empty($AccArr)){
            $this->view->src_name = $AccArr['0']['ACCT_NAME'];
        }
		
		
		  $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
		$this->view->adapaterprof            = $adapaterprof;
		$this->view->bs_id            = $resultdata[0]['BS_ID'];
		$this->view->ps_subject       = $resultdata[0]['PS_SUBJECT'];
		$this->view->upload_by        = $resultdata[0]['UPLOADBY'];
		$this->view->ef_date          = $resultdata[0]['PS_EFDATE'];
		$this->view->total_amount     = $resultdata[0]['PS_TOTAL_AMOUNT'];
		$this->view->success_amount   = $resultdata[0]['PS_SUCCESS_AMOUNT'];
		$this->view->failed_amount    = $resultdata[0]['PS_FAILED_AMOUNT']; 
		$this->view->ps_type          = $resultdata[0]['PS_TYPE'];  
		$this->view->ps_ccy           = $resultdata[0]['PS_CCY'];    
		$this->view->ps_suc           = $resultdata[0]['PS_SUCCESS_TXAMOUNT'];  
		$this->view->ps_tot           = $resultdata[0]['PS_TXCOUNT'];  
		$this->view->ps_fai           = $resultdata[0]['PS_FAILED_TXAMOUNT'];  
		$this->view->ps_uploaded      = $resultdata[0]['PS_UPLOADED']; 
		$this->view->validation       = $resultdata[0]['VALIDATION'];
		$this->view->source_acct      = $resultdata[0]['SOURCE_ACCOUNT']; 
		//$this->view->masterBankName = $this->_bankName;
		$this->view->bankname		  = $this->_bankName;
		$this->view->file_id          = $resultdata[0]['FILE_ID'];
		$this->view->beneficiary_acct = $resultdata[0]['BENEFICIARY_ACCOUNT'];
		$this->view->beneficiary_name = $resultdata[0]['BENEFICIARY_ACCOUNT_NAME'];
		$this->view->beneficiary_ccy  = $resultdata[0]['BENEFICIARY_ACCOUNT_CCY'];

		$downloadURL2 = $this->view->url(array('module'=>'newbatchpayment','controller'=>'index','action'=>'downloadtrx2','txt'=>'1','BS_ID'=>$change_id),null,true);

		$this->view->download2 = $this->view->formButton('download','download',array('class'=>'btnwhite hov', 'onclick'=>"window.location = ".$this->_db->quote($downloadURL2).";")); 

		//reupload
		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'payroll');
		$adapterDataPayroll = $this->_db->FetchAll($select);

		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'manytomany');
		$adapterDataMtm = $this->_db->FetchAll($select);

		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'emoney');
		$adapterDataEmoney = $this->_db->FetchAll($select);

		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'onetomany');
		$adapterDataOtm = $this->_db->FetchAll($select);

		$select = $this->_db->select()
		  ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		  ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		  ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		  ->where("CUST_ID = ?",$this->_custIdLogin)
		  ->where("TRF_TYPE = ?", 'manytoone');
		$adapterDataMto = $this->_db->FetchAll($select);

      if($this->_request->isPost())
      {
      
        $filter = new Application_Filtering();
        $confirm = false;
        $error_msg[] = "";
        
        $BULK_TYPE  = $this->_request->getParam('ps_type');
        
        if($BULK_TYPE == '4'){
        //awal
          
              $BS_ID  = $this->_request->getParam('bs_id');

              // $where = array(
              //        'BS_ID = ?' => $BS_ID
              //      );
              // $this->_db->delete('TEMP_BULKTRANSACTION',$where);

              $extension = 'txt';
              $fileName = $adapterDataOtm[0]['FILE_PATH'];
              $fixLength = $adapterDataOtm[0]['FIXLENGTH'];
              $fixLengthType = $adapterDataOtm[0]['FIXLENGTH_TYPE'];
              $fixLengthHeader = $adapterDataOtm[0]['FIXLENGTH_HEADER_ORDER'];
              $fixLengthHeaderName = $adapterDataOtm[0]['FIXLENGTH_HEADER_NAME'];
              $fixLengthContent = $adapterDataOtm[0]['FIXLENGTH_CONTENT_ORDER'];
              //$delimitedWith = $adapterDataOtm[0]['DELIMITED_WITH'];
              $delimitedWith = '|';

              $PS_SUBJECT   = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
              $PS_EFDATE    = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
              $ACCTSRC    = $filter->filter($this->_request->getParam('source_acct'), "ACCOUNT_NO");
              $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID");

              $paramSettingID = array('range_futuredate', 'auto_release_payment');

              $settings = new Application_Settings();
              $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
              $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
              $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
              $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

              $adapter = new Zend_File_Transfer_Adapter_Http();
              $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
              $adapter->setDestination ( $this->_destinationUploadDir );
              $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
              $extensionValidator->setMessage(
                                'Error: Extension file must be *.'.$extension
                              );

              $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
              $sizeValidator->setMessage(
                'Error: File exceeds maximum size'
              );

              $adapter->setValidators ( array (
                $extensionValidator,
                $sizeValidator,
              ));

              if ($adapter->isValid ())
              {

                          $srcData = $this->_db->select()
                          ->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE','CCY_ID'))
                          ->where('ACCT_NO = ?', $ACCTSRC)
                          ->limit(1);

                          $acsrcData = $this->_db->fetchRow($srcData);

                          if(!empty($acsrcData)){
                            $accsrc = $acsrcData['ACCT_NO'];
                            $accsrctype = $acsrcData['ACCT_TYPE'];
                            $accsccy = $acsrcData['CCY_ID'];
                          }

                          $svcInquiry = new Service_Inquiry($this->_userIdLogin,$ACCTSRC,$acsrcData['ACCT_TYPE']);
                          $resultKursEx = $svcInquiry->rateInquiry();

                          //rate inquiry for display
                          $kurssell = '';
                          $kurs = '';
                          $book = '';
                          if($resultKursEx['ResponseCode']=='00'){
                            $kursList = $resultKursEx['DataList'];
                            $kurssell = '';
                            $kurs = '';

                            foreach($kursList as $row){
                              if($row["currency"] == 'USD'){
                                $row["sell"] = str_replace(',','',$row["sell"]);
                                $row["book"] = str_replace(',','',$row["book"]);
                                $row["buy"] = str_replace(',','',$row["buy"]);
                                $kurssell = $row["sell"];
                                $book = $row["book"];
                                if($ACCTSRC_CURRECY=='IDR'){
                                  $kurs = $row["sell"];
                                }else{
                                  $kurs = $row["buy"];
                                }
                              }
                            }

                          }

                $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                $NUMBER = Rand();
                  $random = md5($NUMBER);
                $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
            
                $fileName = substr($newFileName,85);

                $adapter->addFilter ( 'Rename',$newFileName  );

                if ($adapter->receive ())
                { 
                  $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);
                  //var_dump($adapterDataOtm);die;
                  //@unlink($newFileName);
                  //end
                  
                  $dateNow = date("Y-m-d");
                  $dateUpload = $data[0][2];
                  if ($dateUpload != $dateNow) {
                    
                    $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                    $this->view->error    = true;
                    $this->view->report_msg = $this->displayError($error_msg);


                  }else{

                    $psefdatenow = date("Y-m-d");

                    if (!empty($adapterDataOtm)) {
                      //unset header if not fixlength
                      if ($fixLength != 1) {
                        unset($data[0]);
                        unset($data[1]);  
                      }
                    }
                    else{
                      //unset defaults
                      unset($data[0]);
                      unset($data[1]);
                     
                    }

                    $totalRecords = count($data);

                    //proses convert ke order yg benar
                    if($totalRecords)
                    {

                      foreach ($adapterDataOtm as $key => $value) {
                        $headerOrder[] = $value['HEADER_CONTENT'];
                      }

                      foreach ($data as $datakey => $datavalue) {

                        if (!empty($headerOrder)) {
                          foreach ($headerOrder as $key => $value) {

                            $headerOrderArr = explode(',', $value);

                            if (count($headerOrderArr) > 1) {
                              $i = 0;
                              $contentStr = '';
                              foreach ($headerOrderArr as $key2 => $value2) {

                                if ($i != count($headerOrderArr)) {
                                  $contentStr .= $data[$datakey][$value2].' ';
                                }
                                $i++;
                              } 
                              $newData[$datakey][] = $contentStr;
                            }
                            else{
                              $newData[$datakey][] = $data[$datakey][$value];
                            }
                          } 
                        }
                        else{
                          $newData[$datakey][] = null;
                        }
                      }
                    }

                    //check mandatory yang bo setup saat buat business adapter profile
                    $mandatoryCheck = $this->validateField($newData, $adapterDataOtm);

                    //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                         
                    //$fixData = $this->autoMapping($newData, $adapterDataOtm);
                    $fixData = $data;
                    if($totalRecords)
                    {
						//var_dump($this->_maxRow);
						//var_dump($totalRecords);
                      if($totalRecords <= $this->_maxRow)
                      {
                        //die('here');
                        $rowNum = 0;

                        $paramPayment = array(  "CATEGORY"        => "BULK CREDITT",
                                                "FROM"            => "I",
                                                "PS_NUMBER"       => "",
                                                "BS_ID"           => $BS_ID,
                                                "PS_SUBJECT"      => $PS_SUBJECT,
                                                "PS_EFDATE"       => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                "PSFILEID"        => $PSFILEID,
                                                "PS_FILE"         => $fileName,
                                                "_dateFormat"     => $this->_dateDisplayFormat,
                                                "_dateDBFormat"   => $this->_dateDBFormat,
                                                "_addBeneficiary" => $this->view->hasPrivilege('BADA'),
                                                "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
                                                "_createPB"       => $this->view->hasPrivilege('CBPW'),
                                                "_createDOM"      => $this->view->hasPrivilege('CBPI'),
                                                "_createREM"      => false,
                                                );

                        $paramTrxArr = array();
                         
                        foreach ( $fixData as $row )
                        {
                           //print_r(count($row));
                          // if(count($row)==8)
                          // {
                            $rowNum++;
                            $benefAcct  = trim($row[1]);
                            $ccy    = 'IDR';
                            $amount   = trim($row[2]);
                            $purpose  = '';
                            $message  = trim($row[5]);
                            $addMessage = trim($row[6]);
                            $type     = trim($row[3]);
                            $bankCode   = trim($row[4]);
                            $cust_ref   = '';

                            //$TRA_SMS          = trim($row[7]);
                            $TRA_SMS  = '';
                            $TRA_EMAIL  = '';
                            $REFRENCE   = '';

                            $fullDesc = array(
                              'BENEFICIARY_ACCOUNT'    => $benefAcct,
                              'BENEFICIARY_ACCOUNT_CCY'    => $ccy,
                              'TRA_AMOUNT'         => $amount,
                              'TRA_MESSAGE'          => $message,
                              'TRA_PURPOSE'          => $purpose,
                              'REFNO'            => $addMessage,
                              'TRANSFER_TYPE'        => $type,
                              'CLR_CODE'           => $bankCode,
                              'CUST_REF'           => $cust_ref,
                            );
                            // print_r($fullDesc);die;

                            $filter = new Application_Filtering();

                            $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                            $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                            $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                            $ACBENEF      = $filter->filter($benefAcct, "ACCOUNT_NO");
                            $ACBENEF_EMAIL    = $filter->filter($email, "EMAIL");
                            $ACBENEF_CCY    = $filter->filter($ccy, "SELECTION");
                            $ACBENEF_ADDRESS  = $filter->filter($bankCity, "ADDRESS");
                            $CLR_CODE     = $filter->filter($bankCode, "BANK_CODE");
                            $TRANSFER_TYPE    = $filter->filter($type, "SELECTION");
                            $TRANS_PURPOSE    = $filter->filter($purpose, "SELECTION");
                            $CUST_REF       = $filter->filter($cust_ref, "CUST_REF");
                            $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                            if($TRANSFER_TYPE == 'RTGS'){
                              $chargeType = '1';
                              $select = $this->_db->select()
                                      ->from('M_CHARGES_OTHER',array('*'))
                                      ->where("CUST_ID = ?",$this->_custIdLogin)
                                      ->where("CHARGES_TYPE = ?",$chargeType);
                              $resultSelecet = $this->_db->FetchAll($select);
                              $chargeAmt = isset($resultSelecet['0']['CHARGES_AMT']);
                            }
                            else if($TRANSFER_TYPE == 'SKN'){
                              $chargeType1 = '2';
                              $select1 = $this->_db->select()
                                      ->from('M_CHARGES_OTHER',array('*'))
                                      ->where("CUST_ID = ?",$this->_custIdLogin)
                                      ->where("CHARGES_TYPE = ?",$chargeType1);
                              $resultSelecet1 = $this->_db->FetchAll($select1);
                              $chargeAmt = isset($resultSelecet1['0']['CHARGES_AMT']);
                            }
                            else{
                              $chargeAmt = '0';
                            }

                            $filter->__destruct();
                            unset($filter);
                            if($accsccy != 'IDR' ){
                              // print_r($kurs);
                              // print_r($TRA_AMOUNT_num);die;
                              $TRA_AMOUNT_NET = $TRA_AMOUNT_num;
                              $TRA_AMOUNT_EQ  =  $TRA_AMOUNT_num/$kurs;
                            }else{
                              $TRA_AMOUNT_NET = $TRA_AMOUNT_num;
                              $TRA_AMOUNT_EQ = $TRA_AMOUNT_num;
                            }

                            $paramTrx = array("TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                      "TRA_AMOUNT"        => $TRA_AMOUNT_NET,
                                      "TRA_AMOUNTEQ"        => $TRA_AMOUNT_EQ,
                                      "TRANSFER_FEE"        => $chargeAmt,
                                      "TRA_MESSAGE"         => $TRA_MESSAGE,
                                      "TRA_REFNO"         => $TRA_REFNO,
                                      "ACCTSRC"           => $ACCTSRC,
                                      "ACBENEF"           => $ACBENEF,
                                      "ACBENEF_CCY"         => $ACBENEF_CCY,
                                      "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                      "BENEFICIARY_RESIDENT"    => $BENEFICIARY_RESIDENT,
                                      "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,
                                      "BANK_CODE"         => $CLR_CODE,
                                      "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                      "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                      "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                      "BENEFICIARY_CATEGORY"    => $BENEFICIARY_CATEGORY,
                                      "BANK_NAME"         => $BANK_NAME,
                                      "TRANS_PURPOSE"       => $TRANS_PURPOSE,
                                      "PS_NOTIF"          => $TRA_NOTIF,
                                      "CUST_REF"          => $CUST_REF,
                                      "PS_SMS"          => $TRA_SMS,
                                      "PS_EMAIL"          => $TRA_EMAIL,
                                      "REFRENCE"          => $REFRENCE,

                                     );
                            if($accsccy != 'IDR' ){
                              $paramTrx['RATE']     = $kurssell;
                              $paramTrx['RATE_BUY']   = $kurs;
                              $paramTrx['BOOKRATE']   = $book;
                            }
                          
                            array_push($paramTrxArr,$paramTrx);
                            
                          // }
                          // else
                          // {
                          // // die('here');
                          //  $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
                          //  $this->view->error    = true;
                          //  $this->view->report_msg = $this->displayError($error_msg);
                            
                            
                          //  break;
                          // }
                        }
                      }

                      else
                      {
                        $error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }
                      
                      $confirm = true;

                    }
                    else
                    {
                      $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
                      $this->view->error    = true;
                      $this->view->report_msg = $this->displayError($error_msg);
                    }
                    
                  }

                }
              }
              else
              {
                // print_r($adapter->getMessages());die;
                $this->view->error = true;
                foreach($adapter->getMessages() as $key=>$val)
                {
                  if($key=='fileUploadErrorNoFile')
                    $error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                  else
                    $error_msg[] = $val;
                  break;
                }
                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;
              }

        //akhir
        }else if($BULK_TYPE == '5'){
        //awal
              $BS_ID  = $this->_request->getParam('bs_id');

              $extension = 'txt';
              $fileName = $adapterDataMto[0]['FILE_PATH'];
              $fixLength = $adapterDataMto[0]['FIXLENGTH'];
              $fixLengthType = $adapterDataMto[0]['FIXLENGTH_TYPE'];
              $fixLengthHeader = $adapterDataMto[0]['FIXLENGTH_HEADER_ORDER'];
              $fixLengthHeaderName = $adapterDataMto[0]['FIXLENGTH_HEADER_NAME'];
              $fixLengthContent = $adapterDataMto[0]['FIXLENGTH_CONTENT_ORDER'];
              //$delimitedWith = $adapterDataMto[0]['DELIMITED_WITH'];
              $delimitedWith = '|';

              $PS_SUBJECT         = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
              $PS_EFDATE          = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
              $ACBENEF            = $filter->filter($this->_request->getParam('beneficiary_acct'), "ACCOUNT_NO");
              $ACBENEF_BANKNAME   = $filter->filter($this->_request->getParam('beneficiary_name'), "ACCOUNT_NAME");
              $ACBENEF_ALIAS      = $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
              $ACBENEF_CCY        = $filter->filter($this->_request->getParam('beneficiary_ccy'), "SELECTION");
              $PSFILEID           = $filter->filter($this->_request->getParam('file_id'), "FILE_ID");

              $minLen = 10;
              $maxLen = 20;
              $error_msg[] = "";

                  $paramSettingID = array('range_futuredate', 'auto_release_payment');

                  $settings = new Application_Settings();
                  $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
                  $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
                  $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                  $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

                  $adapter = new Zend_File_Transfer_Adapter_Http();

                  $adapter->setDestination ( $this->_destinationUploadDir );

                  $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
                  $extensionValidator->setMessage(
                    'Error: Extension file must be *.'.$extension
                  );

                  // $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                  // $sizeValidator->setMessage(
                  //  'File exceeds maximum size'
                  // );

                  // $adapter->setValidators ( array (
                  //  $extensionValidator,
                  //  $sizeValidator,
                  // ));

                  if ($adapter->isValid ())
                  {
                    // die('ger');
                    $sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
							//var_dump($sourceFileName);die;
							//	$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
						
							$date = date("dmy");
							$time = date("his");
							$newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
							$fileName = $newFileName;
							//var_dump($fileName);
							//var_dump($newFileName);
							$adapter->addFilter('Rename', $newFileName);
							$newFileName = $this->_destinationUploadDir.$fileName;

                    if ($adapter->receive ())
                    {
                      $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                      $dateNow = date("Y-m-d");
                      $dateUpload = $data[0][2];
                      if ($dateUpload != $dateNow) {
                        
                        $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);

                      }else{
                          $psefdatenow = date("Y-m-d");
                    
                          //@unlink($newFileName);
                      
                          if (!empty($adapterDataMto)) {
                            //unset header if not fixlength
                            if ($fixLength != 1) {
                              unset($data[0]);
                              unset($data[1]);  
                            }
                          }
                          else{
                            //unset defaults
                            unset($data[0]);
                            unset($data[1]);
                            
                          }

                          $totalRecords = count($data);

                          //proses convert ke order yg benar
                          if($totalRecords)
                          {

                            foreach ($adapterDataMto as $key => $value) {
                              $headerOrder[] = $value['HEADER_CONTENT'];
                            }

                            foreach ($data as $datakey => $datavalue) {

                              if (!empty($headerOrder)) {
                                foreach ($headerOrder as $key => $value) {

                                  $headerOrderArr = explode(',', $value);

                                  if (count($headerOrderArr) > 1) {
                                    $i = 0;
                                    $contentStr = '';
                                    foreach ($headerOrderArr as $key2 => $value2) {

                                      if ($i != count($headerOrderArr)) {
                                        $contentStr .= $data[$datakey][$value2].' ';
                                      }
                                      $i++;
                                    } 
                                    $newData[$datakey][] = $contentStr;
                                  }
                                  else{
                                    $newData[$datakey][] = $data[$datakey][$value];
                                  }
                                } 
                              }
                              else{
                                $newData[$datakey][] = null;
                              }
                            }
                          }

                          //check mandatory yang bo setup saat buat business adapter profile
                          $mandatoryCheck = $this->validateField($newData, $adapterDataMto);

                          //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                          //$fixData = $this->autoMapping($newData, $adapterDataMto);
                        //  $fixData = $data;
                          //var_dump($data);die('here');
                          //if(empty($fixData)){
                            $fixData = $data;
                            $totalRecords = count($data);
                          //}
                          if($totalRecords)
                          {
                            if($totalRecords <= $this->_maxRow)
                            {
                              // die('here');
                              $rowNum = 0;

                              $paramPayment = array( "CATEGORY"         => "BULK DEBET",
                                                     "FROM"             => "I",
                                                     "PS_NUMBER"        => "",
                                                     "BS_ID"            => $BS_ID,
                                                     "PS_SUBJECT"       => $PS_SUBJECT,
                                                     "PS_EFDATE"        => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                     "PSFILEID"         => $PSFILEID,
                                                     "PS_FILE"          => $fileName,
                                                     "_dateFormat"      => $this->_dateDisplayFormat,
                                                     "_dateDBFormat"    => $this->_dateDBFormat,
                                                     "_addBeneficiary"  => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                                     "_beneLinkage"     => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                                     "_createPB"        => $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
                                                     "_createDOM"       => false,        // cannot create DOM trx
                                                     "_createREM"       => false,        // cannot create REM trx
                                                    );

                              $paramTrxArr = array();
                              
                              foreach ( $fixData as $key=>$row)
                              {
                                // var_dump(count($row));
                                // if(count($row)==5)
                                // {
                                  // die('here');
                                  $rowNum++;
                                  $sourceAcct   = trim($row[1]);
                                  $ccy          = "IDR";
                                  $amount       = trim($row[2]);
                                  $purpose      = '';
                                  $message      = trim($row[3]);
                                  $cust_ref     = '';
                                  $addMessage   = trim($row[4]);

                                  // if(!empty($row[4]) || !empty($row[5])){
                                    $TRA_NOTIF      = '2';
                                  // }else{
                                  //  $TRA_NOTIF      = '1';
                                  // }
                                  $TRA_SMS        = '';
                                  $TRA_EMAIL        = '';
                                  $REFRENCE       = '';


                                  $filter = new Application_Filtering();

                                  $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                                  $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                                  $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                                  // $ACCTSRC       = $filter->filter($sourceAcct, "ACCOUNT_NO");
                                  $ACCTSRC      = $sourceAcct;
                                  $TRANSFER_TYPE    = 'PB';

                                  $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                                  $filter->__destruct();
                                  unset($filter);

                                  $paramTrx = array(  "TRANSFER_TYPE"   => $TRANSFER_TYPE,
                                            "TRA_AMOUNT"    => $TRA_AMOUNT_num,
                                            "TRA_MESSAGE"     => $TRA_MESSAGE,
                                            "TRA_REFNO"     => $TRA_REFNO,
                                            "ACCTSRC"       => $ACCTSRC,
                                            "ACBENEF"       => $ACBENEF,
                                            "ACBENEF_CCY"     => $ACBENEF_CCY,
                                            "ACBENEF_EMAIL"   => '',

                                          // for Beneficiary data, except (bene CCY and email), must be passed by reference
                                            "ACBENEF_BANKNAME"      => &$ACBENEF_BANKNAME,
                                            "ACBENEF_ALIAS"       => &$ACBENEF_ALIAS,
                                            "PS_NOTIF"          => $TRA_NOTIF,
                                            "CUST_REF"          => $cust_ref,
                                            "PS_SMS"          => $TRA_SMS,
                                            "PS_EMAIL"          => $TRA_EMAIL,
                                            "REFRENCE"          => $REFRENCE,
                                          //  "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                          //  "ACBENEF_ADDRESS1"      => $ACBENEF_ADDRESS,
                                          //  "ACBENEF_ADDRESS2"      => &$ACBENEF_ADDRESS2,
                                          //  "ACBENEF_ADDRESS3"      => &$ACBENEF_ADDRESS3,

                                          //  "ORG_DIR"           => $ORG_DIR,
                                          //  "BANK_CODE"         => $CLR_CODE,
                                          //  "BANK_NAME"         => $BANK_NAME,
                                          //  "BANK_BRANCH"         => $BANK_BRANCH,
                                          //  "BANK_ADDRESS1"       => $BANK_ADDRESS1,
                                          //  "BANK_ADDRESS2"       => $BANK_ADDRESS2,
                                          //  "BANK_ADDRESS3"       => $BANK_ADDRESS3,
                                           );

                                  array_push($paramTrxArr,$paramTrx);
                                // }
                                // else
                                // {
                                //  $error_msg[] = $this->language->_('Wrong File Format').'';
                                //  $this->view->error    = true;
                                //  $this->view->report_msg = $this->displayError($error_msg);
                                //  break;
                                // }
                              }
                            }
                            // kalo jumlah trx lebih dari setting
                            else
                            {
                              // die('here1');
                              $error_msg[] = 'The number of rows to be imported should not more than '.$this->_maxRow.'.';
                              $this->view->error    = true;
                              $this->view->report_msg = $this->displayError($error_msg);
                            }
                            
                            $confirm = true;

                          }
                          else //kalo total record = 0
                          {
                            //$error_msg[] = 'Wrong File Format. There is no data on csv File.';
                            $error_msg[] = $this->language->_('Wrong File Format').'.';
                            $this->view->error    = true;
                            $this->view->report_msg = $this->displayError($error_msg);
                          }

                      }
                    }
                  }
                  else
                  {
                    // print_r($adapter->getMessages());die;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                      if($key=='fileUploadErrorNoFile'){
                        $error_msg[] = $this->language->_('File cannot be left blank. Please correct it').'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }else{
                        $error_msg[] = $val;
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      break;
                      }
                    }
                  }

        //akhir     
        }else if($BULK_TYPE == '11'){
        //awal
           
            $extension = 'txt';
            $fileName = $adapterDataPayroll[0]['FILE_PATH'];
            $fixLength = $adapterDataPayroll[0]['FIXLENGTH'];
            $fixLengthType = $adapterDataPayroll[0]['FIXLENGTH_TYPE'];
            $fixLengthHeader = $adapterDataPayroll[0]['FIXLENGTH_HEADER_ORDER'];
            $fixLengthHeaderName = $adapterDataPayroll[0]['FIXLENGTH_HEADER_NAME'];
            $fixLengthContent = $adapterDataPayroll[0]['FIXLENGTH_CONTENT_ORDER'];
            //$delimitedWith = $adapterDataPayroll[0]['DELIMITED_WITH'];
            $delimitedWith = '|';

            $PS_SUBJECT   = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
            $PS_EFDATE    = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
            $ACCTSRC      = $filter->filter($this->_request->getParam('source_acct'), "ACCOUNT_NO");
            $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID"); 
            $BS_ID        = $this->_request->getParam('bs_id');

            $paramSettingID = array('range_futuredate', 'auto_release_payment');

            $settings = new Application_Settings();
            $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
            $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
            $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
            $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $adapter->setDestination ( $this->_destinationUploadDir );
            $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
            $extensionValidator->setMessage(
              'Error: Extension file must be *.'.$extension
            );

            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
            $sizeValidator->setMessage(
              'Error: File exceeds maximum size'
            );

            $adapter->setValidators ( array (
              $extensionValidator,
              $sizeValidator,
            ));

            if ($adapter->isValid ())
            {
              $sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
							//var_dump($sourceFileName);die;
							//	$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
						
							$date = date("dmy");
							$time = date("his");
							$newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
							$fileName = $newFileName;
							//var_dump($fileName);
							//var_dump($newFileName);
							$adapter->addFilter('Rename', $newFileName);
							$newFileName = $this->_destinationUploadDir.$fileName;

              if ($adapter->receive ())
              {
                  $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                  $dateNow = date("Y-m-d");
                  $dateUpload = $data[0][2];
                  if ($dateUpload != $dateNow) {
                    
                    $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                    $this->view->error    = true;
                    $this->view->report_msg = $this->displayError($error_msg);

                  }else{
                      $psefdatenow = date("Y-m-d");

                      //@unlink($newFileName);

                      if (!empty($adapterDataPayroll)) {
                        //unset header if not fixlength
                        if ($fixLength != 1) {
                          unset($data[0]);
                          unset($data[1]);  
                        }
                      }
                      else{
                        
                        unset($data[0]);
                        unset($data[1]);              
                      } 

                      $totalRecords = count($data);
                      
                      //proses convert ke order yg benar
                      if($totalRecords)
                      {

                        foreach ($adapterDataPayroll as $key => $value) {
                          $headerOrder[] = $value['HEADER_CONTENT'];
                        }

                        foreach ($data as $datakey => $datavalue) {

                          if (!empty($headerOrder)) {
                            foreach ($headerOrder as $key => $value) {

                              $headerOrderArr = explode(',', $value);

                              if (count($headerOrderArr) > 1) {
                                $i = 0;
                                $contentStr = '';
                                foreach ($headerOrderArr as $key2 => $value2) {

                                  if ($i != count($headerOrderArr)) {
                                    $contentStr .= $data[$datakey][$value2].' ';
                                  }
                                  $i++;
                                } 
                                $newData[$datakey][] = $contentStr;
                              }
                              else{
                                $newData[$datakey][] = $data[$datakey][$value];
                              }
                            } 
                          }
                          else{
                            $newData[$datakey][] = null;
                          }
                        }
                      }
                      
                      $mandatoryCheck = $this->validateField($newData, $adapterDataPayroll);

                      //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                      $fixData = $this->autoMapping($newData, $adapterDataPayroll);
                      
                      if(empty($fixData)){
                        $fixData = $data; 
                        $totalRecords = count($data);
                      }
                      
                      if($totalRecords)
                      {
                        if($totalRecords <= $this->_maxRow)
                        {
                          $rowNum = 0;

                          $paramPayment = array(  "CATEGORY"        => "BULK PAYROLL",
                                                  "FROM"            => "I",
                                                  "PS_NUMBER"       => "",
                                                  "BS_ID"           => $BS_ID,
                                                  "PS_SUBJECT"      => $PS_SUBJECT,
                                                  "PSFILEID"        => $PSFILEID,
                                                  "PS_EFDATE"       => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                  "PS_FILE"         => $fileName,
                                                  "_dateFormat"     => $this->_dateDisplayFormat,
                                                  "_dateDBFormat"   => $this->_dateDBFormat,
                                                  "_addBeneficiary" => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                                  "_beneLinkage"    => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                                  "_createPB"       => $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
                                                  "_createDOM"      => $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
                                                  "_createREM"      => false,        // cannot create REM trx
                                                  );

                          $paramTrxArr = array();
                          // Zend_Debug::dump($fixData); die;

                          foreach ( $fixData as $row )
                          {
                            // if(count($row)==4)
                            // {
                              // var_dump($row);die;
                              $rowNum++;
                              $benefAcct    = trim($row[1]);
        //                      $benefName    = trim($row[1]);
                              $ccy      = "IDR";
                              $amount     = trim($row[2]);
                              $message    = trim($row[3]);
                              $reference    = '';
                              $sms_notif    = '';
                              $email_notif  = '';
                              $addMessage   = trim($row[4]);
        //                      $email      = trim($row[6]);
        //                      $phoneNumber  = trim($row[7]);
                              $type       = 'PB';
        //                      $bankCode     = trim($row[6]);
                              //$bankName     = trim($row[10]);
          //                    $bankCity = trim($row[10]);
        //                      $benefAdd   = trim($row[9]);
        //                      $citizenship  = strtoupper(trim($row[10]));
                              //$resident = strtoupper(trim($row[11]));

                              /*
                               * Change parameter into document
                               */
                              $fullDesc = array(
                                'BENEFICIARY_ACCOUNT'     => $benefAcct,
                                'BENEFICIARY_NAME'      => '',
                                'BENEFICIARY_ACCOUNT_CCY'   => $ccy,
                                'TRA_AMOUNT'        => $amount,
                                'TRA_MESSAGE'       => $message,
                                'REFNO'           => $addMessage,
                                'SMS_NOTIF'         => $sms_notif,
                                'EMAIL_NOTIF'         => $email_notif,
                                'BENEFICIARY_EMAIL'     => '',
                                'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
                                'TRANSFER_TYPE'       => 'PB',
                                //'CLR_CODE'          => $bankCode,
                                //'BENEFICIARY_BANK_NAME'   => $bankName,
          //                      'BENEFICIARY_CITY' => $bankCity,
                                'BENEFICIARY_ADDRESS'   => '',
                                'BENEFICIARY_CITIZENSHIP'   => ''
                                //'BENEFICIARY_RESIDENT' => $resident
                              );

                              $filter = new Application_Filtering();

                              $SMS_NOTIF    = $filter->filter($sms_notif, "SMS");
                              $EMAIL_NOTIF    = $filter->filter($email_notif, "EMAIL");

                              $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                              $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                              $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                              $ACBENEF      = $filter->filter($benefAcct, "ACCOUNT_NO");
                              $ACBENEF_BANKNAME   = $filter->filter('', "ACCOUNT_NAME");
                              $ACBENEF_ALIAS    = $filter->filter('', "ACCOUNT_ALIAS");
                              $ACBENEF_EMAIL    = $filter->filter('', "EMAIL");
                              $ACBENEF_PHONE    = $filter->filter('', "MOBILE_PHONE_NUMBER");
                              $ACBENEF_CCY    = $filter->filter($ccy, "SELECTION");
                              $ACBENEF_ADDRESS  = $filter->filter('', "ADDRESS");
                              $ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
                              //$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
                              //$BANK_NAME      = $filter->filter($bankName, "BANK_NAME");
                              //$BANK_CITY      = $filter->filter($bankCity, "ADDRESS");
                              //$CLR_CODE     = $filter->filter($bankCode, "BANK_CODE");
                              $TRANSFER_TYPE    = $filter->filter($type, "SELECTION");

                              $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                              if($TRANSFER_TYPE == 'RTGS'){
                                $chargeType = '1';
                                $select = $this->_db->select()
                                        ->from('M_CHARGES_OTHER',array('*'))
                                        ->where("CUST_ID = ?",$this->_custIdLogin)
                                        ->where("CHARGES_TYPE = ?",$chargeType);
                                $resultSelecet = $this->_db->FetchAll($select);
                                $chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
                              }
                              else if($TRANSFER_TYPE == 'SKN'){
                                $chargeType1 = '2';
                                $select1 = $this->_db->select()
                                        ->from('M_CHARGES_OTHER',array('*'))
                                        ->where("CUST_ID = ?",$this->_custIdLogin)
                                        ->where("CHARGES_TYPE = ?",$chargeType1);
                                $resultSelecet1 = $this->_db->FetchAll($select1);
                                $chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
                              }
                              else{
                                $chargeAmt = '0';
                              }

                              $filter->__destruct();
                              unset($filter);

                              $paramTrx = array("TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                        "TRA_AMOUNT"        => $TRA_AMOUNT_num,
                                        "TRANSFER_FEE"        => $chargeAmt,
                                        "TRA_MESSAGE"         => $TRA_MESSAGE,
                                        "TRA_REFNO"         => $TRA_REFNO,
                                        "ACCTSRC"           => $ACCTSRC,
                                        "ACBENEF"           => $ACBENEF,
                                        "ACBENEF_CCY"         => $ACBENEF_CCY,
                                        "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                        "ACBENEF_PHONE"       => $ACBENEF_PHONE,
                                        "SMS_NOTIF"         => $SMS_NOTIF,
                                        "EMAIL_NOTIF"       => $EMAIL_NOTIF,
                                      // for Beneficiary data, except (bene CCY and email), must be passed by reference
                                        "ACBENEF_BANKNAME"      => $ACBENEF_BANKNAME,
                                        "ACBENEF_ALIAS"       => $ACBENEF_ALIAS,
                                        "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // W: WNI, N: WNA
                                      //  "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,   // R: RESIDENT, NR: NON-RESIDENT
                                        "ACBENEF_ADDRESS1"      => $ACBENEF_ADDRESS,
                                        "REFERENCE"         => $reference,
                                      //  "ACBENEF_ADDRESS2"      => &$ACBENEF_ADDRESS2,
                                      //  "ACBENEF_ADDRESS3"      => &$ACBENEF_ADDRESS3,

                                      //  "ORG_DIR"           => $ORG_DIR,
                                        //"BANK_CODE"         => $CLR_CODE,
                                      //  "BANK_NAME"         => $BANK_NAME,
                                      //  "BANK_BRANCH"         => $BANK_BRANCH,
                                      //  "BANK_ADDRESS1"       => $BANK_ADDRESS1,
                                      //  "BANK_ADDRESS2"       => $BANK_ADDRESS2,
                                      //  "BANK_ADDRESS3"       => $BANK_ADDRESS3,
                                       );

                              array_push($paramTrxArr,$paramTrx);
                          }
                        }
                        // kalo jumlah trx lebih dari setting
                        else
                        {
                          $error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
                          $this->view->error    = true;
                          $this->view->report_msg = $this->displayError($error_msg);
                        }

                        $confirm = true;

                      }
                      else //kalo total record = 0
                      {
                        $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }

                  }
              //  
              }

            }
            else
            {
              $this->view->error = true;
              foreach($adapter->getMessages() as $key=>$val)
              {
                if($key=='fileUploadErrorNoFile')
                  $error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                else
                  $error_msg[] = $val;
                break;
              }
              $errors = $this->displayError($error_msg);
              $this->view->report_msg = $errors;
            }

        //akhir
        }else if($BULK_TYPE == '22'){
        //awal
            
            $extension = 'txt';
            $fileName = $adapterDataMtm[0]['FILE_PATH'];
            $fixLength = $adapterDataMtm[0]['FIXLENGTH'];
            $fixLengthType = $adapterData[0]['FIXLENGTH_TYPE'];
            $fixLengthHeader = $adapterDataMtm[0]['FIXLENGTH_HEADER_ORDER'];
            $fixLengthHeaderName = $adapterDataMtm[0]['FIXLENGTH_HEADER_NAME'];
            $fixLengthContent = $adapterDataMtm[0]['FIXLENGTH_CONTENT_ORDER'];
            //$delimitedWith = $adapterDataMtm[0]['DELIMITED_WITH'];
            $delimitedWith = '|';
            $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID"); 
            $BS_ID        = $this->_request->getParam('bs_id');

                $filter   = new Application_Filtering();
                $adapter  = new Zend_File_Transfer_Adapter_Http ();

                $max    = $this->getSetting('max_import_single_payment');

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
                $extensionValidator->setMessage(
                                  'Error: Extension file must be *.'.$extension
                                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                              'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
                            );

                $adapter->setValidators(array($extensionValidator,$sizeValidator));
                // die('here');
                if ($adapter->isValid ())
                {
                  
                 $sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
							//var_dump($sourceFileName);die;
							//	$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
						
							$date = date("dmy");
							$time = date("his");
							$newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
							$fileName = $newFileName;
							//var_dump($fileName);
							//var_dump($newFileName);
							$adapter->addFilter('Rename', $newFileName);
							$newFileName = $this->_destinationUploadDir.$fileName;

                  if ($adapter->receive ())
                  {

                    $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                    $dateNow = date("Y-m-d");
                    $dateUpload = $data[0][2];
                    if ($dateUpload != $dateNow) {
                      
                      $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                      $this->view->error    = true;
                      $this->view->report_msg = $this->displayError($error_msg);

                    }else{
                        $psefdatenow = date("Y-m-d");

                        //@unlink($newFileName);

                        if (!empty($adapterDataMtm)) {
                          //unset header if not fixlength
                          if ($fixLength != 1) {
                            unset($data[0]);  
                            unset($data[1]);
                          }
                        }
                        else{
                          //unset defaults
                          unset($data[0]);
                          unset($data[1]);
                         
                        }

                        $totalRecords = count($data);

                        //proses convert ke order yg benar
                        if($totalRecords)
                        {

                          foreach ($adapterDataMtm as $key => $value) {
                            $headerOrder[] = $value['HEADER_CONTENT'];
                          }

                          foreach ($data as $datakey => $datavalue) {

                            if (!empty($headerOrder)) {
                              foreach ($headerOrder as $key => $value) {

                                $headerOrderArr = explode(',', $value);

                                if (count($headerOrderArr) > 1) {
                                  $i = 0;
                                  $contentStr = '';
                                  foreach ($headerOrderArr as $key2 => $value2) {

                                    if ($i != count($headerOrderArr)) {
                                      $contentStr .= $data[$datakey][$value2].' ';
                                    }
                                    $i++;
                                  } 
                                  $newData[$datakey][] = $contentStr;
                                }
                                else{
                                  $newData[$datakey][] = $data[$datakey][$value];
                                }
                              } 
                            }
                            else{
                              $newData[$datakey][] = null;
                            }
                          }

                          //check mandatory yang bo setup saat buat business adapter profile
                          $mandatoryCheck = $this->validateField($newData, $adapterDataMtm);

                          //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                          $fixData = $this->autoMapping($newData, $adapterDataMtm);

                        }

                        if(empty($fixData)){
                          $fixData = $data;
                          $totalRecords = count($data);
                        }
                        
                        if ($totalRecords && empty($mandatoryCheck)){
                          if($totalRecords <= $max)
                          {
                            $no =0;
                            $paramTrxArray = array();

                            foreach ( $fixData as $columns )
                            {
                              // if(count($columns)==14)
                              // {
                                $params['PAYMENT_SUBJECT']      = trim($columns[1]);
                                $params['SOURCE_ACCT_NO']       = trim($columns[2]);
                                $params['CCY']            = trim('IDR');
                                $params['BENEFICIARY_ACCT_NO']    = trim($columns[3]);
                                $params['BENEFICIARY_ACCT_CCY']   = trim('IDR');
                                $params['AMOUNT']           = trim($columns[4]);
                                $params['MESSAGE']          = trim($columns[8]);
                                $params['TRANS_PURPOSE']      = '';
                                $params['ADDITIONAL_MESSAGE']     = trim($columns[9]);
                                // $origDate = trim($columns[8]);
                                // $date = str_replace('/', '-', $origDate );
                                // $date2 = date_create($date);
                                $params['PAYMENT_DATE']       = trim($columns[7]);
                                $params['TRANSFER_TYPE']      = trim($columns[5]);
                                $params['BANK_CODE']        = trim($columns[6]);
                                $params['CUST_REF']         = '';
                                // if(!empty($columns[12]) || !empty($columns[13])){
                                //  $params['TRA_NOTIF']      = '2';
                                // }else{
                                //  $params['TRA_NOTIF']      = '1';
                                // }
                                $params['BENEFICIARY_NAME']       = '';
                                $params['BENEFICIARY_NAME']       = '';
                                $params['TRA_NOTIF']        = '';
                                $params['PS_SMS']         = '';
                                $params['PS_EMAIL']         = '';
                                $params['TREASURY_NUM']       = '';
                                // $params['LLD_TRANSACTION_PURPOSE']  = trim($columns[14]);
                                $params['LLD_TRANSACTION_PURPOSE']  = '';

                                $PS_SUBJECT     = $filter->filter($params['PAYMENT_SUBJECT'],"PS_SUBJECT");
                                $PS_EFDATE      = $filter->filter($params['PAYMENT_DATE'],"PS_DATE");
                                $TRA_AMOUNT     = $filter->filter($params['AMOUNT'],"AMOUNT");
                                $TRA_MESSAGE    = $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
                                $TRA_REFNO      = $filter->filter($params['ADDITIONAL_MESSAGE'],"TRA_REFNO");
                                $ACCTSRC      = $filter->filter($params['SOURCE_ACCT_NO'],"ACCOUNT_NO");
                                $ACBENEF      = $filter->filter($params['BENEFICIARY_ACCT_NO'],"ACCOUNT_NO");
                                $ACBENEF_BANKNAME   = $filter->filter($params['BENEFICIARY_NAME'],"ACCOUNT_NAME");
                                $ACBENEF_CCY    = $filter->filter($params['CCY'],"SELECTION");
                                $CLR_CODE     = $filter->filter($params['BANK_CODE'], "BANK_CODE");
                                $TRANSFER_TYPE    = $filter->filter($params['TRANSFER_TYPE'], "SELECTION");
                                $CUST_REF       = $filter->filter($params['CUST_REF'], "SELECTION");
                                $BENEFICIARY_ACCT_CCY     = $filter->filter($params['BENEFICIARY_ACCT_CCY'], "BENEFICIARY_ACCT_CCY");
                                $TRA_NOTIF      = $filter->filter($params['TRA_NOTIF'], "TRA_NOTIF");
                                $TRA_SMS      = $filter->filter($params['PS_SMS'], "PS_SMS");
                                $TRA_EMAIL      = $filter->filter($params['PS_EMAIL'], "PS_EMAIL");
                                $TREASURY_NUM   = $filter->filter($params['TREASURY_NUM'], "TREASURY_NUM");
                                $LLD_TRANSACTION_PURPOSE    = $filter->filter($params['LLD_TRANSACTION_PURPOSE'], "LLD_TRANSACTION_PURPOSE");

                                $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                                if($TRANSFER_TYPE == 'RTGS'){
                                  $chargeType = '1';
                                  $select = $this->_db->select()
                                          ->from('M_CHARGES_OTHER',array('*'))
                                          ->where("CUST_ID = ?",$this->_custIdLogin)
                                          ->where("CHARGES_TYPE = ?",$chargeType);
                                  $resultSelecet = $this->_db->FetchAll($select);
                                  $chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

                                  //$param['TRANSFER_FEE'] = $chargeAmt;
                                }
                                else if($TRANSFER_TYPE == 'SKN'){
                                  $chargeType1 = '2';
                                  $select1 = $this->_db->select()
                                          ->from('M_CHARGES_OTHER',array('*'))
                                          ->where("CUST_ID = ?",$this->_custIdLogin)
                                          ->where("CHARGES_TYPE = ?",$chargeType1);
                                  $resultSelecet1 = $this->_db->FetchAll($select1);
                                  $chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

                                  //$param['TRANSFER_FEE'] = $chargeAmt1;
                                }
                                else{
                                  $chargeAmt = '0';
                                  //$param['TRANSFER_FEE'] = $chargeAmt2;
                                }

                                if($BENEFICIARY_ACCT_CCY != $ACBENEF_CCY){
                                  $CROSS_CURR = '2';
                                }else{
                                  $CROSS_CURR = '1';
                                }

                                $paramPayment = array(
                                    "CATEGORY"          => "SINGLE PAYMENT",
                                    "FROM"              => "I",       // F: Form, I: Import
                                    "PS_NUMBER"         => "",
                                    "PS_SUBJECT"        => $PS_SUBJECT,
                                    "BS_ID"             => $BS_ID,
                                    "PS_EFDATE"         => $psefdatenow,
                                    "PSFILEID"          => $PSFILEID,
                                    "PS_FILE"           => $fileName,
                                    "PS_CCY"			=> $ACBENEF_CCY,
                                    "_dateFormat"       => 'yyyy-MM-dd',
                                    "_dateDBFormat"     => $this->_dateDBFormat,
                                    "_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                    "_beneLinkage"      => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                    "_createPB"         => $this->view->hasPrivilege('CRIP'),               // cannot create PB trx
                                    "_createDOM"        => $this->view->hasPrivilege('CRDI'), // privi CDFT (Create Domestic Fund Transfer)
                                    "_createREM"        => $this->view->hasPrivilege('CRIR'),               // cannot create REM trx
                                    "TRA_CCY"           => $BENEFICIARY_ACCT_CCY,
                                    "CROSS_CURR"        => $CROSS_CURR
                                );

                                $paramTrxArr[0] = array(
                                    "TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                    "TRA_AMOUNT"          => $TRA_AMOUNT_num,
                                    "TRANSFER_FEE"        => $chargeAmt,
                                    "TRA_MESSAGE"         => $TRA_MESSAGE,
                                    "TRA_REFNO"           => $TRA_REFNO,
                                    "ACCTSRC"             => $ACCTSRC,
                                    "ACBENEF"             => $ACBENEF,
                                    "ACBENEF_CCY"         => $ACBENEF_CCY,
                                    "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                    "ACBENEF_BANKNAME"    => $ACBENEF_BANKNAME,
            //                        "ACBENEF_ALIAS"     => $ACBENEF_ALIAS,
                                    "ACBENEF_CITIZENSHIP" => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                    //                                "ACBENEF_ADDRESS1"      => $BANK_CITY,
            //                        "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,
                                    "CUST_REF"             => $CUST_REF,
                                    "BENEFICIARY_RESIDENT" => $BENEFICIARY_RESIDENT,
                                    "BANK_CODE"            => $CLR_CODE,
            //                        "BENEFICIARY_BANK_NAME"   => $BANK_NAME,
            //                        "LLD_IDENTICAL"       => "",
            //                        "LLD_CATEGORY"        => "",
            //                        "LLD_RELATIONSHIP"      => "",
            //                        "LLD_PURPOSE"         => "",
            //                        "LLD_DESCRIPTION"       => "",
                                    "LLD_TRANSACTION_PURPOSE" => $LLD_TRANSACTION_PURPOSE,
                                    "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                    "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                    "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                    "BENEFICIARY_ACCT_CCY"    => $BENEFICIARY_ACCT_CCY,
                                    "PS_NOTIF"                => $TRA_NOTIF,
                                    "PS_SMS"                  => $TRA_SMS,
                                    "PS_EMAIL"                => $TRA_EMAIL,
                                    "REFERENCE"               => $TREASURY_NUM,

                                    "BANK_NAME"   => $BANK_NAME,

                                );

                                $arr[$no]['paramPayment'] = $paramPayment;
                                $arr[$no]['paramTrxArr'] = $paramTrxArr;

                                $paramTrx = array(
                                    "PS_SUBJECT"        => $PS_SUBJECT,
                                    "TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                    "TRA_AMOUNT"        => $TRA_AMOUNT_num,
                                    "TRANSFER_FEE"        => $chargeAmt,
                                    "TRA_MESSAGE"         => $TRA_MESSAGE,
                                    "TRA_REFNO"         => $TRA_REFNO,
                                    "ACCTSRC"           => $ACCTSRC,
                                    "ACBENEF"           => $ACBENEF,
                                    "ACBENEF_CCY"         => $ACBENEF_CCY,
                                    "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                    "ACBENEF_BANKNAME"      => $ACBENEF_BANKNAME,
            //                        "ACBENEF_ALIAS"       => $ACBENEF_ALIAS,
                                    "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                    //                                "ACBENEF_ADDRESS1"      => $BANK_CITY,
            //                        "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,
                                    "CUST_REF"          => $CUST_REF,
                                    "BENEFICIARY_RESIDENT"    => $BENEFICIARY_RESIDENT,
                                    "BANK_CODE"         => $CLR_CODE,
            //                        "BENEFICIARY_BANK_NAME"   => $BANK_NAME,
            //                        "LLD_IDENTICAL"       => "",
            //                        "LLD_CATEGORY"        => "",
            //                        "LLD_RELATIONSHIP"      => "",
            //                        "LLD_PURPOSE"         => "",
            //                        "LLD_DESCRIPTION"       => "",
                                    "LLD_TRANSACTION_PURPOSE" => $LLD_TRANSACTION_PURPOSE,
                                    "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                    "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                    "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                    "BENEFICIARY_ACCT_CCY"    => $BENEFICIARY_ACCT_CCY,
                                    "PS_NOTIF"        => $TRA_NOTIF,
                                    "PS_SMS"          => $TRA_SMS,
                                    "PS_EMAIL"        => $TRA_EMAIL,
                                    "REFERENCE"         => $TREASURY_NUM,

                                    "BANK_NAME"   => $BANK_NAME,

                                );

                                array_push($paramTrxArray,$paramTrx);
                              // }
                              // else
                              // {
                              //  // die('ge');
                              //  $this->view->error    = true;
                              //  break;
                              // }
                              $no++;
                            }

                            if(!$this->view->error)
                            {
                             
                              $i = 0;
                              foreach($resWs as $key=>$dataAcctType){
                                //Zend_Debug::dump($dataAcctType);
                                $arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
                              }

                              $sourceAccountType  = $resWs['accountType'];

                              // echo "<pre>";
                              // var_dump($arr);
                              // die();

                              $content['payment'] = $payment;
                              $content['arr']   = $arr;
                              $content['errorTrxMsg']   = $errorTrxMsg;
                              $content['sourceAccountType']   = $sourceAccountType;
                              $content['paramPayment'] = $paramPayment;
                              $content['paramTrxArray'] = $paramTrxArray;

                              $sessionNamespace = new Zend_Session_Namespace('confirmImportCreditBatch');
                              $sessionNamespace->content = $content;

                              $this->_redirect('/singlepayment/importbatch/confirm');
                            }

                          }
                          else
                          {
                            // die('here');
                            $this->view->error2 = true;
                            $this->view->max  = $max;
                          }
                        }else{
                          // die('here1');
                          $this->view->error = true;

                          if (!empty($adapter->getMessages())) {
                            $error_msg = array($adapter->getMessages());
                          }

                          if (!empty($mandatoryCheck)) {
                            foreach ($mandatoryCheck as $key => $value) {
                              array_push($error_msg, $value.' Field Cannot be left blank');
                            }
                          }

                          $this->view->report_msg = $this->displayError($error_msg);
                        }

                    }
   
                  }
                }
                else
                {
                  // die('here3');
                  $this->view->error = true;
                  $error_msg = array($adapter->getMessages());
                  $this->view->report_msg = $this->displayError($error_msg);
                }

        //akhir
        }

        if($confirm)
        {
          $content['paramPayment'] = $paramPayment;
          $content['paramTrxArr']  = $paramTrxArr;
          $content['errorTrxMsg']  = $errorTrxMsg;
          $content['payment'] = $payment;
          if($BULK_TYPE=='4'){
            $content['sourceAccountType'] = $sourceAccountType;
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
            $sessionNamespace->content = $content;
            $this->_redirect('/multicredit/bulkbatch/confirm');

          }else if($BULK_TYPE=='5'){
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
            $sessionNamespace->content = $content;
            $this->_redirect('/multidebet/bulkbatch/confirm');
            
          }else if($BULK_TYPE=='11'){
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
            $sessionNamespace->content = $content;
            $this->_redirect('/payrollpayment/bulk/confirm');
          }
        }
        
      }

    }
	

} 