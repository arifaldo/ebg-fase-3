<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'General/SystemBalance.php';

class Popup_CheckbalanceController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti
	
	public function initController(){	
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction(){
	
		$fields = array(
						'acctno'  		=> array(	'field' 	=> 'ACCT_NO',
													'label' 	=> 'Account Number',
													'sortable' 	=> true),
						'bene_name'  	=> array(	'field' 	=> 'ACCT_NAME',
													'label' 	=> 'Account Name',
													'sortable' 	=> true),
						'acct_name'  	=> array(	'field' 	=> 'ACCT_NAME',
													'label' 	=> 'Account Name',
													'sortable' 	=> true),
						'ccy'   		=> array(	'field'    => 'CCY_ID',
													'label'    	=> 'CCY',
													'sortable' 	=> true),
						'effective'   	=> array(	'field'    => '',
													'label'    => 'Effective Balance',
													'sortable' => true),
						'status'   		=> array(	'field'    => '',
													'label'    => 'Account Status',
													'sortable' => true),
				);
		
		/*
			ACCOUNT_BANK_NAME 	= $systemBalance->getCoreAccountName();
			EFFECTIVEBALANCE 	= $systemBalance->getEffectiveBalance();
			
		*/
		
		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$resultaccounts = $CustomerUser->getAccounts();
		
		$date = date("Y-m-d  H:i:s ");
		//2012-08-10 00:00:00.000
		$dateFormat = Zend_Registry::get('date');
		$dateFormat = $dateFormat['database']['default']['format'];
		
		if(is_array($resultaccounts)){
			foreach ($resultaccounts as $key => $row){
				
				$data[$key]['ACCT_NO'] 			= $row["ACCT_NO"];
				$data[$key]['ACCT_ALIAS_NAME'] 	= $row["ACCT_ALIAS_NAME"];
				$data[$key]['CCY_ID'] 			= $row["CCY_ID"];
				
				$systemBalance = new SystemBalance($this->_custIdLogin,$row["ACCT_NO"],Application_Helper_General::getCurrNum($row["CCY_ID"]));
				$rr = $systemBalance->checkBalance();
					
				$data[$key]['ACCOUNT_BANK_NAME'] = $systemBalance->getCoreAccountName();
				$data[$key]['EFFECTIVE_BALANCE'] = Application_Helper_General::displayMoney($systemBalance->getEffectiveBalance());
				$data[$key]['MESSAGE'] 			 = Application_Helper_General::convertDate($date, $this->view->displayDateTimeFormat,$dateFormat);
				
			}
			
		}
		
		$this->view->nowDate	= Application_Helper_General::convertDate($date, $this->view->displayDateTimeFormat,$dateFormat);
		$this->view->resultdata = $data;
	}
}
