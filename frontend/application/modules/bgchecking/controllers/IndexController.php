<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'General/Gclient.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/Getest/lib/class.geetestlib.php';
require_once 'SGO/Extendedmodule/Getest/config/config.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';

class bgchecking_IndexController extends Application_Main
{
	public function initController()
	{
		$this->_helper->_layout->setLayout('newlayout');
	}

	public function indexAction()
	{
		$bgnumb = $this->_getParam('bgnumb');
		$pass = $this->_getParam('password');

		//---------------------- GENERATE CAPTCHA IMAGE  --------------------------------------
		$captcha = Application_Captcha::generateCaptcha();
		$this->view->captchaId = $captcha['id']; //returns the ID given to session &amp; image
		$this->view->captImgDir = $captcha['imgDir'];
		//---------------------	END GENERATE CAPTCHA IMAGE -------------------------------------
		$confirmPage = $this->_getParam('confirmPage', 1);

		$app = Zend_Registry::get('config');
		$setting = new Settings();

		$this->view->urlFo = $setting->getSetting('url_fo');

		if ($app['captcha']['emulate'] == 1) {
			$this->view->capchahide = 'block';
		} else {
			$this->view->capchahide = 'none';
		}

		$this->view->bgnumb = empty($bgnumb) ? $this->_getParam('bgnumber') : $bgnumb;

		if ($this->_request->isPost()) {

			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;
			//$GtSdk = new GeetestLib(CAPTCHA_ID, PRIVATE_KEY);

			/*if ($app['captcha']['emulate'] == 1){
				$captchaValid = true;
			}else{

				if ($_SESSION['gtserver'] == 1) {   //服务器正常
					$data = array();
					$resultcapca = $GtSdk->success_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'], $data);
					if ($resultcapca) {

						$captchaValid = true;
						//echo '{"status":"success"}';
					} else{
						$captchaValid = false;
						// echo 'here';
						//echo '{"status":"fail"}';
					}
				}else{  //服务器宕机,走failback模式
					if ($GtSdk->fail_validate($_POST['geetest_challenge'],$_POST['geetest_validate'],$_POST['geetest_seccode'])) {
						$captchaValid = true;
						//echo '{"status":"success1"}';
					}else{
						$captchaValid = false;
						// echo 'here1';
						// echo '{"status":"fail1"}';
					}
				}

			}*/
			$filters = array(
				'bgnumb' => array('StripTags', 'StringTrim', 'StringToUpper'),
				'password' => array(),
			);

			$validators = array(
				'bgnumb'         => array(
					'NotEmpty',
					//					'Alnum',
					'messages' => array(
						$this->language->_('bgnumb cannot be empty'),
						//						$this->language->_('Invalid User ID Format'),
					),
				),
				'password'         => array(
					'NotEmpty',
					//					'Alnum',
					'messages' => array(
						$this->language->_('pass cannot be empty'),
						//						$this->language->_('Invalid Customer ID Format'),
					),
				),
				
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_getAllParams(), $this->_optionsValidator);
			
			$bgnumb = $zf_filter_input->bgnumb;
			$pass   = $zf_filter_input->password;
			//if($confirmPage == 1 && $captchaValid)
			if ($bgnumb == null) {
				$this->view->error = true;
				$errors = "Bank Guarantee Number field cannot be empty";
				$this->view->error_bgnumb = $errors;
			} 
			
			if ($pass == null) {
				$this->view->error = true;
				$errors = "Password field cannot be empty";
				$this->view->error_pass = $errors;
			}
			
			if ($confirmPage == 1 && Application_Captcha::validateCaptcha($this->_getParam('captcha')) === true) {
				if ($bgnumb == null) {
					$this->view->error = true;
					$errors = "Bank Guarantee Number field cannot be empty";
					$this->view->error_bgnumb = $errors;
				} else if ($pass == null) {
					$this->view->error = true;
					$errors = "Password field cannot be empty";
					$this->view->error_pass = $errors;
				} else {

					$bgdata		 = $this->_db->select()
						->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
						->where('A.BG_NUMBER = ?', $bgnumb)
						->query()->fetchAll();

					$AESMYSQL = new Crypt_AESMYSQL();
					$BG_NUMBER     = urldecode($bgdata[0]['PASS_CHECKING']);
					$resultDecrypt = $AESMYSQL->decrypt($BG_NUMBER, '');
					
					if ($pass !== $resultDecrypt) {
						$this->view->error = true;
						$errors = "Password Salah";
						$this->view->error_pass = $errors;
						return false;
					}

					if (!empty($bgdata)) {

						$data = $bgdata['0'];

						$statusbg = $data['BG_STATUS'];

						if ($statusbg == '1' || $statusbg == '2' || $statusbg == '3' || $statusbg == '4') {
							$this->view->error = true;
							$errors = "Data not available";
							$this->view->error_bgnumb = $errors;
						} else {
							$_SESSION['pass_checking'] = $pass;

							$this->setbackURL('/bgchecking/index');
							$this->_redirect('/bgchecking/detail/index/bgnumb/' . $bgnumb);
						}
					} else {
						$this->view->error = true;
						$errors = "Data not available";
						$this->view->error_bgnumb = $errors;
					}
				}
			} else {
				if (Application_Captcha::validateCaptcha($this->_getParam('captcha')) == false) {
					$errors = $this->language->_('Invalid Captcha');
					$this->view->error_captcha = $errors;
				}
			}
		}

		$this->view->confirmPage = $confirmPage;
	}
}
