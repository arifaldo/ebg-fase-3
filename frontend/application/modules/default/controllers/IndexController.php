<?php
require_once 'General/Settings.php';
require_once 'General/CustomerUser.php';
require_once 'General/Login.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Gclient.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/Getest/lib/class.geetestlib.php';
require_once 'SGO/Extendedmodule/Getest/config/config.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Service/Account.php';
require_once 'SGO/Helper/Encryption.php';
require_once 'Crypt/AESMYSQL.php';

// require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class IndexController extends Application_Main
{

	public function getUserIP()
	{
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];

		if (filter_var($client, FILTER_VALIDATE_IP)) {
			$ip = $client;
		} elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
			$ip = $forward;
		} else {
			$ip = $remote;
		}

		return $ip;
	}

	public function get_browser_name($user_agent)
	{
		if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
		elseif (strpos($user_agent, 'Edge')) return 'Edge';
		elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
		elseif (strpos($user_agent, 'Safari')) return 'Safari';
		elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
		elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

		return 'Other';
	}

	public function initController()
	{
		//$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
	}


	public function testemailAction()
	{

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		try {
			Application_Helper_Email::sendEmail('mahabatan62@gmail.com', 'User is Locked Notification', 'USER LOGIN');
		} catch (Exception $e) {
			echo '<pre>';
			print_r($e);
			die;
		}


		/*$psnumber = $this->_getParam('a');
		$email = $this->_getParam('e');
		$sqlTransaction  	= "SELECT * FROM T_TRANSACTION WHERE PS_NUMBER = ? ";
		//$transactionData 				= $this->_db->fetchAll($sqlTransaction, (string) 'P20200615QTDQ7BRQW'); 
		$transactionData 				= $this->_db->fetchAll($sqlTransaction, (string) $psnumber);
		//var_dump($transactionData);die;
		$transactionData = $transactionData;
		$cu = new CustomerUser($this->_customerId, $this->_userId);
		$emailList = array();
		//$emailList[0]['USER_EMAIL'] = 'benny@sgo.co.id';
		$emailList[0]['USER_EMAIL'] = $email;
		$sendTransfer = array();
		$sendTransfer->error_code = '0000';
		//var_dump($transactionData);die;
		if (!empty($emailList) && $emailList != '') {
			//var_dump($transactionData);die;
			try {
				Application_Helper_Email::sendEmail('fikri@sgo.co.id', 'User is Locked Notification', 'USER LOGIN');
				$cu->sendEmailPaymentNotification($transactionData, $sendTransfer, $emailList); //send email payment notification
			} catch (Exception $e) {
				echo '<pre>';
				var_dump($e);
				die;
			}
		}*/
	}


	public function historycastAction()
	{

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$psnumber = $this->_getParam('a');
		$email = $this->_getParam('e');
		$sqlTransaction  	= "SELECT
  SUM(b.`TRA_AMOUNT`) AS total,
  a.*
FROM
  `T_PSLIP` AS a
  LEFT JOIN `T_TRANSACTION` AS b
    ON a.`PS_NUMBER` = b.`PS_NUMBER`
WHERE a.`CUST_ID` = ?
AND DATE(a.`PS_EFDATE`)<=DATE('2020-09-15')
GROUP BY a.`PS_EFDATE`";
		//$transactionData 				= $this->_db->fetchAll($sqlTransaction, (string) 'P20200615QTDQ7BRQW'); 
		$transactionData 				= $this->_db->fetchAll($sqlTransaction, (string) 'FIF00A!');
		//var_dump($transactionData);die;
		foreach ($transactionData as $key => $value) {
			$this->_db->insert('T_FORECAST', array(
				'F_DATE' => $value['PS_EFDATE'],
				'F_CUST_ID' => $value['CUST_ID'],
				'F_AMOUNT' => $value['TRA_AMOUNT'],
				'F_BANK_CODE' => '008'
			));
		}
	}


	public function convertkeyAction()
	{

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();


		$sqlTransaction  	= "SELECT * FROM M_APIKEY";
		//$transactionData 				= $this->_db->fetchAll($sqlTransaction, (string) 'P20200615QTDQ7BRQW'); 
		$transactionData 				= $this->_db->fetchAll($sqlTransaction);
		//var_dump($transactionData);die;
		$transactionData = $transactionData;
		$cu = new CustomerUser($this->_customerId, $this->_userId);
		$fieldArr = array('account_currency', 'account_name', 'account_number');
		foreach ($transactionData as $key) {
			if (!in_array($key['FIELD'], $fieldArr)) {

				$value = array('VALUE' => $cu->sslencrypt($key['VALUE']));
				$wherearr = array(
					'APIKEY_ID = ?' => $key['APIKEY_ID'],
					'FIELD = ?'	=> $key['FIELD']
				);
				try {
					//echo '<pre>';
					//	var_dump($value);
					//		var_dump($wherearr);
					$customerupdate = $this->_db->update('M_APIKEY', $value, $wherearr);
				} catch (Exception $e) {
					echo '<pre>';
					var_dump($e);
					die;
				}
			}
		}

		$sqlTransaction  	= "SELECT * FROM M_APICREDENTIAL";
		//$transactionData 				= $this->_db->fetchAll($sqlTransaction, (string) 'P20200615QTDQ7BRQW'); 
		$transactionData 				= $this->_db->fetchAll($sqlTransaction);
		//var_dump($transactionData);die;
		$transactionData = $transactionData;
		$cu = new CustomerUser($this->_customerId, $this->_userId);
		//$fieldArr = array('account_currency','account_name','account_number');
		foreach ($transactionData as $key) {
			//if(!in_array($key['FIELD'], $fieldArr)){

			$value = array(
				'SENDER_ID' => $cu->sslencrypt($key['SENDER_ID']),
				'AUTH_USER' => $cu->sslencrypt($key['AUTH_USER']),
				'AUTH_PASS' => $cu->sslencrypt($key['AUTH_PASS']),
				'SIGNATURE_KEY' => $cu->sslencrypt($key['SIGNATURE_KEY'])

			);
			$wherearr = array(
				'ID = ?' => $key['ID']
			);
			try {
				//echo '<pre>';
				//	var_dump($value);
				//		var_dump($wherearr);
				$customerupdate = $this->_db->update('M_APICREDENTIAL', $value, $wherearr);
			} catch (Exception $e) {
				echo '<pre>';
				var_dump($e);
				die;
			}
			//	}

		}
	}


	public function testredAction()
	{
		$redis = new Redis();
		$redis->connect('127.0.0.1', 6379);
		echo "Connection to server sucessfully";
		//check whether server is running or not 
		echo "Server is running: " . $redis->ping();
		die;
		//   try{
		//   if( $socket = fsockopen( $host, $port, $errorNo, $errorStr )){
		//   	if( $errorNo ){
		//   	  throw new RedisException(“Socket cannot be opened”);
		//   	}	
		//   }
		// }catch( Exception $e ){
		//   echo $e -> getMessage( );
		// }

	}
	public function testrabbitAction()
	{

		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);

		$selectTrx1	= $this->_db->select()
			//->from(	array('TEMP_BULKPSLIP'),array('*'))
			->from(array('P' => 'TEMP_BULKPSLIP'), array('P.*'))
			->join(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID')
			->order('P.VALIDATION DESC')
			->where('P.PS_CATEGORY = ?', 'BULK PAYMENT')
			->where('DATE(P.PS_CREATED) = DATE(NOW())');

		$pslipTrxData1 = $this->_db->fetchAll($selectTrx1);

		foreach ($pslipTrxData1 as $key) {

			$PS_FILE = $key['PS_FILE'];

			$imgsrc = LIBRARY_PATH . '/data/uploads/document/temp/' . $PS_FILE;
			$file_contents = file_get_contents($imgsrc);


			$file = explode(PHP_EOL, $file_contents);
			$extension = 'txt';
			$delimitedWith = '|';
			$newFileName = $imgsrc;
			$data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);
			unset($data[0]);

			$totalRecords = count($data);

			if ($totalRecords) {

				if ($totalRecords <= $this->_maxRow) {
					$rowNum = 0;

					$paramPayment = array(
						"CATEGORY"      	=> "BULK PAYROLL",
						"FROM"       		=> "I",
						"PS_NUMBER"     	=> "",
						"PS_SUBJECT"   	 	=> $key['PS_SUBJECT'],
						"PSFILEID"			=> $key['FILE_ID'],
						"PS_EFDATE"     	=> $key['PS_EFDATE'],
						"PS_FILE"     		=> $key['PS_FILE'],
						"_dateFormat"    	=> $this->_dateDisplayFormat,
						"_dateDBFormat"    	=> $this->_dateDBFormat,
						"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
						"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
						"_createPB"     	=> $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
						"_createDOM"    	=> $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
						"_createREM"    	=> false,        // cannot create REM trx
					);

					$paramTrxArr = array();
					// Zend_Debug::dump($fixData); die;

					foreach ($data as $row) {
						// if(count($row)==4)
						// {
						// var_dump($row);die;
						$rowNum++;
						$benefAcct 		= trim($row[0]);
						//$benefName 		= trim($row[1]);
						$ccy 			= "IDR";
						$amount 		= trim($row[1]);
						$message 		= trim($row[2]);
						$reference 		= trim($row[3]);
						$sms_notif 		= trim($row[4]);
						$email_notif 	= trim($row[5]);
						$addMessage 	= '';
						//$email 		= trim($row[6]);
						// 											$phoneNumber	= trim($row[7]);
						$type 			= 'PB';
						// 											$bankCode 		= trim($row[6]);
						//$bankName 		= trim($row[10]);
						//										$bankCity = trim($row[10]);
						// 											$benefAdd		= trim($row[9]);
						// 											$citizenship	= strtoupper(trim($row[10]));
						//$resident = strtoupper(trim($row[11]));

						/*
							 * Change parameter into document
							 */
						$fullDesc = array(
							'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
							'BENEFICIARY_NAME' 			=> '',
							'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
							'TRA_AMOUNT' 				=> $amount,
							'TRA_MESSAGE'				=> $message,
							'REFNO' 					=> $addMessage,
							'SMS_NOTIF'					=> $sms_notif,
							'EMAIL_NOTIF' 				=> $email_notif,
							'BENEFICIARY_EMAIL' 		=> '',
							'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
							'TRANSFER_TYPE' 			=> 'PB',
							//'CLR_CODE' 					=> $bankCode,
							//'BENEFICIARY_BANK_NAME' 	=> $bankName,
							//											'BENEFICIARY_CITY' => $bankCity,
							'BENEFICIARY_ADDRESS'		=> '',
							'BENEFICIARY_CITIZENSHIP' 	=> ''
							//'BENEFICIARY_RESIDENT' => $resident
						);



						$filter = new Application_Filtering();

						$SMS_NOTIF 		= $filter->filter($sms_notif, "SMS");
						$EMAIL_NOTIF 		= $filter->filter($email_notif, "EMAIL");

						$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
						$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
						$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
						$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
						$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
						$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
						$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
						$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
						$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
						$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
						$ACBENEF_CITIZENSHIP = $filter->filter('', "SELECTION");
						//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
						//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
						//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
						//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
						$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");

						$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

						if ($TRANSFER_TYPE == 'RTGS') {
							$chargeType = '1';
							$select = $this->_db->select()
								->from('M_CHARGES_OTHER', array('*'))
								->where("CUST_ID = ?", $this->_custIdLogin)
								->where("CHARGES_TYPE = ?", $chargeType);
							$resultSelecet = $this->_db->FetchAll($select);
							$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
						} else if ($TRANSFER_TYPE == 'SKN') {
							$chargeType1 = '2';
							$select1 = $this->_db->select()
								->from('M_CHARGES_OTHER', array('*'))
								->where("CUST_ID = ?", $this->_custIdLogin)
								->where("CHARGES_TYPE = ?", $chargeType1);
							$resultSelecet1 = $this->_db->FetchAll($select1);
							$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
						} else {
							$chargeAmt = '0';
						}


						$filter->__destruct();
						unset($filter);

						$paramTrx = array(
							"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
							"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
							"TRANSFER_FEE" 				=> $chargeAmt,
							"TRA_MESSAGE" 				=> $TRA_MESSAGE,
							"TRA_REFNO" 				=> $TRA_REFNO,
							"ACCTSRC" 					=> $key['SOURCE_ACCOUNT'],
							"ACBENEF" 					=> $ACBENEF,
							"ACBENEF_CCY" 				=> $ACBENEF_CCY,
							"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
							"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,
							"SMS_NOTIF"					=> $SMS_NOTIF,
							"EMAIL_NOTIF"				=> $EMAIL_NOTIF,
							// for Beneficiary data, except (bene CCY and email), must be passed by reference
							"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
							"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
							"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
							//	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
							"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
							"REFERENCE"					=> $reference,
							//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
							//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

							//	"ORG_DIR" 					=> $ORG_DIR,
							//"BANK_CODE" 				=> $CLR_CODE,
							//	"BANK_NAME" 				=> $BANK_NAME,
							//	"BANK_BRANCH" 				=> $BANK_BRANCH,
							//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
							//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
							//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
						);

						array_push($paramTrxArr, $paramTrx);
					}
				}

				echo "<pre>";
				var_dump($paramPayment);
				var_dump($paramTrxArr);
				die();

				$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
				$resultVal	= $validate->checkCreatePayroll($paramPayment, $paramTrxArr);
				$payment 	= $validate->getPaymentInfo();
			}
		}
	}

	private function convertFileToArray($newFileName, $extension, $delimitedWith)
	{

		$file_contents = file_get_contents($newFileName);

		//if csv occured
		if ($extension === 'csv') {

			if (!empty($delimitedWith)) {
				$data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
			}
			//if fix length
			else if ($fixLength == 1) {

				$fileContents = file($newFileName);

				$contentOrder = $fixLengthContent;

				// if with header
				if ($fixLengthType == 1) {
					//karena yg diambil hanya order dri row ke 2 saja
					$startArrIndex = 1;
					$surplusIndex = 0;
				} else if ($fixLengthType == 3) {
					//karena yg diambil hanya order dri row ke 1 saja
					$startArrIndex = 0;
					$surplusIndex = 1;
				}

				$contentOrderArr = explode(',', $contentOrder);

				if (count($contentOrderArr) > 1) {
					foreach ($fileContents as $key => $value) {
						if ($key >= $startArrIndex) {
							foreach ($contentOrderArr as $key2 => $value2) {
								//first order, startIndex from 0
								if ($key2 == 0) {
									$startIndex = 0;
									$endIndex = (int) $value2 + 1;

									$data[$key + $surplusIndex][] = substr($value, $startIndex, $endIndex);
								} else {
									$startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
									$endIndex = (int) ($value2 - ($startIndex - 1));

									$data[$key + $surplusIndex][] = substr($value, $startIndex, $endIndex);
								}
							}
						}
					}
				}
			} else {
				$data = $this->_helper->parser->parseCSV($newFileName);
			}
		}
		//if txt occured
		else if ($extension === 'txt') {

			$lines = file($newFileName);

			$checkMt940 = false;
			$checkMt101 = false;
			foreach ($lines as $line) {
				if (strpos($line, '{1:') !== false) {
					$checkMt940 = true;
				} else if (strpos($line, ':20:') !== false) {
					$checkMt101 = true;
				}
			}

			//if mt940 format
			if ($checkMt940) {

				$data = $this->_helper->parser->mt940($newFileName);
				$mtFile = true;
			} else if ($checkMt101) {
				$data = $this->_helper->parser->mt101($newFileName);
				$mtFile = true;
			} else {
				//parse csv jg bs utk txt
				if (!empty($delimitedWith)) {
					$data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
				}
				//if fix length
				else if ($fixLength == 1) {

					$fileContents = file($newFileName);

					$contentOrder = $fixLengthContent;

					// if with header
					if ($fixLengthType == 1) {
						//karena yg diambil hanya order dri row ke 2 saja
						$startArrIndex = 1;
						$surplusIndex = 0;
					} else if ($fixLengthType == 3) {
						//karena yg diambil hanya order dri row ke 1 saja
						$startArrIndex = 0;
						$surplusIndex = 1;
					}

					$contentOrderArr = explode(',', $contentOrder);

					if (count($contentOrderArr) > 1) {
						foreach ($fileContents as $key => $value) {
							if ($key >= $startArrIndex) {
								foreach ($contentOrderArr as $key2 => $value2) {
									//first order, startIndex from 0
									if ($key2 == 0) {
										$startIndex = 0;
										$endIndex = (int) $value2 + 1;

										$data[$key + $surplusIndex][] = substr($value, $startIndex, $endIndex);
									} else {
										$startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
										$endIndex = (int) ($value2 - ($startIndex - 1));

										$data[$key + $surplusIndex][] = substr($value, $startIndex, $endIndex);
									}
								}
							}
						}
					}
				} else {
					$data = $this->_helper->parser->parseCSV($newFileName, '|');
				}
			}
		}
		//if json occured
		else if ($extension === 'json') {

			$datajson = json_decode($file_contents, 1);
			$i = 0;
			foreach ($datajson as $key => $value) {
				if ($i == 0) {
					$data[$i] = array_keys($value);
					$data[$i + 1] = array_values($value);
				} else {
					$data[$i + 1] = array_values($value);
				}

				$i++;
			}
		}
		//if xml occured
		else if ($extension === 'xml') {

			$xml = (array) simplexml_load_string($file_contents);

			$i = 0;
			foreach ($xml as $key => $value) {
				foreach ($value as $key2 => $value2) {
					if ($i == 0) {
						$data[$i] = array_keys((array)$value2);
						$data[$i + 1] = array_values((array)$value2);
					} else {
						$data[$i + 1] = array_values((array)$value2);
					}

					$i++;
				}
			}

			for ($i = 0; $i < count($data); $i++) {
				if ($i > 0) {
					foreach ($data[$i] as $key => $value) {
						if (empty($value)) {
							$data[$i][$key] = null;
						}
					}
				}
			}

			// print_r($data);die();
		} else if ($extension === 'xls' || $extension === 'xlsx') {
			try {
				$inputFileType = IOFactory::identify($newFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($newFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($newFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
			}

			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			for ($row = 1; $row <= $highestRow; $row++) {
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

				$data[$row - 1] = $rowData[0];
			}
		}

		return $data;
	}

	public function indexAction()
	{

		$this->_helper->layout()->disableLayout();
		//---------------------- GENERATE CAPTCHA IMAGE  --------------------------------------
		$captcha = Application_Captcha::generateCaptcha();
		//var_dump($captcha);
		$this->view->captchaId = $captcha['id']; //returns the ID given to session &amp; image
		$this->view->captImgDir = $captcha['imgDir'];
		//---------------------	END GENERATE CAPTCHA IMAGE -------------------------------------

		if (isset($_GET['changepw'])) {
			$this->_redirect('/');
		}
		//die('here');
		if (Zend_Auth::getInstance()->hasIdentity()) {
			$this->_redirect('/home/dashboard');
		}
		//	
		$errors = $this->_helper->getHelper('FlashMessenger')->getMessages();
		$flashMessanger = $this->_helper->getHelper('FlashMessenger');

		// if(!empty($errors)){
		// 	$errors = $errors['0'];
		// }
		if (count($flashMessanger->getMessages()) > 0) {
			$this->view->errors = $flashMessanger->getMessages()[0]["error"];
		}

		$ns = new Zend_Session_Namespace('language');
		// var_dump($ns);die;

		$this->view->gClient = Gclient::getGClient();

		$set = $this->_getParam('lang');

		$ns = new Zend_Session_Namespace('language');
		$sessionLanguage = $ns->langCode;

		// $ns = new Zend_Session_Namespace('language');
		// var_dump($set);
		// var_dump($this->_getAllParams ());
		if ($set) {
			// var_dump($set);die;
			//// Start Multi Language
			$defaultlanguage = $set;
			$ns = new Zend_Session_Namespace('language');
			$sessionLanguage = $ns->langCode;



			if (!is_null($sessionLanguage)) {
				$setlang = $sessionLanguage;
			} else {
				$setlang = $locale->getLanguage();
			}
			$ns->langCode = $lang;
			// var_dump($ns);
			if (!empty($setlang) && isset($setlang)) {
				$defaultlanguage = $setlang;
				$ns->langCode = $defaultlanguage;
			}
			Zend_Loader::loadClass('Zend_Translate');

			$frontendOptions = array(
				'cache_id_prefix' => 'MyLanguage',
				'lifetime' => 86400,
				'automatic_serialization' => true
			);
			$backendOptions = array('cache_dir' => APPLICATION_PATH . '/../../library/data/cache/language/frontend');
			$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
			Zend_Translate::setCache($cache);

			$translate = new Zend_Translate('tmx', APPLICATION_PATH . '/../../library/data/languages/frontend/language.tmx', $defaultlanguage);
			// Zend_Debug::dump($translate);die;
			Zend_Registry::set('language', $translate);
			//// End Multi Language

			$lang = $set;
			$ns->langCode = $lang;
			//$this->_redirect('/');
			if (Zend_Auth::getInstance()->hasIdentity()) {
				$this->_redirect('/home/dashboard');
			}
		}
		$ns = new Zend_Session_Namespace('language');
		$defaultlanguage = 'id';
		$ns->langCode = $defaultlanguage;
		// var_dump($ns);die;
		//print_r($defaultlanguage);die;

		$translate = new Zend_Translate('tmx', APPLICATION_PATH . '/../../library/data/languages/frontend/language.tmx', $defaultlanguage);
		// var_dump($ns);die;
		// 		Zend_Debug::dump($translate);
		Zend_Registry::set('language', $translate);

		// print_r($this->language);die;
		$confirmPage = $this->_getParam('confirmPage', 1);
		$tokenAuth = $this->_getParam('tokenAuth', 0);

		$challengeCode = NULL;
		$challengeCode = new Service_Token(NULL, NULL);
		$challengeCode = $challengeCode->generateChallengeCode();

		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');

		$app = Zend_Registry::get('config');
		if ($app['captcha']['emulate'] == 1) {
			$this->view->capchahide = 'block';
		} else {
			$this->view->capchahide = 'none';
		}

		if ($this->_request->isPost()) {
			$token = '';

			$filters = array(
				'userId' => array('StripTags', 'StringTrim', 'StringToUpper'),
				'custId' => array('StripTags', 'StringTrim', 'StringToUpper'),
				'password' => array(),
				'challengeCode' => array('StripTags', 'StringTrim', 'StringToUpper'),
				'responseCode' => array('StripTags', 'StringTrim', 'StringToUpper'),
			);

			$validators = array(
				'userId'         => array(
					'NotEmpty',
					//					'Alnum',
					'messages' => array(
						$this->language->_('User ID cannot be empty'),
						//						$this->language->_('Invalid User ID Format'),
					),
				),
				'custId'         => array(
					'NotEmpty',
					//					'Alnum',
					'messages' => array(
						$this->language->_('Customer ID cannot be empty'),
						//						$this->language->_('Invalid Customer ID Format'),
					),
				),
				'password'         => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Password cannot be empty'),
					),
				),
				'challengeCode'         => array(),
				'responseCode'         => array(
					'Digits',
					'messages' => array(
						$this->language->_('Invalid Response Token Format'),
					),
				),
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_getAllParams(), $this->_optionsValidator);
			// ========================== END FILTERING =======================================

			//================================BYPASSCAPCAY (|| true========================================//

			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
			$sessionNameRand->randomTransact = $this->_getParam('randomTransact');
			if ($this->_getParam('randomTransact') == $sessionNameRand->randomTransact) {
				try {
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					$blocksize = 256;  // can be 128, 192 or 256
					$custidEnc =  $this->_getParam('custId');
					$useridEnc =  $this->_getParam('userId');
					$authkeysEnc =   $this->_request->getParam('password');
					// 					print_r($custidEnc);die;
				} catch (Exception $e) {
					$custidEnc = '';
					$useridEnc = '';
					$authkeysEnc = '';
				}
			} else {
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/');
			}
			//decrypt

			$custidDec = SGO_Helper_AES::decrypt($custidEnc, $passwordRand, $blocksize);
			$useridDec = SGO_Helper_AES::decrypt($useridEnc, $passwordRand, $blocksize);
			$authkeysDec = SGO_Helper_AES::decrypt($authkeysEnc, $passwordRand, $blocksize);


			//
			//print_r($custId);die;
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;
			/*$GtSdk = new GeetestLib(CAPTCHA_ID, PRIVATE_KEY);

			if ($app['captcha']['emulate'] == 1) {
				$captchaValid = true;
			} else {

				if ($_SESSION['gtserver'] == 1) {   //服务器正常
					$data = array();
					$resultcapca = $GtSdk->success_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'], $data);
					if ($resultcapca) {

						$captchaValid = true;
						//echo '{"status":"success"}';
					} else {
						$captchaValid = false;
						// echo 'here';
						//echo '{"status":"fail"}';
					}
				} else {  //服务器宕机,走failback模式
					if ($GtSdk->fail_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'])) {
						$captchaValid = true;
						//echo '{"status":"success1"}';
					} else {
						$captchaValid = false;
						// echo 'here1';
						// echo '{"status":"fail1"}';
					}
				}
			}*/

			//var_dump($confirmPage);
			//var_dump($captcha);die;
			//ECHO $this->_getParam('captcha[input]');
			//var_dump ($this->_getParam('captcha'));die;

			if ($confirmPage == 1 && Application_Captcha::validateCaptcha($this->_getParam('captcha')) === true) {
				$captchaValid = true;
				$compIdPost = $custidDec;
				$userIdPost = $useridDec; //$this->_getParam('userId');
				$userStatusPost = 3;

				$dataCompId = $this->_db->select()
					->from(
						array('M_USER')
						//				array('CUST_TOKEN_AUTH' => "IFNULL(CUST_TOKEN_AUTH,'N')")
					)
					->where('CUST_ID = ?', $compIdPost)
					->where('USER_ID = ?', $userIdPost)
					->where('USER_STATUS != ?', $userStatusPost)
					->limit(1);
				// echo $dataCompId;die;
				$dataComp = $this->_db->fetchRow($dataCompId);

				/*try 
						{
							$this->_db->beginTransaction();		
		
							$datalang =  array(					
													'USER_LANG' => 'id'
												);							
							
							$where = array('USER_ID = ?' => $this->_userIdLogin);
							$result = $this->_db->update('M_USER',$datalang,$where);
							
							$this->_db->commit();							
							
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
							
						}*/

				if (!$zf_filter_input->isValid()) {
					$errors = '';
					$listMsg = $zf_filter_input->getMessages();
					foreach ($listMsg as $key) {
						foreach ($key as $msg) {
							$errors .= $msg . PHP_EOL;
						}
					}
				} elseif (!$dataComp) {
					$errors = $this->language->_('Invalid Company ID or User ID');
					Application_Helper_General::writeLogAnonymous('FLGN', 'Failed Login into System Invalid Company ID or User ID' . $user_ip = $this->getUserIP(), $useridDec, $custidDec);
				}

				$custId = ($custidDec);
				$tokenAuth = $this->_db->select()
					->from(
						array('M_CUSTOMER'),
						array('CUST_TOKEN_AUTH' => "IFNULL(CUST_TOKEN_AUTH,'N')")
					)
					->where('CUST_ID = ?', (string)$custId)
					->where('CUST_TOKEN_AUTH = ?', 'Y')
					->limit(1);

				$tokenAuth = $this->_db->fetchRow($tokenAuth);
				$tokenAuth = (isset($tokenAuth['CUST_TOKEN_AUTH']) && $tokenAuth['CUST_TOKEN_AUTH'] == 'Y' ? 1 : 0);


				$custModel = $this->_db->select()
					->from(
						array('M_CUSTOMER'),
						array('CUST_MODEL')
					)
					->where('CUST_ID = ?', (string)$custId)
					->limit(1);

				$custModel = $this->_db->fetchRow($custModel);



				$cekpass 	= false;
				$userId = ($useridDec); //($zf_filter_input->userId);
				$custId = ($custidDec); //($zf_filter_input->custId);
				$blocksize = 256;
				$passwordRand = $sessionNameRand->randomTransact;
				// 				$custId = SGO_Helper_AES::decrypt( $this->_getParam('custId'), $passwordRand, $blocksize );
				$pass = ($authkeysDec); //($zf_filter_input->password);
				// print_r($tokenAuth);die;
				$tokenAuth = 0;
				if ($tokenAuth == '1') {
					$responseCode = $zf_filter_input->getEscaped('responseCode');
					if (!(empty($responseCode))) {
						$tokenIdUser = $this->_db->select()
							->from(
								array('M_USER'),
								array('TOKEN_ID')
							)
							->where('USER_ID = ?', $userId)
							->limit(1);
						$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
						$tokenIdUser = $tokenIdUser['TOKEN_ID'];

						if (!(empty($tokenIdUser))) {
							$cekToken = new Service_Token($custId, $userId, $tokenIdUser);
							$cekToken = $cekToken->verify($zf_filter_input->getEscaped('challengeCode'), $responseCode);

							if ($cekToken['ResponseCode'] == '00') {
								$Login 		= new Login();
								$result   	= $Login->authenticate($custId, $userId . '@HOST' . $_SERVER['REMOTE_ADDR'], $pass);

								$cekpass	= ($result['responseCode'] == '00') ? true : false;
								$errors		= ($result['responseCode'] == '00') ?   "" : $result['responseDesc'];
							} else {
								$CustUser = new CustomerUser($custId, $userId);
								$CustUser->setCustId($custId);
								$CustUser->setUserId($userId);

								// 								$CustUser->setFailedTokenMustLogout();
								$CustUser->setUserFailToken();
								$CustUser->setUserLock();
								$CustUser->setUserLogout();

								$cekpass = false;
								$errors = 'Token ' . $cekToken['ResponseDesc'];
							}
						} else {
							$cekpass = false;
							$errors = 'Token has not been enabled';
						}
					} else {
						$cekpass = false;
						$errors = 'Response token cannot be empty ';
					}
				} else {
					$tokenIdUser = $this->_db->select()
						->from(
							array('M_USER'),
							array('USER_FAILEDATTEMPT')
						)
						->where('USER_ID = ?', $userId)
						->limit(1);
					$capcalock = $this->_db->fetchRow($tokenIdUser);
					if ($capcalock['USER_FAILEDATTEMPT'] == '3') {
						$this->view->reqcap = 1;
					}

					if ($capcalock['USER_FAILEDATTEMPT'] >= 3 && Application_Captcha::validateCaptcha($this->_request->getPost('captcha')) === true) {

						$Login 		= new Login();

						// 						print_r($pass);die;
						$result   	= $Login->authenticate($custId, $userId . '@HOST' . $_SERVER['REMOTE_ADDR'], $pass);

						$cekpass	= ($result['responseCode'] == '00') ? true : false;
						$errors		= ($result['responseCode'] == '00') ?   "" : $result['responseDesc'];
					} else if ($capcalock['USER_FAILEDATTEMPT'] < 2) {
						// die('re');

						$Login 		= new Login();

						$result   	= $Login->authenticate($custId, $userId . '@HOST' . $_SERVER['REMOTE_ADDR'], $pass);

						$cekpass	= ($result['responseCode'] == '00') ? true : false;
						$errors		= ($result['responseCode'] == '00') ?   "" : $result['responseDesc'];
					} else if ($capcalock['USER_FAILEDATTEMPT'] = 2) {
						// die;

						$Login 		= new Login();

						// 						print_r($pass);die;
						$result   	= $Login->authenticate($custId, $userId . '@HOST' . $_SERVER['REMOTE_ADDR'], $pass);

						$cekpass	= ($result['responseCode'] == '00') ? true : false;
						$errors		= ($result['responseCode'] == '00') ?   "" : $result['responseDesc'];
						$this->view->reqcap = 1;
					} else {
						$this->view->reqcap = 1;
						$errors = $this->language->_('Invalid Captcha');

						// $cekpass = true;
					}
				}

				if ($cekpass) {
					try {
						$this->_db->delete('TEMP_SESSION_USER', array(
							'CUST_ID = ?' => $custId,
							'USER_ID = ?' => $userId,
						));
						$this->_db->insert('TEMP_SESSION_USER', array(
							'CUST_ID' => $custId,
							'USER_ID' => $userId,
							'USER_LASTACTIVITY' => new Zend_Db_expr('now()'),
							'TOKEN_WS' => $token,
						));
					} catch (Exception $e) {
						Application_Helper_General::exceptionLog($e);
					}


					$frontendOptions = array(
						'lifetime' => 86400,
						'automatic_serialization' => true
					);
					$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
					$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
					$cacheID = 'USERLIST';


					$userlist = $cache->load($cacheID);

					if (empty($userlist)) {
						$selectuser = $this->_db->select()
							->from(
								'M_USER',
								array('*')
							);

						$userlist = $this->_db->fetchAll($selectuser);
						$listuser = array();
						foreach ($userlist as $keyuser => $valuser) {
							$listuser[$valuser['USER_ID']] = $valuser['USER_FULLNAME'];
						}
						$cache->save($listuser, $cacheID);
					}


					// echo "<pre>";
					// var_dump($userlist);die;

					$Settings = new Settings();
					$allSetting = $Settings->getAllSetting();

					$minDiff = $allSetting['timeoutafter'];

					$customerModel = new Zend_Session_Namespace('cust_model');
					$getCustomerModel = $this->_db->select()
						->from('M_CUSTOMER', 'CUST_MODEL')
						->where('CUST_ID = ?', $custidDec)
						->query()->fetch();

					$customerModel->model = $getCustomerModel['CUST_MODEL'];

					if (intval($getCustomerModel['CUST_MODEL']) !== 1) {
						$this->_db->delete('M_WIDGET', [
							'USER_ID = ?' => $useridDec,
							'CUST_ID = ?' => $custidDec,
							'WIDGET_ID = ?' => '1',
						]);

						$this->_db->insert('M_WIDGET', [
							'USER_ID' => $useridDec,
							'CUST_ID' => $custidDec,
							'WIDGET_ID' => '1',
						]);
					}

					$objSessionNamespace = new Zend_Session_Namespace('Zend_Auth');
					$timeExprSession = 600 + $minDiff * 60; //tambah 600 karena scheduler force logout 10 menit sekali
					$objSessionNamespace->setExpirationSeconds($timeExprSession);
					// set keterangan login ke auth
					$CustomerUser	= new CustomerUser(strtoupper($custId), strtoupper($userId));
					$type = $allSetting['system_type'];
					$listAccount 	= $CustomerUser->getAccounts();
					$accountList 	= Application_Helper_Array::simpleArray($listAccount, 'ACCT_NO');

					$listPrivilege = $CustomerUser->getPrivileges($type);
					$privilege = Application_Helper_Array::simpleArray($listPrivilege, 'FPRIVI_ID');
					//echo '<pre>';
					//var_dump($privilege);die;

					$privilege[] = 'systemPrivilege';

					// set keterangan login ke auth
					Zend_Session::regenerateId();
					$auth = Zend_Auth::getInstance();
					$data = new stdClass();



					#$select->where("TIME (EXP_TIME) >= TIME(NOW())");

					$frontendOptions = array(
						'lifetime' => 259200,
						'automatic_serialization' => true
					);
					$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/acctbalance/'); // Directory where to put the cache files
					$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
					$cacheID = 'NOTIFCATION';

					$notif = $cache->load($cacheID);
					//var_dump($select_int);
					if (empty($notif)) {
						$select = $this->_db->select()
							->from(
								'M_NOTIFICATION',
								array('*')
							);
						$select->where("DATE (EFDATE) <= DATE(NOW())");
						$select->where("TIME (EFDATE) <= TIME(NOW())");
						$select->where("DATE (EXP_DATE) >= DATE(NOW())");
						$select->where("TARGET IN ('1','3')");

						$notif = $this->_db->fetchAll($select);



						$cache->save($notif, $cacheID);
					}


					// echo $select;die;


					$data->userIdLogin = $username;
					$data->userNameLogin = $login['BUSER_NAME'];
					$data->userBranch = $login['BRANCH_NAME'];
					$data->userBranchId = $login['ID'];
					$data->userBranchCode = $login['BRANCH_CODE'];
					$data->priviIdLogin = $priviId;
					$data->userGroupLogin = $groupArr;
					if (!empty($notif)) {
						$data->notifCount = count($notif);
						$data->notif = $notif;
					}
					//var_dump($login);die;
					// print_r($result);die;
					$data->isCommLinkage = false;
					$data->userIdLogin    = $userId;
					$data->userNameLogin  = ucwords(strtolower($result['userNameLogin']));
					$data->custNameLogin  = ucwords(strtolower($result['custNameLogin']));
					if ($result['custSameUser'] == 1) {
						$parampriv = array('PRPP');
						foreach ($privilege as $key => $value) {
							if (in_array($value, $parampriv)) {
								unset($privilege[$key]);
							}
						}
					}
					$data->priviIdLogin   = $privilege;
					$data->allSetting     = $allSetting;
					// $data->userGroupLogin = $groupArr;
					$data->custIdLogin    = strtoupper($custId);
					$data->tokenIdLogin    = $result['tokenId'];
					$data->accountList    = $accountList;
					$data->lastLogin = $result['lastLogin'];
					$data->custNameLogin = $result['custNameLogin'];
					$data->custSameUser = $result['custSameUser'];

					//theme
					$data->_userTheme = $dataComp['USER_THEME'];

					// cek userSessionHash before login
					$userSessionHash	= $this->_db->fetchrow(
						$this->_db->SELECT('SESSION_HASH')->FROM('M_USER')
							->WHERE('USER_ID = ?', $userId)
							->WHERE('CUST_ID =?', $custId)
					);
					//


					// ---------------------------------------- HASH -----------------------------------------------  //
					$hash = $this->hashKey($userId, $custId);
					$data->userHash     = $hash;

					$data->custModel = $custModel['CUST_MODEL'];

					try {
						$auth->getStorage()->write($data);
					} catch (Exception $e) {
						var_dump($e);
						die;
					}


					// End set keterangan login ke auth
					//$browser = substr($_SERVER['HTTP_USER_AGENT'], 0,24);
					$browser = $_SERVER['HTTP_USER_AGENT'];
					$sessionHash = md5($_SERVER['REMOTE_ADDR'] . $browser);

					//update USER_ISLOGIN
					$updateArr['USER_ISLOGIN'] = 1;
					$updateArr['USER_ISLOCKED'] = 0;
					$updateArr['USER_LASTLOGIN'] = new Zend_Db_Expr('now()');
					$updateArr['USER_LASTACTIVITY'] = new Zend_Db_Expr('now()');
					$updateArr['USER_FAILEDATTEMPT'] = 0;
					$updateArr['USER_ISNEW'] = 0;
					$updateArr['USER_HASH'] = $hash;
					$updateArr['SESSION_HASH'] = $sessionHash;
					$whereArr = array(
						'CUST_ID = ?' => (string)$custId,
						'USER_ID = ?' => (string)$userId
					);
					$customerupdate = $this->_db->update('M_USER', $updateArr, $whereArr);


					if ($userSessionHash['SESSION_HASH'] != $sessionHash) {

						//send email
						$EmailLoginNotif 				= $allSetting['femailtemplate_loginnotif'];
						$templateEmailMasterBankName	= $allSetting['master_bank_name'];
						$templateEmailMasterBankAppName = $allSetting['master_bank_app_name'];
						$templateEmailMasterBankEmail	= $allSetting['master_bank_email'];
						$templateEmailMasterBankTelp	= $allSetting['master_bank_telp'];
						$templateEmailMasterBankWapp 	= $allSetting['master_bank_wapp'];
						$url_fo 	= $allSetting['url_fo'];
						$userData	= $this->_db->fetchrow(
							$this->_db->SELECT()->FROM('M_USER')
								->WHERE('USER_ID = ?', $userId)
								->WHERE('CUST_ID =?', $custId)
						);

						$lastlogin = Application_Helper_General::convertDate($userData['USER_LASTLOGIN'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

						// masking user id
						$lengthUserId = strlen($userId);
						$maskchar = substr($userId, 1, $lengthUserId - 3);
						$lengthMaskchar = strlen($maskchar);
						$mask = str_repeat("*", $lengthMaskchar);
						$userIdMask = str_replace($maskchar, $mask, $userId) . "<br>";
						// end masking						
						// echo "<pre>";
						$paramip = var_export(unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $_SERVER['REMOTE_ADDR'])));

						$data = array(
							'[[user_fullname]]' => $userData['USER_FULLNAME'],
							'[[user_login]]' => $userIdMask,
							'[[user_email]]' => $userData['USER_EMAIL'],
							'[[user_lastlogin]]' => $lastlogin,
							'[[user_agent]]' => $this->get_browser_name($_SERVER['HTTP_USER_AGENT']), //$browser,
							'[[comp_accid]]' => $userData['CUST_ID'],
							'[[ip_address]]' => $_SERVER['REMOTE_ADDR'],
							'[[app_url_link]]' => $url_fo . '/tools/forgotpassword/',
							'[[city]]' => $paramip['geoplugin_city'],
							'[[country]]' => $paramip['geoplugin_countryName'],
							'[[master_bank_name]]' => $templateEmailMasterBankName,
							'[[master_bank_app_name]]' => $templateEmailMasterBankAppName,
							'[[master_bank_email]]' => $templateEmailMasterBankEmail,
							'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,
							'[[master_bank_wapp]]' 	=> $templateEmailMasterBankWapp
						);

						$EmailLoginNotif = strtr($EmailLoginNotif, $data);
						if (!empty($userData['USER_EMAIL'])) {
							Application_Helper_Email::sendEmail($userData['USER_EMAIL'], 'Login Notification', $EmailLoginNotif);
						}
						//end send email
					}


					//update latest transaction cache
					$this->latestTransaction($custId, $userId);
					// 	print_r($cekpass);
					// die;
					$ns = new Zend_Session_Namespace('language');
					$ns->langCode = "id";
					Application_Helper_General::writeLog('FLGN', 'Login Success ' . $user_ip = $this->getUserIP());
					$this->_redirect('/home/dashboard');
				} else {
					$confirmPage = 1;
				}
			} else {
				$captchaValid == false;
				$errors = $this->language->_('Invalid Captcha');

				/*if ($captchaValid == false) {
					$errors = $this->language->_('Invalid Captcha');
				}*/
			}
		} // END REQUEST IS POST
		else {
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;
		}

		$this->view->challengeCode = $challengeCode;
		$this->view->confirmPage = $confirmPage;
		$this->view->tokenAuth = $tokenAuth;

		if (!empty($errors)) {
			//var_dump($errors);
			//$flashMessanger->addMessage(["error" => $errors]);
			$this->view->errors = $errors;
		}

		//		$this->view->userId = (isset($zf_filter_input->userId)) ? $zf_filter_input->userId : $this->_getParam('userId');
		$this->view->userId = (isset($useridEnc)) ? $useridEnc : $useridEnc;

		//		$this->view->custId = (isset($zf_filter_input->custId)) ? $zf_filter_input->custId : $this->_getParam('custId');
		$this->view->custId = (isset($custidEnc)) ? $custidEnc : $custidEnc;

		$selectlandingpage = $this->_db->select()
			->from(
				'M_LANDINGPAGE',
				array('*')
			);
		$selectlandingpage->where("DATE (EFDATE) <= DATE(NOW())");
		$selectlandingpage->where("TIME (EFDATE) <= TIME(NOW())");
		$selectlandingpage->where("DATE (EXPDATE) >= DATE(NOW())");
		$selectlandingpage->where("STATUS != 4");
		// echo $selectlandingpage;
		$landingpage = $this->_db->fetchAll($selectlandingpage);
		$this->view->landingpage = $landingpage;

		// return $this->_redirect("/");
		// var_dump($notifs);
	}

	public function hashKey($userId, $custId)
	{
		do {
			$key = null;
			for ($i = 0; $i < 3; $i++)
				$key .= rand(0, 9);

			$browser = $_SERVER['HTTP_USER_AGENT'];
			$hash = md5($_SERVER['REMOTE_ADDR'] . $browser . $key);
			$checkHash = null;
			// CHECK HASH
			$checkHash = $this->_db->fetchOne(
				$this->_db->SELECT()
					->FROM('M_USER', array('USER_HASH'))
					->WHERE('USER_ID =?', $userId)
					->WHERE('CUST_ID =?', $custId)
					->WHERE('USER_HASH =?', $hash)
			);
		} while (!empty($checkHash));

		return $hash;
	}

	private function latestTransaction($custId, $userId)
	{
		$arrPayType 	= array_combine($this->_paymenttype["code"], $this->_paymenttype["desc"]);

		$casePayType = "(CASE P.PS_TYPE ";
		foreach ($arrPayType as $key => $val) {
			$casePayType .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$casePayType .= " END)";

		$frontendOptions = array(
			'lifetime' => 900,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

		$cacheID = 'LT' . $custId . $userId;

		if (!$result = $cache->load($cacheID)) {
			$select = $this->_db->select()
				->from(array('P' => 'T_PSLIP'), array())
				->join(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
				->joinLeft(array('M' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = M.PROVIDER_ID', array('P.PS_NUMBER', 'PAYMENT_TYPE' => $casePayType, 'T.BENEFICIARY_ACCOUNT', 'P.PS_CCY', 'P.PS_TOTAL_AMOUNT', 'M.PROVIDER_NAME'))
				->where('P.CUST_ID = ?', $custId)
				->where('P.PS_RELEASER_USER_LOGIN = ?', $userId)
				->where('P.PS_STATUS = 5')
				->where('T.TRA_STATUS = 3')
				->order('P.PS_CREATED DESC')
				->limit(5);

			$result = $this->_db->fetchAll($select);

			$cache->save($result, $cacheID);
		}
	}

	public function langAction()
	{
		$set = $this->_getParam('set');

		//// Start Multi Language
		$defaultlanguage = $set;
		$ns = new Zend_Session_Namespace('language');
		$sessionLanguage = $ns->langCode;

		if (!is_null($sessionLanguage)) {
			$setlang = $sessionLanguage;
		} else {
			$setlang = $locale->getLanguage();
		}
		if (!empty($setlang) && isset($setlang)) {
			$defaultlanguage = $setlang;
			$ns->langCode = $defaultlanguage;
		}
		Zend_Loader::loadClass('Zend_Translate');

		$frontendOptions = array(
			'cache_id_prefix' => 'MyLanguage',
			'lifetime' => 86400,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => APPLICATION_PATH . '/../../library/data/cache/language/frontend');
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		Zend_Translate::setCache($cache);

		$translate = new Zend_Translate('tmx', APPLICATION_PATH . '/../../library/data/languages/frontend/language.tmx', $defaultlanguage);

		Zend_Registry::set('language', $translate);
		//// End Multi Language

		$lang = $set;
		$ns->langCode = $lang;
	}

	public function logoutAction()
	{
		Application_Helper_General::writeLog('FLGT', 'Logout From System ' . $user_ip = $this->getUserIP());
		if (Zend_Auth::getInstance()->hasIdentity()) {
			$auth = Zend_Auth::getInstance()->getIdentity();
			$this->_userIdLogin   = $auth->userIdLogin;
			$this->_custIdLogin   = $auth->custIdLogin;
			Zend_Auth::getInstance()->clearIdentity();
			$CustomerUser	= new CustomerUser(strtoupper($this->_custIdLogin), strtoupper($this->_userIdLogin));
			$CustomerUser->forceLogout();

			$forceBySession = $this->_request->getParam('session');

			if ($forceBySession == '1') {
				$this->_redirect('/authorizationacl/index/disableuser');
			}
		}
		$this->_redirect('/');

		//		$this->_redirector->gotoUrl('login/');
	}


	public function notifrefreshAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$select = $this->_db->select()
			->from(
				'M_NOTIFICATION',
				array('*')
			);
		$select->where("DATE (EFDATE) >= DATE(NOW())");
		$select->where("TIME (EFDATE) <= TIME(NOW())");
		$select->where("DATE (EXP_DATE) >= DATE(NOW())");
		$select->where("TARGET IN ('1','3')");
		#$select->where("TIME (EXP_TIME) >= TIME(NOW())");
		// echo $select;die;
		$notif = $this->_db->fetchAll($select);
		if (!empty($notif)) {
			$notifCount = count($notif);
			$notif = $notif;
		}
		$string = '';
		foreach ($notif as $key => $value) {
			$val = $value['SUBJECT'];
			$string  .= '<a class="dropdown-item" href="#">' . $val . '</a><div class="dropdown-divider"></div>';
		}
		echo $string;
	}


	public function updatenotifAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$tblName = $this->_getParam('id');

		$insertArr = array(
			'NOTIF_ID'			=> $tblName,
			'USER_ID' 			=> $this->_userIdLogin
		);
		$this->_db->insert('M_NOTICEPOPUP', $insertArr);
	}

	public function balanceinfoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();


		$app      = Zend_Registry::get('config');
		$bankCode = $app['app']['bankcode'];

		$acctNo  = $this->_getParam('acct');

		$ccy = Application_Helper_General::getCurrNum('IDR');

		$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
		$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
		$result2 = $svcAccount->inquiryAccontInfo('AI', TRUE);
		$result3 = $svcAccount->inquiryDeposito();
		$result4 = $svcAccount->inquiryLoan();

		$return = array('accountInfo' => $result2, 'balanceInfo' => $result, 'deposito' => $result3, 'loan' => $result4);
		echo "<pre>";
		var_dump($return);
	}

	public function inqcifAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$ccy = Application_Helper_General::getCurrNum('IDR');

		$svcAccount = new Service_Account(null, null, null, null, null, $this->_request->getParam("cif"));

		echo "<pre>";
		var_dump($svcAccount->inquiryCIFAccount());
	}

	// public function unlockdepoAction()
	// {
	// 	$this->_helper->viewRenderer->setNoRender();
	// 	$this->_helper->layout()->disableLayout();

	// 	$svcAccount = new SGO_Soap_ClientUser();

	// 	$svcAccount->callapi("unlockdeposito", [], [
	// 		"account_number" => "701400043856",
	// 		"account_type" => "T",
	// 		"branch_code" => ""
	// 	]);

	// 	echo "<pre>";
	// 	var_dump($svcAccount->getResultPure());
	// }


	public function notifpopAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$select = $this->_db->select()
			->from(
				array('L' =>  'M_NOTIFICATION'),
				array('*')
			);
		// $select1->joinleft(array('T' => 'M_NOTICEPOPUP'), 'T.NOTIF_ID = L.ID', array('T.USER_ID'));
		$select->where("DATE (EFDATE) >= DATE(NOW())");
		$select->where("TIME (EFDATE) <= TIME(NOW())");
		$select->where("DATE (EXP_DATE) >= DATE(NOW())");
		$select->where("TARGET IN ('1','3')");
		// $select->where("USER_ID = ? ", $this->_userIdLogin);
		// echo $select;die;
		#$select->where("TIME (EXP_TIME) >= TIME(NOW())");
		// echo $select;die;
		$notif = $this->_db->fetchAll($select);
		if (!empty($notif)) {
			// $data = array();
			foreach ($notif as $key => $value) {
				$selectdata = $this->_db->select()
					->from(
						array('A' =>  'M_NOTICEPOPUP'),
						array('*')
					);
				$selectdata->where("NOTIF_ID = ?", $value['ID']);
				$selectdata->where("USER_ID = ?", $this->_userIdLogin);
				$data = $this->_db->fetchAll($selectdata);
				if (!empty($data)) {
					// echo 'here';
					unset($notif[$key]);
				}
				unset($data);
			}
		}

		if (!empty($notif)) {
			$notifCount = count($notif);
			$notif = $notif;
		}
		echo json_encode($notif);
		// $string = '';
		// foreach ($notif as $key => $value) {
		// 	$val = $value['SUBJECT'];
		// 	$string  .= '<a class="dropdown-item" href="#">'.$val.'</a><div class="dropdown-divider"></div>';
		// }
		// ?echo $string;

	}


	public function ldapAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$domain = 'btn.co.id';
		$username = 'ebg2';
		$password = 'B@tra123.!@#$%';
		#$password = 'B@tara123456*(';
		$ldapconfig['host'] = 'ldap://btn.co.id';
		$ldapconfig['port'] = 389;

		$ldapconfig['basedn'] = "DC=btn,DC=co,DC=id";


		$ds = ldap_connect($ldapconfig['host'], $ldapconfig['port']);
		ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);

		$dn = "ou=Aplikasi," . $ldapconfig['basedn'];
		$bind = ldap_bind($ds, $username . '@' . $domain, $password);


		$filter = '(&(objectCategory=person)(sAMAccountName=' . strtoupper($username) . '))';
		$dn = "DC=btn,DC=co,DC=id";
		$res = ldap_search($ds, $ldapconfig['basedn'], $filter);
		$first = ldap_first_entry($ds, $res);
		if ($first) {
			$data = ldap_get_dn($ds, $first);
			var_dump($data);
			echo ("Login correct");
		} else {
			echo ("Login incorrect");
		}
	}

	public function verifyldapAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();





		$domain = 'btn.co.id';
		$username = $this->_getParam('u');
		$password = $this->_getParam('p');
		#$password = 'B@tara123456*(';
		$ldapconfig['host'] = 'ldap://btn.co.id';
		$ldapconfig['port'] = 389;

		$ldapconfig['basedn'] = "DC=btn,DC=co,DC=id";


		$ds = ldap_connect($ldapconfig['host'], $ldapconfig['port']);
		ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);

		$dn = "ou=Aplikasi," . $ldapconfig['basedn'];
		$bind = ldap_bind($ds, $username . '@' . $domain, $password);

		$filter = '(&(objectCategory=person)(sAMAccountName=' . strtoupper($username) . '))';
		$dn = "DC=btn,DC=co,DC=id";
		$res = ldap_search($ds, $ldapconfig['basedn'], $filter);
		$first = ldap_first_entry($ds, $res);
		if ($first) {
			$data = ldap_get_dn($ds, $first);
			echo json_encode($data);
		} else {
			echo json_encode(array('status' => '0'));
		}
	}

	public function loginldapAction()
	{
		$this->_helper->layout()->disableLayout();
	}

	public function encryptAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$encryption = new SGO_Helper_Encryption();
		/*echo "encrypt";
		echo "<br>";
		echo $encryption->encrypt("bg");
		echo "<br>";
		echo $encryption->encrypt("590BankGaransi!");
		*/
		echo "<br>";
		echo "decrypt";
		echo "<br>";
		echo $encryption->decrypt("/6rmLrza/tU8a8gh3exWKA==");
		echo "<br>";
		echo $encryption->decrypt("/vUUUwokmhZp1UJ+T/8R8g==");
	}

	public function testmiddleAction()
	{

		$param['data']['document_id'] = "97874fe6-b3e8-4d6b-9b3c-bfcf8b715fe7";

		$getPreliminary = $this->_db->select()
			->from('M_PRELIMINARY')
			->where('CUST_ID = ?', $this->_custIdLogin)
			->query()->fetch();

		$getPreliminary['BG_REG_NUMBER'] = "123123123";
		unset($getPreliminary['LAST_SUGGESTED']);
		unset($getPreliminary['LAST_SUGGESTEDBY']);
		unset($getPreliminary['LAST_APPROVED']);
		unset($getPreliminary['LAST_APPROVEDBY']);

		$this->_db->insert('TEMP_BANK_GUARANTEE_PRELIMINARY', $getPreliminary);

		var_dump($getPreliminary);
		die();

		// move from temp to t bank_guarantee
		$get_temp_bank_guarantee = $this->_db->select()
			->from("TEMP_BANK_GUARANTEE")
			->where('DOCUMENT_ID = ? ', $param['data']['document_id'])
			->query()->fetch();
		// ->where('DOCUMENT_ID = ? ', "96a9d028-47a6-4e29-b0f6-b8e2af140967");

		unset($get_temp_bank_guarantee["NTU_COUNTING"]);
		if (array_key_exists("SAVE_LINK", $get_temp_bank_guarantee)) {
			unset($get_temp_bank_guarantee["SAVE_LINK"]);
		}

		// $currentDate = date("ymd");
		// $seqNumber   = mt_rand(1111, 9999);
		// $bg_number_gen = strval($get_temp_bank_guarantee["BG_BRANCH"]) . strval($currentDate) . strval($get_temp_bank_guarantee["USAGE_PURPOSE"]) . strval($seqNumber);

		$get_temp_bank_guarantee["BG_ISSUED"] = new Zend_Db_Expr("now()");

		// insert history

		$getGuaranteeSign = $this->_db->select()
			->from("T_BANK_GUARANTEE_SIGNER")
			->where('DOCUMENT_ID = ? ', $param['data']['document_id'])
			->where('SIGNER_TYPE = ? ', 2)
			->query()->fetch();

		$historyInsert = array(
			'DATE_TIME'         => new Zend_Db_Expr("now()"),
			'BG_REG_NUMBER'         => $get_temp_bank_guarantee["BG_REG_NUMBER"],
			'CUST_ID'           => 'BANK',
			'USER_LOGIN'        => $get_temp_bank_guarantee['TTD2_USERID'],
			'HISTORY_STATUS'    => 13
		);
		$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
		//

		if ($get_temp_bank_guarantee["COUNTER_WARRANTY_TYPE"] == 3) {
			$get_detail_insurance = $this->_db->select()
				->from("TEMP_BANK_GUARANTEE_DETAIL")
				->where("BG_REG_NUMBER = ?", $get_temp_bank_guarantee["BG_REG_NUMBER"])
				->where("PS_FIELDNAME = ?", "Insurance Name")
				->query()->fetch();

			$get_detail_lf = $this->_db->select()
				->from("M_CUST_LINEFACILITY")
				->where("CUST_ID = ?", $get_detail_insurance["PS_FIELDVALUE"])
				->query()->fetch();

			$total = intval($get_detail_lf["DOC_DEADLINE"]);

			$Date1 = date("Y-m-d");
			$date = new DateTime($Date1);

			while ($total > 0) {
				$date->modify('+1 day');
				$Date2 = $date->format('Y-m-d');
				$check_weekend = date("N", strtotime($Date2));

				if ($check_weekend < 6) {
					$total--;
				}
			}

			$get_temp_bank_guarantee["BG_CG_DEADLINE"] = $Date2;
			// $get_temp_bank_guarantee["BG_CG_DEADLINE"] = date("Y-m-d", strtotime(date("Y-m-d") . " + " . ($get_detail_lf["DOC_DEADLINE"] - 1) . " days"));
		}

		$get_temp_bank_guarantee["BG_STATUS"] = "15";
		$get_temp_bank_guarantee["COMPLETION_STATUS"] = "2";

		$get_temp_bank_guarantee["REBATE_STATUS"] = $get_temp_bank_guarantee["REDEBATE_STATUS"];
		$get_temp_bank_guarantee["REBATE_TYPE"] = $get_temp_bank_guarantee["REDEBATE_TYPE"];
		$get_temp_bank_guarantee["REBATE_AMOUNT"] = $get_temp_bank_guarantee["REDEBATE_AMOUNT"];
		unset($get_temp_bank_guarantee["REDEBATE_STATUS"]);
		unset($get_temp_bank_guarantee["REDEBATE_TYPE"]);
		unset($get_temp_bank_guarantee["REDEBATE_AMOUNT"]);
		unset($get_temp_bank_guarantee["IS_TRANSFER_COA"]);
		unset($get_temp_bank_guarantee["TRANSFER_COA_TIMEOUT"]);
		unset($get_temp_bank_guarantee["IS_VALID"]);
		unset($get_temp_bank_guarantee["IS_TRANSFER_INS"]);

		// die();

		$this->_db->insert("T_BANK_GUARANTEE", $get_temp_bank_guarantee);

		// move from temp to t detail
		$get_temp_bank_guarantee_detail = $this->_db->select()
			->from("TEMP_BANK_GUARANTEE_DETAIL")
			->where('BG_REG_NUMBER = ? ', $get_temp_bank_guarantee["BG_REG_NUMBER"])
			->query()->fetchAll();

		foreach ($get_temp_bank_guarantee_detail as $value) {
			$this->_db->insert("T_BANK_GUARANTEE_DETAIL", $value);
		}

		// move from temp to t file
		$get_temp_bank_guarantee_file = $this->_db->select()
			->from("TEMP_BANK_GUARANTEE_FILE")
			->where('BG_REG_NUMBER = ? ', $get_temp_bank_guarantee["BG_REG_NUMBER"])
			->query()->fetchAll();

		foreach ($get_temp_bank_guarantee_file as $value) {
			$value["BG_NUMBER"] = $get_temp_bank_guarantee["BG_NUMBER"];
			$this->_db->insert("T_BANK_GUARANTEE_FILE", $value);
		}

		// move from temp to t split
		$get_temp_bank_guarantee_split = $this->_db->select()
			->from("TEMP_BANK_GUARANTEE_SPLIT")
			->where('BG_REG_NUMBER = ? ', $get_temp_bank_guarantee["BG_REG_NUMBER"])
			->query()->fetchAll();

		if (!empty($get_temp_bank_guarantee_split)) {
			foreach ($get_temp_bank_guarantee_split as $value) {
				$value["BG_NUMBER"] = $get_temp_bank_guarantee["BG_NUMBER"];
				unset($value["IS_TRANSFER"]);
				$this->_db->insert("T_BANK_GUARANTEE_SPLIT", $value);
			}
		}

		// move from TEMP_BANK_GUANRANTEE_PRELIMINARY to T
		$get_temp_bank_guarantee_preliminary = $this->_db->select()
			->from("TEMP_BANK_GUARANTEE_PRELIMINARY")
			->where('BG_REG_NUMBER = ? ', $get_temp_bank_guarantee["BG_REG_NUMBER"])
			->query()->fetch();

		$this->_db->insert('T_BANK_GUARANTEE_PRELIMINARY', $get_temp_bank_guarantee_preliminary);


		// move from TEMP_BANK_GUANRANTEE_PRELIMINARY_MEMBER to T
		$get_temp_bank_guarantee_preliminary_member = $this->_db->select()
			->from("TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER")
			->where('BG_REG_NUMBER = ? ', $get_temp_bank_guarantee["BG_REG_NUMBER"])
			->query()->fetchAll();

		foreach ($get_temp_bank_guarantee_preliminary_member as $member) {
			$this->_db->insert('T_BANK_GUARANTEE_PRELIMINARY_MEMBER', $member);
		}


		// last delete all from table temp
		// $this->_db->delete("TEMP_BANK_GUARANTEE", [
		// 	'DOCUMENT_ID = ? ' => $param['data']['document_id']
		// ]);

		// $this->_db->delete("TEMP_BANK_GUARANTEE_DETAIL", [
		// 	'BG_REG_NUMBER = ? ' => $get_temp_bank_guarantee["BG_REG_NUMBER"]
		// ]);

		// $this->_db->delete("TEMP_BANK_GUARANTEE_FILE", [
		// 	'BG_REG_NUMBER = ? ' => $get_temp_bank_guarantee["BG_REG_NUMBER"]
		// ]);

		// $this->_db->delete("TEMP_BANK_GUARANTEE_SPLIT", [
		// 	'BG_REG_NUMBER = ? ' => $get_temp_bank_guarantee["BG_REG_NUMBER"]
		// ]);


		// send email
		$Settings = new Settings();
		$allSetting = $Settings->getAllSetting();
		$getEmailTemplate = $allSetting['bemailtemplate_onrisk_obligee'];

		$data = [
			'[[obligee_name]]' => $get_temp_bank_guarantee["RECIPIENT_NAME"],
			'[[contact_obligee_name]]' => $get_temp_bank_guarantee["RECIPIENT_CP"],
			'[[master_bank_name]]' => $allSetting["master_bank_name"],
			'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_bank_email]]' => $allSetting["master_bank_email"],
			'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
			'[[bg_number]]' => $get_temp_bank_guarantee["BG_NUMBER"],
			'[[bg_subject]]' => $get_temp_bank_guarantee["BG_SUBJECT"],
			// '[[bg_issued_date]]' => date("d M Y", strtotime($get_temp_bank_guarantee["BG_ISSUED"])),
			'[[bg_issued_date]]' => date("d M Y"),
			'[[bg_status]]' => "ON RISK",
			'[[kode_cek_bg]]' => "123456",
			'[[app_url_link]]' => "https://garansi.btn.co.id/bgchecking?bgnumber=" . $get_temp_bank_guarantee["BG_NUMBER"],
		];

		$getEmailTemplate = strtr($getEmailTemplate, $data);

		Application_Helper_Email::sendEmail($get_temp_bank_guarantee["RECIPIENT_EMAIL"], 'BG Onrisk Notification', $getEmailTemplate);

		// send email cabang

		$get_branch = $this->_db->select()
			->from("M_BRANCH")
			->where('BRANCH_CODE = ? ', $get_temp_bank_guarantee["BG_BRANCH"])
			->query()->fetch();

		$Settings = new Settings();
		$allSetting = $Settings->getAllSetting();
		$getEmailTemplate = $allSetting['bemailtemplate_onrisk_obligee'];

		$data = [
			'[[obligee_name]]' => $get_temp_bank_guarantee["RECIPIENT_NAME"],
			'[[contact_obligee_name]]' => $get_temp_bank_guarantee["RECIPIENT_CP"],
			'[[master_bank_name]]' => $allSetting["master_bank_name"],
			'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_bank_email]]' => $allSetting["master_bank_email"],
			'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
			'[[bg_number]]' => $get_temp_bank_guarantee["BG_NUMBER"],
			'[[bg_subject]]' => $get_temp_bank_guarantee["BG_SUBJECT"],
			// '[[bg_issued_date]]' => date("d M Y", strtotime($get_temp_bank_guarantee["BG_ISSUED"])),
			'[[bg_issued_date]]' => date("d M Y"),
			'[[bg_status]]' => "ON RISK",
			'[[kode_cek_bg]]' => "123456",
			'[[app_url_link]]' => "https://garansi.btn.co.id/bgchecking?bgnumber=" . $get_temp_bank_guarantee["BG_NUMBER"],
		];

		$getEmailTemplate = strtr($getEmailTemplate, $data);

		Application_Helper_Email::sendEmail($get_branch["BRANCH_EMAIL"], 'BG Onrisk Notification', $getEmailTemplate);
	}

	public function dumpAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$reppkDetailService = new Service_Account('16101320003108', '');
		$reppkDetailAcct = $reppkDetailService->inquiryAccountBalance();
		$reppkInfoAcct = $reppkDetailService->inquiryAccontInfo();

		Zend_Debug::dump($reppkDetailAcct['available_balance']);
		die();

		$db = Zend_Db_Table::getDefaultAdapter();

		$svcAccount = new Service_Account('0015901500055306', 'IDR');

		$selectAllLockSaving = $svcAccount->inqLockSaving();

		$filterLockSaving = array_shift(array_filter($selectAllLockSaving['lock'], function ($item) {
			return $item['hold_by_branch'] == '159' && $item['sequence_number'] == '26';
		}));

		Zend_Debug::dump($filterLockSaving);

		if ($filterLockSaving) {
			$db->update('M_MARGINALDEPOSIT_DETAIL', [
				'GUARANTEE_AMOUNT' => round(floatval($filterLockSaving['amount'] / 100), 2, PHP_ROUND_HALF_UP)
			], [
				'MD_ACCT = ?' => '0015901500055306',
				'CUST_ID = ?' => 'ASKRINDO01',
				'HOLD_SEQUENCE = ?' => $filterLockSaving['sequence_number']
			]);
		}

		die('asd');


		$inquiryAccountCif = new Service_Account('0006101500107025', 'IDR'); // 201400113601, 201400113588, 201400113619
		$saveTempCif = $inquiryAccountCif->lockSaving(['balance' => '3000000']);

		Zend_Debug::dump($saveTempCif);
		die();

		$AESMYSQL = new Crypt_AESMYSQL();

		Zend_Debug::dump($AESMYSQL->decrypt(urldecode('Q0pLZFBJdDcrM3RPU2FtZjBGZDV3dz09OjpldTtGx1RgCoe%2FNkfULkj1'), uniqid()));
		die();

		// $get_temp_bank_guarantee_detail = $this->_db->select()
		// 	->from("TEMP_BANK_GUARANTEE")
		// 	->where('BG_REG_NUMBER = ? ', '925716101F4201')
		// 	->query()->fetch();

		// $time1 = date_create($get_temp_bank_guarantee_detail['TIME_PERIOD_START']);
		// $time2 = date_create($get_temp_bank_guarantee_detail['TIME_PERIOD_END']);

		// $saveDiff = date_diff($time1, $time2);
		// echo "<pre>";
		// var_dump(ucwords(strtolower('JAMINAN KUSD JKSDH')));
		// die();

		// $_SERVER['REQUEST_METHOD'] === 'POST'
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			// echo "<pre>";
			// var_dump(apache_request_headers());
			// var_dump($_POST);
			// var_dump($_REQUEST);

			echo json_encode($_POST);
			// echo urldecode(file_get_contents('php://input'));
			return;
			// var_dump(urldecode(file_get_contents('php://input')));

			die();
		}

		// die();
		$inquiryAccountCif = new Service_Account('0000201400006799', 'IDR'); // 201400113601, 201400113588, 201400113619
		// $saveTempCif = $inquiryAccountCif->lockSaving(['balance' => '100000']);
		// $saveTempCif = $inquiryAccountCif->inqLockSaving();
		// $saveTempCif = $inquiryAccountCif->unlockSaving([
		// 	'branch_code' => '14',
		// 	'sequence_number' => '2'
		// ]);


		// deposito
		// $saveTempCif = $inquiryAccountCif->lockDeposito(['balance' => '100000000']);
		$inqDepo = $inquiryAccountCif->inquiryLockDeposito();
		$saveTempCif = $inquiryAccountCif->unlockDeposito('T', $inqDepo['hold_by_branch'], $inqDepo['sequence_number']);

		echo "<pre>";
		var_dump($inqDepo);
		die();

		// $saveTempCif = $inquiryAccountCif->inquiryAccontInfo('AB', FALSE)['cif'];

		$inquiryAccountCif = new Service_Account($saveTempCif, 'IDR');
		$saveTempCif = $inquiryAccountCif->createAccount('003', 'K2');

		$this->dd($saveTempCif);

		die();

		// $Settings = new Settings();
		// $allSetting = $Settings->getAllSetting();

		// $this->dd(file_exists(APPLICATION_PATH . '/../../library/data/uploads/document/submit/61fa5e38c0390-rptsp2dgaji.pdfs'));

		$svcAccount = new Service_Account('9401500202216', 'IDR', null, null, null, null, 'test');
		$data = [
			// 'branch_code' => str_pad(16, 3, '0', STR_PAD_LEFT),
			'branch_code' => '94',
			'balance' => '100000',
			'sequence_number' => '4'
		];
		// $test = $svcAccount->lockSaving($data);
		// $test = $svcAccount->inqLockSaving($data);
		$test = $svcAccount->unlockSaving($data);

		$this->dd($test);
	}

	public function inqlocksavingAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acctNo  = $this->_getParam('acct');

		$serviceCall = new Service_Account($acctNo, 'IDR'); // 201400113601, 201400113588, 201400113619
		$saveTempCif = $serviceCall->inqLockSaving();

		echo "<pre>";
		var_dump($saveTempCif);
		die();
	}

	public function inqlockdepositoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acctNo  = $this->_getParam('acct');

		$serviceCall = new Service_Account($acctNo, 'IDR'); // 201400113601, 201400113588, 201400113619
		$saveTempCif = $serviceCall->inquiryLockDeposito();

		Zend_Debug::dump($saveTempCif);
		die();

		echo "<pre>";
		var_dump($saveTempCif);
		die();
	}

	public function unlockdepoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// $clientUser = new SGO_Soap_ClientUser();
		// $requestopen = array();
		// $requestopen['cif'] = 'AE30481';
		// $requestopen['branch_code'] = '001';
		// $requestopen['product_code'] = 'K2';
		// $requestopen['currency'] = 'IDR';

		// $clientUser->callapi('createaccount', null, $requestopen);
		// $reppkResult = $clientUser->getResult();

		// Zend_Debug::dump($reppkResult);
		// die('createaccount');

		// $depositoDisrburseService =  new Service_Account('4801400001664', '');
		// $resultDepoDisburse = $depositoDisrburseService->depositoDisbursement('IDR', '4801400001664', 'NIKI YUNIAR WICAKSON', 'IDR', '101320001354', 'ANASABAH AE30481 LAL', '25000000', '10000000', '10000000', '10000000', '10000000', "Disb T to REPPK", 'test');

		// Zend_Debug::dump($resultDepoDisburse);
		// die('depodisburse');

		// $callServiceRunningNumber = new Service_Account('', '');
		// $runningNumberResult = $callServiceRunningNumber->inquiryRunningNumber([
		// 	'type' => 'SKNGN'
		// ]);
		// $remittenceProduct = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['remittance_product'] : '00';
		// $runningNumber = ($runningNumberResult['response_code'] == '0000') ? $runningNumberResult['running_number'] : '00';

		// $remittenceString = implode('', ['001', '01', $remittenceProduct, str_pad($runningNumber, 7, '0', STR_PAD_LEFT)]);

		// $transferData = [
		// 	'transfer_type' => '1',
		// 	'remittance' => $remittenceString,
		// 	'source_account_number' => '101320001354',
		// 	'source_account_name' => 'ANASABAH AE30481 LAL',
		// 	'source_address' => 'KEBONAGUNG',
		// 	'beneficiary_bank_code' => 'CENAIDJA',
		// 	'beneficiary_account_number' => '12341231231',
		// 	'beneficiary_account_name' => 'Ahmad Hilmi',
		// 	'beneficiary_address' => 'Jalan test',
		// 	'amount' => 10000000,
		// 	'remark1' => 'CLOSE REF NUMBER',
		// 	'remark2' => 'x',
		// 	'remark3' => 'x',
		// 	'fee' => 2900,
		// 	'account_fee' => '1601306666109',
		// ];

		// $transferOtherService =  new Service_Account('', '');
		// $result = $transferOtherService->transferDomestic($transferData);

		// Zend_Debug::dump($result);
		// die('transfer domestic');

		$acct = $this->_request->getParam('acct');

		$inquiryAccountCif = new Service_Account($acct, 'IDR'); // 201400113601, 201400113588, 201400113619

		// deposito
		$inqDepo = $inquiryAccountCif->inquiryLockDeposito();

		if ($inqDepo && $inqDepo['response_code'] && $inqDepo['response_code'] != '0000') {
			header('Content-Type: application/json');
			$resposne = [
				'message' => $inqDepo['response_desc']
			];
			echo json_encode($resposne);

			die();
			return;
		}

		if (!$inqDepo) {
			header('Content-Type: application/json');
			$resposne = [
				'message' => 'rekening tidak ter lock'
			];
			echo json_encode($resposne);
			return;
		}

		$saveTempCif = $inquiryAccountCif->unlockDeposito('T', $inqDepo['hold_by_branch'], $inqDepo['sequence_number']);

		header('Content-Type: application/json');
		$resposne = [
			'message' => $saveTempCif
		];
		echo json_encode($resposne);
		return;
	}

	public function lockdepoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();


		$param = [
			"SOURCE_ACCOUNT_NUMBER" => '1601306666109',
			"SOURCE_ACCOUNT_NAME" => 'ANASABAH AE30481 LAL',
			"BENEFICIARY_ACCOUNT_NUMBER" => '16101320002835',
			"BENEFICIARY_ACCOUNT_NAME" => 'NASABAH S329737',
			// "BENEFICIARY_ACCOUNT_NUMBER" => '16101320001643',
			// "BENEFICIARY_ACCOUNT_NAME" => 'ANASABAH AE30481 LAL',
			"PHONE_NUMBER_FROM" =>  '081299997777',
			"AMOUNT" => 1000000,
			"PHONE_NUMBER_TO" =>  '085645057334',
			"RATE1" => 0,
			"RATE2" => 0,
			"RATE3" => 0,
			"RATE4" => 0,
			"DESCRIPTION" => null,
			"DESCRIPTION_DETAIL" => null,
		];

		$service =  new Service_TransferWithin($param);
		$response = $service->sendTransfer();

		Zend_Debug::dump($response);
		die('transfer inhouse');

		$inquiryAccountCif = new Service_Account('0006101500107025', 'IDR'); // 201400113601, 201400113588, 201400113619
		$saveTempCif1 = $inquiryAccountCif->lockSaving(['balance' => '3000000']);

		// $inquiryAccountCif = new Service_Account('1601500750611', 'IDR'); // 201400113601, 201400113588, 201400113619
		// $saveTempCif2 = $inquiryAccountCif->lockSaving(['balance' => '40000000']);

		Zend_Debug::dump($saveTempCif1);
		// Zend_Debug::dump($saveTempCif2);
		die('lock saving');

		$acct = $this->_request->getParam('acct');

		$inquiryAccountCif = new Service_Account($acct, 'IDR'); // 201400113601, 201400113588, 201400113619

		// deposito
		$checkAcct = $inquiryAccountCif->inquiryDeposito();

		if (strtolower($checkAcct['hold_description']) == 'y') {

			header('Content-Type: application/json');
			$resposne = [
				'message' => 'rekening sudah ter lock'
			];
			echo json_encode($resposne);

			die();
		}

		$lockDepo = $inquiryAccountCif->lockDeposito(['balance' => $checkAcct['original_balance']]);

		if ($lockDepo && $lockDepo['response_code'] && $lockDepo['response_code'] != '0000') {
			header('Content-Type: application/json');
			$resposne = [
				'message' => $lockDepo['response_desc']
			];
			echo json_encode($resposne);

			die();
		}

		if (!$lockDepo) {
			header('Content-Type: application/json');
			$resposne = [
				'message' => 'server didn\'t response'
			];
			echo json_encode($resposne);
			die();
		}

		header('Content-Type: application/json');
		$resposne = [
			'message' => $lockDepo
		];
		echo json_encode($resposne);
		die();
	}

	function generateTransactionID($paymentRef, $counter = 0)
	{
		//		$sql_countTransaction = dbQuery("select count(transaction_id) as countTransaction from sb_transaction where ps_number = '{$paymentID}'");
		//		$rs_countTransaction = tep_db_fetch_array($sql_countTransaction);

		$countTransaction = ($counter < 1) ? 1 : $counter + 1;
		$seqNumberTransaction = str_pad($countTransaction, ((strlen($countTransaction) == 1) ? 2 : ((0 + strlen($countTransaction)) - strlen($countTransaction))), "0", STR_PAD_LEFT);
		$transactionID = $paymentRef . $seqNumberTransaction;

		return $transactionID;
	}

	function generateCloseNumber($kode)
	{

		$currentDate = date("ymd");
		$length = 4;
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[random_int(0, $charactersLength - 1)];
		}

		$trxId = $kode . $currentDate . $randomString;

		return $trxId;
	}

	function runprogAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$db = Zend_Db_Table::getDefaultAdapter();

		$x = 0;
		$generateCloseRefNumber = $this->generateCloseNumber("TK");

		$data = $db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->where("A.BG_STATUS = 15")
			->where("DATE(A.BG_CLAIM_DATE) < DATE(NOW())")
			->where("A.BG_NUMBER NOT IN (SELECT B.BG_NUMBER FROM TEMP_BANK_GUARANTEE_CLOSE B WHERE B.SUGGESTION_STATUS NOT IN (7,11) AND B.CHANGE_TYPE != 3)")
			->query()->fetchAll();

		$saveTransaction = [];

		if ($data) {

			foreach ($data as $row) {

				try {
					//$db->beginTransaction();

					// $dataUpdate  = ['BG_STATUS' => 16, 'CLOSING_TYPE' => 2, 'CLOSE_REF_NUMBER' => $generateCloseRefNumber, 'BG_OFFRISK_DATE' => $row['BG_CLAIM_DATE'], 'BG_UPDATED' => new Zend_Db_Expr('now()'), 'BG_UPDATEDBY' => 'SYSTEM'];
					// $whereUpdate = ['BG_NUMBER = ?' => $row['BG_NUMBER']];
					// $db->update('T_BANK_GUARANTEE', $dataUpdate, $whereUpdate);

					// $dataUpdateTemp  = ['SUGGESTION_STATUS' => 11, 'LASTUPDATEDFROM' => '-', 'LASTUPDATEDBY' => 'SYSTEM'];
					// $whereUpdateTemp = ['BG_NUMBER = ?' => $row['BG_NUMBER']];
					// $db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataUpdateTemp, $whereUpdateTemp);

					// $historyInsert = array(
					// 	'DATE_TIME'         => new Zend_Db_Expr("now()"),
					// 	'CLOSE_REF_NUMBER'        => $generateCloseRefNumber,
					// 	'BG_NUMBER'        => $row['BG_NUMBER'],
					// 	'USER_FROM'        => 'SYSTEM',
					// 	'BG_REASON'        => 'Penutupan Tanpa Klaim',
					// 	'HISTORY_STATUS'        => 11, //pengajuan dibatalkan
					// );

					// $db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

					$dataAcct = $db->select()
						->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
						->where("A.BG_NUMBER = ?", $row['BG_NUMBER'])
						->query()->fetchAll();

					$haveEscrow = false;

					if ($dataAcct) {
						$no = 1;
						foreach ($dataAcct as $rowAcct) {
							if ($rowAcct['ACCT_DESC'] != 'Escrow') {
								$svcAccount = new Service_Account($rowAcct['ACCT'], null);
								$result = $svcAccount->inquiryAccountBalance();

								if ($result['response_code'] == '0000') {
									$unlockSaving = $svcAccount->unlockSaving([
										'branch_code' => $rowAcct['HOLD_BY_BRANCH'],
										'sequence_number' => $rowAcct['HOLD_SEQUENCE']
									]);

									//var_dump($unlockSaving);

									if ($unlockSaving['response_code'] == "0000") {
										$statusService = true;

										$transactionID = $this->generateTransactionID($generateCloseRefNumber, $no++);
										$insertBGTransaction = array(
											'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
											'TRANSACTION_ID' => $transactionID,
											'BG_NUMBER' => $rowAcct['BG_NUMBER'],
											'SERVICE' => 3,
											'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
											'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
											'SOURCE_ACCT_CCY' => 'IDR',
											'SOURCE_ACCT' => $result['account_number'],
											'SOURCE_ACCT_NAME' => $result['account_name'],
											'SOURCE_ACCT_TYPE' => $result['account_type'],
											'SOURCE_PRODUCT_TYPE' => $result['product_type'],
											'TRA_CCY' => 'IDR',
											'TRA_AMOUNT' => $rowAcct['AMOUNT'],
											'REMARKS1' => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
											'TRA_STATUS' => 1,
											'BANK_RESPONSE' => $unlockSaving["response_code"] . " - " . $unlockSaving["response_desc"],
											'UUID' => $unlockSaving["uuid"],
										);

										$saveTransaction[] = [
											'tableName' => 'T_BG_TRANSACTION',
											'data' => $insertBGTransaction
										];

										$historyServiceInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'CLOSE_REF_NUMBER'        =>  $transactionID,
											'BG_NUMBER'        => $row['BG_NUMBER'],
											'CUST_ID'        => $row['CUST_ID'],
											'USER_FROM'        => 'SYSTEM',
											'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
											'HISTORY_STATUS'        => 12, //penutupan tanpa claim
										);

										$saveTransaction[] = [
											'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
											'data' => $historyServiceInsert
										];
									} else if ($unlockSaving['response_code'] == "9999") {
										$statusService = false;
										$transactionID = $this->generateTransactionID($generateCloseRefNumber, $no++);
										$insertBGTransaction = array(
											'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
											'TRANSACTION_ID' => $transactionID,
											'BG_NUMBER' => $rowAcct['BG_NUMBER'],
											'SERVICE' => 3,
											'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
											'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
											'SOURCE_ACCT_CCY' => 'IDR',
											'SOURCE_ACCT' => $result['account_number'],
											'SOURCE_ACCT_NAME' => $result['account_name'],
											'SOURCE_ACCT_TYPE' => $result['account_type'],
											'SOURCE_PRODUCT_TYPE' => $result['product_type'],
											'TRA_CCY' => 'IDR',
											'TRA_AMOUNT' => $rowAcct['AMOUNT'],
											'REMARKS1' => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
											'TRA_STATUS' => 3,
											'BANK_RESPONSE' => $unlockSaving["response_code"] . " - " . $unlockSaving["response_desc"],
											'UUID' => $unlockSaving["uuid"],
										);

										$saveTransaction[] = [
											'tableName' => 'T_BG_TRANSACTION',
											'data' => $insertBGTransaction
										];

										$historyServiceInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'CLOSE_REF_NUMBER'        =>  $transactionID,
											'BG_NUMBER'        => $row['BG_NUMBER'],
											'CUST_ID'        => $row['CUST_ID'],
											'USER_FROM'        => 'SYSTEM',
											'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
											'HISTORY_STATUS'        => 12, //penutupan tanpa claim
										);

										$saveTransaction[] = [
											'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
											'data' => $historyServiceInsert
										];
									} else {
										$statusService = false;

										$transactionID = $this->generateTransactionID($generateCloseRefNumber, $no++);
										$insertBGTransaction = array(
											'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
											'TRANSACTION_ID' => $transactionID,
											'BG_NUMBER' => $rowAcct['BG_NUMBER'],
											'SERVICE' => 3,
											'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
											'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
											'SOURCE_ACCT_CCY' => 'IDR',
											'SOURCE_ACCT' => $result['account_number'],
											'SOURCE_ACCT_NAME' => $result['account_name'],
											'SOURCE_ACCT_TYPE' => $result['account_type'],
											'SOURCE_PRODUCT_TYPE' => $result['product_type'],
											'TRA_CCY' => 'IDR',
											'TRA_AMOUNT' => $rowAcct['AMOUNT'],
											'REMARKS1' => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
											'TRA_STATUS' => 4,
											'BANK_RESPONSE' => $unlockSaving["response_code"] . " - " . $unlockSaving["response_desc"],
											'UUID' => $unlockSaving["uuid"],
										);

										$saveTransaction[] = [
											'tableName' => 'T_BG_TRANSACTION',
											'data' => $insertBGTransaction
										];

										$historyServiceInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'CLOSE_REF_NUMBER'        =>  $transactionID,
											'BG_NUMBER'        => $row['BG_NUMBER'],
											'CUST_ID'        => $row['CUST_ID'],
											'USER_FROM'        => 'SYSTEM',
											'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
											'HISTORY_STATUS'        => 12, //penutupan tanpa claim
										);

										$saveTransaction[] = [
											'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
											'data' => $historyServiceInsert
										];
									}
								}

								if ($result['response_code'] != '0000') {

									$result2 = $svcAccount->inquiryDeposito();

									$unlockDeposito = $svcAccount->unlockDeposito('T', $rowAcct['HOLD_BY_BRANCH'], $rowAcct['HOLD_SEQUENCE']);

									if ($unlockDeposito['response_code'] == "0000") {
										$statusService = true;

										$transactionID = $this->generateTransactionID($generateCloseRefNumber, $no++);
										$insertBGTransaction = array(
											'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
											'TRANSACTION_ID'        => $transactionID,
											'BG_NUMBER'        => $rowAcct['BG_NUMBER'],
											'SERVICE'        => 2,
											'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
											'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
											'SOURCE_ACCT_CCY'        => 'IDR',
											'SOURCE_ACCT'        => $result2['account_number'],
											'SOURCE_ACCT_NAME'        => $result2['name'],
											'SOURCE_ACCT_TYPE'        => $result2['account_type'],
											'SOURCE_PRODUCT_TYPE'        => $result2['product_type'],
											'TRA_CCY'        => 'IDR',
											'TRA_AMOUNT' => $rowAcct['AMOUNT'],
											'REMARKS1'        => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
											'TRA_STATUS' => 1,
											'BANK_RESPONSE' => $unlockDeposito["response_code"] . " - " . $unlockDeposito["response_desc"],
											'UUID' => $unlockDeposito["uuid"],
										);

										$saveTransaction[] = [
											'tableName' => 'T_BG_TRANSACTION',
											'data' => $insertBGTransaction
										];

										$historyServiceInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'CLOSE_REF_NUMBER'        =>  $transactionID,
											'BG_NUMBER'        => $row['BG_NUMBER'],
											'CUST_ID'        => $row['CUST_ID'],
											'USER_FROM'        => 'SYSTEM',
											'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
											'HISTORY_STATUS'        => 12, //penutupan tanpa claim
										);

										$saveTransaction[] = [
											'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
											'data' => $historyServiceInsert
										];
									} else if ($unlockDeposito['response_code'] == "9999") {
										$statusService = false;

										$transactionID = $this->generateTransactionID($generateCloseRefNumber, $no++);
										$insertBGTransaction = array(
											'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
											'TRANSACTION_ID'        => $transactionID,
											'BG_NUMBER'        => $rowAcct['BG_NUMBER'],
											'SERVICE'        => 2,
											'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
											'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
											'SOURCE_ACCT_CCY'        => 'IDR',
											'SOURCE_ACCT'        => $result2['account_number'],
											'SOURCE_ACCT_NAME'        => $result2['name'],
											'SOURCE_ACCT_TYPE'        => $result2['account_type'],
											'SOURCE_PRODUCT_TYPE'        => $result2['product_type'],
											'TRA_CCY'        => 'IDR',
											'TRA_AMOUNT' => $rowAcct['AMOUNT'],
											'REMARKS1'        => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
											'TRA_STATUS' => 3,
											'BANK_RESPONSE' => $unlockDeposito["response_code"] . " - " . $unlockDeposito["response_desc"],
											'UUID' => $unlockDeposito["uuid"],
										);

										$saveTransaction[] = [
											'tableName' => 'T_BG_TRANSACTION',
											'data' => $insertBGTransaction
										];

										$historyServiceInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'CLOSE_REF_NUMBER'        =>  $transactionID,
											'BG_NUMBER'        => $row['BG_NUMBER'],
											'CUST_ID'        => $row['CUST_ID'],
											'USER_FROM'        => 'SYSTEM',
											'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
											'HISTORY_STATUS'        => 12, //penutupan tanpa claim
										);

										$saveTransaction[] = [
											'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
											'data' => $historyServiceInsert
										];
									} else {
										$statusService = false;
										$transactionID = $this->generateTransactionID($generateCloseRefNumber, $no++);
										$insertBGTransaction = array(
											'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
											'TRANSACTION_ID'        => $transactionID,
											'BG_NUMBER'        => $rowAcct['BG_NUMBER'],
											'SERVICE'        => 2,
											'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
											'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
											'SOURCE_ACCT_CCY'        => 'IDR',
											'SOURCE_ACCT'        => $result2['account_number'],
											'SOURCE_ACCT_NAME'        => $result2['name'],
											'SOURCE_ACCT_TYPE'        => $result2['account_type'],
											'SOURCE_PRODUCT_TYPE'        => $result2['product_type'],
											'TRA_CCY'        => 'IDR',
											'TRA_AMOUNT' => $rowAcct['AMOUNT'],
											'REMARKS1'        => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
											'TRA_STATUS' => 4,
											'BANK_RESPONSE' => $unlockDeposito["response_code"] . " - " . $unlockDeposito["response_desc"],
											'UUID' => $unlockDeposito["uuid"],
										);

										$saveTransaction[] = [
											'tableName' => 'T_BG_TRANSACTION',
											'data' => $insertBGTransaction
										];

										$historyServiceInsert = array(
											'DATE_TIME'         => new Zend_Db_Expr("now()"),
											'CLOSE_REF_NUMBER'        =>  $transactionID,
											'BG_NUMBER'        => $row['BG_NUMBER'],
											'CUST_ID'        => $row['CUST_ID'],
											'USER_FROM'        => 'SYSTEM',
											'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
											'HISTORY_STATUS'        => 12, //penutupan tanpa claim
										);

										$saveTransaction[] = [
											'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
											'data' => $historyServiceInsert
										];
									}
								}
							} else {
								$haveEscrow = true;
							}
						}

						if (!$haveEscrow) {
							if ($statusService == true) {
								$insertBGPslip = array(
									'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
									'BG_NUMBER'        => $row['BG_NUMBER'],
									'PS_CONFIRMED'        => new Zend_Db_Expr("now()"),
									'PS_CONFIRMEDBY'        => 'SYSTEM',
									'PS_APPROVED'        => new Zend_Db_Expr("now()"),
									'PS_APPROVEDBY'        => 'SYSTEM',
									'PS_EFDATE'        => $row['BG_CLAIM_DATE'],
									'PS_STATUS'        => '5', // Proses Selesai
									'TYPE'        => '2', // Tanpa Claim
								);

								$saveTransaction[] = [
									'tableName' => 'T_BG_PSLIP',
									'data' => $insertBGPslip
								];
							} else {
								$insertBGPslip = array(
									'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
									'BG_NUMBER'        => $row['BG_NUMBER'],
									'PS_CONFIRMED'        => new Zend_Db_Expr("now()"),
									'PS_CONFIRMEDBY'        => 'SYSTEM',
									'PS_APPROVED'        => new Zend_Db_Expr("now()"),
									'PS_APPROVEDBY'        => 'SYSTEM',
									'PS_EFDATE'        => $row['BG_CLAIM_DATE'],
									'PS_STATUS'        => '4', //Service Dalam Proses
									'TYPE'        => '2', // Tanpa Claim
								);

								$saveTransaction[] = [
									'tableName' => 'T_BG_PSLIP',
									'data' => $insertBGPslip
								];
							}
						}
					}

					// BEGIN SEND EMAIL
					$Settings = new Settings();
					$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
					$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
					$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
					$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
					$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
					$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
					$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
					$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
					$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
					$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
					$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
					$templateEmailMasterBankWapp = $Settings->getSetting('master_bank_wapp');
					$url_bo  = $Settings->getSetting('url_bo');
					$template = $Settings->getSetting('femailtemplate_closebg');
					$email_grup_bts = $Settings->getSetting('email_grup_bts');
					$email_grup_cstd = $Settings->getSetting('email_grup_cstd');
					$email_grup_ficd = $Settings->getSetting('email_grup_ficd');

					$translate = array(
						'[[close_ref_number]]' => $generateCloseRefNumber,
						'[[bg_number]]' => $row['BG_NUMBER'],
						'[[bg_subject]]' => $row['BG_SUBJECT'],
						'[[bg_status]]' => 'Off Risk',
						'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
						'[[master_bank_app_name]]' 	=> $templateEmailMasterBankAppName
					);

					$mailContent = strtr($template, $translate);
					$subject = 'Notifikasi Penutupan Bank Garansi';

					//1. Principal (email kontak principal pada table T_BANK_GUARANTEE)
					$email_principal = $row['CUST_EMAIL'];

					if (!empty($data)) {
						$mail = Application_Helper_Email::sendEmail($email_principal, $subject, $mailContent);
					}
					//2. Obligee (email kontak obligee pada table T_BANK_GUARANTEE)
					$email_obligee = $row['RECIPIENT_EMAIL'];
					if (!empty($data)) {
						$mail = Application_Helper_Email::sendEmail($email_obligee, $subject, $mailContent);
					}

					//3. Kantor cabang penerbit (email cabang pada table M_BRANCH)
					$selectBranch	= $db->select()
						->from(
							array('A'	 		=> 'M_BRANCH'),
							array('BRANCH_EMAIL' => 'A.BRANCH_EMAIL')
						)
						->where("A.BRANCH_CODE = ?", $row['BG_BRANCH']);
					$email_cabang = $db->fetchOne($selectBranch);

					if (!empty($data)) {
						if (!empty($email_cabang)) {
							$mail = Application_Helper_Email::sendEmail($email_cabang, $subject, $mailContent);
						}
					}

					//4. Bila kontra Line Facility, email ke BTS Grup dan CSTD Grup (bts_email dan cstd_email pada table M_SETTING)

					if ($row['COUNTER_WARRANTY_TYPE'] == '2') {
						if (!empty($data)) {
							$mail = Application_Helper_Email::sendEmail($email_grup_cstd, $subject, $mailContent);
						}
					}

					//5. Bila kontra Asuransi, email ke BTS Grup dan FICD Grup (bts_email dan ficd_email pada table M_SETTING)
					if ($row['COUNTER_WARRANTY_TYPE'] == '3') {
						if (!empty($data)) {
							$mail = Application_Helper_Email::sendEmail($email_grup_ficd, $subject, $mailContent);
						}
					}

					if (!empty($data)) {
						$mail = Application_Helper_Email::sendEmail($email_grup_bts, $subject, $mailContent);
					}
					// END SEND EMAIL

					$x = 1;
					//$db->commit();		   
				} catch (Exception $e) {
					$db->rollBack();
				}
			}

			foreach ($saveTransaction as $key => $value) {
				$this->insertTable($value['tableName'], $value['data']);
			}
		}

		// simpan log cron
		echo 'AFFECTED ROW(S): BG_CLOSING (' . $x . ')' . PHP_EOL;
		Application_Helper_General::cronLog(basename(__FILE__), 'BG_CLOSING', '1');
	}

	function insertTable($tableName, $data)
	{
		$db = Zend_Db_Table::getDefaultAdapter();

		$db->beginTransaction();
		$db->insert($tableName, $data);
		$db->commit();
	}

	function errorAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer('error/error', null, true);

		$errors = $this->_getParam('error_handler');

		$select = $this->_db->select()
			->from(array('M_SETTING'), array('*'))
			->query()->fetchAll();
		$setting = Application_Helper_Array::listArray($select, 'SETTING_ID', 'SETTING_VALUE');
		// echo '<pre>';print_r($setting);
		$this->view->masterTitle = $setting['master_bank_app_name'];
		$this->view->masterEmail = $setting['master_bank_email'];
		$this->view->masterFax   = $setting['master_bank_fax'];
		$this->view->masterBankTelp   = $setting['master_bank_telp'];

		switch ($errors->type) {
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

				// 404 error -- controller or action not found
				$this->getResponse()->setHttpResponseCode(404);
				$this->view->message = 'Page not found';
				break;
			default:
				// application error
				$this->getResponse()->setHttpResponseCode(500);
				$this->view->message = 'Application error';
				break;
		}

		// Log exception, if logger available
		if ($log = $this->getLog()) {
			//$log->crit($this->view->message, $errors->exception);
			//$logFormat = date('r') . ', "%message%" (%priorityName%)' . PHP_EOL;
			//$simpleFormatter = new Zend_Log_Formatter_Simple($logFormat);
			//$log->setFormatter($simpleFormatter);
			switch ($errors->type) {
				case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
				case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
				case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
					// 404 error -- controller or action not found
					$priority = Zend_Log::WARN;
					break;
				default:
					// application error
					$priority = Zend_Log::ERR;
					break;
			}

			$requestParam = '';
			foreach ($errors->request->getParams() as $key => $val) {
				$requestParam .= $key . ':' . $val . ', ';
			}
			$requestParam = substr($requestParam, 0, -2);

			$log->log(
				$this->view->message . PHP_EOL .
					'Error Message: ' . $errors->exception->getMessage() . PHP_EOL .
					'Request Param: ' . $requestParam . PHP_EOL .
					'Stack Trace:' . PHP_EOL . $errors->exception->getTraceAsString() . PHP_EOL,
				$priority
			);
		}

		// conditionally display exceptions
		if ($this->getInvokeArg('displayExceptions') == true) {
			$this->view->exception = $errors->exception;
		}

		$this->view->request = $errors->request;

		echo "<pre>";
		var_dump($errors);
		echo "</pre>";
	}

	public function getLog()
	{
		$bootstrap = $this->getInvokeArg('bootstrap');
		if (!$bootstrap->hasPluginResource('Log')) {
			return false;
		}
		$log = $bootstrap->getResource('Log');
		return $log;
	}
}
